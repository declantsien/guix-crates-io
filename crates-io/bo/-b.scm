(define-module (crates-io bo -b) #:use-module (crates-io))

(define-public crate-bo-bin-0.4 (crate (name "bo-bin") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0.81") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "~0.3.22") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3.3.0") (default-features #t) (kind 2)) (crate-dep (name "termion") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1") (default-features #t) (kind 0)))) (hash "0v5mq29pm7nhk415yafaa8vn3bwd1dxdm13mvjfk0s3ahqmksw2v")))

