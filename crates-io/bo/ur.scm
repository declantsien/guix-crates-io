(define-module (crates-io bo ur) #:use-module (crates-io))

(define-public crate-bourbaki-0.0.0 (crate (name "bourbaki") (vers "0.0.0") (hash "0rmdr6ypjkia7vkhqvjrabn2dyw9pjjw78m2d1ms76qm9d4n1562")))

(define-public crate-bourse-book-0.1 (crate (name "bourse-book") (vers "0.1.0") (hash "151p2ldnn1hcvmr1gbxra2s8gfzs2ky13vrw1qrxnanab5bw2bv1")))

(define-public crate-bourse-book-0.1 (crate (name "bourse-book") (vers "0.1.1") (hash "0clsjrpdqbf7yn6rnkdviskksvlrga0j61z3gxmdan4k9aahrcc3")))

(define-public crate-bourse-book-0.1 (crate (name "bourse-book") (vers "0.1.2") (hash "1177aiackl63j6lday2hhfprskha0ihqjw3sk3kndx84916ymm6i")))

(define-public crate-bourse-book-0.2 (crate (name "bourse-book") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0adsglfrldhxm4pxgvf6jnr24i2gpw5xsz1q1gj6ys2fzavhwkja")))

(define-public crate-bourse-book-0.3 (crate (name "bourse-book") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0k6z42dq3n4vb2c28zclj3bc845ln00lfk8rc3ncmz9gr6l7azqq")))

(define-public crate-bourse-book-0.3 (crate (name "bourse-book") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0hqszh5yhp2v43l4w6pxmyxk5gsw7jf03m11fm4j5bmjvcd0s86n")))

(define-public crate-bourse-book-0.4 (crate (name "bourse-book") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0438zchnsb57gdg0a3ssdfyqw59a0y74ganvpkhbic0hwybh6cpi")))

(define-public crate-bourse-de-0.1 (crate (name "bourse-de") (vers "0.1.0") (deps (list (crate-dep (name "bourse-book") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bourse-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "kdam") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "18bcvj2inj04qbkrxbqv49siaxi8pnyb21ngay2mzfy92pybq434")))

(define-public crate-bourse-de-0.1 (crate (name "bourse-de") (vers "0.1.1") (deps (list (crate-dep (name "bourse-book") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "bourse-macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "kdam") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0hn5fqqic36rxy29afp9kj8g91qc4yvlny8ar6d1p7gqf6wslfh4")))

(define-public crate-bourse-de-0.1 (crate (name "bourse-de") (vers "0.1.2") (deps (list (crate-dep (name "bourse-book") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "bourse-macros") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "kdam") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1hcrrqc304h2hay6i7187mf9191v4zlwlm812xcfl0ndb3r7icsr")))

(define-public crate-bourse-de-0.2 (crate (name "bourse-de") (vers "0.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "bourse-book") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "bourse-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "kdam") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1sk9mmr465i7ca62a7mwqbdqg9zs0gw3rs88s8sjri3d024rlnhm")))

(define-public crate-bourse-de-0.3 (crate (name "bourse-de") (vers "0.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "bourse-book") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bourse-macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "kdam") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0smwyc4graqsrsl0p0g17r1xv7y3gkfjk59ag61lmqm24rhpz0ma")))

(define-public crate-bourse-de-0.3 (crate (name "bourse-de") (vers "0.3.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "bourse-book") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "bourse-macros") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "kdam") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0x6r511xj4rrpdhppqi2hg42j7kal127kimy7r8j7vla6axfccl1")))

(define-public crate-bourse-de-0.4 (crate (name "bourse-de") (vers "0.4.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "bourse-book") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bourse-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "kdam") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "19gif7ckvdbi6ac8z1n9ii5jcx1xx6gzzpjqhwn60xxd1ra2hd45")))

(define-public crate-bourse-macros-0.1 (crate (name "bourse-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d99incn6h6mwcx64jmxwkgigsd8wvmlzmfwp3q5zsdlkp5xm879")))

(define-public crate-bourse-macros-0.1 (crate (name "bourse-macros") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pzx0v5apwfxx84a29p2y6kcmpc2j0m8amfvp6mz7andkq7rw0s2")))

(define-public crate-bourse-macros-0.1 (crate (name "bourse-macros") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zlqlvr92p6g14385wrjlgprb5jnqskzncqsvrhrx1pymqdzk69n")))

(define-public crate-bourse-macros-0.2 (crate (name "bourse-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0biz85298bxd3yp95y8cjzk2xv0fpz08l25ijwxm10456jr9hdbh")))

(define-public crate-bourse-macros-0.3 (crate (name "bourse-macros") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n0vycg5n0qivhkj6m40lw9ngsiyfkfpxm25d1klzr8ljfbm42hn")))

(define-public crate-bourse-macros-0.3 (crate (name "bourse-macros") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zkpr5ynm46czj32nhl811ic2c0p2776wki4sgrvi4c8065p27sx")))

(define-public crate-bourse-macros-0.4 (crate (name "bourse-macros") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0569cjn8qngfbi71qajwkyvzzlrw4lrggpa5bcfqwcscy16fh25s")))

