(define-module (crates-io bo bc) #:use-module (crates-io))

(define-public crate-bobcat-0.1 (crate (name "bobcat") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv_codegen") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sqlite") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1shjp9vvnyg5fywrksfmsq6qlyfmyc2gx29fdnr4al09ajsgg9k9")))

