(define-module (crates-io bo ml) #:use-module (crates-io))

(define-public crate-boml-0.1 (crate (name "boml") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 2)))) (hash "167rmds2wzlnnrh15myzafs3z1kx7agjpkwaimn5463k27f8kzhv")))

(define-public crate-boml-0.2 (crate (name "boml") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 2)))) (hash "0cg0np5vfkvcxfqp2465cn3nkc6b962d9jzs478xjhcmzj7ny75w")))

(define-public crate-boml-0.3 (crate (name "boml") (vers "0.3.1") (deps (list (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 2)))) (hash "1iba2rmj0djqyf1fipxavhgc8jalxbzkg92z61agyfy70hzvkzc5")))

(define-public crate-bomlamaal-0.1 (crate (name "bomlamaal") (vers "0.1.0") (hash "1n2yafv09vx8gjz750kd6mfylm6az3abdp9divv7ws621hc3b89d")))

