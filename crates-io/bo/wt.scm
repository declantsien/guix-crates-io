(define-module (crates-io bo wt) #:use-module (crates-io))

(define-public crate-bowtide-0.0.0 (crate (name "bowtide") (vers "0.0.0") (hash "009vcym5bgpqdl8jymnrywjjw0x2l76i2d22w5ddg2551d6pm7nk")))

(define-public crate-bowtie-0.2 (crate (name "bowtie") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1mdkw1lnxvqvx2xnbx5z5g9vxi23j3lf3yxk1n46xf7ii6hwh93m")))

(define-public crate-bowtie-0.2 (crate (name "bowtie") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0gp8vb6p90mbwkgcsvyjm15x76av0fz99l00dgc4vyz1jqv4fkv8")))

(define-public crate-bowtie-0.2 (crate (name "bowtie") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0knq9x80pd4nml1g9vpk0lpg2wgxd4qd871xc02w4x6zwayh7akh")))

