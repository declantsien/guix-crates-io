(define-module (crates-io bo ld) #:use-module (crates-io))

(define-public crate-boldline-0.1 (crate (name "boldline") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.11.3") (features (quote ("unstable"))) (default-features #t) (kind 0)))) (hash "1lq9v06j5gl4x776bxj9hfcc0sfna3vhaknxaqz6d5g8mq4hb4wz")))

(define-public crate-boldline-0.1 (crate (name "boldline") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.11.3") (features (quote ("unstable"))) (default-features #t) (kind 0)))) (hash "1bcg8njpb6f7alkgki2cngcna3z17d56cld6lshzkic6vg456glv")))

