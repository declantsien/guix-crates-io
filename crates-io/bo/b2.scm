(define-module (crates-io bo b2) #:use-module (crates-io))

(define-public crate-bob2-0.0.1 (crate (name "bob2") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "0hpr9k0430bx5gfs5sn0w2s895hmncp1qslgnw45lzb9fj1y1fj1")))

