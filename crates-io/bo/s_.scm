(define-module (crates-io bo s_) #:use-module (crates-io))

(define-public crate-bos_books_codes-0.1 (crate (name "bos_books_codes") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w5kh60va4dpkkzklhnvxbdnlbmg39nhvhmx51i3bb9y3z9nn97j")))

(define-public crate-bos_books_codes-0.1 (crate (name "bos_books_codes") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fv6zw2yysdcqywjjxzmis4rr29pwy1kca3jx2f9qk21ygbngka1")))

(define-public crate-bos_books_codes-0.1 (crate (name "bos_books_codes") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vn2icg0a9p77lcfb6bn4kb0vb5xzbhdhq4b060gg27kr0vdfscr")))

