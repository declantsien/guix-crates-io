(define-module (crates-io bo bo) #:use-module (crates-io))

(define-public crate-boboweike_art-0.1 (crate (name "boboweike_art") (vers "0.1.0") (hash "1pwm65hhfy9f0nw3mxpp2hnmbl9lkygw1h11jk7g4y5iw49x1bki")))

(define-public crate-boboweike_art_v1-0.1 (crate (name "boboweike_art_v1") (vers "0.1.0") (hash "14pxbp5qznp8hfcmmz2lmfg69zicqv6y8dvnfxylms4d1pn1zg2j")))

