(define-module (crates-io bo o-) #:use-module (crates-io))

(define-public crate-boo-hoo-0.1 (crate (name "boo-hoo") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "16z2ygbjwb0y6mn5a25n1mbayrah0f76ngdiamqfpb1rp7hhc3bq")))

(define-public crate-boo-hoo-0.2 (crate (name "boo-hoo") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "119yibbcrjzfxyaxahzxsz66yrmfd9057bsginfvh5alkp86f1iz")))

