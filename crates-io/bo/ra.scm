(define-module (crates-io bo ra) #:use-module (crates-io))

(define-public crate-bora-0.0.0 (crate (name "bora") (vers "0.0.0") (hash "02rh09d304i373kj4i11331baklw5xhf2lfw5wkyg4j8mppvn0za")))

(define-public crate-bora-0.0.1 (crate (name "bora") (vers "0.0.1") (hash "1068lgm8mv8qf2fbpqsflcjqvr96xvjd7vqmmlgvgzgnh3050qib")))

