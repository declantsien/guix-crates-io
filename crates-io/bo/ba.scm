(define-module (crates-io bo ba) #:use-module (crates-io))

(define-public crate-boba-0.0.0 (crate (name "boba") (vers "0.0.0") (hash "1kvfv2855nkprb5cf02ckkr83ha529sn71g55hy9z7r2fkjrmza1")))

(define-public crate-boba-3 (crate (name "boba") (vers "3.0.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (kind 0)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0452ga8hmp4b03nsms5mkn0v4ksl6r5dvf2ygq3791km2pmnhbmr") (features (quote (("std" "bstr/std") ("default" "std"))))))

(define-public crate-boba-4 (crate (name "boba") (vers "4.0.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (kind 0)) (crate-dep (name "bubblebabble") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0igpvsdyzaii8f6nlqj97pmyf95zl9a5fh4762ww5n716lfpgxh7") (features (quote (("std") ("default" "std"))))))

(define-public crate-boba-4 (crate (name "boba") (vers "4.1.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (kind 0)) (crate-dep (name "bubblebabble") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0csfnpa0wbv8hxhm332cc9jsn6ia69q6s8villbxhjlcp25g9zlb") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4 (crate (name "boba") (vers "4.1.1") (deps (list (crate-dep (name "bstr") (req "^0.2") (kind 0)) (crate-dep (name "bubblebabble") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0c9v5y7ni2za300g5ic9zw3cjkls2f4i26kkqimr60yrpr3x0lg1") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4 (crate (name "boba") (vers "4.1.2") (deps (list (crate-dep (name "bstr") (req "^0.2") (kind 0)) (crate-dep (name "bubblebabble") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1kj1mfhybqbq6nd9f4i74ydmmw6pbh93zh35v9hz14q6dl4sbx46") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4 (crate (name "boba") (vers "4.2.0") (deps (list (crate-dep (name "bstr") (req "^0.2.4") (kind 0)) (crate-dep (name "bubblebabble") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "08v0qx9b4a7m1ajgnl0kw36pp9q468vz4n3hlj8r125gly88bska") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4 (crate (name "boba") (vers "4.3.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.3") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "1rsccnlrd97arvlbbirxq3fdhkwcn3mlza9pxg90l777zbyaswgw") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-4 (crate (name "boba") (vers "4.3.1") (deps (list (crate-dep (name "version-sync") (req "^0.9.3") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "1sz6cygf3vl6x0w0fa222m9g789r3amsmvjn1ri6nzms509q4h1w") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-boba-5 (crate (name "boba") (vers "5.0.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.3") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "02hbi9jn76ri40vlnjac59yia1nsa29bxkxqfa81wl6ipp13cb17") (features (quote (("std") ("default" "std"))))))

(define-public crate-bobascript-0.1 (crate (name "bobascript") (vers "0.1.0") (deps (list (crate-dep (name "bobascript-parser") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s9bl8rfjp32wsbyk5jip9p3va83ns1s7bw1v6mhpjw5af6b42my") (features (quote (("super_debug" "debug") ("debug"))))))

(define-public crate-bobascript-0.1 (crate (name "bobascript") (vers "0.1.1") (deps (list (crate-dep (name "bobascript-parser") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14fhgczg0zjn9p3ya8fn8i3mddvl06wsv9dqpxlrdwwaz9dh0iw5") (features (quote (("super_debug" "debug") ("debug")))) (yanked #t)))

(define-public crate-bobascript-0.1 (crate (name "bobascript") (vers "0.1.2") (deps (list (crate-dep (name "bobascript-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rkdm1lfjbxwwpyl0asrwxbylddmyds9kpp4y7p7f7rphib68h7s") (features (quote (("super_debug" "debug") ("debug"))))))

(define-public crate-bobascript-0.1 (crate (name "bobascript") (vers "0.1.3") (deps (list (crate-dep (name "bobascript-parser") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fla42kf5dq7ni9hvvl2kfm50scqywvr707rg3sns81f6bh9szi6") (features (quote (("super_debug" "debug") ("debug"))))))

(define-public crate-bobascript-0.1 (crate (name "bobascript") (vers "0.1.4") (deps (list (crate-dep (name "bobascript-parser") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vfjmifnjrccwrk7f5brrlrmfx781j2h919qgp3k9k5pcr618656") (features (quote (("super_debug" "debug") ("debug"))))))

(define-public crate-bobascript-parser-0.1 (crate (name "bobascript-parser") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xfnaghimm5l85q94h0gaydgpc0w0arl802phjjzdivbkd6ad9m2")))

(define-public crate-bobascript-parser-0.1 (crate (name "bobascript-parser") (vers "0.1.1") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "12ywnca4ba44y5wpdwyypbrs7y9b14fgb01rlxj8crwlw3dafw1n")))

(define-public crate-bobascript-parser-0.1 (crate (name "bobascript-parser") (vers "0.1.2") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ln7dm5vxpj77mj6sdfkdha82svds1x8340xm4jy42kx2cz699vz")))

(define-public crate-bobascript-parser-0.1 (crate (name "bobascript-parser") (vers "0.1.3") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vw9df6zdw3fg8qq8kn9rz4zc4l4sac08v77xi9zpbrc5g04hf1m")))

(define-public crate-bobascript-parser-0.1 (crate (name "bobascript-parser") (vers "0.1.4") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "17zmg37f8a1wx5pnvrpazxj1mg36hrg5q2nkbnsh992k69ssqx4k")))

