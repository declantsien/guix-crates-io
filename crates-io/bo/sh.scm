(define-module (crates-io bo sh) #:use-module (crates-io))

(define-public crate-bosh-0.0.0 (crate (name "bosh") (vers "0.0.0") (deps (list (crate-dep (name "bosh_compiler") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17lxcxfpvqyxzrf6wwb625scxljfx6g3kgdb6dk9gn17m0dbyaha")))

(define-public crate-bosh-rs-0.1 (crate (name "bosh-rs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jp4526hlarg0nivq7miqwnrpj6q3s22vlindhq0gcbm4hv8rppc")))

(define-public crate-bosh-rs-0.2 (crate (name "bosh-rs") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sd4x70myh008j6zn7nz4lafiic37lb634zx9l1fnbs3ih4kxdvb")))

(define-public crate-bosh-rs-0.2 (crate (name "bosh-rs") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "06y197jbmpq7afmssdww9i7xlniricwfnjndg5nj9j9r3jkjq4ii")))

(define-public crate-bosh-rs-0.3 (crate (name "bosh-rs") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "read-from") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "069492x465lc6dx85h985g84smdq703bzdi6j6jvb7lcb59k3xih")))

(define-public crate-bosh-rs-0.4 (crate (name "bosh-rs") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "read-from") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "0akwdb379kmf74hwsprb1k91q2947rc7pqm98fxnrhfp1dda8bhz")))

(define-public crate-bosh_compiler-0.0.0 (crate (name "bosh_compiler") (vers "0.0.0") (deps (list (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "stdweb") (req "^0.3.0") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "0z11h9b1wm06a3difrx994f120kj36vm0dscna4slab5rdb1drk1")))

