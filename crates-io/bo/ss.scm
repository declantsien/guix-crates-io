(define-module (crates-io bo ss) #:use-module (crates-io))

(define-public crate-boss-0.0.1 (crate (name "boss") (vers "0.0.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "08xnjlq91k2v5vj2z6fpdc92qfwwq5y4byr05n4qw2s7hmi4g9j4")))

(define-public crate-boss-0.0.2 (crate (name "boss") (vers "0.0.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "05v18a5cnnh0sdh3m6bfr8397yb525dvarcxh8y92vw2xvj6fqx6")))

(define-public crate-boss-0.0.3 (crate (name "boss") (vers "0.0.3") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0mrwfy2vjy30r929xmafafqkr12l6plpngg61dyajc4aq0nb5d1x")))

(define-public crate-bossa-2 (crate (name "bossa") (vers "2.0.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1q9v6fh3m13ri1k3r1kahhqirva0s8i0yx6sqnzkmf7q9vih97v8")))

(define-public crate-bossa-2 (crate (name "bossa") (vers "2.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "11p03gh3m24pkcjw306dy9h67y9885lx7fmr62g98z00wbgd4gr7")))

(define-public crate-bossa-2 (crate (name "bossa") (vers "2.2.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1igw3maqsgsg7nhrws2amcnkpkzczf2ndk351sy4j5l6k6zfx86q")))

(define-public crate-bossa-2 (crate (name "bossa") (vers "2.3.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0zpri7b5dghh2sra8v1wj4mfryccpvyxj9rv8ddla81q75f4mbj1")))

(define-public crate-bossac-2 (crate (name "bossac") (vers "2.0.0") (deps (list (crate-dep (name "bossa") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "flexi_logger") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kpkkd6l66yyjmfr8hgxnpcs55djf60y3l1wi8i4xzn6m8vi0qry")))

(define-public crate-bossac-2 (crate (name "bossac") (vers "2.1.0") (deps (list (crate-dep (name "bossa") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "flexi_logger") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kv161b740jyhwj4b091b2p51a7ycadsj4hdnzswk1c66ml5x56h")))

(define-public crate-bossac-2 (crate (name "bossac") (vers "2.1.1") (deps (list (crate-dep (name "bossa") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "flexi_logger") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "11xqs6v5pplisgp8225nwjk3km1a253qqnxbx8b323j0zkgvmzf9")))

(define-public crate-bossy-0.1 (crate (name "bossy") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0fhs0kkbz6f521jg1py1vjax0fvkp8bvnhfvfc003jf9fbqxw50w") (yanked #t)))

(define-public crate-bossy-0.1 (crate (name "bossy") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6.0") (default-features #t) (kind 2)))) (hash "01pvas0n9gkz9babwj5r02yqz6ikd8b3xsfv9vxajalc4lrmw4iw")))

(define-public crate-bossy-0.1 (crate (name "bossy") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6.0") (default-features #t) (kind 2)))) (hash "124hiydwgs6sgidimfp9wa00izjinqvhkc9r1yjqkd088y8wfksb")))

(define-public crate-bossy-0.1 (crate (name "bossy") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6.0") (default-features #t) (kind 2)))) (hash "0g7ja01irwk2nznrjm7w2bp2d541pi8bkzwhk57039k19wmz8fm7")))

(define-public crate-bossy-0.1 (crate (name "bossy") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.81") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwinbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "137sn1jimax5psw7qjakd7wn9cjrpvwxq310r0iv0hzd50sp33hg")))

(define-public crate-bossy-0.2 (crate (name "bossy") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.81") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwinbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0g24ngdgl9c4kqv5954yks2gyy95p0bhbghh0ck3isgvjyilm9xy")))

(define-public crate-bossy-0.2 (crate (name "bossy") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.81") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwinbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1v6vxvg33p0x4a1bsi2gkvn8xny49h6dpqyqsiz2zaw8sr5xf160")))

