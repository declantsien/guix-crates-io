(define-module (crates-io bo ow) #:use-module (crates-io))

(define-public crate-boow-0.1 (crate (name "boow") (vers "0.1.0") (hash "0zql8hlrxv1h0jvcawzwhvsb2xn14g0sp9zdxz5s25i6z560rdsp")))

(define-public crate-boow-0.1 (crate (name "boow") (vers "0.1.1") (hash "19iyxflrbpfn0ikscwxj26i1v3snxvx0qifapmcph3922222dbx4")))

(define-public crate-boow-0.1 (crate (name "boow") (vers "0.1.2") (hash "1zvflyac26nqvd7kjlg4hjfrzi2g9kcr1is70mq5mblg6g9a5ja0")))

(define-public crate-boow-0.1 (crate (name "boow") (vers "0.1.3") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)))) (hash "0921ipqfqv36i2lgkd18k2a98kqfb36pkidqya7qpx9f3b4haibz") (features (quote (("std") ("default" "std"))))))

