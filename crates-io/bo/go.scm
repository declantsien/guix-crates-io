(define-module (crates-io bo go) #:use-module (crates-io))

(define-public crate-bogo-alloc-0.1 (crate (name "bogo-alloc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.148") (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (features (quote ("wyrand"))) (kind 0)))) (hash "1nbwp4ca83gk7xlshzlbz26x8ismdvkkg3vvmlhnkw3472miln4i")))

(define-public crate-bogo-alloc-0.1 (crate (name "bogo-alloc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.148") (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (features (quote ("wyrand"))) (kind 0)))) (hash "004lmx3g0bpk06m3i2kibcibs7ipfbmb8sj1g1xa8ig1bzrpzpkj")))

(define-public crate-bogo-plus-plus-2 (crate (name "bogo-plus-plus") (vers "2.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0z5s1cljd6q0s9xmkc2sasjg7lyr6k6wyjmwh3vxmr562kd7fgn9")))

(define-public crate-bogo-plus-plus-2 (crate (name "bogo-plus-plus") (vers "2.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1gqpjl13xsha11k7brfwcg04gfw4x77shz8qa4kfkgckhfxpjp16")))

(define-public crate-bogo-plus-plus-2 (crate (name "bogo-plus-plus") (vers "2.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1paylg7xfv1w02wxxn2020xm8i0sm0pfn8xbl5192gnpg7mbp5iz")))

(define-public crate-bogo-plus-plus-2 (crate (name "bogo-plus-plus") (vers "2.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "13dqd8a2387yfyk8f2gxf8ixg9ghvvby8kffid2cyfi9x9izlwgi")))

(define-public crate-bogobble-0.1 (crate (name "bogobble") (vers "0.1.0") (hash "1nyxf3bykyr97ypixacnhgrhjbxy223qhjqax4sg75lpnndnqzv8")))

(define-public crate-bogobble-0.1 (crate (name "bogobble") (vers "0.1.1") (hash "1m73zfzsymjvf3969rd8ix8w4cifpl00vsykx55h30mblfl6yc98")))

(define-public crate-bogon-0.1 (crate (name "bogon") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.199") (features (quote ("derive"))) (default-features #t) (kind 1)))) (hash "11667v25cv4snhprcapjz8va33bk4rfxhr2wpa12zmk8dh13swkk")))

(define-public crate-bogosort-0.130 (crate (name "bogosort") (vers "0.130.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0svz274v04aayyccb5lvbcmix64b6z9307xvsdsr2lwds0zlvdd8")))

(define-public crate-bogosort-0.142 (crate (name "bogosort") (vers "0.142.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wcfvmahh7walqkz37mqdiiscd9whkskm6cm5h0nlcc5paxyywqy")))

