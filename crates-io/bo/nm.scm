(define-module (crates-io bo nm) #:use-module (crates-io))

(define-public crate-bonmin-src-0.1 (crate (name "bonmin-src") (vers "0.1.0+1.8.9") (deps (list (crate-dep (name "cbc-src") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "coin-build-tools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "ipopt-src") (req "^0.2") (kind 0)))) (hash "1vyyhpx8yfbr8pqdjcvrcga2ijjwcnxkaisg9jq84l83vwjwrik0") (features (quote (("filtersqp") ("default" "ipopt-src/default") ("cplex")))) (links "Bonmin")))

(define-public crate-bonmin-src-0.1 (crate (name "bonmin-src") (vers "0.1.1+1.8.9") (deps (list (crate-dep (name "cbc-src") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "coin-build-tools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "ipopt-src") (req "^0.2") (kind 0)))) (hash "17d81i32hlk2agprpjb1b497zcs71nqam1s7s2lwkm6cmxfj97p9") (features (quote (("filtersqp") ("default" "ipopt-src/default") ("cplex")))) (links "Bonmin")))

