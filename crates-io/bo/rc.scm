(define-module (crates-io bo rc) #:use-module (crates-io))

(define-public crate-borc-0.1 (crate (name "borc") (vers "0.1.0") (deps (list (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "040havbybrpm5n0r3gxv9b6bjqqr9asjn9kkr3wi3vgqd5zcfqsd")))

(define-public crate-borc-0.2 (crate (name "borc") (vers "0.2.0") (deps (list (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0gr59pfsgzig6pq4jbn22xxkxzkr9rakwr7prxmi7g300s7ffrvd")))

(define-public crate-borc-0.3 (crate (name "borc") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1fa4v9q9pd3pb16033a1dlvg10axip6d171c6xbmm5hq2k7rgr4g")))

(define-public crate-borc-0.4 (crate (name "borc") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1sa6k5a43pnfr5zj82ka7rr622lzicgdljv6vk9wa2z22w4bna72")))

(define-public crate-borc-0.5 (crate (name "borc") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1r93v7q3pb85g55hnl80awaksyfl77f7zwxil4km5vb5vw9rlvc5")))

(define-public crate-borc-0.6 (crate (name "borc") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0p28x4fm60ns0glwc6v9phy6zqkkaljkrp8h7c595bkan2vsz2f2")))

