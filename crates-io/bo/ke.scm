(define-module (crates-io bo ke) #:use-module (crates-io))

(define-public crate-bokeh-0.1 (crate (name "bokeh") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6") (default-features #t) (kind 0)))) (hash "13nayphldk23qwc07raafpy9h8pah35w5ir336j9hr6ijh0ihv08") (features (quote (("default" "image")))) (v 2) (features2 (quote (("image" "dep:image"))))))

