(define-module (crates-io bo cu) #:use-module (crates-io))

(define-public crate-bocu1-0.1 (crate (name "bocu1") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_trace" "release_max_level_off"))) (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.39") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "11vbxwav7lzbz0agvrq8p2pr53swzf1ia8py1gkwhlrgf5alj05z")))

