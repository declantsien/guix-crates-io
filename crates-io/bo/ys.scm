(define-module (crates-io bo ys) #:use-module (crates-io))

(define-public crate-boys-0.1 (crate (name "boys") (vers "0.1.0") (deps (list (crate-dep (name "GSL") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1fd5sdhy1xlnr3mnpfgaqhwdq67hgbmbq844fkjwlwi7phr2hg7h")))

