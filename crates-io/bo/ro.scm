(define-module (crates-io bo ro) #:use-module (crates-io))

(define-public crate-boron-0.0.1 (crate (name "boron") (vers "0.0.1") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1xzcc5zyqq4jpxdkq07maynhcfj7q3apwdq28w2kw5pq568fa4x9")))

(define-public crate-boron-0.0.2 (crate (name "boron") (vers "0.0.2") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "14lj4gkbjnrkjzpkz8jh7z4m7z2xqlzpd429b87k7bj1aky9s6f1")))

(define-public crate-boron-lang-0.1 (crate (name "boron-lang") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1s8j5h2q0mc1zrkv48ai6wf1ahv2p9844fpjlswlv9plrnhjmlv8")))

(define-public crate-boron-lang-0.2 (crate (name "boron-lang") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1vlm14z9c27aqg4zyq4qrsvzpm00c1q8a2rfgihbz9rssn34bz9k")))

(define-public crate-boron-lang-0.3 (crate (name "boron-lang") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "141an132hhnj3q4qpilr4xf1cgjih3xg9j8apylcdpf3hdrkbv7n")))

(define-public crate-boron-lang-0.4 (crate (name "boron-lang") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0s8ky57p8fddcfzlg7p6yb686z68n6lmqkd62qc56c4kavrkbxn0")))

(define-public crate-boron-lang-0.5 (crate (name "boron-lang") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1nzcw7376r0bm6dksnvv7h1d229lqcygn29pinzhsacz197aq225")))

(define-public crate-boron-lang-0.5 (crate (name "boron-lang") (vers "0.5.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0zk1q2l2q3hbd319lygzw84f0b2b83j36a2ar7n395ry0hc35p2v")))

(define-public crate-boron-lang-0.5 (crate (name "boron-lang") (vers "0.5.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "01awy4a9jss2i0l0x4qw3a4dcbwdx1wyrhivvj3wvqw4z3qlvf2w")))

(define-public crate-boron-lang-0.5 (crate (name "boron-lang") (vers "0.5.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0k6r03r93py8c87b778ki2ds4v36lbcr7z020h9g6x3sgickpmyv")))

(define-public crate-boron-lang-0.6 (crate (name "boron-lang") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0i7grdcmkqk5zixpcqv40fh3y6xwp4ykk2xaqv5gd269k774bfnj")))

(define-public crate-boron-lang-0.7 (crate (name "boron-lang") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0myydscaiajfwvq09cqqb2fhx9m7hn6vx8jgpzyxd1qg1jpp4nl0")))

(define-public crate-boron-lang-0.8 (crate (name "boron-lang") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0h73dam9x2l5409ii34lw7czzvrm9q104b2iqjgwzr0jqarbds2l")))

(define-public crate-boron-lang-0.9 (crate (name "boron-lang") (vers "0.9.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0d47151913s1yp3s9fazy9d6gy8f6adckf4bmbj52i62lbbri11k")))

(define-public crate-boron-lang-0.10 (crate (name "boron-lang") (vers "0.10.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "08wfz612bynrgg474cgvciak0khj3n2wmfgrmj15drx01xhjv0p3")))

(define-public crate-boron-lang-0.11 (crate (name "boron-lang") (vers "0.11.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "00skrfgxv8k0jnin75ld5bn8cv8y8m7aqw55r79z6rvsqmzx74np")))

(define-public crate-boron-lang-0.12 (crate (name "boron-lang") (vers "0.12.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0p4l8iwp32nq1hv0jzyr623jnpk0diwyprrnqzsp23x7rz0gka6q")))

(define-public crate-boron-lang-0.13 (crate (name "boron-lang") (vers "0.13.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0jy9vs696hrlr23fpvphvah69r1sfr2kzkv6b09gbnxd6r7yb63b")))

(define-public crate-boron-lang-0.13 (crate (name "boron-lang") (vers "0.13.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0kcz5kspliinpfwaplg50nc4rhi57j6r8ff685p9jx838znlidn1")))

(define-public crate-boron-lang-0.13 (crate (name "boron-lang") (vers "0.13.2") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0kcqqzy3ghg8vzjar48r5yzy6rz8hcmvjmrwz77nd63bjhwwmhis")))

(define-public crate-boron-lang-0.14 (crate (name "boron-lang") (vers "0.14.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1xyhzqaw5x3kijqxp0p43yd001m8sc3wlh6q58mvapj4wh43r7gh")))

(define-public crate-boron-lang-0.14 (crate (name "boron-lang") (vers "0.14.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "15q2br5six09im7kwail5q9jb8mdxj6s0q3g2xilglcjj0r7nhld")))

(define-public crate-boron-lang-0.15 (crate (name "boron-lang") (vers "0.15.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0s4s31464vjvi8fjvjvj1s2rcphfm0y04x844a6x2s2ldzcr2nz3")))

(define-public crate-boron-lang-0.15 (crate (name "boron-lang") (vers "0.15.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "039a6qnlf26l9z6vbqwp4n61yf0bs4la9p64vpqg4wgzizgsnsyd")))

(define-public crate-boron-lang-0.16 (crate (name "boron-lang") (vers "0.16.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1xj3519pj40ar6lzy3bakxzm79h87a14zvc16z4pna0dxqymg1dm")))

(define-public crate-boron-lang-0.17 (crate (name "boron-lang") (vers "0.17.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1r770k68cy1lg3fzp7s4wd2j44p12wfq0z321vxf0q4m5502ifvf")))

(define-public crate-boron-lang-0.17 (crate (name "boron-lang") (vers "0.17.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0kx3w3dz15mlbw8m6ly7d15mmry9rj1dyncxscmg0k88qz85rqgi")))

(define-public crate-boron-lang-0.18 (crate (name "boron-lang") (vers "0.18.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "03iqxg14iqzv4rz3bxrx06362yvj5ni3sf8q3mnm54q4ljhqgf4w")))

(define-public crate-boron-lang-0.19 (crate (name "boron-lang") (vers "0.19.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1dbvlcb3m8qh22g47n550yzg6r7z7fdp9lli81n3zhgkm5hzb8vw")))

(define-public crate-boron-lang-0.19 (crate (name "boron-lang") (vers "0.19.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "15q3dq2ydg43cfac9dv6122wbbfnf0c5i9dh2wz965hycww01q2n")))

(define-public crate-boron-lang-0.20 (crate (name "boron-lang") (vers "0.20.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1baid9745h4i1hs5i9w53k8k56plcls7cdf2r1hpy49241cb7308")))

(define-public crate-boron-lang-0.20 (crate (name "boron-lang") (vers "0.20.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1gnl3ai6vci8kwshizdbkfakf2bik7b58iid5wcrk7dswnxlhmbg")))

(define-public crate-boron-lang-0.20 (crate (name "boron-lang") (vers "0.20.2") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0jv1bng929x6zmr7mv28fnipwf673wcayw59h6mzqc3fpavhlnxc")))

(define-public crate-boron-lang-0.21 (crate (name "boron-lang") (vers "0.21.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0b29aq6sf2qxlazzjws25q2bj211swpzp9lgflb93a9lwg6g8ir1")))

(define-public crate-boron-lang-0.22 (crate (name "boron-lang") (vers "0.22.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1zwg44mh8a08c5brq6isx08rx9s3sxb14x902w6i89yh0sqaqwrv")))

(define-public crate-boron-lang-0.23 (crate (name "boron-lang") (vers "0.23.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "17ws70mp0ha0a93ps0vi3d6bmlzjsm3ayxmqak3q6n47ljxvcdav")))

(define-public crate-boron-lang-0.23 (crate (name "boron-lang") (vers "0.23.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0wz1kjvgpi1gv4h66fj5dwcis2yrv4nkc9xw0pm9jpzppy8lmg0c")))

(define-public crate-boron-lang-0.23 (crate (name "boron-lang") (vers "0.23.2") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0qsc9lyc227098s4bj45qyr78xas1dh01c4vy0zngg0qz75acbc8")))

(define-public crate-boron-lang-0.24 (crate (name "boron-lang") (vers "0.24.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "056xjhhygvijm7nxxrjs0b1r3q15m952gqr8yz5a8c7dilcbli3k")))

(define-public crate-boron-lang-0.25 (crate (name "boron-lang") (vers "0.25.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "06ipkb782ibiz0a16drjs243zzp67djn2a57vfhhxg7jnkc15wgd")))

(define-public crate-boron-lang-0.25 (crate (name "boron-lang") (vers "0.25.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1x8gszymg8ksnd15bx48jssx19ar0n9iyfyyy3pa1vm6cjb0avav")))

(define-public crate-boron-lang-0.26 (crate (name "boron-lang") (vers "0.26.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "02mbphlia9cj6k37ilccminh47g6av8j5lsy3hbvzm8fbj4927sf")))

