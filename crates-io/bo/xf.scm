(define-module (crates-io bo xf) #:use-module (crates-io))

(define-public crate-boxfnonce-0.0.1 (crate (name "boxfnonce") (vers "0.0.1") (hash "128567dccb5k3jrirxxnir6m8ijkj642g3w8lchw80v0r9x6p8yi")))

(define-public crate-boxfnonce-0.0.2 (crate (name "boxfnonce") (vers "0.0.2") (hash "0crjra3k4w29h2z8hcjkvjbgvw3jps0p3154lv69kijhn9wssv4w")))

(define-public crate-boxfnonce-0.0.3 (crate (name "boxfnonce") (vers "0.0.3") (hash "0xfx8rrk320k2yjshspz4wjcjny0f901c1ljyvk9j479xxdi1043")))

(define-public crate-boxfnonce-0.1 (crate (name "boxfnonce") (vers "0.1.0") (hash "13nr940xkffi9cargfjkzxgrdzccj3c92gs0rpixh8pkc32n1v6b")))

(define-public crate-boxfnonce-0.1 (crate (name "boxfnonce") (vers "0.1.1") (hash "09ilf4zyx92hyhkxlsxksfyprzr9iwq5gqqb22aaqr32c8fwp22r")))

(define-public crate-boxfuture-0.0.1 (crate (name "boxfuture") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1kjvak8spwjpfzbzkwfwa6ri2bbn70saqpgm263mbq29kyp8jjix")))

