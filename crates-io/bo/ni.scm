(define-module (crates-io bo ni) #:use-module (crates-io))

(define-public crate-bonitox-0.1 (crate (name "bonitox") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hf-hub") (req "^0.3.1") (features (quote ("tokio"))) (default-features #t) (kind 0)) (crate-dep (name "llama-cpp-2") (req "^0.1.41") (features (quote ("sampler"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xyskxm86kbyjffj9f0ikmfii5d7yb4dbczgblp6a2f83sny5wgn")))

