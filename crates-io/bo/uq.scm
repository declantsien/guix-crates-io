(define-module (crates-io bo uq) #:use-module (crates-io))

(define-public crate-bouquin-0.1 (crate (name "bouquin") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mut_static") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "0parqz4vhqajiql04ai5jmr0rc9k9mgi7w404d9ab4dwcimyd7rv")))

(define-public crate-bouquin-0.1 (crate (name "bouquin") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mut_static") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "0y57lkid2r6l5ld73z0qs5pvld2z0iy8cjyaz8s4dynshy1jzsh2")))

