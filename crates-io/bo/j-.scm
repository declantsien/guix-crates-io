(define-module (crates-io bo j-) #:use-module (crates-io))

(define-public crate-boj-cli-0.0.1 (crate (name "boj-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dizic1z7fp7s685693p19cm32pdkrryz482vmwfzll7ks48ln1j")))

