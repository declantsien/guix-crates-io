(define-module (crates-io bo rn) #:use-module (crates-io))

(define-public crate-born-0.0.0 (crate (name "born") (vers "0.0.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "macrotest") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.27") (default-features #t) (kind 2)))) (hash "0lhmfx2wchyfw4k99fflxrr178g7m4r93z48wmk4mhk3xsjqm8g8")))

(define-public crate-born-0.0.1 (crate (name "born") (vers "0.0.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "macrotest") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.27") (default-features #t) (kind 2)))) (hash "0rk4mz262vdb037wyi2m5na2k083bs4sibkrmmrb9vpznxcg8dis")))

