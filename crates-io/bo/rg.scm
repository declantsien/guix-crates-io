(define-module (crates-io bo rg) #:use-module (crates-io))

(define-public crate-borg-0.1 (crate (name "borg") (vers "0.1.0") (hash "0p24nz5jzf7369avny4sbwz8vkanra8znf3i380w9rndcxzj9kwn")))

(define-public crate-borg-hive-0.0.1 (crate (name "borg-hive") (vers "0.0.1") (deps (list (crate-dep (name "app_dirs") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "assert_matches") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.19.3") (default-features #t) (kind 0)) (crate-dep (name "keyring") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_codegen") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.4.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "15250a35giy60bcvzmasfzpd4j8qxgq09qdr39cpc5i9jfnq30ng") (yanked #t)))

(define-public crate-borg-hive-0.0.2 (crate (name "borg-hive") (vers "0.0.2") (deps (list (crate-dep (name "app_dirs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "keyring") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "slog-async") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "slog-scope") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0jccxfxkyhi1yapzw3hxiqwyyxc63xzvlvcrfhasj9y98l3m7kav") (yanked #t)))

(define-public crate-borg-prometheus-exporter-1 (crate (name "borg-prometheus-exporter") (vers "1.0.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1kfnrpqx4lpra5iqpy723rkpnm3121l9nxvdkqq7c8sjz07d8kdx")))

(define-public crate-borg-prometheus-exporter-1 (crate (name "borg-prometheus-exporter") (vers "1.1.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "118555hzq5qfmq12qyma744mcmc8bnh4682bbf57ydvzjmw86kzq")))

(define-public crate-borg-prometheus-exporter-1 (crate (name "borg-prometheus-exporter") (vers "1.2.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "197sfpp2dbr6gaww7r4d5909g64r0rd44m69y3sj30c0xp3i62ls")))

(define-public crate-borg-prometheus-exporter-1 (crate (name "borg-prometheus-exporter") (vers "1.2.1") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "03gzib8iwl551lm6sma8kdymakh4ppv730rdf56v3v868yhr76kj")))

(define-public crate-borg_ros-0.1 (crate (name "borg_ros") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ni875acks5cf5iy0jw60ghp9fc7mvk5szhg851f6qvbrnbsx30z") (yanked #t)))

(define-public crate-borgbackup-0.1 (crate (name "borgbackup") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "0xaw6rmcn2lrkzncb0dgvc9njcapk5liihx2c34ixaz1050j30bw") (features (quote (("vendored"))))))

(define-public crate-borgbackup-0.2 (crate (name "borgbackup") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "1s4vkd0lni3s62zqj8hskn5hgjlagslcxpqfc2sgl6z023ganca5") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.2 (crate (name "borgbackup") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req ">=0.4.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "03d1hcqlzqpv0cx0pagzdrcw55411kyjj0mg3hib62nnd0m3iyy1") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.3 (crate (name "borgbackup") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qrhlb8maz7b925n8rgdgg6cxhjgf7qw74dd1cz6qiwi2zlz0fgg") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.4 (crate (name "borgbackup") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0w82n2bibhcvlj6vzzg0swn6x8z12rrgqyv0wvd4acnal8shr9bd") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.5 (crate (name "borgbackup") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0w5mypxx71qjb4919nbr58aj5qqgcjab1l7i7k5mm31im0rknsb6") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.6 (crate (name "borgbackup") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dm2jjrrqd94vpv4yxizszs6a5s8kak5y7wxf4azylhalcqrmlpl") (features (quote (("vendored")))) (yanked #t) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.6 (crate (name "borgbackup") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req ">=0.4.22") (features (quote ("serde"))) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qnahs26v2l9wkg3186jgpjwwkrbps90mpbgbqy5897wv6vjcd8q") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.6 (crate (name "borgbackup") (vers "0.6.2") (deps (list (crate-dep (name "chrono") (req ">=0.4.22") (features (quote ("serde"))) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0c6lgf87sbyjvwnxhb9s69prgmcbsfgmsz58y1qwqpx3rl3l4p74") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.6 (crate (name "borgbackup") (vers "0.6.3") (deps (list (crate-dep (name "chrono") (req ">=0.4.22") (features (quote ("serde"))) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "07vnbd0wj2pqz5fnsspqb4sbsd69y09k1w19yx0hbgdjadnbbb4s") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.7 (crate (name "borgbackup") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.22") (features (quote ("serde"))) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0091s579jcz5iql6i2ql6rzj01nrlbq2d27aqrv825jvpx4vis10") (features (quote (("vendored")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgbackup-0.8 (crate (name "borgbackup") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.22") (features (quote ("serde"))) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req ">=1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "~3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req ">=1.23.1") (features (quote ("process" "macros" "io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mv64l949ff39p1r08i9nzd7nh59jpb199lqig44ws0ddyd73jbr") (features (quote (("vendored")))) (yanked #t) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-borgflux-0.1 (crate (name "borgflux") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "01rk2val32h1q6hrvqbfgxvxj46i3z6xx7r20kgl8bl1ng2a5j17")))

