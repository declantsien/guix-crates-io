(define-module (crates-io bo y_) #:use-module (crates-io))

(define-public crate-boy_i_sure_hope_this_works-0.1 (crate (name "boy_i_sure_hope_this_works") (vers "0.1.0") (hash "0spsard354ffv8va6dr1c3b0s9q0rx0kqr4kkws2af3adm3aa8i0") (yanked #t)))

(define-public crate-boy_i_sure_hope_this_works-0.1 (crate (name "boy_i_sure_hope_this_works") (vers "0.1.1") (hash "1d8kk1fj4khs4yqwblqkjr5x3mg2v55vhd0i572f8jp4kibpljj3")))

(define-public crate-boy_i_sure_hope_this_works-1 (crate (name "boy_i_sure_hope_this_works") (vers "1.0.0") (hash "0hkbicjzxdclivzh8lprl888xwx42fm398sn6fc4bvf44z9ck0np")))

