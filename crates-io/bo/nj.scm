(define-module (crates-io bo nj) #:use-module (crates-io))

(define-public crate-bonjour-sys-0.1 (crate (name "bonjour-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "1anmgjsp7zrxphardfs8ranmbvhyx2g074ndfhw98qs0w2lnkdwg")))

(define-public crate-bonjour-sys-0.1 (crate (name "bonjour-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "0aqlhby7srfvfxs11kgm5bhrhb037jf0z46wzrvm1c8505zlb73l")))

(define-public crate-bonjour-sys-0.2 (crate (name "bonjour-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "05qslwl2xjv6qyas10kv2k4gh3i3wq6830710p92v600gq9h23hz")))

(define-public crate-bonjour-sys-0.2 (crate (name "bonjour-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "1v3lwr8k7rwwbm1nh1mdmzwj383zdzzvdd1ilz2d1jv4ycp9b4jy")))

(define-public crate-bonjour-sys-0.2 (crate (name "bonjour-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "1jh4aqrfcl67127wrcjhw91hmmj6rrpswbi3akk47p8dsy6bvih9")))

