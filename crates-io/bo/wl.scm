(define-module (crates-io bo wl) #:use-module (crates-io))

(define-public crate-bowling-0.0.1 (crate (name "bowling") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09p79g62zyyvlasc8h93pp2mnl0yk6famh8jl4m597iaccfmgdqs")))

(define-public crate-bowling_score-0.1 (crate (name "bowling_score") (vers "0.1.0") (deps (list (crate-dep (name "bscore") (req "^0.1") (default-features #t) (kind 0)))) (hash "1r37g251110byn9ldnfr5r9bxg62cl7qkgk4ncc9lpgsvhy6wx3r") (yanked #t)))

(define-public crate-bowling_score-1 (crate (name "bowling_score") (vers "1.0.0") (deps (list (crate-dep (name "bscore") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v11rj07n7qw2lw7sz6d3wfwmwcgkdgia4dzxvj1xwnxgjsvq0fb") (yanked #t)))

(define-public crate-bowling_score-1 (crate (name "bowling_score") (vers "1.0.1") (deps (list (crate-dep (name "bscore") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qg2bz1jpkga1aagriahs4c252h95dbaz5linv56bxs28fi419is")))

(define-public crate-bowling_score-1 (crate (name "bowling_score") (vers "1.1.1") (deps (list (crate-dep (name "bscore") (req "^1.1") (default-features #t) (kind 0)))) (hash "1p75xlcbwhiaxz86743w8zprf8ar0qjl2sip5y0b2n6kxpq8yiba")))

(define-public crate-bowling_score-1 (crate (name "bowling_score") (vers "1.2.1") (deps (list (crate-dep (name "bscore") (req "^1.2") (default-features #t) (kind 0)))) (hash "1rd847nmsbswaw220i5z7jzp93l20x9flkfvq4vvwnc6kk2ms980")))

