(define-module (crates-io bo nk) #:use-module (crates-io))

(define-public crate-bonk-0.1 (crate (name "bonk") (vers "0.1.0") (hash "0dcvw6ph98i5lkg9nm9v7iypf3kkjgl5xpkqkxmp6y5sylr8a2xr")))

(define-public crate-bonky-0.2 (crate (name "bonky") (vers "0.2.0") (hash "12f36aib82mh4hrbv6qn0wfckvxjqfn7p2mb5q0dwxjd0zzjkxhl") (rust-version "1.63.0")))

(define-public crate-bonky-0.3 (crate (name "bonky") (vers "0.3.0") (hash "1jpd0m0683nbfrpr6mkjg8cniszvv64djl4ggiycgdkasr0m0sr8") (rust-version "1.63.0")))

(define-public crate-bonky-0.3 (crate (name "bonky") (vers "0.3.1") (hash "165453m49ijl8wvlmx2bg86v1m6113zfaxb0k4yiv9hfdvbyqqcq") (rust-version "1.63.0")))

(define-public crate-bonky-0.3 (crate (name "bonky") (vers "0.3.2") (hash "05nblnkk7v7zcla7w0wf1p01cdgzk79v2x0qs9l3dqzy391zs8lp") (rust-version "1.63.0")))

(define-public crate-bonky-0.4 (crate (name "bonky") (vers "0.4.0") (hash "06yycmav4rc3ng8n33zvww2kpqcwh7rkffv6hwcn20g219zn7c4m")))

