(define-module (crates-io bo xi) #:use-module (crates-io))

(define-public crate-boxify-0.1 (crate (name "boxify") (vers "0.1.0") (deps (list (crate-dep (name "boxify-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hkr7ms33xq061s04pvbsgh0b9bnm5v0cr57h1lq1gh8vvb09fxr")))

(define-public crate-boxify-macro-0.1 (crate (name "boxify-macro") (vers "0.1.0") (deps (list (crate-dep (name "litrs") (req "^0.4.1") (features (quote ("proc-macro2"))) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("visit-mut" "full"))) (default-features #t) (kind 0)))) (hash "143nf51ishgr1b8wh53309ya1cmm5851wikbzcz0ky48xrzbbyid")))

(define-public crate-boxing-0.1 (crate (name "boxing") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.11") (features (quote ("criterion" "flamegraph"))) (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "0yf4d6p9f0kkmqvb6cbiwa7izysca9zqwnfx1zdjmgab37im2m93")))

(define-public crate-boxing-0.1 (crate (name "boxing") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.11") (features (quote ("criterion" "flamegraph"))) (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "07yfmsglmr0j9z8wfgfi2lkp50m3p568paxkzm7h9w6060dj7rcq")))

(define-public crate-boxing-0.1 (crate (name "boxing") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.11") (features (quote ("criterion" "flamegraph"))) (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "126sv9ycfmhlqh2hn816pw48ykyq603a1gk52pz38nw0xw97z0bs")))

(define-public crate-boxing-arena-0.9 (crate (name "boxing-arena") (vers "0.9.0") (hash "033bgn8fv93sjrmr0q969zq7h7bhpa6qw9f9c8ln27cdpl17cvz2") (yanked #t)))

(define-public crate-boxing-arena-0.9 (crate (name "boxing-arena") (vers "0.9.1") (hash "17rpml18ygzyddw75c9zgidmd3dgvi5qrpv057zcsif4ksn7ar4l") (yanked #t)))

(define-public crate-boxing-arena-0.9 (crate (name "boxing-arena") (vers "0.9.2") (hash "17vn61jy5yfyfg2npw55s39w3qj3h4qpvf2jv5yr5n1j766iqhb1")))

(define-public crate-boxing-arena-0.9 (crate (name "boxing-arena") (vers "0.9.3") (hash "091aqxzhnbqxmxhp0hm7rgifvhwnmb45xmzjkj765xyf3p15ixn5")))

(define-public crate-boxing-day-0.1 (crate (name "boxing-day") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d8ixwva9dqq1bzc2hycgpza920jxg8yq99dyp6ph8g15xw4hh54")))

