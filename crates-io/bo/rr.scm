(define-module (crates-io bo rr) #:use-module (crates-io))

(define-public crate-borromean-0.1 (crate (name "borromean") (vers "0.1.0") (hash "1x0cf0ivlanbsxppm82akpf707pkhkq9qlgdcqd4qrj4r2hqb32h")))

(define-public crate-borrow-0.1 (crate (name "borrow") (vers "0.1.0") (hash "10z1dix82j08r5d73vvxs9ii2bwvzc4059isid0ajb9kcar9hps1")))

(define-public crate-borrow-bag-0.1 (crate (name "borrow-bag") (vers "0.1.0") (deps (list (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "1vcvixljrf4a5r822576531p3y8spjagaylh0vm03dbvbrk3xyzb")))

(define-public crate-borrow-bag-0.2 (crate (name "borrow-bag") (vers "0.2.0") (deps (list (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "0a8hyqhmlpmvk7w00zsqhnkd0myffx2ir8sysq1fsz9372s719p9")))

(define-public crate-borrow-bag-0.3 (crate (name "borrow-bag") (vers "0.3.0") (deps (list (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "1psi3dk0zy8bbybn4gz1b46yy8cjjgp66x44rll3k0xf6jypp5df")))

(define-public crate-borrow-bag-0.3 (crate (name "borrow-bag") (vers "0.3.1") (deps (list (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "1p840i131azp7s0h8bzrawwvwqdr0amhvy1dbiza3dkh6mz938y4")))

(define-public crate-borrow-bag-0.3 (crate (name "borrow-bag") (vers "0.3.2") (deps (list (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "0mjmkxlic7fgpk99qzm584a014ksa2s9acf77bdjglrz8mzijbxj")))

(define-public crate-borrow-bag-0.4 (crate (name "borrow-bag") (vers "0.4.0") (deps (list (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "0pm1p7r3sx7zi94b9irp8n11ss75b8j7wr9bl4r3mgs0n3ph1y2w")))

(define-public crate-borrow-bag-1 (crate (name "borrow-bag") (vers "1.0.0") (deps (list (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "0vx5bgc7i39xwsky2ncyma6f5y50nyyazqhw5vsicqz252lmzkx8")))

(define-public crate-borrow-bag-1 (crate (name "borrow-bag") (vers "1.1.0") (hash "1mmgjzm2ivb6y7bj0j6kslqp8qri6g3n4jzq3hjjgxcf3fc1za12")))

(define-public crate-borrow-bag-1 (crate (name "borrow-bag") (vers "1.1.1") (hash "1kzwqyakbdl8cndskfhz8vvr091l5p571abbm5gz6jd19pizwm5n")))

(define-public crate-borrow-graph-0.0.1 (crate (name "borrow-graph") (vers "0.0.1") (deps (list (crate-dep (name "mirai-annotations") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1anp6psfp889vmsjwdbwdy3lpdgm160ngnm4051ay3vkxc5dnp69")))

(define-public crate-borrow-or-share-0.0.0 (crate (name "borrow-or-share") (vers "0.0.0") (hash "1i1nv89fp1i2igw01kq9h9xqbvpgmwc1xvq5nfkk4cjdj58lx5fw") (features (quote (("std") ("default" "std"))))))

(define-public crate-borrow-or-share-0.0.1 (crate (name "borrow-or-share") (vers "0.0.1") (hash "11rpgnq9kr5jxr97v6z4wr4a2598hs2wyb67p383l17j2c4xr9vv") (features (quote (("std") ("default" "std"))))))

(define-public crate-borrow-or-share-0.1 (crate (name "borrow-or-share") (vers "0.1.0") (hash "0wfmyigdyiijkhv0s5cwnb3g0f34ri1wry9yx5ji8cfgallwnq2f") (features (quote (("std") ("default" "std"))))))

(define-public crate-borrow-or-share-0.2 (crate (name "borrow-or-share") (vers "0.2.0") (hash "0y1m20c5ycdhi7943ybgf3qihrfsvb8784inwvyb24593kw5yrxp") (features (quote (("std") ("default"))))))

(define-public crate-borrow-or-share-0.2 (crate (name "borrow-or-share") (vers "0.2.1") (hash "0bgh8rwl0113nllfigclzm4f3ddcfhsq1cf8m2f7x2dqdv7i0by9") (features (quote (("std") ("default"))))))

(define-public crate-borrow-or-share-0.2 (crate (name "borrow-or-share") (vers "0.2.2") (hash "0ciski5i69a8mx6f0fh901hn73fii3g39lpl8k3xgi88651b9siy") (features (quote (("std") ("default"))))))

(define-public crate-borrow-owned-0.1 (crate (name "borrow-owned") (vers "0.1.0") (deps (list (crate-dep (name "futures-channel") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "11s7jvzjbb3jyv3ss8p26bnv2lcaa7a06r8vnwa406pn70hqlkr1") (features (quote (("default") ("async" "futures-channel"))))))

(define-public crate-borrow_as-0.1 (crate (name "borrow_as") (vers "0.1.0") (deps (list (crate-dep (name "tuple_utils") (req ">=0.3.0, <0.4.0") (default-features #t) (kind 0)))) (hash "10iwsx1pcl6jbppa16ab57al3dhz6gadfm62xc5mhi8qvy8ksgi4")))

(define-public crate-borrow_db-0.0.0 (crate (name "borrow_db") (vers "0.0.0") (hash "02kc7kav7vli7c0s61mras3j5gs2wxnicy10b9bv53k89r5fpzhn") (yanked #t)))

(define-public crate-borrow_trait-0.1 (crate (name "borrow_trait") (vers "0.1.0") (deps (list (crate-dep (name "atomic_refcell") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cell") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0svh3qd65b56nv3kd0p2jlqmhldnh44d0645d8id1f6d5whnlibk") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-borrow_trait-0.1 (crate (name "borrow_trait") (vers "0.1.1") (deps (list (crate-dep (name "atomic_refcell") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cell") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0dkq255g2lhsk86xkacb4839xv3wsbgsbjndayqqjm9ibzyda4cd") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-borrow_with_ref_obj-0.1 (crate (name "borrow_with_ref_obj") (vers "0.1.0") (hash "0f678688hd7w8ijx7r2h5zqs61wpqn5yn8d66lfylgfmky4c0a73")))

(define-public crate-borrow_with_ref_obj-0.1 (crate (name "borrow_with_ref_obj") (vers "0.1.1") (hash "0l5vs9sbd2ani02xm4576srxmi088h5cz85x0xhsrlia5hj00wkr")))

(define-public crate-borrow_with_ref_obj-0.1 (crate (name "borrow_with_ref_obj") (vers "0.1.2") (hash "0130d7p603gbfy8iqwddc710qdk5bcpcgrplniixi8c3zn4hj3hq")))

(define-public crate-borrow_with_ref_obj-0.1 (crate (name "borrow_with_ref_obj") (vers "0.1.3") (hash "0f2qaxnav8l15lpcfay5hh0wwp8bvddkmq6d1balmlmzxv2yb9mg")))

(define-public crate-borrowck_sacrifices-0.1 (crate (name "borrowck_sacrifices") (vers "0.1.0") (hash "19qjbn80wszzawwv8k31hy9966s39kwzsf2h2p0jl6v31h6kg3ig")))

(define-public crate-borrowed-byte-buffer-0.0.1 (crate (name "borrowed-byte-buffer") (vers "0.0.1") (hash "0swqgiag9nqb5jkmi3h8p7cngfs2sh7k7zdy94af3j5c31zxcy4m")))

(define-public crate-borrowed-byte-buffer-0.1 (crate (name "borrowed-byte-buffer") (vers "0.1.0") (hash "13xdams3d7x7bmynsyb0ll6y9lls306hg9w4sb6l5jckyb22sj57")))

(define-public crate-borrowed-byte-buffer-0.1 (crate (name "borrowed-byte-buffer") (vers "0.1.1") (hash "1vi003sxsjsvwsmws55zvjss7nj6nf73l3f5pdc3glixa29njqgg")))

(define-public crate-borrowed-thread-0.1 (crate (name "borrowed-thread") (vers "0.1.0") (hash "04a6rl3cfirshhdawq2yf07bqyamg1cx8krvkci3lxizn5rl7p2n")))

(define-public crate-borrowed-thread-0.1 (crate (name "borrowed-thread") (vers "0.1.1") (hash "158p2qdxgkcwsjd5p4546q9zj2kxm5rm4d424qabhbl12nc7knms")))

(define-public crate-borrowed-thread-0.1 (crate (name "borrowed-thread") (vers "0.1.2") (hash "0dww33jpskj0pzl309hhi7890m89gb67p1drggjazmpxk1m3c33g")))

(define-public crate-borrowed-thread-0.1 (crate (name "borrowed-thread") (vers "0.1.3") (hash "04q9kincimk437z81g0wmp7yr8ihnh97fbqri73dchh1d47ijvw1")))

(define-public crate-borrowfix-0.1 (crate (name "borrowfix") (vers "0.1.0") (hash "0inf76n5ls86wbb6y2spd0nd5jy2xjygrmgi4z8waa0wvqnj8ghp")))

(define-public crate-borrowfix-0.1 (crate (name "borrowfix") (vers "0.1.1") (hash "1xm80shq5d49qydgrg0ak42pi4f31cd7p6p791f80kqjmhj3pizh")))

(define-public crate-borrowfix-0.1 (crate (name "borrowfix") (vers "0.1.2") (hash "1rzbz02i0br4dw4w5zjwhwcjk6b9q2q8g86d5klg233cncj5bq40")))

(define-public crate-borrowfix-0.1 (crate (name "borrowfix") (vers "0.1.3") (hash "0mrp1vg9zz9mkpqvwv8pn437x08sdv726kblwhrmpz1r0x4nq1w5")))

(define-public crate-borrowfix-0.1 (crate (name "borrowfix") (vers "0.1.4") (hash "0zv3rzmhz5605rgs2ng4y9nm92b2pzfz48dpm7vp8khjmk9p528h")))

(define-public crate-borrowing_exerci-0.1 (crate (name "borrowing_exerci") (vers "0.1.0") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0lk416203df3jqdfgvh1bf7rld4c6kh7lbvgcca4sifj13lkhwx3") (features (quote (("ok") ("error"))))))

(define-public crate-borrowing_exerci-0.2 (crate (name "borrowing_exerci") (vers "0.2.0") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "12anacp0hphr29c7wrqp58k2xqw38p8lsrzsdnh29ams4gwy0hrp") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2 (crate (name "borrowing_exerci") (vers "0.2.1") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "05zxrvfpvrbm3ylf4gaqz6gvbx7nglpq3sygwxyi3lc0pjxakgvj") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2 (crate (name "borrowing_exerci") (vers "0.2.2") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1g1p9k0bpip35l1k9jjwp6g2qj8i8k0m7mffdrj7a499akg358l5") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2 (crate (name "borrowing_exerci") (vers "0.2.50") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0lkba52jcl1a4ig43d18b2kj2v11wgbwm3qkw96497nvhjm2fxqp") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2 (crate (name "borrowing_exerci") (vers "0.2.51") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0qyc8spjq9w8crpzh4knmadl932wkifsig4c9mcmmwqaccxy1ff0") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.2 (crate (name "borrowing_exerci") (vers "0.2.52") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0r6zwlar486l1mipxyfdlgg2z92f0w6xgmkwvh2ai9zmrdadspsp") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3 (crate (name "borrowing_exerci") (vers "0.3.50") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0jqvk66b6j506d860qiix6vsiyv1x6rqvkni7czxppz7awjh42zg") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3 (crate (name "borrowing_exerci") (vers "0.3.51") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "068kqhpyxzbdfai9msalgcfbiy00v34vbzqj2p8gggc1n2r5g7jf") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3 (crate (name "borrowing_exerci") (vers "0.3.52") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "18051r431q2kjvkvmvbd53x0c30wwdj40pg9z177py1cfhli80va") (features (quote (("ok") ("err"))))))

(define-public crate-borrowing_exerci-0.3 (crate (name "borrowing_exerci") (vers "0.3.53") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1snf6fa56sksbp3vxg0gbd8sais0hjvql9grva077iy8qr0x6idd") (features (quote (("ok") ("err") ("cp"))))))

(define-public crate-borrowing_exerci-0.4 (crate (name "borrowing_exerci") (vers "0.4.50") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "15dv06rszahfd6fvx673z7iriy8n2shf9ld7krx2j7r17qcn6362") (features (quote (("okey") ("okay") ("ok") ("err_11") ("err_10") ("err_09") ("err_08") ("err_07") ("err_06") ("err_05") ("err_04") ("err_03") ("err_02") ("err_01") ("err") ("cp"))))))

(define-public crate-borrowing_exerci-0.4 (crate (name "borrowing_exerci") (vers "0.4.51") (deps (list (crate-dep (name "bat") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0fqbg459qn9p070biw76arvjfzy5xbb7f8i2gsv46ypg0vjz324s") (features (quote (("okey") ("okay") ("ok") ("err_11") ("err_10") ("err_09") ("err_08") ("err_07") ("err_06") ("err_05") ("err_04") ("err_03") ("err_02") ("err_01") ("err") ("cp"))))))

(define-public crate-borrowme-0.0.1 (crate (name "borrowme") (vers "0.0.1") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "0qfj4c6f1hn95k32npn0vy66q5hzc5nlnlgv3l1hfzzfb71g4gff") (rust-version "1.65")))

(define-public crate-borrowme-0.0.2 (crate (name "borrowme") (vers "0.0.2") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "1i5cr7znlp0kykv1mql2c3v73s5mnsl340827pi4fa18hg52v14v") (rust-version "1.65")))

(define-public crate-borrowme-0.0.3 (crate (name "borrowme") (vers "0.0.3") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "0qj18kx11z9gz78glgg4sg9aiznx52nvlzpr33zw74idsj0bplir") (rust-version "1.65")))

(define-public crate-borrowme-0.0.4 (crate (name "borrowme") (vers "0.0.4") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "121a4an59v9any18zk8dirhvydzd4747mfh5m78zc20q1dcqlm59") (rust-version "1.65")))

(define-public crate-borrowme-0.0.5 (crate (name "borrowme") (vers "0.0.5") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "1kh9bjyf75nd6lhr1s55halhqh017gvx124s3nrazpgyqj9r6bil") (rust-version "1.65")))

(define-public crate-borrowme-0.0.6 (crate (name "borrowme") (vers "0.0.6") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "0aarnlvcvyggrwqg2kbyplg62aay40gbi8yjgmp1aarphnwg895a") (rust-version "1.65")))

(define-public crate-borrowme-0.0.7 (crate (name "borrowme") (vers "0.0.7") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "0jdir9zqc8g3rr0alnvwz1sy75mhfzv30b75658xbpl8axfnxiyk") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.8 (crate (name "borrowme") (vers "0.0.8") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "17avlbqnxggvcmqjj818ibdzyrjy9ccpnwy17pampdggxqvhlxzk") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.9 (crate (name "borrowme") (vers "0.0.9") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "1z8zgn716fs1jzvlbh73iy7g1n1bhzjd7ly0yyfp1gnfmkmz8yng") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.10 (crate (name "borrowme") (vers "0.0.10") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "1967c5hrjr2mrg4wwpikpsrm0gwxwyjfzhd6gxsgbv1yravzbnla") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.11 (crate (name "borrowme") (vers "0.0.11") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "04ykhnynxm7c6byn78s9y30ldfvzbqzigg58ziv4fkqs1w8fki86") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.12 (crate (name "borrowme") (vers "0.0.12") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "0jbfphkxcqv963dc9477x77vr0q8k4p5h26g5zpvww1cchlvkgap") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.13 (crate (name "borrowme") (vers "0.0.13") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "0h65dbcsdd1kiyc6zgdjpy64ziaq6av9wdw4chxn0wgw114zbjz2") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.14 (crate (name "borrowme") (vers "0.0.14") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "1803s92pz8gfyyspmp27fnk98683x94y5ng3x5pglgzy2z4a7ldy") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-0.0.15 (crate (name "borrowme") (vers "0.0.15") (deps (list (crate-dep (name "borrowme-macros") (req "=0.0.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "06f5hjpzfpx1sb5q3lgbmhpd288ihx51irzgyaqwl1595y4dwxfc") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.1 (crate (name "borrowme-macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1schdr1fc15yv3pnsyrmdj3fn6cx6jqinwbb010r4vzxq9b6vy9j") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.2 (crate (name "borrowme-macros") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1f2jvc7c7zdl87zw6x23vgcyyyxv06ix8frf0bi4alrmwl3rrn0c") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.3 (crate (name "borrowme-macros") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1k4w258cg23xvr0rq2i6prqx4am8wrj0f18z5frjzy39v97h9li0") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.4 (crate (name "borrowme-macros") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0bhrpdxrya1mfxig1ra3gls3f8fw26a0lprpjsqs9vxpd4zykm5z") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.5 (crate (name "borrowme-macros") (vers "0.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0hqjzmfdmj9fimxnj02nb0p6619agr2fwsy6sqw1429910v6xcjj") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.6 (crate (name "borrowme-macros") (vers "0.0.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1830b8ys55svmqs1rcwfh20rczh7yi5zqizwzcg1bvqy2z8n13vz") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.7 (crate (name "borrowme-macros") (vers "0.0.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1xm4df8yj0xn9js2xl4vmd2yf6910kd8l5zhna9fwab73p5pbyvc") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.8 (crate (name "borrowme-macros") (vers "0.0.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0mfjqxvzf0gh726s1hyidqgjg9jj5x6n0dhgg5inmfbraq1cbax1") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.9 (crate (name "borrowme-macros") (vers "0.0.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1c547fzsv19j920gz4b50qab8lnhxhkz96s3j8d3rxag48qazf81") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.10 (crate (name "borrowme-macros") (vers "0.0.10") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1plvhgbz0jpa8i3msdj8abmlgxrm1adibinj1ss8g64d3qsby4v0") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.11 (crate (name "borrowme-macros") (vers "0.0.11") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1f9bnb6aqd97hxs4ch771b05j44c9kfcb27qyia1vr53pzfawdiz") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.12 (crate (name "borrowme-macros") (vers "0.0.12") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0nc0dbp6dsvsa894zj0sz3krlkrn076g66z9bnbhpi7ckxrldas7") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.13 (crate (name "borrowme-macros") (vers "0.0.13") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0l11r3j9q6lha6h2wwlzaqlv133z3cf3v5v4yv72jy47qzid50vh") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.14 (crate (name "borrowme-macros") (vers "0.0.14") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1y53cvrxkirf08w9sp2q3q1ij4jnb15w3i5w4310zgwd37mzv6p1") (rust-version "1.65")))

(define-public crate-borrowme-macros-0.0.15 (crate (name "borrowme-macros") (vers "0.0.15") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1l20q2yzzahmswbidfnry4idaizhk9xar7a230n7idad6jdls3xh") (rust-version "1.65")))

(define-public crate-borrown-0.1 (crate (name "borrown") (vers "0.1.0") (hash "15m74nh5bhkcfgr8sivk44cs0irn5x7yll23crhfsf76d2rmg2q0")))

(define-public crate-borrowned-0.1 (crate (name "borrowned") (vers "0.1.0") (hash "0im6mjr8s1s46jkpkgp0wjxhqk20ig6pdlgz5i4h5ixmjjlrm9wx")))

(define-public crate-borrowned-0.1 (crate (name "borrowned") (vers "0.1.1") (hash "1qilj0xy5cjgaky5x7x3hx5zd2bgkan3bl7328126f1xj7bm0nzp")))

(define-public crate-borrowned-0.2 (crate (name "borrowned") (vers "0.2.0") (hash "1p8s24h8igc09pnn8420lhp2kvb1rhg5b9xnn9pc5ip6kh8agmnv")))

(define-public crate-borrowned-0.3 (crate (name "borrowned") (vers "0.3.0") (hash "1iahr5jwscjkdgvr4lfsy3wq2aly20r24kfw09qx4y0ghgzfk5h7")))

(define-public crate-borrowned-0.4 (crate (name "borrowned") (vers "0.4.0") (hash "17f813zikl58winii6jd0bamidgm2fhwh6w6b3kkv0cmdhrw3r9x")))

(define-public crate-borrowned-0.5 (crate (name "borrowned") (vers "0.5.0") (hash "1145igp4w6lk0cm8nkr13rlcd2dhh1rcq4wais4dwsizgc3q8n2k")))

(define-public crate-borrowned-0.6 (crate (name "borrowned") (vers "0.6.0") (hash "1iclkm2jhn7i3367w6q9pmy2dmz8dr0nhv9d8dxw3mmqgm2d2fxj")))

