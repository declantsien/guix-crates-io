(define-module (crates-io bo rk) #:use-module (crates-io))

(define-public crate-bork-0.2 (crate (name "bork") (vers "0.2.1") (hash "1wmmv2xjbkcqnvsyhhqs6jm4j172bnc0slhh0w13qv43df2gq0ir")))

(define-public crate-borked-0.1 (crate (name "borked") (vers "0.1.0") (hash "00f0j0xgcndmhjfkpgalv8s2c7m9adva76102jk48fxs6kk7lf9b")))

(define-public crate-borked-0.1 (crate (name "borked") (vers "0.1.1") (hash "1y90css9wqqsdard9ra8lnwsqvhi7p81y9s19lk0lgfrky07snzj") (yanked #t)))

(define-public crate-borked-0.1 (crate (name "borked") (vers "0.1.2") (hash "1al9c7dn7hh8f6qqgdl60hfwaix74ingx96k09jldq6ikb2p4qjq")))

