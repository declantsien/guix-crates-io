(define-module (crates-io bo of) #:use-module (crates-io))

(define-public crate-boof-0.1 (crate (name "boof") (vers "0.1.0") (hash "0rxqfymnifzd7jaznjlb9622mnhbri4ipxacxs2s28kfcrzbzbcp")))

(define-public crate-boof-1 (crate (name "boof") (vers "1.0.0") (hash "0a71x0xj0f2i2d81h3rd5sp2zh4a4d6fkc1fa3j7p1kmakws1an4")))

