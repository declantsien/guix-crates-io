(define-module (crates-io bo ja) #:use-module (crates-io))

(define-public crate-boja-0.1 (crate (name "boja") (vers "0.1.0") (hash "03g309kpw6i9p7jyfgb9z6q58d8zxwlaa7jm8xfxylraidahxfdq")))

(define-public crate-boja-0.2 (crate (name "boja") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "ariadne") (req "^0.2.0") (features (quote ("auto-color"))) (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1aw077pj5acs2lchmi504fkl4mcbw1msrnbpmqlbg55agkyc01d3")))

(define-public crate-boja_guessing_game-0.1 (crate (name "boja_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "116dmxmqdsrm13x8qn4ps02ajmd84f7wr5gxpcy7bb9qbi43h81r")))

