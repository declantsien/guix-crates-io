(define-module (crates-io bo -h) #:use-module (crates-io))

(define-public crate-bo-helper-0.1 (crate (name "bo-helper") (vers "0.1.0") (hash "04lrd1z5y3wbnwhvdbrf9dmlk047lg57l6kmhbfyz7j4mfmbmbiw")))

(define-public crate-bo-helper-0.1 (crate (name "bo-helper") (vers "0.1.3") (hash "1dl62d8qqy1kls777pwg5x70nbhcqzya7sii9s2kd0mvnpqpzbs6")))

(define-public crate-bo-helper-0.1 (crate (name "bo-helper") (vers "0.1.24") (deps (list (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1py5p3fb8ihdhvr2y1m3f24bn1wj4g0w5cg9145qk8lfb9l3zcy2")))

