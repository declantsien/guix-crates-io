(define-module (crates-io bo id) #:use-module (crates-io))

(define-public crate-boids-0.1 (crate (name "boids") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.18") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "puffin") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "puffin") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "puffin_http") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raylib") (req "^3.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "045nmpkf7r27sgvklhy0f9780ih07zp82611qrvq6ssaw40kidw8")))

(define-public crate-boids-rs-0.1 (crate (name "boids-rs") (vers "0.1.0") (deps (list (crate-dep (name "nannou") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "0rr3bz4n4hzw9yp4wznn31i8k3hwryvyn81fm42pgw2396add3h1")))

(define-public crate-boids-rs-1 (crate (name "boids-rs") (vers "1.0.0") (deps (list (crate-dep (name "nannou") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "162m8a84ddm78mir30kp5qjn078rk23hljzmspr46a7qjpwyv76j")))

(define-public crate-boids_rs_bevy-0.1 (crate (name "boids_rs_bevy") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "bevy-inspector-egui") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "09kp0dsvbwcdc047q6m7x9l3lphwy9x6n8599j95cykhg7mv9x5i")))

