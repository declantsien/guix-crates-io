(define-module (crates-io bo oh) #:use-module (crates-io))

(define-public crate-boohashing-0.1 (crate (name "boohashing") (vers "0.1.0") (deps (list (crate-dep (name "digest") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "01spap2fmhi9f4lr9rg9k92m10qahwyjcrmiqkwmlvijink9kcsl")))

