(define-module (crates-io bo gu) #:use-module (crates-io))

(define-public crate-boguin-0.1 (crate (name "boguin") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5.10") (default-features #t) (kind 2)) (crate-dep (name "http-with-url") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1ldvz1lsl53pma0k9pmrdm9iz8vn8h9wxpb62r623pklmpl05na3") (yanked #t)))

