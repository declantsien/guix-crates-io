(define-module (crates-io bo eh) #:use-module (crates-io))

(define-public crate-boehm-rs-0.1 (crate (name "boehm-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "04n8l0kl592i0ljrmcw9l93qjfd7hhmhhlq4q9icdkqm9g0jqjf6")))

(define-public crate-boehm-rs-0.1 (crate (name "boehm-rs") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wrkhls7bvprs1ln4z74vlgnh4c5bp637hixxbwv3szacr1v5ac6")))

(define-public crate-boehm_gc-0.0.1 (crate (name "boehm_gc") (vers "0.0.1") (hash "1vwab2wrlaznc83j6l95dmakd4816klfk56mban1jd5wr16128ny")))

