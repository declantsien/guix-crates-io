(define-module (crates-io bo ol) #:use-module (crates-io))

(define-public crate-bool-0.1 (crate (name "bool") (vers "0.1.0") (deps (list (crate-dep (name "parameterized") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0w21wlm25hplz7nczd7kcy99lv5jgx8yg367g27i9b7hk838hyfg") (features (quote (("global_values") ("default" "global_values"))))))

(define-public crate-bool-0.2 (crate (name "bool") (vers "0.2.0") (deps (list (crate-dep (name "parameterized") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1a9w0rx9531w95ffni48xsx58ny4s0nlfckwy1dkrihng6m7rnsw") (features (quote (("global_values") ("default" "global_values"))))))

(define-public crate-bool-0.3 (crate (name "bool") (vers "0.3.0") (deps (list (crate-dep (name "parameterized") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1j48lr9hxigl3kb10f6dz6fzkykjbhixigjymz3dk7vd4sfqaw1b") (features (quote (("has_alloc") ("global_values") ("default" "global_values" "has_alloc"))))))

(define-public crate-bool-logic-0.1 (crate (name "bool-logic") (vers "0.1.3") (deps (list (crate-dep (name "nugine-rust-utils") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0jpdca94bsg57c5wrm67bd8snyxav872gy367kwd6pabp1xs1mjj")))

(define-public crate-bool-logic-0.2 (crate (name "bool-logic") (vers "0.2.0") (deps (list (crate-dep (name "nugine-rust-utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0dip013s0i2dbyb1xzn82gav7irlzgvd43xbs1qqar0qfj3r1kdi")))

(define-public crate-bool-mappings-0.1 (crate (name "bool-mappings") (vers "0.1.0") (hash "17g57fad7yzdfalgx47a92ryjf7h4yagbf4vnicwcnbv6w92n1x2")))

(define-public crate-bool-mappings-0.1 (crate (name "bool-mappings") (vers "0.1.1") (hash "154hjx3d74ywn9hjksm3cv9w88z23swvk6qdfjh9r1w757apybp7")))

(define-public crate-bool-mappings-0.1 (crate (name "bool-mappings") (vers "0.1.2") (hash "04pzrpnyngy38f9i844vsls0l304saayyvcx92db1m4fsd3awkqq")))

(define-public crate-bool-toggle-1 (crate (name "bool-toggle") (vers "1.0.0") (hash "1chn6qdg6r2hd6zqr4ml5477i1chsk1rk9zba9xd37ihmhs84ms1")))

(define-public crate-bool-toggle-1 (crate (name "bool-toggle") (vers "1.1.0") (hash "1d5c5ci67nb2777day92ahfa69ilq944lnjfbs11nivszsm9drh9")))

(define-public crate-bool-toggle-1 (crate (name "bool-toggle") (vers "1.1.1") (hash "0g37kiymal236ckh6bxv2i5l30x10xzdkisz7n5vix1nqwrh0m7j")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.0") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "11whcbqjwrjdskfk69lz3fm29a69r0ywnbvgb9zbnhjs7jjnpva5")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.1") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "130vmy1czv7b6799jv61fq0bpcgnb5mjrvkf5v6lgq5zbpksl5hw")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.2") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02ns8jwy25c0g3j3hq85pv36rjlxh2c82dmac26a83cjlbr4wnav")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.3") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1mwbnmx12z11fa3xc7svfw3szrc1zy42s1fglkr11nvnv4qinjdn")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.4") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0jf0zqmiv1spz96xyrjn34ryn0yyglq84y9ik6ig8n3q1pg2yhnh")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.5") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1zw63hxrb749b9q6hgzscx7v8f0fx5x65pryvzfcmq3lawicwjfb")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.6") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "17p59nk9ji1hxdmq9qgq8wsqhrik34w2w97l8zd3sw03g0xk2436")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.7") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.1.0") (default-features #t) (kind 0) (package "rustlogic-march1917")) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "06mcnl356rg5yhhacig7888jfwxaiqh35l75c4j637yh75pxikng")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.8") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.1.0") (default-features #t) (kind 0) (package "rustlogic-march1917")) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0m67c18cw3wym5qv4r6ajd86gsdcswmcycpdphb60agvdwspbh1b")))

(define-public crate-bool2cnf-0.1 (crate (name "bool2cnf") (vers "0.1.9") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustlogic") (req "^0.1.0") (default-features #t) (kind 0) (package "rustlogic-march1917")) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0hrv7fl7q3hvaan9h5kwxisbqgmrh53nx07bzhxcqxgf9sp4z6yg")))

(define-public crate-bool32-0.1 (crate (name "bool32") (vers "0.1.0") (hash "0hfpg6as7mx8pbvh0fw9db1hw5vrfl98zj4mxmrz8z115w2mv1qc")))

(define-public crate-bool32-0.1 (crate (name "bool32") (vers "0.1.1") (hash "1z26kipfp33csyyl77rydbfhb012z51azhcdnc8asmsy68vrfig8")))

(define-public crate-bool32-1 (crate (name "bool32") (vers "1.0.0") (hash "0iv8xz6xddwhvzjcx77ydlghqli52sjqdzkxq85gpy2p1djfzl3f")))

(define-public crate-bool_expr_parser-0.3 (crate (name "bool_expr_parser") (vers "0.3.13") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "0jzjhvqc01q2z67z82awnb3zrc5ssd7n5dz3p50bf65h13qlr9is")))

(define-public crate-bool_ext-0.1 (crate (name "bool_ext") (vers "0.1.0") (hash "1im0ihjzy0m4l6jg7aaawawh28jm0pnfg2si04pv827w8ws27abp") (yanked #t)))

(define-public crate-bool_ext-0.1 (crate (name "bool_ext") (vers "0.1.1") (hash "1648c0i0c7dga62jqsv7mp3dkw84zpi2mfxrbp6bk6lhm82j6gj1")))

(define-public crate-bool_ext-0.2 (crate (name "bool_ext") (vers "0.2.0") (hash "0i352wibds2m3p3gw2ak97q9z6db54mqdfvkn7256w8haqdj4mwh")))

(define-public crate-bool_ext-0.3 (crate (name "bool_ext") (vers "0.3.0") (hash "1r5jgqrc5dmqc864ih9z943dvall8hbdhb0hjnarjn1zsn7zvjjj")))

(define-public crate-bool_ext-0.3 (crate (name "bool_ext") (vers "0.3.1") (hash "1lls4v9s70wckscmdnmijrx6l2pwcfcyfs6m05qfs6hqiwq1lxmb")))

(define-public crate-bool_ext-0.4 (crate (name "bool_ext") (vers "0.4.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0c1ifjy1ll36ld9f8b7pxnpblqgxicv443l34pwb6hwqi8m81b1h")))

(define-public crate-bool_ext-0.4 (crate (name "bool_ext") (vers "0.4.1") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0l6micr39xrziahf1lfw2xq2hvfnx35bd8q2ckn60987immhnzd5") (yanked #t)))

(define-public crate-bool_ext-0.4 (crate (name "bool_ext") (vers "0.4.2") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1jpryyz518fqlb636vm1l7cvpgm6k934zz5yyp4d5lp3lgd0xlnb")))

(define-public crate-bool_ext-0.4 (crate (name "bool_ext") (vers "0.4.3") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ng2jylw3l2lw7pynkzink39b0f24ydjg0sw16pa24kdnavvdk2a")))

(define-public crate-bool_ext-0.5 (crate (name "bool_ext") (vers "0.5.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1pgjz6wgh9vvk0sbi2kik9vjrlsiys2rif0fv9wvq5ajvypxw57v")))

(define-public crate-bool_ext-0.5 (crate (name "bool_ext") (vers "0.5.1") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1wayjnr1i5xqc6hxx7w24069ryn854ylzq5ckfjan18pbnpsbapl")))

(define-public crate-bool_ext-0.5 (crate (name "bool_ext") (vers "0.5.3") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1h7xwpfq3jm5m8shmj010bzqjbgc68g3kij02vfrmndkbwwwyhnz") (features (quote (("std") ("default" "std"))))))

(define-public crate-bool_ext-0.6 (crate (name "bool_ext") (vers "0.6.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1izr85rm8vwfjpmmlnp2nl32q9fdcx1rwaab1n7aw8w7b1rins6s") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-bool_ext-0.6 (crate (name "bool_ext") (vers "0.6.1") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "044zsbygb7inwxk1x4v4ksy70q83zgzcbwazid8369p2d9ywc36v") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-bool_ext-0.6 (crate (name "bool_ext") (vers "0.6.2") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0q5yk6m4yhbhs7wwf34iy5zr8v9ak7j6vnpgawqyzp5hfxyr65gg") (features (quote (("std") ("default" "std"))))))

(define-public crate-bool_to_bitflags-0.1 (crate (name "bool_to_bitflags") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 2)) (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "procout") (req "^0.1.13") (features (quote ("procout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "typesize") (req "^0.1") (default-features #t) (kind 2)))) (hash "110asfja58jyca2hd25w43sql32agsr4k1192a9r8lalv70dg21v") (features (quote (("typesize")))) (v 2) (features2 (quote (("procout" "dep:procout"))))))

(define-public crate-bool_to_bitflags-0.1 (crate (name "bool_to_bitflags") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 2)) (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "procout") (req "^0.1.13") (features (quote ("procout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "typesize") (req "^0.1") (default-features #t) (kind 2)))) (hash "17cvwlgh9givfqbjvp478738zzdd4003kx83ka35cf2r5vnwcvzk") (features (quote (("typesize")))) (v 2) (features2 (quote (("procout" "dep:procout")))) (rust-version "1.65")))

(define-public crate-bool_to_bitflags-0.1 (crate (name "bool_to_bitflags") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 2)) (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "procout") (req "^0.1.13") (features (quote ("procout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "to-arraystring") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "typesize") (req "^0.1") (default-features #t) (kind 2)))) (hash "1q5nai0v4xb38ha3q0mi1di4d8pbvg0v0wia1qm7kkkfaqqz0ai4") (features (quote (("typesize")))) (v 2) (features2 (quote (("procout" "dep:procout")))) (rust-version "1.65")))

(define-public crate-bool_traits-0.1 (crate (name "bool_traits") (vers "0.1.0") (hash "1q66n6fv35pil7wd8fsf6rbivsyp9dyh5c8294v46jxidz2xqvqz") (rust-version "1.77.0")))

(define-public crate-bool_traits-0.1 (crate (name "bool_traits") (vers "0.1.1") (hash "1fzmfmcwvg41q459l2b6wis94jclrx7rmil46q4hfdkkyfzigsa1") (rust-version "1.77.0")))

(define-public crate-bool_vec-0.1 (crate (name "bool_vec") (vers "0.1.0") (deps (list (crate-dep (name "count-macro") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "11v3yxphkf3ihfhck8xs1aqj4h8bblyp70vlbxxdz6dn3ly5zax1") (yanked #t)))

(define-public crate-bool_vec-0.1 (crate (name "bool_vec") (vers "0.1.1") (deps (list (crate-dep (name "count-macro") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0phla9q8p0kqyvjqaxljbmch1npgbz0kzy93v2i43snisccwx56c") (yanked #t)))

(define-public crate-bool_vec-0.1 (crate (name "bool_vec") (vers "0.1.2") (deps (list (crate-dep (name "count-macro") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0kjsrk0j34192slnk23jm8c1p57a9f1qsxmlpall976a2rw9pyl3")))

(define-public crate-bool_vec-0.2 (crate (name "bool_vec") (vers "0.2.0") (deps (list (crate-dep (name "count-macro") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0vxwqn6a11gmqpbzbbk7jcyvwgls39rvby9aajag0685082cc5b2") (yanked #t)))

(define-public crate-bool_vec-0.2 (crate (name "bool_vec") (vers "0.2.1") (deps (list (crate-dep (name "count-macro") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02wawr8pxsk8rpj8j9ikf5ixmgb5qkyd0b9l4f2gfz2vhlk9h4md")))

(define-public crate-booldisplay-0.1 (crate (name "booldisplay") (vers "0.1.0") (hash "03i7p3blfmfl3ib5dir1mihyi75d0jpy7x96gqpnhmpsvpkvvhzm")))

(define-public crate-boole-0.1 (crate (name "boole") (vers "0.1.0") (hash "0ajx8rf9m3d7kxnijk9iyyy2qmqvxpv6xj3w7r5w2zmny9kd2aj2")))

(define-public crate-boole-rs-0.0.1 (crate (name "boole-rs") (vers "0.0.1") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "17wpbfq05jmmx7r05114kbzgdjmmmniv7vpckq13zhiq9ssxc2hr")))

(define-public crate-boolean-0.3 (crate (name "boolean") (vers "0.3.0") (deps (list (crate-dep (name "parameterized") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0pf8fcj3li0w7dbxm06li9l4xcxmw7fa2an1hgnj7dj5j7mi5wzi") (features (quote (("has_alloc") ("global_values") ("default" "global_values" "has_alloc"))))))

(define-public crate-boolean-enums-0.1 (crate (name "boolean-enums") (vers "0.1.0") (deps (list (crate-dep (name "interpolate_idents") (req "^0.2.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.15") (optional #t) (default-features #t) (kind 0)))) (hash "1ql8glrd0rx63vbc1f0264n9nijfcz0fblvz7wsa2ypa8cjbip03") (features (quote (("serde" "interpolate_idents" "serde_derive"))))))

(define-public crate-boolean-enums-0.2 (crate (name "boolean-enums") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.15") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 2)))) (hash "11mhqp8rbym17qzcbm5cjyzcd56p3hs91ngvw522c461wn9y6hjm")))

(define-public crate-boolean-enums-0.3 (crate (name "boolean-enums") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.15") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 2)))) (hash "0fjwnbd7kn6cfjywvf2skwmk0dr1ijmrwcq2mznszi36vqrqzr0n") (features (quote (("std") ("default" "std"))))))

(define-public crate-boolean-enums-0.3 (crate (name "boolean-enums") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.15") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 2)))) (hash "0xj3y7iwsw6qpkkhzxmpxzljydp11awwvrn24ca4p5yxcnn7wxcb") (features (quote (("std") ("default" "std"))))))

(define-public crate-boolean-enums-0.3 (crate (name "boolean-enums") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0.15") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 2)))) (hash "176grilhm9sa0r0wdxri62vnc4y1hw06v8b6i41828bis7pkils6") (features (quote (("std") ("default" "std"))))))

(define-public crate-boolean-enums-0.3 (crate (name "boolean-enums") (vers "0.3.3") (deps (list (crate-dep (name "serde") (req "^1.0.15") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "188mf29hgwg5nmvh7yyvkma01capw7zvw0x8xa5qv8glr6j7vjlx") (features (quote (("std") ("default" "std"))))))

(define-public crate-boolean-enums-0.3 (crate (name "boolean-enums") (vers "0.3.4") (deps (list (crate-dep (name "serde") (req "^1.0.15") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1bayp52if5gyrr1fhpc19xjirhxfplqrs7jkpr6bh601c29c0rl6") (features (quote (("std") ("default" "std"))))))

(define-public crate-boolean_checker-0.1 (crate (name "boolean_checker") (vers "0.1.0") (hash "0r6aw9cf9acxy8iq4dnxi0jzniib8lmbzkvsidvhzb693k4qca6n")))

(define-public crate-boolean_expression-0.1 (crate (name "boolean_expression") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "005ri042hc92p4cv1a9xbd184a327pfc2x1nb6h1jycivffkmsvg")))

(define-public crate-boolean_expression-0.2 (crate (name "boolean_expression") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "0d1v9fp6v4p8fks2y1hjhjglc0sdklmyn2b3xf30ak2zdjkl4km2")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nihj0za6mws373rz675bv5gx8kz5h8rdnvdcsn8dp05fa8hdx13")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.2") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "1f47rb2lsr8hj8mga4z68nyd0lzv54bby6710mdxymvkclszv5aa")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.3") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "08hw8qf24sr9br42w2nymirfc2qnq4paczx6amgrr0angdljpgbb")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.4") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "0whhavkmzv6jf8404rdvzx81z57410zdk1xb4laimzdlxly2dbwr")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.5") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cdhg80kklb34jl2cj6lgd26p31wf1jqh1ad37phqchrwig2p8cw")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.6") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cc6ph9ji5ynxc0il3mxq84p87xcgfa3dhx68xkdwywy7y6b5kq5")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.7") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gywdphhjw605fbj05w50b1s4a9s1wd6i1b1jng36g8zmiy6dxnv")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.8") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gxf3plrzhq1kf9bn2n00xpjxx7iid7grywpyc5ab033dybsg1j3")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.9") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "114zfvaf34vfmk3lx0dwl5jq4wvdp2dfmza9s3k8k7zvm1zm5g6q")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.10") (deps (list (crate-dep (name "indoc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "04y95khnj1kxf7i161gnl0d7xgdik7f1agp02dbs3yhywkr4ybf3")))

(define-public crate-boolean_expression-0.3 (crate (name "boolean_expression") (vers "0.3.11") (deps (list (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "10zspcm7h2dsywi4d26h8crj10qf0mn2clnk5wjx58l18iifycvw")))

(define-public crate-boolean_expression-0.4 (crate (name "boolean_expression") (vers "0.4.0") (deps (list (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "1dc5rrkaps962bzav1hhbksq4bnkwmfyb049kl242dz18kikk91x")))

(define-public crate-boolean_expression-0.4 (crate (name "boolean_expression") (vers "0.4.1") (deps (list (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "1lnybvqwj4ipimlwqzgfy20lq2jqlkqahxfdvr829fzpjmafvqaa")))

(define-public crate-boolean_expression-0.4 (crate (name "boolean_expression") (vers "0.4.2") (deps (list (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "1ilaccp28p7069v388b9qwiwdsxxhp29kmj89jagmvwvkdr7nahj") (yanked #t)))

(define-public crate-boolean_expression-0.4 (crate (name "boolean_expression") (vers "0.4.3") (deps (list (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "1npc72hy6188jnr7j80zhsgv9vh8b0ha9a057p5rah4m77y3vbx3")))

(define-public crate-boolean_expression-0.4 (crate (name "boolean_expression") (vers "0.4.4") (deps (list (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "1h77ny61dvp3r7lag33im0hyp9dx75lfj582bv8rrpzms533p8aj")))

(define-public crate-boolector-0.1 (crate (name "boolector") (vers "0.1.0") (deps (list (crate-dep (name "boolector-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i8cg112v32yxnvafrh2c008frrf73q6yfgbc8dx7wbhsakbdg8a")))

(define-public crate-boolector-0.1 (crate (name "boolector") (vers "0.1.1") (deps (list (crate-dep (name "boolector-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14hzx98i7bb31i1bnlvslmbadl56p9ixn48xjaj00nq6qbhv2bk9")))

(define-public crate-boolector-0.1 (crate (name "boolector") (vers "0.1.2") (deps (list (crate-dep (name "boolector-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lkz93ddk005gqwirx70aq5liinrwir1b8a8vs180gyyxng79bvp")))

(define-public crate-boolector-0.2 (crate (name "boolector") (vers "0.2.0") (deps (list (crate-dep (name "boolector-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j3c5n3vfvcb4dicmlgg5w9w1y3yyr1yy89bminiy5br3cz3wi1i")))

(define-public crate-boolector-0.3 (crate (name "boolector") (vers "0.3.0") (deps (list (crate-dep (name "boolector-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s5ad40g4z0a74318h99lni9rgbr0snc6p6iijdhij35l5b208mx")))

(define-public crate-boolector-0.4 (crate (name "boolector") (vers "0.4.0") (deps (list (crate-dep (name "boolector-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "0zp7d80kfjnh1zbpi9rcds9vash9linqcfxwi40bj0yfsl1g4sh4")))

(define-public crate-boolector-0.4 (crate (name "boolector") (vers "0.4.1") (deps (list (crate-dep (name "boolector-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "1liqz03sj27adcwvq158dspv8h1drnm9287jb406ysf4k1hzzmfa")))

(define-public crate-boolector-0.4 (crate (name "boolector") (vers "0.4.2") (deps (list (crate-dep (name "boolector-sys") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "1ah4zvql1i1prgi4g32mqkp8gw6psci2z60ws260wxydl10b9hrd") (features (quote (("vendor-lgl" "boolector-sys/vendor-lgl"))))))

(define-public crate-boolector-0.4 (crate (name "boolector") (vers "0.4.3") (deps (list (crate-dep (name "boolector-sys") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "08gxqlbsy98p89l46ly5kwyj0js2ykyx5bxd4hbgdwf88sggjrsm") (features (quote (("vendor-lgl" "boolector-sys/vendor-lgl"))))))

(define-public crate-boolector-sys-0.1 (crate (name "boolector-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "1ndzsahjmg8p8s29r5r71jm0lb5ds3x847y8ks41xpbrr018mxhq")))

(define-public crate-boolector-sys-0.1 (crate (name "boolector-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "0dp5ganzax4j4l924f94dwshl13hby9cl5k4h688285m2qrkrq6a")))

(define-public crate-boolector-sys-0.1 (crate (name "boolector-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "0lvdaj40nfhxrgm8cgsr9sp616qzbkdgp4g0k06lwsxqj0b75gg2")))

(define-public crate-boolector-sys-0.2 (crate (name "boolector-sys") (vers "0.2.0") (hash "18lkfq5pwc4l5l0zhn6a50dc8kl84j6s5v1vwmianwj13hsnpycs")))

(define-public crate-boolector-sys-0.3 (crate (name "boolector-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mn5rv4ykd06cb7dx3zizklhw98brdvp4vis1kq8gkym34h2lxal")))

(define-public crate-boolector-sys-0.3 (crate (name "boolector-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i7fccirv3ggp8plwzdzpc3yh4q4hlgrimc1dd64z7gplcggw6ps")))

(define-public crate-boolector-sys-0.4 (crate (name "boolector-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kqmg4bbnrw7gsxwa5yfy10n04limvjlc8cyihycv7awjmkfzza7")))

(define-public crate-boolector-sys-0.5 (crate (name "boolector-sys") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "01xhpak0w6iqrmyq7yz0wzhkinh662jpfk1cwapsk454w2w0rh7h")))

(define-public crate-boolector-sys-0.6 (crate (name "boolector-sys") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "1ywdwnsrayzllq56yz9pp9bl9s0vf0c7npgkanri3wgxjjwmw7zn") (features (quote (("vendor-lgl" "cc" "cmake")))) (links "boolector")))

(define-public crate-boolector-sys-0.6 (crate (name "boolector-sys") (vers "0.6.1") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "0a79zcjs9mirvndqn0si726mzx6hyrhr2qi44av4dqymzwpvwz65") (features (quote (("vendor-lgl" "cc" "cmake")))) (links "boolector")))

(define-public crate-boolector-sys-0.6 (crate (name "boolector-sys") (vers "0.6.2") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "07hwrjjpq7g4y56fb2mb802pnjaqx7vl9dq6j4k47q1533bzqdcq") (features (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (links "boolector")))

(define-public crate-boolector-sys-0.6 (crate (name "boolector-sys") (vers "0.6.3") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "1ir7m4farw1m0dd2i99hmqhydpd67z04pbihvp73q4q3fihpl22b") (features (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (links "boolector")))

(define-public crate-boolector-sys-0.7 (crate (name "boolector-sys") (vers "0.7.0") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "0zwxdlig4nqnyzadhnbqh7a6ijb8njaikjf71hcy7r4pw9hrfr8y") (features (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (links "boolector")))

(define-public crate-boolector-sys-0.7 (crate (name "boolector-sys") (vers "0.7.1") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "1rb3b7zd4r82y1psxbsd0phz3xhv8m7i3yqvsm576kfr2hf2s22v") (features (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (links "boolector")))

(define-public crate-boolector-sys-0.7 (crate (name "boolector-sys") (vers "0.7.2") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "14w0d489qrhb4biy4idz92mzd1xdg6f99whh46wly5rpkkvc5w0p") (features (quote (("vendor-lgl" "cc" "cmake" "copy_dir")))) (links "boolector")))

(define-public crate-boolenum-0.1 (crate (name "boolenum") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.30") (default-features #t) (kind 2)))) (hash "1s2xikd5j5p7aywrg4x0qwffi9bv2a12kqcs1br6s0nphpaspj56")))

(define-public crate-boolexpr-0.1 (crate (name "boolexpr") (vers "0.1.0") (hash "0hchq70appxx9xr24jj9dxszg406jwhy8ydc2n55xi4wps21i4lj")))

(define-public crate-boolfuck-0.1 (crate (name "boolfuck") (vers "0.1.0") (hash "04hn6xww5xriz841j2rmy6i0xbm6225lzas6c0537z2cz1fz2lhm") (yanked #t)))

(define-public crate-boolfuck-1 (crate (name "boolfuck") (vers "1.0.0") (hash "1smzv77cac7hxd574g8gibrx74c1b8im1vw24favx35zam68slc2") (yanked #t)))

(define-public crate-boolfuck-1 (crate (name "boolfuck") (vers "1.0.1") (hash "0bg8xkqc97rc7d2mj09a1r8ndijyrq5fgrr38kxlmjli3ii0827y")))

(define-public crate-boolinator-0.1 (crate (name "boolinator") (vers "0.1.0") (hash "0z052qap49ixbikqfm4r64156dv9ynjbz2x4zjxbywzjwbgrmj47")))

(define-public crate-boolinator-2 (crate (name "boolinator") (vers "2.4.0") (hash "1nccxzb1dfkjfrgzqaw1a90p26zlvv6nah5ckcpj6bn9a4zqga6g")))

(define-public crate-boolnetevo-0.3 (crate (name "boolnetevo") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pgxrhx92y77b9crwwqwk1ggxg54ar22kaahx90bgjhv6drjbqga")))

(define-public crate-boolnetevo-0.3 (crate (name "boolnetevo") (vers "0.3.1") (deps (list (crate-dep (name "crossterm") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02aj1imnqk2mfjx5jb2cspiw45gjqb3pcbkhcyqgjardnr93nnxx")))

(define-public crate-boolnetevo-0.3 (crate (name "boolnetevo") (vers "0.3.2") (deps (list (crate-dep (name "crossterm") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j03ksmw06px27mrrmhy056dblqyvp9kcz95llqv57f0k47nkpxd")))

(define-public crate-boolnetevo-0.4 (crate (name "boolnetevo") (vers "0.4.0") (deps (list (crate-dep (name "crossterm") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "13biklszx7py5jmjydl4a3knf9r1kq1hgan2knpm7kg9bgdqxkz5")))

(define-public crate-boolvec-0.1 (crate (name "boolvec") (vers "0.1.0") (hash "0ppc7vzjjdblsliihrnbpahqrh8j1gvd8cnly47ak1a1793cqwz0")))

(define-public crate-boolvec-0.2 (crate (name "boolvec") (vers "0.2.0") (hash "07i6lljsvnj8b6qhpak31v2rkb36cz1gfmmp6r7q7py12x9d6mpv") (yanked #t)))

(define-public crate-boolvec-0.2 (crate (name "boolvec") (vers "0.2.1") (hash "0yjdxpf6vzs5v9lz9z07jcl341qjvv77x3dm4lwbjfxi6z96sycr") (yanked #t)))

(define-public crate-boolvec-0.2 (crate (name "boolvec") (vers "0.2.2") (hash "1vxxz40w5v1qsrghwrva3x38m74jblv55bfc45llwyks70a68fl2") (yanked #t)))

(define-public crate-boolvec-0.2 (crate (name "boolvec") (vers "0.2.4") (hash "1v83049pklqymwfvhdk7kf4vpgcxd50h4glyp7gbbbh9d08fygyh")))

(define-public crate-boolvec-0.2 (crate (name "boolvec") (vers "0.2.5") (hash "01wziwpxiyln4apjkjd0zagav5q2w9nyhhyrchsxn2l28mfy578x")))

(define-public crate-boolvec-0.2 (crate (name "boolvec") (vers "0.2.6") (hash "002csj0x6mvfhz6rw04b4gzfiz03d2jvmqx9qnc4sx8ycgjs6my0")))

