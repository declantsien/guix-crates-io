(define-module (crates-io ii fe) #:use-module (crates-io))

(define-public crate-iife-1 (crate (name "iife") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 2)))) (hash "1gw2vw74565kfzi8ybmql6z5gj333nz33k0vf4fjzwbhwg3n4kvk")))

