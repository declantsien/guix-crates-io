(define-module (crates-io av dl) #:use-module (crates-io))

(define-public crate-avdl-serde-code-generator-0.1 (crate (name "avdl-serde-code-generator") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "03cgah7shp6fzikkr5afks6xhs300raq8s0sliysmjghqxdlzkzj")))

(define-public crate-avdl-serde-code-generator-0.1 (crate (name "avdl-serde-code-generator") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0xk6nx0p19w3kglbdmnqj46yq8gz3ybx5nx5vlb9sp42a02lpm2j")))

(define-public crate-avdl-serde-code-generator-0.2 (crate (name "avdl-serde-code-generator") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1cgva3yzw402r6f3ms64b04liw3rdp7jhqkr8azyjb89m23cr0sz")))

