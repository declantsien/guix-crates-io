(define-module (crates-io av ls) #:use-module (crates-io))

(define-public crate-avlsort-0.1 (crate (name "avlsort") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1bg3mjfavjr2ay1z72kk5ncrdwq9xx8dmfq98vkfvnw03injjabp")))

(define-public crate-avlsort-0.1 (crate (name "avlsort") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1zaw9yjvji4h7jl0bwyjh3sflqhji2g6fij9dnl17958b5nmkf4d")))

(define-public crate-avlsort-0.1 (crate (name "avlsort") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0n2lzsxgw875zk6isppyl1z9hhfx5dhhsnlshwrcirbwx67d3s1d")))

