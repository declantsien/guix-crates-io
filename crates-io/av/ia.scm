(define-module (crates-io av ia) #:use-module (crates-io))

(define-public crate-avian-0.0.0 (crate (name "avian") (vers "0.0.0") (hash "1wps6ypkwaa4baix82026smdbz8gqmlb7q8p4k0hprd893sm2zcz")))

(define-public crate-avian2d-0.0.0 (crate (name "avian2d") (vers "0.0.0") (hash "1q43sq2zjdv2p0xqvjvnxnidsswx1kdkbvx8ir1970v87l6a4n8x")))

(define-public crate-avian3d-0.0.0 (crate (name "avian3d") (vers "0.0.0") (hash "0qf3s199sz8d57fy70gy41a46095955m89bfqzfs3hpc4pz33m98")))

(define-public crate-aviary-0.1 (crate (name "aviary") (vers "0.1.0") (hash "1p1qnzaiyqj95ns1y4wadkmvvrkd7hb5jc4l4r2b1xd3yvh39hn7")))

(define-public crate-aviary-0.1 (crate (name "aviary") (vers "0.1.1") (hash "17rzx2g8x749cr7k8hjblcz6ppy3prg99mzj729p0jhr3ic80b12")))

(define-public crate-aviation-calculator-0.1 (crate (name "aviation-calculator") (vers "0.1.0") (hash "0mcfrnlr4n0hdi8ck0pysifw4vk5b669bh6k88002na636jy1mz9")))

(define-public crate-aviation-calculator-0.1 (crate (name "aviation-calculator") (vers "0.1.1") (deps (list (crate-dep (name "enterpolation") (req "^0.2") (features (quote ("std"))) (kind 0)))) (hash "0bh0qivr2y3v3j5fnn2vlfischqmxg97p96yrq3ja9nx2qivdz1g")))

(define-public crate-aviation-calculator-0.2 (crate (name "aviation-calculator") (vers "0.2.0") (deps (list (crate-dep (name "enterpolation") (req "^0.2") (features (quote ("std"))) (kind 0)) (crate-dep (name "snafu") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1l5n1qfax3mn4j053yb8dr4xg56hkxjbfsgjpzs1zyrjdkvl5viz")))

(define-public crate-aviation-calculator-0.2 (crate (name "aviation-calculator") (vers "0.2.1") (deps (list (crate-dep (name "enterpolation") (req "^0.2") (features (quote ("std"))) (kind 0)) (crate-dep (name "snafu") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0ym99llnw9dysvnw6zd8fga9q6cx3qy3bjknqj06y7rrr8whifl5")))

(define-public crate-aviation-calculator-0.2 (crate (name "aviation-calculator") (vers "0.2.2") (deps (list (crate-dep (name "enterpolation") (req "^0.2") (features (quote ("std"))) (kind 0)) (crate-dep (name "snafu") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1xcf63v60kavsgjg6cqmzf5vgsy93ddm6cy4jx53kr19vqbq5fcx")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "067qn75hixc1fxv4kf8w59hv9dimh9a7hg7a4sqvxr2y640091xi")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "0697daj63fhj54wygfn15ppmjhk28p8j1yway989fb22dllnr85i")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "118xvaa12xv9c02aw24pd6fxgxy2pcpagc4cqh6n6j9ba3q1chm5")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "09936lgdmkj1m7rvgzdmpqfiyvq580sd9a3nhr0i5ggbz90db845")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "05zb12flqsqxwgv4lkvgyzsnbh0ml8jxd389sr238fagz5fryf9k")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1rfs0qr1vj9hgwwvrsnizpl6fnprqyndqc0wlsd6zn7h6bcl9sjp")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "0hymfvc5drrcwqmzjpjnz7fmb7dhwzm7dxfj8yspapphxzl9kzgv")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1gc46wcxzkwlapps8cmx3d8dqwfqn4r3ql3kwrpyiyc5s0pyw4h0")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1wsjib8mrp7fd0whs823jg0wpabnjg8q7a83ywgp5jyr67jrc922")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "03wiwqsrxjszbrqf5azgpmzqv9fy2ix9b1wb3jzzccmxf8271ydi")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1ab0w4kmi0ccynd1y80mpvvd93csmdn8yifvxr918gj4ab84n31b")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1cpf8511bzndknrkjbaz8m8vkfficwdc6rs8ydy6s92cxkna8rlm")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.7.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1vylp4ha0f8hf11zi0f52r09002i7l2vdn2rpl3njixcmk569x5r")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.7.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1mhgydk86iy6cg4i454ki9v2jch6q1qxc7nm6flm1cxbcq4dccwp")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.8.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "0bjg0iysa84v32jsy1ihrbdxg59wvpkxplvfznmblffnk25w53is")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.8.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "0wzm8r856awlg8pkljxqkflxwjhqv3p0b0i4v9nw7v0hgy51xirm")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.9.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1zfbhl26knkqms935nbgx5rnxy5733xb453sldxajfcgnh48156q")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.10.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "01mgj41dpr08gxyya462ia3n1k9ygiqz50rgidi6f4h6x3psb9fk")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.11.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "09bg0br31z6lddp9qpya0zqq11rjp7z20vvfmpm3maiy0fsng4ci")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.11.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1qkai5679is645gr1m2h5kwmb9nn056g98mx6c55zagdfc1fz8n5") (yanked #t)))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.11.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1dyfipg967j875s0hq0w825spyipf1whin7j1vshnhpg7l2zq5wl") (yanked #t)))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.11.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "1k1wp8qzlsswa54026rdqmrc3mk6ihhi1xawpssyw2g7rvg8jmc6") (yanked #t)))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.11.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "0g0xq10x8pif4i2hnfk7vpnja944skcmzrin2lwlbm9v5g8qgsm1") (yanked #t)))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.11.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "0vlfy367b8sk0r12kbadn5qhghwc5jb2926ykg0hwjj5bghjaksw")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.12.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "14zlwxkn40s8snfcpcxx9g623fsb6nv5rxci5mk2dz65w3qcn0z5")))

(define-public crate-aviation_calc_util-2 (crate (name "aviation_calc_util") (vers "2.12.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "grib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.1") (default-features #t) (kind 0)))) (hash "0qr9cvc6vmwlq3p3sfzlm7iyvzggp6kr6cbdfhbdka420q95hjbp")))

