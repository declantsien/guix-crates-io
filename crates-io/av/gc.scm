(define-module (crates-io av gc) #:use-module (crates-io))

(define-public crate-avgcol-0.2 (crate (name "avgcol") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1w4sx9hmcbzqv5lzd1q1zhb4i48kfclrf1ikcy57j0392395arq7") (features (quote (("remote_image" "reqwest"))))))

