(define-module (crates-io av l_) #:use-module (crates-io))

(define-public crate-avl_tree-0.2 (crate (name "avl_tree") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0d3vy90j01nph5kf6l73yxkm78b4kwhskfpk9cv911h8jzfh0503")))

