(define-module (crates-io av fo) #:use-module (crates-io))

(define-public crate-avformat-sys-0.1 (crate (name "avformat-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "0hrllayxmmf4a84hxz9a9mksw9jfim3zv91p5nhqgjnbaqh93hm6") (features (quote (("build_sources"))))))

(define-public crate-avformat-sys-0.1 (crate (name "avformat-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "021znz2qaq569jy22mmfjk3mnk2gvdvq1hqx3ngqa0hlv3yq2wnk") (features (quote (("build_sources"))))))

(define-public crate-avfoundation-0.1 (crate (name "avfoundation") (vers "0.1.0") (hash "1hmi21abwkr5qljriy83zc07kg8frmca4k1xallrx9clha4psxa7")))

(define-public crate-avfoundation-0.1 (crate (name "avfoundation") (vers "0.1.1") (hash "05yxgqyqfjxbmwi1hlh9s0z59pyvh386v5w7ch2hirklmrf5py3g")))

(define-public crate-avfoundation-0.1 (crate (name "avfoundation") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "block") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "cocoa-foundation") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.4") (features (quote ("objc_exception"))) (default-features #t) (kind 0)))) (hash "198zzdha255mh9d1m0kihhy0yy3cybfp43nlgnbncknwzracvqwa")))

(define-public crate-avfoundation-0.1 (crate (name "avfoundation") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "block") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "cocoa-foundation") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.4") (features (quote ("objc_exception"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0xhgn993qafm91lg6dc55y8mccfw1gad6lgdpiwsi085v820lfyz")))

