(define-module (crates-io av ap) #:use-module (crates-io))

(define-public crate-avapi-0.1 (crate (name "avapi") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "18vpz791bm185zmnp5bgq8cdxbg0l7vi12mllkklkx9nz9ic6k04") (yanked #t)))

