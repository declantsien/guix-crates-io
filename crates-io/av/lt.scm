(define-module (crates-io av lt) #:use-module (crates-io))

(define-public crate-avltree-0.1 (crate (name "avltree") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "154f09x5ar561imq2c6clizlhynn6ykildms6ifzq8lafzy64rcg") (features (quote (("default"))))))

(define-public crate-avltriee-0.5 (crate (name "avltriee") (vers "0.5.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "13h9jvyc4ywm7g0l52pma6i1pkw6sxs8b10krnyzbn4p165p4b0w") (yanked #t)))

(define-public crate-avltriee-0.6 (crate (name "avltriee") (vers "0.6.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0hk1cpdx7cp4didgkl96qrsiard19kbfd0y7dkgivvsfqvgbvam6") (yanked #t)))

(define-public crate-avltriee-0.6 (crate (name "avltriee") (vers "0.6.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "08y0959crvbl3aizzxlyf6dr8v95h0jppry8hnjxvrfdl5xml6nw") (yanked #t)))

(define-public crate-avltriee-0.6 (crate (name "avltriee") (vers "0.6.2") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0cyck3fahs0w13xp3zkk49v040mpzg7cviavgmriwrfcas5zwzgc") (yanked #t)))

(define-public crate-avltriee-0.6 (crate (name "avltriee") (vers "0.6.3") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1f6p897czqaz9jzlj0rhhwylxl68kaqwpj9l8mp6l4jm5h55lqd7") (yanked #t)))

(define-public crate-avltriee-0.7 (crate (name "avltriee") (vers "0.7.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1cyhpgj99q5jaqrb9ki83fpsy47765d3c6r1rjjz1y00i64qwczv") (yanked #t)))

(define-public crate-avltriee-0.7 (crate (name "avltriee") (vers "0.7.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0xzmz4qyyv4zx479ayn2rrpai1a14iar86a4r3k56wqqcp6pm0kj") (yanked #t)))

(define-public crate-avltriee-0.7 (crate (name "avltriee") (vers "0.7.2") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1vpk1bbkay3mxxjixrzmb3n2pqzvsmj7qr5lcb88wmfng3zqv48q") (yanked #t)))

(define-public crate-avltriee-0.7 (crate (name "avltriee") (vers "0.7.3") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1v4zzfgx4sh5lz59fxrp7rdm9ycr286k43783y509lhdqgphj1jw") (yanked #t)))

(define-public crate-avltriee-0.8 (crate (name "avltriee") (vers "0.8.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1gjljx6r32nn9s0srz97lh2y4xr81p2606haz7gw6f0hmigd97m8") (yanked #t)))

(define-public crate-avltriee-0.8 (crate (name "avltriee") (vers "0.8.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1fmylmi5y5qm1syb6nwbx2w3w0164ra8si2zb04jfgdbj2fqmmj5") (yanked #t)))

(define-public crate-avltriee-0.9 (crate (name "avltriee") (vers "0.9.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "00mrmm1xkbvlvawmxv0vmbk8ij634gsgiqg2vrf2v9rc3bk679zk") (yanked #t)))

(define-public crate-avltriee-0.10 (crate (name "avltriee") (vers "0.10.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "03a9qp473hbc8g8gr8znn3yg02af18cj5f1jq1n0d9lwszqs3fwj") (yanked #t)))

(define-public crate-avltriee-0.10 (crate (name "avltriee") (vers "0.10.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0x12i9xarzgwiiy48zgarb8636xwbxadrjnk1cdmc4d6c55bdg02") (yanked #t)))

(define-public crate-avltriee-0.11 (crate (name "avltriee") (vers "0.11.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0qcj1lpilj6gz34a8qybdhzwkqsh97nwwnb5bgc4jipbkbavy72d") (yanked #t)))

(define-public crate-avltriee-0.11 (crate (name "avltriee") (vers "0.11.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1rcxv1r7zv5sz7a56prvadscxbq2qx77yvq283jcnw2fh9jj6nwa") (yanked #t)))

(define-public crate-avltriee-0.11 (crate (name "avltriee") (vers "0.11.2") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1ypg6s3zq7amdw71y943c1x60s5shx8slf3mn12hclpq2v8vylb1") (yanked #t)))

(define-public crate-avltriee-0.11 (crate (name "avltriee") (vers "0.11.3") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1dgly0fd1ng4sp3jrklb6amk8kdds2mg0cf0ncwgk1fiv61vxd6k") (yanked #t)))

(define-public crate-avltriee-0.11 (crate (name "avltriee") (vers "0.11.4") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0himrsgwnl288hhs6w6db4kvd0k49702j38ilm66l3xaip6pljkg") (yanked #t)))

(define-public crate-avltriee-0.12 (crate (name "avltriee") (vers "0.12.0") (hash "013315sj8ql71wyb1535q8kaqilygf4r2z9b22sgbhn4278zq4wl") (yanked #t)))

(define-public crate-avltriee-0.13 (crate (name "avltriee") (vers "0.13.0") (hash "0vxim2l560d5jh66disr06rmh29qhwmfdgb4b5167ibij3lam1zc") (yanked #t)))

(define-public crate-avltriee-0.13 (crate (name "avltriee") (vers "0.13.1") (hash "0kb776p2zni4ixszz30lh7bxzw1j37c4wqxxxc2ngnm3wgv2m1yc") (yanked #t)))

(define-public crate-avltriee-0.14 (crate (name "avltriee") (vers "0.14.0") (hash "1lgcxrhcgy4xyh0p9zhhxv4d29ywi1n17f2hvr4zvddciflidjmr") (yanked #t)))

(define-public crate-avltriee-0.14 (crate (name "avltriee") (vers "0.14.1") (hash "048x80rcfbw68z2p4i2fqqggkyma0g09gdiabsz6hw4a81qv8x94") (yanked #t)))

(define-public crate-avltriee-0.14 (crate (name "avltriee") (vers "0.14.2") (hash "0kdy7arwlj06k2kyp8sskhpqgd5w5dcwbcg1va6dlsw7bi06bav7") (yanked #t)))

(define-public crate-avltriee-0.15 (crate (name "avltriee") (vers "0.15.0") (hash "04sy9k2sl46vzkhqbhzvynbxrb4x2l4qpcq1p4jwm4ni9xiniswm") (yanked #t)))

(define-public crate-avltriee-0.16 (crate (name "avltriee") (vers "0.16.0") (hash "0pv9cnqy5rzzjayvlhv5af6lhgk0lw08afy7rzirbsqzqjy0wklm") (yanked #t)))

(define-public crate-avltriee-0.17 (crate (name "avltriee") (vers "0.17.0") (hash "0q15fbyh4v9pg67r25wp54dcksf0j2l647pxmsv1zjpjazs3a5ga") (yanked #t)))

(define-public crate-avltriee-0.18 (crate (name "avltriee") (vers "0.18.0") (hash "07w2miqgryha9jwqpy1r61qnk0imvbhfh7lzk6c2y3fjlh8y8m5z") (yanked #t)))

(define-public crate-avltriee-0.18 (crate (name "avltriee") (vers "0.18.1") (hash "0a8mffwdnpxn1lra3j9k6jckyv2c90sr23fkbrygchd778bakwdh") (yanked #t)))

(define-public crate-avltriee-0.18 (crate (name "avltriee") (vers "0.18.2") (hash "14brssb47jws60swmp721rk57pia8g7g3djk4ni6ffc0p946cchz") (yanked #t)))

(define-public crate-avltriee-0.19 (crate (name "avltriee") (vers "0.19.0") (hash "030fmyb1shnggfzai013bjvy0yiamgnc6cx9ckv7zk2xnjqn35zy") (yanked #t)))

(define-public crate-avltriee-0.20 (crate (name "avltriee") (vers "0.20.0") (hash "19kg183flh97sgw6qxk7a2y6fjnzc2bwq2sb1nnv30fmbs7x6wmw") (yanked #t)))

(define-public crate-avltriee-0.21 (crate (name "avltriee") (vers "0.21.0") (hash "1vf2hhlfyw7llq8223pdh27yzgfg277b686zl6wb9hjayg9cjnc9") (yanked #t)))

(define-public crate-avltriee-0.22 (crate (name "avltriee") (vers "0.22.0") (hash "0adaxbh0zrni733ywv6y6xii5jf2q8w2p8w0z72sbkinzrn9lvg4") (yanked #t)))

(define-public crate-avltriee-0.23 (crate (name "avltriee") (vers "0.23.0") (hash "01bsw7711q0qkijzizbf4ylznmpq0g2ij0x52mjlb90sq8wkxa9z") (yanked #t)))

(define-public crate-avltriee-0.24 (crate (name "avltriee") (vers "0.24.0") (hash "1p4c9y9cv8y81biiwpkzcm2ivn1sfl4pb27dq5y42rhadixg6516") (yanked #t)))

(define-public crate-avltriee-0.25 (crate (name "avltriee") (vers "0.25.0") (hash "01xb8gbljq7bs2f3s0148csd7c6vky07307hkhb0r8m4l3drghh5") (yanked #t)))

(define-public crate-avltriee-0.26 (crate (name "avltriee") (vers "0.26.0") (hash "08qa477w0xy6l3214ny03pc60psiy67alnaxfr8dkgmp0pvgb14m") (yanked #t)))

(define-public crate-avltriee-0.27 (crate (name "avltriee") (vers "0.27.0") (hash "0jysglr2fm2spz93c6gg04j668il1g51dc1j5skjcf30n5ar22x9") (yanked #t)))

(define-public crate-avltriee-0.27 (crate (name "avltriee") (vers "0.27.1") (hash "0q7gvq35fda5bwbxva4j6hi95vdnqi754dadyf3wlm61bwnkrr5n") (yanked #t)))

(define-public crate-avltriee-0.28 (crate (name "avltriee") (vers "0.28.0") (hash "12mzszqbdwmiihl7i5n53snyxk6ahav0czl2pybq8v0mv61a2w2q") (yanked #t)))

(define-public crate-avltriee-0.29 (crate (name "avltriee") (vers "0.29.0") (hash "0y0xmmhzp7w6fh2fv4bpjbnynvl3r4c3ajs5ikyw5jim5pgbcxpk") (yanked #t)))

(define-public crate-avltriee-0.29 (crate (name "avltriee") (vers "0.29.1") (hash "0xinf8amrv4sm480f41wz74g8fld601zmvq0zrw881pdbjyr7298") (yanked #t)))

(define-public crate-avltriee-0.30 (crate (name "avltriee") (vers "0.30.0") (hash "0cg22d7bf4dpp0qrc2k3lgn56ka3xrd6q15jrh0sb4if2dyplpdl") (yanked #t)))

(define-public crate-avltriee-0.30 (crate (name "avltriee") (vers "0.30.1") (hash "00j5rvx7bbhcvkpwki5kq8mcm6hi68jhplhy970xp6w9clnhzxia") (yanked #t)))

(define-public crate-avltriee-0.30 (crate (name "avltriee") (vers "0.30.2") (hash "1xmyv5rliz7s66g8pqsv21jc86a6cgyhfydpgz3dwqpi9igqaq7c") (yanked #t)))

(define-public crate-avltriee-0.30 (crate (name "avltriee") (vers "0.30.3") (hash "09xb52c4kcxn84mpaqir8gchd79n62gf5gpw34d5x11kc7dlgqnn") (yanked #t)))

(define-public crate-avltriee-0.30 (crate (name "avltriee") (vers "0.30.4") (hash "1asl3mrqvn64kg6r41376pa4bv77gkw2zf5b0bpppvcns86hnrrw") (yanked #t)))

(define-public crate-avltriee-0.30 (crate (name "avltriee") (vers "0.30.5") (hash "0vx6525lqbvm4nl541dll4w4kbicqy8x3y97k0rwkj7qc3gcaaq0") (yanked #t)))

(define-public crate-avltriee-0.31 (crate (name "avltriee") (vers "0.31.0") (hash "1v9rpics7wxq4c2b6imdqqpp64k38sfi0n9zzbib7akbkipfslkj") (yanked #t)))

(define-public crate-avltriee-0.31 (crate (name "avltriee") (vers "0.31.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "14b1w1kr2l081vjfm67q9jpna3a8x6zab1nwy5nnjy42qxnq5334") (yanked #t)))

(define-public crate-avltriee-0.32 (crate (name "avltriee") (vers "0.32.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "18211g7impb1xn802zcnxj9xl3ina87n2lch4gbw66yqdv4zzaaa") (yanked #t)))

(define-public crate-avltriee-0.33 (crate (name "avltriee") (vers "0.33.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "19rmr23yszyx4ngx8kax4fqdp7q55fx6f70nfz3rjnqxnq6jdvqa") (yanked #t)))

(define-public crate-avltriee-0.33 (crate (name "avltriee") (vers "0.33.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0j6i76n9zxh4n5z9x99j0whw2cmgqkz2kahqfvw1g6jbpf708529") (yanked #t)))

(define-public crate-avltriee-0.34 (crate (name "avltriee") (vers "0.34.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1z0bj3fccxm8wm0zn6zgcvqcmmcdx9l940jshrcx6pcdqgm8c5cd") (yanked #t)))

(define-public crate-avltriee-0.35 (crate (name "avltriee") (vers "0.35.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "174v6wi8nrv8906ghc729s8m9hslik4k6g7wsda5y8hd4812k4vx") (yanked #t)))

(define-public crate-avltriee-0.36 (crate (name "avltriee") (vers "0.36.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "075kwb5wj03yhr1sqwsavb0ljgi1ms1b59qcswbw7wqw7sm64ggn") (yanked #t)))

(define-public crate-avltriee-0.37 (crate (name "avltriee") (vers "0.37.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0knlp20qxf3p3i2dwwzr63wk5hvfn3ds9a5kk71cm518ipcwpc3c") (yanked #t)))

(define-public crate-avltriee-0.37 (crate (name "avltriee") (vers "0.37.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "09vbx0mqfp9vh92j1k05gjdvjhyj28krbhxns1l10886396akbzf") (yanked #t)))

(define-public crate-avltriee-0.38 (crate (name "avltriee") (vers "0.38.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1r3wf2pj88bsz9h9yvb3kn82w733d9n731z8zpcw1930khh989r8") (yanked #t)))

(define-public crate-avltriee-0.38 (crate (name "avltriee") (vers "0.38.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0i853lzkk3jyz8248jfcxdwvdwc8cm9880as4cgzvvxasi3k22zk") (yanked #t)))

(define-public crate-avltriee-0.38 (crate (name "avltriee") (vers "0.38.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0bsfkg0yyygsi1yni25ag6w26pfrg8chars982kvp8i0n3bcnnn7") (yanked #t)))

(define-public crate-avltriee-0.39 (crate (name "avltriee") (vers "0.39.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0va56fvjf513dfbmv0c9mlirb1yg6fx2sps98kiwsj3xfq4xjp23") (yanked #t)))

(define-public crate-avltriee-0.40 (crate (name "avltriee") (vers "0.40.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "10w2zs7br327m914bipp6fwb97wnfb367b8i5smqdnq7y1mmrxa6") (yanked #t)))

(define-public crate-avltriee-0.41 (crate (name "avltriee") (vers "0.41.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0aqjrx52hnxkl64339b5vaaj2kh5qwgxwzdl5vm639rilaibz84z") (yanked #t)))

(define-public crate-avltriee-0.42 (crate (name "avltriee") (vers "0.42.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0nkrazbqxqjc5ywxvhic25kv6nrkyjmihy45smp0223l9ggpq8p5") (yanked #t)))

(define-public crate-avltriee-0.43 (crate (name "avltriee") (vers "0.43.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1bf1l21r61r13iwadwm0mj2h4xpdcbv31ymzj0qg9fz1m7mnxdl6") (yanked #t)))

(define-public crate-avltriee-0.43 (crate (name "avltriee") (vers "0.43.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0mn4xj6d6q9w3jkp3l7nq711x5gyg84ar1wlipb5khbvgj0xbli8") (yanked #t)))

(define-public crate-avltriee-0.44 (crate (name "avltriee") (vers "0.44.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "19d67qvwld481qys7ywb38chd9ssk8fch4ivya7ywdlgcbrc9wd9") (yanked #t)))

(define-public crate-avltriee-0.45 (crate (name "avltriee") (vers "0.45.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0wh8ipv20p93bb1xkd2dcbi3a3gq9x80cw5s7c83dxshfyd2bxm6") (yanked #t)))

(define-public crate-avltriee-0.45 (crate (name "avltriee") (vers "0.45.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1jim50qia20pxbjcj16ll8g4flib31z168fq9334inxk9w88gci5") (yanked #t)))

(define-public crate-avltriee-0.46 (crate (name "avltriee") (vers "0.46.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1ivsnifh8lp01lllxmk9fhzdrpjvvl5l7nh5izmm6p3d4934ir6j") (yanked #t)))

(define-public crate-avltriee-0.47 (crate (name "avltriee") (vers "0.47.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "17cnsf9yyypdid99vpwc5qza96b61cwv56p1mpksh3clyh4z4id4") (yanked #t)))

(define-public crate-avltriee-0.47 (crate (name "avltriee") (vers "0.47.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1qy7s5y2hfp145rwkpb22607dnl8n7znx2qshppkq79rmw2w9iph") (yanked #t)))

(define-public crate-avltriee-0.48 (crate (name "avltriee") (vers "0.48.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "09cjqgyxi3jvkz8wqfmlm9agrxaxihzl7lr04ahqvi6jw8zy74sg") (yanked #t)))

(define-public crate-avltriee-0.49 (crate (name "avltriee") (vers "0.49.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1bmh8yfy064d6qjzcx91fi3jk2ihwrpvap5p7mqc5rhx44ljri50")))

(define-public crate-avltriee-0.49 (crate (name "avltriee") (vers "0.49.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0bwddsdzc6scqig2si7v51dgsahvrmpk0m0rd0ligq3575aa00zn")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "00hzpajq763829w1yfnlqlb5a8qz78cgflgllghpq7yhsdd6rkm3")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1bk2r1z7gd3p45x0sy4hhczghg9fsql2xn0hgadkxs3i9xbpbm8q")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "196khyd4ri0a0iy5xxzvnrhw3p364bv6vdvxsjahqhm5lcwrf95n")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "18bf5hkph1l9sgwyplhiapjs0gab79x7n8q996i17hjc6bv2nq8c")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.4") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0ci0648897366ymgsp0y995k109r96hdgqbqk66xs7jyp6dkwbn9")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.5") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "002jx5ria1mp30yyfb3pvcg5fxm8qq5w1b61d4vap0srs1333qhg")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.6") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1x4syp03xj5vjvk293kcgig3nq10al56dxp4rdzyx14d4vaxlpjs")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.7") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1j06v4vxph9vlsyw43mjmhrz465prmkmcyfaysn901jqdkal1yi5")))

(define-public crate-avltriee-0.50 (crate (name "avltriee") (vers "0.50.8") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1w04hvlcpms9xy49mar12vdw0iiswqyai3837hz7sr3pdq9zqnaa")))

(define-public crate-avltriee-0.51 (crate (name "avltriee") (vers "0.51.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0hyd86i2fwddpkfm1vdiv7gwr2l4mii4xfy2iiwzw7kdbsagz3kx")))

(define-public crate-avltriee-0.51 (crate (name "avltriee") (vers "0.51.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1ki5nv1sb4mvyph6psgs2apk7fkigp7mq6mc1r0899rjvhvkgb5c")))

(define-public crate-avltriee-0.51 (crate (name "avltriee") (vers "0.51.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0aamil6b96bavr8qs77pyk0hq5gc8021lxb1af4wdrhnjdx3vz22")))

(define-public crate-avltriee-0.52 (crate (name "avltriee") (vers "0.52.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0r5lh1vg7ljmiqkswjcc4v23sqsxs2iw3nvfsnxy4631m2xqy078")))

(define-public crate-avltriee-0.53 (crate (name "avltriee") (vers "0.53.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1y3f2s3c3a8vac2984j79hy3saf1pikz7102b13j4czw93xblwkm")))

(define-public crate-avltriee-0.54 (crate (name "avltriee") (vers "0.54.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1jq0pd9ipwzn995jscbwmgvxfpdikxhvgwlqlk72f34sby0kbja6")))

(define-public crate-avltriee-0.55 (crate (name "avltriee") (vers "0.55.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1gbvrdr1wng24gzbq5z0628z4h5hk1vsx0xdw2c8nkc7rhl5hvgj")))

(define-public crate-avltriee-0.56 (crate (name "avltriee") (vers "0.56.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "105yb851v06q15gs6q0yxc0pjs0jkjh76lvhp1agij9lhh4ccaqz")))

(define-public crate-avltriee-0.56 (crate (name "avltriee") (vers "0.56.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "194xdzg647byr115v8nqrhahvadirpfrvnacr8h1fqj2fayi1z22")))

(define-public crate-avltriee-0.57 (crate (name "avltriee") (vers "0.57.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "190s0pihl6cxdqifj11a3dmgjdzlfb2s03rjqxy0zf79c3acw69d")))

(define-public crate-avltriee-0.57 (crate (name "avltriee") (vers "0.57.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "08j5xsvzdj2lcpsdl0k4ak5502885b691hhasdvw5qw5sbh5p9vc")))

(define-public crate-avltriee-0.57 (crate (name "avltriee") (vers "0.57.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "06rx2b8vzr7ddmikna76cwlbl6jgljpwals44r7hp4nqckpy3xmf")))

(define-public crate-avltriee-0.58 (crate (name "avltriee") (vers "0.58.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1q8qmsaz6w42w9lwa8iwaljb5am97x8hyfx9s05x8n9df0cqyfya")))

(define-public crate-avltriee-0.59 (crate (name "avltriee") (vers "0.59.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1ivjghhag1qs06v7vwq7d5fqg7f8dc765zszymh7x850cw5vv6c1")))

(define-public crate-avltriee-0.60 (crate (name "avltriee") (vers "0.60.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "14dlfqwm0zcgldsa9qcbf1y7mvsj7937i7wz6hbavkn7f9rxwxh0")))

(define-public crate-avltriee-0.61 (crate (name "avltriee") (vers "0.61.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0q4w1fy4y0qm1x0bkyp58jd9iv5mdvrnljh1c0glv29b6dy241jy")))

(define-public crate-avltriee-0.62 (crate (name "avltriee") (vers "0.62.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "18kl1mgh71dpzzvyvv76p14zrddv7f6s0r4r36widk26h5l0c8k3")))

(define-public crate-avltriee-0.63 (crate (name "avltriee") (vers "0.63.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1gvp34imzx6f7czg0l3krb2m1cyg01myirn49v1fk43gimgkb9ly")))

(define-public crate-avltriee-0.63 (crate (name "avltriee") (vers "0.63.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0vf24i5cgvcxmk06yz9z3ynh7x7046z1wwy51c71w3i8yfa13k15")))

(define-public crate-avltriee-0.63 (crate (name "avltriee") (vers "0.63.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1mq6grngzg0kwyn79xy1n4dbi98jvmnzjnrpvf7la42kh32dxxzp")))

(define-public crate-avltriee-0.63 (crate (name "avltriee") (vers "0.63.3") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "06g5wp88gcw6048is53sknqm10dnbpxn1l1z1m1f55rna6p6jscy")))

(define-public crate-avltriee-0.64 (crate (name "avltriee") (vers "0.64.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0fifrs18rp365gl0x23xglw6rsa9va2g2xpdk8q1k0wbqn8r75mn")))

(define-public crate-avltriee-0.64 (crate (name "avltriee") (vers "0.64.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0j1c7zbmmfk81w13gqclcjdy3kyc9b3sg8j4wnq33kvi8b3dqy3k")))

(define-public crate-avltriee-0.65 (crate (name "avltriee") (vers "0.65.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0vrhsnkag5za2inpi08f53znsim1xrx8rfcrpr5vavqxnyyqrkx7")))

(define-public crate-avltriee-0.66 (crate (name "avltriee") (vers "0.66.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0dk9p5i1y4ahxl4spnvmkagb12ahaqdpmic117lkkms5636668h4")))

(define-public crate-avltriee-0.66 (crate (name "avltriee") (vers "0.66.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0xsdyjs18p65fi21xylr316br6ql09ak394qir757kz5hl6xwc2x")))

(define-public crate-avltriee-0.66 (crate (name "avltriee") (vers "0.66.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1vma6da7wnx5psw75inc414c9ab9s02gdrqmkaq4wxd8k6i574jz")))

(define-public crate-avltriee-0.67 (crate (name "avltriee") (vers "0.67.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0klc9k1w0vp65invrsp5chdc2sb334zcfqp3d9qg7zsir47g31jb")))

(define-public crate-avltriee-0.68 (crate (name "avltriee") (vers "0.68.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1yqiki88hg4jzklqz5kzn0jr9wl8a4qp24kxzbbihrp9vn7vy0gw")))

(define-public crate-avltriee-0.68 (crate (name "avltriee") (vers "0.68.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0g4lnq0qb6mda1403zbz1vipqcdkxqpjc4kxqzh73wq4w8nihq04")))

(define-public crate-avltriee-0.69 (crate (name "avltriee") (vers "0.69.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1gfazx7lxfr1nzcqgdr5nzy7n5i5yg9c0y3kr6r3c6kl5zxdbkd6")))

(define-public crate-avltriee-0.70 (crate (name "avltriee") (vers "0.70.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1r9r3jbx4x11vri8sp6jlcqyvb46c7g8ay7rrg3dmmvsbgy1sp3i")))

(define-public crate-avltriee-0.71 (crate (name "avltriee") (vers "0.71.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1sf3mps54fvxjpbnxh8i8qr7b1d6hrz5mc30ikgfy7ci215g7bik")))

(define-public crate-avltriee-0.72 (crate (name "avltriee") (vers "0.72.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0mqdazv2hmfyyfnlgc2pdfjg66n2s66qyfvy9r327ysnzhvkp88m")))

(define-public crate-avltriee-0.73 (crate (name "avltriee") (vers "0.73.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "06ax4hrgbizx8z5irx5nrqr4pswfiz10cvrz7p1za5k5bwgw429n")))

(define-public crate-avltriee-0.74 (crate (name "avltriee") (vers "0.74.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1858d7qf72vdpyz9h5m735pg7bjmq2xrj5pxr1rxpa2icfbfkbdp")))

(define-public crate-avltriee-0.75 (crate (name "avltriee") (vers "0.75.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1bz3knkz4cl8mhz3ky2wxf50kdlynrplwj03gfbla4gjhhw3xppq")))

(define-public crate-avltriee-0.76 (crate (name "avltriee") (vers "0.76.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "09ka39536ydrznm5fkz5qd2kq3h6gfcy55d83rdafl86nqh76333")))

(define-public crate-avltriee-0.76 (crate (name "avltriee") (vers "0.76.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0484gnsyy0jp89i209dj1kwyi2ahhi713kb7jvj9zy1l4kcvxy43")))

(define-public crate-avltriee-0.77 (crate (name "avltriee") (vers "0.77.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "08yz97b5nzpv5vm2pr7gj2d6n3bk6kngpkxxgh2xzwjix1ir7s8a")))

(define-public crate-avltriee-0.77 (crate (name "avltriee") (vers "0.77.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "00bv6sdlq2h4yd4sixaaam8fchdgclfb28rlyhd95nah5p8525vb")))

(define-public crate-avltriee-0.77 (crate (name "avltriee") (vers "0.77.2") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1yqdwwm2gc6c6292ng5pvp4zh1nyvw2abqhrq73h59kd0407y931")))

