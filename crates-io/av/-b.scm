(define-module (crates-io av -b) #:use-module (crates-io))

(define-public crate-av-bitstream-0.1 (crate (name "av-bitstream") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "15v8rz28asvmwfkb86zvh7sba4njjylq694rw6kp8scfmdr2wh0b")))

(define-public crate-av-bitstream-0.1 (crate (name "av-bitstream") (vers "0.1.1") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "err-derive") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1m91inp84n3qp8dn8yakgi7i9xpr45w1yn1rzkpm0d78b3dcx2kf")))

(define-public crate-av-bitstream-0.1 (crate (name "av-bitstream") (vers "0.1.2") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "err-derive") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 2)))) (hash "03z3a4x7dpbrwq9hdmw61vi8053b3d5gc6wi3l81c6zp2fsnxmib")))

(define-public crate-av-bitstream-0.2 (crate (name "av-bitstream") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v4rsmixkyq7g6p5sk5wrryq9vsv972dvg5wzn45c7mjim50zz7d")))

