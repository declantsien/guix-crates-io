(define-module (crates-io av l-) #:use-module (crates-io))

(define-public crate-avl-cont-0.1 (crate (name "avl-cont") (vers "0.1.0") (hash "01rn5flw6ir349msr9i5ayw8g774245hnr4mwinfn12w07yjm6lf")))

(define-public crate-avl-cont-0.1 (crate (name "avl-cont") (vers "0.1.1") (hash "1rjirq2s23d5hqk082j36flxpwi8n6g9aw96pxns2bhzx8n2i9v0")))

(define-public crate-avl-cont-0.1 (crate (name "avl-cont") (vers "0.1.2") (hash "14pav3m1j3qjkhdva3dq52zmbxv6zkr6vyigra6dgblq146xxlwz")))

(define-public crate-avl-cont-0.1 (crate (name "avl-cont") (vers "0.1.3") (hash "1jr5rfm99zr0j2fkwa408ph8iga5nmzd35rvxszifryw7fsz4jfk")))

(define-public crate-avl-cont-0.1 (crate (name "avl-cont") (vers "0.1.4") (hash "0hf548s5jdxw29yjj8p5jldax5ky7kfkb43fbygmnd8rgys9xcgi")))

(define-public crate-avl-cont-0.1 (crate (name "avl-cont") (vers "0.1.5") (hash "0ldfly14yib3ijw60sqd5v3dm19zkqpmpnzqfcwpckgyvymwn9zx")))

