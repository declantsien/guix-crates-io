(define-module (crates-io av am) #:use-module (crates-io))

(define-public crate-avamain-0.1 (crate (name "avamain") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "seqset") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "sync" "fs" "io-util" "time"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.8") (features (quote ("io-util"))) (kind 0)) (crate-dep (name "whois-rust") (req "^1.4.0") (features (quote ("tokio"))) (default-features #t) (kind 0)))) (hash "0rrd495f8855nid9d9ayz3qbvdl468sgdkv8cl7c66bmk2bmrfl2")))

