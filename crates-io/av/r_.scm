(define-module (crates-io av r_) #:use-module (crates-io))

(define-public crate-avr_delay-0.2 (crate (name "avr_delay") (vers "0.2.0") (hash "1jj0bj5ikqzdkjm4f2di16b6sy3nxyxziysqx93730ncx217zzyn")))

(define-public crate-avr_delay-0.2 (crate (name "avr_delay") (vers "0.2.1") (hash "1vlkb8n7y90f7cq0hjdhxwvx689d1j8c1085sgkrvwqshri7ik7j")))

(define-public crate-avr_delay-0.3 (crate (name "avr_delay") (vers "0.3.0") (deps (list (crate-dep (name "avr-config") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bcnsm6h71g8rjpagsq2554zxbj5f23kx85mmyn0yp8zdjm26ssw")))

(define-public crate-avr_delay-0.3 (crate (name "avr_delay") (vers "0.3.1") (deps (list (crate-dep (name "avr-config") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dzjwhfd707awn7v0vbnc8yzpd6gq0bjlbvicmak8w3vi7l746hy")))

(define-public crate-avr_delay-0.3 (crate (name "avr_delay") (vers "0.3.2") (deps (list (crate-dep (name "avr-config") (req "^2.0") (features (quote ("cpu-frequency"))) (default-features #t) (kind 0)) (crate-dep (name "avr-std-stub") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mzpdszzla0wf19fmikk5zwpcamwnv4ksdn86sb3nipdfr1wlh1j")))

