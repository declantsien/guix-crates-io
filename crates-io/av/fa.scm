(define-module (crates-io av fa) #:use-module (crates-io))

(define-public crate-avfaudio-sys-0.1 (crate (name "avfaudio-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (default-features #t) (kind 1)) (crate-dep (name "block") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0gqhil7f37rn7cl5w7zvdq882x8xpck85v72w5bi9h82n4jbxbfg")))

(define-public crate-avfaudio-sys-0.2 (crate (name "avfaudio-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "block") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "01qsrslfiwcyf7nignma5h7wdkhdkigbd1r40lchdkda6ss9mdh1")))

(define-public crate-avfaudio-sys-0.2 (crate (name "avfaudio-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "block") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "01pl325iy5vk6bb2skinlv3xr5a9bfnrx3k27zy9s8f9b4h51zis")))

