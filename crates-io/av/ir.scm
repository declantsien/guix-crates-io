(define-module (crates-io av ir) #:use-module (crates-io))

(define-public crate-avir-rs-0.1 (crate (name "avir-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0821yvcbhjyy9ayb776j8ib30544a9qvqrrzl0hacc7clgimbwm2")))

(define-public crate-avirus-0.2 (crate (name "avirus") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0jkszarzk82vwm4kb47ips43fc5rhzkavjd1a2qhc66ccc2h3fzi")))

(define-public crate-avirus-0.2 (crate (name "avirus") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "03gxqbhy2qfmqpnxdb96l4wiy6sz47an7nbvqjxc86phywk4npha")))

(define-public crate-avirus-0.2 (crate (name "avirus") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "1h8c46nj3r8iflqi2krpbkhzmhnc0xasq65hhg3pbhxhz2xphlnj")))

(define-public crate-avirus-0.2 (crate (name "avirus") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0cxcm1h1qb0wmz3kbkgi97lgf11hpb7bq9lsfxcs6xdbms73bl3x")))

(define-public crate-avirus-0.2 (crate (name "avirus") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1r0zsm8whl7cqj1chf0jd3k2cxb229z4njc9dhss6cax1c139d1z")))

(define-public crate-avirus-0.2 (crate (name "avirus") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1jy23255h9vvfdg7k789nf6ga968nwc0jfm5gjl051jbxsd9j9ha")))

