(define-module (crates-io av ow) #:use-module (crates-io))

(define-public crate-avow-0.1 (crate (name "avow") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "02a7rscq63xpi8x3k1365s4nyq9bic90xgmbvml0aaczd5qfqhhf")))

(define-public crate-avow-0.2 (crate (name "avow") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "17vync7fxjvrridvw1ni2y7sm9dkxjsqmrrwws2l3sldjn7060v6")))

