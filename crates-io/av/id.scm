(define-module (crates-io av id) #:use-module (crates-io))

(define-public crate-avid-0.6 (crate (name "avid") (vers "0.6.1") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "118534jnj60589zlwr9w66frwahs9r05iw3lj3gjaaip97223ifz")))

