(define-module (crates-io av ah) #:use-module (crates-io))

(define-public crate-avahi-sys-0.10 (crate (name "avahi-sys") (vers "0.10.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "1krrpm5nsj4hk21b13lr8gnnj72r20sqppd9fghqncrkwmk2hycz")))

(define-public crate-avahi-sys-0.10 (crate (name "avahi-sys") (vers "0.10.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "0x42ggvh7pg1da3ilcrka103b977qjrf3akd6bykay48nf1hrq3h")))

