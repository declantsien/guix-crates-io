(define-module (crates-io av rd) #:use-module (crates-io))

(define-public crate-avrd-0.1 (crate (name "avrd") (vers "0.1.0") (deps (list (crate-dep (name "xmltree") (req "^0.4") (default-features #t) (kind 1)))) (hash "0knslcsh7plakf2mm54k9vjanxn3bd9pq5yrdhs0qmi2j34mdi3s")))

(define-public crate-avrd-0.1 (crate (name "avrd") (vers "0.1.1") (deps (list (crate-dep (name "xmltree") (req "^0.4") (default-features #t) (kind 1)))) (hash "0g8pmgwvskjfjy3pczxnhxx9a1adz863l3yzg2blxkbrx369afzw")))

(define-public crate-avrd-0.1 (crate (name "avrd") (vers "0.1.2") (deps (list (crate-dep (name "avr-mcu") (req "^0.1") (default-features #t) (kind 1)))) (hash "1rg4x5blmni03x296cqnd2g82y2mrfjxcn60sp6f75s0kax51p32") (features (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-0.2 (crate (name "avrd") (vers "0.2.0") (deps (list (crate-dep (name "avr-mcu") (req "^0.2") (default-features #t) (kind 1)))) (hash "0nlk8npifgzhnq3xv10vigd64wfnrn3ad1h5lkaff2qc1jr99vyr") (features (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-0.3 (crate (name "avrd") (vers "0.3.0") (deps (list (crate-dep (name "avr-mcu") (req "^0.2") (default-features #t) (kind 1)))) (hash "1mbjs80j01461w08xfn3xhmwl31kplivr2j84zg6qa328r9rqh9y") (features (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-0.3 (crate (name "avrd") (vers "0.3.1") (deps (list (crate-dep (name "avr-mcu") (req "^0.3") (default-features #t) (kind 1)))) (hash "0zf1ng3gingj14kj5pibrq4qhyj48lsicmv4svsy0z04ivippz5m") (features (quote (("default") ("all_mcus"))))))

(define-public crate-avrd-1 (crate (name "avrd") (vers "1.0.0") (deps (list (crate-dep (name "avr-mcu") (req "^0.3") (default-features #t) (kind 1)))) (hash "086iizkcnayk9jrybb77i0w3sdabc5liqcmgvsa1412wp0mjbc6w") (features (quote (("default") ("all-mcus"))))))

