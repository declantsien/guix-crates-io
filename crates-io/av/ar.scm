(define-module (crates-io av ar) #:use-module (crates-io))

(define-public crate-avareum-timelock-0.3 (crate (name "avareum-timelock") (vers "0.3.0") (deps (list (crate-dep (name "borsh") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.7.11") (default-features #t) (kind 0)) (crate-dep (name "spl-associated-token-account") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "spl-token") (req "^3.2.0") (features (quote ("no-entrypoint"))) (default-features #t) (kind 0)))) (hash "1lpx2ps3nlqy70d9lnj8hrnpyyk4dx3k6yby39mmsq8x6zxa3csh")))

(define-public crate-avareum-timelock-0.3 (crate (name "avareum-timelock") (vers "0.3.1") (deps (list (crate-dep (name "borsh") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.7.11") (default-features #t) (kind 0)) (crate-dep (name "spl-associated-token-account") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "spl-token") (req "^3.2.0") (features (quote ("no-entrypoint"))) (default-features #t) (kind 0)))) (hash "0k92abgpjxsigbcjq3wmjvh8lfdl6sv08kdlm344k41p8jipsf89")))

(define-public crate-avaro-0.0.1 (crate (name "avaro") (vers "0.0.1") (hash "07irfywrrlnfj660hkvmv88xn24j79q3gss8hma0jb8xwa55w94i")))

(define-public crate-avaro-0.0.2 (crate (name "avaro") (vers "0.0.2-alpha") (deps (list (crate-dep (name "bigdecimal") (req "^0.2.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.0") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.1") (features (quote ("lexer"))) (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.19.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "unicode_categories") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ag4yxvr9s3jcrk1q3j9fk32ni9kpwncv3pahwgnksl352r1b3ib")))

(define-public crate-avaro-0.0.2 (crate (name "avaro") (vers "0.0.2-alpha.1") (deps (list (crate-dep (name "bigdecimal") (req "^0.2.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.0") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.1") (features (quote ("lexer"))) (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.19.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "unicode_categories") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0hh265mgs1l4zjz8jfikiq1rwb3ccqibfgy1gsii2bbw4mvf7yp4")))

