(define-module (crates-io av -c) #:use-module (crates-io))

(define-public crate-av-codec-0.1 (crate (name "av-codec") (vers "0.1.0") (deps (list (crate-dep (name "av-data") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fimjfqpbr9xk6mn0fmkjvdvj9d7q39pbj5jg0b6xbisnlk4q14p")))

(define-public crate-av-codec-0.2 (crate (name "av-codec") (vers "0.2.0") (deps (list (crate-dep (name "av-data") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "err-derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "15kr0l0rzh7q4aqkz39mgb2hr6lis9dbqhaf7c42jw16l9x27vdx")))

(define-public crate-av-codec-0.2 (crate (name "av-codec") (vers "0.2.1") (deps (list (crate-dep (name "av-data") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "err-derive") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "130zc553qzwrkg59pfc254yc3ich54r6mrlsivbcnajd7kl8xmvg")))

(define-public crate-av-codec-0.2 (crate (name "av-codec") (vers "0.2.2") (deps (list (crate-dep (name "av-data") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q1iq9h3pacjfqx4581sgpshc9m5kwrr3pwzjh1lm5zbimdhdyca")))

(define-public crate-av-codec-0.3 (crate (name "av-codec") (vers "0.3.0") (deps (list (crate-dep (name "av-data") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "13y78pdq5w971m4k7aa5a40v23kbmr8ryy1lgcjldrkcq02c7xqz")))

