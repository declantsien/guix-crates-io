(define-module (crates-io rz fs) #:use-module (crates-io))

(define-public crate-rzfs_lib-0.0.0 (crate (name "rzfs_lib") (vers "0.0.0") (hash "086h9rby844wy3gyr4fncgibspf2qmabxbmd8zjmwb18snmvmsfn") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-rzfs_userspace-0.0.0 (crate (name "rzfs_userspace") (vers "0.0.0") (deps (list (crate-dep (name "rzfs_lib") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0329d3lvzrmhd5k4pp5pm82k6pc8ralqg089n812xfxv6sx0bfpr") (yanked #t)))

