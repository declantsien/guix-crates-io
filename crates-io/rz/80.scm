(define-module (crates-io rz #{80}#) #:use-module (crates-io))

(define-public crate-rz80-0.1 (crate (name "rz80") (vers "0.1.0") (deps (list (crate-dep (name "minifb") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "043llws61j7bmc3065ihkqjx0j7nji649c0baw9z0652nma4cl54")))

(define-public crate-rz80-0.1 (crate (name "rz80") (vers "0.1.1") (deps (list (crate-dep (name "minifb") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "1jbbmkwdrig9jmjzf5xpgafqzcbpwh95mfdjap57lfq92rlvcp3c")))

