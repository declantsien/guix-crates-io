(define-module (crates-io rz -e) #:use-module (crates-io))

(define-public crate-rz-embed-0.1 (crate (name "rz-embed") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.82") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0hcns2k460l4y1c0yvkakgfwwrm8gqk8n63b3z34rdvqhsbi2fd9")))

