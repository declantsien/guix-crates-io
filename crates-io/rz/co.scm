(define-module (crates-io rz co) #:use-module (crates-io))

(define-public crate-rzcobs-0.1 (crate (name "rzcobs") (vers "0.1.1") (deps (list (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1iw942kl022vb0h2sydrhga6jpkbw4i29f9lnwgi530jg5rkylv1") (features (quote (("std") ("default" "std"))))))

(define-public crate-rzcobs-0.1 (crate (name "rzcobs") (vers "0.1.2") (deps (list (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0f9ry89a801564pvy8q09b01hbf856z0wg8753lqr1f49fg7bx9a") (features (quote (("std") ("default" "std"))))))

