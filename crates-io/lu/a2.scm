(define-module (crates-io lu a2) #:use-module (crates-io))

(define-public crate-lua2json-0.1 (crate (name "lua2json") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "14aaanp019zi03jbnphq3f7g6xm667lr67hi6303ir5kv59phcfs")))

