(define-module (crates-io lu a_) #:use-module (crates-io))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.0") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "02fmb9f0fccwik9rnnjbwv753x4avjsb0pavscpff9mh624gfk65") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.1") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "0d4w7f9wi6z49nfr49s11r8cml347p890h4iw1kd2arrw3gi45jf") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.2") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1dfg7nry2lriilvs2isv7d2j6f9vmv2glbajr9a01c7visw3zllf") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.3") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1plvsri398hr0xykah6321mg4zwrbdj0q98dy1rd4jh90x6hc3dl") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.4") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "0v0hzpx67qndvd3cjqxjdzjyf83rjmjzkvbb31j44v0j6pm7x2li") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.5") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1783xkzdvm26rdszcbj82bf4qg40w40zr88lvv50pzhwvmg5590h") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.6") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1bc8misjar4h6gdkqf0hz8sx13yxys2h8dzmfj65l29371amkq0b") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.7") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1r8mvl7i9laaz8mmhl3agmdl7fa2qasxkk7pnc7cnrv1v1cj99js") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.8") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "0cnx4h6dw7pqkwhn6sqc4zjj3pwg2kxhqfl8y8rkf5vsj247khi6") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.9") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "028bnv8gg0gqvsqzr2z1qv5cq8pvp39li510jwxbjjmb0k6p1awy") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.10") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "0vf0dmw47bk22k8b3885klfjnhw0p1vx2jyrz0viiykpss6icqlh") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.11") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1b2xm1rhjl857baymnpjir5462ayanz2l5f8vwdwsik9mz9j598p") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.12") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1w3fphpwvi867xvy7cgxfncrdbvzmik4mvjhsa5iw58g88pxmfpn") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.13") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1lsmqf7w2mc4wfwqhbg7vgrc8af4z8cr37xgrbrd8vk0h2zv71ag") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.14") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1w0vzr4l3c4v6b9wrmq4rc9v3dxi01hib9lr1228v454k1dcvccj") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.15") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "02mim6bsp4pld2gclhja287r521mqfk2zqz9fbx3j25dcss2yhvc") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.16") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "1j5jq3gqz23kmkxx7mq5ii42d33agff791zwjikj1p663f0pg7gf") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.17") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "0qr2fblx274cq9q6z0a0l61qjxc8950xdg2dayifcr26qi0ms3ib") (features (quote (("default"))))))

(define-public crate-lua_actor-0.1 (crate (name "lua_actor") (vers "0.1.18") (deps (list (crate-dep (name "fp_rust") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "0.*") (default-features #t) (kind 0)))) (hash "0nzd803nvb6p973lz61wc1gya09ykr5v2ipfwsqk8ah3jx7h4rqb") (features (quote (("default"))))))

(define-public crate-lua_actor-0.2 (crate (name "lua_actor") (vers "0.2.0") (deps (list (crate-dep (name "fp_rust") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1klym43pfyk3sd3l0zg120qxz2f6lrmrjmdpn1jiz34cna13s8cl") (features (quote (("default"))))))

(define-public crate-lua_actor-0.2 (crate (name "lua_actor") (vers "0.2.1") (deps (list (crate-dep (name "fp_rust") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1q2kc5p446jb8d5drqvfgfhpdxgsbjgb6gslq3xr7qvv5wlxgfx4") (features (quote (("default"))))))

(define-public crate-lua_actor-0.2 (crate (name "lua_actor") (vers "0.2.2") (deps (list (crate-dep (name "fp_rust") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0d47m0zmi3bc4408lyzf1b0ixsmf95qp6m91c2n9cfk29x3x570a") (features (quote (("default"))))))

(define-public crate-lua_actor-0.2 (crate (name "lua_actor") (vers "0.2.3") (deps (list (crate-dep (name "fp_rust") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1ym8il9bmqgqnr5s1zfr9jd78v47842wwaz76gg6y8h2fpylwm0j") (features (quote (("default"))))))

(define-public crate-lua_actor-0.2 (crate (name "lua_actor") (vers "0.2.4") (deps (list (crate-dep (name "fp_rust") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "013g8hgi62330fdlxqfrgvp9r8qbhpndryj39qvx50v897x4laiy") (features (quote (("default"))))))

(define-public crate-lua_bind_hash-1 (crate (name "lua_bind_hash") (vers "1.0.0") (hash "1azqn76fl3mray7qnl5iixj9xf8vxbx5igcv9apj199k8kyhnx2k") (yanked #t)))

(define-public crate-lua_bind_hash-1 (crate (name "lua_bind_hash") (vers "1.0.1") (hash "12himfbi77wvq1nbv3pjax0kp6k3x6dca82xgmmdhfcb37ypb5pf")))

(define-public crate-lua_cdtk-0.1 (crate (name "lua_cdtk") (vers "0.1.0") (deps (list (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.8.3") (features (quote ("luajit" "vendored"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0kj3nrd26qcqsqnwgg1pi50hdrjdjzwlk69c5ywip7j14c3an5iq")))

(define-public crate-lua_engine-0.1 (crate (name "lua_engine") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lua_engine_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kq9cm4vr3i545k0dyjjfrjr7y44gq8cwv5p8ny6a1nx403cb5sa")))

(define-public crate-lua_engine-0.1 (crate (name "lua_engine") (vers "0.1.1") (deps (list (crate-dep (name "lua_engine_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lifzsqw2n5f34zgwnw9jsjj0jzqjnb7x4jw8awd5az7z6c0ny5i")))

(define-public crate-lua_engine_macros-0.1 (crate (name "lua_engine_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "02cz2vn7pm3mwa2p8r3csh080bnmvf5px0z1frqv3i1rnv00g8nb")))

(define-public crate-lua_wrapper-0.1 (crate (name "lua_wrapper") (vers "0.1.0") (deps (list (crate-dep (name "rlua") (req "^0.15.3") (default-features #t) (kind 0)))) (hash "0k2c6pfrxjz6lflqb24917rp571i7mh6xpjbgi2h9i59a5rcg2l3")))

