(define-module (crates-io lu nk) #:use-module (crates-io))

(define-public crate-lunk-0.1 (crate (name "lunk") (vers "0.1.0") (deps (list (crate-dep (name "gensym") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0b5y1la6xwwg8va226gi3xijg42ipx7daq59ycp1sznn5d63fmmq")))

(define-public crate-lunk-0.1 (crate (name "lunk") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "082h7737bw763bwk3k5fi40vs997v35k3ylhwcs4ym7lgcy04mrk")))

(define-public crate-lunk-0.1 (crate (name "lunk") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1jb3ffpjwgfblj80m11h9dx598i45hsfpnpr2aqm3nfl4jmbi6dg")))

(define-public crate-lunk-0.1 (crate (name "lunk") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "13h7xm4nl6zg7hcklbc12p991x8pn0qzy9f5rdf36hxp5xp53lbl")))

(define-public crate-lunk-0.2 (crate (name "lunk") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1c0f8yk985ijcym67bqga6pwkwvmhggnjm71hvnpm3han9chk826")))

(define-public crate-lunka-0.1 (crate (name "lunka") (vers "0.1.0") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "013l5vhzgqp5b3rw08arzj70zcmpjjw9py2zfap3c2sbgd54bicg") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib")))) (yanked #t)))

(define-public crate-lunka-0.1 (crate (name "lunka") (vers "0.1.1") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1i4d9pgxbfxga8g3ybj1wlsxgx8pl255k33kjfws1pc0z00wngny") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.2 (crate (name "lunka") (vers "0.2.0") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (features (quote ("alloc"))) (kind 0)))) (hash "1wh3wg1yv0iv4hzjfsv9zdm2y46hzmx8n6hqzk7b88vpj588ycli") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.3 (crate (name "lunka") (vers "0.3.0") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (features (quote ("alloc"))) (kind 0)))) (hash "1hx8lwk7r0mfl8vk1yh2jq430hwxmd419ph829hwnwyrrc4qhp7g") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.4 (crate (name "lunka") (vers "0.4.0") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (features (quote ("alloc"))) (kind 0)))) (hash "1lybaxls7a3ngc7psb3rcm1jh8gwb35ha5wpwlp603gyxi9hcw56") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.4 (crate (name "lunka") (vers "0.4.1") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (features (quote ("alloc"))) (kind 0)))) (hash "0p2f635fk8pkzp91m7p3xb9caaswpk1a4l8cgsmk4jc066bbmfnr") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

(define-public crate-lunka-0.4 (crate (name "lunka") (vers "0.4.2") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (features (quote ("alloc"))) (kind 0)))) (hash "1yl7406d6qhp06f8kmj7hvs9v8zgqjydyvqlryjianr4156i5m0s") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib")))) (yanked #t)))

(define-public crate-lunka-0.4 (crate (name "lunka") (vers "0.4.3") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (features (quote ("alloc"))) (kind 0)))) (hash "0gf66iy2s054f6sa7n4h82xdwfn4cchh8gz4lwjn6qf3abkvvxk4") (features (quote (("use-32-bits") ("stdlibs") ("default" "auxlib" "stdlibs") ("auxlib"))))))

