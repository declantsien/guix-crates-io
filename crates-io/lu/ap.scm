(define-module (crates-io lu ap) #:use-module (crates-io))

(define-public crate-luaparse-0.1 (crate (name "luaparse") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8.0") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "03jfp4whrszfc8kwljgifxr38kw5hhj0q0kv7sargliyicb3sj2a")))

(define-public crate-luaparse-0.1 (crate (name "luaparse") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8.0") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1xjpr0b3fs9an2lsckp0xi1x9izsvna8q7iywc9dpragn3ls1gvn")))

(define-public crate-luaparse-0.2 (crate (name "luaparse") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8.0") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "09ffj1xc86xz3l8fm7p8yjfwn4zqlmprrc7ia6mg7phc1z896k7v")))

(define-public crate-luaparser-0.1 (crate (name "luaparser") (vers "0.1.0") (deps (list (crate-dep (name "lualexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p3v5vbslh39xmjirysrq3rhzlfizvpcbmhxwvn826j84kvrn22h") (features (quote (("nodes") ("default" "nodes"))))))

(define-public crate-luaparser-0.1 (crate (name "luaparser") (vers "0.1.1") (deps (list (crate-dep (name "lualexer") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0dm4ilgx83bi1w793822s7q9l4bamxa0nsq8b6vnhij8xksjbj6m") (features (quote (("nodes") ("default" "nodes"))))))

