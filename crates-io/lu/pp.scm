(define-module (crates-io lu pp) #:use-module (crates-io))

(define-public crate-lupp-0.1 (crate (name "lupp") (vers "0.1.0") (hash "1p9py5apg5yx297gg7xva4wp1a5lhd9nkwp5nzpbz38v1lss0xyl")))

(define-public crate-lupp-0.1 (crate (name "lupp") (vers "0.1.1") (hash "0aa8dvzhxa9z204pkfhsbzvdgdqz4n3v6q1wj64gygnsbl1ql2mm")))

(define-public crate-lupp-0.1 (crate (name "lupp") (vers "0.1.2") (hash "1ssr5dyz16h6m86awr7m77i0fqnmsdnsw0nzwyjlr0ja13403jv7")))

(define-public crate-lupp-0.1 (crate (name "lupp") (vers "0.1.3") (hash "1n2m91kpkh4b2m3n9wvl6zchmxb0hrnnn62967kyv7x6hbxl0qsg")))

