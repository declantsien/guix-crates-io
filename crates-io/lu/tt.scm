(define-module (crates-io lu tt) #:use-module (crates-io))

(define-public crate-lutt-0.1 (crate (name "lutt") (vers "0.1.0") (deps (list (crate-dep (name "cocoa") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03rcbbp9s5v99jka4aj7173grcpcy5zqs9d8daw663g0vx3kyfp1")))

