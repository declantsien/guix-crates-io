(define-module (crates-io lu mu) #:use-module (crates-io))

(define-public crate-lumus-log-0.1 (crate (name "lumus-log") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "10rrm15yx1bsvd1x8xm8ajhgh22afxpfh4z9qlsz9w76j334i15y")))

(define-public crate-lumus-log-0.1 (crate (name "lumus-log") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0rzr928wrmlzyrdfwa9pyri9mz7fnc7m6pjm3f6id43fgwjln4i8")))

(define-public crate-lumus-sql-builder-0.1 (crate (name "lumus-sql-builder") (vers "0.1.0") (hash "0i005lazdr6npwm13s5d9xm2vw7xfgizg4y0k1lpxal9x6sj9ndc")))

(define-public crate-lumus-sql-builder-0.1 (crate (name "lumus-sql-builder") (vers "0.1.1") (hash "1zjbv1z9d20spf8xjpyl0cjp6ygw1j6s08wx0w80ni07i1fm8dh9")))

(define-public crate-lumus-sql-builder-0.1 (crate (name "lumus-sql-builder") (vers "0.1.2") (hash "0207hvcmq03dydzp1lm28b12p3v9f6pr9l469g59i9j4ww2kd2mh")))

(define-public crate-lumus-sql-builder-0.1 (crate (name "lumus-sql-builder") (vers "0.1.3") (deps (list (crate-dep (name "sqlite") (req "^0.34.0") (default-features #t) (kind 2)))) (hash "13xi6pf26fpwwrgp36lg410sqljx5g9972mk6saklkl1xxmzy2j2")))

