(define-module (crates-io lu hn) #:use-module (crates-io))

(define-public crate-luhn-0.1 (crate (name "luhn") (vers "0.1.0") (hash "031jd1x1wdyhdg0p9ygqyqq6kssp4bpqi38w7d4gxz58364i8m9d")))

(define-public crate-luhn-1 (crate (name "luhn") (vers "1.0.0") (deps (list (crate-dep (name "digits_iterator") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kl6fdwpc1hfkwbc2rs507a5n978gls9ipivbf2lrj1c779zp9fm")))

(define-public crate-luhn-1 (crate (name "luhn") (vers "1.0.1") (deps (list (crate-dep (name "digits_iterator") (req "^0.1") (default-features #t) (kind 0)))) (hash "02vhz2xbkk1s76pwn6arfb86fjilqc77zg6833h10fra80qvh42d")))

(define-public crate-luhn-rs-0.0.1 (crate (name "luhn-rs") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1zknj6nm7hq8ym8ndi5mj28lxsxq0ch247n5hjs1s6qnmhsz4iv9")))

(define-public crate-luhn2-0.1 (crate (name "luhn2") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-digitize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ifx47bg7mdk6jinhb4gy8dj7911pw39ql2f7dfl30kxpcqaaclk")))

(define-public crate-luhn2-0.1 (crate (name "luhn2") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-digitize") (req "^0.1") (default-features #t) (kind 0)))) (hash "02167rkhvrjqgx44v5a4y4lfz04qsi4ijpfkn4gikamnsqan797z")))

(define-public crate-luhn2-0.1 (crate (name "luhn2") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-digitize") (req "^0.4") (default-features #t) (kind 0)))) (hash "19125ii0dq4n4ipa4qkwrvysgln1j3pq2x1hkv91kk21vg88vjjk")))

(define-public crate-luhn3-1 (crate (name "luhn3") (vers "1.0.0") (hash "0hkpdjh8xgw0p7z3py095fmpbh6m9wi094q455srb8m569mfpqhx")))

(define-public crate-luhn3-1 (crate (name "luhn3") (vers "1.0.1") (hash "0g5zbic7amzhxvz5scdvwmjz3bxl3fyzgykm2vj9jcf69j071j6i")))

(define-public crate-luhn3-1 (crate (name "luhn3") (vers "1.0.2") (hash "0xfxygfhhzf3b47q92ir1qa07sym9nxhsqrf51bxzxwj6f2zh1iw")))

(define-public crate-luhn3-1 (crate (name "luhn3") (vers "1.0.3") (hash "13dcf94qvk5zq7pf9b28p5jcnlyx8a8f95md0wgrh3rb8v7zg7dh")))

(define-public crate-luhn3-1 (crate (name "luhn3") (vers "1.0.4") (hash "1sflbdq4z9z92mkaq5nsxca9wp228v2bq7xlqcfss03awfyzqqcb") (yanked #t)))

(define-public crate-luhn3-1 (crate (name "luhn3") (vers "1.1.0") (hash "1xk7m8dpgzif8ap1i2mznzmhbs8mwz9nm69baad8d53j7iyvpj7p")))

(define-public crate-luhncalc-1 (crate (name "luhncalc") (vers "1.0.0") (deps (list (crate-dep (name "digits_iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "luhn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1nnmbychyhavahv6xn18jbsbaszkliq8m9brd00hf0hmrqlvpx5l") (yanked #t)))

(define-public crate-luhncalc-1 (crate (name "luhncalc") (vers "1.0.1") (deps (list (crate-dep (name "digits_iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "luhn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0m0j059m1b79y8jvx1ybs6g40vk0m1s6whb5kpyjlv0p5yja1h03")))

(define-public crate-luhnmod10-1 (crate (name "luhnmod10") (vers "1.0.0") (hash "0lvqsjzk3x792ml1dnakci21084g4c9bs5c0n077v6nbqphbr2rs")))

(define-public crate-luhnmod10-1 (crate (name "luhnmod10") (vers "1.1.0") (hash "0hsxmpxnvlay5rbc1khh4zxzl9wrngl5fp1ypda6qj5yvrsax5fv")))

(define-public crate-luhnr-0.1 (crate (name "luhnr") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0x3d96sjqabvw18axf1bj7znfk4bb0ziklg69vl5dj5vav332lar") (yanked #t)))

(define-public crate-luhnr-0.2 (crate (name "luhnr") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "15g9mqjsg9nqq8jjw43h2gd9c313f442biyhjpfdyz4bvafzg78j") (yanked #t)))

(define-public crate-luhnr-0.3 (crate (name "luhnr") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0d18gz8mq91mzn3apbjsf6m4hp9cndb09z9djqwrfpzv39h3dvhd")))

(define-public crate-luhnr-0.3 (crate (name "luhnr") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0vc7gd6jld0cmrcqx2c6ikl8lhx5839wy9z8n882bsybxc39q6fx")))

(define-public crate-luhnr-0.3 (crate (name "luhnr") (vers "0.3.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0sv1cr9x0ql5nwjlrkvb9y3iflchhng66jvyzca6jmdgdlxkx9nh")))

(define-public crate-luhnr-0.3 (crate (name "luhnr") (vers "0.3.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "15v07pb4gln7xb5f2pira86rgjwvarrgkx8bzrwz68y70q1106gq")))

(define-public crate-luhnr-0.3 (crate (name "luhnr") (vers "0.3.4") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0ipknbaha8bn59xdfh6cr4ysszdw3p59wvh3mv78kka38z242pa7")))

(define-public crate-luhny-0.1 (crate (name "luhny") (vers "0.1.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1hkyscmnhzw3y7hlh5y1hk541g04wx739rh88nfx1bmiw4977iqr")))

(define-public crate-luhny-0.2 (crate (name "luhny") (vers "0.2.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "065scgs0m6w0q13dr5kiwm06qnbv7vx3aa0756dw4h2ryj4clh70")))

(define-public crate-luhny-0.3 (crate (name "luhny") (vers "0.3.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1x5kfganvddv6i1fxmff0b5r24l1vynxq5x3jfnsc4fx2n9qxfr3")))

