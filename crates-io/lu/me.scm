(define-module (crates-io lu me) #:use-module (crates-io))

(define-public crate-lumen-0.0.0 (crate (name "lumen") (vers "0.0.0") (hash "1bh3l60y26w2ck4b92wlp3rp7lp1l9d87q1cbbz1fzsi4qsf4a58")))

(define-public crate-lumen-language-0.1 (crate (name "lumen-language") (vers "0.1.0") (deps (list (crate-dep (name "exec") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0qp3khccac9w2yqxa5rjfl6qbmafjagzdjz7b08x6vvrmw2lrz65")))

(define-public crate-lumenpyx-0.1 (crate (name "lumenpyx") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29.9") (default-features #t) (kind 0)))) (hash "1jlpbbrwgax69gnqhs9kjf7mk54bkdxn663rfgsccrbcdn4pjgiv")))

