(define-module (crates-io lu ll) #:use-module (crates-io))

(define-public crate-lull-1 (crate (name "lull") (vers "1.0.1") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "gdk") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "gio") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0v1z7d0ii6a8jgjz68vfdwz6pxynjdr292vxd3xqmifsfxbgwp9i")))

(define-public crate-lull-1 (crate (name "lull") (vers "1.0.2") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "gdk") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "gio") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0l23lzwp5bhgy6kgxmq5l0piw218p70n0z1kb3nz07szcnxrvjh1")))

