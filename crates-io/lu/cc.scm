(define-module (crates-io lu cc) #:use-module (crates-io))

(define-public crate-lucchetto-0.1 (crate (name "lucchetto") (vers "0.1.0") (deps (list (crate-dep (name "lucchetto-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0") (default-features #t) (kind 0)))) (hash "13042j5ls7yd5j3kabl5al3b08ix5l8sgg956smp1r6xzz5ysz7w")))

(define-public crate-lucchetto-0.2 (crate (name "lucchetto") (vers "0.2.0") (deps (list (crate-dep (name "lucchetto-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "00184f06lcgjsjf0j6fqbcdmvxknnw2843v6b49vczdv8c70nd2k")))

(define-public crate-lucchetto-0.3 (crate (name "lucchetto") (vers "0.3.0") (deps (list (crate-dep (name "lucchetto-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "194h0g92kn4rv0byadflraadvdnr4yf7p11g8m1198z1hdndsmv9")))

(define-public crate-lucchetto-0.4 (crate (name "lucchetto") (vers "0.4.0") (deps (list (crate-dep (name "lucchetto-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "rb-sys") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0k1vw3k863fv1fxqmw856gpv0s098mg6q8z2kyzzp0b71r5j9kdh")))

(define-public crate-lucchetto-macros-0.1 (crate (name "lucchetto-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dklcd0awhirihdjjiizxrbssmmc3h4jdzca8nglj909r2f5jai9")))

(define-public crate-lucchetto-macros-0.2 (crate (name "lucchetto-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cv0san07d2i7zdlv53drrvmydbhgj2gq6ak75xl9ncwj1qh5h6s")))

