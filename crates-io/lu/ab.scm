(define-module (crates-io lu ab) #:use-module (crates-io))

(define-public crate-luabins-0.1 (crate (name "luabins") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.8") (default-features #t) (kind 0)))) (hash "076ik5fmvxfasbfmllc4wzgkavkqi70qjh05wcxq04djn1cbjqr3")))

(define-public crate-luable-0.1 (crate (name "luable") (vers "0.1.0") (hash "113jpsz3w9q017zb1156pxxv61kixbbvs807s6jdslx0c6ig7j9s")))

(define-public crate-luable-0.1 (crate (name "luable") (vers "0.1.1") (hash "17cjcxxljc2653j92qjmlrvb4n0cb4wylszaiammv4z6k2pdmcbl")))

(define-public crate-luable-0.1 (crate (name "luable") (vers "0.1.2") (hash "01rf22lc70isczir0wz212pvarcvxxzq6crwgyzx4bmh38smxvrz")))

(define-public crate-luable-0.1 (crate (name "luable") (vers "0.1.6") (hash "0q06af2ja5ph8p13bkdbjn1wq7k7ml9az8bvk933aiv4sx2dghmx")))

(define-public crate-luable-0.1 (crate (name "luable") (vers "0.1.7") (hash "183yqs1mv6ydq40xi74fh56bbbx10k7ngzy34wd5i5c2cygwas2d")))

(define-public crate-luable-0.1 (crate (name "luable") (vers "0.1.8") (hash "1md5wwgm702r19vxjqj6ma5a0sk2kx17mbwvyjwshh9c5b7bxr3f")))

(define-public crate-luabundle-0.1 (crate (name "luabundle") (vers "0.1.0-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clearscreen") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "color-print") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "darklua") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "stacker") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "17ifgrjm8jcaqybs6j5644cbpf12k4zwqsp9cizqv5lnb40vxjg0")))

(define-public crate-luabundle-1 (crate (name "luabundle") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clearscreen") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "color-print") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "darklua") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "stacker") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1a6p88p4020pr9dld8yq5g6v9yc5sjrhz8gvspppkxgk44xlzakx")))

(define-public crate-luabundle-1 (crate (name "luabundle") (vers "1.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clearscreen") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "color-print") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "darklua") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "stacker") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "156hx7sh2cmvymk8fysb64g5dnh3vf67y7g9s5i62ay78xg71rm1")))

(define-public crate-luabundle-1 (crate (name "luabundle") (vers "1.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clearscreen") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "color-print") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "darklua") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "stacker") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "158wq6apj0414wcij9036yi1p4h738ykphghln0ac22algi5r15b")))

