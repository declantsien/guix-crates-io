(define-module (crates-io lu x-) #:use-module (crates-io))

(define-public crate-lux-ik-0.1 (crate (name "lux-ik") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)))) (hash "026ljjz8b3fv71mdbw081n4s82w1h0j9w2vsqw2kn11sq87xxn9g")))

(define-public crate-lux-ik-0.1 (crate (name "lux-ik") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)))) (hash "0yf34mqjy4jsfq0zh8p95cxkvady89bwp4f8pwfhczids8fxksbm")))

