(define-module (crates-io lu a-) #:use-module (crates-io))

(define-public crate-lua-jit-sys-2 (crate (name "lua-jit-sys") (vers "2.0.50") (deps (list (crate-dep (name "bindgen") (req "^0.31.3") (default-features #t) (kind 1)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0wmiwyck18i9ws6xybmc7zhci5fjch1v4jp2r6nm9h3w9wbzaf94") (features (quote (("use_valgrind") ("use_sysmalloc") ("use_gdb_jit") ("use_assert") ("use_apicheck") ("strip") ("sse2") ("sse") ("ps3" "use_sysmalloc" "cellos_lv2") ("optimize_size") ("nummode_2") ("nummode_1") ("force_32_bit") ("extra_warnings") ("enable_lua52_compat") ("disable_jit") ("default" "arch_x64") ("cellos_lv2") ("arch_x86") ("arch_x64") ("arch_powerpc_spe") ("arch_powerpc") ("arch_mipsel") ("arch_mips") ("arch_arm"))))))

(define-public crate-lua-kit-0.1 (crate (name "lua-kit") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1x7f26h93w0hh8bfiph5w5p1dgz9lzmcn8dm9kxjy2rm8a39628a")))

(define-public crate-lua-latest-sys-0.0.0 (crate (name "lua-latest-sys") (vers "0.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "18p6nplmyd4dix7iappil0pvm1rw0d0wrncy06clmpi2rppajp5b") (links "lua5.4")))

(define-public crate-lua-latest-sys-0.0.1 (crate (name "lua-latest-sys") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1k0g035hhmbcspqw1dgah4sakbgp6fr11a40lnyyyn8cglrnm85w") (links "lua5.4")))

(define-public crate-lua-latest-sys-0.0.2 (crate (name "lua-latest-sys") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "15bmm9d6bqar92ajsxp54w8bfkc0dcg7s5kdcnlwib1r8i91g8hv") (links "lua5.4")))

(define-public crate-lua-macro-0.1 (crate (name "lua-macro") (vers "0.1.0") (deps (list (crate-dep (name "lua-macro-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.7.4") (features (quote ("lua54"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "1yp2p88y6yyf0jrkq3rndxjacs8bvmmrxjw4lksd0f0pf10b3s6n")))

(define-public crate-lua-macro-0.1 (crate (name "lua-macro") (vers "0.1.1") (deps (list (crate-dep (name "lua-macro-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.7.4") (features (quote ("lua54"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "1cf21z572ifcff0x4kyqlx0m0mi4r43k6r2vj99cbg7yx2viy16f")))

(define-public crate-lua-macro-impl-0.1 (crate (name "lua-macro-impl") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "0jpc3jcd76nlk0jqb36gi5mjldb2hyn8lwsml30mp8m3qw91an56")))

(define-public crate-lua-macros-0.1 (crate (name "lua-macros") (vers "0.1.8") (deps (list (crate-dep (name "lua") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0fbq0svq4v9xad0w84hc4m3kygyslbn8735mn59q84k92adi3iv9")))

(define-public crate-lua-macros-0.1 (crate (name "lua-macros") (vers "0.1.9") (deps (list (crate-dep (name "lua") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "17yi50fwfw3y8zzv2nfh6fb755s27dl0xyfzv8kpziav3d2p5jz0")))

(define-public crate-lua-pattern-0.1 (crate (name "lua-pattern") (vers "0.1.0") (deps (list (crate-dep (name "document-features") (req "^0.2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "10pj4548gj8apgvklwzbn2kfk8bpqkxrp7jq65y5y8k2zg6aa5n4") (features (quote (("to-regex") ("default")))) (v 2) (features2 (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-lua-pattern-0.1 (crate (name "lua-pattern") (vers "0.1.1") (deps (list (crate-dep (name "document-features") (req "^0.2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0mvci1a07i2jm0sx7h63332fg3f3vdrxik9djkrch0fq0p36vz7n") (features (quote (("to-regex") ("default")))) (v 2) (features2 (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-lua-pattern-0.1 (crate (name "lua-pattern") (vers "0.1.2") (deps (list (crate-dep (name "document-features") (req "^0.2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0irmcn6hnqaimxra2k8r56ygn4zpc0p226ak0bbzwwqpfigi9fam") (features (quote (("to-regex") ("default")))) (v 2) (features2 (quote (("docs" "dep:document-features" "dep:rustc_version"))))))

(define-public crate-lua-patterns-0.1 (crate (name "lua-patterns") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0wpqzdqa0hxdsd2qwkq4b2f87wkmiyj8afy3vbyljz2gjv9xa92k")))

(define-public crate-lua-patterns-0.1 (crate (name "lua-patterns") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "16x4z3l5bi1cbvvxacwfk6a5bbikwzv8a8lghx45sk41ay6whsid")))

(define-public crate-lua-patterns-0.3 (crate (name "lua-patterns") (vers "0.3.0") (hash "05h2jwpbqdb75xg5s457pw0a578h6l2yfcj5fmfq5bx57pp6ph45")))

(define-public crate-lua-patterns-0.4 (crate (name "lua-patterns") (vers "0.4.0") (hash "10d2wlih28wbrw47c1ib9b871qjgadfw14z5xynnci0zy07ajg9g")))

(define-public crate-lua-perf-0.1 (crate (name "lua-perf") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "blazesym") (req "=0.2.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "clap_derive") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.1") (default-features #t) (kind 0)) (crate-dep (name "gimli") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.7.1") (features (quote ("elf64"))) (default-features #t) (kind 0)) (crate-dep (name "iced-x86") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libbpf-cargo") (req "^0.21.2") (default-features #t) (kind 1)) (crate-dep (name "libbpf-rs") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.16.0-RC1") (default-features #t) (kind 0)) (crate-dep (name "procmaps") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "psutil") (req "^3.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (features (quote ("formatting" "local-offset" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("ansi" "env-filter" "fmt"))) (default-features #t) (kind 0)))) (hash "108z8jm8pj6q7dmrk2wnw394p2iwdvvjnsw0lczpadd2qnxsplbc")))

(define-public crate-lua-protobuf-rs-0.1 (crate (name "lua-protobuf-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.9.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-parse") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "protoc-bin-vendored") (req "^3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)))) (hash "0zkjgrr6zml8n82is67gqhhh9hcmz3xyx6mzk2syrp984bsv2zfa") (features (quote (("vendored_protoc" "protoc-bin-vendored") ("module" "mlua/module") ("luajit" "mlua/luajit") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("lua52" "mlua/lua52") ("lua51" "mlua/lua51") ("google_protoc") ("default" "lua54" "module"))))))

(define-public crate-lua-protobuf-rs-0.1 (crate (name "lua-protobuf-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.9.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-parse") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "protoc-bin-vendored") (req "^3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)))) (hash "121jpfm7adg0hqmsmk99rqfk069hf798wsdn2davnx0r7laiwz5x") (features (quote (("vendored_protoc" "protoc-bin-vendored") ("module" "mlua/module") ("luajit" "mlua/luajit") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("lua52" "mlua/lua52") ("lua51" "mlua/lua51") ("google_protoc") ("default" "lua54" "module"))))))

(define-public crate-lua-protobuf-rs-0.1 (crate (name "lua-protobuf-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.9.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-parse") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "protoc-bin-vendored") (req "^3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0hzmmss7wfhh1axw0kbccsszsr67zxbaspw5jfw8ypskdzsfkb5y") (features (quote (("vendored_protoc" "protoc-bin-vendored") ("module" "mlua/module") ("luajit" "mlua/luajit") ("lua54" "mlua/lua54") ("lua53" "mlua/lua53") ("lua52" "mlua/lua52") ("lua51" "mlua/lua51") ("google_protoc") ("default" "lua54" "module"))))))

(define-public crate-lua-rs-0.0.1 (crate (name "lua-rs") (vers "0.0.1") (hash "0bk00jjcz7g1q750wn3ny0mx68i2qspqvl7f1j5d3fch09pq33xw") (yanked #t)))

(define-public crate-lua-rs-0.0.2 (crate (name "lua-rs") (vers "0.0.2") (hash "0dwbxzg2gd4pf9zm6x9hjsvqslmsr30gldjib8bfb5jp7n4vm41y") (yanked #t)))

(define-public crate-lua-rs-0.0.3 (crate (name "lua-rs") (vers "0.0.3") (hash "0zwb4fiqzpj0cmcyl3rcyq8zvdvy1zaf40mzspkwmv7aych3svyd") (yanked #t)))

(define-public crate-lua-rs-0.0.4 (crate (name "lua-rs") (vers "0.0.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "04fxd379fqbvrndh208i4ahasxbcmv97dnsz6qlvbr6q82brbzyi") (yanked #t)))

(define-public crate-lua-rs-0.0.5 (crate (name "lua-rs") (vers "0.0.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1lbf2kxnq5s0cklvccr2q6gjax0dc3a9la47anww12hkxwcj5qc9")))

(define-public crate-lua-rs-0.0.6 (crate (name "lua-rs") (vers "0.0.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1ba1bph7bzgp93y9ljf5akv8x4l1gxa5prsk5q74jgksam28qzk7")))

(define-public crate-lua-rs-0.0.7 (crate (name "lua-rs") (vers "0.0.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "12k70n1b5njriszv6whmgidhqnyaazqn1kyvc3j34hxfn1wjc251")))

(define-public crate-lua-rs-0.0.8 (crate (name "lua-rs") (vers "0.0.8") (deps (list (crate-dep (name "cc") (req "^1.0.18") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0srq8rk8dmgnzp1wszz07kl1w49hg3niffa2pw3xqw4a5g50nr23")))

(define-public crate-lua-rs-0.0.9 (crate (name "lua-rs") (vers "0.0.9") (deps (list (crate-dep (name "cc") (req "^1.0.18") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1c5fqhxqi7gyznd7v6hmc7aibns6h4ibdrr9bb1bs3i5dvj63zcz")))

(define-public crate-lua-rs-0.0.10 (crate (name "lua-rs") (vers "0.0.10") (deps (list (crate-dep (name "cc") (req "^1.0.34") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "09a15n2inv5r94jbgqscjsiw4fm49pdn7k2nslfcy0rlmkcp28f7")))

(define-public crate-lua-rs-0.0.11 (crate (name "lua-rs") (vers "0.0.11") (deps (list (crate-dep (name "cc") (req "^1.0.55") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.71") (kind 0)) (crate-dep (name "version-sync") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1dxbrlkcac3nn0y5qvk448jj7aspr8kf5wmx0f8yzazqc29rzfc0")))

(define-public crate-lua-shared-0.1 (crate (name "lua-shared") (vers "0.1.0") (hash "1n1bvwbpjafr5sw8g1bz5b145z7kq5d8vlyrf3n1407md6y8yqsh")))

(define-public crate-lua-sql-builder-0.1 (crate (name "lua-sql-builder") (vers "0.1.0") (hash "151avy4xrmyfizvy3nnss339q298h11c8y6ld1lwvcv942h0phsg") (yanked #t)))

(define-public crate-lua-sql-builder-0.1 (crate (name "lua-sql-builder") (vers "0.1.1") (hash "0z0ga46vr899qcpgrn3f88bmg0j6wdz5z8sb895jp1y17zbmk1y9") (yanked #t)))

(define-public crate-lua-sql-builder-0.1 (crate (name "lua-sql-builder") (vers "0.1.2") (hash "06kac65hwk1rgrsnhiin4cxx7ksh8j4915774vmfgpplamx9xvi0") (yanked #t)))

(define-public crate-lua-sql-builder-0.1 (crate (name "lua-sql-builder") (vers "0.1.3") (hash "14ph6lxl5ymnw73b3264shshacb3shc0szrfxz21p2w2wjx5m3c6") (yanked #t)))

(define-public crate-lua-sql-builder-0.1 (crate (name "lua-sql-builder") (vers "0.1.4") (hash "0zx1l6rk1hlpj016j367cdgkpl0l7q26qh0drk3yq64088h39aig") (yanked #t)))

(define-public crate-lua-src-535 (crate (name "lua-src") (vers "535.0.0+540b") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nr0zml34zqbfr0fwrcgqckvylvg1nj4757li1zij961kxd8xj5a")))

(define-public crate-lua-src-535 (crate (name "lua-src") (vers "535.0.1+540b") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qif405xw2wsyw03k0lc8pr74x2jzd4l0aslgd78kb47j1i4yz0d")))

(define-public crate-lua-src-535 (crate (name "lua-src") (vers "535.0.2+540rc1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1j44dwlhl0a0cjzqp9bvkhhl9mwyaadqjc0i54ynq6qqzjjv823v")))

(define-public crate-lua-src-535 (crate (name "lua-src") (vers "535.0.3+540rc3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wkf1k9rx4qjdk9nvksa5ddizaxpa0b93vvrd8n35fvy09ba2w06")))

(define-public crate-lua-src-535 (crate (name "lua-src") (vers "535.0.4+540rc4") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "05ynlz7mdqx9xzhxigcm03s3qb6pni5lr9avkqqkwixhcxdvyng4")))

(define-public crate-lua-src-540 (crate (name "lua-src") (vers "540.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "13l2hqzh35n88fcnxkvlm2hwn9wmzrc8wd5sqgjpii2pff5vrh9r")))

(define-public crate-lua-src-540 (crate (name "lua-src") (vers "540.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "17znw1sgvhh5ld4m1cifc6sd8jan44cgk0wc81rzkkwl6z8araz3")))

(define-public crate-lua-src-541 (crate (name "lua-src") (vers "541.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "1gwcl8w46ax23jrqh3ay3yyhvqi271y579m4xwrscd7lgm1cvj78")))

(define-public crate-lua-src-542 (crate (name "lua-src") (vers "542.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "1d6r22l7bxx572rx703w23vrskxq5500w23567ysaczj2gm6d22c")))

(define-public crate-lua-src-543 (crate (name "lua-src") (vers "543.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "0zd23idkgvhm3iw2x3kpfg8ggb0nnmljby85v53nzcxrjkwq1482")))

(define-public crate-lua-src-543 (crate (name "lua-src") (vers "543.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "0iq29k01j975p0nqacpxrjla84czcdfi6adjhl8hxvzi5cri8adp") (features (quote (("ucid"))))))

(define-public crate-lua-src-544 (crate (name "lua-src") (vers "544.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "12y6by7xhqwpj8g5p94nskpvy1sfyiaiqxi0ra14j73qk81vlhbk") (features (quote (("ucid"))))))

(define-public crate-lua-src-544 (crate (name "lua-src") (vers "544.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "0k4179hlmgms3wkbgi5k37sp1hqp3j3xs2aaxy6x7sfm8k4a72vh") (features (quote (("ucid"))))))

(define-public crate-lua-src-546 (crate (name "lua-src") (vers "546.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "1kps2frfbn8r6ayr1r9rl9r4i8zz05rw04fjin9b9d7ih09hrc4c") (features (quote (("ucid"))))))

(define-public crate-lua-src-546 (crate (name "lua-src") (vers "546.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "1a6wdx60i3118xwlykaz97vsz4hmrmjbk8h37md047ing2px89kw") (features (quote (("ucid"))))))

(define-public crate-lua-src-546 (crate (name "lua-src") (vers "546.0.2") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "1ci9l4nkx6fj6s9vlw9raxpf4rjjyldf6plg1k1s84g6xskxm81d") (features (quote (("ucid"))))))

(define-public crate-lua-sys-0.1 (crate (name "lua-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.21.0") (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "metadeps") (req "^1.1.0") (default-features #t) (kind 1)))) (hash "0d3jm2y8hyl3yciarvjca7g09jb0fqlcsv7zmnvnfcybkns3hf88") (yanked #t)))

(define-public crate-lua-sys-0.2 (crate (name "lua-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "~1.0.46") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.16") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.2.3") (default-features #t) (kind 1)) (crate-dep (name "va_list") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.7") (optional #t) (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "1sx1pgkaxpj082vyv3r73vfq6dw2vzj4z12cw9dcdgs1109xarlp") (features (quote (("va-list" "va_list") ("system-lua" "pkg-config" "vcpkg") ("std") ("lua-compat") ("embedded-lua" "cc") ("default" "embedded-lua" "va-list" "std")))) (yanked #t) (links "lua")))

