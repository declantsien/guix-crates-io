(define-module (crates-io lu fa) #:use-module (crates-io))

(define-public crate-lufact-0.1 (crate (name "lufact") (vers "0.1.0") (deps (list (crate-dep (name "lufact-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n4vjqs2gqn2fp3qz73yyz4yapdxgcsqbfzpclg4jq0m62v17cqq")))

(define-public crate-lufact-0.1 (crate (name "lufact") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lufact-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ikbmq83wdgwqq34svmlj3rv8zvm27vgfpmik43g2nfgqyxq75g2")))

(define-public crate-lufact-0.1 (crate (name "lufact") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lufact-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0771zirhyhgkzr2d4j89spmay2mzgi0a0nzkmvfmwlibpn336v57")))

(define-public crate-lufact-sys-0.1 (crate (name "lufact-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v7y9vf6vk2pqw74xhg4z13rggw0cdn2kf6bc2fabn7h5cydxc1w")))

(define-public crate-lufact-sys-0.2 (crate (name "lufact-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xfdy8wg5dy33q427q29x77xvj8967zwn6sf478yi02z5yaiwmb8")))

(define-public crate-lufact-sys-0.2 (crate (name "lufact-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d1yjhkaza81l5s03xr7pk1qjv8i117aa024yvisrvb7f4y4hr3m")))

