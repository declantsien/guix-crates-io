(define-module (crates-io lu sl) #:use-module (crates-io))

(define-public crate-lusl-1 (crate (name "lusl") (vers "1.0.0") (deps (list (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0lpximrmkqzkcf6nrb3bajr1zmcsk80z5q54lvn99iz5nywjmsmg")))

(define-public crate-lusl-1 (crate (name "lusl") (vers "1.1.0") (deps (list (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1a968b2kym9n9k21wl9y48inyp0cj403gqmjqbd3r9rn886bz673")))

(define-public crate-lusl-1 (crate (name "lusl") (vers "1.2.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "zip_archive") (req "^1.2.2") (default-features #t) (kind 0)))) (hash "010x89yy06l09mzmryffvlvpy77la8yf1d0i9jhgfaj584yy6ahj")))

(define-public crate-lusl-1 (crate (name "lusl") (vers "1.2.1") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0xg8z6jp350vh9hh30h9al139dpqkmyxnimjw5bvbqz3vrcjzf73")))

(define-public crate-lusl-1 (crate (name "lusl") (vers "1.2.2") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "03h9rxwklh6lxay74nz4zql41gz4q2ifxc5cvkfwnn412x4rax8s")))

(define-public crate-lusl-2 (crate (name "lusl") (vers "2.0.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0wj2s4f6iy95y14ic1vlhl0vxpfd46qagq7xmnmd6812gl0q0mfn")))

(define-public crate-lusl-2 (crate (name "lusl") (vers "2.0.2") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0mqryzccm7awva4wl715panpzlfz4bn4bv6z0gp0aiv1hny04h1g")))

(define-public crate-lusl-2 (crate (name "lusl") (vers "2.1.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "044wv4yqrn2245i4wm0f0a1mv8wr6fqkryh6fyyd8nwyv3s2ccx7")))

