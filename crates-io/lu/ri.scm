(define-module (crates-io lu ri) #:use-module (crates-io))

(define-public crate-lurien-0.1 (crate (name "lurien") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "grep") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "0hsccd5nm5gxzyp5nwmq62mh51s3djh20nv90b0122qfnpyw4mcm")))

