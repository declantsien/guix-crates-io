(define-module (crates-io lu aj) #:use-module (crates-io))

(define-public crate-luajit-0.1 (crate (name "luajit") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "0zkqlcffhrfw0ihg589rm35piv1s4svmi4hmb5nkfg15xy4kp3q4")))

(define-public crate-luajit-0.1 (crate (name "luajit") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "0m3fp23bx8li0xj89a2nfvmd4zji8ybwwg0nq6zvbpvyizmxhvi1")))

(define-public crate-luajit-bindings-0.2 (crate (name "luajit-bindings") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "089x9jl2vkjx1shp4ghrng5cqzs4qpykmpswckzr5c0xqymhah67")))

(define-public crate-luajit-src-0.0.0 (crate (name "luajit-src") (vers "0.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "07xx2b67mwlyf13b7v28pk63l68c2m490x53f37kdkg0y44xgp8s")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.0.0+694d69a") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "035j7092akqsr4zxz3j1c1gqw0f7f0j6svpl96x8v458mcq4zn40")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.1.0+resty31116c4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ksdmr448m2qlq0i2pr2c7dmaa81jgqbk2w3w5sa3vjqvk9rcbfh")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.1.1+resty29a66f7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "01ijcsp2fpkppzcccgc21yckypvrh7hj9pkryda7xv1djs7f9zc7")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.1.2+resty29a66f7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pz9nxs8q13b7mcglz04c0f0sflnvid2zx36s2b6rvr6hsn33zxr")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.1.3+restyfe08842") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "16q27b04nr8fmf0iz36wsbnaj9mqgvxvw9v5chmlkq6qfy1xxlrn")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.1.4+resty8404d7f") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nx7k9j0jqanpw56wsicv7r2nqyfvavaklxm4kxg86ircwbhb66d")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.2.0+resty5f13855") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wiviqvv0y7wcjvv5jcdxsas248h4z4idfbpl02j60lyx8i5gy29") (features (quote (("lua52compat"))))))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.3.0+restyeced77f") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0621nx5lllz09lpnz5gkwpqs5l6m074ib57wkw0nfxak6fjhfj08")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.3.1+restycd2285f") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z3y8w904s5xa6ljc1k9d2yg1w765r1njm1ka126pafb56gmf1q1")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.3.2+resty1085a4d") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "191vv51bikl4gbl4v709nfnzc8v3a9ghmz12vng5l8hkymb79qmi")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.3.3+resty673aaad") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "13kn2yrjcy6l74nnghi0vcvcwjw3bi7y0bf2vlg1zvi0b3kfl09j")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.3.4+resty073ac54") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "03qkh322rh8fnasxgsmr7cqs98c8h6khdx0fvas4593mjplhj2v4")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.0+resty124ff8d") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v3g5165ws4yq7sgagdyvk22axlg2jd1qclxf4c2w6f7q3ib4vzp")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.1+restyaa7a722") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cr7dl4w0bbdrr37sll4swria44vykphhbpy1hafx5wsbws7ja69")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.2+resty89d53e9") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qdkisf2lghczdckbr5j42z753wvdf9s6iiby53yh2r6v0qkhk7x")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.3+resty8384278") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i7g3ijad3m3aqyabssbhc1sgy7j04q7rjjmz9pwf7nzzmd5vvhr")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.4+restydf15b79") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hfwvj9533dvy34si5r7skaqn1xabi1hxdlbbmzgjs7k1pg83w21")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.5+resty2cf5186") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "15yblw421y66iss5gzfwvs75hjm4xhmnyk6qf9i7h0p680m9kdr7")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.6+resty2cf5186") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4") (default-features #t) (kind 0)))) (hash "05zia3z4id8p9d1qaa7rqnw3gdvwv5bqnx8l08b5nq3h0a7g4pn4")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.7+resty107baaf") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4") (default-features #t) (kind 0)))) (hash "0cdzv1m79srhg068d1bw5yvsnnq9yzs945zppvdfac615xss6xna")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.4.8+resty107baaf") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4") (default-features #t) (kind 0)))) (hash "0kkwr0mk56rcxzh08hif0wvwx2xxwm0kbliyv1c5f652nbl6flg0")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.0+becf5cc") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4") (default-features #t) (kind 0)))) (hash "02vi3b1w569b6m406jr6jli8bl0qdba51shddrpkzvlxpgg6s049")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.1+b94fbfb") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^5.0") (default-features #t) (kind 0)))) (hash "10klvhy87128jwgmhs5n7kydrg592ab6avqmy6d2b2jlzks11q36")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.2+113a168") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^5.0") (default-features #t) (kind 0)))) (hash "1vpz579mvf0jchww7c4jhjq2s25hh7lkln5x6cbb268qvfzcfgl2")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.3+29b0b28") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^5.0") (default-features #t) (kind 0)))) (hash "0bbr3lhi2nbm3mwplxl0v7hmhnj3dnyiy6ph974yav4i2f8bhaqc")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.4+c525bcb") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^5.0") (default-features #t) (kind 0)))) (hash "024rb8i25m6gza3rw0c04hnlnnfaqpvf9v23xx8cn8ids57an41a")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.5+f2336c4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^5.0") (default-features #t) (kind 0)))) (hash "1i1rz4qkcknbsbsmmaaxqxmvwfx6278csxbx8v0v3qzlj2bvmg6q")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.6+9cc2e42") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^6.0") (default-features #t) (kind 0)))) (hash "0ms1amqfxsmdyi23bxb87z7l0fqcr1gf5cwbyj3w3zy9b7c6bcr3")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.7+d06beb0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^6.0") (default-features #t) (kind 0)))) (hash "1r79w95mpcmywl4fxhsm13hxc8zbn7wc32pl9iq8ggxbrpd1y98d")))

(define-public crate-luajit-src-210 (crate (name "luajit-src") (vers "210.5.8+5790d25") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^6.0") (default-features #t) (kind 0)))) (hash "0ijk6xg4fqavhcxyzpfvdzszd1n30a4wpwppq8gqfbkrmpcih7s4")))

(define-public crate-luajit-sys-0.0.1 (crate (name "luajit-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.3") (default-features #t) (kind 1)))) (hash "18s7a4k3dga0ns88w76mwnm7fziba2l0wjrprqj1l2m2h3j0xw5s")))

(define-public crate-luajit-sys-0.0.2 (crate (name "luajit-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.3") (default-features #t) (kind 1)))) (hash "0pcp1jsklk5f78kph5dr3qgggwjhk93bx4hm2prljhykz3wd9cd3")))

(define-public crate-luajit-sys-0.0.3 (crate (name "luajit-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "= 0.3.2") (default-features #t) (kind 1)))) (hash "03fgyspyyyfm744yl5k8c4qc5flh8cxzsh5wk1jp2rdb8apg8bjy")))

(define-public crate-luajit-sys-0.0.4 (crate (name "luajit-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.3") (default-features #t) (kind 1)))) (hash "1pmgbc3jy6vxgjjnqjiy2rynyyb7sjsy9llxnv38in2myk91lxl2")))

(define-public crate-luajit-sys-0.0.5 (crate (name "luajit-sys") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.3") (default-features #t) (kind 1)))) (hash "0kv13l6m733r0zlzdd2ai4faffzf1y031y4p5bkpxycacsdc7wvs")))

(define-public crate-luajit2-sys-0.0.1 (crate (name "luajit2-sys") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v4lc2jxlnvzik5184bvdqr3vqlsw569dnb91w5fal3w5lrfyfab")))

(define-public crate-luajit2-sys-0.0.2 (crate (name "luajit2-sys") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1whssk7syhqbhr2jab3ny57ikxzm38wvlvm0bdj280jsrp67mfrk") (links "luajit")))

