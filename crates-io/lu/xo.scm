(define-module (crates-io lu xo) #:use-module (crates-io))

(define-public crate-luxon-0.1 (crate (name "luxon") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "gix") (req "^0.52.0") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.3.7") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.10.0") (features (quote ("fancy"))) (default-features #t) (kind 0)))) (hash "1kqfnl8c62wvk2qwymfn9hr5r6f0p0asfx442akq0lgx8ym8yiaq")))

