(define-module (crates-io lu be) #:use-module (crates-io))

(define-public crate-lube-0.0.0 (crate (name "lube") (vers "0.0.0") (hash "1rca1s0jjbl1pg89fdjwc4799wdvs0hn9i84nrabqgcad90la38v")))

(define-public crate-lubeck-0.0.0 (crate (name "lubeck") (vers "0.0.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "17zbk4s54ik0gwm19h2xm7v85p3anckfid7908jv907z2q49qnb5") (yanked #t)))

(define-public crate-lubeck-0.0.0 (crate (name "lubeck") (vers "0.0.0-prealpha.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0cn16yyah0bagq0vhasw5f0xwjz9g0r7vfw6fnjlsjv5lha0fs7j")))

(define-public crate-lubeck-0.0.0 (crate (name "lubeck") (vers "0.0.0-prealpha.2") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1kalg50jw0pcak0ha2gdz8cwn5vww0f20gvyihyli9szqhxgv9pk")))

(define-public crate-lubeck-0.0.0 (crate (name "lubeck") (vers "0.0.0-prealpha.3") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1z166nna22gp8k53wg46kk4xmhvh2skdr29qdiyiq2w7wyx7gz6p")))

(define-public crate-lubeck-0.0.0 (crate (name "lubeck") (vers "0.0.0-prealpha.4") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0sg4qf5ggr7j98x0j0zvssxczl4mw90vz8ic3jrbz1ckhxr916x5")))

(define-public crate-lubeck-0.0.0 (crate (name "lubeck") (vers "0.0.0-prealpha.5-abandoned") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1lln1k1sim2qk5722sa0982vsxixw6hxfic1ywfpjfdd2qn5y29s")))

