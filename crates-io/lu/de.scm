(define-module (crates-io lu de) #:use-module (crates-io))

(define-public crate-lude-0.1 (crate (name "lude") (vers "0.1.0") (deps (list (crate-dep (name "containers") (req "^0.7") (kind 0)) (crate-dep (name "loca") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.2") (default-features #t) (kind 0)))) (hash "16dgl2rai2i1mnvy8qgxscwvgp4g080b4l5nnl7758l5ld9flhrb")))

(define-public crate-lude-0.0.0 (crate (name "lude") (vers "0.0.0") (hash "1ix7zgbcd4m0ij2zpw6wgl98svqc2p2d03w6zyjz36xq9sj77cj0")))

