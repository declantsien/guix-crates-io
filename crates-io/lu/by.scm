(define-module (crates-io lu by) #:use-module (crates-io))

(define-public crate-luby-0.0.1 (crate (name "luby") (vers "0.0.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "1wjzdj0c3gqdnb00xwzp2qblj84117bylvvh1mfp5363hfi0k5wn")))

