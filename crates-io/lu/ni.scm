(define-module (crates-io lu ni) #:use-module (crates-io))

(define-public crate-lunify-0.1 (crate (name "lunify") (vers "0.1.0") (deps (list (crate-dep (name "mlua") (req "^0.8") (features (quote ("lua51" "vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "12rfav9rw2zvcqi51d4iljian4n68mm67vd0mali6v2jz79vxywa") (features (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-0.1 (crate (name "lunify") (vers "0.1.1") (deps (list (crate-dep (name "mlua") (req "^0.8") (features (quote ("lua51" "vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0s8ma6abl8fxd2s34f9j71fz837hx4yffmpinad0gl7v49273dba") (features (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-0.1 (crate (name "lunify") (vers "0.1.2") (deps (list (crate-dep (name "mlua") (req "^0.8") (features (quote ("lua51" "vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0x2d51cl20jnn8g2n540zl5amn6glm71xcj2k6bjl3b9jf6ag30n") (features (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-1 (crate (name "lunify") (vers "1.0.0") (deps (list (crate-dep (name "mlua") (req "^0.8") (features (quote ("lua51" "vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1w9b0h2kyz6kc7ibwfb833b0rnmmrnk74migpydsh5im29jdgysx") (features (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-1 (crate (name "lunify") (vers "1.0.1") (deps (list (crate-dep (name "mlua") (req "^0.8") (features (quote ("lua51" "vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pgsjznavgjcdsl88xm9dlyly0iz9wwagj71bpc4x7zns8zlhpb8") (features (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunify-1 (crate (name "lunify") (vers "1.1.0") (deps (list (crate-dep (name "mlua") (req "^0.8") (features (quote ("lua51" "vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1vdacnkl0v8ivm5fjqhcdcbi2xppd8h8q762hzf1xk4m66brwcdg") (features (quote (("integration" "mlua") ("debug"))))))

(define-public crate-lunir-0.1 (crate (name "lunir") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "10n5gnq02v5n0yqy2sh0l32glpprdhk36ldbp3kmv97r2wdgjf4g") (rust-version "1.58.1")))

(define-public crate-lunir-0.2 (crate (name "lunir") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0xnv0ybi97sxm1jj6wl7260fkq2d42h67lyh6hhfbac2gfav5ydm") (rust-version "1.62.1")))

(define-public crate-lunir-lex-lua50-lua51-0.1 (crate (name "lunir-lex-lua50-lua51") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "lifering") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0jgam8kibbsmr4vf1s8rzcjhy510d56llynw4xdsxf55ls1ch3h4") (yanked #t)))

(define-public crate-lunir-lex-lua50-lua51-0.2 (crate (name "lunir-lex-lua50-lua51") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "lifering") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0ry2ivzhr6i6pjprd8a1n8naa6qmphd2bzxlzw1fq9mf5scvz2l4") (yanked #t)))

(define-public crate-lunir-lex-lua50-lua51-0.3 (crate (name "lunir-lex-lua50-lua51") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "lifering") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0b97w2nfdb5hksa946qa5cg46kx0ai2dx28l4zydkz94lhshmz1q") (yanked #t)))

