(define-module (crates-io lu a5) #:use-module (crates-io))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "09mqfmnaf0b33hpj7cr68srfjwccwd1xpbb03vh5d93s8ysyszaf")))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "0f5lj73jnc7ymfhmx84g7pk0qqbmicp3jmyhd6kg9xlcgrv2z9jb")))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "0qxish69ppnhwi0m05dnzxmqzpvwn2hiqlsq41vqh492z9xqlfkr")))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "12as9axm3m8qn6rxzaqm8iqsd459s3x60prc0s76w3ansn50h4bm")))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "1r3b0xbkxlmql7ba3jksvr7ji1195s3vfdfzp9qky9gs076axqxk")))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "12jdddxd5shph4i6d3aqwf7qbw7876bs0yv2d042iiayadfz4597")))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "1cghqxcqi27v71fjsvmsdjknsw7xwskjfsncj5h6h2bkk19bzwpj")))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.7") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "1z3wzg1sdh12mbk9rnn4wl5v82glfzjgw6f22isz0hjvchsa7wix") (yanked #t)))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.8") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "0jx508qzlcgma0ga94qq23jiaidsld2gw7xyxpd0iwna60v0wwjm") (yanked #t)))

(define-public crate-lua51-0.1 (crate (name "lua51") (vers "0.1.9") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "1pqwjf38q8gsf54vspn2a0dxk02ykb6yizrx8z84mghay599fhdj") (yanked #t)))

(define-public crate-lua51-sys-0.1 (crate (name "lua51-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.39.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1xq503j8sa4bhx1wz8s0gsdx714h6w8srpylyyv1arp9m00mfxkn") (links "lua")))

(define-public crate-lua52-sys-0.0.1 (crate (name "lua52-sys") (vers "0.0.1") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "0i31wsfz5qc8g1i20zz7sgw2rffrhynavr2rf0hwhnwcql7jirfn")))

(define-public crate-lua52-sys-0.0.2 (crate (name "lua52-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "0gz0z2j2w1hbxz37fbjwg5r3ndik7jvrpaq69c5jk74np5rdrjyq")))

(define-public crate-lua52-sys-0.0.3 (crate (name "lua52-sys") (vers "0.0.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1rbxpv02rl7sbp23mdpaw545ql56kympr2ihc45jxvyj0vy7hmdp")))

(define-public crate-lua52-sys-0.0.4 (crate (name "lua52-sys") (vers "0.0.4") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "07l7znfmxja589r3q3dcs974ifwfhf28mqkaqlbqbhsmqmkz4cmv")))

(define-public crate-lua52-sys-0.1 (crate (name "lua52-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1gn0vskyvvh9nfniqqhjs8yf1dxnli108x3qn8dcvy1brr8sdsc2")))

(define-public crate-lua52-sys-0.1 (crate (name "lua52-sys") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "11qiif81mn3ydxy5gfba0mmzay713aj8zchvz4r130icqrf87hir")))

(define-public crate-lua52-sys-0.1 (crate (name "lua52-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "151gvyw5a715j2g51cwdq8jj88znfjb8yf3xh7f5br4l7haxnlfl") (links "lua")))

(define-public crate-lua53-ext-0.1 (crate (name "lua53-ext") (vers "0.1.0") (deps (list (crate-dep (name "lua") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "1ifij5brhxmx1c4g63jaldh87w66na4549wsqzy8nv475vkzmn9a")))

(define-public crate-lua53-ext-0.1 (crate (name "lua53-ext") (vers "0.1.1") (deps (list (crate-dep (name "lua") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0rycqm4fsapv69895593azpq0m0w17ncn074n2s8fsq7818542dz")))

(define-public crate-lua53-sys-0.1 (crate (name "lua53-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1djd8mm1kpzi7p0j40c8x9lx6xsmnrlv9pc9syc09w4k8iwck1k4")))

(define-public crate-lua53-sys-0.1 (crate (name "lua53-sys") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jkwvpyxqz765irzh1w7h720cccb4n6sjm1nzmwhcz52alc9ciy7")))

(define-public crate-lua54-rs-0.1 (crate (name "lua54-rs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "1rhc5z2agr0dbpg62pjnqin8vz1n6gscqq34fvz3ardfs57r35f3") (yanked #t)))

(define-public crate-lua54-sys-0.1 (crate (name "lua54-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "1hdsmf2shrwjhz9z1v6dxk2q6k4arkqzff3gwaqa6q72492vnrhp")))

(define-public crate-lua54-sys-0.1 (crate (name "lua54-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "1wl21dcis7hvilkf3jxqhhfpmrwh4dcj72x8ygjmi9k0302hwr9j")))

