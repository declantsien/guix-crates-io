(define-module (crates-io lu do) #:use-module (crates-io))

(define-public crate-ludo-0.0.0 (crate (name "ludo") (vers "0.0.0") (hash "0gjv0mwc6g5wm3kacv4vjg0ygb88r8bd9fc5fazpb9mxpc0pkyka")))

(define-public crate-ludogame-0.1 (crate (name "ludogame") (vers "0.1.0") (hash "1am9czpqndg4sknqcnv2g1xbx6ganc1azpm4is5dqnz24q7cf69j") (yanked #t)))

(define-public crate-ludogame-0.1 (crate (name "ludogame") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1bsfy0pr32f6yvxgf648mhjzngz7snsm4a3gji8w01ibf4xf0wki") (yanked #t)))

(define-public crate-ludomath-1 (crate (name "ludomath") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1817zk62hjwmni7b9svl3fm0j1fnv1jr83jw64ls914nmz87k3hs")))

(define-public crate-ludomath-1 (crate (name "ludomath") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vfkrnlwps0br10327fvg7mfaqznc84jagyfbfcpchfrlzh09njq")))

(define-public crate-ludomath-1 (crate (name "ludomath") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0snrcrl0mnb4alysj3f3a92pjryp8f7c8kq8alk5n135scxjz8z5")))

