(define-module (crates-io lu ke) #:use-module (crates-io))

(define-public crate-luke-0.1 (crate (name "luke") (vers "0.1.0") (deps (list (crate-dep (name "openai") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xbznpfhgs8lxxh1h7g15kayqbz1vag9fb60j5wv7bbask14vx56") (yanked #t)))

(define-public crate-luke-0.1 (crate (name "luke") (vers "0.1.1") (deps (list (crate-dep (name "openai") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1i5fj5sg9l3ixvqgwcbaa1cmp8mbg4v8gih6hb83slr00mrnbcfb") (yanked #t)))

(define-public crate-luke-0.1 (crate (name "luke") (vers "0.1.2") (deps (list (crate-dep (name "openai") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0mk221p7kpa5ns34bsv4mhhzn7l2fjnnfv0sl7nmmirrx3d9v376") (yanked #t)))

(define-public crate-luke-0.1 (crate (name "luke") (vers "0.1.4") (deps (list (crate-dep (name "openai") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1g4x0jjdwbx46cj01qlyn9ppqrma04539r9wvya4sh6nzv99s2lv") (yanked #t)))

