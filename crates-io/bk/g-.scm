(define-module (crates-io bk g-) #:use-module (crates-io))

(define-public crate-bkg-cli-log-0.2 (crate (name "bkg-cli-log") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1wlw3kzmbq0bf2fcin96c5fpjlsmn0qnhiv1b61jvq3pghc9kdqr")))

