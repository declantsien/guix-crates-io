(define-module (crates-io bk up) #:use-module (crates-io))

(define-public crate-bkup-0.1 (crate (name "bkup") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04jdfyqkj0fzl2b8wg33s48x7112f7gb5sravvs4n58gzcmm882y")))

