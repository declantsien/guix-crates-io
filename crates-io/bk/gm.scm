(define-module (crates-io bk gm) #:use-module (crates-io))

(define-public crate-bkgm-0.1 (crate (name "bkgm") (vers "0.1.0") (hash "1nx37jcbm992a4p3k4ibd45zb034j8zalprxhyhjcby4rwdy9089") (yanked #t)))

(define-public crate-bkgm-0.2 (crate (name "bkgm") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)))) (hash "17qa10qnqj18m6hpmqp2y7rj3jj49why5195dnjd7yyb28gzl06f")))

(define-public crate-bkgm-0.3 (crate (name "bkgm") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)))) (hash "1j9m1ky1zr7x4gvl60s3nibvkbvggx2qzd5vzvh2f4m5skbgsgzy")))

(define-public crate-bkgm-0.3 (crate (name "bkgm") (vers "0.3.1") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)))) (hash "0i44g5nr874dnj9x5fz21jqbf43nkpf5lwq3hcpfbzqvaxbcsjps")))

