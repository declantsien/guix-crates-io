(define-module (crates-io bk tr) #:use-module (crates-io))

(define-public crate-bktree-1 (crate (name "bktree") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1a5zr6irwdsdbi9q07gm47jwanmv6p47fq0m615i6z711a7x50zq")))

(define-public crate-bktree-1 (crate (name "bktree") (vers "1.0.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "075j035k2ii5xivmg6dc9nfrbrgkcwc0k1i1jsg3nskgh52fgc8b")))

