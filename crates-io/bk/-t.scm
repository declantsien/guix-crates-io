(define-module (crates-io bk -t) #:use-module (crates-io))

(define-public crate-bk-tree-0.1 (crate (name "bk-tree") (vers "0.1.0") (hash "1j7j1pvpg95hnshkd17pva2kw5k4gzlx0mxaynags7vn8i3hf4nh")))

(define-public crate-bk-tree-0.1 (crate (name "bk-tree") (vers "0.1.1") (hash "0x7b4z0h0f0k7k3n8pm5sb2zxizc5xzr38xcl6b9d7nd2m432y17")))

(define-public crate-bk-tree-0.1 (crate (name "bk-tree") (vers "0.1.2") (hash "0rzr57dpnd80lhs5yk9isnvhcxrn9adhrfm3lqv9cpwly8gsvxn6")))

(define-public crate-bk-tree-0.1 (crate (name "bk-tree") (vers "0.1.3") (hash "16i2ag9z4r8mmvakc18mbndi6sv72f4y77adhdv3lbx9xrh18cji")))

(define-public crate-bk-tree-0.1 (crate (name "bk-tree") (vers "0.1.4") (hash "027glnwb4i2xr7y5qq2hks28p14k6rchh88kjh71kdvxl7ka9iv8")))

(define-public crate-bk-tree-0.1 (crate (name "bk-tree") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1scplzirk0612sk37wj7f209pywwja495b557li2vdjj8npsw1kb")))

(define-public crate-bk-tree-0.1 (crate (name "bk-tree") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1zbq53pzbh4pdzn774qb2hfpsvxhj9nh8nrnr58ic7czi611z9n0")))

(define-public crate-bk-tree-0.2 (crate (name "bk-tree") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0pkncz3a81y4c6ywqqngpsng4g1gh0bv0gc7iyjpxjm3szfriz6v")))

(define-public crate-bk-tree-0.3 (crate (name "bk-tree") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1125hlh3lafi077z1kxdzxdrba0q0adf658l6ml8dpn6lag0722l")))

(define-public crate-bk-tree-0.4 (crate (name "bk-tree") (vers "0.4.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "triple_accel") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0gzg40a6q7fs9dxvhbhvl911bvf2wxb1sqx2bixp2kg90zhzc8b1") (features (quote (("enable-fnv" "fnv") ("default" "enable-fnv"))))))

(define-public crate-bk-tree-0.5 (crate (name "bk-tree") (vers "0.5.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "triple_accel") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ihmq13ws2f9m58ag3p4jri9ad7zdbx7wlmwz0c3k1sbwsw3ya58") (features (quote (("enable-fnv" "fnv") ("default" "enable-fnv")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

