(define-module (crates-io bk #{2d}#) #:use-module (crates-io))

(define-public crate-bk2d-0.1 (crate (name "bk2d") (vers "0.1.0") (deps (list (crate-dep (name "bk2d-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xg3xdlqqgx0l6vy8h4ab3s9hwqkqsgqcb2hqjciflpmx77sl690")))

(define-public crate-bk2d-0.2 (crate (name "bk2d") (vers "0.2.0") (deps (list (crate-dep (name "bk2d-macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02l9i66bcclpqfwbrfj1h34vx70qs4wx7mrrsng2s043gw9dx7ck")))

(define-public crate-bk2d-0.3 (crate (name "bk2d") (vers "0.3.0") (deps (list (crate-dep (name "bk2d-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "00457ybvnrn2gyvjfw8ffy6vcsay6za6nvr83sf69mdyvff8ix99")))

(define-public crate-bk2d-macro-0.1 (crate (name "bk2d-macro") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1qw1vcpql60mn8p0kxzzwjh2wam2wjg9aadb6vnz7y15i9wyn6q6")))

(define-public crate-bk2d-macro-0.2 (crate (name "bk2d-macro") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0ij4rhylsvgsd8j6cnnd1blsfrgn9cjba9kmdfwqwsbrm1l0m8ll")))

(define-public crate-bk2d-macro-0.2 (crate (name "bk2d-macro") (vers "0.2.1") (deps (list (crate-dep (name "darling") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0v09s7dhdanh6xp1w7aagra38c17h8xc4fhl2mkjkykp3zl7szqs")))

(define-public crate-bk2d-macro-0.3 (crate (name "bk2d-macro") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "041vcbffjl2p0xzc7lb72xcz15ahawxpmvsyzj3alzi9ap310im6")))

(define-public crate-bk2d-mode-0.3 (crate (name "bk2d-mode") (vers "0.3.0") (deps (list (crate-dep (name "bk2d") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1k332jhnry6qjjmzd5kjmgvnqqf4z63f6wjy4inabiy158gq24vn")))

