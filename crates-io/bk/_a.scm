(define-module (crates-io bk _a) #:use-module (crates-io))

(define-public crate-bk_allocator-0.1 (crate (name "bk_allocator") (vers "0.1.0") (deps (list (crate-dep (name "bk_primitives") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1h5f3mgkskycx8l5102vsv99czcz0b4nd49v9vk9cgfgjlgclhcj")))

(define-public crate-bk_allocator-0.2 (crate (name "bk_allocator") (vers "0.2.0") (deps (list (crate-dep (name "bk_primitives") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mqc6f2acj18vw6xsf8wkzwd37pdqr7kfxhizlcl7k9fzawzkgf6") (yanked #t)))

