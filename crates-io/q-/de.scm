(define-module (crates-io q- de) #:use-module (crates-io))

(define-public crate-q-debug-0.1 (crate (name "q-debug") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 2)))) (hash "14h0br5hhcvqaak9npivb3k17faxy5h67xlrsm4b8qxq0afizag6") (yanked #t)))

(define-public crate-q-debug-0.1 (crate (name "q-debug") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 2)))) (hash "1yq3p0vlb2a78ibp2a297g5zi0lkd2phfgj6khvh37v2z03ppjp7")))

