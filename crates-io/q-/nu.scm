(define-module (crates-io q- nu) #:use-module (crates-io))

(define-public crate-q-num-0.1 (crate (name "q-num") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (default-features #t) (kind 0)))) (hash "1ypgc4sx7m7jfr53c3sbyam6ffcnqqygsfxfnr8k4sp5s4n3f13n")))

(define-public crate-q-num-0.1 (crate (name "q-num") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (default-features #t) (kind 0)))) (hash "03s1gcm46d898dnqaf9w2dm3s1y8vbvma6nljjsxgyamx49mlmn5")))

