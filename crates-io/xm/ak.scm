(define-module (crates-io xm ak) #:use-module (crates-io))

(define-public crate-xmake-0.1 (crate (name "xmake") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "1l01lyzsbd8jn1ra1633hjfgsp85q84bigb3yrgr2rjzjbs7w6my")))

(define-public crate-xmake-0.2 (crate (name "xmake") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "0aicglyglh70a1kcbqiyqad4lcswbxgwq0gsv0nzy6sf3gj8c9dv")))

(define-public crate-xmake-0.2 (crate (name "xmake") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "0pg4hwqf6zvxkywalaxdy4q2pxwqqc6b236d424hi842p27s522m")))

