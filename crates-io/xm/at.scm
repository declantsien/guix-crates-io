(define-module (crates-io xm at) #:use-module (crates-io))

(define-public crate-xmath-0.1 (crate (name "xmath") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "*") (kind 0)))) (hash "0vcyzkikwkvvjiy0cyiq70v8ifkj35n5wibcyp6hd2binrxi2xam")))

(define-public crate-xmath-0.1 (crate (name "xmath") (vers "0.1.1") (deps (list (crate-dep (name "glium") (req "^0.7") (kind 0)))) (hash "122ldfd6pqaiycn7amkjm12l28jk2gm9zd904wdznkjplindi8jz")))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.0") (deps (list (crate-dep (name "glium") (req "^0.7") (kind 0)))) (hash "1i071nw78q4l09kixhgvk198pqj5ar48dc78jlslh2m8lf0h1y39")))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.1") (deps (list (crate-dep (name "glium") (req "^0.8.2") (kind 0)))) (hash "0mvxc8qgd55kzxgygjpy6h7ph9qli2dhka9fc04ym4dfjmg30qxm")))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.2") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)))) (hash "0ylm1c4g5gix1j6c8a722r3da1rb53iaq4lp1wc2yx59n1qlh2xy") (features (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.3") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)))) (hash "0x92ixz5khhjw5z2c72i2iwnzgq1w15r5x9wvv6cpqgxvzq1frf0") (features (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.4") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)))) (hash "1yqf5g7z176l8mw3mgzsgj9gwc5mjbq9iy2piiswv7g3bja6m0xd") (features (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.5") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)))) (hash "0p4al3q84136radjbjkkfs90plr08lzxhlwz4ynajhnpa79w7z0r") (features (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.6") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)))) (hash "0z9ymi6mfympfb5ylj93gvkxj5n974nw3py3m7qxfmq4lwl4yzrm") (features (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.7") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)))) (hash "1v1m9ckmpcr7pilkv0n45ii7wjvm7apjcn1m88nj26c33zsq9ajx") (features (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.8") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)))) (hash "0123wd3ja1l6rwn876kl24kdkmavdcssm8zcf0h47av8inky4w4g") (features (quote (("glium-support" "glium"))))))

(define-public crate-xmath-0.2 (crate (name "xmath") (vers "0.2.9") (deps (list (crate-dep (name "glium") (req ">=0.14.0, <0.33.0") (optional #t) (kind 0)))) (hash "1rbkwbsr9lwgpjhkbdyz6vl1p808r0cy7n5bn9rn931d68gy9p0b") (features (quote (("glium-support" "glium"))))))

