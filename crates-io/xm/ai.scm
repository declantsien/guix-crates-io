(define-module (crates-io xm ai) #:use-module (crates-io))

(define-public crate-xmail-0.1 (crate (name "xmail") (vers "0.1.1") (deps (list (crate-dep (name "punycode") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xtld") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "010yzixgjh7ia6gfvxf11a9hq69qpxqiiz31h0f1vhcaj5sr9xqk")))

(define-public crate-xmail-0.1 (crate (name "xmail") (vers "0.1.2") (deps (list (crate-dep (name "punycode") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xtld") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0kj2drh5b753bqq5w64c7jiy0iwc7wjw9jix7ws13qxry0bw22fl")))

(define-public crate-xmail-0.1 (crate (name "xmail") (vers "0.1.3") (deps (list (crate-dep (name "punycode") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xtld") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ylcbr4isrnbj4mm7b2p7d4k533pbkb0q498xyvydv7mld9cjvh0")))

(define-public crate-xmail-0.1 (crate (name "xmail") (vers "0.1.4") (deps (list (crate-dep (name "punycode") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "xtld") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cnvxk6avjnw71yz3xs5jbwadrf377xbv1fywdzr93azrjj128b5")))

(define-public crate-xmail-0.1 (crate (name "xmail") (vers "0.1.5") (deps (list (crate-dep (name "punycode") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "xtld") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ni2nm9zx5py5dcbw6qsc8l008nbb897z687rxqmszxwr51sqq12")))

(define-public crate-xmail-0.1 (crate (name "xmail") (vers "0.1.6") (deps (list (crate-dep (name "punycode") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "xtld") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0hlil2l5lj09wmkjmvmcs7s6iazd69r6iwrmbda3s1i6sx34dryd")))

(define-public crate-xmail-0.1 (crate (name "xmail") (vers "0.1.8") (deps (list (crate-dep (name "punycode") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "xtld") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0i9nch2h7pmbkrgk4zclgrbkizjzx6d8j7l140f1h03329nqiiai")))

