(define-module (crates-io xm as) #:use-module (crates-io))

(define-public crate-xmas-elf-0.1 (crate (name "xmas-elf") (vers "0.1.0") (deps (list (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ifxndlbvfczh2zf0wnf6v4n7p3rmy2px8kkjm47d09jvff4i3cn")))

(define-public crate-xmas-elf-0.2 (crate (name "xmas-elf") (vers "0.2.0") (deps (list (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "0acdmgsph1yw3k5nziqvvi2qw2hl1bm995bmkakaw1lgwlxj5ml7")))

(define-public crate-xmas-elf-0.3 (crate (name "xmas-elf") (vers "0.3.0") (deps (list (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kjxkyp99pcqvyvi8wxjmk3idhs9a2ba82w810qwpx7y001zm6vm")))

(define-public crate-xmas-elf-0.4 (crate (name "xmas-elf") (vers "0.4.0") (deps (list (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h30g926plm63y0zn2ssbh3ln2wjkisdrj5szs427i9anj9jmjak")))

(define-public crate-xmas-elf-0.5 (crate (name "xmas-elf") (vers "0.5.0") (deps (list (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qhqpk6ycckrwrakwvdc2x2vkfqn14g1wr2g8f9vyby0aklz6vdz")))

(define-public crate-xmas-elf-0.6 (crate (name "xmas-elf") (vers "0.6.0") (deps (list (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kx61ca5ik69j6gxw6c0jd4grk0bwgr1azhsk58g840fapgqv12m")))

(define-public crate-xmas-elf-0.6 (crate (name "xmas-elf") (vers "0.6.1") (deps (list (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hcas1bm5d5kidf20fn0ddpdrlrbsz61631cg300b7dwm3idvxg7")))

(define-public crate-xmas-elf-0.6 (crate (name "xmas-elf") (vers "0.6.2") (deps (list (crate-dep (name "flate2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "0n6gsiq25kdbwxsmxaznxnadfcii1jgsd7b0blg8svknvzsqsrr2") (features (quote (("compression" "flate2"))))))

(define-public crate-xmas-elf-0.7 (crate (name "xmas-elf") (vers "0.7.0") (deps (list (crate-dep (name "flate2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "04az81f0c0mjzi5ddcqlz8hkk514mkcp3cx6bx08razncsiyjkg7") (features (quote (("compression" "flate2"))))))

(define-public crate-xmas-elf-0.8 (crate (name "xmas-elf") (vers "0.8.0") (deps (list (crate-dep (name "flate2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nidhcaa5ghs8sh2vdcs678263b0lx0sjys4fx7fpb5ywzcb8acd") (features (quote (("compression" "flate2"))))))

(define-public crate-xmas-elf-0.9 (crate (name "xmas-elf") (vers "0.9.0") (deps (list (crate-dep (name "flate2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "1py2kxbylprpf5xx8mfwkv2h31gj90a21rvwkppjxcv5gmvcq87q") (features (quote (("compression" "flate2"))))))

(define-public crate-xmas-elf-0.9 (crate (name "xmas-elf") (vers "0.9.1") (deps (list (crate-dep (name "flate2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1") (default-features #t) (kind 0)))) (hash "1inias7h1cv4zh3szk46byiqhnzm5zc7658q1brzfhl3wwbrii22") (features (quote (("compression" "flate2"))))))

