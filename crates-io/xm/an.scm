(define-module (crates-io xm an) #:use-module (crates-io))

(define-public crate-xman-0.1 (crate (name "xman") (vers "0.1.0") (hash "0nygvmdch47mrxkjfcy9akdf28vwmxaskhlmy9ic1xsafb2rmiq9")))

(define-public crate-xman-1 (crate (name "xman") (vers "1.0.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "soup") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1sg3ssbnnvwb38kxh9blf6w82vavr8xiv6gi62366bwqk0xafd7i")))

(define-public crate-xman-1 (crate (name "xman") (vers "1.0.1") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "soup") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yi0rbvnh2wry5bgazdhrdqig0b7d7pcqi6nk3ibnnkl90pyhgfq")))

(define-public crate-xman-1 (crate (name "xman") (vers "1.0.2") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "soup") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cikwziq5ca5n9z4f7k3v497115046ay8l0s6skdwb0r5qfgw6vh")))

(define-public crate-xman-1 (crate (name "xman") (vers "1.0.3") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "soup") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0avfkzykjwmv0nr7wzjhykpx9kbiqk8gksmyys203is5d0gwrxn0")))

