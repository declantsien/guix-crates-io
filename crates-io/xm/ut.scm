(define-module (crates-io xm ut) #:use-module (crates-io))

(define-public crate-xmutil-1 (crate (name "xmutil") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v4r1rg8vyr9wq2wis9p56glabwia2ax4sszga1c422xx4254fjy")))

(define-public crate-xmutil-1 (crate (name "xmutil") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bsp7vl5x8y0h8dqs92a5gv8apprycdcw9irqra914pmgriqjkv7")))

(define-public crate-xmutil-1 (crate (name "xmutil") (vers "1.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "01dw84hi8jq4xla9sgh76ha6x7rfaj8z91ibc050931w4bvk5jxr")))

