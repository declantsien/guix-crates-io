(define-module (crates-io xm l-) #:use-module (crates-io))

(define-public crate-xml-attributes-derive-0.1 (crate (name "xml-attributes-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~0.12") (default-features #t) (kind 0)))) (hash "06iihsrzwrnr62fk5j8h8zcfn6ii2qvw1rqsk5m1crsj730nmzik")))

(define-public crate-xml-attributes-derive-0.1 (crate (name "xml-attributes-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~0.12") (default-features #t) (kind 0)))) (hash "128zj2fwi0101wb6vr5si3mkjpknci8wrp8i7m1iw2q33sdqjxqy")))

(define-public crate-xml-builder-0.1 (crate (name "xml-builder") (vers "0.1.0") (hash "10dyq1kwjmmylmj4z47ydg1c28ky3da7nvy0l6c5903y5jk9bk3c")))

(define-public crate-xml-builder-0.1 (crate (name "xml-builder") (vers "0.1.1") (hash "07wsm9x1lg50zsbnamyyra31p4jfkxg8mz0cgfwaqrcg8w54l2l9")))

(define-public crate-xml-builder-0.1 (crate (name "xml-builder") (vers "0.1.2") (hash "0iv8cjh16gizh72wya3rcpjsp1awxikx16a90ksrxbyfrs8p1l6i")))

(define-public crate-xml-builder-0.1 (crate (name "xml-builder") (vers "0.1.3") (hash "0mrpzpyxjz2ss6lh7inl7jz89z9gp9r6h3mm1xwxgbfybsixycsc")))

(define-public crate-xml-builder-0.1 (crate (name "xml-builder") (vers "0.1.4") (hash "07gssjw1q3bl5agrrs0lygbddbc48y181iass2niw0v03qrc6x0z")))

(define-public crate-xml-builder-0.2 (crate (name "xml-builder") (vers "0.2.0") (hash "010a84v4gdq5s73nc1ksb6rdj1ir35j5wdfbgff6dvs6rfvvczyh")))

(define-public crate-xml-builder-0.2 (crate (name "xml-builder") (vers "0.2.1") (hash "1dmcylfsbd7mlpnqwbzpgsi17jbplkdagsyfxmswwih7kcnb30kh")))

(define-public crate-xml-builder-0.2 (crate (name "xml-builder") (vers "0.2.2") (hash "0n4gaiifyfzvmxbr1gbzw3000n8k5jq4xfg09vjl0zhrdv7bzsnd")))

(define-public crate-xml-builder-0.2 (crate (name "xml-builder") (vers "0.2.3") (hash "0nw76gs8kp30fpfgsk92f0hfpywpp1fwnyj88pdjmqlyn5k3j45a")))

(define-public crate-xml-builder-0.3 (crate (name "xml-builder") (vers "0.3.0") (hash "0jy18ppz2f541mmshmpz7gh64aq2m62cw3cimhydk4crwxx8wa1r")))

(define-public crate-xml-builder-0.4 (crate (name "xml-builder") (vers "0.4.0") (hash "0lpxgk88dlaqnvk7z2155ijh5znjm48hj6wql8rd1prjmimv1171")))

(define-public crate-xml-builder-0.5 (crate (name "xml-builder") (vers "0.5.0") (hash "1v31rzvjy4rm5iirbmiss5j5g9995440ggfwss6x64lcxgmkxzva")))

(define-public crate-xml-builder-0.5 (crate (name "xml-builder") (vers "0.5.1") (hash "1cq2rr2gsrj82f1qkq8hy4hc41v7phqk085p7vxs84zliqzmmdyc")))

(define-public crate-xml-builder-0.5 (crate (name "xml-builder") (vers "0.5.2") (hash "0frkylldh541r6sn082h46p1aiga90v87isn83y0v07pdalg3i7g")))

(define-public crate-xml-conformance-rs-0.0.0 (crate (name "xml-conformance-rs") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.26.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "15kqijhx8dqfir1nncksm5x3yaswc0vjk8sqvq6wwl8qnxawhi13")))

(define-public crate-xml-creator-0.1 (crate (name "xml-creator") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1jmx86p41dj8bcizx8zfwshl8yvf35xp8ca5vhi6bqykcphv6vmk") (yanked #t)))

(define-public crate-xml-creator-0.1 (crate (name "xml-creator") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1biv4pj2v1x8n09ylcv7k0pd5qd16cl51787pshqqrkjfixhjiaw")))

(define-public crate-xml-data-0.0.1 (crate (name "xml-data") (vers "0.0.1") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (features (quote ("encoding"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "xml-data-derive") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1gw6q04s120vrpd7kj12yah42ydgfbk8a1amdja9w557vg4fbmgp") (features (quote (("derive" "xml-data-derive") ("default" "derive") ("_private-test"))))))

(define-public crate-xml-data-derive-0.0.1 (crate (name "xml-data-derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1bpn4vasigqaa384csbfzpy1jwd3mifd729c22cjh7szigp2h3zj")))

(define-public crate-xml-doc-0.1 (crate (name "xml-doc") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)))) (hash "1qik2r1lf96f2ncqwjr7mcmyvv3zx3ndjrml6ifbwz4spxcmb7li")))

(define-public crate-xml-doc-0.1 (crate (name "xml-doc") (vers "0.1.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)))) (hash "1chfh56nf8jjwn0vn0qgcxy2ns5l3dp942dczvdmwr719iynwicl")))

(define-public crate-xml-doc-0.2 (crate (name "xml-doc") (vers "0.2.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)))) (hash "0p16lx5sh04vszj3gy7dybghyjj5bx5a5qinfwajvd4v6nxgp98w")))

(define-public crate-xml-doc-log4rs-0.2 (crate (name "xml-doc-log4rs") (vers "0.2.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log4rs-macros") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)))) (hash "19kw882n1v3z574gv2vrdbk27pppcml056js5r8wc7n9ndmcm5fd")))

(define-public crate-xml-lib-0.0.1 (crate (name "xml-lib") (vers "0.0.1") (hash "0lbdya4fn1npp7jz3l18wfy8x9zsslbsjqky3sq3yx2pcgpcld5c")))

(define-public crate-xml-lib-0.0.2 (crate (name "xml-lib") (vers "0.0.2") (hash "0bfgj9h41v7dsbdxjn1fzc49xslqbjnr219kfl92mfpsrp4lcz99")))

(define-public crate-xml-lib-0.0.3 (crate (name "xml-lib") (vers "0.0.3") (hash "1h3vbdndsyg4npblrbmy23zi82d2i5wpybfk9h2rf220ds4c5zal")))

(define-public crate-xml-no-std-0.8 (crate (name "xml-no-std") (vers "0.8.19") (hash "167ng0mpsqc7xd53c9kxr4w00ag9mzyw01n151zbb84yfwf4akxc") (rust-version "1.58")))

(define-public crate-xml-paths-0.1 (crate (name "xml-paths") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0jps102bbjr8xyb6mql73djp4il6i3nc1jz3ddnsanfcn3cydmgj")))

(define-public crate-xml-paths-0.1 (crate (name "xml-paths") (vers "0.1.1") (deps (list (crate-dep (name "quick-xml") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0pc18jrm7zp7zpp8mqlmmnqzsliv3wcyxd83a7bqy3pam516hw1f")))

(define-public crate-xml-paths-0.1 (crate (name "xml-paths") (vers "0.1.2") (deps (list (crate-dep (name "quick-xml") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1z33qq13zn94qrsrbxsgkr68kwspb1j61hffyj0k51b6rh4abd32")))

(define-public crate-xml-pretty-0.1 (crate (name "xml-pretty") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmlem") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a07wlmpqx9k49wlxhrkr5nxmjcyrykzcnpkxymw3j7ycsmm2ssj")))

(define-public crate-xml-pretty-0.2 (crate (name "xml-pretty") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmlem") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g7fj65aq16r8x35xl1j29m9yfrqjyqjwd2w691mcc7h6bk1bg0r")))

(define-public crate-xml-pretty-0.2 (crate (name "xml-pretty") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmlem") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0kjlkck70pa8qm27lndqi90pcz1fd4gi4hpwx695lbw0qd0kv3z5")))

(define-public crate-xml-pretty-0.2 (crate (name "xml-pretty") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmlem") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1f8gs0nf63lrzaxhmf1b4zxs3bx4zc9az6smk35n815j2km4w3iw")))

(define-public crate-xml-pretty-0.2 (crate (name "xml-pretty") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmlem") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wbqjjdgdz852rlv0d3awzpjndrcivgrcshfzb918i2s6cpjzgrj")))

(define-public crate-xml-rpc-0.0.1 (crate (name "xml-rpc") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "08xirl6iikrhmh8yb55h4l9qzr7y2m9k8rak9d8bm5iha97jkih2")))

(define-public crate-xml-rpc-0.0.2 (crate (name "xml-rpc") (vers "0.0.2") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0blz8wdrsylryapp3d5kl9r34sr95r5a350bwf8n4qm3vpn16g47")))

(define-public crate-xml-rpc-0.0.3 (crate (name "xml-rpc") (vers "0.0.3") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1pjb4vvfkk78hr029cv4vkipi4inwspi82wvczq4y93jsgiy82kd")))

(define-public crate-xml-rpc-0.0.4 (crate (name "xml-rpc") (vers "0.0.4") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "19fsmrkx2dcc74x974i1ilm8sj9zi5hsh0gqvl3qb3znnwalsr0j")))

(define-public crate-xml-rpc-0.0.5 (crate (name "xml-rpc") (vers "0.0.5") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "12fa5xy3ycpal3wfsx5dsing8hlhb6w1zhl7mrqd98vagm0haxjf")))

(define-public crate-xml-rpc-0.0.6 (crate (name "xml-rpc") (vers "0.0.6") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0blgvxk5rkp9nf6k1bdb02hgng3bzc0vzg3wyikp9vv7y5w42gh4")))

(define-public crate-xml-rpc-0.0.7 (crate (name "xml-rpc") (vers "0.0.7") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1gsb0sjnhhvlwg2kma9ba2g0x1g7lqay8gx0ky2ykw96k5bk6aln")))

(define-public crate-xml-rpc-0.0.8 (crate (name "xml-rpc") (vers "0.0.8") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1mfh54qp767xiqma2d897ci3n7ahcd91bjmznpva7kk7izsrzpj5")))

(define-public crate-xml-rpc-0.0.9 (crate (name "xml-rpc") (vers "0.0.9") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "hyper10") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "11472h0baa22kk88jwyzsjljkz48plp5dvhiw38zd0rzasm321vd")))

(define-public crate-xml-rpc-0.0.10 (crate (name "xml-rpc") (vers "0.0.10") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "hyper10") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1xwgs2iqbwkbp8jpxaybsdp5clhmvclga0nv16xnrawrbg1fwlap")))

(define-public crate-xml-rpc-0.0.11 (crate (name "xml-rpc") (vers "0.0.11") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.15") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rouille") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1hchqyxqayav372nqwv614pmi5dr3c21md94d7lmh07hbw6ggpkk")))

(define-public crate-xml-rpc-0.0.12 (crate (name "xml-rpc") (vers "0.0.12") (deps (list (crate-dep (name "base64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.15") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rouille") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "18h0bvx83jmqqf2n8gizvmi4cwyd7dk4avrcjsj6k994285yqr1p")))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.2") (hash "0wq0yj2dsr1ppc7sl14q8cwbkaji6j6cx1bphqsjzdanf7dg3lgj") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.3") (hash "1qv9y60mp6gcw457jq6fwhkh3pw1cg0h08gns9cilmw75d8h30ad") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.4") (hash "16dimxp8dk1s9qh20agzk8b9vwr6dfkrlq57wsxs3j7p6q7xzda7") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.5") (hash "0d5y8wn586lw4pw5z740wnr8z2pcgrhk1977nldnjsl8qra32x2q") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.6") (hash "0q8x3nw89phnjppah7xj0hpzdk1cifgrc6v8cjjm5v9za148ir1y") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.7") (hash "112j0fcvwcswli9wqh1ymqay3qz0fzlpw80hrpk57n0sja4mr08l") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.8") (hash "1s1zm1dy5j7j2dpcl595wjm834dgjgkc71s8lww7cgx6lvji5q8h") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.9") (hash "1vl0rl1fxhpp2cg7bxqsy4jzac9qal2wv92csjcxp0r3psy2li8v") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.10") (hash "0hbl6qxqqwnsl7pyw5sajxc7r9g6ksgwx526xzm08hdlfbca3qjc") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.11") (hash "0vbxcb9i4gswbs8x89675qa12d8rmy17d25azmhsya1f9b3zys3k") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.12") (hash "1ah763nwn74kgz094bywm4nh70n54bwk0fr0j8wgi1apg0mi8qp9") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.13") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "0y0bl5x34pad2097imvbsfdc4xz51m1ss1djbh4053yzwc7xm58h") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.14") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "1gg8wia7akmz34kr5xj4qyx6mq9rnqqnapz9pni6dsg29xich9bx") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.15") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "077qbizrj3159bm85y27d9hvgiyhlnzqw99y9sygbf4llfnm104w") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.16") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "0zghacg0cyzaxa755757w4xlrl0bvjap9z831r192a14r13dpdfv") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.17") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "05b9rb3k68rw7cal8s4sbfkdd945b1738qxd5qknlx8vi03wgqvx") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.18") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "090fhvbg5xy9sfyd8cznzcgabnhspiivnsgcq78xqn5pagpnbnd5") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.19") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "0nwzz2xkmpnmc2p376f6mhb248jb9m5cga09ss3sd6v02gc827pq") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.20") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "0k9ivskl11m89d5j15sx05r3h7parvj99pxv01sdwkqflmmvc9ys") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.21") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "136fi97ici6fn52c60zqaqv6v4js3vrqls64c712z82xqvrcplxh") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.22") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "1qwy8ry7c8ahbyy49s82bax097x8nwrr2naqvwwv0x5brrcpd8l7") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.23") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "14biy5jn982lvmxrayz3iqhqh3hxqg631mr8iczhx0s9gws9k3wi") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.24") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "14cwd720wcwq5p4bxq4s4iad807bnhvkkcnrglxjry2yygys1p84") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.25") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "1mvj28zca9pziiqcycybmhdnrbxqczmfyljg1fayq4lmcj21l6j4") (yanked #t)))

(define-public crate-xml-rs-0.1 (crate (name "xml-rs") (vers "0.1.26") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "068mcrpww6x7mlxcwr80hfhmr2vd6fgv59sanp9jvnr45gc8zb2b") (yanked #t)))

(define-public crate-xml-rs-0.2 (crate (name "xml-rs") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "10yxbs3z5gywjrsy0dsds51gm9ilg3rbqv6hkh32aacahqifn463") (yanked #t)))

(define-public crate-xml-rs-0.2 (crate (name "xml-rs") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)))) (hash "01llrgminsx6qiyj1lw469rc4wkyhdpyzdsjkizh37bfd25lzysj") (yanked #t)))

(define-public crate-xml-rs-0.2 (crate (name "xml-rs") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "1gwy8hny2643j2yxpkcs3n4gxbfhxk72wgz06cfxf0ps6kwk8z0d")))

(define-public crate-xml-rs-0.2 (crate (name "xml-rs") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "007qnvazx6yrld2pl02666a5683vysggyhwrhq88zsi7q66g5c9m") (yanked #t)))

(define-public crate-xml-rs-0.2 (crate (name "xml-rs") (vers "0.2.4") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "02vagvn411nxi1kzbf8nn7gy2hjmxxfhnqwg2ls05j039y7j2zpf") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "0b5mkvbipc80z17fkkwsc57a0f81nwxg1sjvflw6ml2m9s3gf7pi") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^0.6") (default-features #t) (kind 0)))) (hash "15fxwcm3j8rzqksnj731785mfnw0w0lcn35ycr4akv1mbq15wjiz") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^0.6") (default-features #t) (kind 0)))) (hash "0vmj7xpb0kzlbl747lc3glrghigwli43c3v5pypdy1k4mqa10fxh") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.3") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)))) (hash "1s5wldniv726ril30cwl3wlrv2cfyp90ckccnv927lgq0gk2jbr8") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.4") (deps (list (crate-dep (name "bitflags") (req ">= 0.5, < 0.8") (default-features #t) (kind 0)))) (hash "1vz3ay999h3n0cx11vkfwh4zygc4vyk0v60aqyfj0y9ipnb4prv5") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.5") (deps (list (crate-dep (name "bitflags") (req ">= 0.5, < 0.8") (default-features #t) (kind 0)))) (hash "1irf2djlihkj2r6xp39dk5dlc244vkwfg3p92nfxaav92bnmxcgj") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.6") (deps (list (crate-dep (name "bitflags") (req ">= 0.5, < 0.8") (default-features #t) (kind 0)))) (hash "0qmm2nss16b0f46fp30s2ka8k50a5i03jlp36672qf38magc7iky")))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.7") (deps (list (crate-dep (name "bitflags") (req ">= 0.5, < 0.8") (default-features #t) (kind 0)))) (hash "0jyskdxfvyvn6mnbcbhskvhaa8mqprpkbklk0nsi5bxfjrvsihv3") (yanked #t)))

(define-public crate-xml-rs-0.3 (crate (name "xml-rs") (vers "0.3.8") (deps (list (crate-dep (name "bitflags") (req ">= 0.5, < 0.8") (default-features #t) (kind 0)))) (hash "0fzyr8892g9zry6f17pnfdaxlsaqmyx64wbxgagldik0haln94kj") (yanked #t)))

(define-public crate-xml-rs-0.4 (crate (name "xml-rs") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req ">= 0.5, < 0.8") (default-features #t) (kind 0)))) (hash "1fkc22xzmg2cdv9cg3rg1ypqdygcz4rcx1bw9k6awl7mw272w0l7") (yanked #t)))

(define-public crate-xml-rs-0.4 (crate (name "xml-rs") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req ">= 0.5, < 0.8") (default-features #t) (kind 0)))) (hash "0jmy0xp4f7rgdglcjd8i8vf64v2is942630pl449qrkspa4ycvml")))

(define-public crate-xml-rs-0.5 (crate (name "xml-rs") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)))) (hash "00zmrjxrc006nyshanffgzy175y74kc62j67s46hz8kh1akdgccd") (yanked #t)))

(define-public crate-xml-rs-0.6 (crate (name "xml-rs") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)))) (hash "03hlirrqinv3gsy67q8i5ja26anyqhrn1nzfm6h14hc0bg9r133n") (yanked #t)))

(define-public crate-xml-rs-0.6 (crate (name "xml-rs") (vers "0.6.1") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)))) (hash "08a6lydyf3cmknicf0hnqdwyz5i4hfq20rcpswhig5bbw495x571")))

(define-public crate-xml-rs-0.6 (crate (name "xml-rs") (vers "0.6.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)))) (hash "18g7krn8zx8ifml83w91w2hvw437j5q3vaw4cvx3kryccj5860pl") (yanked #t)))

(define-public crate-xml-rs-0.7 (crate (name "xml-rs") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hp9kf80y9qm3aiqg5psyshqfkcrjgifbcm2c2nc5qlzs80vc71w")))

(define-public crate-xml-rs-0.7 (crate (name "xml-rs") (vers "0.7.1") (hash "1wv7izl41jf3ylhqhw23y1h0m729v2g5k4mgfw72v4kmgvvawiin") (yanked #t)))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.0") (hash "1db4v716rbpgjiasaim2s17rmvsfcq1qzwg6nji6mdf5k34i46sl")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "0c905wsmk995xypxljpxzq6vv660r1pzgyrpsfiz13kw3hf0dzcs")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "08vwh7nqdrzkx3d62qnm4i85aidk641h63664j4ypfqv89f6xdrb")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "12ndxyhzxw2zdr76ql8nfdwb2vwhvdkrxwk4pbjafqfglmjv0zdh")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.4") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "18q048wk3jafgl59sa2m0qv4vk2sqkfcya4kznc5rxqkhsad7myj")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.5") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "0wfm4ma4lbl1b6y6ajr20b8xixbxixac0k8904swwdkcnfgn0jrp")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.6") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "0amfgmph76q3p43vwy3apmgbg39la5sri5fvn86zn797dq7adqa0") (rust-version "1.63")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.7") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "18nxpxqhqbhxncii03gf56hvhyf4s3icdmlks3v7lznxph2037b9") (rust-version "1.63")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.8") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "03pqnq10s0b2pwh91gbbi9ba143jgjrxnl0xi7ngdzni5d7g282g") (rust-version "1.63")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.9") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "1cwffnrl2n5rh487r7qn4vkhc9gyrhdqcjys7b1xk2vynrdw9z61") (rust-version "1.63")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.10") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "1lpq7c9pxcdjiz1ib171k6f5hdgscgwl9asspvckqm2gl97a15fw") (rust-version "1.63")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.11") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "008b8aig9k5j24w03c5kax43l1331hss574v48jmbadza2am340n") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.12") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "1s8dpi71wakzm6d3kbsrsiv9f9bihpgh21qbznfw0sxgbwmm9qpv") (yanked #t) (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.13") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bcjvrygagza3gk8khah2i5khwc0c1071xicd87b6dvaw453i3rd") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.14") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "072lwmk6y84gha04rr13z44y9daq3yfh6kgs7vv8wfh8274rv0sj") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.15-cvss-cries-wolf") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "055vws7nwq86mj4j9g9096ahbl5ivpx98kwzx9rhyiailsr9z5g0") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.15") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "0dlkdx2xlfa1q8ksx4k8dcspbskd1w6gg4hwsap5hhndii5chmjs") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.16") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "18azkgi4r91a39dqbp172dzcbby3gdb42avmxv69km5mlyc0jhs7") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.17") (hash "0i04nb24snjhm5c4c0x8ys9x8gw37niak0bkincwzrvbjbsnpvhy") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.18") (hash "02jgfy3l25kx5lph8264lcflsfzls1yfwb0z8gd97vhannbpxdxs") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.19") (hash "0nnpvk3fv32hgh7vs9gbg2swmzxx5yz73f4b7rak7q39q2x9rjqg") (rust-version "1.58")))

(define-public crate-xml-rs-0.8 (crate (name "xml-rs") (vers "0.8.20") (hash "14s1czpj83zhgr4pizxa4j07layw9wmlqhkq0k3wz5q5ixwph6br") (rust-version "1.58")))

(define-public crate-xml-rsl-0.1 (crate (name "xml-rsl") (vers "0.1.0") (hash "0svzgga2j37k8dbsx02vkj83kag3wdqdcsd5bv3rjs543xd8c21r") (rust-version "1.58")))

(define-public crate-xml-rsl-0.1 (crate (name "xml-rsl") (vers "0.1.1") (deps (list (crate-dep (name "crypto-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tar-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml-rsl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n3901pp95ha5hjj1ry7nlafj8snkgcklv1rsylmm7ly2w336nc8") (rust-version "1.58")))

(define-public crate-xml-rsl-0.2 (crate (name "xml-rsl") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jdks") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ksre-tui") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sdkman-cli-native") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1xl7alpd4sfsjxf1h8410b9hssj30ylf92rdnjjaawja2lzqrg4d") (features (quote (("ordered_attrs" "indexmap") ("bench"))))))

(define-public crate-xml-schema-0.0.1 (crate (name "xml-schema") (vers "0.0.1") (deps (list (crate-dep (name "xml-schema-derive") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.0.1") (default-features #t) (kind 2)))) (hash "0x3ann56f43dqr20xhnv91j2n1kcsvbkma9h6wvjdq45v6jg0fm1")))

(define-public crate-xml-schema-0.0.2 (crate (name "xml-schema") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.0.2") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bl3nfdcckz6cyc392ir5bcmip1xmp8jhb1y381z0d587dpd2321")))

(define-public crate-xml-schema-0.0.3 (crate (name "xml-schema") (vers "0.0.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.0.3") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.3") (default-features #t) (kind 2)))) (hash "00wg4d9gnbvj4iws2jlh15scg2fl3k1550zygp1dij5fqqqcggak")))

(define-public crate-xml-schema-0.0.4 (crate (name "xml-schema") (vers "0.0.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 2)))) (hash "0976lik0m0zz25h0qy29w11299nl8fjmmzdk88q7iygabddafbc2")))

(define-public crate-xml-schema-0.0.5 (crate (name "xml-schema") (vers "0.0.5") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.0.5") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 2)))) (hash "0fgbnhdvfv9z86qc5lh64d3hc8q2b3synxscfav7c5k6sk0h9902")))

(define-public crate-xml-schema-0.0.6 (crate (name "xml-schema") (vers "0.0.6") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.0.6") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 2)))) (hash "1qz9q35l5cq2nqb4pspdk7s1p6amvrxfvlr0sh19sr0ba2gzyiy5")))

(define-public crate-xml-schema-0.0.7 (crate (name "xml-schema") (vers "0.0.7") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.0.7") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 2)))) (hash "0gb32dbkz3mvzvbi0g33xhxn3h13pqsv0y5md6bky3lipw3fi182")))

(define-public crate-xml-schema-0.1 (crate (name "xml-schema") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.8") (default-features #t) (kind 2)))) (hash "0mr5fwn5rx61b94c23pkacd8flcnv8a460qzmc6m9mgzza5jfydn")))

(define-public crate-xml-schema-0.2 (crate (name "xml-schema") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.8") (default-features #t) (kind 2)))) (hash "0nnqccyp0pjkw1ngnbsdm38qx5p6pynkipc6042wki65w7iji6bq")))

(define-public crate-xml-schema-0.3 (crate (name "xml-schema") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "xml-schema-derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-schema-derive") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "yaserde") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "yaserde_derive") (req "^0.9") (default-features #t) (kind 2)))) (hash "1vcnbrjr9nn2b58xz46zpgx6v8l9gx77vb6p0pb0h7c9mswm36nw")))

(define-public crate-xml-schema-derive-0.0.1 (crate (name "xml-schema-derive") (vers "0.0.1") (deps (list (crate-dep (name "Inflector") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "1v55211ia12axhgka4ikk7kmylbgd41aqdamnkqsg7bpmjzcw2n5")))

(define-public crate-xml-schema-derive-0.0.2 (crate (name "xml-schema-derive") (vers "0.0.2") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bgad1r7k0ykhjfpm0d4pq99988inv3725gilr49v8wmn7zvh943")))

(define-public crate-xml-schema-derive-0.0.3 (crate (name "xml-schema-derive") (vers "0.0.3") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ghm315fc33vi0zgmd4xs0ffa7l1zb20ygyy97p61y0357y9mvfk")))

(define-public crate-xml-schema-derive-0.0.4 (crate (name "xml-schema-derive") (vers "0.0.4") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yl7aqwvznra97sh3kfx8zcs8h721cqb1j6wgxv2s06v7qgz1ls1")))

(define-public crate-xml-schema-derive-0.0.5 (crate (name "xml-schema-derive") (vers "0.0.5") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 0)))) (hash "1lh6gj3jhrriwwqd0wamq57csaardm0mpngyl0vj2699cydxpkgl")))

(define-public crate-xml-schema-derive-0.0.6 (crate (name "xml-schema-derive") (vers "0.0.6") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 0)))) (hash "0lq6x272b2i4s1qlblgkqx7yakxz07kkfcp44jhgjqpfypb0zc0h")))

(define-public crate-xml-schema-derive-0.0.7 (crate (name "xml-schema-derive") (vers "0.0.7") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.4") (default-features #t) (kind 0)))) (hash "135by73bqg6k3wbib10lsrczn7wwcir0rfhq0608vpycdld65bmj")))

(define-public crate-xml-schema-derive-0.1 (crate (name "xml-schema-derive") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.8") (default-features #t) (kind 0)))) (hash "0cipzy6b0ymhq9rm0pfabp9m774dv8dc9kfb6h1igspwspp0slly") (features (quote (("default" "reqwest/default-tls"))))))

(define-public crate-xml-schema-derive-0.2 (crate (name "xml-schema-derive") (vers "0.2.0") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.8") (default-features #t) (kind 0)))) (hash "012x10qwlg0kn2qrzvhm3p78hdl8zqrsl7ikaghzmyfxq27jq9pq") (features (quote (("default" "reqwest/default-tls"))))))

(define-public crate-xml-schema-derive-0.3 (crate (name "xml-schema-derive") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (kind 0)) (crate-dep (name "simple_logger") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.9") (default-features #t) (kind 0)))) (hash "0xjqmzixs5dz6annxjqj7v2skbybfxixr22ncwvpmqxfkn6pq22d") (features (quote (("default" "reqwest/default-tls"))))))

(define-public crate-xml-stinks-0.1 (crate (name "xml-stinks") (vers "0.1.0") (deps (list (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.28.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1pz1kbsdh5jik0dkf3h2hz9jkv8ssxn71idh50fzyv8d82r68dsz") (features (quote (("deserializer-static-generics"))))))

(define-public crate-xml-string-0.0.1 (crate (name "xml-string") (vers "0.0.1") (hash "1rkbkf2aviyj2p6pgvvkxv30amcw0nisyh6898q7kay4k32n92p9") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-xml-string-0.0.2 (crate (name "xml-string") (vers "0.0.2") (hash "05lb0bys04y9fm2bylc3pa24zxj2f4h6ibjrkdipma6icwbwygmg") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-xml-xls-parser-0.1 (crate (name "xml-xls-parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_path_to_error") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "02vdw74h7lpxaqrs0zwlypnfv26dhhy55ghahnlgp55qpmjq0n7w")))

