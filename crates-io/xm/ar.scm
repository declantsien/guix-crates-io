(define-module (crates-io xm ar) #:use-module (crates-io))

(define-public crate-xmark-0.0.0 (crate (name "xmark") (vers "0.0.0") (hash "1zk3i3fp3zf6kzglc4i644w44za32s8mfph2nd53bxv27i27xpq0")))

(define-public crate-xmars-1 (crate (name "xmars") (vers "1.0.0") (hash "1vhwb989lmk487271n92f3ncw5scbk4w5h64xspkwvq01ix94h4r")))

