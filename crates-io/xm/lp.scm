(define-module (crates-io xm lp) #:use-module (crates-io))

(define-public crate-xmlparser-0.1 (crate (name "xmlparser") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)))) (hash "0km8v2jmyxhk1qdi0gwk42gspr95aif2jsakcvihjvsdbgl2xj73")))

(define-public crate-xmlparser-0.1 (crate (name "xmlparser") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)))) (hash "1bkydi5l4drb02mxbvl86j00n0w38si9bjn1br8jr3ldk0d0sd3k")))

(define-public crate-xmlparser-0.1 (crate (name "xmlparser") (vers "0.1.2") (deps (list (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)))) (hash "1anl2lz0xxkcbzydk3jcr2v1gfhvr14f7sjiwxam1kwgwyvqryx4")))

(define-public crate-xmlparser-0.2 (crate (name "xmlparser") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bhg5s36sjlds09y90nnc2zkp7avpapj7k9rlj7rc3cv6zwl0npn")))

(define-public crate-xmlparser-0.3 (crate (name "xmlparser") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.3") (default-features #t) (kind 2)))) (hash "0d1qhc60higrccmi0zafq9jv75anrzlsyw28x53ll4d7cf9vkh4l")))

(define-public crate-xmlparser-0.4 (crate (name "xmlparser") (vers "0.4.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.3") (default-features #t) (kind 2)))) (hash "0642rw6j2x7zjmz064qzhaz26j4zq9b1695vzywmwi6ifvqsnirs")))

(define-public crate-xmlparser-0.4 (crate (name "xmlparser") (vers "0.4.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.3") (default-features #t) (kind 2)))) (hash "1amakf52smyvh1n517zd64jhvm7j372v9d3gmi814wdxxlaln2m8")))

(define-public crate-xmlparser-0.5 (crate (name "xmlparser") (vers "0.5.0") (hash "0lg8k8vi0xb71sn819629893lczvy1l9j9377grppkf6slgffz9i")))

(define-public crate-xmlparser-0.6 (crate (name "xmlparser") (vers "0.6.0") (hash "1bw0bvkfih2yxjwm6bsfd1wb2lpp8xc4qbwhfdsj2b1rapqmmwqc")))

(define-public crate-xmlparser-0.6 (crate (name "xmlparser") (vers "0.6.1") (hash "1fvjrhj73p2048b9lvlmm8rwm89b5nfsai7q7api5k0nk6gg4xga")))

(define-public crate-xmlparser-0.7 (crate (name "xmlparser") (vers "0.7.0") (hash "1d6xh4dg69yaw6c1ngsgsqh4hkgfg9rp3djnfl02sgx820zsbsa1")))

(define-public crate-xmlparser-0.8 (crate (name "xmlparser") (vers "0.8.0") (hash "1p7r8awinjksv2xd1xcaiccgai5mj2l3m5f1qlhkr10a330mcli2")))

(define-public crate-xmlparser-0.8 (crate (name "xmlparser") (vers "0.8.1") (hash "0kg3cp5inzh6pj8a0ab9ycfzf39nk57xhifm44rzdvappclpz2np")))

(define-public crate-xmlparser-0.9 (crate (name "xmlparser") (vers "0.9.0") (hash "1vhw7s5672557vdz9s5kkmv0j53xz0haakp6af8h3zxh1zq9bv7c")))

(define-public crate-xmlparser-0.10 (crate (name "xmlparser") (vers "0.10.0") (hash "1sr1d602ng26lx07h3mqn3p3k88sg5d8v8rd1dmrc3fcbdn4j441")))

(define-public crate-xmlparser-0.11 (crate (name "xmlparser") (vers "0.11.0") (hash "0spbddc1h4y5ncbgnvg4bgig17y7h7mvykj5lwn8lphv6ixb0pj7") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.12 (crate (name "xmlparser") (vers "0.12.0") (hash "1397vmn6p9g4vq6j5b3v7ll62v8nfrqmdx1dnvhwv0xnjs7i7pr8") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13 (crate (name "xmlparser") (vers "0.13.0") (hash "15xsi5z44l5d8ql2hc6wnqidlwr7nar2ab77s13i70cyic982sdi") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13 (crate (name "xmlparser") (vers "0.13.1") (hash "1mv2hg0k8cyap8qb05c1a8kz86zcdmf9wdp95nz41pys0c129d6c") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13 (crate (name "xmlparser") (vers "0.13.2") (hash "1x95247fshdljsrdlrcvvyb4vr39xysw7lg71wygc4bgbxjkwqaj") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13 (crate (name "xmlparser") (vers "0.13.3") (hash "1n73ymdxpdq30fgz698095zvh8k5r264rl6pcxnyyrr19nra4jqi") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13 (crate (name "xmlparser") (vers "0.13.4") (hash "09avy9adr1bqrdpbv9y4z5dy4kp3iavjk1zbdnlm5kni0z9pcqia") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-xmlparser-0.13 (crate (name "xmlparser") (vers "0.13.5") (hash "1z8kkbbhq5mg8k02szi8cvivrfv88wajky4p182c84paz5dwf9ad") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-0.13 (crate (name "xmlparser") (vers "0.13.6") (hash "1r796g21c70p983ax0j6rmhzmalg4rhx61mvd4farxdhfyvy1zk6") (features (quote (("std") ("default" "std"))))))

(define-public crate-xmlparser-derive-0.1 (crate (name "xmlparser-derive") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "xmlparser-derive-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xmlparser-derive-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p6r6aqhjw25xzb3m126nkdn5izzq26wppkzcx11dwdl4v9y7sxk")))

(define-public crate-xmlparser-derive-0.1 (crate (name "xmlparser-derive") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "xmlparser-derive-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xmlparser-derive-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yxriqn5ng1d96sdr78n21sch2c6c0ap8k91ymb6m1vx7gvyhnm8")))

(define-public crate-xmlparser-derive-0.1 (crate (name "xmlparser-derive") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "xmlparser-derive-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xmlparser-derive-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ngy66qngybwqr0vivi6qanggv5wj8k7qhhnfmw8b30qxj4c7lv3")))

(define-public crate-xmlparser-derive-0.1 (crate (name "xmlparser-derive") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "xmlparser-derive-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xmlparser-derive-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "064b17s9snx5n8g6pfjbcmyfmqfqziwhk6ljqzb415ljqa2n1xbg")))

(define-public crate-xmlparser-derive-core-0.1 (crate (name "xmlparser-derive-core") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0v8z76p75i8li5g4wr53s49g6mavhzcgkygl662s2zyr5r3mz8zr")))

(define-public crate-xmlparser-derive-core-0.1 (crate (name "xmlparser-derive-core") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "04h7pd38iflr2638w2vjj7lfjgg8nkg3s2y988zckb3qvfpbwa2c")))

(define-public crate-xmlparser-derive-core-0.1 (crate (name "xmlparser-derive-core") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1lnsa7anbkr5hwcxczdmy7lvbdmdfagj0akigdxn18hhh6wb3vw2")))

(define-public crate-xmlparser-derive-utils-0.1 (crate (name "xmlparser-derive-utils") (vers "0.1.0") (deps (list (crate-dep (name "jetscii") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "13f8973bkvrd51y2kva99hi73iakzkygypc9xr1gynmx9mcvswh1")))

(define-public crate-xmlparser-derive-utils-0.1 (crate (name "xmlparser-derive-utils") (vers "0.1.1") (deps (list (crate-dep (name "jetscii") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1h96wjxj3z3vvrz2mwqnfy86if111p08mxlbvx2xkzgmcvhrg7m5")))

(define-public crate-xmlparser-derive-utils-0.1 (crate (name "xmlparser-derive-utils") (vers "0.1.2") (deps (list (crate-dep (name "jetscii") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0gqh36y8mppay74znlzhgq3cpzhlxh5ysbw2fll157lpzgnhd6zl")))

