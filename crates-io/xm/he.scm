(define-module (crates-io xm he) #:use-module (crates-io))

(define-public crate-xmhell-0.1 (crate (name "xmhell") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1ib02iyfv5vjyap5yg1zfrbsb5ngmnbb25fzdhaf3ms73kqs2dis")))

