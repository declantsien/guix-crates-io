(define-module (crates-io xm lt) #:use-module (crates-io))

(define-public crate-xmltojson-0.1 (crate (name "xmltojson") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "196sry1ig935ab1smx2nnija6plwpwqn9xx3b7gx374apg94gn1n")))

(define-public crate-xmltojson-0.1 (crate (name "xmltojson") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "06cd0wg38jh5qx03swbwv2sj71fzf9fncjk46inpp3zyvbksznm1")))

(define-public crate-xmltojson-0.1 (crate (name "xmltojson") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.28.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1fwv8givwdq18n1p4shh8x6i6qxcx1q4xn3h8als9dz3v9ryziim")))

(define-public crate-xmltree-0.2 (crate (name "xmltree") (vers "0.2.1") (deps (list (crate-dep (name "tempfile") (req "~1.1") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "0x1ah8maslc960pjb2ykvzhjg6s32pz7vr6h4llp2hg3585n0jb8")))

(define-public crate-xmltree-0.3 (crate (name "xmltree") (vers "0.3.0") (deps (list (crate-dep (name "tempfile") (req "~1.1") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r4290mi7g1697qdrkbd0kbnpk9k31xhqqrmz6mz1c9lswhfb3gv")))

(define-public crate-xmltree-0.3 (crate (name "xmltree") (vers "0.3.1") (deps (list (crate-dep (name "tempfile") (req "~1.1") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lmha8r7nkwhp4823p4yap9qcpzq5z1klaha32qcxrpvhc579mp5")))

(define-public crate-xmltree-0.3 (crate (name "xmltree") (vers "0.3.2") (deps (list (crate-dep (name "tempfile") (req "~1.1") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3") (default-features #t) (kind 0)))) (hash "10i050rdiggc85whs5dbnvd7dgzkn64mppv124wv4fn5qwvrsaj7")))

(define-public crate-xmltree-0.4 (crate (name "xmltree") (vers "0.4.0") (deps (list (crate-dep (name "xml-rs") (req "^0.4") (default-features #t) (kind 0)))) (hash "1v9076kdfvgdw9k8is4px31b8pkxwqy0j888iqwf2b7gwf5ilgwp")))

(define-public crate-xmltree-0.5 (crate (name "xmltree") (vers "0.5.0") (deps (list (crate-dep (name "xml-rs") (req "^0.5") (default-features #t) (kind 0)))) (hash "1fl2jpa152fx441hz2fkv48w1lnwi4m1r4mz0wix6pl2p939yvr9")))

(define-public crate-xmltree-0.6 (crate (name "xmltree") (vers "0.6.0") (deps (list (crate-dep (name "xml-rs") (req "^0.6") (default-features #t) (kind 0)))) (hash "0lbj3qil5glw63ljwsblqzlj8rb4r84s3dxizdari64v7si8gxrd")))

(define-public crate-xmltree-0.6 (crate (name "xmltree") (vers "0.6.1") (deps (list (crate-dep (name "xml-rs") (req "^0.6") (default-features #t) (kind 0)))) (hash "1xd9njqlw36v1jw3k6g26irg6ljbs65kphhvasjmjsg2791vqshj")))

(define-public crate-xmltree-0.7 (crate (name "xmltree") (vers "0.7.0") (deps (list (crate-dep (name "xml-rs") (req "^0.7") (default-features #t) (kind 0)))) (hash "0cbla8x58vs1xb3r64vynxhkbm9ln62y9711fwipvwdqlr6bbkx9")))

(define-public crate-xmltree-0.8 (crate (name "xmltree") (vers "0.8.0") (deps (list (crate-dep (name "xml-rs") (req "^0.7") (default-features #t) (kind 0)))) (hash "0w0y0jz7lhxg05ca6ngfj0lj8sbrjh4vaqv13q7qaqkhs7lsx3pz")))

(define-public crate-xmltree-0.9 (crate (name "xmltree") (vers "0.9.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "061hsxl9v6q5jl47iykhzkp02hidfs79sfgz1vydh9743dsxbd98")))

(define-public crate-xmltree-0.10 (crate (name "xmltree") (vers "0.10.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1pq4602zdi6p4yq522qnpb284yi174gsp6wdg922bnq0wvphg9kp")))

(define-public crate-xmltree-0.10 (crate (name "xmltree") (vers "0.10.1") (deps (list (crate-dep (name "indexmap") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1cr2r4xcrl6fyrgpmssjjq7n5l1d7ybw50k5rgqg24q3rfnasszp") (features (quote (("default") ("attribute-order" "indexmap"))))))

(define-public crate-xmltree-0.10 (crate (name "xmltree") (vers "0.10.2") (deps (list (crate-dep (name "indexmap") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1p8igrydh3w9mqiyay3wxnddmgbgzfs0a3df5rs38whksi1gsinh") (features (quote (("default") ("attribute-order" "indexmap"))))))

(define-public crate-xmltree-0.10 (crate (name "xmltree") (vers "0.10.3") (deps (list (crate-dep (name "indexmap") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1jqzwhr1a5cknflsshhhjlllmd1xi04qdkjsls2bnmv5mxgagn6p") (features (quote (("default") ("attribute-order" "indexmap"))))))

(define-public crate-xmltree-parse_with_config-0.10 (crate (name "xmltree-parse_with_config") (vers "0.10.3") (deps (list (crate-dep (name "indexmap") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "04q1qrsfvdc67aicsa4dnpd2g6qcpkh5109ck37ifcavkbfa0jx5") (features (quote (("default") ("attribute-sorted") ("attribute-order" "indexmap"))))))

(define-public crate-xmltv-0.1 (crate (name "xmltv") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6") (default-features #t) (kind 0)))) (hash "168jvs3z5xqnlqb8n6ix4vd4bkcpqh7w2kgb8x5yq3pk741qksz4") (yanked #t)))

(define-public crate-xmltv-0.9 (crate (name "xmltv") (vers "0.9.0") (deps (list (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0vxql4jafpmzlmjc34b5dcm9r8j8lacwq0n49j9xyjbx3q1xv47i")))

(define-public crate-xmltv-0.9 (crate (name "xmltv") (vers "0.9.1") (deps (list (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hpgzlhimjfady5ljjvzpyascjy8016vj1pixyx049mcp906dwjv")))

(define-public crate-xmltv-0.9 (crate (name "xmltv") (vers "0.9.2") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lexopt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (default-features #t) (kind 2)) (crate-dep (name "ratatui") (req "^0.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09xc9xihabz392r47yr9m0ggz5bxld7nw92kyjjlynw0z3icvw6j") (v 2) (features2 (quote (("build-binary" "dep:lexopt" "dep:quick-xml" "dep:crossterm" "dep:ratatui"))))))

(define-public crate-xmltv-0.9 (crate (name "xmltv") (vers "0.9.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lexopt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (default-features #t) (kind 2)) (crate-dep (name "ratatui") (req "^0.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00pn9q5fk0h17cs1xw80jysks9kinz2jhv6inlsyakx48mvmy493") (v 2) (features2 (quote (("build-binary" "dep:lexopt" "dep:quick-xml" "dep:crossterm" "dep:ratatui" "dep:chrono"))))))

(define-public crate-xmltv-0.9 (crate (name "xmltv") (vers "0.9.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lexopt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (default-features #t) (kind 2)) (crate-dep (name "ratatui") (req "^0.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xppf7whvihqgqq2xs62wzajxz1cszc9p0hw3ancii2hlfbzcjy2") (v 2) (features2 (quote (("build-binary" "dep:lexopt" "dep:quick-xml" "dep:crossterm" "dep:ratatui" "dep:chrono"))))))

(define-public crate-xmltv-0.9 (crate (name "xmltv") (vers "0.9.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lexopt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31") (features (quote ("serialize"))) (default-features #t) (kind 2)) (crate-dep (name "ratatui") (req "^0.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gbbr6jrsy0f7z1x8hnx2hkafqrxcihmyw33pkmy226cfdcwksbz") (v 2) (features2 (quote (("build-binary" "dep:lexopt" "dep:quick-xml" "dep:crossterm" "dep:ratatui" "dep:chrono"))))))

(define-public crate-xmltv-rs-0.0.1 (crate (name "xmltv-rs") (vers "0.0.1") (deps (list (crate-dep (name "xml-builder") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0qkj5372by0v8a7lp0hlmzajfnm0ddcqvxrxnis045lx4skayxw3")))

(define-public crate-xmltv-rs-0.0.2 (crate (name "xmltv-rs") (vers "0.0.2") (deps (list (crate-dep (name "xml-builder") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0s47k416flbzypi2119gzyspd19prkn2g1wmg0xbj5l33905nslk")))

(define-public crate-xmltv-rs-0.0.3 (crate (name "xmltv-rs") (vers "0.0.3") (deps (list (crate-dep (name "xml-builder") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0ndwzmry483gxjyyqprlqbmq0vllxz968rx2iawhzhmzsrr817si")))

