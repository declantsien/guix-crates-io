(define-module (crates-io xm lj) #:use-module (crates-io))

(define-public crate-xmlJSON-0.1 (crate (name "xmlJSON") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "01809qkamqpsjv3qswh9gl9z0sd5qxqsshrmnf63ra7kgrng0vh8")))

(define-public crate-xmlJSON-0.1 (crate (name "xmlJSON") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "13p4xglm9d1rkb54rmhldiv6padrldny5kz749wh7x13ibywvprm")))

(define-public crate-xmlJSON-0.1 (crate (name "xmlJSON") (vers "0.1.2") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "1089zpaz8ncxfi0qgbngssiy2c1s6ckgfmajbn89rxy48chl790w")))

(define-public crate-xmlJSON-0.1 (crate (name "xmlJSON") (vers "0.1.3") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "14dpj74ks44lk0l0yph968xjdv6c8rl8a20za58ykxmyvbh4hgcq")))

(define-public crate-xmlJSON-0.2 (crate (name "xmlJSON") (vers "0.2.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1d2rya045b19v5558svj9ssb8jfjfmfsfv9hlzbb8zl0l6kxkpfc")))

