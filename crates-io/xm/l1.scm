(define-module (crates-io xm l1) #:use-module (crates-io))

(define-public crate-xml1-0.1 (crate (name "xml1") (vers "0.1.0") (hash "1dfzc43192zbs4m2sbprg97yb6xvajzh7w01b6qjs6lyh0fjbg8r") (yanked #t)))

(define-public crate-xml1-0.1 (crate (name "xml1") (vers "0.1.1") (hash "07mk6syfz3swf12kqsq78a3cy9d678jbq77wcnlsjf3cvj9zwp69")))

(define-public crate-xml1-0.1 (crate (name "xml1") (vers "0.1.2") (hash "1dxbzhv2kb7v4g99r8rczz82mkz8vmc0h4394c74fc55g3v2p89k")))

(define-public crate-xml1-0.1 (crate (name "xml1") (vers "0.1.3") (hash "0fy8hkgl3apq7qr6528xf5y1yr31b2ar7f0385wfiix2j4kn6ds8")))

(define-public crate-xml1-0.1 (crate (name "xml1") (vers "0.1.4") (hash "1kknir163p8ghq5533bmw5zxx5w8bw6giyx34v386034k6aj1xd0")))

(define-public crate-xml1-0.1 (crate (name "xml1") (vers "0.1.5") (hash "1v218njw2xv8n41rbs7w1sdrvqv6fgv0vim7k71vcnkq96h4m8i5")))

(define-public crate-xml1-0.1 (crate (name "xml1") (vers "0.1.6") (hash "1krbbifj3jadci3massyxgq8ggf9m8wm1agkmvnxp9hj3ahs6k66")))

