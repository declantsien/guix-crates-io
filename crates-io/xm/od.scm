(define-module (crates-io xm od) #:use-module (crates-io))

(define-public crate-xmodem-0.1 (crate (name "xmodem") (vers "0.1.0") (deps (list (crate-dep (name "crc16") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.0") (default-features #t) (kind 2)))) (hash "0ac2qjpidsicarmpblicmaq55xip67wljrhx33vlvia18mag9gkk")))

(define-public crate-xmodem-0.1 (crate (name "xmodem") (vers "0.1.1") (deps (list (crate-dep (name "crc16") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.0") (default-features #t) (kind 2)))) (hash "0gbsprrriqcsd7nm4n106sz275xwnxqdh2iqbpbj7l9k1mkpjmik")))

(define-public crate-xmodem-0.1 (crate (name "xmodem") (vers "0.1.2") (deps (list (crate-dep (name "crc16") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.0") (default-features #t) (kind 2)))) (hash "0dw7qirqckvgk62gamggzy5nhjv67kxwksylsjacpmk0393vdwqj")))

(define-public crate-xmodem-0.1 (crate (name "xmodem") (vers "0.1.3") (deps (list (crate-dep (name "crc16") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.0") (default-features #t) (kind 2)))) (hash "18mg4g40vl7nk7m8if1wbknc80lrh32wcgp0dg3vjgjjczdr38i5")))

(define-public crate-xmodem-ftp-0.1 (crate (name "xmodem-ftp") (vers "0.1.0") (hash "1pwpk1zfwpq49pr21b0zz7j8adyjpa3gzz41kgijwm15j26w5is3")))

(define-public crate-xmodmap-pke-0.1 (crate (name "xmodmap-pke") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.94") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (default-features #t) (kind 0)))) (hash "0mg84nrp414xk4gmd6k5c8frmwadxy91k4blmlkfcpks32awmr7d")))

(define-public crate-xmodmap-pke-0.2 (crate (name "xmodmap-pke") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.94") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (default-features #t) (kind 0)))) (hash "1y0d65m2nbl9jzbq34dzkv849phzjvcdxpclzb6ixjzkq20bfp9f")))

(define-public crate-xmodmap-pke-0.2 (crate (name "xmodmap-pke") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.94") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (default-features #t) (kind 0)))) (hash "18ajg51bxysc2vmygpg49g49vxvgm9zcvklbh9v15bmdpxl6hhii")))

(define-public crate-xmodmap-pke-0.2 (crate (name "xmodmap-pke") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2.97") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (default-features #t) (kind 0)))) (hash "0miv3bjq6c3c248kbi045gpdkmscfv2j5y58nqqgigpi26d3qpfr")))

(define-public crate-xmodmap-pke-umberwm-0.0.1 (crate (name "xmodmap-pke-umberwm") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^0.9") (features (quote ("randr"))) (default-features #t) (kind 0)))) (hash "0ay87c16kdibk08ll512ybjrhs4fviwzmkms51yl5z12q8bwny9d") (links "X11")))

(define-public crate-xmodmap-pke-umberwm-0.0.2 (crate (name "xmodmap-pke-umberwm") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^0.9") (features (quote ("randr"))) (default-features #t) (kind 0)))) (hash "1qk9y2v9fa0bnjz73gca9v9p04c9hiamwd1yrl4kp017786an1d0") (links "X11")))

(define-public crate-xmodules-0.1 (crate (name "xmodules") (vers "0.1.0") (deps (list (crate-dep (name "elrond-wasm") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0npy3jbzq2xnca2n06w62sxwg83i50w18xm7cx7z875vz6j4w9b0") (yanked #t)))

(define-public crate-xmodules-0.1 (crate (name "xmodules") (vers "0.1.1") (deps (list (crate-dep (name "elrond-wasm") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0caj76s5fczn33gqaayi2piv5ajzv989l0zy4laxa4qibilv63qd") (yanked #t)))

(define-public crate-xmodules-0.1 (crate (name "xmodules") (vers "0.1.2") (deps (list (crate-dep (name "elrond-wasm") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "11m933xi0i6dfxixj6y1i8aaxzz1vxcww3454b6z050afik8k79s")))

