(define-module (crates-io xm ld) #:use-module (crates-io))

(define-public crate-xmldecl-0.1 (crate (name "xmldecl") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0lg0qrna1bixa9zcyrrmsbx0n1i91gz883ypg7ccy41p42wa9hkl")))

(define-public crate-xmldecl-0.1 (crate (name "xmldecl") (vers "0.1.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1346i8gxcakmq5b57zjkspld5wb22br7g5nhjx0p2md1zyj5bj78")))

(define-public crate-xmldecl-0.2 (crate (name "xmldecl") (vers "0.2.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1lgzymcpzm873ija94xgbdgfmsq8q4h3dvm3y5cp8ky9rf541szg")))

(define-public crate-xmldiff-0.3 (crate (name "xmldiff") (vers "0.3.0") (deps (list (crate-dep (name "quick-xml") (req "^0.22.0") (features (quote ("serialize"))) (default-features #t) (kind 0)))) (hash "13cy7b663hwbqll8jv1809d7in1khfbfaqnadlgz0p4ihfmjww6n")))

