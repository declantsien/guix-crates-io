(define-module (crates-io xm lh) #:use-module (crates-io))

(define-public crate-xmlhelper-0.1 (crate (name "xmlhelper") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "17izwi9xfrrvvq6yb8pfp6wr0c42s3x0zl1war4dfv242qj81ccd")))

