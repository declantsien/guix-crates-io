(define-module (crates-io xm z-) #:use-module (crates-io))

(define-public crate-xmz-snapshot-0.2 (crate (name "xmz-snapshot") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("unstable"))) (default-features #t) (kind 0)))) (hash "1mv965db37315jax244xzjz9xr9gk26ycac7zabpdl8bbpds44dq")))

