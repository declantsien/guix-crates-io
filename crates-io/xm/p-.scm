(define-module (crates-io xm p-) #:use-module (crates-io))

(define-public crate-xmp-writer-0.1 (crate (name "xmp-writer") (vers "0.1.0") (hash "1pya3xjabrhavycl54n7ljwppg6z8rsnpxlbn9rcj3wkpfxl5mwz")))

(define-public crate-xmp-writer-0.2 (crate (name "xmp-writer") (vers "0.2.0") (hash "128r1sfrs9zafv5ndh71f3a06znabcb6rip9w4clpab4iw9vlhs5")))

