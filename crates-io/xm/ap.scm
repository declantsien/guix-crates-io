(define-module (crates-io xm ap) #:use-module (crates-io))

(define-public crate-xmap-0.0.1 (crate (name "xmap") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "0vnx843avaq0cdj3fyv1l4hbcpmq0rwpz1fk7rl95yh0c8gvlfwj")))

