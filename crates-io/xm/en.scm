(define-module (crates-io xm en) #:use-module (crates-io))

(define-public crate-xmenu-0.1 (crate (name "xmenu") (vers "0.1.0") (deps (list (crate-dep (name "colorized") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "1clsj58g8g9bljqdxspcbw1np93i0k2x8wj8jd29hj9m60a1idrp")))

