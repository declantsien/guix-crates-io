(define-module (crates-io xm to) #:use-module (crates-io))

(define-public crate-xmtool-0.1 (crate (name "xmtool") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0nflzzp8z7dxhzbci8dxm5khd3ncscwg40i8x052zflc0c6dpnn8")))

(define-public crate-xmtool-0.1 (crate (name "xmtool") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1zq4k4va7348izn7axh65all7pvjq3rd5dnlxvks0rb1zgmds9vs")))

(define-public crate-xmtool-0.1 (crate (name "xmtool") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "12xzpvg0855cvp4qj9yp5mf3k0w39455b9p3795pivl8nfdi5nbs")))

(define-public crate-xmtool-1 (crate (name "xmtool") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0qsp4vyr380vnf7dhxy7rly9281l6dkhpmq5p7f4n8vc2b9k3pk8")))

(define-public crate-xmtool-1 (crate (name "xmtool") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1wdc4cid5w289n5pmdj9dgy6y3hsv3h8wc1gsslxh5gk5bbghwxx")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "05vcvkxdjclh2figkjbfn6223w73g2j7jfww0s1npn0gwd19c2rq")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1l1c29gha79sigih92gbvdgg2hdd041s7xcj65ma3nqf88w11lp0")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1f5yvn1l0py855yls75jcfjsg915wf7nahzvrn075kmi60i3ghqi")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.44") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0rnb30kj1wav7n7l7alksswaxxd8yn609wc9y8wha9gy28p2rj0y")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.45") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "16xqahf4pvn72lzrah56amckwv9w09jd0gr9vd7kd5h2swa0idkr")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.46") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "060c2b66waxrs9smh5k0azcg0lshn7c0b10vs7njb0dpr8lzh1ci")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.47") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "16v7nv2hy7hyf0bhikg0xrw77xb6qfa83ldf4nwdgfk4n2ns3mlf")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.48") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1vwzfhg1r4hbdr0n9yh94sx0al5j9al31676m6svx3nnr29awjm8")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.50") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0csgcm3m0p4y5k3p51nk5gv42w9f9ja3mcsbg57yspa7dh0rvn9z")))

(define-public crate-xmtool-2 (crate (name "xmtool") (vers "2.0.51") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cyberex") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "xmtool-sys") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1ssibv3w413cf44iw5jd10zf903pvk83d03yv40kzwljib1sn9i0")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "0hmvq64ldwmpgdw9swkgf8kg7wr5hp7hrlsaasmjhfqwq94hx72w")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cyberex") (req "^0.1.17") (default-features #t) (kind 1)))) (hash "1h9wa8yfc4irs018z171qrnvcvjlcby7mjzijzkfmyn7dm1gpc88")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cyberex") (req "^0.1.17") (default-features #t) (kind 1)))) (hash "07pcsd4nwh8vw8c83pd1a52a3b198zsgpx7xgfq0z7z3z7iky79w")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cyberex") (req "^0.1.18") (default-features #t) (kind 1)))) (hash "1877b2bqvxnaq3m1zpbmrhb35l4n372kz53yys9rj60rc7dggx6z")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cyberex") (req "^0.1.18") (default-features #t) (kind 1)))) (hash "02c5bvn40cib31bx04phaxdyl8aa3szmxxizd6zjk4s88r2jnmnp")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cyberex") (req "^0.1.26") (default-features #t) (kind 1)))) (hash "1p553j0kln1haw4wa3xqvlahh2yjzjirw9a4wzas2sizdv1n095i")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cyberex") (req "^0.3.4") (default-features #t) (kind 1)))) (hash "03k1jkj12vndbzshn7x1fmy5wfkpjy1q855vval1spymrrkyna75")))

(define-public crate-xmtool-sys-0.1 (crate (name "xmtool-sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cyberex") (req "^0.3.4") (default-features #t) (kind 1)))) (hash "1lg793yhnwl0jv921q89r8mdmyz8qvk5l32bckaxdf8gnigsvwrs")))

