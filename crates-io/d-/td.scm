(define-module (crates-io d- td) #:use-module (crates-io))

(define-public crate-d-td-0.1 (crate (name "d-td") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dtt") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0fpyr9i06s1gl6hwnm23rk2agg4lyz4ss72h8wpjy1dhak3xxql3")))

(define-public crate-d-td-0.2 (crate (name "d-td") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dtt") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1vgzpq4mjgcmr8qv84zvkdabb0nr9sj15f4findphcfml3mskmms")))

(define-public crate-d-td-0.3 (crate (name "d-td") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dtt") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1fa01kicp0jkkp7k7vxg9caqqj5hy4pkcsfi7gyqxfw435qpglk5")))

(define-public crate-d-td-1 (crate (name "d-td") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dtt") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "141yml1gfa34j6vy96r7facd19kbqczl7dxj6kjk90sf4np7qjzz")))

