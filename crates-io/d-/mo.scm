(define-module (crates-io d- mo) #:use-module (crates-io))

(define-public crate-d-modules-0.1 (crate (name "d-modules") (vers "0.1.2") (deps (list (crate-dep (name "elrond-wasm") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0p49y3iyygr6hkcjkj6s6gj62wc3qzkq0igfhhqdadxli8px2avf")))

(define-public crate-d-modules-0.1 (crate (name "d-modules") (vers "0.1.3") (deps (list (crate-dep (name "elrond-wasm") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0pdxqw9dh1ixmrh796i6wc6a9c004v5cfbldsh8g8ncczxbybhy7")))

