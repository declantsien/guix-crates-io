(define-module (crates-io d- st) #:use-module (crates-io))

(define-public crate-d-stu-0.1 (crate (name "d-stu") (vers "0.1.0") (deps (list (crate-dep (name "terminal_size") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16") (default-features #t) (kind 0)))) (hash "1b8y72ciamcg1g6bx0jgz9km5yyxykjgc2frsxd6c45m56nljmbs")))

(define-public crate-d-stu-0.1 (crate (name "d-stu") (vers "0.1.1") (deps (list (crate-dep (name "terminal_size") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16") (default-features #t) (kind 0)))) (hash "1fzza2alzr4r240hcf71vfyyar0ikinvvh0h21mry3ffi833yv0r")))

(define-public crate-d-stu-0.1 (crate (name "d-stu") (vers "0.1.2") (deps (list (crate-dep (name "terminal_size") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16") (default-features #t) (kind 0)))) (hash "19d32bxkfmqn52p3i9w2vznpmaffsaax2p8gkff7kc7c84422bh2")))

(define-public crate-d-stu-0.1 (crate (name "d-stu") (vers "0.1.3") (deps (list (crate-dep (name "terminal_size") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16") (default-features #t) (kind 0)))) (hash "1l3kqwwkp5yhv36ljzs0vlwl3j9kafzgc107rqff9x2jv92fn3dz")))

(define-public crate-d-stu-0.1 (crate (name "d-stu") (vers "0.1.4") (deps (list (crate-dep (name "terminal_size") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16") (default-features #t) (kind 0)))) (hash "0h78vp45v81gkhk6781qflkg9ycx399k71lwa28gr1y5f073h5l9")))

(define-public crate-d-stu-0.1 (crate (name "d-stu") (vers "0.1.5") (deps (list (crate-dep (name "terminal_size") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16") (default-features #t) (kind 0)))) (hash "18nmfcyqv4rs06mlxdwscnhfddqgi4l15if5s1yyyfz4sc2iz6kd")))

