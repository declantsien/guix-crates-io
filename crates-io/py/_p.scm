(define-module (crates-io py _p) #:use-module (crates-io))

(define-public crate-py_pathfinding-0.1 (crate (name "py_pathfinding") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)))) (hash "19w1j0q2kvjzlxxh50db5968iky7x6h83gr63y1x84kld9c1yvyy")))

(define-public crate-py_pathfinding-0.1 (crate (name "py_pathfinding") (vers "0.1.1") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)))) (hash "08h81lnkjg6508hlkwbil80b7d1bnfm4pzn0zzhm8242a2r079b6")))

(define-public crate-py_pathfinding-0.1 (crate (name "py_pathfinding") (vers "0.1.2") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)))) (hash "0v8hm0lxz6m3bakl358cwz41sji5v7v1gghbzmk0krwb47d4npya")))

(define-public crate-py_pathfinding-0.1 (crate (name "py_pathfinding") (vers "0.1.3") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)))) (hash "1bm33y17k7888r9765hnhqkj175fhdjflg11ymjp0l14k3lqjb3b")))

(define-public crate-py_pathfinding-0.1 (crate (name "py_pathfinding") (vers "0.1.4") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)))) (hash "17n2mjvrilfnsykkb3cca93w31kc283axhwj9ln7yxd64x6wl598")))

(define-public crate-py_pathfinding-0.1 (crate (name "py_pathfinding") (vers "0.1.5") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)))) (hash "0yxlbjjm98hv2hgnfjrrpxfhjnd2dx2kzlivlkh51qlg83var84p")))

