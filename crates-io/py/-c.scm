(define-module (crates-io py -c) #:use-module (crates-io))

(define-public crate-py-comp-0.1 (crate (name "py-comp") (vers "0.1.0") (hash "0m3x7kmmbvnqa281rqhajaw1a1rw74jd6p9jvkn4lgdifimx5p98")))

(define-public crate-py-comp-0.1 (crate (name "py-comp") (vers "0.1.1") (hash "05gicj5g7c47smljgsgy35kr85ywhkwbx52qxakxnzxha0q3p11q")))

(define-public crate-py-comp-0.1 (crate (name "py-comp") (vers "0.1.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xsx744gkyhsafh57y48fgd9bzk8jhy45zkb3b2lgpwnz6ricsj8")))

(define-public crate-py-comp-0.1 (crate (name "py-comp") (vers "0.1.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0acpnd3cvhs38ri0m48whl2j5h6f1m0nn97nqn4z6gksbdj41w0s")))

