(define-module (crates-io py in) #:use-module (crates-io))

(define-public crate-pyin-1 (crate (name "pyin") (vers "1.0.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "creak") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.120") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (features (quote ("rayon" "approx-0_5"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-npy") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray-stats") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "realfft") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "06zlw1m34fqg6ycjx94m00hvkq528xlcqyndd73f6bgi30kdlphj") (yanked #t)))

(define-public crate-pyin-1 (crate (name "pyin") (vers "1.0.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "creak") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.120") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (features (quote ("rayon" "approx-0_5"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-npy") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray-stats") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "realfft") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0lbi9h1fh2i183sys1lmzcfn29m20narc4n71lqcn1hwxp5grn33")))

(define-public crate-pyinrs-0.1 (crate (name "pyinrs") (vers "0.1.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1bxj9br43in6icsgp4fl361av6z7dhymsnjkrv5mf6nahw0kqn40")))

(define-public crate-pyinrs-0.2 (crate (name "pyinrs") (vers "0.2.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "02gfy32gl8ww4cdlm7gvac0rkq92r7x9xqcd8clsdplk3k4m0hxk")))

(define-public crate-pyinrs-0.3 (crate (name "pyinrs") (vers "0.3.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1ry1572yv3nsxnkmpp27kjx70237i91qa6z1gianq82yg44s13db")))

(define-public crate-pyinrs-0.4 (crate (name "pyinrs") (vers "0.4.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1mm8dsqd6rfwfnfb6hrhnwsj1iy06yqygc7l4k7cgr1rxy5irhyg")))

(define-public crate-pyinrs-0.5 (crate (name "pyinrs") (vers "0.5.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "0vrnazpcs36wcicb0cymxqmkd3hqpap1xb3fmhzy8m9yqq6sa1wc")))

(define-public crate-pyinrs-0.6 (crate (name "pyinrs") (vers "0.6.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1297l535b34s95cswn5ric93mb4cpcjha6fyyxz2d45pqkgad54b")))

(define-public crate-pyinrs-0.7 (crate (name "pyinrs") (vers "0.7.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "13rwd5zyy7zcbg8d5x0jx2kpq0sxka7yq49fqlic0aldln13qnw9")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.0.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "0pihynrw89vry9a7g8zvwk5fpp25aczz649smvzcg0kp0lwa3i6l")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.1.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "00bhrpg65ynnzhf6kgn5vnbq5g9m9cj0ld83g4g0c630b7lfmmna")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.2.0") (deps (list (crate-dep (name "mymatrix") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "0hfz2fnv06ca3bhfyzq4bzik74dya1l9jdhkljji4zfwz6hpwx92")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.3.0") (deps (list (crate-dep (name "mymatrix") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1vwyj8pmfm2bf5plvg2lg86knv4vriizbkrbs7zbg4p22zin7b77")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.4.0") (deps (list (crate-dep (name "mymatrix") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1pbxslszgh9vx9brgnhs80q0xa28p88bvq4dxhp7bj1s67n22cg9")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.4.1") (deps (list (crate-dep (name "mymatrix") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1kiikq8sw4cpr9n00p6lvdcvfg59pwq3nq2khk39qr1kv2xw3kpz")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.5.0") (deps (list (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "mymatrix") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)))) (hash "0klcbg47d0ci1b6k2l85mnx9g89c2x5zv0lrci1c7ics2sd5wvvw")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.6.0") (deps (list (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "mymatrix") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)))) (hash "0n3xlnakh8ixaszab8wykm9cs61mp57q2xwk203pn07392vj6jxv")))

(define-public crate-pyinrs-1 (crate (name "pyinrs") (vers "1.7.0") (deps (list (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "mymatrix") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)))) (hash "0mnrhk1wy7y5b5xpr55gi0jj4isrl4886amj34m7jpl53p9h1f6j")))

