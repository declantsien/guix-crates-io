(define-module (crates-io py ch) #:use-module (crates-io))

(define-public crate-pychan-0.1 (crate (name "pychan") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.0-beta.0") (features (quote ("experimental-async" "extension-module"))) (default-features #t) (kind 0)))) (hash "0jnf0c9vsy0qn5dz172zfr6ls7shgj8i9nv6in7iy1v7mayiyzkb")))

