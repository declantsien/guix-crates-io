(define-module (crates-io py pe) #:use-module (crates-io))

(define-public crate-pype-0.1 (crate (name "pype") (vers "0.1.0") (hash "07sk3lmlrwx4w4ykrc4cjxmk41kkhf615hg6sjvq769fgrqklm4l")))

(define-public crate-pype-0.2 (crate (name "pype") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "0z161rx84m81zqsmyrag6y2hb796q0rndvg2459ngpzxl6dsnn3i")))

(define-public crate-pype-0.2 (crate (name "pype") (vers "0.2.1") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "18asl71yb2c3g8c5f51vslx1b29xs4ny10l1lydfs4rgngs0vayv")))

