(define-module (crates-io py aw) #:use-module (crates-io))

(define-public crate-pyawabi-0.1 (crate (name "pyawabi") (vers "0.1.1") (deps (list (crate-dep (name "awabi") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.11") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "16hzjv07wvladi4ax5i9kp3snyz8ddhwrn7yn1fhql6p8pw2ia81")))

(define-public crate-pyawabi-0.2 (crate (name "pyawabi") (vers "0.2.0") (deps (list (crate-dep (name "awabi") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.11") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "106jk6gvf8f3wan92bx59rfdsy8abn9r2fglf5ln55y164qx8sbl")))

(define-public crate-pyawabi-0.2 (crate (name "pyawabi") (vers "0.2.2") (deps (list (crate-dep (name "awabi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.11") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1f8sf6vj85kp6lv2wjzj786jq2p17l4k5nms46fdq4xrb71ybz05")))

