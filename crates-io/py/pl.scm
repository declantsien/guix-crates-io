(define-module (crates-io py pl) #:use-module (crates-io))

(define-public crate-pyplanetarium-0.1 (crate (name "pyplanetarium") (vers "0.1.0") (deps (list (crate-dep (name "planetarium") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15") (features (quote ("extension-module" "abi3-py37"))) (default-features #t) (kind 0)))) (hash "0db7wp6jx9k6laf0xn1zb55211bxxa81cadbzd0kcdir339rngbb")))

(define-public crate-pyplanetarium-0.1 (crate (name "pyplanetarium") (vers "0.1.1") (deps (list (crate-dep (name "planetarium") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15") (features (quote ("extension-module" "abi3-py37"))) (default-features #t) (kind 0)))) (hash "15kv9cl5piafkxnnk2rxldjl4q7zhc3dgval5120j2ma22p2x163")))

(define-public crate-pyplanetarium-0.1 (crate (name "pyplanetarium") (vers "0.1.5") (deps (list (crate-dep (name "planetarium") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16.4") (features (quote ("extension-module" "abi3-py37" "generate-abi3-import-lib"))) (default-features #t) (kind 0)))) (hash "0mmvrn7k58qy57agnb4a0vdn8bsxkisdlc303vczd6siybh55i8w")))

(define-public crate-pyplanetarium-0.1 (crate (name "pyplanetarium") (vers "0.1.6") (deps (list (crate-dep (name "planetarium") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18") (features (quote ("extension-module" "abi3-py37" "generate-import-lib"))) (default-features #t) (kind 0)))) (hash "00nnjrp5cmfg3d197nf59g3awww2hsd5h5hfqdcrr3045nba4zca")))

(define-public crate-pyplanetarium-0.2 (crate (name "pyplanetarium") (vers "0.2.0") (deps (list (crate-dep (name "planetarium") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18") (features (quote ("extension-module" "abi3-py37" "generate-import-lib"))) (default-features #t) (kind 0)))) (hash "0pjmizkwrcl4b4bg395h6lgqc9rwyfvgdzq6wj7li3w50rbvrm78")))

(define-public crate-pyplanetarium-0.2 (crate (name "pyplanetarium") (vers "0.2.1") (deps (list (crate-dep (name "planetarium") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18") (features (quote ("extension-module" "abi3-py37" "generate-import-lib"))) (default-features #t) (kind 0)))) (hash "1in7pf8z3gf52gjx0wp9w9pcibiizfb9yzqj1kzwxmhrybdazpgk")))

