(define-module (crates-io py ca) #:use-module (crates-io))

(define-public crate-pycall-0.1 (crate (name "pycall") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "18nxadlixb8md58dpxfi0fv34x8mjny6nlxqb54xq94y8zxxwkg2")))

(define-public crate-pycall-0.2 (crate (name "pycall") (vers "0.2.0") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "00p7a7p86ybk55r18lvg36p7wxi3jr96fvjfgismhyq16z986113")))

(define-public crate-pycall-0.2 (crate (name "pycall") (vers "0.2.1") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "0iblqy5ixk75mfk6z2ca12x5qnjpfwy2h57r0w5nzxxplqdkwgy6")))

(define-public crate-pycall-0.2 (crate (name "pycall") (vers "0.2.2") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "0l569sgkh6jjsh0wg92kpw11f7m094nj2y5dci10mmm3vfj7z2hc")))

(define-public crate-pycall-0.2 (crate (name "pycall") (vers "0.2.3") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "12fv1lna8mp8ppslyw4v324sv0sj5f4mqqbp2liwrsf6fl7x5lzp")))

(define-public crate-pycall-0.3 (crate (name "pycall") (vers "0.3.0") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1n31ivgnkf30pa55xwwr0ikc0sdhk0vxhw9glgpx8980z8i3j090")))

(define-public crate-pycanrs-0.1 (crate (name "pycanrs") (vers "0.1.0") (deps (list (crate-dep (name "pyo3") (req "^0.18.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "ctrlc") (req "^3.2.5") (default-features #t) (kind 2)))) (hash "01aq29bv1mfgl7hxgdyj9z91yhpz9px3bnzwnr5mv1xmyx25jlc1")))

