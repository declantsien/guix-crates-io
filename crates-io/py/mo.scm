(define-module (crates-io py mo) #:use-module (crates-io))

(define-public crate-pymon-0.0.0 (crate (name "pymon") (vers "0.0.0") (hash "113gnrkyv7rpj761xm1zzw415m6yvd0n0536agiip6cjif56ibcy")))

(define-public crate-pymorx-0.1 (crate (name "pymorx") (vers "0.1.0") (deps (list (crate-dep (name "morx") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.10") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)))) (hash "12ma5pbsmk9vb0gs3lnlws2b09gsfnyrzcv8dj05mnj2ka9x7y2g")))

