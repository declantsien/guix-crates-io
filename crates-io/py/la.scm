(define-module (crates-io py la) #:use-module (crates-io))

(define-public crate-pylauncher-0.1 (crate (name "pylauncher") (vers "0.1.0") (deps (list (crate-dep (name "dbg") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "exec") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "1vjzi0q0sfa7qjcz0r5rr4fk0jmwfij3szbf49r5mrfv0jq794pb")))

(define-public crate-pylauncher-0.1 (crate (name "pylauncher") (vers "0.1.1") (deps (list (crate-dep (name "dbg") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "exec") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "1v9kdjxwxv2mzc1m4qi1s4mfgsphgyp73d9kz8nq950r69ifv82c")))

