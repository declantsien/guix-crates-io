(define-module (crates-io py -p) #:use-module (crates-io))

(define-public crate-py-pkstl-0.1 (crate (name "py-pkstl") (vers "0.1.0") (deps (list (crate-dep (name "pkstl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.8.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.9") (default-features #t) (kind 0)))) (hash "14b9fp1ngmfi3pmvwmpv2bhjnm8ib66lzp0s15940f1hmrwrd68c")))

