(define-module (crates-io py nq) #:use-module (crates-io))

(define-public crate-pynq-z1-bsp-0.1 (crate (name "pynq-z1-bsp") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)))) (hash "0ssa2zgb0j1kr94sxmlbl7l4d4mj65xbzvsymd8vvqkyz0hyy5rv")))

