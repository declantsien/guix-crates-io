(define-module (crates-io py _s) #:use-module (crates-io))

(define-public crate-py_sql-1 (crate (name "py_sql") (vers "1.0.0") (deps (list (crate-dep (name "dashmap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rexpr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "0mnryf90g7bj1zarfv67w6p65r6r835qiw9mgz2x4yj61k04199c")))

(define-public crate-py_sql-1 (crate (name "py_sql") (vers "1.0.1") (deps (list (crate-dep (name "dashmap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rexpr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0q301bn821nyfkhz1s08dylrh1zddbqkxs4i6glydk8g6rn70ss0")))

