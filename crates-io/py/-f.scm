(define-module (crates-io py -f) #:use-module (crates-io))

(define-public crate-py-fetch-1 (crate (name "py-fetch") (vers "1.0.0") (deps (list (crate-dep (name "sysinfo") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0ixp09cadcifb04kc87ijsia43y5iiba762w96641fshggy3vc2v")))

(define-public crate-py-fetch-1 (crate (name "py-fetch") (vers "1.0.1") (deps (list (crate-dep (name "sysinfo") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "07l21alv69wbp2ra0n93jk15017d555rassqrfddjn4x971wyng5")))

(define-public crate-py-fetch-1 (crate (name "py-fetch") (vers "1.1.1") (deps (list (crate-dep (name "sysinfo") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0dxzil2crr0yfp1348yc07vgkrp6jyy243lk4zsis4rkdj3g1qa2")))

(define-public crate-py-fetch-1 (crate (name "py-fetch") (vers "1.1.2") (deps (list (crate-dep (name "sysinfo") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0k3js62nm6a5a5mxif2vpqmd88dn1nn2hcfjnqdwh6xy7k591s02")))

(define-public crate-py-fetch-1 (crate (name "py-fetch") (vers "1.2.2") (deps (list (crate-dep (name "sysinfo") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "00j10zpkkl0830wydnci1hdawir91svyi6ncqn49ja1x3dfmxw67")))

(define-public crate-py-fetch-1 (crate (name "py-fetch") (vers "1.2.3") (deps (list (crate-dep (name "sysinfo") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1m6y302lj198rp777aii8awg6nhzmvk2qig0c8vll4h7rn8qvq95")))

(define-public crate-py-fossil-delta-0.1 (crate (name "py-fossil-delta") (vers "0.1.0") (deps (list (crate-dep (name "cbindgen") (req "^0.5.2") (default-features #t) (kind 1)) (crate-dep (name "fossil-delta") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.7.0") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "16gp1nxhlkw70kmvsccrpxqxmwfmrzc9qp2iacn1jbs8qhdndikg")))

(define-public crate-py-fossil-delta-0.1 (crate (name "py-fossil-delta") (vers "0.1.1") (deps (list (crate-dep (name "cbindgen") (req "^0.5.2") (default-features #t) (kind 1)) (crate-dep (name "fossil-delta") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.7.0") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1c3lbb4mh1zmcgz3ck04fsl7z4pxzdwi5hyhvzryzkzaya0hzhv0")))

(define-public crate-py-fossil-delta-0.1 (crate (name "py-fossil-delta") (vers "0.1.2") (deps (list (crate-dep (name "cbindgen") (req "^0.5.2") (default-features #t) (kind 1)) (crate-dep (name "fossil-delta") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.7.0") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "0nvc7nn6zhy5knnmsvnghimz3pjs5dkxbnmap7kkvqfmyvh2ammg")))

(define-public crate-py-fossil-delta-0.1 (crate (name "py-fossil-delta") (vers "0.1.3") (deps (list (crate-dep (name "fossil-delta") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.8.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "cbindgen") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "0pl8f75rmzqdcrkqiw1xxgrgknmsn8bj0y4wdxwcv9n9dpgknc6v")))

