(define-module (crates-io py wa) #:use-module (crates-io))

(define-public crate-pywavers-0.1 (crate (name "pywavers") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "wavers") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "072a699y2lcb2kpzdbcp6jna51ghh17bb1kj4vpx2fd4r2h3iwhn")))

(define-public crate-pywavers-0.1 (crate (name "pywavers") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "wavers") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1mi096czlr2i7igris7wl4gd1i7qalsmnfqigrdnqjjahdkjamyl")))

