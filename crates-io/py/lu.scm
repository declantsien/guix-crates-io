(define-module (crates-io py lu) #:use-module (crates-io))

(define-public crate-pyluwen-0.5 (crate (name "pyluwen") (vers "0.5.2") (deps (list (crate-dep (name "luwen-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "luwen-if") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "luwen-ref") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("extension-module" "multiple-pymethods"))) (default-features #t) (kind 0)) (crate-dep (name "ttkmd-if") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xfssayfk9gp2l7sr85hxs1nzciz57xdh4s2s20vi518xy6mx1gj")))

