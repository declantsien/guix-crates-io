(define-module (crates-io py qi) #:use-module (crates-io))

(define-public crate-pyqie-0.0.4 (crate (name "pyqie") (vers "0.0.4") (deps (list (crate-dep (name "pyo3") (req "^0.20") (features (quote ("abi3-py37" "extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "pyqie-engine") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.30") (default-features #t) (kind 0)))) (hash "1k0pxw7nnhimgyjcfg6g423d2bwnvbsm1lgl7l41fmqnbrc8fsm5")))

(define-public crate-pyqie-engine-0.0.4 (crate (name "pyqie-engine") (vers "0.0.4") (deps (list (crate-dep (name "noise") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pyqie-platform") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6") (default-features #t) (kind 0)))) (hash "0zcilq3g1g207sf1rnxzzhcbxyg7jm7mvdq855nv1lfdwc7zl8y7")))

(define-public crate-pyqie-platform-0.0.4 (crate (name "pyqie-platform") (vers "0.0.4") (hash "1l6vfcdm8r6jrfbh0zyng84qnc2jrykvf3f5844jk5hmh1fkz72v")))

