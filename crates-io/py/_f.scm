(define-module (crates-io py _f) #:use-module (crates-io))

(define-public crate-py_fstr-0.1 (crate (name "py_fstr") (vers "0.1.0") (deps (list (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0zzknbinscnphh9gg2mbcb0fnr5y9lfp4w56c3fnlm3l9jjmj9ks")))

