(define-module (crates-io py di) #:use-module (crates-io))

(define-public crate-pydis-0.1 (crate (name "pydis") (vers "0.1.0") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dmah86gl9rqvix5wfn79dad5d41rhfab2zwrx6bby27dcsax0x5")))

(define-public crate-pydis-0.1 (crate (name "pydis") (vers "0.1.1") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gljcdvxhw8i1xsfwr9awbr9ya6hdjh25cwp95mfrq9gpmb7m3js")))

(define-public crate-pydis-0.1 (crate (name "pydis") (vers "0.1.2") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "173bykqw28nnffvfwpgdmvcyfwhpg0m6fzpsxb26vxxmlh3b19zb")))

(define-public crate-pydis-0.2 (crate (name "pydis") (vers "0.2.0") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gn0y83aqrwqslyfwv5f8as6l60lac8wb5n8934zy159h2f4q0fs")))

(define-public crate-pydis-0.2 (crate (name "pydis") (vers "0.2.1") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yh40ys4x3c1xjzdarwbdm23xzgpqdrvn1k1f45k78rb8xvmzfjq")))

(define-public crate-pydis-0.2 (crate (name "pydis") (vers "0.2.2") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s3r2lz2hvr093ijgvn45vbv77bc58x8lw1h447cjbqc1krknxzx")))

(define-public crate-pydis-0.3 (crate (name "pydis") (vers "0.3.0") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y17a2qckc2jp5njj50hwi889cid7hs5p252l8017pqzhw6l2h3d")))

(define-public crate-pydis-0.3 (crate (name "pydis") (vers "0.3.1") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h8gb1x4492sbljrxazq16dlhy5pcpzdp5jhj1jzf007iyjya18a")))

(define-public crate-pydis-0.4 (crate (name "pydis") (vers "0.4.0") (deps (list (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v0lmvcrvsz19zlgdynrqv4mzq2rbnbzxkvgpqi5wk61dpa9jwmc")))

