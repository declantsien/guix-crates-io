(define-module (crates-io py li) #:use-module (crates-io))

(define-public crate-pylib-0.1 (crate (name "pylib") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0q5g32ar58619cz4qhrl0p8mf8kx9cq84hjpaz6ii09ypncwyy6a")))

(define-public crate-pylib-0.2 (crate (name "pylib") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1vsiv5rdwlb8ab5slla2fk5cfv5qw9rxkhmdp5m0j5l7ban680vk")))

(define-public crate-pylib-0.3 (crate (name "pylib") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "05fnr6vpkzy6sghjaqbynx0liq9giz4fg8l1nqpcb1q6aw4zcnah")))

