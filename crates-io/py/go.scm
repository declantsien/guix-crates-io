(define-module (crates-io py go) #:use-module (crates-io))

(define-public crate-pygo-0.0.1 (crate (name "pygo") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "~2.33.3") (features (quote ("suggestions" "color"))) (kind 0)))) (hash "1pk3fka38gc34m83cy5x8gqjg37f595jh86km8dd0bw98bjmyqkf")))

(define-public crate-pygo-0.1 (crate (name "pygo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33.3") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0i9khmnwb1p8f4l23ld8zmyca0xvhhm9czs801xmh4ga5988aggz")))

(define-public crate-pygo-0.1 (crate (name "pygo") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33.3") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "084xz8yky9y3mqjdivqxzxbb4m662m4dc7xapl167m3w48h4ak4y")))

