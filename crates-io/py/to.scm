(define-module (crates-io py to) #:use-module (crates-io))

(define-public crate-pytool-0.1 (crate (name "pytool") (vers "0.1.0") (hash "129fmy70iv1vybbxawmydnp1shr4jkigv9qyp3c3n2jy3im4ibww")))

(define-public crate-pytools-rs-0.1 (crate (name "pytools-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "10jzpaza0hr7dx9avg2j703plc068a030x0z1y13j1ms0bgryblq")))

(define-public crate-pytools-rs-0.1 (crate (name "pytools-rs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "107lrirzgxvagpyy878z2kv9cbjasszfd16wi6bmqvcfjfrzxsrp")))

(define-public crate-pytorch-cpuinfo-0.1 (crate (name "pytorch-cpuinfo") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "0rx5yx3qr8x6g8zvbkp8g2f41w64hqfn9k4j0sq2q9x42hm8h4nc") (yanked #t)))

(define-public crate-pytorch-cpuinfo-0.1 (crate (name "pytorch-cpuinfo") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "07mhb6y863bnzfqxy3d8sflpmy8pfcmsyjbmh6h8npws4x8c4q43")))

(define-public crate-pytorch-cpuinfo-0.1 (crate (name "pytorch-cpuinfo") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "build-target") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "0avm252wdqi77577pf5lzncaxr4jnnr5i1kz5vaa53rb9jq094ba")))

(define-public crate-pytorch-cpuinfo-pieces-0.0.1 (crate (name "pytorch-cpuinfo-pieces") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (default-features #t) (kind 1)) (crate-dep (name "build-target") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "1sa8dsljan8hi4k6n0fhzz0lk6q57xwv5vk9f1h52qq47x207md2")))

