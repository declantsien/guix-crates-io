(define-module (crates-io py ad) #:use-module (crates-io))

(define-public crate-pyadvreader-1 (crate (name "pyadvreader") (vers "1.2.0") (deps (list (crate-dep (name "advreader") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1h9gcmm9yz5a99x8y9q8lyy2ipzh1nsa72g012lgc7dkqkcayj04")))

(define-public crate-pyadvreader-2 (crate (name "pyadvreader") (vers "2.0.0") (deps (list (crate-dep (name "advreader") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "08bmnisr578s6az1d3qxfsykxb73ddrlrxv7sjq6rnbnj9bgw39g")))

(define-public crate-pyadvreader-2 (crate (name "pyadvreader") (vers "2.1.0") (deps (list (crate-dep (name "advreader") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1clr222q8rqmcsy2xlfrzxy2z7f1vg8j5zh85gw924iqzc95dfq4")))

(define-public crate-pyadvreader-2 (crate (name "pyadvreader") (vers "2.1.1") (deps (list (crate-dep (name "advreader") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "10k9j425lh060s9mwczys7jq9f4dl2pnrl760v7gq6ia342zycrq")))

(define-public crate-pyadvreader-2 (crate (name "pyadvreader") (vers "2.2.0") (deps (list (crate-dep (name "advreader") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1q4smw0rz4r8ml1ipwb9c52i7m2a754d97vfi2diwh1r9z1qmpvf")))

