(define-module (crates-io py rs) #:use-module (crates-io))

(define-public crate-pyrs-0.1 (crate (name "pyrs") (vers "0.1.0") (deps (list (crate-dep (name "natural") (req "^0.4.0") (features (quote ("serde_support"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.17") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lf45zm6yv5azx7vj8y9dq6p6apyj43rcwdi9vmvj6fp0gr2yh89")))

