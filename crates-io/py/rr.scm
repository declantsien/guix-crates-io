(define-module (crates-io py rr) #:use-module (crates-io))

(define-public crate-pyrrhic-rs-0.1 (crate (name "pyrrhic-rs") (vers "0.1.0") (deps (list (crate-dep (name "cozy-chess") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "12ibccf6s41sq8h52irilxhi2fgfyzb1dvprxshaf54i2p5id7kj")))

(define-public crate-pyrrhic-rs-0.2 (crate (name "pyrrhic-rs") (vers "0.2.0") (deps (list (crate-dep (name "cozy-chess") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "16g2lhs1m7cxvaldid4gaifa2mp4h9yv8q8fzdshsvmq7c2hz4g3")))

