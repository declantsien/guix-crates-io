(define-module (crates-io py tr) #:use-module (crates-io))

(define-public crate-pytrace-0.2 (crate (name "pytrace") (vers "0.2.1") (deps (list (crate-dep (name "glob") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.10.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "pytrace_core") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0yypw8b7l7p6pywvc1imnl9k7c3w01il63xyna14z61rrfms1bqj")))

(define-public crate-pytrace-0.3 (crate (name "pytrace") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.10.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "pytrace_core") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "00brh2l01b1m10hz27plywqqy2fr84w5c3sf1ipzkpiv4khdhm7x")))

(define-public crate-pytrace-0.3 (crate (name "pytrace") (vers "0.3.1") (deps (list (crate-dep (name "glob") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.10.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "pytrace_core") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1aj3nq3nb4158kl4v48nkyygdr3jmq2i2s0qa0mgnfbkxgk0wx1l")))

(define-public crate-pytrace-0.3 (crate (name "pytrace") (vers "0.3.2") (deps (list (crate-dep (name "ctrlc") (req "3.1.*") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.10.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "pytrace_core") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1rw95vz5faxyav4ldnzzg9gjzsj86r018hx9xgkf4b4g3i5962ag") (yanked #t)))

(define-public crate-pytrace-0.3 (crate (name "pytrace") (vers "0.3.3") (deps (list (crate-dep (name "ctrlc") (req "3.1.*") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.10.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "pytrace_core") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "07k6j8ab67yag6ks1mrghn3gkwl060bfy30m8hzb556cv0x8bdwi")))

(define-public crate-pytrace_core-0.2 (crate (name "pytrace_core") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "1.8.*") (default-features #t) (kind 0)))) (hash "08f4n5bripm2qf4np58cslwrzxn3n3d94a9iagx9gkab9681nljg")))

(define-public crate-pytrace_core-0.2 (crate (name "pytrace_core") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "1.8.*") (default-features #t) (kind 0)))) (hash "13f7k80ndykcvaycjyaciyjpbvwdd2a68dj82f8hvfwcy1xsh35s")))

(define-public crate-pytrace_core-0.2 (crate (name "pytrace_core") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "1.8.*") (default-features #t) (kind 0)))) (hash "1vkfyxcmlsrpnjk3hv630ijra2wrvp4i4rpq6xi2b768a4rbkd7i")))

(define-public crate-pytrace_core-0.2 (crate (name "pytrace_core") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "1.8.*") (default-features #t) (kind 0)))) (hash "0fh54da6mq89pchh5817gw5yd4jgkc1snv6p4dplx2l7kz1fcz37")))

(define-public crate-pytrace_core-0.2 (crate (name "pytrace_core") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "1.8.*") (default-features #t) (kind 0)))) (hash "0ps5h5icwpd7i2mxxpznszkylcbx7b7h37sp10f56lz4iqvjf0n4")))

(define-public crate-pytrace_core-0.2 (crate (name "pytrace_core") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "1.8.*") (default-features #t) (kind 0)))) (hash "1744zlfhad4b6vs6sgrlkzm8ma4bp4v8az5al08yzhdhrk4qcvgj")))

(define-public crate-pytrustfall-0.1 (crate (name "pytrustfall") (vers "0.1.0") (hash "01ixy8nxp4ri538y5vliplbbnjw93g1km39xycy3ax8ya32cqpac")))

