(define-module (crates-io py gm) #:use-module (crates-io))

(define-public crate-pygmentize-0.1 (crate (name "pygmentize") (vers "0.1.0") (deps (list (crate-dep (name "winapi-util") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "05la80raccw3v268yj8ssyc45968zmbcr1gf04i7wzg5dhgg7x0z")))

(define-public crate-pygmentize-0.2 (crate (name "pygmentize") (vers "0.2.0") (deps (list (crate-dep (name "winapi-util") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0lxld6z88bb4scq9j4alcqblbs2qpmcfr1n8rr1iqnh43vx1azgf")))

