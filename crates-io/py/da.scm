(define-module (crates-io py da) #:use-module (crates-io))

(define-public crate-pydantic-logfire-0.0.0 (crate (name "pydantic-logfire") (vers "0.0.0") (hash "0x2hmp8ry1saijblvsyw2n06vp17xd9p7ib582p3b387g6ik4cyf")))

(define-public crate-pydantic-logfire-0.0.1 (crate (name "pydantic-logfire") (vers "0.0.1") (hash "0cmlzg9qybkpay1iaia21bdh3fxph9z8fyq7pmh4llv946780aig")))

