(define-module (crates-io py ph) #:use-module (crates-io))

(define-public crate-pyphen-rs-0.1 (crate (name "pyphen-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)))) (hash "1qlgbjwg9lrkwzkp73w9vs6p8vg05z12zpgycz5jr2qky6w27pzz")))

