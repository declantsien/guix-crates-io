(define-module (crates-io py wi) #:use-module (crates-io))

(define-public crate-pywinpty_findlib-0.1 (crate (name "pywinpty_findlib") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.8.0") (default-features #t) (kind 1)))) (hash "0alw11nbsp6md67sf5svsdis2v55zv8ksvccqbii2ry9g5xvfvki")))

