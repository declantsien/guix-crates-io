(define-module (crates-io py de) #:use-module (crates-io))

(define-public crate-pydeco-0.1 (crate (name "pydeco") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.20") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1357ja1ahv85ls9kd6cxcw9ng7v70z6b3n79ppl5jzwmsfwxzdm0")))

(define-public crate-pydestiny-0.1 (crate (name "pydestiny") (vers "0.1.0") (deps (list (crate-dep (name "destiny") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.11.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "04n3dx8jkxygdvdvp117rf009p6lvc60yybgzgm6ga6mc6vdxqd3")))

