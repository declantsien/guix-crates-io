(define-module (crates-io py bu) #:use-module (crates-io))

(define-public crate-pybuild-0.0.1 (crate (name "pybuild") (vers "0.0.1") (deps (list (crate-dep (name "pybuild-parser") (req "^0.0") (default-features #t) (kind 0)))) (hash "0wz9y8k71wgnhfiqwsy2p45z4vf59qb0y1zr4rkwrf6v5azy3dkl")))

(define-public crate-pybuild-parser-0.0.0 (crate (name "pybuild-parser") (vers "0.0.0") (deps (list (crate-dep (name "fancy-regex") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0f6k2s3w7favrbhfcdk5qb224f4ahxccvbs7hj47iafcxyhn1cvi")))

(define-public crate-pybuild-parser-0.0.1 (crate (name "pybuild-parser") (vers "0.0.1") (deps (list (crate-dep (name "fancy-regex") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "13fq4vq5y5bk0y1a989xpzr7c11gyasjzk7iwnfrwak1d2bgygs4")))

