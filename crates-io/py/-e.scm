(define-module (crates-io py -e) #:use-module (crates-io))

(define-public crate-py-ed25519-bindings-0.1 (crate (name "py-ed25519-bindings") (vers "0.1.1") (deps (list (crate-dep (name "ed25519-dalek") (req "^1.0.0") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.9.2") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1a0k9lk2nzmvapz2m1a0a4l7kc46q3vr8j9ryph8zia670fclkll")))

(define-public crate-py-ed25519-bindings-0.1 (crate (name "py-ed25519-bindings") (vers "0.1.2") (deps (list (crate-dep (name "ed25519-dalek") (req "^1.0.1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.9.2") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1025vbplzyn5jm7wab7lm6knpnx0a4rqm0r2cvprhlxa5qdwbspb")))

