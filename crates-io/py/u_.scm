(define-module (crates-io py u_) #:use-module (crates-io))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.0") (hash "1lhb1s536r0wsqp1zr68zqlsb2cr2rf4hlyivp3ghkyk5ikgnlsj")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.2") (hash "1b3xi0safv6cy8mhmizkzzdzcdi0clzjazxpkqyh3s6k080ik5dg")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.3") (hash "0bd6nm9zmnb18a9b2mrvicqxxsyjcsgz9n97h67km62y7zn6441x")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.4") (hash "0pwxgh8n601km834h84f25zhwyh29ccrjc3ygsbpjwaqap3famlg")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.5") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1jdpywnv5016sa2i7blkayx0lc90kxwzj8119wjq85zvvkbjdnig")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.6") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0assk2053jg61mfa4pxg9is9fsy6bp07kbsdiqap1xis9qan8gps")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.61") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0kgpsygqk48w2vdqjxzxky4w4hc256nqs9bigb52grlbhwannka8")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.62") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "04qhqay10gav948z93vkqv5835gxhk0gqrza012fypnnz93vhczn")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.63") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1gjgs783kcfxdbkp0bcrzr1gmwhq8h5ylv7vm2p4i6m2bwij3007")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.64") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1fg15h3yzi9b4g3adx3ax1jlz38rjmfv4jmyvwqb2l2pw73mnny8")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.66") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1sx4sm8f0b99iz810g5njh5dfx894xlww4jxsf8904cc0dg236r1")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.7") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0hzv95zcvn893chprsyg1jh6p95ylbidmzlv96awxaf7qj7v0szp")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.70") (deps (list (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "10jkny1pjx8dsafpzal7073wa3w7n0gy8rn273ml22rvbap106kh")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.71") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0mjy5yg4nr9pr3g4jgih1lji9gy3gbd7fbgk8bdl7h3hm0kxvhy2")))

(define-public crate-pyu_rust_util-0.1 (crate (name "pyu_rust_util") (vers "0.1.80") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "116jnzkvswgsv6inyay4byl7r936s7baqp5l5f2szvrd7irymgkq")))

