(define-module (crates-io py _e) #:use-module (crates-io))

(define-public crate-py_env-1 (crate (name "py_env") (vers "1.0.0") (hash "1w3hljx92xpdgf18xc33absy40sjcwxfyyhjh5j8kvd39yycnb6g") (yanked #t)))

(define-public crate-py_env-1 (crate (name "py_env") (vers "1.0.1") (hash "13s9h203fgshqm2k25j44ivgq5r5wkiladpj75vf18ja7r5f89l2")))

(define-public crate-py_env-1 (crate (name "py_env") (vers "1.1.0") (hash "1x5my2jniqhwf134hl1li6a450zjrkls0jzwbg1g4rg32p43k08r") (yanked #t)))

(define-public crate-py_env-1 (crate (name "py_env") (vers "1.1.1") (hash "1nvxciifv5my6q3qxcl87kipb8cswm9nchjdgy1j21yihyqc7xvj") (yanked #t)))

(define-public crate-py_env-1 (crate (name "py_env") (vers "1.1.2") (hash "0bxakw1igcxjnbw96yh9l5c5z05giv8nnj8b07xgqag17siz6gpy")))

(define-public crate-py_env-2 (crate (name "py_env") (vers "2.0.0") (deps (list (crate-dep (name "pyo3") (req "^0.20.2") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "1mnha69dqw3hs1iil7prq155x3g8pq04zan77b2afmf1dl6bkvzm") (yanked #t)))

(define-public crate-py_env-2 (crate (name "py_env") (vers "2.0.1") (deps (list (crate-dep (name "pyo3") (req "^0.20.2") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "1xm0rkbgadfivlliqbdc93n3m9qbk1vgmj32xnjjlpysg67x24i6") (yanked #t)))

(define-public crate-py_env-2 (crate (name "py_env") (vers "2.0.2") (deps (list (crate-dep (name "pyo3") (req "^0.20.2") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "16r1an6h0129k7n1bw8dfgylxwrw4dq487zsqvhc7zg2h0216arq") (yanked #t)))

(define-public crate-py_env-1 (crate (name "py_env") (vers "1.2.0") (hash "0fpjknsy206mvqd03r76i9gk2ziabfn8gk4rb1kndqnw8bda1az6")))

