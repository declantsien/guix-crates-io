(define-module (crates-io sp #{80}#) #:use-module (crates-io))

(define-public crate-sp800-185-0.1 (crate (name "sp800-185") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.3") (default-features #t) (kind 0)))) (hash "1bgkjmv8744sp33m5f0a7mvyfwsp0w38ps5xkza53h7nnqzdg6iq") (features (quote (("parallelhash" "rayon"))))))

(define-public crate-sp800-185-0.1 (crate (name "sp800-185") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "rayon") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.3") (default-features #t) (kind 0)))) (hash "0rndsr4nb31q1z4bb3xl6jkm7p3sx84bzdqrnllr1w1wril6mkjp") (features (quote (("parallelhash" "rayon"))))))

(define-public crate-sp800-185-0.1 (crate (name "sp800-185") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "rayon") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.3") (default-features #t) (kind 0)))) (hash "17wsi1xj397ianrqbby8nmr78988wcwr939k8gk6d0q06gv096ns") (features (quote (("parallelhash" "rayon"))))))

(define-public crate-sp800-185-0.2 (crate (name "sp800-185") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "rayon") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4") (default-features #t) (kind 0)))) (hash "1jagn8nqh3yz389hh9yl3ydqxywfvxpclnj2jnqr1w6v3lxqxcf0") (features (quote (("parallelhash" "rayon"))))))

