(define-module (crates-io sp ex) #:use-module (crates-io))

(define-public crate-spex-0.1 (crate (name "spex") (vers "0.1.0") (deps (list (crate-dep (name "sipp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n66f28gzhnd2kswmjldlv33b8ncbgmhza8qr95c7ws8ggbfraw1")))

(define-public crate-spex-0.2 (crate (name "spex") (vers "0.2.0") (deps (list (crate-dep (name "sipp") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1hpvgz5xxn2si2djxgv8r48ngw6gqz75c7brrz2ih17mb9f09gsh")))

(define-public crate-spex-0.2 (crate (name "spex") (vers "0.2.1") (deps (list (crate-dep (name "sipp") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "024h6qrahs7x8cgv6asvwvlmbkx8a06d57nmc094xr2qgjydylch")))

