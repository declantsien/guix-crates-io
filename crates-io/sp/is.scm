(define-module (crates-io sp is) #:use-module (crates-io))

(define-public crate-spispopd-0.1 (crate (name "spispopd") (vers "0.1.0") (deps (list (crate-dep (name "clipboard-win") (req "^4.0.3") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "x11-clipboard") (req "^0.5.3") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "1d29rf3xa5m381capy66wg870v254ayrbypfylydc2zci9wnwk9n")))

