(define-module (crates-io sp dc) #:use-module (crates-io))

(define-public crate-spdcp-0.1 (crate (name "spdcp") (vers "0.1.0") (hash "1z87ydwxm9dlxbj7kihjf2rvnd0avzizddrx4bsxlbm7fw88jv07")))

(define-public crate-spdcp-0.2 (crate (name "spdcp") (vers "0.2.0") (hash "1gbixvh6cmxb646ng42r2xl5l6dhzvr195fhh9zgq9py0z65ign7")))

(define-public crate-spdcp-0.3 (crate (name "spdcp") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "198q37n7ykbl5l00d0jmgc5ybdbl8iymwqlkrc7kc154m5kg7117")))

(define-public crate-spdcp-0.4 (crate (name "spdcp") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)))) (hash "0a96m1a8n1h9114k831al4pyknwdvwfn733g1z88lnxaj7va77h0") (features (quote (("wasm" "serde" "wasm-bindgen") ("default"))))))

(define-public crate-spdcp-0.4 (crate (name "spdcp") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)))) (hash "0saf7qqp11p6nghpsi65jp0kgyj5ad254wfrzj7xszb115i2r215") (features (quote (("wasm" "serde" "wasm-bindgen") ("default"))))))

(define-public crate-spdcp-0.4 (crate (name "spdcp") (vers "0.4.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "061xaac19n6kcmcm4xzra0swy8iadq6lg6gsfbhnrsjdbfa6dx4n") (features (quote (("default"))))))

(define-public crate-spdcp-0.4 (crate (name "spdcp") (vers "0.4.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1sh7h17jzn8iykhc33klkfhmy4h5ff779ipcra1v0ikc2ca4wh74") (features (quote (("default"))))))

(define-public crate-spdcp-0.4 (crate (name "spdcp") (vers "0.4.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xgp022j2j4sqhqdrqm68zy3x8nlw83zlzrph8p4iiyx7903gzi5") (features (quote (("default"))))))

