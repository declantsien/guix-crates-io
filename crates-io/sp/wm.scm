(define-module (crates-io sp wm) #:use-module (crates-io))

(define-public crate-spwm_generator-0.1 (crate (name "spwm_generator") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "027can4ly9cqzn968lxn6m8krf2rxfxp6h9p8n4jci36phlblk7q")))

(define-public crate-spwm_generator-0.2 (crate (name "spwm_generator") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "185ms5dlww8x9b4dzpv6z9aagi7jhd722qd87jds64kw4nrffh57")))

(define-public crate-spwm_generator-0.3 (crate (name "spwm_generator") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "15sl8bbi1j91mm4ljzb1cjzgzh8xjmx9yp317y6w1q0an7v4d6nx")))

