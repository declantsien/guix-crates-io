(define-module (crates-io sp v-) #:use-module (crates-io))

(define-public crate-spv-cross-0.23 (crate (name "spv-cross") (vers "0.23.1") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 1)) (crate-dep (name "js-sys") (req "^0.3.10") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.33") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "10cihk9gj5img0fdcay4392gvwa4hvc8kkg5hf7a3hkry7hsixrp") (features (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23 (crate (name "spv-cross") (vers "0.23.2") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 1)) (crate-dep (name "js-sys") (req "^0.3.10") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.33") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "1inh117fbsfbzjscv6pgiz483ba5a66qzxzqr8wa1b6b6zyclqg8") (features (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23 (crate (name "spv-cross") (vers "0.23.3") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 1)) (crate-dep (name "js-sys") (req "^0.3.10") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.33") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "1aa7cfb9pn88yggda5f0w3qpp7nvxs52aif0nkjmlpz09da118jf") (features (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23 (crate (name "spv-cross") (vers "0.23.4") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 1)) (crate-dep (name "js-sys") (req "^0.3.10") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.33") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "0r7fcy4x55xpmd7bf2awiydd0x998iqv1wq2gyb83lsiznrkrnh9") (features (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23 (crate (name "spv-cross") (vers "0.23.5") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 1)) (crate-dep (name "js-sys") (req "^0.3.10") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.33") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "0i9lxpsd39gc9ggi1jrjgrwaggmxwyg9y04ssh0l0v1gkz9ha7hx") (features (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-rs-0.3 (crate (name "spv-rs") (vers "0.3.9") (deps (list (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)))) (hash "1rq88lg7mjxka4ial6nd596h7x09xhfbvlgdiifiksvvgl5d0lk3")))

(define-public crate-spv-rs-0.3 (crate (name "spv-rs") (vers "0.3.10") (deps (list (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)))) (hash "02551c9ai4x7lq4839c8iprhaq0r7c3wzhaix5nmzh54kn20xabw")))

(define-public crate-spv-rs-0.4 (crate (name "spv-rs") (vers "0.4.0") (deps (list (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)))) (hash "1wp7prn1sn5ixiavsj61shrcwmhgr5w16yp88j4sg7xqnjjvl4vl")))

(define-public crate-spv-rs-0.5 (crate (name "spv-rs") (vers "0.5.0") (deps (list (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)))) (hash "0hvdb86m0gdabym2is6c3jd1i0414hb4fw5ndv79ai24y1k5mk24")))

(define-public crate-spv-rs-0.6 (crate (name "spv-rs") (vers "0.6.0") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06zk80d5fk86ncbwa1flr7mnapdcbsgc4vk1h1hggyzfrk4infjn")))

(define-public crate-spv-rs-0.6 (crate (name "spv-rs") (vers "0.6.1") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lgdn5mcyvcd4j2ykdhrd03skfbskvicakji8jj5xm40k50k252n")))

(define-public crate-spv-rs-0.6 (crate (name "spv-rs") (vers "0.6.2") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1j8r6mfk0b19xys7mj3wk8d050f8yy6r1k7l4bz622kwbiwvi3k3")))

(define-public crate-spv-rs-0.7 (crate (name "spv-rs") (vers "0.7.0") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vnkb6mb60nnf65xl35aj45wjp4aiqb2mkn151p7kl298qs3c7l5")))

