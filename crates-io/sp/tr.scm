(define-module (crates-io sp tr) #:use-module (crates-io))

(define-public crate-sptr-0.1 (crate (name "sptr") (vers "0.1.0") (hash "11hy27q9b475zvnr2habrlr156hglrk83ixnv21isq4zr260p48h") (features (quote (("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.1 (crate (name "sptr") (vers "0.1.1") (hash "0jw6qzqfnizjld617rkpk506jn3zsfzszqiw58rkmgwaga8sbl7x") (features (quote (("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2 (crate (name "sptr") (vers "0.2.0") (hash "0shnn9pc77hbc5yayhs9fszvvkfbw4rayvcx042jqwnk3cmdaqjw") (features (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2 (crate (name "sptr") (vers "0.2.1") (hash "0psinx1rdxn56ansx0xrp59gp9250gck9cisinzim7d1rfyzpczr") (features (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2 (crate (name "sptr") (vers "0.2.2") (hash "18a1xrgzk6k4j289nk0frl8zblcnvbbl1dzbvrml821hj129cvgm") (features (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2 (crate (name "sptr") (vers "0.2.3") (hash "03jvvr822x5pffyvswz6crmzaf8s4y3igj85ngsc27hv6y2yxpjw") (features (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.3 (crate (name "sptr") (vers "0.3.0") (hash "1g4ag30pwgrm7x7bawihy4iwia2gml3yrxrpbh8bjvgaf5m52wga") (features (quote (("uptr") ("opaque_fn") ("default"))))))

(define-public crate-sptr-0.3 (crate (name "sptr") (vers "0.3.1") (hash "0hcryk2wh9s7bhi121xgmqyk61605777gnjq559fi48v29jl638y") (features (quote (("uptr") ("opaque_fn") ("default"))))))

(define-public crate-sptr-0.3 (crate (name "sptr") (vers "0.3.2") (hash "0shddkys046nnrng929mrnjjrh31mlxl95ky7dgxd6i4kclkk6rv") (features (quote (("uptr") ("opaque_fn") ("default"))))))

