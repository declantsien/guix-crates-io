(define-module (crates-io sp el) #:use-module (crates-io))

(define-public crate-spel-0.1 (crate (name "spel") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1d552kdjwplww0488z8icmqqs1f9ksnj49v1cs8vy7b35vml7q3l")))

(define-public crate-spel-0.1 (crate (name "spel") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pallrq7ay1vypknivbz18zav8mk7hgrxiqfzc3sdnq4aykij46k")))

(define-public crate-spel-0.1 (crate (name "spel") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0kqv2aqrpzvlp06mrjp4ybxffwqkdjcrg1kddz3qfjf8riwszr7i")))

(define-public crate-spel-0.1 (crate (name "spel") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "easy-http-request") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "1pkiybs0xc1vwvva2x1rl36i6sx48acml9w37y8gh3cg1m5qlpgn")))

(define-public crate-spel-0.1 (crate (name "spel") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1g1f2v96k2jmiplgsgpmrcq8qxp1qclbk2kvacnc0ip0ql0nhbws")))

(define-public crate-spell-0.1 (crate (name "spell") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fd4nqbf40jsjxbdc4mccizxkcx7iy885wkm51cbz2lbg8mrn0sg")))

(define-public crate-spell-0.1 (crate (name "spell") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "09gx6dh9fvpdjw603gbwj3pzypcf2i7zjnm0blv6pccywnw543l2")))

(define-public crate-spell-0.1 (crate (name "spell") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "097jffl8mwj910m313z64m68hp4asmfm50k6m2ma8zzbb78mldw9")))

(define-public crate-spell-0.1 (crate (name "spell") (vers "0.1.6") (deps (list (crate-dep (name "csv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1n685y32izlm2bbc49n5wgbi7v1dj0ghyqwvf98py7y0y6gr1g75")))

(define-public crate-spell_checker-0.1 (crate (name "spell_checker") (vers "0.1.0") (hash "1i6icwpx25xgsac2735bs91b76hl9vr0j4xgfz54fifn5wqqywq5")))

(define-public crate-spellabet-0.1 (crate (name "spellabet") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.29.0") (default-features #t) (kind 2)))) (hash "147v3lwcll0kafdmnx6bnjk30daqd0s8llwrx54v59jzrbhwg4ig") (rust-version "1.64")))

(define-public crate-spellabet-0.1 (crate (name "spellabet") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.29.0") (default-features #t) (kind 2)))) (hash "0ynkkaaijk96nsq6cmfi7l6h3m41i24fck0f7qix3a1bpf3kywkr") (rust-version "1.64")))

(define-public crate-spellabet-0.2 (crate (name "spellabet") (vers "0.2.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.29.0") (default-features #t) (kind 2)))) (hash "1jjza056p337zj0dbkjdp65x9knqmp6qcy36blxrfw8khbivv7k9") (rust-version "1.65")))

(define-public crate-spellbook-0.0.0 (crate (name "spellbook") (vers "0.0.0") (hash "0f11c7aw6qgx2hn6dcs4blg71s00zl8xljqpdwip30p9jhdjyhnl") (rust-version "1.60")))

(define-public crate-spellbound-0.1 (crate (name "spellbound") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "cocoa") (req "^0.20.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "hunspell-sys") (req "^0.1.3") (default-features #t) (target "cfg(all(unix, not(target_os = \"macos\")))") (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "objbase" "spellcheck"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "039rmbxj8534z8sy82kgxldjwf1yvz3w7zc517x65v5kr7f6wxdx")))

(define-public crate-spellbound-0.1 (crate (name "spellbound") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "cocoa") (req "^0.20.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "hunspell-sys") (req "^0.1.3") (default-features #t) (target "cfg(all(unix, not(target_os = \"macos\")))") (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "objbase" "spellcheck"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0f2qiy1mc902x25z6xwvlzb36jagyvzly87b07la3ypslahx7fx6")))

(define-public crate-spellcast-0.1 (crate (name "spellcast") (vers "0.1.0") (deps (list (crate-dep (name "clone_box") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0q2r45qbl5c1ygr432aywx1afqqvwfil2jyb47axvxpam44jyrj5")))

(define-public crate-spellcast-0.1 (crate (name "spellcast") (vers "0.1.1") (deps (list (crate-dep (name "clone_box") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1xj9fdj2j4nhnb67gixm2j49276c5w6xgxbm32vbx9sxhqdd4nwx")))

(define-public crate-spellcast-0.1 (crate (name "spellcast") (vers "0.1.2") (deps (list (crate-dep (name "clone_box") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1w8c52j96b1m3vw0k4ykql93nl3fql0604ip76km2kh19gdaiixg")))

(define-public crate-spellcheck-0.1 (crate (name "spellcheck") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "10lwylvd5qjxylr1wcplyp4mfn2lfq4604dzyy5jb1mv56ajdvah")))

(define-public crate-spellcheck-0.1 (crate (name "spellcheck") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0msqrihbsbppj2a78ggz2rqh340yrqqz23k0ial5bzqglb2l0hk4")))

(define-public crate-spellcheck-0.1 (crate (name "spellcheck") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1dwm38rh4zzjvcwi9ivq05cplgmilvphqigkr65d65wramz1i1nr")))

(define-public crate-spellcheck-rs-0.1 (crate (name "spellcheck-rs") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.6.0-alpha.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "0ahqf1abn8f3m948d2bkijiqrczvcdbdyzxjryvm7d7hbrl1qnll")))

(define-public crate-spellcheck_toy-0.2 (crate (name "spellcheck_toy") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "stringedits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05hgxf5c271wchrsmzz623ydbjfg9ibspsp2akj7p9i6lgb2vma0")))

(define-public crate-spellcheck_toy-0.3 (crate (name "spellcheck_toy") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "stringedits") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1hdxjs2nzgff16arnbl55bznyl05a6ckkdq2rmyn3iqxx43xijxi")))

(define-public crate-spellcheck_toy-0.3 (crate (name "spellcheck_toy") (vers "0.3.1") (deps (list (crate-dep (name "stringedits") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0y14c3j36vhn8z5dr1nai6c0y4sfkz1hbswpkiglrx9vyh6w54v4")))

(define-public crate-spellcheck_toy-0.3 (crate (name "spellcheck_toy") (vers "0.3.2") (deps (list (crate-dep (name "stringedits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1dnqyw4mjk3n2iydqb2cvmam2a10p6zj4snx6vzml14bdrmflqj0")))

(define-public crate-spelling-0.1 (crate (name "spelling") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "1iv270g48h84hf6f00r9nhvwf9d8n15rgp80x1yb3zaarhfkf3nd")))

(define-public crate-spelling-0.2 (crate (name "spelling") (vers "0.2.0") (deps (list (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "0r6j2hyvm4l1nb13c4vf2d41y970ab7fr45nsy8fqyfaj7mbn3k0") (features (quote (("use_rayon"))))))

(define-public crate-spelling-0.2 (crate (name "spelling") (vers "0.2.1") (deps (list (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "0frpvmlk36pdb5pzijzgjwvplh9ripasfk80dx7f3m9zvxpf1vdc") (features (quote (("use_rayon"))))))

(define-public crate-spelling-0.2 (crate (name "spelling") (vers "0.2.2") (deps (list (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "06q79r31ifczl78q5q7f9sx7ikzsrj9mhn5jm18b9wzl3zshy2n7") (features (quote (("use_rayon"))))))

(define-public crate-spelling-0.2 (crate (name "spelling") (vers "0.2.3") (deps (list (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)))) (hash "0ljv153yxkfa98lc2lpslg7yf7350rly24xmshff1q0a5miqzk11") (features (quote (("use_rayon" "rayon") ("default" "use_rayon"))))))

(define-public crate-spelling-0.2 (crate (name "spelling") (vers "0.2.4") (deps (list (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)))) (hash "0arzdzm9y65pj2kzmflfvbsd911zkiiri4r26s1wjcxqxlzhllvh") (features (quote (("use_rayon" "rayon") ("default" "use_rayon"))))))

(define-public crate-spelling_bee_solver-0.1 (crate (name "spelling_bee_solver") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zvrqp8ag0wqf9kg6fws6zydiqxnr2crm6yxc3mjaazp548bgiiz")))

(define-public crate-spelling_corrector-0.1 (crate (name "spelling_corrector") (vers "0.1.0") (deps (list (crate-dep (name "generator") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1vmvp88qzag8i1yms70qnji8icj6p7l9q5qjrxp7vy4mpcgbl1d4")))

(define-public crate-spelling_corrector-0.1 (crate (name "spelling_corrector") (vers "0.1.1") (deps (list (crate-dep (name "generator") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "0wfzfyhwzv4a7p4d39agqg86drn58ppq9fhvhvz92lmrzxpv9hkp")))

(define-public crate-spelling_corrector-0.2 (crate (name "spelling_corrector") (vers "0.2.0") (deps (list (crate-dep (name "generator") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "0326xjlrn6a8zqbqandbmhzmlavkjsj5pca44h9mkr8lvrk6kqpk")))

(define-public crate-spelling_corrector-0.2 (crate (name "spelling_corrector") (vers "0.2.1") (deps (list (crate-dep (name "generator") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "0g591aspr0vqjff08xaxsx0qq5zlkpv9l4d4c79sg6wcczf05fp8")))

(define-public crate-spelling_corrector-0.2 (crate (name "spelling_corrector") (vers "0.2.2") (deps (list (crate-dep (name "generator") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1m6nnzgw2mcg3qds0ldxj9k2pq2cd9xgz0ki25f0l4x533xqfbjr")))

(define-public crate-spelling_corrector-0.2 (crate (name "spelling_corrector") (vers "0.2.3") (deps (list (crate-dep (name "generator") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1jxdch966x0k7nyjlilj5wxrd3g1kysv3vqwr0mpbf7qzndlaqd8")))

(define-public crate-spellingbee-0.2 (crate (name "spellingbee") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sk4qk5g3nd1in88ddi1avpkr3w3hzw1lj9g7gvy188pl1017xx1")))

(define-public crate-spellingbee-0.3 (crate (name "spellingbee") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jw2hsnz41p6pac8n4ay5hdlbdl4rxi2cpxralflxcsyygskszy3")))

(define-public crate-spellingbee-1 (crate (name "spellingbee") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0vdg2gj69znd55pj61jwsqv3l0ayc0k5f6xfn2lgm9k0by1w6bi3")))

(define-public crate-spelltest-0.1 (crate (name "spelltest") (vers "0.1.0") (hash "09j93nqxaafdb18plh06xsgghbrsigqbyyg4vqnikm0n5wl7yp2x")))

(define-public crate-spelltest-0.1 (crate (name "spelltest") (vers "0.1.2") (hash "0aakin61a42rswxkgc1yisiksixnc7wgpxn3lrvp39sfrwgng3va")))

(define-public crate-spelltest-0.1 (crate (name "spelltest") (vers "0.1.3") (hash "1aik1ps1n4b0qkg5s63m4vwkg0h5yxc5xwdylgfg88sh40pzr6f4")))

(define-public crate-spelltest-0.2 (crate (name "spelltest") (vers "0.2.0") (hash "17kg298pay9k6y6j988wsxzak4rhhjv5cw3sl9acrhql4wnkkhdy")))

