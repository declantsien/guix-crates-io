(define-module (crates-io sp ik) #:use-module (crates-io))

(define-public crate-spike-0.0.0 (crate (name "spike") (vers "0.0.0") (hash "1bh9fwa93cpmiy4ypczxabxa1wyfqqggsqygy6wlyrd9hh78xbzn")))

(define-public crate-spike-0.0.1 (crate (name "spike") (vers "0.0.1") (deps (list (crate-dep (name "matchit") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "touche") (req "^0.0.8") (features (quote ("server"))) (kind 0)))) (hash "1ggnywwzb8vbjrvwc4nwjkly6bkyjjilf9cm2x7lx5jxdixh33qw")))

(define-public crate-spike-0.0.2 (crate (name "spike") (vers "0.0.2") (deps (list (crate-dep (name "matchit") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "touche") (req "^0.0.9") (features (quote ("server"))) (kind 0)))) (hash "0xl1pyxipw12wmcnwyvcp1pnjs5i0jk2kvmz0qgx49pl178k3xxq") (features (quote (("json" "serde" "serde_json"))))))

(define-public crate-spike-dasm-wrapper-0.0.1 (crate (name "spike-dasm-wrapper") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0jhhcidajkspdb9fkabqbhzbsqslhiv3di3v79zap1ss38qmq1mp")))

(define-public crate-spike-dasm-wrapper-0.0.2 (crate (name "spike-dasm-wrapper") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "096p2x9lf0m2cs35rhbcddb4b107dqbpylh9g53ml11wrvvmanr3")))

(define-public crate-spike-rs-0.1 (crate (name "spike-rs") (vers "0.1.0") (hash "1z26fsa3k73vrpw3lbyw60zj3v75b7w0pyf813mhv6rk8kjasjya")))

(define-public crate-spike-sys-0.1 (crate (name "spike-sys") (vers "0.1.0") (hash "11pm2cbcshxbab8nbl92aw8vnxw5v92j90nnas1mp947nhwfygpi")))

(define-public crate-spike-sys-0.1 (crate (name "spike-sys") (vers "0.1.1") (hash "0i6pnwbr1vdih250811c955ff23m5ckrjgvrg01zms95qa9spy8w")))

(define-public crate-spike-sys-0.1 (crate (name "spike-sys") (vers "0.1.2") (hash "184hv79zkva55w651hsilpxs768mxxaxldvzgxrgw5602ia6wk5c")))

