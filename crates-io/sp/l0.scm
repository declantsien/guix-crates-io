(define-module (crates-io sp l0) #:use-module (crates-io))

(define-public crate-spl06-007-0.1 (crate (name "spl06-007") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "14f8ff8g5m3klnmsbkdn7q0hz0fvjfj5drs8djmvmwbcbg99zh3j")))

(define-public crate-spl06-007-0.2 (crate (name "spl06-007") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "16dd5f7fm2y21yd1zg6glqk5rs1wfg7a54k5c22mw90apfgahhfv")))

(define-public crate-spl06-007-0.2 (crate (name "spl06-007") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "003f0i0hncxxh3v06rdm2nf7nz8qirvbnh36y8m8j3aadn90jsvx")))

(define-public crate-spl06-007-0.2 (crate (name "spl06-007") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)))) (hash "0yy4aa58mpihlz8aw5kqsfrapskg81z1hfgmn7ms7z5w6f3bh46y")))

(define-public crate-spl06-007-0.3 (crate (name "spl06-007") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)))) (hash "0bz4mms9nnba4ni2kmrcpak882b2pg4azkhb2cykfygvxs8y1hfc")))

(define-public crate-spl06-007-0.3 (crate (name "spl06-007") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)))) (hash "11h2pc3p8qr7q7qdf53mx76piiapw9ns59r4m896hpgn4yyvkn6r")))

(define-public crate-spl06-007-0.3 (crate (name "spl06-007") (vers "0.3.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)))) (hash "03msjrr28famwswy0v4rjymmys36z3bj207jcj84kn24zkdjyy7i")))

(define-public crate-spl06-007-0.3 (crate (name "spl06-007") (vers "0.3.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)))) (hash "163zcpb1h7l2vb1pagkngyah5yz1ig4fpa0ycksrqk7wh43da71p")))

