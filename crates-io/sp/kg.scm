(define-module (crates-io sp kg) #:use-module (crates-io))

(define-public crate-spkg-0.0.0 (crate (name "spkg") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "175ak1ln1d09gvac3c4ipjrxz80pak7sybdaqml5rm99yhvda5c1")))

