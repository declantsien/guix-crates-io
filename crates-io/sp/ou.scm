(define-module (crates-io sp ou) #:use-module (crates-io))

(define-public crate-spout_texture_share-0.1 (crate (name "spout_texture_share") (vers "0.1.0") (deps (list (crate-dep (name "autocxx") (req "^0.22.4") (default-features #t) (kind 0)) (crate-dep (name "autocxx-build") (req "^0.22.4") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^4.3") (features (quote ("fancy"))) (default-features #t) (kind 1)))) (hash "1zw16yxnr0fgaqidp0fmj1k1fnxqa79w6bhmj1d4pqa27giacfk5")))

