(define-module (crates-io sp uz) #:use-module (crates-io))

(define-public crate-spuz-0.0.0 (crate (name "spuz") (vers "0.0.0") (hash "132gbblda8xw6p2x6762fjc3lh496silslminl2a71w65vjh994g")))

(define-public crate-spuz_folder-0.0.0 (crate (name "spuz_folder") (vers "0.0.0") (hash "0wl8qn6c5h3n76vsn4rvjjg28xk2k26p4d9hzpbai7vyq0b14qac")))

(define-public crate-spuz_get-0.0.0 (crate (name "spuz_get") (vers "0.0.0") (hash "1cxjp30hmn6mm7lrmlqaygd16bcd0c8fyyld75cmk8x6c6jraq9s")))

(define-public crate-spuz_piston-0.0.0 (crate (name "spuz_piston") (vers "0.0.0") (hash "10l318iq7kr13cpksi2az65v36ri6fn2vlij51w03gkg0x3ggcca")))

(define-public crate-spuz_piston-0.1 (crate (name "spuz_piston") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "01kymm74rkx3g8n96bzg016361kmjy0jvd271plw9zajqqadcdys")))

(define-public crate-spuz_piston-0.1 (crate (name "spuz_piston") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "03dny33k623kf746py0hby1hbahivbazjzfm2d3lrmjrkh414wvl")))

(define-public crate-spuz_spawner-0.0.0 (crate (name "spuz_spawner") (vers "0.0.0") (hash "1yvq4imgark824x0f127a60djimac2ivv1d68wf0nnx80g48nakc")))

(define-public crate-spuz_wrench-0.0.0 (crate (name "spuz_wrench") (vers "0.0.0") (hash "026x8sfdm435vyxacgmnddd47zb6lrxkrmsva1ymy6wbklvd14m8")))

