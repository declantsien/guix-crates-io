(define-module (crates-io sp ez) #:use-module (crates-io))

(define-public crate-spez-0.1 (crate (name "spez") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "spez-macros") (req "= 0.1.0") (default-features #t) (kind 0)))) (hash "0h08wfhzp4nxk8b6pdzqrnzivlsqnzacd72aqk42la83gki960hx")))

(define-public crate-spez-0.1 (crate (name "spez") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "spez-macros") (req "= 0.1.0") (default-features #t) (kind 0)))) (hash "12rnryxfb68psyw3ynnnm8p8fby2g58cva3v41xzfycqlzb2yk9h")))

(define-public crate-spez-0.1 (crate (name "spez") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s4619r9p3z6avb1hxhbg548kgj6sa6xxgbbp3p8h9ya9l7rczn8")))

(define-public crate-spez-macros-0.1 (crate (name "spez-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p6i2z84a8jq0k3v4i7aq60l57mw5q32j48mvkd6rjwcgl1hdyn7")))

