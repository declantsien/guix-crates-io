(define-module (crates-io sp yb) #:use-module (crates-io))

(define-public crate-spybeep-0.1 (crate (name "spybeep") (vers "0.1.0") (deps (list (crate-dep (name "freqiterator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mki") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "1bv6mhbiavp9n60j0i9r7yb7bbkis07nlk4r0wa03fiiaxz54asy")))

(define-public crate-spybeep-0.1 (crate (name "spybeep") (vers "0.1.1") (deps (list (crate-dep (name "freqiterator") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mki") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "0xljl1rixyqdsv5xmdda7kiap9i2igf3sbi7q91mc91dcsfvqcrk")))

(define-public crate-spybeep-0.2 (crate (name "spybeep") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "freqiterator") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mki") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "0k1yj3vipdrpi1zjxb1zfm72wxvkmi2sd6wq1wa9jf8wpannl7i5")))

(define-public crate-spybeep-0.2 (crate (name "spybeep") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "freqiterator") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mki") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "08rcwyjbvzdr6xwvydabzy1zf920czf6p04m5sg8y4j21jfpk2xn")))

(define-public crate-spybeep-0.2 (crate (name "spybeep") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "freqiterator") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mki") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "0znggihf51vxmg2g7fxdpyh9vwwzph38fh7yv10r5rkxwxvfgw1f")))

(define-public crate-spybeep-0.2 (crate (name "spybeep") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "freqiterator") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mki") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "111hr2lq7r8jxv1l3cjsp373lg29w8xn667fsbs0bnxmmpa6vz1h")))

(define-public crate-spybeep-0.3 (crate (name "spybeep") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "freqiterator") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "mki") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17") (kind 0)))) (hash "1xhghjigzyqznhlgvbhzyr89kh40wf3dkpkb6rfi0rk7f7l7w0q3")))

