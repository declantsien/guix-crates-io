(define-module (crates-io sp _v) #:use-module (crates-io))

(define-public crate-sp_vcard-0.1 (crate (name "sp_vcard") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "sp_vcard_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0d4havvkk3r1y8m8m57yh7n5fvnshgfprcqjlwf9z4mbb4ly0iip")))

(define-public crate-sp_vcard-0.2 (crate (name "sp_vcard") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "sp_vcard_derive") (req "^0") (default-features #t) (kind 0)))) (hash "0wvmjhqcjrczvzj244s5h9582nnq8qg4j4akc83arh9ngcwplx6k")))

(define-public crate-sp_vcard_derive-0.1 (crate (name "sp_vcard_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pdb7iqkdq2rzz3310mxxrsxdcrrrww05rvc5jb9j09k9vnj4bp5")))

(define-public crate-sp_vcard_derive-0.2 (crate (name "sp_vcard_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "000xd5jz1im75njlcajbrxfapm91llrwd2i0p35dlcg129jnmz5g")))

