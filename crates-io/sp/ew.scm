(define-module (crates-io sp ew) #:use-module (crates-io))

(define-public crate-spew-0.1 (crate (name "spew") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.10") (kind 0)) (crate-dep (name "bevy") (req "^0.10") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "0wwkap8k1mqlzqwi2cbl510rx4pn1pv75iangpgwzgzn2qx8gwc1")))

(define-public crate-spew-0.1 (crate (name "spew") (vers "0.1.1") (deps (list (crate-dep (name "bevy") (req "^0.10") (kind 0)) (crate-dep (name "bevy") (req "^0.10") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "16imi5zmxifgl5q83b53vv59gwh6xk5qpi1k3asq00ddkqxw1k7a")))

(define-public crate-spew-0.2 (crate (name "spew") (vers "0.2.0") (deps (list (crate-dep (name "bevy") (req "^0.10") (kind 0)) (crate-dep (name "bevy") (req "^0.10") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "1ka0v2xzv3p4pw55ha1hlmd5aqbk81l1dpcys0g8vkai8fr7qn2f")))

(define-public crate-spew-0.2 (crate (name "spew") (vers "0.2.1") (deps (list (crate-dep (name "bevy") (req "^0.10") (kind 0)) (crate-dep (name "bevy") (req "^0.10") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "1aiqyjbwbj6fag2b7ir9ps9r78v737zvac5d8rn6p9d0jwvxxplb")))

(define-public crate-spew-0.2 (crate (name "spew") (vers "0.2.2") (deps (list (crate-dep (name "bevy") (req "^0.10") (kind 0)) (crate-dep (name "bevy") (req "^0.10") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "1yghx8srggmrf2gvlr0k75h944gd0dylk3bw3d6w2pm8mijbfphx")))

(define-public crate-spew-0.3 (crate (name "spew") (vers "0.3.0") (deps (list (crate-dep (name "bevy") (req "^0.11") (kind 0)) (crate-dep (name "bevy") (req "^0.11") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "1g3ppjj3hf3h7hnrmnb37j87aydympcrwf5wpmbbfng9a79xhwa0")))

(define-public crate-spew-0.4 (crate (name "spew") (vers "0.4.0") (deps (list (crate-dep (name "bevy") (req "^0.12") (kind 0)) (crate-dep (name "bevy") (req "^0.12") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "0sqqwfx3gjqr12ci6piz4v30h3i8x1hybwvrh7yr590i89svqw62")))

(define-public crate-spew-0.5 (crate (name "spew") (vers "0.5.0") (deps (list (crate-dep (name "bevy") (req "^0.13.0") (kind 0)) (crate-dep (name "bevy") (req "^0.13.0") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "15m6cg7s9lw76q7vy8ayj5mbvy4jmf6g318acqdcnq4a9lx3wafc")))

(define-public crate-spew-0.5 (crate (name "spew") (vers "0.5.1") (deps (list (crate-dep (name "bevy") (req "^0.13.0") (kind 0)) (crate-dep (name "bevy") (req "^0.13.0") (features (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (kind 2)))) (hash "0i4pr74wvfxinx57w22sddd57frjcyz8saajl235cvacwv92x8hd")))

