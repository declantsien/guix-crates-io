(define-module (crates-io sp s3) #:use-module (crates-io))

(define-public crate-sps30-0.1 (crate (name "sps30") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "ieee754") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sensirion-hdlc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17my9fx0248gi631nfmsf44rlfjhjqy30qh4rczm8q7j5sgd20mv")))

(define-public crate-sps30-i2c-0.0.1 (crate (name "sps30-i2c") (vers "0.0.1") (hash "0g05ib6hl9lgq5h3r7mqyfw5h614c741ks2iqyfx43yg8qr6nlv9") (yanked #t)))

(define-public crate-sps30-i2c-0.1 (crate (name "sps30-i2c") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "03yp25477wpvyczz18vgmprv7pad7j4f2kdqbs7zq0921pwkmdzg")))

