(define-module (crates-io sp gl) #:use-module (crates-io))

(define-public crate-spglib-0.1 (crate (name "spglib") (vers "0.1.0") (deps (list (crate-dep (name "spglib-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n071xx9pzrcvn9xx01laxm1j1pasxr6ra7vpj45pj96kajhrjbs") (yanked #t)))

(define-public crate-spglib-0.1 (crate (name "spglib") (vers "0.1.1") (deps (list (crate-dep (name "spglib-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gas2609dm6j053i7fpxpwjygw7pcz8lvx5znflb047n2z2qwx27") (yanked #t)))

(define-public crate-spglib-0.1 (crate (name "spglib") (vers "0.1.2") (deps (list (crate-dep (name "spglib-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lqih76gy5gz3pj9mq96h4vd259ripanjy09kif46mlr7pbw675a")))

(define-public crate-spglib-0.1 (crate (name "spglib") (vers "0.1.3") (deps (list (crate-dep (name "spglib-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "01fx7is57xf7w6v0kgk5n4glc9alwrxswcixq8njggxv6yig335y")))

(define-public crate-spglib-1 (crate (name "spglib") (vers "1.15.1") (deps (list (crate-dep (name "spglib-sys") (req "^1.15") (default-features #t) (kind 0)))) (hash "070iyb23zqcaxgd9kd03xccp5cmj44g9cbp5dcdbdc0hpkf67kpw")))

(define-public crate-spglib-sys-0.1 (crate (name "spglib-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "0s8dq9p9aw5pp2bf65n5ldg59v5bzzl2m7m9khp27wxfbaas98ca")))

(define-public crate-spglib-sys-0.1 (crate (name "spglib-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "0njrpp9m9ls76zfayx5ni0853zfs410kb4zxh83lb0dg378grbd6")))

(define-public crate-spglib-sys-1 (crate (name "spglib-sys") (vers "1.15.1") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "06p7vyf6vmc4lnd7hw5di0csa8cdimfqzq9kcvvp1ak9f67lmsxd") (yanked #t)))

(define-public crate-spglib-sys-1 (crate (name "spglib-sys") (vers "1.15.1+patch1") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0932zlmvq116dxrz058wrvxcmbvlwjf5244b3sh4wwqpmpw1f8vk")))

(define-public crate-spglib-sys-1 (crate (name "spglib-sys") (vers "1.16.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1pc0yvz4bxgmi9izg4dq5spvk2jzmfa0ad7k5zwydwxm7rh4qjb3")))

