(define-module (crates-io sp il) #:use-module (crates-io))

(define-public crate-spilo-0.0.0 (crate (name "spilo") (vers "0.0.0") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "irc") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z4h6g24lv1mcz35hv3w7y7lf1nj13p9v44y9k7s3d08q5m62xcz")))

