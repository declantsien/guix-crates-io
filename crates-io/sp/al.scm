(define-module (crates-io sp al) #:use-module (crates-io))

(define-public crate-spalinalg-0.0.0 (crate (name "spalinalg") (vers "0.0.0") (hash "10zz2safj5xbmf78waim7kawmai491dm0yyznbpdizgwas8haxpc")))

(define-public crate-spalinalg-0.0.1 (crate (name "spalinalg") (vers "0.0.1") (hash "1g1zx6r0bysm5yqac2l54yclm32g99w39r57n00jdrgfad00crdc")))

(define-public crate-spalinalg-0.0.2 (crate (name "spalinalg") (vers "0.0.2") (hash "00bs91djsdlf7wz9s5qnp7lrlq7kpqkph17rdpz3j44nz5d3zrvp")))

(define-public crate-spalm-0.1 (crate (name "spalm") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0gwsyfszqq1jnjmk3hih73p4yrsk0axhhal5wccdsbskfqp5g1ya") (yanked #t)))

(define-public crate-spalm-0.1 (crate (name "spalm") (vers "0.1.0-alpha") (deps (list (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1lcpwrdzd60p3231q2fd4jmffhfirqa407wngs3sl9hlh3gncaga")))

(define-public crate-spalm-0.1 (crate (name "spalm") (vers "0.1.1-dev") (deps (list (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1r7c5n979sjwwarr2zh3zgxknqz6azkxs7na3pqz009vpql2ackw")))

