(define-module (crates-io sp lo) #:use-module (crates-io))

(define-public crate-splop-0.1 (crate (name "splop") (vers "0.1.0") (hash "1z5pz7xqbac0bmxmdv2r375qv2dlc09mmj99wg9r9cjj5anbznv8")))

(define-public crate-splop-0.2 (crate (name "splop") (vers "0.2.0") (hash "0kjp073gbr7xqi2lyfaq0vpm7psf1lpd6i5xzn9shv5flmxzlgs5")))

(define-public crate-splot-0.1 (crate (name "splot") (vers "0.1.0") (hash "10dqj5hvrd347v7qb6v9pvliv0qh779m5jssgy0hm1w50d8sz8c0")))

(define-public crate-splotch-0.0.1 (crate (name "splotch") (vers "0.0.1") (deps (list (crate-dep (name "footile") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pointy") (req "^0.3") (default-features #t) (kind 0)))) (hash "11x9qzxmrv1gfc4wfzmfn2k7cp8pd31jcs9k8fikiw4310dsc64z")))

