(define-module (crates-io sp -y) #:use-module (crates-io))

(define-public crate-sp-yew-0.1 (crate (name "sp-yew") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sp-dto") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.17") (features (quote ("web_sys"))) (default-features #t) (kind 0)))) (hash "129jvwqlhhc0sgkp6mvb7mjks5mfg90vbjjj9nkp0vcvm8ixigsz")))

