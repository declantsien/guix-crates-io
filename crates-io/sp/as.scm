(define-module (crates-io sp as) #:use-module (crates-io))

(define-public crate-spaserver-0.2 (crate (name "spaserver") (vers "0.2.0") (deps (list (crate-dep (name "actix") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.5") (default-features #t) (kind 0)))) (hash "1965yyr8ss2drj116ypvfx3jqdyl99agkjxx168r6ndyc9qbiy3h")))

