(define-module (crates-io sp sc) #:use-module (crates-io))

(define-public crate-spsc-0.0.0 (crate (name "spsc") (vers "0.0.0") (hash "0zfq3r1y5gqzad0sjl3sg5knz6y4rdq9cqjy3hd8m3ljfc2sqgnh")))

(define-public crate-spsc-bip-buffer-0.1 (crate (name "spsc-bip-buffer") (vers "0.1.0") (deps (list (crate-dep (name "deterministic") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0bpwmg56zlzg42rh5pja6llpshz2gx6dd4c854dz6jk5l2m5zp2b") (features (quote (("test_nosched") ("default"))))))

(define-public crate-spsc-bip-buffer-0.1 (crate (name "spsc-bip-buffer") (vers "0.1.1") (deps (list (crate-dep (name "cache_line_size") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "core_affinity") (req "^0.5.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0pvbncpf8yrwi8qav6l5w1bh50nzg08l6vx1w4z8bhbxf3jc93lw") (features (quote (("default"))))))

(define-public crate-spsc-bip-buffer-0.2 (crate (name "spsc-bip-buffer") (vers "0.2.0") (deps (list (crate-dep (name "cache_line_size") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "core_affinity") (req "^0.5.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "streaming-harness-hdrhist") (req "^0.2") (default-features #t) (kind 2)))) (hash "1r7wlpz3p8242664p662dw4zr2g0kkvw973407asd759av7a8037") (features (quote (("default") ("debug"))))))

(define-public crate-spsc-bip-buffer-0.2 (crate (name "spsc-bip-buffer") (vers "0.2.1") (deps (list (crate-dep (name "cache_line_size") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "core_affinity") (req "^0.5.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "streaming-harness-hdrhist") (req "^0.2") (default-features #t) (kind 2)))) (hash "0gwxg69iiypv4vz2k7fqzqskp5872h5pbdlc9kbi1d1q5r8kb28j") (features (quote (("nightly_perf_example") ("default") ("debug"))))))

(define-public crate-spsc-bounded-queue-0.0.1 (crate (name "spsc-bounded-queue") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.39") (default-features #t) (kind 2)))) (hash "0gcy2mjfjkkrcr2j9rc2faz0574gy8bjk02w20l0i8anf3pnvi2a") (yanked #t)))

(define-public crate-spsc-buffer-0.1 (crate (name "spsc-buffer") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "186yl97j4h9xfclzpqqn0bki5mhr15grc7img0hsw92ciyig19g4")))

(define-public crate-spsc-buffer-0.1 (crate (name "spsc-buffer") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0fsv5zpxkax2n46flxhyajq1yblgh8f33la39gp86hksqcwkyv5y")))

(define-public crate-spsc-queue-0.1 (crate (name "spsc-queue") (vers "0.1.0") (hash "1i4bmnsx4wz5qfsygk4bqgc2mrqzx5s3q6pvwlc69x4bsq9nmkji")))

(define-public crate-spsc-ringbuf-core-0.1 (crate (name "spsc-ringbuf-core") (vers "0.1.0") (hash "0gvha7kbd1vyplk2zlykrkkzdw9c6y9hvbxw507s1lsk6d7a1mcn")))

