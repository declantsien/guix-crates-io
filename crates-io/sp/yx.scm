(define-module (crates-io sp yx) #:use-module (crates-io))

(define-public crate-spyxpo-0.0.1 (crate (name "spyxpo") (vers "0.0.1") (hash "11i02s35r6swkhs1m6640kmv1yfqn2gqrrmnfzmwqjnak4agr68f")))

(define-public crate-spyxpo-0.0.2 (crate (name "spyxpo") (vers "0.0.2") (hash "0n00l947w7dcykb6wrh1ng6k422wa21cvnibibh14np0292jgq0m")))

