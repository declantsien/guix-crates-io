(define-module (crates-io sp ie) #:use-module (crates-io))

(define-public crate-spielrs_diff-0.1 (crate (name "spielrs_diff") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("fs" "macros" "rt-threaded" "stream"))) (default-features #t) (kind 0)))) (hash "09adz1hh5dibqq4qs30j54yzdn66k912nqaj0zppfp3xmnv2napi")))

(define-public crate-spielrs_diff-0.2 (crate (name "spielrs_diff") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("fs" "macros" "rt-threaded" "stream"))) (default-features #t) (kind 0)))) (hash "0jc6kfq3s1wdw8li1szazmq4sa9fyksf6v4sn5mvlc3chbj2jyyg")))

(define-public crate-spielrs_diff-0.2 (crate (name "spielrs_diff") (vers "0.2.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.7.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jn3lfsgzbkk9m513q1z3wdchja540aqb1cz3d6inbjmqp8kzbba")))

