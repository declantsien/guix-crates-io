(define-module (crates-io sp oo) #:use-module (crates-io))

(define-public crate-spooder-0.1 (crate (name "spooder") (vers "0.1.0") (hash "0kgrf29iiyqczkv85ki837d52g1090k7h821dgvcakwdjdfj81yd")))

(define-public crate-spook-0.1 (crate (name "spook") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.11") (default-features #t) (kind 0)))) (hash "1jwpp75agkg1gzs91lh3gv2swbsf53cdny61wryj8v9mm983isnn")))

(define-public crate-spook-0.1 (crate (name "spook") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.17") (default-features #t) (kind 0)))) (hash "07m816lvhl5pkzmw67ick54iixa8ifqqx0a7iv63pwm09770qrw8")))

(define-public crate-spook-0.1 (crate (name "spook") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.34") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "httparse") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.17") (default-features #t) (kind 0)))) (hash "14syiy7dqzsd2y6vz6nzqlbqphzrvbbgkcn0223ihj7fivmfa1pn")))

(define-public crate-spooks-0.0.0 (crate (name "spooks") (vers "0.0.0") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0pfi7bgx47n3rlw20b0l5mri6i4hb6kl89kjcvwhm1r56kycxdp3")))

(define-public crate-spooks-0.1 (crate (name "spooks") (vers "0.1.0") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0xvra84x7r9aj6l75y2241bxkwkqa1k2rxndlj9gpbx0s0drln72")))

(define-public crate-spooks-0.1 (crate (name "spooks") (vers "0.1.2") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0cxlsjc6l6791sfb0f7kjl9w3vmsqy2g0dx0pjd0jnppid6gl3sm")))

(define-public crate-spooks-0.2 (crate (name "spooks") (vers "0.2.0") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0lhvg374gkbas2fspglca1xmjnd449azjv427rk0xqfp88k6xkzm")))

(define-public crate-spooks-0.2 (crate (name "spooks") (vers "0.2.1") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0w1b0piglic7hz0kc610w0l8fq77ylmkp0vsz9gkricdak8ddps8")))

(define-public crate-spooks-0.2 (crate (name "spooks") (vers "0.2.2") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1mg5c7fpk51nml01d0qqm3kiizvcnzq08x1f93bp28mhijkipacq")))

(define-public crate-spooks-0.2 (crate (name "spooks") (vers "0.2.3") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0fn3csqzif1yksppki68h2zsij2ckry08df1wmm14hbn7gsm7na3")))

(define-public crate-spooks-0.2 (crate (name "spooks") (vers "0.2.4") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0chacn06rgwh4y480ww2jxiar7i55k0kqpcd20sppqy5klxf3kf3")))

(define-public crate-spooks-0.2 (crate (name "spooks") (vers "0.2.5") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0k6cppnwblw5ax8fw9yaj98gvly27b6a3pl5saxw2znz2pmj1pgc")))

(define-public crate-spooks-0.2 (crate (name "spooks") (vers "0.2.6") (deps (list (crate-dep (name "easy_reader") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0jxd9qw5xhnp01v58w4i48vifbb5533a8jcq4v17qgqwar61n04d")))

(define-public crate-spooky-0.1 (crate (name "spooky") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1vd17bjj79srqkwh78xbryv0v5p819yc72c9aiapprj86c7vrj8b")))

(define-public crate-spooky-0.1 (crate (name "spooky") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1wjxym8xqh1lwvs5nlhmxp3gg74k12jpixkd4zyjfra5q1md2x3y")))

(define-public crate-spooky-season-0.0.0 (crate (name "spooky-season") (vers "0.0.0") (hash "10hbqmg45lxgiafjsg9mhy91kkq9cdm59abqdrvfk9875fy682ay")))

(define-public crate-spooky-season-1 (crate (name "spooky-season") (vers "1.0.0") (deps (list (crate-dep (name "time") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1cj2fbszdigsgl8b1c631kq9pkn04y9sv2zks6qmzmr5yi41nk65")))

(define-public crate-spool-0.1 (crate (name "spool") (vers "0.1.0") (hash "1icyf5mdr7js0arfjbnd71hfi21czgrn2phgp1fpafwk5zk3k8y7")))

(define-public crate-spool-0.1 (crate (name "spool") (vers "0.1.1") (hash "1r2307k0fqczmxw601w2h5xzlcyvsyl0cdr4rv6s212765b2il7f")))

(define-public crate-spool-0.1 (crate (name "spool") (vers "0.1.2") (hash "00ddn40v67yd393gn07fisc1sjjncsgl1y8ahmlf1dw4ags9x87d")))

(define-public crate-spool-0.1 (crate (name "spool") (vers "0.1.3") (hash "16p8jgpy3zs9h1c0021h26y0s1vmmia3i86nimz3y81ch2zvmzrk")))

(define-public crate-spooles_ffi-0.1 (crate (name "spooles_ffi") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "03851dmbay44adhnv6i1bb5gv16gw8hcj64mmgiwa2g9p5ssznh5") (yanked #t)))

(define-public crate-spooles_ffi-0.1 (crate (name "spooles_ffi") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.73") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "1wb0qbg4ify8x3clsvvbb35p3xznzvr54z44zz0zacmcy1xgchpk")))

(define-public crate-spoolq-0.1 (crate (name "spoolq") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "textnonce") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1n47dfvjkgnn9mg7jn62q5gclbafa5nl9bpwra440rwm2aj5cmsm")))

(define-public crate-spoolq-0.1 (crate (name "spoolq") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "textnonce") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1iakd6jv60h78sa7wx98cbfnigngfvbapygbl6g6mxf9ynhkls7s")))

(define-public crate-spoolq-0.2 (crate (name "spoolq") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "textnonce") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1x2ivyg0s56baqwnv1zdb2sq6x44a4mhhl888g2j374ih9d605na")))

(define-public crate-spoolq-0.2 (crate (name "spoolq") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "textnonce") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "00gbx2bab2gcf7h92jwh2pdmxp6nifmvsn0b0chbyymd3j8wsx7g")))

(define-public crate-spoolq-0.2 (crate (name "spoolq") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "textnonce") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "05vm7wgjn71fvg67y53s1zb1kb35xsba83w8nv52lwxwaypmfcf3")))

(define-public crate-spoolq-0.2 (crate (name "spoolq") (vers "0.2.3") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "textnonce") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "03a1s6rprcs1pq5dbxx84sh4wy9nr3wqgl2yjlr4ibl8k93glsv1")))

(define-public crate-spools-0.1 (crate (name "spools") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jqaczxlnngw8dmiy6wl9m8dri24jaa3x9aqk5zmb6hqwix7qfav")))

(define-public crate-spools-0.2 (crate (name "spools") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06bsajacjdpifb9g5hjfkyqn4n65ng861pbdx7nm7xrskz7klz3z")))

(define-public crate-spools-0.3 (crate (name "spools") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ghzqffnbwa3iq8pqsgnmqzxivizl89k95spxqdq2w3rn69qi6l6")))

(define-public crate-spools-0.3 (crate (name "spools") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0m61vl9jvgmgfg11s8x06w5iac1hpyk66pbhidl7msz72hf38swq")))

(define-public crate-spools-0.4 (crate (name "spools") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05dcf1zm0m26nbqgp05lblmf0cmm9hla7nhbkzwlsh2n84g6nji3")))

(define-public crate-spools-0.4 (crate (name "spools") (vers "0.4.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0nw896893ndi31h50lha6s3dv9772yn3swlzg7zvgd95jb1qail1")))

(define-public crate-spools-0.5 (crate (name "spools") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zk44pzmbqpg0pjkqmzgccb3d45sy04jvm8939j7265514m9xszv")))

(define-public crate-spools-0.5 (crate (name "spools") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ik0ng7cz3m9six3asvny70rj4gfx4nsr92079vsh2rajdcfr5h0")))

(define-public crate-spoon-0.1 (crate (name "spoon") (vers "0.1.0") (hash "1bk6hiiy8vwb77x11mykr9aqhrk0fvfpim81m9wyighnky5z2lab") (rust-version "1.56")))

(define-public crate-spoons-0.1 (crate (name "spoons") (vers "0.1.0") (hash "1vskq1n3mryxsmfwcjmbp89gf26aqlgaha3ni9kimls64vbc04gf")))

(define-public crate-spoopy-0.1 (crate (name "spoopy") (vers "0.1.0") (hash "0g8vvc52xyb6kaa29kzbp1l8qwlkpjd43c3wam05ypbhz1d37g1w")))

