(define-module (crates-io sp dp) #:use-module (crates-io))

(define-public crate-spdp_sys-0.1 (crate (name "spdp_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "09ril3y43n5g7wm2y6b0kry799cb0nh2hxq554dkj0ahngba8wqs") (links "spdp")))

