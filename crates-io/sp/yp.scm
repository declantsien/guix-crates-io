(define-module (crates-io sp yp) #:use-module (crates-io))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "032h8rqln472sskn02k7w44fx1pqzasj24ag9v4j4glipppcrcvs")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0j525400qmmyp0kwlwx28vyzybgzyqa941fzn9zrl8n02ldshcrb")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hhs7y4mbr7g2bjjy2imzfzq9aiq8gp5vr31vrhdnzxvr87mr5gk")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0j5cnllgwr6s1lslcqrr2wwqkaqsj7j9zllxqg54nsrgdypw5rzx")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xb7hj6s3jm0xks9frw7dgra00wjzz9pylzd95pr58h67p05adz2")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12w1dymbrik25cp2xd3lcc5d4qkijvsq952wmh4525s4pgs1bj03")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "030cwdbl7k0cfh589c9pd46b53ibf2akiz2yf7pkfrd40whdaz9d")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "05phv1qqvcms93qfv7y4qb97zcgm9lm2lv8y1i8d0mxa28vij78k")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1rc46nkm91h227d4f5d5h1qzvhhwp13ims7g91f57clk9vajzn20")))

(define-public crate-spyparty-0.1 (crate (name "spyparty") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1sgacadmy6d4d4zjy8g4c4mfgsa7a7fixn00vaz9i3ci8ssl97am")))

