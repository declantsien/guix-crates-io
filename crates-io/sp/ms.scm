(define-module (crates-io sp ms) #:use-module (crates-io))

(define-public crate-spms_ring-0.1 (crate (name "spms_ring") (vers "0.1.1") (deps (list (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1bfkm7ba0rnaj1di978p1l0q67cp8b09znsr6h35ng0vm4f337y5")))

(define-public crate-spms_ring-0.1 (crate (name "spms_ring") (vers "0.1.2") (deps (list (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17256rpyji4isnmgg2m76d0bmv8k47v4fyz83mnc2y15p083dlhv")))

(define-public crate-spms_ring-0.1 (crate (name "spms_ring") (vers "0.1.3") (deps (list (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1xbzq9s3nkjn6p7al1bkwsnn0aq1s7xr9kmd987pb537y4h6cj6z")))

(define-public crate-spms_ring-0.1 (crate (name "spms_ring") (vers "0.1.4") (deps (list (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1mkhji3blhv5jx5vdp7rdfjbzaqx4568jk518d8639g87z603smk")))

