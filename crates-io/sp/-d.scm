(define-module (crates-io sp -d) #:use-module (crates-io))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-alpha.7") (deps (list (crate-dep (name "kvdb") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1fyxifgpkicgnpfazxqhm18xj9ss6vlxsjr5g2g13adpqc6havn9")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-alpha.8") (deps (list (crate-dep (name "kvdb") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "16vdnc12wb3hb4rfz7agb4phf2n7b3s3bvahf4gh78xfz1jmpfmn")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-rc1") (deps (list (crate-dep (name "kvdb") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1cg1cl9xq1im8x28b3za49mdg0xxyc3n7yxz33x49nk1fd8z7226")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-rc2") (deps (list (crate-dep (name "kvdb") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0ri2n0kmgdgx15901q6147qnd0yr0346kz2q1ra7v1v1nfxgcpjm")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-rc3") (deps (list (crate-dep (name "kvdb") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1mra436xxg0dzbqbr4zgmpgp6r9cwizadkwsd1j613krrpnqs4p8")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-rc4") (deps (list (crate-dep (name "kvdb") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "08yq44as60n9jyq34ws2va0qpakbsfp8fc5j58myw02v2il1dsma")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-rc5") (deps (list (crate-dep (name "kvdb") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0zz7mq3kbqlm7yfa300szphxgpjshk3dsbc34d9c8jyzif97i949")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0-rc6") (deps (list (crate-dep (name "kvdb") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "09rhbzvfzbrvbh06i5bx1jhyvq4p2vas18kbq2hdl3b63i7fpyi3")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1pap4jfhxm2isqn6y555mjvp3icg0gys44vzqbgvrrggrqp3a72c")))

(define-public crate-sp-database-2 (crate (name "sp-database") (vers "2.0.1") (deps (list (crate-dep (name "kvdb") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0ya92mkcqqfqnfcb2620pn8pxlakcbm3jlsyvwfbhdm2fbjiphaj")))

(define-public crate-sp-database-3 (crate (name "sp-database") (vers "3.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "14h4dxgbvngm6kx59rkhhv95ki2raxy6a968jlfikvryj4cg1hy8")))

(define-public crate-sp-database-4 (crate (name "sp-database") (vers "4.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "17d9l30v1g2l6b10klc0jm4azpgqvfrnnfg6c029bx7h42yl1z4c")))

(define-public crate-sp-database-5 (crate (name "sp-database") (vers "5.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0r4d5p0qxyphp98r6dzq7b4b3mmrayli1ss9bdfr9qd9licyzmjd")))

(define-public crate-sp-database-4 (crate (name "sp-database") (vers "4.1.0-dev1") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0qjcvcpy43phm8rzlaylxn9mpks9zg01gr79p8mmfvn3pa4cd71w")))

(define-public crate-sp-database-5 (crate (name "sp-database") (vers "5.1.0-dev1") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "03blb3ksng1p1jq0f9wkmlf6x94vh5kxcn9szsh4q1bc84jzxgj2")))

(define-public crate-sp-database-5 (crate (name "sp-database") (vers "5.1.0-dev.2") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0bmziia1fkbnhc706f7vvxppyqmccixidcsg70ld7dxa6j35bpsg")))

(define-public crate-sp-database-5 (crate (name "sp-database") (vers "5.1.0-dev.3") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1n04d4n0brf2bs78bm59my6l7q2hkw68fldwcrj7nascmwkr2ahl")))

(define-public crate-sp-database-5 (crate (name "sp-database") (vers "5.1.0-dev.4") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "18pq5kn3v94ybvg99drnklliidm4x5rmnhnkbwipmxndx3d7bnwy")))

(define-public crate-sp-database-5 (crate (name "sp-database") (vers "5.1.0-dev.5") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1rjm6ljcppr7yb35ydimcgk8qx9yv174ah8hxlvv2rf4c42w05i9")))

(define-public crate-sp-database-5 (crate (name "sp-database") (vers "5.1.0-dev.6") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1zz4spvsqxky04svb3aw6q8v8gbb725h3qgym66lar899zw8g2lv")))

(define-public crate-sp-database-6 (crate (name "sp-database") (vers "6.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1wc7bg2qksp1a4j1ywa12dpazp8r8sy5dbb0mxz6sy7pi75x213l")))

(define-public crate-sp-database-7 (crate (name "sp-database") (vers "7.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1nm5zgj8d74g7iyw3nh51bvizs76fcxq9c86g8ddb79h9wdwl5mc")))

(define-public crate-sp-database-8 (crate (name "sp-database") (vers "8.0.0-dev.1") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0v06rh3wgbvym8443dpvba9w004nixgm7j1c37fnvmv8f6agqnpx")))

(define-public crate-sp-database-8 (crate (name "sp-database") (vers "8.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1giy1wv0hwpl34sw14ajlllrkdb8c3snnkmqiq5032dgd2agf9db")))

(define-public crate-sp-database-9 (crate (name "sp-database") (vers "9.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "150asvivv6s5pyx80r44jgbnl7rxkjx9sbglyqm52wba1mqqqvlw")))

(define-public crate-sp-database-10 (crate (name "sp-database") (vers "10.0.0") (deps (list (crate-dep (name "kvdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0ab9k36v82zyvks1y35y47i7spz9a7mpzl6v6y0mg52vpz6vwb3j")))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-alpha.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0scn19vn435az43ys2w6bs8n6nfryicx0pbv4ax24ycm77nfg3k7") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-alpha.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "19h225b05ly19mbqy4853m34mc2n5g2rmnbrygs43x1fap2m6fr7") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-alpha.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1cjs3f0fwv9w2hlh52785flpr6fa25465k0m8y33jwc95px3ykqj") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-alpha.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0d41fc59kg5sqi448z81iplgqvl70h4y74bysdjkyfwx5a98m5nz") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-alpha.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1s6046kbz7aw3fb60r3i4krdaxbair0j9w1wg6bdz53np80cpi5j") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-alpha.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1c7lw8mmkqrda99y18m3zpmg2v81ydp2f3r9q5mqm1i3y0cbvzdf") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-rc1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0wjl8bj5swxcssxarx27v083nj09dq601zjzgqzw6a0wgw41dkfb") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-rc2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "00pnl0zr5phlg837kx1bb63knppxcmcwkybkx7ipzdv9fplj0nzp") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-rc3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1vx4rmm92bdik570693z16czwhd0rldk7gy7igpjj9nd6fsakrxa") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-rc4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0q4m5d2kswk0wxpnq8gbksqv8dyqf5d2hgnad0w2nkhc5jzg73ay") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-rc5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1a8npdzl4bdw9yl5v741bz972s0dd4y3aii61m5dk22g49s84f6v") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0-rc6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "15gi0iiqng8h8lqi62zwdyj2m3dbj54vfpx3q6k0l0k4ph26xicg") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1p26hhia449ns34qa0g05zy9gfmi617rfjl3drzngx70hjq50dwa") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-2 (crate (name "sp-debug-derive") (vers "2.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1a7vaxgvlzg4n4p87xybqq3k49czhxi3p4l2wc69bazyy8j1m2jm") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-3 (crate (name "sp-debug-derive") (vers "3.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "07pkjyz6r65b2zv4b57fs83p63jk2vq90pzc9psshysf7gr7a0p8") (features (quote (("std"))))))

(define-public crate-sp-debug-derive-4 (crate (name "sp-debug-derive") (vers "4.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "1wkgjln5zdfsbjar087mxp8n3x0xqjzfg0bf2yb0fap2f94ncxnn") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-5 (crate (name "sp-debug-derive") (vers "5.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "0m346hrb6zkfvdvq6yccdv16n744gx207sapmaj9cfvpa9rvmsd9") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-6 (crate (name "sp-debug-derive") (vers "6.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "0bfzqcblfmpn92rsp8aya8ic2rmxw2v0b9b2xmxpvpjl7p39vyv6") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-7 (crate (name "sp-debug-derive") (vers "7.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "1aw96cbzx67hvawyddyrv54zrr4fkz66ri9pv2ww9nprkvniw8b2") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8 (crate (name "sp-debug-derive") (vers "8.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "030lh6qrk0ddhcmn6jbp9bzr8zfg1j1jhi678i8rj5ig9n0k3xf7") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8 (crate (name "sp-debug-derive") (vers "8.1.0-dev1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "0vn88hpyw2ws2rzpsbxpfll0ri2ifpcs85fdjaa4wgp7mg0dh0ap") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8 (crate (name "sp-debug-derive") (vers "8.1.0-dev.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "08q6kwz3a6zf9wjypqdzdarc72sjywcdbj35dd4k293ys14g9r7i") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8 (crate (name "sp-debug-derive") (vers "8.1.0-dev.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "0dilfwcplzq5qz5b63q2p24rzizg1pxmx65vwv68rn0m1cj16hs4") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8 (crate (name "sp-debug-derive") (vers "8.1.0-dev.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "1na3zwgv5pawyjbs3zmh1h6nk5wc0ayaivdnjbhc2s3srbfxysjn") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8 (crate (name "sp-debug-derive") (vers "8.1.0-dev.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "0w52nlsll3xr748dpv9gz3y03ldd7c38ird1d191sxzfap3j34fs") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8 (crate (name "sp-debug-derive") (vers "8.1.0-dev.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "0lf4qrx660g5igww1bc1vwsndgfv6aa7k5bw40rf8fr986psmlnw") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-9 (crate (name "sp-debug-derive") (vers "9.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "1rglm58x8gj4nh1077cj772vg79hv5my2k7x7z8jbs61yryawbgi") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-10 (crate (name "sp-debug-derive") (vers "10.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.31") (default-features #t) (kind 0)))) (hash "159xlbd0qj1s3c9qpwdwfxdihnysaj1qqwhady7da93imnh3bcml") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-11 (crate (name "sp-debug-derive") (vers "11.0.0-dev.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (default-features #t) (kind 0)))) (hash "0jy1q04dx2nzwa3rq4dn1ablvb7n4ijd02axgb81bi5snqg5lv3r") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-11 (crate (name "sp-debug-derive") (vers "11.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (default-features #t) (kind 0)))) (hash "1y0bwndvvfsh4s5q11msjrm6dl5qljszaaw825p5d405c5sx7xqn") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-12 (crate (name "sp-debug-derive") (vers "12.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "18g4s9z8nvm8hxmp291ckikwh766zaz9xdcm25fbmlq8awd5wlsh") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-13 (crate (name "sp-debug-derive") (vers "13.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "063ak44kyjvyza4q3xh3cpb46d0hrj1v28297zwhv51y1ik2rzch") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-14 (crate (name "sp-debug-derive") (vers "14.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)))) (hash "1gkx01f91gknvvbawj416ix0y8167n2y6np23sw9yagplnh9zl28") (features (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-dto-0.1 (crate (name "sp-dto") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "1hf1jwq1cmss72428sdgvvwvm8rx01kkvd942km5j7ni56f71iy8")))

