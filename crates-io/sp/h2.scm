(define-module (crates-io sp h2) #:use-module (crates-io))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.0") (hash "0s01mcbna06p0iqf14lym498rangwxmwzjszn9ixm4f0xsvcf9pf")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.1") (hash "0aj21ln7csnbpwiqarc937v9rdg4idzpqfbljn71w1ax717fxr7c")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.2") (hash "0bijxz2j3vwhx59fw9myihsndl9q5knpc8v97phlv7dnf8a30048")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.3") (hash "1kmgb6aqgfv0gsi6vli9qpnk423izdfss419x4qdn1z7c0d5s40r")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.4") (hash "0bbc2fbgc9wf2jyklqqilkrxbl858ym9bvd5kfbjfxzqrxijbn0s")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dikxks2bs7slihig8dy99p5a94bf4agsw787k16mq55wy0bq35n")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1p2fsx0x4a8v6pp67m8j4lsr25cx2aks92z2hji844274cl58wp1")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rna2ncxdw17dz8pwjdjq2k89ywwh6w6x0qmgm5qdmg8q8v9z0wd")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0g6lz0lhll3pb4nqg0w17p9m4wjy1f9iaiql00k54dqlx2z0qhyi")))

(define-public crate-Sph2-rs-0.1 (crate (name "Sph2-rs") (vers "0.1.9") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1h6v4ic5w215zcla9khmsbvym541v0xmszjnvnvnl1rh9ybv9ahz")))

