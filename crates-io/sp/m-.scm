(define-module (crates-io sp m-) #:use-module (crates-io))

(define-public crate-spm-helper-0.1 (crate (name "spm-helper") (vers "0.1.0") (deps (list (crate-dep (name "dialoguer") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "1mzzdpafasdkmkrkk0b3g0f6xrhnsf19bkgwpr58100fg93z204x") (yanked #t)))

