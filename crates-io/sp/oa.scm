(define-module (crates-io sp oa) #:use-module (crates-io))

(define-public crate-spoa-0.1 (crate (name "spoa") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "spoa-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ppb4cks37cyf3pnphl006knbv5ws3h46kfkw0ymaf4van38yag4") (features (quote (("simde" "spoa-sys/simde") ("default"))))))

(define-public crate-spoa-0.1 (crate (name "spoa") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "spoa-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 2)))) (hash "1y08d314q2cj1rgd121rdnp00vjb68nbhi3fr4295fmh7mch0hca") (features (quote (("simde" "spoa-sys/simde") ("default"))))))

(define-public crate-spoa-sys-0.1 (crate (name "spoa-sys") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "1fkk602ds0yy8p35156z8yrg8yl78gr3n9si1rm2i7zjlld88dyg") (features (quote (("simde") ("default")))) (links "spoa")))

(define-public crate-spoa-sys-0.1 (crate (name "spoa-sys") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "0sqrljvqw47mhmvyg31gr3kaiivmcyvahr5qs18zbx5d5gx3qs4l") (features (quote (("simde") ("default")))) (links "spoa")))

