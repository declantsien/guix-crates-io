(define-module (crates-io sp ng) #:use-module (crates-io))

(define-public crate-spng-0.1 (crate (name "spng") (vers "0.1.0-alpha+1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "1kql6mwsvnrmgrw49ibzmcydc9a537q7iz7067l69zc269h59764")))

(define-public crate-spng-0.1 (crate (name "spng") (vers "0.1.0-alpha+2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "1a5cpwfxfs3pds0w8cd7w009sgy9fxyzjln9cchgc8gvapj6f2vp")))

(define-public crate-spng-0.1 (crate (name "spng") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "0gj6q3qx59iahdvz9ic8ix5p7kyd4k3370lavy69jk6v3wzi662m")))

(define-public crate-spng-0.1 (crate (name "spng") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "0c4mbiqif7q78wsi1hj7zc190bmvcq77y16c0hcrdri5qkncik4w")))

(define-public crate-spng-0.1 (crate (name "spng") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "12sgi4s0l38rnp0p74r42jzbrq3424hn8mg810v7rdsrlncrvly0")))

(define-public crate-spng-0.1 (crate (name "spng") (vers "0.1.0-alpha.6") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "0dggrmc1w2g105wymvv6nv3wp9y39wv254ch07jvsqdgkcm98gik") (features (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

(define-public crate-spng-0.1 (crate (name "spng") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1nvg1ddhys2yyr02x5xyr6yc14n560gawkr9j2p8vc9j8pwsa3ac") (features (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

(define-public crate-spng-0.2 (crate (name "spng") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)))) (hash "1axnabdgyx59k88781q4jc3c3kr78hivb88558xf8xx5l1wgazz3") (features (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

(define-public crate-spng-0.2 (crate (name "spng") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spng-sys") (req "^0.2.0-alpha.2") (default-features #t) (kind 0)))) (hash "03lvmd7jhf8d41x2fc6smf72s63yiw78rda2nrwl1xxw3r58ch4c") (features (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

(define-public crate-spng-sys-0.1 (crate (name "spng-sys") (vers "0.1.0-alpha+1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z54p3yn06m26cx3mrghn9dr10vyqzlx35qwi867kb3npgb5d711") (links "spng")))

(define-public crate-spng-sys-0.1 (crate (name "spng-sys") (vers "0.1.0-alpha+2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "19vxy0dy63b0x2krfxjhg5br0l1wlqpcaz0gxvsh96v01zsc1byn") (links "spng")))

(define-public crate-spng-sys-0.1 (crate (name "spng-sys") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "1caww0x2b309hr5ylsc094adgzn13xlp7mclb61m13ww98c5f8dq") (links "spng")))

(define-public crate-spng-sys-0.1 (crate (name "spng-sys") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pczfy4ilkynqkmn49dqs6qyqvi8yycc69sqsaiar0m2v5708b9r") (links "spng")))

(define-public crate-spng-sys-0.1 (crate (name "spng-sys") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w50izm8pizn87rhyd6bd6vbpp2jjxpswgxn4vi7g82ax8vv19bl") (links "spng")))

(define-public crate-spng-sys-0.1 (crate (name "spng-sys") (vers "0.1.0-alpha.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.1.2") (features (quote ("libc" "static"))) (kind 0)))) (hash "09rc5s7vfz22yr2q3mfrx9pjabkn8v72absf19x22yvql1xbh2j5") (features (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (links "spng")))

(define-public crate-spng-sys-0.1 (crate (name "spng-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.1.2") (features (quote ("libc" "static"))) (kind 0)))) (hash "0mc1dg698wv45cfh1jb6a9mrsr6s2p8gzzcn7ihzwfyl4xcb9xag") (features (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (links "spng")))

(define-public crate-spng-sys-0.2 (crate (name "spng-sys") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.1.2") (features (quote ("libc" "static"))) (kind 0)))) (hash "1ga8wkbyrdnxgyk12rav80nm3mgrbyyr8blcys7n9wikdmq4rz0i") (features (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (links "spng")))

(define-public crate-spng-sys-0.2 (crate (name "spng-sys") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.1.2") (features (quote ("libc" "static"))) (kind 0)))) (hash "046y7r9mlm9w00zkf5frkk0r00hd1zz4zsxl9qahvq20jwxsqn7d") (features (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (links "spng")))

