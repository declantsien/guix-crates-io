(define-module (crates-io sp p-) #:use-module (crates-io))

(define-public crate-spp-rust-0.1 (crate (name "spp-rust") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0j15423zmhdvcq39v9v5ama7vx6lhmn8v6qr3xlxw5n1dcc6wgbn")))

(define-public crate-spp-rust-0.1 (crate (name "spp-rust") (vers "0.1.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "08f6j01maza6792bjzdapi73mp6vvpp80af70vrsp86821cs6mjc")))

(define-public crate-spp-rust-0.2 (crate (name "spp-rust") (vers "0.2.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (kind 0)))) (hash "0im7fd3h480nncl98r9h7mp2kv34brgvn6ba82c33vqh1y68rgzb")))

(define-public crate-spp-rust-0.2 (crate (name "spp-rust") (vers "0.2.1") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (kind 0)))) (hash "03ng06wyj0nw8zsih878hyb3swds2xc04iv0i02sf9j546ckhlxx")))

(define-public crate-spp-rust-0.2 (crate (name "spp-rust") (vers "0.2.2") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (kind 0)))) (hash "19ig6lp7l3s9h55r9fgh1mpyqkqaa0yjxbw59abd88jj390pmgmd")))

(define-public crate-spp-rust-0.2 (crate (name "spp-rust") (vers "0.2.3") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (kind 0)))) (hash "0fvznbq1pz65fsfjly29943d7ni30zidfzgaf85z86d306nyp38l")))

