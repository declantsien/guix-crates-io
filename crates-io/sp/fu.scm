(define-module (crates-io sp fu) #:use-module (crates-io))

(define-public crate-spfunc-0.1 (crate (name "spfunc") (vers "0.1.0") (deps (list (crate-dep (name "cauchy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0gmiic8l1g0ds0kvgn0lhhjihhrxx9pkw6l645p9j5mjxz557948")))

