(define-module (crates-io sp ol) #:use-module (crates-io))

(define-public crate-spolyfy-1 (crate (name "spolyfy") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "0p1v1wmgkg1i67zb01zij1rdbvp8ipzb17igg7d5wlpj94zdkxi7") (yanked #t)))

(define-public crate-spolyfy-1 (crate (name "spolyfy") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "1iidj7vdp45437jjbjf0pad91ni08lh376a85vc3xrmysi2ypfab") (yanked #t)))

(define-public crate-spolyfy-1 (crate (name "spolyfy") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "0h32xih2dywhh8zksnlmx22m7ag8as6f4c0qxii4mnaqgq4zmrf6") (yanked #t)))

(define-public crate-spolyfy-1 (crate (name "spolyfy") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "1lms3vyn1l6ld0b85h7s0fz5ls810ja7rkwd83ihmrs03qv5iry1")))

