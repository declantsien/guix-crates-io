(define-module (crates-io sp sa) #:use-module (crates-io))

(define-public crate-spsa-0.1 (crate (name "spsa") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "packed_simd_2") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0fqgfh4dc9ysjlvs3i464cnlkf11zhwkfrsc5ndi1s7lfqj3r5j8")))

(define-public crate-spsa-0.2 (crate (name "spsa") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "packed_simd_2") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0vq1vvr9dg1686xs01d5dsqj90i324m2dk2mpr8f1dkyz39b882a")))

(define-public crate-spsa-0.2 (crate (name "spsa") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "packed_simd_2") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0njxzdx4vxi4hw13pyjycx32n9bxm4ixd50qxks9ld45yfbmh66a")))

