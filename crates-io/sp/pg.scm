(define-module (crates-io sp pg) #:use-module (crates-io))

(define-public crate-sppg-0.1 (crate (name "sppg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0m9nsal7wdpddkph561y2w7gljay6vjlzh2647qql2pnqxcbgdsw")))

(define-public crate-sppg-0.1 (crate (name "sppg") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m5v8jknwrgpcfzlqgxfxbjb0rlvqb5ld5sjl9iw9g5p35y3kj1g")))

(define-public crate-sppg-0.2 (crate (name "sppg") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0zn9snva8ihn0zw9g3smnzm84vksb4hd9cxf0qdv8m7qhp1rjblh")))

(define-public crate-sppg-0.3 (crate (name "sppg") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0s7yp2y9rmcwi6i44sxzq0a7wcyc014ffjnay6ajlav808f0jr4l")))

