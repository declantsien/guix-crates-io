(define-module (crates-io sp wf) #:use-module (crates-io))

(define-public crate-spwf-0.0.0 (crate (name "spwf") (vers "0.0.0") (hash "1zdzywncvwprijk9m0d10rxr8is66s40p64dwvmvphhgqs4anxam")))

(define-public crate-spwf-0.0.1 (crate (name "spwf") (vers "0.0.1") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.22") (features (quote ("async"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04bmcc8kyv8i35kgyz96ds3nwf3qypnw6rgy3w5sfdcp37zsvhdp")))

(define-public crate-spwf-0.0.2 (crate (name "spwf") (vers "0.0.2") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.24") (features (quote ("async"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00f2d3hiyjk8zfl945lvvqb86splqwm1dfivsxalm8v0qr1skwh6")))

