(define-module (crates-io sp ok) #:use-module (crates-io))

(define-public crate-spokes-0.1 (crate (name "spokes") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "12pcmaz7wk2asns9m4vag9ai8k4hbwbi7y627vz7yxxc3rcr550j") (features (quote (("serde1" "serde") ("default"))))))

