(define-module (crates-io sp mc) #:use-module (crates-io))

(define-public crate-spmc-0.1 (crate (name "spmc") (vers "0.1.0") (hash "1a0h8azqk8qz3f02z7cygzzmqncl7i4pfz0j7nl78rbvnpihxnsb") (yanked #t)))

(define-public crate-spmc-0.2 (crate (name "spmc") (vers "0.2.0") (hash "07jwh5f2zqhfl5bkh4sxmzpz3w161zy715fjqf7rgj6wz1lk6k64") (yanked #t)))

(define-public crate-spmc-0.2 (crate (name "spmc") (vers "0.2.1") (hash "1pssc006fwcbxjji67kzy8nzzsjrm3zqhwyiqj8ya4x4q5hspgck") (yanked #t)))

(define-public crate-spmc-0.2 (crate (name "spmc") (vers "0.2.2") (hash "06mdy6zzjwld5jgb1zfrmzrg5yvf31dbiq2wwls1im2zzg8i27yd") (yanked #t)))

(define-public crate-spmc-0.2 (crate (name "spmc") (vers "0.2.3") (deps (list (crate-dep (name "loom") (req "^0.2.2") (default-features #t) (kind 2)))) (hash "1nhbjc65avbb4nffk6b49spbv7rsmmnrppj2qnx39mhwi57spgiw") (yanked #t)))

(define-public crate-spmc-0.3 (crate (name "spmc") (vers "0.3.0") (deps (list (crate-dep (name "loom") (req "^0.2.2") (default-features #t) (kind 2)))) (hash "1rgcqgj6b3d0cshi7277akr2xk0cx11rkmviaahy7a3pla6l5a02")))

(define-public crate-spmc-logger-0.1 (crate (name "spmc-logger") (vers "0.1.0") (deps (list (crate-dep (name "ring") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1mck770pl8rqfvdhnrxh39v8iyvys551i5pr7xdlygr1c6j1kg8c")))

(define-public crate-spmc_buffer-0.1 (crate (name "spmc_buffer") (vers "0.1.0") (hash "1ajsx5ap6aahmk91gx80jd5ffyy81pdhfg0699n9y4l33pbx1rj2")))

(define-public crate-spmc_buffer-0.2 (crate (name "spmc_buffer") (vers "0.2.0") (hash "1yh0xyrz62p8lj75byrahfm2znjq2ww7idjqp4nhxbycn850hmsa")))

(define-public crate-spmc_buffer-0.3 (crate (name "spmc_buffer") (vers "0.3.0") (hash "0fxywyhswq6i07i94p87fcyv9v968drmbpkiwwsr8jbfam1h47i3")))

(define-public crate-spmc_buffer-0.3 (crate (name "spmc_buffer") (vers "0.3.1") (deps (list (crate-dep (name "testbench") (req "^0") (default-features #t) (kind 0)))) (hash "05qf2gn6a43q96n0hf09d7viny3wnr2sgy1svcafxmys3zwv4nic")))

(define-public crate-spmc_buffer-0.4 (crate (name "spmc_buffer") (vers "0.4.0") (deps (list (crate-dep (name "testbench") (req "^0") (default-features #t) (kind 0)))) (hash "0k0773ksq9amhhqkliqydwdrigpnc08a227a11qpchbgj55isk3f")))

(define-public crate-spmc_buffer-0.4 (crate (name "spmc_buffer") (vers "0.4.1") (deps (list (crate-dep (name "testbench") (req "^0") (default-features #t) (kind 0)))) (hash "0f9vnf34csff7r7i84plmh5ms2jk8c3xg8mqi35rzmnr3kdc063w")))

(define-public crate-spmc_buffer-0.4 (crate (name "spmc_buffer") (vers "0.4.2") (deps (list (crate-dep (name "testbench") (req "^0") (default-features #t) (kind 0)))) (hash "0sn6nf4s183vib3wp7najyyldbc064nnm1g46zn7dp0wzp8ynqgh")))

(define-public crate-spmc_buffer-0.5 (crate (name "spmc_buffer") (vers "0.5.0") (deps (list (crate-dep (name "testbench") (req "^0") (default-features #t) (kind 0)))) (hash "1pww8z6gys05ygm5zvv53z4w7xl7ps470lx0n2zr0wdw2zgwr9j2")))

(define-public crate-spmc_buffer-0.5 (crate (name "spmc_buffer") (vers "0.5.1") (deps (list (crate-dep (name "testbench") (req "^0") (default-features #t) (kind 0)))) (hash "0wcda2zn3fcj7gnp6ifcshy8a864sszpg70ik3yvs36v8fvp0363")))

(define-public crate-spmc_ring-0.1 (crate (name "spmc_ring") (vers "0.1.0") (deps (list (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "05xggy4k2b8hpkb3gh8pajz6nvkw494pvvbixz074m435yhs3awa")))

(define-public crate-spmcq-0.1 (crate (name "spmcq") (vers "0.1.0") (hash "0pfcjs3wpl3l32fjk8iykgg8g548mjwngrk8jkswrxbwyb3j09g4")))

