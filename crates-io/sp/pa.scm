(define-module (crates-io sp pa) #:use-module (crates-io))

(define-public crate-sppark-0.1 (crate (name "sppark") (vers "0.1.0") (hash "0yvr73r3kv5bb650ipyz6y6hlr7m79igkcz36i4p9cdrp6pi6mvy") (links "sppark")))

(define-public crate-sppark-0.1 (crate (name "sppark") (vers "0.1.1") (hash "1zc4virvp7aspgqjjpvvaa9sbhs48xwn34wrj9hiia96l4vwah6i") (links "sppark")))

(define-public crate-sppark-0.1 (crate (name "sppark") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "which") (req "^4.0") (default-features #t) (kind 1)))) (hash "1ll8wy7z78pxhc705lfi5jp96sl1b9qq8bs2nlwkq6jc89cc0ar0") (links "sppark")))

(define-public crate-sppark-0.1 (crate (name "sppark") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "which") (req "^4.0") (default-features #t) (kind 1)))) (hash "0sl9jjg0yrw9vcflraaxw4hx6nyzsi8p9wjdghcs2p5kmrmliffg") (links "sppark")))

(define-public crate-sppark-0.1 (crate (name "sppark") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "which") (req "^4.0") (default-features #t) (kind 1)))) (hash "0knzhfkddn9xr2z1fyyphxz2mxiy03z6r3rw2f05gf4w8r8dafm9") (yanked #t) (links "sppark")))

(define-public crate-sppark-0.1 (crate (name "sppark") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "which") (req "^4.0") (default-features #t) (kind 1)))) (hash "0iiw0mqcih1zz36dnyc9xvzfgmjn5nkx8p012y4ylrkqjfc6syms") (links "sppark")))

(define-public crate-sppark-0.1 (crate (name "sppark") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "which") (req "^4.0") (default-features #t) (kind 1)))) (hash "0jwsd0ixy16k8sn0g45v65pd94pwp0vf3mvpsiq3dmgav8ann9pv") (links "sppark")))

