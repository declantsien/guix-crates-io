(define-module (crates-io sp ha) #:use-module (crates-io))

(define-public crate-sphalerite-0.1 (crate (name "sphalerite") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0kg24nny34ysbc4lvl068zxhp7gi5y4mfai3529vn1q8r6d33nvx")))

