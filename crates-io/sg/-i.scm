(define-module (crates-io sg #{-i}#) #:use-module (crates-io))

(define-public crate-sg-index-query-0.1 (crate (name "sg-index-query") (vers "0.1.0") (deps (list (crate-dep (name "cosmwasm-schema") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cw-storage-plus") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1zbywanaaqf6hky0bd2kjj9kijh0by5x6bdiq2q4lfh45w8jv0f7") (features (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-sg-index-query-0.1 (crate (name "sg-index-query") (vers "0.1.1") (deps (list (crate-dep (name "cosmwasm-schema") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cw-storage-plus") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1iq73mdxscjf9pqbglndi42gspy1q1aih318x9pgg89fjvwmvz2b") (features (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

