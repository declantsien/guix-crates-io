(define-module (crates-io sg r_) #:use-module (crates-io))

(define-public crate-sgr_macros-0.3 (crate (name "sgr_macros") (vers "0.3.0") (deps (list (crate-dep (name "const_format") (req "^0.2.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "0zin8ajgx6b1p5jybrql47h0v5x6p4sgirawh15vs3c0ashwxpxk") (features (quote (("default") ("const" "const_format"))))))

(define-public crate-sgr_release_test-0.1 (crate (name "sgr_release_test") (vers "0.1.0") (hash "0n0xy52bykvgkf9bi4r1i5lglj9jnh40h1gr4v38pfazhjy9kkyv")))

(define-public crate-sgr_string-0.1 (crate (name "sgr_string") (vers "0.1.0") (hash "1wph2ybam4rblg7436468n1agj725bnz209m45pspbfqfb84dr8c")))

(define-public crate-sgr_string-0.1 (crate (name "sgr_string") (vers "0.1.1") (hash "0r3nkqqxr3c2dqpmw72a0d2lcxhrzrj8mxrd56ivzjc02khhil0j")))

(define-public crate-sgr_string-0.1 (crate (name "sgr_string") (vers "0.1.2") (hash "06f1nl27kqa667ppvqizsllvz48d948wvj487n6hp3bmgiqq4xbj")))

