(define-module (crates-io sg da) #:use-module (crates-io))

(define-public crate-sgdata-0.1 (crate (name "sgdata") (vers "0.1.0") (deps (list (crate-dep (name "owning_ref") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1f959g4n8y8djdqn0lrdp903r39w6yyshyikqwzwcxhnm22y86ah")))

(define-public crate-sgdata-0.2 (crate (name "sgdata") (vers "0.2.0") (deps (list (crate-dep (name "owning_ref") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1c8ag70195ccn6fk5idpw20y7gnsan7zlqkciv8gr3x1znyn30q4")))

