(define-module (crates-io sg ut) #:use-module (crates-io))

(define-public crate-sgutils-0.0.1 (crate (name "sgutils") (vers "0.0.1") (deps (list (crate-dep (name "libsgutils2-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0ly4gnqnqw50nbscs2lcan1626y2k2x5wgybhzxngz9pkf4m0zcj")))

