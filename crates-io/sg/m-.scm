(define-module (crates-io sg m-) #:use-module (crates-io))

(define-public crate-sgm-rs-0.1 (crate (name "sgm-rs") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "07nbqjc1i9i96999cjwyix55by7y1wck4b6llwdaf222rfc8mnal")))

