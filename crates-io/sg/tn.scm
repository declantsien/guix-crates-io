(define-module (crates-io sg tn) #:use-module (crates-io))

(define-public crate-sgtnclient-0.0.0 (crate (name "sgtnclient") (vers "0.0.0") (hash "0vzvj5dkw54i1s3m9vx16vs1jx8n54kvx8f3z4abx9pfvrr9hxg6") (yanked #t)))

(define-public crate-sgtnclient-0.0.1 (crate (name "sgtnclient") (vers "0.0.1") (hash "073d33hm3xqx2fjwkiygdpz7g7nxz48np17if4i4xh5akaslvmj0") (yanked #t)))

