(define-module (crates-io sg -b) #:use-module (crates-io))

(define-public crate-sg-basic-whitelist-0.1 (crate (name "sg-basic-whitelist") (vers "0.1.0") (deps (list (crate-dep (name "cosmwasm-schema") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cw-ownable") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sg-basic-whitelist-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13qd9cmmpzm4iaabmgy87vjacq80hmfvldgr6v4qqddwhv12i6v3") (features (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-sg-basic-whitelist-derive-0.1 (crate (name "sg-basic-whitelist-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0afnmwv9fqf4c9kgk3rfgpl7b646jrdksfqs2g2p8csa5slj39rd")))

