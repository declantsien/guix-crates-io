(define-module (crates-io sg ri) #:use-module (crates-io))

(define-public crate-sgrif-test-publish-0.1 (crate (name "sgrif-test-publish") (vers "0.1.0") (hash "17ry509kcgsl3qbipmkpi35ip83w61x2qg6hycslrdb0fsfjhpia") (yanked #t)))

(define-public crate-sgrif-test-publish-0.1 (crate (name "sgrif-test-publish") (vers "0.1.1") (hash "1arlywjbbfh9lb4h6hc7fhsj4nkp522q2h662l8ihnlp1g256wil") (yanked #t)))

