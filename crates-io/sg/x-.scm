(define-module (crates-io sg x-) #:use-module (crates-io))

(define-public crate-sgx-cpu-0.1 (crate (name "sgx-cpu") (vers "0.1.0") (deps (list (crate-dep (name "raw-cpuid") (req "^9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nwv9qs3qhij30hkfw2yldr8w702c50v3s7zmzfp96sca25l7j5m")))

(define-public crate-sgx-cpu-0.1 (crate (name "sgx-cpu") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "raw-cpuid") (req "^9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lrc8ixw3wb5n5k715g41d4ib24mv631szr6wihk1ham4cdjv3ws") (yanked #t)))

(define-public crate-sgx-cpu-0.1 (crate (name "sgx-cpu") (vers "0.1.2") (deps (list (crate-dep (name "console") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "raw-cpuid") (req "^9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1y90xklsa40x12jd9h7r2b721dd4cjjkj8bdvz5856s1wzxwk5a5")))

(define-public crate-sgx-decode-pib-0.1 (crate (name "sgx-decode-pib") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)))) (hash "19g38ng648n15dlajibnk7g2amjz7yczknds2h22x6v44hx40ypx")))

(define-public crate-sgx-decode-pib-0.1 (crate (name "sgx-decode-pib") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)))) (hash "1nbdxqsfxywqbl264ianwfmx1sqnpnqbxhq6f8gj77di7pm2gqna")))

(define-public crate-sgx-decode-pib-0.1 (crate (name "sgx-decode-pib") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "0w9winhdxmprlr1m5dwka1i36zl0cfl8svlgcpz624r4mi28inxi")))

(define-public crate-sgx-isa-0.1 (crate (name "sgx-isa") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.5") (default-features #t) (kind 0)))) (hash "1bap80mxi54w919sp4jsrdi41p3xi1ljmxpjprr14skd6apyljl4")))

(define-public crate-sgx-isa-0.1 (crate (name "sgx-isa") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^0.5") (default-features #t) (kind 0)))) (hash "1s6fkvbsjxq88fs3ww7w9rbkja55iqmq82kk10d4xmn9kxf1ahqr") (features (quote (("large_array_derive"))))))

(define-public crate-sgx-isa-0.1 (crate (name "sgx-isa") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^0.5") (default-features #t) (kind 0)))) (hash "1gnylk497wqh7xfgjavl9ipd9957dpp9n5xgknjk1cmvqx72g9v3") (features (quote (("try_from") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.2 (crate (name "sgx-isa") (vers "0.2.0-rc1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "1wf4sq9k13lzg1phrs4iw3id03j7j83qvq7idvh5yyw8lmw2mgyh") (features (quote (("try_from") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.2 (crate (name "sgx-isa") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "0s4g028dziqpk73skbzx1y7yh94d22zwdq65s1g24177kpinlxfh") (features (quote (("try_from") ("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3 (crate (name "sgx-isa") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "1cydwmnb8xy8gg6p9z8ja2kj9hpy4s01hjjpcv7mv3vkdvi79pz2") (features (quote (("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3 (crate (name "sgx-isa") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "03n3h9fwyc49ksklxccjv4xjwaf5v4hycv2dfb8bi1n7gwq5k6bl") (features (quote (("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3 (crate (name "sgx-isa") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mbedtls") (req "^0.5") (features (quote ("sgx"))) (kind 2)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ix8fmqjs68jxav1h7yq9142ch78zly4mc5kp5z1zcigy2s7s2vm") (features (quote (("sgxstd") ("nightly") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.3 (crate (name "sgx-isa") (vers "0.3.3") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mbedtls") (req "^0.5") (features (quote ("sgx"))) (kind 2)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xxv776gmmh24zbwy2vmhv0gphb7wnr4mj5kk2n3x8s80jag1bam") (features (quote (("sgxstd") ("nightly") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.4 (crate (name "sgx-isa") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mbedtls") (req "^0.8") (features (quote ("std"))) (kind 2)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1pzhr0ykb02xgcx7yl7b727liapxd5iwh3w9i056qcjhjq1nxx80") (features (quote (("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-isa-0.4 (crate (name "sgx-isa") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mbedtls") (req "^0.12") (features (quote ("std"))) (kind 2)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1j9zp2ibya5b8i33f2za3jg4p7g2y0kknr5nzmklwrmg3k2nlx7h") (features (quote (("sgxstd") ("large_array_derive"))))))

(define-public crate-sgx-keyreq-0.1 (crate (name "sgx-keyreq") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)) (crate-dep (name "sgx-isa") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0585klsfbj5q42lg2r7xqfsf2b533r3i3b32k5x47caqlb6xp7yd")))

(define-public crate-sgx-keyreq-0.2 (crate (name "sgx-keyreq") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rdrand") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "sgx-isa") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "17d47b9vlda4yxpa5l1d741h4knpgqgv9yfmy1x4nnvzr6rh9m67") (features (quote (("std") ("default" "std"))))))

(define-public crate-sgx-keyreq-0.2 (crate (name "sgx-keyreq") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rdrand") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "sgx-isa") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "04xf53cddh8kasl9v0293bla1pm4a5xz44lq006jl8j6v6rh8ipb") (features (quote (("std") ("default" "std"))))))

(define-public crate-sgx-panic-backtrace-0.1 (crate (name "sgx-panic-backtrace") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (kind 0)))) (hash "10bcvw68mh5mr6lkgc9mq2vmiz813vd7k51ihydbsz876pzcpaq6")))

(define-public crate-sgx-quote-0.0.9 (crate (name "sgx-quote") (vers "0.0.9") (deps (list (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "1x31vwl6a9rwhgqv8hr48hbigdczn594vnhl1hndwl5fnlfkrcyy")))

(define-public crate-sgx-quote-0.1 (crate (name "sgx-quote") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "08cfnp48khv59vn1wdcgj7ip4ydp0q8b7i20nc6x30bvmxvhar51")))

(define-public crate-sgx-type-debug-0.1 (crate (name "sgx-type-debug") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "02222zwabx6lc1hj20b4z7chq36d5blbvsx3fjrvn2lhc3a0vz1x")))

