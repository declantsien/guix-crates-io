(define-module (crates-io sg b_) #:use-module (crates-io))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.0") (hash "16wxncanvncrghx7zwyw0fmippmavmay69z10w10lxwbns6jfm49") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.1") (hash "1nxmjj3na8zwpybv9lsff84y43dndx56zjspasw1wly0knsywb6x") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.2") (deps (list (crate-dep (name "yew") (req "^0.20") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "0bzwswplqvnv17ffy9yln6kdw6x701d1a1plz9jw6airbahikvvy") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.4") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "0sh48cy5vxzjnxg1lnrbqqi08w17snfb3l89pnr4m3kf3iha9c6r") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.5") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "0dki47ms6w6nmj3qbsf00h91gkhr8qa7w5cjmssr8rgass4wvnba") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.6") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "1s4xassdwsxxwsrafkmxnfd3aqvlkw1ayi2a780s0b79sszip4ib") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.7") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "13xnsyca249j84cp0rrznbsrvallhv2c99wzap35yn3x67n92wwy") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.8") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "1s3qlijd9x9zgpnhvpjd6dad12z3nz1cyibr3gzwf5q4qcmy43c1") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.9") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "0wijzkppvs7bhidr6k06fpfhmnwpwbgc5rfxda2skymfw297ix0x") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.10") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "1qkz8iw7l0d0xn2z744yqg78194dbmm1wdw7b8k5pzca2rhwqjmj") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.11") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "1x08pylfzg2hi3i5cqqi72y7nvwkfhin8k2vg18a8p8zh8bay542") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.12") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "033g1jysfq938py6v9hdqd826k918manqj25lz0hwl6k1yd3pwf0") (yanked #t)))

(define-public crate-sgb_wrobel-0.1 (crate (name "sgb_wrobel") (vers "0.1.15") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew_integration"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "1x1asaind3ajbq202bzsk2a3fjrvrihnsmc50r1s7k69rpl6anm3")))

