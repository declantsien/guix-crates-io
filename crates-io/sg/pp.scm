(define-module (crates-io sg pp) #:use-module (crates-io))

(define-public crate-sgpp-0.1 (crate (name "sgpp") (vers "0.1.0") (hash "1p1d6vv5fmqmqhsb5sv4agn3ba33qz8w13drv73fx4vaxypnx5mq")))

(define-public crate-sgpp-0.2 (crate (name "sgpp") (vers "0.2.0") (hash "0nzpa09cibw9i0yqh011ldmgxn4ib4mifqyiwv69xn2zh1cpn68i")))

(define-public crate-sgpp-1 (crate (name "sgpp") (vers "1.0.0") (hash "0vb88jgwbmysdkr8aaiidrqb0y115r33wwx7k2sljbs8dh8640nk")))

(define-public crate-sgpp-2 (crate (name "sgpp") (vers "2.0.0") (hash "00sij8ly23v4hrzs5778g3afqr7gia99j8allx40jswlc3wjjf6y")))

(define-public crate-sgpp-2 (crate (name "sgpp") (vers "2.0.1") (hash "12vxhpmxg54cdc3vpgq6989907zacc8z9a6ln7vsyh5d9k13wm2w")))

