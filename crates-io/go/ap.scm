(define-module (crates-io go ap) #:use-module (crates-io))

(define-public crate-GOAP-0.1 (crate (name "GOAP") (vers "0.1.0") (hash "033y7gjazr7gckrcrlps0pbfvn8v9q1bfl0w8b8s850gdsi4ibmq")))

(define-public crate-goap-ai-0.0.0 (crate (name "goap-ai") (vers "0.0.0") (hash "1yrh5m5v4682zcmbgkqjfzijdphphh5a8mm6cqm5gz50402ax6pl")))

