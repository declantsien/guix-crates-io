(define-module (crates-io go bb) #:use-module (crates-io))

(define-public crate-gobble-0.1 (crate (name "gobble") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1s17q2ixrljkcy94ipgdjwn7jkrgdm30zj9ldrjf48d7sd0vmmm4")))

(define-public crate-gobble-0.1 (crate (name "gobble") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "184fl0ny53952i7va60rp2ppkg1jzyhc91xwmxxzg79mkppkh0ji")))

(define-public crate-gobble-0.1 (crate (name "gobble") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0zyag4lvsgx51gka7w0m3d7fsinvcgsb0a7hf0d8ai2rp0740kh6")))

(define-public crate-gobble-0.1 (crate (name "gobble") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1skxb0qr4xvnhnrw5w1sf6546g5qpqpckbdrdrim86049q59ba42")))

(define-public crate-gobble-0.1 (crate (name "gobble") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0g1c9iik793975n0f61zllpwb47bxsy7bxhpx5hsax6g0w712l4g")))

(define-public crate-gobble-0.1 (crate (name "gobble") (vers "0.1.5") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0c86hv5qb0lf868051siy5dipmcqds8s6wfm3s2anm1cfrbl0d60")))

(define-public crate-gobble-0.1 (crate (name "gobble") (vers "0.1.6") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "021jdxh2hrkxnnij70vsldfzk1jgcdvfpl3a15fc3rwv36k9s9vq")))

(define-public crate-gobble-0.2 (crate (name "gobble") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0blym4mb04ck5p1k1fy6n9g8xmnz7i1r6pn4wmb3jhdpivfbq2z8")))

(define-public crate-gobble-0.2 (crate (name "gobble") (vers "0.2.1") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0s5ff9sg5cl1gci50yfglncz3c3rl8bpng2xacpvyvqbzv6dq1r5")))

(define-public crate-gobble-0.3 (crate (name "gobble") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1my2ipqv4l18kbl40jg8w6smr0q6jlqld117v022lisbv98y33m4")))

(define-public crate-gobble-0.4 (crate (name "gobble") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "03v4p0rq08gg5mdbqg2f5bg137284595mm2swy0dl0avfx5wmkmr")))

(define-public crate-gobble-0.4 (crate (name "gobble") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "14nnl52qzqa3rkxx0pq48d3faq4pz1kvflq1m0v0l2zk6477ir7c")))

(define-public crate-gobble-0.4 (crate (name "gobble") (vers "0.4.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1lg0kw6bxpllg8wpsrch49yfvmgdx5hp4960p2q78ngkbfmgaafx")))

(define-public crate-gobble-0.4 (crate (name "gobble") (vers "0.4.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "03gvvj0p2ffkiscxbcz24jx3f65p9k8qx6kqa7sfgynaic9f6y6x")))

(define-public crate-gobble-0.4 (crate (name "gobble") (vers "0.4.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1l94aiqm79y34vfq5j2csmjny9k1xznw0hw3sap8bqinxrma8sfr")))

(define-public crate-gobble-0.5 (crate (name "gobble") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "0hr814gam57yy9imx95gy9w7abxk0chczp0vkf35mhn2pr07w1dk")))

(define-public crate-gobble-0.5 (crate (name "gobble") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1ql1a17h41lia6l9399zq0i79l8a2q6slbrsnqnixx8pdxipcxzy")))

(define-public crate-gobble-0.5 (crate (name "gobble") (vers "0.5.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1dmkkvw1xdjgf7qfw5xlc2xa6mvpzgdxjss1y5a46jiv0ghksfh9")))

(define-public crate-gobble-0.5 (crate (name "gobble") (vers "0.5.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "14m5hayma30d5rw7g46jxg5s7c1zs1lpbrk5gpf4kfm22dd8hx68")))

(define-public crate-gobble-0.6 (crate (name "gobble") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "04kmzdckksslaw0lcy3p9sn977vjzidnbgwzybjkmx75nyvww0m8")))

(define-public crate-gobble-0.6 (crate (name "gobble") (vers "0.6.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1ri5xk5mibqjpxbc7v3wr5cpbqa66ihpgkki31nhrm721bbgjqvh")))

(define-public crate-gobble-0.6 (crate (name "gobble") (vers "0.6.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "0acjig9ni5r0rv3hkql09kd2mgxwx7qi333gw7bhb02kv77g033z")))

(define-public crate-gobble-0.6 (crate (name "gobble") (vers "0.6.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "058xv5b4hw40xa7zg0hbxmgyaydm46sk2idimyfnycja7xyhzl7r")))

(define-public crate-gobbledygit-0.1 (crate (name "gobbledygit") (vers "0.1.0") (deps (list (crate-dep (name "git2") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1n5pdjlk0h3y93qk15wqmm7j4953db6lcy9gvbg7m5jna9a4yjfk")))

(define-public crate-gobbledygook-0.1 (crate (name "gobbledygook") (vers "0.1.0") (hash "0qm93awbpa2hnw0c73ywn30z2z03ziag95f3a9pcvf7vnn9i581c")))

