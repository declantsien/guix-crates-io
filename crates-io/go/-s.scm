(define-module (crates-io go -s) #:use-module (crates-io))

(define-public crate-go-spawn-0.1 (crate (name "go-spawn") (vers "0.1.0") (deps (list (crate-dep (name "state") (req "^0.5.2") (features (quote ("tls"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1jhcmffagdsa8824gdhrk8qz2mj89b6v74bjyrckjphc62rqxlrd")))

(define-public crate-go-spawn-0.1 (crate (name "go-spawn") (vers "0.1.1") (deps (list (crate-dep (name "state") (req "^0.5.2") (features (quote ("tls"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0lpwgnm9s07vfbj260q8vy6hlgzfv3c5myrap1n71wahs7cjbji6")))

(define-public crate-go-spawn-0.1 (crate (name "go-spawn") (vers "0.1.2") (deps (list (crate-dep (name "state") (req "^0.5.2") (features (quote ("tls"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "13ak9h1mgfyh25md49a9mc2m040vk1sbxa3hkyrg520r7nhykf5p")))

(define-public crate-go-srp-0.1 (crate (name "go-srp") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "1pzxs9pig2vmi83pf232h9mb8jwbwvz3bgl8kdlgdcya028k129m")))

(define-public crate-go-srp-0.1 (crate (name "go-srp") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "1j5draj7n258qmcy2g924f5dhas0rpaks30w5qgjij3q3flr7kcy")))

(define-public crate-go-srp-0.1 (crate (name "go-srp") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "1phj47gdz0h6r4nvcfpxlzh3rhj6j43h5q4wcdikag723qxd5nqg")))

(define-public crate-go-srp-0.1 (crate (name "go-srp") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "1x5r1ivf24bxi8rplgqmkwylmv10wmrikdapiw3cy0sg7h8936z9")))

(define-public crate-go-srp-0.1 (crate (name "go-srp") (vers "0.1.4") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "0saqyg6lz8xlgyp0594drsbv2f2y6rz5kh065hgg6xrn7ppyg5fz")))

(define-public crate-go-srp-0.1 (crate (name "go-srp") (vers "0.1.5") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "0lvc0yv98yp0qi2nk75jkpjjpai4jayr875hwnbazzkj33w8dxy0")))

(define-public crate-go-srp-0.1 (crate (name "go-srp") (vers "0.1.6") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "0y91ik3a0ia21mdzpkk4f29a9c0ic27bh6iv90xzhzpagmmch31m")))

