(define-module (crates-io go -p) #:use-module (crates-io))

(define-public crate-go-parse-duration-0.1 (crate (name "go-parse-duration") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)))) (hash "0d4rqml3ph6xbqzz21mn3p4jcsfalirc66l1b5fscsg5cdq6c6k4")))

(define-public crate-go-parse-duration-0.1 (crate (name "go-parse-duration") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)))) (hash "1g9c5vd9d38k7nql3vf4qakfggf8fvhy4qhfmyrfbxbi92aqi2sm")))

(define-public crate-go-parser-0.1 (crate (name "go-parser") (vers "0.1.0") (hash "183ccxjsvy4m167cvgb0psa4xfk2b5j7fmijm9b3jchjdrbhzgsa") (features (quote (("default") ("btree_map"))))))

(define-public crate-go-parser-0.1 (crate (name "go-parser") (vers "0.1.4") (deps (list (crate-dep (name "borsh") (req "^0.10.3") (optional #t) (default-features #t) (kind 0)))) (hash "19in71c92i8qj2k7n10r5dr7qr1p153509y38mcg6wwvn4hvxgqc") (features (quote (("default") ("btree_map")))) (v 2) (features2 (quote (("serde_borsh" "dep:borsh"))))))

(define-public crate-go-parser-0.1 (crate (name "go-parser") (vers "0.1.5") (deps (list (crate-dep (name "borsh") (req "^0.10.3") (optional #t) (default-features #t) (kind 0)))) (hash "1z4pp0kwhk6l5rxqa1xvqfghikwybi73rkgf38g2a3z8193hzzr7") (features (quote (("default") ("btree_map")))) (v 2) (features2 (quote (("serde_borsh" "dep:borsh"))))))

(define-public crate-go-pmacro-0.1 (crate (name "go-pmacro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02yfairn5c6l5kjy8gvszxrdhlgv75dfhfdpp9pkznf07knxbrpc")))

(define-public crate-go-pmacro-0.1 (crate (name "go-pmacro") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0aj4cgiydway1nm550hni10caf24gpnrsazcl78inb016pmh13ry")))

(define-public crate-go-pmacro-0.1 (crate (name "go-pmacro") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "112y1514gnzybwv1v0v1ircpfnkgcgwyv2xa9jplyicvlbw6qfl6")))

