(define-module (crates-io go ua) #:use-module (crates-io))

(define-public crate-gouache-0.1 (crate (name "gouache") (vers "0.1.0") (hash "064rphn2ac294bzsjmmzhq9527llf1icmr4idwv5agfmplr7y2mb")))

(define-public crate-gouache-0.2 (crate (name "gouache") (vers "0.2.0") (deps (list (crate-dep (name "gl") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.21.0") (default-features #t) (kind 2)) (crate-dep (name "ttf-parser") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "usvg") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1c4czrh9mnr2glapd39nk4924d8v6689j982qxliklsvkcmbdp1x") (yanked #t)))

