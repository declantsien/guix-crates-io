(define-module (crates-io go e-) #:use-module (crates-io))

(define-public crate-goe-admin-0.0.0 (crate (name "goe-admin") (vers "0.0.0") (hash "1qwk63nqsxkjbqm1p84ywmlcr8y5c41hc61izajpna768drwag5d")))

(define-public crate-goe-algorithm-0.0.0 (crate (name "goe-algorithm") (vers "0.0.0") (hash "10r293ypi0b2awi1l810y8kb0dv8kb23h505g7d4wrwdabgcdykv")))

(define-public crate-goe-amqp-0.0.0 (crate (name "goe-amqp") (vers "0.0.0") (hash "0r66w6cf5qaqyjapyfa6lmy1rslh117l7syvyblibhy3kj5zyl4i")))

(define-public crate-goe-android-0.0.0 (crate (name "goe-android") (vers "0.0.0") (hash "0dsqv8ddrbbvl7bal6clyvxf9cxpd46ikqk27a89snygly5ripxf")))

(define-public crate-goe-auth-0.0.0 (crate (name "goe-auth") (vers "0.0.0") (hash "0lhprdl8gn9ic8fn8pnv85w4dbq3xd5rmzxxbbd7jddbpmh00bxb")))

(define-public crate-goe-batch-0.0.0 (crate (name "goe-batch") (vers "0.0.0") (hash "1ik3ca5czlfyd5bisdm9cqqb39hd7lbv8b6dic9l3j9i7adcnc9b")))

(define-public crate-goe-bench-0.0.0 (crate (name "goe-bench") (vers "0.0.0") (hash "11fz3cff2pvsf8w475klxhw8m4p1bz3xrqifdnn456v7wv9n8c4a")))

(define-public crate-goe-cache-0.0.0 (crate (name "goe-cache") (vers "0.0.0") (hash "11smylrk6vv0faxrsmrqw0drsi747is51jllhjaqb6fr8pvl5a5g")))

(define-public crate-goe-cassandra-0.0.0 (crate (name "goe-cassandra") (vers "0.0.0") (hash "0ggzsrgxfpxgna31ii2mibfgi3nbv926kdn473x3bhcymjns3js1")))

(define-public crate-goe-circuit-breaker-0.0.0 (crate (name "goe-circuit-breaker") (vers "0.0.0") (hash "0xs739laa1nh93214zqm4dfgx1c8cbq757c267sy5gj8z5527yb7")))

(define-public crate-goe-cli-0.0.0 (crate (name "goe-cli") (vers "0.0.0") (hash "1a7b7x00a5hnjsgm1ayq8p5pj7afgfslr9jizy5gj7y72in16f5y")))

(define-public crate-goe-cluster-0.0.0 (crate (name "goe-cluster") (vers "0.0.0") (hash "1lkjr3zxbz4ysr73hiawjzhqnh6r60pjkwhzm256r2jggna6ain6")))

(define-public crate-goe-codec-0.0.0 (crate (name "goe-codec") (vers "0.0.0") (hash "1cd9qvh73qbd06ibli4w8lg6gha2izi43zsc0gq2hvkv6d6kvqzz")))

(define-public crate-goe-codegen-0.0.0 (crate (name "goe-codegen") (vers "0.0.0") (hash "1qynj5rzm585fv6mmpdyjb0x6hj6qfz3r7m2nj6pqb19y502wi12")))

(define-public crate-goe-config-0.0.0 (crate (name "goe-config") (vers "0.0.0") (hash "06226l28ry3x8zlxzlij0rv0bxcgmmi77b0g1cpd1zp7ipq0lrlw")))

(define-public crate-goe-connection-0.0.0 (crate (name "goe-connection") (vers "0.0.0") (hash "1ynaz1vpx4hcx6m7jq8db52dv6l1724ijgz9857czfk4jal0p0r1")))

(define-public crate-goe-consul-0.0.0 (crate (name "goe-consul") (vers "0.0.0") (hash "1xk81dvxwyqrs4bw4822xh0mvn8khqbwsjmz4hlna4fbyzx2fh8c")))

(define-public crate-goe-cookie-0.0.0 (crate (name "goe-cookie") (vers "0.0.0") (hash "0vhw3i3b567fmf8ycprpg0d88m1mmwfc4k1pr14vhd2ccg8hi8wa")))

(define-public crate-goe-core-0.0.0 (crate (name "goe-core") (vers "0.0.0") (hash "13hn7ysa1dlncfnxah2ah7lwbgy0pjwzf1fsgql6gw99z1crw67d")))

(define-public crate-goe-cors-0.0.0 (crate (name "goe-cors") (vers "0.0.0") (hash "0ywn6x03z3z1rq3zsqixm6ygxzkwbrz4zldf9ncy4b3k58j3g1i7")))

(define-public crate-goe-discovery-0.0.0 (crate (name "goe-discovery") (vers "0.0.0") (hash "1il225zhfgxscip8ldvdffcalg16910h67y7bb5dl5qddrxc3k5g")))

(define-public crate-goe-ftp-0.0.0 (crate (name "goe-ftp") (vers "0.0.0") (hash "1ym8c6xsrfanj67654xa9svkq76dqsspqhx2nql5amsbawdypsgh")))

(define-public crate-goe-gateway-0.0.0 (crate (name "goe-gateway") (vers "0.0.0") (hash "08fcxbacfywz62fbqbqsqf8vkal2vmqpaikjb7s5la2hk07z509a")))

(define-public crate-goe-graphql-0.0.0 (crate (name "goe-graphql") (vers "0.0.0") (hash "1iw8g349gv5vgqhn5946rr8r8nahzzi1v1yf13c8nr74k1ckyz3g")))

(define-public crate-goe-health-0.0.0 (crate (name "goe-health") (vers "0.0.0") (hash "1z6zwllb3k2sasj6v543cn8f5d9f32ryrby2znzbqgdbqq546wjc")))

(define-public crate-goe-http-0.0.0 (crate (name "goe-http") (vers "0.0.0") (hash "0w9084pf7rl6iqaig8gp6zvcfgchkqb2gs9rh7xaivi3a857fvls")))

(define-public crate-goe-ios-0.0.0 (crate (name "goe-ios") (vers "0.0.0") (hash "1v9h03z156hp0lsm8gj5nn1n4lc5n87hbmwkmhv436yq2py5j85b")))

(define-public crate-goe-kafka-0.0.0 (crate (name "goe-kafka") (vers "0.0.0") (hash "1n33ifvkz7al0llxj3dn2qhr80ssa8aj7gf0vq90884rx90kfpx3")))

(define-public crate-goe-log-0.0.0 (crate (name "goe-log") (vers "0.0.0") (hash "0pi8s6jhv10rgmig321lb6kpdz27vpfci54q9qsgwf8v9cp696gc")))

(define-public crate-goe-mac-0.0.0 (crate (name "goe-mac") (vers "0.0.0") (hash "1m3qwaq7dbdlk3wdxjjkwjp1fr9c0c3h6psdx2shpqgg3ks86c01")))

(define-public crate-goe-macro-0.0.0 (crate (name "goe-macro") (vers "0.0.0") (hash "08asb46iqdzhxkcjskcp6800bk7z75najfg12m4s9jmdkfn2nsgy")))

(define-public crate-goe-mail-0.0.0 (crate (name "goe-mail") (vers "0.0.0") (hash "0hx2pz338k0xdrrhqcd5p4nnss95rgr1z7ayp3nlnwgh14gimfq9")))

(define-public crate-goe-metrics-0.0.0 (crate (name "goe-metrics") (vers "0.0.0") (hash "003yjlw01naja0svqrj9v0bizsgwaq6qfpyfc0w1926q16jbf6wj")))

(define-public crate-goe-mobile-0.0.0 (crate (name "goe-mobile") (vers "0.0.0") (hash "04dcwjbkgqzr8v0r0383lq3rmqi4ybdq9h3kabjw6i0w0j2x9m8i")))

(define-public crate-goe-mongodb-0.0.0 (crate (name "goe-mongodb") (vers "0.0.0") (hash "1z6vzmg24vfsf83i45s3018gkbk9vy1llfzvm8bgrky40yxw2rrr")))

(define-public crate-goe-mqtt-0.0.0 (crate (name "goe-mqtt") (vers "0.0.0") (hash "1mgmvrqysb5a1rb6rd94sf1vcv6wxwa2v79csc0q7m5qysgqr5mv")))

(define-public crate-goe-mysql-0.0.0 (crate (name "goe-mysql") (vers "0.0.0") (hash "1fp86w7vgp9s6mhivc3isnv0jrl46i9mwnxs4anxljrgqn4cyl33")))

(define-public crate-goe-net-0.0.0 (crate (name "goe-net") (vers "0.0.0") (hash "009phjy9qjajax2kqgs63slhbg8796nzcsqh6920d6sqzryjcyvz")))

(define-public crate-goe-player-0.0.0 (crate (name "goe-player") (vers "0.0.0") (hash "0qa2p7gwkmm65kdpv4sk1w4p3npcxh998khrqkzv501bp2lanm6h")))

(define-public crate-goe-pool-0.0.0 (crate (name "goe-pool") (vers "0.0.0") (hash "1kzi3ahs8lag8zvhgfvx03bncxxs2qjwn4vdfsdz1lg3qz2wbj1x")))

(define-public crate-goe-process-0.0.0 (crate (name "goe-process") (vers "0.0.0") (hash "07jz54rrwnl0kmb96rig897kxv5w21bbhvghhz1fp4m0dhr2cxn7")))

(define-public crate-goe-prometheus-0.0.0 (crate (name "goe-prometheus") (vers "0.0.0") (hash "1jrn091x97a6bzjq3brn997j0rmr8wdg5waz3975qgb39xfpy635")))

(define-public crate-goe-quic-0.0.0 (crate (name "goe-quic") (vers "0.0.0") (hash "0b7ayg7c59mqbbarhs4yqm61p9apczvnwqg9nz4xkz1jjk9pv6rv")))

(define-public crate-goe-rabbitmq-0.0.0 (crate (name "goe-rabbitmq") (vers "0.0.0") (hash "0ac6xn19vrdbygjlmg3vpw56zhxhp8c68z7h81yvzm73cz4z6m5l")))

(define-public crate-goe-ratelimit-0.0.0 (crate (name "goe-ratelimit") (vers "0.0.0") (hash "15yhbjdmsmi1vp3qvqzgqc22irnn9llnh92g7w30iscfag9f11is")))

(define-public crate-goe-redis-0.0.0 (crate (name "goe-redis") (vers "0.0.0") (hash "1mf21xlh7sykjhzf5xqm64c6irfxijnlc0bsmgvp4v0pn6hdwngz")))

(define-public crate-goe-router-0.0.0 (crate (name "goe-router") (vers "0.0.0") (hash "04gl4s0y5plrdn9hh0j15iqmf97dzzgm6sw1qvss74hj9kv8hk05")))

(define-public crate-goe-rpc-0.0.0 (crate (name "goe-rpc") (vers "0.0.0") (hash "1sa3dx5l9045mg9m73ad6fk84slblbzczhfnail606rsgizxhxr8")))

(define-public crate-goe-rt-0.0.0 (crate (name "goe-rt") (vers "0.0.0") (hash "190plwhha9yiayasrm1dpxyamdfrvzsxb699lh8l2r188k9np9b6")))

(define-public crate-goe-rtmp-0.0.0 (crate (name "goe-rtmp") (vers "0.0.0") (hash "09vf7wsp7k91csyp3salxwsq39al5ip7igr0iz6srw49f34rafk0")))

(define-public crate-goe-session-0.0.0 (crate (name "goe-session") (vers "0.0.0") (hash "0pfynh1qbp9yrw1qihlxqm1n9in6ls34sa9qdxara80h79mcycxm")))

(define-public crate-goe-sharding-0.0.0 (crate (name "goe-sharding") (vers "0.0.0") (hash "0dhx4gdcggz7ck2q20jacxxhd8ly99h8csbl48fpjl9d9zqmlxl0")))

(define-public crate-goe-signal-0.0.0 (crate (name "goe-signal") (vers "0.0.0") (hash "00ib9fwinirzyq23f86sp63vkrsxbhcb5z8p6znk0zs1fnsggb86")))

(define-public crate-goe-sql-0.0.0 (crate (name "goe-sql") (vers "0.0.0") (hash "03xsy43inv373dzjp85slfj0l5svykxnssr9i94n619ja4b4dpjh")))

(define-public crate-goe-sqlite-0.0.0 (crate (name "goe-sqlite") (vers "0.0.0") (hash "1c1ijs5rd7jmi0k7r9q5qf0j86jcyq40m1j9ffkg2iidhj0lbldj")))

(define-public crate-goe-stomp-0.0.0 (crate (name "goe-stomp") (vers "0.0.0") (hash "0zpq0fk9r000x580h305crf4lkxfm0hikfh3rz33w3h9wjawiqjl")))

(define-public crate-goe-system-0.0.0 (crate (name "goe-system") (vers "0.0.0") (hash "1x2d2hljn2p4fbggzc67kajqan06nh32hdmq7xamiyk64bz9ccnn")))

(define-public crate-goe-template-0.0.0 (crate (name "goe-template") (vers "0.0.0") (hash "0rvlqwkmrv4ycdbsxqxvdkgk0r8780yx7pmpbq1i3jzpd7f7yzpq")))

(define-public crate-goe-test-0.0.0 (crate (name "goe-test") (vers "0.0.0") (hash "1f8r5djxvac4ynpx5425jannzp4m3whijjk0cnb31qfgjwmjf7gv")))

(define-public crate-goe-threadpool-0.0.0 (crate (name "goe-threadpool") (vers "0.0.0") (hash "0xz6dlc5j7d2h9gap0zqcwsgrq8b6pq7114hids4q5k60y97r3d9")))

(define-public crate-goe-tls-0.0.0 (crate (name "goe-tls") (vers "0.0.0") (hash "0wkdsf0x5818wc50730c8ii6a7967d49cm0hic9209w2bwz6r884")))

(define-public crate-goe-type-0.0.0 (crate (name "goe-type") (vers "0.0.0") (hash "11d7ga6biay7ivadz19vn87gwm6wv6h27nk8xd48nlpwn6cna3iz")))

(define-public crate-goe-unix-0.0.0 (crate (name "goe-unix") (vers "0.0.0") (hash "0x2gdyq0nn1sb8cqpm1n47v2dpi5a7jppz683jaylhn2ml5lrgp1")))

(define-public crate-goe-util-0.0.0 (crate (name "goe-util") (vers "0.0.0") (hash "0bi49mi4z679s2fmhh0l5w6c1qii4hmaysrisizj1509lbh6ql9g")))

(define-public crate-goe-wasm-0.0.0 (crate (name "goe-wasm") (vers "0.0.0") (hash "1xmrzdswk2dgv8cvymiimspqzxhp9f4r8ji6si6iqx93snyag9ry")))

(define-public crate-goe-web-0.0.0 (crate (name "goe-web") (vers "0.0.0") (hash "0nfbyk707gy4p1bpn1ivv7i1skixxi9smxzk0wf1iv35nqjmpp2w")))

(define-public crate-goe-win-0.0.0 (crate (name "goe-win") (vers "0.0.0") (hash "07rrmpzav69ll63r5myb82nj7kl1jgbf8rm1kjvkazs0x9s0bdgs")))

