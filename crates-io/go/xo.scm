(define-module (crates-io go xo) #:use-module (crates-io))

(define-public crate-goxoy-address-parser-0.0.1 (crate (name "goxoy-address-parser") (vers "0.0.1") (hash "0plw413q5ir1hmj67721wfpq5302fxhipgw0rxx2pdlgxq5wqn49") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.2 (crate (name "goxoy-address-parser") (vers "0.0.2") (hash "0p6xvky6325j65wpw4sm3kbjhi2xjsx2jd3zdkd6l1i4sri7jqqc") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.3 (crate (name "goxoy-address-parser") (vers "0.0.3") (hash "1dw8qp5jng4hdx6a99kgnhbbv9qx1yxllpg8x20k5zslrmfmf329") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.4 (crate (name "goxoy-address-parser") (vers "0.0.4") (hash "1c3cjq30g1w85vj2hw6sl8j5i0s5wj4blns4gcvr83jfc6vm1cm4") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.5 (crate (name "goxoy-address-parser") (vers "0.0.5") (hash "037fb1ls004dw3qn2yah3g2zshzz413gpvsxymiqb50y6j48mlwx") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.6 (crate (name "goxoy-address-parser") (vers "0.0.6") (hash "0kmmyh8iysyid0d6pzjnr035vz4y27ymh6k043q6hc2v483x2251") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.7 (crate (name "goxoy-address-parser") (vers "0.0.7") (hash "0ala4ndw8ixc1i5w7cgrikn70v1309gmkis5irzp4z8876zlic3x") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.8 (crate (name "goxoy-address-parser") (vers "0.0.8") (hash "0lfyjfrv6nsw468w1lvgbncbj0jbv0ln9bmds7fs4b05ypqc2s9v") (yanked #t)))

(define-public crate-goxoy-address-parser-0.0.9 (crate (name "goxoy-address-parser") (vers "0.0.9") (hash "1fhp3zsrkcyl908zbql1wjvzrmjwsymk4frw4148cldk6hcfm47k")))

(define-public crate-goxoy-address-parser-0.0.10 (crate (name "goxoy-address-parser") (vers "0.0.10") (hash "0ajxxqzxgmq1w2klfpfr0yw85fcmkcpgznxh8fk48if3f964jc10")))

(define-public crate-goxoy-file-chunker-0.0.1 (crate (name "goxoy-file-chunker") (vers "0.0.1") (deps (list (crate-dep (name "blake3") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)))) (hash "0am30csn3wn5d7hq623dsvljisq1l2v0xc6kg8xwnaq743a75zkb")))

(define-public crate-goxoy-file-chunker-0.0.2 (crate (name "goxoy-file-chunker") (vers "0.0.2") (deps (list (crate-dep (name "blake3") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)))) (hash "1zm08d7p0vsg15m06zgjr4ji8x4wz2znwljwgrx9rqflwf1m0j00")))

(define-public crate-goxoy-file-chunker-0.0.3 (crate (name "goxoy-file-chunker") (vers "0.0.3") (deps (list (crate-dep (name "blake3") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "1yp3y19y6kbzpwdg2yzj82c1j6y2xd1f1f9z2pynv01a4ciafchf")))

(define-public crate-goxoy-fragmented-db-0.1 (crate (name "goxoy-fragmented-db") (vers "0.1.0") (deps (list (crate-dep (name "goxoy-hash") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "goxoy-key-value") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0bmzdwdmzbk676kyvph9im23ywr99yjfkq73sginc7jy1m3jvzbs")))

(define-public crate-goxoy-fragmented-db-0.1 (crate (name "goxoy-fragmented-db") (vers "0.1.1") (deps (list (crate-dep (name "goxoy-hash") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "goxoy-key-value") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1yalh9jgr2rwmlp2w9rswqz5l9k3bn2qfpfbz0bw9vl5hcs44yaw")))

(define-public crate-goxoy-hash-0.0.1 (crate (name "goxoy-hash") (vers "0.0.1") (deps (list (crate-dep (name "blake3") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "02a36nscvyxhz4qqacczyyf9x1vnprxkg7gyql4ijdpi8rzwpma6") (rust-version "1.66.1")))

(define-public crate-goxoy-interpreter-0.1 (crate (name "goxoy-interpreter") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "032wbki8x77zxxbmf4ik6wkw6lll8g8vk5kacq7w3a897426msmc") (features (quote (("show_raw_bytecode") ("show_bytecode"))))))

(define-public crate-goxoy-interpreter-broom-0.0.1 (crate (name "goxoy-interpreter-broom") (vers "0.0.1") (deps (list (crate-dep (name "hashbrown") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1mv7f8y95hlp4dr6fx1c9ycw8fd7nhbi92fwwckb795z0knpnmsy") (yanked #t)))

(define-public crate-goxoy-key-value-0.1 (crate (name "goxoy-key-value") (vers "0.1.0") (deps (list (crate-dep (name "rusqlite") (req "^0.29.0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "16c3dvshsqplhvvsp7rcvqfra9pi2gr7mnyc7xmxl1ndm2qng6wv") (yanked #t)))

(define-public crate-goxoy-key-value-0.1 (crate (name "goxoy-key-value") (vers "0.1.1") (deps (list (crate-dep (name "rusqlite") (req "^0.29.0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "03fxkv6wyh737jbsbg4y7r6axjqhky0nc3zpshqz0gd1229di0ih") (yanked #t)))

(define-public crate-goxoy-key-value-0.1 (crate (name "goxoy-key-value") (vers "0.1.2") (deps (list (crate-dep (name "rusqlite") (req "^0.29.0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0aiggs1kwjfibchcnslnxqls1l5md3dd9g8nvxp3k3kf80z74vhb")))

(define-public crate-goxoy-mempool-0.1 (crate (name "goxoy-mempool") (vers "0.1.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0i281rkz0j6n23659sachqyw8rxn7szmrq52hdin2ix0qhg8advd")))

(define-public crate-goxoy-p2p-0.1 (crate (name "goxoy-p2p") (vers "0.1.0") (deps (list (crate-dep (name "bs58") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "goxoy-address-parser") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "goxoy-key-value") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1c38ka21l0h4y0sqn9sw8w3yn7vy76fakr8y6bxkws3dg18kfjjc")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.0") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "08yh2iqfj7q2ac9la6kn58naj62a8vi0da4fmh2qyfgkqp4m31kg") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.1") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "17qm7srmrgi3xzxa40xp17qqixnrvpv4pspi58ngp9l65x5j8mvl") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.2") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0kg9s2bl2safc6nmf29qj54xzxli7jq6h3dkmva015w88c88snrv") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.3") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1n23rma4yxnj4dp2l7xi86gnpyd0b226d1sc6n9xdhy2aiw6qi5g") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.4") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0jd7ip50fwfd56hgfby4qm8hipklrzh1sgydfjdgadrw7dvs4031") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.5") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1gy9p38ly88w9ymq2fp0b7d3r173fa36h5xa90hjy8hdc75garfw") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.6") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0lx25hvms0bh7mqsnpz37vp96rf1gm8ay51mivx8iyvqi6hm992g") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.7") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "029jiq7kl2qafj3fcwd32ibrinwcyghzcvq03q1prs8cg8a7zs1m") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.8") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1id14i2g8bi7z1dv5ac0fm311i67pkkv4zidvr47s9ir76l4nrn4") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-client-0.1 (crate (name "goxoy-socket-client") (vers "0.1.9") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1imky79g0ixz4v34lhy5z99rwdsr6l7xl3jki1y0zg0m2rawlf3b") (rust-version "1.66.1")))

(define-public crate-goxoy-socket-server-0.1 (crate (name "goxoy-socket-server") (vers "0.1.0") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "goxoy-socket-client") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "17hrv0210xpiqkgw08am1s7awldxcdjczf17bpmyab70zkd6vl4w") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-server-0.1 (crate (name "goxoy-socket-server") (vers "0.1.1") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "goxoy-socket-client") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0jld8vj2ldyvizhw44c34vgr8dgpd2hm2vvijhny52pwcn2jd7wf") (yanked #t) (rust-version "1.66.1")))

(define-public crate-goxoy-socket-server-0.1 (crate (name "goxoy-socket-server") (vers "0.1.2") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "goxoy-socket-client") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0kwnlpj4h17k6jrm9dgi5lzkgl7qsahwa59wc2jsfmgwkz7rrbdb") (rust-version "1.66.1")))

(define-public crate-goxoy-sysinfo-0.1 (crate (name "goxoy-sysinfo") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.29.10") (default-features #t) (kind 0)))) (hash "0p33h0iw9jz6azaz6adyihyi0lgjyljan1m1q3l383xl5h3hi8hy") (yanked #t)))

(define-public crate-goxoy-tar-archive-0.1 (crate (name "goxoy-tar-archive") (vers "0.1.0") (deps (list (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0hv660n0w6v72v6h7ns6ylnnn6j9kw0cjnxnp0hh6qmqpzkkvd94")))

(define-public crate-goxoy-tar-archive-0.1 (crate (name "goxoy-tar-archive") (vers "0.1.1") (deps (list (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1hx748bb1lvs4850w5azw455a8syvxx3bpjm3h427351ilqzxpk7")))

(define-public crate-goxoy-tar-archive-0.1 (crate (name "goxoy-tar-archive") (vers "0.1.2") (deps (list (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0fpk76bfpn7gphpnq9v69k02i3xp7wa7s3mdh27hjnkz76p3x6sj")))

(define-public crate-goxoy-tar-archive-0.1 (crate (name "goxoy-tar-archive") (vers "0.1.3") (deps (list (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0sxhdcjangqwxd25cadxndjqnlby2b4jm8a7k2imqjfvgzlmvrd7")))

(define-public crate-goxoy_storage-0.1 (crate (name "goxoy_storage") (vers "0.1.0") (deps (list (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0wbqr8z9g0xh1xc9l6g5p0hxfsy28g0s91gjcrcqzcp5yxh277wa")))

(define-public crate-goxoy_tcp_common-0.1 (crate (name "goxoy_tcp_common") (vers "0.1.0") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1jmndrfdxqvinnnh6wx8r5v7yg2zwdshz44zkmavw0md1gwj13g9")))

(define-public crate-goxoy_tcp_common-0.1 (crate (name "goxoy_tcp_common") (vers "0.1.1") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1j77xw300qyk0mv7n5wdirnazrdx5hrlgnhdy5sy6sabnk35q72c")))

(define-public crate-goxoy_tcp_common-0.1 (crate (name "goxoy_tcp_common") (vers "0.1.2") (deps (list (crate-dep (name "goxoy-address-parser") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1pmzkd57fplmiqlhspz5xb3znha09qfqbk4xf28p1wkxqb5sq5b1")))

