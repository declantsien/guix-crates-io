(define-module (crates-io go -f) #:use-module (crates-io))

(define-public crate-go-flag-0.1 (crate (name "go-flag") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0awd9yrqjzqcwf6ksdm1kana2ww1anlgdfmafgsi6xahrb4l0jkb")))

