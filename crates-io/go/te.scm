(define-module (crates-io go te) #:use-module (crates-io))

(define-public crate-gotest-0.1 (crate (name "gotest") (vers "0.1.0") (hash "0pa88flk5lnkg0lzwadd6120al0l3h4gwsiklhxx9yq8rqzw5vln") (yanked #t)))

(define-public crate-gotest-0.1 (crate (name "gotest") (vers "0.1.1") (hash "0dfahqiixhw7662xj93klvi9j7y5c6fr6kls7zrn990qav5ll1j6")))

