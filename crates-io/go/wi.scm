(define-module (crates-io go wi) #:use-module (crates-io))

(define-public crate-gowiz_country-0.0.1 (crate (name "gowiz_country") (vers "0.0.1") (hash "06gvlwqqrl0jn2jcn9mblik5p1mb4wj6q8qxcsb9sdncz88xlb8i")))

(define-public crate-gowiz_country-0.0.2 (crate (name "gowiz_country") (vers "0.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1km0jygxkh2qm66d31wwykplc28iwv5rfm93a5lgx2q73p53dj55")))

(define-public crate-gowiz_country-0.0.3 (crate (name "gowiz_country") (vers "0.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1hhk356jnn81wd82b0bz8kqb8xncrxv1dc0dkwh6g1c26d8qipj4")))

(define-public crate-gowiz_country-0.0.4 (crate (name "gowiz_country") (vers "0.0.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0nbglf8xq3kslyyma36fkdg4wn6z4r3yj0bsxii3lv9c8qr3m87h")))

