(define-module (crates-io go ry) #:use-module (crates-io))

(define-public crate-gory-0.0.11 (crate (name "gory") (vers "0.0.11") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1fkpqpv2287fgb4bmzkgwvhl7hvh8nhipvrqby749rdm9bl81vl8")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1sa58zbwg5shkj35hcagyq3rihlrdjssxy9bdnbg2nlv8b1g506n")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1gap3ghsmgy73hj850k1sz1mxzwacfpadb28442z17zwhsi1lagb")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0wxri4328a23pbcinjrqqglzkn92j40hgvr7rmg6k571xlq32xgw")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1vhbxj5n4mk2xn4jbz5x69dkwsjyfwag5039sls8babciq6vprzi")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1w8480b2yxvciv903l8zsq0jrc9rsw01dq1rdqyzyn0dp9mlb88v")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.10") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1zx3jyq2f6h2j2nm2pfg93f7dj9k454c18d1ivi4vwb6ifc2f70y")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.11") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1j7vs1pal7x31z1pramhb66h0svswli0drhzalr0p70pcri31p6p")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.13") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0blm41mqcpswzn34majlkd6i96agi4jybqyinwlnd9bsmzhkrvhd")))

(define-public crate-gory-0.1 (crate (name "gory") (vers "0.1.14") (deps (list (crate-dep (name "lazy_static") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1pg18vc0bvfma1wb69x9jjb6rjf51kiyqif6qckacr46lfg1rz8q")))

