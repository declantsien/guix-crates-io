(define-module (crates-io go of) #:use-module (crates-io))

(define-public crate-goof-0.1 (crate (name "goof") (vers "0.1.0") (hash "09c96ks6sq26wbd5vqx1axvdp45p9jbjd25llvkyfv0n9myc76x6")))

(define-public crate-goof-0.2 (crate (name "goof") (vers "0.2.0") (hash "1y4n4apk9z26c17rbyv6cgcs9y8lk0gk6sbjdcrbdbb4wa8grzj5")))

(define-public crate-goof-0.2 (crate (name "goof") (vers "0.2.1") (hash "1jx64l5d47dz4y4aysbiywj8dbvmbafpd7jm1bp41s4f6fcib2q8")))

(define-public crate-goof-0.2 (crate (name "goof") (vers "0.2.2") (hash "14ch42lhhvm2dds8rfb0psxihms9xwja56pblpwz9vkn1hhkzhly")))

(define-public crate-goof-0.2 (crate (name "goof") (vers "0.2.3") (hash "1p5fyqqnjkvy80mg7qqsc4li2klj20wmfwdrdcnfnplmj98lkzlk")))

(define-public crate-goofy-0.1 (crate (name "goofy") (vers "0.1.0") (deps (list (crate-dep (name "digest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json" "cookies" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0vw0lz7fzzcmlczbi8m17iakmb0dy50rwmx023ql6yz43m3p895n")))

(define-public crate-goofy-0.2 (crate (name "goofy") (vers "0.2.0") (deps (list (crate-dep (name "digest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json" "cookies" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "01w4i03cg5sdkl7qncklk87wv1x8jx400f5r98aahf8j2b9nyglj")))

(define-public crate-goofy_standardlib-0.1 (crate (name "goofy_standardlib") (vers "0.1.0") (hash "1pzxc6sd7migggwcgcnyd5fw5cq674kpw62w8j4b05rbncmckbj0")))

(define-public crate-goofy_standardlib-0.1 (crate (name "goofy_standardlib") (vers "0.1.1") (hash "1377p38q41s9z6r0l4v82hcz6khbg2292n3r4x56q9kbd6rswwyz")))

