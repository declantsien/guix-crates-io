(define-module (crates-io go _c) #:use-module (crates-io))

(define-public crate-go_core-0.1 (crate (name "go_core") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "01hrj31b5v2vsgh47a98hxk96nwnc8j1167h92143332q0mmsz1r")))

