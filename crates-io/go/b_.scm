(define-module (crates-io go b_) #:use-module (crates-io))

(define-public crate-gob_archive-1 (crate (name "gob_archive") (vers "1.0.0") (deps (list (crate-dep (name "gob_rs") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "07v1cvazbr5qjkdpwmdfl1iqk0wbx82blr9i2ap3xjy4smh7f5my")))

(define-public crate-gob_archive-1 (crate (name "gob_archive") (vers "1.0.1") (deps (list (crate-dep (name "gob_rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0ycifgglp9lpqyfiiww9vkhkd6k5l8ab9ahsk9py80qg4ar5iy5b")))

(define-public crate-gob_archive-1 (crate (name "gob_archive") (vers "1.0.2") (deps (list (crate-dep (name "gob_rs") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1jbqfpgbm5i4vqh2d5ahzng672bwbw2y0y97mg3z90x5zhklj4mq")))

(define-public crate-gob_rs-1 (crate (name "gob_rs") (vers "1.0.0") (hash "1w4pvnmi9ncn5pf1hw5xzzvksj93aj6vb921901b3pc9jsw7r5y6") (yanked #t)))

(define-public crate-gob_rs-1 (crate (name "gob_rs") (vers "1.1.0") (hash "1xig2pvhj3lfdcgypkyafybkxgx2xj5rz88xcx58gx3isg1a6rsv")))

(define-public crate-gob_rs-1 (crate (name "gob_rs") (vers "1.2.0") (hash "1mz9w3rx1dsaydih866fnvb1fx7lqqgg66bg05pn2205424k48lm")))

(define-public crate-gob_rs-1 (crate (name "gob_rs") (vers "1.2.1") (hash "04989q6kfmvdfnk3nnwmdj577rwjiy2kraxbaazry9j94s2pa20y")))

(define-public crate-gob_rs-1 (crate (name "gob_rs") (vers "1.3.0") (hash "1j8j1pj6fsxmay5vnqcddagvvpjsl277xl9h12qj05awnvsgb764")))

