(define-module (crates-io go sp) #:use-module (crates-io))

(define-public crate-gospel-0.1 (crate (name "gospel") (vers "0.1.0") (deps (list (crate-dep (name "gospel-dump") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "1abj4maf4l8m7ma5rjbhkybhmwy88sqxkfagzb4zvmj187n3bhfi") (features (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.1 (crate (name "gospel") (vers "0.1.1") (deps (list (crate-dep (name "gospel-dump") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0ffvf7ssxz08gkx0mfqx3rabf2l2lyz9z04rjlbrj4jp2jdnz04m") (features (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.2 (crate (name "gospel") (vers "0.2.1") (deps (list (crate-dep (name "gospel-dump") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0sg6dccd9vwj1pqd6ckashpwm234wsr3acfxiyqw943charaqzvv") (features (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.3 (crate (name "gospel") (vers "0.3.0") (deps (list (crate-dep (name "gospel-dump") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0hm0ij9m5fq58jaibnyys4l97q3b7wlm9xydd3x7sqprp7xhw22c") (features (quote (("dump" "gospel-dump"))))))

(define-public crate-gospel-0.3 (crate (name "gospel") (vers "0.3.1") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "19v5ibpvmp0q3blbiwxa0ij8y56a6s29z00d77lg3ngfykkcrvnc")))

(define-public crate-gospel-dump-0.1 (crate (name "gospel-dump") (vers "0.1.0") (hash "0adzyw1ay57da6a795zyk1irravkqdgmsjpji26821v0mb5f0x4s")))

(define-public crate-gosper-0.1 (crate (name "gosper") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.13") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "1qvwh44k5npkivc02sf87asfcqx3lm143pxkhp5j0qmpcy8b8z86")))

(define-public crate-gospin-0.1 (crate (name "gospin") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jcpxdgabr2wpk84xfr0ks1984jb89ri1m7j4m7i520xzgfzh3d2")))

(define-public crate-gospin-0.1 (crate (name "gospin") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16zzjd8z8jdx3wli9d1m0ld8awvw4l80hlcya32j3fnbjdvmfh97")))

(define-public crate-gospin-0.1 (crate (name "gospin") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1b5lya4fq1r54zz4yfg6vbac2hml90ps9zyvdinsbm1qgdyh3gk8")))

(define-public crate-gospin-0.1 (crate (name "gospin") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (features (quote ("ansi-parsing"))) (kind 0)))) (hash "014w33pglmaln4pgkw52m771lddw6k8azp46acy97cnjrz0qravm")))

