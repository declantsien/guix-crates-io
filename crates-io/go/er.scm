(define-module (crates-io go er) #:use-module (crates-io))

(define-public crate-goertzel-0.0.0 (crate (name "goertzel") (vers "0.0.0") (hash "0gzs0gpjbyibk9m9ylzvmwagiir4dyg0f7709h0mniq33vrgczkh")))

(define-public crate-goertzel-0.0.1 (crate (name "goertzel") (vers "0.0.1") (hash "0pbkk3gi4c0nh51j051rh8ajqb1hg21b8yf9xjvxj4nc1101vl12")))

(define-public crate-goertzel-0.2 (crate (name "goertzel") (vers "0.2.0") (hash "07jgraqz68lsl12h8zx5chcg80spf5gf6q5pyyppkzsakmavbcyz")))

(define-public crate-goertzel-filter-0.1 (crate (name "goertzel-filter") (vers "0.1.0") (deps (list (crate-dep (name "itertools-num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "10y3q1f1m5kk8jvy7lfsisrq1bfm4s123b7d8pq53c6af9bgwdsb")))

(define-public crate-goertzel-filter-0.1 (crate (name "goertzel-filter") (vers "0.1.1") (deps (list (crate-dep (name "itertools-num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0y0wz5vi97ypvjlhb08qmmw35pj95yirlkxbbw6pxhb5xhfkkjd1")))

(define-public crate-goertzel-filter-0.2 (crate (name "goertzel-filter") (vers "0.2.0") (deps (list (crate-dep (name "itertools-num") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wfnfrnkriqhh5x8hh0zrklpkhb2lfrzl7qshs4xw4i9wasxw8y4")))

(define-public crate-goertzel-nostd-0.1 (crate (name "goertzel-nostd") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00152mv584rdl8fi8iqswkg63xsch8hgkc0cqpnisfwwk03gi4ri")))

(define-public crate-goertzel-nostd-0.2 (crate (name "goertzel-nostd") (vers "0.2.0") (deps (list (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0dcm7114pv91risab680qyv1g8ajmv7imxfc3c49wq0d91k25p6i")))

(define-public crate-goertzel-nostd-0.2 (crate (name "goertzel-nostd") (vers "0.2.1") (deps (list (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0b5xf1bgykxd2hsrv90njxk288qjymis0vx4vhak4519b5xhngx6")))

(define-public crate-goertzel-nostd-0.3 (crate (name "goertzel-nostd") (vers "0.3.0") (deps (list (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "03qzprxl4lxxzg2dhxcdwlaj8znvkj7l97f98vxxhyqbvsk3b3bc")))

(define-public crate-goertzel_algorithm-0.1 (crate (name "goertzel_algorithm") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1bnfay6pxqq0hbkbvy9hnxgl98p14p5d5fgw5xcwjb939ixc669d") (rust-version "1.60.0")))

