(define-module (crates-io go su) #:use-module (crates-io))

(define-public crate-gosumemory_helper-0.1 (crate (name "gosumemory_helper") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "164l2am8xrqzrmgslypb4mr3icabpv82snqavaljv0n039j3ysfz")))

