(define-module (crates-io go ud) #:use-module (crates-io))

(define-public crate-gouda-0.1 (crate (name "gouda") (vers "0.1.0") (deps (list (crate-dep (name "deno_core") (req "^0.112.0") (default-features #t) (kind 0)) (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1n7ansqymigwbs9mgk51kisz9v0w3zmy2jkb6l41xm6i6db7vdbw")))

(define-public crate-gouda-0.2 (crate (name "gouda") (vers "0.2.0-alpha") (deps (list (crate-dep (name "deno_core") (req "^0.112.0") (default-features #t) (kind 0)) (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0mcacml5ilj3m6f4nl9fs7xxnyk9w52q4cphwkambcjn8n4apimx")))

