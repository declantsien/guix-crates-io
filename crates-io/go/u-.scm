(define-module (crates-io go u-) #:use-module (crates-io))

(define-public crate-gou-git-0.1 (crate (name "gou-git") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0b0myw3hylnml88jrihpb5mh0ygbpgs9kxg94i614gf6jiphq20i")))

(define-public crate-gou-git-0.2 (crate (name "gou-git") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pretty-log") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "1zb7p28n2g2qr84434c61ri7q3msyldvrsjkn3lmykbsj1yhicip")))

