(define-module (crates-io go du) #:use-module (crates-io))

(define-public crate-godunov-0.1 (crate (name "godunov") (vers "0.1.0") (deps (list (crate-dep (name "gauss-quad") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-lapack") (req "^0.10") (default-features #t) (kind 0)))) (hash "1xrafjrqf5wb8scabym03k46z27id7myg092zn2mj2hifg6gcb9c")))

