(define-module (crates-io go gl) #:use-module (crates-io))

(define-public crate-goglob-0.2 (crate (name "goglob") (vers "0.2.0") (deps (list (crate-dep (name "goglob-common") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "goglob-proc-macro") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1hzf711l2pkqc4psd3d3h6l0yzw66g3pm5bfiwqis7i8pf5dbzy2") (features (quote (("serde" "goglob-common/serde")))) (v 2) (features2 (quote (("proc-macro" "dep:goglob-proc-macro"))))))

(define-public crate-goglob-common-0.2 (crate (name "goglob-common") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.136") (optional #t) (default-features #t) (kind 0)))) (hash "0vwymzfdghwvbgzszr4zay9gkfq15ihy8sskx1k38hf31cf88m8j") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-goglob-proc-macro-0.2 (crate (name "goglob-proc-macro") (vers "0.2.0") (deps (list (crate-dep (name "goglob-common") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1i3gn2l0yiabaqx09y576midl7xzjr6f1rla4zfq3bv3gd43bi11")))

(define-public crate-goglob-proc-macro-tests-0.2 (crate (name "goglob-proc-macro-tests") (vers "0.2.0") (deps (list (crate-dep (name "goglob") (req "^0.2.0") (features (quote ("proc-macro"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "1hrd4mv6b9iqr86ak8x1zivxz06ln7vywmidklzrk4lzvyih70af")))

(define-public crate-goglob-serde-tests-0.2 (crate (name "goglob-serde-tests") (vers "0.2.0") (deps (list (crate-dep (name "goglob") (req "^0.2.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.136") (default-features #t) (kind 0)))) (hash "0mjmr6bpzkkwzwr1axgkkzhxz1gz54k3gyv8ack4zcp4xli4kzqj")))

