(define-module (crates-io go rn) #:use-module (crates-io))

(define-public crate-gorn-0.1 (crate (name "gorn") (vers "0.1.1") (deps (list (crate-dep (name "blake2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0phcbzcizp3rimsy4ll5nsgp9fwcb7yi189pwvym23mrj1ly21ly")))

