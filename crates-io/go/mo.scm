(define-module (crates-io go mo) #:use-module (crates-io))

(define-public crate-gomod-parser-0.0.1 (crate (name "gomod-parser") (vers "0.0.1") (deps (list (crate-dep (name "winnow") (req "^0.5.34") (default-features #t) (kind 0)))) (hash "02n8hc4mkw3c8icgkyklbdf06fjzpwvypys9yd818s116rakmd87") (rust-version "1.64")))

(define-public crate-gomod-parser-0.0.2 (crate (name "gomod-parser") (vers "0.0.2") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "winnow") (req "^0.5.34") (default-features #t) (kind 0)))) (hash "12008zsmx7hjakxz3yia0l84v1qynxharsb55sp6xqfzw60hmsdx") (rust-version "1.64")))

(define-public crate-gomod-parser-0.0.3 (crate (name "gomod-parser") (vers "0.0.3") (deps (list (crate-dep (name "indoc") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "winnow") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hhwzx4kd0j5sxxq5my77jl1990xf7998akd5bw6mca8jqd91lqv") (rust-version "1.64")))

(define-public crate-gomod-rs-0.1 (crate (name "gomod-rs") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4") (default-features #t) (kind 0)))) (hash "0jnw70p53hhjfs3gal8d7jqbilzp37z9g3hfnba61wsirqigv0qd")))

(define-public crate-gomod-rs-0.1 (crate (name "gomod-rs") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4") (default-features #t) (kind 0)))) (hash "1228ggxgwlx38w4vd0rcd0x6g59k3aryngrm6hafr87xbk1x025s")))

(define-public crate-gomoku-0.1 (crate (name "gomoku") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0qwmbxfw4xq9c34z3w17fv31z5i25ysi8imibn615r7wv1gcns6n")))

(define-public crate-gomoku-core-0.1 (crate (name "gomoku-core") (vers "0.1.0") (hash "10kv4nibab6b3c4a7lhi68wy59sfjhsfii0xivcgjz868h9p50na")))

(define-public crate-gomoku-core-0.1 (crate (name "gomoku-core") (vers "0.1.1") (hash "1ramdchx9knfd0l98ixw0s226vjfhysg05fq8hk39g7fbgshra2q")))

