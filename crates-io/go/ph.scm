(define-module (crates-io go ph) #:use-module (crates-io))

(define-public crate-gopher-0.0.1 (crate (name "gopher") (vers "0.0.1") (hash "1bghjq13p0yzvv7k298in515ysfl5lk2xb3adljwb5xmspyz1jsp")))

(define-public crate-gopher-0.0.2 (crate (name "gopher") (vers "0.0.2") (hash "1h5kv7p6367rjrczvz9x7yaq7mbdawymz0kl56zlcm6nrgh46ail")))

(define-public crate-gopher-0.0.3 (crate (name "gopher") (vers "0.0.3") (hash "17kqrd45zhcq5hwxqirgi5s0k56szn21jqwyf2xax1yg646jsnqj")))

(define-public crate-gopher-core-0.1 (crate (name "gopher-core") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-proto") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1") (default-features #t) (kind 2)))) (hash "0ycb4i5gjgmb5vwr873c945g357hxwqraiqxnk3j092wvv2yfl0d")))

(define-public crate-gopher-core-0.2 (crate (name "gopher-core") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-proto") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1") (default-features #t) (kind 2)))) (hash "0s5mlbw1p9bk8xnshfwzmz8agx2f0rn8943pm9j0sg7lfzv7rh8w")))

(define-public crate-gopher-core-0.3 (crate (name "gopher-core") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-proto") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1") (default-features #t) (kind 2)))) (hash "0k2f2hcpqp6qwsggs8zd90qwjwpra0w57pbg96hw9a06y2zlg227")))

(define-public crate-gopher-core-0.4 (crate (name "gopher-core") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tokio-proto") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1g5m3p60hxk728sqd5b95hpmhbxp7l44x4925mb4n64c7g4h93yv")))

(define-public crate-gopher-core-0.5 (crate (name "gopher-core") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-core") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "tokio-io") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "00xjal7cq6304sargw81r1qv2jq6kk19srm3l9i77nxz35s5lhzz")))

(define-public crate-gophermap-0.1 (crate (name "gophermap") (vers "0.1.0") (hash "1njvwm4jarn08n0dw94zv4zrp3i8ia37cag072842fsg8g74xzih")))

(define-public crate-gophermap-0.1 (crate (name "gophermap") (vers "0.1.1") (hash "0lp45n8lnslddyzrgymwj5z6b0bs8pzp7c5sssv0r3p6b0srpfm9")))

(define-public crate-gophermap-0.1 (crate (name "gophermap") (vers "0.1.2") (hash "0fxg6hfbghw0vl01psbahc530qj08k9c5nq7jfydr9fmz9miihkf")))

(define-public crate-gophers-0.0.1 (crate (name "gophers") (vers "0.0.1") (deps (list (crate-dep (name "native-tls") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "14p7pk63a5n4rrxqynjkq825ssliz754n2p8qchnqczx809dfp72")))

(define-public crate-gophersweeper-1 (crate (name "gophersweeper") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zvj83lgfg3q7l9z5pjrj71a41saw9pslinyhmx4svp00crxgic8") (yanked #t)))

(define-public crate-gophersweeper-1 (crate (name "gophersweeper") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1c3b5df5rijgngf61mf71jkrj64iac6bsljyyrm5jm3hlf0myf1p") (yanked #t)))

(define-public crate-gophersweeper-1 (crate (name "gophersweeper") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0hqxmfy1zpn2100f3889c09isya49w6hpaawmx265vd5633zr8dm") (yanked #t)))

(define-public crate-gophersweeper-1 (crate (name "gophersweeper") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1727nga2fwscb5ly9ig14gpr2splwcgrp12zqgsawjdwb8l6f1fm") (yanked #t)))

(define-public crate-gophersweeper-1 (crate (name "gophersweeper") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "17kr778fwmr63ydhmhgzv8rr3d75jnnk0wrlmwivc6rrim9a3nw9") (yanked #t)))

(define-public crate-gophersweeper-1 (crate (name "gophersweeper") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0hc9q6nd436lw30xz36ay6qhmxkmfgv71js4plr05hz8ijsz0ac2") (yanked #t)))

(define-public crate-gophersweeper-1 (crate (name "gophersweeper") (vers "1.0.6") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0q2rhri5cli45hdsd81qzcfbq385cj5asja5bldpsyv77psa3f43")))

(define-public crate-gophersweeper-lib-0.1 (crate (name "gophersweeper-lib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0mzy2dhliwv7184n1z91wgvv4jv3aa03mkqslkc46yq86jbc37wq") (yanked #t)))

(define-public crate-gophersweeper-lib-0.1 (crate (name "gophersweeper-lib") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0w69309ymjpi7mhlp95l4ziccqfsanajdnjf719zbbqgchiz4prz") (yanked #t)))

