(define-module (crates-io go ri) #:use-module (crates-io))

(define-public crate-gorilla-0.1 (crate (name "gorilla") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1s35fpqz9mm576d4p6js4kvidhxzlicid4k9naxh4lm3rlhnq1sk") (yanked #t)))

(define-public crate-gorilla-0.2 (crate (name "gorilla") (vers "0.2.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "00yy5mg1qj9n6bjh5v185lfghg0mcsas9saapcb9w4hsg8yc9i74")))

(define-public crate-gorilla-0.2 (crate (name "gorilla") (vers "0.2.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "10dwbbr1rkwxlhyjr45ngz4kr14vqjinh59zfybwpf2qw97qnvr4") (rust-version "1.56")))

