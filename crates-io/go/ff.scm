(define-module (crates-io go ff) #:use-module (crates-io))

(define-public crate-goff-0.1 (crate (name "goff") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.120") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.120") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0k89mqqdfv3msvbjk2s8xw8r2zfs66nmn6wqb1nw3wfza13mkv1s")))

(define-public crate-goffin-0.1 (crate (name "goffin") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ns2mb1b5akd4i28yg8hwzyrp9nhi8ny2337v2vd25n8g9k9m3aj")))

