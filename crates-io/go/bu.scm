(define-module (crates-io go bu) #:use-module (crates-io))

(define-public crate-gobuild-0.1 (crate (name "gobuild") (vers "0.1.0-alpha.1") (hash "1b0rqmhbf5mhs6kq7v5lsrhd4b2q3qaq2cwph4zj79nrslzq51w7")))

(define-public crate-gobuild-0.1 (crate (name "gobuild") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "17mcm6kih1m6g1n57hdxg5b9mg8k2h8nr50nh5gfngdzvnj5dqbi")))

