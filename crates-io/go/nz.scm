(define-module (crates-io go nz) #:use-module (crates-io))

(define-public crate-gonzales-0.0.1 (crate (name "gonzales") (vers "0.0.1-beta") (hash "1bzg9zy7cdv3j8mqg98p8dxzii1rj32c8fw6shqfdvnbg5xnb907")))

(define-public crate-gonzales-0.0.2 (crate (name "gonzales") (vers "0.0.2-beta") (hash "0b8izz3kp8zany3ibri94hmzwg2jacmyf0nhw2xjgkj3sjjxz792")))

(define-public crate-gonzales-0.0.3 (crate (name "gonzales") (vers "0.0.3-beta") (deps (list (crate-dep (name "actix-router") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("async_tokio" "html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.4.3") (features (quote ("criterion" "protobuf" "flamegraph"))) (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 2)) (crate-dep (name "route-recognizer") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "01w0prwnqqp527qqb593fd25y4cnhgczk1fz9k8yygp4l3h3m9j7")))

(define-public crate-gonzo-0.0.0 (crate (name "gonzo") (vers "0.0.0") (hash "01x1ggmhjmy3n0qylf8sjbfir0bx0hw3hkrzhyb2lfacnc7lhdby")))

