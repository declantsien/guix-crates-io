(define-module (crates-io go rs) #:use-module (crates-io))

(define-public crate-gorse-0.1 (crate (name "gorse") (vers "0.1.0") (hash "1n1705s6qcwyfqaddkn319r55r13lsv1zc4l3gcxlpi3a074m90k") (rust-version "1.56")))

(define-public crate-gorse_rs-0.4 (crate (name "gorse_rs") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 2)) (crate-dep (name "redis") (req "^0.22.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "086imwh39fwssnz5dachq7cz8mwzf4fxrnvybz7cbvx4hqzxc4yz")))

(define-public crate-gorse_rs-0.4 (crate (name "gorse_rs") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 2)) (crate-dep (name "redis") (req "^0.22.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.22.0") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "1hg19x3hwg6r0jhd226sah20d52dl8k62fczxjm2097k4sb2kz4s")))

