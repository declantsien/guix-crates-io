(define-module (crates-io go -h) #:use-module (crates-io))

(define-public crate-go-heap-rs-0.1 (crate (name "go-heap-rs") (vers "0.1.0") (hash "1bcd8kr2fjgysszpcjcng7zarvh91zqpnn1xlin7kn3823iibdwv")))

(define-public crate-go-heap-rs-0.1 (crate (name "go-heap-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0aqc3ml49733gk1hprins3r0ic0irgpwz6aafyvfy1f1jhjixaj3")))

