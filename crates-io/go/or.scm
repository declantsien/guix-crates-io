(define-module (crates-io go or) #:use-module (crates-io))

(define-public crate-goorm_edu_rs_kit-0.1 (crate (name "goorm_edu_rs_kit") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qw8qgyrq6nwpvdwqnfrqzh9q22142wd2z0l69rpb3lnk8xvr8dn")))

(define-public crate-goorm_edu_rs_kit-0.1 (crate (name "goorm_edu_rs_kit") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0kaz967fmglw36wbzn6rk7zy5h4ipw045sza57ill11b1j5m01s3")))

