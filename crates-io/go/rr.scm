(define-module (crates-io go rr) #:use-module (crates-io))

(define-public crate-gorrosion-0.1 (crate (name "gorrosion") (vers "0.1.0") (hash "18ag6ix7dg11hhcq9gwmxkljgvbx484p2pa8ldgxpcpz2i6fzp2c")))

(define-public crate-gorrosion-0.1 (crate (name "gorrosion") (vers "0.1.1") (hash "1p1781n0qjbx1dvy50fni97aqsd2hrl2ng5c30smp1qnflc1dqqg")))

(define-public crate-gorrosion-0.1 (crate (name "gorrosion") (vers "0.1.2") (hash "0di7vz01nzhq835ch58qmsflr0qvclj6fxmb697n7n20ywpszvrm")))

(define-public crate-gorrosion-gtp-0.1 (crate (name "gorrosion-gtp") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "141a98yfszp82md5a2rr8zmnm6b1fsi3hhgz489dk9kd15wby2c4")))

(define-public crate-gorrosion-gtp-0.1 (crate (name "gorrosion-gtp") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "006f9l640mfd9vgzw4dba0qzv3ms4clkwgpfv75pkjwjw2dk1mbh")))

(define-public crate-gorrosion-gtp-0.1 (crate (name "gorrosion-gtp") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "0k3gzpp2lq2mlwl52r8zll172l31x0k03q70ypbqyk6kfglz80r0")))

(define-public crate-gorrosion-gtp-0.2 (crate (name "gorrosion-gtp") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "19yqsfqvwvf2a6rv8wdmwixx0b2ngy9bj5mwr4xg16a7xy7jn1m6")))

(define-public crate-gorrosion-gtp-0.2 (crate (name "gorrosion-gtp") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "15vc24j66rmfnxhl3fj6d4iw5d8yqnd0bkbsk4bpf2a50rrhbrsx")))

(define-public crate-gorrosion-gtp-0.3 (crate (name "gorrosion-gtp") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "125n6xvhdfd2dblwigjc70xyhjsgwqycxn630ah20j34a9ha0wid")))

(define-public crate-gorrosion-gtp-0.4 (crate (name "gorrosion-gtp") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "0sxq3s30460pj2ik8z8z63xznvn7x57nh7pga5rq2wfwd8y4plbq")))

(define-public crate-gorrosion-gtp-0.4 (crate (name "gorrosion-gtp") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "1crz8vh9p9vfjxhk5nvp1pgz1y0sihnhwd6mlcdr1f5shb6bw1jc")))

