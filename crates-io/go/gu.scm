(define-module (crates-io go gu) #:use-module (crates-io))

(define-public crate-goguryeo_core-0.1 (crate (name "goguryeo_core") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1i4vs6rr1wjpkwwsxq6i9m3gyxcya0ifs3qf3s1s399fb0m5i4lk")))

