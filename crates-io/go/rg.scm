(define-module (crates-io go rg) #:use-module (crates-io))

(define-public crate-gorgon-0.0.0 (crate (name "gorgon") (vers "0.0.0") (deps (list (crate-dep (name "axum") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cyberdeck") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "04y5sjk4qfmrppxykkl8vjvwrnsd6hqzlar86q1yxa2g0ich1wi6")))

(define-public crate-gorgondb-0.0.1 (crate (name "gorgondb") (vers "0.0.1") (deps (list (crate-dep (name "blake3") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "1cz21r3ws5djap1zz88zyg0yj2k54fw8lb0fbvkigl6glv7slg9h")))

