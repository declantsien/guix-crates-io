(define-module (crates-io go lb) #:use-module (crates-io))

(define-public crate-golb-0.1 (crate (name "golb") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.3") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "upon") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0c88b2c9n2fad4m918nkys9jw9l12zccsv297g9qk18j821814i2")))

