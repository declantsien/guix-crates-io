(define-module (crates-io go ll) #:use-module (crates-io))

(define-public crate-gollum05670_my_art-0.1 (crate (name "gollum05670_my_art") (vers "0.1.0") (hash "0p5dbpb4g2v5rwvf5dr75b1wvl588b0mghn808zd8a5rgsrriaf3") (yanked #t)))

(define-public crate-gollvm-0.1 (crate (name "gollvm") (vers "0.1.0") (hash "1z2c8vibmhd1d0m2xsanys95af09dfb2jz4ql5swy4n6k4gff8vy")))

