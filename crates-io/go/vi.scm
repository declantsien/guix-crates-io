(define-module (crates-io go vi) #:use-module (crates-io))

(define-public crate-govind-measure-1 (crate (name "govind-measure") (vers "1.8.1") (deps (list (crate-dep (name "gemachain-metrics") (req "=1.8.1") (default-features #t) (kind 0)) (crate-dep (name "gemachain-sdk") (req "=1.8.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "11h1lyvzzmzprz272z0c8vvjy033sa8n2cr9rfxx86w4cd9kpbn2")))

