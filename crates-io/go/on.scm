(define-module (crates-io go on) #:use-module (crates-io))

(define-public crate-goon_proc_macros-0.1 (crate (name "goon_proc_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j3440n5aqbig5q2xpiffaib9l1ngmj0h8ic5m8lsxj6llbz2q9b")))

(define-public crate-goon_rs-0.1 (crate (name "goon_rs") (vers "0.1.0") (deps (list (crate-dep (name "goon_proc_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09lnzc6a153kwas39d5ijvb9inzh6qfr51p5qpdxy2ah51zl9nl0")))

