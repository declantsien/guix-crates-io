(define-module (crates-io go bs) #:use-module (crates-io))

(define-public crate-gobs-0.1 (crate (name "gobs") (vers "0.1.0") (deps (list (crate-dep (name "dot_vox") (req "^4.1.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.27.0") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.24.0") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "02qipy3y8416rfk7v9rxxwm9ci83ya97z4p60y67vvknjrdqsxsk") (yanked #t)))

(define-public crate-gobs-0.1 (crate (name "gobs") (vers "0.1.1") (deps (list (crate-dep (name "dot_vox") (req "^4.1.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.27.0") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.24.0") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "028vwia2wr2anx5v01z44b5cbmx8nw2jv9lqa8l2ldklzpv6nnkj")))

(define-public crate-gobs-0.2 (crate (name "gobs") (vers "0.2.0") (deps (list (crate-dep (name "dot_vox") (req "^4.1.0") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.29.0") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.26.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1q9bx5aspcg9axdzhcm810v8p0bxl8rbl15vns69hyqvqrl1s3rb")))

