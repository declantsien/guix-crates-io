(define-module (crates-io go lr) #:use-module (crates-io))

(define-public crate-golrs-0.1 (crate (name "golrs") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1iarjdl2xxb0bq2a2cfsqpwqr23lxqxdsk1rbq3aq71cfllm1ncz")))

(define-public crate-golrs-0.2 (crate (name "golrs") (vers "0.2.0") (deps (list (crate-dep (name "metrohash") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "08brinya6xm4kdd4dxhmcyfplmcl076qsiq3lhy4j0zhs09bchp7")))

