(define-module (crates-io kk cl) #:use-module (crates-io))

(define-public crate-kkcloud-0.0.3 (crate (name "kkcloud") (vers "0.0.3") (deps (list (crate-dep (name "kkcloud-client") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "kkcloud-server") (req "^0") (default-features #t) (kind 0)))) (hash "0w7ayf1mywf022nfhr52ryc2lhmffyyql0nx6b1fyivifq7hzkfr")))

(define-public crate-kkcloud-0.0.4 (crate (name "kkcloud") (vers "0.0.4") (deps (list (crate-dep (name "kkcloud-client") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "kkcloud-server") (req "^0") (default-features #t) (kind 0)))) (hash "1p68fxl6qs1rxfixg6d4kpjia0damc4l3gm9x0h5l6ywpbkdf8yk")))

(define-public crate-kkcloud-client-0.1 (crate (name "kkcloud-client") (vers "0.1.0") (deps (list (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "00f7ncir21r087g90lmn0b1q6ni4dxr39nd43p7vh70v6jz5sg0s")))

(define-public crate-kkcloud-engine-0.1 (crate (name "kkcloud-engine") (vers "0.1.0") (deps (list (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "1drwy28qxl3ll9wq10b4wbxnh50x7fkmxckpni07lnzkcmalw5vj")))

(define-public crate-kkcloud-framework-0.0.3 (crate (name "kkcloud-framework") (vers "0.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "113qf1zb2a43r265n7v791lgqbyh6fy0xhiz0mf7vffnrqyf0cp1")))

(define-public crate-kkcloud-framework-0.0.4 (crate (name "kkcloud-framework") (vers "0.0.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lzcxyz4j7bq66g1rrcx68cira580xq92d48qrmkp9rbfivsq9w5")))

(define-public crate-kkcloud-server-0.1 (crate (name "kkcloud-server") (vers "0.1.0") (deps (list (crate-dep (name "kkcloud-engine") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "kkcloud-framework") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dxd5prjdgrs0hda4pxl9n8kcwcazncd7znf6kn837p4d78gi600")))

