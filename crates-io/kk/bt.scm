(define-module (crates-io kk bt) #:use-module (crates-io))

(define-public crate-kkbt-0.1 (crate (name "kkbt") (vers "0.1.0") (hash "13rzw9qfigfpnxrrb5prifvxi460v1ayg97ayna4bs43cqxw0zps") (yanked #t)))

(define-public crate-kkbt-0.1 (crate (name "kkbt") (vers "0.1.1") (hash "0lq5148ix8d6m0w8d0l7yby9fnq6c7wq473qsn5mjrs6p8wx4xq2")))

(define-public crate-kkbt-0.1 (crate (name "kkbt") (vers "0.1.2") (hash "1ndx52ndv1ac6lcgidzvgf3p8qm0fy4a1ikm24nzqwdc418fy4f8")))

(define-public crate-kkbt-0.1 (crate (name "kkbt") (vers "0.1.3") (hash "0xlpk2s14336ya3k5ahj8wkfwh65jcp2966g44a6zic7svkqha23")))

