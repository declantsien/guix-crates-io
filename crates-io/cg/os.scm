(define-module (crates-io cg os) #:use-module (crates-io))

(define-public crate-cgos-0.1 (crate (name "cgos") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0mmhb890qhbhc2dyv47msa700wzn2pbacmgwhfvrxz4lzawar83m")))

(define-public crate-cgos-0.2 (crate (name "cgos") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "013lsrc8v1l1ambhazbyw7n1dgsvgg4dhzs1q90g9nvqzpdzjsma")))

