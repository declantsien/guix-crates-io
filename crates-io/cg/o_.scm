(define-module (crates-io cg o_) #:use-module (crates-io))

(define-public crate-cgo_oligami-0.3 (crate (name "cgo_oligami") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "16pignaz4qvq4crx3751i7mi9ac3pzb4kvwjxm1dhgnzpbrhhn2g")))

(define-public crate-cgo_oligami-0.3 (crate (name "cgo_oligami") (vers "0.3.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vgrcr13p2zk5givfapa547wdxgl0cqvflwb59q8cnx3y3i0lsyf")))

(define-public crate-cgo_oligami-0.3 (crate (name "cgo_oligami") (vers "0.3.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r9r26fqzwhabdj6437q08s3n4wfi95b6rph8vyacas73niw9hyz")))

(define-public crate-cgo_oligami-0.3 (crate (name "cgo_oligami") (vers "0.3.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vlqabnr3b957lp1rfh2ag6h6655brk43jix1fl3qazwmqscbh6p")))

(define-public crate-cgo_oligami-0.3 (crate (name "cgo_oligami") (vers "0.3.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pvikds299vrddrfx14sx1ka554mymd95mmd3490brpbm5ws7414")))

(define-public crate-cgo_oligami-0.3 (crate (name "cgo_oligami") (vers "0.3.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ns9f3pr6bv618qywjkgawrdxa6q3nawzn1m51ynxk11hwd7w1cs")))

