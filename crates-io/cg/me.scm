(define-module (crates-io cg me) #:use-module (crates-io))

(define-public crate-cgmemtime-0.1 (crate (name "cgmemtime") (vers "0.1.0") (deps (list (crate-dep (name "lexopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.7.1") (default-features #t) (kind 0)) (crate-dep (name "syscall-numbers") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "0v874ip2gar9bxqf0h0i54135smyx1c6rmrqnanhlnl2yw9kbxqz")))

(define-public crate-cgmemtime-rs-0.1 (crate (name "cgmemtime-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (features (quote ("signal" "resource"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hbrxwjryk571rf6855p9rpwzms9af2465zj0xadfybi8azhil1f")))

