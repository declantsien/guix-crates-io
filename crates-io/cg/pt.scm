(define-module (crates-io cg pt) #:use-module (crates-io))

(define-public crate-cgpt-0.1 (crate (name "cgpt") (vers "0.1.0") (hash "19jbn6qb242fcjz5gcyvjg1sp123qf6gn8g2h9pvakxzafd0886j")))

(define-public crate-cgpt-0.1 (crate (name "cgpt") (vers "0.1.1") (hash "0x282zlihcw25iina3fpasm88lbrbccwp91qwiryx7yy8qni4r5a")))

(define-public crate-cgpt-0.1 (crate (name "cgpt") (vers "0.1.2") (hash "0f8r77li3y8cjvkpwpkzqjxy2afp7zd203fmcpk1hclif1x5znkf")))

(define-public crate-cgpt-0.1 (crate (name "cgpt") (vers "0.1.3") (hash "0gd4bgcf2vqqb87xc7ia0ag77y80mg9zjqdx59xzq1c7bch5zw65")))

(define-public crate-cgpt-0.1 (crate (name "cgpt") (vers "0.1.4") (hash "0r3v4ay3pfwpr76mmmmg3jwvgzg7jxhl2im2pw29kv6s070m5ph9")))

(define-public crate-cgpt-cmd-0.0.1 (crate (name "cgpt-cmd") (vers "0.0.1") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0k4g3n9ywm77hxl70g0ixllph89yaipqcwmrcaiwl7c2hpxl6hwk")))

(define-public crate-cgpt-cmd-0.0.2 (crate (name "cgpt-cmd") (vers "0.0.2") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.154") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0agjxb10nz2hcmmj4fx5rnsjv0v9i1l7bwfd6zf95dp5ck45bdjw")))

