(define-module (crates-io cg i-) #:use-module (crates-io))

(define-public crate-cgi-attributes-0.1 (crate (name "cgi-attributes") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yqxgsihr8c9nz7s0m7kz1x54052gmagdaa2rr23n1vfjlny5lrm")))

(define-public crate-cgi-bin-0.0.1 (crate (name "cgi-bin") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("io"))) (default-features #t) (kind 0)))) (hash "0nqcsrhdxbd3d70z2rlg65yy5wf9a3x9xncx77w8w3lwqwy3dy5z")))

