(define-module (crates-io cg en) #:use-module (crates-io))

(define-public crate-cgen-rs-0.1 (crate (name "cgen-rs") (vers "0.1.0") (hash "1wlq498ahhn98j97r7bwhf9f31pz6wxfz9pii9p4x6qlarbg6bbc") (yanked #t)))

(define-public crate-cgen-rs-0.1 (crate (name "cgen-rs") (vers "0.1.1") (hash "1ky1lmgjc591cs8mbydyisg5ck028kx1kib75ryl4riq7m2iirbz") (yanked #t)))

(define-public crate-cgen-rs-0.1 (crate (name "cgen-rs") (vers "0.1.2") (hash "1in6hjz3n18mdrbfx9wab2drq6zjsmkj1k350169zwy8s5dmw0sx") (yanked #t)))

