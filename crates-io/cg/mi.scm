(define-module (crates-io cg mi) #:use-module (crates-io))

(define-public crate-cgminer-rest-0.1 (crate (name "cgminer-rest") (vers "0.1.2") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1prcvkv5rsjdwgqb9dzd2k26h4j5y9a976aid531s7vxw6n0qzwx")))

