(define-module (crates-io cg ol) #:use-module (crates-io))

(define-public crate-cgol-0.1 (crate (name "cgol") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (features (quote ("png"))) (kind 0)))) (hash "0r9wssdwlna1zs4jfypn3fcjvg7r5863bb6bncl731hsv226jjzs")))

(define-public crate-cgol-0.1 (crate (name "cgol") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.7") (features (quote ("png"))) (kind 0)))) (hash "0y1x2fil41m1h2zgcsn76jpbpr9j4ihwlrmd91qni1mxrq1dq2zf")))

(define-public crate-cgol-tui-0.2 (crate (name "cgol-tui") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "145ip2yrwhrhmmmzvh3rfkrp33a3amh3p6i18lpx813xahc5kdc3")))

(define-public crate-cgol-tui-0.2 (crate (name "cgol-tui") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "19n7rl4b5i8j4i6szn5asvr3bxi92j4kmp1rh55dad98kala6h5d")))

(define-public crate-cgol-tui-0.3 (crate (name "cgol-tui") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "11wzwzmsa688cq7mp36pm5hz3p42a0d86ak5fbs8gas95dq3ziwx")))

