(define-module (crates-io cg ns) #:use-module (crates-io))

(define-public crate-cgns-sys-0.2 (crate (name "cgns-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)))) (hash "0680g2qz78pjy7lx1ig3akihc87wrra3v9yllsnny1byi5mwf69f")))

