(define-module (crates-io cg -s) #:use-module (crates-io))

(define-public crate-cg-sys-0.1 (crate (name "cg-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1rwh6qqi1gscq317jjp442mjmaql8zm69rjafk89rksnp6zmhgvc") (features (quote (("static"))))))

