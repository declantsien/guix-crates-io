(define-module (crates-io cg -m) #:use-module (crates-io))

(define-public crate-cg-math-0.1 (crate (name "cg-math") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.7.0") (default-features #t) (kind 0)) (crate-dep (name "parry2d-f64") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "timeit") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0qwrxp90k5ai6fk524n4l2c0ds9sbqgzdyz7p6lmc52swjna0ssx") (rust-version "1.71.0")))

(define-public crate-cg-math-0.1 (crate (name "cg-math") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.7.0") (default-features #t) (kind 0)) (crate-dep (name "parry2d-f64") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "timeit") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0cld5vkmiz7qci5z51rgf05agbalb72n7h2kdlddfpbf39c14psy") (rust-version "1.71.0")))

(define-public crate-cg-math-0.1 (crate (name "cg-math") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.7.0") (default-features #t) (kind 0)) (crate-dep (name "parry2d-f64") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "timeit") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0vh6g4ifsnbis0pk3d08hvkpg8aijsxc85vz92sdaia9b6fnhxyy") (rust-version "1.71.0")))

