(define-module (crates-io cg at) #:use-module (crates-io))

(define-public crate-cgats-0.1 (crate (name "cgats") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 2)) (crate-dep (name "deltae") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "mktemp") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "statistical") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0li5grl3npf699k2fcr7rh7c9fs7j405azvrjl2dr0gyr0dc621b")))

(define-public crate-cgats-0.1 (crate (name "cgats") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 2)) (crate-dep (name "deltae") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "mktemp") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "statistical") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1f491mh6khxqlf1c6bcy1qml48z6npa2n35bz2qppzqr756lyzaz")))

(define-public crate-cgats-0.1 (crate (name "cgats") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 2)) (crate-dep (name "deltae") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "mktemp") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "statistical") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0gy6djyxf1ckjfawvajc7dv2g4rzmdpmx05nbkgslka1w4b65izv")))

(define-public crate-cgats-0.2 (crate (name "cgats") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (default-features #t) (kind 2)) (crate-dep (name "deltae") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1w1qzkyq8vpv5wpi44ibzj5dkcjamzpcral82i79yx9a3pqxlimr")))

