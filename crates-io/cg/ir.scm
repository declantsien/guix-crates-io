(define-module (crates-io cg ir) #:use-module (crates-io))

(define-public crate-cgirs-0.0.1 (crate (name "cgirs") (vers "0.0.1") (hash "1a2jzsmaad6ds5gh236s0nnjxa5hf6pl0qlj38sdh32brrd4khbn")))

(define-public crate-cgirs-0.0.2 (crate (name "cgirs") (vers "0.0.2") (hash "0zmp3phk720nbb4nbqnbijfhkcq2pi3p03hz88jdaxrr0x4h1dq0")))

(define-public crate-cgirs-0.0.3 (crate (name "cgirs") (vers "0.0.3") (hash "0s9pcz8v6dia5nzwl06299dscsw2vf7zzlzqhnflczm3rqp5vdmb")))

(define-public crate-cgirs-0.1 (crate (name "cgirs") (vers "0.1.0") (hash "0bv8bi7g3wx2jpq3z8mlghd9624fy8ci6nvcsz12p8snpzrgyjps")))

(define-public crate-cgirs-1 (crate (name "cgirs") (vers "1.0.0") (hash "0dn6z5swd3bvjczj14gaz38a7ki32flvdcwxiqw15h6fkyx9pcaf")))

(define-public crate-cgirs-1 (crate (name "cgirs") (vers "1.1.0") (hash "13spf5vm9lg2b9dw7lxllvrwjl5271jzd278fb48pfixc12vc97v")))

