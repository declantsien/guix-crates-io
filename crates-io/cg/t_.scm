(define-module (crates-io cg t_) #:use-module (crates-io))

(define-public crate-cgt_bacpop-0.1 (crate (name "cgt_bacpop") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16") (default-features #t) (kind 0)))) (hash "13j10nghg06bs7bqvkvmmbqi5mrhhypkxax67srs19vg85dmsfjc")))

(define-public crate-cgt_derive-0.4 (crate (name "cgt_derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "0nx51kg4jd0z6vcjamqk6svfzc2n4m226ab76ankg6bm1bg9in2h")))

(define-public crate-cgt_derive-0.4 (crate (name "cgt_derive") (vers "0.4.1") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "07z925c6kb2zbdpzaqxq6d6762jplva7nz5dlq9zd600mlp3mm36")))

