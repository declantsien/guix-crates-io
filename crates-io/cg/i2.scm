(define-module (crates-io cg i2) #:use-module (crates-io))

(define-public crate-cgi2-0.7 (crate (name "cgi2") (vers "0.7.0") (deps (list (crate-dep (name "cgi-attributes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ml20dgggiilrb65khgwbvdbs64sc9b2hv42hc40rs3vp729d09b")))

