(define-module (crates-io cg is) #:use-module (crates-io))

(define-public crate-cgisf_lib-0.1 (crate (name "cgisf_lib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1d4q6ar8gp5j8hzgnvjla5y0b8c3ifb2bgy8jykrvy2ji9fjfl6f") (yanked #t)))

(define-public crate-cgisf_lib-0.1 (crate (name "cgisf_lib") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1r712mr2jrl5z4ycq9spmzdvpygyb11qbs3rm2k3h9cr8q3qprgc") (yanked #t)))

(define-public crate-cgisf_lib-0.1 (crate (name "cgisf_lib") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1kfca3zqg06hb1plihcbjhjrhksvhjq00b1fyg70w87cghpicigw") (yanked #t)))

(define-public crate-cgisf_lib-0.1 (crate (name "cgisf_lib") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11by9sljc4a09vz12ch6rssrnbi4yqxla97cxyzl3y90kjjgi5jz")))

(define-public crate-cgisf_lib-0.2 (crate (name "cgisf_lib") (vers "0.2.0") (deps (list (crate-dep (name "const_format") (req "^0.2") (features (quote ("rust_1_64"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0mg6c6a2h27i1280zws5w4j9wkw22f6lj3y6y8wvczp4xqmnsyq2")))

(define-public crate-cgisf_lib-0.2 (crate (name "cgisf_lib") (vers "0.2.1") (deps (list (crate-dep (name "const_format") (req "^0.2") (features (quote ("rust_1_64"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1l6zcbybivrl840j3kxm5c7fkrmqdjka135cvqkavyyl45dvm2bw")))

