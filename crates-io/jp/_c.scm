(define-module (crates-io jp _c) #:use-module (crates-io))

(define-public crate-jp_cli-0.1 (crate (name "jp_cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0f9va1laibpy4y3qlk9s2r8y63avpc8j510c2101mbg7l7mvn6wi")))

(define-public crate-jp_cli-0.2 (crate (name "jp_cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1cfb5zaqww1v2bpbndf5s4r7kdkars116sck1va0f24vr5w22ix0")))

(define-public crate-jp_cli-0.3 (crate (name "jp_cli") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "exit") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0jgx323zhi3i9m57w032gvvj5r5w8wcz0b9pdmh29gcfkazxyq5k")))

(define-public crate-jp_cli-0.4 (crate (name "jp_cli") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "exit") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0dv53wq54508q326fi5n60zqpza9iffsz2g24ns46iijkfpjkddd")))

