(define-module (crates-io jp db) #:use-module (crates-io))

(define-public crate-jpdb-0.1 (crate (name "jpdb") (vers "0.1.0") (hash "11ykjjyax46sycibdsks8r9svpjb4rmpkadblj2y2wcv9g1mq2rg")))

(define-public crate-jpdb-0.2 (crate (name "jpdb") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json" "gzip" "tls"))) (default-features #t) (kind 0)))) (hash "16nchipfaxxravmka8r9q0h1q5kj4f4p4djhx00kgi9dpq79kvyh")))

(define-public crate-jpdb-0.2 (crate (name "jpdb") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json" "gzip" "tls"))) (default-features #t) (kind 0)))) (hash "07hv72qaqhn29fdq14y7sd0kv3xinc7c33ira9whf9fchhblnlcj")))

(define-public crate-jpdb-0.3 (crate (name "jpdb") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json" "gzip" "tls"))) (default-features #t) (kind 0)))) (hash "0d804p1fad31k6ak7da2adn07wj1z2r2a0kk5hgmfz9s1vwq25i8")))

(define-public crate-jpdb-0.4 (crate (name "jpdb") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json" "gzip" "tls"))) (default-features #t) (kind 0)))) (hash "1dq1gs57wnd4541rmg5byqgfk0zwdcd8nnjnh9ra8c3hbc1x55qa")))

(define-public crate-jpdb-0.5 (crate (name "jpdb") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json" "gzip" "tls"))) (default-features #t) (kind 0)))) (hash "0z7zk0q9lqd2rm4d9a05s19ziwpsflayvk6r8fzf5z6p1z1wnkb1")))

