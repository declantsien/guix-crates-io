(define-module (crates-io jp ut) #:use-module (crates-io))

(define-public crate-jput-0.1 (crate (name "jput") (vers "0.1.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yd4j8hd0k57jghkfl07ln192ahxknimhl2bn2hpxrpjrsmhzs9s") (yanked #t)))

(define-public crate-jput-0.1 (crate (name "jput") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cksjw6qyr2p4p885yjsyzysshx25731gpz20hj7kxqmfdba8kqs") (yanked #t)))

(define-public crate-jput-0.1 (crate (name "jput") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1prrdawr8n6kqyr2p8wqilv4br3lggpsnb282x1isk1f28zsf5x6")))

