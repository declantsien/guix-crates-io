(define-module (crates-io jp _m) #:use-module (crates-io))

(define-public crate-jp_multimap-0.1 (crate (name "jp_multimap") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 2)))) (hash "0npw3xnlnlviz5ziiigvl1k4w89b8ljjg1bpmcvngq9c74kgw082")))

