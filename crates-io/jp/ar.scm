(define-module (crates-io jp ar) #:use-module (crates-io))

(define-public crate-jpar-0.1 (crate (name "jpar") (vers "0.1.0") (deps (list (crate-dep (name "bytecount") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3.4") (default-features #t) (kind 0)))) (hash "0lf0x7ani4kxp05cs0cwghskqwmvv9iqij5kq7s80xvci53zv9dj") (features (quote (("alloc"))))))

(define-public crate-jpark011-guessing_game-0.1 (crate (name "jpark011-guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1a7kn558xs51zwdp1883mjnw3mp94jkjkyv55nl21zk9qmwjjkjp")))

(define-public crate-jpark011-guessing_game-0.1 (crate (name "jpark011-guessing_game") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "07wrk8glr8nc4yj3fvmrp7h7shy1xm1qg5b46ixp9vk09ki575vp") (yanked #t)))

(define-public crate-jpark011-guessing_game-1 (crate (name "jpark011-guessing_game") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0h8h487kfh5cf5imyyg7bm5qs7xcq9y60q7kcysb8rmnkzbffbf7")))

(define-public crate-jpark011-guessing_game-1 (crate (name "jpark011-guessing_game") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1h7r14mw49g3apy96mjyw2nw68wbsyscgjg9css0ij97w6b8f5pm")))

