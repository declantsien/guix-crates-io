(define-module (crates-io jp l-) #:use-module (crates-io))

(define-public crate-jpl-sys-0.0.1 (crate (name "jpl-sys") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)))) (hash "0lqiplvq2sxcz8ylr8vlba1nyimf1rzcsj208mk0rhqs33ya1zqx") (yanked #t) (links "jpl")))

(define-public crate-jpl-sys-0.0.2 (crate (name "jpl-sys") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)))) (hash "0m4wskz3aylm7bwslbajkz2r5vi74xs968jc9qr03r18y81g89jg") (links "jpl")))

