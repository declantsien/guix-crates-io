(define-module (crates-io jp -r) #:use-module (crates-io))

(define-public crate-jp-rs-0.1 (crate (name "jp-rs") (vers "0.1.0") (deps (list (crate-dep (name "pa-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0nnm5krff400x7ildf9krmc9k9kmxcnfjrqmq2f39ckn5vq6fjpj")))

(define-public crate-jp-rs-0.1 (crate (name "jp-rs") (vers "0.1.1") (deps (list (crate-dep (name "pa-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1sm4gl1dbja7my36sjb0hyb6fa41dmasnirp92jmglhi3zcb7dky")))

(define-public crate-jp-rs-0.1 (crate (name "jp-rs") (vers "0.1.2") (deps (list (crate-dep (name "pa-rs") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "150w90mbszs3y5vgnrlh7229sx74w3aals1s5avk6zw6wd8lr5pb")))

(define-public crate-jp-rs-0.1 (crate (name "jp-rs") (vers "0.1.3") (deps (list (crate-dep (name "pa-rs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "05nxqzd4l98gfy0pnl2nzlrq2a30lqp9w2y04dkxbhhc44dpy6q5")))

(define-public crate-jp-rs-0.1 (crate (name "jp-rs") (vers "0.1.4") (deps (list (crate-dep (name "pa-rs") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0nw8nj2b1ik76nnc9ca6imjp4l7kiw31snfi9qwn2hqzfvxbqajg")))

