(define-module (crates-io jp cd) #:use-module (crates-io))

(define-public crate-jpcd_game_of_life-0.0.1 (crate (name "jpcd_game_of_life") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "084lsf7pxnf1jl0i93d3kf2i6gcp0ggb4mkr6ajhd0sxpbjanwd2")))

(define-public crate-jpcd_game_of_life-0.0.2 (crate (name "jpcd_game_of_life") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0cvfvxkp81ddph3j03c4rddks19wfbrcknj9qwxqm94p2w1y7kj2")))

