(define-module (crates-io jp ho) #:use-module (crates-io))

(define-public crate-jpholiday-0.1 (crate (name "jpholiday") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1jxrpagbdmy0ih5bsjfsv7p0394pb2m30lsgq9cl6l7544z32i9v") (yanked #t)))

(define-public crate-jpholiday-0.1 (crate (name "jpholiday") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "012v66dggpb3bla9fkbi2lxp21q5jd3f1i4s4zx9p6j55f6y1lq5") (yanked #t)))

(define-public crate-jpholiday-0.1 (crate (name "jpholiday") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1v8rxj45ngi0g7qairfkb36hswd3a25cbz3iq2vfvrh45iddbf4s")))

(define-public crate-jpholiday-0.1 (crate (name "jpholiday") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "18vsd2cv1cj4fmqbwalpnkvsc279ls4rr87yxv1sg9df7l7fwbqd")))

(define-public crate-jpholiday-0.1 (crate (name "jpholiday") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1pwvr0qdxl8yz82l0cizjncqhdbv2hprxnvf8s27hwxsgwwjd7ki")))

