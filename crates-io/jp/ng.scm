(define-module (crates-io jp ng) #:use-module (crates-io))

(define-public crate-jpng-0.1 (crate (name "jpng") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19") (features (quote ("jpeg" "jpeg_rayon" "png_codec"))) (default-features #t) (kind 0)))) (hash "0fcgz4sspwfmf4zdxc5cwfs3wrslv47m42jq70f4j3vbls07vf52")))

