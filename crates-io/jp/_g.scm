(define-module (crates-io jp _g) #:use-module (crates-io))

(define-public crate-jp_graph-0.1 (crate (name "jp_graph") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8") (default-features #t) (kind 2)))) (hash "0g11ng03iwjhracdxkj8b0fbf5dmpz8ap3ldnx7l513ix1h43njv")))

