(define-module (crates-io jp ro) #:use-module (crates-io))

(define-public crate-jprops-0.1 (crate (name "jprops") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "05qpjq2gdfzbhzqqn5znq7kng1q2kydvsififif5w0bw10ywdiak")))

