(define-module (crates-io jp -l) #:use-module (crates-io))

(define-public crate-jp-location-relation-0.1 (crate (name "jp-location-relation") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1l266bw2gm2gvsc8qqq9wg7znimfcqag3cm5gy7a3mcc6i5daj6j")))

(define-public crate-jp-location-relation-0.1 (crate (name "jp-location-relation") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0pg5ys96gxi569xgmww7fnkby5v5gabcm16qgqj9nmhyvc3zl45j")))

