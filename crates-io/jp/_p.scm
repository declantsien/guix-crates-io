(define-module (crates-io jp _p) #:use-module (crates-io))

(define-public crate-jp_partition-0.1 (crate (name "jp_partition") (vers "0.1.0") (deps (list (crate-dep (name "multimap") (req "^0.1.0") (default-features #t) (kind 0) (package "jp_multimap")) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v92ddsc3g63dywdlvn55jn8zsac9ni3aifpbxf1xdvxlk3jkc6b")))

