(define-module (crates-io ps g-) #:use-module (crates-io))

(define-public crate-psg-core-0.1 (crate (name "psg-core") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1705mvdpzzn1hijh6d86rmhlbyyfnr6lf9q1c30dy50hs7l69nq8")))

(define-public crate-psg-core-0.1 (crate (name "psg-core") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "178nv58v3shas7xy6n92inml3mng8b73sfl77m1bkr2d4mdwvp5x")))

(define-public crate-psg-core-0.1 (crate (name "psg-core") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0h4dji5jk8dyfr8j8vrfzjid86ak2hqsqz8karsnaqqsyd6gyich")))

