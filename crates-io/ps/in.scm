(define-module (crates-io ps in) #:use-module (crates-io))

(define-public crate-psina-0.1 (crate (name "psina") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0ixkn9a18q2253d2yvr88lyk79vwdv047nf2bfi7iqk9mr7p9lq7") (yanked #t)))

(define-public crate-psina-0.1 (crate (name "psina") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1dxskjvv6k9a6pj3n2zw5xwr7wg8ls56ki5zq91nsmnvw9cmc6dz") (yanked #t)))

(define-public crate-psina-0.1 (crate (name "psina") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0yfcs4s2kryha3lwm5c8jmygh15a61rjx1ha1s9dpzwpnxi3aa7p")))

(define-public crate-psina-0.1 (crate (name "psina") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0nyzygm2wcg9yysiwd60a381gjvp6x0sw2hf3qd253arawhqx6ji")))

(define-public crate-psinode-0.0.0 (crate (name "psinode") (vers "0.0.0") (hash "10fkfraajn7wz7kr6jckxzra0dshrbglfra20i0dvf25c722wgf2")))

