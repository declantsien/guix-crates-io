(define-module (crates-io ps he) #:use-module (crates-io))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.0") (deps (list (crate-dep (name "sysinfo") (req "^0.23.9") (default-features #t) (kind 0)))) (hash "1dzpv648qggv6im5w7x2vyc82xs306wzfghbw8xw5qvbfbml5kwq") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.1") (deps (list (crate-dep (name "sysinfo") (req "^0.23.9") (default-features #t) (kind 0)))) (hash "0cngzyhr96xp7xnfgd1rdsb2zcrjn46iiks9hx7svadzr510y1n6") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.3") (deps (list (crate-dep (name "sysinfo") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "1zwggrd3751qkjmxkn34s1vnf1sj6crdp5mjj5pv80fzs4b33cjd") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.4") (deps (list (crate-dep (name "sysinfo") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "0sg6kqzf50xd9b7bhb5r8r6h9x6iw2k977vpj43l76lbbblw4pz1") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.5") (deps (list (crate-dep (name "sysinfo") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "07rd0vc57az431q9jq6dd4kr2rww1wknf2zjqnmwqfgz2d94djv2") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.7") (deps (list (crate-dep (name "sysinfo") (req "^0.27.1") (default-features #t) (kind 0)))) (hash "152gf9y5rl7dv6qb53siwpjv7397wxc8yb2wrsgp77isqaj3lyfs") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.8") (deps (list (crate-dep (name "sysinfo") (req "^0.27.1") (default-features #t) (kind 0)))) (hash "1rqa4sjbciq9k3vsdgcy1ymzv031wnj1l24kr47r53rkmvfg3yh9") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.9") (deps (list (crate-dep (name "sysinfo") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "00gjxvxsj0byz7kqmjdvg5r290kqdykrpab79w25g88cv87i3fwc") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.10") (deps (list (crate-dep (name "sysinfo") (req "^0.29.0") (default-features #t) (kind 0)))) (hash "0f2v3dd84i8ardym76qm0gw07mz94h41y9aaf393qgm21cxkblr3") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.11") (deps (list (crate-dep (name "sysinfo") (req "^0.30") (default-features #t) (kind 0)))) (hash "18lhcj43fjlz395wgg4hng8ddnyrvlx0x5d40xqf98w8g8nhrjak") (rust-version "1.58")))

(define-public crate-pshell-1 (crate (name "pshell") (vers "1.0.12") (deps (list (crate-dep (name "sysinfo") (req "^0.30") (default-features #t) (kind 0)))) (hash "1yb4s6jcjj74knngiv0p0h79x3cg3qjszdrvwik19whr66qmfc8g") (rust-version "1.58")))

