(define-module (crates-io ps x-) #:use-module (crates-io))

(define-public crate-psx-sem-0.1 (crate (name "psx-sem") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rbdh6r4lxlpyqb1x3j2ngzvyhhy36zci8n8p1x7f0kppw6kmqzs")))

(define-public crate-psx-shm-0.1 (crate (name "psx-shm") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xp3zmfcpcy5jmbkqq4g70yfz994jzsihr7slzfcjf2i4pjp8b28")))

