(define-module (crates-io ps ys) #:use-module (crates-io))

(define-public crate-psysinfo-0.1 (crate (name "psysinfo") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "0fpf17f1lkpnvq84hj1fy1xhfx5r9i9dalhrh3r7pdhp45zydj7r")))

(define-public crate-psysinfo-0.1 (crate (name "psysinfo") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "0srvzdvsvqrzgd894hhlglphylmn03262w9dxl965h2jbgbcdizy")))

(define-public crate-psysinfo-0.1 (crate (name "psysinfo") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "183im6vz527ykkjvmp5piwjmp9vn6fmny2ywqmngf0x3x93ggsy6")))

(define-public crate-psysinfo-0.1 (crate (name "psysinfo") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "045a2kxnmzwxsa219ja0x4p96za39a4x1d3mvvqkvlhjm7hn0pgb")))

