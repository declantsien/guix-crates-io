(define-module (crates-io ps f2) #:use-module (crates-io))

(define-public crate-psf2-0.1 (crate (name "psf2") (vers "0.1.0") (hash "0hc8fp9lx8a59zklzdfnxmi98v9i6xxymh3vfm8y5bz9s36h9b23")))

(define-public crate-psf2-0.1 (crate (name "psf2") (vers "0.1.1") (hash "09r2zj30bs63krrcfkf986zq3wxiyilmnfy7s64p3vbz7h8j6sdi") (features (quote (("std") ("default" "std"))))))

(define-public crate-psf2-0.2 (crate (name "psf2") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1vk14569mnklmbjg22s9gdpfz7xjhpfhfv5k4qzpxyj23b3yr880") (features (quote (("std") ("default" "std"))))))

(define-public crate-psf2-0.3 (crate (name "psf2") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("inline-more"))) (optional #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ixpi0yyg02pmi0jr7afl60n7bqnvm4bz9vfar03gj2hmzlzy5vc") (features (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.3 (crate (name "psf2") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("inline-more"))) (optional #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0zcagrr4i7i2sfy57x1pcsym57p9af0r3aqgic80bwp4hsn1ib91") (features (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.3 (crate (name "psf2") (vers "0.3.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("inline-more"))) (optional #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1mssvf2mr1n6rfwrkp3bc95danr2xbwvqv0dxwrvlq9xcf5vx54l") (features (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.3 (crate (name "psf2") (vers "0.3.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("inline-more"))) (optional #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ls0hjvqx6swyfmb32cidsmcdzvgffmnf5ypw6y9pynh0296c6xs") (features (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

(define-public crate-psf2-0.4 (crate (name "psf2") (vers "0.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("inline-more"))) (optional #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0mslzwnyd0p9z784454pb4xmpm2djg5lj76nhvzi7haqxqr62vnv") (features (quote (("unicode" "hashbrown" "rustc-hash") ("std") ("default" "std" "unicode"))))))

