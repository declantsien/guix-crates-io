(define-module (crates-io ps eq) #:use-module (crates-io))

(define-public crate-pseqsid-0.1 (crate (name "pseqsid") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "1nlgmflwrvx9zsjflpwnkz4fshc3mlvyiglny0knl9mppin2vczr")))

(define-public crate-pseqsid-0.2 (crate (name "pseqsid") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "0iq4bb0z7mhaybkhis4fc0q13jr5mhg44fjm8kf95mr6g1n2i9iw")))

(define-public crate-pseqsid-1 (crate (name "pseqsid") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "0pgzjk2cdk18gvppj5b6hkayn9f0k0ddhrl6dc49b8kmsnqd6h2d")))

(define-public crate-pseqsid-1 (crate (name "pseqsid") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "01p0bnf9cxv66375cy7s3yb91b9ykabsvpr4y2d6d4sl7jqf4bs4")))

(define-public crate-pseqsid-1 (crate (name "pseqsid") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "1xpchbnsgjnxhflbgsi22iwcacl1nlc5lk4r3hcjvxglqfghi2jh")))

