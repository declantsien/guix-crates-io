(define-module (crates-io ps p3) #:use-module (crates-io))

(define-public crate-psp34-full-0.2 (crate (name "psp34-full") (vers "0.2.1") (deps (list (crate-dep (name "ink") (req "^4.3") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2.9") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1q65drgj2b2bqmggbcd0vn4b7dnwml34xfna66pgzjgnxk0k2hc3") (features (quote (("test-only") ("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("enumerable") ("default" "std" "contract" "enumerable") ("contract"))))))

