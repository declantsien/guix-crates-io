(define-module (crates-io ps xm) #:use-module (crates-io))

(define-public crate-psxmem-0.1 (crate (name "psxmem") (vers "0.1.0") (deps (list (crate-dep (name "deku") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0vdlma2mbl4l35xldgf17bxlzqm4xdhwh3z44422pja341363ylx")))

(define-public crate-psxmem-0.1 (crate (name "psxmem") (vers "0.1.1") (deps (list (crate-dep (name "deku") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0nxfw7jaf1zskdk6yybblwz2hky2h9zn4mw2dbzc04lxzxf4krp1")))

(define-public crate-psxmem-0.1 (crate (name "psxmem") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "deku") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0fmhn0cm93npxl40jm87fz4w68fapf5dgx45dx5lif7zah2275z8")))

(define-public crate-psxmem-0.1 (crate (name "psxmem") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "deku") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.13") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1r04s17wjb3d2pbcfh4wfr823ba3q7j6rj0lf7abjkmn1d38wz72")))

