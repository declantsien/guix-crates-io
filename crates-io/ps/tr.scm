(define-module (crates-io ps tr) #:use-module (crates-io))

(define-public crate-pstr-0.0.0 (crate (name "pstr") (vers "0.0.0") (deps (list (crate-dep (name "once_cell") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "00ajnjj9m644rrr0gvy5rm38lrww41nns5wxg3mqphdikqikzdyi")))

(define-public crate-pstr-0.1 (crate (name "pstr") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "02f62br8kxlxxkg5mqk2f0lcmbq14x0n4wzy8prq3c2ic1qd0mda")))

(define-public crate-pstr-0.2 (crate (name "pstr") (vers "0.2.0") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "0i373pww69xbyrz4d5j82rs6kpgah8bqxi43l46z202ajlhkwvrv")))

(define-public crate-pstr-0.2 (crate (name "pstr") (vers "0.2.1") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "1d7v0k8g821i513isnkl6sxmj0xyzvckb39kw0bj8j8zviq37c5f")))

(define-public crate-pstr-0.2 (crate (name "pstr") (vers "0.2.2") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "0gck81d76l9nw8vj279789kg5frihfcqjv8nk4fwzyh6md4v4fni")))

(define-public crate-pstr-0.2 (crate (name "pstr") (vers "0.2.3") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "1hdxcs95np62k2rj21ykg7zrdacww5yl1gzxc4bdbqi4bha5s62f")))

(define-public crate-pstr-0.3 (crate (name "pstr") (vers "0.3.0") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "0lib6by061sgr9x611k2dlj0mhn891z98jg45yhm3za16vfvfnb8")))

(define-public crate-pstr-0.4 (crate (name "pstr") (vers "0.4.0") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "0zaq4qygk5pfsnfgjk559zdcmy0wfg1zl1jigml8wwdi7h2hxs3h")))

(define-public crate-pstr-0.5 (crate (name "pstr") (vers "0.5.0") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "1qfjzn8zwxpzmxddknc8khh70dk90rws6qr1iyi5q1p4367lrfqy")))

(define-public crate-pstr-0.5 (crate (name "pstr") (vers "0.5.1") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "0azr9sh2shvl4qpan0xx3g7msjanhiccnw3rl3c9nn9rb9iax191")))

(define-public crate-pstr-0.6 (crate (name "pstr") (vers "0.6.0") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "17cy6v7cf6qv3x9yn26icm6pmz2zfhnyrhr1c5an33svxf53p4s0")))

(define-public crate-pstr-0.7 (crate (name "pstr") (vers "0.7.0") (deps (list (crate-dep (name "dashmap") (req "^3.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)))) (hash "0hwwn95jij8n9pkw9axvks9kza7q850qnzkl0ljnnisi9c5fh1kz")))

(define-public crate-pstream-0.1 (crate (name "pstream") (vers "0.1.0") (deps (list (crate-dep (name "crc32fast") (req "~1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "vlock") (req "~0.2") (default-features #t) (kind 0)))) (hash "0gdqqylw4fndp65xz1hwiig08k7zxayfdnrqcln9hhb5l7165h71") (features (quote (("io-filesystem") ("default"))))))

(define-public crate-pstream-0.1 (crate (name "pstream") (vers "0.1.1") (deps (list (crate-dep (name "crc32fast") (req "~1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "vlock") (req "~0.2") (default-features #t) (kind 0)))) (hash "1v7w4rkky4pddb9jfwbinyk96hh3l98w8pc10im5cvdzagg6ngm4") (features (quote (("io-filesystem") ("default"))))))

(define-public crate-pstree-0.1 (crate (name "pstree") (vers "0.1.0") (hash "0xlbrhd5rh22d78bgi5pwgqnvjid82z1k747cpcq11gqc512xpmv") (yanked #t)))

(define-public crate-pstree-0.1 (crate (name "pstree") (vers "0.1.1") (hash "183pn8vilh2vpafkazbk3pzaqz4pam95friwascqfy62lgl9zhk1")))

