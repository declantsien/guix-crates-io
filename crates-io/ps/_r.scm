(define-module (crates-io ps _r) #:use-module (crates-io))

(define-public crate-ps_rust_fundamentals-0.1 (crate (name "ps_rust_fundamentals") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0vp9v3pc2yajm7jv4d6w6kh0vk6rsd657g15b2nhris82pi93ghl") (yanked #t)))

