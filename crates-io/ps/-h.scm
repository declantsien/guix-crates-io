(define-module (crates-io ps -h) #:use-module (crates-io))

(define-public crate-ps-hash-0.1 (crate (name "ps-hash") (vers "0.1.0-1") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "ps-base64") (req "^0.1.0-1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0mcc4ywjy1yw6snp5nq7hpyg08js6lv37c7nph6d9ivcywkxzg08")))

(define-public crate-ps-hash-0.1 (crate (name "ps-hash") (vers "0.1.0-2") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "ps-base64") (req "^0.1.0-1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0qv6k5y2wizn28g31kk09f95iyzc65nq4ya4i7vjkyljfv53yqmm")))

(define-public crate-ps-hash-0.1 (crate (name "ps-hash") (vers "0.1.0-3") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "ps-base64") (req "^0.1.0-1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1p8agbfkm0lskc3apmj2633f9q918zsh8mfdwyvyqlzwd74zvy2n")))

