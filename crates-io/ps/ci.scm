(define-module (crates-io ps ci) #:use-module (crates-io))

(define-public crate-psci-0.1 (crate (name "psci") (vers "0.1.0") (hash "01z09r4cmrqlglpik7bm80kp9nrf2lldfxnp6h9w9ssy1lpkxdh2") (features (quote (("smc") ("hvc") ("default" "hvc"))))))

(define-public crate-psci-0.1 (crate (name "psci") (vers "0.1.1") (hash "1awv3p506r00fv8yap6jzi730pcyj8xyk2x44xr4cd7i8ypf6x1k") (features (quote (("smc") ("hvc") ("default" "hvc"))))))

(define-public crate-psci-0.1 (crate (name "psci") (vers "0.1.2") (hash "0p22a56ql76ix9277hkzbrvl6b35ibhr7h2kwxaxjcqrvzjrd929") (features (quote (("smc") ("hvc") ("default" "hvc")))) (yanked #t)))

(define-public crate-psci-0.1 (crate (name "psci") (vers "0.1.3") (hash "0lryx9411gph6g3i5fs1734mayl0mp5lpzwllcg9nlr222zbfvrv") (features (quote (("smc") ("hvc") ("default" "hvc"))))))

(define-public crate-psci-0.2 (crate (name "psci") (vers "0.2.0") (hash "1m71zwxpzv55vsj2jwa2k3bpwa6v4zihyf0n6hs6xh0ls58bggzi")))

