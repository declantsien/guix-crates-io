(define-module (crates-io ps al) #:use-module (crates-io))

(define-public crate-psalm-0.0.1 (crate (name "psalm") (vers "0.0.1") (hash "15jbm4wza46b46xshfpjry2i7zsl5z4a04j0ymip6qmdcyasw3m7")))

(define-public crate-psalm-0.0.2 (crate (name "psalm") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "15g7j4frvjx3agw0dbldm8j06q46alxyxxfsyvriwsx064wx1b6v")))

(define-public crate-psalm-0.0.3 (crate (name "psalm") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1hikibhibag4vjrdrv2n3jv3xy1w02p4939i9g9bkgq33jl08xrx")))

(define-public crate-psalm-0.0.4 (crate (name "psalm") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0v7vkc3x5fbjjdiflicv0agpfslyynpx3p9jbi661l2r1vv4dy3g")))

(define-public crate-psalm-0.0.5 (crate (name "psalm") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0209c9616b0si78xdq93f9q8pf6nbgvlazl46a5flpnaprzfibnq")))

(define-public crate-psalm-0.1 (crate (name "psalm") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0q9xfnk2brdyqgx992shrzfh0wia0xqlr8zc387ddlbgfa7jhpja")))

(define-public crate-psalm-0.1 (crate (name "psalm") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1mzv2sgsxlyydasfcg5lrr6jv750jkjv0kbhdv6w39msjx0zisr1")))

(define-public crate-psalms-0.0.1 (crate (name "psalms") (vers "0.0.1") (hash "1cbgwyyghkiv7wgizfpzdfk1p0cj8z58rpgn44rhiw624gk451v7")))

(define-public crate-psalms-0.0.2 (crate (name "psalms") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^3.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml-loader") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1x4g01n7g1gha0ayhfzvvh2s46gqy9jc32fanx723q2paj6bqi21")))

(define-public crate-psalms-0.0.3 (crate (name "psalms") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^3.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "014raf5317r21gfvp46iqp055jzc539laifbj8wh9xjh3adrdyb5")))

(define-public crate-psalms-0.0.4 (crate (name "psalms") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1xlaca8r9cnv1k4qy5hjiaa563m295b1ba9wy4lwmdaqrkidfl27")))

(define-public crate-psalms-0.0.5 (crate (name "psalms") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "01y6bywcyafz1kj1q74fjmk6lqblcprk3b0mbin4vxx8h592h1h0")))

(define-public crate-psalms-0.0.6 (crate (name "psalms") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1vmca6567prq29a5fqb4nnr5a4srqylnxzh2d4nsfwhhn6qajwh2")))

(define-public crate-psalms-0.0.7 (crate (name "psalms") (vers "0.0.7") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0") (default-features #t) (kind 0)))) (hash "0wjrmsw22ah1avm2yrc9sywn3nrpdksns6sbj08y1f1fnwrg51rd")))

