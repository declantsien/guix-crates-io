(define-module (crates-io ps #{5_}#) #:use-module (crates-io))

(define-public crate-ps5_camera_firmware_loader-0.0.1 (crate (name "ps5_camera_firmware_loader") (vers "0.0.1") (deps (list (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0gpbmbm5kr5msrj08qw5gwznic3m2qbpc2h12mmvqv3jqdqah69f")))

