(define-module (crates-io ps p2) #:use-module (crates-io))

(define-public crate-psp2-sys-0.1 (crate (name "psp2-sys") (vers "0.1.0") (hash "135mcw2mxlkjbl5sz2f27dh4jfsm0584aqj832n5kar9aq6knsmw") (features (quote (("unsafe") ("default"))))))

(define-public crate-psp2-sys-0.1 (crate (name "psp2-sys") (vers "0.1.1") (hash "0977mglz4ja6idfcg0bbs7m9xj6sw1vk9s037cbslhh5bbglj75h") (features (quote (("unsafe") ("default"))))))

(define-public crate-psp2-sys-0.2 (crate (name "psp2-sys") (vers "0.2.0") (hash "0h7rm43g4g4zgpiyvsbn08vxgigph1jmp2wmqz8hvny5q833x36h") (features (quote (("unsafe") ("default"))))))

(define-public crate-psp2-sys-0.2 (crate (name "psp2-sys") (vers "0.2.1") (hash "05mcipznq03wak6v0hx5779l2x9137faz81nhkw0zqrn6sfm3fir") (features (quote (("unsafe") ("dox") ("default"))))))

(define-public crate-psp2-sys-0.2 (crate (name "psp2-sys") (vers "0.2.2") (hash "1zqjryp1ippybabl927p67n9lcq9yr7k19sgsk34z9hj72m29lmg") (features (quote (("unsafe") ("dox") ("default"))))))

(define-public crate-psp22-0.2 (crate (name "psp22") (vers "0.2.0") (deps (list (crate-dep (name "ink") (req "^4.3") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2.9") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1q3zqd4g9pabrbi0c92rwnxi6vq7j99d4r56yi4wpghbywzyl6kh") (features (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

(define-public crate-psp22-0.2 (crate (name "psp22") (vers "0.2.1") (deps (list (crate-dep (name "ink") (req "^4.3") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2.9") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "094yyy4vxydbxydlvfjnjgmkp9rpjyni5ppbggq5jmy25cb1c2i8") (features (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

(define-public crate-psp22-0.2 (crate (name "psp22") (vers "0.2.2") (deps (list (crate-dep (name "ink") (req "^4.3") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2.9") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1531yrbb3m6q3c4k4m7ch4pvxkvrlsbqagrc3d8bj5rf3kidm6ss") (features (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

(define-public crate-psp22-0.3 (crate (name "psp22") (vers "0.3.0") (deps (list (crate-dep (name "ink") (req "^5.0.0") (kind 0)))) (hash "15dkq543wzphvdvcklcflxp02x9508rkcmlgya8ra2akx2axwg74") (features (quote (("std" "ink/std") ("ink-as-dependency") ("default" "std" "contract") ("contract"))))))

(define-public crate-psp22-1 (crate (name "psp22") (vers "1.0.0") (deps (list (crate-dep (name "ink") (req "^4.3") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2.9") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "05nhlzp3hwbyqh5wyjn1m6b0mpql0w5v8dwiqh5qzgk582b0z88p") (features (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std"))))))

(define-public crate-psp22-2 (crate (name "psp22") (vers "2.0.0") (deps (list (crate-dep (name "ink") (req "^5.0.0") (kind 0)))) (hash "0np2ijl5m9wfm9nazrng6m0f81w387q3v89kn9nid1gcqa0rzqik") (features (quote (("std" "ink/std") ("ink-as-dependency") ("default" "std"))))))

(define-public crate-psp22-full-0.3 (crate (name "psp22-full") (vers "0.3.0") (deps (list (crate-dep (name "ink") (req "^4.3.0") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2.9") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "17iny8jmijrips7d3l3mrli6pfilb31m3qnck87hns4hpgx1il59") (features (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std") ("contract"))))))

