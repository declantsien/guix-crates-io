(define-module (crates-io ps -d) #:use-module (crates-io))

(define-public crate-ps-datachunk-0.1 (crate (name "ps-datachunk") (vers "0.1.0-1") (deps (list (crate-dep (name "ps-cypher") (req "^0.1.0-2") (default-features #t) (kind 0)) (crate-dep (name "ps-hash") (req "^0.1.0-3") (default-features #t) (kind 0)) (crate-dep (name "ps-mbuf") (req "^0.1.0-2") (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7.44") (features (quote ("validation"))) (default-features #t) (kind 0)))) (hash "04nsv41rrhzl9pah69rzyn60qbprv8dbhli3hzn4mj9ki3v5fyh8")))

(define-public crate-ps-deflate-0.1 (crate (name "ps-deflate") (vers "0.1.0-1") (deps (list (crate-dep (name "libdeflater") (req "^1.20.0") (default-features #t) (kind 0)))) (hash "0llq1j29w2yyqp6vmh49idhmqn0al6hrv2kbaqqj9zmi3zvxhrcv")))

(define-public crate-ps-deflate-0.1 (crate (name "ps-deflate") (vers "0.1.0-2") (deps (list (crate-dep (name "libdeflater") (req "^1.20.0") (default-features #t) (kind 0)))) (hash "1yysknv5sviw3jg60zdfisz3h4xasingw64925yggn7w41dwvjb2")))

(define-public crate-ps-deflate-0.1 (crate (name "ps-deflate") (vers "0.1.0-3") (deps (list (crate-dep (name "libdeflater") (req "^1.20.0") (default-features #t) (kind 0)))) (hash "0nc3wrpj0r24kakaj71r69lxgrzyww2qyx4ybf450a4iyghj90hi")))

(define-public crate-ps-driver-deserializer-0.1 (crate (name "ps-driver-deserializer") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1r4k7l6dicnwj962cnvml8vhnp15rsz2lpv9qdp77sr5jz903phr")))

(define-public crate-ps-driver-deserializer-0.1 (crate (name "ps-driver-deserializer") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ddfk0bvw7p89ms57xd82zjbl7psp19h4dyfvd0wl849lwzqayld")))

(define-public crate-ps-driver-deserializer-0.1 (crate (name "ps-driver-deserializer") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sjqfaic0yjifcmivzkhk8lsl1d93q6p787pwmwbl9i3nh47bwgv")))

(define-public crate-ps-driver-deserializer-0.1 (crate (name "ps-driver-deserializer") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1iq9gmr7mchs99dcc4j093zb9q7rppi87wckh2hirqyh0xwj0chg")))

(define-public crate-ps-driver-deserializer-0.1 (crate (name "ps-driver-deserializer") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rg487bzl75hklf76lhdg34wgxynvg3vv0sjmrcx59991bjh2nwm")))

