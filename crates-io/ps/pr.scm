(define-module (crates-io ps pr) #:use-module (crates-io))

(define-public crate-psprompt-0.1 (crate (name "psprompt") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "1rdb2r5klfirl92bspxiag2m60hj8gs09my1vz2lamd3kw03az7h")))

