(define-module (crates-io ps wr) #:use-module (crates-io))

(define-public crate-pswrd-0.0.1 (crate (name "pswrd") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0a168agzmbjzb58210cid026kaqp8ian17ffdi6k2d5c91q5zm15")))

(define-public crate-pswrd-0.1 (crate (name "pswrd") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1r0xq1av1g9nz6lbcc6yy3d017npjc9ya35nsjcgs3l6xb9krwhc")))

(define-public crate-pswrd-0.1 (crate (name "pswrd") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "03c54cln26ywzmqsknhv63mfzvpc2766qd3c1va07y3kz6pfdzfi")))

(define-public crate-pswrd-1 (crate (name "pswrd") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.29.2") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1i6yarb5xpfkc19adbg37s36icjiqdny9hjw4g0cvq0cdm3kc8i3")))

