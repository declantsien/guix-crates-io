(define-module (crates-io ps #{2-}#) #:use-module (crates-io))

(define-public crate-ps2-mouse-0.1 (crate (name "ps2-mouse") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.9.6") (default-features #t) (kind 0)))) (hash "1c69yh258yg5slridkql86d1g2yd4sgwpaql5jpcyi18syq2f8ry")))

(define-public crate-ps2-mouse-0.1 (crate (name "ps2-mouse") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.9.6") (default-features #t) (kind 0)))) (hash "03mac6lqp456435nmmzk6mlhk15b7j0ddyv5iix5gjkfizgnvy7d")))

(define-public crate-ps2-mouse-0.1 (crate (name "ps2-mouse") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1akn2izfw7z2b8pnngrnn8bwdq7b63hjbhafimzimlg3gmvsl39l")))

(define-public crate-ps2-mouse-0.1 (crate (name "ps2-mouse") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.12.2") (default-features #t) (kind 0)))) (hash "01p54fm307qzxiyr0xkwn53pssfbsx4dyy1ca4jigbb1z5s6y8j5")))

(define-public crate-ps2-mouse-0.1 (crate (name "ps2-mouse") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "18043vg2nni9kh8adkck7bw8lsr8s2linpbnbhai3m3pz9ifm8gp")))

