(define-module (crates-io ps ql) #:use-module (crates-io))

(define-public crate-psql-0.0.0 (crate (name "psql") (vers "0.0.0") (hash "1nqvw66adphqf9ckpq15v7fs1vc81rl1sznmwk3hxj1dfs8isgb0")))

(define-public crate-psql_connect-0.1 (crate (name "psql_connect") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.31") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "~0.11.0") (default-features #t) (kind 0)))) (hash "10y5kjsxbkp3fkvkpkxabdc7aw7pbybd2a3kk6ab7102jwsk8mhx")))

(define-public crate-psqlgen-0.1 (crate (name "psqlgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "1hgajyn0f9dy0y9qv118yv71hmnmdxci0vzjpnnsjk0rnwd7vhh2")))

(define-public crate-psqlgen-0.1 (crate (name "psqlgen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "1fqp9w2nrw0zy351k2gak45jdwip2lgh6mfqp0ch1b11xi4ssg00")))

(define-public crate-psqlgen-0.1 (crate (name "psqlgen") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "0s13xx6q3kzr3p12643vrfw60m1q3grhsn50i7dgz1g2df4n5g60")))

