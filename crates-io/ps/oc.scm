(define-module (crates-io ps oc) #:use-module (crates-io))

(define-public crate-psocket-0.1 (crate (name "psocket") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05qdg2qjcxym24xgpps28yj959nig8qqflchd2kj82n0lp9vg34n")))

(define-public crate-psocket-0.1 (crate (name "psocket") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s5iq36pm7bv2fc8b75wm984hlhjgy4jdcfjp7nj8j85hrrgf73c")))

(define-public crate-psocket-0.1 (crate (name "psocket") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kv565r46nm8vkk4dcha7q9zwzrz3w8lq4g10xrpqbfjj2c78kpf")))

(define-public crate-psocket-0.1 (crate (name "psocket") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11r5xqcs8dg04p14pq1cyp9byx2mxji9gxc5qrbia84jmh5d710h")))

(define-public crate-psocket-0.1 (crate (name "psocket") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ay37v0c9cx2ib0z8pf2ffmdw28nwkijyyhjvrrj5sybhlhb2623")))

