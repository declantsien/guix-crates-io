(define-module (crates-io ps f-) #:use-module (crates-io))

(define-public crate-psf-rs-1 (crate (name "psf-rs") (vers "1.0.0") (hash "1qc87b164xlmxsp4v3zdhpgrn0cva561jcjg3xygcjdh4m60ji2m")))

(define-public crate-psf-rs-1 (crate (name "psf-rs") (vers "1.0.1") (hash "0rmk8dyf59fcxvsqq0106cwfsh61ng5jrmymxgv55frdv35714q6")))

(define-public crate-psf-rs-1 (crate (name "psf-rs") (vers "1.0.2") (hash "19psf4ssma2qklgikqd4633fznvgcdh7n7jv8n2h7m9q6pkimq22")))

(define-public crate-psf-rs-1 (crate (name "psf-rs") (vers "1.1.0") (hash "1zr1n4klpal4wrpvbixq99cmwvpq4qi3gdwmki1rdj7dazxwjw5v")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.0.2") (hash "0pfb4v83wpgpy7rkiy6s3412ap55p1vhzyzyap82ii54z2jv7sgc")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.0.3") (hash "0yxlxw3ld1w19q9a3pqiglmqwbpx01x2zxc694nklffnigji0scc")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.1.0") (hash "1raa8a5803prw8cxg0vn47zs9rif19n4i0cybcxfaxba3lsgcv45")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.1.1") (hash "1li2xwr02c0dvw8sh50ifi643lhfb9scxykrli4k76b6w6210gkm")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.1.2") (hash "0gd5v80c6n9g3almy1ya3wm04v1w2zvm16iw71kjf38nlkf7d7xq")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.1.3") (deps (list (crate-dep (name "ahash") (req "^0.8.6") (kind 0)) (crate-dep (name "hash32") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0m5kcjlm3mkd4kjd9mgqgrrlavam68ygc786axv9xn8ba6vxpd82")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.1.4") (deps (list (crate-dep (name "ahash") (req "^0.8.6") (kind 0)) (crate-dep (name "hash32") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1f1l63qx6v8fzhfbw11b47bx9c6hq6d0ppjzjywfsswj46m4v2hd")))

(define-public crate-psf-rs-2 (crate (name "psf-rs") (vers "2.2.0") (deps (list (crate-dep (name "ahash") (req "^0.8.6") (kind 0)) (crate-dep (name "hash32") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "08vi9f3pfgp9cmhanzwrz245gzd5i29p7hhp849bvl87gk212lq6")))

