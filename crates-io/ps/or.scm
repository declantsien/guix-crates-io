(define-module (crates-io ps or) #:use-module (crates-io))

(define-public crate-psort-0.0.1 (crate (name "psort") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "00grwnkm0sljwigf4xghisy7idgcv1w0h2yq1ai8yd32jhmh2wrv")))

(define-public crate-psort-0.1 (crate (name "psort") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0afyl9992ms6g4nb1533r9vmdz4n952r5ghgx5dvq2gqf2vf7ssm")))

(define-public crate-psort-0.1 (crate (name "psort") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0j4fmm0c4lichzybnnz3mb4z74hkwqp2awd4m5kd8k3is4qs5mpn")))

(define-public crate-psort-0.1 (crate (name "psort") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "068bawgm3l225bcabh6wigr1pbv86a69qz76mmiicrrd6560vx19")))

(define-public crate-psort-0.2 (crate (name "psort") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1d54s54zx731fzbl891ci722mwvwghrkpwvrsldwy047bh5lfmqf")))

(define-public crate-psort-0.2 (crate (name "psort") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0vh8x9bc2b2gz82apg7q2r85ncc5rj80wiwr38iaqc14cy0jydrs")))

