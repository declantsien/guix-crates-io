(define-module (crates-io ps fp) #:use-module (crates-io))

(define-public crate-psfparser-0.1 (crate (name "psfparser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "float_eq") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0bwgb877yhhk3hdzrkry44amswr81dn2d00qw99qfmypmrm1y7l2")))

(define-public crate-psfparser-0.1 (crate (name "psfparser") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "float_eq") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0qz6ria3c16sd307jbgixyjg1q8xjsqx8sah0gnilsydv7fc7qyw")))

(define-public crate-psfparser-0.1 (crate (name "psfparser") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "float_eq") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "010y3lak22hkmzcmgwjlm78mr0qfyjryhwcljd7bayb5fkh4kvdd")))

