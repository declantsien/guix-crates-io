(define-module (crates-io ps -c) #:use-module (crates-io))

(define-public crate-ps-cypher-0.1 (crate (name "ps-cypher") (vers "0.1.0-1") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "ps-base64") (req "^0.1.0-1") (default-features #t) (kind 0)) (crate-dep (name "ps-deflate") (req "^0.1.0-3") (default-features #t) (kind 0)) (crate-dep (name "ps-hash") (req "^0.1.0-3") (default-features #t) (kind 0)))) (hash "034sjla2akq8l1n3gh7ssr87h7sv7d22qji44c1ag9waci7ni1dm")))

(define-public crate-ps-cypher-0.1 (crate (name "ps-cypher") (vers "0.1.0-2") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "ps-base64") (req "^0.1.0-1") (default-features #t) (kind 0)) (crate-dep (name "ps-deflate") (req "^0.1.0-3") (default-features #t) (kind 0)) (crate-dep (name "ps-hash") (req "^0.1.0-3") (default-features #t) (kind 0)))) (hash "00zjxl12vl2p16rsh4pj5wysw2gspkvrg7riji8d2k4kibfr483x")))

