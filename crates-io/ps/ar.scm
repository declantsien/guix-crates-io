(define-module (crates-io ps ar) #:use-module (crates-io))

(define-public crate-PSArc-0.0.0 (crate (name "PSArc") (vers "0.0.0") (hash "1cgf5jm4zlmhn7biwagx8xk5fraq7lirfpp1dmpv6kpih48xnh7i")))

(define-public crate-PSArc-cli-0.0.0 (crate (name "PSArc-cli") (vers "0.0.0") (hash "0xn3gvr00nsf8b5j3hr8k4a2i9i0w294jdlsj5y4sgn3pqj061hn")))

(define-public crate-PSArc-lib-0.0.0 (crate (name "PSArc-lib") (vers "0.0.0") (hash "0c7gj9xv3sk9qrd12h4ybszdf13wc6x67f55swkla9bp5mn13r6y")))

(define-public crate-PSArc-lib-0.0.1 (crate (name "PSArc-lib") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (features (quote ("rust_backend"))) (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "17ayg1gvf5ni9xfhg5c4iyi78vjgiafwlsij4bsl5i59h5i4ky77") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-PSArc-wasi-0.1 (crate (name "PSArc-wasi") (vers "0.1.0") (hash "1p0db1vvcqb7sa58yb9v6agm1ihylbd0fxakw359fnlwfiq87w2m")))

