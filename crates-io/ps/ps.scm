(define-module (crates-io ps ps) #:use-module (crates-io))

(define-public crate-pspsdk-sys-0.0.1 (crate (name "pspsdk-sys") (vers "0.0.1") (hash "11d2l6hk7b5jin8z4kbg380jsjgd2mbmxwv72y3mhwf23yxkxr0p") (yanked #t)))

(define-public crate-pspsdk-sys-0.0.2 (crate (name "pspsdk-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "0kv6ai4pqnn14k68cr50bwyirh6d13ynj8n774qf9x94gvh7p91k") (yanked #t)))

(define-public crate-pspsdk-sys-0.0.3 (crate (name "pspsdk-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.69") (kind 0)))) (hash "16zln77dlxblvlg1iarx4drplp4ns5lf8xmxzv0nsh7c1xxvah3x") (yanked #t)))

