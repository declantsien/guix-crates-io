(define-module (crates-io ps bu) #:use-module (crates-io))

(define-public crate-psbus-0.1 (crate (name "psbus") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "=0.8.1") (default-features #t) (kind 0)))) (hash "087rdkilxvzwbgn1lcj4lkdy2z6sfdqq73cs6galbx7xhkzy258q")))

