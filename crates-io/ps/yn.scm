(define-module (crates-io ps yn) #:use-module (crates-io))

(define-public crate-psync-0.1 (crate (name "psync") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "os_type") (req "^2.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.1") (default-features #t) (kind 0)))) (hash "02m180ypl4j5scw6gwpp8v8agg30l80aaphkg38f7aymrzdmslnz")))

