(define-module (crates-io ps to) #:use-module (crates-io))

(define-public crate-pstoedit-0.1 (crate (name "pstoedit") (vers "0.1.0") (deps (list (crate-dep (name "pstoedit-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0jqp9b3c0c3268fcbgcyipamd3vjkddhkbxlh22q2cgywdzkxqx9")))

(define-public crate-pstoedit-0.1 (crate (name "pstoedit") (vers "0.1.1") (deps (list (crate-dep (name "pstoedit-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1yakr3xbs8v2323frm9cvq8kjz690zwxlklkz72g8zbjzjn7002m") (features (quote (("pstoedit_4_00" "pstoedit-sys/pstoedit_4_00"))))))

(define-public crate-pstoedit-sys-0.1 (crate (name "pstoedit-sys") (vers "0.1.0") (hash "1cn7mjsr4frqn6arx9j4zd8ld9nysn9vq80jc04dc3aynqfwhf9z") (links "pstoedit")))

(define-public crate-pstoedit-sys-0.1 (crate (name "pstoedit-sys") (vers "0.1.1") (hash "1vm900676gh3hvv8k7sh257545630f1i7nlzlqlmg83nnnvrlx1n") (features (quote (("pstoedit_4_01" "pstoedit_4_00") ("pstoedit_4_00")))) (links "pstoedit")))

