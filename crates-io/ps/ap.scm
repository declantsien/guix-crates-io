(define-module (crates-io ps ap) #:use-module (crates-io))

(define-public crate-psapi-sys-0.0.1 (crate (name "psapi-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0babgpplhgbzlb1di3rxd6mlq4nbcs7isglgickb7w0nz7cabjzc")))

(define-public crate-psapi-sys-0.1 (crate (name "psapi-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "0y14g8qshsfnmb7nk2gs1rpbrs1wrggajmzp4yby4q6k0wd5vkdb")))

(define-public crate-psapi-sys-0.1 (crate (name "psapi-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1c0bnkngrhghgffn3mqssasrcw4420gw5sx8fxq2jpy28bhwfw8z")))

