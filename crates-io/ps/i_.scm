(define-module (crates-io ps i_) #:use-module (crates-io))

(define-public crate-psi_device_tree-2 (crate (name "psi_device_tree") (vers "2.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09rjac0m1x1c1xlmdl6k8wwaaq6j578ys6z4g2cr76iph6cnrj5x") (features (quote (("string-dedup"))))))

(define-public crate-psi_device_tree-2 (crate (name "psi_device_tree") (vers "2.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0izn2kzik1bhs4raf2wsaf0qsqxk0gylhz0xh2phbh1g3qyzjrd3") (features (quote (("string-dedup"))))))

