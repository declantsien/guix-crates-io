(define-module (crates-io i2 c_) #:use-module (crates-io))

(define-public crate-i2c_hung_fix-0.1 (crate (name "i2c_hung_fix") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0w2khl3kgfwakd20j59llk39m6vnh6j2jqj5zvznxzsgf9b0s6w4")))

(define-public crate-i2c_parser-0.1 (crate (name "i2c_parser") (vers "0.1.0") (hash "04bkj23qk3bwjai3w9n0jkb10702racxdb16gyinsq7b4sza8l3d")))

(define-public crate-i2c_parser-0.1 (crate (name "i2c_parser") (vers "0.1.1") (hash "148lnw0fyqzvg2bf2rwcig9iqrhcnr96w13lzc18d7r67k3v2w48")))

(define-public crate-i2c_parser-0.1 (crate (name "i2c_parser") (vers "0.1.2") (hash "1vm80fjnh5awc1f4bnybfyqn5pdls1rkgha6j4ghi38jgfxgrj45")))

(define-public crate-i2c_parser-0.1 (crate (name "i2c_parser") (vers "0.1.3") (hash "0vdb1zfxf3krkzv13np58qg5sc2z6yibr4aqz4128g4lycbdbf35")))

(define-public crate-i2c_parser-0.1 (crate (name "i2c_parser") (vers "0.1.4") (hash "15dizb5rrzz6f3cnjhhvdmylni7b37pagi2yfx1bs66v4zirs0bl")))

(define-public crate-i2c_parser-0.1 (crate (name "i2c_parser") (vers "0.1.5") (hash "1q2kk7hdh53kpd0585d6gir440z94kw48hk1rp213n4xk7gm3jl2")))

(define-public crate-i2c_parser-0.1 (crate (name "i2c_parser") (vers "0.1.6") (hash "0dpi16diy5mlj001rr5dwv2jpcbbfrjimx0yqk44bahdiwwdxsa3")))

