(define-module (crates-io i2 a-) #:use-module (crates-io))

(define-public crate-i2a-rs-0.1 (crate (name "i2a-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.11.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.14.6") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1zffca8rk5mnzxln2yvm7qjimd5qf871b11lvxnxf647gl4z28cg") (features (quote (("gif_codec"))))))

