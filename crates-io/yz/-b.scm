(define-module (crates-io yz -b) #:use-module (crates-io))

(define-public crate-yz-basic-block-0.0.0 (crate (name "yz-basic-block") (vers "0.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ivhrb4flramsplmq236aif1iy12iyp7dv8pncv4k69clzl2xi3v") (features (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-yz-basic-block-0.0.1 (crate (name "yz-basic-block") (vers "0.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "05li82il578kjh1srrx6l6frghzqj9pd4hn5m5wh4g633pkvpb2v") (features (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-yz-basic-block-0.0.2 (crate (name "yz-basic-block") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0bhzdv0ghydm3vwfx413rf2bhhbl2zqihj0r0l1ggnj2hly73ql3") (features (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-yz-basic-block-0.1 (crate (name "yz-basic-block") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1f9pa3aiqgxvsp6lqishnq4lx6bs4ls5pad2g7rcp19mpwlmb3ip") (features (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-yz-bll32-0.1 (crate (name "yz-bll32") (vers "0.1.0") (hash "1fl7vx024fljv7ikmb2f31k14dkmz8caihddgin0z65n3462vasy")))

