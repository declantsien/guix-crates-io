(define-module (crates-io yz -d) #:use-module (crates-io))

(define-public crate-yz-diary-date-0.1 (crate (name "yz-diary-date") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("alloc" "std"))) (kind 0)))) (hash "0r34qmk29bbl034q0jkq4psi4lzkqy77qw7vr78z2fvcfhjjpqa9")))

(define-public crate-yz-diary-date-0.1 (crate (name "yz-diary-date") (vers "0.1.1") (deps (list (crate-dep (name "camino") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("alloc" "std"))) (kind 0)))) (hash "1i8q96mx2cb8pn8fpb7p2zbjzamji7mmvn2qbswx9mc78fhycm23")))

