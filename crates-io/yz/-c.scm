(define-module (crates-io yz -c) #:use-module (crates-io))

(define-public crate-yz-curvep-exs-0.1 (crate (name "yz-curvep-exs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (features (quote ("parse"))) (optional #t) (default-features #t) (kind 0)))) (hash "03dm6mkvpj9mk7mwflxj1x7729hqa0q9nj075vyxmi81ikfccags") (features (quote (("cli" "serde" "toml"))))))

