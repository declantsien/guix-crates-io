(define-module (crates-io yz -s) #:use-module (crates-io))

(define-public crate-yz-server-executor-0.2 (crate (name "yz-server-executor") (vers "0.2.0") (deps (list (crate-dep (name "async-executor") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (optional #t) (default-features #t) (kind 0)))) (hash "0bl1ka59afwqbvfjd45h6hlffr1716n3c0269b8njz5qf8n9p6qy")))

(define-public crate-yz-server-executor-0.3 (crate (name "yz-server-executor") (vers "0.3.0") (deps (list (crate-dep (name "async-executor") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "event-listener") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16") (optional #t) (default-features #t) (kind 0)))) (hash "0wxyz1ixl9ksxj4qcs929lfcyjg0ygkdn0f14y2ymdhmvb5s3nmb")))

(define-public crate-yz-string-utils-0.1 (crate (name "yz-string-utils") (vers "0.1.0") (hash "0mwj04y1f55jyhica6ch8y9i3q8irfp0dc615mlipc3s7dy6v4pj")))

(define-public crate-yz-string-utils-0.1 (crate (name "yz-string-utils") (vers "0.1.1") (hash "1y7wcfzfsalxs1swmfswkffjmv4ys0x2ig5481j18cc4g0bxmsnr")))

(define-public crate-yz-string-utils-0.1 (crate (name "yz-string-utils") (vers "0.1.2") (hash "1lnbvpy16i8y0mbcmhp7qh3yp69b3s2h4f3kllabnl4z5c4j0k0z")))

(define-public crate-yz-string-utils-0.1 (crate (name "yz-string-utils") (vers "0.1.3") (hash "1z5cbcmkz8r8l39jfwrzxa0hd5c7bs9v5d8knc0k6mj67pa6v82z")))

(define-public crate-yz-string-utils-0.2 (crate (name "yz-string-utils") (vers "0.2.0") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0sv5gvr0pdxaqrrxazsj8p12zgn54422krb2jgm68kiqkbmfphi0")))

(define-public crate-yz-string-utils-0.3 (crate (name "yz-string-utils") (vers "0.3.0") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ys4j5i8bcv7vr3g76wsqqigm7a1gn0scl8l1lm88xjkphpvmhc7")))

(define-public crate-yz-string-utils-0.3 (crate (name "yz-string-utils") (vers "0.3.1") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qhqcifmhkxp4fsrjyd7pvhm8q75bfnm3gg9613r2wawr2p0gpy5")))

(define-public crate-yz-string-utils-0.4 (crate (name "yz-string-utils") (vers "0.4.0") (deps (list (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-ident") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1b7y5mm7k6m9fjfwlc2p3bypwb3592wc2wk3cjpfflh151l0jdlm") (features (quote (("consume-ident" "unicode-ident" "unicode-normalization"))))))

