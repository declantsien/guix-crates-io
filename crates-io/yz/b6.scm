(define-module (crates-io yz b6) #:use-module (crates-io))

(define-public crate-yzb64-0.1 (crate (name "yzb64") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.20.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10") (default-features #t) (kind 0)))) (hash "1cpmyc78fa1zyix3mn284pf4np4x0cdhqaskwkgscqzxcjibijrg")))

(define-public crate-yzb64-0.1 (crate (name "yzb64") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)))) (hash "0f5yywq0pn117imzw79ijg43hw7sd7k6308fl5qdafshqkfcxs13")))

