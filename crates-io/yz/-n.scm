(define-module (crates-io yz -n) #:use-module (crates-io))

(define-public crate-yz-nomstr-0.1 (crate (name "yz-nomstr") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "1qzq4zi9aydzqzvb0ra29rgqkckygqvi2nk6yd59blphcl4in9mh")))

(define-public crate-yz-nomstr-0.2 (crate (name "yz-nomstr") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "01xqw83ywzydhmdaai3js211r9ismrphlgrpl5smi24mf6salhrq") (yanked #t)))

(define-public crate-yz-nomstr-0.2 (crate (name "yz-nomstr") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "0023p631vz2fw0z211iz2hy788bpd9dc4zrxqb2v7jjxz2gd5zd2") (yanked #t)))

(define-public crate-yz-nomstr-0.3 (crate (name "yz-nomstr") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "053vq17icillqr9fa5jdcb29i5qz73zmmpmhl0xzfbi0xgc1pqky")))

