(define-module (crates-io kn db) #:use-module (crates-io))

(define-public crate-kndb-0.0.0 (crate (name "kndb") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^8.0") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "1yk3lcr26r7n9afick6dipkvx5fnr40gbxlx5pvq7qn8rak88j5j")))

