(define-module (crates-io kn it) #:use-module (crates-io))

(define-public crate-knit-0.1 (crate (name "knit") (vers "0.1.0") (hash "00r8bb7fkl4n7il29f2q1c4v8dar941w00fr9hgwz7abkpjqmjy6") (yanked #t)))

(define-public crate-knit-0.1 (crate (name "knit") (vers "0.1.1") (hash "0h8lqyipr3vad6ibixcbi37h4lib0jqm74kv7255k1aq568zbxpb")))

(define-public crate-knit-text-0.1 (crate (name "knit-text") (vers "0.1.0") (hash "014bjswj2yh0hpiai8s3vl6jhiciira6sij1j5243w4lyn7w54i4") (yanked #t)))

(define-public crate-knit-text-0.1 (crate (name "knit-text") (vers "0.1.1") (hash "1751gg50wa096dhb4i32kf4wac7846hy3ny5n4zhbcarhyqzjm98")))

(define-public crate-knitro_rs-0.1 (crate (name "knitro_rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.61") (default-features #t) (kind 1)))) (hash "1rp2lc03w7x7w6wnpngcl4qnld2i8drylsgv88lblzsvjg61250d") (features (quote (("knitro_13") ("knitro_12"))))))

(define-public crate-knitro_rs-0.2 (crate (name "knitro_rs") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)))) (hash "1mcnfamvvfwcqhrvzry47imdkhjv2m46kb5mzmdvyllqnh7mhflv") (features (quote (("knitro_13") ("knitro_12"))))))

(define-public crate-knitting_chart-0.1 (crate (name "knitting_chart") (vers "0.1.0") (deps (list (crate-dep (name "knitting_parse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.7") (default-features #t) (kind 0)))) (hash "0xk2wv84mr3qgcw5z6x07w70x1dcx79jsxii60799kxpps0h3wig")))

(define-public crate-knitting_chart-0.2 (crate (name "knitting_chart") (vers "0.2.0") (deps (list (crate-dep (name "knitting_parse") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.8") (default-features #t) (kind 0)))) (hash "08wad1mvb7fq15xzl4mm13jm6pf8nykh0n0hhm79f12v4srn44r6")))

(define-public crate-knitting_chart-0.2 (crate (name "knitting_chart") (vers "0.2.1") (deps (list (crate-dep (name "knitting_parse") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 0)))) (hash "0sk4zvd55l5agpcx2irjx5cvycvd9qmi2lxg3fwx241nfgysdhyr")))

(define-public crate-knitting_chart-0.2 (crate (name "knitting_chart") (vers "0.2.2") (deps (list (crate-dep (name "knitting_parse") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 0)))) (hash "1crqw50n4rbdkcqr0v1zdmhmqs22r30sh7gzzh2hvsdvildp9n4j")))

(define-public crate-knitting_parse-0.1 (crate (name "knitting_parse") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "046yddiw9b4q8iv08znhnrbb2pc3lp6hz9h0x634gqdd3q5mfr9j")))

(define-public crate-knitting_parse-0.1 (crate (name "knitting_parse") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "05d9sak9knr3zbchwbgf62b8x7mv9l6dvjma42kgjjba5dqnn8d3")))

(define-public crate-knitting_parse-0.1 (crate (name "knitting_parse") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "006ibz1pgxx404y6d419vw3jm7x6k03yp64c40dhw6b2mj9w9sih")))

(define-public crate-knitting_parse-0.2 (crate (name "knitting_parse") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)))) (hash "05200yk6pgchmaxm84ynca64nqyihnqbz6yli4c4g6d2fr3xlzl0")))

(define-public crate-knitting_parse-0.2 (crate (name "knitting_parse") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)))) (hash "13avypln6zbqddxg2z3g7x1v7sdyr1fxyzkqnw0nn2xaj68qcvxs")))

(define-public crate-knitting_parse-0.3 (crate (name "knitting_parse") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "1vaiq148vj8ypfllkqbsgk373zz08rdw915yvk4sz5d25c3dmhfk")))

(define-public crate-knitting_parse-0.3 (crate (name "knitting_parse") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0biaj9l4r1vhhwfv4g8ba0i5g3bgls54vkpw1i2pbbsmzdk0k8b6")))

(define-public crate-knitting_parse-0.3 (crate (name "knitting_parse") (vers "0.3.2") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "1d2b9nv0dyig1ay2mrayrq53g3ahwcvvixbfzs9bzi3jgr2hip61")))

