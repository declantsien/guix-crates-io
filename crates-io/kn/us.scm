(define-module (crates-io kn us) #:use-module (crates-io))

(define-public crate-knusbaum_consul-0.2 (crate (name "knusbaum_consul") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1i73rm1avd6mkyp9gxrdbbgsk9ng710c3j49lsvl34ggcz769phf")))

