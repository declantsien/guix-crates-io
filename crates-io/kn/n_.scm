(define-module (crates-io kn n_) #:use-module (crates-io))

(define-public crate-knn_classifier-0.1 (crate (name "knn_classifier") (vers "0.1.0") (hash "0wbzvz5cszcmjjav0dankvvivbqqxsdbkb1hkxwk4h3vi4apbqg1")))

(define-public crate-knn_classifier-0.1 (crate (name "knn_classifier") (vers "0.1.1") (hash "1dylfj2bfqr1bfvwx19920frq2cm2qk10bpzmc8vlf2n5mxxfkh1")))

(define-public crate-knn_classifier-0.1 (crate (name "knn_classifier") (vers "0.1.2") (hash "1wwa62mc42sy7i2x664ylz8c26w5z83gfl0g51sf7cxkjikaj43r")))

