(define-module (crates-io kn ap) #:use-module (crates-io))

(define-public crate-knapsack-0.1 (crate (name "knapsack") (vers "0.1.0") (hash "1k99xlaj8rgh4yf0sd9ck0srp0sras24d7k84bqklw7qq811qq0b")))

(define-public crate-knapsack-oxigen-1 (crate (name "knapsack-oxigen") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "oxigen") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "08ci2hs44jrspdhb46li6z05v944r4nvds44p24x27w2q2h4lz3d")))

(define-public crate-knaptime-0.0.1 (crate (name "knaptime") (vers "0.0.1") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "10r43ia7645xc4218vgf5x46dyvn8vzqlgfg1lzfn8bwzvq0azyk")))

(define-public crate-knaptime-0.0.2 (crate (name "knaptime") (vers "0.0.2") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03hrhljg4vxg4f83hpx6yiwvwiylg5bs5j9rhbnv838jrpl60wk8")))

(define-public crate-knaptime-0.0.3 (crate (name "knaptime") (vers "0.0.3") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12c8ip3c5sa8a4z9v0xj7zili10wihgkywm2r5nsar2xqqwm9yi9")))

(define-public crate-knaptime-0.0.4 (crate (name "knaptime") (vers "0.0.4") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0c4pbdnwa557hvvs09kbva9mlw68qcqmdhv4cj78vmghslbyldzw")))

