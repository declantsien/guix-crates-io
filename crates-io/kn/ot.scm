(define-module (crates-io kn ot) #:use-module (crates-io))

(define-public crate-knot-0.0.0 (crate (name "knot") (vers "0.0.0") (hash "1a0ffh4kik8hpyr8q4r8byi98vg927nai3d7d9p3272d9r53mban")))

(define-public crate-knotty-0.1 (crate (name "knotty") (vers "0.1.0") (hash "1853gnmcx3f71xjs1wshn1dhi47dhy5kh3s0krzhzvm3cp975i6w")))

