(define-module (crates-io kn ar) #:use-module (crates-io))

(define-public crate-knarkzel-0.1 (crate (name "knarkzel") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "08p7hnhydj4hzgv1589g9gksz6xm5kjjzn0rvcnf5s9d2kxnk154")))

(define-public crate-knarkzel-0.2 (crate (name "knarkzel") (vers "0.2.0") (deps (list (crate-dep (name "getrandom") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.0.2") (features (quote ("tls"))) (kind 0)))) (hash "0a4laqnifjnfgh3msr0kzpq6dxx2n7qkgpsrzidaf5dvg099knds")))

(define-public crate-knarkzel-0.3 (crate (name "knarkzel") (vers "0.3.0") (deps (list (crate-dep (name "getrandom") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.0.2") (features (quote ("tls"))) (kind 0)))) (hash "02qyfw28rmz08xf7hj6gssqvsg3sh0g1072yqhfrq6c0rz4c9hz9")))

(define-public crate-knarkzel-0.4 (crate (name "knarkzel") (vers "0.4.0") (deps (list (crate-dep (name "getrandom") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)))) (hash "0rvphhb08qgyn087bghb3svc0xdbl683wjdq9yi5m2gf3sjr5c5h")))

(define-public crate-knarkzel-0.4 (crate (name "knarkzel") (vers "0.4.1") (deps (list (crate-dep (name "getrandom") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)))) (hash "1iy46ly8pf4354w1lwsrkyjwdj94d77027cqdqizdrqj4m0y5nl8")))

(define-public crate-knarkzel-0.5 (crate (name "knarkzel") (vers "0.5.0") (deps (list (crate-dep (name "getrandom") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)))) (hash "07d33jkp8isdjkids990fsx0k47p6k3831ckl7wq6j63sccs7qxy")))

(define-public crate-knarkzel-0.6 (crate (name "knarkzel") (vers "0.6.0") (deps (list (crate-dep (name "getrandom") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)))) (hash "1ysb52r09ai3y5k0wsipghsla88s4zgj1jxx252id8kzxgdc1s7h")))

