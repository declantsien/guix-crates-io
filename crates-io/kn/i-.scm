(define-module (crates-io kn i-) #:use-module (crates-io))

(define-public crate-kni-rs-0.1 (crate (name "kni-rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0x77l88w01hq5x2i8cqbjf3shfhhdcc3va8d2mr5lna7ylmd3vcg")))

