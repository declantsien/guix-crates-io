(define-module (crates-io cz mq) #:use-module (crates-io))

(define-public crate-czmq-0.1 (crate (name "czmq") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "czmq-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "2.1.*") (default-features #t) (kind 2)) (crate-dep (name "zmq") (req "^0.8") (default-features #t) (kind 0)))) (hash "0iks84x1j78zqx46lmhdklyhvqa4vg012c539l0r6npjkvsca8lr") (features (quote (("draft"))))))

(define-public crate-czmq-sys-0.0.1 (crate (name "czmq-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1rxbqyn2jf2pbiszrlb27bxwyynww1vhy07jzdrr4dy5dd8mnw0l")))

(define-public crate-czmq-sys-0.0.2 (crate (name "czmq-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1d6791lh47rxjvqpm5hgsc6ww3r8q7lvv759wzfbm1c1y8zfhx7j")))

(define-public crate-czmq-sys-0.0.3 (crate (name "czmq-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0p8y79pwf1gw6ajg4wq30kdfclbbx95h077pisi71n9l48k3www6")))

(define-public crate-czmq-sys-0.1 (crate (name "czmq-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "16badc5210146iydhd1lcw2rw24vy89lzxy2vnazh8bxby1jix8k")))

