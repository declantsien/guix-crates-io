(define-module (crates-io cz l-) #:use-module (crates-io))

(define-public crate-czl-hello-0.1 (crate (name "czl-hello") (vers "0.1.0") (hash "0ywapg157nv2vfj3gc7vlsvh06b490vqsbv4h8m00dhh615yknpl")))

(define-public crate-czl-hello-0.1 (crate (name "czl-hello") (vers "0.1.1") (hash "09x3629fc1cx0qwhb68fd7hpkgmr99c9iag1f1wg2lppvqzy3jjc")))

(define-public crate-czl-hello-cargo-0.1 (crate (name "czl-hello-cargo") (vers "0.1.0") (hash "0agfa01bw8ax7byj42p6dhga0vwxy8a18cfj1kprl3qsc94f7zgx")))

