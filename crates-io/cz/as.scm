(define-module (crates-io cz as) #:use-module (crates-io))

(define-public crate-czas-0.0.1 (crate (name "czas") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "122ccn5qd48xrwl8l64qb9gk8pybaczirbf4c7sbi2x9fawa4xbq")))

(define-public crate-czas-0.0.2 (crate (name "czas") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0g7knn47bkcz17pf0bhbj9n103asbgkkkdh15hldb7j9mavh4kdk")))

(define-public crate-czas-0.0.3 (crate (name "czas") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1lga4fkcqv4k12kz9bk3ilmgivdaqhmy1hl4a35vs4zxj7f2d9n5")))

