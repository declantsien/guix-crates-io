(define-module (crates-io ew ot) #:use-module (crates-io))

(define-public crate-ewots-0.1 (crate (name "ewots") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "wyhash") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1smn1iwnnimh0czdffsfhhbx05rja75qb988na50v2lrk9x29yzw") (features (quote (("256") ("16"))))))

