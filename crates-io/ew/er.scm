(define-module (crates-io ew er) #:use-module (crates-io))

(define-public crate-ewer-0.0.1 (crate (name "ewer") (vers "0.0.1") (hash "0ggld0d6pyrvmmvbxzwzbcsqy7d9bwffz9xadmw1csalylgm7gnf")))

(define-public crate-ewer-0.0.2 (crate (name "ewer") (vers "0.0.2") (deps (list (crate-dep (name "tiny_http") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "04xk0h0bai7pvlk56cx6zml4f2lqkkv08a9v0w2fyrv4jmm8pf41")))

