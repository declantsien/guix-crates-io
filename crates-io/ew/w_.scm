(define-module (crates-io ew w_) #:use-module (crates-io))

(define-public crate-eww_shared_util-0.1 (crate (name "eww_shared_util") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "152m1a6s6kl1p57fd3hi91j0rxjmfclv3j0xw3lqpbizma0m9892")))

