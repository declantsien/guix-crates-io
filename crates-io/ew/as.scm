(define-module (crates-io ew as) #:use-module (crates-io))

(define-public crate-ewasm-0.1 (crate (name "ewasm") (vers "0.1.0") (deps (list (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0fkvlmyk81fahf6bmb0hg77gh061dhnp8i79sc4vg11bvpbd7s9z")))

(define-public crate-ewasm-0.1 (crate (name "ewasm") (vers "0.1.1") (deps (list (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0mj4c6snwk01jyh25yig8gfpynqwyfssbk371wpxam58v48kfbdj")))

(define-public crate-ewasm-0.1 (crate (name "ewasm") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1liyaf5cb0harmks0z3q57f7dlp4yjxr62q8zyglkplzh0abhfnl")))

(define-public crate-ewasm-0.1 (crate (name "ewasm") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "wabt") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1sqpa4i9s9w8nv35pdybmyqh557402660a1jrf04gi4kracrlw7v") (features (quote (("extra-pages"))))))

(define-public crate-ewasm-0.2 (crate (name "ewasm") (vers "0.2.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wabt") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0dn7djpw5rpq091mk9lsvh7pkn0xldkmyi54kb9h1j8h7k33inbh")))

(define-public crate-ewasm-0.2 (crate (name "ewasm") (vers "0.2.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wabt") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "195nk2myxahn5arl9yqkcpd8lvhkg9crik5b26f2cr47xspww63a") (features (quote (("extra-pages"))))))

(define-public crate-ewasm-0.2 (crate (name "ewasm") (vers "0.2.2") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wabt") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1l1d6cz1pzqlg98h6p17n2vzzf43dmi19nrqil4l0qk53sw26jxl") (features (quote (("extra-pages"))))))

(define-public crate-ewasm-0.2 (crate (name "ewasm") (vers "0.2.3") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wabt") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1znc4xd6jpdqlb444zxkxz2pqsr3dj4s8vimhrs4aa4lp0q4dbsc") (features (quote (("extra-pages"))))))

(define-public crate-ewasm_api-0.4 (crate (name "ewasm_api") (vers "0.4.0") (hash "1y26d3ic0mm7plcvpyrrlcg8fvznln77ib86b4z17pb6qalmq462")))

(define-public crate-ewasm_api-0.5 (crate (name "ewasm_api") (vers "0.5.0") (hash "0nmskmcmmmi8x52j4srqggc7578z76wwl7xngmgi91851518ahys")))

(define-public crate-ewasm_api-0.5 (crate (name "ewasm_api") (vers "0.5.1") (hash "00lnxpks758a16hhhn0xzyv4ik7yh2cdmwd7ibkfsbv4qh6qzykk")))

(define-public crate-ewasm_api-0.5 (crate (name "ewasm_api") (vers "0.5.2") (hash "0z23x7ykav3ncj2d207nbyrh984yxxml514a6xxr85rbbb3x0hp5")))

(define-public crate-ewasm_api-0.6 (crate (name "ewasm_api") (vers "0.6.0") (hash "0lh584yqmdncy63shipmm7ci8cb25jwwjzjf62bsxfk9sgs6zwa8") (features (quote (("std") ("default" "std"))))))

(define-public crate-ewasm_api-0.7 (crate (name "ewasm_api") (vers "0.7.0") (hash "1gb0a8ww5ds5a5djy5y9nwn8cp1c83l4awxpp3vk8gkfr5jv3h5d") (features (quote (("std") ("default" "std"))))))

(define-public crate-ewasm_api-0.8 (crate (name "ewasm_api") (vers "0.8.0") (hash "00kjbvw1cjzbmiqxc4p6ncmh252cv7gi311ydn19h8lfhc5qd66y") (features (quote (("std") ("default" "std") ("debug"))))))

(define-public crate-ewasm_api-0.9 (crate (name "ewasm_api") (vers "0.9.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "qimalloc") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4.4") (optional #t) (default-features #t) (kind 0)))) (hash "06jbark65pgn6xyhj4d50vgpwvsvjpnh9q6ml3df8jsa54bidiri") (features (quote (("std") ("experimental") ("default" "std" "wee_alloc") ("debug"))))))

(define-public crate-ewasm_api-0.10 (crate (name "ewasm_api") (vers "0.10.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "qimalloc") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4.4") (optional #t) (default-features #t) (kind 0)))) (hash "0mswjra84drj645r03isa6c3vzi3lf3biwc9zmgikk2sff36knkk") (features (quote (("std") ("experimental") ("eth2") ("default" "std" "wee_alloc") ("debug"))))))

(define-public crate-ewasm_api-0.11 (crate (name "ewasm_api") (vers "0.11.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "qimalloc") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4.4") (optional #t) (default-features #t) (kind 0)))) (hash "0ykzlhj1na51fhkq4fa00zrz44cph58rpwg3yq58sk7va2hc52xi") (features (quote (("std") ("experimental") ("eth2") ("default" "std" "wee_alloc") ("debug"))))))

