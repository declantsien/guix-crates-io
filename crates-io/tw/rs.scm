(define-module (crates-io tw rs) #:use-module (crates-io))

(define-public crate-twrs-0.1 (crate (name "twrs") (vers "0.1.0") (hash "08ng794rr7bjlazadi4qvspckyd2n820ja02060vb5w2yyz0pr1c")))

(define-public crate-twrs-sms-0.1 (crate (name "twrs-sms") (vers "0.1.0") (deps (list (crate-dep (name "mockito") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("rustls-tls" "socks" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.6") (default-features #t) (kind 0)))) (hash "11g35wkmjhm9jwrl7qc2jk2vb6wxpf2gxcrq0zqcwk6pwdbkbb4m")))

