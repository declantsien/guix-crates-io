(define-module (crates-io tw js) #:use-module (crates-io))

(define-public crate-twjsrs-0.1 (crate (name "twjsrs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "08fwc9qfd9mgv0hnpabmvim8ja7n5h62c0jyv7kxhrncjw7sc00g") (features (quote (("wrap" "parse") ("default" "parse" "wrap")))) (v 2) (features2 (quote (("parse" "dep:chrono" "dep:thiserror"))))))

