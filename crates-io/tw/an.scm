(define-module (crates-io tw an) #:use-module (crates-io))

(define-public crate-twang-0.1 (crate (name "twang") (vers "0.1.0") (deps (list (crate-dep (name "adi") (req "^0.12") (features (quote ("speaker"))) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ksichpclcm3jc42gc3gs8fl1vwrp7fax2xplgjx55nhsxwizrvr")))

(define-public crate-twang-0.2 (crate (name "twang") (vers "0.2.0") (deps (list (crate-dep (name "adi") (req "^0.12") (features (quote ("speaker"))) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gzcxmh1rc1ylyjl81012rcfp814z14iysqidax1y8akswvy9c9s")))

(define-public crate-twang-0.3 (crate (name "twang") (vers "0.3.0") (deps (list (crate-dep (name "fon") (req "^0.2") (default-features #t) (kind 0)))) (hash "01mrmj79g1izzxdf7nwzxq8d8511r11fksl9mslb1xwhvj418sqd")))

(define-public crate-twang-0.4 (crate (name "twang") (vers "0.4.0") (deps (list (crate-dep (name "fon") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q3z0nrk0r1jjnf3mbdxbcawa2fy5h3xcwph9r3xz8r3h7a8ar48")))

(define-public crate-twang-0.5 (crate (name "twang") (vers "0.5.0") (deps (list (crate-dep (name "fon") (req "^0.3") (default-features #t) (kind 0)))) (hash "05zl5ara7hs06fllcp36cq44mcl94wzyk8z1wqjbc76a9vqh5x5k")))

(define-public crate-twang-0.6 (crate (name "twang") (vers "0.6.0") (deps (list (crate-dep (name "fon") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pvyhav42cqj2invmp5qk5z53jw5bz1r8z8vc8h8r11249rmf8yy")))

(define-public crate-twang-0.7 (crate (name "twang") (vers "0.7.0") (deps (list (crate-dep (name "fon") (req "^0.5") (default-features #t) (kind 0)))) (hash "1fgic4bvv904ahp8ib5r8y0l3k5dy13m8v2x1x38vhbzfr43js2z")))

(define-public crate-twang-0.8 (crate (name "twang") (vers "0.8.0") (deps (list (crate-dep (name "fon") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fl0g22xfv31nnw2m0h7psvbvn54wfgc4axhssrg43bh382qjaj8") (rust-version "1.56.1")))

(define-public crate-twang-0.9 (crate (name "twang") (vers "0.9.0") (deps (list (crate-dep (name "fon") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "splotch") (req "^0.0.1") (default-features #t) (kind 2)))) (hash "03bzhc4clbmsgwzfhlrwrpd01biihc5b9mjbz0lvh5s43b08rr6i") (rust-version "1.60.0")))

