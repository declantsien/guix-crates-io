(define-module (crates-io tw #{-i}#) #:use-module (crates-io))

(define-public crate-tw-id-num-0.1 (crate (name "tw-id-num") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)))) (hash "0nynkgwqa4fc5k5yxz0mpqn21hdf8cjpck59038gfpr4mq36qg2h") (features (quote (("generate" "rand"))))))

(define-public crate-tw-id-num-0.1 (crate (name "tw-id-num") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)))) (hash "0kyqnhiaw68c61m4a47fwj0i8s0gygr5q9ni2l3xqfllxrxxiw60") (features (quote (("generate" "rand"))))))

(define-public crate-tw-id-num-0.1 (crate (name "tw-id-num") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)))) (hash "0nd7lb77d0xj7y7jj7zi3mb6vkiwfkpsgps7bw8xdm1x1q4cja6d") (features (quote (("generate" "rand")))) (rust-version "1.56")))

