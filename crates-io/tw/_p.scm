(define-module (crates-io tw _p) #:use-module (crates-io))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.13") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ms6dm19b4aprbdlnia820x9kwscpcx370jvai02nknsf3qs94vb")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.14") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0bdxvkdyiwpbp4rjfhbmdc7jds6yqr5j6hdflhbpqghsjx6ckz5j")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.15") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1as8l05kb0035zp4dg7m9vb6alz4rlvg9yjx3d66k41n4hq56359")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.16") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0cx361vzk52ikmg0z3j4dd5c8wzqigbal2qbjs7qhfdd9mz86bd5")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.17") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "14qfxv9x51wds3k01ph6hqnzwirmx0mklsqrd5l72cqmv8mbhlgp")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.18") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "124j9ij2wg9nm3h9z5snsyfc4zflrswgpbm0n8yckjfghfvrn2aq")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.19") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0jwmhsscqb0rk4yslrgdqy253vldsg63rbqzvlfdqam4wvxny071")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.20") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1a0ndff6qpcc1zr0swq9wr4fl8v1bg8s55myp7wgrp2ayz8zlhz5")))

(define-public crate-tw_pack_lib-0.1 (crate (name "tw_pack_lib") (vers "0.1.21") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "170ddbg35g3dqw8sra3yiycsxlwz71whwa3r43r7868n36ya0wr2")))

