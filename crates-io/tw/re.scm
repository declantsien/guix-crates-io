(define-module (crates-io tw re) #:use-module (crates-io))

(define-public crate-twre-0.0.0 (crate (name "twre") (vers "0.0.0") (hash "0ya4zrhy0i90dzyql7qmn3zh600ib25rg11fy9n6iv7p4bbhw1v8")))

(define-public crate-twre_ecs-0.0.1 (crate (name "twre_ecs") (vers "0.0.1") (deps (list (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "156i6cbgn2qdna50l9srwlvwn4vvk8b5kx07srsgxsldgpj734a5")))

(define-public crate-twre_ecs-0.0.2 (crate (name "twre_ecs") (vers "0.0.2") (deps (list (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0vfz6974kg1f9s9ixi0yc5z355kz3m4ld0sbn6i1iky8jjb6b0mp")))

