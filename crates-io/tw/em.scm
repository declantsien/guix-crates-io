(define-module (crates-io tw em) #:use-module (crates-io))

(define-public crate-twemoji-assets-0.1 (crate (name "twemoji-assets") (vers "0.1.0+14.0.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0xvyvn8228bk7d9sg9jyzyzyiwsi00ijm9hjq6rm3jfr6gyvb0hf") (features (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1 (crate (name "twemoji-assets") (vers "1.0.0+14.0.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1h3b8s9d6viz5vzs069rkvkxky10y3x4zqmlcgfw9wnh1hrbdwpc") (features (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1 (crate (name "twemoji-assets") (vers "1.1.0+14.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "09j15f03xw2jv3y5barf56gsv4rvvfhjw513iyr498pmfcmlh3aw") (features (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1 (crate (name "twemoji-assets") (vers "1.1.1+14.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1i8ndg7ss1k7rrd0gh9qhpn45k2b1g2z69w4qgj7hzx8j7biqvvd") (features (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1 (crate (name "twemoji-assets") (vers "1.2.0+15.0.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1n1knapsfin9m8sqbzhpbxpqlnbjk3d3hgc5zv0csi6a84mvfpjp") (features (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1 (crate (name "twemoji-assets") (vers "1.2.1+15.0.3") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1x9c5cwx42rf1il6734ji32ah8bc3c27dal5slbks3m6kabby4wg") (features (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-assets-1 (crate (name "twemoji-assets") (vers "1.3.0+15.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "070nb3zlfcxa59kb3n2298ranl9yybflg47araz6sxlr6b12bl40") (features (quote (("svg") ("png") ("names") ("default" "svg" "names"))))))

(define-public crate-twemoji-rs-0.1 (crate (name "twemoji-rs") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0fkqssvmwfw392114inh8vxgp1mvnjpmknf5914gyzhnx2jirby3")))

(define-public crate-twemoji-rs-0.1 (crate (name "twemoji-rs") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0isrb2j5wmg7pj7f2g2ysbq2ay0wy14sin4fg699p45gr8g4f0lf")))

(define-public crate-twemoji-rs-0.1 (crate (name "twemoji-rs") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1pd4fsw1mqcjmpq2szwajz5ya1k4pndd2r81gb76zncvfl73navk")))

