(define-module (crates-io tw ob) #:use-module (crates-io))

(define-public crate-twobit-0.1 (crate (name "twobit") (vers "0.1.0") (hash "00fw91rsfczcnsh1l7zphjds2bgzln7fbg3s22b6b1maziwsmcwb")))

(define-public crate-twobit-0.1 (crate (name "twobit") (vers "0.1.1") (hash "0fip08vxy14jfm9f15ffvhqcb65hhbw5ls65bblpf7f5mb6iji6x")))

(define-public crate-twobit-0.2 (crate (name "twobit") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (target "cfg(bench)") (kind 2)))) (hash "1p9w4mpnckx74rm3avx4p34kk8nvdk05ndbxnaa4ncr75xj4fmf7")))

(define-public crate-twobit-0.2 (crate (name "twobit") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (target "cfg(bench)") (kind 2)))) (hash "01lh7mkanxrd7bkd3qn552r359j96k06x0p9w6n3qb3sd8xpkmfc")))

(define-public crate-twobpp-0.1 (crate (name "twobpp") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14dh4jn85pxzylhxi9ds8i833llmv4jah4pijsrd248fw7hds3g3")))

(define-public crate-twobpp-0.1 (crate (name "twobpp") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09gj3i5qq1ag8k3pgqg5zjbn97wzwmgi0g9dm9zx9qhd9vd7jdl1")))

