(define-module (crates-io tw st) #:use-module (crates-io))

(define-public crate-twstorage-0.1 (crate (name "twstorage") (vers "0.1.0") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ga43azdgffc4nvyx25z4ijda4asfgaarjifdn4js9vzw9as6nx1")))

(define-public crate-twstorage-0.1 (crate (name "twstorage") (vers "0.1.1") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1fspkyyy5frrmbbk5dykx2bplrc52caz0azrb2mpcd9r6jwxhczi")))

