(define-module (crates-io tw ie) #:use-module (crates-io))

(define-public crate-twie-0.4 (crate (name "twie") (vers "0.4.0") (deps (list (crate-dep (name "boxy") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "buf-trait") (req "^0.4") (default-features #t) (kind 0)))) (hash "03719k2cwfaskjna5dvqqkmkjyvkjsrph19mq0ipjj2qd8v2cxpj")))

(define-public crate-twie-0.4 (crate (name "twie") (vers "0.4.1") (deps (list (crate-dep (name "boxy") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "buf-trait") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "byteyarn") (req "^0.4") (default-features #t) (kind 0)))) (hash "1c9wndg04frxpvv41aqx8nxj94xj2vyfvh4ashsbrq6vrjgpm1d1")))

(define-public crate-twie-0.5 (crate (name "twie") (vers "0.5.0") (deps (list (crate-dep (name "boxy") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "buf-trait") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "byteyarn") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ghlyiibjfav6axlmap5j4s52jmsfkaixxm4mykqhz5448asya2r")))

