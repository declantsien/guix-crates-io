(define-module (crates-io tw a_) #:use-module (crates-io))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "15cjpvgnm624da89ydrpqp10gp49h2s8rak5g7d0nw14cqjrvcqc") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "1v1df1fn1pf8yqp9widliv4vnw2p6hpv0ncjaxgvplcif64kykhh") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "09vi59z1n5skl9v537vvb0s3j2qc4fkax8d3f081m4y80msj7jsl") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "1nql7fqay87zmyy3bgkn0mhsbccwp0zl8a2qzgsvlhypcw5ab8z8") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "0ww8l0hbdgmm9f8w7fclcag699vn3hrhjxalh9qlhh9k8yz4ikc9") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "1fiv55psskyrbvm83wlr53ygry2l3prypwka3vlbq93as7ih1bmr") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "1g8abc2i14wakv3kw2rq70qrl2fwzq9snmkzdvn3g7pbhz1z0fx1") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "1vqzjx7fdqqb6a0y7vk9x2ix3mw9fss11pr8ipljvbmrf3s2n8ly") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.8") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "090fr2q48dhqys3pd7m4d9mz687ji64cg5cwlfsqb4fm1628rpvd") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.9") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)))) (hash "1rrhzybfw4313rpwrdgz2z8rq4l3j94f4h6fqsby82d11lp8swdn") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.10") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)))) (hash "0qawg7bcx4yj7z6f6vmhbg6fzmhsyha8c5fxlq58hi33b8a6sldx") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.11") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)))) (hash "1dvl11v3sm7jgdf66mcamck8508dshl6vklyxcpn1pd3g7j28v64") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.12") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0v9kjy9559ma8jmvb5bk428v092fryrfqw202whc2kmbcpa65f97") (yanked #t)))

(define-public crate-twa_pack_lib-0.1 (crate (name "twa_pack_lib") (vers "0.1.13") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cached_file_view") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1d8ysc05h24qmwml70myfiadrggfn27nnigiij69mj0dkf7f42gb") (yanked #t)))

(define-public crate-twa_table_decrypt_lib-0.1 (crate (name "twa_table_decrypt_lib") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "13qjppkrsa318r5knv25sga0pbkcmrc00ix81zkmbibf3wyqqa4b") (yanked #t)))

