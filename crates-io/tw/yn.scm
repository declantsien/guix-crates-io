(define-module (crates-io tw yn) #:use-module (crates-io))

(define-public crate-twyne-0.0.0 (crate (name "twyne") (vers "0.0.0") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cp6jab6y40rqwcdlzccfdckwxj2kcsdbvcsjlnbkg70n3xdjs5i")))

(define-public crate-twyne-0.1 (crate (name "twyne") (vers "0.1.0") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lrlrg8mmavyf8gx5a9lapywlk1wlkr2kc9klv8p58n5gnick8vs")))

(define-public crate-twyne-0.2 (crate (name "twyne") (vers "0.2.0-rc1") (deps (list (crate-dep (name "html_parser") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h5338cqz0mwlf1agr5xq06qbmzw01rgk4gp78cj6bmf1x68q1bq")))

(define-public crate-twyne-0.2 (crate (name "twyne") (vers "0.2.0") (deps (list (crate-dep (name "html_parser") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "19lmacf9gh34fysrm47j6k0xm52m0d9prln5xm3pc4gx4iszr8ri")))

(define-public crate-twyngled-0.0.0 (crate (name "twyngled") (vers "0.0.0") (deps (list (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "twyg") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0sk78q84r1gqimvgl179qw2yapzvawrsl72g4b1w623z8ybgraxn")))

(define-public crate-twyngled-0.0.1 (crate (name "twyngled") (vers "0.0.1") (deps (list (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "twyg") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0sjjcrjsmm4widvy36fkhnmnb4hnj3xpgc78kcph5r9kaxanbwwv")))

