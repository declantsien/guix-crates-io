(define-module (crates-io tw id) #:use-module (crates-io))

(define-public crate-twiddle-0.1 (crate (name "twiddle") (vers "0.1.0") (hash "1i1pkwwyl17g68akv0d6arjmn59k2y1sc1wfms0g6vyvf0x38gv2")))

(define-public crate-twiddle-0.1 (crate (name "twiddle") (vers "0.1.1") (hash "11cc7lpmwd2d96x58f5f8693w2nhdpwizv6f4p4giwpaxl5q1jzq")))

(define-public crate-twiddle-0.1 (crate (name "twiddle") (vers "0.1.2") (hash "09r2sdni3ccs9zslfkkqx1dn8vy9ngh0p2lxzs98yrrclgdf1f74")))

(define-public crate-twiddle-0.1 (crate (name "twiddle") (vers "0.1.3") (hash "0aqlb2ijnz32j91yiri129airqi44sgvhaga2k7a80hiijaalv67")))

(define-public crate-twiddle-0.1 (crate (name "twiddle") (vers "0.1.4") (hash "1y9v653ff8z636a5bl1ga4gxxpc01g970f1fwd4qzsh7hgycxdrk")))

(define-public crate-twiddle-0.2 (crate (name "twiddle") (vers "0.2.0") (hash "19q4a5pq2kypgjljc7x4jpwlhsa2cz8zcxdrx1mir4phz0s5fqhp")))

(define-public crate-twiddle-0.3 (crate (name "twiddle") (vers "0.3.0") (hash "0qkz17g6crmclhzvsq9cq04jzpnfdp613l1i4qsj8s34vfiyn003")))

(define-public crate-twiddle-1 (crate (name "twiddle") (vers "1.0.0") (hash "1lg19bhb8yzm36d4xqf3h4yg1a3hf028g0b3xcz96ad5kbhfalaf")))

(define-public crate-twiddle-1 (crate (name "twiddle") (vers "1.0.1") (hash "0v1fglajm6rjq3q5fxw0pp08whagykrq7syz02sbn4hlafwag15m")))

(define-public crate-twiddle-1 (crate (name "twiddle") (vers "1.1.0") (hash "0wdp4zmwrmn3vvihh818x06z6xwc7accni356xwbi53braqn71q8")))

