(define-module (crates-io tw im) #:use-module (crates-io))

(define-public crate-twiml-0.2 (crate (name "twiml") (vers "0.2.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "00lslwmdj6vqdksfy2cjin2p420k8c39k6il5fbdbx5bkmxhyza7")))

(define-public crate-twiml-0.2 (crate (name "twiml") (vers "0.2.1") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1grwnsfwj5pnbkkx3d6n7kjjj5dwrr9dblyyd9jbab89nyka32jg")))

(define-public crate-twiml-0.3 (crate (name "twiml") (vers "0.3.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1dgx71wp3an6rivdajzzmp140ily53xha23pzx98fn39vha2r6hi")))

