(define-module (crates-io tw ai) #:use-module (crates-io))

(define-public crate-twain-0.1 (crate (name "twain") (vers "0.1.0") (deps (list (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1f40cnmcx061d0aamdh1mlmkwxhcgxbv4l8d5rlxjnsrkwd12afl")))

