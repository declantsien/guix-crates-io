(define-module (crates-io tw oz) #:use-module (crates-io))

(define-public crate-twozero48-0.1 (crate (name "twozero48") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1") (default-features #t) (kind 0)))) (hash "128x7mzqsmvq5dki7lwsf6awl476va144kldfvjxxvljza0xpk9d")))

(define-public crate-twozero48-0.1 (crate (name "twozero48") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1") (default-features #t) (kind 0)))) (hash "0w6qbdxnn32gzkw2n9yxyj3g4imi9289yz0dkjp2fi37ajdzcqpv") (yanked #t)))

(define-public crate-twozero48-0.1 (crate (name "twozero48") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1") (default-features #t) (kind 0)))) (hash "0j6aji3cckp8wliggc55jii0djmawqi9pk3rz8hz2iiwcaa3dr01")))

(define-public crate-twozero48-0.1 (crate (name "twozero48") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1") (default-features #t) (kind 0)))) (hash "1dylirim12wh2bly1fjx87x9c39bf47kfrfk6ynbcrcjdwm43253")))

