(define-module (crates-io tw _u) #:use-module (crates-io))

(define-public crate-tw_unpack-0.1 (crate (name "tw_unpack") (vers "0.1.10") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tw_pack_lib") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "08qzb1jcprkis5fsvzfgdky0xjbrn0g9m1b3mnrks9d0agrsafy9")))

(define-public crate-tw_unpack-0.1 (crate (name "tw_unpack") (vers "0.1.11") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tw_pack_lib") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0l0bbpclznigcgbdql1w57a77cqky28dxxbdhrmgzxynicz8iml8")))

