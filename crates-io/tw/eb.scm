(define-module (crates-io tw eb) #:use-module (crates-io))

(define-public crate-tweb-0.2 (crate (name "tweb") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (features (quote ("clock"))) (default-features #t) (kind 0)) (crate-dep (name "chunked_transfer") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1rg56byw79376xarcq50sf7gx9kd8idc0p2qdv3chpmbqkgwmp4p")))

