(define-module (crates-io tw ic) #:use-module (crates-io))

(define-public crate-twice-0.2 (crate (name "twice") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15qb8vlfchqyqwd92zplhzma2kx9pnndblw2ysdrcwch6zf4f9i0")))

(define-public crate-twice-0.3 (crate (name "twice") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "05nxf9i9lxrkyggiib302z5na4v2i1pny07xg12xxqp9d8yymrqv")))

