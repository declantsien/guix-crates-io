(define-module (crates-io tw -e) #:use-module (crates-io))

(define-public crate-tw-econ-0.1 (crate (name "tw-econ") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0kqvkrprk0d9lifsi3sfasfkmzf3brl94pdfs9yjvf5bbdgmrpf0")))

(define-public crate-tw-econ-0.1 (crate (name "tw-econ") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1lw62jh41nbcqrxqkqs8fcg4nah3d4500fgmsv2l69gf3jyzsphi")))

(define-public crate-tw-econ-0.1 (crate (name "tw-econ") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0c67m3qq8fkb6x3v1r4z736wr3n4pc9s5acfmiafdbi5xpdy01k6")))

(define-public crate-tw-econ-0.2 (crate (name "tw-econ") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1i6ng73r1ccjsn2dlsc07akn4053gbpbijldfqfj3gy28f0v4aqn")))

(define-public crate-tw-econ-0.3 (crate (name "tw-econ") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1irslqvyk7c8wigzi8l44lag4c9inwfdk90jscv6sy8218a50s9i")))

(define-public crate-tw-econ-0.3 (crate (name "tw-econ") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "18d2hxn2yyf7xmpsap5w5rn5hng11i9ppwk0k72nbx3cq2as6n14")))

(define-public crate-tw-econ-0.4 (crate (name "tw-econ") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.6.1") (features (quote ("rt" "sync" "macros"))) (default-features #t) (kind 0)))) (hash "1wlf2jb0c2yl01h6mrj9871nf2hh1c7bxxl020lgkbd0lkkky6wz")))

(define-public crate-tw-econ-0.4 (crate (name "tw-econ") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.6.1") (features (quote ("io-util" "net" "rt" "sync" "macros"))) (default-features #t) (kind 0)))) (hash "1wf97xkanwgav453l8b69ya4fblwkw4avbwsw9x9gs43ny51dd5h")))

(define-public crate-tw-econ-0.4 (crate (name "tw-econ") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.6.1") (features (quote ("io-util" "net" "rt" "sync" "macros"))) (default-features #t) (kind 0)))) (hash "1m2b5ajs77mjkkyz0k72f84dbirjbf19xj10y3zcbqz5rcnj073m")))

(define-public crate-tw-econ-0.4 (crate (name "tw-econ") (vers "0.4.4") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "sscanf") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.6.1") (features (quote ("io-util" "net" "rt" "sync" "macros"))) (default-features #t) (kind 0)))) (hash "1w96dh16jlz4bh80jabd21z05wrh538h2b4jqkm3g7n97scpx1is")))

(define-public crate-tw-econ-0.5 (crate (name "tw-econ") (vers "0.5.0") (hash "1f50v6wy7ylsnjzawvdg3zwzprykfylbj0192sw4mxb7dx2n5fcq")))

