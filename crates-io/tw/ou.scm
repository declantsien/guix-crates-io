(define-module (crates-io tw ou) #:use-module (crates-io))

(define-public crate-twounordered-0.1 (crate (name "twounordered") (vers "0.1.0") (hash "06swhnqsf687sr4jy90739n2h77b6b52mf7pm4ayd864vxjgz6pv") (yanked #t)))

(define-public crate-twounordered-0.2 (crate (name "twounordered") (vers "0.2.0") (hash "0gscp3q8v6alxbjzbl7lsy7p05c5jks6nl7kp1dwkbzi36lx9lb7") (yanked #t)))

(define-public crate-twounordered-0.3 (crate (name "twounordered") (vers "0.3.0") (hash "1zc53fs70972zjl2hrl49dmrmvhrr67mvfcjmrrjsyxykd0r7hlf") (yanked #t)))

(define-public crate-twounordered-0.4 (crate (name "twounordered") (vers "0.4.0") (hash "08csqi07rdaxavlzd06jm1p9banksa15d3ls92j7cpg865lvbxcd") (yanked #t)))

(define-public crate-twounordered-0.5 (crate (name "twounordered") (vers "0.5.0") (hash "0z0xi9w3cxma4gdgfp2f6hczsxbi1sv0f4819ncs5siqkyz39qr7") (yanked #t)))

(define-public crate-twounordered-0.6 (crate (name "twounordered") (vers "0.6.0") (hash "1ql05pscmqg9m0bnjxmqjm69q1fwqrvj8zv77n4xsrmxmz83lnsd") (yanked #t)))

(define-public crate-twounordered-0.6 (crate (name "twounordered") (vers "0.6.1") (hash "0adxg1zwhw37hfhflr6dsvkdgvn3jgmdqm4mbkcwzvsq5h89ib7d") (yanked #t)))

(define-public crate-twounordered-0.6 (crate (name "twounordered") (vers "0.6.2") (hash "0gajq3af6ya8imvrnchdifwkz8dqm105qmi7q4fk98mv7sahhqn0")))

(define-public crate-twounordered-0.6 (crate (name "twounordered") (vers "0.6.3") (hash "133bi03pd5p7pnqcq1rls64236x0ak576xqvfzqj1jf2v9br0psp")))

(define-public crate-twounordered-0.7 (crate (name "twounordered") (vers "0.7.0") (hash "103m3d3mynmjgd5j3si888mmxd91s3vdmakwm19zhsgi7qw1rsb1")))

(define-public crate-twounordered-0.7 (crate (name "twounordered") (vers "0.7.1") (hash "1fjp0sl0400ilpa96ynkc2319cbc2bc77ay1kjzw9y7ylj6i4h0g")))

