(define-module (crates-io tw ol) #:use-module (crates-io))

(define-public crate-twolame-sys-0.1 (crate (name "twolame-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1yhsdnxq35hz51i5g4m76f8fhmwjlhz5c9a3yvqjv9cpqc969crf") (yanked #t) (links "twolame")))

(define-public crate-twolame-sys-0.1 (crate (name "twolame-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (kind 1)))) (hash "0wbw0vasy04zq01n43xjxqd5lqq8j24lspl80gzhxb2ymd7scij6") (links "twolame")))

(define-public crate-twoliter-0.0.0 (crate (name "twoliter") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "env" "std"))) (default-features #t) (kind 0)))) (hash "1xg7al91x4jzwkmhpczscz2hf4hia6j00v9xnncjcrpbvcsmxka7")))

