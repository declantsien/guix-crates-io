(define-module (crates-io tw od) #:use-module (crates-io))

(define-public crate-twod-0.1 (crate (name "twod") (vers "0.1.0") (hash "07g79b0mm4v0cyycvgvw9356qlw4m2fha1rk7w11ma3ml25dshhw")))

(define-public crate-twodarray-0.1 (crate (name "twodarray") (vers "0.1.0") (hash "10ing9c2w2zwr2zm2iw1brh1z1zbn0i4b503di71adz4szs6a55c")))

(define-public crate-twodarray-0.1 (crate (name "twodarray") (vers "0.1.1") (hash "03w3s6k0lp1qbgd99qm2khxz61p49fpfjr5zcgrglnyypfqnxbpz")))

(define-public crate-twodarray-0.1 (crate (name "twodarray") (vers "0.1.2") (hash "12v26jpqaxdnn707p4zv6v2vqx899mkdhh24603nc8qaws67zy3j")))

(define-public crate-twodarray-0.1 (crate (name "twodarray") (vers "0.1.3") (hash "0ay25bvn1f4fl0b3p30mp3ib43ssdbf5zxbpx0k8sq9m32k3209p")))

(define-public crate-twodee-0.1 (crate (name "twodee") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "lyon") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "02r0xx1vfgi3pn4pkrcs3mfzhj10v8ag5izxs01djrv7r0qlfljg")))

