(define-module (crates-io p_ ru) #:use-module (crates-io))

(define-public crate-p_rust-0.1 (crate (name "p_rust") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1gyjbw4h4w0vmnisbq251l0567xw3qa39sszaba2yr0101ij3h53")))

