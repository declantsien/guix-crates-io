(define-module (crates-io jm bg) #:use-module (crates-io))

(define-public crate-jmbg-0.1 (crate (name "jmbg") (vers "0.1.0") (hash "1v5wk9aiwja8rprn0jwfdql4mdwigqixl3zy4g111ajciybkbdlc")))

(define-public crate-jmbg-0.1 (crate (name "jmbg") (vers "0.1.1") (hash "05fxmpkxi00h4gb4bqaz7ga9ck8d2i2rrnj25jjcz6lvi5amdc0a")))

(define-public crate-jmbg-0.1 (crate (name "jmbg") (vers "0.1.2") (hash "1mw1hbxa95d7yc9x2y7drixjh131a26hrhm2ds4haq26ymlvvc6v")))

(define-public crate-jmbg-0.1 (crate (name "jmbg") (vers "0.1.3") (hash "0nic9lw21qz5ap1cgxwk7imwxs4jxkc3dcw0f04iyqfzrrlsswlc")))

