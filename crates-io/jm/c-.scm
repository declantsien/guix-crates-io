(define-module (crates-io jm c-) #:use-module (crates-io))

(define-public crate-jmc-tools-0.0.1 (crate (name "jmc-tools") (vers "0.0.1") (hash "1w410qy9h74n7xfdm5piyfys4048naz6jj3c715d5jx61km8ppg0") (yanked #t)))

(define-public crate-jmc-tools-0.0.2 (crate (name "jmc-tools") (vers "0.0.2") (hash "0h26aihmd3glz919r72yb2g4rlc3cjcr4n3cbpwrwlfazl83cair") (yanked #t)))

(define-public crate-jmc-tools-1 (crate (name "jmc-tools") (vers "1.0.0") (hash "1b1yv7kshqg37ni0jxncfv7l1ixl63gav7jdx1xm9hdri8kyxqql")))

