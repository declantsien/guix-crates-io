(define-module (crates-io jm sp) #:use-module (crates-io))

(define-public crate-jmspack-rust-0.1 (crate (name "jmspack-rust") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0g3rxs52qf3hly6hjqvsa4s87mybsn6ic2mjjw0g4f8i9yx7yp88") (rust-version "1.56.0")))

