(define-module (crates-io lg it) #:use-module (crates-io))

(define-public crate-lgit-0.1 (crate (name "lgit") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "01ksc61b512c8rrmp8p9vzma1xgddd30mk6mbk66lv05322c6nkf")))

(define-public crate-lgit-0.2 (crate (name "lgit") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dpmcdxdajgry3sp25ihrmv7lmmkb013jqbr41g6fp6b39i8dkdl")))

(define-public crate-lgit-0.2 (crate (name "lgit") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xjf3hyf6xgrf7ynbjk4685s9bwr5q7sajxyv14cphp6483cr886")))

(define-public crate-lgit-0.3 (crate (name "lgit") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bd1hy4rm7iixlyyffd4cmqkk5z3w843sbdgw1pm9890nkjbr1bv")))

(define-public crate-lgit-0.4 (crate (name "lgit") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0yzbjzwbj0i2g36ymjr988wwfiyqjayk53hzkc23js7jrlwihm8y")))

(define-public crate-lgit-0.4 (crate (name "lgit") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0xvbrkl1jf3jmvy4qbn074fgzg8y31s9w0qk0qg0v8nnafl9kf11")))

(define-public crate-lgit-0.5 (crate (name "lgit") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (features (quote ("fuzzy-select"))) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0p9hc2wi7pf3jd6zx694r7zwkfd2cpshvv5zc12m5hp5wzr6wp5x")))

(define-public crate-lgit-0.6 (crate (name "lgit") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (features (quote ("fuzzy-select"))) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1xqn7gd4136gs3ck4a7rn214rj7g803rdc589irrmqya1b0d76xf")))

(define-public crate-lgit-0.7 (crate (name "lgit") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (features (quote ("fuzzy-select"))) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1hyk3afi7cqq06aza0s6iyac0lhqa21bmfcnpxds4csnyfvff9af")))

(define-public crate-lgit-0.7 (crate (name "lgit") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (features (quote ("fuzzy-select"))) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0s7sbsqlcca9l794m40fphx837z34b1bq1r2hpz8m8rvvg3s60h2")))

(define-public crate-lgit-0.7 (crate (name "lgit") (vers "0.7.2") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (features (quote ("fuzzy-select"))) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0603j8d5h7msgr421s7gsad9rz9d4by1xlxm1jpajvp04fc1g6rn")))

(define-public crate-lgit-0.7 (crate (name "lgit") (vers "0.7.3") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (features (quote ("fuzzy-select"))) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1lim9jmkas8cqgr4y5ic7y0cn8df7wmjsn1kizn6vr34s4c1wx78")))

