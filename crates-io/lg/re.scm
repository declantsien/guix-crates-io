(define-module (crates-io lg re) #:use-module (crates-io))

(define-public crate-LGremote-0.1 (crate (name "LGremote") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10gl4i9w3ljkn736w84w50svk242h6kl3ndzzpsrn850wvpfh1kg")))

(define-public crate-LGremote-0.1 (crate (name "LGremote") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17z8149wsqlnlzwv6k8y8v0hf8fgbyv64x2zcw051q2d6c56w0l9")))

(define-public crate-LGremote-0.1 (crate (name "LGremote") (vers "0.1.2") (deps (list (crate-dep (name "phf") (req "^0.10") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0krzr430mvmy4ms11j2ay7nx53yr02bl859hqgq1mh0fy50b2xw2")))

(define-public crate-LGremote-0.2 (crate (name "LGremote") (vers "0.2.0") (deps (list (crate-dep (name "phf") (req "^0.10") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09d9afylixpr6z6gfdfvsqic7wpnj02vg81476fzq9m1blrb8g7n")))

(define-public crate-LGremote-0.2 (crate (name "LGremote") (vers "0.2.1") (deps (list (crate-dep (name "phf") (req "^0.10") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0f1614f69nlifrg0kzjc76n0dg1viw84szv9q3ddh281xi9kdx1p")))

