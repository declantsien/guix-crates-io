(define-module (crates-io lg eo) #:use-module (crates-io))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.0") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0f36rfpb9llwhfhh0q5jr93vcm8y03v4238nslagdhs0l2lkjh2l")))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.1") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "19accd0lm3n7ng93pws1fx4b3zbm35rfyakj92z82hdps89j5jsa")))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.2") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1zn06nf2l18sx0vyzi82bj35q4airqhj5ixhxx584q9x2kb6cx7r")))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.3") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0nylnnqqy22g3700zag1kgha8a9bgwcyibv9jp6c4gdkcayg2kwh")))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.4") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1ccab1k69sjqawrd3klg4k1csh9k4182xbdy71qslq63df101v85")))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.5") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "17zjh3ircyil8a4shcpipc8mial3fnmpcdz7xa78d2g617g6cvf1")))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.6") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0v70ayq80nj0yxd4i3b7vgwhq6azqm4zas05i66yjxw46mqxa3rs")))

(define-public crate-lgeo-1 (crate (name "lgeo") (vers "1.0.7") (deps (list (crate-dep (name "lmaths") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1cc08cvkwl17z0899b3fd96kb71d7fcysrwq6rpsifsy0mndj46w")))

