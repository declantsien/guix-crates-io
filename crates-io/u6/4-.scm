(define-module (crates-io u6 #{4-}#) #:use-module (crates-io))

(define-public crate-u64-id-0.1 (crate (name "u64-id") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pxd2ylk0q4ng9l0xzh507pay9ik9gqbhwssmp1f1nj1wz6iwzh9") (features (quote (("default" "rand" "serde"))))))

