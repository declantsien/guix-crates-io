(define-module (crates-io uu le) #:use-module (crates-io))

(define-public crate-uule-converter-0.1 (crate (name "uule-converter") (vers "0.1.0") (deps (list (crate-dep (name "base64-url") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "03p4kirbc9c6bp8a2qimp9l8kl6f1227ymj2109xm2k83hij9d1x")))

