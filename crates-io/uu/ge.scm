(define-module (crates-io uu ge) #:use-module (crates-io))

(define-public crate-uugear_ffi-0.0.1 (crate (name "uugear_ffi") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.37.2") (default-features #t) (kind 1)))) (hash "0fgzh0pzi7skpfhjsqiwbaxwy1d6f0rdhw9f699p7avrnrjdr5gy")))

(define-public crate-uugear_ffi-0.0.2 (crate (name "uugear_ffi") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.37.2") (default-features #t) (kind 1)))) (hash "1sm4i3vpirj0pa5zahfv4in4iw8d732mlf3xvim970x700vh195i")))

