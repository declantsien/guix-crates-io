(define-module (crates-io uu ti) #:use-module (crates-io))

(define-public crate-uutils_term_grid-0.3 (crate (name "uutils_term_grid") (vers "0.3.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "1ixvj893jrcvv76m7cgr99fc25la6xl2101qva6ni646aqm4b2dk") (rust-version "1.70")))

(define-public crate-uutils_term_grid-0.4 (crate (name "uutils_term_grid") (vers "0.4.0") (deps (list (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("unicode-width"))) (kind 0)))) (hash "0xpnglm2kllsn749sbcz7acia99z6hly1z9plb9wwdkdlc78wbsb") (rust-version "1.70")))

(define-public crate-uutils_term_grid-0.5 (crate (name "uutils_term_grid") (vers "0.5.0") (deps (list (crate-dep (name "ansi-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yk03swjy484i00slziixqs45j69krl5gf0p9mm5935rw66ji3cz") (rust-version "1.70")))

(define-public crate-uutils_term_grid-0.6 (crate (name "uutils_term_grid") (vers "0.6.0") (deps (list (crate-dep (name "ansi-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ys1y4bjdgwhvd3c9b2c8hx2dmnxjsgqgg3sll1mgfmlmnsfz7gq") (rust-version "1.70")))

