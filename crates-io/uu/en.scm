(define-module (crates-io uu en) #:use-module (crates-io))

(define-public crate-uuencode-0.1 (crate (name "uuencode") (vers "0.1.0") (hash "0vrsqwc0sjw7q3a3fvw6j0zsllmqpd46x9j37ijd13ga4vjl4k3z")))

(define-public crate-uuencode-0.1 (crate (name "uuencode") (vers "0.1.2") (hash "06irbk382x4jl7n2srl68srvrd80kn3n3y66qj6cvs0p1w3mc4vr")))

(define-public crate-uuencode-0.1 (crate (name "uuencode") (vers "0.1.3") (hash "12nh7p116l5apa61y4s1hj262xd38mh3rimbf8yqfq5sb8pqw8aj")))

(define-public crate-uuencode-0.1 (crate (name "uuencode") (vers "0.1.4") (hash "1avpwhv6fw14lb9g138ywiiw27kls8cy911awlphi8m949hzrh0v")))

(define-public crate-uuencode-0.1 (crate (name "uuencode") (vers "0.1.5") (hash "1h1ad56df3p4y5wzf3wk4xld2mphkfviab3ig1lxwax82m3431yf")))

