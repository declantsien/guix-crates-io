(define-module (crates-io cm as) #:use-module (crates-io))

(define-public crate-cmasfo-0.0.0 (crate (name "cmasfo") (vers "0.0.0") (hash "0b4046w9hj90l2s5m8a7g99wg1kddkm1j75iyhrdl2v6i16p5hms")))

(define-public crate-cmasfo-app-0.0.0 (crate (name "cmasfo-app") (vers "0.0.0") (hash "16wi3xnc9q03iy16l12q1dchf59y3vnqzrabwnvd1h6ckxz6yvs3")))

(define-public crate-cmasfo-cms-0.0.0 (crate (name "cmasfo-cms") (vers "0.0.0") (hash "0dg1k4089apxliw302jqa8z2v710idb5wyfrmaic4aa25jf4ynxj")))

(define-public crate-cmasfo-dev-0.0.1 (crate (name "cmasfo-dev") (vers "0.0.1") (hash "1l8gszab0lvanfnzl0d8yf55i3hll9h14f3729lirw5hsyvgpjd5") (yanked #t)))

(define-public crate-cmasfo-dev-0.0.2 (crate (name "cmasfo-dev") (vers "0.0.2") (hash "1fcg3vbnhwbrcx9gdr8xw7ydsc5g5m466nvma4i708gyh9mi76wr") (yanked #t)))

(define-public crate-cmasfo-dev-0.0.3 (crate (name "cmasfo-dev") (vers "0.0.3") (hash "16kl79rqws712n3qxcbp3b1ifgbaz031i93w4wdiikg8371fiaaw") (yanked #t)))

(define-public crate-cmasfo-dev-0.0.31 (crate (name "cmasfo-dev") (vers "0.0.31") (hash "1dsdz9spg0vx4cychzajfjaysjgjddnymwr40sjybjwsn9hfqxh6") (yanked #t)))

(define-public crate-cmasfo-dev-0.0.32 (crate (name "cmasfo-dev") (vers "0.0.32") (hash "05c83r8z6l718gxmin6yr0isgr260zfafnhjvmgblchycp8pg4dr") (yanked #t)))

(define-public crate-cmasfo-dev-0.0.33 (crate (name "cmasfo-dev") (vers "0.0.33") (hash "1jvv4ccwnm5xjnmgam5jl9hpg5jydm4wq5dwvjhck0dw8yfvl6ym") (yanked #t)))

(define-public crate-cmasfo-dev-0.0.34 (crate (name "cmasfo-dev") (vers "0.0.34") (hash "0v4ndnlbglh1ky8g1h5p0bgcqynlcnrzj433l1yw2nw3xm9yyv6y") (yanked #t)))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)))) (hash "0hhdcqzp96knm6qh3vck1gzxy6433d1wgj3cknfvzzx9kykn9m33") (yanked #t)))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)))) (hash "06lr8zx4787qi9q6qby7bhzl9nrp7g5pfkxa9jlw6dhpbddaqd8n") (yanked #t)))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.11") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0") (default-features #t) (kind 0)))) (hash "0v0s3gd16k7zq3by3xbbdhvca5vqkg5viaqj7pv1i7ak6s7335yr") (yanked #t)))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.12") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0") (default-features #t) (kind 0)))) (hash "0kdg5ha5qwxgsyhibqn4c8ar3iwmbmz544n6c8sbi96q04c9d60q") (yanked #t)))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.13") (hash "1gbsxlq06n0nh4q4dzg5hyalaahdg3vfdib11lv82ksybwxngd27") (yanked #t)))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.14") (hash "0fa35ph0arb3mwc7b6r20848ym5zzlkbinhwq0aqmfwflwsfczm9")))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.15") (hash "0xk01v625j591jxi5wh3s0p7hfxyavlrm4lzrwi3bxk7a8m3sfz9")))

(define-public crate-cmasfo-dev-0.1 (crate (name "cmasfo-dev") (vers "0.1.16") (hash "15hlrimpzxj4vpbyvd010504nh2w71d707kd7km54f4fnpxa7na5")))

(define-public crate-cmasfo-net-0.0.0 (crate (name "cmasfo-net") (vers "0.0.0") (hash "0whwfcwrxf312sx3wqspgri2p04g91gxrrfl7x30lbz8sp9fcwlx")))

(define-public crate-cmasfo-ssg-0.0.0 (crate (name "cmasfo-ssg") (vers "0.0.0") (hash "1pzqinqnbbc9kczk49a1sq3n5kahj7cv6fa92xj6fxs80iw9vzj0")))

(define-public crate-cmasfo-sys-0.0.0 (crate (name "cmasfo-sys") (vers "0.0.0") (hash "094q268mmqys7p03vxq3hhm6n5j9p5ni2kbsi766qbmciz3bz6zj")))

(define-public crate-cmasfo-web-0.0.0 (crate (name "cmasfo-web") (vers "0.0.0") (hash "1h25fqy9w688rj44rgsi0gldry9gcik909hmz5c6683l51qybzgi")))

