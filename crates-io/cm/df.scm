(define-module (crates-io cm df) #:use-module (crates-io))

(define-public crate-cmdfactory-0.3 (crate (name "cmdfactory") (vers "0.3.2") (deps (list (crate-dep (name "open") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "os_pipe") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^5.6.0") (default-features #t) (kind 0)) (crate-dep (name "shared_child") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1scj7cbmnjg1b0wy0vv8y2c738srg1z8gw511ifq3fkrf6a48q8y") (yanked #t)))

(define-public crate-cmdfactory-0.3 (crate (name "cmdfactory") (vers "0.3.3") (hash "1fa9jkb2zk7invmrc5zsa6fyjixm9x98lmb3z2rb69k731cg0w0p") (yanked #t)))

