(define-module (crates-io cm d-) #:use-module (crates-io))

(define-public crate-cmd-args-0.1 (crate (name "cmd-args") (vers "0.1.0") (hash "0md7bi1qh5gkswbmmzmc4z7nx8qfx4ki14frxkchnhywlaagscwh")))

(define-public crate-cmd-args-0.2 (crate (name "cmd-args") (vers "0.2.0") (hash "15rwdfwh7xayc4rm4x7brmi97paj1faj4cp27bxibnxr42rrgyf2")))

(define-public crate-cmd-derive-0.1 (crate (name "cmd-derive") (vers "0.1.0") (deps (list (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ala52acap4a2rq8nak0lckb36ji1lr9k0zjx33jv8j4lcrig197")))

(define-public crate-cmd-derive-0.1 (crate (name "cmd-derive") (vers "0.1.1") (deps (list (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing" "printing" "derive" "extra-traits"))) (kind 0)))) (hash "0h7ajwq2rl90phawf3hda37q45aqx46rzrxj5i85svw486gl6bpp")))

(define-public crate-cmd-exists-0.1 (crate (name "cmd-exists") (vers "0.1.0") (hash "1dl5f9zgr7algfbjsb3p2mq9425j2pzkw0dwgbcj2mapx39vihx6")))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc13") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0s3xq6k8rnj8f42b2nq3j7mrk8080a36qxx705mcmmk0pk44vkyc") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc14") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "030dscn9zv823vjrjnc3ysx8z7byv1yva84mskkay5iv4nlrzaf0") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc15") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19vwchg8q7d2nxrbw79x20dgz35inwbpb4vrrzgpxaphm5jh8c9c") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc16") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0la7x0l2iab61396ycsvwnxnpzxkv288iy4lwdj2vf1cvpvld79l") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc17") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0imr05fbxyi5kz332wbx7qhh5c0wr2kdwfbg7xc5mdjv08fm6aj2") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc18") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "136hs2sy7298rxkra235pl4d40273amyp7rsp785gk1319k262i6") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc19") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0as3kp2hflw2by09sj5aan6kl250hngp2rsn8zi8klp38v81r6z8") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0-rc20") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y0zywkxqi2gf0b86a1k3k71qhwqn8afs4inc61b4q1r5frca7vx") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pvlnaz59ls0xcqljvkcdn5yf02adzh2k5cl0skhwb4nal2y0n03") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jinzr85g6gskjry573i49vq9bai334ixh5c4q7n67qh08kf5ps3") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p38idcr8nm552kj7paz9gg5zlpk9rxb42x9iwpbiga424wxy09l") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-impl-0.9 (crate (name "cmd-impl") (vers "0.9.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0alsiq8r7qhb2r72jn11z81zq7r3q4wvfdhscyvnjwfnphj60qwm") (features (quote (("debug" "syn/extra-traits"))))))

(define-public crate-cmd-macro-0.1 (crate (name "cmd-macro") (vers "0.1.0") (deps (list (crate-dep (name "cmd-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14a2im90apdms2m6a7s6gc8i5d9qyanhvydg2j35pnh3kwfdpi98")))

(define-public crate-cmd-macro-0.1 (crate (name "cmd-macro") (vers "0.1.1") (deps (list (crate-dep (name "cmd-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pkfhc4dffx87pllynyivp52z6n7q5rhkwbg2k09c2ghz1j9bn6n")))

(define-public crate-cmd-macro-0.1 (crate (name "cmd-macro") (vers "0.1.2") (deps (list (crate-dep (name "cmd-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wq7qx5j2jmhh8yar6kyzir1y6pfqvvjdc3sr762k9cyrlbrgm7s")))

(define-public crate-cmd-minesweeper-0.1 (crate (name "cmd-minesweeper") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1g00vh3jvyh51vqvdv7spf80wchjm46n9p1hpm6bm0cd63b6nq3r")))

(define-public crate-cmd-minesweeper-0.1 (crate (name "cmd-minesweeper") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "09zc4k2mklp1nnyvy8969by43pkv6sa6zwfsr8l6cp18pi2gk8vd")))

(define-public crate-cmd-minesweeper-0.1 (crate (name "cmd-minesweeper") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1xdnfxszlrw72jw303dw6dvmagwkqnn7vr5nhwp5mng3ibysgcw3")))

(define-public crate-cmd-minesweeper-0.1 (crate (name "cmd-minesweeper") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1b933md5x75gr0n53w7yra3l8676izln7xg9xvkvyymw84wg04b6")))

(define-public crate-cmd-pandoc-0.1 (crate (name "cmd-pandoc") (vers "0.1.0") (hash "10785n6ik9fj12lnf6sh3j9qjlh7nmpxl2fhl9dykr1pvs2ccrs5")))

(define-public crate-cmd-pandoc-0.2 (crate (name "cmd-pandoc") (vers "0.2.0") (hash "0lkp7zk4gj2vl0zqqm2i3zg0704i2iq4796d3jd7bqkb65ggdl8n")))

(define-public crate-cmd-proc-macro-0.1 (crate (name "cmd-proc-macro") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.20") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a7lh923z6qgc2hgs3pwph2a6pi462a31lym31dyc96jsxx180d0") (yanked #t)))

(define-public crate-cmd-proc-macro-0.1 (crate (name "cmd-proc-macro") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.20") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11vfkibspws0nvwpqrm0gm890bx8nhr52a38axris3z57rlf5rsz") (yanked #t)))

(define-public crate-cmd-proc-macro-0.1 (crate (name "cmd-proc-macro") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.20") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17nbskny5hsprb00v69vjm816npwmpvf121diy8n1pgd9406fr90")))

(define-public crate-cmd-runner-0.0.2 (crate (name "cmd-runner") (vers "0.0.2") (hash "1bq2lbql0bk4dqbzrbkjm7r4gbcq2ngfka6r7fk9ywqmjil286a8")))

(define-public crate-cmd-toys-0.1 (crate (name "cmd-toys") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "11znr6r6n85wxjzvjs3d007k0jlj5rgdz80q9x9lsmnh99hs6ad1")))

(define-public crate-cmd-toys-0.1 (crate (name "cmd-toys") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1pfwimpsms2qf6sfcpga7mmwziisl6640gcf3bv1z0wjg32fmc33")))

(define-public crate-cmd-toys-0.1 (crate (name "cmd-toys") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1sacr4nbvp4v0bc6jlncqbdv1hqyma8knna01v60miz25833nm72")))

(define-public crate-cmd-toys-0.1 (crate (name "cmd-toys") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "0680scb3x2k45f0qak5l4phcx7v4vzfya0iqsw59virb89fs1mv6")))

(define-public crate-cmd-toys-0.1 (crate (name "cmd-toys") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wh4m86va1m053y24pnkdmq8dndxjxnvzswd61kskjhw0s4cgmds")))

(define-public crate-cmd-utils-0.1 (crate (name "cmd-utils") (vers "0.1.0") (hash "1i6ab5m9g7v9677yw8wys3kqy509rsw8r57ra7am11xq5krv0phd")))

(define-public crate-cmd-utils-0.2 (crate (name "cmd-utils") (vers "0.2.0") (hash "16bx3mdf390m40hmyl0whkwnqbd1jw4dy1vfxxj05m3kck24gjjp")))

(define-public crate-cmd-utils-0.3 (crate (name "cmd-utils") (vers "0.3.0") (hash "0260m39zsbsdix482q1yrn36765l72qi94pss94sfav5ajymyr46")))

(define-public crate-cmd-utils-0.3 (crate (name "cmd-utils") (vers "0.3.1") (hash "1fvxbqq3vdcd4s25s6lqaj539vnisn2i69wccjnhcs552ah6a55n")))

(define-public crate-cmd-wrapped-0.2 (crate (name "cmd-wrapped") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "01ir1lkd0jz6r1mvsrnzcw8di03rqpl1vf68b29gwlfkys4nlvbs")))

(define-public crate-cmd-wrapped-0.3 (crate (name "cmd-wrapped") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dl0yzxwzkdlvi2zs16qkd9k849kx0x8d7yajly7pfh8zn35gsv1")))

