(define-module (crates-io cm _c) #:use-module (crates-io))

(define-public crate-cm_contract_sdk-0.0.1 (crate (name "cm_contract_sdk") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.21.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "1cb1w5vni1pfqvjziqsbbq71r3hcabpa1zhfiw3dhn0gg7q177mw") (yanked #t)))

(define-public crate-cm_contract_sdk-0.0.2 (crate (name "cm_contract_sdk") (vers "0.0.2") (deps (list (crate-dep (name "base64") (req "^0.21.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "1l91ns7fdipmfajnzz9fjx927c1jqc2haj335q6j47i1j3pl1hg4")))

