(define-module (crates-io cm dl) #:use-module (crates-io))

(define-public crate-cmdline-parser-0.1 (crate (name "cmdline-parser") (vers "0.1.0") (hash "07cdwlp5l9mygnvjn6v2vk4bpy69q95l5amyra6i4r9kmbhkw383")))

(define-public crate-cmdline_words_parser-0.0.2 (crate (name "cmdline_words_parser") (vers "0.0.2") (hash "0wrwmi3s9cmxmks4wgmkgdflv2pz0kxyqrni7kaxrg39i1ci0srl") (features (quote (("no_std"))))))

(define-public crate-cmdline_words_parser-0.1 (crate (name "cmdline_words_parser") (vers "0.1.0") (hash "1sh944s6cjjs6cdz0v8rzgipznim5xbv9lp54f26y2jsnzcx90ig") (features (quote (("no_std"))))))

(define-public crate-cmdline_words_parser-0.2 (crate (name "cmdline_words_parser") (vers "0.2.0") (hash "1qxlgbs4kn66m6bv4n7wxl0749zyyvdlnzh0jc80738ma14br7dg") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-cmdline_words_parser-0.2 (crate (name "cmdline_words_parser") (vers "0.2.1") (hash "0adxna3j9yycy3jjr204aa4lmsl0qr4fz89lppc77xns0f7hgn3m") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

