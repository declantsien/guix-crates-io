(define-module (crates-io cm dt) #:use-module (crates-io))

(define-public crate-cmdtaglib-0.1 (crate (name "cmdtaglib") (vers "0.1.0") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1aza67y2xdv86359aizwp5dkavkaa8i430k2l3hy3xbwf7dmwirr")))

(define-public crate-cmdtaglib-0.1 (crate (name "cmdtaglib") (vers "0.1.1") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1lcm5qr6mjma7grfp61qgam0hircc7hq8irrvqp01bi66d31a2si")))

(define-public crate-cmdtaglib-0.1 (crate (name "cmdtaglib") (vers "0.1.2") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x9whgc19sl1977xivqg590zvgxans80jc4fkr2ffvcjv5p3jwrg")))

(define-public crate-cmdtaglib-0.1 (crate (name "cmdtaglib") (vers "0.1.3") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01jcybhwvg27giw216i9a2jf6fyxbnksq5dim60z8dh942k9xc66")))

(define-public crate-cmdtaglib-0.1 (crate (name "cmdtaglib") (vers "0.1.4") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0i1whq2wcaxyb9lhd3yanm322gsb8graa8g6nyhh72ygql0w2dz2")))

(define-public crate-cmdtaglib-0.1 (crate (name "cmdtaglib") (vers "0.1.5") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1f8s6fjjc0nh5b9qxrxinlc1vzidr92v9r0i3mlh0gipc0g0r03l")))

(define-public crate-cmdtaglib-0.2 (crate (name "cmdtaglib") (vers "0.2.0") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12bndy3fvpkn200nz317kcicchz93b13nc68bcf24iyl3shnxbfv")))

(define-public crate-cmdtaglib-0.2 (crate (name "cmdtaglib") (vers "0.2.1") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g5qkxvzpvr454fxc0p780j7gd4zx6ws16rd1rizn3l99rh0wrga")))

(define-public crate-cmdtaglib-0.2 (crate (name "cmdtaglib") (vers "0.2.2") (deps (list (crate-dep (name "taglib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10343wsi1sx7vxcsvak376vlqyddspar4sgcn1da4qf7qvrrk9v5")))

(define-public crate-cmdtree-0.0.1 (crate (name "cmdtree") (vers "0.0.1") (hash "029r09a18p2hw2yrn8rqhlgvj89i3n1lfgnr1yradbgca0g2097d")))

(define-public crate-cmdtree-0.1 (crate (name "cmdtree") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)))) (hash "182r3rjlkpl29xl850wqd7h3rr7x1rhiq5k3p5f1hb66cp4bgykp")))

(define-public crate-cmdtree-0.2 (crate (name "cmdtree") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)))) (hash "0z94lwfcswz6hwckz5lb1yxa7rhmcffv8c1lw7z3492j5mcy3qd4")))

(define-public crate-cmdtree-0.3 (crate (name "cmdtree") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)))) (hash "0qm37pbadnik3f4yzmcg811sg1hw85dchj4dwv9j6x6p5vgng5z3")))

(define-public crate-cmdtree-0.4 (crate (name "cmdtree") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)))) (hash "1nrikg06b6zx0lm943nmw069c0r9a48bffkji7pad44xqiyjhcw9")))

(define-public crate-cmdtree-0.4 (crate (name "cmdtree") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)))) (hash "0dz35wzw5r77bgiqdjl86a7a11h7ah3wn2gi9g1zd15wf90lhs3p")))

(define-public crate-cmdtree-0.4 (crate (name "cmdtree") (vers "0.4.2") (deps (list (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)))) (hash "0lny040mp68dmqy998mhbhby3mv907hxyrishp33rc5n1hadjh72")))

(define-public crate-cmdtree-0.5 (crate (name "cmdtree") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)))) (hash "1dcrla3549yq5drypdisa97rijnhpyn4cnkc0dmx462yc8hakmaa")))

(define-public crate-cmdtree-0.6 (crate (name "cmdtree") (vers "0.6.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.6") (default-features #t) (kind 0)))) (hash "0d51liq8cxiybnh0nmywf5ipmr52prm1x8my650y6nj702i0nk6n")))

(define-public crate-cmdtree-0.6 (crate (name "cmdtree") (vers "0.6.1") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.6") (default-features #t) (kind 0)))) (hash "1fr029ypc6dr06rxfak2lapcym8nsijq8485f6ywimcnag093jjq")))

(define-public crate-cmdtree-0.7 (crate (name "cmdtree") (vers "0.7.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linefeed") (req "^0.6") (default-features #t) (kind 0)))) (hash "1yb3lbv4iz81gryidyyms3pk52mh0aljzpw4db0wl9glp1gw24z4")))

(define-public crate-cmdtree-0.8 (crate (name "cmdtree") (vers "0.8.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linefeed") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "12qws36lq1m7ywfvx6448zd4gzxp3rs9ig23r15dvnqs637y7869") (features (quote (("runnable" "linefeed") ("default" "runnable"))))))

(define-public crate-cmdtree-0.9 (crate (name "cmdtree") (vers "0.9.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linefeed") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0w08xq31vdzk3hg8wgpxhipnqsp3fl3fh0xqf3kx33v6712yd7sk") (features (quote (("runnable" "linefeed") ("default" "runnable"))))))

(define-public crate-cmdtree-0.10 (crate (name "cmdtree") (vers "0.10.0") (deps (list (crate-dep (name "colored") (req "^1.8") (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "linefeed") (req "^0.6") (optional #t) (kind 0)))) (hash "1v54b1i2npcbwqp0n7bzgxqrxjbzx20vq4fi74wgyszs76hg0miy") (features (quote (("runnable" "linefeed") ("default" "runnable"))))))

(define-public crate-cmdtree-0.10 (crate (name "cmdtree") (vers "0.10.1") (deps (list (crate-dep (name "colored") (req "^2") (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "linefeed") (req "^0.6") (optional #t) (kind 0)))) (hash "05rhcjgkr7g2wf6720gax091387ym6gczwph9sv06r5xchvbbyal") (features (quote (("runnable" "linefeed") ("default" "runnable"))))))

