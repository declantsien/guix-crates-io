(define-module (crates-io cm ph) #:use-module (crates-io))

(define-public crate-cmph-sys-0.1 (crate (name "cmph-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0d06k10cc4w47px1v0pqciqdajwgfyn8mfl41hlnv65nj7m148l0")))

