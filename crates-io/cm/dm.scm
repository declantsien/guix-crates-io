(define-module (crates-io cm dm) #:use-module (crates-io))

(define-public crate-cmdmat-0.1 (crate (name "cmdmat") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.6.10") (default-features #t) (kind 0)))) (hash "12gvq592gs3bnhh15q0l4yvblpkp3hh6vh26y2lzhfml3z87lhjw")))

(define-public crate-cmdmat-0.1 (crate (name "cmdmat") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.6.10") (default-features #t) (kind 0)))) (hash "1k14ri06aspsza26dvs6pvml31wrgdlg9yfw6fmbrl52xb1aqmqp")))

(define-public crate-cmdmat-0.1 (crate (name "cmdmat") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "110d62c57n9p0i4scva7hx0mlmyfdnz0nnrlhac3cfqikj9shj8h")))

(define-public crate-cmdmat-0.1 (crate (name "cmdmat") (vers "0.1.3") (deps (list (crate-dep (name "smallvec") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1d7ls1vvscrai28alnac25m3xfs20qwgjh5v1x6582zm54j0zkz3")))

(define-public crate-cmdmat-0.1 (crate (name "cmdmat") (vers "0.1.4") (deps (list (crate-dep (name "smallvec") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "08sncw605gj9ll1lncsmp6js5qadn1ghzl92indvdmm9pgyscfx9")))

