(define-module (crates-io cm -t) #:use-module (crates-io))

(define-public crate-cm-telemetry-0.1 (crate (name "cm-telemetry") (vers "0.1.0") (hash "145xpmxm1pmcw5avgb5g7ig3451w2jmzzr9zrsmhhwgsvqr8k9aq")))

(define-public crate-cm-telemetry-0.1 (crate (name "cm-telemetry") (vers "0.1.1") (hash "11yyw62231vwpd0jfdm5i36ib1p79pf0p42jg4n3nd9xh47cd363")))

(define-public crate-cm-telemetry-0.2 (crate (name "cm-telemetry") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0mnxidg1w6mzv5dh5ra9zn90g3q91617jl7a11qw6magsnbk9yd3")))

(define-public crate-cm-telemetry-1 (crate (name "cm-telemetry") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1nm1zh84il5kljr00rxv55v5lw60s5xh36jmg2ja9llal816h45v")))

(define-public crate-cm-telemetry-1 (crate (name "cm-telemetry") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0n1wn6azq8m68yv5jybfcfhl7anj4p5l9lkcdmvnbc6yv027mjkc")))

(define-public crate-cm-telemetry-1 (crate (name "cm-telemetry") (vers "1.0.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1i75rwb9398cfrli1q14c3y1zzkgp0byiv1i7pnml9i3xqrz8ak9")))

(define-public crate-cm-telemetry-2 (crate (name "cm-telemetry") (vers "2.0.0") (deps (list (crate-dep (name "binread") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_default") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "046dmmwnvd243hnqqj1qyqriyw6gyqp9nkw2cf0k24z8y7dgm7bs")))

(define-public crate-cm-telemetry-2 (crate (name "cm-telemetry") (vers "2.1.0") (deps (list (crate-dep (name "binread") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_default") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gn4276jblyzpf5wni8awjalxc6zx1gz6iargpkli76v7df6wqvx")))

(define-public crate-cm-telemetry-2 (crate (name "cm-telemetry") (vers "2.2.0") (deps (list (crate-dep (name "binread") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_default") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ldzm27nvzy9nsci0bqg39p541rm37l1rr3fxbss8wgll7nifvvk")))

(define-public crate-cm-telemetry-2 (crate (name "cm-telemetry") (vers "2.3.0") (deps (list (crate-dep (name "binread") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_default") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "0yyvdqqadscmz5dflf5miqadmjqbrj5jg2slxsilins1f12w700p")))

(define-public crate-cm-telemetry-2 (crate (name "cm-telemetry") (vers "2.3.1") (deps (list (crate-dep (name "binread") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "046q2f23nb9vdpxav5n1qspsf2nplpyaav47f929wm3crrhlj4dn")))

