(define-module (crates-io cm ds) #:use-module (crates-io))

(define-public crate-cmdshelf-2 (crate (name "cmdshelf") (vers "2.0.1") (deps (list (crate-dep (name "is_executable") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "057yxw35mkamww4irr80if8zrq1cz941wp91jl23pxbscc8x2321")))

(define-public crate-cmdshelf-2 (crate (name "cmdshelf") (vers "2.0.2") (deps (list (crate-dep (name "is_executable") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "03scgm54xmyfbzv6lgy5cbd29k3lwx7hj4h6ywpqik2icklcb1d9")))

(define-public crate-cmdstruct-1 (crate (name "cmdstruct") (vers "1.0.0") (deps (list (crate-dep (name "cmdstruct-macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0282crcg7ksb84100k3rqmpiwqb2p7m3y38yxyym5dp91qb5xcvb")))

(define-public crate-cmdstruct-1 (crate (name "cmdstruct") (vers "1.1.0") (deps (list (crate-dep (name "cmdstruct-macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0a0vmmm224dabfqclg2dwhbp2mwj88a8c3i7xbfxphznmz49ixgk")))

(define-public crate-cmdstruct-1 (crate (name "cmdstruct") (vers "1.2.0") (deps (list (crate-dep (name "cmdstruct-macros") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "10ni04fvwclh4khq7ygr9zw23b7qh5s6k7gs6iqw57igghfp5jf4")))

(define-public crate-cmdstruct-2 (crate (name "cmdstruct") (vers "2.0.0") (deps (list (crate-dep (name "cmdstruct-macros") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1md4m1rilh9pk3jfqb0inljqzvp6dvngrr1vzl7m1qjqskbwfghi")))

(define-public crate-cmdstruct-2 (crate (name "cmdstruct") (vers "2.0.1") (deps (list (crate-dep (name "cmdstruct-macros") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "138k12ld7jykvi4xs0zkmmd7kmjmsq1m1mvqmcpfx0b0gbsqsfcw")))

(define-public crate-cmdstruct-macros-1 (crate (name "cmdstruct-macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kciv1ydcqp4190rkladank0kw3isa1h4dn664748x9r1w0r8657")))

(define-public crate-cmdstruct-macros-1 (crate (name "cmdstruct-macros") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17f31cmslyrmqjrf34b41hfnsbf0f0ah1k9w8ig4yiyhbnwxx8hc")))

(define-public crate-cmdstruct-macros-1 (crate (name "cmdstruct-macros") (vers "1.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18ksg4zb7h14pjb9wraph4dsxfk01r0vq6mf81sbz36v6s7kk88k")))

(define-public crate-cmdstruct-macros-2 (crate (name "cmdstruct-macros") (vers "2.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03jxbdsg0p4a51r9hp9fcaziml7mhdhrrak5brz3q0bkc02pivys")))

(define-public crate-cmdstruct-macros-2 (crate (name "cmdstruct-macros") (vers "2.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1w9k0xn6pwlszcsbcxzjqyqwvji50lpghv53g9fnhwp0xq2myvkj")))

