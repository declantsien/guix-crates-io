(define-module (crates-io cm o-) #:use-module (crates-io))

(define-public crate-cmo-rs-0.1 (crate (name "cmo-rs") (vers "0.1.0") (deps (list (crate-dep (name "sma-rs") (req ">=0.1.1, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req ">=0.1.2, <0.2.0") (default-features #t) (kind 0)))) (hash "0wdiqhyikznhkslzlrmmcifphkwnbl84hb9s365gs7l1lnrvwla6")))

