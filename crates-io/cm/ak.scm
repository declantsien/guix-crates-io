(define-module (crates-io cm ak) #:use-module (crates-io))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.0") (hash "1xnva9w22bysrri4pscrav3f4avgdj6jh47l10d997n4pia5gj5q")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.1") (hash "0brhjr93q84ffd9prmm0clcgx62ir923d6xqv7i624lb7sf9mhd3")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0frl1c2vizkhpz7kjp9rhsrxsc19rwk8ngzrb70mi62x4s0n0yy5")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.3") (deps (list (crate-dep (name "gcc") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "002kgi4091mabzdrx4w2cbmshvms2bllrpmbfwifvqiwcxm7y0sw")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.4") (deps (list (crate-dep (name "gcc") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0a76rw8y1va6r0mr0wknzy0sqwrh2l79g84gl33w4k6xl4c6hsvv")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.5") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "18cjnnfgw60i92fv0fv02k2k72cc6lsn567igkdmicdcfcmi10yq")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.6") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1gvlj15i8mn7wnfp1wrfz3pvxll33f3s98z1w18flr3h02yfz7ya")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.7") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "19wbxfdc0di35z2wc8rhp502843n8i53pkfw3iq7r1nwbgspfwjb")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.8") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1chvbjq2xcb9g3wzvcsf41jsgb5ah2739x2x915nq028wvv2sd2r")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.9") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1fbaq3wpj6fqmq4k99k1qzxngrkyiq0cg6w5qncybpsjvzpyngi0")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.10") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "151h87if31rjn2z23lfj0i7rqmvch9r4i803yh2bprrr3z69zvpv")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.11") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "145mk3xq7hmcscf66zwialq4j78fhdysp1wly9wia897w1x7cimn")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.12") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0wxzb7mml552qwv70l58zh68h6ri747swl94pj2iwx0sn15qjyxb")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.13") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1bi4cfvm91xy9smyzyb8f903w8rhf2ssycj22bzzbp70scq47c6a")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.14") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0dsbyja9c12ixly9d94ihin83mh5a0dw6jgsc9wb9aarzl944540")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.15") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1a0cwxy4xhjcixn6rjqwvh3p90ynfc6hfprrjmbp0lsgssm5aryj")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.16") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0ch5cpmn0n0y0zjgvh1mwfj5rpcbqhz94yf55bshm3jznv9shqgb")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.17") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0y29v47g6gdmps30vqmp41wfkpybkng50hm0iqxrbvsnrv75pkyz")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.18") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "10zjplmhk2sxlimqll782sjxgwwdv576ads4vz0q98cpw0kwynqf")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.19") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0am8c8ns1h6b1a5x9z2r1m3rszvya5nccl2pzszzjv5aiiaydgcf")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.20") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "01q4gxdwwac26jql3b37ir68dyyn7bh856nd3dy7w24myrfq19m3")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.21") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "098vjjgpggs0iykcrqnzc2x891d20sbx1xgr72pjfiki7y5cdb71")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.22") (deps (list (crate-dep (name "gcc") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1b3n7san1c4b0ghag97fh98ny1x4pxckd4g7qgf1diflgsc6i3fi")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.23") (deps (list (crate-dep (name "gcc") (req "^0.3.46") (default-features #t) (kind 0)))) (hash "08hif3z30kvpbk8phbkziyv90r1spfhhgrw9zifggj0jjjvqw9wj")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.24") (deps (list (crate-dep (name "gcc") (req "^0.3.48") (default-features #t) (kind 0)))) (hash "07xg162qzf9k871hmashsdjv4yfb3bg373qnjyad176wscsvpsxq")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.25") (deps (list (crate-dep (name "gcc") (req "^0.3.53") (default-features #t) (kind 0)))) (hash "0gcvi8iag0khg5zjqa4hjzr6v9m5078y5bjgvv9p5kavlm0nb2hc")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.26") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "17bqdgdjplnv2bmr0fpnzif307qs3s8bbvf1jckp75gwl7khfz1m")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.27") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "017nxg8j1zwanr7l3hn7ns2yvfd1gam08v8nywklw0y2rdiw03lv")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.28") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b2wnrin88rkc47h2lmk8gzz46p93s1lprbpas86lb5wgidd2k71")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.29") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fr8rrrnnl58zw0sf9ywjc2ni3vkykgvfdkbs3v7grb9gbm43msn")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.30") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z6s76ndw5m9914h25ys90d6sq7kbi338fnb02045ppdxg77ixjw")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.31") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cv72kd746my5ihjjmdzc7avjr8yvhd447rfpxr6swhwqcsh4iwm")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.32") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "033mgrb9hbpwx1dqbjfbav67ynlcj829bx64y3sam8x1iy9j2s5m")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.33") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yzfqpa71m5nmadxpklr80pqvw514kmdnmdjmyqam78lnlxvykvh")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.34") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "03n7yqx9hkbb8smd7mi0az7542906i75bi9873hz0j0glx7332w4")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.35") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jk1ph1yxbvzkll84jdsay395m3y8n9kdllia0rnzlf9z7j5xikf")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.36") (deps (list (crate-dep (name "cc") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "155s0s33affg76p9l9hp9qsbpmxlgkqhxf0cj65ld8ybkvjj5i4k")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.37") (deps (list (crate-dep (name "cc") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "01ickz92mxwan15n4zzmic305glrvb34hwjgf01frmi6y3xpj6y1")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.38") (deps (list (crate-dep (name "cc") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1j6fggcv675nwncflv6sbwh81ajf89lkg9jj0kygphsgagn0w8cn")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.39") (deps (list (crate-dep (name "cc") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0dk5m0vkqs7z83qspfkiwibqkgqq7m4dpxv4l0flbcbw88imbz6r")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.40") (deps (list (crate-dep (name "cc") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1w0zgqdbbhl9w6px7avc6d5p43clglrmjfdn2n26mdsli5n3i91c")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.41") (deps (list (crate-dep (name "cc") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0a9j1fch0w97mh0qavm5i9njmv3p8qjz9qsq3xwdc9givjbcb11w")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.42") (deps (list (crate-dep (name "cc") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "0qkwibkvx5xjazvv9v8gvdlpky2jhjxvcz014nrixgzqfyv2byw1")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.43") (deps (list (crate-dep (name "cc") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1ajc2c9i8sn0ljc8zl9vsfb5q2n1hdn7xzd33b2sy1kr2ri7bya9")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.44") (deps (list (crate-dep (name "cc") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1fv346ipxmvff6qrnh78rild0s8k72ilfjkdsrk869562y62cmhf")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.45") (deps (list (crate-dep (name "cc") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "0m8868gi7wrfszjk6qfmb8bgawywqsd5fbm1rnjgn78p6yv10qpb")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.46") (deps (list (crate-dep (name "cc") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1290ilzm0i09xva4wz8n24l5sp5flh4m4jmdmrjfdvv329a5if5p")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.47") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "1hh79zj121g44x49cg8kxzz5fwvjn0ph4s8myv5dbf7m2027cl1r")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.48") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "0fj4wxqdk8zzh1l0zrywpcx50a6jcj0j1wwxp1l7piaa23pqrbg8")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.49") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "0z3bqqi2gj9vx97zhcjnacwi41bi906zj5dj5rgp4c0b21p9ad6v")))

(define-public crate-cmake-0.1 (crate (name "cmake") (vers "0.1.50") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "0c3i3548mqbizpgbff94jjgkcd2p6q9fxjjh89zzf5dqcfaph753")))

(define-public crate-cmake-parser-0.1 (crate (name "cmake-parser") (vers "0.1.0-alpha") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0f0lbk17nmm1sa9gnv0qi7hs7vdxs3pwwagxinfhyq2crbxxjrnx")))

(define-public crate-cmake-parser-0.1 (crate (name "cmake-parser") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "check_keyword") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cmake-parser-derive") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "inflections") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "18bglsy9bh77hw7ilbhhy0c6xfv2zzhw1mxwh3n86nb016i3a159")))

(define-public crate-cmake-parser-derive-0.1 (crate (name "cmake-parser-derive") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "inflections") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1gn1khs9svg4z1dlhqb829ldj9fqvxc42yq2h4lddy21ihf2n1dj")))

(define-public crate-cmake-version-manager-0.1 (crate (name "cmake-version-manager") (vers "0.1.0") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1psxanwy1npv3whq262vm5gdwak8bar8cb23nz1q5q3a1s4i28yg")))

(define-public crate-cmake-version-manager-0.1 (crate (name "cmake-version-manager") (vers "0.1.1") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xvah7qn01a7clhji6zh6100vg4zicw3hb9wgjs6nir64bnpl7a6")))

(define-public crate-cmake-version-manager-0.1 (crate (name "cmake-version-manager") (vers "0.1.2") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0sywdg467zrycgiyb00nz9mckpf5xcizs6h80wf80j4f95vqrb74")))

(define-public crate-cmake-version-manager-0.2 (crate (name "cmake-version-manager") (vers "0.2.0") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "07w3rnzkq32kjd8aib88lz946ny2qf5crgq484c203wnnvgzn1p5")))

(define-public crate-cmake-version-manager-0.2 (crate (name "cmake-version-manager") (vers "0.2.1") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1jhvghk02gdf79q6b4yc3hmlb1hs4cahvgvgcxqbssh18n07fz0q")))

(define-public crate-cmake-version-manager-0.2 (crate (name "cmake-version-manager") (vers "0.2.2") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0c5297dq5kaq4xjady5zw2549whzd8b3l0jfbwpy5zm0wfrfgywp")))

(define-public crate-cmake-version-manager-0.2 (crate (name "cmake-version-manager") (vers "0.2.3") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0clpdn07kakf4m4cjicl8ip7y5xwmwifcdh7f9mk284jpm8mjnzq")))

(define-public crate-cmake-version-manager-0.2 (crate (name "cmake-version-manager") (vers "0.2.4") (deps (list (crate-dep (name "ansi-builder") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.19") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.185") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (target "cfg(target_os=\"macos\")") (kind 0)) (crate-dep (name "term-inquiry") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "07m29fj6dc54ciz0853m7sf18maqb8dyilispbjxf3sxrxjb7vw6")))

(define-public crate-cmake_config-0.1 (crate (name "cmake_config") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.6") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (default-features #t) (kind 0)))) (hash "0hd1zy83g162wprr30fsnh5z0k3xnhqyswz9fzvp1mr0hp5g18nr")))

(define-public crate-cmake_config-0.1 (crate (name "cmake_config") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.6") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (default-features #t) (kind 0)))) (hash "1qnsi2m4rb1rw0piz44sj9zx4sa2c602g3d5xmf25wl1jxzz0w71")))

(define-public crate-cmake_tui-1 (crate (name "cmake_tui") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cassowary") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yrj1260677nmb7w8s1gra79d978j30n8lb8klcwa5sww4hfqf26")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "0nsda6xndmqxy5n3nk9b3dxxk442lvh1ldh98gdzknbdmgx3391z")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "1s0w0mh53jpllyqr69gm6i5rplyqmip3hkfh3vvww4737zhslpkp")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "022zjf6yrqdvxz3v3hbvs14v1kd105py67nxz89gxnlppfsgrkfx")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "0ndg1rvwxfclfyaprygkdjgzh9cqn57xkddn5sf3g7zr0k9ir4x5")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "1lq3qf74jjk0jjp2c2b73whkngbqmf02c1gl6qqxf7zksyb8mlqw")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "17nn9ibfng78zcnaariywx339za3s1l2ghs97h4gc80fdgjxk8z6")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.5") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "1dir3if3marilk4570x2rpjzy3nsxz5178kcl1rxp2hj4vhx1zp2")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.6") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "0fph52gz3a65h3kh2p9hmzg44nfdypl4gkm7bxx3qb0awva9i0cq")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "0sg6i6ynxshxzs0pp6pxybs1293la07giq0cxpgni5vzha9qp3s4")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "nom-supreme") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "0qjki1bgsjbd01gp0iisvkgw33pn0fzx8ras5fkjb70ri0wbrfv1")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "nom-supreme") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "0lwm343nslbkqq1i51gb9hasls9xdllf9c45pp56nl8bl699jbw4")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.10") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "nom-supreme") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "1nyq6xfcj5lrc3kcrhfc4azy1ijgyhf4pgd1qrbsn8sbw9ks9q8c")))

(define-public crate-cmakefmt-0.1 (crate (name "cmakefmt") (vers "0.1.11") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "nom-supreme") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "1hjhnis26dhzszihgls23q0khvg7ax6bz31czmfshbxi1a20fgl5")))

(define-public crate-cmakegen-0.1 (crate (name "cmakegen") (vers "0.1.0") (hash "18qv1x36ypfl2gv3y1x9xv4v5wy0by2s458zkr607sdhkb33jgq5")))

(define-public crate-cmakegen-0.1 (crate (name "cmakegen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1kj5r3qa27qgqz2x01i2j4i55njigv5lmjs47vigqygghl25v5cq")))

