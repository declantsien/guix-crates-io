(define-module (crates-io cm pb) #:use-module (crates-io))

(define-public crate-cmpb-2 (crate (name "cmpb") (vers "2.3.2") (deps (list (crate-dep (name "prost") (req "^0.11.9") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0sdafc5x9s3inknvhrmp023sibnjgrjh0jb2xwyxv4fcp9f716kg")))

