(define-module (crates-io cm ic) #:use-module (crates-io))

(define-public crate-cmice-0.1 (crate (name "cmice") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mice") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "13v5mfs585vna8llf3kla4yp9aw792ybj8rhgwf83vw3hg4pxxlh")))

