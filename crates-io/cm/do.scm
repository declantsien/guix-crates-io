(define-module (crates-io cm do) #:use-module (crates-io))

(define-public crate-cmdopts-0.1 (crate (name "cmdopts") (vers "0.1.0") (hash "0lgpngbkx05xwb5j9j2f20zzqy4wrnndm8sri5ymwljhq6l35h0h")))

(define-public crate-cmdopts-0.2 (crate (name "cmdopts") (vers "0.2.0") (hash "0z3gzva90khb55i3qv9ajskfqbkfwijv458wf7ggk5zcj7bknflp")))

