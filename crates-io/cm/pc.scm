(define-module (crates-io cm pc) #:use-module (crates-io))

(define-public crate-cmpchain-0.1 (crate (name "cmpchain") (vers "0.1.0") (deps (list (crate-dep (name "trybuild") (req "^1.0.32") (default-features #t) (kind 2)))) (hash "0q0as7v2hy7a9ig9ba8p4wxpd2alqnhgg20igzh4fpynnibmbngg")))

