(define-module (crates-io cm dw) #:use-module (crates-io))

(define-public crate-cmdwrap-0.1 (crate (name "cmdwrap") (vers "0.1.0") (deps (list (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.175") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1h7narj2mq56xfmkqsdjirv4nln503mi0ndg9x5pairimhdqjjjb")))

(define-public crate-cmdwrap-0.1 (crate (name "cmdwrap") (vers "0.1.1") (deps (list (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.175") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0kvj55h1pyj66j6xfz9drag1d3rcgv9414dxzvpywybdlfd20ql8")))

