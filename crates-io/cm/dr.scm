(define-module (crates-io cm dr) #:use-module (crates-io))

(define-public crate-cmdr-0.1 (crate (name "cmdr") (vers "0.1.0") (deps (list (crate-dep (name "cmdr_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v5gb3b7n0kyi06wa3cbnfyjx4hx75qvrfvcp7m0kk50y7dangpz")))

(define-public crate-cmdr-0.1 (crate (name "cmdr") (vers "0.1.1") (deps (list (crate-dep (name "cmdr_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nw3z1rgdlgymxh6nnvkbhg7lhlqyj7r2qfpkkrzd7yz8nc11qbw")))

(define-public crate-cmdr-0.1 (crate (name "cmdr") (vers "0.1.2") (deps (list (crate-dep (name "cmdr_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "160fws467wympvjqyzn5ad1c9g23i3p2y07b43ndnzi8ywp8f8zl")))

(define-public crate-cmdr-0.1 (crate (name "cmdr") (vers "0.1.3") (deps (list (crate-dep (name "cmdr_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "17vanamxv0pkzz935yd20nsfljpqg0yfwfnisp8kvy5kqxyd9yi9")))

(define-public crate-cmdr-0.2 (crate (name "cmdr") (vers "0.2.0") (deps (list (crate-dep (name "cmdr_macro") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3") (default-features #t) (kind 0)))) (hash "0hz9azjspv23nh0cap049x6chm29c3c441ishxigvnmnry5qi5m3")))

(define-public crate-cmdr-0.2 (crate (name "cmdr") (vers "0.2.1") (deps (list (crate-dep (name "cmdr_macro") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3") (default-features #t) (kind 0)))) (hash "0cjwzds95z7yv6461h4wxkv19l5q2ir5s6v1cf483ykf8cpzzkdl")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.0") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3") (default-features #t) (kind 0)))) (hash "1qhwqpzfwj764a1ycq7y3bq187mimzg1a4xyars5nw3v3sdaiv05")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.1") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3") (default-features #t) (kind 0)))) (hash "1bmdp8zhq1p13n9vs3y6f6i43ypgqa6n8j21ly47c8xzhabb8xgq")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.2") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3") (default-features #t) (kind 0)))) (hash "1vzlyin2yflkdm3ninl1zl89bnsdj4fnskxb3q3i3cadf68fq983")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.3") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3") (default-features #t) (kind 0)))) (hash "1ycp9a9f777bi4jqzj33yfnh6wn8q68ixj6gp5m28qkqs786630l")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.4") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3") (default-features #t) (kind 0)))) (hash "1yk4dn5p7pgx65wdf72r1yprnhjp00sm4pjm46rb2zsxzyridgdx")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.5") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^4") (default-features #t) (kind 0)))) (hash "08whidcsz19j4hkihswdmkyi575n2iji0rzzmpn0v6g7xx30ff0d")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.6") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^4") (default-features #t) (kind 0)))) (hash "18z4qdmh5f9xm8jsrncdgwmav2dqm8s89mci6vg731mxnp01g097")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.7") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^4") (default-features #t) (kind 0)))) (hash "0i21hxqym0lc38sd3mfzzd43pzfyd0b9wf08pgm3yxnfnz7hbmrp")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.8") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^4") (default-features #t) (kind 0)))) (hash "00wc9sb3iyf5d5vvqsz7iyb0q2bl9b4abz7ybbxqhcw1qvvdps9d")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.9") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "05apdv61f31kjwa19ld8q2ckb4anmsj47ryvy3rxqny5ay87skwl")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.10") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1231sxw9qj3ib8dl4wafg3qwayl1nv4mdvcw1p6h32vyi5g896c6")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.11") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.3") (default-features #t) (kind 0)))) (hash "1bkhw18n7yi4k4vcgfcjsa3p620g7na4qmpr3caffksyv23d0alk")))

(define-public crate-cmdr-0.3 (crate (name "cmdr") (vers "0.3.12") (deps (list (crate-dep (name "cmdr_macro") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^6.2.0") (default-features #t) (kind 0)))) (hash "11fbr9p11hnnv10nf09n6pg9a0kiai29kvwfln71gcnbniyiarb8")))

(define-public crate-cmdr_macro-0.1 (crate (name "cmdr_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "12wja8c358zc9jmhx2wfgl4rnwi6s22j9bns8nqrrvmh0kmmqkyj")))

(define-public crate-cmdr_macro-0.1 (crate (name "cmdr_macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1rnnx69pqv6wz7cgsvcyr5mrzy9l2aqihnkh5i2jjvlaf13zs62b")))

(define-public crate-cmdr_macro-0.1 (crate (name "cmdr_macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1idnshmf1nkkqb9k98k8i6y272aiank6qp427djp7qp4dmjfc7ix")))

(define-public crate-cmdr_macro-0.1 (crate (name "cmdr_macro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1lbhpa0dx44n02m2sr3wfsx44dv5r28n91fcln49f41izbnsl6c1")))

(define-public crate-cmdr_macro-0.2 (crate (name "cmdr_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1i2q3hxyg18w782phch3mi6bshh6jdlkfbxym7qzmz1bczfldm9p")))

(define-public crate-cmdr_macro-0.2 (crate (name "cmdr_macro") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1sl9hrr40nf3sn4czybmvagj1qyfb30r96kdc77l4zw37bga376f")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1impzpfm42yhgqnx71rnwfpgs5h92s2j3qnsshdg90bv7h1cidac")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "0fqrwa8lwybk4n84slk0m0i7mgv5vl1dmrzdy7v7sl1vf7fa61ga")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "0613w09phhgz735cv816mdaqg0ik25293922hm0mxi5vk3vm6wkq")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1vgr11jjc7r7ld7zhpzvy3clawd10ljcxf9zvaxbnxzqa9pnz83a")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.4") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1m2ifps1l9ah3001s5y5vp1hg4khzw43rss3bggyi94v0gh6sax3")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.5") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "00s3fq1acg1mgbx0fsch2s5zzbk0wv0xbj3cgwk0cccqvxvqmcyz")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.6") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "0cw9bajngzl0jxi9h085dl30sxrn44zcv5v9jiphyynr9l4nimgv")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.7") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1nimgv8i3h00lmd29if4d3apiqphcm676phh7lmcn5v2q641n883")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.8") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1sydnv0y12909rbrs9i3xcp17iyg4j6d054z6k3vpvcmvlk21i8j")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.9") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.35") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1p8q3y4yc3f45yawasdwlk7pjrjqm84ndc3fqb630bkfv6mlkx6c")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.10") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1p5kw8casqi990i9cwn4jx76hi26ggbmfg8nl1vdgy3x2gv10cvw")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.11") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "19r1csi8da1f6f7y6ziynvrlcv4v09jmxdvcdfs6yknhx6gywn53")))

(define-public crate-cmdr_macro-0.3 (crate (name "cmdr_macro") (vers "0.3.12") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1nqqhz3x3543n2i13hfv1q8z6ifk1xw7wlmxyy0m5ny51rpvnfkp")))

