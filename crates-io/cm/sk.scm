(define-module (crates-io cm sk) #:use-module (crates-io))

(define-public crate-cmsketch-0.1 (crate (name "cmsketch") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_mt") (req "^4.2.1") (default-features #t) (kind 2)))) (hash "1x5avf1d8zs01nlqpyxdyp4vw1kk0f1mcphlibm99na2g8fg101l")))

(define-public crate-cmsketch-0.1 (crate (name "cmsketch") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_mt") (req "^4.2.1") (default-features #t) (kind 2)))) (hash "17l80j4s0hqh3rsa8cbyjz8hdavcmza6bgwbiq4hwhjy7i2m3js9")))

(define-public crate-cmsketch-0.1 (crate (name "cmsketch") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_mt") (req "^4.2.1") (default-features #t) (kind 2)))) (hash "1vs10a7vmj7v9vpiza5yiyc19szhgnazgg01q9kngd4z4ss3mj2j")))

(define-public crate-cmsketch-0.1 (crate (name "cmsketch") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_mt") (req "^4.2.1") (default-features #t) (kind 2)))) (hash "16ar5r68y5fkkp4j5m3a95dm5nv3kg0ysqs3px4j07sj1pqw66h4")))

(define-public crate-cmsketch-0.1 (crate (name "cmsketch") (vers "0.1.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_mt") (req "^4.2.1") (default-features #t) (kind 2)))) (hash "1w62djvj6z1p7xnbs8r00iqdcnlw9r9wshzbkazm6578hw2lczj6")))

(define-public crate-cmsketch-0.1 (crate (name "cmsketch") (vers "0.1.5") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_mt") (req "^4.2.1") (default-features #t) (kind 2)))) (hash "03nn4sy06l9l177hcn7542yz7flxyzwkcymi18jyldvwp2c0awck")))

(define-public crate-cmsketch-0.2 (crate (name "cmsketch") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_mt") (req "^4.2.1") (default-features #t) (kind 2)))) (hash "1zvg5gzwhp2ijy4jwwqn4w806qh9c0fvn3wnhnmacifabm6lpa8p")))

