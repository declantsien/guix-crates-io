(define-module (crates-io cm p_) #:use-module (crates-io))

(define-public crate-cmp_any-0.8 (crate (name "cmp_any") (vers "0.8.1") (hash "08bydg7d4jxw1dyvpg9i4mwdn5645q3j96ijclpwx0rl4lrq5cg9")))

(define-public crate-cmp_by_derive-0.1 (crate (name "cmp_by_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rust-format") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.80") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0m047cnkcjxl0wqrwlx00pzgqyz1f120l56xz4kfsvlz2zhvr65k")))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.0") (hash "1chn8awsbb8615mkff9qcfb55clpawl70djlvcp83ajli8a95sfp") (yanked #t)))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.1") (hash "0r339gi02v6cqqd64bzrknkm71an0albyj1fzpad50n6q4i3hpry") (yanked #t)))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.2") (hash "0c4z7dxfj2pjszmzw131hirq03zj0z4dsaz9g9f17gbm43pqkr2b") (yanked #t)))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.3") (hash "1w3pd83mkkmp6g8i4lj5lv83s23b81cvnv88xawyaga2f26w83n5") (yanked #t)))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.4") (hash "0dc157as9khcgrh4k6ql8gamzgr0sby66ql0c19k968n7dpbacj0") (yanked #t)))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.5") (hash "0ml4g994gl8miiwayrl2c910svwf32rkd364bx096hycaqbhfhbq") (yanked #t)))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.6") (hash "1z8p9nvi1dk8b6hllrfqf5a5mbkh6lxxqjbdslvlr7jr6nypcp0r") (yanked #t)))

(define-public crate-cmp_floats-0.1 (crate (name "cmp_floats") (vers "0.1.7") (hash "0cgczq7jhqxnydk311k7llmzp216n61abqf136ag8xafv0yxasz9") (yanked #t)))

(define-public crate-cmp_json-0.1 (crate (name "cmp_json") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1ayw5jw84xdqs74gwvklzkhpf7rkpv016v8ly1n77xnr4szns63x")))

(define-public crate-cmp_macro-0.1 (crate (name "cmp_macro") (vers "0.1.0") (hash "004yr01i37bbb5xr745qjaa1qzqms66qicfnwfwip3715429ng7n")))

(define-public crate-cmp_wrap-0.1 (crate (name "cmp_wrap") (vers "0.1.0") (hash "1ljs7wvmavvzih4l340r94gksn2ysj45zrmx6jazjr3lfgv48h8r")))

(define-public crate-cmp_wrap-0.1 (crate (name "cmp_wrap") (vers "0.1.1") (hash "0rf47rxdi9hgmh0g96d6i1lz2gzgqlibqzx6j5x3v7nnz2wn0iln")))

(define-public crate-cmp_wrap-0.1 (crate (name "cmp_wrap") (vers "0.1.2") (hash "0fjaaflmwb4283jkjvyjvar9x1pcggjb0il0bmswr4f8xpyc8lx5")))

(define-public crate-cmp_wrap-0.2 (crate (name "cmp_wrap") (vers "0.2.1") (hash "1bm9f4a241169fl522byw1752z55km2caknq1j5js9491qbw5v4i")))

(define-public crate-cmp_wrap-0.2 (crate (name "cmp_wrap") (vers "0.2.2") (hash "16kpijkw5sxjx73zyiyi2xjzncgsqvbykl582s6q6f59f81bmj5x")))

