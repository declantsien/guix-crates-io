(define-module (crates-io cm da) #:use-module (crates-io))

(define-public crate-cmdargs-0.1 (crate (name "cmdargs") (vers "0.1.0") (deps (list (crate-dep (name "cmdargs-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "008gywhj8pqjd4w33xwllc50vjjgp92r6hgk90crrvwpgdkw9hn8")))

(define-public crate-cmdargs-0.1 (crate (name "cmdargs") (vers "0.1.1") (deps (list (crate-dep (name "cmdargs-macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10jxf0a0ak86msyvhl5cn15k7znl96bhbj1hck4adislp6px8879")))

(define-public crate-cmdargs-macros-0.1 (crate (name "cmdargs-macros") (vers "0.1.0") (hash "0cs7a4w5sf7b2f7l3p08hf12w27wy2nv6rfqjf7pcq9kh4wplqsh")))

(define-public crate-cmdargs-macros-0.1 (crate (name "cmdargs-macros") (vers "0.1.1") (hash "0qxcbfrqqbq16k08sm5jaa6kj28iynwmafl80qfcygzqzlmigpa1")))

