(define-module (crates-io cm os) #:use-module (crates-io))

(define-public crate-cmos-0.1 (crate (name "cmos") (vers "0.1.0") (deps (list (crate-dep (name "cpuio") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1m9gj7kbrvhq6qdq1zi6jbzs2hddsiphni3wkkacrvvkpn43zb7g")))

(define-public crate-cmos-0.1 (crate (name "cmos") (vers "0.1.1") (deps (list (crate-dep (name "cpuio") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15a0lsswn4hc74savchfbp35y2n4jabda68d8sk2xqa4aslpflqa")))

(define-public crate-cmos-0.1 (crate (name "cmos") (vers "0.1.2") (deps (list (crate-dep (name "cpuio") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mcwaghvgml42x721w56gcpkvqisrhbjydm57riw3jk52qi0p7z5")))

(define-public crate-cmos-rtc-0.1 (crate (name "cmos-rtc") (vers "0.1.0") (deps (list (crate-dep (name "x86_64") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1mvlf6z9agsh8svcp12l2d4hfp90klf2vdlls61pf2iahb8fcrrn")))

(define-public crate-cmos-rtc-0.1 (crate (name "cmos-rtc") (vers "0.1.1") (deps (list (crate-dep (name "x86_64") (req "^0.14.10") (default-features #t) (kind 0)))) (hash "107cyylllwmzrfj958fh28qa0l50qgvifz0rs81q5zcdw3k3cn4y")))

(define-public crate-cmos-rtc-0.1 (crate (name "cmos-rtc") (vers "0.1.2") (deps (list (crate-dep (name "x86_64") (req "^0.14.10") (default-features #t) (kind 0)))) (hash "0wmmp3azzj5y68869mgby8ggk7jqg0i5xxr1rqh793qhpbc0klgx")))

