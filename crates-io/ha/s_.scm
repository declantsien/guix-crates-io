(define-module (crates-io ha s_) #:use-module (crates-io))

(define-public crate-has_colors-0.1 (crate (name "has_colors") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "13xcz96a515wb58332s389p5f29zxdyckf6l5hsrc7av7zsr4fmd")))

(define-public crate-has_command-0.1 (crate (name "has_command") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0asf9gahps8kbx910j048cf0rh4f4wrwbq97wbx8zk36daf8pwh5")))

(define-public crate-has_fields-0.1 (crate (name "has_fields") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "01l8bs35i3vphv79qcx0lf98ym8glrc1ngwhrviddicc8h3rqidn")))

(define-public crate-has_fields-0.1 (crate (name "has_fields") (vers "0.1.1") (deps (list (crate-dep (name "has_fields_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "139p40n9r8ajg7kr5ibd9blwzj3x2gkc86s3miy62i8i7wmvw93p")))

(define-public crate-has_fields_macros-0.1 (crate (name "has_fields_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10drrx9r48mnx064rnl2rqqcfmfb6ixzzqn4z3zcad1svvf747yy")))

(define-public crate-has_flag-0.1 (crate (name "has_flag") (vers "0.1.0") (hash "1yk9kfxlw2ww4hlwq5vcz6bmc6n34miismk6mw42y90b5105vlhi") (yanked #t)))

(define-public crate-has_flag-0.1 (crate (name "has_flag") (vers "0.1.1") (hash "02dnpqi6wznj6mr979p59j3a7dwcpjqk1klkz5wpg8lfc08sxax4")))

(define-public crate-has_impl-0.1 (crate (name "has_impl") (vers "0.1.0") (hash "1781qafblw8zqhdggdf5r7iixzx63msr2cl240qqlsiwbldy7b11")))

