(define-module (crates-io ha xc) #:use-module (crates-io))

(define-public crate-haxcel-0.1 (crate (name "haxcel") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("winuser" "libloaderapi" "debugapi" "processthreadsapi" "handleapi" "namedpipeapi" "winbase" "fileapi" "errhandlingapi" "wincon"))) (default-features #t) (kind 0)) (crate-dep (name "xladd") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1msd4q51d41d8gn0gv9xk7p5lp5as9hs1qkvhlrxvwg1xmkh4prl")))

