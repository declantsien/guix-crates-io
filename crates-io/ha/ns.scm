(define-module (crates-io ha ns) #:use-module (crates-io))

(define-public crate-hansard-0.1 (crate (name "hansard") (vers "0.1.0") (deps (list (crate-dep (name "atom_syndication") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)))) (hash "0y0dsil8lf3lan000lsm9sqiqza7za7b7gxi71inhfagh4w2lyxj")))

(define-public crate-hansard-0.1 (crate (name "hansard") (vers "0.1.1") (deps (list (crate-dep (name "atom_syndication") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)))) (hash "05wh20nphg505fd19djxc7ry268525b387g5awilywipn36m42p6")))

(define-public crate-hansard-0.1 (crate (name "hansard") (vers "0.1.2") (deps (list (crate-dep (name "atom_syndication") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.2") (default-features #t) (kind 0)))) (hash "039qsjkwjkvnjpngcrv87ggbl9bxklmw37j89kpkdms523ccky0b")))

(define-public crate-hansard-0.1 (crate (name "hansard") (vers "0.1.3") (deps (list (crate-dep (name "atom_syndication") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jwwmx4kv858af6mdr05w82h7k64d64vlm70i01vvbczicd65iy0")))

