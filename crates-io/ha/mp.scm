(define-module (crates-io ha mp) #:use-module (crates-io))

(define-public crate-hampel-0.1 (crate (name "hampel") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bkggh7viq8g24bing445fq02abqqnjsyyhhai0ncf7s9k58205v")))

(define-public crate-hampel-0.1 (crate (name "hampel") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1laryn2kx98acnnxshn83gbjq807kkbcy93a39lblxi38r9pmwp2")))

