(define-module (crates-io ha ta) #:use-module (crates-io))

(define-public crate-hatanaka-0.0.1 (crate (name "hatanaka") (vers "0.0.1") (deps (list (crate-dep (name "rinex") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "0apclginqnxmkq99rr848cf3fapfc1bp66lcri3cnkfxcx5p2xgr") (yanked #t)))

(define-public crate-hatanaka-0.0.2 (crate (name "hatanaka") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1vnkl2pvgwjpplvyjpi8g9jy2wmq16ydh5g4gfy071vikjgg6ljd") (yanked #t)))

(define-public crate-hatanaka-0.1 (crate (name "hatanaka") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "~2.27.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "rinex") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0wa5x0pmnrgsf6xs40kz5nkv9k57pbq6yclm2dv4nr3yms1vrnzx") (yanked #t)))

(define-public crate-hatanaka-1 (crate (name "hatanaka") (vers "1.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "~2.27.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "rinex") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0rr6n4jkg4vp1wprzq5brz0i38ff1541pj1mlf2j2gz66i67qcmx") (yanked #t)))

(define-public crate-hatari-0.0.1 (crate (name "hatari") (vers "0.0.1") (hash "090y872l3x5v74q7p3p278x6ssi50cllbvldavcw71bn4mn7arwv")))

(define-public crate-hatari-0.0.2 (crate (name "hatari") (vers "0.0.2") (hash "1jqw8s0ggcxbq9pcm0rczl4ykingrls6x79245r1pg5chwmzc0c7")))

