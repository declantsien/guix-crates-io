(define-module (crates-io ha nj) #:use-module (crates-io))

(define-public crate-hanja-0.1 (crate (name "hanja") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7.21") (default-features #t) (kind 1)))) (hash "06i54syf9vj7mxyxnnhzxnj2cx3n58q0qgcs838yxblcdf5sgcnj")))

(define-public crate-hanja-0.1 (crate (name "hanja") (vers "0.1.1") (deps (list (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7.21") (default-features #t) (kind 1)))) (hash "1r6xg4v746bhdhrrxv6xz7xnbdfq6kzlh0wgi75lzd1pdn7h6cgj")))

(define-public crate-hanjie-0.1 (crate (name "hanjie") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1cnp7p4r4gm3nh41a14mgmhy4sp7sjmjhfqsp59hc12xq45972pj") (rust-version "1.75")))

(define-public crate-hanjie-0.1 (crate (name "hanjie") (vers "0.1.1") (deps (list (crate-dep (name "bevy") (req "^0.12") (default-features #t) (kind 0)))) (hash "1dljsg8qxjja3r8bxrl7r9k6awvag9pmwk3bgq2kilqicig8pj97") (rust-version "1.75")))

(define-public crate-hanjie-0.1 (crate (name "hanjie") (vers "0.1.2") (deps (list (crate-dep (name "bevy") (req "^0.12") (default-features #t) (kind 0)))) (hash "102w3vpn7d6gcibikn8i4ad1zwprv8ly48paj7y5c4ch1sn47lss") (rust-version "1.75")))

