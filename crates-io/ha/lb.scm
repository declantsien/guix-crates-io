(define-module (crates-io ha lb) #:use-module (crates-io))

(define-public crate-halbu-0.1 (crate (name "halbu") (vers "0.1.0") (deps (list (crate-dep (name "bit") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.177") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1d1k4y2fsaw26kb7lklhc3bqqz9y7ffgfslfj9n33qxdqsar9gqn")))

