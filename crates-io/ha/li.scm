(define-module (crates-io ha li) #:use-module (crates-io))

(define-public crate-halide-build-0.1 (crate (name "halide-build") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "0jlm08jkjhjlsg0dj2m02b814wk1vdxwr4inx8ggk1wcrx0g52f5") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.2 (crate (name "halide-build") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "06ps202xx75p28460d2w3rp331skwy88vwiqdilc6sb8hpijznpc") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3 (crate (name "halide-build") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "08z3fn81ixjyw856qr5bjzgyjnw8chk6rsc5422qpjyar1ngmxy7") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3 (crate (name "halide-build") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "1lkx1v68sjxnzfnpjv50410q2r22x7ysswg5qsgkq76js3hi3q8i") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3 (crate (name "halide-build") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "1psf1pmh89nnqhkb0jv39rcvkcyh316iz406pli3lx4fjfdv6lvw") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3 (crate (name "halide-build") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "0f9s82jjny8p4bswmgwyhhf42zilipm8dk9jlqbf9hhgd9f1w2ws") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3 (crate (name "halide-build") (vers "0.3.4") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "0kjdafpp2ynxrsx0v1h2b39s51c401phl499a8mk8l2h5ah8sbvv") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.3 (crate (name "halide-build") (vers "0.3.5") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "0vx7f7gjq34yb28vh00wzv8ip6rj3y7rpv9j1il977lw8gczcywr") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.4 (crate (name "halide-build") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (optional #t) (default-features #t) (kind 0)))) (hash "18xj0nqkrh769kq7mkdyhgq0jsk1aj8a36x187p3vchpqaz7fswn") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.4 (crate (name "halide-build") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (optional #t) (default-features #t) (kind 0)))) (hash "1fck2svxd9l6n9jr6ygxfn4ml1s3hmcv1prmnh4ag0r4ijz5jyqq") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.5 (crate (name "halide-build") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("env"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xcw2sl3k48ynaglsldajkh17b2zv9mqf395xclc430bk20kaa5a") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.5 (crate (name "halide-build") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("env"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xpylrajg225hq53q4xkf35wwsqwl8znz0w9s8k3xzh0isynjzrm") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-build-0.6 (crate (name "halide-build") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("env"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mjw4wlc3vzay2xzmld6lkcd8vdmfygzjrh81bh51gx2xn2vgfqp") (features (quote (("default") ("bin" "clap"))))))

(define-public crate-halide-runtime-0.1 (crate (name "halide-runtime") (vers "0.1.0") (deps (list (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1kr9kxqjzl8iza6ic9nhgx3m4n1md7wn7c85d2g92xc5yzppvgx5")))

(define-public crate-halide-runtime-0.2 (crate (name "halide-runtime") (vers "0.2.0") (deps (list (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0swpdp8wqrvhydszsf20p1q116li7hzbk70zvl732k4vjrf3zg6y")))

(define-public crate-halide-runtime-0.2 (crate (name "halide-runtime") (vers "0.2.1") (deps (list (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1r3lp2r9pa0kjyrkyc5gfwr5i9wkg517pmwc9x4827jf15aylccy")))

(define-public crate-halide-runtime-0.3 (crate (name "halide-runtime") (vers "0.3.0") (deps (list (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1hcxf7xfb06pwhga571gx7b2v4sx6z1aadr62yh8bl86l7n57098")))

(define-public crate-halide-runtime-0.4 (crate (name "halide-runtime") (vers "0.4.0") (deps (list (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1g8casy5cwm7cs2xc0w7jgk0xbz0ycnimn33kn1pzgx2sjdaz6n9") (features (quote (("gpu"))))))

(define-public crate-halide-runtime-0.5 (crate (name "halide-runtime") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "halide-build") (req "^0.4") (default-features #t) (kind 1)))) (hash "0xi25bsn8rsklwzfzzfiqhj28179y5s1rfsd77xv33b4rimxaavj") (features (quote (("gpu") ("default" "gpu"))))))

(define-public crate-halide-runtime-0.5 (crate (name "halide-runtime") (vers "0.5.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "halide-build") (req "^0.4") (default-features #t) (kind 1)))) (hash "0zzdl3bk5951zan4py2rbl2jf04bvcn59zl7gjc64n2iqk138v5g") (features (quote (("gpu") ("default" "gpu"))))))

(define-public crate-halide-runtime-0.6 (crate (name "halide-runtime") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "halide-build") (req "^0.5") (default-features #t) (kind 1)))) (hash "1dkmx74wk986dfvkl1f04706v1gi5jhcdicvxshdh2aqhp3jm3ah") (features (quote (("gpu") ("default" "gpu"))))))

(define-public crate-halide-runtime-0.6 (crate (name "halide-runtime") (vers "0.6.1") (deps (list (crate-dep (name "bindgen") (req "^0.61") (default-features #t) (kind 1)) (crate-dep (name "halide-build") (req "^0.6") (default-features #t) (kind 1)))) (hash "1j62d3zl2drzjp855rdbafsxynpjmbr3bg64vzdwffsf63bwlsrr") (features (quote (("gpu") ("default" "gpu"))))))

(define-public crate-haligtree-0.1 (crate (name "haligtree") (vers "0.1.0") (hash "1jdha1a5dkakv59n3ry6fwy2n4fjxs9al76vgsgv0w6gi6q1nh6s")))

(define-public crate-halima-0.1 (crate (name "halima") (vers "0.1.0") (hash "0gp35j4s7s92nmy3zmd647ss92xfk6ndanc29qfpjnmbzrhkrbzw")))

(define-public crate-halima-0.2 (crate (name "halima") (vers "0.2.0") (hash "1m9ncd9fa7d9avp54jgmva1rmry7f7slxrmfr5y5mijn5vdn80jj")))

(define-public crate-halio-0.1 (crate (name "halio") (vers "0.1.0-alpha-1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "taskio") (req "^0.1.0-alpha-1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0grmalgfsjw61ky2d1s7hpwi5hrad6899bmbhijh7pq80c0b4rwx") (features (quote (("full" "embedded-hal") ("embedded-hal"))))))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "081fys4f0wd3n7f32lya18vml9xbdk16kwzw8jch69cdnkbmb3d8")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "0g00bx9zh56rb6vg9hgdkwgpvfykk23nidlf9zhf90hs8wlqczhx")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "0ifnkkc35kf43lnsbalsf8n2mb5gsvhi8s4bbnhnsj0as5ibdxs9")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "00c02a3vhq92wm1swlwiz944ibq8p50a8ka703sl88392ggv48yk")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.5") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "07bckkgn78k1a8xw5jfcz0mxwriskrb2bqf3kfxahh6s99mfwy02")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.6") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "1622sn77qqsk7slk3j2fpznqx29dbaygxkp87b7bnjpzv4b8z2h7")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.7") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "1hqx2dmv8s9ksl6r161z7szq6vjdfmkdvqfd87hha91gfpvv5gl8")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.8") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 1)))) (hash "0c66xzig23csd4wlwnnlspsmcqi4sr35qn7wp9zbxkwp8wzfkjgc")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.8") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.4") (default-features #t) (kind 1)))) (hash "1hwv7x5mw0ymljy58k8kx8a6d8z11vccrvc6nsi2d92bc7hplnw0")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.9") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.10") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5") (default-features #t) (kind 1)))) (hash "1dz6zxw3bw3nq4n5xhd8jf0gxrckpm5q9lpp9mr8mliaxf7knfak")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.10") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.10") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5") (default-features #t) (kind 1)))) (hash "0m39601979l7j7na33jy8xrx467qyjii0hwbg1halcm3qy35244x")))

(define-public crate-halite-sys-0.1 (crate (name "halite-sys") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("backtrace"))) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.10") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5") (default-features #t) (kind 1)))) (hash "06nys23hv3j3753wjb7r0ybsfibchzb6r03vdpq49np6rajqc06s")))

(define-public crate-halite3bdk-0.1 (crate (name "halite3bdk") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1512cbffnl6qanckzvzcc1alzmahyf0jn2g555cmvr32d00j4pmb")))

