(define-module (crates-io ha lc) #:use-module (crates-io))

(define-public crate-halcyon-0.0.1 (crate (name "halcyon") (vers "0.0.1") (hash "1bikx9mvfijz25jny1h8k32r4q19hlsm38diqhw68vqn68189ly2")))

(define-public crate-halcyon_build-0.0.1 (crate (name "halcyon_build") (vers "0.0.1") (hash "0fzwdx0rgg2rs3h5p2n8kiwlxjnwjbkff5k6fq5andn4mdv7ml9p")))

(define-public crate-halcyon_dom-0.0.1 (crate (name "halcyon_dom") (vers "0.0.1") (hash "072mv7q0lzp3yi66c0g9kdxkh9np6bn9j4xg9sr7aq81ib47scbz")))

(define-public crate-halcyon_macro-0.0.1 (crate (name "halcyon_macro") (vers "0.0.1") (hash "0isbfhxb6aiz2kbcffk8qvyc7nv10sykk9iw6ygfml58mhybh2al")))

(define-public crate-halcyon_macro_impl-0.0.1 (crate (name "halcyon_macro_impl") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0acg2iaj4ziriin0gm0ccz4srrra7vp10gcy6xf53hwjff5xymqa")))

