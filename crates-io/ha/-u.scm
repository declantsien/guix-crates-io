(define-module (crates-io ha -u) #:use-module (crates-io))

(define-public crate-ha-utils-0.0.1 (crate (name "ha-utils") (vers "0.0.1") (deps (list (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "0f53aqs49v005qydkxlk3vm2f7xfcgjb81h9zyk4frnn6v1m9d16")))

