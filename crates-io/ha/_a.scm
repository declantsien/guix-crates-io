(define-module (crates-io ha _a) #:use-module (crates-io))

(define-public crate-ha_api-0.1 (crate (name "ha_api") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05yyhqdjxb704y60mk19v30w3bam2xs7si8hg8i433zbwcbhymc1")))

