(define-module (crates-io ha zm) #:use-module (crates-io))

(define-public crate-hazmat-0.0.0 (crate (name "hazmat") (vers "0.0.0") (hash "0b0mz86dgy4465xcj4s543nsrj77gpkq5y7h30ay53hrzmy7gwk8")))

(define-public crate-hazmat-0.1 (crate (name "hazmat") (vers "0.1.0") (deps (list (crate-dep (name "hazmat-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0c4j5lk62v7dm7ii27m4ng6qp05b00243krbz24dnpk1hw6lryny")))

(define-public crate-hazmat-macros-0.0.0 (crate (name "hazmat-macros") (vers "0.0.0") (hash "10pi74db3sa95fv6dwqjznfydbpmfamhhq6rx6j09j8hkcg081x7")))

(define-public crate-hazmat-macros-0.1 (crate (name "hazmat-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kpsyn5bbfjjkf8h7ssfxgc44l2r08jsgq49f3klq1x4sbwr52k3")))

