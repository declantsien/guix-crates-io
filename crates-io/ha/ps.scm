(define-module (crates-io ha ps) #:use-module (crates-io))

(define-public crate-hapsi-0.0.1 (crate (name "hapsi") (vers "0.0.1") (hash "1lpl8qkiahwh5paf5zj8jkmn5jaahm1ma1k7i11dk3li35s2xr0k")))

(define-public crate-hapsi-0.0.2 (crate (name "hapsi") (vers "0.0.2") (hash "1dln68z1ahblglayi16dx56apzibwv8s4gz2r2c7b1xgx6fj3p3d")))

(define-public crate-hapsi-0.0.3 (crate (name "hapsi") (vers "0.0.3") (deps (list (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "1sdr3aiznf18cmkwp6x0vf8didfy57chkyakik30xn3z1kwxr1aq")))

