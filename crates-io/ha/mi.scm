(define-module (crates-io ha mi) #:use-module (crates-io))

(define-public crate-hamilton-0.0.0 (crate (name "hamilton") (vers "0.0.0") (hash "1jsl3f52v7xrqjpx179q82mi95i0m91940bw7gxwaf0dd3qmc5c4")))

(define-public crate-hamiltonian-0.0.0 (crate (name "hamiltonian") (vers "0.0.0") (hash "0w99k1hc5zwzmv4aia0pqvgrh8052nl1d6y0xv53nncwijyzzbhg") (features (quote (("default"))))))

(define-public crate-hamiltonian-0.0.1 (crate (name "hamiltonian") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0p1qdxljbccn0l7rs95snmii7lwh1y2jz76q6d2ghzb2ffmnd0py") (features (quote (("default"))))))

