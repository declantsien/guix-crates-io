(define-module (crates-io ha ys) #:use-module (crates-io))

(define-public crate-haystack-0.0.0 (crate (name "haystack") (vers "0.0.0") (hash "13pldqnq95ci5sd3b9m49mbq3949kz56ch61qlx4mg1bn050yp11") (yanked #t)))

(define-public crate-haystack-0.0.1 (crate (name "haystack") (vers "0.0.1") (hash "1bqpc0v9pfykpv5hls99c5rs6zvyaakng8li3k3simsh5rcd5iig") (yanked #t)))

