(define-module (crates-io ha nc) #:use-module (crates-io))

(define-public crate-hancock-0.1 (crate (name "hancock") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "18x2f6frlxc9a5n7qjszb6c7dxhxfnzpaydnv7yc7dy18v2kbbz5")))

(define-public crate-hancock-0.1 (crate (name "hancock") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0wn183wb13gv3ii3sk6hfss4yvrs8q41lz7sm6i15bi4kgqqrckc")))

(define-public crate-hancock-0.2 (crate (name "hancock") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "sfv") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "1xnc12pzkvvdz1val7zab4zxll47ayj58hb6sggp50j7vj6wbrdb")))

