(define-module (crates-io ha mb) #:use-module (crates-io))

(define-public crate-hambands-0.1 (crate (name "hambands") (vers "0.1.0") (hash "1fviczczqp12hzd099sb6y0c8nss86y39lpjx5fvala6cf8dhjjv")))

(define-public crate-hambands-0.1 (crate (name "hambands") (vers "0.1.1") (hash "087smdy82nks31hhch9xpb88jjda57ns0fa9qplxv8jw7hhmqs5p")))

(define-public crate-hambands-0.1 (crate (name "hambands") (vers "0.1.2") (hash "07k973vfhs0vg6vbr8qzgyf3iyv44nsg99rnc3rp5l7jq9a03gzg")))

(define-public crate-hambands-1 (crate (name "hambands") (vers "1.0.0") (hash "0d6islvn5w2h2jn53r5w0pqyjmq7jpkvlwxcq6v3czfdlx2shhld")))

