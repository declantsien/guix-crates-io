(define-module (crates-io ha mc) #:use-module (crates-io))

(define-public crate-hamcall-0.1 (crate (name "hamcall") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.30.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1kz45kkb6z4cw0jbi7rnzsja4j390linp53h5qvxnf3nss1iq5ag")))

(define-public crate-hamcall-0.2 (crate (name "hamcall") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0wdmy6mjjh42974119xfv3h7b8d1146g28i9arrfn67wpa8p03qn")))

(define-public crate-hamcall-0.2 (crate (name "hamcall") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1zrh49rzjnjqvp8fc5aqaa2ja4abxgm4wljyrbrsdf326fwfv72q")))

(define-public crate-hamcrest-0.1 (crate (name "hamcrest") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "1aq4dq2d6xck7cfy8dh2yj3pxmibh7qbap4f0457d25r16s81h97")))

(define-public crate-hamcrest-0.1 (crate (name "hamcrest") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.77") (default-features #t) (kind 0)))) (hash "1m49rf7bnkx0qxja56slrjh44zi4z5bjz5x4pblqjw265828y25z")))

(define-public crate-hamcrest-0.1 (crate (name "hamcrest") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.77") (default-features #t) (kind 0)))) (hash "0yjkj21xnyskhph937zvlsbf2k2c9226pjn637k8qxvp339hglik")))

(define-public crate-hamcrest-0.1 (crate (name "hamcrest") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.77") (default-features #t) (kind 0)))) (hash "0lrvh63gpfaqjbarl5m6rhjfgjzkrb4zbqiy64vqjz4437sygnd6")))

(define-public crate-hamcrest-0.1 (crate (name "hamcrest") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0manrp74hbjvmz4x8hmpnzq5mhfpx86jc9kn15rcbvcx681pawgq")))

(define-public crate-hamcrest-0.1 (crate (name "hamcrest") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1xnpdvha6wigr7gbsr6p36lx62ampiwyjp5m9rq0p9fmdzdnc3d7")))

(define-public crate-hamcrest2-0.1 (crate (name "hamcrest2") (vers "0.1.6") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "0lnkdqmwd4ca88fw9fcvyr66mj2scyiiav6fcs6jp9bk6z4bbk3r")))

(define-public crate-hamcrest2-0.2 (crate (name "hamcrest2") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "1s8dc5qfvk7apapfm4n0mbpklkg2zpg02kgvwaq8mbb319zqr30y")))

(define-public crate-hamcrest2-0.2 (crate (name "hamcrest2") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "0vkzdpwy3qcv5p2b4jzzr6gxh687xsm0i0bhahvsls3iyz27zg28")))

(define-public crate-hamcrest2-0.2 (crate (name "hamcrest2") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "19iv59wfqk8fr1jki7ik76xm9bvkdirby8vp77hk1sswkf2jpyr3")))

(define-public crate-hamcrest2-0.2 (crate (name "hamcrest2") (vers "0.2.3") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "1zvx22h5sh7m0ircnla3z0ajj8xxjipdzhh2vwcm4vxmszp7yszv")))

(define-public crate-hamcrest2-0.2 (crate (name "hamcrest2") (vers "0.2.4") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "0ldlaisy22syf13zaj2nrd1wc3a6vjdrpgyhzap9li1v59n4xhps")))

(define-public crate-hamcrest2-0.2 (crate (name "hamcrest2") (vers "0.2.5") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "0yq7vjhnm8p6y1ynx409f6a0amhlmraa4ank5cyclvhf98dyskj7")))

(define-public crate-hamcrest2-0.2 (crate (name "hamcrest2") (vers "0.2.6") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*") (default-features #t) (kind 0)))) (hash "1v8g6w2b1mgf7y6rl9cq2f4js1pl2jdynb5jamrkwlbbj9xh16qy")))

(define-public crate-hamcrest2-0.3 (crate (name "hamcrest2") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0x8hx7jyzz2bl0wf6nir62imd26yhp6qcr7zf76cjpg05p33gy29")))

