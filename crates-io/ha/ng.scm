(define-module (crates-io ha ng) #:use-module (crates-io))

(define-public crate-hangar-0.0.1 (crate (name "hangar") (vers "0.0.1") (deps (list (crate-dep (name "cargo-shim") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.30") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.11") (default-features #t) (kind 0)))) (hash "1i8rq5dcjwac00ni41kczm93m89clnl82744lnz83gh6d0niizlf")))

(define-public crate-hangar-0.0.2 (crate (name "hangar") (vers "0.0.2") (deps (list (crate-dep (name "cargo-shim") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.30") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.11") (default-features #t) (kind 0)))) (hash "14niihnhsinzd4gkqc0q287f1qgpfqakdsfkcc432vdqv63byxg9")))

(define-public crate-hangar-0.0.3 (crate (name "hangar") (vers "0.0.3") (deps (list (crate-dep (name "cargo-shim") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.30") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.11") (default-features #t) (kind 0)))) (hash "1wj1q7dsf2n2v4wrcz03wrgdsgkr8d10bvbaxp6cm0c9x3k7k8nf")))

(define-public crate-hangar-0.0.4 (crate (name "hangar") (vers "0.0.4") (deps (list (crate-dep (name "cargo-shim") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.30") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.11") (default-features #t) (kind 0)))) (hash "0m4bap2kn33hkamz4d4jj01gf1vw2gflmdq18av5phnqbj8rxvni")))

(define-public crate-hangdev-0.1 (crate (name "hangdev") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0xb538sw6gwi5yik2jk6fx5ljrxlnfi21zpzwd9h32i4c92frw6n")))

(define-public crate-hanger-0.1 (crate (name "hanger") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("parsing" "full"))) (default-features #t) (kind 0)))) (hash "04pz4pj5wc52zyslgzzbdhv1227bp4sqzwy7mvxm3w9qrcv0piwy")))

(define-public crate-hangeul-0.1 (crate (name "hangeul") (vers "0.1.0") (hash "0aygd7whvl08k8izwrb59na0qbp1381939m4ggwwq36vfmd8r9m9")))

(define-public crate-hangeul-0.1 (crate (name "hangeul") (vers "0.1.1") (hash "1dd5zx58hcjrj3804zrgmj7invwipaxy4g1cc09fm8g1jca16l7b") (yanked #t)))

(define-public crate-hangeul-0.1 (crate (name "hangeul") (vers "0.1.2") (hash "0av1ycljvhrh32aafmqis5cwc8qwd0486m4dww9xd56xl9b3xaga")))

(define-public crate-hangeul-0.1 (crate (name "hangeul") (vers "0.1.3") (hash "1b9bg3hcn9hbvifhi7xf7fy258ljfin33qjj0fxx93q7h8ym6gln")))

(define-public crate-hangeul-0.2 (crate (name "hangeul") (vers "0.2.0") (hash "1hwm2a3q1jjkw2c28b5rznp84my4ixpf8db9w73k4hd3k4hdwg6i")))

(define-public crate-hangeul-0.3 (crate (name "hangeul") (vers "0.3.0") (hash "1j3q451r6ipvjidy5mgb1qdnb360m3lkbplfp3p37fl1gv5aaxhf")))

(define-public crate-hangeul-0.4 (crate (name "hangeul") (vers "0.4.0") (hash "0mz8rmi73c9zkn34p4cysvc1myamhsigbk3h01chskppwl8cw8r7")))

(define-public crate-hangeul-id-0.1 (crate (name "hangeul-id") (vers "0.1.0") (hash "1bvbmmas8xpxfbir31la7hi2z8lmhslxz0cqnnyilbppw7h8rm23")))

(define-public crate-hangman-1 (crate (name "hangman") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1x1lh62nc5zcx107hr8h4jxq6pbq1syvbpzq02159s1hv77898hs")))

(define-public crate-hangman_first_crate_budzix_dev-0.1 (crate (name "hangman_first_crate_budzix_dev") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1dgrfbb3fkiv3h03x3gm8hmc6kc38yxvw5nfp83x5lwbc7zhiz84")))

(define-public crate-hangman_solver-0.0.1 (crate (name "hangman_solver") (vers "0.0.1") (deps (list (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "memoise") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1s1fd3lbjvq9zz5bcsn2fgs710zndc390zkphs5sbj4yp133xjna")))

(define-public crate-hangman_solver-0.0.2 (crate (name "hangman_solver") (vers "0.0.2") (deps (list (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "memoise") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0ndrdpzvcbnn0i3vz09finvbq6na4alh0ihr0wq5bb37kaalazcg")))

(define-public crate-hangman_solver-0.0.4 (crate (name "hangman_solver") (vers "0.0.4") (deps (list (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "memoise") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1wbp38q0xw68msc6crdcfrzv6wybqnkn4wsd8k4j2ig5b4vd63z4")))

(define-public crate-hangman_solver-0.1 (crate (name "hangman_solver") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module" "abi3" "abi3-py310"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "10z81cbi49qvgvahfiryqxybgdyza1g3jipah0vi46c9bsfrxjml") (features (quote (("default"))))))

(define-public crate-hangman_solver-0.1 (crate (name "hangman_solver") (vers "0.1.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module" "abi3" "abi3-py310"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1m8ws994wyvg8i4k4a7rj2ks3wv01b8xm25lqg93hj9da2in0vwk") (features (quote (("default"))))))

(define-public crate-hangman_solver-0.1 (crate (name "hangman_solver") (vers "0.1.2") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module" "abi3" "abi3-py310"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0zkd08k48g59876mx8y3x3ch972sb5mjcmbbf8v6kv615s9c7fbp") (features (quote (("default"))))))

(define-public crate-hangman_solver-0.1 (crate (name "hangman_solver") (vers "0.1.3") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module" "abi3" "abi3-py310"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "12wk4zf7mnsk7kdwsxqg18a9hbb215vfk5zqia11gm19ralkamav") (features (quote (("default"))))))

(define-public crate-hangman_solver-0.1 (crate (name "hangman_solver") (vers "0.1.4") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module" "abi3" "abi3-py310"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0kgj2cyhxi58l484c01zrkh4m4ppcwmj2nv7hy42gijnjxkz9aym") (features (quote (("default"))))))

(define-public crate-hangman_solver-0.2 (crate (name "hangman_solver") (vers "0.2.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module" "abi3" "abi3-py310"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1in4lzmhmfyc81nrq9hir0xa277cbld9346i9ri826mk3zcxkbf5") (features (quote (("default"))))))

(define-public crate-hangman_solver-0.2 (crate (name "hangman_solver") (vers "0.2.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module" "abi3" "abi3-py310"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "08qr13g52a74yyz63nys0x4ghifs2z0sy43n9sf70lfd11rx6g02") (features (quote (("default" "terminal_size"))))))

(define-public crate-hangman_solver-0.3 (crate (name "hangman_solver") (vers "0.3.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "counter") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.13.0") (default-features #t) (kind 1)) (crate-dep (name "js-sys") (req "^0.3.69") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.2") (features (quote ("extension-module" "abi3" "abi3-py312"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 1)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (optional #t) (default-features #t) (kind 0)))) (hash "0snhc1bykrv7yz2x6yfn0czjfac135kx8qf8h4qk054lknyhqqj0") (features (quote (("default" "terminal_size")))) (v 2) (features2 (quote (("wasm-bindgen" "dep:wasm-bindgen" "dep:js-sys"))))))

(define-public crate-hangmanrs-0.1 (crate (name "hangmanrs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1gc6d1214qfhrsgwrccb87x4kb4v4gw5lmhifbbxlzk1sx2hxwb1")))

(define-public crate-hangmanrs-0.1 (crate (name "hangmanrs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "11h277zhbyklm9bcpcgs834rbchgwnvqpr6bk7p1vhji3x9gklw6")))

(define-public crate-hangmanrs-0.1 (crate (name "hangmanrs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0dvifcccipdps50ph87dzm1n5adi4n20v8y0gpd56z5igxd930pv")))

(define-public crate-hangmanrs-0.1 (crate (name "hangmanrs") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0p057xkipnmj5zcv91080y2jg4xryda19d06na4myamjw729142l")))

(define-public crate-hangmanrs-0.1 (crate (name "hangmanrs") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "167fh12ls4siv8gavx3pxwxa7rk26369jarq8fq94ml0g595313j")))

(define-public crate-hangouts-rs-0.1 (crate (name "hangouts-rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "01396cclpzwydpfn2drc7rwxhp5cij0y13hh6hwqsl0lnsf25dj8") (features (quote (("serde-impl" "serde" "chrono/serde") ("raw") ("default" "raw"))))))

(define-public crate-hangul-0.1 (crate (name "hangul") (vers "0.1.0") (hash "1d6k8nha3d4gd2lvf1inblwi1j7v8gwd5439z8m8q48xzhs852ni")))

(define-public crate-hangul-0.1 (crate (name "hangul") (vers "0.1.1") (hash "0a7m3jxmcg91sz0k37drhh3bmi513l9wpz0h0845yjj14yjxhhyj")))

(define-public crate-hangul-0.1 (crate (name "hangul") (vers "0.1.2") (hash "0xw1ky0azihap2bcvxc3qahb7v7hc76yslc3c9hmp14jadn0knlw")))

(define-public crate-hangul-0.1 (crate (name "hangul") (vers "0.1.3") (hash "1bdf14sl8cr4jcl8par59d830ljag6kbgbp8647xjwgfvadnh7qj")))

