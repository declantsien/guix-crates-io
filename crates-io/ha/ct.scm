(define-module (crates-io ha ct) #:use-module (crates-io))

(define-public crate-hactool-sys-0.1 (crate (name "hactool-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "05qpc45hrl1r1nysjbdywhjqknpj1x1rhypak06pxflbjlp4nwr4")))

(define-public crate-hactool-sys-0.2 (crate (name "hactool-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0k4yd5ipg3vfkihw4msls66jyp1drf3rgf7274dqxiah4cmrklz4")))

(define-public crate-hactool-sys-0.3 (crate (name "hactool-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qzpmrb50qhh32xvm3nassvyf3b2ml82d8qqd5i33i8hz4ip7hrg")))

(define-public crate-hactool-sys-0.3 (crate (name "hactool-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b2rgmvcwi7fv15429rq1426j8kxw3dzy68hd1268ir4lcsfn6if")))

(define-public crate-hactool-sys-0.3 (crate (name "hactool-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dbasibfa0p1sl6rxzzwhq7k4fwm6q66708wlw5siwqgjdk96p2g")))

(define-public crate-hactool-sys-0.4 (crate (name "hactool-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0amgw6bzkc8bn7x5ybkvrg48cp1hkiyhi4506mqqd282wradrps6")))

(define-public crate-hactool-sys-0.4 (crate (name "hactool-sys") (vers "0.4.1") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mk72a0gn9ba2791x092hh32nycpw2wi5nqy509scvb27k9jpvhk")))

(define-public crate-hactool-sys-0.4 (crate (name "hactool-sys") (vers "0.4.2") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12bdpv7mm6jgk1zfl0bbh38n5fb4lv50j1k9zsfp1wvc5is2w8zm")))

(define-public crate-hactool-sys-0.4 (crate (name "hactool-sys") (vers "0.4.3") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hna4xcwqlqg2c0473hg9avw9rkp41kapwf151j72v423jkgf313")))

(define-public crate-hactool-sys-0.4 (crate (name "hactool-sys") (vers "0.4.4") (deps (list (crate-dep (name "bindgen") (req "^0.48.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f7fni7z0161p9sfq1ijf3zghmk64pjxqjmgrzhjgvmwhvgylhwf")))

