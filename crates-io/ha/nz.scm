(define-module (crates-io ha nz) #:use-module (crates-io))

(define-public crate-hanzi-0.1 (crate (name "hanzi") (vers "0.1.0") (hash "1gi4ya750vcrr65r60ym5qd0q56r6y69q1xng1i9wfxkvi0lv855")))

(define-public crate-hanzi4096-0.1 (crate (name "hanzi4096") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "1kj7j3ykfpywlbyc39mk6qsw3v5xzi28apk0sqpg80vy4ikjxfik") (yanked #t)))

(define-public crate-hanzzok-0.1 (crate (name "hanzzok") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "libhanzzok") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17hmkvgylaqbfdhn7a89lw6p5qpzvgxmvrx28rsvl2dl861sb9xh") (rust-version "1.56.0")))

