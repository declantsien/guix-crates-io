(define-module (crates-io ha nk) #:use-module (crates-io))

(define-public crate-hankblminigrep-0.1 (crate (name "hankblminigrep") (vers "0.1.0") (hash "182kyfp7icn5vf7rbh12zqgvka916h3mv0vlz7w9xi6g5s69zgng")))

(define-public crate-hankblminigrep-0.1 (crate (name "hankblminigrep") (vers "0.1.1") (hash "1zmb5d651hi1p6izg4l49qqb0pbl67c9k8a839i1dnjmvsp3qnaz")))

(define-public crate-hanko-0.0.0 (crate (name "hanko") (vers "0.0.0") (hash "1inbnk0na5jdyccdx6schmw22w668xf7qdhaa0j0d03pnis8si8j")))

(define-public crate-hanky-0.0.1 (crate (name "hanky") (vers "0.0.1") (hash "17xd7hhc66drycwk21szl8iwz9lq3gzrmrfs4lyzmx5pn8wrwl61")))

