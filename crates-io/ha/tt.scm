(define-module (crates-io ha tt) #:use-module (crates-io))

(define-public crate-hatter-0.0.1 (crate (name "hatter") (vers "0.0.1") (hash "0lgp05n9i9fw1v0vdq3nv0qpzk93w09l12jr8yrpjqs0k586hif8")))

(define-public crate-hatter-0.0.2 (crate (name "hatter") (vers "0.0.2") (deps (list (crate-dep (name "rustyline") (req "^6.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0i8p6z0ha12alpl315n9k45b3lvch70d8ly0gmsxylczska2vj4g") (features (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.0.3 (crate (name "hatter") (vers "0.0.3") (deps (list (crate-dep (name "rustyline") (req "^6.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "14a1giak5h00kcvkpx4rw0sgcp78snkl03fdmgxbl2i79kyaskcy") (features (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1 (crate (name "hatter") (vers "0.1.0") (deps (list (crate-dep (name "rustyline") (req "^6.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1c95b88alsg05ikg5wxyx8fn14llpsj9xid8rnqk5vhjarrnn8cw") (features (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1 (crate (name "hatter") (vers "0.1.1") (deps (list (crate-dep (name "rustyline") (req "^6.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0hg6xs2rkp8f1k424yxchqqk8cpplwisp15d8ibkddwb1bf4chsp") (features (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1 (crate (name "hatter") (vers "0.1.2") (deps (list (crate-dep (name "rustyline") (req "^6.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "05glaqyq37zq0j5bzh6wkh6cmm3f8x2b01wb2mprl1pxd04qqagf") (features (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1 (crate (name "hatter") (vers "0.1.3") (deps (list (crate-dep (name "rustyline") (req "^6.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "12hf4xx0601j6bbqw9zhn6b06316ljrpkmhz8z9i9f3sqqqhlzkx") (features (quote (("repl" "rustyline"))))))

(define-public crate-hatter-0.1 (crate (name "hatter") (vers "0.1.4") (deps (list (crate-dep (name "rustyline") (req "^6.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wiv8kir9d656a2fyy60n0vyjrbkwcw7bh6sbc5575igprczhacc") (features (quote (("repl" "rustyline"))))))

(define-public crate-hatto-0.1 (crate (name "hatto") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16.5") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "spdx-rs") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0nc6sz60yvhwwscnnzcq1bv8pgsr0fmk14yjvk3kfkarijsf8c4j")))

(define-public crate-hatty-0.1 (crate (name "hatty") (vers "0.1.0") (deps (list (crate-dep (name "macaddr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "04i4f4lz46i235kxqyjxk0x9f1kpjrhn086kgxqvpwjiw9y1mkg7")))

(define-public crate-hatty-1 (crate (name "hatty") (vers "1.0.0") (deps (list (crate-dep (name "macaddr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1g0ancyl55anpf79f3yb0x07s6wd3l98lppqf09lm9dqbchxa1n0")))

(define-public crate-hatty-1 (crate (name "hatty") (vers "1.0.1") (deps (list (crate-dep (name "macaddr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "10zgara799nbldrs9jy8m45ab4y8qm9wrlcri494k3qbf5kpz1rq")))

