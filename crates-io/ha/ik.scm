(define-module (crates-io ha ik) #:use-module (crates-io))

(define-public crate-haiku-0.1 (crate (name "haiku") (vers "0.1.0") (hash "0lrrphzkqrwlhc7i54c4f9ys51ghy1bddkqqiz9k69q72j14b4zn")))

(define-public crate-haiku-0.1 (crate (name "haiku") (vers "0.1.1") (deps (list (crate-dep (name "haiku-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.6") (default-features #t) (kind 2)))) (hash "0dnndii50y5a9ywk52df2n83yrxjc54ihyxa4r4ywhsvf0jdlkvp")))

(define-public crate-haiku-0.2 (crate (name "haiku") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "haiku-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "1ks4l2r22mbjafp9wqcs63nahckdlk7iky5pv2kgy070gmhlbm56")))

(define-public crate-haiku-generator-0.1 (crate (name "haiku-generator") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0pyr5q3lhkpycjw91nfwr8rfp8hybr7k7hq40bxskd859rf44fz9")))

(define-public crate-haiku-sys-0.1 (crate (name "haiku-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)))) (hash "1z5lx93ag86xw5l7br3pm21d1k2kypgyyfv9q6rc9djw7hgrwhvg")))

(define-public crate-haiku-sys-0.2 (crate (name "haiku-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "1shdychk07naijjaxaaiyj2985iy1qq3bhchp0wgv440rvbirx12")))

(define-public crate-haikunator-0.1 (crate (name "haikunator") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "07wj74m5xrdcfyv2pd3bxwlbix7ivw8glzxm26h43gf6zjybmkr1") (features (quote (("nightly"))))))

(define-public crate-haikunator-0.1 (crate (name "haikunator") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "11cfjxp5nrvkj94vpb6jnhhj0h9mrjfniz89zby1p2pxw2bsp89i") (features (quote (("nightly"))))))

(define-public crate-haikunator-0.1 (crate (name "haikunator") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "0bxz0rm3y8hms21rdgngvz3hkbsw08h8mb2pcanqx8lj0p3rjvjh") (features (quote (("nightly"))))))

