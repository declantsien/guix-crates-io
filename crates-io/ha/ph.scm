(define-module (crates-io ha ph) #:use-module (crates-io))

(define-public crate-haph-0.0.0 (crate (name "haph") (vers "0.0.0") (hash "0cqmphdhipvxnsbrybbfxpbgwrrvzc3lyya6dqv8qkw8zspfl5cw")))

(define-public crate-haphazard-0.0.0 (crate (name "haphazard") (vers "0.0.0") (hash "1v0d7qgr1z56v1l9zvsic8nhfs6nz74pf3kbwsywdd75nab583xr")))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "16b7mc03d6gabsrji3z134x1pnich2f3gw4klqx7yaxsc48crfwl") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1mdnsdx0ig3qgr984w2bhgfqrs99cnkm2wavxq1a0nv7kdbqfkbb") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1k8m82ayn0qnkai6r43d67l8v9vm52cnm8lr3yszndfkwqzykci4") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "0r8yfgznzq0zig99g523rq2z67zph7jc5piav615sp9k8qmkk786") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1nsnf7v17adp7a45zjr4yjk4ig7xbwy5gbd5q1n421ccncwrgd89") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1abk5s8x3qxyffxz28wxl6239dk81hignhsw8gsknidi9dc7d1gp") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1zfgvb6db2pvc7cw73icwqw00yqkrs82qfzr4rd6707pbhrgb5lb") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "0vqhybsrqcgmxny9vxf9wqxb8ka0ikr395wn8nnr6zv0l028ysnp") (features (quote (("std") ("default" "std"))))))

(define-public crate-haphazard-0.1 (crate (name "haphazard") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.7.1") (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "regex") (req "^1.6") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)))) (hash "0djwq3sz26vh671jzj0m7xadnzv10fkda20bmkhw38dal1kg9jzf") (features (quote (("std") ("default" "std")))) (rust-version "1.60.0")))

