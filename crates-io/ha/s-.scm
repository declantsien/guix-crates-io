(define-module (crates-io ha s-) #:use-module (crates-io))

(define-public crate-has-env-flag-0.1 (crate (name "has-env-flag") (vers "0.1.0") (hash "1zxgzr6ab4aj7rb9i3aknljgl5b08ivi991bhv5amasb9ag690c2")))

(define-public crate-has-env-flag-0.1 (crate (name "has-env-flag") (vers "0.1.1") (hash "01fbgvhm7jvxi23pw89im48ccx64d0n5sr6x0ljzfvdjx7wwx9vd")))

(define-public crate-has-env-flag-0.1 (crate (name "has-env-flag") (vers "0.1.2") (hash "19fx49mar3fwd1lcv8q8rw74sjf4knjhcfw4g9asdpkf4awsvs9d")))

