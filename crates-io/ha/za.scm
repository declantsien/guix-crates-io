(define-module (crates-io ha za) #:use-module (crates-io))

(define-public crate-hazard-0.1 (crate (name "hazard") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "1dnn19fz4fg8k6l9ncxf1q7hn8gvwb3g7wabw1xbm7d4an0fysf6")))

(define-public crate-hazard-0.2 (crate (name "hazard") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "12fs5fmqy7hhpimmzmbqnw2q9fz0bbmhj4b4ja0rwgyy4q12fl1v")))

(define-public crate-hazard-0.3 (crate (name "hazard") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "0xxshp28ah20sknsi06x7m3kwpjg4rmb7hfgbj1849c5l66k1c0x")))

(define-public crate-hazard-0.3 (crate (name "hazard") (vers "0.3.1") (hash "1w82p263ywrfmsias0ydnncj08fg1vjbiikcdnql9nfcn93imqhd")))

