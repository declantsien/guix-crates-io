(define-module (crates-io ha mt) #:use-module (crates-io))

(define-public crate-hamt-0.1 (crate (name "hamt") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)))) (hash "0ph3z95dsykpbpg4alsj1d8ql8g58jlq2vkbz8w2b7np2gy5g9ay")))

(define-public crate-hamt-0.1 (crate (name "hamt") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)))) (hash "0jysy28ghxg4q5zy4fddgyagcznyddryf7hkxz1s052qv4cbb1rw")))

(define-public crate-hamt-0.1 (crate (name "hamt") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)))) (hash "07halnj5s1m27ipck1cyrk7xm2g6wjrf4qsi44bx37pg3qb0fp4k")))

(define-public crate-hamt-0.1 (crate (name "hamt") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)))) (hash "137w7sld1jc6j3mlm8d8hip0238m4nyhn7hqj251f7cs20r0833g")))

(define-public crate-hamt-0.1 (crate (name "hamt") (vers "0.1.4") (deps (list (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)))) (hash "1i2rxri7xaxb2m0k8d718csw9bfix8c8y20d7wsrf9s180y8vm6a")))

(define-public crate-hamt-0.1 (crate (name "hamt") (vers "0.1.6") (deps (list (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)))) (hash "0rq3cxpy2lzg8v4vq9xr5z4jzh9cwshrrrz8ah9288n0z7dfx5fv")))

(define-public crate-hamt-0.1 (crate (name "hamt") (vers "0.1.7") (deps (list (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)))) (hash "0qwz4iy08lla1i1m92fphi18nlrdg9mxbl06zn0bllrbh6ap025y")))

(define-public crate-hamt-0.2 (crate (name "hamt") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0l6b45zayrl6ds3ry4hzwnskdwpa29frypkgzvwyyjms0jacyj53") (features (quote (("default"))))))

(define-public crate-hamt-rs-0.2 (crate (name "hamt-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "025hpynf5yicc7xdsv9b9mp3lzlycfkznfdzjzcjasvbbhfnlb3i") (features (quote (("hashmap_default_hasher") ("default"))))))

(define-public crate-hamt-rs-0.2 (crate (name "hamt-rs") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "1303sax6f46bqkbvdvpiai0kfg41381z4rsfdacka7277zrfvvka") (features (quote (("hashmap_default_hasher") ("default"))))))

(define-public crate-hamt-rs-0.2 (crate (name "hamt-rs") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0q0wyylwinmhqzdn4r4wm3dsgzdjpib5vd606a4pkvpz0y3y78yz") (features (quote (("rust_alloc") ("hashmap_default_hasher"))))))

(define-public crate-hamt-rs-0.2 (crate (name "hamt-rs") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0yby45rz9k1p3hjcg8a2rsmg84m19ib1vs3rscfz1xqp7yxm8kz1") (features (quote (("rust_alloc") ("hashmap_default_hasher"))))))

(define-public crate-hamt-rs-0.3 (crate (name "hamt-rs") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "1ran344p39lz0blg4ghjw24i67zgiymdqics313mafmfsydgb8v0")))

(define-public crate-hamt-sync-0.1 (crate (name "hamt-sync") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "046ilmvlw25xvf6cjgvbxkvw5qz83djx8hn6sjwd6z19ps0qsczw")))

(define-public crate-hamt-sync-0.1 (crate (name "hamt-sync") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1g500bb6hygh4qa43apqsjaxiryw7n4acvkl5fwp3b2j2hdg3bhb")))

(define-public crate-hamt-sync-0.1 (crate (name "hamt-sync") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h4xzrpy9l63mbi00dxs8gv9xihpq7p2r0cza4ax4bw8jnhrqi5d")))

(define-public crate-hamt-sync-0.2 (crate (name "hamt-sync") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "17qy5n5ki9ni0vada6rvh3gc31s47qacqf765s9i3vaz4wc3zq0x")))

(define-public crate-hamt-sync-0.2 (crate (name "hamt-sync") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0h3gm3cnw7z9dnafd92az9v3wgpw824ijsw5fshr0s036ic0mw1c")))

(define-public crate-hamt-sync-0.2 (crate (name "hamt-sync") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "00fca1b6qmck34vsqifwfw5xhvsm0ab0aaiaz0yv8f9cv9nqc75y")))

(define-public crate-hamt-sync-0.2 (crate (name "hamt-sync") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0vklljmvfsgihrfnc6xqcvz83n2xyf57h75xm20yz8zjn32d2kaw")))

(define-public crate-hamt-sync-0.2 (crate (name "hamt-sync") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1r00233jhldmr1phgj1x4qkjg88027nyg64ji8cp1xl5cqsjb8vv")))

(define-public crate-hamt-sync-0.2 (crate (name "hamt-sync") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0z13ym8gly53h605lclqxkdg0w99why7ihyflglk5wf2inl6syqr")))

