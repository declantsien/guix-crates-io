(define-module (crates-io ha zy) #:use-module (crates-io))

(define-public crate-hazy-0.1 (crate (name "hazy") (vers "0.1.0") (hash "1dpq1jcz68lvj2ng812x9dmw251wgxjsxncas37zadqszpjwqvnc")))

(define-public crate-hazy-0.1 (crate (name "hazy") (vers "0.1.1") (deps (list (crate-dep (name "hazy_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "089fwgw8jcb548bvx28kw84a8xvmhwvsk7m57r1bfpvadnw3r3ha")))

(define-public crate-hazy-transport-0.0.1 (crate (name "hazy-transport") (vers "0.0.1") (deps (list (crate-dep (name "datagram-transport") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0gvr6lmzw1b635gz4f329sw6hnlg26c8mbbd4j53xjj7a2j5fv51")))

(define-public crate-hazy_derive-0.1 (crate (name "hazy_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0wrhq24d71yzbgg9g72vanj1zw895d9wy289a4g5nx6klbb15dyp")))

(define-public crate-hazy_derive-0.1 (crate (name "hazy_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0v9w2l7q0vnif5r9a0il8g0a55zpa6gn45g7wr1v63qql4nn3kry")))

