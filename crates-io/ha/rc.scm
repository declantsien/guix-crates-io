(define-module (crates-io ha rc) #:use-module (crates-io))

(define-public crate-HArcMut-1 (crate (name "HArcMut") (vers "1.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0aplz8f81mvf1g7wrpgydjx5m7dgbajmsfd18dq7dzxciq5v7ssc")))

(define-public crate-HArcMut-1 (crate (name "HArcMut") (vers "1.0.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "181cdnryxxn53wmmi53viv073di1fy4i42zyh8rdij5i6dgd8wd1")))

(define-public crate-HArcMut-1 (crate (name "HArcMut") (vers "1.0.2") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1lxjwr1ixdys3r5c53axw2qsl3hi1aklwrd9y46d3xqpqhxyba2b")))

(define-public crate-HArcMut-1 (crate (name "HArcMut") (vers "1.1.0") (deps (list (crate-dep (name "arc-swap") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1lk4rjznka1xd140mg7vdhmlck99dgd2gx5hscf0v41rq8r503nr")))

(define-public crate-HArcMut-1 (crate (name "HArcMut") (vers "1.1.1") (deps (list (crate-dep (name "arc-swap") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.2") (default-features #t) (kind 0)))) (hash "0n5qr2q77r91wj131m52zaw8fp4xgwnq4wwvqyyrfxnlxw6315gp")))

