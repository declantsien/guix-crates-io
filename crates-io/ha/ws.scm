(define-module (crates-io ha ws) #:use-module (crates-io))

(define-public crate-haws-0.1 (crate (name "haws") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1iwdig5g3m46nda22x2089349ph2cnwfgas1jd5fg9v4bk18p5l0") (yanked #t)))

(define-public crate-haws-0.1 (crate (name "haws") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0062k4hw5d828cs4953bc4zdnxnjrir6d5b6k2asy4vjq3pgq3nb") (yanked #t)))

(define-public crate-haws-0.1 (crate (name "haws") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "135r9p9zlfnw237xsqnwgd1ar9ws4rf0ain9gzas5jhsb234w2mg")))

(define-public crate-haws-0.1 (crate (name "haws") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0w433lcbfccvyl9zqjpydh5ssdrwnwf5qr4sh9apdc0h02w7yi6c")))

