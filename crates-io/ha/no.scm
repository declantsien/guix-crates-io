(define-module (crates-io ha no) #:use-module (crates-io))

(define-public crate-hanoi-0.0.0 (crate (name "hanoi") (vers "0.0.0") (hash "1vg5ycyr6dzyxd6p9gwnrdqsp3pbsv9imk5vm7cph1s2yfgwh6iv")))

(define-public crate-hanoi-speedrapp-0.1 (crate (name "hanoi-speedrapp") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.27.2") (features (quote ("persistence"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "pretty-duration") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.26.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04nnlinqdk2sblx3z2s28rn33qi62ik2j0ha8cqkn2dwmpphdpir")))

(define-public crate-hanover-flipdot-0.1 (crate (name "hanover-flipdot") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1y8901f1yzsfg1mzczqgh8hfv1rjpp6h2bngzx9zr8ahxxp50d6c")))

(define-public crate-hanower-0.2 (crate (name "hanower") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0w7j7pfswv40jvc5wckkrh1ps896d40ficz8795mmqg4ci5lr9ig")))

(define-public crate-hanower-0.2 (crate (name "hanower") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "05xgba71mjw92b070ys5528cic5fiycnm0lrc6i5sdfy0gqqzz7g")))

(define-public crate-hanower-0.2 (crate (name "hanower") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vb68r51k0drayyhy4rc924sn9qy9k7lsp32q2hr5p55758yz3bj")))

