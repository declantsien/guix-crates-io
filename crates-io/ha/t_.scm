(define-module (crates-io ha t_) #:use-module (crates-io))

(define-public crate-hat_trie-0.1 (crate (name "hat_trie") (vers "0.1.0") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "0lr0fhbhj2qw7nqpbymhipg2k8hbl845mwmlgi0b08jkxhrnf5vy")))

(define-public crate-hat_trie-0.1 (crate (name "hat_trie") (vers "0.1.1") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "0xp615a8fi77p7wakr34jfb1snx6dvzfw4vygnhk3v6iyxnvc4df")))

(define-public crate-hat_trie-0.1 (crate (name "hat_trie") (vers "0.1.2") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "1zr8shpw4rf8xlk478xjngxicxjsfg13ydqpv6by73jjnjz6ww2f")))

(define-public crate-hat_trie-0.1 (crate (name "hat_trie") (vers "0.1.3") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "1b29y2i5b8b6qrxg316x098zx88bq6mw4g9fbcbg9zhcnja5z9wz")))

(define-public crate-hat_trie-0.1 (crate (name "hat_trie") (vers "0.1.4") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "0c5rkx6l28286mybh5pr7vjp4zzk0j044wsrpn0ancbwzxyi0cr7")))

(define-public crate-hat_trie-0.1 (crate (name "hat_trie") (vers "0.1.5") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "0hghy5an3mfadn0gfr841z7nbr5802mjmckmj5jhdis1mz7swwdy")))

(define-public crate-hat_trie-0.2 (crate (name "hat_trie") (vers "0.2.0") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "1xvkji84ny9b3mbfv1kmaybc9fw70yadb0kavwyf34kgj68cscy8")))

(define-public crate-hat_trie-0.2 (crate (name "hat_trie") (vers "0.2.1") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "0zrsgp0gi6fwplf4p1fmcr198p1iqyf79iz9w9f0wcwqky4k2w1w")))

(define-public crate-hat_trie-0.2 (crate (name "hat_trie") (vers "0.2.2") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "1j43iynlpnz9v9i7k4dzmk7wh02jchjhf009czp9v5mxfbkm7376")))

(define-public crate-hat_trie-0.2 (crate (name "hat_trie") (vers "0.2.3") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "1gv1gjp4pad1fmsl7gk4rqjzy2s62ld0ibp653zhkncsdv2hd0zn")))

(define-public crate-hat_trie-0.2 (crate (name "hat_trie") (vers "0.2.4") (deps (list (crate-dep (name "ahtable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.5") (default-features #t) (kind 0)))) (hash "02m7km41lf1wpvy0lpvc4mk2zyz08hnp3zf00fkz0fyxi33022kz")))

