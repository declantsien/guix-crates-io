(define-module (crates-io ha op) #:use-module (crates-io))

(define-public crate-haopterus-0.0.1 (crate (name "haopterus") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1kxrg66kdpslrzyp00ijgrpgxvda606cngn8nj13vc4029iw6b37")))

(define-public crate-haopterus-0.0.2 (crate (name "haopterus") (vers "0.0.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "134d2lr44l1v86ziv4rlz18p6r24acsxql3l2abz393g74qifp2n")))

