(define-module (crates-io ha se) #:use-module (crates-io))

(define-public crate-haseo-0.1 (crate (name "haseo") (vers "0.1.0") (deps (list (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1i6x6dnibj0sv5gdmvs3y2zzcd5h358s8mp5c81807m9rlhbyafd")))

(define-public crate-haseo-0.1 (crate (name "haseo") (vers "0.1.1") (deps (list (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0c2p978sv7b4hbx9gprk0pjfkvhfkgss03iwjqc0mszhmkxyhj3m")))

(define-public crate-haseo-0.1 (crate (name "haseo") (vers "0.1.3") (deps (list (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "036aq1aw1ncbsmbaiyhbix82b2dqvys8kaxffldgy2vbzbg55jw0")))

(define-public crate-haseo-0.1 (crate (name "haseo") (vers "0.1.5") (deps (list (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zd90wxwq1c25gnfhp9v8ly9qmd4qi2r5h9dwx45q3v1vllv7gcp")))

(define-public crate-hasetsu-0.1 (crate (name "hasetsu") (vers "0.1.0") (hash "1yrfvvvlrpk9961swm2kcwzcvvvb31nigc4s8xm883s8gwrp313a")))

