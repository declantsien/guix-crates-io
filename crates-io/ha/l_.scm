(define-module (crates-io ha l_) #:use-module (crates-io))

(define-public crate-hal_sensor_dht-0.1 (crate (name "hal_sensor_dht") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0b88m41sw6w2sdinkyrp7fhg4439vysjbdj8jqrbv7y4a9ns8kdb") (features (quote (("floats"))))))

