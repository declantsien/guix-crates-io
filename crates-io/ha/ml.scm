(define-module (crates-io ha ml) #:use-module (crates-io))

(define-public crate-haml-0.1 (crate (name "haml") (vers "0.1.0") (hash "1dijim5qrg8zy842q20x6awkvwml9fxjy8323liz73rbw2p34fph")))

(define-public crate-haml-0.1 (crate (name "haml") (vers "0.1.1") (hash "0wcasmcvva55dhqnxkkb5dqn5yxz5azr6sda60x1i8438vnhll39")))

(define-public crate-haml-0.1 (crate (name "haml") (vers "0.1.2") (hash "0biwxa0240a08ab2d4a3np2jlp0s3lb0nvyaqjhbw147d5jkax6b")))

(define-public crate-hamlet-0.1 (crate (name "hamlet") (vers "0.1.0") (hash "0sv4ds64yijx3kh1180fjlzs0j9knaii314kk1mvnn7jzxfvczpx")))

(define-public crate-hamlet-0.2 (crate (name "hamlet") (vers "0.2.0") (hash "0qalkimk32s3xdic2h2n8c1qx0rw6mp4lv8mrhil5hgbnyymqkf5")))

(define-public crate-hamlet-0.2 (crate (name "hamlet") (vers "0.2.1") (hash "0jdn817nkynfh8n7zd6qkv8x4dl6zj4ay7qci49hr7dsjg8657zj")))

(define-public crate-hamlib-0.1 (crate (name "hamlib") (vers "0.1.0") (hash "0kwf9gqyvjc6cgr4dw34p3zkydfqwflvrp46sc10bx0drhjyhl8m")))

(define-public crate-hamlrs-0.0.1 (crate (name "hamlrs") (vers "0.0.1") (hash "0399a79z1wd1qxf9ixcd8dhx360i7f5lcj2zc25y5nz5c0zrjawm") (yanked #t)))

(define-public crate-hamlrs-0.1 (crate (name "hamlrs") (vers "0.1.0") (hash "11qjr7c91afid6c85558s06q1hg3f9cqs49vhhw9lx1zdzyi5djh")))

(define-public crate-hamlrs-0.1 (crate (name "hamlrs") (vers "0.1.1") (hash "1kar5gvrm6kxmq82m3lnca4raxzz96p14fdz8s6p1dq52c366vlv")))

(define-public crate-hamlrs-0.1 (crate (name "hamlrs") (vers "0.1.2") (hash "15pfmdwdd6krpm1xysxzy9i16hfxlk6l4q7f6n5pjb1p6286syg3")))

(define-public crate-hamlrs-0.2 (crate (name "hamlrs") (vers "0.2.0") (hash "1vsj61n2ls9rxydz5kmlkx3j7wf11dmaf6zcrwgn9a9k5q6cdwq0")))

(define-public crate-hamlrs-0.2 (crate (name "hamlrs") (vers "0.2.1") (hash "0lf4xgfzsvgmscsbd4sald9dcayz1j6kqxkskslwsw433vk4hkbv")))

(define-public crate-hamlrs-0.3 (crate (name "hamlrs") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "1ssyx3vr1fy0npfmp7jk0i6xplnzsw504p39khiq103jjilfinp2")))

(define-public crate-hamlrs-0.4 (crate (name "hamlrs") (vers "0.4.0") (hash "1hv4xi5z98wcihb3p9r3q3b31n6839jfcih11l0k5310znphyp12")))

(define-public crate-hamlrs-0.4 (crate (name "hamlrs") (vers "0.4.1") (hash "0nvk21njz6m8yby2dq8n61qzphk8gwwy2q13n12sy9g9zx7knkap")))

(define-public crate-hamlrs-0.4 (crate (name "hamlrs") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "13yw7sky6akm120w8qazhy0v7k2a6n6cq44h5xls8yb74hxp60dw")))

(define-public crate-hamlrs-0.4 (crate (name "hamlrs") (vers "0.4.3") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "138pj1lh8ki54ngpj4jpi3gp5m1yzr42jyli3zl5m12lmg6401jw")))

(define-public crate-hamlrs-cli-0.1 (crate (name "hamlrs-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "hamlrs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0sydmk2qb779rxia9r9b7llc536dphqd1zmghpjp3jya7rb38da3")))

(define-public crate-hamlrs-cli-0.1 (crate (name "hamlrs-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "hamlrs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "14mhn83lk063yiylzkrxg2y3agdw66q0bql17x8pp2m5cw7091xr")))

(define-public crate-hamlrs-cli-0.1 (crate (name "hamlrs-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "hamlrs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0m255kvdix2hnkbc6zkqn06vpwvsdak3ypqcggrxmhgy60l581dm")))

(define-public crate-hamlrs-cli-0.2 (crate (name "hamlrs-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "hamlrs") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1j0z8zqz2bb33yypmvrnrzqdmyd35br6sxy9qwnwx5a1x1mvmgvr")))

(define-public crate-hamlrs-cli-0.4 (crate (name "hamlrs-cli") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "hamlrs") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "16cwrs0s3palcwdbqhvnhqis29c3kpmvbmnz3fxw3kjr8yhi952q")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "1c2cv51wr1z4pfc10h2x6rdx0cs2ys0abigyc3r9zffydnkw8pz7")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "0vkybnq771zldndrqlv9qjdiskrnzwkgh1qzzwwfisw9wkc2zqy7")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "11vgvkkj24b0f7lsa3l858b4658f3x3ddlbcc0d0zyb7q043rh1g")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.3") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "0hb3arvl06vch11wz8f7s282grcvivmbv2kzli9d98gwz9gr62af")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.4") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "1m8jp5vfb2b94c1gd7wrqwj14pcd4plykyikjpwvay02z64zqwmn")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.5") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "1fr65v77v3f2yy07n2a71fvcmcvd3dxk4w0s04knxz5azzplg8my")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.6") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "12l7lhn8ddi6aksffqr17h7lvamlav3fdaxr5jjc7ldz60mr8364")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.7") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "0wqk6j608qigyj2f55klxyk529cpdrqnf0lrx4d58s9cl9sjd3xk")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.8") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "196ppw6l9r8q210misnipdwikz38w1ad6kz8rf0vqqw02q5b5ivi")))

(define-public crate-hamlx-0.1 (crate (name "hamlx") (vers "0.1.10") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rapid-fs") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rapid-utils") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "xml") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "0as3sgi81gwd1zfv5avdk2swzazzr4i1zlfc7dr2rind9a7pqa53")))

