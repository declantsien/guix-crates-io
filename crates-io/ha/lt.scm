(define-module (crates-io ha lt) #:use-module (crates-io))

(define-public crate-halt-0.1 (crate (name "halt") (vers "0.1.0") (hash "11cybflp3q3kc2fh45c5s23q860lql7mrxc2bhpqw180ins5wy6i")))

(define-public crate-halt-0.2 (crate (name "halt") (vers "0.2.0") (hash "0ahhv7q5b932358n4wh10abmbpr1a0bgqbd6g34rdbsis1hlqp9r")))

(define-public crate-halt-0.3 (crate (name "halt") (vers "0.3.0") (hash "0lfqz7bhnz3c4z46f6laj1icxdsyy4w2bbl7vad2l66bwr5vv3y8")))

(define-public crate-halt-0.4 (crate (name "halt") (vers "0.4.0") (hash "14q3v1wx6av2p4izinijqnw13pmd5057rzcvzgffkspflh3z95am")))

(define-public crate-halt-0.5 (crate (name "halt") (vers "0.5.0") (hash "1mxv5q9c3hi0fb3h9kzdcimiyby0w9kcx5v5s2ns0j5qa6fcky8i")))

(define-public crate-halt-0.5 (crate (name "halt") (vers "0.5.1") (hash "14z5bqkrmnl5lxmc9znwrswspzlyxshwk8zalkwajmms8291cs4g")))

(define-public crate-halt-1 (crate (name "halt") (vers "1.0.0") (hash "1fwl0ng7kj8c3vc1fphjrjx55rlg76vnhrq0g6kf3lq9zwwijgc0")))

(define-public crate-halt-2 (crate (name "halt") (vers "2.0.0") (hash "04zh4cxg8ilvllcg3q7kb0kfikxx4smi3dgd1gkrzw2iawk2mgsg")))

(define-public crate-halt-2 (crate (name "halt") (vers "2.0.1") (hash "0kln2r21c5g4bvjjq7iqkgd303bca8n4cmkavl6jr2d4byz8zgib")))

(define-public crate-halt-2 (crate (name "halt") (vers "2.0.2") (hash "1ygddi36b7sh6lrq5sz24fwhmq2anwwvzaiinpvfr3xs95zvwh3y")))

(define-public crate-haltia-0.0.0 (crate (name "haltia") (vers "0.0.0") (hash "00jyri4sgknny8fq6ff3hq5s87gmd68wl51a07p0pbhnfh2lk6q3") (features (quote (("default")))) (rust-version "1.70")))

(define-public crate-haltia-0.0.1 (crate (name "haltia") (vers "0.0.1") (deps (list (crate-dep (name "haltia-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0qlmg7h7dws0k1k0cf7kxbhrzix7cwrv2lwj25wms3vs0c968wvr") (features (quote (("default")))) (rust-version "1.70")))

(define-public crate-haltia-sys-0.0.0 (crate (name "haltia-sys") (vers "0.0.0") (hash "168njd21a3fqyj001pr4if0kv2b0jnlwykfvz293c4l8l23jh99a") (features (quote (("default")))) (rust-version "1.70")))

(define-public crate-haltia-sys-0.0.1 (crate (name "haltia-sys") (vers "0.0.1") (hash "0v92qs2sk1qmknq9ja7rm0hfx5wlw67kwj3bhrhnmkg9a8gb8ryr") (features (quote (("default")))) (rust-version "1.70")))

(define-public crate-halton-0.1 (crate (name "halton") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.1") (default-features #t) (kind 2)))) (hash "1m8gc4zwp9zhx8jcar3v0pqi575kip0shqzahs745kgb12hrcalh")))

(define-public crate-halton-0.2 (crate (name "halton") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)))) (hash "12am07mbjmhyi267i85930ilpaykgs5rdv0nifkclcgkn6c2i7n2")))

(define-public crate-halton-0.2 (crate (name "halton") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bmc33v9iqh3v0a3w51zxjlgwma6abgr1wb853hinnnqwd498qmy")))

