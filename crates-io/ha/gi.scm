(define-module (crates-io ha gi) #:use-module (crates-io))

(define-public crate-hagix-0.0.0 (crate (name "hagix") (vers "0.0.0") (deps (list (crate-dep (name "hagix-core") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "hagix-derive") (req "^0.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "03fnhz091kgps2a3z92y9vm32p8xd8hxxz46d2215fkyv9nhrypk") (v 2) (features2 (quote (("derive" "dep:hagix-derive"))))))

(define-public crate-hagix-core-0.0.0 (crate (name "hagix-core") (vers "0.0.0") (hash "14s8gswnazpl143p4f58a3ssrp01s3zj4g0s5r109bnhdkh9ahwi")))

(define-public crate-hagix-derive-0.0.0 (crate (name "hagix-derive") (vers "0.0.0") (deps (list (crate-dep (name "hagix-core") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "hagix-derive-core") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "06id8fsvfh43gn1zwaklhgjyl8g1p91g88qv7pzwyhkp7z7y698v")))

(define-public crate-hagix-derive-core-0.0.0 (crate (name "hagix-derive-core") (vers "0.0.0") (deps (list (crate-dep (name "hagix-core") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1ph5zh0b8cxspqa2d9i2b0j5g833h147inlqnl8q0vi3d0dn98dl")))

