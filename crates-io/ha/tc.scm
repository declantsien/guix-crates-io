(define-module (crates-io ha tc) #:use-module (crates-io))

(define-public crate-hatch-0.1 (crate (name "hatch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_ssm") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1r551480vr7iccxa1p53631vd7bdk97fv1hwjbgny9hhsa8b33sp") (yanked #t)))

(define-public crate-hatch-0.1 (crate (name "hatch") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_ssm") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0fki4f64ki2drnmv8hmy279iyqdiz9xv717kqg6c2nc0hbq2cv16")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.0") (hash "0mnl65a05hmlyncqfn33kqnhqw7mxk9y646wbk4agcja5l84nx32")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.1") (hash "1mqmkp6skbvdwnci5j3spphl6ixmpmfkvhpsgv2m6mp78krwmnrd")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.2") (hash "0hwrmpsv7hvx0yl5g0vb14c1zvd0zbsqz4pg8wk697xgqdcz3ss8")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.3") (hash "1dfyh6rnwxmbhmldqd7ylipwg37zx783cf0faha20rv87qf0f7n4")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.4") (hash "0hgbddylpgj2nxjb2pamnidbj7nwc0cx69ym8vmylw2hxdi5v84j")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.5") (hash "1mm937hbvp7l6rqklqmjv1vak8q714pk8164bihlmc78mv2gm621")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.6") (hash "11y5g0ljawhjydwh9qqmj84lxf7db81flc8jdk68n6mrdzav0shs")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.7") (hash "06vg9v6z86waspy2m68am3nkkr87ywih128kcj9dgc04lvg0lxar")))

(define-public crate-hatch_result-0.1 (crate (name "hatch_result") (vers "0.1.8") (hash "0v3n12n0nzv4xr7rh1wcvy3cdb4c624hhs9yz05ffjn88pn81jd2")))

(define-public crate-hatchling-0.1 (crate (name "hatchling") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rdf") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "0120bajnckwcksh71wsc6whf1y6ppqdhl38a5ljc325w4l2z9fff")))

