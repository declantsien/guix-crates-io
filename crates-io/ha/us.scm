(define-module (crates-io ha us) #:use-module (crates-io))

(define-public crate-haus-0.0.0 (crate (name "haus") (vers "0.0.0") (hash "0n3qh2zbh1yxs5pxyw9jf0v5rmnwkhac7dyahvwbah0sb4k0d2gg")))

(define-public crate-haus-cli-0.0.0 (crate (name "haus-cli") (vers "0.0.0") (hash "1rbj29wgr6pblf2dphy34sfjaiwg0ar9383gnliw1axnwgihcqy8")))

(define-public crate-haussmann-0.0.1 (crate (name "haussmann") (vers "0.0.1") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "0ii310zny2f7f449wpfsc4vnixw7kfhk0kp3p11bw62pz9v7b38x") (yanked #t)))

(define-public crate-haussmann-0.0.2 (crate (name "haussmann") (vers "0.0.2") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "1i33irxw30ixk87sri5i6zv76fwv5jxrfwiqd1nxf9jmblzpci8z")))

(define-public crate-haussmann-0.0.3 (crate (name "haussmann") (vers "0.0.3") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)))) (hash "18lbzjlscjbdpf3f4knjxkm7x2ya3aapv4bdx12rzvsw878jriav")))

