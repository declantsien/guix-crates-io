(define-module (crates-io ha ze) #:use-module (crates-io))

(define-public crate-haze-0.1 (crate (name "haze") (vers "0.1.0") (hash "0lqrdq837f6r5f1cdcsy6yv5x5vmnzbv5sxpvwrq7y9k49xx1zd5")))

(define-public crate-haze_core-1 (crate (name "haze_core") (vers "1.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.3.0") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "08bvwgmypgpc260q2dzqmq3p3km6vgnv6i8r54k90n81vz7v874q")))

(define-public crate-haze_core-1 (crate (name "haze_core") (vers "1.4.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.3.0") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "19xrb685n4vc289y4zz02dcfjzb00gmnl385rg5kacgh4g9alr55")))

(define-public crate-hazel-0.0.1 (crate (name "hazel") (vers "0.0.1") (deps (list (crate-dep (name "bitboard") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.29.0") (default-features #t) (kind 0)))) (hash "0ri7y6vb7rxfvivvczprqrnynlwpvzy73x1k7dcvr39abvbb3mf9") (yanked #t)))

(define-public crate-hazelcast_rest-0.1 (crate (name "hazelcast_rest") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "02vwkgh6xh6y30wx3z66xbz1gccwcwxviykz16yv6zssxk6292a7")))

(define-public crate-hazen-0.1 (crate (name "hazen") (vers "0.1.0") (hash "0qj6zrr0akivl61bqdwap6r0bcxl7lqbw54yhp5rckcsqmplkwa5")))

