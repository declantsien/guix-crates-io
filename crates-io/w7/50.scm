(define-module (crates-io w7 #{50}#) #:use-module (crates-io))

(define-public crate-w7500x-0.1 (crate (name "w7500x") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1pzgkqvfl5nqfklpcqiy44kffk19ndzgahdzgxpn53a9xp7qgrpp") (features (quote (("rt" "cortex-m-rt/device")))) (yanked #t)))

(define-public crate-w7500x-pac-0.2 (crate (name "w7500x-pac") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0jgglii2gyg7qbmfz7zfikvykxi8lyc9zs797h25ral53lxwaxix") (features (quote (("rt" "cortex-m-rt/device"))))))

