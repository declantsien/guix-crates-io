(define-module (crates-io eh ru) #:use-module (crates-io))

(define-public crate-ehrust-0.1 (crate (name "ehrust") (vers "0.1.0") (deps (list (crate-dep (name "libeh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v3bd3diz8phfini2zwardslvy7sigc5z917mwvkawlk7hgz86kn")))

(define-public crate-ehrust-0.1 (crate (name "ehrust") (vers "0.1.1") (deps (list (crate-dep (name "libeh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h19q3l94jjg3kfp2y64lwfy3cbhi4nmc3w51lrqqnwdiak0lczk")))

