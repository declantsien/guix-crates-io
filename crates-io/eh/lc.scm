(define-module (crates-io eh lc) #:use-module (crates-io))

(define-public crate-ehlcd2d-0.1 (crate (name "ehlcd2d") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-io-async") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "194ggdsz0y6mgvs48srjqazbml8jrykl21k500y5da65i1lkcvf7") (yanked #t)))

(define-public crate-ehlcd2d-0.1 (crate (name "ehlcd2d") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-io-async") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0xxn87yyd3m5xhfywr3zf18d73byilqd073nyp85q9r22j6wkir0")))

