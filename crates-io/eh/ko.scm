(define-module (crates-io eh ko) #:use-module (crates-io))

(define-public crate-ehko-0.1 (crate (name "ehko") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y0l5mi1amhmpwp7ahjfw23xa7q7dwjgczp7lcliymip0azj5z8b")))

(define-public crate-ehko-0.1 (crate (name "ehko") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02fn8q93mk8fqb6jy4kwm5j941ajhw726632y5n4mh0m0njxc950")))

(define-public crate-ehko-0.1 (crate (name "ehko") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04ka920xh123a6fp151snj9baxi0qkdrhbc2isj0j9hrccpcny42")))

