(define-module (crates-io eh us) #:use-module (crates-io))

(define-public crate-ehuss-feat-test-0.1 (crate (name "ehuss-feat-test") (vers "0.1.0") (deps (list (crate-dep (name "rgb") (req "^0.8.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)))) (hash "01rwqw3cfr7gwran4bj8jk6kamdfkjhmzaym46cp350n4xq3p961") (v 2) (features2 (quote (("serde" "dep:serde" "rgb?/serde"))))))

(define-public crate-ehuss-feat-test-0.2 (crate (name "ehuss-feat-test") (vers "0.2.0") (deps (list (crate-dep (name "rgb") (req "^0.8.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)))) (hash "0wgfqal3b7rymy27g88kk61xlyjvdx0s297snqs1zm892ccv5s88") (v 2) (features2 (quote (("serde2" "dep:serde" "rgb?/serde"))))))

(define-public crate-ehuss-hello-world-0.1 (crate (name "ehuss-hello-world") (vers "0.1.0") (hash "0i5gbr60129k9pxkzjm8wyjdzn509vq3qkc1b0rha1j251wp5kzr")))

(define-public crate-ehuss-test-0.1 (crate (name "ehuss-test") (vers "0.1.0") (hash "0hqwf4cbqjhcqf8m3mdf7gm7c5qdr9s9adb08lgzch20yz98dwgk")))

(define-public crate-ehuss-test-0.1 (crate (name "ehuss-test") (vers "0.1.1") (hash "1kc5md2v6nfbsdp2zh9885ri20raggygr4v2n65yvpw4hfr6r9dn")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.0") (hash "0l8mgnrf6nxybx49aiqifkw0f1v6qprvsjhq0ijiin1bc384022s")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.1") (hash "11kfq6m2x0cvnd4srg2i6x2inwbm36s1s09di2dg3ax7q873qivh")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.2") (hash "0pr9ls61rdfff370g980dwl35aj45rdjgqgafm92h2yp65cmfy6w")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.3") (hash "0azdvr2362l0nxf7b789bww5sl3x1f9l1mfy47fhh99kvc9np420")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.4") (hash "08dk4isdl420qgz3z5xcxi252haavrf5vkd9r80kpymsk3dqim0x")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.5") (hash "14rhx3pxj0zjfpicmnpk1399yw692bcwl0p18x8314na1abwwyrd")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.6") (hash "08i4fjibyq1fp50mshczlqk01lvqhq222i0nj6vm738l0ch27jpi")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.7") (hash "0szr6av6fnklhgw0iqnqkf42wa3hr4lb54czh11mcpql32l76r36")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.8") (hash "01n91d8kaypsxqfsl9k0yq4w1xiv4m9qyjnw31czvnigqz3jnrdi")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.9") (hash "12v86a1c459haq2wjswdlzmvqkrfzfnxhpzp8apvlrydid6yla00")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.10") (hash "06qdfkhkc30xcg50s76hlv5dw32rgblm4lkbzx628nf0xjzphnlb")))

(define-public crate-ehuss-test1-0.1 (crate (name "ehuss-test1") (vers "0.1.11") (hash "1p359pxn0dcr69fys6lnhigx0da7xk3z8b3bn7v2dyh5mx3wm0q7")))

(define-public crate-ehuss-test2-0.1 (crate (name "ehuss-test2") (vers "0.1.0") (deps (list (crate-dep (name "ehuss-test1") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ckh95wadbxlzr67vhqbjsk89z6arxi2n36ykj9i7xhry98glz3l")))

(define-public crate-ehuss-test2-0.1 (crate (name "ehuss-test2") (vers "0.1.1") (deps (list (crate-dep (name "ehuss-test1") (req "^0.1") (default-features #t) (kind 0)))) (hash "0inbif5r4qxspkvkqj316lzdya8zcync1w6c25p0z5k9m6frka9z")))

(define-public crate-ehuss-test2-0.1 (crate (name "ehuss-test2") (vers "0.1.2") (deps (list (crate-dep (name "ehuss-test1") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "02h8pyj9c5f6jkraqpzl7dsr9fr15fymj7hhnjwmxmvcyfza7nsw")))

(define-public crate-ehuss-test2-0.1 (crate (name "ehuss-test2") (vers "0.1.4") (deps (list (crate-dep (name "ehuss-test1") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0mk3gi9lp2zpbs5r2rznf59b0pxizv6nz3rj432nvimrps2iklka")))

(define-public crate-ehuss-test2-0.1 (crate (name "ehuss-test2") (vers "0.1.5") (deps (list (crate-dep (name "ehuss-test1") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "1a2kgmy04bh1hi77hdr9n38j60p71qsfswnwn4hn9am5scg0b3cz")))

