(define-module (crates-io oh ol) #:use-module (crates-io))

(define-public crate-ohol_public_data-0.1 (crate (name "ohol_public_data") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "1jgzzqyv2n6w9hg6q1q380p7qkxbh45yscyf25n4psfif2silm23")))

