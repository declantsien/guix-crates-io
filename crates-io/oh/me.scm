(define-module (crates-io oh me) #:use-module (crates-io))

(define-public crate-ohmers-0.1 (crate (name "ohmers") (vers "0.1.0") (deps (list (crate-dep (name "redis") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "stal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ppa525zfm4lpff300488zhqs9nxqgh1jkpdaxnvw9yh9ri318vk")))

(define-public crate-ohmers-0.1 (crate (name "ohmers") (vers "0.1.1") (deps (list (crate-dep (name "redis") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "stal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0annr3r59vga85zwdyyygwnhvc7x1cih6z8rq4maqixj74n6chln")))

