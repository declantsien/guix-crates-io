(define-module (crates-io oh wi) #:use-module (crates-io))

(define-public crate-ohwid-0.1 (crate (name "ohwid") (vers "0.1.0") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (kind 0)))) (hash "16xlnnsjszqq4qzk3l85cdpk2mb4szy1zqs8a0yiqpxi87d3jsz3") (yanked #t)))

(define-public crate-ohwid-0.1 (crate (name "ohwid") (vers "0.1.1") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ns9wcpn73m2sw50bzia45bpxh128x81jr9h5yp4kns2lc0xp793") (yanked #t)))

(define-public crate-ohwid-0.1 (crate (name "ohwid") (vers "0.1.2") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "01ig1b708fq79vbiky18n9w1pyg18rl39cqmqhljlr5zbv9qp7li")))

