(define-module (crates-io oh os) #:use-module (crates-io))

(define-public crate-ohos-cli-0.1 (crate (name "ohos-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1d46x6al092yx74s1fs8wv0kvbagfm6rh6iqyqbhl2xgaxm469dw")))

(define-public crate-ohos-hilog-binding-0.0.1 (crate (name "ohos-hilog-binding") (vers "0.0.1") (hash "0xmrvgz7wz3l0jp4yxcpp1zf6cgzw3cd980wayq400mrq3mkrfjn")))

(define-public crate-ohos-image-binding-0.0.1 (crate (name "ohos-image-binding") (vers "0.0.1") (deps (list (crate-dep (name "napi-sys-ohos") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "04824a2iknx3lc96rk15ybch267xm6wfjxdv0zrh6hy2mpdkvz6r")))

(define-public crate-ohos-init-binding-0.0.1 (crate (name "ohos-init-binding") (vers "0.0.1") (hash "1zhlaimvgzhf9gaj54z3zbyvq9dv93bki2abib36k4v7qzs5i23n")))

(define-public crate-ohos-init-binding-0.0.2 (crate (name "ohos-init-binding") (vers "0.0.2") (hash "1d7fhxg4vz3s75iwbfjaw02v4rfxry51hszh9i529lq86rzlda0k")))

(define-public crate-ohos-nj-core-6 (crate (name "ohos-nj-core") (vers "6.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "fluvio-future") (req "^0.6.0") (features (quote ("task" "subscriber"))) (kind 0)) (crate-dep (name "futures-lite") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "inventory") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "nj-sys") (req "^0.1.1") (default-features #t) (kind 0) (package "oh-napi-sys")) (crate-dep (name "num-bigint") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "19p58dbbxjrhks5m4yigzyqjjykyhlbpcxwc572bhd0qcy199nb0") (features (quote (("serde-json" "serde_json") ("convert-uuid" "uuid"))))))

(define-public crate-ohos-nj-core-6 (crate (name "ohos-nj-core") (vers "6.0.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "fluvio-future") (req "^0.6.0") (features (quote ("task" "subscriber"))) (kind 0)) (crate-dep (name "futures-lite") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "inventory") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "nj-sys") (req "^0.1.1") (default-features #t) (kind 0) (package "oh-napi-sys")) (crate-dep (name "num-bigint") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustix") (req "=0.38.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1vjk0iqy1lds2l1sjnbyk8m05rnmlmbkjap916p81frp358hfpl8") (features (quote (("serde-json" "serde_json") ("convert-uuid" "uuid"))))))

(define-public crate-ohos-nj-derive-3 (crate (name "ohos-nj-derive") (vers "3.4.2") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "fluvio-future") (req "^0.6.0") (features (quote ("timer"))) (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing" "visit-mut" "derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "13n9h6yxr376gi0gc4b2d1gzxmb1gqq0gj1g98yxcschs6r0z0pl")))

(define-public crate-ohos-node-bindgen-6 (crate (name "ohos-node-bindgen") (vers "6.0.2") (deps (list (crate-dep (name "nj-core") (req "^6.0.1") (optional #t) (default-features #t) (kind 0) (package "ohos-nj-core")) (crate-dep (name "nj-derive") (req "^3.2.0") (optional #t) (default-features #t) (kind 0) (package "ohos-nj-derive")) (crate-dep (name "nj-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0) (package "oh-napi-sys")))) (hash "1amcbnjm2bdw87hz5xbpbzrbn7dvmm5vvb8hs5bjrkdnydm38312") (features (quote (("uuid" "nj-core/convert-uuid") ("serde-json" "nj-core/serde-json") ("node" "nj-sys" "nj-core" "nj-derive") ("default" "node"))))))

(define-public crate-ohos-node-bindgen-6 (crate (name "ohos-node-bindgen") (vers "6.0.3") (deps (list (crate-dep (name "nj-core") (req "^6.0.1") (optional #t) (default-features #t) (kind 0) (package "ohos-nj-core")) (crate-dep (name "nj-derive") (req "^3.2.0") (optional #t) (default-features #t) (kind 0) (package "ohos-nj-derive")) (crate-dep (name "nj-sys") (req "^0.1.1") (optional #t) (default-features #t) (kind 0) (package "oh-napi-sys")))) (hash "0c1d4f5z3xd7mbi4b5f1hbqmffzdmp9gqp546g2dk9nmfj0y2zn5") (features (quote (("uuid" "nj-core/convert-uuid") ("serde-json" "nj-core/serde-json") ("node" "nj-sys" "nj-core" "nj-derive") ("default" "node"))))))

(define-public crate-ohos-sys-0.1 (crate (name "ohos-sys") (vers "0.1.0") (hash "09z3gdvn6xyzg4hmf097aqn947aik6fkps4gcm734b6lcp2lhj9b")))

(define-public crate-ohos_hilog-0.1 (crate (name "ohos_hilog") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ohos_hilog-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)))) (hash "0v0x7bkp2lcrlzr1nzsprjj409z7m4zaldz7dacfa1xm6960x07f") (features (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-ohos_hilog-0.1 (crate (name "ohos_hilog") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ohos_hilog-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)))) (hash "1c816i35k9y59a0lblzd2v0h62vvlpbdgqqj53ffbs6qjp0pxvng") (features (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-ohos_hilog-0.1 (crate (name "ohos_hilog") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.10") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ohos_hilog-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)))) (hash "1x4hv3fqviizm8cq38cj7bdigqyzzwfs4pp6ipi3c0l5zygia68g") (features (quote (("regex" "env_logger/regex") ("default" "regex"))))))

(define-public crate-ohos_hilog-sys-0.1 (crate (name "ohos_hilog-sys") (vers "0.1.0") (hash "06z7k6n4a2ry2n7ily4674231wgvcd2lyjaa9q7r0zxm0sfxfj5v")))

(define-public crate-ohos_ndk_env-0.1 (crate (name "ohos_ndk_env") (vers "0.1.0") (hash "1yl8xh2bd1h54nbj1c597siy220bkkpnx1ig4fh7f1gwk92awlai")))

(define-public crate-ohos_ndk_env-0.1 (crate (name "ohos_ndk_env") (vers "0.1.1") (hash "02yl885hba4ay8cs2a6zfg7z1k7j40wbv68wwy5lf8q8778kdjy5")))

(define-public crate-ohos_ndk_env-0.1 (crate (name "ohos_ndk_env") (vers "0.1.2") (hash "1c90yh0gnrablr176nq5dprxmvl6g02csyqcxy4m4nl347lk3ic4")))

(define-public crate-ohos_ndk_env-0.1 (crate (name "ohos_ndk_env") (vers "0.1.3") (hash "0klb68vf5wak7ahmh8zngz64zszk1l58m8azmm3glmrj78glyzag")))

(define-public crate-ohos_ndk_env-0.1 (crate (name "ohos_ndk_env") (vers "0.1.4") (hash "0q44jil9hb9f23h05008cjbd7ic1zikxwvpsghgphx5w15199lpn")))

