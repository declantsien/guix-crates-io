(define-module (crates-io oh _m) #:use-module (crates-io))

(define-public crate-oh_my_guess-0.1 (crate (name "oh_my_guess") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "04nmib04ah59lcx7ls6ra6q3wq384pi06k19x0lmly08rdgidyih") (yanked #t)))

(define-public crate-oh_my_guess-0.1 (crate (name "oh_my_guess") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "15vsxvnrz9q8vlq276m211wghxijhrryw34a3ja3vsz0i1g7iqdj")))

