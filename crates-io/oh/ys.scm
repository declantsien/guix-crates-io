(define-module (crates-io oh ys) #:use-module (crates-io))

(define-public crate-ohys-api-0.1 (crate (name "ohys-api") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "087qdf3xg68rx7jdy3vrbg187i8g739wcwmhjf1gfv7p82fihkin")))

