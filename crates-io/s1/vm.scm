(define-module (crates-io s1 vm) #:use-module (crates-io))

(define-public crate-s1vm-0.1 (crate (name "s1vm") (vers "0.1.0") (deps (list (crate-dep (name "bwasm") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "parity-wasm") (req "^0.41") (default-features #t) (kind 0)) (crate-dep (name "wasmi-validation") (req "^0.4") (default-features #t) (kind 0)))) (hash "07rgbx1nm0sn5p99l7dm0r129hadisyfyfc0z9dcsnkwjqvwsvm3")))

(define-public crate-s1vm-0.1 (crate (name "s1vm") (vers "0.1.1") (deps (list (crate-dep (name "bwasm") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "parity-wasm") (req "^0.41") (default-features #t) (kind 0)) (crate-dep (name "wasmi-validation") (req "^0.4") (default-features #t) (kind 0)))) (hash "1lvzkvrkczp1v03q6ki6wyk8y6937kqnhwpn1zv8ac62yyvgsbbc")))

