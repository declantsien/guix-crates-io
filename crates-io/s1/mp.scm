(define-module (crates-io s1 mp) #:use-module (crates-io))

(define-public crate-s1mple-0.0.1 (crate (name "s1mple") (vers "0.0.1") (deps (list (crate-dep (name "cranelift") (req "^0.82.1") (default-features #t) (kind 0)) (crate-dep (name "cranelift-module") (req "^0.82.1") (default-features #t) (kind 0)) (crate-dep (name "cranelift-native") (req "^0.82.1") (default-features #t) (kind 0)) (crate-dep (name "cranelift-object") (req "^0.82.1") (default-features #t) (kind 0)))) (hash "1hlk99hrl54y2qgyzyj2wz800bjfa657lbh9rm93cpkbfrkay7w1")))

