(define-module (crates-io s1 #{00}#) #:use-module (crates-io))

(define-public crate-s100-0.0.0 (crate (name "s100") (vers "0.0.0") (hash "0j67gn5lay4aahkzn72564j0rxzh7gqzwh6indx1j3llzadnk79h") (yanked #t)))

(define-public crate-s100-0.1 (crate (name "s100") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "iso8211") (req "^0.1.1-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "libxml") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)))) (hash "18z1pzdfhly8wyfdwxiwcj31p9bay9l6qp3jmk2hwc5xlby5kqn6") (yanked #t)))

