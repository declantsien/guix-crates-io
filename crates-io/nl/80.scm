(define-module (crates-io nl #{80}#) #:use-module (crates-io))

(define-public crate-nl80211-0.0.1 (crate (name "nl80211") (vers "0.0.1") (deps (list (crate-dep (name "buffering") (req "^0.3.2") (features (quote ("copy"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "neli") (req "^0.4.3-r1") (default-features #t) (kind 0)))) (hash "1x01lj20ys3qp8a3c5m1h46702mfl9vf307zk3nx1sq03ydsnqmd")))

(define-public crate-nl80211-0.0.2 (crate (name "nl80211") (vers "0.0.2") (deps (list (crate-dep (name "buffering") (req "^0.3.2") (features (quote ("copy"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "neli") (req "^0.4.3-r1") (default-features #t) (kind 0)))) (hash "0jgx12qy0a004sc4qpr3ahgn9gma3rln9gsxiq4cdw6dd8h4dmx0")))

(define-public crate-nl80211-buildtools-0.1 (crate (name "nl80211-buildtools") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02ikb67bqzbj0ijzcxcdqw8wakgz7yk43mb1wr6fq1b1zy6383xs")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.0") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11ffp4wr5pnx2lcbavbwhwan8myg1pwyljx77yg48km6dzskxy7i")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.1") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0cgl0106c4fkvsq8zsyvznhm6p4zgj0knvkdqx6cg45057c5gbrw")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.2") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0pgimicgckksyymwak9ri2xygbrq7qzx4j1g1j36pm2n8w5yj4qh")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.3") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0nifd4ysij99dxng6vms84hd8g50wcymfzrprb5vknll7bk7d5vm")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.4") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "06b5dz5rzdf2nny9s8f7n179bkcmgydgfwygm40gnh92mpszd7rh")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.5") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0h9ii3f6yhky3ixln4k5kqbjr37366ca7ff6jijr6hnygg4i10dh")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.6") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ygyyji0dnypg2fvzip63dwh9bh3vqfwnlxzpr8pr208fagdh7mx")))

(define-public crate-nl80211-ng-0.1 (crate (name "nl80211-ng") (vers "0.1.7") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0n0ihj8a3iviq7nwlh8w3gglcp21xv3s4qxn7q6pppn74avarmih")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.0") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rw06fwn6521fhzg3xvhc6mr89ad9brmm0kl7ghhld598pmjfz02")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.1") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15p3s4q4d5swb94fkpdcpvk6v93c8z2sijmfil4hhv6c6fbf9968")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.2") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0jyhnykz7x2limslhypzhfkjzsfbb5183l8mjapaybsqxyy93qc6")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.3") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ajwby29glv94prb68ww844jn9mx37ffb72zfj68xm69iljc8zva")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.4") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0c63yg92yyr93azxf87hx5ajxsdsmnc8mdprzc0y3b4zg4nszgjb")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.5") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0pjlk1q31cvcanjb7y2vifn3xs8hgnhxr6w304w9n6gg63hzn58h")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.6") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1yj32s6k4yj7hvzckwwkapqr548h62f24km2dml4naps87wik4wi")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.7") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0y3kglpk6n19qjiwzg04xpy0vi4cdin1iyk1wsk02l13siqgwp3c")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.8") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0vlhx57p1zadngs9vmm47zyyfpcy46ww0166clfb64rb60jamzqp")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.9") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1yjwpxa6x0pwd1vkb0s74pvzbaqrbglgsxqrd8wc4jmvj18ch9sk")))

(define-public crate-nl80211-ng-0.2 (crate (name "nl80211-ng") (vers "0.2.10") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1nwdvmscapvxxfanlcvza5yvw88y0g4bvhbxw4crrj00y7ygpsw7")))

(define-public crate-nl80211-ng-0.3 (crate (name "nl80211-ng") (vers "0.3.0") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18cyb1wkj3ipnm2k8gf98hs9jjppp8q594cl2imggxp7csbacdfr")))

(define-public crate-nl80211-ng-0.3 (crate (name "nl80211-ng") (vers "0.3.1") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rgbmafki6xylpb0kmsvz8kgh77lf2f5pwk767gd68g5iyvibl6b")))

(define-public crate-nl80211-ng-0.4 (crate (name "nl80211-ng") (vers "0.4.0") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02q3is01frm2hg04771an2gicgvya4gzzg0lkdla8c84nnqa86zd")))

(define-public crate-nl80211-ng-0.4 (crate (name "nl80211-ng") (vers "0.4.1") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "07dxd7nb9cad37sr99d18r34kzx7cffchj6ps1rjvlhhpjs24q46")))

(define-public crate-nl80211-ng-0.5 (crate (name "nl80211-ng") (vers "0.5.0") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0nnwi0my6jvcia9jbhvszl77wmrnvz8v96jyxvb0w9jrjk0g11v3")))

(define-public crate-nl80211-ng-0.5 (crate (name "nl80211-ng") (vers "0.5.1") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1i60f30vkkid21wi15l4y6z528wa7m7fg5pa82jf5k2vpibjjmsz")))

(define-public crate-nl80211-ng-0.5 (crate (name "nl80211-ng") (vers "0.5.2") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0i8n7fraz4d77pr715xv36k72ii9af75jadqapl78rvji83jijy6")))

(define-public crate-nl80211-ng-0.5 (crate (name "nl80211-ng") (vers "0.5.3") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ilq0aachq0yzw3r42bj342vv86hyf2snxyxxnb5lmlj070n4inh")))

(define-public crate-nl80211-ng-0.5 (crate (name "nl80211-ng") (vers "0.5.5") (deps (list (crate-dep (name "neli") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "neli-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1h6scf549j3d4qmwiw90j7157frcqgggizh3hcw23kl5xvl330bi")))

(define-public crate-nl80211-rs-0.1 (crate (name "nl80211-rs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "netlink-rust") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nl80211-buildtools") (req "^0.1") (default-features #t) (kind 1)))) (hash "1g99s952w5ncf1mnn05aniqjgh3ml7k6xl9f5k2mii2q5sajvg7z")))

(define-public crate-nl80211rs-0.1 (crate (name "nl80211rs") (vers "0.1.0") (hash "0hpl4140zdvbrdc39bwf9psw7l6k62fwv1f7fdacrm0k7wx2d4sm")))

