(define-module (crates-io nl og) #:use-module (crates-io))

(define-public crate-nlog-0.1 (crate (name "nlog") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "0xwdw4higfpmi56h434gp69ddkhxcdzxl9jnsbfnpc52x2aa2xd2")))

(define-public crate-nlog-0.1 (crate (name "nlog") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "0ljfxdcs3r61ihskiv73rrqn4n1v58317ls950rv1zz18jicr7my")))

(define-public crate-nlog-0.2 (crate (name "nlog") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "winuser"))) (default-features #t) (kind 0)))) (hash "0w7k750lwch5d0xlfdbqnnpjq8adx9i8yd9qhrhg1n6hpfasj98f")))

(define-public crate-nlog-0.2 (crate (name "nlog") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "winuser"))) (default-features #t) (kind 0)))) (hash "01mnqxia04xlq5rlnblbrz7fdwl85k8jn6141286qw53j1f5yw5x")))

