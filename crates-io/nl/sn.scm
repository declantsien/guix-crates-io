(define-module (crates-io nl sn) #:use-module (crates-io))

(define-public crate-nlsn-delaunay-0.1 (crate (name "nlsn-delaunay") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0gbxvs5j4gg7cjknqph535k9ckznjcfqhh18q8md7cal2halqxq0")))

(define-public crate-nlsn-delaunay-0.1 (crate (name "nlsn-delaunay") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "082ddw8hw6f61vn16rs1p15g5xkk1b9lr5wz2nnfa09yr2lrb1qy")))

