(define-module (crates-io nl op) #:use-module (crates-io))

(define-public crate-nlopt-0.1 (crate (name "nlopt") (vers "0.1.0") (deps (list (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "0zjip0i7w9ic6sdmak7jzphcbkc4m4y4icn6gvyckp1c29i1vqhz")))

(define-public crate-nlopt-0.2 (crate (name "nlopt") (vers "0.2.0") (deps (list (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "0csfaz7rcdcfvw6d0lhf3ji0jbkg25p9ph1w616hqgg2r9grhjgj")))

(define-public crate-nlopt-0.3 (crate (name "nlopt") (vers "0.3.0") (deps (list (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "0f8j9msnbnj6ikd86wpskscdwk4ggxsx3p96hl64lvx7s4q7gdwr")))

(define-public crate-nlopt-0.3 (crate (name "nlopt") (vers "0.3.1") (deps (list (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "0z1vrh2snmqmyzajhf9ynhqhvp2d0js0pfw8sf6xq10j4cps1260")))

(define-public crate-nlopt-0.3 (crate (name "nlopt") (vers "0.3.2") (deps (list (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "16j2ns0adnpjskx0m9hq3kjca8zadm49nrhd0327kmv6k970y85h")))

(define-public crate-nlopt-0.4 (crate (name "nlopt") (vers "0.4.0") (deps (list (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "1qvxbk53cxww3wq3p60xqwqkgiygqpfr4y1i51vnp8zg90x9ryk7")))

(define-public crate-nlopt-0.5 (crate (name "nlopt") (vers "0.5.0") (deps (list (crate-dep (name "cmake") (req "^0.1.40") (default-features #t) (kind 1)) (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "000yxmqmad5bs18fa1qk0l385m9gk0hs8k1a2ymvaa66qs4a0hjq")))

(define-public crate-nlopt-0.5 (crate (name "nlopt") (vers "0.5.1") (deps (list (crate-dep (name "cmake") (req "^0.1.40") (default-features #t) (kind 1)) (crate-dep (name "num-iter") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "0gnsni5n61y15fwl6bcsbnlvap7rc522z096189vr3w74z3478r6")))

(define-public crate-nlopt-0.5 (crate (name "nlopt") (vers "0.5.2") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "1sbhzr2c53bjs3g76irqbdhi5zdz1322j1z2n8qc6pfrihnz23xm")))

(define-public crate-nlopt-0.5 (crate (name "nlopt") (vers "0.5.3") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0iri2rvilk9fqp8azri3nhi9hgcam714rhhnm60v558m2hznxnqw")))

(define-public crate-nlopt-0.5 (crate (name "nlopt") (vers "0.5.4") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0767aakw4ayna11jklvdb9gcfzqgl5l2xnx3gqx1h35algdkfg37")))

(define-public crate-nlopt-0.6 (crate (name "nlopt") (vers "0.6.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0vpwci7c4m6fgd49llc7armpamas4q8zvax0v63nm6yv44dyz025")))

(define-public crate-nlopt-0.7 (crate (name "nlopt") (vers "0.7.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0qlxg37b09psp9l3nd5skf0pqip0l9baf86zan76rl74r5bqkfhi")))

