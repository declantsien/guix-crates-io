(define-module (crates-io nl oq) #:use-module (crates-io))

(define-public crate-nloq-0.1 (crate (name "nloq") (vers "0.1.0") (deps (list (crate-dep (name "nl-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "object-query") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1k90ijkfclnb9ywfr082wghd4mw9czkv1j3vkv0rgk7ac6nyk0q1") (features (quote (("default"))))))

(define-public crate-nloq-0.1 (crate (name "nloq") (vers "0.1.1") (deps (list (crate-dep (name "nl-parser") (req "^0.1.1") (kind 0)) (crate-dep (name "object-query") (req "^0.1.3") (kind 0)))) (hash "14v1zg72y7nlsrvbb3y7s5zyr22ip5dv4yrxlpbkcp33sj1mcfkz") (features (quote (("std" "nl-parser/std" "object-query/std") ("default" "std"))))))

