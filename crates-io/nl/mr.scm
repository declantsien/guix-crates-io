(define-module (crates-io nl mr) #:use-module (crates-io))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0pa1qrfdc61wbvb06yqdagy8gsb5p6ip56nx0vrn0z2qwmslkn1r")))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12586jk7bjqjvlpp5j6cgpa4z5sqa4ajza5biyvkazjmclcakyf9")))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1vnz0584mw3v8320yzq4kl9rhnbxiwb0yx53cwbl6bcb6fpl9180")))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1352h75qizsgc1b988ykcxh0wrgxq13ydiy30v874pkx2lvqsql9")))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.4") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "15f6ksip0gvbr4bjf89f3d3c1xgiqa3z8j79alg5045h6x64icjg")))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.5") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1f000nvn7q3hm2ck7dwjs9v4xmxsqm7hmh0m7d87846ysvykzfg3")))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.6") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "19kdwbgvkyq8j1f7w59plki13y3x5mhvw03gwqd1a0ndn8g9sgcj")))

(define-public crate-nlmrs-0.1 (crate (name "nlmrs") (vers "0.1.7") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1a2ch1m381l17llcfq9zh8xf3xx6l2na6cryp7cajf0vzgi5akv2")))

