(define-module (crates-io nl p-) #:use-module (crates-io))

(define-public crate-nlp-annotations-0.1 (crate (name "nlp-annotations") (vers "0.1.0") (hash "04xi58b8jh8jq0x9z8b1nwv0rbbz0kvn6j2zlhvgs1qsr326h4d2") (yanked #t)))

(define-public crate-nlp-annotations-0.1 (crate (name "nlp-annotations") (vers "0.1.1") (hash "1yzpw794jcw7ddc3v82i3y08g106yn9gv7v2z2a24vgg6pps3awx") (yanked #t)))

(define-public crate-nlp-annotations-0.1 (crate (name "nlp-annotations") (vers "0.1.2") (hash "1n3h3h0vgki2nrb2yklgklmdikgscx6gjr5fblg78nnf6w53m1f1") (yanked #t)))

(define-public crate-nlp-io-0.1 (crate (name "nlp-io") (vers "0.1.1") (deps (list (crate-dep (name "nlp-annotations") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0dfphdy9rfnj429p9d7bi6vwgvdvd8k16dmdd7p3iqya3xx267p2") (yanked #t)))

(define-public crate-nlp-io-0.1 (crate (name "nlp-io") (vers "0.1.2") (deps (list (crate-dep (name "nlp-annotations") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0gqbny0m7qlylxd5gi0bq2v6sg1lb15vjqgdqhg1c8plvr10drgl") (yanked #t)))

(define-public crate-nlp-tokenize-0.1 (crate (name "nlp-tokenize") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "196kxrazrb5yv9cfq55d7jx0wan78bdzva2p5fb260d26xsrb68b") (yanked #t)))

