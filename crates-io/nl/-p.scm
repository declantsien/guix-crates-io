(define-module (crates-io nl -p) #:use-module (crates-io))

(define-public crate-nl-parser-0.1 (crate (name "nl-parser") (vers "0.1.0") (hash "00acbnjmz3xai16kzvgxlgzdvy0gicsimgfxmg4zjly2gac4lbk7") (features (quote (("std") ("default" "std"))))))

(define-public crate-nl-parser-0.1 (crate (name "nl-parser") (vers "0.1.1") (hash "1hd0rhg0bwzkl3yxj9awmqyhy5mnzs3drggg4rg9nf6fzi6d8clr") (features (quote (("std") ("default" "std"))))))

