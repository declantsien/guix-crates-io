(define-module (crates-io nl s_) #:use-module (crates-io))

(define-public crate-nls_term_grid-0.1 (crate (name "nls_term_grid") (vers "0.1.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "01j58blafz3fkqfm6p37j31zyxh99swvrib7iriik2sqv1d7y91b") (rust-version "1.70.0")))

(define-public crate-nls_term_grid-0.1 (crate (name "nls_term_grid") (vers "0.1.1") (deps (list (crate-dep (name "unicode-width") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "1djvjkk7azz72drja3xnpv3331pxqy47792zr0kvql3kskjw4vxw") (rust-version "1.70.0")))

(define-public crate-nls_term_grid-0.2 (crate (name "nls_term_grid") (vers "0.2.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "0zprwan97h2xk9wafhylxm8vl1p4lj1bk23a1mhs2sbcri0wja7v") (rust-version "1.70.0")))

(define-public crate-nls_term_grid-0.3 (crate (name "nls_term_grid") (vers "0.3.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0hsn1nb9hg4js4jwfllajrp416nhn0w7yx2nbgi2z7dp1azi282k") (rust-version "1.70.0")))

