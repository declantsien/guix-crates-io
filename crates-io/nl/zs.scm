(define-module (crates-io nl zs) #:use-module (crates-io))

(define-public crate-nlzss11-1 (crate (name "nlzss11") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "15w6fbafj8qjkzwp9pfxjpr8gy2bvmpdphkinhjr53hh424j8x7q")))

(define-public crate-nlzss11-1 (crate (name "nlzss11") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "nlzss11-zlib") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0kqz4y39lmk14vca58b4773jbm713jjal2r89zs76z9sf5p42frr") (features (quote (("zlib" "nlzss11-zlib"))))))

(define-public crate-nlzss11-zlib-1 (crate (name "nlzss11-zlib") (vers "1.0.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0a87faj9mridqbfajamk19fz1rgdc67ddd46zdjasv49bbdf73aa")))

