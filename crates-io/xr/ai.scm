(define-module (crates-io xr ai) #:use-module (crates-io))

(define-public crate-xraise-0.1 (crate (name "xraise") (vers "0.1.0") (deps (list (crate-dep (name "psutil") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.12") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0mgpralmh5rc45k5lz13al6r88190xb0a75iip1f9fcg8qp7chak")))

