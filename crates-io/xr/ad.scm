(define-module (crates-io xr ad) #:use-module (crates-io))

(define-public crate-xradar-0.1 (crate (name "xradar") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "time" "macros" "process" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1a15b5is58ggarfrgfc1vpasvkbac5657s1ri7zrdzjz3548jxkw") (rust-version "1.56.0")))

