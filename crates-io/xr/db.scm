(define-module (crates-io xr db) #:use-module (crates-io))

(define-public crate-xrdb-0.1 (crate (name "xrdb") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0mgnhn3qa1sd9jaj6rsm4ls1n3hhvh5lzvbx6lhbnjxhsdf3qp25") (yanked #t)))

(define-public crate-xrdb-0.1 (crate (name "xrdb") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0v8c1ysxv7flxpdkjbr3wd633z9vl1b5ky92irkqfbn944ddjbfc")))

