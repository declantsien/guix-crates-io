(define-module (crates-io xr un) #:use-module (crates-io))

(define-public crate-xrun-0.1 (crate (name "xrun") (vers "0.1.0") (deps (list (crate-dep (name "comfy-table") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0zw2899mmqn1arwylcwmncfnayz17kmqwpcq3jynsslpxc9y105m")))

(define-public crate-xrun-0.2 (crate (name "xrun") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "198vbl7646kax76rd83620zp92nhh4viswp0cdg8al4w5sb17zdx")))

(define-public crate-xrun-0.3 (crate (name "xrun") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "05yspw32fcwsvj21475wi3d205703qkkxdn1xwpp8injx3wfj606")))

(define-public crate-xrun-0.4 (crate (name "xrun") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0cpnlq11bik35jd17vh5bnassl53zd27s6gfda3hph4z9gnkajy3")))

(define-public crate-xrun-0.5 (crate (name "xrun") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1h6rgdh6wmn5dcr8bb8pzgcxv0j3bgf0ygm41265smhmw274wixg")))

(define-public crate-xrunits-0.1 (crate (name "xrunits") (vers "0.1.1") (hash "1rwlfjy4bd50a3frsxa426h4zx880y1hhy02l6sjaj9lr2pp8whv")))

(define-public crate-xrunits-0.1 (crate (name "xrunits") (vers "0.1.2") (hash "0n0gfgjvwr4fdwfi8v852rn4w87zv2gbkr5v6hjpadi8h1m77g37")))

