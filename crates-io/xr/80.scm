(define-module (crates-io xr #{80}#) #:use-module (crates-io))

(define-public crate-xr806-pac-0.0.1 (crate (name "xr806-pac") (vers "0.0.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1l8q01w4zazr7afbg7997rhpq10bisahxxq80n6i0ispyn7wrrz1") (features (quote (("rt" "cortex-m-rt/device"))))))

