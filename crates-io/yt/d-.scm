(define-module (crates-io yt d-) #:use-module (crates-io))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.0") (hash "1gnb48zf6fmmy5vr45mb9bn7ja4kmi3vcd45kbkxmchs46131cpg")))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "13iqga64z5a8yz72z05pqcagd58g5hmqnnr9y36jra32a3dgim85")))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1758sf86nh5qicbdvnj03kr7rf0396lmxnc2d13jp7r71q1jrmlk")))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "01zzr42gslr2bc4jrkvy1c2n3gdqy562qhkcah7l5z27ngiavsxh")))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0xjpnxqqrx261p7x0830qbmcy0kw9bhc2f1w4m5al6ig96kiqfg3")))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0r56c8w5zx8j0hkrzjk1v1rqxilq2i1qg7zz50wp9628qhqbvqv6") (features (quote (("yt-dlp") ("youtube-dlc"))))))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b2vxabjq5frha0xfc6dzvmhcfrjpn9bmb6iyqjjxwvq2gpgk631") (features (quote (("yt-dlp") ("youtube-dlc"))))))

(define-public crate-ytd-rs-0.1 (crate (name "ytd-rs") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1sb5278286sp3bdllsvn9rw8ycddfchyxhisngf6pk9g67km64a9") (features (quote (("yt-dlp") ("youtube-dlc"))))))

