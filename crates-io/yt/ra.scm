(define-module (crates-io yt ra) #:use-module (crates-io))

(define-public crate-ytranscript-0.1 (crate (name "ytranscript") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0xm6sn0bxck9m5klpl2d964nciza0vviqwk1ppyyigs89ilq4412")))

