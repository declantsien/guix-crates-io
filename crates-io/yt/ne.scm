(define-module (crates-io yt ne) #:use-module (crates-io))

(define-public crate-ytnef-0.1 (crate (name "ytnef") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ytnef_sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "007ri94sq7rg7mb3mmwp0d1zkrsdja99d319p6h2akdkx9n5mgry")))

(define-public crate-ytnef-0.2 (crate (name "ytnef") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ytnef_sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mix8w3bi6cgq2lvibqs81xm76bm4ijqlqdb2nbq2q8xd43liq85")))

(define-public crate-ytnef-0.3 (crate (name "ytnef") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ytnef_sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "08rd25wgcrzvcz2vpdx1m3g7x4b60bgsss0badnv0wfk6hz18a2s")))

(define-public crate-ytnef-0.4 (crate (name "ytnef") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ytnef_sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "11ip2n4j31w623i59yn7jjj68wja9a1jgxl0vwrzyzivbhwng0xv")))

(define-public crate-ytnef_sys-0.1 (crate (name "ytnef_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1c21420da14pg8kcfhpc8gx1skwiqbygffww869vnlznsjjvdhwr")))

(define-public crate-ytnef_sys-0.2 (crate (name "ytnef_sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "07jadvf76km2q5h9la8gxizbjdzikmah1zpvwhzwky4qj5wyx8k8")))

(define-public crate-ytnef_sys-0.3 (crate (name "ytnef_sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1n1kasqr89rqmmlgmy27zkjl5s51ilkn1vqsfxnwknqd1nj0c2m8")))

(define-public crate-ytnef_sys-0.3 (crate (name "ytnef_sys") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "091fny6bn4gm2pl27bl3s3xnjhlw6vv796qwcnmnc8fgajvdljvh")))

