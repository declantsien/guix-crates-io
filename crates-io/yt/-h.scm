(define-module (crates-io yt -h) #:use-module (crates-io))

(define-public crate-yt-hello-world-1 (crate (name "yt-hello-world") (vers "1.0.0") (hash "0m62mhvs2jffv062vpjbz582w8hiy646fnayz1vixgsh6jiv9d6x") (yanked #t)))

(define-public crate-yt-hello-world-1 (crate (name "yt-hello-world") (vers "1.0.1") (hash "15s81grhh4i4f2fyhbkz4aj0cqgqh494l2qhmsgnnzkpb2l2adab") (yanked #t)))

(define-public crate-yt-hello-world-0.9 (crate (name "yt-hello-world") (vers "0.9.10") (hash "0rbgpk6m1bvcrk1qgi16g3ik5j09f9ajiw5slb884bkhq47ar6g3") (yanked #t)))

(define-public crate-yt-hello-world-0.9 (crate (name "yt-hello-world") (vers "0.9.11") (hash "1mxb1fjyw78xwfxr09lldgqmq5zk5ma3h0b3mwz7wqiz13mmamqc") (yanked #t)))

(define-public crate-yt-hello-world-0.9 (crate (name "yt-hello-world") (vers "0.9.12") (hash "0q2lvcymars0v444w05blk3zl6vvgg6rbywsfz5i9ng4zjas5yhi") (yanked #t)))

