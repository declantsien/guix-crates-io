(define-module (crates-io yt _d) #:use-module (crates-io))

(define-public crate-yt_downloader-0.1 (crate (name "yt_downloader") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 0)) (crate-dep (name "rafy") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "06043jwn53gbvws9lr1lvi7gfg2jrswrvnbspwxqr5190bg2500p")))

(define-public crate-yt_downloader-1 (crate (name "yt_downloader") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 0)) (crate-dep (name "rafy") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0c1vyksyfs034kh3k5hkhxzzqklf9hnnln88cr6dyjxcjsqh09ma")))

