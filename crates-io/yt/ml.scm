(define-module (crates-io yt ml) #:use-module (crates-io))

(define-public crate-ytml-0.1 (crate (name "ytml") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "078bj1ahwbl74n9v22rbsgdfa03zrq581skaw32781gz5d3yvzd1") (yanked #t)))

(define-public crate-ytml-0.1 (crate (name "ytml") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "0fmfv7ciqj6m02556i54rg8b9inw8w3qdd97nhskzyxhvx16vp4y") (yanked #t)))

(define-public crate-ytml-0.1 (crate (name "ytml") (vers "0.1.2") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "1j9cnd3azylql5l8zz425nnm7vpsj4zwi4vz40bzjlm4dajczx1i") (yanked #t)))

(define-public crate-ytml-0.1 (crate (name "ytml") (vers "0.1.3") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "0s2y1ni0sbd7l6hmbvgcghiqrp30v9gv3djfr6madg3b24aziv07") (features (quote (("file-handling")))) (yanked #t)))

(define-public crate-ytml-0.2 (crate (name "ytml") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "17xzbbyvxzjcy1rgawn0mbx1lnap64s6qs9c81mlir0vkxsf2rxw") (features (quote (("file-handling")))) (yanked #t)))

(define-public crate-ytml-0.2 (crate (name "ytml") (vers "0.2.1") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "09wmfqfjz0m8xxywcmhc55w7x0jmh08xja2mi2f8vgxpxxbrl951") (features (quote (("fs")))) (yanked #t)))

(define-public crate-ytml-0.2 (crate (name "ytml") (vers "0.2.2") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "1115k0cjrvh3dnqr5kpp6xfvxmggbjrk5893c9hv1i7vs3p01x1q") (features (quote (("fs")))) (yanked #t)))

(define-public crate-ytml-0.2 (crate (name "ytml") (vers "0.2.3") (deps (list (crate-dep (name "pest") (req "^2.5.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "1i0jga6l5h7khlp92cr0ry433nr15w40ssx719828bdnlw60si1n") (features (quote (("fs")))) (yanked #t)))

