(define-module (crates-io yt -t) #:use-module (crates-io))

(define-public crate-yt-tools-0.3 (crate (name "yt-tools") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "16x0cvhfricbfxzgm882rywvwa7l8v5fr0apz50grymfg7j94xxb") (features (quote (("default" "console_error_panic_hook"))))))

