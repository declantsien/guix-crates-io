(define-module (crates-io yt _t) #:use-module (crates-io))

(define-public crate-yt_tsu-0.1 (crate (name "yt_tsu") (vers "0.1.0") (deps (list (crate-dep (name "mpeg2ts") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15b79bmcfmyj428yfacxyjcvj0fqc5vxm5b8sl44mkv7133w97nx")))

(define-public crate-yt_tsu-0.1 (crate (name "yt_tsu") (vers "0.1.1") (deps (list (crate-dep (name "mpeg2ts") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0dnvz827w2vqmy8yf9vfvpp45pc45cd6zyfc5xs9p6435rn0jmn3")))

