(define-module (crates-io vi ft) #:use-module (crates-io))

(define-public crate-vifterpreter-0.1 (crate (name "vifterpreter") (vers "0.1.0") (deps (list (crate-dep (name "bilge") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "binrw") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "0qjx2lxzh9c4fki2l3d2l9vkbjiaspps5v38ysgc47liaqvh5a4j")))

