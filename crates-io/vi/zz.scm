(define-module (crates-io vi zz) #:use-module (crates-io))

(define-public crate-vizz-0.1 (crate (name "vizz") (vers "0.1.0") (deps (list (crate-dep (name "readonly") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.21") (default-features #t) (kind 0)))) (hash "0qai94rnw8ipsq0y5dvky3n4sywdbxkcs3wc20bmzkj5w3q36aln")))

(define-public crate-vizz-0.2 (crate (name "vizz") (vers "0.2.0") (deps (list (crate-dep (name "readonly") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vizz_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "16am99287wd1h6v5i0690ri8x4ycywwcfrab037waw8854g7dj62") (features (quote (("derive" "vizz_derive") ("default" "derive"))))))

(define-public crate-vizz_derive-0.1 (crate (name "vizz_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fhhrymk532qh6ipqlm550qb1f14db701560px6m5j7asf9ikr33")))

