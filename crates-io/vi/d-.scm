(define-module (crates-io vi d-) #:use-module (crates-io))

(define-public crate-vid-sys-0.1 (crate (name "vid-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "0cx5v2ihkr6jk3sss5l6b0ykfcgpnwgv16ydzyvcpg5x15zar318") (features (quote (("deprecated-apis")))) (links "vid")))

(define-public crate-vid-sys-0.2 (crate (name "vid-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "1cr40yw1h6hrin378g50aqbvjcnvrcd919d2xvy8fb96056ig6yy") (features (quote (("deprecated-apis")))) (links "vid")))

(define-public crate-vid-sys-0.3 (crate (name "vid-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "0r4ipqf892lz6pz5dhging02hzmlhzrfh1drkfmh5s1jss1pb79v") (features (quote (("deprecated-apis")))) (links "vid")))

