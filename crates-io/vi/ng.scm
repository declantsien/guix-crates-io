(define-module (crates-io vi ng) #:use-module (crates-io))

(define-public crate-vingrep-0.1 (crate (name "vingrep") (vers "0.1.0") (hash "125w0dnaprn10dmk6mh8lm0xyv28bpc3y7q26051j8mf46mm5asr")))

(define-public crate-vingt-et-un-0.1 (crate (name "vingt-et-un") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0vg7k1713v6xl7l1w5hldqy18wvgs39i4k47i5y9v3ld6lssm7g3")))

(define-public crate-vingt-et-un-0.1 (crate (name "vingt-et-un") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1qiw54ip64z9zhnimdy4rff2w1f1li3hhirarbpha0nhmyq3r6xl")))

