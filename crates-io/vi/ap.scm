(define-module (crates-io vi ap) #:use-module (crates-io))

(define-public crate-viaptr-0.1 (crate (name "viaptr") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "triomphe") (req "^0.1.11") (optional #t) (kind 0)))) (hash "0j5v0dsdxfyv1s8gmzk6hn9a6jd29hhnsl5yl5skl9drkhkrmffh") (features (quote (("default" "alloc") ("alloc"))))))

