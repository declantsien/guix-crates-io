(define-module (crates-io vi vo) #:use-module (crates-io))

(define-public crate-vivox_rs_sys-0.0.1 (crate (name "vivox_rs_sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)))) (hash "1jhdzpjv922w87armcnsji9nr8qf4c55q4k0yriphm9v3lmd8rlr") (yanked #t)))

(define-public crate-vivox_rs_sys-0.0.2 (crate (name "vivox_rs_sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)))) (hash "0z5sww857v67n8hnq28ydmrpg00kf890yf4z0p52rkqf2dj7bb9f")))

(define-public crate-vivox_rs_sys-5 (crate (name "vivox_rs_sys") (vers "5.9.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)))) (hash "0w3jx3sa91i02nyar6jdqa5izy5y2ldqw6vd0rbx8vnx3dngrn2m")))

