(define-module (crates-io vi ab) #:use-module (crates-io))

(define-public crate-viable-0.1 (crate (name "viable") (vers "0.1.0") (hash "19h7ragv8ingvbdf1q1l4n4fd6jwgz7yggvh0z308f51rvyzamb5")))

(define-public crate-viable-0.1 (crate (name "viable") (vers "0.1.1") (deps (list (crate-dep (name "viable-impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p4wf4km6n1cxzqswp8vh8r88cfk4s2fxf71zq3v044mr9brq2q4")))

(define-public crate-viable-0.1 (crate (name "viable") (vers "0.1.2") (deps (list (crate-dep (name "viable-impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ab35ki5kfn1kml3r6qprldnlyzymr6r4afazrw93q6qq2n7j8ji")))

(define-public crate-viable-0.1 (crate (name "viable") (vers "0.1.3") (deps (list (crate-dep (name "viable-impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "1x1lv8w58w0s91brjnjlpgh3zbwkwjvlcbcyc2kk1clpmwbmw36d")))

(define-public crate-viable-0.1 (crate (name "viable") (vers "0.1.4") (deps (list (crate-dep (name "viable-impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "15953p6hh2rph36nphki1yhrd87bq26wnm4m4k4yrnzj3r2wg66y")))

(define-public crate-viable-0.1 (crate (name "viable") (vers "0.1.5") (deps (list (crate-dep (name "viable-impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "1l1zcvzdpwsk1pn3zngmp2n28bk9mcqb35d00jvn21mlkksh52hx")))

(define-public crate-viable-0.2 (crate (name "viable") (vers "0.2.0") (deps (list (crate-dep (name "viable-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q6bk7992f1jvg8zvrgjyn4gc5c59lv4lks1k18pafmhwykc2ikp")))

(define-public crate-viable-impl-0.1 (crate (name "viable-impl") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "02ghdpdcjx5azkhx1hci6bwlyjq99jvwhgym96jc600pgh2fy67h")))

(define-public crate-viable-impl-0.1 (crate (name "viable-impl") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.55") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "160ibccfjnc52kcj269pbgshzyg7p3cjp0cy1s4ddl95ldb79qxr")))

(define-public crate-viable-impl-0.1 (crate (name "viable-impl") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.55") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1lyxpxxgn62laq4p7bh8j57d7qd3v4rpmrh93arsdpi9w6h6j3ax")))

(define-public crate-viable-impl-0.1 (crate (name "viable-impl") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.55") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0wk6vvf4pj8g65i3gyhjqcw821klhz4scfanjs7dpz41qxbq3ram")))

(define-public crate-viable-impl-0.1 (crate (name "viable-impl") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.55") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "01vrilm12m2hy5zxhhi69s9iwf77nrwcvdxajdi0inscbhy8j22a")))

(define-public crate-viable-impl-0.1 (crate (name "viable-impl") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.55") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1iays3ypdm0p8y0dhknrrvlj81ax405kw54ss1jr206qqlj99xf5")))

(define-public crate-viable-impl-0.2 (crate (name "viable-impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.55") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "18934n72asggkm79n2xx6hcjgkrdxdaqghy56x8iykn5ysagirf1")))

(define-public crate-viable-impl-0.2 (crate (name "viable-impl") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.55") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0vrssnagg1bc71cxcv9f5fkyr3jxw98g5hgdavl909xvsr1wzg2m")))

