(define-module (crates-io vi fi) #:use-module (crates-io))

(define-public crate-vifi-prompt-0.1 (crate (name "vifi-prompt") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1vi0qzk4nqml16jc9axkn65lvc8m289r8lx1wivb4h8iaw6mxkpb")))

(define-public crate-vifi-prompt-0.1 (crate (name "vifi-prompt") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0xsi8ycmfha9kdxfmvkjficr8awz7sk3nxyx9089044s2c19gqf3")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0jibv2y0r64yy6ndsb7lpm4s2hha10yyh13af6jzqjakl2yspf11")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1iwzxlhfc4qidkr2v5ly0jf3gkhg303i6v8bayw1pf8imxsalf00")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1zy7gwgqx9a171nrh33f62yqi7637mkyw1r0fvr39qcd042zhdnb")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "045ysacifa5figfr3k9zcbazfg9v7blym1qwav1fp2i4iz32sc8h")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1z696kmr6jspw5bimvnp5gl8irdgmpz5d16q8yybvmlh8cmbh3xi")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.5") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0xvfhl8qf53y1ccfzr3fw9h521jz3aym8qazcrs2ayq7r6g1i4i3")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.6") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1njlm5bw5k9wkbdn4369c7zkmi7ibbvnd21z896k8n3s53lbzrg1")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.7") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0sr7vajkl8q6rr8cws4qrrm8avx3s45hc78mhk9x0ggilhk86g33")))

(define-public crate-vifi-prompt-0.2 (crate (name "vifi-prompt") (vers "0.2.8") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1y8mxlxknalg3w3ic4l3x3lkdyc5cp9dsl90z0aq3ix51kjbxig7")))

