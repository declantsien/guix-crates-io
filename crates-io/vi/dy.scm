(define-module (crates-io vi dy) #:use-module (crates-io))

(define-public crate-vidyano-cli-0.3 (crate (name "vidyano-cli") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "172j5r5109rcd1995hafqqlxm4izwdwzv6jccnbfair9bpldl46y") (yanked #t)))

(define-public crate-vidyut-0.0.0 (crate (name "vidyut") (vers "0.0.0") (hash "1r77rhc1rkz3mdad7bng18hazrg246wiz00m9jwpnn7jhfkp6ggp")))

(define-public crate-vidyut-prakriya-0.0.0 (crate (name "vidyut-prakriya") (vers "0.0.0") (hash "01h8nhgjs8r5arkqv2acpf2danvc3x8yc2w14s2k0i1syiyda35m")))

