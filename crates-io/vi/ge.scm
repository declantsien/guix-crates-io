(define-module (crates-io vi ge) #:use-module (crates-io))

(define-public crate-vigem-0.1 (crate (name "vigem") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.6") (default-features #t) (kind 0)))) (hash "0x7ikzv0l7fkjzibwvxdn0jpam78njlb73634vc1km4qx4vdsy7y")))

(define-public crate-vigem-0.2 (crate (name "vigem") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.6") (default-features #t) (kind 0)))) (hash "0r0jj7lg3rwbcb5j8gf4vl4ln0imzklr1x07f1gbfg4c6q4cw400")))

(define-public crate-vigem-0.3 (crate (name "vigem") (vers "0.3.0") (hash "0pv8pibd0czbn26r1x2vcsrljyglfk3a35frncjxxys1hjz6qhzp")))

(define-public crate-vigem-0.4 (crate (name "vigem") (vers "0.4.0") (hash "11zb7yzn3gnxpsy82q58lqlfcc7q8ma6r3dkcbc0c14qlx80icdj")))

(define-public crate-vigem-0.5 (crate (name "vigem") (vers "0.5.0") (hash "08hs972z1xpsvgyirq4y0js5ng6d5vvrx3zcxx2s0095br3fvzz3")))

(define-public crate-vigem-0.6 (crate (name "vigem") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "vigem-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "08rdblrjyplcqg8fkqxkfzynz1fpprvhmb4fa67bfrm065hg5jbf")))

(define-public crate-vigem-0.7 (crate (name "vigem") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "vigem-sys") (req "^1") (default-features #t) (kind 0)))) (hash "0fg1yvybh4hczgx403mwsdy76arbkm29lvanjkml3vxmn5hsayw8")))

(define-public crate-vigem-0.8 (crate (name "vigem") (vers "0.8.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "vigem-sys") (req "^1") (default-features #t) (kind 0)))) (hash "1f0hzqxhn5xmaxanv8rsv6zl77za5g7lh5xhx8bg9ccr54qwpqbz")))

(define-public crate-vigem-0.9 (crate (name "vigem") (vers "0.9.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "vigem-sys") (req "^1") (default-features #t) (kind 0)))) (hash "1n3x0bg59zjkq2yklz0gs5nz1zv7px9jwry3gcrjr63ixpznb4fc")))

(define-public crate-vigem-0.9 (crate (name "vigem") (vers "0.9.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "vigem-sys") (req "^1.1") (default-features #t) (kind 0)))) (hash "0lpb6npyibfi3zzqn8gw2z6lrif51cxl62fp24ck8hap5jgxfy7j")))

(define-public crate-vigem-client-0.1 (crate (name "vigem-client") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (default-features #t) (kind 0)))) (hash "0k88wnd1psrk28827di415f9q9mddgawypd1j8790bxial1dn96n") (features (quote (("unstable"))))))

(define-public crate-vigem-client-0.1 (crate (name "vigem-client") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (default-features #t) (kind 0)))) (hash "1qvd598bapxqxhvrhh8jd6g4b7svyd99kfbqd303dsy4isf38pln") (features (quote (("unstable"))))))

(define-public crate-vigem-client-0.1 (crate (name "vigem-client") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (default-features #t) (kind 0)))) (hash "1m8jqa08d94lay01014jc62zpf4xhbvkv4vxm413n7cbx096k22x") (features (quote (("unstable"))))))

(define-public crate-vigem-client-0.1 (crate (name "vigem-client") (vers "0.1.3") (deps (list (crate-dep (name "rusty-xinput") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "urandom") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (default-features #t) (kind 0)))) (hash "0nc6dk71g34azi74id3q57wpl7mpx34bh2c7zpn8gak2g0zpmgp6") (features (quote (("unstable_xtarget_notification") ("unstable_ds4"))))))

(define-public crate-vigem-client-0.1 (crate (name "vigem-client") (vers "0.1.4") (deps (list (crate-dep (name "rusty-xinput") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "urandom") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("std" "handleapi" "setupapi" "fileapi" "winbase" "ioapiset" "synchapi" "errhandlingapi" "xinput" "winerror"))) (default-features #t) (kind 0)))) (hash "0ljdcckl8vkwppf8x4nidg2qxv77ibg3bc6zwjqiw7pykvwycmxq") (features (quote (("unstable_xtarget_notification") ("unstable_ds4"))))))

(define-public crate-vigem-sys-0.1 (crate (name "vigem-sys") (vers "0.1.0") (hash "1w0fy1a53yvll5hypmrn01f4hp39ba5973swjc1qdnfgbqbrdha8")))

(define-public crate-vigem-sys-0.2 (crate (name "vigem-sys") (vers "0.2.0") (hash "1xhgfl93ywcrhqpqrnxl07pqml7c134nl7frvwhp64adgi0hp0jn")))

(define-public crate-vigem-sys-1 (crate (name "vigem-sys") (vers "1.0.0") (hash "0x15kf9dnrxrj0rrl440fa6yh78iij6djc8mlv3qy9xm7gw3291i")))

(define-public crate-vigem-sys-1 (crate (name "vigem-sys") (vers "1.1.0") (hash "1iva5fmps8szrsvavdkz5bl7wl0cvcs8wpryqfcxi2vx0grmgkkn")))

(define-public crate-vigenere-0.0.1 (crate (name "vigenere") (vers "0.0.1") (hash "0yhgf32jyyi5y0ynxmgzwshx5xmsyd9c4jql8p3pf2v5csc9lvzm")))

(define-public crate-vigenere-0.0.2 (crate (name "vigenere") (vers "0.0.2") (hash "1x6mzrwrxhlcwcg3iiivfs33bdfkdqbygmgvyrw9rf42q81jxf5k")))

(define-public crate-vigenere-0.0.3 (crate (name "vigenere") (vers "0.0.3") (hash "0wnv5xcpmicwjwhiag4vzihbri3mgld777g358vrs8ipx5h99xw2")))

(define-public crate-vigenere-cipher-0.1 (crate (name "vigenere-cipher") (vers "0.1.0") (hash "1xdibi6qw759maj7bw6qnpij1bm5camxgaw1rw46n45kiwdwlc1n")))

(define-public crate-vigenere-rs-0.1 (crate (name "vigenere-rs") (vers "0.1.0") (hash "1m7l4h9h6y7dnxfhswgglafxi3hnficb7plm5fs01dmgy27rr5s7")))

