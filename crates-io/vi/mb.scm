(define-module (crates-io vi mb) #:use-module (crates-io))

(define-public crate-vimba-sys-0.1 (crate (name "vimba-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0bxr550fa2rg9d7i3xi45vd6wm1pzmah26fx0g4lk72v26x9l6k7") (rust-version "1.56")))

(define-public crate-vimba-sys-0.1 (crate (name "vimba-sys") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0h7hqv8vj6ncnm7xxfa8ddfpffspd04ri7x1bcddkjhy1grbka0x") (rust-version "1.56")))

(define-public crate-vimba-sys-0.1 (crate (name "vimba-sys") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "177bfnnirz4yd4ibz2z3adgvvr91as2k0zdxxavhw8lcm6frd05k") (rust-version "1.56")))

(define-public crate-vimba-sys-0.2 (crate (name "vimba-sys") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1n8x88503gspmz6qs4l6ylcjirsa8l47lkf4zvvy0ymddqkdj3zd") (rust-version "1.56")))

