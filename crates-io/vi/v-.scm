(define-module (crates-io vi v-) #:use-module (crates-io))

(define-public crate-viv-camp-0.1 (crate (name "viv-camp") (vers "0.1.0") (hash "09xrhp4q1py29mm8frg0nvklg8njbx3xq3dy2xrsq2hylxmd61jq") (rust-version "1.76.0")))

(define-public crate-viv-helloworld-0.1 (crate (name "viv-helloworld") (vers "0.1.0") (hash "17kk3i88hcdhxfxxymn3wsrmg37zqwa9bqqmgjirqqhfy0g8hxgy") (rust-version "1.74.1")))

