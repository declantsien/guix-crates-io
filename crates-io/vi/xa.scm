(define-module (crates-io vi xa) #:use-module (crates-io))

(define-public crate-vixargs-0.1 (crate (name "vixargs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.6") (features (quote ("derive" "color"))) (default-features #t) (kind 0)))) (hash "08x2wjjiklcb7lgj1h12c96q8sc41qww0clzj2pgdi89vckqiy9v")))

