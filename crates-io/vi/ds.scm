(define-module (crates-io vi ds) #:use-module (crates-io))

(define-public crate-vidseq-0.0.1 (crate (name "vidseq") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "gstreamer") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "gstreamer-app") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "0jb6hsb0kcdlwkvsvkxf695hc80gi1xc0lsyp7znk95w1f431y28")))

