(define-module (crates-io vi t_) #:use-module (crates-io))

(define-public crate-vit_logger-0.1 (crate (name "vit_logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "colorized") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.36") (default-features #t) (kind 0)))) (hash "1zql5diwhr7js0y227b9vp6fg9klsnjwvhm12dmv4j4ddbyn23aw")))

(define-public crate-vit_logger-0.1 (crate (name "vit_logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "colorized") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.36") (default-features #t) (kind 0)))) (hash "0njjl9nhjakd8211alpj5j7dxb6hrcqzbih7nb602ifzc3r41kdw")))

