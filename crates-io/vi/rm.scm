(define-module (crates-io vi rm) #:use-module (crates-io))

(define-public crate-virmin-0.1 (crate (name "virmin") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gsscnqph5q2rk3hcn8flr3dk7p31657zlhnymgami6yvmccv341")))

(define-public crate-virmin-0.2 (crate (name "virmin") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "170ca6wv5prqv4bicyli88xv1w5i4rxvnznd39wd8p4zil8k5ync")))

(define-public crate-virmin-0.3 (crate (name "virmin") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1m3gdxypk3f07sv2qkc1y03sadghiiqcdskgr472k7zndl713p5k")))

