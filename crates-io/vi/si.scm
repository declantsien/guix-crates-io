(define-module (crates-io vi si) #:use-module (crates-io))

(define-public crate-visibility-0.0.0 (crate (name "visibility") (vers "0.0.0") (hash "0qzqfpkxmg7sbp5aa2dg622534hlnlzghcp3m0gajl53m8qkl9h3")))

(define-public crate-visibility-0.0.1 (crate (name "visibility") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lch0vxlqvqdq8nnql1pz792190pa4gghnnyy6r3skp31b6db0c8") (features (quote (("nightly"))))))

(define-public crate-visibility-0.1 (crate (name "visibility") (vers "0.1.0-rc1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ilcpk5qnj680iqz72ka3zd7p4qvay48vncqa9krl0m3mj21py1i") (features (quote (("nightly"))))))

(define-public crate-visibility-0.1 (crate (name "visibility") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y70n5dpvj4vmf7jw5ab7livqhhg28gkxn2rivr7q8mrkncrizdk") (features (quote (("nightly"))))))

(define-public crate-visible-0.0.1 (crate (name "visible") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gp75mm95sabfpxwm6m1lrwviiiicihfa8n6g6yixz60smgh0i50")))

(define-public crate-visible_area_detection-0.1 (crate (name "visible_area_detection") (vers "0.1.0") (deps (list (crate-dep (name "direction") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "grid_2d") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rgb_int") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shadowcast") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qffq02dc8qil9f3iaj0ywvxcmppa3dbq2mbssfkgd22adwxva46")))

(define-public crate-visible_area_detection-0.1 (crate (name "visible_area_detection") (vers "0.1.1") (deps (list (crate-dep (name "direction") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "grid_2d") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rgb_int") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shadowcast") (req "^0.8") (default-features #t) (kind 0)))) (hash "0iq81kba370v6qdy8hqrawmwh0n79f186hb6mgk2yq4r9qr7s4b0") (features (quote (("serialize" "serde" "shadowcast/serialize" "grid_2d/serialize" "rgb_int/serialize"))))))

(define-public crate-visible_area_detection-0.1 (crate (name "visible_area_detection") (vers "0.1.2") (deps (list (crate-dep (name "direction") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "grid_2d") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rgb_int") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shadowcast") (req "^0.8") (default-features #t) (kind 0)))) (hash "0h3149s86kk3chr38811zrn0w4mrkli72l70c6mnklhkisk58sj5") (features (quote (("serialize" "serde" "shadowcast/serialize" "grid_2d/serialize" "rgb_int/serialize"))))))

(define-public crate-visible_area_detection-0.2 (crate (name "visible_area_detection") (vers "0.2.0") (deps (list (crate-dep (name "direction") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "grid_2d") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rgb_int") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shadowcast") (req "^0.8") (default-features #t) (kind 0)))) (hash "0mf2i3sgp107wcw0gj5xwxy95li9x5rmkihfkyvx0lwwnx5jds22") (features (quote (("serialize" "serde" "shadowcast/serialize" "grid_2d/serialize" "rgb_int/serialize"))))))

(define-public crate-visible_area_detection-0.3 (crate (name "visible_area_detection") (vers "0.3.0") (deps (list (crate-dep (name "direction") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "grid_2d") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rgb_int") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shadowcast") (req "^0.8") (default-features #t) (kind 0)))) (hash "095bz5gmz31q9h6p3zxa8875rqx5y8pj901b7hi5cv0paxbigl14") (features (quote (("serialize" "serde" "shadowcast/serialize" "grid_2d/serialize" "rgb_int/serialize"))))))

(define-public crate-visija-0.0.0 (crate (name "visija") (vers "0.0.0") (hash "19drfsq73mqmvad9ayyl4km4bdy503pwp8by0mgyqfqfm3d81nrg")))

(define-public crate-visim-0.1 (crate (name "visim") (vers "0.1.0") (deps (list (crate-dep (name "gl_dstruct") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "12kqjyf8wfz7zxfxkpzhwy8yqd6xvfwjn3fqzgvrh10cwjdkrqi7") (features (quote (("gl_debug" "gl_dstruct/debug"))))))

(define-public crate-visinline-0.1 (crate (name "visinline") (vers "0.1.0") (hash "1nfils1swak7hzp3ihsa7l4ri4xrl25p704pmfdzmbyk5swj3zpj")))

(define-public crate-vision-0.0.1 (crate (name "vision") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.9") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1z3dlm73pp60skd63gh6dffsd2x4i2ypkl21xlfi8ndw9nqbc4rd")))

(define-public crate-vision-0.0.2 (crate (name "vision") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.9") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0krz3n6psfyfkb0p7isnqwcmp7wgswlfhqcyfw2fjfj9dfb3brff")))

(define-public crate-visionary-0.0.0 (crate (name "visionary") (vers "0.0.0") (hash "0hiq2yy4ng88pzkwmajsk9pjzshs5vlwyjhyhb3hd4swrd45vmn2")))

(define-public crate-visioncortex-0.1 (crate (name "visioncortex") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n5aihawg1d8d8j7gg9k5pbzglihl8mz6fgbsx9ss24s6rqmc858")))

(define-public crate-visioncortex-0.2 (crate (name "visioncortex") (vers "0.2.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ddvsjsi3f0c4yvk8szgc9zydgalmqh2c7rr8fvs6224gi503qsf")))

(define-public crate-visioncortex-0.3 (crate (name "visioncortex") (vers "0.3.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q02z080491zkjxx4snm496cyajkp50bwvk4cxfdhjglg2svc9ng")))

(define-public crate-visioncortex-0.4 (crate (name "visioncortex") (vers "0.4.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ki7l9qhidrnlavzgb5bnzqa2gabya9aw0f0gl43s3768jikm95y")))

(define-public crate-visioncortex-0.5 (crate (name "visioncortex") (vers "0.5.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iimqkydqv6wvjbim6cpi8kxij7px1ad4wxr65cdnldx9aiysmj3")))

(define-public crate-visioncortex-0.6 (crate (name "visioncortex") (vers "0.6.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "197jiljv98g704f2l1x5dhfk21annqda2wah3z6n8vffk10pgph6")))

(define-public crate-visioncortex-0.6 (crate (name "visioncortex") (vers "0.6.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "18xcx7bxclccv2ar73i4w14z0pvda1l92ajmgz17ynz5l1yiq4ik")))

(define-public crate-visioncortex-0.7 (crate (name "visioncortex") (vers "0.7.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "059v5172qrvydbjn43hgxn7sdknxhhz1k4lvc30cc96rbg1zw4qm")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "048j71j706vzpksfz9wqd7g7ammldp2rc02f324x7cn3d45f5f42")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mc7yj1si5644vppzqs1sbskv6nw6gjxrbax7s7wm19g9vq9azyq")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.2") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "18y6hbkq6c2cizzsqv3w1sam5g5cv132xjsf0r9az3pn6nphqqvj")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.3") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bzvlky1q4g2mwgk9mlb4rrgzr2psa2n7nz6x3s3i0irhx2rwb3v")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.4") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rmkhgvh8gbpk4rs5l1jp1zlk5cvzjapi2pxlzlrs4shaaky6qwm")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.5") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w91nfwjl090hzparxj83dgv2221vpnfv9l8vmhpkilcckw1c9gv")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.6") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0snj3s43988bz9ajzkw4plj014r97y59mjyivgik3fscb64p1rly")))

(define-public crate-visioncortex-0.8 (crate (name "visioncortex") (vers "0.8.8") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "flo_curves") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zwzvyczvwq720h6rvsqilqw9fdvz7kqvhdd48ilpmw1q97ibgjd")))

(define-public crate-visionmagic-0.1 (crate (name "visionmagic") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "visioncortex") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0cwdzc87wc2j5l1yilicwv0w0yv7l7rn9gz86a559k2ryn6pj9c1")))

(define-public crate-visionmagic-0.1 (crate (name "visionmagic") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "visioncortex") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "03jp4lqhif9hsb3cl5ax3jw6vxrsjr4kpb4cmpavfdz7z0lcxwi7")))

(define-public crate-visionmagic-0.1 (crate (name "visionmagic") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "visioncortex") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0ig10zjz0hll9nb57p1k1mvycg9j7xsmbl24a3v604wxy96j5s45")))

(define-public crate-visionmagic-0.2 (crate (name "visionmagic") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "visioncortex") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1r5j96h83jrvg4h605jz2b3h2h3iqy31j5k2x5qq3vkyfbwn4njf")))

(define-public crate-visit-0.1 (crate (name "visit") (vers "0.1.0") (deps (list (crate-dep (name "case") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "simple") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0xpflaibyak5haq3hhzvsf4n8gzfxjwg0zfrx2q3hnb3qy0ljg6x")))

(define-public crate-visit-bytes-0.1 (crate (name "visit-bytes") (vers "0.1.0") (deps (list (crate-dep (name "camino_") (req "^1.0") (optional #t) (default-features #t) (kind 0) (package "camino")))) (hash "16am8mqym6vzcn78lcbd9pzqanb3ijb86ajlgxlmkqarpxfvdd4g") (features (quote (("default" "alloc") ("camino" "alloc" "camino_") ("alloc"))))))

(define-public crate-visit_diff-0.1 (crate (name "visit_diff") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (kind 0)) (crate-dep (name "visit_diff_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1") (kind 0)))) (hash "0aqj7xn5l5m1pjs28r35y375fay1bkas3634axxyg6r0xqwj96pv") (features (quote (("std") ("default" "visit_diff_derive" "std"))))))

(define-public crate-visit_diff-0.1 (crate (name "visit_diff") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (kind 0)) (crate-dep (name "visit_diff_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1") (kind 0)))) (hash "1idblwsnlxsgznsbc9cxkc58bxi4c0xmpclz6ds85nbzwkzbg0dw") (features (quote (("std") ("default" "visit_diff_derive" "std"))))))

(define-public crate-visit_diff_derive-0.1 (crate (name "visit_diff_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (default-features #t) (kind 0)))) (hash "0ahbfnh2h4vmq1ba6759jjdk3f6ma62qbdwnqlwcs8lbpgkx3jmk")))

(define-public crate-visita-0.1 (crate (name "visita") (vers "0.1.0") (deps (list (crate-dep (name "visita_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0m7q6k694pzzxab9n9h3sw31d3xcgvm0jrzjbqgyd224ql19njzz") (yanked #t)))

(define-public crate-visita-0.1 (crate (name "visita") (vers "0.1.1") (deps (list (crate-dep (name "visita_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1v38cvj8b00prsrzahqsk33c1rj8s7jpj09k9cr8236v0myg9cid")))

(define-public crate-visita-0.2 (crate (name "visita") (vers "0.2.0") (deps (list (crate-dep (name "visita_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "08zv3ai8mdfl6h63br9ywykwsqvqhk946b2ldpfb20rz0phf55kn")))

(define-public crate-visita-0.2 (crate (name "visita") (vers "0.2.1") (deps (list (crate-dep (name "visita_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "06g6ssxrbqlflf8kj98znyfcr7769y566394zh5syi7z1x47j6z0")))

(define-public crate-visita_macros-0.1 (crate (name "visita_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0678jwqw84l76sb0kl85hwxk0m33h0x3cbdpjhycblsri0fxdfm4") (yanked #t)))

(define-public crate-visita_macros-0.1 (crate (name "visita_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18zpqpizn9w80ccs1lc73342ijhfaw851aw7rqzhmv7nymzr4rjs")))

(define-public crate-visita_macros-0.2 (crate (name "visita_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g11jbc5h9hgin9by5cfa44w33xx6halvkyappy4shn9yk7117ji")))

(define-public crate-visiting_ref-0.1 (crate (name "visiting_ref") (vers "0.1.0") (deps (list (crate-dep (name "futures-channel") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-core") (req "^0.3") (kind 0)) (crate-dep (name "futures-executor") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-threaded"))) (default-features #t) (kind 2)))) (hash "1zazp9jwp8hh8l6n2dz8lzmbyh060bglr57gr46srxhgm47aic25")))

(define-public crate-visiting_ref-0.2 (crate (name "visiting_ref") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-channel") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-core") (req "^0.3") (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "rt-threaded"))) (default-features #t) (kind 2)))) (hash "04hl3gs9v8andc2mvbl93vnz8zh87hmq9dkrf3yzyiv4h7n37p0z")))

(define-public crate-visitor-0.1 (crate (name "visitor") (vers "0.1.0") (hash "0n8bqj20dl19f8x2s4ynvpkgjzjd0mqs8p1cmfp63z7qymm79fvf")))

(define-public crate-visitor-0.1 (crate (name "visitor") (vers "0.1.1") (hash "1z3z7m4zrj4ygmd216r3al7b07246ig7nrzh56zq4z4m8h4ljrr7")))

(define-public crate-visitor-0.2 (crate (name "visitor") (vers "0.2.0") (hash "1yi79dkg4brzk41p3wgavcq735dl3g9dkyf9mcg0r5q827fm5dpv")))

(define-public crate-visitor-0.2 (crate (name "visitor") (vers "0.2.1") (hash "0sv55s9bk45gm1g8q40mh96d7cm1g93bx9grc2n7xgj2n2rc4wxf")))

