(define-module (crates-io vi ps) #:use-module (crates-io))

(define-public crate-vips-0.1 (crate (name "vips") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "vips-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x8slrs7spad9b8z2rggyj89928m54s7b9418cxram1wa7kid8mc")))

(define-public crate-vips-0.1 (crate (name "vips") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "vips-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "11k1k7lallhllyj584m8zys3w19ym6z5ap29yzyh8qrgy8vg8k27")))

(define-public crate-vips-rs-0.1 (crate (name "vips-rs") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "vips-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02axk78prs6149rbpa22wxf63wzh8rmp9qk5ls6my071gbdg3a35")))

(define-public crate-vips-sys-0.1 (crate (name "vips-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.36.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "1290av3sqgqvbcrhh324dsgfc58s3g9cjm09qpvlhjg3wwpha1rp")))

(define-public crate-vips-sys-0.1 (crate (name "vips-sys") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "1ibbxbbdpvr7j43hrsfypz76zdnkknc45vfdw6jqn8npc80ps1rs")))

(define-public crate-vips-sys-0.1 (crate (name "vips-sys") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.11") (default-features #t) (kind 1)))) (hash "0jcwb1zzr3h1d7w5299r87dhxvy5zzcr97n9v3iw8gwa0wq21spb")))

(define-public crate-vips-sys-0.1 (crate (name "vips-sys") (vers "0.1.3-beta.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.11") (default-features #t) (kind 1)))) (hash "0a62wcn37hi773fhd5zd8q4vlci0dw5rj13pwaizfmb4ww9m987d") (links "vips")))

