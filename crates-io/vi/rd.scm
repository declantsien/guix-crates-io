(define-module (crates-io vi rd) #:use-module (crates-io))

(define-public crate-virdant-0.1 (crate (name "virdant") (vers "0.1.0-rc0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.2") (default-features #t) (kind 0)))) (hash "0jl1is5qas0rfyllzcc4kg4hjkys0c73dmwj5rvanhvpq93b3nv4")))

