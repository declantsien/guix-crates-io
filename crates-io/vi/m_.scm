(define-module (crates-io vi m_) #:use-module (crates-io))

(define-public crate-vim_edit-0.1 (crate (name "vim_edit") (vers "0.1.0") (deps (list (crate-dep (name "subprocess") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0slm4g4iviaggig9rzn2510rqaxahfayhww0ilj427bykc91g7rj")))

(define-public crate-vim_edit-0.1 (crate (name "vim_edit") (vers "0.1.1") (deps (list (crate-dep (name "subprocess") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "144fg7fa3xp2wgd3cy3vwvq10ifjm7cnnf8rphs1ip54kf4zlhvh")))

