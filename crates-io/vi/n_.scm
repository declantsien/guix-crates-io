(define-module (crates-io vi n_) #:use-module (crates-io))

(define-public crate-vin_parser-1 (crate (name "vin_parser") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1pan9l8yjrkp5c32dynxa2xsilw47yz0zaxah7bsxady77hviyfp")))

