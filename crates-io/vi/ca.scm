(define-module (crates-io vi ca) #:use-module (crates-io))

(define-public crate-vicardi-0.1 (crate (name "vicardi") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04z9n8a0kchgz5ni0k09pqh1901jf8zkdvk2qla1r8a0a1qd0s2s")))

(define-public crate-vicardi-0.1 (crate (name "vicardi") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "19j040jsi98p66hd2g047q1r7wsmkngmw59q5psfn63w7vb3q5cg")))

(define-public crate-vicardi-0.1 (crate (name "vicardi") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "15v6sffhdqb9gqnci94h8nw8p9d1dglm4n6hacrqia0dmh01zvpc")))

(define-public crate-vicardi-0.1 (crate (name "vicardi") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yzg7sga2nqqxkfqbn1rc776yd7d6k7xsiyw8x98w75p0zcjbd5b")))

(define-public crate-vicardi-0.1 (crate (name "vicardi") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "13yk59mrdpdjn8cvf3qaim8kcj49hs3qwxc7fcfdx3ksyn6bxbqr")))

(define-public crate-vicardi-0.1 (crate (name "vicardi") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10vfzyfbk8n1bgjr5385v4854gb3mz33qah07pcia44hjv4lljnm")))

