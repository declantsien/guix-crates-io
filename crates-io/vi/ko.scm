(define-module (crates-io vi ko) #:use-module (crates-io))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0l6460vdgb4mw777zcc6j7b3yvp8hl6kh13x4dd9issmfsk18jly")))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g5fnrzlnfhk0cjd0p87aplsivx6zsl08qrr7afwr72zvk0w0l5b")))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "1aqf1qmndqndcfxicm9vizhy18wf1vfg2h19f1aq21y5yq9mbnnq")))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "0svpk9yhg5nr12xj72i8h3mxb7zlj5j2azzqnskw3cbis7w6c5c2")))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "18gwp5lpcxzh6rm9vpjjq70q1wp338gq3q0cq2fvq45gj7x5lfip")))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.6") (deps (list (crate-dep (name "clippy") (req "^0.0.90") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "0y08jhwvfh5yd50fsf8s8j3gfrmzpzmqh2lp7z0q6g442qibkqad") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.7") (deps (list (crate-dep (name "clippy") (req "^0.0.90") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^0.14.7") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "10q6hm1rcdf96mzz743cm1hd3i8znzh45bb32cqzd5ri00x8f1hy") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-vikos-0.1 (crate (name "vikos") (vers "0.1.8") (deps (list (crate-dep (name "clippy") (req "^0.0.98") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^0.14.7") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "03vfs840jk89k3h6m90a0p89vpqnmai4sck82kpm50883ha2rsk3") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-vikos-0.2 (crate (name "vikos") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1gih4am94jkyzc0abp566x693h99fmry491rkd2x2nmjqmnladkb")))

(define-public crate-vikos-0.2 (crate (name "vikos") (vers "0.2.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1k3kjhgpp20l360sasz574pdp3f9b7b96fhbq4q2x4cnpwrnl0i7")))

(define-public crate-vikos-0.3 (crate (name "vikos") (vers "0.3.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1fbmzfkydaa2h6nm51amv4rdc1blcby5y7yr11xcczflj350pyva")))

(define-public crate-vikos-0.3 (crate (name "vikos") (vers "0.3.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "0qwp62g48bfvhk1wirklwm8jbg05901ah65z0h6cfccngkg2zwsx")))

