(define-module (crates-io vi de) #:use-module (crates-io))

(define-public crate-video-0.0.2 (crate (name "video") (vers "0.0.2") (hash "152v93ja3cfkyairldzj3psidwkwz7dkaj5h2wc7rsngliarv1my")))

(define-public crate-video-0.1 (crate (name "video") (vers "0.1.0") (hash "14plmg8c0dvcihm2skishhzcqb7srqgg4iq84a7xvdmcl0vni74l")))

(define-public crate-video-0.1 (crate (name "video") (vers "0.1.1") (hash "06j4w6cfcdvlazadgl99dc40spjmix4ldvbrkk74pb73h7x6siyl")))

(define-public crate-video-levels-0.1 (crate (name "video-levels") (vers "0.1.0") (hash "15rkmdsjipdifvn7dbw8vswsrnrbf9fc0hjwdcfxm9vmi8255cn9")))

(define-public crate-video-levels-0.1 (crate (name "video-levels") (vers "0.1.1") (hash "0k7f7pawd5lg6grfc7y10sy0897wq0k2ld53w44ndbh2x8n3crr3")))

(define-public crate-video-levels-1 (crate (name "video-levels") (vers "1.0.0") (deps (list (crate-dep (name "yuv") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0cn5cj5bg0a50sx792pyy3xj0s17363la43bmw78985a9wz01b6v")))

(define-public crate-video-metadata-0.1 (crate (name "video-metadata") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1saq7z510iz16lbpg231hngddndz5j4xp2r8mbrh2y8k7cv1vr6n")))

(define-public crate-video-metadata-0.1 (crate (name "video-metadata") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ckysnmmlyhghb6a97ghz1slj3p4wi1nnfmib2bpbf6afpljimvh")))

(define-public crate-video-metadata-0.1 (crate (name "video-metadata") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1csgb7645q1lzq5rwflqxgqzpfczi68pncp5kgn11cc92hcfx4q8")))

(define-public crate-video-resize-0.1 (crate (name "video-resize") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "v_frame") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1ywiblqrzfgickn6048bpjidxwr5yj6zibagjgpyh6d5hn6ia6qp") (features (quote (("devel") ("default")))) (rust-version "1.61.0")))

(define-public crate-video-resize-0.1 (crate (name "video-resize") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (features (quote ("png"))) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "v_frame") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "yuvxyb") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1484b4sbipxfk7d3yv2jn6n5j86fsxhw2gfqdk7y8kmqz5zwfvdd") (features (quote (("devel") ("default")))) (rust-version "1.56.1")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1ww7s5wy9y9lqc993cb3xxa205yrq5filxlyb4ild76mpw95013a")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.1") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0a8rnkvsnkf692d70333ppqn7v25c4yl16b9nsgckwd23jp68kwi")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.2") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "19z20b4nl4j1z5yhpkrfsdvjzvp37yda59nvk0w0b2my2aylzsfj")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.3") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1xkm4ah4dyja9ghv2ssfrcyyq1jpy9k7jc023wxklzxz5djh6zqy")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.4") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1f46crxns678m8vcfv7n9g2978xz8vm02xzjbaka5flig43cmy4a")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.5") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1a33y8czgg4r84wy5zvjawggg1dqbdpzczzz33hcc0602knqzp3l")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.6") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1rj0x8k6f6xrw94qinjj8splqzzljhkv66kbj0y9s0fq2nqcyw7d")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.7") (deps (list (crate-dep (name "ffmpeg-next") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1ry4g1byp8lkn3gkbnnqjsa9n8az2bz1yvgx0cn5z6zc7vrx14w5")))

(define-public crate-video-rs-0.1 (crate (name "video-rs") (vers "0.1.8") (deps (list (crate-dep (name "ffmpeg-next") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "06gbkl3lyv0ns0mydcp4n7hgdc3x5rkng56frgl5cq8i8j0zc0d4")))

(define-public crate-video-rs-0.2 (crate (name "video-rs") (vers "0.2.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^5.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "14wx7hnqidfsi6kv6bzh8i0bcz85j4m3gi31kqs6wis2jlff2xpk")))

(define-public crate-video-rs-0.2 (crate (name "video-rs") (vers "0.2.1") (deps (list (crate-dep (name "ffmpeg-next") (req "^5.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0zhqgx5l3ya40c8dziv88f8ib20n121qiw7wbignhn4g6j61iq1a")))

(define-public crate-video-rs-0.2 (crate (name "video-rs") (vers "0.2.2") (deps (list (crate-dep (name "ffmpeg-next") (req "^5.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "00r3h7jbly9m9an4iawihyc1cigl0fghsv951ha41dph8xxyq9vy")))

(define-public crate-video-rs-0.2 (crate (name "video-rs") (vers "0.2.3") (deps (list (crate-dep (name "ffmpeg-next") (req "^5.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0vf5k76940pq2v7g6s5rfs6pm8jf0pggm6mlcn731m4snl4cfn84")))

(define-public crate-video-rs-0.2 (crate (name "video-rs") (vers "0.2.4") (deps (list (crate-dep (name "ffmpeg-next") (req "^5.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1yvx6z9vxra33zd6nhgj5x3qry9g4xv5sn5r867k2wj8lr737skg")))

(define-public crate-video-rs-0.2 (crate (name "video-rs") (vers "0.2.5") (deps (list (crate-dep (name "ffmpeg-next") (req "^5.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0agbpp9ck9mclyhwpm8yyklmmrjncm9h3c0ljccf4n8fl090x6q1")))

(define-public crate-video-rs-0.3 (crate (name "video-rs") (vers "0.3.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0r3j247s4a0pwkphzhzkmcbv2hbfif0awfql0dmkrzi0casiak7p")))

(define-public crate-video-rs-0.4 (crate (name "video-rs") (vers "0.4.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1yg9a60rhlgxyqil0pywxwlnz2ghl2b1r606b28ri6dys5c0pagi")))

(define-public crate-video-rs-0.4 (crate (name "video-rs") (vers "0.4.1") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0d8as8a4qr09ys4647rndshmhwbpr8skvbpyq461qcamjwqaamwg")))

(define-public crate-video-rs-0.5 (crate (name "video-rs") (vers "0.5.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0p9anp54zc62891si2sxrk8ixgdqagx18shqvg4mgg20maldhkmk")))

(define-public crate-video-rs-0.6 (crate (name "video-rs") (vers "0.6.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1w5giym0kfv49n3d0r5w9sdxr3rpwlshj25dfs88v6sldwxxvlyq")))

(define-public crate-video-rs-0.6 (crate (name "video-rs") (vers "0.6.1") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0cv1bifx6vfpgl1zrzb44k56varmn8xbw1y4j0l148mbas0x0gi4")))

(define-public crate-video-rs-0.7 (crate (name "video-rs") (vers "0.7.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0b2qb5cszwd93rf4l5ixxwhj0xi05ik4cq57wb93yy543zk4vp8y")))

(define-public crate-video-rs-0.7 (crate (name "video-rs") (vers "0.7.1") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0gb00qx45c8977d84y9f8d6jfl8gkq0sbknhl58amhv2wgm30ibm")))

(define-public crate-video-rs-0.7 (crate (name "video-rs") (vers "0.7.2") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.1") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1vfd8y3p6rdrr5yylik0xm4ppm6i4dqvvpfkkfyk7wygnwqwymgk")))

(define-public crate-video-rs-0.7 (crate (name "video-rs") (vers "0.7.3") (deps (list (crate-dep (name "ffmpeg-next") (req "^7.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1jw7wycq4rvwsqa8vbri1kmshfn3girxpljv19asafzxal51absy")))

(define-public crate-video-rs-0.7 (crate (name "video-rs") (vers "0.7.4") (deps (list (crate-dep (name "ffmpeg-next") (req "^7.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1djiq3hpldpik2f9y1a9sh86gr1m9ybpyv0vaxycfdqywzsq5qqf")))

(define-public crate-video-rs-0.8 (crate (name "video-rs") (vers "0.8.0") (deps (list (crate-dep (name "ffmpeg-next") (req "^7.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "136nfbqn6zzvn7cyjp9icds602q8zzysczc802pw4w3ccbl8hzy4")))

(define-public crate-video-rs-0.8 (crate (name "video-rs") (vers "0.8.1") (deps (list (crate-dep (name "ffmpeg-next") (req "^7.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "04g4n3ydyi42ccwzzkp9rwmyim0z64c9008dgqhvx3x10r9ibn55")))

(define-public crate-video-rs-adder-dep-0.4 (crate (name "video-rs-adder-dep") (vers "0.4.1") (deps (list (crate-dep (name "ffmpeg-next") (req "^6.0") (features (quote ("format" "codec" "software-resampling" "software-scaling"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "06bnb8nwksajzp2n2vjn98sglbhr9i0hisqk1m2vc06nqbvi5xzr")))

(define-public crate-video-summarizer-1 (crate (name "video-summarizer") (vers "1.2.1") (deps (list (crate-dep (name "guid-create") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "minimp3") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0rqwknw0vwxi8259vswyh61fvhiw78mixxdyd0vfpgagwj87gbja")))

(define-public crate-video-summarizer-1 (crate (name "video-summarizer") (vers "1.2.2") (deps (list (crate-dep (name "guid-create") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "minimp3") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "196cz78fcwyig6i6bgvqqx4xnmiszdqjdy6xv7r5lm4lsp1rl5cs")))

(define-public crate-video-summarizer-1 (crate (name "video-summarizer") (vers "1.2.3") (deps (list (crate-dep (name "guid-create") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "minimp3") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "03b8yiddvykb7ygi26jh4jjgvyqa8ipsx937rbmyq3zigplssrwg")))

(define-public crate-video-timecode-0.1 (crate (name "video-timecode") (vers "0.1.0") (hash "14h7alns2mhhmyzizim6aa8f9i7yqva8523cmvjk82vfshkjn40b")))

(define-public crate-video-timecode-0.2 (crate (name "video-timecode") (vers "0.2.0") (hash "04xcrj08lmbj1ib0fzixnrj0h4kxswy6q7wr8xb9sidad3jdv0b8")))

(define-public crate-video-timecode-0.2 (crate (name "video-timecode") (vers "0.2.1") (hash "1p0g90gm4clhry42kqppz2hd3ka8s6v0r27qm58rlc37a5g8nrzr")))

(define-public crate-video-timecode-0.2 (crate (name "video-timecode") (vers "0.2.2") (hash "1cmg3z5y0mnjdy0bvkd9a80p81lx6nz1njn8cnc9wlyrnlwi2y9v")))

(define-public crate-video-timecode-0.3 (crate (name "video-timecode") (vers "0.3.0") (hash "0f1754jb1randlkhmgq34vvkhb1x3qccqcyxjzxzf75h32hb83kz")))

(define-public crate-video-timecode-0.3 (crate (name "video-timecode") (vers "0.3.1") (hash "051w5bsmbxsmk95fh7xpnzwzjfng5mx1m8c23k2xhv6nn8xhjakf")))

(define-public crate-video-timecode-0.3 (crate (name "video-timecode") (vers "0.3.2") (hash "1xlva0aba82r36cgfg3km69hzidd02bmck9427bbd8q8jkr2a8mz")))

(define-public crate-video-timecode-0.4 (crate (name "video-timecode") (vers "0.4.0") (hash "10r0y3dhg5zk46zjsdn94ib2x2y03r49zsj3qylln8w8np3mw3ib")))

(define-public crate-video-timecode-0.5 (crate (name "video-timecode") (vers "0.5.0") (hash "12ha2mjg8rvsq1w5lacc8czk3nq6dv7idcrcm65yyfqsscnfj9y1")))

(define-public crate-video-timecode-0.5 (crate (name "video-timecode") (vers "0.5.1") (hash "0wf9y6psggxdickvmfj1n8s40rpjbwr5zpcnhadj7mrsvij0ksyf")))

(define-public crate-video-timecode-0.5 (crate (name "video-timecode") (vers "0.5.2") (hash "1cm32k8vdksxzl9k5g8n23vc4jmiwf5bksqi4vq90d3pxawinl70")))

(define-public crate-video-timecode-0.5 (crate (name "video-timecode") (vers "0.5.3") (hash "03m1sx1nqczap2a5dkr5admahg29lvy1lqlr8p3fbb1l21xcbjq8")))

(define-public crate-video-timecode-0.6 (crate (name "video-timecode") (vers "0.6.0") (hash "0qcq9sly8s8jg8hh15pc2kh2f3db3fmgzaghs4bb1ja3nam50l1z")))

(define-public crate-video-timecode-0.6 (crate (name "video-timecode") (vers "0.6.1") (hash "0igz36pp1p1k7790i2qlh1ampb664girrw3mbihf0dhxf4yr8lgf")))

(define-public crate-video2ascii-0.1 (crate (name "video2ascii") (vers "0.1.0") (deps (list (crate-dep (name "ansi_rgb") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.63.0") (features (quote ("rgb"))) (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)))) (hash "0kpv9m356azjcwrq0ify7qcckv77d4qmrln1cipwqfiqf1kfg89n")))

(define-public crate-video2ascii-0.1 (crate (name "video2ascii") (vers "0.1.1") (deps (list (crate-dep (name "ansi_rgb") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.63.0") (features (quote ("rgb"))) (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)))) (hash "1r76zi1f3m2lgvwq18chd5qsphapkrvw6pj9akv56dic3ycjzkg5")))

(define-public crate-video_20_publish_crate-0.1 (crate (name "video_20_publish_crate") (vers "0.1.0") (hash "07zzdq79yh1q0z3wddazyp6m99daxlfxxidwnkg9cg07xv7797x4") (yanked #t)))

(define-public crate-video_amogusifier-1 (crate (name "video_amogusifier") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "filehash-rs") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1i088a02g8qln1hfs4b5grklh01j9schdw3jccd823sz906bjfcj")))

(define-public crate-video_amogusifier-1 (crate (name "video_amogusifier") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "filehash-rs") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1714km3v0wjmaspgcgry7z9k37346n0lalmbd11ymzdji3a0lhr3")))

(define-public crate-video_amogusifier-1 (crate (name "video_amogusifier") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "filehash-rs") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "08y3sl1425qlfjc7ysx4ng9mzqn59c3dv090p8b4hn2x7x2sydvh")))

(define-public crate-video_hash_filesystem_cache-0.1 (crate (name "video_hash_filesystem_cache") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "generic_filesystem_cache") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vid_dup_finder_lib") (req "^0.1") (features (quote ("app_only_fns"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0a8xm55ilhcwpas35xk4wahmxvgfq5fsrn2yg273rz63i06yikz6") (features (quote (("parallel_loading") ("default" "parallel_loading"))))))

(define-public crate-video_ludo-1 (crate (name "video_ludo") (vers "1.0.0") (deps (list (crate-dep (name "ffmpeg-sys") (req "^3.4.1") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.25.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "ringbuf") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "07rbrbbdjyagnibrpnj6sn03bb59dgwjwhp42na52wazlg37w0ab")))

(define-public crate-video_web_page_generator-0.1 (crate (name "video_web_page_generator") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0kqx5sckzy62qwpbia7dmcz6j4fbp7y70g6xdw007fy2bgxnm5il")))

(define-public crate-video_web_page_generator-0.1 (crate (name "video_web_page_generator") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rpmcxgx6k0gd09lqlc7hpiw533k30rf63p1jrjz0zrim6dvawr2")))

(define-public crate-video_web_page_generator-0.1 (crate (name "video_web_page_generator") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11zsbp26fdjbqx23lrhsixxfp5bqvrvahhwwj3rmm57vw7zvwld4")))

(define-public crate-video_web_page_generator-0.2 (crate (name "video_web_page_generator") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15pw2qrmpixcdgz81gd13hykynklin3xgrfnv0k0fs4l5s0qkhmk")))

(define-public crate-video_web_page_generator-0.2 (crate (name "video_web_page_generator") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cgpa9jm0ghr2rwpcys2lilpv6a0x4xgyw36ki7sc04krj973v8v")))

(define-public crate-videocore-0.1 (crate (name "videocore") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "13amqfvcxcjz9cbhz31j8csdi529849x8c4nhv9hrfifvd8sq3ax")))

(define-public crate-videocore-0.1 (crate (name "videocore") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0vj6gc8gqdmdjbj441xgm0nj3ld2f9jys9p3xjgval0fim4nsvhk")))

(define-public crate-videocore-0.1 (crate (name "videocore") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0mj3cngvn3g01zwdmr1n1gng68dc9pzvls3py4nqwvjrlbznhsmw")))

(define-public crate-videocore-0.1 (crate (name "videocore") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0r9v3bwmk4csv3yhbaf4gdn5003l7p27lck78mdvqz9vg69dmkn0")))

(define-public crate-videocore-gencmd-0.1 (crate (name "videocore-gencmd") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "edwardium_logger") (req "^1.2.0") (features (quote ("colored_stderr_output"))) (optional #t) (kind 0)) (crate-dep (name "edwardium_logger") (req "^1.2.0") (features (quote ("colored_stderr_output"))) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "06y0bqj3blfxsz6mxay8gbzgsjrc51gigmfflx8kzi0447c0yrym") (features (quote (("run_bindgen" "bindgen") ("mock_vc_ffi" "once_cell") ("global_singleton" "once_cell") ("default" "global_singleton") ("cli_app" "clap" "anyhow" "edwardium_logger"))))))

(define-public crate-videostream-0.1 (crate (name "videostream") (vers "0.1.0") (deps (list (crate-dep (name "videostream-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0klksf3wmlja2dgadi0d5zj8d6z0y64gq4s8pnwbw0mnm6bx6rwb")))

(define-public crate-videostream-0.2 (crate (name "videostream") (vers "0.2.0") (deps (list (crate-dep (name "videostream-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "150hbrvibxrmf7id7kh1ik6j54lhp75xxvgmq97gy3fapac1dwzy")))

(define-public crate-videostream-0.3 (crate (name "videostream") (vers "0.3.4") (deps (list (crate-dep (name "videostream-sys") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1f7hbs9g25a1klv7scx93s4nsf1flg5jl1i7kjip7ljd498zv8qa")))

(define-public crate-videostream-0.4 (crate (name "videostream") (vers "0.4.0") (deps (list (crate-dep (name "videostream-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "08mkz431j6hqnw9d4ggrw6272qglvx4lwb9qr9vrpwfav3aw88wj")))

(define-public crate-videostream-0.4 (crate (name "videostream") (vers "0.4.1") (deps (list (crate-dep (name "videostream-sys") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0whwsmg8zydhrpikkbrykk86n6vrahx6ldbsrr8kdwpc0djhx387")))

(define-public crate-videostream-0.5 (crate (name "videostream") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "13aq9i0dc16m8zjlbdhhwqvdspwb5xa433rymzg65hcxg0phy5j9")))

(define-public crate-videostream-0.5 (crate (name "videostream") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0vkynwg633g95nisnvld3dda57vww3w1ki1h4bw7cswxyrvmd9j6")))

(define-public crate-videostream-0.5 (crate (name "videostream") (vers "0.5.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1rb1n7z50q8wpcbja3jf9bc61040fkm74zvfp4nx2n586kpx3zib")))

(define-public crate-videostream-0.4 (crate (name "videostream") (vers "0.4.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0xvsgrah2yaw1lagfqc68gqq4phcl19nkkpqjyi679y6xvam85m5")))

(define-public crate-videostream-0.4 (crate (name "videostream") (vers "0.4.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1bc0rgd1h5zx513c2k6wcp58ky8wbfdf3z4ix4dgzkcvmq2snz7j")))

(define-public crate-videostream-0.6 (crate (name "videostream") (vers "0.6.0") (deps (list (crate-dep (name "dma-buf") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1r1dlay8546jmh76x7vqivmnhf8h2sdzjhvxz18spcgp681ryfnd")))

(define-public crate-videostream-0.6 (crate (name "videostream") (vers "0.6.1") (deps (list (crate-dep (name "dma-buf") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "13ym47jxvkxvkbnlrf66qxpris7a6n91lkjwczimj007gw40p04p")))

(define-public crate-videostream-0.6 (crate (name "videostream") (vers "0.6.2") (deps (list (crate-dep (name "dma-buf") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1wa2crc2gnll5137124g12vfr4lxssg4rynf50dl6iaba6xz9jlf")))

(define-public crate-videostream-0.6 (crate (name "videostream") (vers "0.6.3") (deps (list (crate-dep (name "dma-buf") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "videostream-sys") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "084zb9jvpybv55s8j30lwafr485x055b6qw78801rrbywsx5ai2m")))

(define-public crate-videostream-0.6 (crate (name "videostream") (vers "0.6.4") (deps (list (crate-dep (name "dma-buf") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "unix-ts") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "videostream-sys") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1n997kdrjp7xhyrsg0anqbkpq1jzwnp7zhgykzp680bk37682ms9")))

(define-public crate-videostream-0.6 (crate (name "videostream") (vers "0.6.5") (deps (list (crate-dep (name "dma-buf") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "unix-ts") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "videostream-sys") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "01c74iixsay0cwxmpywy921wmyj1paagnjf3gpd0v3lr4znqah9r")))

(define-public crate-videostream-0.7 (crate (name "videostream") (vers "0.7.0") (deps (list (crate-dep (name "dma-buf") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "unix-ts") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "videostream-sys") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1xcb371f450n4sarbyhnx6adyn0rjsiqs46k88kmp3xh77lkydn2")))

(define-public crate-videostream-0.7 (crate (name "videostream") (vers "0.7.1") (deps (list (crate-dep (name "dma-buf") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^3.0.0") (default-features #t) (kind 2)) (crate-dep (name "unix-ts") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "videostream-sys") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0j1rrv36aigxmwajxy3qqcvhwih49rdqhdc0kyvahy5sx41lr1fw")))

(define-public crate-videostream-0.8 (crate (name "videostream") (vers "0.8.0") (deps (list (crate-dep (name "dma-buf") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^3.1.1") (default-features #t) (kind 2)) (crate-dep (name "unix-ts") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "videostream-sys") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1vash5mcys1004mznb04w8aqmkw9m5j52dvsfvp04v6b23kdx27b")))

(define-public crate-videostream-sys-0.1 (crate (name "videostream-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b2ckf1nlkvzpcypgbbkki9qgzp86h7li2ih98n63fi2mkp5y0vh")))

(define-public crate-videostream-sys-0.2 (crate (name "videostream-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bsi0chpr26jxlv9l683f9ym9ssg8vmv24wmp6dw44cr9sppjhrs")))

(define-public crate-videostream-sys-0.3 (crate (name "videostream-sys") (vers "0.3.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00yx4dcphwrj000jd73mn7zsh46i5i4c6pbndp9m91a0wv1fbdi9")))

(define-public crate-videostream-sys-0.4 (crate (name "videostream-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b7lyr84wn5127dig3h77h0f0c2v1lg6jhz8gj8pmj9976pl2ldd")))

(define-public crate-videostream-sys-0.4 (crate (name "videostream-sys") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "07mjfsqkv4l9k0yn4nbn8mqnzpbnyf5wny21h1igppkvmhbvv40g")))

(define-public crate-videostream-sys-0.5 (crate (name "videostream-sys") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rin4kh8xpjxma1pjkj54xjbj5cfaz7frwcmm5fg9mdq11p49jri")))

(define-public crate-videostream-sys-0.5 (crate (name "videostream-sys") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p7fndz8h97iri00jml7pp28c28i1ycfrs89cgm4qvn0gvhxbmw2")))

(define-public crate-videostream-sys-0.5 (crate (name "videostream-sys") (vers "0.5.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lnrsj7cn52pfix712spxpx2qalgrjw7jbixrhq6jx4037drw2ib")))

(define-public crate-videostream-sys-0.4 (crate (name "videostream-sys") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k4cxr4s2wxngycma4rkir1abjpixqnvxk0k5hd239mwnnxknvab")))

(define-public crate-videostream-sys-0.4 (crate (name "videostream-sys") (vers "0.4.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08waq3w4pmyamrzz7lbr8mivkh24dzrl84dvdn5h6nmfa9xjazrg")))

(define-public crate-videostream-sys-0.6 (crate (name "videostream-sys") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yd8is2k4k72vjx4sax0lv7r583fd9vm0wpsbfgk1rx33231lyd6")))

(define-public crate-videostream-sys-0.6 (crate (name "videostream-sys") (vers "0.6.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cf8s8d40x5ic0y7lyiwpxyjvvikpw3yyy41fksxdn3y58qjyy5z")))

(define-public crate-videostream-sys-0.6 (crate (name "videostream-sys") (vers "0.6.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p6hldj6sj0k2916w2hrzi88pxz749r7zj7f1cb64d3qdb0b2xlx")))

(define-public crate-videostream-sys-0.6 (crate (name "videostream-sys") (vers "0.6.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05n02rsqs4fs271jwcwwy17vsfclncr3hwaqz39y0nqqwcjxlsjc")))

(define-public crate-videostream-sys-0.6 (crate (name "videostream-sys") (vers "0.6.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qcd1sp27i6qdbvskvs8xa570ivw9dhalpv78lj1r4xi2jy4876b")))

(define-public crate-videostream-sys-0.6 (crate (name "videostream-sys") (vers "0.6.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11kd6lqny16x01imcyabd0bskdl0skp37vq7709x69pvjg929qy8")))

(define-public crate-videostream-sys-0.7 (crate (name "videostream-sys") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r46wp7b06f67m8lai0k585a7jpn4i1ll1ri58jb6b5w6bg2gmxc")))

(define-public crate-videostream-sys-0.7 (crate (name "videostream-sys") (vers "0.7.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lny3xpn2msm4k0z9m7ngff0i6450qxj8qkill7vqwa53fb5zahv")))

(define-public crate-videostream-sys-0.8 (crate (name "videostream-sys") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "169gjrdpsfaym0a4ln0jb3x6mvqq6yclmn32c34h8xni044mxvw7")))

(define-public crate-videotoolbox-sys-0.0.1 (crate (name "videotoolbox-sys") (vers "0.0.1") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "core-graphics") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "coremedia-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "corevideo-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14ygkixglkxqafg76fvln5lh5wf6jnhr51fv9pdb37jk26sq5g4w")))

(define-public crate-videotoolbox-sys-0.0.2 (crate (name "videotoolbox-sys") (vers "0.0.2") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "coremedia-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "corevideo-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sw77smy6y0rg277z1v043sf6dk4n7g9wls95sbqr72mnsvwv37z")))

(define-public crate-videotorno-0.1 (crate (name "videotorno") (vers "0.1.0") (hash "1kmzlygkixyya80zl8hkn6qp514zllhhs3k0dd0iayzbm4kgzpby")))

