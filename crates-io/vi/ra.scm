(define-module (crates-io vi ra) #:use-module (crates-io))

(define-public crate-viral32111-stomp-0.1 (crate (name "viral32111-stomp") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1cl8c8cbv4hq311higw9q93m39i27n6mylb4bm14crddvgqrxz78") (rust-version "1.78")))

(define-public crate-viral32111-stomp-0.1 (crate (name "viral32111-stomp") (vers "0.1.2") (deps (list (crate-dep (name "flate2") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "15wg477479616qx4w03a4pldhhgnp675bnk5mrkvi8ys1njh0732") (rust-version "1.77")))

(define-public crate-viral32111-stomp-0.1 (crate (name "viral32111-stomp") (vers "0.1.3") (deps (list (crate-dep (name "flate2") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "06zhg83jmkvhbqy67q40d040357hnrr4pnp98knv9wnrp2zvhk7s") (rust-version "1.77")))

(define-public crate-viral32111-xml-0.1 (crate (name "viral32111-xml") (vers "0.1.0") (hash "0b7jny39svszyk2cfqrps5cjgq6fqvvzpx6yanv2x6c49mjdjdbs") (rust-version "1.77")))

