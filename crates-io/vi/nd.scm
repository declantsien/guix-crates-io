(define-module (crates-io vi nd) #:use-module (crates-io))

(define-public crate-vindicator-0.1 (crate (name "vindicator") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "noisy_float") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0dy6y2g98ynq47f7h8qf075d60s8gjda4zjbnl9vh5wvrsmq38sf")))

