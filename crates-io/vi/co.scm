(define-module (crates-io vi co) #:use-module (crates-io))

(define-public crate-vicon-sys-0.1 (crate (name "vicon-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "snafu") (req "^0.7.1") (kind 0)))) (hash "09xbhhk1k8xsn27z6fwls5xjhv7zs8pv0hgdj9qnh4q3fdbzlf64")))

(define-public crate-vicon-sys-0.2 (crate (name "vicon-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "snafu") (req "^0.7.1") (kind 0)))) (hash "0ahzahjv9gb8lkcwl6zz65wjvl9a32dcv46s129a69z9zfphnsmb")))

