(define-module (crates-io vi pi) #:use-module (crates-io))

(define-public crate-vipi-0.1 (crate (name "vipi") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1zqbp499bzwkrk1xm9nvbh55b0w5nd2n8jvv896zhcwhyq9pwdvp")))

(define-public crate-vipi-0.1 (crate (name "vipi") (vers "0.1.2") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1ygj3sb024zmfdivgzka64igmq9wzmvcrz6a5vyb5zzkrqzlyf2y")))

(define-public crate-vipi-0.1 (crate (name "vipi") (vers "0.1.3") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1y4x3y1kp9l24zx59f7if7159wj07k4p93xmjc7g1x6bdc5vnlc3")))

(define-public crate-vipi-0.1 (crate (name "vipi") (vers "0.1.4") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0wga0q73zlrmgkzvwl0rsnkj82c3j6fx6p7kl5lvkaqmmh9jgnpd")))

(define-public crate-vipi-0.1 (crate (name "vipi") (vers "0.1.5") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1h0k6xxlna7y46rv45f6m878y5dwwqfcc4vj0ygq63zisj48mnxm")))

(define-public crate-vipi-0.1 (crate (name "vipi") (vers "0.1.6") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1lnb1m8iqnq0fr17l8sjr5yiks9inkbczca3xmwfmb7kssn0dvvi")))

