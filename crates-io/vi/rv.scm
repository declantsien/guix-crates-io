(define-module (crates-io vi rv) #:use-module (crates-io))

(define-public crate-virvadb-0.1 (crate (name "virvadb") (vers "0.1.0") (deps (list (crate-dep (name "rust-ini") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1q0bv49zs4a9rbxhkci0axs5ki4437lqranhqymyyhizly38yfzv")))

(define-public crate-virvadb-0.1 (crate (name "virvadb") (vers "0.1.1") (deps (list (crate-dep (name "rust-ini") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0acijjl9abf8v2kdmchalpmjj2112rih6hindmp0pfc6w06067y3")))

