(define-module (crates-io vi ll) #:use-module (crates-io))

(define-public crate-village-0.1 (crate (name "village") (vers "0.1.0") (hash "095jd9pnixcf0byp2bfwv7wqyyvrp9w8r74kqhrvy1p5m3ha9y7m")))

(define-public crate-villain-0.0.1 (crate (name "villain") (vers "0.0.1") (deps (list (crate-dep (name "html") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1r58qxsjpxa2m4m6dprqkxlwmjaq6rbvkk6mm3hjj66sv5ml67cp")))

(define-public crate-villaleobos_auth_service-0.1 (crate (name "villaleobos_auth_service") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rgi2mnd78dpmib2l1da5nwz9a2iayn00sylzk41gl1kk1ajc7zj") (yanked #t)))

(define-public crate-villey_utils_lib-0.1 (crate (name "villey_utils_lib") (vers "0.1.0") (hash "0bsj20bqk221h7zix51nq2r278l9n6n02r0zy5zw6y86nbg1d1gp")))

