(define-module (crates-io vi da) #:use-module (crates-io))

(define-public crate-vidar-0.1 (crate (name "vidar") (vers "0.1.0") (deps (list (crate-dep (name "derive_builder") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1lw4gq8vidnk96z02hysg1h4qkrj2lsqby746xy16k0kpq2wx165")))

