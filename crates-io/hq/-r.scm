(define-module (crates-io hq -r) #:use-module (crates-io))

(define-public crate-hq-rs-0.1 (crate (name "hq-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hcl-rs") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)))) (hash "0kjdwci6rpf33fm98zknnl7149j3zvynp0gf4gwiqsy1hpljb2wk")))

(define-public crate-hq-rs-0.2 (crate (name "hq-rs") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hcl-rs") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)))) (hash "1v0fiyx072ylnmqyjvsvimqlyj3y4q12bc6w0i7lqcih426c6pii")))

(define-public crate-hq-rs-0.3 (crate (name "hq-rs") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hcl-rs") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)))) (hash "029qzrbclc7m54v7ag27smpkfz32wkqbaalcxscxdjgnd0zsfpc5")))

(define-public crate-hq-rs-0.4 (crate (name "hq-rs") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hcl-rs") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)))) (hash "0ns47dp7iia13ikb0ngi5nhdl4b122cxpspk0154xnc7wxxfpz4i")))

(define-public crate-hq-rs-0.5 (crate (name "hq-rs") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hcl-rs") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)))) (hash "0qwh82dsjjsh9wfjshckqhskkx45ga0pyn9ap1mcrhvxj3gc7w93")))

