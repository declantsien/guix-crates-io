(define-module (crates-io sc am) #:use-module (crates-io))

(define-public crate-scambio-0.1 (crate (name "scambio") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-threaded" "sync"))) (default-features #t) (kind 2)))) (hash "0ih2a89kdxqnmslwnxx0jz8fiwg5qp0sssamfkpcjmmsln213gyh")))

(define-public crate-scambio-0.2 (crate (name "scambio") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-threaded" "sync"))) (default-features #t) (kind 2)))) (hash "00jw5y2fjmwy8vanvr8sl1hb8n5gsa6zi9sr5yyvm865hvmpa2ly")))

(define-public crate-scambio-0.2 (crate (name "scambio") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-threaded" "sync"))) (default-features #t) (kind 2)))) (hash "1rrz8x7bwjny8jzrgpd2rqiynhfgmblj00x4yz2yrcvq3j55iv9x")))

