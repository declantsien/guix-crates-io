(define-module (crates-io sc ia) #:use-module (crates-io))

(define-public crate-scialg-0.1 (crate (name "scialg") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0zxqcz2dkgs72rh4z31xiirq2g7vspfcljdd5dhbc587adx2yvj8")))

(define-public crate-scialg-0.2 (crate (name "scialg") (vers "0.2.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "06lcyypnh6m98cgqy9ajvi18cjd3qpgs0r33j1n161sfwrwr21hn")))

(define-public crate-scialg-0.3 (crate (name "scialg") (vers "0.3.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0b6ih2qh1dnj3vqlg820la1rx9yw3rvjmzgwc97vmiabx85rx0r3")))

