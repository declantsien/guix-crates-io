(define-module (crates-io sc yt) #:use-module (crates-io))

(define-public crate-scytale-0.1 (crate (name "scytale") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_json_path") (req "^0.6.7") (default-features #t) (kind 0)))) (hash "0aba8hh1wmh5rnwgfl98r88ydc1gpa4zw0d2qp4khmp8v818n228")))

(define-public crate-scythe-0.1 (crate (name "scythe") (vers "0.1.0") (hash "0mwfd87b4q5gxkj0zgpsr9hjyv0sinq5yyn9srv113wfs29mx0yw")))

(define-public crate-scythe-0.1 (crate (name "scythe") (vers "0.1.1") (deps (list (crate-dep (name "scythe_data") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scythe_paths") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scythe_platform") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1h84bvfw6jqwhxmbpq7hyvxqpzi3shsgj0ciwjp50r8hqzfxw0g0") (features (quote (("platform" "scythe_platform") ("paths" "scythe_paths") ("default" "data" "paths" "platform") ("data" "scythe_data"))))))

(define-public crate-scythe_data-0.1 (crate (name "scythe_data") (vers "0.1.0") (hash "1fw8by5imcqxfkqmqn87x18aynyk5wn5lva5szzb4qdprf559pvx")))

(define-public crate-scythe_data-0.1 (crate (name "scythe_data") (vers "0.1.1") (hash "0k0zwxj0b5bal1mf3k6nl367kf6wx9nz0r7flbgh21rvmsccp80v")))

(define-public crate-scythe_paths-0.1 (crate (name "scythe_paths") (vers "0.1.0") (hash "0988sbi21fml9m54nws9nlcqidd1v1f2i9nrbaz8jwgrmvshj19v")))

(define-public crate-scythe_paths-0.1 (crate (name "scythe_paths") (vers "0.1.1") (hash "04bmvq7y828zlqh7xhps81hwf784l03ikqwq2v5a57yz905qiph6")))

(define-public crate-scythe_platform-0.1 (crate (name "scythe_platform") (vers "0.1.0") (hash "1bscdlz2cc4v1gpi80y0164dxn3v0ad9fmd8vg16fzbhnkmychr8")))

(define-public crate-scythe_platform-0.1 (crate (name "scythe_platform") (vers "0.1.1") (hash "0r4isg5q57n7yzwws4bgglg0fjyp40lk3lr0qn1mschlppb8qr25")))

