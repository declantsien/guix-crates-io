(define-module (crates-io sc si) #:use-module (crates-io))

(define-public crate-scsi-0.1 (crate (name "scsi") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (kind 0)))) (hash "0nr5bci4ifqcf4fk4bqj96jd0by85plyk1qpvxncr2q17rcshvwy")))

(define-public crate-scsi-0.1 (crate (name "scsi") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (kind 0)))) (hash "08x6j928n25i86dc9yj2mbw87qg5znbngqh8h2z2jcgqksklnxn5")))

(define-public crate-scsi-0.2 (crate (name "scsi") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (kind 0)))) (hash "0m5id8w5p8vnmwlmw6a24vc51wfs4x6w1mmrrb8az9i3lyi8g7lf")))

(define-public crate-scsi-0.2 (crate (name "scsi") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (kind 0)))) (hash "0dmx6a4izq3vsbihdh9mbyqhfsabs67vwsnx1dxk09h4lhl8nsxj")))

(define-public crate-scsir-0.1 (crate (name "scsir") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield-msb") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_System_IO" "Win32_System_Ioctl" "Win32_Storage_FileSystem" "Win32_Storage_IscsiDisc"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "07cck75lv40c2mf45ha454y10idcpxxv3cq4g49kl45dqwa7qxzc")))

