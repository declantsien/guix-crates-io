(define-module (crates-io sc hr) #:use-module (crates-io))

(define-public crate-schrift-rs-0.1 (crate (name "schrift-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1mzyrb7cmh541bx1kpr1wqvs0hky7ycjmklwyfg5jzf1srv96fvm")))

(define-public crate-schroedinger_box-0.0.0 (crate (name "schroedinger_box") (vers "0.0.0") (hash "1p6zf2pppcv21yppc29jrbc3418xiivz7lwdi7misk3wrjfw1rj7")))

(define-public crate-schroedinger_box-0.0.1 (crate (name "schroedinger_box") (vers "0.0.1") (hash "0fv1sa5vzwj8pnd2szzijg1krcj11w118xmmf26vlm3ffcdh5zh6")))

(define-public crate-schroedinger_box-0.0.2 (crate (name "schroedinger_box") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "03xkwn51sm0dk5xajpwkhvx9b8aymq2vwzc3q84ih59jjm8smcmj")))

(define-public crate-schroidnger_equation-0.1 (crate (name "schroidnger_equation") (vers "0.1.0") (hash "1sr3zghzc18zmpgjl7lb0p6ryfpkvnaaj71jjnam07agayqla9w7")))

