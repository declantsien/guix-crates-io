(define-module (crates-io sc ol) #:use-module (crates-io))

(define-public crate-scol-0.1 (crate (name "scol") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "16i2zlm4lajzwjiwrm10ci3pfr3n75691hqy6r4zf5gj63sw92lf")))

(define-public crate-scol-0.1 (crate (name "scol") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "111msjw4z22ihd94hqr24p99j3xz9x83a0cd2lw9pkg7js5gq1mz")))

(define-public crate-scol-0.1 (crate (name "scol") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1knq52b684x659m08ghqwl7xh3jihhk8b91yzrhbyn4as2aqg1q5")))

(define-public crate-scol-0.1 (crate (name "scol") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0lkw922vd65lzfnzplljix90v7138qv9gan4g9z9an6kc7wac76p")))

(define-public crate-scolor-0.1 (crate (name "scolor") (vers "0.1.0") (hash "0ybkchyp3vyv3zn57fq0qh3girfwbhhsb7yayrbyyx71jjn92qlh")))

(define-public crate-scolor-1 (crate (name "scolor") (vers "1.0.0") (hash "073736yysbc2i53qffg667g6mh1vacnw8pbzpfgyss86pgxp1487")))

(define-public crate-scolor-2 (crate (name "scolor") (vers "2.0.0") (hash "0flfby2ppkv2bq2l81lyxghjg0y8bq8swcwmxxsmmljxq5fbs4km")))

(define-public crate-scolor-4 (crate (name "scolor") (vers "4.0.0") (hash "1ly3w4rnplasz9mlpwgcgyyzjymqfl3nwad5c9kk2yd87m2fvalw")))

(define-public crate-scolor-4 (crate (name "scolor") (vers "4.0.1") (hash "158lslg5k0s9yi4ih62cfpvgnchkpcksyr8l25ga03xcibsdk00l")))

(define-public crate-scolor-4 (crate (name "scolor") (vers "4.0.2") (hash "0kdap113nvwsg28akz0a3xkc33dc9fhhx2w47ikdrjf02pyl416q")))

(define-public crate-scolor-4 (crate (name "scolor") (vers "4.1.0") (hash "041n4c82ihis0v58pxwl00n7dapqmkw8pr0lmm85jswpy0l4h46z")))

(define-public crate-scolor-4 (crate (name "scolor") (vers "4.2.0") (hash "10vxw0qiyp9mkrg8r93xlxp0fpbxa381l6zgsvix8s5ak3yrvddj")))

(define-public crate-scolor-4 (crate (name "scolor") (vers "4.2.1") (hash "0xmgn2ivg643nhmjg537xkjxrdh36m89fwiyhpqfig7fnw9ln19a")))

(define-public crate-scolor-5 (crate (name "scolor") (vers "5.0.0") (hash "1bvvk8xliczpahgcx8qhplvyan70lfbz8j8kb7ikf6w59fvkq2r6")))

(define-public crate-scolor-6 (crate (name "scolor") (vers "6.0.0") (hash "1hsaysnnaigqbyy5nq93gc83ffhh28q1m03gjqfrxfdqcmd9jz27")))

(define-public crate-scolor-7 (crate (name "scolor") (vers "7.0.0") (hash "1xjfmzjis83var0dpgx5i0fxjqmizrpbjnhksm8piflvpz9zrh2y") (features (quote (("zero-cost") ("default"))))))

(define-public crate-scolor-7 (crate (name "scolor") (vers "7.0.1") (hash "1wk7sq2ns58hk94xg1wgi7k427lm1rhjga7ihfwxfqvlrq6x06i6") (features (quote (("zero-cost") ("default"))))))

(define-public crate-scolor-8 (crate (name "scolor") (vers "8.0.0") (hash "1b5v2icinn6d6rz1gabs2i3pm63cinwxzx540x5a0kac7z25m3px") (features (quote (("zero-cost") ("default"))))))

