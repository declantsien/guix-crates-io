(define-module (crates-io sc hu) #:use-module (crates-io))

(define-public crate-schubfach-0.0.1 (crate (name "schubfach") (vers "0.0.1") (deps (list (crate-dep (name "ilog") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "oorandom") (req "^11") (default-features #t) (kind 2)))) (hash "0ixmvs2ffj3hczlr9vsgsvb0fdh2wqq4xa0215c0qzfpkn0xmqyp")))

(define-public crate-schuppe-0.0.1 (crate (name "schuppe") (vers "0.0.1") (hash "10mk46k4d1k315wndw4fckjy2hjvwllqszlxp72gdpikff5vshmq")))

(define-public crate-schuppe-0.0.2 (crate (name "schuppe") (vers "0.0.2") (hash "1c8xnp5vzd4ymhzaqkafrkj20kmlfwbwmi8a2z98lpckga5rwnkb")))

