(define-module (crates-io sc or) #:use-module (crates-io))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1g7lg887zb7ar6b2jashxv1dxn7nx2gj94r4dyz2b2xwhbaiz5sk")))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.2") (deps (list (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0wy7xz5nmszll6f5gip2cyp8lmfja846wqhmyiarzafn6fkma8l7")))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "027ahhjpjy8i5swaxdmvpd4fw9l4pkpy4vxccql9v5yww3j792fn")))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.4") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0fcdgg16jmhd9w41p4v89bbpl2q75qz4q6grdr7zcn13a32lsmyz")))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.6") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0s94k1ndqvbqza2ixbjmchzrw16z09wfizq94jp7rk388nkj4z52") (yanked #t)))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.7") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1izgfzibmhxxgh175b43pxi1bb8b6f5d5p6pr2bc41493xfpjdz1")))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.8") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1jd6x3ck3dh2pm9gygns536qxi57g7qc7460kn5mh6jqfr0qwjzg")))

(define-public crate-scorched-0.2 (crate (name "scorched") (vers "0.2.9") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1bni521ad5qxp3vdcmfbxail6i51wrg2q4g5bh42r2s86qc0inmz")))

(define-public crate-scorched-0.3 (crate (name "scorched") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1b1139jqvycbrnmqwp6xymq36qc188lg8kbb1n0fj8p70zj85fz5")))

(define-public crate-scorched-0.3 (crate (name "scorched") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1jrzvp7qf6fyay1kv3ggp47vnnd86bzcis9323j1l8wdi7f98cpc")))

(define-public crate-scorched-0.4 (crate (name "scorched") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1yis3vpahpgzwmk5qns2n4gw8zcqz5lqfr9m7r74zvgpizv7bmd2") (yanked #t)))

(define-public crate-scorched-0.4 (crate (name "scorched") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "10kis902fy1vk7w8ibkc8faij6p8id98ks77l1yga34lw6r3h4vk")))

(define-public crate-scorched-0.4 (crate (name "scorched") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1c65i6y6plbgssbx3a6a21b00rl4ykcya3c7h5jk05g6blbrh2hy")))

(define-public crate-scorched-0.4 (crate (name "scorched") (vers "0.4.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0rsz33x4l13x61w8k5hszm017b2b9dwz63g3wbrj24wjm2f8v2bz")))

(define-public crate-scorched-0.4 (crate (name "scorched") (vers "0.4.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1w9skiwgwb3zp80dn7kcjjgpswqvssjywrpn0c8kri8p87ii2y57")))

(define-public crate-scorched-0.4 (crate (name "scorched") (vers "0.4.5") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0n8q6c71krr02wznics4krjnqk7cpi5a0vmqrbyz703xxcjp22h0")))

(define-public crate-scorched-0.5 (crate (name "scorched") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "064m7a71nnf1f5m8ha6wh3syzacwc91ii6f5bfvpr1f40aipj2wr")))

(define-public crate-scorched-0.5 (crate (name "scorched") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0fhigpkid2517l0krzdk8wfy75jwymrhrhi5cmckxb2nwwvw2q8h")))

(define-public crate-scorched-0.5 (crate (name "scorched") (vers "0.5.2") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)))) (hash "14crzc576qv0va8k579p268i9v9hibfai68ibpy03sd5xnpni7pd")))

(define-public crate-scorched-0.5 (crate (name "scorched") (vers "0.5.3") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)))) (hash "1x1lqc85yw7p5p20xhkpjlllvvchz1hg2xmr7q9pggv939w8iijb")))

(define-public crate-score-0.0.1 (crate (name "score") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.24.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rouille") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0djhgin5bdxj5nfiqfd3k3kgycrshphmfzrjbqsw6g4was1qg0ff")))

(define-public crate-score-0.1 (crate (name "score") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.24.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rouille") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ma2s8bpa7g2fhpi9qi0nf5r4hqa9111pixadj3f4sm1w3mh1w5w")))

(define-public crate-score-0.2 (crate (name "score") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.24.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rouille") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "170p1lsnk1fk58qi3hi06zv8fgshr6nfxvlpdmnr6lrvfhm26nqp")))

(define-public crate-score-tracker-1 (crate (name "score-tracker") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "i18n-embed") (req "^0.14.1") (features (quote ("fluent-system" "desktop-requester"))) (default-features #t) (kind 0)) (crate-dep (name "i18n-embed-fl") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "relm4") (req "^0.7.0-beta.2") (features (quote ("macros" "libadwaita"))) (default-features #t) (kind 0)) (crate-dep (name "relm4-icons") (req "^0.7.0-alpha.2") (features (quote ("table" "hourglass" "plus" "person-add-regular" "person-subtract-regular" "cross-filled" "play" "pause" "refresh" "delete-filled" "cross"))) (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "1pfw1qglyp8dzfcsm9fggni256hx7baqj0xmrx3rcphxfgkayrx2")))

(define-public crate-score-tracker-1 (crate (name "score-tracker") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "i18n-embed") (req "^0.14.1") (features (quote ("fluent-system" "desktop-requester"))) (default-features #t) (kind 0)) (crate-dep (name "i18n-embed-fl") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "relm4") (req "^0.7.0-beta.2") (features (quote ("macros" "libadwaita"))) (default-features #t) (kind 0)) (crate-dep (name "relm4-icons") (req "^0.7.0-alpha.2") (features (quote ("table" "hourglass" "plus" "person-add-regular" "person-subtract-regular" "cross-filled" "play" "pause" "refresh" "delete-filled" "cross"))) (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "0i8bfk6xcdm1vgxym48xilmqqhm2k0mbc3zc7ihv28idcjvpjjml")))

(define-public crate-scoreboard-0.0.0 (crate (name "scoreboard") (vers "0.0.0") (hash "0danzlc4a1shl6d0l4n2lhj8hnbbg8mj28vyyy7f0nvfckan08y0")))

(define-public crate-scoreboard_db-0.2 (crate (name "scoreboard_db") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "mysql") (req "^24.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.157") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fp5f7kgysm0l5m9zqd4pnz5jsikg1fpqq5y4gsmbc22k260xy7j") (v 2) (features2 (quote (("database" "dep:mysql"))))))

(define-public crate-scoreboard_world_cup-0.1 (crate (name "scoreboard_world_cup") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "099295gnig0dkgcjfvl5wplqp6abn22cbhi7qdf9g9vc9lkf5y00")))

(define-public crate-scoreboard_world_cup-0.1 (crate (name "scoreboard_world_cup") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "14n6xd9d6db5b8ypb81cbncbmf6rpy8dxizxrdmmdbf9jp9b6jim")))

(define-public crate-scorex_crypto_avltree-0.1 (crate (name "scorex_crypto_avltree") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base16") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "debug-cell") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0jq1016awdjnjjxkw1h4a112842nlfh9z4h9qh2qwxq2hk94qa9w")))

(define-public crate-scorpion-0.1 (crate (name "scorpion") (vers "0.1.0") (hash "0ga90zyv2226fnxsmw3vfnhikwh9qkcvacmf8xlnrfndvsac7461")))

(define-public crate-scorpion-0.1 (crate (name "scorpion") (vers "0.1.1") (hash "1p9dys2w60jax6c8d4wvkj7lxwad5mcp75qisi5qmw16i7kb60zr")))

(define-public crate-scorpion-0.1 (crate (name "scorpion") (vers "0.1.2") (hash "1qh5v1j47ndizcbi70aydr6cpaapf3ymwhw0fn40bnj4srxjvilf")))

(define-public crate-scorpion-0.1 (crate (name "scorpion") (vers "0.1.3") (hash "0swnmqr2igncs4zw7fgsdaf8yfw9nxss7yfrqinmm8fk7rcssy37")))

(define-public crate-scorpius-0.1 (crate (name "scorpius") (vers "0.1.0") (hash "1gq51f71gsjn17d71b28p1vqvfr7yr3zhabalaw00jxqyr51gqy8")))

(define-public crate-scorpius-0.2 (crate (name "scorpius") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0ps5zf6k5ndq8vlpvkhv0792ffyq9bwbgqnfinnb4rcb1pfls9jd")))

(define-public crate-scorpius-0.3 (crate (name "scorpius") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1pq9vb7gf42wyfdn4dybn8gzia29v51br33yc6cfr6q9lpk62scq")))

(define-public crate-scorpius-0.4 (crate (name "scorpius") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0l1vz6brqgb7m2414sbw7ih2hs8jqmn0n3szyx1r5kcgxq0vh3bb")))

