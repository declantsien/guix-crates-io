(define-module (crates-io sc cc) #:use-module (crates-io))

(define-public crate-sccc-0.1 (crate (name "sccc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ysi0l6rlaf0jcalqpvpcvvpgmx10h33cx216ggj0s6zkiva0jw6")))

