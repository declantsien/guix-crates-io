(define-module (crates-io sc #{16}#) #:use-module (crates-io))

(define-public crate-sc16is752-0.1 (crate (name "sc16is752") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "0psz7rsvl5xdps34zgf1vva13v2y0c5yrzj0ldmg53sjw0rbkgi5")))

(define-public crate-sc16is752-0.1 (crate (name "sc16is752") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "0kb6ipls929vf7jlgllax1xxhz431xxh3ldgf581xxdwmp9c3dfg")))

