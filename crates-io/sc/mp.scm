(define-module (crates-io sc mp) #:use-module (crates-io))

(define-public crate-scmp-0.1 (crate (name "scmp") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "1d9qz7nrm7nfi4ry1czjdjs6xcpn45w5rj6lhdb8ii6cn5qnpc1m")))

(define-public crate-scmp-0.1 (crate (name "scmp") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req ">= 0.26") (default-features #t) (kind 1)))) (hash "1nbrkghn4zgy2llsliryihds6h90igf7iv5nlkf386xpg0aj8bxx")))

(define-public crate-scmp-0.1 (crate (name "scmp") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req ">=0.69.4") (default-features #t) (kind 1)))) (hash "0cpl0wsj83mzmpgzr8gd7cc7cf7yrwisr8q38qlgha49w70zpnv7")))

