(define-module (crates-io sc rf) #:use-module (crates-io))

(define-public crate-scrftch-0.1 (crate (name "scrftch") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6.7") (default-features #t) (kind 0)) (crate-dep (name "psutil") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1vj9970f8ch2vjz5fic8nbbvvqv1kb3xaq1kh77i1kgh5jjm3hm6") (yanked #t)))

(define-public crate-scrftch-0.1 (crate (name "scrftch") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6.7") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0gyjl2b8dj7wws0njlgklcl36ik665csqxzzvaa891rilzg8hkmf") (yanked #t)))

