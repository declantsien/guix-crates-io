(define-module (crates-io sc d3) #:use-module (crates-io))

(define-public crate-scd30-0.2 (crate (name "scd30") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0gi2644qdhh6l96i0bbcfy7hlk0sk2ikzixk8166gp7xjj4cj0n8")))

(define-public crate-scd30-0.2 (crate (name "scd30") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "105kac88dn8rq2wh4v5dc3jl3mpay5j9jb7p0s38nl1yw7ylw4l2")))

(define-public crate-scd30-0.2 (crate (name "scd30") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0k0nn0railvj8y9lrkdliph92dfpp8mk3vq8afbd22ziwxi6lc4q")))

(define-public crate-scd30-0.2 (crate (name "scd30") (vers "0.2.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1xkkqkk4pbyw4l393h74fxn7hglsdl906bckl4rkc093k86l4s0c")))

(define-public crate-scd30-0.3 (crate (name "scd30") (vers "0.3.0") (deps (list (crate-dep (name "crc_all") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4.2") (features (quote ("const-fn"))) (default-features #t) (kind 0)))) (hash "17k3n7bvknyrzz2c1j28kbg6d9bd2cym6zhs5m3cx3ir00b295ia")))

(define-public crate-scd30-modbus-0.1 (crate (name "scd30-modbus") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 0)))) (hash "09wgj3hyzc1cxzql7djb4n5cj0kwpgrhwgs7z6c2s6an4m54mkk5") (features (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.1 (crate (name "scd30-modbus") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 0)))) (hash "01v5xi081zc5f2lb9izn7kgz3wqi1rijy5cfxiwp4p2lkd38pr3k") (features (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.1 (crate (name "scd30-modbus") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 0)))) (hash "0pijv0g19zlpbq8ddzlh1yalgk76axly7ks6rkry52jag8sxgs9q") (features (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.2 (crate (name "scd30-modbus") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal-nb") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 0)))) (hash "0gb18xh2mid21j672j9pan8f0jycsf4srfhv6vchip03biy08rqx") (features (quote (("std") ("default"))))))

(define-public crate-scd30-modbus-0.3 (crate (name "scd30-modbus") (vers "0.3.0") (deps (list (crate-dep (name "embedded-io-async") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1kfy48j7kfj1rbwr687vi95k8il2mkapvq305hrf158c15ys1hcg") (features (quote (("std") ("default"))))))

(define-public crate-scd30_i2c-0.1 (crate (name "scd30_i2c") (vers "0.1.0") (deps (list (crate-dep (name "i2cdev") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1jcijrhsbfrvjnch7191q6dw4a9bvpxwi1ak7kkh26dgfciyhbs2")))

(define-public crate-scd30_i2c-0.1 (crate (name "scd30_i2c") (vers "0.1.1") (deps (list (crate-dep (name "i2cdev") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1z5g82qfjl30506j5n8q4girsk4mxdmwghl7929n7hyyc09z60f9")))

(define-public crate-scd30_i2c-0.1 (crate (name "scd30_i2c") (vers "0.1.2") (deps (list (crate-dep (name "i2cdev") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1waxlriif57lf4qjp77yim5kypm6qblkp043bvkk0z3m11a205gh")))

(define-public crate-scd30pi-0.3 (crate (name "scd30pi") (vers "0.3.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1dhgyc79f1ybkpfikbzzv65ir2jksj26jrrqpkn1b9jfx9f8vlyw")))

(define-public crate-scd30pi-0.4 (crate (name "scd30pi") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1810r901h1kdfnq6c19fma82hirndm4i0msb7401q0c97cwcfya1")))

(define-public crate-scd30pi-0.4 (crate (name "scd30pi") (vers "0.4.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1v0acjy7bvaqz0svm2mhw5pz7zh9fakzpnipnqkj2n3cv0ssr6i8")))

(define-public crate-scd30pi-0.4 (crate (name "scd30pi") (vers "0.4.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1i1xm58px4kl8svglkk745gvdw16sfnlv2s2d9p5m8liribv7q94")))

