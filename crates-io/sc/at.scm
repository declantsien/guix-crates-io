(define-module (crates-io sc at) #:use-module (crates-io))

(define-public crate-scat-0.1 (crate (name "scat") (vers "0.1.0") (hash "00cnn8w6ra971w1yapqxjz2l77dmz69lfbljfvfnmvpl2hi8zfz1")))

(define-public crate-scat-0.1 (crate (name "scat") (vers "0.1.1") (hash "1d7yarxdq1fljc0dw1l17n54ll897ahywjcr4hrd3f6y8affzqmb")))

(define-public crate-scatter-0.0.1 (crate (name "scatter") (vers "0.0.1") (hash "1p66rr0sz8ir46gv0brkjcqwwikwy0nmsxs59mdrvhz6i3636x7d")))

(define-public crate-scatterbrainedsearch-0.2 (crate (name "scatterbrainedsearch") (vers "0.2.0") (hash "0assihif7qaj4vbpix3a48mqvhjhpc4mj0729zq0r1zhg4c6hi5i") (yanked #t)))

(define-public crate-scatterbrainedsearch-0.2 (crate (name "scatterbrainedsearch") (vers "0.2.1") (hash "14cbv8i9gz94srs66g3bwihmx9vqggypwbmjg1i3l843aj0gpwci")))

(define-public crate-scatterbrainedsearch-0.3 (crate (name "scatterbrainedsearch") (vers "0.3.0") (hash "0n0h2pm5rfl68s8403lg5vhbn2nkqlr3gj2yhqsfkdis5jzmn7ms")))

(define-public crate-scatterbrainedsearch-1 (crate (name "scatterbrainedsearch") (vers "1.0.0") (hash "0csr2rc6519nwv87gxnbc83718kl0ys27xsmy8dmx4zxc1vk47b5") (yanked #t)))

(define-public crate-scatterbrainedsearch-1 (crate (name "scatterbrainedsearch") (vers "1.0.1") (hash "0cc6kyzsx21gllr1dyhs1wl5jx4f3a13r77laa13plzyy81z56wd")))

(define-public crate-scatterbrainedsearch-1 (crate (name "scatterbrainedsearch") (vers "1.0.2") (hash "115biha62azcmfiv3gmbqgpybiv53m40ks4ha0rhpmi5w2cjxba9")))

(define-public crate-scatterbrainedsearch-1 (crate (name "scatterbrainedsearch") (vers "1.0.3") (hash "1x61wgwpal995llr1pvb6s3ayglpjbjl4mpyrq84ymhz0n3c8jcx")))

(define-public crate-scatterbrainedsearch-2 (crate (name "scatterbrainedsearch") (vers "2.0.0") (hash "14n9kkx3zjzyhbs337pj1x1980djf2rrc9y90vhlx7rindfhvagp")))

(define-public crate-scatterbrainedsearch-2 (crate (name "scatterbrainedsearch") (vers "2.5.0") (hash "0v39mlfczbmi8sav5lw3flym817nvkjpx3hmqsnpama2qh6f8ckz")))

(define-public crate-scatternotes-0.1 (crate (name "scatternotes") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)) (crate-dep (name "termfmt") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1my8lqw19iahbxfa975rsvh861h1ywxbwr63q1i0qyk20vj5pwrg")))

(define-public crate-scatternotes-0.1 (crate (name "scatternotes") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)) (crate-dep (name "termfmt") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "06annfpvxx9szpr7sm4py76sqrxvyd9zxy60p8vycvwbgc7x6h07")))

(define-public crate-scattr-0.1 (crate (name "scattr") (vers "0.1.0") (hash "02bvsxis9knpyv8vkxy0lpb3vqdnnpv19n557w9234axxpc0rsj9")))

