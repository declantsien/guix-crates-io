(define-module (crates-io sc ai) #:use-module (crates-io))

(define-public crate-scailist-0.1 (crate (name "scailist") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "09fv5hmrdx0zjkg91j3lkwjqilka75vagh6nzpva4hpb1w3k7lhh")))

(define-public crate-scailist-0.1 (crate (name "scailist") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0iflbj6l8s3dqc6j7pral15zcagk1f1xvmq83vydk9r70n9mgzy7")))

(define-public crate-scailist-0.1 (crate (name "scailist") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1974hxp1b2hvda6c614dyf808gn1fpddhhhxr01dvsh6b352gp0i")))

(define-public crate-scailist-0.1 (crate (name "scailist") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1llyjgaz1msmiryj3555b2pndznbwlswpc3w686z8f29c1y7dvzi")))

(define-public crate-scailist-0.2 (crate (name "scailist") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "06qma2xllhbj6k122wa6j1gi4jn75g4aqpahvhhplzdks35hzy16")))

