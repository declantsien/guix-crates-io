(define-module (crates-io sc up) #:use-module (crates-io))

(define-public crate-scupt-0.0.1 (crate (name "scupt") (vers "0.0.1") (hash "1jadklakbw7wmdpmhdqavk6k2pxvg73ajkmm1wgkfgz9x9inimky")))

(define-public crate-scupt-util-0.0.1 (crate (name "scupt-util") (vers "0.0.1") (hash "0wyp52xbxcadxs2d7fg492yq8q3ksb9smlsyv1r6rj1gm4i7c4qs")))

(define-public crate-scuptd-0.0.1 (crate (name "scuptd") (vers "0.0.1") (hash "063p1q0p77l4ibiss80vk45xq07zri902p0z6d2l7glvn1kwk6wr")))

