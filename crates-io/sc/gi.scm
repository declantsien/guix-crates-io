(define-module (crates-io sc gi) #:use-module (crates-io))

(define-public crate-scgi-0.0.1 (crate (name "scgi") (vers "0.0.1") (hash "1jjr24pxbwhinzmlcwz21zhy7giysc5v45scbh3scg73sy07k98c")))

(define-public crate-scgi-0.0.2 (crate (name "scgi") (vers "0.0.2") (hash "0zydiakjwd2d77pddygpbhn9als1i9l0khh3d2fkkbmrd4n7wr2r")))

(define-public crate-scgi-0.0.3 (crate (name "scgi") (vers "0.0.3") (hash "1kyk9vk05m304awnggzmgs5az82zg9dfvishl9siksdk7g25475v")))

(define-public crate-scgi-0.0.4 (crate (name "scgi") (vers "0.0.4") (hash "0b7y3xizyl7amvwpb3c080393wwwdcsk9379qyiyyblcakqzkkhp")))

(define-public crate-scgi-0.0.5 (crate (name "scgi") (vers "0.0.5") (hash "0djpq8ignd3iswzq96drmgfs4xxrgj3ji9xn2j8i8g9ypyrqwdjl")))

(define-public crate-scgi-0.0.6 (crate (name "scgi") (vers "0.0.6") (hash "033jfycsvp1g40vgjrd24bw3m3svnajpprijlp8wvxv1hv5453mh")))

(define-public crate-scgi-0.0.7 (crate (name "scgi") (vers "0.0.7") (hash "13cbbsswvjplcas91ii5iivk9x48zzhshkgk65622yqh7fwxqhm9")))

(define-public crate-scgi-0.0.8 (crate (name "scgi") (vers "0.0.8") (hash "10fpcnnl277v1s2s8rz269gbfjnfn0b4z49ffv7d172ra9x3ax6c")))

(define-public crate-scgi-0.1 (crate (name "scgi") (vers "0.1.0") (hash "12lbm7v97fvxc4nipy8dci3d117lb6g78703354l7wdhhh0qw62k")))

(define-public crate-scgi-0.1 (crate (name "scgi") (vers "0.1.1") (hash "1vfxksjakgc76jlnjrh3lmz5my8nlzzxdya96cpi54fw3f58r4ix")))

(define-public crate-scgi-0.2 (crate (name "scgi") (vers "0.2.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y86jxysfp0q4yzr35riaxzli37jlk2x8d0yxagg2syj7jhbv49i")))

(define-public crate-scgi-0.3 (crate (name "scgi") (vers "0.3.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)))) (hash "1151wlp31984mrhh1wg8y0cjivzsrvsj2v8palqin73lc0b1c9q3")))

(define-public crate-scgi-0.3 (crate (name "scgi") (vers "0.3.1") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)))) (hash "06a1szcwp7ig0889k1ihxq6wxdp2k6ndv6ls5111w974bpywynij")))

(define-public crate-scgi-0.3 (crate (name "scgi") (vers "0.3.2") (deps (list (crate-dep (name "bufstream") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1z4mz544v5mmqw52rpsfvkkbg04847jp49zmyiildm09fvh23cp5")))

(define-public crate-scgi-0.3 (crate (name "scgi") (vers "0.3.3") (deps (list (crate-dep (name "bufstream") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1v433lp7fddadqi4gdjc5mqpmaljm26a9ffk0vzc559b52lirwhh")))

(define-public crate-scgi-0.3 (crate (name "scgi") (vers "0.3.4") (deps (list (crate-dep (name "bufstream") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0mkbcj1kfnpkm62mki45dsa4fv0lb26i9k0ybbhw08d4l4qfgm46")))

