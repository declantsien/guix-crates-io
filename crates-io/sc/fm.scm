(define-module (crates-io sc fm) #:use-module (crates-io))

(define-public crate-scfmt-0.1 (crate (name "scfmt") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "126fz1gbczyrpvqgrw1ivfnvkq3k4m0chl2q9p9z4rakmljhmqq7") (yanked #t)))

(define-public crate-scfmt-0.1 (crate (name "scfmt") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "08ka1v8iy51q7yj1rl4krv0sl9xzm5qz1wq8cm57brkc2s3arxld")))

(define-public crate-scfmt-0.2 (crate (name "scfmt") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "078pw9q2z8kgwzsb7cnda4mmaqyz757p6ilxxrif5jzlvilajqmy")))

(define-public crate-scfmt-0.2 (crate (name "scfmt") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "version") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1iapaqv511afs8y6x03lddwg32rz4n3xglx03riiw6x756rab5n7")))

(define-public crate-scfmt-0.3 (crate (name "scfmt") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "version") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0srz7pcqch4pclzw8g8y0xak8dgnc1n0wgk7pq3yfmxp3wjvhl5n")))

(define-public crate-scfmt-0.3 (crate (name "scfmt") (vers "0.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "version") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "15i3ic05v4r52ps8vmz9qhysyg9bkk0ncznl9njmhpaks8c7pa8j")))

