(define-module (crates-io sc ss) #:use-module (crates-io))

(define-public crate-scss_mass_compiler-1 (crate (name "scss_mass_compiler") (vers "1.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "grass") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0l0c0rfpcmfd0rdyzyxkh9lyxw1c97s9dss0gp2c58ig16d5q0xs")))

(define-public crate-scss_mass_compiler-1 (crate (name "scss_mass_compiler") (vers "1.0.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "grass") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1i4m6rpvqv9m77b21sfv3rmp9b3y6ax27ywadz7f5jqyy3mglkah")))

(define-public crate-scss_mass_compiler-1 (crate (name "scss_mass_compiler") (vers "1.0.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "grass") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1rjx2ng1zk2aniicsc17jjfsizfqlsgzha4wy4kc7rabwxn9v7r3")))

(define-public crate-scss_mass_compiler-1 (crate (name "scss_mass_compiler") (vers "1.0.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "grass") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1wp9f8j1r3fxh0d3yxr2170lb0hr0pd83qwdcybmqs4slsd62g3m")))

(define-public crate-scss_mass_compiler-1 (crate (name "scss_mass_compiler") (vers "1.0.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "grass") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1i7mp0a9vcvyfsxh48n1ir8wmvq3rifrwp3hk3yfspszqnhz1f0m")))

