(define-module (crates-io sc p_) #:use-module (crates-io))

(define-public crate-scp_username_module-0.1 (crate (name "scp_username_module") (vers "0.1.0") (deps (list (crate-dep (name "askama") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1jp5rrqjvbcdz28bar4b31i1wrcmcqs0wnkcixa0lpjbkck1nazm")))

