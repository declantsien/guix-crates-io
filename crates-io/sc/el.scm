(define-module (crates-io sc el) #:use-module (crates-io))

(define-public crate-scell-1 (crate (name "scell") (vers "1.0.0") (hash "157ky7d8qb1faf8hkhmzwxlfm6l9x8ms3hdz7kdkihrm0895s4bp") (features (quote (("unchecked") ("default"))))))

(define-public crate-sceller-0.1 (crate (name "sceller") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0m3ldvqp32cw1bpyrvb60z8mxc817nvk88k35icv3m97qc393lng")))

(define-public crate-sceller-0.1 (crate (name "sceller") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0x42ics558aysc0nxxandls67m25gg93ak9w0pp2fgjzpxb9hffr")))

(define-public crate-sceller-0.1 (crate (name "sceller") (vers "0.1.2") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0clha2vy05bzndvklq839chhvxhfcls637bs8b6j0wwcrcjj1gl2")))

(define-public crate-sceller-0.2 (crate (name "sceller") (vers "0.2.0") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0i05gpz6a1yylldrsxmkv4syml7lc2i96rnga6qz5lv97dn57c22")))

(define-public crate-sceller-0.2 (crate (name "sceller") (vers "0.2.1") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0zfhc6gm2iw0p52mz591f43a61qx0ma8n9spp08nfvf44rbcnam9")))

(define-public crate-sceller-0.3 (crate (name "sceller") (vers "0.3.0") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "15jllrl4dvpasssrlr62nxvi89j0ckv2g4q296jpz8b5p3sjw0gx")))

