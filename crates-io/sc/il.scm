(define-module (crates-io sc il) #:use-module (crates-io))

(define-public crate-scilib-0.1 (crate (name "scilib") (vers "0.1.0") (hash "1kpzgwgpj7pms9vjbhx7rjxgkv3hc1vzczi1jqcf0n3z0dfzn382")))

(define-public crate-scilib-0.1 (crate (name "scilib") (vers "0.1.1") (hash "1f2ffy0a02rhswc5vmcra9ygmx50bfad68d3jwbkydl7s1sj2wsp") (yanked #t)))

(define-public crate-scilib-0.0.0 (crate (name "scilib") (vers "0.0.0") (hash "1wx54yrjazygcdw8j1vn06bjcas58lb8pnj09j3w3ip9yy9jilk5") (yanked #t)))

(define-public crate-scilib-0.2 (crate (name "scilib") (vers "0.2.0") (hash "0qnilsvfrhrxmd02bhfiw0vfnhls01vxdbmq07cm3q96rm98qapb") (yanked #t)))

(define-public crate-scilib-0.2 (crate (name "scilib") (vers "0.2.1") (hash "1jqvgjc1bm400rkiysxy50fz4v1l401zkdb64w991nxhrvj9xg3x") (yanked #t)))

(define-public crate-scilib-0.2 (crate (name "scilib") (vers "0.2.2") (hash "1rhspn9wwlmvanqml0x63fvpzah51mykvl00b9pbv6q7x8xaz1qx")))

(define-public crate-scilib-0.2 (crate (name "scilib") (vers "0.2.3") (hash "0bamhhb9vpl1f8z5m4581aaky0zh072vic741ac7cxb666fidgfl")))

(define-public crate-scilib-0.2 (crate (name "scilib") (vers "0.2.4") (hash "06n263l1j5k422fc1xi82rjl6sff6q2732bdplyxyf3ngzgycvjm") (yanked #t)))

(define-public crate-scilib-0.2 (crate (name "scilib") (vers "0.2.45") (hash "0xfv8mrnawkbzsq2v2zc9pdqziwl91dlq1h4qziw5zcm782imfi3") (yanked #t)))

(define-public crate-scilib-0.2 (crate (name "scilib") (vers "0.2.5") (hash "08znwxk61zx1k3rrvk5x7x22gkxv37x85x8dpixr9hvm87rd080f")))

(define-public crate-scilib-0.3 (crate (name "scilib") (vers "0.3.0") (hash "1w28gc20lha9k6zhng8mq6d0vqd76jv3scc8519j0kqc3fz586bm")))

(define-public crate-scilib-0.4 (crate (name "scilib") (vers "0.4.0") (hash "0mdw274dcxddpdnpv4r2vr767gnpbmfk9ssrd1ac7j7x7cd7i63i")))

(define-public crate-scilib-0.4 (crate (name "scilib") (vers "0.4.1") (hash "0z2s1v1l954xa8shp0714f5al68vahc05gpfhg69wcgjfi83k8p9")))

(define-public crate-scilib-0.4 (crate (name "scilib") (vers "0.4.2") (hash "0hmg5p8j8aj4khr36q667m953xls4zjya9im62qsvmqy9d8rwvqv")))

(define-public crate-scilib-0.4 (crate (name "scilib") (vers "0.4.3") (hash "0n0y6ssr236s9kig2hp9413xfl95m1n3ad492wllnj49zz00q6bh")))

(define-public crate-scilib-0.4 (crate (name "scilib") (vers "0.4.4") (hash "1zw37drzva2ljh81aabjmvbnb1flxgrr658y4q0w6hqsp1z2f5ic")))

(define-public crate-scilib-0.5 (crate (name "scilib") (vers "0.5.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1m9lgrvamahnhjvppghqd3lxxpiibrdi1bpnvzv258p085vn0a7c")))

(define-public crate-scilib-0.6 (crate (name "scilib") (vers "0.6.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "01d0hy7nf4328d1apmpg2zawlfg6w80yxcmll3qflkiz109v31sh")))

(define-public crate-scilib-0.6 (crate (name "scilib") (vers "0.6.1") (deps (list (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0123c1p4cznnnv0iwkxkk7d780pj5fyqzrp6y2y52yslzrhn5y24")))

(define-public crate-scilib-0.6 (crate (name "scilib") (vers "0.6.2") (deps (list (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0vb3450y3mcp3gkl0310j5nnhhm2j7bf7cm01y6g7ikpjaxx935h")))

(define-public crate-scilib-0.7 (crate (name "scilib") (vers "0.7.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0j1p7lrch6cf7al3fi5lzyaz6xj9vigib2fs2kay29isp3hq39v4")))

(define-public crate-scilib-0.7 (crate (name "scilib") (vers "0.7.1") (deps (list (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0gzviig0pnj8nzqd9yq8x4s1yb3myh9l1gjpv5hhhsdjgf7mskwx")))

(define-public crate-scilib-1 (crate (name "scilib") (vers "1.0.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1cvwjfs0ybjm7l2612ad3v2rdyp4v6rwfyk3ljs0pwrvv01k7w5c")))

(define-public crate-scilla-parser-0.1 (crate (name "scilla-parser") (vers "0.1.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "12waxvl8wiyksz9id329a7521yssjr8z319imln6mxwsz4yg0hhd")))

(define-public crate-scilla-parser-0.2 (crate (name "scilla-parser") (vers "0.2.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "02icrpla564vwl3xyvr4l9sanp9s9phmzfq6mcasd788ppzi0gcq")))

(define-public crate-scilla-parser-0.3 (crate (name "scilla-parser") (vers "0.3.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1a44p1a9p8yy5h855hsy73dgclld993girizv3kiwbnd05gj3vwv")))

(define-public crate-scilla-parser-0.4 (crate (name "scilla-parser") (vers "0.4.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "15xlbsvnsrbaz1cmr3x9h7ic0s1s0wkdbhyk8jk8izyq1wa3chil")))

(define-public crate-scilla-parser-0.5 (crate (name "scilla-parser") (vers "0.5.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0sibl2hm6as20y09vabz8zpkn4m5izah75kkvzhkz1hn0dixym2g")))

(define-public crate-scilla-parser-0.6 (crate (name "scilla-parser") (vers "0.6.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1d7g350ryijcni07vy7f0rblz7rsjqh3xb8wpm67qinq3mwxgdl9")))

(define-public crate-scilla-parser-0.7 (crate (name "scilla-parser") (vers "0.7.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0rb6q8jps25jg70nc9q6xvmdzzaj1pflrvj8gslvj892hm6gbxsj")))

(define-public crate-scilla-parser-0.8 (crate (name "scilla-parser") (vers "0.8.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1513hms8n8rfmbix2dn0b6li2ng93zrbs9h7zmas1vlrryvvf51z")))

(define-public crate-scilla-parser-0.9 (crate (name "scilla-parser") (vers "0.9.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1lnd0zl08kxhd87i9b0albpm9fy15yin2zb7xzfdlrk0rdv31v9z")))

(define-public crate-scilla-parser-0.10 (crate (name "scilla-parser") (vers "0.10.0") (deps (list (crate-dep (name "lexpr") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "134lwgf1kigim85k2hn25axflvsy1x1dbf12n3ppw1j2js7wbq3j")))

(define-public crate-scilla-parser-0.11 (crate (name "scilla-parser") (vers "0.11.0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "17d8a7iqysaykz3vrzng5x2zvkky520iaahiargr1pscv5103nvm")))

(define-public crate-scilla-parser-1 (crate (name "scilla-parser") (vers "1.0.0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0w4k4hvp2cav527wqwz5qrld12x746ha44gpvb59rlgyhhrw8cr7")))

