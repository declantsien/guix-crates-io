(define-module (crates-io sc ad) #:use-module (crates-io))

(define-public crate-scad-1 (crate (name "scad") (vers "1.0.0") (deps (list (crate-dep (name "nalgebra") (req "^0.13") (default-features #t) (kind 0)))) (hash "0z1nc619cdp89548mbq4i2fir8a0akwg237dgs8y2407kwi10083")))

(define-public crate-scad-1 (crate (name "scad") (vers "1.0.1") (deps (list (crate-dep (name "nalgebra") (req "^0.13") (default-features #t) (kind 0)))) (hash "10b03wrkyvfvvmslmwfgjqfm7zwnkdn76zl179gv1ppjnad3knwv")))

(define-public crate-scad-1 (crate (name "scad") (vers "1.0.3") (deps (list (crate-dep (name "nalgebra") (req "^0.16.8") (default-features #t) (kind 0)))) (hash "0icw6yp50dlz6dvm09r2fd9yapirg324i64gl4vg8dbdwsflpxcw")))

(define-public crate-scad-1 (crate (name "scad") (vers "1.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.16.8") (default-features #t) (kind 0)))) (hash "0pa8nbljg7alzcfs2snrpr5p2ly6mrq1j9hif382ial30q3by5z1")))

(define-public crate-scad-1 (crate (name "scad") (vers "1.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.16.8") (default-features #t) (kind 0)))) (hash "1dn3gl2zdr1ymr69v4ar1s8a386wy7m1xjnn3f56q4lcmsqd8lpz") (yanked #t)))

(define-public crate-scad-1 (crate (name "scad") (vers "1.2.1") (deps (list (crate-dep (name "nalgebra") (req "^0.16.8") (default-features #t) (kind 0)))) (hash "0jkiys26nlcs8cvz2hl21jypp9m5cd5nhhmrlvinwx17a6vlz5lr")))

(define-public crate-scad-1 (crate (name "scad") (vers "1.2.2") (deps (list (crate-dep (name "nalgebra") (req "^0.16.8") (default-features #t) (kind 0)))) (hash "1yvy7ckfd7r261iywm75na1ykd9cl8h0q8ajb1iwg1jmnbs6vry6")))

(define-public crate-scad_tree-0.1 (crate (name "scad_tree") (vers "0.1.0") (deps (list (crate-dep (name "scad_tree_math") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16zh6lw3bgaxsrk9zqg6m9rm0irf4mc93fyxmxspfq7kikax5ivd")))

(define-public crate-scad_tree-0.1 (crate (name "scad_tree") (vers "0.1.1") (deps (list (crate-dep (name "scad_tree_math") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1i3gq2dswgvrgyais99i0qdc6h88ckrp97yvm28c318pll80bvss")))

(define-public crate-scad_tree-0.1 (crate (name "scad_tree") (vers "0.1.2") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0l12zhmnmqnd9q8spy8a8jy5gk8ja4jcqm5gjx5xnpkzncmzgg9w")))

(define-public crate-scad_tree-0.1 (crate (name "scad_tree") (vers "0.1.3") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0badbds1vx3k3jrps60jgwi1jkrlb0b2xblpzgg9vn2xhmphq55z")))

(define-public crate-scad_tree-0.1 (crate (name "scad_tree") (vers "0.1.4") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0ibba0j4j5h8v25l5xvp3ld7ia6nggba4xfzwhiiv4yiki8cw2s7")))

(define-public crate-scad_tree-0.2 (crate (name "scad_tree") (vers "0.2.0") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "16cmrsrd923l1avjkp9pmybz8w23rga8kdn375cqdb6imk773gqp")))

(define-public crate-scad_tree-0.3 (crate (name "scad_tree") (vers "0.3.0") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0l3dl3g2j0s2cpscpjmxmbbnqwx0n30v99sirhzwpxkmm7lcrmdn")))

(define-public crate-scad_tree-0.3 (crate (name "scad_tree") (vers "0.3.1") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1gpyq4iajikdcp516zq4p3j2nlf1gj3hj4pb80c4bf39pf0ic8v0")))

(define-public crate-scad_tree-0.3 (crate (name "scad_tree") (vers "0.3.2") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1cllpsg9wv2fnwm3ffsy93i9drhnz0pzzy9vz2xlddx69jgmzahb")))

(define-public crate-scad_tree-0.3 (crate (name "scad_tree") (vers "0.3.3") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0hmdv4ms0gx570vvxs3pvx37bhwby5vgvq210kj7kw5cg1l4crzv")))

(define-public crate-scad_tree-0.4 (crate (name "scad_tree") (vers "0.4.0") (deps (list (crate-dep (name "scad_tree_math") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0n4cqramcm1s81wpk8mz2yd6f9f5ygpskiyyw4475w9na88f38sh")))

(define-public crate-scad_tree_math-0.1 (crate (name "scad_tree_math") (vers "0.1.0") (hash "046w14p9d3368n5prfklh54k4d55is6f8jp0qfz32cjvffvw3bv5")))

(define-public crate-scad_tree_math-0.1 (crate (name "scad_tree_math") (vers "0.1.1") (hash "078hprvxpqsg2m4q09rkc5092047wlih199jifqsff5pdslp8rkr")))

(define-public crate-scad_tree_math-0.1 (crate (name "scad_tree_math") (vers "0.1.2") (hash "0rlmw1cymgjyxydijk16wf6ws8pzxws388pdqf4qfzha8klsw115")))

(define-public crate-scad_tree_math-0.1 (crate (name "scad_tree_math") (vers "0.1.3") (hash "0yyk5bqi3k9rpf7fcsfal4d4f4jqch2qj4p9anr52k7lz50sfvyv")))

