(define-module (crates-io sc hm) #:use-module (crates-io))

(define-public crate-schm-0.1 (crate (name "schm") (vers "0.1.0") (hash "0mfd28waagrawymkgjmdpanbs4z4rssmlrlhl1dsakaqk50r2860") (yanked #t)))

(define-public crate-schm-0.1 (crate (name "schm") (vers "0.1.1") (hash "0n6hyf4i981q8k6f8v4vmck7nbj4l2mblb8m67zjhx0mvb4ib2rm") (yanked #t)))

(define-public crate-schm-0.1 (crate (name "schm") (vers "0.1.2") (hash "1mpvrmqm68kn95ib6pykqb352jf3m83zl3c6pkkis32qqx47i7c0") (yanked #t)))

(define-public crate-schmfy-0.1 (crate (name "schmfy") (vers "0.1.0") (hash "0a640bq2hhb0b5463bgjdiyb88g1l848rwia63f00kq4660z6l4j")))

(define-public crate-schmfy-0.1 (crate (name "schmfy") (vers "0.1.1") (hash "05mll8144336lvqmlab2x2qk6mxisc9d7m2ypx6ys2pgm4nxfqac")))

(define-public crate-schmfy-0.2 (crate (name "schmfy") (vers "0.2.0") (hash "032yml2hmp8pzrld2cdzf84nyqxsjv0gwqvzvrab0p15i4gxazrr")))

(define-public crate-schmfy-0.2 (crate (name "schmfy") (vers "0.2.1") (hash "033zhami76qjxiv6sn6gkawfmxfd8qrdaybizxpfdv7w9ngm4n52")))

(define-public crate-schmfy-0.2 (crate (name "schmfy") (vers "0.2.2") (hash "1h6dn1k5s5slgf3lbg1yrzp7qlxqzcppcckrvpcrvkaynlnbvr7r")))

(define-public crate-schmfy-0.3 (crate (name "schmfy") (vers "0.3.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1z2cr1k1cavpyn19c23sfg7hxzz3892a93jbkq8a578lhwpdbzh1")))

(define-public crate-schmitttrigger-1 (crate (name "schmitttrigger") (vers "1.0.0") (hash "1ik7ww68cph2107mrjmwzxzj4yy3p42cbi2z5ysjfaa2bh2frrih")))

(define-public crate-schmoozer-0.1 (crate (name "schmoozer") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.77") (default-features #t) (kind 0)) (crate-dep (name "killswitch") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "net" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "10g4pxxv41p2q29krywwfxq5my171xzfy19xyigwvf0m4bmh3x28") (v 2) (features2 (quote (("tcpconn" "dep:killswitch" "tokio/macros" "tokio/net" "tokio/time")))) (rust-version "1.74")))

(define-public crate-schmoozer-0.1 (crate (name "schmoozer") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.77") (default-features #t) (kind 0)) (crate-dep (name "killswitch") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "net" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "0b1x4d1kl0cxv86p3cr4ynvnziix9jvbbyv89h8blvk6kc92qjwi") (v 2) (features2 (quote (("tcpconn" "dep:killswitch" "tokio/macros" "tokio/net" "tokio/time")))) (rust-version "1.74")))

