(define-module (crates-io sc pt) #:use-module (crates-io))

(define-public crate-scpty-0.1 (crate (name "scpty") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lv6hk83yrxal6h73vz3dvrvfk45fb35qa81vknwjin70x9wy8q8")))

(define-public crate-scpty-0.1 (crate (name "scpty") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1kcdlmrsh8mx0kqrm0bdarq8yqibcqy45zfxx6z8s5w0630ljpw0")))

(define-public crate-scpty-1 (crate (name "scpty") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05m1g0ywc96g6vhyfcmrn4amp463vc6y6kka34p6yfx2alys7p30")))

(define-public crate-scpty-1 (crate (name "scpty") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dd7c46k9d6d23khfr124i7d1r2ln06w65xsnj72zr0ss125brpm")))

(define-public crate-scpty-1 (crate (name "scpty") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1h3l3hnnskm71jdwm19ys7yjrcj46izmma8nfxf0x21m9yj4hbg6")))

