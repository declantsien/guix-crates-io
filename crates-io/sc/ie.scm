(define-module (crates-io sc ie) #:use-module (crates-io))

(define-public crate-scie-0.1 (crate (name "scie") (vers "0.1.0") (hash "0kvrb6diqkhiy4amv0n3ag7ky5ipvbvjw0ayfi80w58hrb4awm78")))

(define-public crate-scie_model-0.1 (crate (name "scie_model") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1099xzrab066z15y7zjp1391dllmy87h1ybixw00xafkr1c5wfjz")))

(define-public crate-science-0.1 (crate (name "science") (vers "0.1.0") (hash "0bna4kwsmlan684q03c5gs7m1nv2xy0h724dvpp5c1dn225vcsqi")))

(define-public crate-scienced-0.1 (crate (name "scienced") (vers "0.1.0") (hash "1rksx4gn3vh0hrns889s9yv0h5lg8qm2vk62i7hnphjvlcl8aawf")))

(define-public crate-scienceobjectsdb_rust_api-0.2 (crate (name "scienceobjectsdb_rust_api") (vers "0.2.0-rc1") (deps (list (crate-dep (name "prost") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "prost-derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.6") (default-features #t) (kind 1)))) (hash "0izcgfa8hz6vw0vfnn026z6gws7yywzn6qfg9q4n7qc3rz12ybkp")))

(define-public crate-scienceobjectsdb_rust_api-0.3 (crate (name "scienceobjectsdb_rust_api") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "prost") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "prost-derive") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0") (default-features #t) (kind 1)))) (hash "02vzb6l3d6jla9l34j2nqw7qy608gjm1y01fyzf4c805j8c8grm1")))

(define-public crate-scienceobjectsdb_rust_api-0.3 (crate (name "scienceobjectsdb_rust_api") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "prost") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "prost-derive") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0") (default-features #t) (kind 1)))) (hash "1vx12pnx0d6h63lshyqir22w3rc8fr2pwqhda359fp79c0zb73r4")))

(define-public crate-sciencer-0.1 (crate (name "sciencer") (vers "0.1.0") (hash "1j9avlgxk4gs4jdjlxfradl9nf1c2j1b3sx85wgv8qn4ckah45bh")))

(define-public crate-scient-0.1 (crate (name "scient") (vers "0.1.0") (hash "0ximalzysnpnyx1w0j4i6d6qh2f14vskzcv4mkxr6b1gbjf7dmkv") (yanked #t)))

(define-public crate-scient-0.1 (crate (name "scient") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1zklbb9ga4jsa1f866pacv2nhvmwnd6kvrfbgvpx29hqcsbpgzwl") (yanked #t)))

(define-public crate-scient-0.0.10001 (crate (name "scient") (vers "0.0.10001") (hash "06fw202ip4cfc1a6bwwy9sgdqxjqj3m5zgxcn9kkyc8fqmrb2xr8")))

(define-public crate-scientific-0.1 (crate (name "scientific") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.126") (optional #t) (default-features #t) (kind 0)))) (hash "073imyj7sxcskkih5xflxx3w4ffmfgrp621kc2wdvxxyd108c22q") (features (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.1 (crate (name "scientific") (vers "0.1.2") (deps (list (crate-dep (name "scientific-macro") (req "<0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.126") (optional #t) (default-features #t) (kind 0)))) (hash "197z71d22afgi4k5awbc2pm2qrr9w3g3chh13d2a82v46hhwvslk") (features (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.1 (crate (name "scientific") (vers "0.1.3") (deps (list (crate-dep (name "scientific-macro") (req "<0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0dk1hx4wpirp31k4v4i3ibv76hswclla325cyk4s00ia0cr4fjhr") (features (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.2 (crate (name "scientific") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1v8jmdazxkwxa41ngap9kqqp18iz8y06s5nlyfx21cjx4xw6kwad") (features (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.2 (crate (name "scientific") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "04bhc7xyq8279fkwqs1grzg34j9n0wsv5xgmv7qvil3w6naljzjd") (features (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.2 (crate (name "scientific") (vers "0.2.2") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0im1vzi6qskkn5hqi9bggkszybvq5xbscgbzipv6dwx92pg4ilav") (features (quote (("std") ("default") ("debug") ("arc"))))))

(define-public crate-scientific-0.5 (crate (name "scientific") (vers "0.5.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "scientific-macro") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "16kg2f6k2xp5dhzdcgwqfw000280m3ij6wfn57mrxpidfv5z12w5") (features (quote (("std") ("macro" "scientific-macro") ("default" "macro") ("debug") ("arc"))))))

(define-public crate-scientific-0.5 (crate (name "scientific") (vers "0.5.1") (deps (list (crate-dep (name "num-integer") (req "^0.1.39") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scientific-macro") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.16") (optional #t) (default-features #t) (kind 0)))) (hash "0lgnvj3bx5qwbppviqy8kqdghijqvxjbhw9gw4m5did5ms9fy38y") (features (quote (("std") ("macro" "scientific-macro") ("default" "macro") ("debug") ("arc"))))))

(define-public crate-scientific-0.5 (crate (name "scientific") (vers "0.5.2") (deps (list (crate-dep (name "num-integer") (req "^0.1.39") (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1.0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scientific-macro") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "0nczqfc6ngdfcx6z52bncn3jgncb3wd43a6vd0f4az13is5ijlyw") (features (quote (("std") ("macro" "scientific-macro") ("default" "macro") ("debug") ("arc")))) (rust-version "1.65.0")))

(define-public crate-scientific-constants-0.1 (crate (name "scientific-constants") (vers "0.1.0") (hash "1x9l1x9x0bfz1iig27m331h9yk5r24463qjdbybkjiji2q7yncad")))

(define-public crate-scientific-constants-0.1 (crate (name "scientific-constants") (vers "0.1.1") (hash "1x9g2x290k4wmfc1xhswhdj0364qwxlmvs0xil570zj1d88c8liq")))

(define-public crate-scientific-macro-0.1 (crate (name "scientific-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "scientific") (req "<0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)))) (hash "0cj9n14wx6lzil498dsnljkjm0yr8210ls18cmb9fy70fjxqdz2v")))

(define-public crate-scientific-macro-0.5 (crate (name "scientific-macro") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "proc-macro"))) (kind 0)))) (hash "1s7pmsz9wy2wp4dq059gp5h04x4i1lzyr6bh6rb1pycb2n7llfdv")))

(define-public crate-scientific-macro-0.5 (crate (name "scientific-macro") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "proc-macro"))) (kind 0)))) (hash "1lig1i8kf5isl86g1syxg6gzxf1y2vcrq0sxl2zmbdib962livnj") (rust-version "1.65.0")))

(define-public crate-scientifiction-0.1 (crate (name "scientifiction") (vers "0.1.0") (hash "1brraf839vxcrfxn3k6bqvbri75vjv5a3pl4v9nyn4vwrfjs3bwl")))

(define-public crate-scientist-0.1 (crate (name "scientist") (vers "0.1.0") (hash "0b57wdcnw3h8b1fk3pd3rbh4ca4w6wjn4y1sf6k7y3ydqywnmjz5")))

(define-public crate-scientist-0.1 (crate (name "scientist") (vers "0.1.1") (hash "1ins8ks8bxfvx02bwj1lyqc05mkkk9qk64rygjwz7h05p9kajbxn")))

(define-public crate-scientist-0.1 (crate (name "scientist") (vers "0.1.2") (hash "0232vsd4f81lac0llwvw15nx10ja013pfyr15i08wp0hz95n7kvd")))

(define-public crate-scientisto-0.1 (crate (name "scientisto") (vers "0.1.0") (deps (list (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "0kb496d84k0gm2dmxqnwy9bvsb726kmd98g0vydypsglabz16ng7")))

(define-public crate-scientisto-0.1 (crate (name "scientisto") (vers "0.1.1") (deps (list (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "105bn0g630ffv3bx5diqsk9dsv14gadk5q8mfzdf6kijl4xbqdw2")))

(define-public crate-scientisto-0.1 (crate (name "scientisto") (vers "0.1.2") (deps (list (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "0582vdammrjmd8n518sxf2pl1g4y41f8a4zpwg7y0grn6yqk08ka")))

(define-public crate-scientisto-0.1 (crate (name "scientisto") (vers "0.1.3") (deps (list (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "08zgkhaxklaf9qqd7769q8d3p29nlzlb4lpqrim6h6ic0d08fnzh")))

(define-public crate-scientisto-0.1 (crate (name "scientisto") (vers "0.1.4") (deps (list (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "0wyqp793fcv1hn3fgm4m048qph20qyjdxlrkzhs1b8s797j8cc41")))

(define-public crate-scientisto-0.2 (crate (name "scientisto") (vers "0.2.0") (deps (list (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "03zxy0snbsvw1s1lpdgf11gchjlygrg5lsdzlivlffizcri8gx9p")))

(define-public crate-scientisto-0.3 (crate (name "scientisto") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "1arwqb9cbm5v0lzvw2p1z0h9ckqjb4xk38q5fa7z8xacxj0k0wj1")))

