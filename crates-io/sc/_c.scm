(define-module (crates-io sc _c) #:use-module (crates-io))

(define-public crate-sc_compression-0.1 (crate (name "sc_compression") (vers "0.1.0") (deps (list (crate-dep (name "lzham") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rust-lzma") (req "^0.5") (default-features #t) (kind 0)))) (hash "00zlhjdb6jiprw2np20ykg08bdc48jr6prap42rg401w5mac0q3z")))

