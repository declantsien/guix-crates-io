(define-module (crates-io sc f-) #:use-module (crates-io))

(define-public crate-scf-core-1 (crate (name "scf-core") (vers "1.0.0") (deps (list (crate-dep (name "lang_extension") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1il7c021gxjgkhmhdy84rplnknggrfkqcig5wl6215w7ghq4wxxl")))

(define-public crate-scf-core-1 (crate (name "scf-core") (vers "1.1.0") (deps (list (crate-dep (name "lang_extension") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1ahyyj4asjvbqdhqxhbdbx9fra6ism1iy9bxwn79aax90vsp8hx1")))

(define-public crate-scf-core-1 (crate (name "scf-core") (vers "1.1.1") (deps (list (crate-dep (name "lang_extension") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0a4p180f693x8z8j67s8s889w8p3hcn8qin18577qhq4sr8fxwq7")))

(define-public crate-scf-core-1 (crate (name "scf-core") (vers "1.2.0") (deps (list (crate-dep (name "lang_extension") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0q20y24lhihp1a3iwcaij1kgrsi8jhrd7c8ncghjdvyn8didvc6i")))

(define-public crate-scf-core-1 (crate (name "scf-core") (vers "1.2.1") (deps (list (crate-dep (name "lang_extension") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0z3712jzvp72l865z0jq9jl9lxlfpbclgniahkvpidq5didczpvm")))

