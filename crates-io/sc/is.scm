(define-module (crates-io sc is) #:use-module (crates-io))

(define-public crate-scispeak-0.1 (crate (name "scispeak") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "disambiseq") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "fxread") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "gzp") (req "^0.11.3") (features (quote ("deflate_rust"))) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0lbwidlwb8p9sfc1bl79iwmllcymfygj8ygl0vkb68sml0ws76fw")))

(define-public crate-scissrs-0.1 (crate (name "scissrs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "0l11qz0gn7x1w37nbhyrp7d6jg73j2k3x32nfn0bcifzhymlkwz0")))

