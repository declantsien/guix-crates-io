(define-module (crates-io sc fg) #:use-module (crates-io))

(define-public crate-scfg-0.1 (crate (name "scfg") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0v5fr3nkj97i12fzrfx9hdgil0j87g76b4k8v6di9drxwbwfd3dj") (features (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-scfg-0.2 (crate (name "scfg") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "shell-words") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0chla6j8jd1qbmnrn7s72y6s1nwb674591cxi3xm6ii8xs5m4ba9") (features (quote (("preserve_order" "indexmap") ("default"))))))

(define-public crate-scfg-0.3 (crate (name "scfg") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shell-words") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0m7rn0l1w4gvq5d85nijbl6a5fhzbbw8w00sm8mdzfm99cm3nm6c") (features (quote (("preserve_order" "indexmap") ("default"))))))

