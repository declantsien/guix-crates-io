(define-module (crates-io sc ir) #:use-module (crates-io))

(define-public crate-scirocco-0.1 (crate (name "scirocco") (vers "0.1.0") (hash "1d6vqc7azx85kd8k184y2i9cnmv4q7x581r14jbzzvz0ikgy1adk")))

(define-public crate-scirust-0.0.2 (crate (name "scirust") (vers "0.0.2") (hash "0fw36n3w8mckhhps9059annik8121xf6faif818whczszv17n2r0")))

(define-public crate-scirust-0.0.4 (crate (name "scirust") (vers "0.0.4") (deps (list (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1r9ndrgy2w3pv01477ml824sq2w9hjays3i6nvbcw9rxv8ydm11s")))

(define-public crate-scirust-0.0.5 (crate (name "scirust") (vers "0.0.5") (deps (list (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1sj1p091da0mhi85x7k2ssprm49cqbxv1597v1kg0jg6mik8diws")))

