(define-module (crates-io sc op) #:use-module (crates-io))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.0") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1qp0b0ilsgfz1hc0m70i2ba2bq8ydm85gqv0dq3hxlnxlin16kx5")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.1") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0gw6cdgnkvz8x6glpkhvdyb74h0lw4533i2qkr8krh7pyra1na3c")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.2") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "083ja3gwx8i6ywlijciqwibrw3qkh41d069wnbqhzdz5h32davnj")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.3") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "095k6rsyywh20w4snil1k55kag02cljc7ran6bcp7idzs6z2l402")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.31") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0fglcbarrl5aqygfdl7a1j6688dgpssnyjabf21m35gk890hk16y")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.32") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0sbzhxm0mnnygahjbrq639xc8z5c3a974az24v7nnn55p4sx0nv5")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.33") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "07z42rhj456sgavc5qmhg36g7jnr7dnr5akfx3rhjrdqpdy8m5ls")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.34") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "03nbbkj7ggsp3wrz5z1qfqrznjvfgzj08mri9yr74lwlkf70nwvj")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.35") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "13i03s5v6b4lyfxc0smnrfa3xkkc1ik5fckgkbqc7jqsf1npq4c7")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.36") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "145gcnzhg3pqzc176rc31yx146rhbxfdqn3zsfin08jxd6a9n9by")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.37") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "10n7yqilj3qkslwppnai0d3jwx7pkkgqjaf9whbkhd7ihksr25pq")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.38") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1yc02527qb11vmg4h9j9yjqfpdx9s344m3znfk7rz0d28amhb317")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.39") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1lckrx5sxdscjmzvqq05wk3xam819ag35gby4ia43vzxjqh2v4vf")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.40") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1337bs3gc4124yfr78hiib4gv8lv58ri6pb3jbzpr4kwmp38rwly")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.41") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "0xd3mxszmlhs84zf6zd0raax9l1xg2dhqrb07i4j7yn0ak3x3mq7")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.42") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "19n6n41l6wdl55lyw72i0psmnimpbghfidnbh10z6la9isg6smix")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.43") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "0cksaqk5i1hah8zq3gw154bwi6bh1gbxj2hx6g73cnzf9zhxklvk")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.44") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "1ingbvj4y11jn7spdy1ddxsxf71934zx6zmk8h0i7zz0mnkvkl6i")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.45") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "1slfdl2l5ys3yr72yz3a43zz4zlyabrradqbps6bxdpgv7clr08q")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.46") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "1p8a1nq114yk2d45wbz04600rnfvkgldffqx68fc6lzx3d56i47i")))

(define-public crate-scop-1 (crate (name "scop") (vers "1.0.47") (deps (list (crate-dep (name "indexmap") (req "^1.6.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4" "stdweb"))) (default-features #t) (kind 0)))) (hash "0iihgaglrfbp03s2dd6kh7m9ngq6d04b80p49ir5fpj29dwfqli5")))

(define-public crate-scope-0.0.1 (crate (name "scope") (vers "0.0.1") (hash "18fhm477rzqfg465qailx9ngph0xrwm3dzz205ak9s5d7zabryqs")))

(define-public crate-scope-0.0.2 (crate (name "scope") (vers "0.0.2") (hash "0ahqkdiw0i4065jkxyj6jppf6fqid7vfim3lm2n9j4pjjx07l3j1")))

(define-public crate-scope-exit-0.1 (crate (name "scope-exit") (vers "0.1.0") (hash "04j49ph4dry26rivwnxgz171fj56vcq7air4ikjp2ymc0qqyvpsg")))

(define-public crate-scope-exit-0.2 (crate (name "scope-exit") (vers "0.2.0") (hash "16hflc3jfm8ckf061c6rwn8h3igh2dpmygb37qqqvi5nclv5d6r5")))

(define-public crate-scope-guard-1 (crate (name "scope-guard") (vers "1.0.0") (hash "0d3c58jn19s6gldqp86icdmyrghvyxqldl1mni0s4jqmn1yp0mmb")))

(define-public crate-scope-guard-1 (crate (name "scope-guard") (vers "1.1.0") (hash "18ngvr6n5rjmza4yni54x58bm9m00zj59qs4w4vsn0w059zhs8b5")))

(define-public crate-scope-guard-1 (crate (name "scope-guard") (vers "1.2.0") (hash "0b08kvvcgndwrqg6qqi0zdf5jhr179q9m7qz97j5g8fkmc8dmkwa") (features (quote (("std"))))))

(define-public crate-scope-lock-0.1 (crate (name "scope-lock") (vers "0.1.0") (hash "0vp1q8ifs6j8p01q6mb9h6ygcxbvqkj5ylc0vxq5c1kdb40rcx0p") (yanked #t)))

(define-public crate-scope-lock-0.2 (crate (name "scope-lock") (vers "0.2.0") (hash "00f06wbn5iq5jlsb0syypc3y19pnfrr4ilb5hxbxcw53bc8i1x14") (yanked #t)))

(define-public crate-scope-lock-0.2 (crate (name "scope-lock") (vers "0.2.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.2") (features (quote ("send_guard"))) (default-features #t) (kind 0)))) (hash "175vccww4vmk5d7kzmxdafp5b4qvxfy4fsab6yck9axx7dw1v13c") (yanked #t)))

(define-public crate-scope-lock-0.2 (crate (name "scope-lock") (vers "0.2.2") (deps (list (crate-dep (name "parking_lot") (req "^0.12.2") (features (quote ("send_guard"))) (default-features #t) (kind 0)))) (hash "0zcg32xqiir0m413abslrdcxrz5v2ajg2qjnzgqr24r4aa1bf37j")))

(define-public crate-scope-lock-0.2 (crate (name "scope-lock") (vers "0.2.3") (deps (list (crate-dep (name "parking_lot") (req "^0.12.2") (features (quote ("send_guard"))) (default-features #t) (kind 0)))) (hash "1grgqcmipsjqfsxvwpvgs8pg9xaaa9vc73cxzw8k4d1yipivjdpg")))

(define-public crate-scope-monitor-0.1 (crate (name "scope-monitor") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "homedir") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.19.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0rx4mnkh44wx86cq4zcxk46vyg2bmhwd3m9wzyfzf964a6125i0d") (rust-version "1.66.1")))

(define-public crate-scope-monitor-0.1 (crate (name "scope-monitor") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "homedir") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.19.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1shd922rs0333rkp7fx8cj43ghjn99ca0brsp7nx12xiqby58d6w") (rust-version "1.66.1")))

(define-public crate-scope-threadpool-0.1 (crate (name "scope-threadpool") (vers "0.1.0") (hash "1myl34xwwpl2zac4g77zkl3rmkrpanm5czi5v1mlv4lan0hg4cm1") (yanked #t)))

(define-public crate-scope-threadpool-0.2 (crate (name "scope-threadpool") (vers "0.2.0") (hash "1pwg9ykizjr29iczf7s7hsfpcnbw8cklrfr3iml5nrgh5yxpi1h2") (yanked #t)))

(define-public crate-scope-threadpool-0.2 (crate (name "scope-threadpool") (vers "0.2.1") (hash "0z505y2za1ljmlcnjywdlb0pgpkikaxh7q8hml04zs5aki9j2hcs") (yanked #t)))

(define-public crate-scope-threadpool-0.2 (crate (name "scope-threadpool") (vers "0.2.2") (hash "02pmrfzj45sj2j8s6ijfarbsm9yl43wgipa8w0709nnip8csm54i") (yanked #t)))

(define-public crate-scope-threadpool-0.3 (crate (name "scope-threadpool") (vers "0.3.0") (hash "1r7xq8v1rv6aagx8sfl08d7yqldxcfqd4awqaqly6qhzxbsr2bnw") (yanked #t)))

(define-public crate-scope-threadpool-0.3 (crate (name "scope-threadpool") (vers "0.3.1") (hash "0n10i0x12hcfz461167h434lry0dr1j7dgz6s8jc2wqg9fwh4ny6")))

(define-public crate-scope-tui-0.3 (crate (name "scope-tui") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libpulse-binding") (req "^2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libpulse-simple-binding") (req "^2.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26") (features (quote ("all-widgets"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1ywm0c9px8njkpmd1z6y1r7d4bhdw4lhh6gnfbbscg32rpyjzjsp") (features (quote (("default" "tui" "pulseaudio")))) (v 2) (features2 (quote (("tui" "dep:ratatui" "dep:crossterm") ("pulseaudio" "dep:libpulse-binding" "dep:libpulse-simple-binding"))))))

(define-public crate-scope_gc-0.1 (crate (name "scope_gc") (vers "0.1.0") (hash "1z3c89x75g3p323lz27jh4nb6cv73hjxzynka4iia8wjynj8hhqn") (yanked #t)))

(define-public crate-scope_gc-0.1 (crate (name "scope_gc") (vers "0.1.1") (hash "04x1x3m4qgzzszv137bf0pnapwhpjx3194rgn281p9jang2x4h22")))

(define-public crate-scope_gc-0.1 (crate (name "scope_gc") (vers "0.1.2") (hash "1jfgigmx40bxkw5igc49yvi0pwwz6b6mz8wq669wn0s1x5vfxbyr")))

(define-public crate-scope_gc-0.2 (crate (name "scope_gc") (vers "0.2.0") (hash "0qsrbhf2w5nisinjkykzkkxw3plb2lpv9l23ja9sb5zazyd9zbss")))

(define-public crate-scope_gc-0.2 (crate (name "scope_gc") (vers "0.2.1") (hash "1ivqr09mywza3arhahsbgm1d0350kg6dcmbzjzgfn6nfi19fl60v")))

(define-public crate-scope_gc-0.2 (crate (name "scope_gc") (vers "0.2.2") (hash "0n7zqa60iwa0jfnykxj20zkp0zi91dnx2w16m3rxp0h0dnbwajzq") (features (quote (("root_ref_coerce_unsized" "_nightly") ("_nightly"))))))

(define-public crate-scope_gc-0.2 (crate (name "scope_gc") (vers "0.2.3") (hash "0022v0cs88q23lspawm35376whw9faim19sxn760l4mgjm1zm192") (features (quote (("root_ref_coerce_unsized" "_unsize" "_coerce_unsized") ("_unsize") ("_coerce_unsized"))))))

(define-public crate-scope_gc-0.2 (crate (name "scope_gc") (vers "0.2.5") (hash "1mip50y47nqqss975k5xq59qfnw4yh76svd6bqnavn4ac1gmcdz0") (yanked #t)))

(define-public crate-scope_gc-0.2 (crate (name "scope_gc") (vers "0.2.6") (hash "1clagm2pgj7ldhlk5mjvr9kg62qw9g38xlp48g5im7d4jg4amxky") (features (quote (("root_ref_coerce_unsized" "_unsize" "_coerce_unsized") ("_unsize") ("_coerce_unsized"))))))

(define-public crate-scope_timer-0.1 (crate (name "scope_timer") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "18bdp38r48sy637nbfb4162fhbxp88v87x95hjkbf2dcm5pin2jm")))

(define-public crate-scope_timer-0.1 (crate (name "scope_timer") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "0k6wshjvg6jidwkvrckas0xw2dhvr46mgm5mxgjvc49yp75gsdh7")))

(define-public crate-scope_timer-0.2 (crate (name "scope_timer") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "1qk9kd4cryq0dpskw504h8qr32w3qlz2kd0rc29gxi1z2p17snya")))

(define-public crate-scope_timer-0.2 (crate (name "scope_timer") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "067jxr012a884syh5d6i3sp5wp88f6c9wpc1d9hghiqmgwhnqc0s") (yanked #t)))

(define-public crate-scope_timer-0.2 (crate (name "scope_timer") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "0m5k7jmkvnl6870rsc8a0sxl9cy5p55i93fh7zdl3jklmvhpw76q") (yanked #t)))

(define-public crate-scope_timer-0.2 (crate (name "scope_timer") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "1ygrvx56vqa1s4lahq2brznr78im89xjp78hgcnzhcinkbwi4kgb")))

(define-public crate-scopeclock-0.1 (crate (name "scopeclock") (vers "0.1.0") (hash "0ch61zx28b6r0zs5byrwxdqd2jkq0350bipy4f5p3sp5aj73zznw")))

(define-public crate-scoped-0.1 (crate (name "scoped") (vers "0.1.0") (hash "06vk8ai5mpl1x7hfxgq5y0q2jvz4lcj6az0y5jqhbqk2pymmvgyk")))

(define-public crate-scoped-0.1 (crate (name "scoped") (vers "0.1.1") (hash "0p7gz8s66nxih4lnhbm5j4algc6xxgpsgcnqskz8v27745q278b3")))

(define-public crate-scoped-0.1 (crate (name "scoped") (vers "0.1.2") (hash "14lbl43ivswmhvhnkx0002c76y040frh2pnhwgcbp7yv10idkznk")))

(define-public crate-scoped-arena-0.1 (crate (name "scoped-arena") (vers "0.1.0") (hash "0i0a747facfb4skd6ld7lji4dpvgwsxilwc62clysi84b84qnip1") (features (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-scoped-arena-0.1 (crate (name "scoped-arena") (vers "0.1.1") (hash "1wws5bmy704vqiafd7ygg34ssirdmrswqmp75bfkmngdrvdpj27h") (features (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-scoped-arena-0.2 (crate (name "scoped-arena") (vers "0.2.0") (hash "1xwxmp8x1bxmv5dgm91sir1hkwjv54s407mcq4r4fpdp2wpmlc23") (features (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.2 (crate (name "scoped-arena") (vers "0.2.1") (hash "09yxh8jcjhrv9j2qjvz1mf0dapax1is9lr87qrbhd2ajrhssa8nx") (features (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.3 (crate (name "scoped-arena") (vers "0.3.0") (hash "1gaa9n95lmyqbhhm9smpmlf2y59vkc7bgvqrv5xkxzviac1wjxsn") (features (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.4 (crate (name "scoped-arena") (vers "0.4.0") (deps (list (crate-dep (name "bumpalo") (req "^3.7") (features (quote ("allocator_api"))) (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1v2y2hg18qpibxqk85nb7xxn90nby3bppr194v4049bfc0kjgkjr") (features (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-arena-0.4 (crate (name "scoped-arena") (vers "0.4.1") (deps (list (crate-dep (name "bumpalo") (req "^3.7") (features (quote ("allocator_api"))) (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0rhx1zayjnghdiljvfqnx4v4j1im56ccx3mkg3hbnfnmyb90ypb4") (features (quote (("default" "alloc") ("allocator_api") ("alloc"))))))

(define-public crate-scoped-callback-0.1 (crate (name "scoped-callback") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qqg2b8nrhgr8d4j41dk0fh2n8x2rcchnf6r9pvh9lk6md407vdn")))

(define-public crate-scoped-callback-0.1 (crate (name "scoped-callback") (vers "0.1.1") (deps (list (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x1zmzq3i0n86avkkzri9ar9b5qgpicljjsjs8if4jrpvycmwsxd")))

(define-public crate-scoped-callback-0.2 (crate (name "scoped-callback") (vers "0.2.0") (deps (list (crate-dep (name "futures-util") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0wi29j63h0z37y20lm9sx6z5hl8a6sq3qga7f8rv5kwcq9phhwwb") (features (quote (("std") ("default" "async" "std") ("async" "futures-util"))))))

(define-public crate-scoped-env-1 (crate (name "scoped-env") (vers "1.0.0") (hash "0s5lbpnmaclcfs7ic9327i03babm4j6rnq5fyjzx69spfk08dpkq")))

(define-public crate-scoped-env-2 (crate (name "scoped-env") (vers "2.0.0") (hash "1wsa9lc3dx8zqradpccpy4kaj652h2zk2zc9gmwlnyjyvswhry8j")))

(define-public crate-scoped-env-2 (crate (name "scoped-env") (vers "2.1.0") (hash "09n0q6v9plj1s3vmd8s3nh8wclcwirayrx0bnwdswn4hinkkhqx8")))

(define-public crate-scoped-executor-0.0.0 (crate (name "scoped-executor") (vers "0.0.0") (hash "1y8nilvpb4klib0nxi3fbmrqyxi27vxla766s457spzgr59m3w8b")))

(define-public crate-scoped-futures-0.1 (crate (name "scoped-futures") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.24") (features (quote ("alloc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13yc96vhaz714bygg0j57rv6h8i2ykndly5jzviccagd5qm2fwbn") (features (quote (("std" "futures"))))))

(define-public crate-scoped-futures-0.1 (crate (name "scoped-futures") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.24") (features (quote ("alloc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fdg86lacgkvzrs46lkx5bb39ddghnrb9d8imlvvpj4iag7qdv0m") (features (quote (("std" "futures"))))))

(define-public crate-scoped-futures-0.1 (crate (name "scoped-futures") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (features (quote ("executor"))) (kind 2)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16jwr5sfpns04m28fl1vg8j0v71fnz1qc8z06j51gw3jf4i0alv5") (features (quote (("std"))))))

(define-public crate-scoped-futures-0.1 (crate (name "scoped-futures") (vers "0.1.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("executor"))) (kind 2)) (crate-dep (name "pin-utils") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rx4ggkh63d1syxp5xl9b84kxv4ix85j4qw7sfdhr59pqqj3wixi") (features (quote (("std") ("default" "std"))))))

(define-public crate-scoped-gc-0.1 (crate (name "scoped-gc") (vers "0.1.0") (hash "0sw5qpb7mirf2jy934bc9bn60ln4x0w0mrhzyddd1npzgn2s3bcz")))

(define-public crate-scoped-gc-0.1 (crate (name "scoped-gc") (vers "0.1.1") (hash "14cf4j7xfd2v7xbi4bims5g0a9m4fff5bllnxsdvjq5zpyz9w0d4")))

(define-public crate-scoped-gc-0.1 (crate (name "scoped-gc") (vers "0.1.2") (hash "1hkcnl5frig24rmcjjmgsf8p3vdsnrsiv3hp92f8d2q0w88c524z")))

(define-public crate-scoped-gc-0.1 (crate (name "scoped-gc") (vers "0.1.3") (hash "0kgz827c5ckjv9mbmsi5s7270md5ymkaz2rvyqf9k1i6n39xpy2s")))

(define-public crate-scoped-gc-0.1 (crate (name "scoped-gc") (vers "0.1.4") (hash "0g35k3ksg1km0bq0kqi0v4w73i76nn87asw0pj5xjzifv70y1160")))

(define-public crate-scoped-gc-0.1 (crate (name "scoped-gc") (vers "0.1.5") (hash "088qqmhmgpiv6yd2kdxsyp7i3sm13f6b8jjqh42dwpbb9lg3c0if")))

(define-public crate-scoped-gc-derive-0.1 (crate (name "scoped-gc-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0hic2qw550ikadp3rlci81a55n22vwh0w77minlrvr8n165hzxi8")))

(define-public crate-scoped-gc-derive-0.1 (crate (name "scoped-gc-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0v5abpaxxsdpvi7gkrnv74z5k4xyx1iv4bl6i8mdy3fg9sd6z5g9")))

(define-public crate-scoped-gc-derive-0.1 (crate (name "scoped-gc-derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0kxsjrrc6nsqgdq5nka4layaqi2s5qg723rnhdkgg2s72kvaqmq5")))

(define-public crate-scoped-gc-derive-0.1 (crate (name "scoped-gc-derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1kc3yfsbsbjbxzmnz695zyziw237w32gm1djsqd90d5k1mm461vb")))

(define-public crate-scoped-gc-derive-0.1 (crate (name "scoped-gc-derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1ys29db51s0yhhbisa96xf2jc5qip5csa9dhx213c16dkyaahw0d")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "09nkxvm45j2lpr8jw4fknl540av8adc48wqa7jnb6j0yf83j014b")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "1is3wvbqcmc2690s7ydf1h2pfx313rpkld6s0c0q2plhqwp53bz7")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "198lnrx9kkjd83i7ib82si7kaip0g1lia5qfnq2pvsh20ivc6g26")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vkxyw6z0wqm3rykwbz63pjgj811xrs4s0lgxs0jinlqzrcjh2vy")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.4") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "1amfc4gsy98g8kbmjbr1g72nngj7mqihgp0pg2mnn8anj18vjl4w")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.5") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "08f7d3vvhq53d0pv6yl5jcsx0ak3gqwjm75jmclbx1bc0qis9wap")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.6") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jj2nncf9dn7w20d2xl1g9426gs271lnsrrv3gigr2jm9vhi0hpk")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.7") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fwkrdd96bjnqd5gdz1w0kjpwhfx9gy45ygy0fx2v6n083my4pzg")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.8") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mgg4s4qdvlk3sgg2j85r3pzxr3zsg0dk20wsc3rw5w6ybv84xcv")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.9") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "1avf7ihgh72nd1nw15yzwrbmv770n3779l4x9m66m12j3my9cs2j")))

(define-public crate-scoped-pool-0.1 (crate (name "scoped-pool") (vers "0.1.10") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "08dniyq2p0knwkafsc6aabxs3aa843d8m7z59553qcxrzy43n74i")))

(define-public crate-scoped-pool-1 (crate (name "scoped-pool") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hqbq2k5idk7csrvivwvhvbs9g9jbl5cddfjkvjmqm04wwaklyl1")))

(define-public crate-scoped-sleep-0.1 (crate (name "scoped-sleep") (vers "0.1.0") (hash "0wk789d4034lslj32jx2w98mw1f5spwzxnq80pyr0zg5yikzs1pk")))

(define-public crate-scoped-stream-sink-1 (crate (name "scoped-stream-sink") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-sink") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (features (quote ("sink"))) (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("test-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0xfipy3b8bcjpc7s5yfsq96n71smfpa230i17yd3q1gfdkqdfh78")))

(define-public crate-scoped-stream-sink-1 (crate (name "scoped-stream-sink") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-sink") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (features (quote ("sink"))) (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("test-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1d3dj3ns30l82il30hkskrrnfp89s9r7sv53673a1a1pxcjqla77")))

(define-public crate-scoped-stream-sink-1 (crate (name "scoped-stream-sink") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-sink") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (features (quote ("sink"))) (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("test-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1gfqnpi3dgvnm6ri79yv32b27frqsys1k197r3l1zc67b2lwhlvv") (features (quote (("std" "futures-core/std" "futures-sink/std") ("default" "std"))))))

(define-public crate-scoped-stream-sink-1 (crate (name "scoped-stream-sink") (vers "1.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-sink") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (features (quote ("sink"))) (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("test-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0nyn1d9byvslniz4ci7dynrfxcbg6s0xafpn20m7cn0ampz8352n") (features (quote (("std" "futures-core/std" "futures-sink/std") ("default" "std"))))))

(define-public crate-scoped-stream-sink-1 (crate (name "scoped-stream-sink") (vers "1.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3") (kind 0)) (crate-dep (name "futures-sink") (req "^0.3") (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (features (quote ("sink"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("test-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "09c3hs8xlmn1whgbl9vsbi8vs7nsv7lh4kjkqf4c1lxx26qcgdl5") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:futures-util" "futures-core/std" "futures-sink/std") ("either" "dep:either"))))))

(define-public crate-scoped-stream-sink-1 (crate (name "scoped-stream-sink") (vers "1.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3") (kind 0)) (crate-dep (name "futures-sink") (req "^0.3") (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (features (quote ("sink"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("test-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "11gq3slcrp4m9cp9lnpcqzknydkfysa89srkifq3vz2xbpacpwn0") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:futures-util" "futures-core/std" "futures-sink/std") ("either" "dep:either"))))))

(define-public crate-scoped-thread-0.1 (crate (name "scoped-thread") (vers "0.1.0") (hash "0lx61xfnkkkm9kd844a7n299zpk0dx8836bbaz7li69drgs3sz7y")))

(define-public crate-scoped-thread-0.1 (crate (name "scoped-thread") (vers "0.1.1") (hash "0y20azsfp9nayhzhx59x38kq2s4xiiivivbg087adnfixy4s77ac")))

(define-public crate-scoped-thread-pool-1 (crate (name "scoped-thread-pool") (vers "1.0.1") (deps (list (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w2w4ibjidinj0x1wwpi4k84wr7sbrbvrx0cvpqna2w7s2n3vyk3") (yanked #t)))

(define-public crate-scoped-thread-pool-1 (crate (name "scoped-thread-pool") (vers "1.0.2") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "0l3kqqci3kdadqjy4f0v8zcb6nvfr3zawli5zhzgcr87qnh45x54") (yanked #t)))

(define-public crate-scoped-thread-pool-1 (crate (name "scoped-thread-pool") (vers "1.0.3") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "119d5q6d3md342azvf3s4ywpl4mpkyss6x4b2k9xp322k3rbzw8q") (yanked #t)))

(define-public crate-scoped-thread-pool-1 (crate (name "scoped-thread-pool") (vers "1.0.4") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "variance") (req "^0.1") (default-features #t) (kind 0)))) (hash "1axj2p1563v5m2pl5dyxfa4ajdkbvz8w9bqw73f6bsfmc7xvcyzl")))

(define-public crate-scoped-threadpool-std-0.1 (crate (name "scoped-threadpool-std") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)) (crate-dep (name "pwhash") (req "^1") (default-features #t) (kind 0)))) (hash "0hpjbcs2247mp51n3zywxm53dvd8f6r5zx4q983shfz0ah4fj4ha")))

(define-public crate-scoped-threadpool-std-0.1 (crate (name "scoped-threadpool-std") (vers "0.1.1") (deps (list (crate-dep (name "pwhash") (req "^1") (default-features #t) (kind 2)))) (hash "0n2smwm2hrwdcw8i4qdc8lq21wfjqyqrjsknqycq5c8q770lzzcy")))

(define-public crate-scoped-tls-0.1 (crate (name "scoped-tls") (vers "0.1.0") (hash "0z8lgy007ffc9rdl6rhl7jxx6imxx641fmm7i044bsb3y0nw45zl")))

(define-public crate-scoped-tls-0.1 (crate (name "scoped-tls") (vers "0.1.1") (hash "190qj9zdxidsjc49d6blwycy3jcc36zs6x7lfqj9x234r4wx8x46") (features (quote (("nightly"))))))

(define-public crate-scoped-tls-0.1 (crate (name "scoped-tls") (vers "0.1.2") (hash "0a2bn9d2mb07c6l16sadijy4p540g498zddfxyiq4rsqpwrglbrk") (features (quote (("nightly"))))))

(define-public crate-scoped-tls-1 (crate (name "scoped-tls") (vers "1.0.0") (hash "1hj8lifzvivdb1z02lfnzkshpvk85nkgzxsy2hc0zky9wf894spa")))

(define-public crate-scoped-tls-1 (crate (name "scoped-tls") (vers "1.0.1") (hash "15524h04mafihcvfpgxd8f4bgc3k95aclz8grjkg9a0rxcvn9kz1") (rust-version "1.59")))

(define-public crate-scoped-tls-hkt-0.1 (crate (name "scoped-tls-hkt") (vers "0.1.0") (hash "0cv928dmvbvs2gr7ng8mn6rwj746lv4kqml6dcdwxlwqalkyk5if")))

(define-public crate-scoped-tls-hkt-0.1 (crate (name "scoped-tls-hkt") (vers "0.1.1") (hash "1rgivdy308yq5fc4a81xzn4jxcmlxalikc6rr947hg31676ix4bb")))

(define-public crate-scoped-tls-hkt-0.1 (crate (name "scoped-tls-hkt") (vers "0.1.2") (hash "0qrsmm2013ry5yl7sv5qb6hkr48y1sp3c4d7mbxyh9xjvpmdgsf2")))

(define-public crate-scoped-tls-hkt-0.1 (crate (name "scoped-tls-hkt") (vers "0.1.3") (hash "162fwrlzgwg7qc3rql3m6fh9ndq3ry9l7pvs7s2q14jzciq8qx2k")))

(define-public crate-scoped-tls-hkt-0.1 (crate (name "scoped-tls-hkt") (vers "0.1.4") (hash "1p6x35cffxr6y293ip4m8pnf60bbgyvg06q7rb3gdn8h6ifpdp1x")))

(define-public crate-scoped-trace-0.1 (crate (name "scoped-trace") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "0sz74zbnbi7sqnl53zby3gydpdv2q3d0ikq0jfla3my4xnxpmhkx") (rust-version "1.61")))

(define-public crate-scoped-vec-0.0.1 (crate (name "scoped-vec") (vers "0.0.1") (deps (list (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1b6fj54pwy0jail0lgnbryq6zqlwkrpy66jqcz378i49g20rh8aj")))

(define-public crate-scoped_allocator-0.1 (crate (name "scoped_allocator") (vers "0.1.0") (hash "1mmzf0s9rk2bdmnh2a8xxbli6gi33sx6cxamfi9vcms0bncwpr5n")))

(define-public crate-scoped_allocator-0.1 (crate (name "scoped_allocator") (vers "0.1.1") (hash "1l3w0arkmffb9ka8mg6qvcybhbdfivafzhibg3q9v3v4yykdw1q9")))

(define-public crate-scoped_allocator-0.1 (crate (name "scoped_allocator") (vers "0.1.2") (hash "0vhqxrn0zi3splw75ca9g37vqhrvsyhwq17f7n35ifyfrjp93lav")))

(define-public crate-scoped_allocator-0.1 (crate (name "scoped_allocator") (vers "0.1.3") (hash "1l7zwbk9qkb3nnliadsgb8j13f4yjy1lk29q4vzfahfrk9y8ykpb")))

(define-public crate-scoped_allocator-0.1 (crate (name "scoped_allocator") (vers "0.1.4") (hash "13a11zqnvm4kw4l5sk9apy6i5vqc6dxqvwng2af2n9cd3014k5m4")))

(define-public crate-scoped_async_spawn-0.1 (crate (name "scoped_async_spawn") (vers "0.1.0") (deps (list (crate-dep (name "pin-cell") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "pin-weak") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "scoped-tls") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9.0") (features (quote ("union" "const_generics"))) (default-features #t) (kind 0)))) (hash "0sbw6x94i78k2scfn31gjjrbi0g03g67r7rnyfrvgzp7x2jhd7pi")))

(define-public crate-scoped_cell-0.1 (crate (name "scoped_cell") (vers "0.1.0") (hash "052cl2avskp93xcwncg945nzzph34kpc8hzha1hlimgac50q35iy")))

(define-public crate-scoped_css-0.0.1 (crate (name "scoped_css") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1cb1cdzhiap2a8qm9xmlhwjxvvk6s5mmx5v512dp8yji6bzz0sz8")))

(define-public crate-scoped_log-0.1 (crate (name "scoped_log") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0vh2vbr0ax99z2zmd0skj1v6mg817zvyqj73dhy1y7fgcikfi17p") (yanked #t)))

(define-public crate-scoped_name-0.2 (crate (name "scoped_name") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ustr") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "126pr8kq57dzyrpxw7ajq31q3xbiqmlhjnw8xk0m8bh9bf7g4vkn")))

(define-public crate-scoped_reference-0.1 (crate (name "scoped_reference") (vers "0.1.0") (hash "1q8liqx1d211rqwcgm1g4nryi71va2vk2d2iskrjxncw5p88alw5")))

(define-public crate-scoped_reference-0.1 (crate (name "scoped_reference") (vers "0.1.1") (hash "0p2sv5zl8d319g1pxrv60mpas2d55ziz857zxjpbl8wx804zqyfi")))

(define-public crate-scoped_reference-0.1 (crate (name "scoped_reference") (vers "0.1.2") (hash "06abf948saa73w6hwbfvr715vny10inlhisdqmq04mj15absc9fh")))

(define-public crate-scoped_reference-0.1 (crate (name "scoped_reference") (vers "0.1.3") (hash "02f4s28znv2lcnrzrg4ycfyblmij6c2fd43s2wd0rma8d0qjk3j8") (features (quote (("std") ("default" "std"))))))

(define-public crate-scoped_signal-0.1 (crate (name "scoped_signal") (vers "0.1.0") (deps (list (crate-dep (name "array-macro") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "1jpp82i6fyv5bdm6k27zhnxjkyh6hn8b4jhhr88qxbksqp3cwkk8")))

(define-public crate-scoped_signal-0.1 (crate (name "scoped_signal") (vers "0.1.1") (deps (list (crate-dep (name "array-macro") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "0699wmsyjl6vp9x1s7lln8fsqi2z1bnw8j63bl0b4p8i7y7wih3c")))

(define-public crate-scoped_signal-0.1 (crate (name "scoped_signal") (vers "0.1.2") (deps (list (crate-dep (name "arr_macro") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "array-macro") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "1fd61qw5494raf5gxxh8jl1xqy1ha1khc9snmjf63f2b4q4qi902")))

(define-public crate-scoped_spawn-0.1 (crate (name "scoped_spawn") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0wma51d30j5wvcjm75w6fg3pm7ja14c7bzf2hmhlkjrl5gijh3rp")))

(define-public crate-scoped_spawn-0.1 (crate (name "scoped_spawn") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2.2") (features (quote ("macros" "rt-core" "time"))) (default-features #t) (kind 2)))) (hash "0zqcavsv5jncwvcic4hpvkj8gkgr5nk85dkd446bv9g8sly7d4a1")))

(define-public crate-scoped_spawn-0.2 (crate (name "scoped_spawn") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2.2") (features (quote ("macros" "rt-core" "time"))) (default-features #t) (kind 2)))) (hash "0md7xz92hz0vd4w85qagdklif0g4qsg7g93csm6i8l3lacazfdnh")))

(define-public crate-scoped_spawn-0.2 (crate (name "scoped_spawn") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2.2") (features (quote ("macros" "rt-core" "time"))) (default-features #t) (kind 2)))) (hash "08b6rpw60yvk1sv7g413wvbqh66s7vm8flwcjl4a3rqwm5avyxvf")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.0.0") (hash "08xjng8ifc760hlxhfljvcz6w71rsk1g7xkm8j1yzfp4bnv8syjn")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.0.1") (hash "0jbb8icndjzvx4dmjcklsb2daid6qld4cp8nywr0nij0ljsl9kia")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.0.2") (hash "07zvrsw5bwz5zc63wa0z31mg37z1jmmdld5qq27551s076d810rr")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.0.3") (hash "02akih5yyfgamj5mlcmdhfl6ccysc23225njjq7vs1c1hcvnz4rs")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.0.4") (hash "19rxlzv04gk8npjprmdlbg2jpyfscs6yafjdc51adw783pq6ajwm")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.0.5") (hash "040wipk3jbj1dk6ycfnyna04dagqs2rgmhzv2v0kadkwvjwywyz1")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.0.6") (hash "0ak77xaqhgy1k0rrxk4mxb55jih005lmx1504zphlgax36q8rq16")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.1.0") (hash "0ganah1x2x9gqvxf9yg4hh6cjwjw57axp1s1bqd58rlwx4970xlq")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.1.1") (hash "15chirm9hamnsp1vfafrkr1ib58kv2403s6va34ri1b9v24z3n4r")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.1.2") (hash "0247isrks28xap7kcq56zsh9js9qj4zc5h7i2k1syyr98mmhchb1")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.2.0") (hash "15fcwl1pnvb1m8j2hh6bi3xhjb38f0sjjs0qkxcjwgcbpg4zywg2")))

(define-public crate-scoped_stack-1 (crate (name "scoped_stack") (vers "1.2.1") (hash "0mi79p57wmxfrbi4r64fg91z5w830ng2228dba38y4nya982fnas")))

(define-public crate-scoped_stateful_threadpool-0.1 (crate (name "scoped_stateful_threadpool") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)))) (hash "1m7vxigmgm3pickv6xzdj2r5r2yk5i3rambk897k08py2a38gimg") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-scoped_tasks_prototype-0.1 (crate (name "scoped_tasks_prototype") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0m5585i1wbxhsjnjk1kfp0l1gx007wf6lb6kf2ypq096vh6m9a44") (rust-version "1.65")))

(define-public crate-scoped_thread_local-0.1 (crate (name "scoped_thread_local") (vers "0.1.0") (deps (list (crate-dep (name "reborrow") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "12a64g0knmbs8b707fxvd0qjhdkvn4h6cv0cfai5fcl5syp012fq")))

(define-public crate-scoped_thread_local-0.1 (crate (name "scoped_thread_local") (vers "0.1.1") (deps (list (crate-dep (name "reborrow") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1ic5b24z352dsvhja9kgvqnhnhb1xc0fq9mrxb41lazwndpas81q")))

(define-public crate-scoped_thread_local-0.1 (crate (name "scoped_thread_local") (vers "0.1.2") (deps (list (crate-dep (name "scopeguard") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0s21d9ck4s6hbavh6mc9gs59lxbskg309w4y1bn4ibbp9agj697y")))

(define-public crate-scoped_thread_local-0.1 (crate (name "scoped_thread_local") (vers "0.1.4") (hash "13snbxf4wnxz63r93kx7q8l5i6qh0d3w5r3cr97ybc3ah376kv9d")))

(define-public crate-scoped_thread_local-0.1 (crate (name "scoped_thread_local") (vers "0.1.5") (hash "10rw532wdk81xw1xfd1mr9h9z17s8xw63sv5lbc440gag6cvdvbl")))

(define-public crate-scoped_thread_local-0.1 (crate (name "scoped_thread_local") (vers "0.1.6") (hash "0r7s6kd48wjaaj75sima2s49pr9wd5dnj6azvrxxd0fcsj0h6ci9")))

(define-public crate-scoped_thread_local-0.2 (crate (name "scoped_thread_local") (vers "0.2.0") (hash "0zzk31wf6a993h3kw52d7hiqbnmndfymcqk2kskfwij42i2cmz0h") (yanked #t)))

(define-public crate-scoped_thread_local-0.3 (crate (name "scoped_thread_local") (vers "0.3.0") (hash "0wpzb7pppii6x7lyhc65j3n54kgs177bsd1610n8hsb82fwqxyrd")))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 2)))) (hash "0szx3zvja72p0pqg7ri14f707p2acr9sacp8v3bhrh7gs85hxwdl") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 2)))) (hash "1kh43pilmmih5iykk7q76av685yign2rr6rgckhvwdbrhzz9c4v2") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 2)))) (hash "1wbcsxgfwfj20xaw3zlpszfj5av8r3v6k30yiqhxicfvzhn3jylb") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 2)))) (hash "0z9r63q7p0fkbinhmnx846hwqcq8884mchpp9fb0b3illqvgqxqs") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 2)))) (hash "00njmq94k2mawcm3jp3dfc8b3hvnh5q9083rr8c8gdvr107ihc55") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 2)))) (hash "0bvwa1w9yzy5l7lxacxf7ws13ri1mpy0l0a19952hlvzdlwjn7k3") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.1") (default-features #t) (kind 1)))) (hash "172vyijlfaad7zymqyllrs5iccrhrfm39vahnywxrn0sqbi28wnq") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^0.1.15") (default-features #t) (kind 2)))) (hash "0nm8rp9ds93g29pbjywpcpr6nfmbgx19bs4njsmbg31yi749kwry") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)))) (hash "1401ns7pgwk73drvfkibq7qky7c9c2cc8is8ac4ixw7g7kz5k92f") (features (quote (("nightly"))))))

(define-public crate-scoped_threadpool-0.1 (crate (name "scoped_threadpool") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "1a26d3lk40s9mrf4imhbik7caahmw2jryhhb6vqv6fplbbgzal8x") (features (quote (("nightly"))))))

(define-public crate-scoped_threads-0.1 (crate (name "scoped_threads") (vers "0.1.0") (hash "0na3vq9m4bi83ncvlic8aycgrw0291pjq5j6mdfvkrk0qavdx803") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-scopefunc-0.1 (crate (name "scopefunc") (vers "0.1.0") (hash "1xxclpzc5ipdhhf2xkrxzc0n4kzr1w1spixcx76l4cb5gqv8h7wp")))

(define-public crate-scopefunc-0.1 (crate (name "scopefunc") (vers "0.1.1") (hash "0qm7db39xrk7wx3zrnw0vghg84npzdpbpgknkfj12g73nc0x6f4q")))

(define-public crate-scopegraphs-0.1 (crate (name "scopegraphs") (vers "0.1.0") (hash "0bgg255z01cm7zw5aka5wgqg9dsrsanbbls43y6x1md9rkn7b3a5")))

(define-public crate-scopegraphs-0.1 (crate (name "scopegraphs") (vers "0.1.2") (deps (list (crate-dep (name "scopegraphs-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bp8s0lsdx8hw8y2w6wjgxic2mxil2svfl6rl5a2q6lmfbp3d7zx") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot") ("default" "dot" "dynamic-regex"))))))

(define-public crate-scopegraphs-0.1 (crate (name "scopegraphs") (vers "0.1.3") (deps (list (crate-dep (name "scopegraphs-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lx98fc4884j062ffpq6462b1yzbvvwc8hi5sm51nb7aikca7a2f") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot" "scopegraphs-lib/dot") ("default" "dot" "dynamic-regex")))) (rust-version "1.77")))

(define-public crate-scopegraphs-0.2 (crate (name "scopegraphs") (vers "0.2.0") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "dot") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14zn75xrxp8mw64cdc3rwclhq6w1gfn2903py3ch18gfbb00xjqh") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("documentation") ("default" "dot" "dynamic-regex")))) (v 2) (features2 (quote (("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot" "dep:dot")))) (rust-version "1.77")))

(define-public crate-scopegraphs-0.2 (crate (name "scopegraphs") (vers "0.2.1") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "dot") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "104wl9zwjck3kbbsc42hacxdb5kspxwh50k6w8z6225m9iv3zfzh") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("documentation") ("default" "dot" "dynamic-regex")))) (v 2) (features2 (quote (("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot" "dep:dot")))) (rust-version "1.77")))

(define-public crate-scopegraphs-0.2 (crate (name "scopegraphs") (vers "0.2.3") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "dot") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1bpmycm6r1n25zsn37sl0fiwcj9ddv704f1z572ikp2mqimvkdkh") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("documentation") ("default" "dot" "dynamic-regex")))) (v 2) (features2 (quote (("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot" "dep:dot")))) (rust-version "1.77")))

(define-public crate-scopegraphs-0.2 (crate (name "scopegraphs") (vers "0.2.4") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "dot") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05wpjlq521py6cj0ib0cz7y67dxxv6451z7vpr80zcdw3099gyhb") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("documentation") ("default" "dot" "dynamic-regex")))) (v 2) (features2 (quote (("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot" "dep:dot")))) (rust-version "1.77")))

(define-public crate-scopegraphs-0.2 (crate (name "scopegraphs") (vers "0.2.5") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "dot") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rgmp52jq3sv6df82j6pslhgcdzimbyjmqds3s99lfmdy8b8hhwm") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("documentation") ("default" "dot" "dynamic-regex")))) (v 2) (features2 (quote (("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot" "dep:dot")))) (rust-version "1.77")))

(define-public crate-scopegraphs-0.2 (crate (name "scopegraphs") (vers "0.2.6") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "dot") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0m60r08mnnybm794x3rz9kz4m2m0cpnjgb7fzj41s2jal3yb62jv") (features (quote (("dynamic-regex" "scopegraphs-regular-expressions/dynamic") ("documentation") ("default" "dot" "dynamic-regex")))) (v 2) (features2 (quote (("dot" "scopegraphs-regular-expressions/dot" "scopegraphs-macros/dot" "dep:dot")))) (rust-version "1.75")))

(define-public crate-scopegraphs-lib-0.1 (crate (name "scopegraphs-lib") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1365s97hlldvvlaj9byrc3f8cphs5gcw87r32g81n3s39pfwl7vy")))

(define-public crate-scopegraphs-lib-0.1 (crate (name "scopegraphs-lib") (vers "0.1.2") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "189gny5dzdmci7qqi0jynmzhk4vfcb0zk0ldddvmv499j5sg1l55")))

(define-public crate-scopegraphs-lib-0.1 (crate (name "scopegraphs-lib") (vers "0.1.3") (deps (list (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "dot") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-prust-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "15sm676qzzs39ksvgfgjgq6258ncnfm945jr9m14r19wpa410sgq") (features (quote (("default")))) (v 2) (features2 (quote (("dot" "dep:dot")))) (rust-version "1.77")))

(define-public crate-scopegraphs-macros-0.1 (crate (name "scopegraphs-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (features (quote ("rust-code-emitting" "dot"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15g7aanlblcwwykxw7z4b86hdkmznjlw4kpk61czhyxs764682hi") (features (quote (("dot" "scopegraphs-regular-expressions/dot"))))))

(define-public crate-scopegraphs-macros-0.1 (crate (name "scopegraphs-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (features (quote ("rust-code-emitting" "dot"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wyiz7fzn33ghq5pn8z7qpvch52zag6gx5w15s5fpic8knivl209") (features (quote (("dot" "scopegraphs-regular-expressions/dot"))))))

(define-public crate-scopegraphs-macros-0.1 (crate (name "scopegraphs-macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.1.0") (features (quote ("rust-code-emitting" "dot"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c971aa12hkxj16gb6sgl69wccxfskpyv592l541pq9akzb7w9yb") (features (quote (("dot" "scopegraphs-regular-expressions/dot"))))))

(define-public crate-scopegraphs-macros-0.2 (crate (name "scopegraphs-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (features (quote ("rust-code-emitting" "dot"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rlzvm8kd2s0a3fl7kmlv9f85njpdb01ri5v2hhp65y8wwjlnnyf") (features (quote (("dot" "scopegraphs-regular-expressions/dot"))))))

(define-public crate-scopegraphs-macros-0.2 (crate (name "scopegraphs-macros") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (features (quote ("rust-code-emitting" "dot"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gmlyfnpw3jlchg1f0vqcybhs2hhgs4ysx5lrqq6ds5s7m9272jz") (features (quote (("dot" "scopegraphs-regular-expressions/dot"))))))

(define-public crate-scopegraphs-macros-0.2 (crate (name "scopegraphs-macros") (vers "0.2.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "scopegraphs-regular-expressions") (req "^0.2.0") (features (quote ("rust-code-emitting" "dot"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0k3n07qplfw2cc750kf092xbaln0144l04pl7ws6sk53p3svqnfs") (features (quote (("dot" "scopegraphs-regular-expressions/dot")))) (rust-version "1.75")))

(define-public crate-scopegraphs-prust-lib-0.1 (crate (name "scopegraphs-prust-lib") (vers "0.1.0") (hash "1ih5iz07z920gvkvlfm0ky1jgg7j2zzr1rhgks2i23w5d7sj7n4x") (features (quote (("thread_safe"))))))

(define-public crate-scopegraphs-regular-expressions-0.1 (crate (name "scopegraphs-regular-expressions") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1c54np9klsw6118479b3rhy45m0vszl46vyy9s0f608f0llzamk9") (features (quote (("rust-code-emitting" "quote") ("pretty-print" "quote") ("dynamic" "quote") ("dot" "pretty-print"))))))

(define-public crate-scopegraphs-regular-expressions-0.1 (crate (name "scopegraphs-regular-expressions") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1w5fxlnf5cc29z3czpbphzzc2fh648vdn06a1v2liznsagg2969v") (features (quote (("rust-code-emitting" "quote") ("pretty-print" "quote") ("dynamic" "quote") ("dot" "pretty-print"))))))

(define-public crate-scopegraphs-regular-expressions-0.1 (crate (name "scopegraphs-regular-expressions") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0ybgflf2cp4vwy804c63339gqli298hi6ajg43r21k2x2dsx47b6") (features (quote (("rust-code-emitting" "quote") ("pretty-print" "quote") ("dynamic" "quote") ("dot" "pretty-print"))))))

(define-public crate-scopegraphs-regular-expressions-0.2 (crate (name "scopegraphs-regular-expressions") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "02ljpzpr6nsjkwfmf1zvjwfyv51hwav9y5kj7fbvsz5bvncwppn4") (features (quote (("rust-code-emitting" "quote") ("pretty-print" "quote") ("dynamic" "quote") ("dot" "pretty-print"))))))

(define-public crate-scopegraphs-regular-expressions-0.2 (crate (name "scopegraphs-regular-expressions") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1xh1550yj3qyxx94ah3qxd4zzn1z9gmg119khygcaimzs6q161f7") (features (quote (("rust-code-emitting" "quote") ("pretty-print" "quote") ("dynamic" "quote") ("dot" "pretty-print"))))))

(define-public crate-scopegraphs-regular-expressions-0.2 (crate (name "scopegraphs-regular-expressions") (vers "0.2.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0b337n1jmrdyjdwi1x5a4wc507y1iarcs56q8y7yxkl7gdw5qvsw") (features (quote (("rust-code-emitting" "quote") ("pretty-print" "quote") ("dynamic" "quote") ("dot" "pretty-print")))) (rust-version "1.75")))

(define-public crate-scopegraphs-regular-expressions-0.2 (crate (name "scopegraphs-regular-expressions") (vers "0.2.10") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("parsing" "derive" "printing" "clone-impls"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0kjyni72cd413wwx8i147mf3nif3xfqi9qrk6a80krhxqccvwicx") (features (quote (("dynamic") ("dot")))) (rust-version "1.75")))

(define-public crate-scopeguard-0.1 (crate (name "scopeguard") (vers "0.1.0") (hash "1d3rxqgv4kpgg9rg7iw2q547csl2hzny3v4m1ndb16ca4pz2p6bk")))

(define-public crate-scopeguard-0.1 (crate (name "scopeguard") (vers "0.1.1") (hash "0yc9hk3nfkk3iyd703x9q9i1hiia9vmr9vghgjrskxmn7h80460n")))

(define-public crate-scopeguard-0.1 (crate (name "scopeguard") (vers "0.1.2") (hash "0mvc3ac86gmfnymmvjsdvl8djrb9xr8m2n6yv1hwab8yghapd82r")))

(define-public crate-scopeguard-0.2 (crate (name "scopeguard") (vers "0.2.0") (hash "12i95gcv612pkb6g19b65910aq1h93gb6lza70qjbyigh7jwf72m")))

(define-public crate-scopeguard-0.3 (crate (name "scopeguard") (vers "0.3.0") (hash "0vbi63vqi424g545883p7kkmig8lzkbnp03m5vflfb5xkyk8w46f") (features (quote (("use_std") ("default" "use_std")))) (yanked #t)))

(define-public crate-scopeguard-0.3 (crate (name "scopeguard") (vers "0.3.1") (hash "1frh1bf69kikkdhqfav7klbzfbdwzsaqb74wyvqlcwyzjg9di9bs") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-0.3 (crate (name "scopeguard") (vers "0.3.2") (hash "06799fck72ba7rkda5099jp8xgc8bfng7rw0v9y51hjbmk1v57n7") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-0.3 (crate (name "scopeguard") (vers "0.3.3") (hash "09sy9wbqp409pkwmqni40qmwa99ldqpl48pp95m1xw8sc19qy9cl") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-1 (crate (name "scopeguard") (vers "1.0.0") (hash "03aay84r1f6w87ckbpj6cc4rnsxkxcfs13n5ynxjia0qkgjiabml") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-1 (crate (name "scopeguard") (vers "1.1.0") (hash "1kbqm85v43rq92vx7hfiay6pmcga03vrjbbfwqpyj3pwsg3b16nj") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopeguard-1 (crate (name "scopeguard") (vers "1.2.0") (hash "0jcz9sd47zlsgcnm1hdw0664krxwb5gczlif4qngj2aif8vky54l") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-scopelint-0.0.1 (crate (name "scopelint") (vers "0.0.1") (deps (list (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0qw7j06lmhya710vx26v6dd90njf8iih1qdfaag4dm635sa8jr22")))

(define-public crate-scopelint-0.0.2 (crate (name "scopelint") (vers "0.0.2") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "135jcg4m17z80j8lmyss4gnqpyj9cn6bd0d8i5sivmn1nfa296gn")))

(define-public crate-scopelint-0.0.3 (crate (name "scopelint") (vers "0.0.3") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1z984607qc7hsy704gws7wcilvnyrkbs77bdbkgyjb23kxv3qxr7")))

(define-public crate-scopelint-0.0.4 (crate (name "scopelint") (vers "0.0.4") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1xsf226j8znp18d9mklfyspsa4csgc606zqmi7q290dvwma5yj3j")))

(define-public crate-scopelint-0.0.5 (crate (name "scopelint") (vers "0.0.5") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "04dlqimqv3dfqlaa19gcwa1afkn3ij55kwlwg0mnjgd7n2lavi2d")))

(define-public crate-scopelint-0.0.6 (crate (name "scopelint") (vers "0.0.6") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0vdxa2lcnq8aydrnxv9aa3r77r5v43imfss4z790nr1p395vq8ly")))

(define-public crate-scopelint-0.0.7 (crate (name "scopelint") (vers "0.0.7") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1rhbbw3qclyv7fb5bhizvhk322z8i9r5wxlc305qyq4ryyyrxg5r")))

(define-public crate-scopelint-0.0.8 (crate (name "scopelint") (vers "0.0.8") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1wxpb0asyhhcm8f3kdzywyybaa18d1rz6wm71hjz33wkd1gmkdz1")))

(define-public crate-scopelint-0.0.9 (crate (name "scopelint") (vers "0.0.9") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "00ckk4b0bcbabqrgyn1df40300nn66n12sfl7jmg9451116khxwb")))

(define-public crate-scopelint-0.0.10 (crate (name "scopelint") (vers "0.0.10") (deps (list (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0q9x0ba3vj23z9k0hxgk94i0hd6x7ypyn2rcnczk4gz2fq1r622g")))

(define-public crate-scopelint-0.0.11 (crate (name "scopelint") (vers "0.0.11") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "grep") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1bmcmz4f1925vzryg9wdapnv955lb2260ji2fi1g0dl3xqc7zwwx")))

(define-public crate-scopelint-0.0.12 (crate (name "scopelint") (vers "0.0.12") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1sr827lshr4qh7dxc4x02ldkgq6i9b3x3cdrdcw9w3lqb63ssa9s")))

(define-public crate-scopelint-0.0.13 (crate (name "scopelint") (vers "0.0.13") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "128f33i5f16gf3rfsphg98asxyi98x8jm419x76pnhg3yr8lrkz2")))

(define-public crate-scopelint-0.0.14 (crate (name "scopelint") (vers "0.0.14") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "00k5j3ahzgf3hnsm12n7aws0zdrisig6qph9m7nkrw740wx81a79")))

(define-public crate-scopelint-0.0.15 (crate (name "scopelint") (vers "0.0.15") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0y77asnrrap70zs9ix0vnalcqw5738gfflyshi4fvyr9am81zrkx")))

(define-public crate-scopelint-0.0.16 (crate (name "scopelint") (vers "0.0.16") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0kbdmn64zn9lj1b0a8iw1pr1062yl4rrbzm63pp385ydqnvhaga3")))

(define-public crate-scopelint-0.0.17 (crate (name "scopelint") (vers "0.0.17") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "09k72y6qfp9542mynzb9yh7qc3zb0f5scnyl83dav46x02criyz0")))

(define-public crate-scopelint-0.0.18 (crate (name "scopelint") (vers "0.0.18") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1s5yyk79r3imx3lkxnpxsny5xkc81xm141crd0lyj1hysqjkw8wq")))

(define-public crate-scopelint-0.0.19 (crate (name "scopelint") (vers "0.0.19") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "03y7ml0i63gphxv4mbq6qx84g4mdj6lmpzp3dc8n37wkz7asb63k")))

(define-public crate-scopelint-0.0.20 (crate (name "scopelint") (vers "0.0.20") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "solang-parser") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "taplo") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0h4503ja1w7f33d0pwlpkjdvj7k6acgcjwg9l9xhw565s5860lx2")))

(define-public crate-scopetime-0.1 (crate (name "scopetime") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ln1vjjs0cc7kvzcmsl9fb0ykb71gqfms4iv0hs7ccay3nqngic9") (features (quote (("enabled") ("default"))))))

(define-public crate-scopetime-0.1 (crate (name "scopetime") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0s4fg704zlc1xmwx2y5zifhndpb2ahfh4kg966gyr1kbwcd2hz0q") (features (quote (("enabled") ("default"))))))

(define-public crate-scopetime-0.1 (crate (name "scopetime") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ngkasx6l072cvigx7q5r33i8acjardr4g8jnwdrcym4758f5vb6") (features (quote (("enabled") ("default"))))))

