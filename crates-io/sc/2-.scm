(define-module (crates-io sc #{2-}#) #:use-module (crates-io))

(define-public crate-sc2-macro-1 (crate (name "sc2-macro") (vers "1.0.0") (deps (list (crate-dep (name "sc2-proc-macro") (req "^1") (default-features #t) (kind 0)))) (hash "11cq2yxnn88wb0y53xj6fd954v0mv0lbbjwpa4mlim133zznba3g")))

(define-public crate-sc2-proc-macro-1 (crate (name "sc2-proc-macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0247y4wj8wziwv2jxqqnp2k0z8c011pxwjgvy414ys2cri4p8iiv")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.0") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "1dnxzk3r5d4vki1sbvn0flhwf364xb90j43nb9s8bghh81dm1na4")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.1") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "1qz7vml0rwxd8h7s0lwvscaq446mnqbf3a6964bk5q8vdvk1d2yw")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.2") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "1ak5kg2s3kcdbkb4zrhd474vk5xp41kny8d60wzwcig1vyi6dsb3")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.3") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "0vw5r1zhfmcwawr413zbb1y0jjljfq534wmwcbrn9lwbz1w0ka3g")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.4") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "09mn9hk46phi5niy1zw8klphgwzbah47jz65xj9ay6bggl8kbky4")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.5") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "1n4wwss8gjs1dh462si3qjqavbyad8vhlpc0p273dkanw95bf2dq")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.6") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "0md6zdgrx4hw5pf33ij46gws8rgfckxm01qrgc7mias43qq72viz")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.7") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "09dz096s37addmfw9hlzly8xxb13nbmr8j4hj8271qzjaaqlw1zn")))

(define-public crate-sc2-proto-0.1 (crate (name "sc2-proto") (vers "0.1.8") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)))) (hash "16iia9cnzrrfpwnr5rnnnxh159r8wl2h8zni2v79708wknb5ly0a")))

(define-public crate-sc2-proto-0.2 (crate (name "sc2-proto") (vers "0.2.0-alpha.0") (deps (list (crate-dep (name "protobuf") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^1.5") (default-features #t) (kind 1)))) (hash "0g8l40x37kzax0jkgfha7xidvpjgdy6n9klsawwknhyh12srby8w")))

(define-public crate-sc2-proto-0.2 (crate (name "sc2-proto") (vers "0.2.1") (deps (list (crate-dep (name "protobuf") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2.3") (default-features #t) (kind 1)))) (hash "1gysjqynbb9gcy189q1clgks9dx7bap05x0kd5zy5ly2in5lim94")))

(define-public crate-sc2-proto-0.2 (crate (name "sc2-proto") (vers "0.2.2") (deps (list (crate-dep (name "protobuf") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2.20") (default-features #t) (kind 1)))) (hash "0s6d6i4ddkqf5ph6k50aqvycfi2n4qgmj0nq6kz7v30l6x7bvfy3")))

(define-public crate-sc2-proto-0.2 (crate (name "sc2-proto") (vers "0.2.3") (deps (list (crate-dep (name "protobuf") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2.20") (optional #t) (default-features #t) (kind 1)))) (hash "19amyqzc1a6q15m48q1r9byss8gygg7alz5zv9zxriqvsb28agki")))

(define-public crate-sc2-proxy-0.1 (crate (name "sc2-proxy") (vers "0.1.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "portpicker") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^2.3.0") (features (quote ("with-bytes"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sc2-proto") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "websocket") (req "^0.22.2") (default-features #t) (kind 0)))) (hash "16w53gsl2icrcrlv08g05xhjdayh495zdr9ibq95415xdsxf5m2l")))

(define-public crate-sc2-techtree-0.1 (crate (name "sc2-techtree") (vers "0.1.0") (deps (list (crate-dep (name "noisy_float") (req "^0.1.9") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jbf25fpm0vf8dn87ac8rsrd4i2xl0pkzg4n5w33j53p7hmrl59b")))

