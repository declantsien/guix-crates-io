(define-module (crates-io sc hi) #:use-module (crates-io))

(define-public crate-schindel-0.1 (crate (name "schindel") (vers "0.1.0") (deps (list (crate-dep (name "murmurhash3") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1jq5l5rpjvd8n5glzcihih5pw0gcpgzpbnsd9xz6qvgywk1j2vkw")))

(define-public crate-schip-0.0.0 (crate (name "schip") (vers "0.0.0-prerelease") (hash "01gij0xaknbp63kd9a6mq5j2yardfwx5v7awjyyfvry8h1hwzdfs")))

(define-public crate-schip8-0.1 (crate (name "schip8") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1b4h6ip6r7fnk144vpkb1qhaxgdjp8n63jbbq8gawcq7j5c11c6z")))

(define-public crate-schipperke-0.0.0 (crate (name "schipperke") (vers "0.0.0-prerelease") (hash "1mkr7k83jjyffvka83lqpimsbrf2ccl8ssw9hm3yngvrbi1j1bw8")))

(define-public crate-schiro-0.0.1 (crate (name "schiro") (vers "0.0.1") (hash "1444qvbvxpz7bhpq71fwicks67ap3a2afhws2bvw4l43vx1146vj") (yanked #t)))

