(define-module (crates-io h2 #{64}#) #:use-module (crates-io))

(define-public crate-h264-0.0.0 (crate (name "h264") (vers "0.0.0") (hash "1xbw43869qr0051dzcxgvl9a0gyzknxcd347a9v5h3vkf6iv337g")))

(define-public crate-h264-dec-0.0.0 (crate (name "h264-dec") (vers "0.0.0") (hash "0z26syn6xgdmbnkzyz3bjm67xq5sz8fzgf68kwq9k3nm8r16y406")))

(define-public crate-h264-decoder-0.1 (crate (name "h264-decoder") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytesio") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rgljgrazivgbg718cv1a9rrmb0lpa15rljsnyfxm83m4b88qxyg")))

(define-public crate-h264-decoder-0.2 (crate (name "h264-decoder") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytesio") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0sq3l1hrvijjc0k25rrjcfkxiaaxzihn2h55qlssgpjv7nkp4bg5")))

(define-public crate-h264-decoder-0.2 (crate (name "h264-decoder") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytesio") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wf11gl52a6shz0l6wdfkds8iwby6dwjvgvfp8iqsq0dkna47wy8")))

(define-public crate-h264-decoder-0.2 (crate (name "h264-decoder") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytesio") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1vhayn94yr77ccady9fbv2m5g2lcyx9xrab0ignhq8dfn685xi4n")))

(define-public crate-h264-decoder-0.2 (crate (name "h264-decoder") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytesio") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h0ndwc8qjd5ah3pp622mkqmpwz5a7bknfnv5dv3rip3w21v701m")))

(define-public crate-h264-enc-0.0.0 (crate (name "h264-enc") (vers "0.0.0") (hash "02yzxfykc2msrgdrvdnp36rlivh8lxvb42nsfpk5pg85fxw1bdwv")))

(define-public crate-h264-profile-level-id-0.0.0 (crate (name "h264-profile-level-id") (vers "0.0.0") (hash "03jh2d6r5km2azx2ksxw5rmhsfprw6213zpq8nsq5wmcysiq8s5j")))

(define-public crate-h264-profile-level-id-0.1 (crate (name "h264-profile-level-id") (vers "0.1.0") (deps (list (crate-dep (name "bitpattern") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "0nk1clbmcygnsmv9682bxvnxjs0jbqx6y8jkbmi71m5l0whr8kap") (yanked #t)))

(define-public crate-h264-profile-level-id-0.1 (crate (name "h264-profile-level-id") (vers "0.1.1") (deps (list (crate-dep (name "bitpattern") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "19dwpmxs5y49xyg8ly96v7idwgms53xhraadr7fvf52gzh1vqbp1")))

(define-public crate-h264-profile-level-id-0.2 (crate (name "h264-profile-level-id") (vers "0.2.0") (deps (list (crate-dep (name "bitpattern") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "0jd6fjpk8plglpk5m6q0ri6l70zrj446cf39n34n8yw4m7vdpl25")))

(define-public crate-h264-reader-0.2 (crate (name "h264-reader") (vers "0.2.0") (deps (list (crate-dep (name "bitreader") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0nmbhvzjk4m1xsk169kqcvcqfhcciqmws72ypgc7jbb8g0lvb5cz")))

(define-public crate-h264-reader-0.3 (crate (name "h264-reader") (vers "0.3.0") (deps (list (crate-dep (name "bitreader") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1pbk8hxkjz7xmvc254cq9z1f9ybjv4fnkyx1cw60h1c0ws4sxhc8")))

(define-public crate-h264-reader-0.4 (crate (name "h264-reader") (vers "0.4.0") (deps (list (crate-dep (name "bitreader") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0wx21gp80za90v7npsvd0bw32l2v3jl1rngakzhp3ljzawcq204w")))

(define-public crate-h264-reader-0.5 (crate (name "h264-reader") (vers "0.5.0") (deps (list (crate-dep (name "bitreader") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "rfc6381-codec") (req "^0.1") (default-features #t) (kind 0)))) (hash "188x15sb669pws0rn781yfl19xf8f4n1zgzi08cxb8zcpmlpdn68")))

(define-public crate-h264-reader-0.6 (crate (name "h264-reader") (vers "0.6.0") (deps (list (crate-dep (name "bitstream-io") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "rfc6381-codec") (req "^0.1") (default-features #t) (kind 0)))) (hash "0b1ww96c18hxpvmksl3nz8sfryv4grv22qvn3w1acx0v5y39bh53")))

(define-public crate-h264-reader-0.7 (crate (name "h264-reader") (vers "0.7.0") (deps (list (crate-dep (name "bitstream-io") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "rfc6381-codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "1psh86y4zx7pqgz96isbdnpzlb79xkmijji56gy1rirc6b68s4dx")))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "1w3sjnwwd8h13b56fr9vxjy8775pzsn932qkvw5697jawz03v20k")))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "0j0yjnqsbbyv1z5rkxq36vh7fh1cz6k1c7jlzz8f1jxn8r2nfyws")))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "1y0fq80n1agkrddqlwa9zfn2ad8f35cwl12y1jm8qgsb48sxmxy4")))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.3") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "0m0j5a3pdzd6n5r8786dd9bhvgaflg7pg73h3vjvzy4jplma6j5m")))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.4") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "1iw7hq6vsk77r3x2ii8b52sbgjql4d0k7bm9w2cii7hsxb6r2s3k")))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.5") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "0dcriwdzczr0f6gcvdbad7hillv985x0fbhddqwmnm8n02833lyb") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.6") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "13dv0vgz3550vb6ljar8g9f44yg3hp0l98wh4yw9ndx5gfn99iyf") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.7") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "19679pjlhmpxwjzjxyv0wji0mzmm24il3bz4ahpbw5bbynvh7s9m") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.8") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "03a73vhvr7cxl21kkxkqs7xv7yh42387g4yyh8yj3d643xqa25p2") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.9") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "1d0bmsami2pk5kqkgfwcm1gxcb2bjv0rq3vgk7rhng8hyg78k2z7") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.10") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "0qcgxi07wj8iyvcwljm8ppwlswqzsfmvixyy6fc6pdpivl3jpigs") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.11") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "000lxnlipm55fx9qygifk2bfc2xnjdrhsh5vqyh4hp8mpxmw265m") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1 (crate (name "h264_nal_paging") (vers "0.1.12") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (kind 0)))) (hash "177i4c93kdar9pr9m7gkmkk69iam17qpa7qgdh68297gcyynyyq4") (features (quote (("test" "tokio/full"))))))

(define-public crate-h264_webcam_stream-0.1 (crate (name "h264_webcam_stream") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 2)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 2)) (crate-dep (name "jpeg-decoder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "openh264") (req "^0.3.0") (features (quote ("encoder" "decoder"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "v4l") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0qzll3f73k2pqgrjzi6wk9pjl9qpgsr2q66r4cvc7xk7jhdnvpjv")))

(define-public crate-h264bsd-0.1 (crate (name "h264bsd") (vers "0.1.0") (deps (list (crate-dep (name "av-codec") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "av-data") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "h264bsd-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 2)))) (hash "03arnpi50a6pmmf0slpyp2asi3vlxcw3law9icvscw53vf4dgcfg")))

(define-public crate-h264bsd-0.1 (crate (name "h264bsd") (vers "0.1.1") (deps (list (crate-dep (name "av-codec") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "av-data") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "h264bsd-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 2)))) (hash "0rxhmq043z000h8sr6hxb1hxj4y3bsm5vdzqx0fk7h8sln7j4gm5")))

(define-public crate-h264bsd-sys-0.1 (crate (name "h264bsd-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1y6l21sl4fvh7fdsgwmwf51x839yfnxkq0qxcfl7ich3d88nqd2c")))

