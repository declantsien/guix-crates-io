(define-module (crates-io h2 #{56}#) #:use-module (crates-io))

(define-public crate-h256only-0.4 (crate (name "h256only") (vers "0.4.0") (deps (list (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cjw9jxc6yrskfi0fymkp34khswbcxg0y6gp0m8qvbigg550q86w")))

(define-public crate-h256only-0.4 (crate (name "h256only") (vers "0.4.1") (deps (list (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pi2r7igaksm2y73r6gvzr8dlrdlm1sq4b8kwhqkcsd1wf43m413")))

