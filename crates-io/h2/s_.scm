(define-module (crates-io h2 s_) #:use-module (crates-io))

(define-public crate-h2s_core-0.1 (crate (name "h2s_core") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1jnx4wl5i3p3gdxxdxf3wjyi6nncf7axdkxwnb49j97lz4jg22b5") (rust-version "1.65")))

(define-public crate-h2s_core-0.1 (crate (name "h2s_core") (vers "0.1.1") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1x5k3f7ad1136d15qds8m0klxds5rd44f5d5jgrz6dfqflffdpj2") (rust-version "1.65")))

(define-public crate-h2s_core-0.1 (crate (name "h2s_core") (vers "0.1.2") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1pz5vm7fyxhkwqivh6a74rf2rxkwpxxj4kjyphxrma3m198vn7kz") (rust-version "1.65")))

(define-public crate-h2s_core-0.1 (crate (name "h2s_core") (vers "0.1.3") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1kzglk36ap43rs096cq17nq9y1aqcmqqhs12j2rqjm1r0589fczr") (rust-version "1.65")))

(define-public crate-h2s_core-0.1 (crate (name "h2s_core") (vers "0.1.4") (hash "1npdvpn9n041dhj3ny623y79d6wsigyin2a84kvx83j0xmq2mxmd") (rust-version "1.65")))

(define-public crate-h2s_core-0.1 (crate (name "h2s_core") (vers "0.1.5") (hash "1jqdlm0cc3qnf0rv18cjv12wak7p9lkyrbyycspzrrjc555anpyy") (rust-version "1.65")))

(define-public crate-h2s_core-0.1 (crate (name "h2s_core") (vers "0.1.6") (hash "1xpj87k88iwwsm65jr8d71dh1ygs98p4qygmm4llby95kk8x61k7") (rust-version "1.65")))

(define-public crate-h2s_core-0.17 (crate (name "h2s_core") (vers "0.17.0") (hash "0jznpknhc1fwlgh0hrid0kcpf3r4cn8d63bjm5arpgygrcgdh06z") (rust-version "1.65")))

(define-public crate-h2s_core-0.18 (crate (name "h2s_core") (vers "0.18.0") (hash "0yil8ynqwc7jcwp8axvf0nr0k6q3qr83kw07wcz3p75mdc0k1qrs") (rust-version "1.65")))

(define-public crate-h2s_macro-0.1 (crate (name "h2s_macro") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1fm05zbbxw92w72wfi2r9zdqsx7dkj5yhr0g75fqsy6rkl81vhbk") (rust-version "1.65")))

(define-public crate-h2s_macro-0.1 (crate (name "h2s_macro") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "03xj6h2v2wvy2sxf4hdqy7srmf4a2v3h0j54a36apyd31vwvpjlb") (rust-version "1.65")))

(define-public crate-h2s_macro-0.1 (crate (name "h2s_macro") (vers "0.1.2") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1g0h47kiadppigjx2yrwxjbdp50jwv60i0jg0v0k8prxbn9l2fz1") (rust-version "1.65")))

(define-public crate-h2s_macro-0.1 (crate (name "h2s_macro") (vers "0.1.3") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "=0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "0dha0xnkjarxjfb1s7mwd6j3rhkw4wmzlaprswz4f8adrf06l5zl") (rust-version "1.65")))

(define-public crate-h2s_macro-0.1 (crate (name "h2s_macro") (vers "0.1.4") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "=0.1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "0vq013hc57h36mx2s1hzdfs9b3d9waqa09jrj9xbjhhz9jighz8y") (rust-version "1.65")))

(define-public crate-h2s_macro-0.1 (crate (name "h2s_macro") (vers "0.1.5") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "0x5xc2lc98syiyg31i3n6hvdjnikx6hq299i847s99dq4hlg5xqb") (rust-version "1.65")))

(define-public crate-h2s_macro-0.1 (crate (name "h2s_macro") (vers "0.1.6") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1cnqgbgwij2kvc95zbv7vf0yja9g732as1y5zmdj6xilgck4pkq7") (rust-version "1.65")))

(define-public crate-h2s_macro-0.17 (crate (name "h2s_macro") (vers "0.17.0") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "0c2zgxqxb96fq348m4sc1gz7z0by3ijckqnfmn45mmzmc67hlrsv") (rust-version "1.65")))

(define-public crate-h2s_macro-0.18 (crate (name "h2s_macro") (vers "0.18.0") (deps (list (crate-dep (name "darling") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "h2s_core") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)))) (hash "1pkp8h9649hipshk11kcx2b4y2h273fd2snw40c8idb3ghhqsh0p") (rust-version "1.65")))

