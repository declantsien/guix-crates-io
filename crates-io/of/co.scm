(define-module (crates-io of co) #:use-module (crates-io))

(define-public crate-ofCourse-0.1 (crate (name "ofCourse") (vers "0.1.0") (hash "10zvcg853p39qaxvfmivy4fjcc2cyv8az5cx8fcw28hfsc98av3f")))

(define-public crate-ofCourse-1 (crate (name "ofCourse") (vers "1.0.0") (hash "0cr9bypqis0jy4f48v7pazgxz5qnq21h1nh5inckpa4jbvbca5fq")))

(define-public crate-ofCourse-1 (crate (name "ofCourse") (vers "1.1.0") (hash "1gmby4gpn7agssqk76hz3yjgl7r0bzkv8wjg6pnjfgha2kpj0n22")))

(define-public crate-ofCourse-1 (crate (name "ofCourse") (vers "1.1.1") (hash "0qzj0q96j319pnyzmdbyff927aqfxc210gbazijmimr9gx9ifiwf")))

