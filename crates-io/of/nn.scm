(define-module (crates-io of nn) #:use-module (crates-io))

(define-public crate-ofnn-0.1 (crate (name "ofnn") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xns2ixxdjf77z0lgs3fqr97n7ljh4sa58h0cx8abmzfy8is4phw") (features (quote (("floats-f64"))))))

