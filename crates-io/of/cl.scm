(define-module (crates-io of cl) #:use-module (crates-io))

(define-public crate-ofcli-0.1 (crate (name "ofcli") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1g4pixd8anlpjyzhw0ahdcyfc7v56rqa6296ixznwqvdh06ydsf2")))

