(define-module (crates-io of fi) #:use-module (crates-io))

(define-public crate-office-0.8 (crate (name "office") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.1.19") (kind 0)))) (hash "1fdms3cnax21lpg5670pnq0f1byj54nn4gdjssc85ckk9va1dq97")))

(define-public crate-office-0.8 (crate (name "office") (vers "0.8.1") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.2.0") (kind 0)))) (hash "0yfjwk5zi8apa81q3mhfwk6n6ijgbqzfw6mxn81mlqprkb5yim34")))

(define-public crate-office-0.8 (crate (name "office") (vers "0.8.2") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.2.0") (kind 0)))) (hash "1zxf38xpklqs02if5bc8rr42c6hdaic9vqdw6z7vnv042ifzk9ns") (yanked #t)))

(define-public crate-office-converter-0.1 (crate (name "office-converter") (vers "0.1.0") (hash "1s9a9g7x52ixfd0bzp8zvcpdhhbz0vi4rkx59hjhajayqx735p2r")))

(define-public crate-office-crypto-0.1 (crate (name "office-crypto") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.12.3") (default-features #t) (kind 0)) (crate-dep (name "cbc") (req "^0.1.2") (features (quote ("block-padding"))) (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "ecb") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "docx") (req "^1.1.2") (default-features #t) (kind 2)))) (hash "0c68v35s8wsvxw2w0xypg4h4gjj53ygg6g0safz4ys850q868qrh")))

(define-public crate-office-hours-1 (crate (name "office-hours") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "0gmh5a4g3h5z8r5b476ljpxyam6b9dkcba7ih2j3r6vbns57ds3p")))

(define-public crate-office-hours-1 (crate (name "office-hours") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "0ir793975fhmdxzxpgl5g8hj98fxwdnpv1r7y7lsgg38y801vikf")))

(define-public crate-office-jobs-0.0.1 (crate (name "office-jobs") (vers "0.0.1") (hash "11kq8kyh9s250ywrn7yhzps827rmvnf3wid0lcnbnyzlc3nx5144")))

(define-public crate-office-jobs-0.0.2 (crate (name "office-jobs") (vers "0.0.2") (hash "1clq500bxljb2rc610dbw70frnqksp9m0q1fzqnknrciym2mr8l3")))

(define-public crate-office-jobs-0.0.3 (crate (name "office-jobs") (vers "0.0.3") (hash "14b4fb85ppxq6pay2ip539hr443mhd2wmppk28msmym5r1sj9z5m")))

(define-public crate-office-jobs-0.0.4 (crate (name "office-jobs") (vers "0.0.4") (hash "0af1ylxf0wx6bzic1nivkyfiyig26n773544w0g0x0vxi45lv4ra")))

(define-public crate-office-jobs-0.0.5 (crate (name "office-jobs") (vers "0.0.5") (hash "02k9cgn5mn81xzy4w82z5c195d5d2pzra39ka3kk3qkydkl20p5f")))

(define-public crate-office-jobs-0.0.6 (crate (name "office-jobs") (vers "0.0.6") (hash "118b2ykn15hik7a117jaq8x9vjwg2lj4g24gsam6viacydw38l30")))

(define-public crate-office-jobs-0.0.7 (crate (name "office-jobs") (vers "0.0.7") (hash "0va9ngd8kf3177gya8lzja0kac0pzkbbknxabbp1hi78xaxvfnyl")))

(define-public crate-office-jobs-0.0.8 (crate (name "office-jobs") (vers "0.0.8") (hash "1z16yhgxmpja61djhknca9zc499yzrsil4m6rhil6rbwykwfrl00")))

