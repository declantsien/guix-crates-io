(define-module (crates-io of ut) #:use-module (crates-io))

(define-public crate-ofuton-0.1 (crate (name "ofuton") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.10.13") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0bf2jasvbqz79vy77rkwss00qslsijmqxbipjwdk2p40q9b2y4yd")))

