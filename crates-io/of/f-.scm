(define-module (crates-io of f-) #:use-module (crates-io))

(define-public crate-off-rs-0.1 (crate (name "off-rs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "030yawv4ii1j509c9hd24ag43dnz9vyyviyh2xmqa1wzkpn8walh")))

(define-public crate-off-rs-0.1 (crate (name "off-rs") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1c4zjn5d5ymj08w3hk6wwp6d69h5w6mjjr0vfw59hzr0khygckz1")))

(define-public crate-off-rs-0.1 (crate (name "off-rs") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1za1rsl4mc8kl7sl0ghgcv82bi61v62wb9s14rlrinkp6kncp8v0")))

(define-public crate-off-rs-0.1 (crate (name "off-rs") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "01gcqzkhw7vqcncrqscicrwwd205cz907n9dd09qw4m3raw2n8pk")))

(define-public crate-off-rs-0.1 (crate (name "off-rs") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1ff9y9wzsmh4w2091kfmzf4abw0ii1iy5phjysbrrd47r6pbcl1x")))

(define-public crate-off-rs-1 (crate (name "off-rs") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0y5gdv3ssbwl2nvvm6sxl5f4px79rr917ivxzc3gy32kig32kvj9")))

(define-public crate-off-side-0.1 (crate (name "off-side") (vers "0.1.0") (deps (list (crate-dep (name "indent-stack") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("proc-macro" "span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0wf445hclqdj0rkm12jpa9hg1yml2g8sm3972rv470qdz2zw14ky")))

(define-public crate-off-side-0.1 (crate (name "off-side") (vers "0.1.1") (deps (list (crate-dep (name "indent-stack") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("proc-macro" "span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1dnqg4jpqhb2rs80hmzv8alq0kqi4cdmlq82yycp2clrkvcnf8mz")))

