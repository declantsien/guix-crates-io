(define-module (crates-io of cp) #:use-module (crates-io))

(define-public crate-ofcp_data-0.1 (crate (name "ofcp_data") (vers "0.1.0") (hash "1myln3xsj1spisn1gv0zayjkbsy01vbrks1xlvp6lrh0cs70zwjk")))

(define-public crate-ofcp_data-0.2 (crate (name "ofcp_data") (vers "0.2.0") (hash "07aks8dkjjj3ni92l8pr0m6rb0jfziglfp0hdqz0msvl7hhymky8")))

(define-public crate-ofcp_data-0.3 (crate (name "ofcp_data") (vers "0.3.0") (hash "0v1jmd4hf7iniy29wa477g98s04rqxk92sg4zi17k7kckxhqd510")))

(define-public crate-ofcp_data-0.4 (crate (name "ofcp_data") (vers "0.4.0") (hash "0rcjzwx16452iygy2hv20awv5qa85rzf552lhlsjzclfc068ffn1")))

(define-public crate-ofcp_data-0.5 (crate (name "ofcp_data") (vers "0.5.0") (hash "0lcsn0a5jplb8lmidqa3d44wjyjvvz00y31lqrqlxp7hm3z8rnhl")))

(define-public crate-ofcp_data-0.6 (crate (name "ofcp_data") (vers "0.6.0") (hash "178fydvp5csn0xda4h7nila4zspyafsn25dx0ar35fd9511v20kk")))

(define-public crate-ofcp_data-0.7 (crate (name "ofcp_data") (vers "0.7.0") (hash "1wvgr5dqd2adsy40ihx0sv1dfn6hxlqvrbd3hkfpk4nw8nsxg264")))

(define-public crate-ofcp_data-0.8 (crate (name "ofcp_data") (vers "0.8.0") (hash "0218vg69j02gwdapvs22mf6fdgix35n64121468qnv8ibsy2k3ay")))

(define-public crate-ofcp_data-0.9 (crate (name "ofcp_data") (vers "0.9.0") (hash "1xnv7ncgahhmgn59bwdyx3jif4rdnln1vgv1g9lcjki5n2mfv7d8")))

(define-public crate-ofcp_data-0.11 (crate (name "ofcp_data") (vers "0.11.0") (hash "0zya9nczn2q643h0bb7w805zway2ibikjdg32ag2h50xypz0ip3k")))

(define-public crate-ofcp_data-0.12 (crate (name "ofcp_data") (vers "0.12.0") (hash "0ap8qqpj6f2c6vvsqiqlqp2i18nm95yjqcff31q24vy4m2vlh8vi")))

(define-public crate-ofcp_data-0.12 (crate (name "ofcp_data") (vers "0.12.1") (hash "0xpzmh9ldvlz7hjfgmcvx8l73dzgirw0718ng9prc44yhrfgpril")))

(define-public crate-ofcp_data-0.13 (crate (name "ofcp_data") (vers "0.13.0") (hash "1walrsgyiajrwqsbgs5ymckmlvva1vrpi7r9ijv6f1b6l3ksld4r") (yanked #t)))

(define-public crate-ofcp_data-0.14 (crate (name "ofcp_data") (vers "0.14.0") (hash "0g48iya6qagcnp3fg16y8r35k6bm8v77zill347bp9nlscgbj5fn") (yanked #t)))

(define-public crate-ofcp_data-0.13 (crate (name "ofcp_data") (vers "0.13.1") (hash "0rk781d9y5ng1gi2p2jqx27ppcs04l7xhinq32g9k6k78545j8i6") (yanked #t)))

(define-public crate-ofcp_data-0.13 (crate (name "ofcp_data") (vers "0.13.2") (hash "1vc028kw9x63jhiwyvdly4l7ldjph07h4wchwqf2m74zplcfb4sn") (yanked #t)))

(define-public crate-ofcp_data-0.15 (crate (name "ofcp_data") (vers "0.15.0") (hash "0fz3rzypqi78s9xb10jqbvnpazs0v6h1nx72161z0g11q5sfw2d4") (yanked #t)))

(define-public crate-ofcp_data-0.15 (crate (name "ofcp_data") (vers "0.15.1") (hash "19vg8b27gbzvvzg4ami7icbiihwpwhn3gpdv5dn1si2klccv9jlv") (yanked #t)))

(define-public crate-ofcp_data-0.15 (crate (name "ofcp_data") (vers "0.15.2") (hash "19ny9fx2chc6xxl2h7wnam78w2awjjchnix5r0ihcgdqp9lx4h1x") (yanked #t)))

(define-public crate-ofcp_data-0.16 (crate (name "ofcp_data") (vers "0.16.0") (hash "054hyqqdy273jq2bbxm5dbs7mic26k0i01v4pxcxjns042m8w8qs") (yanked #t)))

(define-public crate-ofcp_data-0.17 (crate (name "ofcp_data") (vers "0.17.0") (hash "1rbd2hii4fsrfkav2kdgwkvsd94ppmmrnrawfdg646xfdcjsnyn9") (yanked #t)))

(define-public crate-ofcp_data-0.17 (crate (name "ofcp_data") (vers "0.17.1") (hash "1bwf974d8hx9ps5vyd3rn5fcmb5a0fcbr4qxw308mcqpdk6iz1k9") (yanked #t)))

(define-public crate-ofcp_data-0.17 (crate (name "ofcp_data") (vers "0.17.2") (hash "0pxxxi0lc6zilqpps47ym10242al6j3jdixafhl7m4vfm8sgvvxg") (yanked #t)))

(define-public crate-ofcp_data-0.17 (crate (name "ofcp_data") (vers "0.17.3") (hash "1xwz10zw30zw1lwrcvm05q17y9m990mpfd0xn9widjirn73gc020") (yanked #t)))

(define-public crate-ofcp_data-0.18 (crate (name "ofcp_data") (vers "0.18.0") (hash "0qv9s38ybsfawm6cvfvlrnqicfldwqllhfn02amhl1yim4pq8n0d") (yanked #t)))

(define-public crate-ofcp_data-0.17 (crate (name "ofcp_data") (vers "0.17.4") (hash "1cc3683w4f7qbmhjvysprrrgr20f77hkk6l19ml8jrccc2zvxpws")))

(define-public crate-ofcp_data-0.19 (crate (name "ofcp_data") (vers "0.19.0") (hash "1lb55iq7m7z6c5457sg7vda8np2zdgdzar4kpjxvx4v6b06s1gn5") (yanked #t)))

(define-public crate-ofcp_data-0.20 (crate (name "ofcp_data") (vers "0.20.0") (hash "0aav68aciwhfxp0nh3hp24wpfajxgwf5n2b2n9l48asn92dg6h67") (yanked #t)))

(define-public crate-ofcp_data-0.20 (crate (name "ofcp_data") (vers "0.20.1") (hash "0hp2bpqx6mds4xjna7s0mdnjqdnxjqbc26hzf71xmf6s86581p9c") (yanked #t)))

(define-public crate-ofcp_data-0.20 (crate (name "ofcp_data") (vers "0.20.2") (hash "0yv5vc6lff7zy7fhr9cmqy5z1f7r80b794rvdqjgl913z17sia6b") (yanked #t)))

(define-public crate-ofcp_data-0.20 (crate (name "ofcp_data") (vers "0.20.3") (hash "00rmkgpw089iydi9hx81wqzpd3n7ax4xgpby0wg63xglllipx3jl")))

(define-public crate-ofcp_data-0.21 (crate (name "ofcp_data") (vers "0.21.0") (hash "0hn1nz381i8ijjvlk58397sbpdlhinaq3j90acvkjc3j8xy2ibm2")))

(define-public crate-ofcp_data-0.21 (crate (name "ofcp_data") (vers "0.21.1") (hash "13xfbgp8jha4nn7h8dzz12psfhgicg214jvj2flik6hijwd9bhm3")))

(define-public crate-ofcp_data-0.21 (crate (name "ofcp_data") (vers "0.21.2") (hash "00ndzl7vpa5037x1vdcj0kikcqd4j0mg26i334474dsf4lp346z9")))

