(define-module (crates-io of i-) #:use-module (crates-io))

(define-public crate-ofi-pass-0.2 (crate (name "ofi-pass") (vers "0.2.1") (deps (list (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mocktopus") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0gl2j0an9iav72i7hjzhzwcdgy5k9m8llcg4jq2hjfp3k68sc13d")))

(define-public crate-ofi-pass-0.3 (crate (name "ofi-pass") (vers "0.3.0") (deps (list (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mocktopus") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "058da29lm9np031ykj1xpvgv9dmz4rfigxxawhvsvrsc3crv5lix")))

(define-public crate-ofi-pass-0.3 (crate (name "ofi-pass") (vers "0.3.1") (deps (list (crate-dep (name "directories") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mocktopus") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0wmkb7wvdhmfp04j8jsg30bhzvcnr4kkr10x8vygji5hjnnrxkdp")))

(define-public crate-ofi-pass-0.4 (crate (name "ofi-pass") (vers "0.4.0") (deps (list (crate-dep (name "directories") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mocktopus") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "stderrlog") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "15bbrb3yzyjnf36y6yfaslcv1fycqycc5hqmrbw65ihyw08qb406")))

