(define-module (crates-io of f6) #:use-module (crates-io))

(define-public crate-off64-0.0.1 (crate (name "off64") (vers "0.0.1") (hash "0fsxz2kb7kdz2s2h02dp1id22d7115qcqzwgfiz3hx991x8hwkvq")))

(define-public crate-off64-0.0.2 (crate (name "off64") (vers "0.0.2") (hash "14rfgi649c87mq03m4p95gz9c10a04scn6mn2s2zpkgl0sra05c6")))

(define-public crate-off64-0.1 (crate (name "off64") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1zlkxxq4l15z1z4w39xc69z0gdmw4cxi8c5ig1c0dim69r69h42r") (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.2 (crate (name "off64") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "12rh53l6w4p8h1v8724fil9j0gpfpr7z7sl9bjhyq0b274j25b6v") (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.3 (crate (name "off64") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "07a06rqnqx5grqc56mm1jdah5n45zd6rq3y824bd04rkakg0dw9j") (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.3 (crate (name "off64") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0zpc3kcm7q2mnk9pqwpzgp9k5vrppbbcmyc7b5xjdj9lvfppk3kz") (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.4 (crate (name "off64") (vers "0.4.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1jivibjgr02qnfdvpdqk4awz84cvl7hrsy8wdbr27nlvydia4pvm") (features (quote (("default" "chrono")))) (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.5 (crate (name "off64") (vers "0.5.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0vpqx2gdcv9p84y63sgdvx438gx6pqncmc73vj3h2gyll74bswqp") (features (quote (("default" "chrono")))) (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.5 (crate (name "off64") (vers "0.5.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1bfx93iy4i0vaxl7z2bjvwn6nyvz9by3j04w0qffj8kym5y2ajff") (features (quote (("default" "chrono")))) (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-off64-0.6 (crate (name "off64") (vers "0.6.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "11xv7qb58c9a7i5pr3x9w1lgxgr89snwcswdawyy5ln86h8b0d23") (features (quote (("default" "chrono")))) (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

