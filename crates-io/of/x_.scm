(define-module (crates-io of x_) #:use-module (crates-io))

(define-public crate-ofx_sys-0.1 (crate (name "ofx_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "cgmath") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19ljqrk108xdcb54plc1dxlxis68l71c2pprpf64w5h17xply8n4") (yanked #t)))

(define-public crate-ofx_sys-0.1 (crate (name "ofx_sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "cgmath") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gywbajxnagkc8l26g6bqjqzrljdv9306lkf93lg4pwgv1c2klih")))

(define-public crate-ofx_sys-0.2 (crate (name "ofx_sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "cgmath") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xf5naigq556vbflv3ndxh2a5i6dk4mrvs0clr144y08wb589vqc")))

