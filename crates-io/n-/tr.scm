(define-module (crates-io n- tr) #:use-module (crates-io))

(define-public crate-n-tree-0.0.0 (crate (name "n-tree") (vers "0.0.0") (hash "1hshfbwym3s83rxxfp781yb8aayffn6qad3ks0ny30ypz89dsiv6")))

(define-public crate-n-tree-0.0.1 (crate (name "n-tree") (vers "0.0.1") (hash "1xv9iv12sdyk26s0hmk99p85ly3kff9l92jd02cli9ndpxlfmrgd")))

