(define-module (crates-io n- ro) #:use-module (crates-io))

(define-public crate-n-roman-0.1 (crate (name "n-roman") (vers "0.1.0") (hash "1v81ivfi8mdc6rz58pcc97q1n5gwf2c63f0p4ggwydbd4acz0pxb") (yanked #t)))

(define-public crate-n-roman-0.1 (crate (name "n-roman") (vers "0.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.34") (default-features #t) (kind 0)))) (hash "09gx8h89jnybcf8qa5ayaqcl6zsjjb5pkcifam7xqzysmn1gdaz2") (yanked #t)))

(define-public crate-n-roman-0.1 (crate (name "n-roman") (vers "0.1.2") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.34") (default-features #t) (kind 0)))) (hash "1z737qhn6bl6wqhvp6gbr093afh936n3nyq5isjmaahi5p2nryh3")))

