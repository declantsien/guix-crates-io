(define-module (crates-io gn un) #:use-module (crates-io))

(define-public crate-gnunet-0.0.1 (crate (name "gnunet") (vers "0.0.1") (hash "1aqh1z2ic3a5bnmgsdx6kpwdl03588qdy1ibnjx9yp8p1wi4mxfl")))

(define-public crate-gnunet-0.0.2 (crate (name "gnunet") (vers "0.0.2") (hash "0lszv4brbhl79q7ghcnk1ilkdnq97xilchv3i2cl4svg4wwnysq5")))

(define-public crate-gnunet-0.0.3 (crate (name "gnunet") (vers "0.0.3") (hash "1dc548q13w5bbjkssd2c90bk1xhbir89ksx7rpjsnjbisd35k3nn")))

(define-public crate-gnunet-0.0.4 (crate (name "gnunet") (vers "0.0.4") (hash "1g0nimnp6zvpn5awarzg0ylq44alrpfxd4k0fs0sjncma5riys7d")))

(define-public crate-gnunet-0.0.5 (crate (name "gnunet") (vers "0.0.5") (hash "04wcqcbj2gpnwb21mil8crh79aq9f13gs3g6avrxz0qhcjkb14rz")))

(define-public crate-gnunet-0.0.6 (crate (name "gnunet") (vers "0.0.6") (hash "1jjsnldygghrv5vpfncg6mqw45h4gj7v5wx7ip56mg0jvprw9pkd")))

(define-public crate-gnunet-0.0.7 (crate (name "gnunet") (vers "0.0.7") (hash "17sicq3ykz2mrpdlsfjrh4zw4vp9230bzxnn5fpcxh9dlcwaikbl")))

(define-public crate-gnunet-0.0.8 (crate (name "gnunet") (vers "0.0.8") (hash "04kcxzzi5hxggy3f5pzzfm3w6wb16kf3p2csnkwklf0wclzvlbgy")))

(define-public crate-gnunet-0.0.9 (crate (name "gnunet") (vers "0.0.9") (hash "1jw92p2j17a033sb0kp7hswfj8mpigbyvmrzdrblrp6mm8qqrcls")))

(define-public crate-gnunet-0.0.10 (crate (name "gnunet") (vers "0.0.10") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "123nzcf57cfk2sc72flk5dd49lipc0aqikqyfvl6bgdrddf3072j")))

(define-public crate-gnunet-0.0.11 (crate (name "gnunet") (vers "0.0.11") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "error_def") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "*") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1bfixqf4fiz7rm9q7mq60spg0bfq16x0h0l89788xmarc5af6wdw")))

(define-public crate-gnunet-0.0.12 (crate (name "gnunet") (vers "0.0.12") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "error_def") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "*") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1adw967m6ldffa3qs3djshfcan1lmhrphcfdqbsa07g91bgvb2k2")))

(define-public crate-gnunet-0.0.13 (crate (name "gnunet") (vers "0.0.13") (deps (list (crate-dep (name "byteorder") (req ">= 0.3.10") (default-features #t) (kind 0)) (crate-dep (name "error_def") (req ">= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req ">= 0.1.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">= 0.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req ">= 0.2.31") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req ">= 0.3.0") (default-features #t) (kind 0)))) (hash "0wbshb7785bfbpf68frp4x5jb6b9pvfyqv3lmr9z4qyfbrm3cibh")))

(define-public crate-gnunet-0.0.14 (crate (name "gnunet") (vers "0.0.14") (deps (list (crate-dep (name "byteorder") (req ">= 0.3.10") (default-features #t) (kind 0)) (crate-dep (name "error_def") (req ">= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req ">= 0.1.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">= 0.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req ">= 0.2.31") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req ">= 0.3.0") (default-features #t) (kind 0)))) (hash "06xgbc1ymj7g3q05vq3laygrcmmz9i32i6czmfv1mlwxi0i2fz68")))

(define-public crate-gnunet-0.0.15 (crate (name "gnunet") (vers "0.0.15") (deps (list (crate-dep (name "byteorder") (req ">= 0.3.10") (default-features #t) (kind 0)) (crate-dep (name "error_def") (req ">= 0.3.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req ">= 0.1.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">= 0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">= 0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req ">= 0.1.8") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req ">= 0.2.31") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req ">= 0.3.0") (default-features #t) (kind 0)))) (hash "0bvq7xiqwaq50hfcysqmxsf9r27cx3x77dqzgig2xsiwb6k6paxm")))

(define-public crate-gnunet-rs-0.0.0 (crate (name "gnunet-rs") (vers "0.0.0") (deps (list (crate-dep (name "gnunet-sys") (req "^0.0") (default-features #t) (kind 0)))) (hash "0cdqz610sk3fmslm2hn6irik8knabq9nwia7wbl02lvd5b9ysi9r") (features (quote (("peerstore" "gnunet-sys/peerstore") ("fs" "gnunet-sys/fs"))))))

(define-public crate-gnunet-rs-0.0.1 (crate (name "gnunet-rs") (vers "0.0.1") (deps (list (crate-dep (name "async-std") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "gnunet-sys") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "19674bzhdbj0wpj2l44454m772m87mqmakmlxi8y0s65ngb2gdr4") (features (quote (("peerstore" "gnunet-sys/peerstore") ("fs" "gnunet-sys/fs") ("cadet" "gnunet-sys/cadet"))))))

(define-public crate-gnunet-rs-0.0.2 (crate (name "gnunet-rs") (vers "0.0.2") (deps (list (crate-dep (name "async-std") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "gnunet-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ksgmx6q05fql611ygffyvhfxchjj609jkg0yn3ymg52ii2bafjk") (features (quote (("peerstore" "gnunet-sys/peerstore") ("fs" "gnunet-sys/fs") ("cadet" "gnunet-sys/cadet"))))))

(define-public crate-gnunet-sys-0.0.0 (crate (name "gnunet-sys") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1k2yz6gl46rmc6ary8767mb52yjv7scki3r7zibxz3s3hwa4gjyc") (features (quote (("peerstore") ("fs") ("default" "core") ("core"))))))

(define-public crate-gnunet-sys-0.0.1 (crate (name "gnunet-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1cfg9388wbpzisv1spm4hcyvglpkf7mbl6r3w80wv006hldszzf1") (features (quote (("peerstore") ("fs") ("default" "core") ("core") ("cadet"))))))

(define-public crate-gnunet-sys-0.0.2 (crate (name "gnunet-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0fpgbqsizy2rnla72cc7c1y7k2hh53kgvkwy1hy1apz56g70zrs1") (features (quote (("peerstore") ("fs") ("default" "core") ("core") ("cadet"))))))

(define-public crate-gnunet-sys-0.0.3 (crate (name "gnunet-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "013qi28c123vpabiwimy3fsj7akl3zspaci4xgw8x0psrsq3zs07") (features (quote (("peerstore") ("fs") ("default" "core") ("core") ("cadet"))))))

(define-public crate-gnunet-sys-0.0.4 (crate (name "gnunet-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1ir49kffswaxshfwzbzgcdaxh93bz2rm51y17qa5fcvv0rhqjgpg") (features (quote (("peerstore") ("fs") ("default" "core") ("core") ("cadet"))))))

