(define-module (crates-io gn al) #:use-module (crates-io))

(define-public crate-gnal_tsur-0.1 (crate (name "gnal_tsur") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "18nz0335b80l7imcmk00n1cd1lrlkbvddqaymggarg68g2ic1dny")))

