(define-module (crates-io gn ul) #:use-module (crates-io))

(define-public crate-gnulightning-0.1 (crate (name "gnulightning") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)))) (hash "19cag8ii21z1np8xgpzvjmj9axc9nf7szf21ys7n788v12jffvia") (yanked #t)))

(define-public crate-gnulightning-sys-2 (crate (name "gnulightning-sys") (vers "2.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1pc32zsabvfjam3f981hdywclyj8biwzjj68qlppc495gx8r17fh")))

