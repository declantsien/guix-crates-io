(define-module (crates-io gn ut) #:use-module (crates-io))

(define-public crate-gnutella-0.1 (crate (name "gnutella") (vers "0.1.0") (hash "1px9wsbh756jjlkdaiqpx246fdxy1hiv88fz0i29bg3l4jpiab6f")))

(define-public crate-gnutella-1 (crate (name "gnutella") (vers "1.0.0") (hash "1k27xbk8g411sa2s1931gk07866gxnqfzwdvpkf8s5xg4agc5v47")))

(define-public crate-gnutls-0.1 (crate (name "gnutls") (vers "0.1.0") (deps (list (crate-dep (name "gnutls-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1n400c5mmz1q7jqlnz1avdby907zq8w2j0vxh8gbxrvfjjp75884")))

(define-public crate-gnutls-0.1 (crate (name "gnutls") (vers "0.1.1") (deps (list (crate-dep (name "gnutls-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "00fka13ds67ks6vg3ayk28ckrjan7kl3jymyhmx6kyfzvsf3ilz3")))

(define-public crate-gnutls-0.1 (crate (name "gnutls") (vers "0.1.2") (deps (list (crate-dep (name "gnutls-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1paqf6n11qdzigmadpiqqxawmvkrp7wpdkiqhl4imh8xd1vh4qm1")))

(define-public crate-gnutls-0.1 (crate (name "gnutls") (vers "0.1.3") (deps (list (crate-dep (name "gnutls-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "19vkqzanb8y5dqqz94f7i7d0xrqxsd6aprf145kg99jyxdqpl4rv")))

(define-public crate-gnutls-sys-0.1 (crate (name "gnutls-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "0yjwxqw8bgsjmyjjdabyvkkqjfrxdzbrridilvgh07n5qk7vbcqb")))

(define-public crate-gnutls-sys-0.1 (crate (name "gnutls-sys") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "127ghvc9dz1fx9i97jxh0jqms9rximqrj54dcsp8lxcnzf6mlx3k")))

(define-public crate-gnutls-sys-0.1 (crate (name "gnutls-sys") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "1qz94534l6x78xd8gh8ri2cl2l40g53avj0irkpla8qayygxa1rm")))

