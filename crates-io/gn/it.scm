(define-module (crates-io gn it) #:use-module (crates-io))

(define-public crate-gnit-0.1 (crate (name "gnit") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "read_input") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "0niy73zwc5vkfjd8l23pmw298cvxaf40s6p4702fg63n554l56c4") (yanked #t)))

