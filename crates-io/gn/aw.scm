(define-module (crates-io gn aw) #:use-module (crates-io))

(define-public crate-gnaw-0.0.1 (crate (name "gnaw") (vers "0.0.1") (hash "02fy7v50d6czyjw10f98nh7i4ar9gn2dm3mgqzqnjrh1xpx8y0hf")))

(define-public crate-gnaw-0.0.2 (crate (name "gnaw") (vers "0.0.2") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "12qd550vxg2ic77wv0k9gyky3w3f7fda1m13vj5hacfslpy8d5x7")))

