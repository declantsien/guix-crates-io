(define-module (crates-io gn or) #:use-module (crates-io))

(define-public crate-gnore-0.1 (crate (name "gnore") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termprompt") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05bzhr79ga755lhwmzshfgxhh6q73yhznv1n77yrjrpb1qj30v4a")))

