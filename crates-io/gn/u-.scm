(define-module (crates-io gn u-) #:use-module (crates-io))

(define-public crate-gnu-echo-rs-0.1 (crate (name "gnu-echo-rs") (vers "0.1.0") (deps (list (crate-dep (name "ascii_converter") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "10dm0sxybjy5yq7mmmpbkxkvqrxgmq1v9cn2wdw09fdcq7xp2zz8")))

(define-public crate-gnu-libjit-0.1 (crate (name "gnu-libjit") (vers "0.1.1") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mc1g7bb5wdwhyqc6717whar3brn7wccrxqij2ml4yjqny8a3l19")))

(define-public crate-gnu-libjit-0.2 (crate (name "gnu-libjit") (vers "0.2.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zkgl2h1ygpkw77lpq221zdbvkzb7yh524lv27v9k8hirn23v5k6")))

(define-public crate-gnu-libjit-0.3 (crate (name "gnu-libjit") (vers "0.3.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rj5jyjcd53rgzvb7s1zb4gizni6qx4f3r60z6xcxbpjbh9c131i")))

(define-public crate-gnu-libjit-0.4 (crate (name "gnu-libjit") (vers "0.4.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "04zzp0yxmyz5b37p8zswi0v0manyp371vh7mwvyf6mf7nnah6srd")))

(define-public crate-gnu-libjit-0.5 (crate (name "gnu-libjit") (vers "0.5.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fnjxbkshwcm716jf2yiv13f6hpgikb9112fgpxjqfrwdwkn5c6m")))

(define-public crate-gnu-libjit-0.6 (crate (name "gnu-libjit") (vers "0.6.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rsr7dx004zr4iywir046bm0vlprkp8ppgvi6ankg4rvsj9vw00a")))

(define-public crate-gnu-libjit-0.7 (crate (name "gnu-libjit") (vers "0.7.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wv8asv6kaphjvw09vsy3kdbg7shw7qa806chfaasp5m12sdm6sw")))

(define-public crate-gnu-libjit-0.8 (crate (name "gnu-libjit") (vers "0.8.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lza20bmlvz1f551jdklbwwcmpw49b4vcxam3gg47gk8nvazj2pj")))

(define-public crate-gnu-libjit-0.9 (crate (name "gnu-libjit") (vers "0.9.0") (deps (list (crate-dep (name "gnu-libjit-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y0y8lqkiw6hjrgahr20vcffs7983nxjn056is7ccpsznmvnaadh")))

(define-public crate-gnu-libjit-sys-0.0.0 (crate (name "gnu-libjit-sys") (vers "0.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0s7wri9sc3bksz0z3bhsql1gb0zcjij4sgwylldxl0yffd1yhyaz")))

(define-public crate-gnu-libjit-sys-0.0.1 (crate (name "gnu-libjit-sys") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "045czw6bsr1zlp8m87ajwxxkyf6110hxinc48k0pk4zr2a9gpk3l")))

(define-public crate-gnu-libjit-sys-0.0.2 (crate (name "gnu-libjit-sys") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1hpmf71lwg9ifzvcns2ciwf6labjkw01vhsgmpd0gw7qm4m7qsa2")))

(define-public crate-gnu-libjit-sys-0.0.3 (crate (name "gnu-libjit-sys") (vers "0.0.3") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1hnnbs6897n3677y83simb05ndsy929dpbc2f7gcq8q3aa8cx7h6")))

(define-public crate-gnu-libjit-sys-0.0.5 (crate (name "gnu-libjit-sys") (vers "0.0.5") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0nvvi4skzfcpc92ba7s9xaapws4a4kngjqvfvrgzxi8530sqzip6")))

(define-public crate-gnu-libjit-sys-0.0.6 (crate (name "gnu-libjit-sys") (vers "0.0.6") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "04i2pqx558qppyb84b529c2dgb8a09dlqb7csa6ljyk8gg7zg9di")))

(define-public crate-gnu-libjit-sys-0.0.7 (crate (name "gnu-libjit-sys") (vers "0.0.7") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "101ilvx6p7ml296by4y8wdp9d51kiax6wdfzaql7yzbfa56s309x")))

(define-public crate-gnu-libjit-sys-0.0.8 (crate (name "gnu-libjit-sys") (vers "0.0.8") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0svsdh8r9d18lvx9msl6z6ik3bajriizpf72mg5b3843cyrxbcqc")))

(define-public crate-gnu-ln-0.1 (crate (name "gnu-ln") (vers "0.1.0") (hash "1d8gfgg1fj9dfk9l37xv84v7d35psv7l8q7az078cg6zbjkd900i")))

(define-public crate-gnu-ln-0.2 (crate (name "gnu-ln") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "00j3xgnkknf8a6d78n82b783314mgxr3ja9pxhv500agcimjjq2n")))

(define-public crate-gnu-readline-sys-0.1 (crate (name "gnu-readline-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "106j4jzbxxwsvwz2pzd3b4isnc7fdba8w15rsgrlqp31vbrbfxs4") (links "readline") (rust-version "1.56")))

