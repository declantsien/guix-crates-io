(define-module (crates-io gn us) #:use-module (crates-io))

(define-public crate-gnusay-0.1 (crate (name "gnusay") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1rfj8xn948r6q89jakhzraz7mmgz6mm9lwplbffci42br09cf5a8")))

(define-public crate-gnusay-0.1 (crate (name "gnusay") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)))) (hash "0crdjfy4q396sshxwdg96vvm4vl0aq9wrh7kjsz54fn8vi88wmhi")))

(define-public crate-gnusay-0.2 (crate (name "gnusay") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)))) (hash "119xq646r3k89xyf4xiyliqsd6mmragy4vayvn6rxjcy8sigij8m")))

(define-public crate-gnusay-0.3 (crate (name "gnusay") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1nfyg06qppkgba99apn2klsxz6g5hxrvp09ng3kc3m65dqjv7lgz")))

