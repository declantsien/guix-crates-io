(define-module (crates-io gn up) #:use-module (crates-io))

(define-public crate-gnupg-0.1 (crate (name "gnupg") (vers "0.1.0") (hash "1rb7adjwcg2drivkw8acwbad8vy6908pxykc5f5mmihlhjmvyjly")))

(define-public crate-gnupg-0.2 (crate (name "gnupg") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1nvbq1pvg9fbkfy92hsfij9b0n1szh3j6rkdv28mzv1gw5a4in4y") (yanked #t)))

(define-public crate-gnupg-rs-0.1 (crate (name "gnupg-rs") (vers "0.1.0") (hash "1353i68i5rrvkjqb0vqiljsylpj7c78mwxncl717bf17vr3jl9fq")))

(define-public crate-gnuplot-0.0.0 (crate (name "gnuplot") (vers "0.0.0") (hash "1k17yysmch6jrs0lkqpsm6xa1wzysahvb9zgg8yga17yabkcvh7f")))

(define-public crate-gnuplot-0.0.1 (crate (name "gnuplot") (vers "0.0.1") (hash "0wk74rdscsn99q2rmb52hi1qh0nbf6j5938gz0b2sdiv0scrq9rr")))

(define-public crate-gnuplot-0.0.2 (crate (name "gnuplot") (vers "0.0.2") (hash "10q25r2p8nhihm0qan2mr3n1a5vqbg6anqhczrdikcbc6c55a1mc")))

(define-public crate-gnuplot-0.0.3 (crate (name "gnuplot") (vers "0.0.3") (hash "1mk87k2b405mafmcsffa014zhafcja6ri4i8l68cwvlyr8rvsy9w")))

(define-public crate-gnuplot-0.0.4 (crate (name "gnuplot") (vers "0.0.4") (hash "0rf0fv521zf5jrjbcs6syg4gxy311blpiz73lysm7hgm71qxlg6c")))

(define-public crate-gnuplot-0.0.5 (crate (name "gnuplot") (vers "0.0.5") (hash "1mwrgaaj69p7ih7n26n1xf82kd4836p0qkc4d2rjzkdq9h2lazck")))

(define-public crate-gnuplot-0.0.6 (crate (name "gnuplot") (vers "0.0.6") (hash "017kpv6ij0y1n80x7s57zm6psy399rs2f4b2wwlydgzkb3h72gba")))

(define-public crate-gnuplot-0.0.7 (crate (name "gnuplot") (vers "0.0.7") (hash "1r5ym59kgrp8qdwa42jdv6zj9103b07066c9w5nipdf44q4rrm3c")))

(define-public crate-gnuplot-0.0.8 (crate (name "gnuplot") (vers "0.0.8") (hash "0pbil4qpad754xl2k5dykkfgx29jn8wi5rigcds7r0pvmy7zkngj")))

(define-public crate-gnuplot-0.0.9 (crate (name "gnuplot") (vers "0.0.9") (hash "1x6xgbdkb1pcqmv2kvvn32a5fcp8whr33xillpzk1g983j8jpjdy")))

(define-public crate-gnuplot-0.0.10 (crate (name "gnuplot") (vers "0.0.10") (hash "1z6y3fk71lr5p844mqpadh70qj3lmbahfs6hc59nzvn90rw7m9jn")))

(define-public crate-gnuplot-0.0.11 (crate (name "gnuplot") (vers "0.0.11") (hash "1g0kvwvk6j2ldmhpywlc4l22xcwcxkbajdiha3ns11fd87mw7b9a")))

(define-public crate-gnuplot-0.0.12 (crate (name "gnuplot") (vers "0.0.12") (hash "114qk2qw8n5fm3yy7vkx8dhz2wira32kqr16mn6cm20qzaw2nmcp")))

(define-public crate-gnuplot-0.0.14 (crate (name "gnuplot") (vers "0.0.14") (hash "1jvfcq84anqmvdvyxfciwg0yi7d0880ikhahadjs273fj2iimyy4")))

(define-public crate-gnuplot-0.0.15 (crate (name "gnuplot") (vers "0.0.15") (hash "1nr72xszn70c9nkq696haj3b6jxsvdkzkq8rkyk3z4vh3kykq5fv")))

(define-public crate-gnuplot-0.0.16 (crate (name "gnuplot") (vers "0.0.16") (hash "00xrcfhbyrcabnhc6y02h8fd3p1nmlybz5ina25aiiifh4dmmccb")))

(define-public crate-gnuplot-0.0.17 (crate (name "gnuplot") (vers "0.0.17") (hash "0iz8iv9rm9qsm7295y7jdv39z9w0x7j2sxnm9j490cf63abh3386")))

(define-public crate-gnuplot-0.0.18 (crate (name "gnuplot") (vers "0.0.18") (deps (list (crate-dep (name "getopts") (req "= 0.2.14") (default-features #t) (kind 2)))) (hash "07l592md4x91nc7wf10lw6q5rj5zc3jr86ql2snsav0pxbwlf10x")))

(define-public crate-gnuplot-0.0.19 (crate (name "gnuplot") (vers "0.0.19") (deps (list (crate-dep (name "getopts") (req "= 0.2.14") (default-features #t) (kind 2)))) (hash "02rcjlk2smd070aglszspvaih3gmg5skx43b3zshbcwy6n1hzpi8")))

(define-public crate-gnuplot-0.0.20 (crate (name "gnuplot") (vers "0.0.20") (deps (list (crate-dep (name "getopts") (req "= 0.2.14") (default-features #t) (kind 2)))) (hash "0p53h8mrygib3macjdc5lc32vlnjzsmc9x1kk4ycdbqh4285iiag")))

(define-public crate-gnuplot-0.0.21 (crate (name "gnuplot") (vers "0.0.21") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "0855cqg36qlrnl1nmbzfciwxjlscqk3hscxpycsziv9r68wz0jsq")))

(define-public crate-gnuplot-0.0.22 (crate (name "gnuplot") (vers "0.0.22") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "15zgpdz7q95xfsmg5kwcb2grvzzqgz1siyav1injxiilkwab0z63")))

(define-public crate-gnuplot-0.0.23 (crate (name "gnuplot") (vers "0.0.23") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "0j2mzlfaxcddkm8ca3r3mxzq771lcpyksx933i6v5z8l5311ng0i")))

(define-public crate-gnuplot-0.0.24 (crate (name "gnuplot") (vers "0.0.24") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "0677y4hcsirc4xca7q17ciry3y7m9bcs12fg15l043j037idpyrc")))

(define-public crate-gnuplot-0.0.25 (crate (name "gnuplot") (vers "0.0.25") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "0qfm16k93gzvy3j6mf47j9xrnbrk4j26sr280cph03dqklbp7wxx")))

(define-public crate-gnuplot-0.0.26 (crate (name "gnuplot") (vers "0.0.26") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "04yra4330k5yczpszdakzxag395p1fdxfmz4radk920n0lcxgkll")))

(define-public crate-gnuplot-0.0.27 (crate (name "gnuplot") (vers "0.0.27") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "13gy2icmwwn8c556xai34q4ls3786n3029fp0blrbw2khjzg7lrs")))

(define-public crate-gnuplot-0.0.28 (crate (name "gnuplot") (vers "0.0.28") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1ivfn6qa14am24jjzqrdl5gykakx245s4ac7kw0c3cncr7cp06fv")))

(define-public crate-gnuplot-0.0.29 (crate (name "gnuplot") (vers "0.0.29") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1vqa8k6d2k57dpn6b67x2mllabdmbjal0ipql2l0kzw9a46v83d6")))

(define-public crate-gnuplot-0.0.30 (crate (name "gnuplot") (vers "0.0.30") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "0s6bj7gpp8gr1ify58zfildvv4vqwgdjm348gq8c4r7faw0r0lk3")))

(define-public crate-gnuplot-0.0.31 (crate (name "gnuplot") (vers "0.0.31") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "0zxxffqqcahj00ai93ig4dzlqh49lkmmjc4ai40bh0h2h7f0ga8q")))

(define-public crate-gnuplot-0.0.32 (crate (name "gnuplot") (vers "0.0.32") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "19rdxaz4jgj9p6xj3dkmnmp11jzsyyampv62l0m1lkjxwwa27z1j")))

(define-public crate-gnuplot-0.0.33 (crate (name "gnuplot") (vers "0.0.33") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "13wbp7ksgpbs5vhz55ljv21hn9kbhyl6ld4y27xn29pilb4qi9ai")))

(define-public crate-gnuplot-0.0.34 (crate (name "gnuplot") (vers "0.0.34") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "0ph7f0a2a8dvddpch9c1zn139x1fddah29wzsfipk75q78i6zxqc")))

(define-public crate-gnuplot-0.0.35 (crate (name "gnuplot") (vers "0.0.35") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "19aq83ydz0v0wy9dnmgy3sd3fzdr3ppr5ahchh0by0qxbf64q8pn")))

(define-public crate-gnuplot-0.0.36 (crate (name "gnuplot") (vers "0.0.36") (deps (list (crate-dep (name "argparse-rs") (req "= 0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1hc8plj47031djhqw8w188kxxv70cj6zpbbs535q23ib1fi2ywnh")))

(define-public crate-gnuplot-0.0.37 (crate (name "gnuplot") (vers "0.0.37") (deps (list (crate-dep (name "argparse-rs") (req "=0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1wxh5d1xjbw47viy32jxkhkqnvacvb3lfcafygjhvhbyg8kkgr4d")))

(define-public crate-gnuplot-0.0.38 (crate (name "gnuplot") (vers "0.0.38") (deps (list (crate-dep (name "argparse-rs") (req "=0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0883rakrn1s0bdym85zhqx0x99qki5mvr7x8nm8rphf78wahz663")))

(define-public crate-gnuplot-0.0.39 (crate (name "gnuplot") (vers "0.0.39") (deps (list (crate-dep (name "argparse-rs") (req "=0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0cgvl2pvkfr4iwr1g5gbnpw88jr46g589jbxr7qbsqpgi9xlca95")))

(define-public crate-gnuplot-0.0.40 (crate (name "gnuplot") (vers "0.0.40") (deps (list (crate-dep (name "argparse-rs") (req "=0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0w5qvlgz4m0cmf7vkcfgagksn3dfgrjf407cjhpsk4qrzmfxlwci")))

(define-public crate-gnuplot-0.0.41 (crate (name "gnuplot") (vers "0.0.41") (deps (list (crate-dep (name "argparse-rs") (req "=0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "1fsg6bw2k84iy48zkijxzj4a3fwrbkp68w5hy8ifqqszrv86nfb0")))

(define-public crate-gnuplot-0.0.42 (crate (name "gnuplot") (vers "0.0.42") (deps (list (crate-dep (name "argparse-rs") (req "=0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "14mavibfgwpn3gnsvn5gykl2j08m847rai131pgliwxn2rhxay1j")))

(define-public crate-gnuplot-0.0.43 (crate (name "gnuplot") (vers "0.0.43") (deps (list (crate-dep (name "argparse-rs") (req "=0.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0v5bz2hw08s5czgjsj682ygdh383z526gf9ncix8a0lcg0i0prww")))

(define-public crate-gnuplotter-0.1 (crate (name "gnuplotter") (vers "0.1.0") (hash "0wcrxrh1c6k1b9lhmpd911gjrvinf9qydm7hc3gv87m6d7rdy90b") (yanked #t)))

(define-public crate-gnuplotter-0.1 (crate (name "gnuplotter") (vers "0.1.2-alpha") (hash "1isxc19f7270hh61dsgljx2wqnjz6dw44c13qklp6xzs4dl73n5c") (yanked #t)))

(define-public crate-gnuplotter-0.1 (crate (name "gnuplotter") (vers "0.1.5-alpha") (hash "09wv1y32hxvvnlij63hwsv44fjwdm6v4797rvjw9g71kmp1rxfx8") (yanked #t)))

(define-public crate-gnuplotter-0.1 (crate (name "gnuplotter") (vers "0.1.6-alpha") (deps (list (crate-dep (name "gnuplotter_core") (req "^0.1.6-alpha") (default-features #t) (kind 0)) (crate-dep (name "gnuplotter_macros") (req "^0.1.6-alpha") (default-features #t) (kind 0)))) (hash "030grg5cw7mqhlb9dk5ngprb2w3gf8ss0c61myk0p3xpfn1rvn27") (yanked #t)))

(define-public crate-gnuplotter-0.2 (crate (name "gnuplotter") (vers "0.2.2-alpha") (deps (list (crate-dep (name "gnuplotter_core") (req "^0.2.2-alpha") (default-features #t) (kind 0)) (crate-dep (name "gnuplotter_macros") (req "^0.2.2-alpha") (default-features #t) (kind 0)))) (hash "1afb22iwdrcr61j7a6vnvgc4k1vzrgjd2n2l24gl999dlc51ha7v")))

(define-public crate-gnuplotter-0.2 (crate (name "gnuplotter") (vers "0.2.3-alpha") (deps (list (crate-dep (name "gnuplotter_core") (req "^0.2.3-alpha") (default-features #t) (kind 0)) (crate-dep (name "gnuplotter_macros") (req "^0.2.3-alpha") (default-features #t) (kind 0)))) (hash "0d7iil7xj72di4vjbbg9kyz3wz5yy55x7ddxclxj3d0z3d4zbs92")))

(define-public crate-gnuplotter_core-0.1 (crate (name "gnuplotter_core") (vers "0.1.6-alpha") (hash "0xhxmd8gd7cxnqr9jq72kw4zzflhmfp66vl0crb4sq8ckvznp8am") (yanked #t)))

(define-public crate-gnuplotter_core-0.2 (crate (name "gnuplotter_core") (vers "0.2.2-alpha") (deps (list (crate-dep (name "either") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0x4qqy67998jksbinbl01xigi409kk209326s1ibj0lxxvskhzxn")))

(define-public crate-gnuplotter_core-0.2 (crate (name "gnuplotter_core") (vers "0.2.3-alpha") (deps (list (crate-dep (name "either") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0p5nsmghliwxxl1yinsydbh8nm77vqg65nbrxv3g28inq4zi1cvz")))

(define-public crate-gnuplotter_macros-0.1 (crate (name "gnuplotter_macros") (vers "0.1.6-alpha") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "gnuplotter_core") (req "^0.1.6-alpha") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 0)))) (hash "112d9yg0g6b5kp49kmg1ljgixi01xvs7plpvjjwxfwa1d9z9qp13") (yanked #t)))

(define-public crate-gnuplotter_macros-0.2 (crate (name "gnuplotter_macros") (vers "0.2.2-alpha") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "gnuplotter_core") (req "^0.2.2-alpha") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro" "full"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "196ydhh624740qlf3z7xpx655ahq5byq9k2nwbjw3vyqwzcp4paj")))

(define-public crate-gnuplotter_macros-0.2 (crate (name "gnuplotter_macros") (vers "0.2.3-alpha") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "gnuplotter_core") (req "^0.2.3-alpha") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("derive" "parsing" "printing" "clone-impls" "extra-traits" "proc-macro" "full"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "03q2d46g2hb89c75npsbpff8vkzjzl5x9i4f1dm5i4fr7wrjz20k")))

