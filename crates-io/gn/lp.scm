(define-module (crates-io gn lp) #:use-module (crates-io))

(define-public crate-gnlp-0.1 (crate (name "gnlp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rust-bert") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0v65cgsywz1bxbkfm0yp9qgi19aim4cwn00gzkg5z880ggl8ywf2")))

