(define-module (crates-io v8 -a) #:use-module (crates-io))

(define-public crate-v8-api-0.1 (crate (name "v8-api") (vers "0.1.0") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_9"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0475q8lh0i59kf7nz48yn56llabl3qc2wj3vrnp0gsi81zdq2lqh")))

(define-public crate-v8-api-0.1 (crate (name "v8-api") (vers "0.1.1") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "13i0x5yll24q6x6d5j1k5il19sbyji83kwz24c5a5d0ad5afm525")))

(define-public crate-v8-api-0.1 (crate (name "v8-api") (vers "0.1.2") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0d7brjsr8j8gpg89qg4yjljc8kcr79sjppr68lk8hgm6xaskyxza")))

(define-public crate-v8-api-0.1 (crate (name "v8-api") (vers "0.1.3") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1wzcfa4b1mw4pwzpiai2bcq0ca39s3m4wwz4r5r6995rgr9z8xkm")))

(define-public crate-v8-api-0.1 (crate (name "v8-api") (vers "0.1.4") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0z59qlcjgb4dm8zl2br6m8kgwl230f2svdfkv42n587xk546gna7")))

(define-public crate-v8-api-0.2 (crate (name "v8-api") (vers "0.2.0") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "00k0jk5qr2agi5v9ib0f3qhm6g64gl56sx5yqqdhfpsdag4b3jwy")))

(define-public crate-v8-api-0.3 (crate (name "v8-api") (vers "0.3.0") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0fmc62kk4giandfs570sjc8z6qb6i78gphxj6sixsksdbppl67jv")))

(define-public crate-v8-api-0.4 (crate (name "v8-api") (vers "0.4.0") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1bsnh918f6zm5cclnwhwz3frlljyp76njqx1lsbpv7p4qzd89wcj")))

(define-public crate-v8-api-0.5 (crate (name "v8-api") (vers "0.5.0") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0r24zn2154n0q3hb2zynrlvrpvqai3ghy4if7m1y00xa7sn4gnfg")))

(define-public crate-v8-api-0.6 (crate (name "v8-api") (vers "0.6.0") (deps (list (crate-dep (name "clang") (req "^0.12.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "11mhq99wb4v393hl2k78jf4394w2rnakma6z52hrwcwln89w36yn")))

(define-public crate-v8-api-0.7 (crate (name "v8-api") (vers "0.7.0") (deps (list (crate-dep (name "clang") (req "^0.13.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0x6a8s96b1zznvbc0scxbkci4b56s12mnrn866kmsr75b2niqmc5")))

(define-public crate-v8-api-0.7 (crate (name "v8-api") (vers "0.7.1") (deps (list (crate-dep (name "clang") (req "^0.14.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1pzr1qjxn00bdvjs0fqdbw2qlqfqqyva43acc6zh7ra1vxppk95q")))

(define-public crate-v8-api-0.7 (crate (name "v8-api") (vers "0.7.2") (deps (list (crate-dep (name "clang") (req "^0.15.0") (features (quote ("clang_3_5"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1zyacr9kbqpjnj7my74n63mmdz3ih0b3b0apf390cp1dznlgnv6x")))

(define-public crate-v8-api-0.7 (crate (name "v8-api") (vers "0.7.3") (deps (list (crate-dep (name "clang") (req "^0.15.0") (features (quote ("clang_3_5" "runtime"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "091w6hxmlk37frm12xp2234xg95inrhdbshlyx7sz9iyll54zd5s")))

(define-public crate-v8-api-0.8 (crate (name "v8-api") (vers "0.8.0") (deps (list (crate-dep (name "clang") (req "^0.15.0") (features (quote ("clang_3_5" "runtime"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0az8zv7xaq570jshbn7bx6s4llqhp1dk8gjnbgxnk7a07f7wyv6l")))

