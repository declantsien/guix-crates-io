(define-module (crates-io v8 fi) #:use-module (crates-io))

(define-public crate-v8find4rs-0.1 (crate (name "v8find4rs") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs_io") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "exe") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1xhvy3ld77hxab2jqdfdyvsn4863l4gw4s30ypg8dgx5xp84yg9j")))

