(define-module (crates-io v8 #{53}#) #:use-module (crates-io))

(define-public crate-v853-pac-0.0.1 (crate (name "v853-pac") (vers "0.0.1") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "18ws1qrnwc2gs47zixp42n2p3zgn3wdswfxl6mp0c3mjfmhl7yxj") (features (quote (("rt"))))))

