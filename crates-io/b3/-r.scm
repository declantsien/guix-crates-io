(define-module (crates-io b3 -r) #:use-module (crates-io))

(define-public crate-b3-rs-0.0.0 (crate (name "b3-rs") (vers "0.0.0") (hash "15nmx0lzmv9wpbk48lfm496lbirv8hrik9zr8z3cdbg09a4hmk3a") (features (quote (("std") ("default" "std"))))))

(define-public crate-b3-rs-0.1 (crate (name "b3-rs") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "nano-leb128") (req "^0.1.0") (kind 0)))) (hash "1afsd2ffv2hij10v0h4mmlp0j3hl8gxrq0snip0i26ifn7kxlzqx") (features (quote (("std" "nano-leb128/std") ("default" "std"))))))

(define-public crate-b3-rs-0.1 (crate (name "b3-rs") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "nano-leb128") (req "^0.1.0") (kind 0)))) (hash "04qmgxfxqvw5csdciad6f5dgvz4ih19x79bxh4p0ina8anz8ziaa") (features (quote (("std" "nano-leb128/std") ("default" "std"))))))

(define-public crate-b3-rs-0.1 (crate (name "b3-rs") (vers "0.1.0") (deps (list (crate-dep (name "nano-leb128") (req "^0.1.0") (kind 0)))) (hash "0z3q7h06z6hzb4zwbgpwmvwijq30aj7s2c781zjvanvwyfrh6ir4") (features (quote (("std" "nano-leb128/std") ("default" "std"))))))

