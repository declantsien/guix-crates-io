(define-module (crates-io b3 -d) #:use-module (crates-io))

(define-public crate-b3-display-handler-0.1 (crate (name "b3-display-handler") (vers "0.1.0") (hash "1a3jikjajhdgsc2sbddfc25xw3qywn78rz44f7xcvynfbplfi67f")))

(define-public crate-b3-display-handler-0.1 (crate (name "b3-display-handler") (vers "0.1.1") (hash "1x5lpid7liqsm4iyg0kqlhik4q737vbr6x0zj7yaqii3as51m7ml")))

(define-public crate-b3-display-handler-0.1 (crate (name "b3-display-handler") (vers "0.1.2") (hash "0ig3fq9kwzfaxn6zjwsi8f36ddmbs2i34n31yqlac8x0qvppkwkb")))

