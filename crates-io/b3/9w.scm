(define-module (crates-io b3 #{9w}#) #:use-module (crates-io))

(define-public crate-b39wc-1 (crate (name "b39wc") (vers "1.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colour") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "microprofile") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0wp3hlnvagps0i8w89nny1hdsxypm3qybnb8l67mwd1djv67kbsz")))

