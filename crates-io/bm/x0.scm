(define-module (crates-io bm x0) #:use-module (crates-io))

(define-public crate-bmx055-0.1 (crate (name "bmx055") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "nb") (req "^1.1") (default-features #t) (kind 0)))) (hash "1rni78v46zp99494qkldvr163lnw245zxpqq1kimpss413gvn07x")))

