(define-module (crates-io bm p8) #:use-module (crates-io))

(define-public crate-bmp8bit-0.1 (crate (name "bmp8bit") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)))) (hash "1gyv5bx62qmiws6s6v6y3dglsnkh0jqfjcci8q8k3b4nd8spzcin")))

