(define-module (crates-io bm i0) #:use-module (crates-io))

(define-public crate-bmi088-0.1 (crate (name "bmi088") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "1z71z9g1k9i26cgjzdmsbgg4fbp7is4wsgni8may184zsqvw4h35")))

