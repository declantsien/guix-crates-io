(define-module (crates-io bm w-) #:use-module (crates-io))

(define-public crate-bmw-derive-0.1 (crate (name "bmw-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1y4ix0zw4x7824qsraflzk30xs4r5zdc42lj25lggwyl67f57hzm")))

