(define-module (crates-io bm i-) #:use-module (crates-io))

(define-public crate-bmi-calculator-0.2 (crate (name "bmi-calculator") (vers "0.2.0") (deps (list (crate-dep (name "ndless") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "ndless-handler") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0g04xapadnmn6xrjjv3qnlpd6prhvi3lw8wkcy5dlihca3fh56w4")))

(define-public crate-bmi-calculator-0.2 (crate (name "bmi-calculator") (vers "0.2.2") (deps (list (crate-dep (name "ndless") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "ndless-handler") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1asxic7vmvmnbylsrr78dhv96kxicykppa8g1mpnsz68lqb526im")))

(define-public crate-bmi-calculator-0.2 (crate (name "bmi-calculator") (vers "0.2.3") (deps (list (crate-dep (name "ndless") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "ndless-handler") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "01n09dlippbbpkws2r58amz9xhv02bxavbv4f5v4kmvpn43c6dr6")))

