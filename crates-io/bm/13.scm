(define-module (crates-io bm #{13}#) #:use-module (crates-io))

(define-public crate-bm1397-protocol-0.1 (crate (name "bm1397-protocol") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)) (crate-dep (name "crc-any") (req "^2.4.3") (kind 0)) (crate-dep (name "defmt") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "1karj9xmgd1scz4wmnvwal6pw77g4ymdnvf1lh0avn4wa35qm58q") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

(define-public crate-bm1397-protocol-0.2 (crate (name "bm1397-protocol") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)) (crate-dep (name "crc-any") (req "^2.4.3") (kind 0)) (crate-dep (name "defmt") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "08bk126678jmvsnrpahwc5wm7l9cxbw08lqak3z30yanlm6fw5rq") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

