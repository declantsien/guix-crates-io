(define-module (crates-io bm ff) #:use-module (crates-io))

(define-public crate-bmff-0.1 (crate (name "bmff") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.23") (default-features #t) (kind 0)))) (hash "1apikni3pn36ldasxyj8rnq3wrsrw7s9ijakx3490m3wxhmq87h9") (rust-version "1.66")))

(define-public crate-bmff-0.1 (crate (name "bmff") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.23") (default-features #t) (kind 0)))) (hash "0d2h46979gvdhca10lg7z4k56zackk4ll7dlnhd3v8jldwky8wwk") (rust-version "1.66")))

