(define-module (crates-io bm p_) #:use-module (crates-io))

(define-public crate-bmp_rs-0.0.0 (crate (name "bmp_rs") (vers "0.0.0") (hash "1vgj2wrdg6w1mxdl9zvcmpjpz68vqximqqb4rfa6mqygx9i5wvr3")))

(define-public crate-bmp_rs-0.0.1 (crate (name "bmp_rs") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "1.1.*") (default-features #t) (kind 0)))) (hash "0gflia3ks8m59affqmr1id8x1r562cnpchnn7gspay3i0q5yk6f2")))

(define-public crate-bmp_rs-0.0.2 (crate (name "bmp_rs") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "1.1.*") (default-features #t) (kind 0)))) (hash "1sn9kzpgjfby68qlas9kwi6c2jvhv81f78drhn4h8pn4ja89ix1d")))

(define-public crate-bmp_rs-0.0.3 (crate (name "bmp_rs") (vers "0.0.3") (deps (list (crate-dep (name "byteorder") (req "1.1.*") (default-features #t) (kind 0)))) (hash "1xjpfd1r86zyr2fj50d1q660lpwbgzsgjblq979j9krgbhb257pa")))

(define-public crate-bmp_rs-0.0.4 (crate (name "bmp_rs") (vers "0.0.4") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "02d6rshbl78zpmmnsx62q0vyybb57ayf9l3r6vgid69m9c5lbr7y")))

(define-public crate-bmp_rs-0.0.5 (crate (name "bmp_rs") (vers "0.0.5") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "0awf4c2zcsfv2bpzhqw70iy96nwwkdgd3w1fn715s4y0sg6dw8jp")))

(define-public crate-bmp_rs-0.0.6 (crate (name "bmp_rs") (vers "0.0.6") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "14v14kz81ps5gzldriyb4ip8a5yplrpszdpnhpil0bsd1npbl29d")))

(define-public crate-bmp_rs-0.0.9 (crate (name "bmp_rs") (vers "0.0.9") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)))) (hash "0hhzpqa2hf47pyvblfb875hl7an23lqrk4gil218cp1j389gxyx9")))

