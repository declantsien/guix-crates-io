(define-module (crates-io bm pf) #:use-module (crates-io))

(define-public crate-bmpfntgen-0.1 (crate (name "bmpfntgen") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1p0cahyn7wi7rbfr87vvxn43h8qafgc8gq7djr2kmmgwh0gvz7sf")))

