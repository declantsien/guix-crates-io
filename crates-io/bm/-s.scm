(define-module (crates-io bm -s) #:use-module (crates-io))

(define-public crate-bm-ssz-0.2 (crate (name "bm-ssz") (vers "0.2.0") (deps (list (crate-dep (name "bm") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "primitive-types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "0hi5k9b0dfxl3js43lpsxqjsl8r65zibzsm6cqfssiynjvyjy1zw")))

