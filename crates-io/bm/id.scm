(define-module (crates-io bm id) #:use-module (crates-io))

(define-public crate-bmidi-0.0.1 (crate (name "bmidi") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "*") (default-features #t) (kind 2)) (crate-dep (name "dsp-chain") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "pitch_calc") (req "^0.9.7") (default-features #t) (kind 0)) (crate-dep (name "synth") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "time_calc") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "0374x7cwqhshz3fkfhbd5ygf98d2wzmsfjrq9yirjgch4qkkh3aq")))

(define-public crate-bmidi-0.0.2 (crate (name "bmidi") (vers "0.0.2") (deps (list (crate-dep (name "pitch_calc") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0jxq9rvw9lg0swf3a2h4mbqrrc6syr52abir19flv3vs245x36v0")))

(define-public crate-bmidi-0.0.3 (crate (name "bmidi") (vers "0.0.3") (deps (list (crate-dep (name "pitch_calc") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0drk847rv37imw5b4rfpa2nawbl5vcl8pgyxsn5zmgr43ny5jy8x")))

