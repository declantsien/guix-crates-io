(define-module (crates-io bm p-) #:use-module (crates-io))

(define-public crate-bmp-client-0.1 (crate (name "bmp-client") (vers "0.1.0") (hash "0kn7iml79m44kba350aha0h056z91rb9xfb7sjnc396lv7q05mb0")))

(define-public crate-bmp-client-0.1 (crate (name "bmp-client") (vers "0.1.1") (deps (list (crate-dep (name "bmp-protocol") (req "=0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("tcp"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("net" "rt-core" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-util") (req "^0.3") (features (quote ("codec"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cb4n73mjmig5hwc9370mndfxx7niy0991ai61nd94yxnrik7j5s")))

(define-public crate-bmp-encoder-0.1 (crate (name "bmp-encoder") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)))) (hash "16695q56j7y5z736bd694arkykh1wzcqkvlrmdspf4da45b2a1hg")))

(define-public crate-bmp-monochrome-0.1 (crate (name "bmp-monochrome") (vers "0.1.0") (hash "14sz5243979q2809r6fi5q3v8j1mx44gjbr3ni68cy0xkbbbzlax")))

(define-public crate-bmp-monochrome-0.2 (crate (name "bmp-monochrome") (vers "0.2.0") (hash "1jw6f4m2gdcnyrr6d6iacvjz16lp8h482a19rhb4di69a0dqa1jp")))

(define-public crate-bmp-monochrome-0.3 (crate (name "bmp-monochrome") (vers "0.3.0") (hash "06h87yjqf4481ixqc9r8kpc0cdsg7pr7y5pg2rvl7y1jn89xxrmf")))

(define-public crate-bmp-monochrome-0.4 (crate (name "bmp-monochrome") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0s0qhw0vdjvw2acx4zx7yxhs8y46l8jxgjwz504pv246vcrxf8ax")))

(define-public crate-bmp-monochrome-0.6 (crate (name "bmp-monochrome") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "19d3mppjyiwlslhmhypsavhx9snqxh68n3ab9q96mmr8hxm2hq0f")))

(define-public crate-bmp-monochrome-0.7 (crate (name "bmp-monochrome") (vers "0.7.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "00vf1jknm8fc2nc8r0sqk8yvych4zjc3w2vzm0krf2l47ab2kcrd")))

(define-public crate-bmp-monochrome-0.8 (crate (name "bmp-monochrome") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1i52qb7nh71lr42iyf7kfzaar9d6hndpahfraxbypdasfmgq618y")))

(define-public crate-bmp-monochrome-0.9 (crate (name "bmp-monochrome") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1pkmc487nwhgg0nhiwrymqkvhikca16754hppq62a553yv5yswhi")))

(define-public crate-bmp-monochrome-0.10 (crate (name "bmp-monochrome") (vers "0.10.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1iy5snzfw2s58vki1scghw85jp0v6d8i7pk53886r7bvsv0n41qa")))

(define-public crate-bmp-monochrome-0.11 (crate (name "bmp-monochrome") (vers "0.11.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1gy9yzh919095fx9shdx1fvrr8p1yr28a5hcmdgnc3mjzxbspa0d")))

(define-public crate-bmp-monochrome-0.12 (crate (name "bmp-monochrome") (vers "0.12.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0gqh2m6n9icd56bk90an9d2yc5irkd5nky56mb4pa1y9pj99pdif")))

(define-public crate-bmp-monochrome-0.13 (crate (name "bmp-monochrome") (vers "0.13.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1xh3m3cxdnfsmxhxjfhrmc2id8r246v0g53x0ynnny7f448lhcm9")))

(define-public crate-bmp-monochrome-0.14 (crate (name "bmp-monochrome") (vers "0.14.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0aadyjjpdsjv4nzkh5m3dyz3hh7dih1bc2zwndy5yy1911ykihv7") (yanked #t)))

(define-public crate-bmp-monochrome-0.15 (crate (name "bmp-monochrome") (vers "0.15.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0n123rmda6ma560l09mdz16ldvqrjj7qa4i22n0s8mg1gdbb2jhb") (features (quote (("fuzz" "arbitrary"))))))

(define-public crate-bmp-monochrome-0.16 (crate (name "bmp-monochrome") (vers "0.16.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1zc24r6lcvwnawldh2ramblfrzaxq4ppzpf19041j2qzwp23mdvm") (features (quote (("fuzz" "arbitrary"))))))

(define-public crate-bmp-monochrome-0.17 (crate (name "bmp-monochrome") (vers "0.17.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.10") (features (quote ("bmp"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1nxrkfqlaapgnvbavhmyrcs1i9hsl397rrd4f6z68wk6hg6iikiz") (features (quote (("fuzz" "arbitrary" "image"))))))

(define-public crate-bmp-monochrome-1 (crate (name "bmp-monochrome") (vers "1.0.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.10") (features (quote ("bmp"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1hc0mlmy26jpvx34rw896ki60wkb6c2finnsfs3zvzfin5bwpmi0") (features (quote (("fuzz" "arbitrary" "image"))))))

(define-public crate-bmp-monochrome-1 (crate (name "bmp-monochrome") (vers "1.1.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.10") (features (quote ("bmp"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0rv50kszwmjg3ricw3j6924n44bclmfdhbnwfrsdw7vnscmhi6l2") (features (quote (("fuzz" "arbitrary" "image"))))))

(define-public crate-bmp-protocol-0.1 (crate (name "bmp-protocol") (vers "0.1.0") (hash "1l21w1vqd4f74k8k6hsgy1x5gm2p3zizlndlpiz05z2k27kzhip1")))

(define-public crate-bmp-protocol-0.1 (crate (name "bmp-protocol") (vers "0.1.3") (deps (list (crate-dep (name "bgp-rs") (req "=0.6") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("tcp"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("fs" "net" "rt-core" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-util") (req "^0.3") (features (quote ("codec"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kmfkj52phrjhn5905ri1yk3z9gawbc0r81a7bryfpv7yfkj0b07")))

(define-public crate-bmp-rust-0.1 (crate (name "bmp-rust") (vers "0.1.0") (hash "1qqxg4pfm81c8yj5iv36w9296nqba86qxjxbqs6inzmx6p01bk6h")))

(define-public crate-bmp-rust-0.1 (crate (name "bmp-rust") (vers "0.1.1") (hash "16m33symxx1p4app5nzmp5cf3wzzfbf3sfffarz8m8k1v93q1wlh")))

(define-public crate-bmp-rust-0.1 (crate (name "bmp-rust") (vers "0.1.2") (hash "15i3p7hjaxn82wlyxp33cnr9yyikd497qb44jfmj68wxk9i9yxyy")))

(define-public crate-bmp-rust-0.2 (crate (name "bmp-rust") (vers "0.2.0") (hash "1bzvqv42mrpvchahzrxh1xfxfnwblgaqjzdx2xhqfjspgf29xqkm")))

(define-public crate-bmp-rust-0.2 (crate (name "bmp-rust") (vers "0.2.1") (hash "1vhp9pqkv5xhzc2va39y0lq7w2k1w1vkynh9k8xm0kay6dqi23ml")))

(define-public crate-bmp-rust-0.2 (crate (name "bmp-rust") (vers "0.2.2") (hash "1vm4rlgnkj8kp5p2nr6f314r6gchh1sihbi1qrq5ap183gylm1ms") (yanked #t)))

(define-public crate-bmp-rust-0.2 (crate (name "bmp-rust") (vers "0.2.3") (hash "0ajdvnpvhn6zlw4rw07ipb75x4779q4q0ndd71cjq6gs5j62rh2p") (yanked #t)))

(define-public crate-bmp-rust-0.2 (crate (name "bmp-rust") (vers "0.2.4") (hash "0qkzr2hdjwvlz3j1mmlf504czc19sg8y3db31bi46liwizm992g1")))

(define-public crate-bmp-rust-0.2 (crate (name "bmp-rust") (vers "0.2.5") (hash "0sw2k9ag4d1aq19gr9rbbxrvvx8y8jmsvnjhlm31ffp6d9jks7zg")))

(define-public crate-bmp-rust-0.2 (crate (name "bmp-rust") (vers "0.2.6") (hash "0vfm9xh0y3nkka07kpskrx4maxxyswhz9dmjyjckc0iy9yimwgac")))

(define-public crate-bmp-rust-0.3 (crate (name "bmp-rust") (vers "0.3.0") (hash "1yarhsd1rb9lig7sl46x9vc4ygi5qr51yfbawhx6wis39r46wscy")))

(define-public crate-bmp-rust-0.3 (crate (name "bmp-rust") (vers "0.3.1") (hash "1ins2pv7xyiqvg6vjrjxm84ciadf50x7lz01qn6c5wlii788sgcs")))

(define-public crate-bmp-rust-0.3 (crate (name "bmp-rust") (vers "0.3.2") (hash "0dd2fza3zpbxxw36mh69bifk4j3kv39xsabnj0xsapq5zh3fpays")))

(define-public crate-bmp-rust-0.3 (crate (name "bmp-rust") (vers "0.3.3") (hash "0hwrbv49i50j8qhyjrbbks5sbs4x0wiyj24c174ccqkga5c1dcbp")))

(define-public crate-bmp-rust-0.3 (crate (name "bmp-rust") (vers "0.3.4") (hash "08qckl9jg6c5y82km5y4395mmcvk1znqqdw3qmvv7n1lnsl0cd88")))

(define-public crate-bmp-rust-0.4 (crate (name "bmp-rust") (vers "0.4.0") (hash "1va1q5ms05glwi06q9zcibrgczsynl4jiw2z1g9ppqz5hmgh7wr0")))

(define-public crate-bmp-rust-0.4 (crate (name "bmp-rust") (vers "0.4.1") (hash "190gidhr9850hf5llchmkgvh4nnd3x7rngg4hizs12cr0ylrbcvk")))

