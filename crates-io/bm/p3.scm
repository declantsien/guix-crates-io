(define-module (crates-io bm p3) #:use-module (crates-io))

(define-public crate-bmp388-0.0.1 (crate (name "bmp388") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0zx7qj0xfqhxlxv578g0x6jy74b3gnw9xhn6a5safp395p6fg3y0") (yanked #t)))

(define-public crate-bmp388-0.0.2 (crate (name "bmp388") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1ly3075b0g98wyw8jw1da3gn9aqg2y7gh1q55kqqhbsynx6ipr4k") (yanked #t)))

(define-public crate-bmp388-0.0.3 (crate (name "bmp388") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "089sxllrg9yjh941capy2jcsr4gc2vm04kfz5wzp4rxgcq5nyw9i") (yanked #t)))

(define-public crate-bmp388-0.1 (crate (name "bmp388") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "05255ihqmh9xlpssjizglpgqkjl9ba86sflfwnaznnjrndpp5915")))

(define-public crate-bmp390-0.1 (crate (name "bmp390") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0") (features (quote ("eh1" "embedded-hal-async"))) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0jsfjapn6ii976rc4c5rpkkw82azfkwsjjjax8796ba5ksy07np0")))

(define-public crate-bmp390-0.2 (crate (name "bmp390") (vers "0.2.0") (deps (list (crate-dep (name "defmt") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3.8") (features (quote ("unstable-test"))) (default-features #t) (kind 2)) (crate-dep (name "embassy-time") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0") (features (quote ("eh1" "embedded-hal-async"))) (kind 2)) (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "uom") (req "^0.36.0") (features (quote ("f32" "si" "autoconvert"))) (kind 0)))) (hash "0718j121rgaks9i0kksv3vr685c5bdc0jy600818a78kw8n4jf73") (v 2) (features2 (quote (("embassy-time" "dep:embassy-time"))))))

