(define-module (crates-io bm oj) #:use-module (crates-io))

(define-public crate-bmoji-0.1 (crate (name "bmoji") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "emojis") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kkbvx36ga9mrvz7172779wzw1hf41w6hw2k76nxgmrgzpaq4y4a")))

(define-public crate-bmoji-0.2 (crate (name "bmoji") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "emojis") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xfh4jm5j9m32qqy1yvqiz487fcrifdp49iqf4a8h1xysj1ngp4j")))

(define-public crate-bmoji-0.2 (crate (name "bmoji") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "emojis") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lqdzdrkgf7f7jgmjab5f79pzpdcnzvzjsy9plbvivjw2cmib8rh")))

