(define-module (crates-io bm f-) #:use-module (crates-io))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.0") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v3dcj3nc108fs8vh6xvfwsw3n7i2dvbhbdy75fjmxkiwkly6qnr") (yanked #t)))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.1") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y5ipcsvg8yipb5zfb5i6sc4ilb5ql9wb5x9vl6pm93rv9a4x3kv")))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.2") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "15yjk7387jl9cpgz5lqkhcd8qyvfjsd1ax56qbbm89xv8sspnbab")))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.3") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "17lgbyv3bwlwk57ffax8wngj8d5lqmi0w213bppzkmhw5fi5iqva")))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.4") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jkp9v56yqx05fmhgccdcksqvbcqbi2vfmkr4sfa45mqgp9n6xqn")))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.5") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dpajmaicl37makpxf43im4ivc709mdaswbjq2lxnghqkqlkf68w")))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.6") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1db5b213l5sanhm9mss2xls196nxrqnmwnpsg3f4n9qxqnfc9rgw")))

(define-public crate-bmf-parse-0.1 (crate (name "bmf-parse") (vers "0.1.7") (deps (list (crate-dep (name "bstringify") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kawxrgzykyss02zj0kg21apif1ga6mbx8w9h31slp73gnicgnl9")))

