(define-module (crates-io bm os) #:use-module (crates-io))

(define-public crate-bmos_client-1 (crate (name "bmos_client") (vers "1.0.0") (deps (list (crate-dep (name "json_minimal") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "00fcj4w81ywbn2x4amlg1ha9kipdfr3lwd3mjzdzccid938xdydx")))

(define-public crate-bmos_client-1 (crate (name "bmos_client") (vers "1.0.1") (deps (list (crate-dep (name "json_minimal") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0mi0z2jib9ycbs4cjw41bnimqla2l2k4i93s492p1w0csfs0awny")))

(define-public crate-bmos_server-1 (crate (name "bmos_server") (vers "1.0.0") (deps (list (crate-dep (name "openweathermap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "random-number") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (features (quote ("image" "ttf"))) (kind 0)) (crate-dep (name "soloud") (req "^0.4.0") (features (quote ("openal"))) (kind 0)))) (hash "1895a9gjvab2x5ma962d4r5gljii96cdb39gxsbrzz7wvq5kb3y4")))

(define-public crate-bmos_server-1 (crate (name "bmos_server") (vers "1.0.1") (deps (list (crate-dep (name "openweathermap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "random-number") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (features (quote ("image" "ttf"))) (kind 0)) (crate-dep (name "soloud") (req "^0.4.0") (features (quote ("openal"))) (kind 0)))) (hash "0jx13q1j11440qfb6x98b1va4alj9lz60nl2ccgalvpgknlxbr92")))

(define-public crate-bmos_server-1 (crate (name "bmos_server") (vers "1.0.2") (deps (list (crate-dep (name "openweathermap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "random-number") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (features (quote ("image" "ttf"))) (kind 0)) (crate-dep (name "soloud") (req "^0.4.0") (features (quote ("openal"))) (kind 0)))) (hash "0j8fmbzkdb3flwvhhdxgkjx2hd0d0626bnwjhb3hpbjddyb5yc3w")))

(define-public crate-bmos_server-1 (crate (name "bmos_server") (vers "1.0.3") (deps (list (crate-dep (name "openweathermap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "random-number") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (features (quote ("image" "ttf"))) (kind 0)) (crate-dep (name "soloud") (req "^0.4.0") (features (quote ("openal"))) (kind 0)))) (hash "1r1rl51fza0d1dqzhsh9b6kj0baxaws5vpfdz0r5xyvx5k00lky3")))

