(define-module (crates-io bm a2) #:use-module (crates-io))

(define-public crate-bma2xx-0.1 (crate (name "bma2xx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0h1pbqrp7y1y14n3zsvg94bnjw0c24qj0s7vrp01z4l19q95g306")))

