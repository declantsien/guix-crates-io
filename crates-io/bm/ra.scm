(define-module (crates-io bm ra) #:use-module (crates-io))

(define-public crate-bmrankin_grrs-0.1 (crate (name "bmrankin_grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cf3nfa45mwm01whsdcxgfi2g89hhpigl2ivgbag9wj1mm4g4k95") (yanked #t)))

