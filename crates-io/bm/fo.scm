(define-module (crates-io bm fo) #:use-module (crates-io))

(define-public crate-bmfont-0.1 (crate (name "bmfont") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "*") (default-features #t) (kind 2)) (crate-dep (name "image") (req "*") (default-features #t) (kind 2)))) (hash "07cal793h6hignzaykr0my31xkrhyca6l2yj0m6l0bwmnjbvby22")))

(define-public crate-bmfont-0.2 (crate (name "bmfont") (vers "0.2.0") (deps (list (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.6") (default-features #t) (kind 2)))) (hash "0xfh8fyv0kahkkp3b56pcrg9pazw0x3633qcaw81mx3limx0sh5g")))

(define-public crate-bmfont-0.2 (crate (name "bmfont") (vers "0.2.1") (deps (list (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.6") (default-features #t) (kind 2)))) (hash "0xgc7lzif4czvn3i9m76929109ij2kk774nbyn1jbns0dxknymhc")))

(define-public crate-bmfont-0.3 (crate (name "bmfont") (vers "0.3.1") (deps (list (crate-dep (name "glium") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "1r3i8p4r62x2vvsdbndjs7dmcjyjsl5df2w2rrzc5blw8vxzihwc")))

(define-public crate-bmfont-0.3 (crate (name "bmfont") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "010pwy0aydfwy7847sv9i1q9vmp2fw4wxh798vxll8dh5k7g4gxz") (features (quote (("parse-error") ("default" "parse-error"))))))

(define-public crate-bmfont-0.3 (crate (name "bmfont") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "18z31rcpmnw861cnv15301xf7a6bjf2kfw0yvj5xmvk4v9wc2r3l") (features (quote (("parse-error") ("default" "parse-error"))))))

(define-public crate-bmfont_parser-0.1 (crate (name "bmfont_parser") (vers "0.1.0") (hash "1dj7ld5vwfq0jlpzv01kxxhm0v1wkgxlly9mpw10ps9bjcl79d4w")))

(define-public crate-bmfont_parser-0.1 (crate (name "bmfont_parser") (vers "0.1.1") (hash "04lrcxs0sq68gfrfqskf9kq12vabpzwi4hxihs9q1xcp0fbzsavj")))

(define-public crate-bmfont_parser-0.2 (crate (name "bmfont_parser") (vers "0.2.0") (hash "135fywg97wfb3gi87wkwgs78nyi98y08w0wgq99czmy9qb36mfky")))

(define-public crate-bmfont_rs-0.1 (crate (name "bmfont_rs") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 2)) (crate-dep (name "roxmltree") (req "^0.14.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.67") (default-features #t) (kind 2)))) (hash "18nqckkg6ma6k95b990cjgqic6parlvm70akgqyhmkckwkc0kmbh") (features (quote (("xml" "roxmltree") ("serde_boolint"))))))

(define-public crate-bmfont_rs-0.2 (crate (name "bmfont_rs") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 2)) (crate-dep (name "roxmltree") (req "^0.14.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.67") (default-features #t) (kind 2)))) (hash "19ikhqwhlx84hr33wlh0a6wriqwwj9kq15b4ywc3lfdnn4h6ffml") (features (quote (("xml" "roxmltree") ("serde_boolint"))))))

