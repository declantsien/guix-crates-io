(define-module (crates-io bm i1) #:use-module (crates-io))

(define-public crate-bmi160-0.1 (crate (name "bmi160") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1kmckwnkq9h5hs9mlxvnjfr95amlpkz5iwh33ywpsbp6cm09v2vn")))

(define-public crate-bmi160-0.1 (crate (name "bmi160") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0f5krdxfmha6xx7sfffrs0pnvki959v41b2rwf9mgzsd9smy9l4d")))

(define-public crate-bmi160-0.1 (crate (name "bmi160") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0zj8yh2z4gp8z7q7cv3p41c4ixi8b7sfv8h6sbn82l25vi08a650")))

(define-public crate-bmi160-1 (crate (name "bmi160") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0") (features (quote ("eh1"))) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1gzd7nllh9p8z37y81y3nlb8vrm6dyg63iwzr1x6fk1hxs9b0q0j")))

(define-public crate-bmi160-1 (crate (name "bmi160") (vers "1.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0") (features (quote ("eh1"))) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1an2w8x8jr9sbdfcjn7d85yjsjqqrqs31mcc6rvjzqs4lp285w88")))

