(define-module (crates-io bm k_) #:use-module (crates-io))

(define-public crate-bmk_linux-0.2 (crate (name "bmk_linux") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)))) (hash "0ix5s4gbx9r1qn5bn9z7wg8pd11xcd7v5whg2wx1s6y0sdf63ngl")))

(define-public crate-bmk_linux-0.2 (crate (name "bmk_linux") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)))) (hash "15ahy4mf60aszc5xflaifj5q4b55hj0ks7lqpl901mm4jgcfssyi")))

(define-public crate-bmk_linux-0.2 (crate (name "bmk_linux") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)))) (hash "1h170hhg9px4dn5n91i4q91nc7fm35003w8cg3mnngkmfj7rnznx")))

