(define-module (crates-io bm p2) #:use-module (crates-io))

(define-public crate-bmp280-0.1 (crate (name "bmp280") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19nwnzgr8ldmqz5wx86i5iqcyphpdvxpnf4qgn8b6wjnqdxzpj7d")))

(define-public crate-bmp280-0.1 (crate (name "bmp280") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1fi4pxwqyyar3026qlswl77mfx676qwn8lqmacww7qlisyypy3qn")))

(define-public crate-bmp280-0.1 (crate (name "bmp280") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0j8wj1jbh3yab89a154k60nmys53ly0x1jj8sdfb17nka4yrfxg6")))

(define-public crate-bmp280-0.2 (crate (name "bmp280") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0k53g5b6idvcy0lpvda10iaz51yfhf5byw0a2075mdx8ssgfga2s")))

(define-public crate-bmp280-0.3 (crate (name "bmp280") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1bph209ldmf9jypr2kmv1qadwsnlkphl00lby121314bj4hgsj30")))

(define-public crate-bmp280-0.4 (crate (name "bmp280") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "07pab46s2y0icyr6q4s92f6lv0gw09ckm0hvgp4kkqc4nav9kp5a")))

(define-public crate-bmp280-core-0.1 (crate (name "bmp280-core") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1d778xr5ch2zffr88a1hsqysx5y1lr5lb0d4z01405ka358i4jp1")))

(define-public crate-bmp280-core-0.1 (crate (name "bmp280-core") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0scigqxmic5h3wbika5grkp0mhjs76y4lx9hrvcyja3mcmx3a3r4")))

(define-public crate-bmp280-core-0.1 (crate (name "bmp280-core") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "14nq66abd65jpckq80vpgwb8bmkgkrbg9n4qr37p0m1pqv8gcprl")))

(define-public crate-bmp280-core-0.2 (crate (name "bmp280-core") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "18xz4nfdr4746b64ll8fjjkgqd37yhi8pf8zar5w902856b38158")))

(define-public crate-bmp280-driver-0.0.6 (crate (name "bmp280-driver") (vers "0.0.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wf2sb1rbbr8rzj42fwv4dnsbx4qx71rzsc7535k1xqsqlgpm2bs")))

(define-public crate-bmp280-driver-0.0.7 (crate (name "bmp280-driver") (vers "0.0.7") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0wxkrdymsmabd1gnggl0xz5ygb4g7mynxmv1s59hck47bmkyn0v8")))

(define-public crate-bmp280-ehal-0.0.1 (crate (name "bmp280-ehal") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "19mmclzs0rhcqakjy40b2a9pfwsc4mcxfma8b3jp9mlrrg7zjmrs")))

(define-public crate-bmp280-ehal-0.0.2 (crate (name "bmp280-ehal") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "03rlf0ny42bjyjmq7rxf5f4ccq66lbdclrps35ys3r61fvq3b67d")))

(define-public crate-bmp280-ehal-0.0.3 (crate (name "bmp280-ehal") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1i7iac540fi4ba2wx7xag1j9wpaj9jbyijyk0rzmqdp2d8dyrhnp")))

(define-public crate-bmp280-ehal-0.0.5 (crate (name "bmp280-ehal") (vers "0.0.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1k4mzjc70wjzydj222y4z981kqb4kd6l00jygp8cz3s7gq9rhb2y")))

(define-public crate-bmp280-ehal-0.0.6 (crate (name "bmp280-ehal") (vers "0.0.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0xybn2x6j4vp8n1fcxl5a6q3dfp35ii0gzxpdvvnysn9pfpfz16z")))

(define-public crate-bmp280-rs-0.1 (crate (name "bmp280-rs") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.6.10") (kind 0)))) (hash "1lhca6a14v9ikk5xw05i5zh0ysy1lmxzd1na2qjp321l8p2kbx22")))

(define-public crate-bmp280-rs-0.1 (crate (name "bmp280-rs") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.6.10") (kind 0)))) (hash "0byqlqcd0m37ig5sblbk47ib3lb68q24kcvs3jr53ywd4skwqp2b")))

(define-public crate-bmp280-rs-0.1 (crate (name "bmp280-rs") (vers "0.1.2") (deps (list (crate-dep (name "defmt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.6.10") (kind 0)))) (hash "1aqnds55h806kmnba3x3bkyp2mqgz74hzxwhxclbaiqk91gw3abw")))

(define-public crate-bmp280-spi-0.1 (crate (name "bmp280-spi") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "108zysbvskbmxqpxcxjq9dvy1jxngdmbxsj3gd45k2sbsqa1fa0d") (yanked #t)))

