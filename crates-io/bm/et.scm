(define-module (crates-io bm et) #:use-module (crates-io))

(define-public crate-bmetal-0.1 (crate (name "bmetal") (vers "0.1.0") (hash "1zcs2rayqxpdaimb2vnyvzb9smf3xdhhf1qbmmy3pg4bigh4sjm0")))

(define-public crate-bmetal-0.1 (crate (name "bmetal") (vers "0.1.1") (hash "0gn09a1899gh1sf1d2n19zcpzipzkqmqd2wrs4q5cn16mb1j7091")))

(define-public crate-bmetal-0.1 (crate (name "bmetal") (vers "0.1.2") (hash "1k0xbys49r1f3iqvk53ys95x0bdxw5y18yscmdcs9yhimiciy24s")))

(define-public crate-bmetal-0.1 (crate (name "bmetal") (vers "0.1.3") (hash "1x2l0icgwn2p7wdkkziwyyvxcrwigjd3d202d2d9srnx5qzchchp")))

