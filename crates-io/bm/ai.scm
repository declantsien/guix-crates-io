(define-module (crates-io bm ai) #:use-module (crates-io))

(define-public crate-bmail-0.1 (crate (name "bmail") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "enum-kinds") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)))) (hash "06syyz8v7s1zrd3jb7g12a6w26w1d9hkardy2vwj8mba6768y0rc")))

