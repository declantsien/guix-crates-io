(define-module (crates-io bm i2) #:use-module (crates-io))

(define-public crate-bmi270-0.1 (crate (name "bmi270") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1lz4dsyrfdyj9m7fhi62zml6k07sj6jsr9n76rqlrykmrrvcq0vv")))

(define-public crate-bmi270-0.1 (crate (name "bmi270") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0b2gfiq74r7gbsj86c69rlzc4xz1qmb86dpv1y6kkj50hmqjdzvh")))

