(define-module (crates-io c7 #{4d}#) #:use-module (crates-io))

(define-public crate-c74d-0.1 (crate (name "c74d") (vers "0.1.0") (hash "1lj1by0h14ra0yda1w42sc7vhszm3cvv7038n4jr9ss4j80vhrjp")))

(define-public crate-c74d-0.1 (crate (name "c74d") (vers "0.1.1") (hash "0w6p3ry0fym84y5jdv5rpcbvg5vqaa8gfr6vi3jl44lz8l081w7y") (rust-version "1.56.0")))

