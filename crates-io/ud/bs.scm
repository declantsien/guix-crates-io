(define-module (crates-io ud bs) #:use-module (crates-io))

(define-public crate-udbserver-0.1 (crate (name "udbserver") (vers "0.1.0") (deps (list (crate-dep (name "gdbstub") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "unicorn-engine") (req "^2.0.0-rc7") (features (quote ("use_system_unicorn"))) (default-features #t) (kind 0)))) (hash "1329fhzi22ll3h8hrqaaknq1w1f1smg5jhxp4jzlpjszq3by5yx1") (features (quote (("capi"))))))

