(define-module (crates-io ud pf) #:use-module (crates-io))

(define-public crate-udpflow-0.1 (crate (name "udpflow") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "time" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1psi4hyf5szag2hqjiba8vy0djf6c482apad0d0xpk099aw3xkgx")))

