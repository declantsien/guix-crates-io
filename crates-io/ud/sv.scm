(define-module (crates-io ud sv) #:use-module (crates-io))

(define-public crate-udsv-0.1 (crate (name "udsv") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ym7drwqzczkj8v1hh6v7v30v8q3b75b7c28g8iglc0kf2rb4m1m")))

