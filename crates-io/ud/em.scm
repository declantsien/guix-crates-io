(define-module (crates-io ud em) #:use-module (crates-io))

(define-public crate-udemy_jan24_mypackage-0.1 (crate (name "udemy_jan24_mypackage") (vers "0.1.0") (deps (list (crate-dep (name "array_tool") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "11fj22kgj27cl9pjimwwjkaqgips1ad47vmaagkg2frn1k9hl23a")))

(define-public crate-udemyone_v1_hello_dont_use-0.1 (crate (name "udemyone_v1_hello_dont_use") (vers "0.1.0") (hash "18xfvvhif8rr26q5z3374kqjcp8ayg00nj40yl585a3an5njwci4")))

