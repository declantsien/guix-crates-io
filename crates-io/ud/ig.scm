(define-module (crates-io ud ig) #:use-module (crates-io))

(define-public crate-udigest-0.1 (crate (name "udigest") (vers "0.1.0") (deps (list (crate-dep (name "digest") (req "^0.10") (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "udigest-derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0bnfd9vsjp6xd1yn5iqjnbh98id1ll2xc6pj8wyxr9crjp5sbr58") (features (quote (("std" "alloc") ("alloc")))) (v 2) (features2 (quote (("derive" "dep:udigest-derive"))))))

(define-public crate-udigest-derive-0.1 (crate (name "udigest-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "02fnsybmn4jadm41hrah244i9m1bg3wg4v5rwxbhiah5v8hz2acb")))

