(define-module (crates-io ud mp) #:use-module (crates-io))

(define-public crate-udmp-parser-0.1 (crate (name "udmp-parser") (vers "0.1.2") (hash "1a6w9kz5dajwm32z2lcd6gxyn06h42ryf95q54szn4xska13k6ir") (rust-version "1.65")))

(define-public crate-udmp-parser-0.2 (crate (name "udmp-parser") (vers "0.2.0") (hash "1dbldwa5b9knzhipvxvm2ajcg8cmh3hlrgcvsg7dvjxp1pgc23d2") (rust-version "1.65")))

(define-public crate-udmp-parser-rs-0.1 (crate (name "udmp-parser-rs") (vers "0.1.0") (hash "0k8mrfj60f7vw9xx5vljr4b4zcg5m05xgbgf5vsjq9ia1qy5kch3") (yanked #t) (rust-version "1.65")))

(define-public crate-udmp-parser-rs-0.1 (crate (name "udmp-parser-rs") (vers "0.1.1") (hash "1460lhbd6lvh538jnd18zb44nvh053icrh6pa8731k5a94f4zy3b") (yanked #t) (rust-version "1.65")))

(define-public crate-udmp-parser-rs-0.1 (crate (name "udmp-parser-rs") (vers "0.1.2") (hash "1g1nz5f9rlq79b47fvyll732mfchph5mclpdqsjr8mhjvd31dj9l") (yanked #t) (rust-version "1.65")))

