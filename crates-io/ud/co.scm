(define-module (crates-io ud co) #:use-module (crates-io))

(define-public crate-udcord-0.0.1 (crate (name "udcord") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1nm8cwmn4cagisjd8i0gasjp65byw3viybxkiqmzr92qhq5zap5f")))

