(define-module (crates-io ud ps) #:use-module (crates-io))

(define-public crate-udpsec-0.1 (crate (name "udpsec") (vers "0.1.0") (deps (list (crate-dep (name "rand_core") (req "^0.5.1") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^1.2.0") (features (quote ("reusable_secrets"))) (default-features #t) (kind 0)))) (hash "0fx1dj4b3jfcrbh4q9adnsn455wg1dmbvdksqzv6pq7xw9n58i9h")))

(define-public crate-udpsec-0.2 (crate (name "udpsec") (vers "0.2.0") (deps (list (crate-dep (name "rand_core") (req "^0.5.1") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "wait_not_await") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^1.2.0") (features (quote ("reusable_secrets"))) (default-features #t) (kind 0)))) (hash "13xjqnysgdk10xpdx1x4dr80sj1c30vqz5xa5chmfb5hxx2ryqd3")))

(define-public crate-udpsocket2-0.0.1 (crate (name "udpsocket2") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)))) (hash "0c2cisaran75bnkl956n3pca866k6si7f00d7q7q7czvxaxfc3cg")))

