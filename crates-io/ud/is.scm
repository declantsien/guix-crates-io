(define-module (crates-io ud is) #:use-module (crates-io))

(define-public crate-udisks-0.1 (crate (name "udisks") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0schsgz3q1qgxfzm260lp3jkxsc4jq7b1jwnghf3z9xhldyzxiam")))

(define-public crate-udisks2-0.1 (crate (name "udisks2") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.36") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "zbus") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "18709rmckmil97g9z1qnv7n8qn0v43nyzy3r9k1v8agxjnani45i") (rust-version "1.75")))

