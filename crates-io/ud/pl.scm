(define-module (crates-io ud pl) #:use-module (crates-io))

(define-public crate-udplite-0.1 (crate (name "udplite") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.59") (default-features #t) (target "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"android\"))") (kind 0)) (crate-dep (name "mio_06") (req "^0.6.14") (optional #t) (default-features #t) (kind 0) (package "mio")) (crate-dep (name "mio_07") (req "^0.7.0") (features (quote ("os-util"))) (optional #t) (default-features #t) (kind 0) (package "mio")))) (hash "1dzs5hjx7w2vv6i9zs4ac0y7kbskcm2w7mr5wp0r46nzpnqqkpdm")))

