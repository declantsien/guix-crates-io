(define-module (crates-io ud sx) #:use-module (crates-io))

(define-public crate-udsx-0.1 (crate (name "udsx") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12vncl7nqhmd30v9zvyzgj6rmmqiiyvf4iwq38s53h65nqk7mavh") (yanked #t)))

(define-public crate-udsx-0.2 (crate (name "udsx") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k68ja8v9hdzanvvq481p00p9hkrrn6jk5g41b4j3xamqzmrxcxs") (yanked #t)))

(define-public crate-udsx-0.3 (crate (name "udsx") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "01rqqc07hyvrcr9dn3vgrkmiy74ya2jh8a480qcwrlfsxgm3znb5") (yanked #t)))

(define-public crate-udsx-0.4 (crate (name "udsx") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ngndhdx8y2g9sxgi1i874i0aqvy56nmdp50i60ai34j0kjwdla2") (yanked #t)))

(define-public crate-udsx-0.5 (crate (name "udsx") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0flygfy8fa1bkjqy7rvnvrblp2knk8j6i2li6gq45cg4m4pz7d96") (yanked #t)))

(define-public crate-udsx-0.6 (crate (name "udsx") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vy6srwa1y61khq0sa2abc7y9f9yin94ifr9nxnx60rfblxmajl3") (yanked #t)))

(define-public crate-udsx-0.7 (crate (name "udsx") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xijw1vvmcg2mqwg2i0sv2qzzij7vm4fy4vyw7qigfr3fbxl0am2") (yanked #t)))

(define-public crate-udsx-0.7 (crate (name "udsx") (vers "0.7.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j30i1sk3mhqhsjbvv0k7n820g123qr8947i0w6fadrhgy58ad1f") (yanked #t)))

(define-public crate-udsx-0.7 (crate (name "udsx") (vers "0.7.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pcyyp0rl7mvz1hm9cd415varalvnb0dqwair6ihz5p8jxr1gq2v") (yanked #t)))

(define-public crate-udsx-0.8 (crate (name "udsx") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x15kp1azpldc7y0nfnpcb9gw4ff93hva41qc8n0z31drcm3zd8y") (yanked #t)))

(define-public crate-udsx-0.8 (crate (name "udsx") (vers "0.8.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gin448sp9fsnwj4jxyy8h96nc3xrwwm5gkbcrih89jlfvi9z2zi") (yanked #t)))

(define-public crate-udsx-0.9 (crate (name "udsx") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k2nk2q1ymgmaf5ixx9yb6vjrmhg1q55y19yjhxllz0ham31hr3g") (yanked #t)))

(define-public crate-udsx-0.10 (crate (name "udsx") (vers "0.10.0") (hash "0g8k3qhms0yd8fqq34cmi7nzcl4s0s3n659kg8g5qcgli06ymvzn") (yanked #t)))

(define-public crate-udsx-0.10 (crate (name "udsx") (vers "0.10.1") (hash "196qqlnjydajs8al3lxmih7cz5x3z86n2hh93raahvp4i0iyydnf") (yanked #t)))

(define-public crate-udsx-0.11 (crate (name "udsx") (vers "0.11.0") (hash "0d6x4gkn7w3lr2kv0l7dcv0s1glxv0vnp4577l5zs2rprxbzid0x") (yanked #t)))

(define-public crate-udsx-0.12 (crate (name "udsx") (vers "0.12.0") (hash "1yzv1xs4kklvqfw760b6agz1vbdiyzlmwlms90gpgdr7wvm878vm") (yanked #t)))

(define-public crate-udsx-0.12 (crate (name "udsx") (vers "0.12.1") (hash "02h89bmdfd2kngfig2xrqbjvclaj3zxmmyb43kg9iyclv23ld76x") (yanked #t)))

(define-public crate-udsx-0.13 (crate (name "udsx") (vers "0.13.0") (hash "1p9c710a2k83ay7jyrz45gx65hfwyr1b3dka38cmhwy8lcj8xccz") (yanked #t)))

(define-public crate-udsx-0.14 (crate (name "udsx") (vers "0.14.0") (hash "10h7gc80v9c0jrwhrk9v8h80gc30bhz7gcwlniv1fg8q593mvqmd") (yanked #t)))

