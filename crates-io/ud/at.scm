(define-module (crates-io ud at) #:use-module (crates-io))

(define-public crate-udatatable-0.1 (crate (name "udatatable") (vers "0.1.0") (deps (list (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ufmt-write") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 2)))) (hash "0sf55mlnrjlh00ma4dylyzlkv1gwzy2iv290ma5fxmfxm3nwk8kj") (features (quote (("plot"))))))

(define-public crate-udatatable-0.1 (crate (name "udatatable") (vers "0.1.1") (deps (list (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ufmt-write") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 2)))) (hash "1ffknb9nzhwnnkckiki4ddnnqlkiwwsgiqd4wsz02ysd5hc8vgzs") (features (quote (("plot"))))))

