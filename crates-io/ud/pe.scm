(define-module (crates-io ud pe) #:use-module (crates-io))

(define-public crate-udpexchange-0.1 (crate (name "udpexchange") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "const-lru") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.148") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static-alloc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1lj2v5h6mrd637s0sq0jqvv5l9msl1szy1c7k94djwcgfm1h921l") (features (quote (("mini" "libc"))))))

(define-public crate-udpexchange-0.1 (crate (name "udpexchange") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "const-lru") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.148") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static-alloc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0al88fr38jb5ibjy204g9fxv19xn4niyp8nx194001q3nj1wqsn4") (features (quote (("replay") ("mini" "libc"))))))

