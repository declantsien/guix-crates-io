(define-module (crates-io cp f_) #:use-module (crates-io))

(define-public crate-cpf_cnpj-0.1 (crate (name "cpf_cnpj") (vers "0.1.0") (hash "14zga99lb7kp6z7i04h2fyqkrk129jh6h6zsxl64ms6n9i20qyfx")))

(define-public crate-cpf_cnpj-0.1 (crate (name "cpf_cnpj") (vers "0.1.1") (hash "1ydb155cdksp0b1ads95rj0vka5vrb95q90jvnc7yn4wiwrm2kk1")))

(define-public crate-cpf_cnpj-0.2 (crate (name "cpf_cnpj") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "023n4sbl75cx33s4my7gl542y9dr45jnr0dan49bpg8kx0yzl3b3")))

(define-public crate-cpf_cnpj-0.2 (crate (name "cpf_cnpj") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1rmcf377flcrqi1gx3ghr3kmddbfrik760gaicjka45ir6hcr5nl")))

(define-public crate-cpf_util-0.1 (crate (name "cpf_util") (vers "0.1.0") (hash "1cvf9z7g98gsc7792wf0a704r9l3r79nmjjhdyxs0vnzxxfw89sf")))

(define-public crate-cpf_util-0.1 (crate (name "cpf_util") (vers "0.1.1") (hash "1kf4z1gs9kh452nhhvp628zr8rpdhhfk1w7al5rsrhp7r3vyhqxw")))

