(define-module (crates-io cp ac) #:use-module (crates-io))

(define-public crate-cpace-0.1 (crate (name "cpace") (vers "0.1.0") (deps (list (crate-dep (name "curve25519-dalek") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0sd5xjmy4a9pifvfnlyiv1jwkp8bq6d4rris3rm7808qfxwg3aks")))

