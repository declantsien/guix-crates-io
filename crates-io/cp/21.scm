(define-module (crates-io cp #{21}#) #:use-module (crates-io))

(define-public crate-cp211x_uart-0.1 (crate (name "cp211x_uart") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "12i61f0ap6853jg3qppry36aqlx43xsfdxg9z8pcwcaiz5hblf6b")))

(define-public crate-cp211x_uart-0.1 (crate (name "cp211x_uart") (vers "0.1.1") (deps (list (crate-dep (name "hidapi") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1shb5029lf7rhx18cqaid75gb2vknm5kwng827g2753z2cb1bm8x")))

(define-public crate-cp211x_uart-0.1 (crate (name "cp211x_uart") (vers "0.1.2") (deps (list (crate-dep (name "hidapi") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "19ysn3mxrbnw00zv8sy70pkmfpmc8dnja65n59n3w0hmlznjmasc")))

(define-public crate-cp211x_uart-0.1 (crate (name "cp211x_uart") (vers "0.1.3") (deps (list (crate-dep (name "hidapi") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "12d3gmr5677lym9dvf8dj4laiwy4djv2sqn8pdbzh2vx4k205k2b")))

(define-public crate-cp211x_uart-0.2 (crate (name "cp211x_uart") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hid") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "122jhyqsp7lsc36ns8afnc6jxvkbqrk77xsngwp6r0iihhgc7w40") (features (quote (("static" "hid/static") ("default") ("build" "static" "hid/build"))))))

(define-public crate-cp211x_uart-0.2 (crate (name "cp211x_uart") (vers "0.2.2") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hid") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0rgsyf7qhfdvsbh2q93mdhynly1haknlw84qvdplvd2dn1mzg7zd")))

