(define-module (crates-io cp ui) #:use-module (crates-io))

(define-public crate-cpuid-0.0.1 (crate (name "cpuid") (vers "0.0.1") (hash "0b8qhi7cqcbjla2wav0dqlq1cb1yx3bsj81vhixaj1gij5fpn8ps")))

(define-public crate-cpuid-0.0.2 (crate (name "cpuid") (vers "0.0.2") (hash "0m02fy8j0hp3sb9v63vjmm4rid09d0963harmbhlic6xw9ww8x7f")))

(define-public crate-cpuid-0.0.3 (crate (name "cpuid") (vers "0.0.3") (hash "1di75z9vw12pqmwivfmzm12kdiq4vhx9g86ahv1prp2axgyc9ci8")))

(define-public crate-cpuid-0.0.4 (crate (name "cpuid") (vers "0.0.4") (hash "1i65i5cq49qix77jwj42w0rycdjfa0435a5a4fsk543r9fbawp7n")))

(define-public crate-cpuid-0.0.5 (crate (name "cpuid") (vers "0.0.5") (hash "0q68r9lxqlbiq1idq70f3qxl86rcjanw2dxfxxc5jqsmn7m9p4n0")))

(define-public crate-cpuid-0.0.6 (crate (name "cpuid") (vers "0.0.6") (hash "1147zsi6l90rbbar58lqnn5qm0fw33fw9wfm8s1f0hyzxjnlkqsw")))

(define-public crate-cpuid-0.0.7 (crate (name "cpuid") (vers "0.0.7") (hash "0mdizczdzjfp7b90i8y74za6p1x261i90c8nc72bxa8vji77cqg8")))

(define-public crate-cpuid-0.0.8 (crate (name "cpuid") (vers "0.0.8") (hash "0g3gnr7l9fspkykg7vc15yqpm24c63c62zwrxifr8zypx4ncdkhq")))

(define-public crate-cpuid-0.0.9 (crate (name "cpuid") (vers "0.0.9") (hash "0pq5xb4l8mc9q95ym0pf88zd3qm6vl7r4wp97j1qvq5n4j654y28")))

(define-public crate-cpuid-0.1 (crate (name "cpuid") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1hz2fvixbbmmyarfwf4vp8y5arbpi8d855xhslsxxf0xy66b9m6c")))

(define-public crate-cpuid-0.1 (crate (name "cpuid") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "~0.2") (default-features #t) (kind 0)))) (hash "1afv7gyvj5qv2lp0jzmn25fmcs6hmb27305vpz56ikls45av1i50") (features (quote (("unstable"))))))

(define-public crate-cpuid-bool-0.0.0 (crate (name "cpuid-bool") (vers "0.0.0") (hash "0g4pl47b3jygqwfi1n7sg43d0lknps430rlvf6p6srk44rm4g6ch")))

(define-public crate-cpuid-bool-0.1 (crate (name "cpuid-bool") (vers "0.1.0") (hash "1r3v22cxly1shvw8qi0153708kggdqvh8jp0g82wbxi06d1mqdvd")))

(define-public crate-cpuid-bool-0.1 (crate (name "cpuid-bool") (vers "0.1.1") (hash "10r1mvbacgn5qq81zjyxdsgasclirksbdlaif330vaq10g166rzc")))

(define-public crate-cpuid-bool-0.1 (crate (name "cpuid-bool") (vers "0.1.2") (hash "0d16n378jl0n2dslspqxgsiw9frmjirdszhj5gfccgd0548wmswa")))

(define-public crate-cpuid-bool-0.2 (crate (name "cpuid-bool") (vers "0.2.0") (hash "1fpzag3g655p1lr08jgf5n89snjc2ycqx30mm0w3irc9fc3mvcnw")))

(define-public crate-cpuid-bool-0.99 (crate (name "cpuid-bool") (vers "0.99.99") (hash "0y7dkr70ahhqvzgfkf28m5q5lq2w449yj3krf85yliw5habd9bdn")))

(define-public crate-cpuid_dump_rs-0.1 (crate (name "cpuid_dump_rs") (vers "0.1.2") (deps (list (crate-dep (name "libcpuid_dump") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0qbzh1mwsvgfxb2wv8ry39sza2369jg6hba8cbva13rpsdlxy73g")))

(define-public crate-cpuinfo-0.1 (crate (name "cpuinfo") (vers "0.1.0") (hash "1m9cb71rzgv146xnkg25w106rsy87y3y5jxhcyii4vy4j1z4pgvd")))

(define-public crate-cpuinfo-0.1 (crate (name "cpuinfo") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "101cqa9dkxkp9raqz6kp6sfqn3nnwkhfil728n9qp9vyq9q8kdlx") (features (quote (("default"))))))

(define-public crate-cpuio-0.1 (crate (name "cpuio") (vers "0.1.0") (hash "0cznf1bizm3s3vxpah6bawy6pckqjbxz10cjvk02lgdxgsgab4vh")))

(define-public crate-cpuio-0.2 (crate (name "cpuio") (vers "0.2.0") (hash "1ald0q8745vcr8qa4rxcn9613xjc8jn0x77ph8xwynpwrh4f7f12")))

(define-public crate-cpuio-0.3 (crate (name "cpuio") (vers "0.3.0") (hash "19wam4sgwnkns63mqmm7hnv92ml0vr3p95k5z90j3486zd752cfm")))

