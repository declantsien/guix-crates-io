(define-module (crates-io cp lu) #:use-module (crates-io))

(define-public crate-cplus_demangle-0.1 (crate (name "cplus_demangle") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03j2vvgrz2qfqklsmjrj0q700sqyzmhyfdz4kkrx35vahdd5zclw")))

(define-public crate-cplus_demangle-0.1 (crate (name "cplus_demangle") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rmx40zqya4mq5ba5xw64msf5599hz4ymyrawbk0p76a0vl3b18s")))

(define-public crate-cplus_demangle-0.1 (crate (name "cplus_demangle") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "04gx9d919w1i4khvzj04929kvlbfiz429m2gl6y2i7gcqg1chi3x")))

