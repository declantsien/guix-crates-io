(define-module (crates-io cp io) #:use-module (crates-io))

(define-public crate-cpio-0.1 (crate (name "cpio") (vers "0.1.0") (hash "1wazn6y1wbq3qjdrwlhc88n04zgprkypslvribs4991nd0x78m99")))

(define-public crate-cpio-0.2 (crate (name "cpio") (vers "0.2.0") (hash "0hn2rph6c4l84r8hhhbazk6i2r3pbh90dddi0l55np5vl76pf0gh")))

(define-public crate-cpio-0.2 (crate (name "cpio") (vers "0.2.1") (hash "1a326qbhzk322b1vsr373v682h1alwil7sdxynawr98ds80fglgb")))

(define-public crate-cpio-0.2 (crate (name "cpio") (vers "0.2.2") (hash "1lv23p0v2f7kw1qqsw2wzpq6dbk46kaprjv2fs1v9vs38py7rrr7")))

(define-public crate-cpio-0.3 (crate (name "cpio") (vers "0.3.0") (hash "134h2m2x34y01dg54psic05k3pnnhp41wmjkmw1619fcwj1y1wb0")))

(define-public crate-cpio-0.4 (crate (name "cpio") (vers "0.4.0") (hash "19ilvk47k07wnk4cbvr3bzgqw62pf08kcqa694q69iwhfgnavqw0")))

(define-public crate-cpio-archive-0.1 (crate (name "cpio-archive") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tugger-file-manifest") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "15021rga2n79bsv4knm22dj94cxbs6zgzd69m2j3lr7371dld2py")))

(define-public crate-cpio-archive-0.2 (crate (name "cpio-archive") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tugger-file-manifest") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1aqaycvbdlik8azf8v411xnman78ml8218yk2gn763xg8c33504z")))

(define-public crate-cpio-archive-0.3 (crate (name "cpio-archive") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tugger-file-manifest") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1khjc0llbdqq8l51bh60gmjackk46f8f5kj51x7gvg51gbllqz84")))

(define-public crate-cpio-archive-0.4 (crate (name "cpio-archive") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tugger-file-manifest") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1h4cdbbnph1idrxn3fks14x4zkknwf38fj0nsgndcccra54gh96h")))

(define-public crate-cpio-archive-0.5 (crate (name "cpio-archive") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tugger-file-manifest") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1zpsnvfi4q1rbbprjg2c5flxf48bhb24p7g42j474gq9a5p3yba2")))

(define-public crate-cpio-archive-0.6 (crate (name "cpio-archive") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple-file-manifest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ngpk541jvv7ihdjynrfl5pliz0czbnzfmhrqz2q4yijm0f439zj")))

(define-public crate-cpio-archive-0.7 (crate (name "cpio-archive") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "simple-file-manifest") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1yn8jrz447wflxm756vlqdw9i2y81mzc57ms19r3qhlw8pwl9vyz")))

(define-public crate-cpio-archive-0.8 (crate (name "cpio-archive") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "simple-file-manifest") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0h6nhjgz6yxlxpr908vfmaby4p906wjxdia8b8ramhvpd5b24f9c")))

(define-public crate-cpio-archive-0.9 (crate (name "cpio-archive") (vers "0.9.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "simple-file-manifest") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0hdpnjw16zll757jwipi54k3wwxi1bdpsdknikd84gbdf4yi7mb3")))

(define-public crate-cpio_reader-0.1 (crate (name "cpio_reader") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "18fw8hqnv66csvicrk24clzqzw13rrmx3qz7jk20hnvcv190qhmx")))

(define-public crate-cpio_reader-0.1 (crate (name "cpio_reader") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0p7vbabpnylwg7mv0q0znfn0sfl68j8mxqdja44p1p3qajbbfpb2")))

(define-public crate-cpiotools-0.1 (crate (name "cpiotools") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "cpio") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "141gsadgrvf790d592bjn7281p7ldn26v9wrsbb6mdr2lpvwxrxw")))

