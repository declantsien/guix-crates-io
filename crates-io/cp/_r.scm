(define-module (crates-io cp _r) #:use-module (crates-io))

(define-public crate-cp_r-0.1 (crate (name "cp_r") (vers "0.1.0") (deps (list (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "1zx9m9y4xpa9zrv29jf9j9dlsdva9irjsq5wqcqkhc3p9rzva3fy")))

(define-public crate-cp_r-0.1 (crate (name "cp_r") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "00mb0v6k2p6m042zg5cm68rzr5i64c17cq7ikcs7z7abmqg6fvs8")))

(define-public crate-cp_r-0.2 (crate (name "cp_r") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "0vzi62fd0idwbwsmykjxls3v7sw611w6k1l93ic48yq6fqmpkl13")))

(define-public crate-cp_r-0.3 (crate (name "cp_r") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "1f67vhw12anj7ggb5khdlr2w9rq2laviwz3kshpf7p9371944g7h")))

(define-public crate-cp_r-0.3 (crate (name "cp_r") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "18k2gv196gfx6aw6j3idkf72mc61k8nj62qy3pq3l1cxjavmksx8")))

(define-public crate-cp_r-0.4 (crate (name "cp_r") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "18jyd95wh1bdpx45pbxi2qr1lk5cgk9g28spbdcfddjqs98drawz")))

(define-public crate-cp_r-0.5 (crate (name "cp_r") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "1i5h4g6a0f9zi1fx30l8dz0qfim13mrn6l2p5kb6iapj0bgs1dvs")))

(define-public crate-cp_r-0.5 (crate (name "cp_r") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "12p8ccdikz7a29645xsbfxz1j5bjhd35f3wmg4qjs78nzrg307gj")))

