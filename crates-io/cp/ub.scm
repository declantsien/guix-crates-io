(define-module (crates-io cp ub) #:use-module (crates-io))

(define-public crate-cpubaker-0.1 (crate (name "cpubaker") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)))) (hash "16dgfwjx2d2y9dr5vzy5d2kd3c4j2idhhysqsx78ij3igd2dsdqb")))

(define-public crate-cpubaker-0.1 (crate (name "cpubaker") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)))) (hash "1wymb8342nxaycb7chq3z9pic009xyl7z6qc84hyrz7sxb8gavfq")))

(define-public crate-cpubars-0.1 (crate (name "cpubars") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)))) (hash "1xq99qssl3an1m6i9ppf6l740p6nxq34rafq2a3jcwdzy90bm4py")))

(define-public crate-cpubars-0.1 (crate (name "cpubars") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)))) (hash "0h07v5rmi5f7bxmkhjf5vxksfhj8bmkl8r9ygbnhgvvx9ikmf3dy")))

(define-public crate-cpubars-0.2 (crate (name "cpubars") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0p2nl7yvmkm5q6mkcz9vrfx5wjzs10cv0zfvnk0hv7c5cyy8ffcn")))

(define-public crate-cpubars-0.2 (crate (name "cpubars") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1b345kss2lfh0d9xni1byhpgl4wqa9k0h6l3xlvp9bzyzrz7qnxa")))

(define-public crate-cpubars-0.3 (crate (name "cpubars") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "12y0xahmvlw1q8vdqmx6cj7hqb2ipp0dn41rr3kac41dd0478skb")))

(define-public crate-cpubars-0.3 (crate (name "cpubars") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1hppfnj4yag3xh5wlj7aih0qfbp8frzbrbccsyc12fnp338h1z1f")))

(define-public crate-cpubars-0.3 (crate (name "cpubars") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0cb8x7ls48f4b37pn9a0967x410h0vbjjfic00yrqiy064vckpfp")))

(define-public crate-cpubars-0.4 (crate (name "cpubars") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0fdlz7jbx5lml1a7kw304ycyq8rkx206ipk8hmp7gslpy3vk6rca")))

(define-public crate-cpubars-0.4 (crate (name "cpubars") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0ip220blcssmh77bzbxfa6rblk8qg79a5h6sk60g2g2gnfkd9d0m")))

(define-public crate-cpubars-0.4 (crate (name "cpubars") (vers "0.4.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0gza73vjpf2x2dkvws9ck5gy0b6iw61mm019lhdddvzd5kjrnvr1")))

(define-public crate-cpubars-0.4 (crate (name "cpubars") (vers "0.4.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "086yf84892c7cf5x4ppw50dydgan5gyvl0vqabx3py6zsjgjm0ys")))

(define-public crate-cpubars-0.5 (crate (name "cpubars") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "psutil") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "132193llkw1dcr2xf1w3x8iifjgf5vm28y0y5mk1fxbnk00iqnmg")))

(define-public crate-cpubind-bash-builtin-0.1 (crate (name "cpubind-bash-builtin") (vers "0.1.0") (deps (list (crate-dep (name "bash-builtins") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "core_affinity") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0kyf1rdrs2p6szdz2z8lnmyc1pxfc8yvxx569n9zcigwjwab3x1p")))

(define-public crate-cpubits-0.0.0 (crate (name "cpubits") (vers "0.0.0") (hash "08y2d6dmi774f4v69dwd308b8x6wjvmpsv9vabknfz4zjpsfg37z")))

(define-public crate-cpuburn-0.1 (crate (name "cpuburn") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)))) (hash "1k7v5z0lf4h9rknjhd7nrryn3k9ca0yb37br7ik22yh0wqh2n4m1")))

(define-public crate-cpuburn-0.1 (crate (name "cpuburn") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)))) (hash "1ckmxj2crzfl8crbmfwamrm4n765jj2mkaldzr54k74ic7wvhag4")))

