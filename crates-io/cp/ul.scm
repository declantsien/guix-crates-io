(define-module (crates-io cp ul) #:use-module (crates-io))

(define-public crate-cpulimit-0.1 (crate (name "cpulimit") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.15") (features (quote ("derive" "color" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "cpulimiter") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0rqm6c5ns77vph2dw617m7bxhrxk39ilzm9d30imxw94c8p54kj1")))

(define-public crate-cpulimit-0.2 (crate (name "cpulimit") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.15") (features (quote ("derive" "color" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "cpulimiter") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.2.2") (features (quote ("termination"))) (default-features #t) (kind 0)))) (hash "09w7srs7y551wj9q740q5hasl59nannsny8dwh0dwz4fr1805nw5")))

(define-public crate-cpulimiter-0.1 (crate (name "cpulimiter") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (kind 0)) (crate-dep (name "libc") (req "^0.2.125") (default-features #t) (kind 0)))) (hash "0h4f55wcx5ll7zqbnfhhhxnlpmaxyxznz1zvhc0bmyvy9k5yy3qx")))

(define-public crate-cpulimiter-0.1 (crate (name "cpulimiter") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (kind 0)) (crate-dep (name "libc") (req "^0.2.125") (default-features #t) (kind 0)))) (hash "0hz7xyzjwlggh3ndrf5zjszy061ggla5dzj9mm7wdch4cikixj8p")))

(define-public crate-cpulimiter-0.2 (crate (name "cpulimiter") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (kind 0)) (crate-dep (name "libc") (req "^0.2.125") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0ihvc9596lrs7xpxfcqjf6iicjxkn978znaw1rzgj89k9sr7rcli")))

