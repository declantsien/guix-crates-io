(define-module (crates-io cp re) #:use-module (crates-io))

(define-public crate-cprep-0.0.0 (crate (name "cprep") (vers "0.0.0") (hash "0qilal7j7xjgi8cnvgr199lcc16m2pw7fnp6rcivgj4m4s4b2prj")))

(define-public crate-cpreprocess-1 (crate (name "cpreprocess") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "12yxirjw3j8zqsb0ybkpdmz2svqa4279fjnrdqmzw6izh3br54n7")))

(define-public crate-cpreprocess-1 (crate (name "cpreprocess") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1hhq783zs1i6gza0imns3kl778h8hagkvm0algw1rrbljb95xqza")))

(define-public crate-cpreprocess-1 (crate (name "cpreprocess") (vers "1.0.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-faithful-display") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0z4gnzqzw56wvvvxz46wbjj5r7cla1pivpgqjvvapxgqd1rfyag5") (features (quote (("nightly" "proc-macro-faithful-display"))))))

