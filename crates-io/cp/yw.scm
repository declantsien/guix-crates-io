(define-module (crates-io cp yw) #:use-module (crates-io))

(define-public crate-cpywm-0.0.1 (crate (name "cpywm") (vers "0.0.1") (deps (list (crate-dep (name "pyo3") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18.1") (default-features #t) (kind 0)))) (hash "1cycl8mmxfnnz282hpjwg3l1y9r61pzzzvi103y0fm9wjsrmgfjr") (yanked #t)))

