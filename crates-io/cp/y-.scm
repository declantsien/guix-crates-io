(define-module (crates-io cp y-) #:use-module (crates-io))

(define-public crate-cpy-binder-0.1 (crate (name "cpy-binder") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "0yqvp2r7d4fhbxgn3j9rs5kpsa97zsr3qcbdmajp8cczjav85k1p") (features (quote (("python" "pyo3"))))))

(define-public crate-cpy-binder-0.2 (crate (name "cpy-binder") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "1v2953hd6b8z3jp2ncgjjbdp86qd54bh55v9anx49hr65g0aq0n9") (features (quote (("python" "pyo3"))))))

(define-public crate-cpy-binder-0.3 (crate (name "cpy-binder") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "0lzfj1plrh0kd55hj2r54fi92d535c926af9kgw7cvvmri4yv0zc") (features (quote (("python" "pyo3"))))))

(define-public crate-cpy-binder-1 (crate (name "cpy-binder") (vers "1.0.0") (deps (list (crate-dep (name "pyo3") (req "^0.18") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04d7453g4yhwdlc7snj9nrm9pz6fpfz295lrm64rva7qdpavqvw2") (features (quote (("python" "pyo3"))))))

