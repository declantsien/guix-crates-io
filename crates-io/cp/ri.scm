(define-module (crates-io cp ri) #:use-module (crates-io))

(define-public crate-cprint-0.1 (crate (name "cprint") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "047kv1fjmy2brgn50w4w35xvdk4h25ip2s9f94a6jdk62iam4klf")))

(define-public crate-cprint-0.2 (crate (name "cprint") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "14plpmzjdbnq3f1r82asvr3l8lii33dw9s208ysd2cpn2q9hjbs3")))

(define-public crate-cprint-0.3 (crate (name "cprint") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0hyrnijsj3q2znmmnhf0z5xm9psi6wmwxn6liynv66pq2skzryif") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.3 (crate (name "cprint") (vers "0.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "11k3i2flcp2ap22h79sq69rybssyn1kg14s3jzw242mx1i6vd9qk") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.4 (crate (name "cprint") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1c3izh5mki93f9f4x570w94dvjin43i1qnqfasxab6nacd1m33b3") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.4 (crate (name "cprint") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1rw1ffqwwrsn598nl6smbj3fxi24h2ldvx00j7xiqbvyk6g81jz4") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5 (crate (name "cprint") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1zrmrn4wizb3y1xhcjahixrzjcayddv1q1y93b3m6z9b117a3gza") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5 (crate (name "cprint") (vers "0.5.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0gav6bpb4186n62acmvscn6gflxw5isdi5ahqix826nw5wr179lx") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5 (crate (name "cprint") (vers "0.5.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "084b8vif6ppbxdk7lk1vykdcg3xrm44p1524kr4wnrlxk4sq9077") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-0.5 (crate (name "cprint") (vers "0.5.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1mffwwj6qbk2xysjric4y1d9yvyb1cdskn0qwijlcczb07z61l42") (features (quote (("default" "cprint") ("cprint" "coloration") ("coloration") ("ceprint" "coloration"))))))

(define-public crate-cprint-1 (crate (name "cprint") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1s7k2vmm38r37y5q8qccli0chyrw0nbdm451msyvpfbbpq14rgv4") (features (quote (("default" "cprint") ("cprint" "cformat") ("cformat") ("ceprint" "cformat"))))))

