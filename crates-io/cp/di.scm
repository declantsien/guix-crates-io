(define-module (crates-io cp di) #:use-module (crates-io))

(define-public crate-cpdir-0.1 (crate (name "cpdir") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "mktemp") (req "^0.5") (default-features #t) (kind 2)))) (hash "0xmi1ra450czqpcix2rlhrxgksbk2wb9ldnnfg6g40h028911kdh") (rust-version "1.68.2")))

