(define-module (crates-io cp le) #:use-module (crates-io))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.0") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "097h1gxs34yxzazaj2fb6jb5jxldkplzsfmpmlpavislks152i17")))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.1") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hdczp19agf0x2r62k1cbpk7lk7syjzh3sawgr5ydhbglw8hssvi")))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.2") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ip6s5jd5cp7c8y88vb7ix5zhrzjw91bnyysc6j5ssyzi59fqqn3")))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.3") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14hvvw98a78w1czc8py7y0wvm60pbwi52i32z296c8rn44b60dzn")))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.4") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "06mck8vfq84957jr43n9q2qmx2v8khgvrrzvsp521b4cnmy5y1np")))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.5") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gg0vbwzss3prc7rlvky0gjq75mz6hqlvjii8gsh65nznpsr4php")))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.6") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "03rizm0pn2s7mcqijs09ycbainlsixqyjzqh05xqmrnhj70r111x")))

(define-public crate-cplex-rs-0.1 (crate (name "cplex-rs") (vers "0.1.7") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cplex-rs-sys")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1iy9r1xm049bzy5pafnh6lypn75xmf79nzfqqrhqknlrk2g9wzfq")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "12m578mvqq16blsypxkfppf9d84ba1yj2xwqysl26ryw8jx5bz5c") (links "static=cplex")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "021h24kr5d9vny79vg7z7qxn7fxwzk6zz6d4m8j3fc0vmhidd8iw") (links "static=cplex")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "0n9spdcl5m1g5f4zi3yq3bzxiy92x8vzqcpld7z030xxiim58ypi") (links "static=cplex")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 1)))) (hash "1cnpzb10cb8bizxzd4666872hhbqnd2iapdkc4adan95wbpfnvv0") (links "static=cplex")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 1)))) (hash "00dlqcgsfhkyql05c82b87p3ha9m7rla6wl80j1pgc4y6kbllchd") (links "static=cplex")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 1)))) (hash "1qjdi2m5fsmhsf9m83vdy4f3801srpa0bqz6a6jgd7ahmg4x3fk7") (links "static=cplex")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 1)))) (hash "0j79pv4maz79pzssr3qy8q362jxag1icrcxr82bvshb89cym6zby") (links "static=cplex")))

(define-public crate-cplex-rs-sys-0.1 (crate (name "cplex-rs-sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 1)))) (hash "0k5habrnizsjp84j08nwnms3n1h2y5vr8z3r1nziihmnj9z9fl75") (links "static=cplex")))

(define-public crate-cplex-sys-0.2 (crate (name "cplex-sys") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "03vp9xms7grya6k483b88xl0k61477qp7jphx21wgwa6101wnssc")))

(define-public crate-cplex-sys-0.2 (crate (name "cplex-sys") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "14bs4441a8nvz41qyza4vn37hfyzjsgx86jps9j6qvrydh26bvg2")))

(define-public crate-cplex-sys-0.3 (crate (name "cplex-sys") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "03hirlri4gw2mxx0glx8bdfrxks6nfrdhk43v4526x845wzxq6zq")))

(define-public crate-cplex-sys-0.4 (crate (name "cplex-sys") (vers "0.4.0") (deps (list (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "1hd8vw8ah5p8z9cflr9dblnmaqyr3yjhg7z5h2jgc20hkl0rxjjv")))

(define-public crate-cplex-sys-0.4 (crate (name "cplex-sys") (vers "0.4.1") (deps (list (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "0k4qd4sb18spp6dqwmhcq0js8wihxx2dddmv2v315sgqwbvmprkn")))

(define-public crate-cplex-sys-0.5 (crate (name "cplex-sys") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "1cnsmnn2f11j7ad81lcyvhvsgpnc33bwn7d4j3192azkhyxy2224")))

(define-public crate-cplex-sys-0.5 (crate (name "cplex-sys") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "112kki4q38ml2c8jgnw7lz39ccnilqbdg5gsa8nln50nw0sda2f0")))

(define-public crate-cplex-sys-0.5 (crate (name "cplex-sys") (vers "0.5.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "137h395k6pcxz6xksn82snklzbbah4y19hf0063yb4pj1mbnjj9n")))

(define-public crate-cplex-sys-0.6 (crate (name "cplex-sys") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "03lslfirqcqfg8sxgq654sqf8nb2zy7j34z8nlyd29bmiff3kqsh")))

(define-public crate-cplex-sys-0.6 (crate (name "cplex-sys") (vers "0.6.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "0fjggp42lfnixnh5ghdlcavkg9kg0mav7gv3ivmc6m1splm5an6w")))

(define-public crate-cplex-sys-0.7 (crate (name "cplex-sys") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "1lsv570r16g2nsd9pvvmal9w3bg7p19jpx1fzdi12fi4kaxra7ji")))

(define-public crate-cplex-sys-0.8 (crate (name "cplex-sys") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "1aw5r9zkfgmwhghniz6anf54w8f5xs8hzzyxfs4907n3qfadhdfr")))

(define-public crate-cplex-sys-0.8 (crate (name "cplex-sys") (vers "0.8.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "05ph86n1b2vlbfpa8zr27n4f4acd4vq3f2xp39al8xr5l546x2c3")))

(define-public crate-cplex-sys-0.9 (crate (name "cplex-sys") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "1bvrh5kh9v03f520k3rk8qizq0is98lqb0phahsl28nc1x50x449")))

(define-public crate-cplex-sys-0.9 (crate (name "cplex-sys") (vers "0.9.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "127982h79mffa4h23wsr4bn5b3jfsc053i43cqzzvir0pxa7h34v")))

(define-public crate-cplex_dynamic-0.1 (crate (name "cplex_dynamic") (vers "0.1.0") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wdhml8h9kgrqvhh8pkp6brbrdcd1lbfmzaxnlxwf62d438g0pi7")))

(define-public crate-cplex_dynamic-0.1 (crate (name "cplex_dynamic") (vers "0.1.1") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16xpqix7iya3ywz3s1c5b64cbnn8ws7c9c2qk3rhachfdvy0w37r")))

