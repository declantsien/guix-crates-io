(define-module (crates-io cp ur) #:use-module (crates-io))

(define-public crate-cpurender-0.1 (crate (name "cpurender") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.9.9") (features (quote ("repr_simd"))) (default-features #t) (kind 0)))) (hash "0aq4qly73f7rhwi47b9q7g7nljyks0p3w77hq0s7ik0h3p5lgq29")))

