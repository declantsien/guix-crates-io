(define-module (crates-io cp wd) #:use-module (crates-io))

(define-public crate-cpwd-1 (crate (name "cpwd") (vers "1.0.0") (deps (list (crate-dep (name "clipboard2") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07gmpwjkkqlbzlccj5g216vwf16sq58cbpdb0y8fplg352195jiy")))

(define-public crate-cpwd-1 (crate (name "cpwd") (vers "1.0.1") (deps (list (crate-dep (name "clipboard2") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1spqmlf2kr3y7k7kryqjrf612rlrf35rmyjkg0lypcrjz08frkqj")))

