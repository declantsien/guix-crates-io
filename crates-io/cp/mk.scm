(define-module (crates-io cp mk) #:use-module (crates-io))

(define-public crate-cpmk-1 (crate (name "cpmk") (vers "1.0.0") (hash "1xh470p75qg8b3klwr3a8iqnbqrg1cj040cphxbkdfvck73z2n9n")))

(define-public crate-cpmk-1 (crate (name "cpmk") (vers "1.0.1") (hash "1sx65qzb41na3ials8vcr92fj7ssql4b3c3ingdh0rygkk39asxs")))

(define-public crate-cpmk-1 (crate (name "cpmk") (vers "1.0.2") (hash "1s70rcgigjs0kgg7xpw1klb4zywjaabwhzy3d44v38jrjj3sjqxx")))

(define-public crate-cpmk-1 (crate (name "cpmk") (vers "1.0.3") (hash "1an8wp556faxw1mq9nmd5bbmbcq6wyr2c2ifbr0d719r3sc4jhsj")))

(define-public crate-cpmk-2 (crate (name "cpmk") (vers "2.0.0") (hash "0bkgwm2m5njnv5gimb79c3palvv547ivgw7csplmaxcjlyiy3wqj")))

(define-public crate-cpmk-2 (crate (name "cpmk") (vers "2.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1b3myykqs2vd283f4mdplp34r5dph1m4wyfmy62wwi2gi1aimbs4")))

(define-public crate-cpmk-2 (crate (name "cpmk") (vers "2.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0d3bm9mwnaz6vzwl379xmbmimc90ifffv0d5xpwc9hl6h0az59a8")))

(define-public crate-cpmk-2 (crate (name "cpmk") (vers "2.1.4") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lgmjn9kqjar4iq97lwsg5zx1lib6s7cmidjysb28synbn91ka80")))

