(define-module (crates-io cp uc) #:use-module (crates-io))

(define-public crate-cpucycles-0.1 (crate (name "cpucycles") (vers "0.1.0") (deps (list (crate-dep (name "ffi") (req "^0.1") (default-features #t) (kind 0) (package "cpucycles-sys")) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d19bdf6yg3jqsfq3zxv2551qnn9w1rqql1wfvfk8pdggvf23sz1")))

(define-public crate-cpucycles-0.1 (crate (name "cpucycles") (vers "0.1.1") (deps (list (crate-dep (name "ffi") (req "^0.1.1") (default-features #t) (kind 0) (package "cpucycles-sys")) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h7qs5lv1a59wmf7n12i8qj2bk3whwrkj8daqpwv0nxkfydv7kpy")))

(define-public crate-cpucycles-0.2 (crate (name "cpucycles") (vers "0.2.0") (deps (list (crate-dep (name "ffi") (req "^0.1.2") (default-features #t) (kind 0) (package "cpucycles-sys")) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)))) (hash "07m2f5a8x19z9885mlzn0l6na64bxjaracxy452y4x5zgazl43ib")))

(define-public crate-cpucycles-sys-0.1 (crate (name "cpucycles-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1gafw324pvb6wha9whdfcj1zrv2vg51lamvpqzpn5689wq274jkv")))

(define-public crate-cpucycles-sys-0.1 (crate (name "cpucycles-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1") (default-features #t) (kind 1)))) (hash "0wxi2mcsk1zcfd6jvvna01a35iffymz5kaw4rlsc857j8vlbcm19")))

(define-public crate-cpucycles-sys-0.1 (crate (name "cpucycles-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1") (default-features #t) (kind 1)))) (hash "185hahrvjhv8iii27laddicw572d4gjrk8bkm61ss66fqgj81biz")))

