(define-module (crates-io cp la) #:use-module (crates-io))

(define-public crate-cplat-0.1 (crate (name "cplat") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1hms4wrfxjdsz61w64n6lwyax4lnlpc66gfpv6q7hlqfhxvcar0g")))

