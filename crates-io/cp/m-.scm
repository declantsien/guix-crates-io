(define-module (crates-io cp m-) #:use-module (crates-io))

(define-public crate-cpm-rs-0.1 (crate (name "cpm-rs") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1bphd7l7xpar8w1rrf9ad18j2fmzv3ck534kf1wkbyqryr9935qx")))

(define-public crate-cpm-rs-0.1 (crate (name "cpm-rs") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1hl85d4lh5dvyx41gk6d2dm3vn395dmjw53ya4x6xq86mlyi9xa3")))

(define-public crate-cpm-rs-0.1 (crate (name "cpm-rs") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0l8cj900x9lf7fz574vrc2zmanqv8msi66dblygwslkq1wr34b5f")))

(define-public crate-cpm-rs-0.1 (crate (name "cpm-rs") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1hi2afx52pdmd57qd3aqrc3w6lgll9lyryr81jg7pr4lvpmcfsva")))

(define-public crate-cpm-rs-0.1 (crate (name "cpm-rs") (vers "0.1.5") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1l3an11m8msw70mmw4839bn8i6nd6wy875nydxkjsmc3bd2axnh1")))

(define-public crate-cpm-rs-0.1 (crate (name "cpm-rs") (vers "0.1.6") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "08v3ya615mcnd2zbkdi6xh752hmkxw9idmypwcw7ccxibdihy1fb")))

