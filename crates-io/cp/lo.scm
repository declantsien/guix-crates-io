(define-module (crates-io cp lo) #:use-module (crates-io))

(define-public crate-cplogger-0.0.0 (crate (name "cplogger") (vers "0.0.0") (hash "071ap0brx5czb9rhmsc3z1xy5v5h3g3syrlrwf0rq974gqcq4x3d") (yanked #t)))

(define-public crate-cplogger-0.0.1 (crate (name "cplogger") (vers "0.0.1") (hash "0mv8msa7n9flg1hifgp7s2hjrk90b0zbkpcybbidr470msn44vyi") (yanked #t)))

(define-public crate-cplogger-0.0.2 (crate (name "cplogger") (vers "0.0.2") (hash "0anxzz74869sfwa6gadx8pgdm2wp9l4w65ckp4y9hq3hwyqiwnf5")))

