(define-module (crates-io cp ne) #:use-module (crates-io))

(define-public crate-cpnets-1 (crate (name "cpnets") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports" "cargo_bench_support"))) (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.0") (features (quote ("abi3" "auto-initialize"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "09c7g12hgfmxvb39g23big5hhx3pijzw5rfmch0fsj1zpff0yvrs")))

(define-public crate-cpnets-1 (crate (name "cpnets") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports" "cargo_bench_support"))) (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.0") (features (quote ("abi3" "auto-initialize"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "01z2k3w5y2ggn87nwjfm09varmrx7dcmhdlchz080d0cjxdlvcwk")))

