(define-module (crates-io cp li) #:use-module (crates-io))

(define-public crate-cpli-0.0.0 (crate (name "cpli") (vers "0.0.0") (hash "0jz6d4c9094xnpm37m26yzy1smi2clxz8kip5vj04lvdis44sbx6")))

(define-public crate-cplib-0.1 (crate (name "cplib") (vers "0.1.0") (hash "0bn46jahzmc3nssj34mrhdq17rygzbrbvij0d8h9f57rwasjlg89")))

