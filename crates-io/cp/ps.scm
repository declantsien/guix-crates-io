(define-module (crates-io cp ps) #:use-module (crates-io))

(define-public crate-cppStream-0.0.1 (crate (name "cppStream") (vers "0.0.1") (hash "0rybl04c287hhi1nr2r64gx1nvhckwrd7nn5q6bfd8gds6xxi43j") (yanked #t)))

(define-public crate-cppStream-0.0.2 (crate (name "cppStream") (vers "0.0.2") (hash "0c949iqghda2af43klandzqwlyjydbpx94jc1c1frcwh96c25vss")))

(define-public crate-cppStream-0.0.3 (crate (name "cppStream") (vers "0.0.3") (hash "0x099iwgdnjgz6125im298409nb7cja9py4rf8ir664yh6yzb7gp")))

(define-public crate-cppStream-0.0.4 (crate (name "cppStream") (vers "0.0.4") (hash "05hm8r32brw5n34v48nvxqlsirmypzsfmabfky56mhw9w90jbhsy")))

(define-public crate-cppStream-0.0.5 (crate (name "cppStream") (vers "0.0.5") (hash "15zrr5v2qbnk3y1jvjxssmw8f5zbvkk28b5fl9zns189wgmyn6sf")))

(define-public crate-cppStream-0.1 (crate (name "cppStream") (vers "0.1.0") (hash "18d87x38lq08nf5fgyj6y3vy12zdbmr8ii8j9h5rdcwbldf16sr0")))

(define-public crate-cppStream-0.1 (crate (name "cppStream") (vers "0.1.1") (hash "0fr0gyjnf1pbz12s93415zk07rpcmvh03zhfby3brcc0cv6dlm12")))

