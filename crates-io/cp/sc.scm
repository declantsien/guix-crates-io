(define-module (crates-io cp sc) #:use-module (crates-io))

(define-public crate-cpsc323-lexer-0.1 (crate (name "cpsc323-lexer") (vers "0.1.0") (hash "0kgc8a8rd0nc05hhsnllnxdr7nlrgczjgbaf6w3zp76h7f3ip22l")))

(define-public crate-cpsc323-parser-0.1 (crate (name "cpsc323-parser") (vers "0.1.0") (deps (list (crate-dep (name "tabled") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0dcj01g1sskdsz2xna35sp1fz5bz39nlz7z5xlm9pigipjyp5alx")))

