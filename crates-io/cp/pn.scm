(define-module (crates-io cp pn) #:use-module (crates-io))

(define-public crate-cppn-0.1 (crate (name "cppn") (vers "0.1.1") (deps (list (crate-dep (name "acyclic-network") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1fgpbbkh6w1kxs9bhq94adni434cbiawv1qr6fvj7krsj8qjkd8y")))

(define-public crate-cppn-0.1 (crate (name "cppn") (vers "0.1.2") (deps (list (crate-dep (name "acyclic-network") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ak6xpp1ixaxjs7ak8945jgmgcxgwkrxgmrwf2bsk28j2bl6g480")))

(define-public crate-cppn-0.2 (crate (name "cppn") (vers "0.2.0") (deps (list (crate-dep (name "acyclic-network") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bwpzc1hpc16d50wlrj4yx7l27qzsql0rgflqndxy2ql4bffkdxq")))

(define-public crate-cppn-0.3 (crate (name "cppn") (vers "0.3.0") (deps (list (crate-dep (name "acyclic-network") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0glxm67yifhx1llpkg7girs94m0ixxgazk9i1cpxh95xwbpycrfl")))

