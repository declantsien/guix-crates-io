(define-module (crates-io cp pv) #:use-module (crates-io))

(define-public crate-cppvtbl-0.1 (crate (name "cppvtbl") (vers "0.1.0") (deps (list (crate-dep (name "cppvtbl-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0yxrxbdr3nwa0il034annm3zm3sh8gabnrg3lyq2kbwrjq5dz87z") (features (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.1 (crate (name "cppvtbl") (vers "0.1.1") (deps (list (crate-dep (name "cppvtbl-macros") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "05wapfw6awqqmj2vy5lwi569nf5sz4bza8a8n30nj3ggcysnjyr6") (features (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.1 (crate (name "cppvtbl") (vers "0.1.2") (deps (list (crate-dep (name "cppvtbl-macros") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "02xxjgcrndjd57137mly3qavlfzrj17q21qcgdmac1s4vag4z3gc") (features (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.2 (crate (name "cppvtbl") (vers "0.2.0") (deps (list (crate-dep (name "cppvtbl-macros") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "06f756h21fk4sjsaxdx0nmqnjrc4j1m0f7i19qfd41wq6lzqnk2p") (features (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-0.2 (crate (name "cppvtbl") (vers "0.2.1") (deps (list (crate-dep (name "cppvtbl-macros") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "01hrfwb9009gfkhn27g1270v8s084cbd6fj3wsm669nf38fcwc0p") (features (quote (("macros" "cppvtbl-macros") ("default" "macros"))))))

(define-public crate-cppvtbl-macros-0.1 (crate (name "cppvtbl-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dvsx09bb5j7dw0bwzxvkin9gi9xi5wjn97c8dkism2sjkhfzm4d")))

(define-public crate-cppvtbl-macros-0.1 (crate (name "cppvtbl-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mbxnmazzxa3zasgi15qvmbbrhsfnlik2h6g3f1l4h51sbzbqkc6")))

(define-public crate-cppvtbl-macros-0.2 (crate (name "cppvtbl-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1facxminkr3xaq6rr0kg21ss80nx4axzsn76bnnbndsv3z4riayi")))

(define-public crate-cppvtbl-macros-0.2 (crate (name "cppvtbl-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "019g2ywp29585a9170flsyahcb6z8fiyj7m652d25fsp9j9il205")))

