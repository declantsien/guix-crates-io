(define-module (crates-io cp up) #:use-module (crates-io))

(define-public crate-cpuprofiler-0.0.1 (crate (name "cpuprofiler") (vers "0.0.1") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1hdvcbrkbvjggq4w9sl41yzzhq75svk2nzipll3f6rn6hihmclgp")))

(define-public crate-cpuprofiler-0.0.2 (crate (name "cpuprofiler") (vers "0.0.2") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0120828c25zlh490nfyiwslh446kkcfja07zls8vw83558gi4ldk")))

(define-public crate-cpuprofiler-0.0.3 (crate (name "cpuprofiler") (vers "0.0.3") (deps (list (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0dbh3lf7ihf74bw53wjjqnsmq06arjbqv8fp6ab4a8b8pdv7kw1k")))

(define-public crate-cpuprofiler-0.0.4 (crate (name "cpuprofiler") (vers "0.0.4") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0az588yyl9r13w4k7xfdh5ckfaq52fwpjry2q2hblazxpjflgy23")))

(define-public crate-cpuprofiler-static-0.1 (crate (name "cpuprofiler-static") (vers "0.1.0") (deps (list (crate-dep (name "cpuprofiler") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)))) (hash "0dclmricbbmmbdpffaahm8k5nfcjgpj4kl6hcs3bvgbr1ic18wh1") (features (quote (("unwind") ("gperftools") ("default"))))))

