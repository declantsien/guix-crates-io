(define-module (crates-io cp ca) #:use-module (crates-io))

(define-public crate-cpcalendars-0.1 (crate (name "cpcalendars") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0cnpi04mnlnv0yppyhbq0a6p4a4g20ccgmgwrhlxawhw4dikilwl")))

(define-public crate-cpcalendars-0.1 (crate (name "cpcalendars") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1sqspasfrkdnqh65455imj0vglfxa01bphqjb8kq6b55p0d4330c")))

