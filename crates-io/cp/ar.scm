(define-module (crates-io cp ar) #:use-module (crates-io))

(define-public crate-cpar-0.1 (crate (name "cpar") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0l6x344n5q351jwswnl6vsn8h374x63i81c00i98zc5iw89k8lvp")))

(define-public crate-cpar-0.1 (crate (name "cpar") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1zxx85z3wggghhldwid09fqli7wkrrawhnj7glyw6zssnbdgf8xc")))

