(define-module (crates-io cp -r) #:use-module (crates-io))

(define-public crate-cp-rs-0.1 (crate (name "cp-rs") (vers "0.1.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hxfb0bqanmpziy7r3f5cyp9p2k05wiyf3y1qck2rz5hp9fh5abq")))

(define-public crate-cp-rs-0.2 (crate (name "cp-rs") (vers "0.2.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "160vdalmsrnz895v87w89z561jjlwc3bkr8n2z18pgw016nz145v")))

(define-public crate-cp-rs-0.2 (crate (name "cp-rs") (vers "0.2.1") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "085l4awm46asb3gnc0x1zbx6q6hkbq0a7jbx4kgs005rzvk7qzfs")))

(define-public crate-cp-rs-0.2 (crate (name "cp-rs") (vers "0.2.2") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12ib7mplyhiaj55drng67na7rsr3lwmqdakbha6i532z672js84b")))

(define-public crate-cp-rs-0.2 (crate (name "cp-rs") (vers "0.2.3") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0qz7byy434hhcxqxcyqggb4y6blhxk3jh4p03j0mvxiizg40qicn")))

