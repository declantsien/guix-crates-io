(define-module (crates-io cp pr) #:use-module (crates-io))

(define-public crate-cppr-0.1 (crate (name "cppr") (vers "0.1.0") (hash "07acjqnvp2vlvmay9dhhk3cj9vdlwc9rgldjm4z8zqy88j64qwpn")))

(define-public crate-cppr-0.1 (crate (name "cppr") (vers "0.1.1") (hash "1wifjrf5qrlzpiaj0il6745x1l1m4kd5r4nqrg7gvi4r938dwhid")))

(define-public crate-cppr-0.1 (crate (name "cppr") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0wgz6r3q03nzl3bdwvzbwns9ar0ivmkz1cy1rf9v45g8kzhrfiig")))

(define-public crate-cpprs-0.1 (crate (name "cpprs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0402ljs0cdl8223w88c1bpk87w9j6xbiychfl0g8rbqmiwp36k15")))

