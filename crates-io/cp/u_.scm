(define-module (crates-io cp u_) #:use-module (crates-io))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0j7rfwkz8j7w52s5rqs1wrc1ar0lai889svpkdq819xr62n15z0l")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1mv812aq63cdvhxprflb32gkjdxk0ik41syhv12czg2q476skzgj")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.11") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0ryjk8s4bd2ba5znf8f5b0dfng920bbxb946iwcc1zv1b920c6dh")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.12") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1vbjiv8ba06mb6kcxlvvxxdnqqfr0b8plk2fg4vifzgdfrkw53fj")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.13") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "020wqq6lyjkizv3h02fa82gnhgb70s1hwpkva9xprgqlja6wgzpl")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.14") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0cq15yk59an73cgq5mp8xwr2l7hmvn28vmh08bmkyb3pnjcwl64d")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.15") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "07qs280yffjl38yw63zp19d6zknqxn0pf8yzcis1b8lf66phpcni")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.16") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0d9lcniki9wcx09756p2l92zgxbsr0dgz9di9bpb70dd8nk82vnq")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.17") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "18pyzlas9qs1h1i5y80hj5hkgdq8qsq4nk2iahngbgciyz5cblh1")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.18") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1kinzbz5qklyqws3a97svaznk6gdvjsc5qvmhfqxnkl6wx9zkf25")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.20") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "04pfry5q40cbx7mmgdwphc3zkz1364i455dbbzrxnms16yckxsjj")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "17v9xdqcpfg0a9cfqnxqqnx8zkrmi02lk82r8ysfs1l5h1zgvx8v")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.30") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "00ssldb0ncypqq1l2s8dp7kbiczsnkmklp8h04d6s3y3aaqjc4s2")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.31") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "02hy90fi33gr64q7yx4w2licgxk8k3wqf222kds5qri0fp6ars2z")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "088dw6ri7jfdc2mnr08c6s8xcgac3qz1740wifl3hc9ydgqkikj9")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.40") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "00am07165w9g09d3qv6zp6jib8vq9f8hx8x26mgx8lfq5gf4m702")))

(define-public crate-cpu_cycles_reader-0.1 (crate (name "cpu_cycles_reader") (vers "0.1.41") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0vwly3vs8jgsc2yrh3lyxx6r3afcd16jj9gkskxgak5pdqp7zpzr")))

(define-public crate-cpu_cycles_reader-0.2 (crate (name "cpu_cycles_reader") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0xa3rqw2f95pfghp35v6vm8bya2zanaiax8hsnjsxb6jk49nxn28")))

(define-public crate-cpu_cycles_reader-1 (crate (name "cpu_cycles_reader") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1mjx6jf72ib3bkjpnw33ld40cvm2df4rr1vw095z1byiwqxf7ngj")))

(define-public crate-cpu_cycles_reader-1 (crate (name "cpu_cycles_reader") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1vkzcn20apkv8jn5m553m8xpzkzxilzl7h6x1z3lyy9j9xvmhjkk")))

