(define-module (crates-io cp tc) #:use-module (crates-io))

(define-public crate-cptc-0.1 (crate (name "cptc") (vers "0.1.0") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (features (quote ("wayland-data-control"))) (default-features #t) (kind 0)))) (hash "1a339pw3vhz09hyzbq5093c2vx3m5km21khalsaypgsr8rp8bmf4")))

(define-public crate-cptc-0.1 (crate (name "cptc") (vers "0.1.1") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (features (quote ("wayland-data-control"))) (default-features #t) (kind 0)))) (hash "0zax6nij61fzgai6nzb5szjkydcqplgrvdxcskk0fh57yg51fix7")))

(define-public crate-cptc-0.1 (crate (name "cptc") (vers "0.1.2") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (features (quote ("wayland-data-control"))) (default-features #t) (kind 0)))) (hash "06mg9z8vdpv932ydnfwxbzvv62gln6540lxj1k0zpgksb2fksd14")))

(define-public crate-cptc-0.1 (crate (name "cptc") (vers "0.1.3") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (features (quote ("wayland-data-control"))) (default-features #t) (kind 0)))) (hash "1971756bflgn11v532j1qb0kghg6a7kzlbibiyp6sdcrzhy4bifg")))

(define-public crate-cptc-0.1 (crate (name "cptc") (vers "0.1.4") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (features (quote ("wayland-data-control"))) (default-features #t) (kind 0)))) (hash "1plhcc8dp54l67nhf3bh2mm5gbvy9dwcmjvxm1j7dxpmavmdq3k6")))

