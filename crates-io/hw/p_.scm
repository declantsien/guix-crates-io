(define-module (crates-io hw p_) #:use-module (crates-io))

(define-public crate-hwp_macro-0.1 (crate (name "hwp_macro") (vers "0.1.0") (hash "00mf3dw9j3f4q89fnd6b50g7gh47wj1pgcsi8lwca5hcjpa57190")))

(define-public crate-hwp_macro-0.2 (crate (name "hwp_macro") (vers "0.2.0") (hash "08mgg54031d91cc0s7y0l3lxs1cym6l95acjpqb4mmdxilppkm0f")))

