(define-module (crates-io hw cl) #:use-module (crates-io))

(define-public crate-hwclock-0.2 (crate (name "hwclock") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1rqbhzl9gn9krf7cq3a43g3sklv9hzr13ih77zs63r4sm6z9lzm8")))

