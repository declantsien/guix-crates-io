(define-module (crates-io hw io) #:use-module (crates-io))

(define-public crate-hwio-types-1 (crate (name "hwio-types") (vers "1.0.0") (deps (list (crate-dep (name "measurements") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "1qwvjmxinsn3v3fjrjgi4mcxhiblcfww41kr12jf6qkfhdc6m4iq")))

(define-public crate-hwio-types-1 (crate (name "hwio-types") (vers "1.0.1") (deps (list (crate-dep (name "measurements") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "1hin0r9nhh5ahjqjx1z456116n0ckd1fwc47y8av8faw67vp7sl0")))

