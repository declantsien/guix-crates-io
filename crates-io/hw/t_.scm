(define-module (crates-io hw t_) #:use-module (crates-io))

(define-public crate-hwt_ui-0.1 (crate (name "hwt_ui") (vers "0.1.0") (deps (list (crate-dep (name "druid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "1d5l87wn487as3y092vc755bfw9zq2w3qwxhqmcbnhwgma3l5vqz")))

(define-public crate-hwt_ui-0.2 (crate (name "hwt_ui") (vers "0.2.0") (deps (list (crate-dep (name "druid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1nsrmmpq5r9qwwldmzb7b2vx4xy22vm09y3fglig2w71zzrvbriv")))

