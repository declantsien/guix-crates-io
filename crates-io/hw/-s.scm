(define-module (crates-io hw -s) #:use-module (crates-io))

(define-public crate-hw-skymodel-0.1 (crate (name "hw-skymodel") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "include_bytes_aligned") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "03319b5aaxpgm75y54a603gn7i5m0z1wkspvjaq7vx96pz0nls4x")))

(define-public crate-hw-skymodel-0.1 (crate (name "hw-skymodel") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "include_bytes_aligned") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "13jdfrgixm24kv1bsshayavqxm7qxl83dws3gbnic04mg0dc4g84")))

