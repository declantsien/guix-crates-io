(define-module (crates-io hw -m) #:use-module (crates-io))

(define-public crate-hw-msg-0.1 (crate (name "hw-msg") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "08fivv9zrz3hz2bgp4kq2y263r47rvgqn1z05y5md5659gpykbar")))

(define-public crate-hw-msg-0.2 (crate (name "hw-msg") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1cdyry4r8q5i0zrk2fwvpmqvwqww7w26xy6pzrx40f26phnzw8h3")))

(define-public crate-hw-msg-0.3 (crate (name "hw-msg") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "11ffaz39x198akwkqlxyvnwbpapr7mbqana688hbhcrsdi00kkdy")))

(define-public crate-hw-msg-0.3 (crate (name "hw-msg") (vers "0.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0q0n9hvw5idc8wgkc3iam919mpx280ir1dl91k62is577b3s23r3")))

(define-public crate-hw-msg-0.4 (crate (name "hw-msg") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "1jbbhgx0d1ykpswa754klym4rblsy477jcx01b3yn3xffbd8qwrl") (features (quote (("logging")))) (v 2) (features2 (quote (("default" "dep:log"))))))

(define-public crate-hw-msg-0.4 (crate (name "hw-msg") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "1m58k9hjr38xiy1b8j9783ndvdzd2nzvi7as7x2fcbnh515cndc5") (features (quote (("logging")))) (v 2) (features2 (quote (("default" "dep:log"))))))

