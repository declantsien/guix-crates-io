(define-module (crates-io hw id) #:use-module (crates-io))

(define-public crate-hwid-0.0.1 (crate (name "hwid") (vers "0.0.1") (hash "1108m84x0q5zc9p9xbbn8p7anlcm90c6588vclhmlkx2bpj4j4yb")))

(define-public crate-hwid_get_current_rs-0.0.0 (crate (name "hwid_get_current_rs") (vers "0.0.0") (hash "05lpfjdslbn3yfsjr9ayvg7risxqqgmnfqx8p23x200497d8wcw0")))

