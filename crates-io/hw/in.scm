(define-module (crates-io hw in) #:use-module (crates-io))

(define-public crate-hwinfo-0.0.1 (crate (name "hwinfo") (vers "0.0.1") (deps (list (crate-dep (name "CoreFoundation-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "IOKit-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dmidecode") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.0") (default-features #t) (kind 0)))) (hash "1n4jydx1xfsxkfd43z8db1l3bfjdxxq7r6cfpwq1s8yr0b5k50kx")))

