(define-module (crates-io hw -e) #:use-module (crates-io))

(define-public crate-hw-exception-0.1 (crate (name "hw-exception") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (features (quote ("extra_traits"))) (default-features #t) (kind 0)))) (hash "00hbz5yv2lhq5agg07pirpsks8lx9mzlh2hsflrvcih7vx6dqv5y")))

