(define-module (crates-io dc sc) #:use-module (crates-io))

(define-public crate-dcsctp-0.0.0 (crate (name "dcsctp") (vers "0.0.0") (hash "073786ilykksj34c09mjylrl9a88wndlvvylfl9fqlgmwqwqy39b")))

(define-public crate-dcsctp-proto-0.0.0 (crate (name "dcsctp-proto") (vers "0.0.0") (hash "1xiwyy8l7ma30n9sfb9yiwwjdmb13zn6wwk20v3z8ghl2plj950a")))

