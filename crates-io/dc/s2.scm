(define-module (crates-io dc s2) #:use-module (crates-io))

(define-public crate-dcs2-0.1 (crate (name "dcs2") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.11.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.16") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (kind 0)))) (hash "1cdvnyj42qizsy0a2q79gigr7pxc23mbkikifzr8vhmdk9b8c063")))

(define-public crate-dcs2-clock-0.1 (crate (name "dcs2-clock") (vers "0.1.0") (deps (list (crate-dep (name "dcs") (req "^0.1.0") (default-features #t) (kind 0) (package "dcs2")))) (hash "1mlkccgqbhcm96arr76fm1gm3bslh9y3f065kragx33cw46inc03")))

(define-public crate-dcs2-raft-0.1 (crate (name "dcs2-raft") (vers "0.1.0") (deps (list (crate-dep (name "clock") (req "^0.1.0") (default-features #t) (kind 0) (package "dcs2-clock")) (crate-dep (name "dcs") (req "^0.1.0") (default-features #t) (kind 0) (package "dcs2")) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00bfl62w2wgiwzxlsjgc79n0bwddx7aidgwf4jq133j8044s30qz")))

