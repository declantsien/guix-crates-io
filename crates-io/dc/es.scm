(define-module (crates-io dc es) #:use-module (crates-io))

(define-public crate-dces-0.1 (crate (name "dces") (vers "0.1.0") (hash "02jh38m3zlgvrhi5jx82yjvy8hdvki11748kigz89kq7sr706s42")))

(define-public crate-dces-0.1 (crate (name "dces") (vers "0.1.1") (hash "1fymfn2iqv9nf3kjzvyz7418zq0zw2gch7bh2zkwsf7flary0a1y")))

(define-public crate-dces-0.1 (crate (name "dces") (vers "0.1.2") (hash "0nkm3w0c3ny94qzw2i1qvjxgxpbc27ghmkahwxyr34lxqj7r9k8r")))

(define-public crate-dces-0.1 (crate (name "dces") (vers "0.1.3") (hash "07allwan1mg5xqycx5pyph5rcsvhfnn55inz3ndl31akaym7ymg9")))

(define-public crate-dces-0.1 (crate (name "dces") (vers "0.1.4") (hash "0x4mnvcf5bhflpfqppk355yjwikczgss0bifn35r42z4xa1844j4")))

(define-public crate-dces-0.1 (crate (name "dces") (vers "0.1.5") (hash "015yzb23nh9zwb3xqni9pfddqqw1wmcna7bjwka2iv1mjm9lzrz0")))

(define-public crate-dces-0.1 (crate (name "dces") (vers "0.1.6") (hash "0xzjphvz9imc8b18707gkrgxxrf5krvj8fmm6zp31rkzy47gpxsc")))

(define-public crate-dces-0.2 (crate (name "dces") (vers "0.2.0") (hash "01nqapifv0vx8r58xngk7saync7hyi6lxzx12qf0cz0zrr4c5626") (features (quote (("no_std") ("default"))))))

(define-public crate-dces-0.3 (crate (name "dces") (vers "0.3.0") (hash "1xvpn80krsy43c1dnwgxbkaa2aybbbk3v7rcrx303y6xk94jw7xa") (features (quote (("no_std") ("default"))))))

(define-public crate-dces-0.3 (crate (name "dces") (vers "0.3.1") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "17dyyi1xg1pa1lvbjdcvisfgqwfh23mwg1grljcnrnkshpcqd88g") (features (quote (("no_std") ("default"))))))

