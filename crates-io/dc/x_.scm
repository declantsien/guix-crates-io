(define-module (crates-io dc x_) #:use-module (crates-io))

(define-public crate-dcx_screencapturer-0.1 (crate (name "dcx_screencapturer") (vers "0.1.1") (deps (list (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "1zb5p8bldiyg6298xwy0bs34607c2bxr8g3rsc2wvgpvfian1s46")))

(define-public crate-dcx_screencapturer-0.1 (crate (name "dcx_screencapturer") (vers "0.1.2") (deps (list (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "09i8vnf9cz44cl16xdambjgzslvxf0pm31wlm00kcfvb0zs7w6qz")))

(define-public crate-dcx_screencapturer-0.1 (crate (name "dcx_screencapturer") (vers "0.1.3") (deps (list (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "035wczmamnya239g61rl8lj37ga6vck5facqbw5ci9kcdq3dapvr")))

(define-public crate-dcx_screencapturer-0.1 (crate (name "dcx_screencapturer") (vers "0.1.4") (deps (list (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "0jaghlfq613670yw5zf0p0gqxbc8z08h2sg2y7a8icz3gj50cc6b")))

