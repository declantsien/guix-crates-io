(define-module (crates-io dc rs) #:use-module (crates-io))

(define-public crate-dcrs-0.0.1 (crate (name "dcrs") (vers "0.0.1") (hash "06mp8601nvb62vd2d3a7abya2lgw407al1mczvghrsxsssrdnlc5") (yanked #t)))

(define-public crate-dcrs-0.0.2 (crate (name "dcrs") (vers "0.0.2") (hash "0rfhw0ispxjq0svrihnh6f58ipg5x3gc911clrsxl9irhdpwm7gr") (yanked #t)))

(define-public crate-dcrs-0.0.3 (crate (name "dcrs") (vers "0.0.3") (hash "02qiaykfgm6121wbr87lgpz56g8rlnh8kh8dpglhq7dpi836x0fs") (yanked #t)))

