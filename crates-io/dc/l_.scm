(define-module (crates-io dc l_) #:use-module (crates-io))

(define-public crate-dcl_data_structures-0.4 (crate (name "dcl_data_structures") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "deep_causality_macros") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0cbyrrpvrs06k024b7mshzjnsrp9m79isiigkis523plyw1ialjw") (rust-version "1.65")))

(define-public crate-dcl_data_structures-0.4 (crate (name "dcl_data_structures") (vers "0.4.3") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "deep_causality_macros") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1y5gvxidql453sdaf2g8j3ycm7y0dx3c4b9kv4jh9jy3a2rw3ya2") (rust-version "1.65")))

(define-public crate-dcl_data_structures-0.4 (crate (name "dcl_data_structures") (vers "0.4.4") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "05jia0fxvnz8qhycgallyx9kpxqh4hxj9iqsc8x2vv0r8asgrv54") (rust-version "1.65")))

(define-public crate-dcl_data_structures-0.4 (crate (name "dcl_data_structures") (vers "0.4.5") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1cympygq0nwmwa50lmhz96zygp4pidr3xb4ad3pa1cqr440kdjw1") (rust-version "1.65")))

(define-public crate-dcl_data_structures-0.4 (crate (name "dcl_data_structures") (vers "0.4.6") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1bwdhv43lpzb0zv1jbblnx08cb286fhvji0zccms583ckfmlcqv8") (rust-version "1.65")))

(define-public crate-dcl_data_structures-0.4 (crate (name "dcl_data_structures") (vers "0.4.7") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0is6lany9hwq9xb7lpdimx4s8rcygg01ghkq27l8f0c7cb8id0q6") (rust-version "1.65")))

