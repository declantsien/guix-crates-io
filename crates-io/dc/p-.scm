(define-module (crates-io dc p-) #:use-module (crates-io))

(define-public crate-dcp-rs-0.0.1 (crate (name "dcp-rs") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "testcontainers") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "14kanbilzckyl02vvalgvjm66mqxjvfyn788456pq1qj3nlm1v7l")))

