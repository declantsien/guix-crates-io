(define-module (crates-io dc sr) #:use-module (crates-io))

(define-public crate-DCSR-0.0.2 (crate (name "DCSR") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51.0") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "082gg006ahs2gdx9kmpipn5v48fs9g5rwnbxv1wk99lbpnq3c8lj")))

(define-public crate-DCSR-0.0.3 (crate (name "DCSR") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51.0") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "1v3g75d9yysqkyncfq6l90bjhc7dnqqr694x4x49rmsd9nsl1c50")))

(define-public crate-DCSR-0.0.4 (crate (name "DCSR") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "125x792d5qa16bdawddigixxac5ab204pznwksi3rsd606yx98lv")))

(define-public crate-DCSR-0.0.5 (crate (name "DCSR") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "1vyvfqipxpswc90a0fkap1d33182249f3lnb9xfmm3pc2wqawij9")))

