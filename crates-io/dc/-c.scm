(define-module (crates-io dc -c) #:use-module (crates-io))

(define-public crate-dc-cli-0.1 (crate (name "dc-cli") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "dc-ock") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "19j66rq43f3npyg224nhv36py2acpg11mk3zf5v3iqq2bfzskvq1")))

