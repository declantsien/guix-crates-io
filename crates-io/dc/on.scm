(define-module (crates-io dc on) #:use-module (crates-io))

(define-public crate-dconf-0.1 (crate (name "dconf") (vers "0.1.0") (deps (list (crate-dep (name "dconf-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "gobject-sys") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1nr62lxmin37gq1i5fks9v33lj2abwlnwi6r1cm17qyi0y081v4y")))

(define-public crate-dconf-0.1 (crate (name "dconf") (vers "0.1.1") (deps (list (crate-dep (name "dconf-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "gobject-sys") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "0h410w06ppg1c6yqbcki5dhlkxgnk4fwkqgq4ym1xcqk8afisxys")))

(define-public crate-dconf-sys-0.1 (crate (name "dconf-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "glib") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "0vjpykwhhxgmgxmd9iig3nk6zc3l27qvdb5z53d77dwlhn6j7wd3")))

(define-public crate-dconf_rs-0.1 (crate (name "dconf_rs") (vers "0.1.0") (hash "1268zb9bilxq662l8xzzm3kil9x6z8sp4wlkqycdrq2ap2yh0gf8")))

(define-public crate-dconf_rs-0.2 (crate (name "dconf_rs") (vers "0.2.0") (hash "0rpnqxkjg7ak5kkpli9ny7jlpra9c4p6hccfril9varc51470kzs")))

(define-public crate-dconf_rs-0.2 (crate (name "dconf_rs") (vers "0.2.1") (hash "1cq9ylr8q9a9kmx478lcpjwxq3ycg5kbhk2pv0q90i4vpw7fhmch")))

(define-public crate-dconf_rs-0.3 (crate (name "dconf_rs") (vers "0.3.0") (hash "12swi0npq88kbdwnc3n324dzknir674agrh13h305876h654cikh")))

