(define-module (crates-io dc al) #:use-module (crates-io))

(define-public crate-dcalc-0.1 (crate (name "dcalc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0m5ljxwnxl0dkgv8xfjzgd4k20lgsmk7s5fzvm1ki3llgp71al9s") (rust-version "1.56")))

