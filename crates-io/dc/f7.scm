(define-module (crates-io dc f7) #:use-module (crates-io))

(define-public crate-dcf77-0.1 (crate (name "dcf77") (vers "0.1.0") (hash "1lqx7ccjcmrg9wav879w5i0z9862gl8mxan7lanxply6hlkzydah")))

(define-public crate-dcf77_chrono-0.1 (crate (name "dcf77_chrono") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1w6kwyym9l9kbmscx03ajnibb8s24lsb4j87gf3hczich5v4n0zn")))

(define-public crate-dcf77_chrono-0.2 (crate (name "dcf77_chrono") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "14xkc7ah00zc16yx9q287lvfdfb9sqng172fik7g0crm821gfkqh")))

(define-public crate-dcf77_chrono-0.2 (crate (name "dcf77_chrono") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)))) (hash "0pfyj49p8x5mmgk235l0gjk97bxdg9kr50y0dpmlyid7kg948959")))

(define-public crate-dcf77_utils-0.1 (crate (name "dcf77_utils") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.7.13") (default-features #t) (kind 0)) (crate-dep (name "radio_datetime_utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12akbpz2yhxcvn5n40g5csfjr2jn1v2xr2wvzpprcvf8kxvpxmsq")))

(define-public crate-dcf77_utils-0.1 (crate (name "dcf77_utils") (vers "0.1.1") (deps (list (crate-dep (name "heapless") (req "^0.7.13") (default-features #t) (kind 0)) (crate-dep (name "radio_datetime_utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05z5mky47fva1751bphvzah092cbr4gd75khaxsiivymgavpm0c9")))

(define-public crate-dcf77_utils-0.2 (crate (name "dcf77_utils") (vers "0.2.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "00x8r4lwjjlpd42cdvdq0x62ci61lfmj1baqidy7drwhfmdjidi8")))

(define-public crate-dcf77_utils-0.2 (crate (name "dcf77_utils") (vers "0.2.1") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0hiq3sall6g2b2zigr8s13jhn5l40hr3r82zkckg29s3jnk7q86w")))

(define-public crate-dcf77_utils-0.2 (crate (name "dcf77_utils") (vers "0.2.2") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0bybsjvi7s7w5i7ynkpvwyw06l8z9j02mfxad16h00frfkxy89z6")))

(define-public crate-dcf77_utils-0.3 (crate (name "dcf77_utils") (vers "0.3.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0lpwza9mmk9gfx8swpv0mkhf3243lg0f1h9ik2mrsd5dgv8c7qqy")))

(define-public crate-dcf77_utils-0.3 (crate (name "dcf77_utils") (vers "0.3.1") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0w9v83yylpcaxvz37ks04vha45haj8jp844w2gccq1918sxi7fim")))

(define-public crate-dcf77_utils-0.4 (crate (name "dcf77_utils") (vers "0.4.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0m7sbq4vin3r49sy6mqhmd7blhxrqli9m57h5x9kcnhg7zbjym2y")))

(define-public crate-dcf77_utils-0.5 (crate (name "dcf77_utils") (vers "0.5.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1yscrcl7wg5ybyfk6jwdfjxar86wm7ppd329nl0rlmibmwbqckc2")))

(define-public crate-dcf77_utils-0.6 (crate (name "dcf77_utils") (vers "0.6.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.5") (default-features #t) (kind 0)))) (hash "1pjwka57z0x1j4wy8qwsk3bd66klr6wavr10dgwn1vxaqarfrf1f")))

