(define-module (crates-io dc mi) #:use-module (crates-io))

(define-public crate-dcmimu-0.1 (crate (name "dcmimu") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "041n9nn7k60ixavhnrgsymzijj55pnrp9qagwvhj30z493gynwn2")))

(define-public crate-dcmimu-0.1 (crate (name "dcmimu") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0wikwjb37mh2v97s9rz15hrzypcps9srbq2xnq4awq3l5zhbyb9n")))

(define-public crate-dcmimu-0.1 (crate (name "dcmimu") (vers "0.1.2") (deps (list (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1a3hc32cgbbyyr12wg850a4qja7dkd491bh7653vnpl9ajszn6fb")))

(define-public crate-dcmimu-0.1 (crate (name "dcmimu") (vers "0.1.3") (deps (list (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1pq5is8wqj9x7zaf3xmii49kncvhsaw3hd9kfahrv7m054666sa7")))

(define-public crate-dcmimu-0.1 (crate (name "dcmimu") (vers "0.1.5") (deps (list (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0s81yy22bivmdc1vf95p4ih8bwmv9m7gy2q4gvblkjyvqyi1sjf8")))

(define-public crate-dcmimu-0.2 (crate (name "dcmimu") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0r0m2nqwkk3hk56milw87hhcvrj8ms5krw8xclbcrm200s4azdjd")))

(define-public crate-dcmimu-0.2 (crate (name "dcmimu") (vers "0.2.1") (deps (list (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1wmn682m8h2m37vw5mwwqbi0n0if5lz21s5mf3b0rd5chgn5lf29")))

(define-public crate-dcmimu-0.2 (crate (name "dcmimu") (vers "0.2.2") (deps (list (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^0.2") (default-features #t) (kind 2)))) (hash "0w01izzv3zvjymkbvvpjjlxm9bvvb490b2f10chw4nii4f4iqav9")))

