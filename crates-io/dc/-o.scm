(define-module (crates-io dc -o) #:use-module (crates-io))

(define-public crate-dc-ock-0.1 (crate (name "dc-ock") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "1y1jbhpzfrcxk21kn1z1zrafbqn6b4yx4sj20bmgy8c1d6q9n14i") (yanked #t)))

(define-public crate-dc-ock-0.1 (crate (name "dc-ock") (vers "0.1.1") (deps (list (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "0ka2aj4b3hsmlw5021mpj9m0xz0hpyjv55vpgv0x0xq5zwla8qws") (yanked #t)))

(define-public crate-dc-ock-0.1 (crate (name "dc-ock") (vers "0.1.2") (deps (list (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "0ld79hwg168xw0ps0554rlvwdhyvb343grgg7i957f050fhylvvg") (yanked #t)))

(define-public crate-dc-ock-0.2 (crate (name "dc-ock") (vers "0.2.0") (deps (list (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "10i0wyn9v7v5mqbrxamvhh5cgjql7qv4ggfb4j2r6i64a3fdnmxy") (yanked #t)))

(define-public crate-dc-ock-0.3 (crate (name "dc-ock") (vers "0.3.0") (deps (list (crate-dep (name "config") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "05ji8m3vdk5g3z87sdba8vawmkmwi516xpnlqn23299d1ad4s16p") (yanked #t)))

(define-public crate-dc-ock-1 (crate (name "dc-ock") (vers "1.0.0") (deps (list (crate-dep (name "config") (req "^0.13.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0mknpd7npxjwk4ph5g0rkbv60x68p4gcgbds2kmq3sz3bnjgh7zc") (features (quote (("bbin" "config" "directories")))) (yanked #t)))

(define-public crate-dc-ock-2 (crate (name "dc-ock") (vers "2.0.0") (hash "0xslxqc59hfazy5jk1gsb2wfqn3dk6anv6jwc746xwcyrwz3n4yz")))

(define-public crate-dc-ock-2 (crate (name "dc-ock") (vers "2.0.1") (hash "02hc00slk8nlyfic0xxl5vd7r6n9vabd2a5p24c74j6wjpsbm71r")))

