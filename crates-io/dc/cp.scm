(define-module (crates-io dc cp) #:use-module (crates-io))

(define-public crate-dccp-0.1 (crate (name "dccp") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11") (default-features #t) (kind 0)))) (hash "03hbwny4f1d7diz7ywyrr0szkij8xnkj4kvqch5qxl2lbfdpxi2g")))

