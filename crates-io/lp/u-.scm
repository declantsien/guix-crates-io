(define-module (crates-io lp u-) #:use-module (crates-io))

(define-public crate-lpu-macros-0.1 (crate (name "lpu-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "119pcbalk1wqjhl8acjk9m79f57n2f1qfczb86sz86a9cg8fh1gn")))

(define-public crate-lpu-macros-0.2 (crate (name "lpu-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1sry8g3ryndwc0gssakarvd5nadakf17664asgkbs9rayg98z83h")))

