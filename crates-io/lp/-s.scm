(define-module (crates-io lp -s) #:use-module (crates-io))

(define-public crate-lp-solvers-0.0.1 (crate (name "lp-solvers") (vers "0.0.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "07lxyh0izar93m62w6q2k65nfyp389dh45zpxwicnwyl2y65cmw0")))

(define-public crate-lp-solvers-0.0.2 (crate (name "lp-solvers") (vers "0.0.2") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "0yl0vsq99vbcr1gsw2qksr8k3prbrfjdl08xi3cklr2l12gpv8lg") (features (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-0.0.3 (crate (name "lp-solvers") (vers "0.0.3") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "1g0g0dlhc24s4jpm2m5a3whkpgwid5lsaxsyj2n3azkri7ysycj1") (features (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-0.0.4 (crate (name "lp-solvers") (vers "0.0.4") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "002xd94bagavnwa7hsdjwq49dak14dv9l892q7xr7a4176lychrs") (features (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-0.0.5 (crate (name "lp-solvers") (vers "0.0.5") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "1fl8r5nzw4zbjpq2g04pz4yms35jy9m23jlpwa2ipmgw16j535bw") (features (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-1 (crate (name "lp-solvers") (vers "1.0.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "1j1z2786s1xdc2in9b2x130i2piv1h24lgldrnz98vz2mmf9n27z") (features (quote (("cplex" "xml-rs"))))))

(define-public crate-lp-solvers-1 (crate (name "lp-solvers") (vers "1.0.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "07nkan0a3rwmrrlvpnhdnmbkzmj624z11f716n9d82wvzyqwnj5j") (features (quote (("cplex" "xml-rs"))))))

