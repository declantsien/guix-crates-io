(define-module (crates-io lp c4) #:use-module (crates-io))

(define-public crate-lpc43xx-0.0.0 (crate (name "lpc43xx") (vers "0.0.0") (deps (list (crate-dep (name "volatile-register") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1lbvxavs1bynbi8l3aihjsxn4haq56sjlxv0b8xxb0v7chv34zq2")))

(define-public crate-lpc43xx-0.1 (crate (name "lpc43xx") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1x8f19ryxi206jhx6mhrg467ip3xhaaym744nfxz8mvqp42ds02g") (features (quote (("rt" "cortex-m-rt"))))))

(define-public crate-lpc43xx-hal-0.0.0 (crate (name "lpc43xx-hal") (vers "0.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "lpc43xx") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xclsk66zib0mf4g5zvi6v6q3c27mbnc77z1kir9i02cih35hsyj")))

