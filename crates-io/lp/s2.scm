(define-module (crates-io lp s2) #:use-module (crates-io))

(define-public crate-lps22-0.1 (crate (name "lps22") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "generic-array") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hal") (req "^0.2.0") (default-features #t) (kind 0) (package "embedded-hal")))) (hash "1xvqf15gsf4pqrzmyx6qjgrynp012ljvy8p42s7gnlbb60gz6n8z")))

(define-public crate-lps22hb-0.0.1 (crate (name "lps22hb") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fw1ysnd117a8jqs0la4h0cpckznyb6kc9yi7dwhxxwl1181acs8")))

(define-public crate-lps22hb-0.0.2 (crate (name "lps22hb") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cawj86z2f68sg4ndyczgjqv45rdcrjnm1xjm7pcv071qnp1897s")))

(define-public crate-lps22hb-0.0.3 (crate (name "lps22hb") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "044qk0cykvf93n36lckik735idr1s09gb7icqnbrsvw7am4jkpxf")))

(define-public crate-lps22hb-0.1 (crate (name "lps22hb") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "16cdw9kja0aqfqfvsx6ldp3ykz69f57zgr1zybkc03i5g4a7sahl")))

(define-public crate-lps25hb-0.0.1 (crate (name "lps25hb") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "07yf14fh0cfl94n0zyrinsdd99src248a3dfd04midi1wg8zqwrk")))

(define-public crate-lps25hb-0.0.2 (crate (name "lps25hb") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "092sflzkmknfninziq65l3kqy4c79nc5jgmqvr90nd53r3frrzvy")))

(define-public crate-lps25hb-0.0.3 (crate (name "lps25hb") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r0akc35c06wwb5gqr9qpgl0chfkibp7pyi9m144mnwqg7m5mvim") (yanked #t)))

(define-public crate-lps25hb-0.0.4 (crate (name "lps25hb") (vers "0.0.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nl8m0sw7hzrg04cjchxxxza3z06pvszl9cvq4mvvpf3qygh28md")))

(define-public crate-lps25hb-0.1 (crate (name "lps25hb") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qfz6yk9sb3gz463dnpj5jjlcgpdr70fj0qi2w6i40dgl1afnwq4")))

