(define-module (crates-io lp as) #:use-module (crates-io))

(define-public crate-lpass-blob-0.1 (crate (name "lpass-blob") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "08w96dixwqba5ah5iia77w1p8czcr9amij2nc146kra9k2wqcdvf")))

