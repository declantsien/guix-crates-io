(define-module (crates-io lp ws) #:use-module (crates-io))

(define-public crate-lpwstr-0.1 (crate (name "lpwstr") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("errhandlingapi" "winbase" "winerror"))) (default-features #t) (kind 2)))) (hash "0za458244a97f7gknwdvf5dmh73xd0az2wdfnfvf3fbcaj64rwkj") (yanked #t)))

