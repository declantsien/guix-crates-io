(define-module (crates-io lp #{50}#) #:use-module (crates-io))

(define-public crate-lp5018-0.1 (crate (name "lp5018") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0fwmhp6vbskvdk0qybg07fhjyin5ll60wm3wiw9nxwjxnhylnpr4") (yanked #t)))

(define-public crate-lp5018-0.1 (crate (name "lp5018") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1yjq5cg8sqdjgyzzwkp2zdlkz1n7bkbpy487vp3znvy2nwhz3bbx") (yanked #t)))

(define-public crate-lp5018-0.1 (crate (name "lp5018") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1d911rd8v1fjq22lx30f2cnmav900axl4rnasm0mi343l58bm0nr")))

(define-public crate-lp5018-0.1 (crate (name "lp5018") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0aa759f665skm0gn4d1g76i76b8hancs7cp9p0vdynap6qwss1kh")))

(define-public crate-lp5018-0.1 (crate (name "lp5018") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "05fhfn202qvn9qljrdz2pyckylq98xrw4dqs56nsxycy9r59rqk7")))

(define-public crate-lp50xx-0.0.1 (crate (name "lp50xx") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "03n9jzbd2vwkqww48fwss0xv0i3rhk7n3xsz0xr8mkk7qcqjq1pf") (features (quote (("default"))))))

(define-public crate-lp50xx-0.0.2 (crate (name "lp50xx") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1lh9kbg5fy4pvhh1803kkaa9lvy8lsr9700zgmn2zilcdmns1lrg") (features (quote (("default"))))))

(define-public crate-lp50xx-0.0.3 (crate (name "lp50xx") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "00drbp4ww5dxbx1n45m0y1ab2qhabz0927xjisv6f7vja3cjgnvm") (features (quote (("default"))))))

(define-public crate-lp50xx-0.0.4 (crate (name "lp50xx") (vers "0.0.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0cg54408rv0a7990qksia19yg9ssa6rph044i7kly311bw66rv8f") (features (quote (("default"))))))

