(define-module (crates-io lp -m) #:use-module (crates-io))

(define-public crate-lp-modeler-0.3 (crate (name "lp-modeler") (vers "0.3.0") (hash "0zf3blx8wnjclncbfqz7wbf4l28s7n4qp0rrmpasdhmkpfkdic60")))

(define-public crate-lp-modeler-0.3 (crate (name "lp-modeler") (vers "0.3.1") (hash "1f1f463h7nw4vrss5w36rx7c452lmpn76fyrb8y93c8xvm2391l9")))

(define-public crate-lp-modeler-0.3 (crate (name "lp-modeler") (vers "0.3.2") (deps (list (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1n1y2smki5bwn56zikisyak58nqcrrwma20hyxk0vsrdmvhdcjmh")))

(define-public crate-lp-modeler-0.3 (crate (name "lp-modeler") (vers "0.3.3") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1gq0pzc7wgq9gssicp4ixmzqsf1y4xy4jwfbwik3xc7wrwbs84c3")))

(define-public crate-lp-modeler-0.4 (crate (name "lp-modeler") (vers "0.4.0") (deps (list (crate-dep (name "uuid") (req "^0.7.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0qqlc7k0v6bd7qyl37kv572c20inw2spkl4pg62fyrah6r6jdghf")))

(define-public crate-lp-modeler-0.4 (crate (name "lp-modeler") (vers "0.4.1") (deps (list (crate-dep (name "uuid") (req "^0.7.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1mh6fdrvhhaqk8d5srirydk4nwxlkvj6lh3flsr6ipqb17h69f1l")))

(define-public crate-lp-modeler-0.4 (crate (name "lp-modeler") (vers "0.4.2") (deps (list (crate-dep (name "uuid") (req "^0.7.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1i1qsz50413jgvhw78wjd41a006mnnsq4vb42d5kb9a4cn0z03ry")))

(define-public crate-lp-modeler-0.4 (crate (name "lp-modeler") (vers "0.4.3") (deps (list (crate-dep (name "coin_cbc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "11v1yj0nf7zib5zwqf76f24dq785l9g4g8flm0n4rvbfqlszm5mv")))

(define-public crate-lp-modeler-0.5 (crate (name "lp-modeler") (vers "0.5.0") (deps (list (crate-dep (name "coin_cbc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "minilp") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0rfln4kcshcfs6j1ppdl3xkry68lhl8ahiwj8jcv3l0rim9lp7z6") (features (quote (("native_coin_cbc" "coin_cbc"))))))

