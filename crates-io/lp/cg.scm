(define-module (crates-io lp cg) #:use-module (crates-io))

(define-public crate-lpcg-0.1 (crate (name "lpcg") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.3") (features (quote ("png"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "07av1ymdvcacimv9qd9fk48bl5wg65hmcngdcpig65h7gpf8qwdn")))

(define-public crate-lpcg-0.2 (crate (name "lpcg") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.3") (features (quote ("png"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0ihibx8bnmm89wp0mnxaqk2snkbkh12r0849jd482pzivc06r9pp")))

(define-public crate-lpcg-0.3 (crate (name "lpcg") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.24.3") (features (quote ("png"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0gdlpxga2r49zyzg02nwx3rfz03bwiiyk2pq2dpr7017sz0a1qqw")))

