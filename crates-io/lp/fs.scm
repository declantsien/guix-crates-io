(define-module (crates-io lp fs) #:use-module (crates-io))

(define-public crate-lpfs-0.1 (crate (name "lpfs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 2)))) (hash "0ls4fpjacrbd2x3pczjwb28l7d8q458ykr05hmsk86385mckx85k") (features (quote (("task") ("stat") ("pid_stat") ("pid_comm") ("default" "apm" "cpuinfo" "stat" "_self" "task" "pid_stat" "pid_comm") ("cpuinfo") ("apm") ("_self"))))))

(define-public crate-lpfs-0.2 (crate (name "lpfs") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 2)))) (hash "0jnx1bk5irlwa8vj74zp5kx39isawx3w14smyp3fzzz20gijbgw2")))

