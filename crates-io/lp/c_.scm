(define-module (crates-io lp c_) #:use-module (crates-io))

(define-public crate-lpc_checksum-0.1 (crate (name "lpc_checksum") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1vl1q93sq3hj037csqgcb72wiy4vj8gn5a6xs3w4jj5cj0d8sz3v")))

