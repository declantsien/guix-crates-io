(define-module (crates-io lp -t) #:use-module (crates-io))

(define-public crate-lp-types-0.0.0 (crate (name "lp-types") (vers "0.0.0") (deps (list (crate-dep (name "latexify") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.158") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.158") (default-features #t) (kind 0)) (crate-dep (name "sprs") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1k1z04gvy7lnlk7nrnjw62shghlvglyp7rhmzsdpr1hys0wgimir") (features (quote (("default"))))))

