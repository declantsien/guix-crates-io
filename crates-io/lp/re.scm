(define-module (crates-io lp re) #:use-module (crates-io))

(define-public crate-lprefix-0.1 (crate (name "lprefix") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "02gwnl2yshv4f5xs028gg7pf8ccmph3mcvm77ws3izwmiw07xj6m") (yanked #t)))

(define-public crate-lprefix-0.1 (crate (name "lprefix") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1ikwxmwzrjm3flyyx6l4y07g4dsqmq0zpsayn8anmzhkjdxfpawa")))

