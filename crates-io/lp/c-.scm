(define-module (crates-io lp c-) #:use-module (crates-io))

(define-public crate-lpc-usbd-0.1 (crate (name "lpc-usbd") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "usb-device") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ghks6qv523zvzslldakva1054a8s91mywxj4xh7216k14q2rhxq")))

