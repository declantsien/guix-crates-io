(define-module (crates-io lp #{58}#) #:use-module (crates-io))

(define-public crate-lp586x-0.1 (crate (name "lp586x") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (default-features #t) (kind 2)))) (hash "0flsr0jl2szlx46yijwmny5pgrrn0rvcsq5cp7ch6hjq8yr969z6")))

