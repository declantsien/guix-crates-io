(define-module (crates-io lp so) #:use-module (crates-io))

(define-public crate-lpsolve-0.1 (crate (name "lpsolve") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lpsolve-sys") (req "^5.5") (default-features #t) (kind 0)))) (hash "1kbf16ymk0vdraqqs46sdwv3b4v1scv0q57jq6jvbmwhhwd6fv2i")))

(define-public crate-lpsolve-sys-5 (crate (name "lpsolve-sys") (vers "5.5.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pd8ysg60pc0gg79v1067s83jfwil9b7y8hlxnidwjxm0qnqcxh7")))

