(define-module (crates-io lp #{55}#) #:use-module (crates-io))

(define-public crate-lp55231-0.1 (crate (name "lp55231") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1g1n3wwjrrz2py4xcap2acjbqq8pz7hn3pnqyycic1v5cqdwaqjx")))

(define-public crate-lp55231-0.2 (crate (name "lp55231") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1xk5397nzllq90iny3dcb21cdw4w974fa08yfhiwmprcqvb6rlk7")))

(define-public crate-lp55231-0.2 (crate (name "lp55231") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "06330yb3kbhl50lzvn9ci9vb1q8vib55b11mqzbxjqnq5wqbpbzp")))

