(define-module (crates-io rk #{33}#) #:use-module (crates-io))

(define-public crate-rk3399-pac-0.1 (crate (name "rk3399-pac") (vers "0.1.0") (deps (list (crate-dep (name "portable-atomic") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03mz7zax6x1qrdapxrp7frzrg4hcfkh39rzh2ir383j92havf4pl")))

(define-public crate-rk3399-pac-0.1 (crate (name "rk3399-pac") (vers "0.1.1") (deps (list (crate-dep (name "portable-atomic") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bx9b04ja7hpvv5w870hywyv9a0zn3rnh6i2vqk10xrgqnwf6kw2")))

(define-public crate-rk3399-pac-0.1 (crate (name "rk3399-pac") (vers "0.1.2") (deps (list (crate-dep (name "portable-atomic") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gk6fm0inm30ijgfjiv0qnxsflxryzg2phwlhmqi39a1v23d3r86")))

(define-public crate-rk3399-pac-0.1 (crate (name "rk3399-pac") (vers "0.1.3") (deps (list (crate-dep (name "portable-atomic") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ajrky5d67v9ggyv914n46sz447h28qdr0r0qjf9kb047p6x9y97")))

(define-public crate-rk3399-pac-0.1 (crate (name "rk3399-pac") (vers "0.1.4") (deps (list (crate-dep (name "portable-atomic") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0irxpspjbd6g2sjc7ykfykwaci0pm4pka5w05wq2waqmpak7mpy3")))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.0") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fl6svn4wdf7v61qz5dlfnpss53pzgix93a0c47n6xjka7k2c2d4") (yanked #t)))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.1") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0f61sgad6f7hwr0ak67i6fli57sjg5srcjn5qagl2bcsdja9za7g") (yanked #t)))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.2") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10d02z991ps9rijhrscczxlbiafgcqlf3jzin7r7zrmc6m6g482x") (yanked #t)))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.3") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0w72sd9gq4b5pfh25i26r6q153g8imfqavff5ym466wg59nflvs3") (yanked #t)))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.4") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lswlk45f589f8m6b5v9a3d2icg2rd49dg8g10fa2k289xschy0v") (yanked #t)))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.5") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03bdss74gxd0qgjzzza1q9rvvyl1q4n2rgskagn48bk0a1q88ka9") (yanked #t)))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.6") (deps (list (crate-dep (name "portable-atomic") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03vyl43251ylx5k6wkz79cmx65n2xx06lyg5p7gdnnxqcjj2kccd") (yanked #t)))

(define-public crate-rk3399-rs-0.1 (crate (name "rk3399-rs") (vers "0.1.7") (deps (list (crate-dep (name "portable-atomic") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0iv01k1mmxb15l7prb8n5p69n1f0603vv1064b8x0zhzbd6w6mwg") (yanked #t)))

