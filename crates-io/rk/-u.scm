(define-module (crates-io rk -u) #:use-module (crates-io))

(define-public crate-rk-utils-0.1 (crate (name "rk-utils") (vers "0.1.0") (hash "1rjh00sfnximi7x4y55sqzpblh38dmivyzawv958vlfr49p3y29b")))

(define-public crate-rk-utils-0.2 (crate (name "rk-utils") (vers "0.2.0") (hash "1qflldb89l45ryh04rg5d0x5yl4pnan3xbbvcgffkdkr61abnky5")))

(define-public crate-rk-utils-0.2 (crate (name "rk-utils") (vers "0.2.1") (hash "09vlyp7bif2a4b4qvfyg43kpbhwbq3sq7ajqj31312m3jb5871ii")))

(define-public crate-rk-utils-0.2 (crate (name "rk-utils") (vers "0.2.2") (hash "0djkzzavrphb7mi7mkfr8j5rryn1wk05i31w9dym9l7a6izmxm30")))

