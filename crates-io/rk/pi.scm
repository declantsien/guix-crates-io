(define-module (crates-io rk pi) #:use-module (crates-io))

(define-public crate-rkpi2-0.1 (crate (name "rkpi2") (vers "0.1.0") (deps (list (crate-dep (name "os_pipe") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "zstd") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1bd2sglgpr15mlzsjndix6jd3brpda65npj4zyxalx9zvlrh2v76")))

(define-public crate-rkpi2-0.1 (crate (name "rkpi2") (vers "0.1.2") (deps (list (crate-dep (name "os_pipe") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "zstd") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "04vp2nbgqhcxb1d1wagy2zblb8j1ybyfncgrgmb1ph8i3i5zf7zl")))

