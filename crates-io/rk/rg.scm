(define-module (crates-io rk rg) #:use-module (crates-io))

(define-public crate-rkrga-0.1 (crate (name "rkrga") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rkrga-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z3aphg68kn8747vkdibrph522b9k16jqq1vl5y6a8yjfvfyai9r")))

(define-public crate-rkrga-0.1 (crate (name "rkrga") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rkrga-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1p8194qmk4d37i8r6y3br1dvyzg2zagiarxpnrpw5irky7c7k08i") (features (quote (("use-bindgen" "rkrga-sys/use-bindgen") ("default"))))))

(define-public crate-rkrga-0.1 (crate (name "rkrga") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rkrga-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "05f0x5mxc2zh7r80x04fssipczym42bdrxblr84a532h4imvwl8b") (features (quote (("v1_3_1" "v1_3_0") ("v1_3_0" "v1_2_6") ("v1_2_6") ("use-bindgen" "rkrga-sys/use-bindgen") ("default"))))))

(define-public crate-rkrga-sys-0.1 (crate (name "rkrga-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "1244k3p2n6xy0jsyi85213wsmwmdinh6vfq7n2zg5rr1swv8zal0")))

(define-public crate-rkrga-sys-0.1 (crate (name "rkrga-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (optional #t) (default-features #t) (kind 1)))) (hash "0nnk698advlmpa9ajlkypiqdp1zjzfmpqlmq4hs564xz9l1xjb6n") (features (quote (("use-bindgen" "bindgen") ("default"))))))

