(define-module (crates-io rk no) #:use-module (crates-io))

(define-public crate-rknock-0.1 (crate (name "rknock") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1avsbcdni7xlds1fz3aj2ic2q6in20jy132fma1dgzvf0c5h4xxm")))

(define-public crate-rknock-0.1 (crate (name "rknock") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1i64k8d2qk7by77jwiq9qnkvgbqw796clkb5iyczc1j1727kn0pr")))

