(define-module (crates-io rk ms) #:use-module (crates-io))

(define-public crate-rkmstools-0.1 (crate (name "rkmstools") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "drm") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "edid") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06q9hqfvjpmwgckk4ri7xkhi9wwq3qy8jwjc8kbj1lyv0wxw3kpv")))

(define-public crate-rkmstools-0.1 (crate (name "rkmstools") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "drm") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "edid") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1dg31cnrfy93z7n1fki2s63i7pq16wfp1wzqm38ijbmsywhbq51d")))

