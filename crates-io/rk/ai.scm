(define-module (crates-io rk ai) #:use-module (crates-io))

(define-public crate-rkaiq-0.1 (crate (name "rkaiq") (vers "0.1.0") (deps (list (crate-dep (name "gst") (req "^0.17") (default-features #t) (kind 2) (package "gstreamer")) (crate-dep (name "gst-base") (req "^0.17") (default-features #t) (kind 2) (package "gstreamer-base")) (crate-dep (name "gst-video") (req "^0.17") (default-features #t) (kind 2) (package "gstreamer-video")) (crate-dep (name "rkaiq-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0sclzazgq2a0l59z1147lxpc1rbkrxh8c3ffrgyzl4pfkr9kpzqy") (features (quote (("rel_2_0") ("rel_1_0") ("default" "rel_2_0"))))))

(define-public crate-rkaiq-sys-0.1 (crate (name "rkaiq-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "1w1qh7p4702q4xyscz9vl35xql92819aa26yisfzrrn1dcxfzasw")))

