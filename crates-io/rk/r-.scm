(define-module (crates-io rk r-) #:use-module (crates-io))

(define-public crate-rkr-gst-0.1 (crate (name "rkr-gst") (vers "0.1.0") (deps (list (crate-dep (name "adler32") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)))) (hash "1xiqxl6zmar7b9nyz347n2bp6hjp67shm60ajhvg8db8zk1hlniv")))

(define-public crate-rkr-gst-0.1 (crate (name "rkr-gst") (vers "0.1.1") (deps (list (crate-dep (name "adler32") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^0.19.3") (default-features #t) (kind 0)))) (hash "05bx7b9ssani11539xlng34g7p49a70crcv8k5k4vppwy17zfmfn")))

(define-public crate-rkr-gst-0.1 (crate (name "rkr-gst") (vers "0.1.2") (deps (list (crate-dep (name "adler32") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1s6a4lslhx5gq3l9kx8swc5bkaclwlrbpk16j0vqsy33lx9g8ga8")))

