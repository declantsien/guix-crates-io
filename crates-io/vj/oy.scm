(define-module (crates-io vj oy) #:use-module (crates-io))

(define-public crate-vjoy-0.1 (crate (name "vjoy") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "vjoy-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1151r6c7lgkpabl4h6qjipq014yj2lranv1vcx52hfl19afy419b")))

(define-public crate-vjoy-0.2 (crate (name "vjoy") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "vjoy-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0586pg9r8w4swqxlciqcq6sa2phnrpy5b75yvcbsr7rqvc3kflsj")))

(define-public crate-vjoy-0.3 (crate (name "vjoy") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "vjoy-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1s21brwypvlw5hsqwp5p9mw1nmd17yijm588mjl0i5yaw3s6p7hd")))

(define-public crate-vjoy-0.3 (crate (name "vjoy") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "vjoy-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "04w2za3jshs7ypb7p08dqpsrkb7d5ddqn3cdm44vj1rmwzm0r4nv")))

(define-public crate-vjoy-0.4 (crate (name "vjoy") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "profiling") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "vjoy-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xxv5hzvvb544cdanyrvrp8pl0qbql2340ngaxabhqyjlb2wq7k8")))

(define-public crate-vjoy-0.5 (crate (name "vjoy") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "profiling") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "vjoy-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1f7r9xd7l34sigbfx9s6g10fl26gddii1nf7azg632dpllnarnhq")))

(define-public crate-vjoy-0.6 (crate (name "vjoy") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "profiling") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "vjoy-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1fdjfvjbdpknd751v9l7zva47ripjrss2jqdkp60miylz5awmzzm")))

(define-public crate-vjoy-sys-0.1 (crate (name "vjoy-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0jagl95kma58yql96nqb4k0cxsxfg20pf60gfz5zdlj4wyx0f4fi")))

(define-public crate-vjoy-sys-0.1 (crate (name "vjoy-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "19hrh94fqnsvdqq5lj2gsws39s46x6szkpsbfjs9jhaakj325z44")))

(define-public crate-vjoy-sys-0.1 (crate (name "vjoy-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1p14yxmd15l3yhagdd3hbw0h916ab31m14nwpyvm0xfbmahh6krh")))

(define-public crate-vjoy-sys-0.2 (crate (name "vjoy-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0ch30nhpypyjsvmdsw1h0lsyicbi2bjw4zyngxd2dxr714fyymvd")))

(define-public crate-vjoy-sys-0.3 (crate (name "vjoy-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1dvqalyz2dd1sfwh8a17j5rckagh8r708yh6fi8pjjnj3sjzblaq")))

(define-public crate-vjoy-sys-0.4 (crate (name "vjoy-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1qlyjy2h7d81jaqzc8r1x8z3y8qbrd92cwygrzkyayhn52qvklnj")))

(define-public crate-vjoy-sys-0.4 (crate (name "vjoy-sys") (vers "0.4.1") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1jyj5ksbc3m9g7kgsydr1cl837wyxiln40178q3qmgdps4bbvw5i")))

