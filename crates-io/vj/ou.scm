(define-module (crates-io vj ou) #:use-module (crates-io))

(define-public crate-vjoule_api-1 (crate (name "vjoule_api") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0ffbc9wpgjjzy5i0ha1r0qgdw7l6395m4qf1zxjpqc3fyxna4fjg") (yanked #t)))

(define-public crate-vjoule_api-1 (crate (name "vjoule_api") (vers "1.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1j3qh16i00cczvpcrwzx2z8flr5pnlyf4pvwjzjx0qxdl5w1l95c")))

(define-public crate-vjoule_api-1 (crate (name "vjoule_api") (vers "1.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "12g3wcsvhl7qffqyvb9s25czaqf7xpl8nj9bq3h62a8i8r4bk5s3")))

(define-public crate-vjoule_api-1 (crate (name "vjoule_api") (vers "1.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1d0h87x0i6ax0fybwyc5x1p1yc4jlvw68s5ifw9l77lrvnma0j3z")))

(define-public crate-vjoule_api-1 (crate (name "vjoule_api") (vers "1.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1iq6bpxmqgn29g2g83b3gsygamxmxdvhcb98spfq9av932qm0s26")))

