(define-module (crates-io sf rm) #:use-module (crates-io))

(define-public crate-sfrm-0.1 (crate (name "sfrm") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("fs" "macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "08hgx5snyl5wwb4p66y8c5sblblyin3ppg1i5mhslqaqh3v5l21i")))

