(define-module (crates-io sf sd) #:use-module (crates-io))

(define-public crate-sfsdb-0.1 (crate (name "sfsdb") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^0.13.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13xfm6k8pvn98abc09g5kk0iabcn1q3qpg2lay5jjq5krv4v4hf5")))

