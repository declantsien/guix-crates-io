(define-module (crates-io sf sm) #:use-module (crates-io))

(define-public crate-sfsm-0.1 (crate (name "sfsm") (vers "0.1.0") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hfh3i5w47sfcc6ga2rba16kdzpbhy7lp7zjl5nj717n3dq04yfp")))

(define-public crate-sfsm-0.1 (crate (name "sfsm") (vers "0.1.1") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0541v53crjrcyxbzfhb471x6l5p6p3c4xshs8axgz7md3l0mm4qy")))

(define-public crate-sfsm-0.2 (crate (name "sfsm") (vers "0.2.0") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0mmi7pxhc228p331pifzcd25hzsdn0rafrcjg10cssygkgcqqvdr")))

(define-public crate-sfsm-0.2 (crate (name "sfsm") (vers "0.2.1") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0iprq51whjcspywylmzivl8b5j7clgllix8avlkiyl2fx0yz8r3d")))

(define-public crate-sfsm-0.2 (crate (name "sfsm") (vers "0.2.2") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "15bwnbg8m362v25kz4r4ly55vm0326q4n5gqhscx65lz9bppj68b")))

(define-public crate-sfsm-0.2 (crate (name "sfsm") (vers "0.2.3") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0y06ba56slqmrr9cgzncwzm98x7i79v209x68d87s5d5fixafbi3")))

(define-public crate-sfsm-0.2 (crate (name "sfsm") (vers "0.2.6") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "04ky5pdaspwls4pqdbvrlx5w1dgyxahssnvw5df579r9424607bb")))

(define-public crate-sfsm-0.2 (crate (name "sfsm") (vers "0.2.8") (deps (list (crate-dep (name "sfsm-base") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1i4fjr6hhzbhzfd59szxhlw4x70rvkp2shych5dnnm65vhwflc6s")))

(define-public crate-sfsm-0.3 (crate (name "sfsm") (vers "0.3.0") (deps (list (crate-dep (name "sfsm-base") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1i25mikib1z7qln2152agg86zph4dx5hqp2rnjfa04vpig97max7")))

(define-public crate-sfsm-0.4 (crate (name "sfsm") (vers "0.4.0") (deps (list (crate-dep (name "sfsm-base") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.4.0") (kind 0)))) (hash "187imzf4gxl2w5l5ap3xyk594551mlx5lib2n1s4pmzq2rjc322i") (features (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

(define-public crate-sfsm-0.4 (crate (name "sfsm") (vers "0.4.1") (deps (list (crate-dep (name "sfsm-base") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.4.1") (kind 0)))) (hash "1c1010af3kdykmw68hxwv2k1vv6fsdd8x96gljyrnnn2nb7ya7b8") (features (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

(define-public crate-sfsm-0.4 (crate (name "sfsm") (vers "0.4.2") (deps (list (crate-dep (name "sfsm-base") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.4.2") (kind 0)))) (hash "1brz6zdgcrqr5bryldrs1rbnj6vvw8lf4k8ah3bm2lh64gy6iqwk") (features (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

(define-public crate-sfsm-0.4 (crate (name "sfsm") (vers "0.4.3") (deps (list (crate-dep (name "sfsm-base") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "sfsm-proc") (req "^0.4.3") (kind 0)))) (hash "13bdig0cwkwkcf9gsc2kpzsymzmzq0agni2n4ps2m8gbn3lrqa1s") (features (quote (("trace-steps" "sfsm-proc/trace-steps") ("trace-messages" "sfsm-proc/trace-messages") ("trace" "sfsm-proc/trace"))))))

(define-public crate-sfsm-base-0.1 (crate (name "sfsm-base") (vers "0.1.0") (hash "08hj38rw63gymn9cn62kas6c7484ch42wk58jdx7yz4llvg99gdl")))

(define-public crate-sfsm-base-0.1 (crate (name "sfsm-base") (vers "0.1.1") (hash "05w1va2slb5appp3bsvziz4z7cg0hfsxrfd83zmhscqzralry1br")))

(define-public crate-sfsm-base-0.1 (crate (name "sfsm-base") (vers "0.1.2") (hash "1akdlw29c7m737xy3k851khvqv12qg2lsm68any4hn6l654f3f2c")))

(define-public crate-sfsm-base-0.1 (crate (name "sfsm-base") (vers "0.1.4") (hash "0a1kycj8pjflgf6h1p8wr6klfq2kx3dv6a15gn1wy5b49s1sl7kd")))

(define-public crate-sfsm-base-0.1 (crate (name "sfsm-base") (vers "0.1.6") (hash "1hw91wc5vq7vb49iqy5brlrjg2gljag8bq8j6wr3c4z1ijajfl18")))

(define-public crate-sfsm-base-0.3 (crate (name "sfsm-base") (vers "0.3.0") (hash "0kj4hphzvmx13hvk7k1hdlp5pw66hbv5gikyha4x4iiwjhszg69m")))

(define-public crate-sfsm-base-0.4 (crate (name "sfsm-base") (vers "0.4.0") (hash "09hbjvw5lknd8kxm7q125i4ll1rs26lb2mwvrxwjzmkgjvd8f5dx")))

(define-public crate-sfsm-base-0.4 (crate (name "sfsm-base") (vers "0.4.1") (hash "06q6fs31xwy2m3nqpg341dnnbjj2yzsz7xzv0zghvzvg5wa93xbr")))

(define-public crate-sfsm-base-0.4 (crate (name "sfsm-base") (vers "0.4.2") (hash "12zc759r1pv7pbgnkmknsq28yli92lmaanhvpa1d5xp45z3bnxg0")))

(define-public crate-sfsm-base-0.4 (crate (name "sfsm-base") (vers "0.4.3") (hash "1kmzsv51fzqxswf3j6sij5xd2rpr8dninfs1rrx9lvw3n4cf3naw")))

(define-public crate-sfsm-proc-0.1 (crate (name "sfsm-proc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1h3bcqj47gaq7k1xgf4kxh7dbqhgsfb5bmr0qxnipx32gpi6xbfx")))

(define-public crate-sfsm-proc-0.1 (crate (name "sfsm-proc") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "168idgikdswyhhlpvrz26gljd0z12347f1nqb40fw6rg293crz8c")))

(define-public crate-sfsm-proc-0.2 (crate (name "sfsm-proc") (vers "0.2.2") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1gn5jmfq4hvv715vmc6b8176l5hqk20j2qs8dhs5n7cy451p577w")))

(define-public crate-sfsm-proc-0.2 (crate (name "sfsm-proc") (vers "0.2.3") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "13pw9j3ilx0qcprgkvqpzs4qirv5brbr7am32jja5rxss957fac5")))

(define-public crate-sfsm-proc-0.2 (crate (name "sfsm-proc") (vers "0.2.4") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0jbrr77z5y45z2vpdj031h40c3hlz8nzrqqyrcwadigd686i7265")))

(define-public crate-sfsm-proc-0.2 (crate (name "sfsm-proc") (vers "0.2.6") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "06i1jc6svc2yggssd9268xbg9y703l7q28rhnqq42z343gx25vyl")))

(define-public crate-sfsm-proc-0.2 (crate (name "sfsm-proc") (vers "0.2.8") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "10zrmxvg2x8h62ka7jcmcb5x7qq9mvqx4ddlchgzk8gkq08crg2v")))

(define-public crate-sfsm-proc-0.3 (crate (name "sfsm-proc") (vers "0.3.0") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1g262wi47xw92fjhx174jy9yyblfk80hibad64f8npc4khb6vxrb")))

(define-public crate-sfsm-proc-0.4 (crate (name "sfsm-proc") (vers "0.4.0") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0a0qc9hybmyvig665z7ffprnck32r2979mbgx8z3w977wwa5a4dj") (features (quote (("trace-steps") ("trace-messages") ("trace"))))))

(define-public crate-sfsm-proc-0.4 (crate (name "sfsm-proc") (vers "0.4.1") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0vcj9d45sq0npy6vx24c34g41qgxvm9py8qg63yc4bwnzazdyz08") (features (quote (("trace-steps") ("trace-messages") ("trace"))))))

(define-public crate-sfsm-proc-0.4 (crate (name "sfsm-proc") (vers "0.4.2") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1m824plrh7xgmhw0zn8rd8a2h7gqf5iq40h7659p2kp12nszaxv3") (features (quote (("trace-steps") ("trace-messages") ("trace"))))))

(define-public crate-sfsm-proc-0.4 (crate (name "sfsm-proc") (vers "0.4.3") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "sfsm-base") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0js7xap7r1rlg55955i83m95xhznvl5vqdqr30lnm4mn91973c3b") (features (quote (("trace-steps") ("trace-messages") ("trace"))))))

