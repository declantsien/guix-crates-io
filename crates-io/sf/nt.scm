(define-module (crates-io sf nt) #:use-module (crates-io))

(define-public crate-sfnt-0.1 (crate (name "sfnt") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qidbqkh81q6wcvfi0fwza9qcpg8cqj6xmpj4mwym3kvyhvkzq7i")))

(define-public crate-sfnt-0.2 (crate (name "sfnt") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "10z6cbn55i2zhcw24vraccjs5r66l1g6vafsywg33gma2q9n3xc0")))

(define-public crate-sfnt-0.3 (crate (name "sfnt") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1c9wgagvs3yancjwcbypqgyvbjj26rr3zl8mab3hb8xzk2gv9xfl")))

(define-public crate-sfnt-0.4 (crate (name "sfnt") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1wqh2g6apir9jz4qzjc85ad3wghmc3j0dgfamwcbxlfynzywq4ga")))

(define-public crate-sfnt-0.5 (crate (name "sfnt") (vers "0.5.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "11976p0rkwmk7j9s0yna20y93l7d8g9idsmws50xs6gn1lp3l282")))

(define-public crate-sfnt-0.6 (crate (name "sfnt") (vers "0.6.0") (deps (list (crate-dep (name "microbench") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0rs4xhi7qwxcldm6lxxszssxd2cl3b9mr5kpwy544frhpzhfid2a")))

(define-public crate-sfnt-0.7 (crate (name "sfnt") (vers "0.7.0") (deps (list (crate-dep (name "microbench") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0k02rhjqfqf5m1ih2r1hbyjcqyzy9i4l3hdahh54kb92dbpd9vrd")))

(define-public crate-sfnt-0.8 (crate (name "sfnt") (vers "0.8.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tarrasque-macro") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hrjlvz0xmbzjy8cr96ax80xyjj3kjim4vnmr7mrx08m47dgp0mn")))

(define-public crate-sfnt-0.8 (crate (name "sfnt") (vers "0.8.1") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.7") (default-features #t) (kind 0)))) (hash "1rrpmplddanzfly9p6jzxd1z21a9clsh7009z6grlrsqr9zmw86h")))

(define-public crate-sfnt-0.9 (crate (name "sfnt") (vers "0.9.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.8") (default-features #t) (kind 0)))) (hash "0drlvzvzgzkrlwkmjlzrzi5ggdhm2pkn9bk4k7rcxqiyxkc9443b")))

(define-public crate-sfnt-0.10 (crate (name "sfnt") (vers "0.10.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.9") (default-features #t) (kind 0)))) (hash "11375szz2irssplw1kkjz8wsz8457p0d9dfxpcv8k6cfbqflz0a8")))

(define-public crate-sfnt-0.11 (crate (name "sfnt") (vers "0.11.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.9") (default-features #t) (kind 0)))) (hash "0xx4jm8dngvq1aqz0z9c23jq32f01g1ajqa8j7qba0gpkxsqwcgw")))

(define-public crate-sfnt-0.12 (crate (name "sfnt") (vers "0.12.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque") (req "^0.10") (default-features #t) (kind 0)))) (hash "0y41ymabz0wihmjsxdhcgzbpf2i67q48675pfjkfy9hr2iirbgx4")))

(define-public crate-sfnt2woff-zopfli-sys-0.0.1 (crate (name "sfnt2woff-zopfli-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.30.0") (default-features #t) (kind 1)) (crate-dep (name "gcc") (req "^0.3") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x2agncm6q6k4rj5rk5m2rs1708avm9qs4cmf3vn63yn8x828vv8")))

