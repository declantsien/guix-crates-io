(define-module (crates-io sf xr) #:use-module (crates-io))

(define-public crate-sfxr-0.1 (crate (name "sfxr") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.31.0") (default-features #t) (kind 2)))) (hash "0j18cyryi26v9qqhy229xjih5q38aa0z5lgxvcim02v8q3w7zg0g")))

(define-public crate-sfxr-0.1 (crate (name "sfxr") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.31.0") (default-features #t) (kind 2)))) (hash "04bsyp4wyhid88vxcnjcgmmy5wsl5zn5mpwwpr5y29y42vvmk0z6")))

(define-public crate-sfxr-0.1 (crate (name "sfxr") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.31.0") (default-features #t) (kind 2)))) (hash "0kg67gnjjfc67nxw12p3336g17a58byryfccv7cwz84n6b0mjc8y")))

(define-public crate-sfxr-0.1 (crate (name "sfxr") (vers "0.1.3") (deps (list (crate-dep (name "prng") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rng_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.31.0") (default-features #t) (kind 2)))) (hash "05ivdvpvggaqvq2vmnix0fdm21zd1amf66mq6jr5wd5yialhas3l")))

(define-public crate-sfxr-0.1 (crate (name "sfxr") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.31.0") (default-features #t) (kind 2)))) (hash "03ngslv13k43x7yvjmb1xwhndwqz9kcbijmiw5k7gjpvay9r5wm2")))

