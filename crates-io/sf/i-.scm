(define-module (crates-io sf i-) #:use-module (crates-io))

(define-public crate-sfi-core-0.0.0 (crate (name "sfi-core") (vers "0.0.0") (hash "1lk1jxaq00xsw92aaxg7ryhwawygmkrg7y98w16dc3nwkrypmna5")))

(define-public crate-sfi-gtk-0.0.0 (crate (name "sfi-gtk") (vers "0.0.0") (hash "1g3pd1hixg99xy1a0hqzr8r6alymbbva9x33kx4js5j7gbs6v7cp")))

(define-public crate-sfi-server-0.0.0 (crate (name "sfi-server") (vers "0.0.0") (hash "1j3gi2sbsnhg0jmwn9znkcz3xlmrjin7cmw9qlnqmpkxksjswawi")))

(define-public crate-sfi-web-0.0.0 (crate (name "sfi-web") (vers "0.0.0") (hash "0dg8pir860ah2iz32ilz1c0cw8p6znx1ql5xnviz5k5p9fbskack")))

