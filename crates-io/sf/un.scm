(define-module (crates-io sf un) #:use-module (crates-io))

(define-public crate-sfunc-0.0.1 (crate (name "sfunc") (vers "0.0.1") (deps (list (crate-dep (name "assert") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0l1jmwv49r5canmk0rf2l224ldrsfxyn72nhzidg1kxcrgisnibp")))

(define-public crate-sfunc-0.0.2 (crate (name "sfunc") (vers "0.0.2") (deps (list (crate-dep (name "assert") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "1bv8lszp0diy1nln26s5k04zanivj0vwcnx900r5gnrxpkdqrb9g")))

(define-public crate-sfunc-0.0.3 (crate (name "sfunc") (vers "0.0.3") (deps (list (crate-dep (name "assert") (req "^0.0.3") (default-features #t) (kind 2)))) (hash "0wi9qz6s9ibjlc71zb3fabvqkdyja3zbbscrazn06fj91qkh4lcd")))

(define-public crate-sfunc-0.0.4 (crate (name "sfunc") (vers "0.0.4") (deps (list (crate-dep (name "assert") (req "^0.0.4") (default-features #t) (kind 2)))) (hash "0rcjzhfhn1kfd2z6gkfpcx64qn02hdcsrwsbarbv7xm6s02w06v2")))

