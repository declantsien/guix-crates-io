(define-module (crates-io sf co) #:use-module (crates-io))

(define-public crate-sfconversions-0.1 (crate (name "sfconversions") (vers "0.1.0") (deps (list (crate-dep (name "extendr-api") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.9") (default-features #t) (kind 0)))) (hash "17znkb1306qldfxhmdvkrkjpbywarzfmcr4308g1w16i36v9vrnd")))

(define-public crate-sfconversions-0.2 (crate (name "sfconversions") (vers "0.2.0") (deps (list (crate-dep (name "extendr-api") (req ">=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "geo") (req ">=0.25.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req ">=0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rstar") (req ">=0.11.0") (default-features #t) (kind 0)))) (hash "00qnxd0ny8gmf6cazbzha94vgwbnjcx5v0m0zflfl1s8dlc9hfkd")))

