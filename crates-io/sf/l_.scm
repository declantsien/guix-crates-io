(define-module (crates-io sf l_) #:use-module (crates-io))

(define-public crate-sfl_parser-1 (crate (name "sfl_parser") (vers "1.0.1") (hash "1y1v0nqk9zi9cx7ydwmsnb0qgm91qvbal7kghjc8bvcd3awbzadl") (yanked #t)))

(define-public crate-sfl_parser-1 (crate (name "sfl_parser") (vers "1.0.2") (hash "0cxcinczb8msm17mrpps0aiaqaxijwpjs87rwwa08kyd5lxd68in")))

(define-public crate-sfl_parser-1 (crate (name "sfl_parser") (vers "1.1.0") (hash "1fp8cqldcnia0df18yhl0rnkpvywarm271hy66w5n23vmx0hwy3r")))

(define-public crate-sfl_parser-1 (crate (name "sfl_parser") (vers "1.2.0") (hash "11p0ay8b2rw19i847brra96kh7jv7fvxy63ihjcfsc0d0jzkh3c8")))

(define-public crate-sfl_parser-1 (crate (name "sfl_parser") (vers "1.3.0") (hash "0sbgzmvz4z7cicgq4ccqkcr8snddrljhs2ns89z6zmiffg7758m0")))

(define-public crate-sfl_parser-1 (crate (name "sfl_parser") (vers "1.3.1") (hash "0l3mgy8vhazx1r5dvbd4znzpjyfyr0g422gx6n3z7jsqsm8f9fyv")))

