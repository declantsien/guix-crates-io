(define-module (crates-io sf rx) #:use-module (crates-io))

(define-public crate-sfrx-0.1 (crate (name "sfrx") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "100868gh1v9vdxgr84fb04bghxil3ik4d3cxxv24327fqcrcgf62")))

(define-public crate-sfrx-0.1 (crate (name "sfrx") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i5fl0l3ck3z96k24aiyvc69444az4lgm9nriirkd0whcgladpnp")))

(define-public crate-sfrx-0.1 (crate (name "sfrx") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ziszwq0bzkxdkd6nv6173dhg9j65jwivgp5s9nq2zsdhpzd7whr")))

(define-public crate-sfrx-0.1 (crate (name "sfrx") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aa8kn6l1jzg5iq0yncvg6prs4wqqzv6i1hi2dqf8414mh5jwvrr")))

