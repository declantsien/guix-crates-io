(define-module (crates-io sf mt) #:use-module (crates-io))

(define-public crate-sfmt-0.1 (crate (name "sfmt") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sfmt-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06q2bmail7m577h8s1svgi5cpwhq6cllvfdg8a6ql0kqyna5hf4r")))

(define-public crate-sfmt-0.2 (crate (name "sfmt") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "03z93lbgqwrrrkr51idai715hkay7759pa8jgxr55a30fflr7zlb")))

(define-public crate-sfmt-0.2 (crate (name "sfmt") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "05vi0gcqdf1lxl98ff79h9116j0f64xprfwfj8n327hs0y83hd0p")))

(define-public crate-sfmt-0.2 (crate (name "sfmt") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fg1jn4vw15cpigxifarlmkcqfgcgiifr44yxkgd1s0gqmqgp4ql")))

(define-public crate-sfmt-0.2 (crate (name "sfmt") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d4mniw78c9zqjhvw52icarvc67v946l8k64r5k1kzqkhl9hp6ax")))

(define-public crate-sfmt-0.3 (crate (name "sfmt") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iicqydfmb29azb7mhbm1plb4wvr1gnsx7z5avn8b9k28v00xrdq")))

(define-public crate-sfmt-0.3 (crate (name "sfmt") (vers "0.3.1") (deps (list (crate-dep (name "packed_simd") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "12cvb4c2qnxnwzipcrr2bbc803f5m49y1srksaqm1is0h7rlpl8w")))

(define-public crate-sfmt-0.4 (crate (name "sfmt") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "04s4v8i5xiq78gcvknngdydlkxal0yrv8a4rhwngljab930iqaxg")))

(define-public crate-sfmt-0.5 (crate (name "sfmt") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "0s0irnin4xw7h52nm0ajz87zqx0b1bf4ldznfmpsdhylxv8nhyli")))

(define-public crate-sfmt-0.6 (crate (name "sfmt") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "02g39qfj8xf0nyjjwy942b79a2qbn1hd0y81vkak74g1536kk4i6") (features (quote (("thread_rng" "rand") ("default" "thread_rng"))))))

(define-public crate-sfmt-0.7 (crate (name "sfmt") (vers "0.7.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0rbmgk9r13j5g0i38lj2g1h9y5mpvy9djz3629bghf34ag0s72g3") (features (quote (("thread_rng" "rand") ("default" "thread_rng"))))))

(define-public crate-sfmt-sys-0.1 (crate (name "sfmt-sys") (vers "0.1.0") (hash "1g2qhqlj2g6kacq208cydzxavr0qhpszjmqc4nyay36jf65mmrdv")))

