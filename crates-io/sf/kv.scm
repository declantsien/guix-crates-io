(define-module (crates-io sf kv) #:use-module (crates-io))

(define-public crate-sfkv-0.1 (crate (name "sfkv") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "postcard") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1ap8p7f5lmbwmmcmnri1vhsls7s7map83vipdw5c39v67ynbzx95") (features (quote (("postcard-values" "postcard" "serde") ("default" "postcard-values"))))))

