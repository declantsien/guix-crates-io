(define-module (crates-io sf v-) #:use-module (crates-io))

(define-public crate-sfv-rs-0.1 (crate (name "sfv-rs") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1xsmc3jzyp9wd1yc22snds2fc2xdcyj1zmkj8hfg7mdd6r6ifakw")))

