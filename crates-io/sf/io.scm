(define-module (crates-io sf io) #:use-module (crates-io))

(define-public crate-sfio-promise-0.1 (crate (name "sfio-promise") (vers "0.1.0") (hash "1wn4yh7jqj3zpi4xwgp9nxg9w54hvhk95x6l42lrxi5hqyr36xkc")))

(define-public crate-sfio-promise-0.2 (crate (name "sfio-promise") (vers "0.2.0") (hash "1bwsqy6b84ihprn7bchqv3cv9g2ba9qsbkx705j1lmf1rbjpnm4i")))

(define-public crate-sfio-rustls-config-0.1 (crate (name "sfio-rustls-config") (vers "0.1.0") (deps (list (crate-dep (name "pem") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pkcs8") (req "^0.10.2") (features (quote ("encryption" "pkcs5" "pem" "std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "rustls-webpki") (req "^0.100.1") (default-features #t) (kind 0)) (crate-dep (name "rx509") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0bnf4sr5fnzh6byc353cia86mwrf6wzhv2gn3n290y5snmmmqpyx")))

(define-public crate-sfio-rustls-config-0.1 (crate (name "sfio-rustls-config") (vers "0.1.1") (deps (list (crate-dep (name "pem") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pkcs8") (req "^0.10.2") (features (quote ("encryption" "pkcs5" "pem" "std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "rustls-webpki") (req "^0.100.1") (default-features #t) (kind 0)) (crate-dep (name "rx509") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y6yfzn3r42dsk2hbrc231s3m6ablgnaarkvy43sscgjs03f50c7")))

(define-public crate-sfio-rustls-config-0.1 (crate (name "sfio-rustls-config") (vers "0.1.2") (deps (list (crate-dep (name "pem") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pkcs8") (req "^0.10.2") (features (quote ("encryption" "pkcs5" "pem" "std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "rustls-webpki") (req "^0.101.3") (default-features #t) (kind 0)) (crate-dep (name "rx509") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b9qgkbwckh1ggyqjq8fncnz3c1zhxx1nsp62mhkci0pnfn2spbm")))

(define-public crate-sfio-rustls-config-0.2 (crate (name "sfio-rustls-config") (vers "0.2.0") (deps (list (crate-dep (name "pem") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pkcs8") (req "^0.10.2") (features (quote ("encryption" "pkcs5" "pem" "std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "rustls-webpki") (req "^0.102") (default-features #t) (kind 0)) (crate-dep (name "rx509") (req "^0.2") (default-features #t) (kind 0)))) (hash "1n52pc78ppb322305qyqj1wsp5llk2v35br18j1cn4wpfsra8lqg") (rust-version "1.74")))

(define-public crate-sfio-rustls-config-0.3 (crate (name "sfio-rustls-config") (vers "0.3.0") (deps (list (crate-dep (name "pem") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pkcs8") (req "^0.10.2") (features (quote ("encryption" "pkcs5" "pem" "std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "rustls-webpki") (req "^0.102") (default-features #t) (kind 0)) (crate-dep (name "rx509") (req "^0.2") (default-features #t) (kind 0)))) (hash "02hv06m9lkp5iknx0jqda7xhc64pg200ipkkvx27r0sjzmpiiv7w") (rust-version "1.74")))

(define-public crate-sfio-rustls-config-0.3 (crate (name "sfio-rustls-config") (vers "0.3.1") (deps (list (crate-dep (name "pem") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pkcs8") (req "^0.10.2") (features (quote ("encryption" "pkcs5" "pem" "std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "rustls-webpki") (req "^0.102") (features (quote ("std" "aws_lc_rs"))) (kind 0)) (crate-dep (name "rx509") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f2pkpdvphj3rh86rspf0j3jy27lbgr5dwllqxw9ngkznfzllgg8") (rust-version "1.74")))

(define-public crate-sfio-rustls-config-0.3 (crate (name "sfio-rustls-config") (vers "0.3.2") (deps (list (crate-dep (name "pem") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pkcs8") (req "^0.10.2") (features (quote ("encryption" "pkcs5" "pem" "std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.23") (features (quote ("std" "logging" "tls12" "ring"))) (kind 0)) (crate-dep (name "rustls-webpki") (req "^0.102") (features (quote ("std" "ring"))) (kind 0)) (crate-dep (name "rx509") (req "^0.2") (default-features #t) (kind 0)))) (hash "1027hj49cyr525b1mza0jkq37zjww0srav016aw33wlcf6ldhpg7") (rust-version "1.74")))

(define-public crate-sfio-tokio-ffi-0.5 (crate (name "sfio-tokio-ffi") (vers "0.5.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0aa5nhs2nx6m9w9xbwqsz1xh14g254249v7pdqcrzq6siiikkymj")))

(define-public crate-sfio-tokio-ffi-0.5 (crate (name "sfio-tokio-ffi") (vers "0.5.1") (deps (list (crate-dep (name "oo-bindgen") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "142dxaw5jrycy98ax4d6f8khhq1acqmblxx0c9hsq04rxvn1s0dk")))

(define-public crate-sfio-tokio-ffi-0.6 (crate (name "sfio-tokio-ffi") (vers "0.6.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "08zxvgfr1spdhnkc6xc0x2zzmlw05hrjkcm6h6w3a5ln5wzqzjcj")))

(define-public crate-sfio-tokio-ffi-0.7 (crate (name "sfio-tokio-ffi") (vers "0.7.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.7") (default-features #t) (kind 0)))) (hash "06ig3chwsvrqnfsjsd79nsmknfwf8s6ibbg5imf42g6nc28045ja")))

(define-public crate-sfio-tokio-ffi-0.8 (crate (name "sfio-tokio-ffi") (vers "0.8.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.8") (default-features #t) (kind 0)))) (hash "11i2mly2bs2lnfk35yqx3n0vvvaxjvww31cvfjgdg89g1wsa2r3j")))

(define-public crate-sfio-tokio-ffi-0.9 (crate (name "sfio-tokio-ffi") (vers "0.9.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.8") (default-features #t) (kind 0)))) (hash "1jzyzqmys2nl56c3i4sbhdvgdqahq16v66iy1dy058i5wn7433gi")))

(define-public crate-sfio-tokio-mock-io-0.2 (crate (name "sfio-tokio-mock-io") (vers "0.2.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("sync"))) (default-features #t) (kind 0)))) (hash "11i56fnha1h3b1ds9nin9gd2whqh3ihd7p6rzygf3rk0f5mgm2r5")))

(define-public crate-sfio-tracing-ffi-0.5 (crate (name "sfio-tracing-ffi") (vers "0.5.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.6") (default-features #t) (kind 0)))) (hash "0ky2dbfysnj7kfi0v3d5nx067b3mm96x9w46pj72wzrxq3mjnn2f")))

(define-public crate-sfio-tracing-ffi-0.7 (crate (name "sfio-tracing-ffi") (vers "0.7.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.7") (default-features #t) (kind 0)))) (hash "09px68xqdf0mp6kg5jgq3pd3vwnb23599n5jvdfmfn95522dqibr")))

(define-public crate-sfio-tracing-ffi-0.8 (crate (name "sfio-tracing-ffi") (vers "0.8.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.8") (default-features #t) (kind 0)))) (hash "1d78icx1g3xnyj9r74w150qzc19cvf0jb1ky54r6yfs1vg15m5cl")))

(define-public crate-sfio-tracing-ffi-0.8 (crate (name "sfio-tracing-ffi") (vers "0.8.1") (deps (list (crate-dep (name "oo-bindgen") (req "^0.8") (default-features #t) (kind 0)))) (hash "0p6gj7kk8b2ysc688wqxrvl473w1cwqjr25qfj72jyra9k42kgp1")))

(define-public crate-sfio-tracing-ffi-0.9 (crate (name "sfio-tracing-ffi") (vers "0.9.0") (deps (list (crate-dep (name "oo-bindgen") (req "^0.8") (default-features #t) (kind 0)))) (hash "0dggmn78q640zxxxxyl3pmh39frz0vyw15d3yj3gw3lwrkkmgnca")))

