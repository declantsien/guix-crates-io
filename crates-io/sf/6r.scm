(define-module (crates-io sf #{6r}#) #:use-module (crates-io))

(define-public crate-sf6rs-0.1 (crate (name "sf6rs") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01z168j9x7ncghd187p2cnb5867ld3mxhwy2jngbrcapvisdb86b")))

(define-public crate-sf6rs-0.3 (crate (name "sf6rs") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09df6cgn8f9swh873yz3nrs3l2as5rk9q2ywzg7ca0pvagz7fx1q")))

(define-public crate-sf6rs-0.3 (crate (name "sf6rs") (vers "0.3.2") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f19n2mkagpgqq1xhw5v6i075v2vyqrbgw0v2p520ahyqwyqwj4h")))

(define-public crate-sf6rs-0.3 (crate (name "sf6rs") (vers "0.3.3") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0924hdkc31xx6absp8z5adzz9cxialzhy97f9sg9axbaqsz4mv12")))

