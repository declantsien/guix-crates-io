(define-module (crates-io sf lo) #:use-module (crates-io))

(define-public crate-sfloat-0.0.0 (crate (name "sfloat") (vers "0.0.0") (hash "055apviw13j1qirv2hldd5bs8fmxw65vz44y5wr8xqchxgzwp8ks")))

(define-public crate-sflow-0.0.1 (crate (name "sflow") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.30") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "1ls17vv8c2xidgf0rshlhv48ap5hi51dalkfa8j541pxhh36vzi7")))

