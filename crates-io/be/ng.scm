(define-module (crates-io be ng) #:use-module (crates-io))

(define-public crate-beng-beng-0.1 (crate (name "beng-beng") (vers "0.1.0") (hash "0lqvgqfs5ac8jslf512vn9cd6sj6rmbn247x6pswpxm0fwpn31a5") (yanked #t)))

(define-public crate-beng-beng-0.1 (crate (name "beng-beng") (vers "0.1.1") (hash "06s1rs65wbij88rmkflzw0r4z6i63b3if4fxl05gdils0iy79vs0") (yanked #t)))

(define-public crate-beng-beng-0.1 (crate (name "beng-beng") (vers "0.1.2") (hash "0l1kc7a2qlslbyhdyfx7mbkzqdld5d5754417xj288df7rd29pi3") (yanked #t)))

(define-public crate-bengbenge-0.1 (crate (name "bengbenge") (vers "0.1.0") (hash "0i0vlb8b3wqy1biy5sj5qii7n7q0s1hy1i6yln9w9p9jdxlcn023") (rust-version "1.56")))

(define-public crate-bengbenge-0.2 (crate (name "bengbenge") (vers "0.2.0") (hash "05lnw49z72vfm3zyij4gr2xm9kisyksbiph44140cngjnr9dj17j") (rust-version "1.56")))

(define-public crate-bengbenge-0.3 (crate (name "bengbenge") (vers "0.3.0") (hash "1qzvz2grk9ydrjd7c97a5ivi8h8xj4b3h21gb6yi2njp5qml022k") (rust-version "1.56")))

(define-public crate-bengbenge-0.3 (crate (name "bengbenge") (vers "0.3.1") (hash "0n2fdmajan6x5xf4sv11l20ff1q86vygl12simr4vzj3j42sipxg") (rust-version "1.56")))

(define-public crate-bengbenge-0.3 (crate (name "bengbenge") (vers "0.3.1-alpha.2") (hash "043qfnr1db5cs06w487riyhdq485nq77iq9kclksk7fwdhm9qmsw") (rust-version "1.56")))

(define-public crate-bengbenge-0.3 (crate (name "bengbenge") (vers "0.3.1-alpha.3") (hash "0mlk0vy0rapqbna63xvh1zhqbldqial3vl22iw7h2ajlh0v4lc2k") (rust-version "1.56")))

(define-public crate-bengine-0.1 (crate (name "bengine") (vers "0.1.0") (hash "0a5iv3zdpbww7vs96lcrjnvb19f0vq3ddi62xddaw56d2b6hffmk")))

(define-public crate-bengreen-0.1 (crate (name "bengreen") (vers "0.1.0") (deps (list (crate-dep (name "x86") (req "^0.7") (default-features #t) (kind 0)))) (hash "10dkm819sxqaxks103x3br0wv7fj1xgl184ayyxalbisqf22wka2")))

(define-public crate-bengreen-0.1 (crate (name "bengreen") (vers "0.1.1") (deps (list (crate-dep (name "x86") (req "^0.7") (default-features #t) (kind 0)))) (hash "0rpchdvg1gr6s5zaszi0rry20jxg2595xf4z8k8vc63hn9sbm05j")))

