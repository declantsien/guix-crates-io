(define-module (crates-io be rk) #:use-module (crates-io))

(define-public crate-berk-0.1 (crate (name "berk") (vers "0.1.0") (hash "0v3yvhgwf431dv9h5m9ldfrk28xc2n2kscvzg5qbpzq60yyyqb78")))

(define-public crate-berkelium-0.0.1 (crate (name "berkelium") (vers "0.0.1") (hash "1l0ayj6w5sxmk19zvw6ixgqg2cx4n7m1l2qbkwc9m0sl5m038lhx")))

