(define-module (crates-io be rr) #:use-module (crates-io))

(define-public crate-berries-0.0.0 (crate (name "berries") (vers "0.0.0") (hash "1gfn0m1n25z20v0mbxr46kqlzhf7ab686d87109mkaipygfac0kq")))

(define-public crate-berror-0.1 (crate (name "berror") (vers "0.1.0") (hash "19q19ww2bhcxpndhrwbwlckbjfjsn01mq36nvkpacp78k8c9c72q")))

(define-public crate-berry-cli-0.1 (crate (name "berry-cli") (vers "0.1.0") (hash "0v30xldy0fl3r1jsrk3mgb1s8r7z1ngyb68djj79sarl0gx5p2g0")))

(define-public crate-berry-db-0.1 (crate (name "berry-db") (vers "0.1.0") (hash "0d73jzjjgjhmck5dlbvbrgbfxcz3cj7d5brv1kzh0iv6z05wxdpj")))

(define-public crate-berry-fs-0.1 (crate (name "berry-fs") (vers "0.1.0") (hash "0gqigaq41az1yhi7b30p8rj9bslqz0rvkr0g0f1rj2avs66v7356")))

(define-public crate-berry-gui-0.1 (crate (name "berry-gui") (vers "0.1.0") (hash "0gvzfw9r8x0pk3a1ck1105m1ccgkd8q4skns1n0dc60k5dkkcrpw")))

(define-public crate-berry_kv-0.0.0 (crate (name "berry_kv") (vers "0.0.0") (hash "0hhnihq0j660yagchn4sxv78jnhnnf3rgcq0dkdb5d9hmbv176kv")))

(define-public crate-berryimu-0.1 (crate (name "berryimu") (vers "0.1.0") (deps (list (crate-dep (name "i2cdev") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0macmns0nbxkpdjslfvwa8mkxjqmfkil0sy3g3qb9abcj9h999gs")))

(define-public crate-berryimu-0.2 (crate (name "berryimu") (vers "0.2.0") (deps (list (crate-dep (name "i2cdev") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "spidev") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "1d6f7c7pkidnsw8jmgb07gynibmvifividhaacivhrwshc471k87") (features (quote (("spi" "spidev") ("i2c" "i2cdev") ("default" "i2c" "spi"))))))

