(define-module (crates-io be dl) #:use-module (crates-io))

(define-public crate-bedlam-0.0.0 (crate (name "bedlam") (vers "0.0.0") (hash "1a3356w3518hbqddjlfg634q91rn7x4q3knm7q24sggjv4gphv21")))

(define-public crate-bedlam-cli-0.0.0 (crate (name "bedlam-cli") (vers "0.0.0") (hash "0aa8jcpfn4gjif54x333dcz34p84d0bqg7yr0fzs859jxsgvcy57")))

