(define-module (crates-io be au) #:use-module (crates-io))

(define-public crate-beau_collector-0.1 (crate (name "beau_collector") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.11") (default-features #t) (kind 2)))) (hash "1a3lmv00w9vfy1nmmrgcydqb9z11hxk0q8k9v93frk58ya9dy6fq")))

(define-public crate-beau_collector-0.1 (crate (name "beau_collector") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.11") (default-features #t) (kind 2)))) (hash "1fmywlbypjaprw4ricz211k3apjmjg13868221x8kwx6p1prajg8")))

(define-public crate-beau_collector-0.2 (crate (name "beau_collector") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.11") (default-features #t) (kind 2)))) (hash "0bl8hain51frgy0w54rai35yma1m1p2s7b8kags20xw1lj8f6x4i")))

(define-public crate-beau_collector-0.2 (crate (name "beau_collector") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.11") (default-features #t) (kind 2)))) (hash "0kyzy2fnl7clzf4iblwkhak343p596ac6lavq4r3vg9wdl34789k")))

(define-public crate-beautician-0.1 (crate (name "beautician") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0haa5klfnslcfiywpb4s5q18kqp3k81lp1rm71m81g14pccmbqg5")))

(define-public crate-beautician-0.1 (crate (name "beautician") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0lfy0asr3iba952fg51xgpb6nkjjdw8kbm9rh900k5l6v7dfn0gj")))

(define-public crate-beautiful_output-0.1 (crate (name "beautiful_output") (vers "0.1.0") (hash "0f7l8a6ikmyidm130hvldpkmqj1vmz9kg3jrsw83s66h9q9h8isd")))

(define-public crate-beautiful_output-0.1 (crate (name "beautiful_output") (vers "0.1.1") (hash "1jqh3sd7p7lm90r8kg83h5mf664ylwmqkdcv9l6ygbcsiqzy1ivx")))

(define-public crate-beautiful_output-0.1 (crate (name "beautiful_output") (vers "0.1.2") (hash "0bdwdyxi9sdy2h0fvnd1qjzaq7v61z6b9iswscsz73rrknw10604")))

(define-public crate-beautiful_output-0.1 (crate (name "beautiful_output") (vers "0.1.3") (hash "0is1957l32wq4fx8ignqd3qnnz6c8rqa7h0j83fll2n7hny50rv1")))

(define-public crate-beautylog-0.1 (crate (name "beautylog") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "18iismpasbkmi513vca31y7dc3109wr0izm1ygk1sbx9jndsjmql")))

(define-public crate-beaux2d-0.1 (crate (name "beaux2d") (vers "0.1.0") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "02fwk3ds02xwap5gv51ysxvnan9ha2adr5n21p3gsqf81g7859m9")))

(define-public crate-beaux2d-0.1 (crate (name "beaux2d") (vers "0.1.1") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "1984a39r6wphn77wp5dsgi4wcj2racbrl19cg825ciy4fb3q8mnx")))

(define-public crate-beaux2d-0.1 (crate (name "beaux2d") (vers "0.1.2") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "11d5y6c8rqs2nyp62ndd6m2bsnzyawxnz5j77g67h1f1qj5pmnqg")))

(define-public crate-beaux2d-0.1 (crate (name "beaux2d") (vers "0.1.3") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "021pa1lr3jwi71kdzhw48v5lgdfv8r2zx4criwkjsnz0hz75sfg8")))

(define-public crate-beaux2d-0.1 (crate (name "beaux2d") (vers "0.1.4") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "1aav7h10dd5jra6q1qsxqshw2gb8kzci80bgkmzq28dchv9x6wr3")))

(define-public crate-beaux2d-0.2 (crate (name "beaux2d") (vers "0.2.0") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "1q9h7phqpmbsyfr9q5g9jiyhy9gdjab52svzpndh516izz1fgaff")))

(define-public crate-beaux2d-0.2 (crate (name "beaux2d") (vers "0.2.1") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "0mc90lfcws7knfjcmcl6yg1yq1458w8ad9s4g8xyc3ijv4mppk9x")))

(define-public crate-beaux2d-0.2 (crate (name "beaux2d") (vers "0.2.2") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "19lxv6j7cibm8q2n3lsknggp2kb4wd6y52mry41ribb6xgmjrvnl")))

