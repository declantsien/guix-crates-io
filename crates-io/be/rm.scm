(define-module (crates-io be rm) #:use-module (crates-io))

(define-public crate-bermuda-0.0.1 (crate (name "bermuda") (vers "0.0.1") (deps (list (crate-dep (name "dirs2") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s3fn5ihxr51x2sn7nvmcy9v9rcihagrnnpzkclpv9jniydn1sfc")))

