(define-module (crates-io be zi) #:use-module (crates-io))

(define-public crate-bezier-0.1 (crate (name "bezier") (vers "0.1.0") (hash "0xy1q2v21zjr9brf8pwzv4pg876m8zkrsn8nhpa8a466vbn78b9m")))

(define-public crate-bezier-interpolation-0.1 (crate (name "bezier-interpolation") (vers "0.1.0") (deps (list (crate-dep (name "freetype-rs") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hb8i68894pcs00bf389mnirnj1xxb69xf6q9rdysbm2vqzp0986") (yanked #t)))

(define-public crate-bezier-interpolation-0.2 (crate (name "bezier-interpolation") (vers "0.2.0") (deps (list (crate-dep (name "freetype-rs") (req "^0.7") (default-features #t) (kind 0)))) (hash "0w707kb3gsplkyd43nla6r5xs5fhp6g2q7sw04lfm11zihijk788") (yanked #t)))

(define-public crate-bezier-interpolation-0.3 (crate (name "bezier-interpolation") (vers "0.3.0") (deps (list (crate-dep (name "freetype-rs") (req "^0.7") (default-features #t) (kind 0)))) (hash "03l47wb5k094zvfn4rlc5q52k4nd88467ml11l37jkk5zx7jjk2r")))

(define-public crate-bezier-nd-0.1 (crate (name "bezier-nd") (vers "0.1.0") (deps (list (crate-dep (name "geo-nd") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1dp4zjy700mzjmkr310r1w7r2lwid3h1bsd3azx8w2r1dyqwvrv2")))

(define-public crate-bezier-nd-0.1 (crate (name "bezier-nd") (vers "0.1.1") (deps (list (crate-dep (name "geo-nd") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0vamgf917rhg93n28i24b4m5pqi4lms6p2dfzfznxif4vi0yp9md")))

(define-public crate-bezier-nd-0.1 (crate (name "bezier-nd") (vers "0.1.2") (deps (list (crate-dep (name "geo-nd") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1zdpnhbwhrnz89kcavhp1pslqx7wdamclirid7d042h19pzwf1rh")))

(define-public crate-bezier-nd-0.1 (crate (name "bezier-nd") (vers "0.1.3") (deps (list (crate-dep (name "geo-nd") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wwfdq3rxk0qlb57v9cgi3whjk4rxwhj4ajlkyk7bpx43yhxvx7i")))

(define-public crate-bezier-nd-0.1 (crate (name "bezier-nd") (vers "0.1.4") (deps (list (crate-dep (name "geo-nd") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1f81waghlpkfid8wywncpqqjfjlnyyaw2zh2r4gm3dl0n7am97pr")))

(define-public crate-bezier-nd-0.5 (crate (name "bezier-nd") (vers "0.5.0") (deps (list (crate-dep (name "geo-nd") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0sqpzq6193fi03bjnh0c74wim0mml6nhhf15xa4qdxwpn412ldc5")))

(define-public crate-bezier-rs-0.1 (crate (name "bezier-rs") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.22") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1l2sqhjdzz15v1b55qvn99n74iv25kzgdd69d7bizkqgqqp62dj8") (rust-version "1.65.0")))

(define-public crate-bezier-rs-0.2 (crate (name "bezier-rs") (vers "0.2.0") (deps (list (crate-dep (name "dyn-any") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.22") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ny8kgl0fp28209b8l8n9r7mivjb5dcmakqr6d38qch5y8v8s8nn") (rust-version "1.66.0")))

(define-public crate-bezier-rs-0.3 (crate (name "bezier-rs") (vers "0.3.0") (deps (list (crate-dep (name "dyn-any") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "1dgcnh3a3j45wxppj7bf0i9zzr13fdpagv8d5v562gmh2idmyvyd") (rust-version "1.66.0")))

(define-public crate-bezier-rs-0.4 (crate (name "bezier-rs") (vers "0.4.0") (deps (list (crate-dep (name "dyn-any") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "1pj1ax64kgqinxc45kgjq39yx4sbqscv7p0sz22gkqi68cqsmqxx") (rust-version "1.66.0")))

(define-public crate-bezier1-0.1 (crate (name "bezier1") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bp4kvwl46zjzv6d476fqnlkj1acpsbq7hg8i0wvyh56rcq4wzan")))

(define-public crate-bezier1-0.2 (crate (name "bezier1") (vers "0.2.0") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gccdqm3j1rxg19m3gnxkzd9j0s9s91gq1rk24gs3djnpsc4wlnw")))

(define-public crate-bezier2-0.1 (crate (name "bezier2") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "16jx9kypk8d5587dxjka5ybydggjycffv8dnf5b2iwrd8ydi1sc3")))

(define-public crate-bezier2-0.1 (crate (name "bezier2") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "17ri6gs5qxzyd9fk7p5j2b5fx59h5yp1649bhnj7f3kwkk57h62b")))

(define-public crate-bezier2-0.1 (crate (name "bezier2") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dz14b3r8yjrdadnpkdbas7vjpkfilmw2nrr2k4m15qlgif7wfmn")))

(define-public crate-bezier2-0.2 (crate (name "bezier2") (vers "0.2.0") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j403mzb3ijnqfivpg6y25qa9m2i7v86lkdl920az1y9crzy6ndh")))

(define-public crate-bezier_easing-0.1 (crate (name "bezier_easing") (vers "0.1.0") (hash "08p4l906kjs9ykm1a1jxnlm0jq26m5k00bjsh2xzjhhg9yqbjss0")))

(define-public crate-bezier_easing-0.1 (crate (name "bezier_easing") (vers "0.1.1") (hash "1pdy4s2nyry7s5b1c5b6z4bjmg87w1lzj4s2skfbcklcl95p790g")))

(define-public crate-beziercurve-wkt-0.1 (crate (name "beziercurve-wkt") (vers "0.1.0") (deps (list (crate-dep (name "quadtree-f32") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1asi9h5ysy2w7gfcynzpa3wddq0ggrsxvnym3kbal42sykr4hzpr") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-beziercurve-wkt-0.1 (crate (name "beziercurve-wkt") (vers "0.1.1") (deps (list (crate-dep (name "quadtree-f32") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fv50y5mky5ca024lsc2n1p79dl1mpw4my237szk8prssm5kyab1") (features (quote (("serialization" "serde") ("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-beziercurve-wkt-0.1 (crate (name "beziercurve-wkt") (vers "0.1.2") (deps (list (crate-dep (name "quadtree-f32") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1p1zwwga8dn7xarh9x8y2fnsamjidj01l7gkmpznzpr4flrq841m") (features (quote (("serialization" "serde") ("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-beziercurve-wkt-0.1 (crate (name "beziercurve-wkt") (vers "0.1.3") (deps (list (crate-dep (name "quadtree-f32") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1b1lyidja97czm86my18g4g7vbiwzmnpbas0ajqg81gfrxlbgxl8") (features (quote (("serialization" "serde") ("parallel" "rayon") ("default" "parallel"))))))

