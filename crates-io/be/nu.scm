(define-module (crates-io be nu) #:use-module (crates-io))

(define-public crate-benu-0.0.1 (crate (name "benu") (vers "0.0.1-rc0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0hxicsrv9941cis7d967r1x8qgkm1qrp2v0sggc9z3cdj4rq2dgv") (features (quote (("salt" "rand") ("default" "salt" "debug") ("debug"))))))

(define-public crate-benu-0.0.1 (crate (name "benu") (vers "0.0.1-rc2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1v8aazh885pmpma4593ln47n12imchk60pwkhqsjr0g9km3kgxzp") (features (quote (("salt" "rand") ("default" "salt" "debug") ("debug"))))))

