(define-module (crates-io be ar) #:use-module (crates-io))

(define-public crate-bear-0.1 (crate (name "bear") (vers "0.1.0") (hash "0ifk23lgdhhi5s2mw4npkrv7fckbwxshnfkcsplbbwkq8gshr51w")))

(define-public crate-bear-0.1 (crate (name "bear") (vers "0.1.1") (hash "0nq64ffgcbaickalpbv237vz4cimpgx1zf12ibppqahdpcgcwg0s")))

(define-public crate-bear-0.2 (crate (name "bear") (vers "0.2.0") (hash "04bih4pl3di6q2q7x0w3riif2f457afgwjj4n8416a56a7ak4cq9")))

(define-public crate-bear-0.2 (crate (name "bear") (vers "0.2.1") (hash "13sr4r83z34v0jc6xy6a7h819n7dz6r3fvm6gdqi58159hxprxxn")))

(define-public crate-bear-0.2 (crate (name "bear") (vers "0.2.2") (hash "13hkylpdc4658wrv2hpg8v79pbqvaic5kmfnn39x68r23vi4p367")))

(define-public crate-bear-lib-terminal-0.9 (crate (name "bear-lib-terminal") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "17k007y54szf48wcgr9jc8gnx7dbyh297mds2gws7y9fw34vl9gx")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.0.0") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0b2qrjmfnvnb5bmz98dpvvqra6mfi1wz82dhbfxn66ab2725dcv1")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.0.1") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "102jdz15yznjkpji1ixkc8n6cl3xx3mzry0vm07v8lybvnc705b8")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.1.0") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1ihn61gps8a7jyy09lhxh8blmhkmdhdn87y60rahwmc8msanyrhs")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.1.1") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "08gxcdx527hxk2j71l9c6lwzigx65nql7116qya0304gawlwl5q2")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.2.0") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1fsihgap8gx4rk7j83q0js2g9ifskzb3wvnly257ifpzffm072v4")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.3.0") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "07qz5ydgihd3ygs2iavcvy0zv8f7b4j8nqja9hqqijmq2j52vz68")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.3.1") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0ldid02wv2wj63x1019agcny6khdi1b24i2a2bvi1w7kvf3s10c5")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.3.2") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1xl6b4jbln9pzbpihfpkjlq8dki80b9jyq5yfx3lcc5i18dpkf0f")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.3.3") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.2") (default-features #t) (kind 0)))) (hash "046fl78i8b04mxspvsqlm0fk9c0zn0d449265y83cb1jgzssp0h3")))

(define-public crate-bear-lib-terminal-1 (crate (name "bear-lib-terminal") (vers "1.4.0") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.3") (default-features #t) (kind 0)))) (hash "0rz7f9h06sky00fnmdkp19lp8iij1c3agfw35g6sya9i9rma2w4r")))

(define-public crate-bear-lib-terminal-2 (crate (name "bear-lib-terminal") (vers "2.0.0") (deps (list (crate-dep (name "bear-lib-terminal-sys") (req "^1.3") (default-features #t) (kind 0)))) (hash "05kij0gr0i0phikh1qc5mdgi73f3ly9x1fhy3snzizm3jpv4jm9i")))

(define-public crate-bear-lib-terminal-sys-1 (crate (name "bear-lib-terminal-sys") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "11kzhbqsfr0aq4lrkyl7srj3ymdhfqhi2kby0284mvqbp28vn0q9")))

(define-public crate-bear-lib-terminal-sys-1 (crate (name "bear-lib-terminal-sys") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0rkd52b5lpk57wcsrh5j09x6lz095s44ax8flyjbshqlvrqpia43")))

(define-public crate-bear-lib-terminal-sys-1 (crate (name "bear-lib-terminal-sys") (vers "1.1.1") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0ivglshqd4lpgg4spb4kkwfqf8c8mqw35cfrik5wlhifmjcb90v7")))

(define-public crate-bear-lib-terminal-sys-1 (crate (name "bear-lib-terminal-sys") (vers "1.2.0") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "05rvqwkic7qsm6a2as1nrzgl3ip9biap3547vpsw3h7hxci4r91g")))

(define-public crate-bear-lib-terminal-sys-1 (crate (name "bear-lib-terminal-sys") (vers "1.2.1") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1lh3cm8dw99hsrs2mwgssygxqlw00wsiyf90dksiqp5fc93izi3s")))

(define-public crate-bear-lib-terminal-sys-1 (crate (name "bear-lib-terminal-sys") (vers "1.2.2") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1b0qiczrrk1imysxb3vq8jmmmlhjaycf66kfrhyjd5nmjsczc7a5")))

(define-public crate-bear-lib-terminal-sys-1 (crate (name "bear-lib-terminal-sys") (vers "1.3.0") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "17r71abbdz4l0vkwwv9p3lsmvy21sai8xabz2wl73w025hxwh11s")))

(define-public crate-beard-0.1 (crate (name "beard") (vers "0.1.0") (hash "1xrrg78ia6ysny2d6qz5x9jdvlpqkp11ns99vjmw8b8v79al5c5w")))

(define-public crate-beard-0.2 (crate (name "beard") (vers "0.2.0") (hash "10v52dflms4h0ag88zpihxnb57g22n84378rsx0dxbavx08psrfa")))

(define-public crate-beard-0.2 (crate (name "beard") (vers "0.2.1") (hash "0fy6cbdwjy91mhdv7hdb3nq6vp0nh0p2sr4gf2f5hcj06wh265wj")))

(define-public crate-bearer-0.1 (crate (name "bearer") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "tilde-expand") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0ixk4gqvp0x687a7pc0hym6njfnd86cvpvkw7zd7pnnf9r8kqaz2")))

(define-public crate-bearer-0.2 (crate (name "bearer") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "tilde-expand") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "106lfsxk41k15hzyq59zwza3xhc4szl6jb86qp2i77avaj4rpwgs")))

(define-public crate-bearer-0.2 (crate (name "bearer") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "tilde-expand") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0y92xhzlbq18i9ypinjmxqfjwbpm40fypqi9i9r6rqwikms0c4gw")))

(define-public crate-bearer-0.2 (crate (name "bearer") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "tilde-expand") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1vh37x1m0zgx2fxmmc73chndmxd9jcgnxrixcsc4iz4322cgmjir")))

(define-public crate-bearer-0.2 (crate (name "bearer") (vers "0.2.3") (deps (list (crate-dep (name "cabot") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "tilde-expand") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "040f8a3vq2pl47rdfjjmf1d745wy91y6ynlfkf9rd7lxizwc18qr")))

(define-public crate-bearing-0.0.0 (crate (name "bearing") (vers "0.0.0") (hash "0hhdkhg1icc34bllqbmayckmzp7zy5x709yjrlqy1ywrvfmdglri") (yanked #t)))

(define-public crate-bearings-0.0.0 (crate (name "bearings") (vers "0.0.0") (hash "12msmjfhzvqig0021xq7v7vzlpipjfgwfch82nnfb94d3qc261z9")))

(define-public crate-bearings-proc-0.0.0 (crate (name "bearings-proc") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lkyvah9pz587cxfnaq390bgwgfhx0pdkqgvnjim5l4m0vcqxgyx")))

(define-public crate-bearnote-0.1 (crate (name "bearnote") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("suggestions" "color"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1hmg3gglipmml3k909q7q3nvhd34zyzghdc69yvmcclgyqz520d9")))

(define-public crate-bearssl-0.0.1 (crate (name "bearssl") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.31.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "0q6sxcznyqjnqs51y9dj7z6ip64fzhw0jhwsxvm3w4ni31f08cqi")))

(define-public crate-bearssl-sys-0.0.1 (crate (name "bearssl-sys") (vers "0.0.1") (hash "096s76drblsbdhwghx3s93yl42gngqkisjxnjv7w0nr4yckmlhyy")))

(define-public crate-bearssl-sys-0.0.2 (crate (name "bearssl-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rust_c") (req "^0.1.0") (features (quote ("build"))) (default-features #t) (kind 1)))) (hash "1qfpij40w7k12fr9p35wyy98d6085lf4i0nzqpdy6ryjh6yzqlwm")))

(define-public crate-bearssl-sys-0.0.3 (crate (name "bearssl-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zl4z087d44l7zdi3iflpb6lqcv2fmr2pi3z1jfrgzi664dwj50g")))

(define-public crate-bearssl-sys-0.0.4 (crate (name "bearssl-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gh2lvmfwndx0k7424wiisqsbq7f2z783267r1q8qbfzwxxw7kkx")))

(define-public crate-beary-0.1 (crate (name "beary") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^2.1.0") (features (quote ("sqlite" "chrono" "uuid" "serde_json" "r2d2" "64-column-tables"))) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.10") (default-features #t) (kind 0)) (crate-dep (name "rust-texas") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.3") (default-features #t) (kind 0)))) (hash "0zbiq5v647g2rg1galnhpw536c8pys5y5x0hg5vmhnlrz0yy8348")))

