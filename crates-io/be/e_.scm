(define-module (crates-io be e_) #:use-module (crates-io))

(define-public crate-bee_code-0.2 (crate (name "bee_code") (vers "0.2.0") (hash "1n1hh1l2dsrf7hkqm23bwqb4w4a4dlrh5rvrqq9c7s9mv8ns4d9m")))

(define-public crate-bee_code-0.3 (crate (name "bee_code") (vers "0.3.0") (hash "04m8xa41mvqf36y4pdxg3ljhbrc3s5mfrbbksfnnm2whrfh7czbr")))

