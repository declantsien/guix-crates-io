(define-module (crates-io be si) #:use-module (crates-io))

(define-public crate-besida-0.1 (crate (name "besida") (vers "0.1.0") (hash "1r9hhj5gn4gxkbrlknn4by6rq0ww833hgxhx9w4ryskw0fzb55lk")))

(define-public crate-besida-0.1 (crate (name "besida") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1ihsk7ayli0qak8s5cbq69xjdjpz9sxzki50sn9zzy42k9x7yjmk")))

