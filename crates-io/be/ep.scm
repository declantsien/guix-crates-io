(define-module (crates-io be ep) #:use-module (crates-io))

(define-public crate-beep-0.1 (crate (name "beep") (vers "0.1.0") (deps (list (crate-dep (name "dimensioned") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "0n7an4z42rqksj406ahdhd62vscgmc6bfba3c2vw40pksdan8ikh")))

(define-public crate-beep-0.2 (crate (name "beep") (vers "0.2.0") (deps (list (crate-dep (name "dimensioned") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.2") (kind 0)))) (hash "0hlqxpfsmncg8346cq6rzf4z4y6ln9lfbx7j9ks7gqsn0js4afc5")))

(define-public crate-beep-0.2 (crate (name "beep") (vers "0.2.1") (deps (list (crate-dep (name "dimensioned") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.2") (kind 0)))) (hash "0g0iq122g5czbdcfifpff3g4lhmjx1gvki043nr3a48ybcsfc679")))

(define-public crate-beep-0.3 (crate (name "beep") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (kind 0)))) (hash "0nmx061sy2cbdjh2dq4rszsib271qq0vw13gd4jyaagswsw9mndd")))

(define-public crate-beep-evdev-0.1 (crate (name "beep-evdev") (vers "0.1.0") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "01jmdk8zra8v9kdvh8z9n3cn0pz6idk0j72xqbmsa2al6237mlwm")))

(define-public crate-beep-evdev-0.1 (crate (name "beep-evdev") (vers "0.1.1") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1zps770dy48q03zagz4fim4as20ij585nd2hfw1r9hi0sx4q0lvh")))

(define-public crate-beep-evdev-0.1 (crate (name "beep-evdev") (vers "0.1.2") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0g325i333ix0hjf9xgl4a6izfinh984h1gsdz3ip6v8y42wcvicp") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.2 (crate (name "beep-evdev") (vers "0.2.0") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0sggpqqcajch5sm2zc07lzny8ms3ycq66mfhn68p55dqiyz30qrf") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.3 (crate (name "beep-evdev") (vers "0.3.0") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0iw3dzgrqwchn5kiskr5312klv629l7d033kjrci8d2a012iq9zp") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.3 (crate (name "beep-evdev") (vers "0.3.1") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "053q9sxcgnqjpab87lykffvw0bg7mrgphp1v884kiqyrmcia4pi5") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-beep-evdev-0.3 (crate (name "beep-evdev") (vers "0.3.2") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "15nbp4aanw73icgyc5p28dckf2sfdij9cpl6n693q9b7ysjbpalm") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-beep-proto-0.0.1 (crate (name "beep-proto") (vers "0.0.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1229606kqgyw8nhznr0003f5wnacma4ph92mp84m6fw22rpfwb90")))

(define-public crate-beep-proto-0.0.2 (crate (name "beep-proto") (vers "0.0.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "foreign-types-shared") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.16") (default-features #t) (kind 0)))) (hash "0vinqng45f4bs94pq2mv6fg58cn1qpsb59l06fh7c3qppgqgdw51")))

(define-public crate-beepboop-0.1 (crate (name "beepboop") (vers "0.1.0") (hash "1zncykkd5p7avwvd651zh0i4pnpd5pg7bshz8pa4ahvfk6i7v7wm")))

(define-public crate-beeper-0.0.0 (crate (name "beeper") (vers "0.0.0") (hash "1qla876caaq0fqj9w81p2spgbnvnckdriffd718gi91n3139g2m6") (yanked #t)))

(define-public crate-beepy-display-0.1 (crate (name "beepy-display") (vers "0.1.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "framebuffer") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1njxnfshnys7zalja7vvaakdpjaxzdfaa9g078ipfyvcgbdy20jr")))

(define-public crate-beepy-display-0.1 (crate (name "beepy-display") (vers "0.1.1") (deps (list (crate-dep (name "embedded-graphics") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "framebuffer") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1wf2hjia12z6fmlrwq1b8nfkyz48hgx6mdznhnsj2sdz7cgz3xv0")))

