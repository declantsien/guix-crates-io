(define-module (crates-io be ta) #:use-module (crates-io))

(define-public crate-betabear-0.1 (crate (name "betabear") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1.48") (default-features #t) (kind 0)))) (hash "09xqgcxvp8c1x09y8yi4353s38ps1vkii2x7zb2kws14vvllknwa")))

(define-public crate-betabear-0.1 (crate (name "betabear") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.1.48") (default-features #t) (kind 2)))) (hash "1k9iidqm777i464pl58zh5js0217x1ihm0ddlcqdfz1q256nlnsi") (features (quote (("unstable"))))))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1jvgyy9lnr5ab9qg2ns0wsdrcbxli9jmwbpzidlrn1zhdqs26sc7") (yanked #t)))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "13a9i083jdddprzqcj944sjymw2kghaxinh9k0ys6gwzjyl9pj13") (yanked #t)))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0cmsmfxlfq4xbg94claqgbplnzxr6v75kxbbvrpfq97ldzximll7") (yanked #t)))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "17b3sc8aq7y0glyc8ym32gcjzmycclfx0djy23g7x1b3nan8l8cc") (yanked #t)))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0ag6s5jjkwpgn56c6hg13m98fw3cpgm9gbkhax1xkax4pfnz529h") (yanked #t)))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1zcmymn0l03cf1s00ximvla5285lhx52nahxrgicd0s6xw02zhp6") (yanked #t)))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1b47dyajxz4q8wp9z424ghbga7d7qq52clh4fy641vadmy23rzzm")))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1l2vi8gdr3bcrw7hgq6kbpvsxbfphzn1qsplinbbvkhs33rwb7b3")))

(define-public crate-betacode-0.1 (crate (name "betacode") (vers "0.1.8") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1qglwysr1jzhixw4f0dsqfa3pz2m2ny9a9drqip86ybjj51d0i8p")))

(define-public crate-betacode-0.2 (crate (name "betacode") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0msdva1yps2cg8bcb12f54vxhpn3ffmylq20ycycmcai8vd87xis")))

(define-public crate-betacode-1 (crate (name "betacode") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0ldbmbwbxrs10nz26p7vc8yb2kkf396rkxsr7ah4qliq4b3cqa67")))

(define-public crate-betacode-1 (crate (name "betacode") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0q48wak04zszrrk1f4zyavj6x46mzc3fkckq1ypfsnn1sqhqaxzh")))

(define-public crate-betacode-1 (crate (name "betacode") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1712v28iayxi1ldml4d5l9sb8xn0045hh4ybrzir6qjfmyiglg57")))

(define-public crate-betacode-1 (crate (name "betacode") (vers "1.0.3") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0glmj3ybfj3h7p524ykzwpanw6s8ld7fk8as7dp8lxma9jd58jy7")))

(define-public crate-betacode-1 (crate (name "betacode") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1lw9qiphrg1ww6x1shn32ai9bgmznicpa81anr1b96qnms2j049m")))

(define-public crate-betacode-1 (crate (name "betacode") (vers "1.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1n7fwnychhw79y7bncnhzgc32m0dz6yz3piymglkdqcb9dqp5905")))

(define-public crate-betacode2-0.1 (crate (name "betacode2") (vers "0.1.0") (hash "1mvpm6i695bz92m7w1w0ax5is7zc9bd0ykxa79pdi3hlm0gz2cs8")))

(define-public crate-betacode2-0.1 (crate (name "betacode2") (vers "0.1.2") (hash "0mgl7amxz8h86r7bg839fpqqz9chm6879hk7vpiv9rjzfqj42f7c")))

(define-public crate-betacode2-0.1 (crate (name "betacode2") (vers "0.1.3") (hash "0zp7j6r6i3bfq0id3hc9aymdxvmic655sn7h4r8awyfqg68kjxv4")))

(define-public crate-betacode2-0.1 (crate (name "betacode2") (vers "0.1.4") (hash "1zy7mvzmcyv6k6jbps3vlyd3wfb5s3glssywady5r307q9zmxh4l")))

(define-public crate-betaconvert-0.1 (crate (name "betaconvert") (vers "0.1.0") (deps (list (crate-dep (name "betacode") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04q1zl6hwlsyq1zm3c9xr92xjypi31jf0n01a6lzi8l3dh815khz")))

(define-public crate-betaconvert-0.1 (crate (name "betaconvert") (vers "0.1.1") (deps (list (crate-dep (name "betacode") (req ">=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xalq5z5q0l1j71sxpvbvm7dhcv9vnzpa7kyxy4rdcf41zqsycw9")))

(define-public crate-betaconvert-0.1 (crate (name "betaconvert") (vers "0.1.4") (deps (list (crate-dep (name "betacode") (req ">=1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0k72dmnx73x59hkcqasg30nf19ixk6vxiqhb0kya8i4r8qma6bia")))

(define-public crate-betadin-0.1 (crate (name "betadin") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer" "unicode"))) (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9") (default-features #t) (kind 0)))) (hash "0fmw23q6s286h8n5hrnw787biaq7d1xzf81bq42kjjxf86rdb2sf")))

(define-public crate-betadin-0.1 (crate (name "betadin") (vers "0.1.1") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer" "unicode"))) (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9") (default-features #t) (kind 0)))) (hash "1slmxaz71969k5w1d4r4g9ig8a3zbxzvy43a4s631fi867bqvjag")))

(define-public crate-betadin-0.1 (crate (name "betadin") (vers "0.1.2") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9") (default-features #t) (kind 0)))) (hash "04mnaszkb7dcf9z1rl765x2q52blfg3x1zd4h3bqga5h5zkabadz")))

(define-public crate-betadin-0.2 (crate (name "betadin") (vers "0.2.0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9") (default-features #t) (kind 0)))) (hash "0f1zqfhgpxbw3w885h1kzgkkbp3iygbj8d6hynnsmvgia182sdrr")))

(define-public crate-betareduction-0.1 (crate (name "betareduction") (vers "0.1.0") (deps (list (crate-dep (name "aleph-syntax-tree") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17makr0hirjl4zd9084lpsp4kdspbcihmfk80v6z2wad96j7wr49")))

