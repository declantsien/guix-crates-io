(define-module (crates-io be ag) #:use-module (crates-io))

(define-public crate-beagle-0.1 (crate (name "beagle") (vers "0.1.0") (hash "062517b08lnyfy310xc5k4z2fwx52jf7w787j9lapx1lyn92lm6v")))

(define-public crate-beagle-0.1 (crate (name "beagle") (vers "0.1.1") (hash "1wkcqlabl1pc0b1yz44v5grblrnvdgmf5ml5s99ghb521ccc16l9")))

(define-public crate-beagle-0.1 (crate (name "beagle") (vers "0.1.2") (hash "0vjiqrmqy58i1p2831flv8yg4rhbk945h4d5cwmb2p9py7gwbyqn")))

(define-public crate-beagle-0.1 (crate (name "beagle") (vers "0.1.3") (hash "1z6sfdy6hiyiqbnh2kwihy3yxc1fyi44hil7z7nq4wdk4413lg2b")))

(define-public crate-beagle-0.1 (crate (name "beagle") (vers "0.1.4") (hash "0qnvgzg0hhlri9ch0yngbr7rfgqfx3qnpdz6lqn0c46gb1zid18f")))

