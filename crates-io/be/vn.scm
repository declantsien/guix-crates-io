(define-module (crates-io be vn) #:use-module (crates-io))

(define-public crate-bevnet-0.1 (crate (name "bevnet") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "const-fnv1a-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jxyvk7rma5b7hda0fcnr9sym0xmy3cncnw762scpn5wz4q550wc")))

