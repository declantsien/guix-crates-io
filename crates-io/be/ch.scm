(define-module (crates-io be ch) #:use-module (crates-io))

(define-public crate-bech32-0.1 (crate (name "bech32") (vers "0.1.0") (hash "03cc75qnvkjwap39m12xjm282mbn7v3pcmliz46hxl8glqdalsgk")))

(define-public crate-bech32-0.2 (crate (name "bech32") (vers "0.2.0") (hash "1hib525b32g5m766w43si73s124mbl8j75jdp0gag4sfgdimp10h")))

(define-public crate-bech32-0.2 (crate (name "bech32") (vers "0.2.1") (hash "18k1sa2hdy8n5cb5dpiknap6scdrjlphidfnivv5qs852h1mxay1")))

(define-public crate-bech32-0.2 (crate (name "bech32") (vers "0.2.2") (hash "0mn5gixgzihsqx7w7cq0s8lmgkn286i7mscl9zxz3ifmabfynm8r")))

(define-public crate-bech32-0.3 (crate (name "bech32") (vers "0.3.0") (hash "0hyg8kf5z55igl0g11dd7xn8dgwzh3b1hyf7y78f3q7j6a1zf2gb")))

(define-public crate-bech32-0.3 (crate (name "bech32") (vers "0.3.1") (hash "0p5picwr6k7z64xrkr89bnk45nqb6g2qrmqir5krk8whyxx7vnbm")))

(define-public crate-bech32-0.3 (crate (name "bech32") (vers "0.3.2") (hash "16s6mapr2v5fjq4rpaj2c6ij70rx0h61lm536822366vlawj8frl")))

(define-public crate-bech32-0.4 (crate (name "bech32") (vers "0.4.0") (hash "0a6knjnv7flgh339mzgnpi2aa9krgiww9vhn6mjj1in8gm3vw58h")))

(define-public crate-bech32-0.4 (crate (name "bech32") (vers "0.4.1") (hash "1nnj2xg2q2kjzm4y5syjfqzl9wwd7lzfzr1rj35452ipbjzsj224")))

(define-public crate-bech32-0.5 (crate (name "bech32") (vers "0.5.0") (hash "1n5ysyssijbljwlq1awv811k696x2fp9vzn7c0whqq8nzl3vj85d")))

(define-public crate-bech32-0.6 (crate (name "bech32") (vers "0.6.0") (hash "1760rs8cy9zbai3628dlwspim8r4s7lxdq42469drabaa526152q") (features (quote (("strict"))))))

(define-public crate-bech32-0.7 (crate (name "bech32") (vers "0.7.0") (hash "0l2kg44m0vpl0jv11q5pj2nq94wcgb8c0z8509ngcsi085m6aziy") (features (quote (("strict"))))))

(define-public crate-bech32-0.7 (crate (name "bech32") (vers "0.7.1") (hash "1wjxvll33x51713fgrr321dwc2izj5lzf8xbanyg5impbb1qj04y") (features (quote (("strict"))))))

(define-public crate-bech32-0.7 (crate (name "bech32") (vers "0.7.2") (hash "170y6lnmhxy710fsyp2x6myaycxmiaa0k46i3h47lyd7gfxngkyd") (features (quote (("strict"))))))

(define-public crate-bech32-0.7 (crate (name "bech32") (vers "0.7.3") (hash "1lai6xi1li8y30kx9icscmi2j6s6j323sy9hfdzm1fbgz4svxard") (features (quote (("strict"))))))

(define-public crate-bech32-0.8 (crate (name "bech32") (vers "0.8.0") (hash "1m436w6iaxsgbafzprxbcp57kyaf9zy0vxk0p5f5wvr5pjb70zvc") (features (quote (("strict"))))))

(define-public crate-bech32-0.8 (crate (name "bech32") (vers "0.8.1") (hash "12xzbgh12v3wnv5gc4rnr6vyylyghc9xhxzp9b3ib7v3znxz17yg") (features (quote (("strict") ("std") ("default" "std"))))))

(define-public crate-bech32-0.9 (crate (name "bech32") (vers "0.9.0") (hash "1bb696isv6h302dxvppyapmirq147zdwbmgi3r8fn3hvavkqnwy5") (features (quote (("strict") ("std") ("default" "std"))))))

(define-public crate-bech32-0.9 (crate (name "bech32") (vers "0.9.1") (hash "0igl565rfpxwbh0g36cb7469sjkiap8yd21kcr0ppi2jfbwr6syq") (features (quote (("strict") ("std") ("default" "std"))))))

(define-public crate-bech32-0.10 (crate (name "bech32") (vers "0.10.0-alpha") (hash "16qax8qybh3mdcr3digzaz52l3fz5a7dlbl0aa6agr959kn1vk41") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bech32-0.10 (crate (name "bech32") (vers "0.10.0-beta") (hash "1sj5knjx2zrklalzhp8nw6s6zx8miva72d4hbh5ny6kqnb9fxxwq") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bech32-0.10 (crate (name "bech32") (vers "0.10.0-beta2") (hash "0lack53fp5s67dkw0jlk90r9d64nybqw6zrbf932sy2jhq700xa1") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-bech32-0.10 (crate (name "bech32") (vers "0.10.0-beta.2") (hash "02608jqawsfhhqri2ssiyvfbwrlrnxarnqkk64ngk2i3280my3nz") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-bech32-0.10 (crate (name "bech32") (vers "0.10.0") (hash "189y1lwmw4q0gmn2p39bkiszj0pxkx8vb8iig17m3h24mv2db70g") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-bech32-0.11 (crate (name "bech32") (vers "0.11.0") (hash "07cmbj8ynqpj01wzj4pgj27i2c6n66gf8zma8k6xxdz3jrhl8rfr") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bech32-no_std-0.7 (crate (name "bech32-no_std") (vers "0.7.3") (hash "0vj1dm1vjh88ph0yaaala821k7wax146pkk10gbggsqpv4cg0mj7") (features (quote (("strict") ("std") ("default" "std"))))))

(define-public crate-bech32-utils-0.2 (crate (name "bech32-utils") (vers "0.2.0") (deps (list (crate-dep (name "bech32") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "02911a5wj20sn4a1wam6gx4a176d3pc51r4f99jiqldy3yvs1mvs")))

(define-public crate-bech32grs-0.10 (crate (name "bech32grs") (vers "0.10.0-beta") (hash "0i7hg08gh2yzhgd4ang08jayz8ik9j297fk85n6wy5xyiynhl6af") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bech32grs-0.11 (crate (name "bech32grs") (vers "0.11.0") (hash "12s9m0wah3y947rihqwbzj8ixkqqrnzpl7jnk6z7znrd04frwgjs") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-bech32m-0.0.0 (crate (name "bech32m") (vers "0.0.0") (hash "1ffa6yiq7x9zvidavbkfh261ns7knh5m4rvd7j9y34wwr5gcj87b")))

