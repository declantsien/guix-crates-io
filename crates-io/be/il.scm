(define-module (crates-io be il) #:use-module (crates-io))

(define-public crate-beil-0.1 (crate (name "beil") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.28.3") (default-features #t) (kind 0)) (crate-dep (name "symbolic") (req "^8.6.0") (features (quote ("demangle"))) (kind 0)))) (hash "1mjdvvs4bypkdyrx2c9vhzkc96y1x3kdi8r2kdbmr26gc1m0nlkg")))

(define-public crate-beil-0.2 (crate (name "beil") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.28.3") (default-features #t) (kind 0)) (crate-dep (name "symbolic") (req "^8.6.0") (features (quote ("demangle"))) (kind 0)) (crate-dep (name "uuid") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1b2i5vh6w5543d40v0wg80sn2v55p8bkb1ac85px82qng2zp10ah")))

