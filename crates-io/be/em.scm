(define-module (crates-io be em) #:use-module (crates-io))

(define-public crate-beemovie-0.1 (crate (name "beemovie") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0g4x9nbgkwx42zzwbrai5wffmjsc5qymlqvrc18r2ywcqxrgwcjp")))

(define-public crate-beemovie-0.1 (crate (name "beemovie") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "00hzqnwghisifqns59lyvbjrwa77cziih6wgiwfrbx5alr1p7m7m")))

(define-public crate-beemovie-0.1 (crate (name "beemovie") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1rfwh4wx9lyvrhnh6i34prcrpcghha09x35di7algf2ls4f1rw3w")))

(define-public crate-beemovie-0.1 (crate (name "beemovie") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0xsafdwgkq3qx53waly2ws27rq17abzdcmdkmhhx394ppgiqcm4s")))

(define-public crate-beemovie-0.1 (crate (name "beemovie") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0p5fhyzhcc0qb3wn43nkxzq2zw6x8mfss8wl7gkwjnrvk5piv03m")))

(define-public crate-beemovie-0.1 (crate (name "beemovie") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0qx9h21yrsvx150nnnp8zn9zl70g5g27bcykdnvmj3x4b5gyifx6") (yanked #t)))

(define-public crate-beemovie-0.2 (crate (name "beemovie") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "11y8sqg52iv6mkpw4y7qgscr3p72xwdjrgvhrb36398i0j035cqc")))

(define-public crate-beemovie-0.2 (crate (name "beemovie") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1dwmr4vay8m9bg5zyfy747m7i7gbbnk4sz2wq2y68z3x97k15mk2") (yanked #t)))

(define-public crate-beemovie-0.2 (crate (name "beemovie") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "04d6n6ybgzg54mjsk1s6lp612ba6rmqymnai7xlvxlslqf33swbf")))

(define-public crate-beemovie-0.2 (crate (name "beemovie") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0s8zkz09d8gbswgp6ybq3287hs24jbxdc0nhkbss30nk0fi3dl4p")))

(define-public crate-beemovie-1 (crate (name "beemovie") (vers "1.0.0") (deps (list (crate-dep (name "promptly") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0n735p3rlm0kp5szbbympj24swhg738j1rlnm3cbs094c3lpcjcm")))

(define-public crate-beemovie-1 (crate (name "beemovie") (vers "1.0.1") (deps (list (crate-dep (name "promptly") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1lzkknb5yfw084n8mxhyzybiij9akwvpljwpbpgw41crs4lggsn9")))

(define-public crate-beemovie-cli-0.1 (crate (name "beemovie-cli") (vers "0.1.0") (deps (list (crate-dep (name "beemovie") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1l2ggiykl4ldmp4k8jd246r09dl3v7ijz8kfgwjs253m6dpkhy2j")))

(define-public crate-beemovie-cli-0.1 (crate (name "beemovie-cli") (vers "0.1.1") (deps (list (crate-dep (name "beemovie") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1ly9sqcq0qz8nl8m12v9sxncr7jpl2pj1mxc7dkljpfp6zlirasw")))

(define-public crate-beemovie-cli-0.1 (crate (name "beemovie-cli") (vers "0.1.2") (deps (list (crate-dep (name "beemovie") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ggwwbphrldmx0xb2w32nah1qx0c6m4bz2xfwslkrlzp7xx0qdfg")))

(define-public crate-beemovie-cli-0.1 (crate (name "beemovie-cli") (vers "0.1.3") (deps (list (crate-dep (name "beemovie") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0bm2jhnbz9dj5dmykkqgbpsxbva45y0gzl6kn57mfk1ssfg2lk25")))

(define-public crate-beemovie-gui-0.1 (crate (name "beemovie-gui") (vers "0.1.0") (deps (list (crate-dep (name "beemovie") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gio") (req "^0.9.0") (features (quote ("v2_44"))) (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.9.0") (features (quote ("v3_16"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.10") (features (quote ("blocking"))) (optional #t) (default-features #t) (kind 0)))) (hash "0kzfmzrkan6mmikb6hsplzj5nqgnc9b9c9v20yh8imdk4ivlpsbn") (features (quote (("notify" "notify-rust") ("discord" "reqwest" "dirs"))))))

