(define-module (crates-io be lg) #:use-module (crates-io))

(define-public crate-belgium-0.1 (crate (name "belgium") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2.19") (optional #t) (default-features #t) (kind 0)))) (hash "0fkzb2wb8qsx99sndncj33krgvbml5li86bs7s05zxd46nkvrxk0") (features (quote (("default" "getopts"))))))

(define-public crate-belgium-0.2 (crate (name "belgium") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2.19") (optional #t) (default-features #t) (kind 0)))) (hash "172kqr0r89a8jldcnn2d9cvplqahvx2w8fm1fc0mzxwvih2m08qs") (features (quote (("default" "getopts"))))))

