(define-module (crates-io be nj) #:use-module (crates-io))

(define-public crate-benjamin_batchly-0.1 (crate (name "benjamin_batchly") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "dashmap") (req "^5.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.21") (kind 0)) (crate-dep (name "tokio") (req "^1.19") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("time" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "0gfh6drwckq0fn7cafl2cdjfypc2d0xl0vi0220x1if3wbwbjfvn")))

(define-public crate-benjamin_batchly-0.1 (crate (name "benjamin_batchly") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "dashmap") (req "^5.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.21") (kind 0)) (crate-dep (name "tokio") (req "^1.19") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("time" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "06g4f3f7r0xjblw35v00aaffzggv15hzib84cp7k0wibsxl666xw")))

(define-public crate-benjicrate-0.1 (crate (name "benjicrate") (vers "0.1.0") (hash "1g9lj61gx9ggvi9xm6cvpkbfibcp2syl390bg09h7b3awxblwxq5")))

(define-public crate-benjicrateinner-0.1 (crate (name "benjicrateinner") (vers "0.1.0") (deps (list (crate-dep (name "benji") (req "^0.1.0") (default-features #t) (kind 0) (package "benjicrate")))) (hash "1v2xdmagc3dgvazlzp4s5g4vxshl4mf9ghjwz46jj68lq905xx49")))

