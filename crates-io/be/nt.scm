(define-module (crates-io be nt) #:use-module (crates-io))

(define-public crate-bento-0.0.1 (crate (name "bento") (vers "0.0.1") (hash "0lr4hcymgpqir0kynnnkrs8zxb637cab8zjvh6331xfbjxn3bdm0")))

(define-public crate-bento-box-0.1 (crate (name "bento-box") (vers "0.1.0") (hash "0rb3llrj7qs393gzw1grdb098gg93lzb9b2dy7np4ahdjnyljd2a")))

(define-public crate-bento4-src-0.1 (crate (name "bento4-src") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 0)))) (hash "15nkh4j905xkj268ffz013wcvn1s1267bn2i6sgxr5g120qvr9h0")))

(define-public crate-bento4-src-0.1 (crate (name "bento4-src") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 0)))) (hash "0ln4z9f05fdhlnw0x9sp2659ghv8wi94fndh422hs94fjwl5w638")))

(define-public crate-bento4-src-0.1 (crate (name "bento4-src") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 0)))) (hash "0sh5g2nn2kbvc5d85zk38q0fidsi1hzh7n67l98ahz9qa2najj1n")))

(define-public crate-bentobox-0.1 (crate (name "bentobox") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-alloc") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "0qjnn1ajalbzk4gphqsllyg49mzffj2adhcab85knm1chmvm24fw")))

(define-public crate-bentobox-0.1 (crate (name "bentobox") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-alloc") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "0j4b5mdmzm5apsn289x9izcizxl7klfpbqz0l9jx9v7ap2yzsfxs")))

