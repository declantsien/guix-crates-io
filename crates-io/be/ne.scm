(define-module (crates-io be ne) #:use-module (crates-io))

(define-public crate-bene-0.1 (crate (name "bene") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1pam7qjmix2kq77pkqspi8rax74xnwf2jdyg1mmadv365kwi01n5")))

(define-public crate-bene-0.1 (crate (name "bene") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1hxfi9gvzfymviahz8nm3nkaj0syhrjg54q6vrp1j4i75n5zgb9g")))

(define-public crate-benemalloc-0.1 (crate (name "benemalloc") (vers "0.1.0-BETA") (deps (list (crate-dep (name "allocations") (req "^0.1.0-BETA") (default-features #t) (kind 0)))) (hash "07fyf4zz44fgzc3xr4rb2zacjpngkhgdrz43gsbchgbwlb9qjykb") (features (quote (("track_allocations") ("default"))))))

(define-public crate-benemalloc-0.1 (crate (name "benemalloc") (vers "0.1.1-BETA") (deps (list (crate-dep (name "allocations") (req "^0.1.0-BETA") (default-features #t) (kind 0)))) (hash "05nrqhbm679mzf092mww1pnnygq2834yvxifccj11x1dqvbcdcb1") (features (quote (("track_allocations") ("default"))))))

