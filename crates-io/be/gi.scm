(define-module (crates-io be gi) #:use-module (crates-io))

(define-public crate-beginner_tools-0.1 (crate (name "beginner_tools") (vers "0.1.0") (hash "1aaczwyc6icsx77pg66zgvx40pmydw1j6yhd5w4607rzdgi6a4j9")))

(define-public crate-beginner_tools-0.1 (crate (name "beginner_tools") (vers "0.1.1") (hash "09m69nd3vx1nffbjr0djxswa0c9v48na97hd5fdy90gc4z8d9jqx")))

(define-public crate-beginner_tools-1 (crate (name "beginner_tools") (vers "1.0.0") (hash "1dch35y6sxvzhv3ilclb4qk3v8wahifmwp736f9n84dqqk4fp884")))

(define-public crate-beginner_tools-1 (crate (name "beginner_tools") (vers "1.1.0") (hash "1p5n94q1i4xn5fiyqgi4sq3f9bdicjhwkkz9p2m4fk9dpafiyiq8")))

(define-public crate-beginner_tools-1 (crate (name "beginner_tools") (vers "1.1.1") (hash "1c2523dc3crl2bqd42ycpz8z9ig0xcm4ah8hhcy9jdnsdsb4d3kr")))

(define-public crate-beginnerror-0.1 (crate (name "beginnerror") (vers "0.1.0") (hash "1bcxgdh8wbp91zf7g2zvr2dmv373s34n323y3d6c6ka7qccbglxh") (yanked #t)))

(define-public crate-beginnerror-0.1 (crate (name "beginnerror") (vers "0.1.1") (hash "0x79ri159bay38avnnpmxiag60didd467xg3vzvl4bfzgljxx7hb") (yanked #t)))

(define-public crate-beginnerror-0.1 (crate (name "beginnerror") (vers "0.1.2") (hash "105wv7pip7dxzwk9lywl379kf7990dmy29vl3lcjmw7s38083vja")))

