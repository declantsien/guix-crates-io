(define-module (crates-io be rg) #:use-module (crates-io))

(define-public crate-bergamot-0.0.0 (crate (name "bergamot") (vers "0.0.0") (hash "0z3saw7vnpmbdhdg11mkvxmvjy2ghhykn9b8szrbjm0gd0l5ndxg")))

(define-public crate-berghain-0.0.1 (crate (name "berghain") (vers "0.0.1-preview") (hash "0q1w4qv7ajs0ki29cm0sn85nigcqni06dac86cpchaw4s9ix1gi9")))

(define-public crate-bergwerk-0.1 (crate (name "bergwerk") (vers "0.1.0-pre") (deps (list (crate-dep (name "blake3") (req "=0.3.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1kvpxy672j7vfyzmy64xnaqkm3p42dn1awmxdp9b089d0xvnsxyd") (yanked #t)))

(define-public crate-bergwerk-0.1 (crate (name "bergwerk") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "0v8bfrq5zwrbcmh3s8m5jj7fhvkndfyr4z9vga3hk2hpyps5sldg")))

