(define-module (crates-io be xp) #:use-module (crates-io))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.0.0") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0a20mhdhc5hq6slv664pc4kdvdsfn5vr566dn8khfav65yilrjk5")))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.0.1") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1dgsydyc3w4iwg8zfxfh3y756h8yl5xsb4ydq104b5ha4qj5mp6w")))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.0.2") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1h580kkq4vhnimvwyvdfhkis8vqk75izlyaak20mgrdk137sdvlx")))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.1.0") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0a1l7vy88vpr61w8ya4vr37xnwsv113jgfkljgyanvay05l3fqs3")))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.1.1") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0xsfpvgwprx3f35a7fp5q7byy4m0cnh4rgz253rg852yl91i37nq")))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.1.2") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0rh2bb32znmn3ibjn0zwrvwahw3xkm7gkl2sz8z8vd32rzma649x")))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.1.3") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "08kjb02akgc5wpva8anl3z0f1kd7phk4585zy97iq5hdhq27m9pd")))

(define-public crate-bexpand-1 (crate (name "bexpand") (vers "1.2.0") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "19z536xarisiasginv89ddhyyqhriw9vvwrrkz2z431rp2fpsp84")))

