(define-module (crates-io be nb) #:use-module (crates-io))

(define-public crate-benbot-0.1 (crate (name "benbot") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dj6b7rj3n9glgpx53g3xs5nisi99n97090h6y5iv12ca5m45wf0")))

