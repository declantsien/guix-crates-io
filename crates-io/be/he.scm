(define-module (crates-io be he) #:use-module (crates-io))

(define-public crate-behelit-0.1 (crate (name "behelit") (vers "0.1.0") (hash "1hndipr3pd8lxpprhfk086igpq5kdfgjqg9yd7msx0crjjdj7gnz")))

(define-public crate-behemoth-0.0.0 (crate (name "behemoth") (vers "0.0.0") (hash "1as3sgapwjks6nd34xz6y0dln3pk0skkgwm9n2bwjfaxpk9rsl7d")))

(define-public crate-beherit-0.1 (crate (name "beherit") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^1.0.0-rc.3") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1l02mkd4qq626hmbdlw3icyxwbzm1rldc61bz8xy40j5q8km8rzq")))

