(define-module (crates-io be db) #:use-module (crates-io))

(define-public crate-bedblocks-0.1 (crate (name "bedblocks") (vers "0.1.0") (deps (list (crate-dep (name "bio") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap-stdin") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "04x2qr061dxzzszkbmzhk837va5zfbg942lzk8v154jr9hflj09j")))

