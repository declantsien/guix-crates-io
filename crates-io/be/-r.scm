(define-module (crates-io be -r) #:use-module (crates-io))

(define-public crate-be-rust-master-0.1 (crate (name "be-rust-master") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03dj2nmcrk0crzi10ij4jwlg1pdkx7nk0n9wf18p0zw248pcaik2")))

