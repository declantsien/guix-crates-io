(define-module (crates-io be e6) #:use-module (crates-io))

(define-public crate-bee64-1 (crate (name "bee64") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.21.7") (default-features #t) (kind 0)))) (hash "07q3aj82svz59z20fqawb9fm8nnm1i5n2rqivmbjjilmd1lxcpkc") (rust-version "1.48.0")))

