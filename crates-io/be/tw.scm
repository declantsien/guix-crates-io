(define-module (crates-io be tw) #:use-module (crates-io))

(define-public crate-between-0.1 (crate (name "between") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1ys5pzph58vhj1bfrf4m1qh4f9xiwgnhhihi92wj81ivm56pwrrd")))

(define-public crate-between-us-1 (crate (name "between-us") (vers "1.0.0") (hash "1x8acj5sirgxy1ipqz2a9jyfykh16qy2vy93zdmw973ddwsc9c1s")))

