(define-module (crates-io be ts) #:use-module (crates-io))

(define-public crate-betsy-0.1 (crate (name "betsy") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.9") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "happv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.9") (default-features #t) (kind 0)))) (hash "0db7rm652kiv3kz99qndsdkfqd5b2vdnki5bhj8yc2rpgd58gyz0")))

