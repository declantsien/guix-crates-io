(define-module (crates-io be eg) #:use-module (crates-io))

(define-public crate-beeg-0.1 (crate (name "beeg") (vers "0.1.0") (deps (list (crate-dep (name "comat") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "1kb7pbzwksbgz29xffnw02vz069izm22if4vdwbaa0krw8jg8x0c")))

(define-public crate-beeg-0.1 (crate (name "beeg") (vers "0.1.1") (deps (list (crate-dep (name "clipp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "comat") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0zqr9zy430vhxvx47lhld46fmiznhvv5hx0vadz15rjpdsz5iqka")))

(define-public crate-beeg-0.1 (crate (name "beeg") (vers "0.1.2") (deps (list (crate-dep (name "clipp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "comat") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0jv3f634zdcs46rz9a056qqg7fk5pr0j9l3i7k4x8xxb0ww2i6c0")))

