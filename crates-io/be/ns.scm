(define-module (crates-io be ns) #:use-module (crates-io))

(define-public crate-bens_fractions-0.1 (crate (name "bens_fractions") (vers "0.1.0") (hash "09yqsmdppqd36k8b92dhsljiva2pk86s3ma2sajb8y32hwandp39") (yanked #t)))

(define-public crate-bens_naive_rational_number-0.1 (crate (name "bens_naive_rational_number") (vers "0.1.0") (hash "1asa4va0b9wk9fhkmyc985l2p1ybwbhl2jsrfi7xgcj67ywiyfvd") (yanked #t)))

(define-public crate-bens_naive_rational_number-0.1 (crate (name "bens_naive_rational_number") (vers "0.1.1") (hash "0shvqxjgb2rjw4irviag355dlk1afi6760ck9948plq9x3f747yn") (yanked #t)))

(define-public crate-bens_naive_rational_number-0.1 (crate (name "bens_naive_rational_number") (vers "0.1.2") (hash "0nyas6gv37qxdk4dbb8ahhzr8mny7zv71rfkc722vv4npjrgh57k") (yanked #t)))

(define-public crate-bens_naive_rational_number-0.1 (crate (name "bens_naive_rational_number") (vers "0.1.3") (hash "08109wgl08jj3yqalz5j8pjphmp7sw8spqg14hba3y8k3gr01hq8") (yanked #t)))

(define-public crate-bens_naive_rational_number-0.1 (crate (name "bens_naive_rational_number") (vers "0.1.4") (hash "08zisi89c4xp1bykwgnpb9fr3xxrjnbzm3wlwhis82flicdcf91s") (yanked #t)))

(define-public crate-bens_naive_rational_number-0.1 (crate (name "bens_naive_rational_number") (vers "0.1.5") (hash "0l84kxa0aic7zvpd8h5v9b7f5g1zpphzsp2dqcvjmplx5dsm5hph") (yanked #t)))

(define-public crate-bens_number_theory-0.2 (crate (name "bens_number_theory") (vers "0.2.2") (hash "132pgbz805ky5gmav7igq3zxhvqnhcnpbh5gxwjhyw1bnni6p0d4") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.0") (hash "0rvicrd89j6lxmijdf6g6ssgkijpg7768gziwd83kh0j735b2wkv") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.1") (hash "0j4javq7nc6lf5flc7w27z3n91rzlzya0p22wiic11sdyqrz8p3g") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.2") (hash "1kw45vyjkhsp8divdmi6w22sc6bgph71vbcy3hm2d7brwwy3ys9p") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.3") (hash "0dn9sjlxpgq683ggc76dy304plcbvl922v5acfrwbrw8gvl75bn7") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.4") (hash "10v5p62g5laqy4ad5idlnq82zgs2hsmy05dfyc2q8n3q1n907xmk") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.5") (hash "0h6kzi6hbhii1h1fmms6vfggjrhx8pasrc5n123afvxy75gxafyj") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.6") (hash "11y9shkihjala9ak84j3dqjhsn6wqnbi9pdlmwkn42qri049hrgj") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.7") (hash "085fzi5xsiaw11f62qmhmbgs9vmgs0l5gkm71zrj1fkjbk74h0m3") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.8") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1ahs5fj9chyyxw26a1djnpifyq3x6sldangrf9lnl1zmdlzkd00g") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.9") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "17bg88lv7643qf8zdbv3xhi5i8zcnwkzg0616rfgws4h89i1l2wh") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.10") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1zhgpws7nxpkckfi2ap5vm09jd53g3a1xggqsbblrfqshiwxww77") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.11") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1m8zl1kwxza56zdxlwmvdgbmhphxwg7dcmb8gld7qv15yf86v23z") (yanked #t)))

(define-public crate-bens_number_theory-0.3 (crate (name "bens_number_theory") (vers "0.3.12") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0qrfdjj7w8rcjgqn43fx991icsljh3snqrpclrcpk4k446fy2m8r") (yanked #t)))

(define-public crate-bens_number_theory-0.4 (crate (name "bens_number_theory") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0zai0lxwd7qils42s331hjdn016yxmlhyrvbymwga5grdv08psrx") (yanked #t)))

(define-public crate-bens_number_theory-0.4 (crate (name "bens_number_theory") (vers "0.4.1") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1iwym3sngrlrm41nzhbhn66ny7064h9fdd3z5m5s8k4i1ivvr4f2") (yanked #t)))

(define-public crate-bens_number_theory-0.4 (crate (name "bens_number_theory") (vers "0.4.2") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1m4k2d3calgd21grr7fi9kavl05x8pb6c647l98h21ayjfc53xql") (yanked #t)))

(define-public crate-bens_number_theory-0.4 (crate (name "bens_number_theory") (vers "0.4.3") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1rxgf00yhzrljcvpi33s7gh4n7j6dd2lgga4sdxy520b83595zlx") (yanked #t)))

(define-public crate-bens_number_theory-0.4 (crate (name "bens_number_theory") (vers "0.4.4") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0s9pq93xk7nszbzcm2fzxpf7zhpsn6bj5rp0c7jf54ac7an7ac68") (yanked #t)))

(define-public crate-bens_number_theory-0.5 (crate (name "bens_number_theory") (vers "0.5.0") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1h7ry1y4cmrfcx1dwghpcpxrwak6nih4q486bh7kx96w6g8051sz") (yanked #t)))

(define-public crate-bens_number_theory-0.5 (crate (name "bens_number_theory") (vers "0.5.1") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0nlpgzb5rgmhdyq51iw1216523cyhirv759w2wjvrz01kd89xxzp") (yanked #t)))

(define-public crate-bens_number_theory-0.5 (crate (name "bens_number_theory") (vers "0.5.2") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "08pvhxb5nwpps7s5b9lr7jla6mbvalhvlpphbdm49vl8bnz9y4ys")))

