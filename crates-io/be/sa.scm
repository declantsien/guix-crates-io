(define-module (crates-io be sa) #:use-module (crates-io))

(define-public crate-besafe-1 (crate (name "besafe") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "1sqdw0n4z2k8ygq0w8dx2md0s48jrizgwlc41nz6fwpd0xas53f4")))

