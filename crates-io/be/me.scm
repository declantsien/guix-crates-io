(define-module (crates-io be me) #:use-module (crates-io))

(define-public crate-beme-0.1 (crate (name "beme") (vers "0.1.0") (hash "0vzgl72gn2wchcwl87qxlz3h26kpv9pc5636fb2zx9sj2ilagfwb")))

(define-public crate-bemeurer-0.1 (crate (name "bemeurer") (vers "0.1.0") (hash "1l9ck7gcw62mfcphpfn7phphh8jlj5x3jlwldidy38g0269bhhwc")))

