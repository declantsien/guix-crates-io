(define-module (crates-io be vo) #:use-module (crates-io))

(define-public crate-bevoids-0.1 (crate (name "bevoids") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)))) (hash "16l74kvhshrdbvaxzv11q5a9sgk8s3k9dkjbfz9cab7iq9z7lq1g")))

