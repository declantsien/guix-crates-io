(define-module (crates-io be to) #:use-module (crates-io))

(define-public crate-beton-0.1 (crate (name "beton") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "heckcheck") (req "^2.0.1") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4.9") (default-features #t) (kind 2)))) (hash "0qszrxqq9m61rm0saj67lzm5lzd3frgj5rc8vbs80yl3sx5cd70x")))

