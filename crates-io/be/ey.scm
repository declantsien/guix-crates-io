(define-module (crates-io be ey) #:use-module (crates-io))

(define-public crate-beeyan-bicycle-book-wordcount-0.1 (crate (name "beeyan-bicycle-book-wordcount") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l9n7dkp5wsgcjpmpc1l90w3dpq6rf6a5mjk2dwr2manlbf26mz6")))

