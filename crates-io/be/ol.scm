(define-module (crates-io be ol) #:use-module (crates-io))

(define-public crate-beolyd5_controller-0.1 (crate (name "beolyd5_controller") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0bi3dpww5dz60vpnjyh6a5xrldg4sprdlsh50yqnxg7sq6lp5j10") (yanked #t)))

(define-public crate-beolyd5_controller-0.1 (crate (name "beolyd5_controller") (vers "0.1.1") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "14m8h2blfbv0g690lzrgz8x907zv7spk5dwmdfgw6lbw6cyhgz4i") (yanked #t)))

(define-public crate-beolyd5_controller-0.2 (crate (name "beolyd5_controller") (vers "0.2.0") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1c9qcf7wkdi5gs23mnc63dasslfxg477mbdk74cqbhbzq78h7b3j") (yanked #t)))

(define-public crate-beolyd5_controller-0.3 (crate (name "beolyd5_controller") (vers "0.3.0") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0lvfwkx8whgjylz7mdhnlkjjr06dvgqc4jz45qqnal9il2476rc6") (yanked #t)))

(define-public crate-beolyd5_controller-0.4 (crate (name "beolyd5_controller") (vers "0.4.0") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zwyws6m32y8ajzz6rn0dw3gmz108yjql5alqrag1q57y05w4fx8") (yanked #t)))

(define-public crate-beolyd5_controller-1 (crate (name "beolyd5_controller") (vers "1.0.0") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yz7znpm036hn8x7d60rlnwf94p12m70bcl0aa90absyl08ryzil") (yanked #t)))

(define-public crate-beolyd5_controller-1 (crate (name "beolyd5_controller") (vers "1.0.1") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vg9xy2y3isjh6d6vgbpfjh5n99vjj4bbfg08hfxnjr1zbyx8dnb")))

(define-public crate-beolyd5_controller-1 (crate (name "beolyd5_controller") (vers "1.0.2") (deps (list (crate-dep (name "hidapi") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sshykvgibfc6qmcdgwmwzrzrvvvk0zjf4bj2d061cxvg48krdlr")))

