(define-module (crates-io be nr) #:use-module (crates-io))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ym8l2zgm9551m7hib2xg96g164x5h6i8pzhlm06j9kq561d5y63") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vj4f2wynxhlpzqnvw6grv6ylvbxmkhsjaxppy32capnn87dx0y7") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0iwycjigg13qahxl6rzk2gmm5nlg9zz01d8zszzbib8hs5kqzpk1") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "1crxgkf00f371hl2pg0fbwgdamwmz99k12qyrc1sngx1agvzj4pb") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bx8malqk8dv5ffi308ka41mwhskgfmakmaa3nkk60zg0hkz8sj3") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.5") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "1v4ccgk6kf9j16jr72sy73371wdxkipbarkhv52js69mmap5852a") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.6") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "07arvydmqy4y82hbig349lkifqp8wrl07c97xd7ym9cy8nllkxyw") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.7") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "195k07d4ck1885crxhn924zl1087l46yyjlcqwgj39f2v9s4257g") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.8") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ag104c7p7hk2c2p88k8z82fn9xvf1xqcyl4gk40damcc4d3rbd7") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.9") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "14alj3iizk5fca2ma42yznw41walxa9irddb7bqzgx675kh901s8") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.10") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0a22xb8d1kv6pbxp4nzkxfr09vhqfaaljvkim56p2npfwl8jj725") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.11") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rnvd65sfvy4wwwcaxg5wpq88nx6mrwwvs50faq7kw8wzd8cp1b1") (features (quote (("default"))))))

(define-public crate-benri-0.1 (crate (name "benri") (vers "0.1.12") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0r9qpkj2vk791hdmcx64adinp30rxnkrz2s6gaph4yisdj9rk97m") (features (quote (("default"))))))

