(define-module (crates-io be ap) #:use-module (crates-io))

(define-public crate-beap-0.1 (crate (name "beap") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0h3wbqpj0g7bsf1p689dglbcnx6q0v7lipic8xxkhb6msilyrlkc")))

