(define-module (crates-io be en) #:use-module (crates-io))

(define-public crate-been-0.1 (crate (name "been") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "termtree") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0hv3vs3mazih42vc24mr5jvxrbvv4xihxc8j4wv9kz0amvsi1iz8")))

(define-public crate-beenz-0.1 (crate (name "beenz") (vers "0.1.0") (hash "1hkrzcrpijfqwgd5grqg3dgmgqlivgbj5mq5n8cp5kv14231y5p5")))

