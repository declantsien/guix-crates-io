(define-module (crates-io be hr) #:use-module (crates-io))

(define-public crate-behrens-fisher-0.1 (crate (name "behrens-fisher") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "special") (req "^0.8") (default-features #t) (kind 0)))) (hash "0808r4qcmslxpdgm3ryhjak86b3aaq17k1970xspjhcpzfdwcys7")))

(define-public crate-behrens-fisher-0.2 (crate (name "behrens-fisher") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "special") (req "^0.8") (default-features #t) (kind 0)))) (hash "0mkri7nshz3fcdfrwc4rwbski5ii7r1zsh01z64i1wzb92jidb92")))

