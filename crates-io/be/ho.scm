(define-module (crates-io be ho) #:use-module (crates-io))

(define-public crate-behold-0.1 (crate (name "behold") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 2)))) (hash "0r56anh9gxcrfdh009jics34mhfgvd8hbmfyy3ykpm4cchg73zm9") (features (quote (("unstable"))))))

(define-public crate-behold-0.1 (crate (name "behold") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 2)))) (hash "0wcrin6vzff1wsln5ld3x5dyr6m07ycrjy2fp7f2zg3jc4y18g9j") (features (quote (("unstable"))))))

(define-public crate-behold-0.1 (crate (name "behold") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 2)))) (hash "06fgskzmn9b8ksaaj42bws2dl0sjh01nbcnqnf1vb7rlfzd6h0k5") (features (quote (("unstable"))))))

(define-public crate-behold-0.1 (crate (name "behold") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.3") (default-features #t) (kind 2)))) (hash "1v94lnvxvmhsf1611lnsal1gpbn3r4rlz5azpg9w19n9grb05fi4") (features (quote (("unstable"))))))

