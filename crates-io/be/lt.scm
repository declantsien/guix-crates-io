(define-module (crates-io be lt) #:use-module (crates-io))

(define-public crate-belt-0.0.1 (crate (name "belt") (vers "0.0.1") (hash "1angv84vvx2y0ghx4nygk2lj0f1kzmd9f3axvbfbvsaz43wz144m")))

(define-public crate-belt-block-0.0.0 (crate (name "belt-block") (vers "0.0.0") (hash "1s0bla4a7z0fmpq079pw50395nfqn0kl9311x05rxa4hv5qh4bmw")))

(define-public crate-belt-block-0.1 (crate (name "belt-block") (vers "0.1.0") (deps (list (crate-dep (name "cipher") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "0zqrnah10br0snxj77zdg94dbaajyrl4bdhkyh7ll4m53igbdjpn") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-belt-block-0.1 (crate (name "belt-block") (vers "0.1.1") (deps (list (crate-dep (name "cipher") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "1vmb8zp5ipfadxzy357fmwsipwv5mjvjkyf3d8mpcl2rzpyf66ss") (features (quote (("zeroize" "cipher/zeroize") ("default" "cipher")))) (rust-version "1.56")))

(define-public crate-belt-block-0.1 (crate (name "belt-block") (vers "0.1.2") (deps (list (crate-dep (name "cipher") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "0azqlly190mvg6dz11bz0295fgm7lkzf73x70k9wrqll77pixanr") (features (quote (("zeroize" "cipher/zeroize") ("default" "cipher")))) (rust-version "1.56")))

(define-public crate-belt-cbc-0.0.0 (crate (name "belt-cbc") (vers "0.0.0") (hash "10vjiany39naf4xp0r4ldwsj7gm2bszkhq8nfapyl4z2nqnz346w")))

(define-public crate-belt-cfb-0.0.0 (crate (name "belt-cfb") (vers "0.0.0") (hash "0579icx20fnsfbfyh2y0xbj5cjwacqg2mgr365zm50hbjwc3md5y")))

(define-public crate-belt-cipher-0.0.0 (crate (name "belt-cipher") (vers "0.0.0") (hash "0d22mqxv9ycp8xwrqifff5763ijgpfnaj662qalx64glgpf9hcw6")))

(define-public crate-belt-ctr-0.0.0 (crate (name "belt-ctr") (vers "0.0.0") (hash "0xn466p9n5kvxy81vlzc7mapswmkya9vicvhnxykchj46g38i0nh")))

(define-public crate-belt-ctr-0.1 (crate (name "belt-ctr") (vers "0.1.0") (deps (list (crate-dep (name "belt-block") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "belt-block") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "02s8q0hmavc47sgzhrxasqh9kkzpjki86yd7axcnmschzx51691z") (features (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("alloc" "cipher/alloc")))) (rust-version "1.56")))

(define-public crate-belt-ecb-0.0.0 (crate (name "belt-ecb") (vers "0.0.0") (hash "0ax42rghsj42zg6x1n1gdi407lqnrgsls6i5304v6h1v0g88c555")))

(define-public crate-belt-hash-0.0.0 (crate (name "belt-hash") (vers "0.0.0") (hash "1w1bhxbn2dlw2fpq5ggk3ma49qrl0l4v7rcbkihgcz2h4yl5ggrv")))

(define-public crate-belt-hash-0.1 (crate (name "belt-hash") (vers "0.1.0") (deps (list (crate-dep (name "belt-block") (req "^0.1.1") (kind 0)) (crate-dep (name "digest") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "149x7k7z4gyhd5j9wm389lmwbhbcahr80775q7g97i7wzlxf93jd") (features (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (rust-version "1.57")))

(define-public crate-belt-hash-0.1 (crate (name "belt-hash") (vers "0.1.1") (deps (list (crate-dep (name "belt-block") (req "^0.1.1") (kind 0)) (crate-dep (name "digest") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "12nv3prfh5j4fk9m7q0jdmaa05lmxvyl5ygdk80nwbs7p2rhbi7v") (features (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (rust-version "1.57")))

(define-public crate-belt-hash-0.2 (crate (name "belt-hash") (vers "0.2.0-pre.0") (deps (list (crate-dep (name "belt-block") (req "^0.1.1") (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.3") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "0yvz6n89hbb55zjjqm97qn7ay46b4fffx29jbcyq073pgb4zjnvi") (features (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (rust-version "1.71")))

(define-public crate-belt-hash-0.2 (crate (name "belt-hash") (vers "0.2.0-pre.1") (deps (list (crate-dep (name "belt-block") (req "^0.1.1") (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.4") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "1dfmiqs08bma937f8gadgdqr47fvhcp6sfcyd5rkh6dy17qaq986") (features (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (rust-version "1.71")))

(define-public crate-belt-hash-0.2 (crate (name "belt-hash") (vers "0.2.0-pre.2") (deps (list (crate-dep (name "belt-block") (req "^0.1.1") (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.7") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.7") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "1kpc43yr4xpr4hczx5dihyfpapri5si9y349rg6sy69vap12qrfw") (features (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (rust-version "1.71")))

(define-public crate-belt-hash-0.2 (crate (name "belt-hash") (vers "0.2.0-pre.3") (deps (list (crate-dep (name "belt-block") (req "^0.1.1") (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.8") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "=0.11.0-pre.8") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "1ihf9gfzf5rh2wyx8n9jsp51c5vhmij807ryny599fahm2qw69sg") (features (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("oid" "digest/oid") ("default" "oid" "std")))) (rust-version "1.71")))

(define-public crate-belt-mac-0.0.0 (crate (name "belt-mac") (vers "0.0.0") (hash "1z1gvbn6c2vfgkbdrmxxd2m39cffy2diivqkcqi3yg6bn96flyhr")))

(define-public crate-belt-mac-0.1 (crate (name "belt-mac") (vers "0.1.0") (deps (list (crate-dep (name "belt-block") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "digest") (req "^0.10.3") (features (quote ("mac"))) (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "18kfr752bp0n79dccg40i1qnlpqi1an12s07wx8hszyw4i8i9hm2") (features (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (rust-version "1.57")))

(define-public crate-belt-wblock-0.0.0 (crate (name "belt-wblock") (vers "0.0.0") (hash "07g14ncwn3a7i1hmi5l039yij868g5dm23lsh7svsh9qri7zii3p")))

