(define-module (crates-io be tt) #:use-module (crates-io))

(define-public crate-better-0.1 (crate (name "better") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dmrbhav2vk3pjxf6i6n0x60mviamb88r733843cmiys2clx0kyi")))

(define-public crate-better-0.1 (crate (name "better") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v70h1ps14l9brv3vdqh0y8qx44fmy3ddrpzi6fxqfmg91m9s0ka")))

(define-public crate-better-as-0.1 (crate (name "better-as") (vers "0.1.0") (hash "0wa3s9pwsikvcq2plm8szjg7cff49gpf4aki2q9md6f20k6wn7xg") (yanked #t)))

(define-public crate-better-as-0.2 (crate (name "better-as") (vers "0.2.0") (hash "1f5jy1kgva9yd2xxyjwi6rp4v6lyhp9gmdirxwjlym421kvkrf76") (features (quote (("std") ("default" "std"))))))

(define-public crate-better-bae-0.1 (crate (name "better-bae") (vers "0.1.7") (deps (list (crate-dep (name "better-bae-macros") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "10gq2s8w8gfia7arih2k75yh9h946jz913yd0kwsq55dzj4kzxjm")))

(define-public crate-better-bae-0.1 (crate (name "better-bae") (vers "0.1.8") (deps (list (crate-dep (name "better-bae-macros") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1ns1169yal7xhm33vnv69spwi1j0yijavq4im784mllhivwjrwxh")))

(define-public crate-better-bae-0.1 (crate (name "better-bae") (vers "0.1.9") (deps (list (crate-dep (name "better-bae-macros") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1i9b8al1kzs7xd7gcyyx9nikpycd8x8ahfspwsw9r3avabq5c8n4")))

(define-public crate-better-bae-macros-0.1 (crate (name "better-bae-macros") (vers "0.1.7") (deps (list (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1q51da6n0qhdrb58m231b76i6a5fpxf47gy18p4mh2q2j4rr02hf")))

(define-public crate-better-bae-macros-0.1 (crate (name "better-bae-macros") (vers "0.1.8") (deps (list (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1z9864k0m5d6z95kiqp449v9ajwj40k7jzxmj6i2ch18c43nx065")))

(define-public crate-better-bae-macros-0.1 (crate (name "better-bae-macros") (vers "0.1.9") (deps (list (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1g1dxcixvsyw50z1936abf4h3zrqb61yc8i4hh89kbqiq92323vb")))

(define-public crate-better-blockmap-0.1 (crate (name "better-blockmap") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.15") (features (quote ("integer"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0qvnqb8bgyh8lpljfzryj6hkvwqzi9p1n1nqb2wn3l2p4kb7j8js")))

(define-public crate-better-blockmap-1 (crate (name "better-blockmap") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.15") (features (quote ("integer"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1zh4n2vylz7rkilykxcp3d0kyjs143ai3y4hv9sw1z1m45h86hwh")))

(define-public crate-better-blockmap-2 (crate (name "better-blockmap") (vers "2.0.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.15") (features (quote ("integer"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "06gf820p5ghfxlm6b9pa8fwffbpb148wi7wh9ns4kqlk3blyvax8") (features (quote (("window_size" "rug"))))))

(define-public crate-better-blockmap-2 (crate (name "better-blockmap") (vers "2.0.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.15") (features (quote ("integer"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "060ri3imnqf03y4b283gqvg388vz81727a0x2nzbp6vqqqprvdrp") (features (quote (("window_size" "rug"))))))

(define-public crate-better-debug-1 (crate (name "better-debug") (vers "1.0.0") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "1j3zr5hrq5a360ddwyh5xiafa612fzdrh0n4b411sssaaxnm07nx")))

(define-public crate-better-debug-1 (crate (name "better-debug") (vers "1.0.1") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "0qyjymxcwaxqiy2f7a8y5bvi2l49zkrc5vwz48g1nqw2c2nwhpsx")))

(define-public crate-better-default-derive-0.1 (crate (name "better-default-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0xqc4dj5hrs06az3zvfs7hgasz2dxdycjlr9fnwjdjhdxamz02zw")))

(define-public crate-better-default-derive-0.1 (crate (name "better-default-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0nlq1bgd4mxsbbr4iabwwj7jsxszhf2q14ww671w1vjsxid0bsyg")))

(define-public crate-better-future-0.0.0 (crate (name "better-future") (vers "0.0.0") (hash "0d0si1ypfdj3z8zbd4y163yd4rx8jspy9bpiadwck068y3bc0b3x")))

(define-public crate-better-graceful-0.1 (crate (name "better-graceful") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "consoleapi"))) (default-features #t) (kind 0)))) (hash "1fbra2h20c15af3xlpsk2d07wwh3frawq1kf3bys67ib5d55q298")))

(define-public crate-better-hand-0.1 (crate (name "better-hand") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0pv8xlpdq09iiw5m6ipn7brk6pii1ygmbl10zg4f05bn9zn5mvrv")))

(define-public crate-better-hand-0.1 (crate (name "better-hand") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "14masfpkk1aimqmpq26cb7fkxxd46c9zfvjxdhrwnkpbhrlm7hk2")))

(define-public crate-better-hand-0.2 (crate (name "better-hand") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0v8f72vnpdlxzqcf9q9ngw9smpqj0jv1l9fcpvkgqmxkm69b71ml")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ad3vnxrrpc7xpd43g2fh9fdgb9nl8vp745qri0gfpdkmphck12f")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1xbzrzrqgycpznpfx93krdx2nz98v171gxxgfn6rgzjq21bnr6q3")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0fc10jzzhf8kmrk1pbhw5nh1irxb8drn77g1df8r0bxzaff56057")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1mdjlq3n5490vdj2gikk4yp6gmw5q02myxca2i32icq7845lxmhr")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1m6xaib5i23c16h7h4jw6s26p7b1208k86hgq69hfadj412d4l6p")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1l1xgf73pfy0nmswgdciqm4jcdqlpqvvq61fml93x7nkck83yfxw")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0j3bkbxraqv6snq6ffjp9qrmmak2ax6sz9h7l6p8rl2i6b374yii")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.6") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1mn4jrgaq19whrfyvz444mdi1zkv2c78w5xsbb3z2dbvv77ab4y7")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.7") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0sxjgwp0kf2ybmy572w9n6rqz2kgz6m4z8n8ksaka7s87f11s4vg")))

(define-public crate-better-hand-1 (crate (name "better-hand") (vers "1.1.8") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rs_poker") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "02bkz59snc3pr91anny1bhx7ifqzyf9pza0yfzxzgzyj3s407xpm")))

(define-public crate-better-ls-0.0.1 (crate (name "better-ls") (vers "0.0.1") (deps (list (crate-dep (name "comfy-table") (req "^6.0.0") (default-features #t) (kind 0)))) (hash "0j0i1r2vbc9cbpb8v6f7r2nph48mgha1q7287y9jcwlg88i7cq6s")))

(define-public crate-better-ls-0.0.2 (crate (name "better-ls") (vers "0.0.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^6.0.0") (default-features #t) (kind 0)))) (hash "0pda3as8bvgwc06k2zqf4ci9d6g7pa5cpz1igxdyx8q5cn6fz5yp")))

(define-public crate-better-macro-1 (crate (name "better-macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cxblnawa0vdcxqzyb00r9kl12hzccv15nzrhvxwl6lflrz2p4j2")))

(define-public crate-better-macro-1 (crate (name "better-macro") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yx4q1n21n84sbvxplnwad7prmv61kpyphm3143swks2qra0jc5w")))

(define-public crate-better-macro-1 (crate (name "better-macro") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vap2sl0zi3n3rk91w3n81ibvnx60d5yymbviwq7ldd3zny3r058")))

(define-public crate-better-macro-1 (crate (name "better-macro") (vers "1.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xy149n3vafqzw0yyrk0hizlzmpblc5l83f57r5by3nqfmdswf0m")))

(define-public crate-better-num-0.1 (crate (name "better-num") (vers "0.1.0") (hash "0av6wvjybszwcbh85ib7lg1zxa7rja5fp4bmvn836ink1rs7qli9") (features (quote (("u8") ("u16") ("i8") ("i16") ("english") ("default" "english" "u8"))))))

(define-public crate-better-panic-0.1 (crate (name "better-panic") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.30") (features (quote ("std" "libbacktrace" "libbacktrace" "libunwind" "dladdr"))) (kind 0)) (crate-dep (name "console") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1hnzvxixs67yh0bx4nw84yxqmj909cdmcgvbm9nmjzj0rffn9jjv")))

(define-public crate-better-panic-0.1 (crate (name "better-panic") (vers "0.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.30") (features (quote ("std" "libbacktrace" "libbacktrace" "libunwind" "dladdr"))) (kind 0)) (crate-dep (name "console") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "15s5riwriadq8y55gipd9vwnxsx6idj68wxhnj0x1snsxjp1akkk")))

(define-public crate-better-panic-0.1 (crate (name "better-panic") (vers "0.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3.30") (features (quote ("std" "libbacktrace" "libbacktrace" "libunwind" "dladdr" "dbghelp"))) (kind 0)) (crate-dep (name "console") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mikas34acbpa5x3rj6wavic7mqg965bf8x9hlgnk090xdq4jwb4")))

(define-public crate-better-panic-0.2 (crate (name "better-panic") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.37") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.9.0") (kind 0)) (crate-dep (name "syntect") (req "^3.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0xl48v6pd9ys7wp0ni62i6q73xpd1nhf92z09sjc9n3lrj0ac4ix")))

(define-public crate-better-panic-0.3 (crate (name "better-panic") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.37") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.0") (kind 0)) (crate-dep (name "syntect") (req "^4.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0djh7qs39z0mbkzxs4nrc9ngnyjpsxq67lqfv75q91i63b8y3abg")))

(define-public crate-better-path-0.1 (crate (name "better-path") (vers "0.1.0") (deps (list (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1d9hlf5y7ic630n3vlfsn7msr7760fdikpdf8rgh1md4f3zml1j0") (features (quote (("default")))) (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-better-path-0.0.1 (crate (name "better-path") (vers "0.0.1") (deps (list (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "049wdghy3wljhwlbrh6vs0a0q8b06brz8px2g35wnlyy5sanny9n") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-better-qs-2 (crate (name "better-qs") (vers "2.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 0)))) (hash "17l4fghlc6iqy4ssn9kj6mlbj6siq55zlfgksqv42dqn6vad0srk") (features (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-better-qs-2 (crate (name "better-qs") (vers "2.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0sq99wml6y93hiygw7alg4b5ppsw9rkbypazbdbwdzqwl5b4sn1k") (features (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-better-qs-2 (crate (name "better-qs") (vers "2.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1r7s6r601aam91pahh6fdk1il7zzs1nph7q6lxswin4jxy8xjrm9") (features (quote (("regex1" "regex" "lazy_static") ("default" "regex1"))))))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.0") (hash "1qgh0343xb8vjwfw5wngm5ajzi5shx6hxd0ws3kpjci0lk43k8ly") (yanked #t)))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.1") (hash "12ixkjd54kcs9pbgjwhw5nh16qlkdllb16fhhvv1dvvghqymp7fn") (yanked #t)))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.2") (hash "1klv300za0kdrzwka925k1jfrhz8ix5lxjhazpmrj3x50v1wcf0g") (yanked #t)))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.3") (hash "151ck2aj6qysvb7sy75ndsvwpcn8i05hv227021fajpdm3kwg61g") (yanked #t)))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.4") (hash "14s9yyiidihacpmghhpfkhfizgf9vla3ybzbp2bf80qji3bx22p5") (yanked #t)))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.5") (hash "1z0zimi86jdqfvzbdkvk0dr4pg53p8l5vvhf3paypb0z0qrfq30x") (yanked #t)))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.6") (hash "04cpi955iwq3fv8zrzniwij93bsdfps6nx4dcbsx08bcv8i1hz3b") (yanked #t)))

(define-public crate-better-rs-0.1 (crate (name "better-rs") (vers "0.1.7") (hash "1j3m622nbzls0d2p3lkbp6x86pi37p5r134fmsab2yk4qkbhgzsr") (yanked #t)))

(define-public crate-better-sink-0.0.0 (crate (name "better-sink") (vers "0.0.0") (hash "02l0vj0ppzcja52iv1ailr0k3r6qskwhhlxhxv7gil2290lzplc4")))

(define-public crate-better-stopwatch-0.1 (crate (name "better-stopwatch") (vers "0.1.0") (hash "1k8y13yc4lzg81dqll1dq0r3885c8ba546j5p5f64hxfsjzicgdm")))

(define-public crate-better-stopwatch-0.2 (crate (name "better-stopwatch") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0izg1sq7smkh3zbn5wn8rmimqfvv9javr26jnqqv6vxniiplwf9y") (yanked #t)))

(define-public crate-better-todo-cli-0.1 (crate (name "better-todo-cli") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0yri0868mlh0ibfa80bhd8r03x2kl6v7vh9j8iv3sjkmcpdqfxli")))

(define-public crate-better-todo-cli-0.1 (crate (name "better-todo-cli") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0aghvsx4py2wp4lsvyf08sgh8pbj6ny7rxyc8sy3g65w8q3bx55c")))

(define-public crate-better-uptime-0.1 (crate (name "better-uptime") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1y3vb9q0q9r8smfipkrg5r8fdwbrvjgdil8psv88fyaclfibyczh")))

(define-public crate-better-vdf-0.1 (crate (name "better-vdf") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0n60snb3d8lfvc6ldga4l9pv435n3wi4nvp5qrjvci1bl4xi0nfx")))

(define-public crate-better-web-view-0.5 (crate (name "better-web-view") (vers "0.5.0") (deps (list (crate-dep (name "boxfnonce") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "webview-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09wzcw14mq64kgn6y4ihszlfkdg0xxpmznipnr7k0rfyf57a8srg") (features (quote (("default" "V1_30") ("V1_30"))))))

(define-public crate-better-web-view-0.6 (crate (name "better-web-view") (vers "0.6.0") (deps (list (crate-dep (name "boxfnonce") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "webview-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10l0v6s9cgc0rnlkxpc1af7wjdr2hmbw384fxk5byyn24448fxf0") (features (quote (("default" "V1_30") ("V1_30"))))))

(define-public crate-better_any-0.1 (crate (name "better_any") (vers "0.1.0") (deps (list (crate-dep (name "better_typeid_derive") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "=1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 2)))) (hash "180bz02vhj78vil28i1bxbbsphmjdgr3150s2f29y7m4d8mmcsqv")))

(define-public crate-better_any-0.1 (crate (name "better_any") (vers "0.1.1") (deps (list (crate-dep (name "better_typeid_derive") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "=1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 2)))) (hash "186l77d2db2d48r72gn5956g10q841hvdz4yw4jwf5vwjfyswndk")))

(define-public crate-better_any-0.2 (crate (name "better_any") (vers "0.2.0-dev.1") (deps (list (crate-dep (name "better_typeid_derive") (req "=0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "=1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 2)))) (hash "0w8r1vdfdywym2gj49vkpxmlbd01qd546112bdql1gqhb4nf3l6a") (features (quote (("nightly") ("derive" "better_typeid_derive") ("default" "any") ("any"))))))

(define-public crate-better_any-0.2 (crate (name "better_any") (vers "0.2.0") (deps (list (crate-dep (name "better_typeid_derive") (req "=0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "=1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 2)))) (hash "0gfb086dzgs0vdv08kl6q8bfrzqymf40cpk8wvxiyyga833yp58p") (features (quote (("nightly") ("derive" "better_typeid_derive") ("default" "any") ("any"))))))

(define-public crate-better_btree-0.1 (crate (name "better_btree") (vers "0.1.1") (hash "0lblvqxnii2kbxd7l5sm0yqpbqp3dxv1i6qnkzr6fb7vzlq4w4p2")))

(define-public crate-better_btree-0.1 (crate (name "better_btree") (vers "0.1.2") (hash "0x1awzvw3xkzmam6dpxrzqcaf728wgaxvlbj4n4v3n99rxbjfr6r")))

(define-public crate-better_btree-0.1 (crate (name "better_btree") (vers "0.1.3") (hash "0bbj9ycjsqgz79b2qlgy3i52ns21bikkk5b6l9mbnxrcy1370k10")))

(define-public crate-better_btree-0.1 (crate (name "better_btree") (vers "0.1.4") (hash "1hxkg19ya4ccxd0yxbf698y8zi0l179s97iw79q3zbpb5wf0xv1v")))

(define-public crate-better_btree-0.2 (crate (name "better_btree") (vers "0.2.0") (hash "08n9fxir7j6s25n2y1zs02c2ibcxiks0a8hn49i3hlg8rgvadgwp")))

(define-public crate-better_btree-0.2 (crate (name "better_btree") (vers "0.2.1") (hash "1hfqdlq20g68l35v7z76p8wrbd0dnr7b0yg8baaypg78lp796s4f")))

(define-public crate-better_btree-0.2 (crate (name "better_btree") (vers "0.2.2") (hash "0aa8vznryfpma2x851yyq7jqmr5lfrzgk8rzj0hk6jqnzbjwhlf2")))

(define-public crate-better_btree-0.2 (crate (name "better_btree") (vers "0.2.3") (hash "06clj9miy81zxzmbrykqbfvzykdrd6qdcgwq5nx636gjcfap4xr6")))

(define-public crate-better_btree-0.2 (crate (name "better_btree") (vers "0.2.4") (hash "142gvgq4yzsvr7qq0jmxx2ldlcksx2x66j9ndv0kssrrawl7lihj")))

(define-public crate-better_btree-0.2 (crate (name "better_btree") (vers "0.2.5") (hash "1zb0hlcra5kyvcmrajx179waaxz3czymj0dxd6x00bjx19f482vx")))

(define-public crate-better_btree-0.2 (crate (name "better_btree") (vers "0.2.6") (hash "057i2zl8rg70dgf4h0kg2d372zshmf3rcmhxd3b4kcxvpc3378hp")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.0") (hash "003kpvxpr7k1bj1m371fyp8ykd88hfj84d2r80wc4i31g0y9il2c")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.1") (hash "01fd7lvxbpnb6b0ln8i7a3aaax2mlk2rfqipiqfz9kd2nmagh8rb")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.2") (hash "018aicji6rpwp48b1h7pa0xj0wk6r5hzrx6nnmybvgk7imfj0pl9")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.3") (hash "1c582ac0h7kxp17fp000lfiijjnsxqgc94w1ddibsw9m6x9zng5m")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.4") (hash "0yq0p2fhlxgmnwasa3iq0pwg7f2n38b3szqwi0npprfvp66d4wcv")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.5") (hash "0g9zxzbwds79flr8mh4r12kap52qsl1m1ffbx35z05phf46aq03j")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.6") (hash "1jskvg1bpn1nqscj5b8n1ky0bazqv0n9mbbsl5rsbm2dcgqj3yak")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.7") (hash "08vlvgrjizv3c3ynv7krdf6v14d5hz4bvzwc641gbayvgqcabq22")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.8") (hash "0rqsrd5s1i5ccv6fyih56127267xv8s6mka8x19xniphji96kpm7")))

(define-public crate-better_file_maker-0.1 (crate (name "better_file_maker") (vers "0.1.9") (hash "0grlr07azrrbqnfsq5gv6xkv4c8ajg3zg6ij40an39b3ncfbj7i6")))

(define-public crate-better_input-0.0.1 (crate (name "better_input") (vers "0.0.1") (hash "0jppsqbxiqk4cwlihjf1kry84j2dxxsby52y4555nii36jykcda9")))

(define-public crate-better_io-0.0.0 (crate (name "better_io") (vers "0.0.0-alpha.0") (hash "05hiij9ghrqwwa9n6yirrb34wwqjik2v19pl97yrlbzqli8bvzyf")))

(define-public crate-better_io-0.0.0 (crate (name "better_io") (vers "0.0.0") (hash "1sapww55r4am6yykcyvlv3sdms12nsaa1ddcixczpar4ibxk6lb5")))

(define-public crate-better_io-0.0.1 (crate (name "better_io") (vers "0.0.1") (hash "1x2sqcq2jfig352k50gmgkcr97vig2y8x5x67v2zk374q239rnbl")))

(define-public crate-better_io-0.1 (crate (name "better_io") (vers "0.1.0") (hash "0a0dmxv14vn6zd8fhsmwji418xqbaclzz3bzl2r11fp7j5zy3zcj")))

(define-public crate-better_peekable-0.1 (crate (name "better_peekable") (vers "0.1.2") (hash "1qxm6hjwkkflk5xd5ym47gb7yrip1sfwv6bgd542n6fx2mbs67hv")))

(define-public crate-better_peekable-0.1 (crate (name "better_peekable") (vers "0.1.3") (hash "1zryzp178hp8jhzpnm7vx54sv80217j8r0appss9pa6l52zsvr1g")))

(define-public crate-better_peekable-0.2 (crate (name "better_peekable") (vers "0.2.1") (hash "1ydfdlfprz81gdsp3l7d8izgj2fycrs4s9r6h7ysp9685s6hwc5z") (yanked #t)))

(define-public crate-better_peekable-0.2 (crate (name "better_peekable") (vers "0.2.0") (hash "11bl2jh7rgbx6rn1fbpm7izwq392blaj654m4rhg0sragdbn95vj")))

(define-public crate-better_peekable-0.2 (crate (name "better_peekable") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1jk46r5dbarhy2slbc392w5bml423aqnhk8l7z0wpg0rc5qws5md")))

(define-public crate-better_peekable-0.2 (crate (name "better_peekable") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1vh70f667mpvv4bfh0qyaq5mp5snlmy28dazw2ikpvw4bry8x0jj")))

(define-public crate-better_peekable-0.2 (crate (name "better_peekable") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1jfyz8vgjhk6ql70xgcr2q720c3whqy296j9ci14hkcjxjd2v6k2")))

(define-public crate-better_progress-0.1 (crate (name "better_progress") (vers "0.1.0") (deps (list (crate-dep (name "terminal_size") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17wwp950b6iys6wicgkyk1585rams131hppa4w18s90nh4p64l4g")))

(define-public crate-better_range-0.0.1 (crate (name "better_range") (vers "0.0.1") (deps (list (crate-dep (name "stainless") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1llgr332y7rbsbg4lcjbw9xdppnlm1csbwprfb5zskiafqwb0wc2")))

(define-public crate-better_regex-0.1 (crate (name "better_regex") (vers "0.1.0") (hash "1hqal80gbql46kdj92ynn2m137v1jnmgfpxy06xxxxcjsm3d11fi")))

(define-public crate-better_scoped_tls-0.1 (crate (name "better_scoped_tls") (vers "0.1.0") (deps (list (crate-dep (name "scoped-tls") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "08r1i91hblc6rp3js2vbdawcqxwgxnwd134yn6iqms9rxk6qwgmp")))

(define-public crate-better_scoped_tls-0.1 (crate (name "better_scoped_tls") (vers "0.1.1") (deps (list (crate-dep (name "scoped-tls") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1ph14yy6h8is61l2sy6vx97knrj5zn9z04daxi5bn1zvng4xqkkr")))

(define-public crate-better_string_cache-0.1 (crate (name "better_string_cache") (vers "0.1.0") (hash "15ic5cywzzg33wc9h3kw5i0dd639l103hx1sakv0id1zkj40vs2f")))

(define-public crate-better_string_cache_codegen-0.1 (crate (name "better_string_cache_codegen") (vers "0.1.0") (hash "1ynpjh4scxrqjqvzakgyjckv9i87vpyx7hwf8qa2fjbg1ar58yyy")))

(define-public crate-better_term-0.1 (crate (name "better_term") (vers "0.1.0") (hash "1608ygjx62mnz81hv7qnpnrd25319lz1f64yxrrzb3jpag5az2kz")))

(define-public crate-better_term-0.1 (crate (name "better_term") (vers "0.1.1") (hash "0ianpvsrxbg48q2d90sghbscq4w5s5yv4jf7p85z1a22aynag1fj")))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.0.0") (hash "06bgvh1nvra416n4hxz2qh1ycxafqxh7alsif9j128jcg14l51qq")))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.0.1") (hash "0ifh2vclzrbvqqxxffkfg7sxf4j71fp621jx30pma70j7kb9v576")))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.0.2") (hash "0w42pyv1mhbfr7ih5h5ji3p0iaxz963wvwgl21vc6pma26r2v87b")))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.0.3") (hash "1sja88q9f92dkygfv1yirg8n1xka3zic45yjnq8s4rialwhr5xbm")))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.0.4") (hash "0a9cw6fm9qshgs4j2wr7acindkp957j0svd110hf9lxlsyclvcs4")))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.2.0") (hash "1fsaxw1r3qbv17vq9k3w7giw2rc8nsm9xl66jhiblpp5k1r8yh6k") (features (quote (("input") ("default" "input")))) (yanked #t)))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.0") (hash "13mj37jb9pxd3zypknx9b37maqb1hj08gd0air2i2wws3izxgw49") (features (quote (("input") ("default" "input")))) (yanked #t)))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.0") (hash "039czb2mvd9spj2k7xw89pp8dlwd45wxj0h4r6x89slzb6d7cvjf") (features (quote (("input") ("default" "input")))) (yanked #t)))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.5.0") (hash "1z9n16vi7csxrgln1abh64mn3v1sjxfd7gyjz0b92a45kasfryx1") (features (quote (("input") ("default" "input")))) (yanked #t)))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.2") (hash "19ssj9him3h0aspajjxxazfm79ixa73crlhkmxlaj3gaafyanqr2") (features (quote (("input") ("default" "input"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.3") (hash "13jkdkalr1w49k2xcs75c6dl8kzih07943p5hf6c9p9wn8d2yq37") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.4") (hash "199bmgc42s2r3j8wmcynmfp496qqb9wb03xncgm37r7six2d05qz") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.5") (hash "1z54xys3kb5whvfn1kxh5h4dvk7yxqb9lj1h7ipgywphyw3hz5jb") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.6") (hash "19min9iwqp77i6rf6y2k5dq4m25pckny5vaprbbfd556scbb5rsa") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.7") (hash "0nrbpmwh99si2d2q9rzjghx9dm44hggfpsziyl6m5y4r2w8gkjc8") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.8") (hash "1ji207xh3hsfikcydqhxw3g8wyi8rh9qwa4xnbsi1rb7mgy3lawd") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.3.9") (hash "1i5q0yz9z04wq271rhbimnnjijs5im5wijdx26wm48xlhk37pw0g") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.1") (hash "11h8gcs1p27yfwkykivc7rxvx7m33zadcwnpfpc15r0gi2h60xyk") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.2") (hash "19a7xm3a2ksnnf5k8hd1sbfafq5xk1avz7560iqjxlc0ibgg8y43") (features (quote (("output") ("input") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.3") (hash "1w4qh9azl0ynkpncxys6byp69fzvvbh83by133w7q5a79n91dbzy") (features (quote (("output") ("input") ("fancy") ("default" "input" "output")))) (yanked #t)))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.4") (hash "00m1gl5r89m7c5drpz1b3ibqhb15l4ka84fq6kh1z693wgfypp1z") (features (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.5") (hash "1fjhc7pjdjwz9y45zmqvrk6c446kslkgxbzvdal1zj0z7xf50cbb") (features (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.6") (hash "0sd7ijgfwmlw2krndp924hkzav2i3syqlkm3lbicd67wcrnfim5i") (features (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

(define-public crate-better_term-1 (crate (name "better_term") (vers "1.4.61") (hash "08kjblx1zm0hyp3qqrax61lxpm7844cr2yjzic12w8h6mcxnqnwm") (features (quote (("output") ("input") ("fancy") ("default" "input" "output"))))))

(define-public crate-better_todos-1 (crate (name "better_todos") (vers "1.1.0") (deps (list (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "11a5c1fh73k6wrb1xayjj3mq595h08lxnddl2h5pag427dw5h56w")))

(define-public crate-better_todos-1 (crate (name "better_todos") (vers "1.1.1") (deps (list (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1rm0dg5iwblyb83dgnkq8i9c2rspzxw8rng2xwdx54aaa7a6jma4")))

(define-public crate-better_todos-1 (crate (name "better_todos") (vers "1.1.2") (deps (list (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1sa0n985nypjk56zikzv9z51x2paf9ijg4v45wa6j4hmf1bc5lpy")))

(define-public crate-better_typeid_derive-0.1 (crate (name "better_typeid_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h4i9p7056h2ihywv8whbdg8qnvpb8y59iwhrk9kyjm0x402ymyy")))

(define-public crate-better_typeid_derive-0.1 (crate (name "better_typeid_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qr6kaqxarf1g7fr3iin7lf53lzbq8676vznsfvh0lya2awfrvix")))

(define-public crate-betterchars-0.1 (crate (name "betterchars") (vers "0.1.0") (hash "08bclmw5ywsqm41j1wbgllm9bkd848pwrry69b1cbn3p4abgwn7w")))

(define-public crate-betterchars-0.1 (crate (name "betterchars") (vers "0.1.1") (hash "1ymwl5xgznndf3mgvnb2advjgmni2fbh1xlvwnhgnbs8fgxpc078")))

(define-public crate-betterchars-0.1 (crate (name "betterchars") (vers "0.1.2") (hash "0f0zssk4v0g5k7dqf4hpx1y0q2dxr0clm7rdxqpyy3n779vx1ysy")))

(define-public crate-betterchars-0.1 (crate (name "betterchars") (vers "0.1.3") (hash "19h5hi1c8x554c4knmnfmmnffvsznm1s2qcfvv2mmgpdbfiibb5a")))

(define-public crate-betterchars-0.1 (crate (name "betterchars") (vers "0.1.4") (hash "1xvxsnw661bjqx0dz87v1nrsmh34ss9vf8wdcymc45nhy0nx0042")))

(define-public crate-bettercp-0.1 (crate (name "bettercp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lwyxm3dvhjwsbwbkya7vjy06rmb40dw7dnwnpd1swqdx6a84wqz")))

(define-public crate-betterops-0.1 (crate (name "betterops") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0pflzlxhjhkajm2nslvnlhgr919146v849b72hm9hai4d2gcz5bp")))

(define-public crate-betterops-0.1 (crate (name "betterops") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0653cl16c5ggpn8jafy133c9mcv5p9ygxg6lgiwcnvsg0gy9n5i2")))

(define-public crate-betterschool-sdk-1 (crate (name "betterschool-sdk") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1p3np78azwwj01dg0vdynngwk9xisfkqxv3s0914kkggdxaxxxhg")))

(define-public crate-betterschool-sdk-1 (crate (name "betterschool-sdk") (vers "1.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1rnlrbrhapqjy4f76z0j4lcdqi40xvx00srln2hrdfn7j7pknniy")))

(define-public crate-betterschool-sdk-1 (crate (name "betterschool-sdk") (vers "1.0.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1lqgrw28689rcb203ijrla9z2d4fmymcyx8qwzs9kxw2jm03k9rd")))

(define-public crate-betterschool-sdk-1 (crate (name "betterschool-sdk") (vers "1.0.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "183l56zddvnwqrfxmfl4sz6l4ziycj1rhi7159rlh98ymvnlxviw")))

(define-public crate-betterschool-sdk-1 (crate (name "betterschool-sdk") (vers "1.0.4") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "13krvx8lggxg2annm2fzdpyszj7jbkx0jms9j7ga1ifm5p6lvn7h")))

(define-public crate-betting-0.1 (crate (name "betting") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0i95prcyfr0vlcja0zysgs60afp2fmcbjkh5jqjyrv8fq8gpq1iv")))

(define-public crate-betting-0.1 (crate (name "betting") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0pbh4mkj1i9zcrqk80jlqc6pqhzfpl6x5byyz5i5m5nh55z9j3vq")))

(define-public crate-betting-0.1 (crate (name "betting") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "19w1il5vad5bxcapr8hq4pd78fsnv77klkk4s6fb1jhy2llyw228")))

(define-public crate-betting-0.1 (crate (name "betting") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hn9sd15kvda0gmk5apfnd7wmqq2f9cdl0rc4xrn652abm3l5qkf")))

(define-public crate-betting-0.1 (crate (name "betting") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ldjgcbifa01vhn7q0shxq5ic3n0fma2fs9njh2rny0rz3lscapn")))

(define-public crate-betting-0.1 (crate (name "betting") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "11k6kqwnw6s09kzmm2vs33ar0ay2ikdwzzqb8y9sd8ml30csssim")))

(define-public crate-betting-0.1 (crate (name "betting") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0slp1lx6lx2s6bss99h9d9hdcilw135f99jxhap9swbwdsmnd4c8")))

(define-public crate-betting-0.2 (crate (name "betting") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w79ydrnpszgkpg4w8qckrk5slmn8l9j31mi72cigxv4bfckgbwn")))

(define-public crate-betting-0.2 (crate (name "betting") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "07spzrcswz8g2s92jy0shfa3jvzggm2912lxgf6253ddx5xabycz")))

(define-public crate-betting-0.2 (crate (name "betting") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0snv345z56w6yhkiv0yyblr2k125dk2inyidr6fgsmv6bv82cfn4")))

(define-public crate-betting-0.2 (crate (name "betting") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vyr2pg5n81rdj1kn3ixgbn2zyin674s7mdw8jl2yr47nid43s5p")))

(define-public crate-betting-0.2 (crate (name "betting") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s7mk6vcifvf9ch2z3v204shdhvgn4a8vr5siwnkmrjp01b5w2k4")))

(define-public crate-betting-0.2 (crate (name "betting") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r855xiyfc9ksqqdvgiwsymvrymbm0y7kksmsg3wlwydr9jzx9y3")))

(define-public crate-betty-0.1 (crate (name "betty") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "04vwwbgr0rgf35f432zvqzr0qshrkbq7g3mcgg2qng3pq2qbnvsg")))

(define-public crate-betty-0.1 (crate (name "betty") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1dl2lp8977jnqqgpax1xciphwqgk1axfhlzg4c1lk79w9ma1wqyr")))

