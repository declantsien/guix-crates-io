(define-module (crates-io be ul) #:use-module (crates-io))

(define-public crate-beul-0.1 (crate (name "beul") (vers "0.1.0") (hash "0xpqri6pcx3ydx3fswr6wk0a9a2si0hv4vv1by2b2g2wz9z95zl9")))

(define-public crate-beul-0.1 (crate (name "beul") (vers "0.1.1") (hash "1jcfhxjfjrqyabfbfipd70iafskc3lr40hm1sbda8ahlbkxx8yj3") (rust-version "1.51")))

(define-public crate-beul-1 (crate (name "beul") (vers "1.0.0") (hash "0byxzr8a6sjd6ipvxkg1nsisyllz693hj859kpk63c9py3nvmhyk") (rust-version "1.68")))

