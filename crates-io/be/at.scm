(define-module (crates-io be at) #:use-module (crates-io))

(define-public crate-beat-0.1 (crate (name "beat") (vers "0.1.0") (hash "1azihd7jgrjcf7wrcqlgm9v392l2la6hys06nsh4pknz8ymd335h")))

(define-public crate-beat-detector-0.0.0 (crate (name "beat-detector") (vers "0.0.0") (deps (list (crate-dep (name "lowpass-filter") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "minimp3") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1dwvsyzqg037yw1yvxvff7pgjv4dbfc1sghnaxx78xflf0zfd6i1")))

(define-public crate-beat-detector-0.1 (crate (name "beat-detector") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.9") (features (quote ("termination"))) (default-features #t) (kind 2)) (crate-dep (name "lowpass-filter") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "minimp3") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "spectrum-analyzer") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "ws2818-rgb-led-spi-driver") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "12a02hs8k37p0rm9bvm269gh0vhis73bsxg34j67irirg81606bk")))

(define-public crate-beat-detector-0.1 (crate (name "beat-detector") (vers "0.1.2") (deps (list (crate-dep (name "cpal") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.9") (features (quote ("termination"))) (default-features #t) (kind 2)) (crate-dep (name "lowpass-filter") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "minimp3") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "ringbuffer") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "spectrum-analyzer") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ws2818-rgb-led-spi-driver") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "17jxi2nlksgrsd2fghflw3gnda7yyvpz4jl4xn2zjblnpmykq8pa")))

(define-public crate-beatmap_parser-0.1 (crate (name "beatmap_parser") (vers "0.1.0") (deps (list (crate-dep (name "ogg_metadata") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1mh6p1g4llxjswll06arb1j325vjcf2yiqixqvq58zi5lq8fsnxv") (features (quote (("default") ("beatsaver" "reqwest" "tempfile" "zip") ("audio" "ogg_metadata"))))))

(define-public crate-beatmap_parser-0.1 (crate (name "beatmap_parser") (vers "0.1.1") (deps (list (crate-dep (name "ogg_metadata") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1yhqgddzii58yi52g2p2i8rkkc30kk1rxyqb2pbr6kvr378hskwz") (features (quote (("default") ("beatsaver" "reqwest" "tempfile" "zip") ("audio" "ogg_metadata"))))))

(define-public crate-beatmap_parser-0.1 (crate (name "beatmap_parser") (vers "0.1.2") (deps (list (crate-dep (name "ogg_metadata") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "14cq2idfspf5lyi6x2m9dziryfxggbbjw50hfid0i77xfwn9xmn5") (features (quote (("default") ("beatsaver" "reqwest" "tempfile" "zip") ("audio" "ogg_metadata"))))))

(define-public crate-beatmap_parser-0.1 (crate (name "beatmap_parser") (vers "0.1.3") (deps (list (crate-dep (name "ogg_metadata") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0am1lzm8g42ck23zg90k4p2iss562sj30d2286x00p4nb3gvqq4m") (features (quote (("default") ("beatsaver" "reqwest" "tempfile" "zip") ("audio" "ogg_metadata"))))))

(define-public crate-beatmap_parser-0.1 (crate (name "beatmap_parser") (vers "0.1.4") (deps (list (crate-dep (name "ogg_metadata") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0sylaknbfxg9ni9r13s4z3fm3l4a72jri63a1gmk6c61qqlmdqrx") (features (quote (("default") ("beatsaver" "reqwest" "tempfile" "zip") ("audio" "ogg_metadata"))))))

(define-public crate-beatrice-0.1 (crate (name "beatrice") (vers "0.1.0") (deps (list (crate-dep (name "async-fs") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "async-net") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "fixed-buffer") (req "^0.5") (features (quote ("futures-io"))) (default-features #t) (kind 0)) (crate-dep (name "futures-io") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "permit") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "safe-regex") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "safina-executor") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safina-sync") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "safina-timer") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "temp-file") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0imm5m9w4ck39p2w10bd8b3df9qx5h7qynp7w0nv2h8yyvdgzxzs") (features (quote (("urlencoded" "serde" "serde_urlencoded") ("json" "serde" "serde_json") ("default"))))))

(define-public crate-beatrice-0.2 (crate (name "beatrice") (vers "0.2.0") (deps (list (crate-dep (name "async-fs") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "async-net") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "fixed-buffer") (req "^0.5") (features (quote ("futures-io"))) (default-features #t) (kind 0)) (crate-dep (name "futures-io") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "permit") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "safe-regex") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "safina-executor") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safina-sync") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "safina-timer") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "temp-file") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0afzqqk84fvhfbk6fg1gslqda1y5bp84x3n4kzs754axxzgq7a0j") (features (quote (("urlencoded" "serde" "serde_urlencoded") ("json" "serde" "serde_json") ("default"))))))

(define-public crate-beatrice-0.3 (crate (name "beatrice") (vers "0.3.0") (deps (list (crate-dep (name "async-fs") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "async-net") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "fixed-buffer") (req "^0.5") (features (quote ("futures-io"))) (default-features #t) (kind 0)) (crate-dep (name "futures-io") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "permit") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "safe-regex") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "safina-executor") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safina-sync") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "safina-timer") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "temp-file") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0ckmfxdll8c19vyx8a1gpfzzs4nq2wxbg25qjx4lmr3r8dxxrm2i") (features (quote (("urlencoded" "serde" "serde_urlencoded") ("json" "serde" "serde_json") ("default"))))))

(define-public crate-beatrice-0.3 (crate (name "beatrice") (vers "0.3.1") (deps (list (crate-dep (name "async-fs") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "async-net") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "fixed-buffer") (req "^0.5") (features (quote ("futures-io"))) (default-features #t) (kind 0)) (crate-dep (name "futures-io") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "include_dir") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "permit") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "safe-regex") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "safina-executor") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safina-sync") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "safina-timer") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "temp-file") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "19n6pw75bjiwx3hlsj54nddfb8nlyjyq6n9y3wdla9w3ygwdwdkq") (features (quote (("urlencoded" "serde" "serde_urlencoded") ("json" "serde" "serde_json") ("default"))))))

(define-public crate-beatrice-0.3 (crate (name "beatrice") (vers "0.3.2") (deps (list (crate-dep (name "async-fs") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "async-net") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "fixed-buffer") (req "^0.5") (features (quote ("futures-io"))) (default-features #t) (kind 0)) (crate-dep (name "futures-io") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "include_dir") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "permit") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "safe-regex") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "safina-executor") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safina-sync") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "safina-timer") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "temp-file") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "00hfmsm74jfhwfja5adbwrizwdq2pda2f4y9b6ajr04wc4k44q31") (features (quote (("urlencoded" "serde" "serde_urlencoded") ("json" "serde" "serde_json") ("default"))))))

(define-public crate-beats-0.1 (crate (name "beats") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "06qqmgw3nrl1gqzjqjjzwkj2pw4di8rn41xhd7804d8abc1ms3vp")))

(define-public crate-beats-0.1 (crate (name "beats") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "11l7pnwqbihacq9xyplb8as0s6cbjfcvqcmm9rgw58mqa5xf2ig8")))

(define-public crate-beats-0.1 (crate (name "beats") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dwirnz9q638m93ysnsgsy188qqgwspb7h3ylwyc080gjrnpdn21")))

(define-public crate-beats-0.1 (crate (name "beats") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0s0fplwqss9ald5cqhnjjiqlfpmhhqknvmi18sm1x907yi3cgczx")))

(define-public crate-beatsabermaprs-0.1 (crate (name "beatsabermaprs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1xpi357pvd38zacmqzrb94qsbf4bcfqa3pp74577d0dcwffi5sxa")))

(define-public crate-beatsaver-rs-0.0.1 (crate (name "beatsaver-rs") (vers "0.0.1") (deps (list (crate-dep (name "async-std") (req "^1.7.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0r914qpssawbbqx4806dcm9i1f3ccnj5fpcdskhh7w7wdxrxf0rg")))

(define-public crate-beatsaver-rs-0.0.2 (crate (name "beatsaver-rs") (vers "0.0.2") (deps (list (crate-dep (name "async-std") (req ">=1.7.0, <2.0.0") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req ">=0.1.0, <0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req ">=0.5.0, <0.6.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req ">=0.4.0, <0.5.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.4.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req ">=0.10.0, <0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">=1.0.0, <2.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req ">=0.9.0, <0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "surf") (req ">=2.1.0, <3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req ">=0.2.0, <0.3.0") (features (quote ("macros"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req ">=1.5.0, <2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req ">=2.2.0, <3.0.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req ">=1.1.0, <2.0.0") (default-features #t) (kind 0)))) (hash "1k15ib9sb96qfny4cqdmw14c8ih1nyw9yw09n6192xrp0b9106w7") (features (quote (("ureq_backend" "sync" "ureq") ("sync") ("surf_backend" "async-std" "surf" "async") ("reqwest_backend" "tokio" "reqwest" "async") ("hash" "sha2") ("default" "reqwest_backend") ("async" "async-trait"))))))

(define-public crate-beatsaver-rs-0.1 (crate (name "beatsaver-rs") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req ">=1.7.0, <2.0.0") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req ">=0.1.0, <0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req ">=0.5.0, <0.6.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req ">=0.4.0, <0.5.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.4.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req ">=0.10.0, <0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">=1.0.0, <2.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req ">=0.9.0, <0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "surf") (req ">=2.1.0, <3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req ">=0.2.0, <0.3.0") (features (quote ("macros"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req ">=1.5.0, <2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req ">=2.2.0, <3.0.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req ">=1.1.0, <2.0.0") (default-features #t) (kind 0)))) (hash "0212s5lrkl0ww7hrdn2q9s3g700mi5vwlwjxy77gb12pharwmycy") (features (quote (("ureq_backend" "sync" "ureq") ("sync") ("surf_backend" "async-std" "surf" "async") ("reqwest_backend" "tokio" "reqwest" "async") ("hash" "sha2") ("default" "reqwest_backend") ("async" "async-trait"))))))

(define-public crate-beatsaver-rs-0.1 (crate (name "beatsaver-rs") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.7") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.1") (default-features #t) (kind 0)))) (hash "1ra9fm551vp62px6wrjly6b7b3874lhihhlmb3idqm0j77gk75nx") (features (quote (("ureq_backend" "sync" "ureq") ("sync") ("surf_backend" "async-std" "surf" "async") ("reqwest_backend" "tokio" "reqwest" "async") ("hash" "sha2") ("default" "reqwest_backend") ("async" "async-trait"))))))

(define-public crate-beatsaver-rs-0.2 (crate (name "beatsaver-rs") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.7") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("macros" "rt-multi-thread"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.1") (default-features #t) (kind 0)))) (hash "12037m1kbj3bbxs6g1px4blfvqq9di1zajv15rg43bvfasw37791") (features (quote (("ureq_backend" "sync" "ureq") ("sync") ("surf_backend" "async-std" "surf" "async") ("reqwest_backend" "tokio" "reqwest" "async") ("hash" "sha2") ("default" "reqwest_backend") ("async" "async-trait" "futures"))))))

