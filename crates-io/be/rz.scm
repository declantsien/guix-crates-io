(define-module (crates-io be rz) #:use-module (crates-io))

(define-public crate-berzelius-0.0.0 (crate (name "berzelius") (vers "0.0.0") (deps (list (crate-dep (name "berzelius-lints") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "102rgh5v1hg2sf3z9qqc83ld1zlj3jnnzn5inm66kw6b2rsrfl98") (yanked #t)))

(define-public crate-berzelius-lints-0.0.0 (crate (name "berzelius-lints") (vers "0.0.0") (hash "0s1ygqjp7wbxdsscr3pa4gcvvbvgs7c86gsknzh3w0zhwf7j19fh") (yanked #t)))

