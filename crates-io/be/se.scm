(define-module (crates-io be se) #:use-module (crates-io))

(define-public crate-beserial-0.0.0 (crate (name "beserial") (vers "0.0.0") (hash "0bxhyb13gc1j77fnp235i0wn4lv955qhpganxq0iv8npgh42w2dr")))

(define-public crate-beserial-0.1 (crate (name "beserial") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "0slk3i7i9wms1qrzcb8a3l8li21isrrk6vjaa348ilvl1gmp3pph")))

(define-public crate-beserial-0.2 (crate (name "beserial") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jn2rlvz225452ci8nwzs3ni5bbnd4688fzcz39cyv46ahybrj3f")))

(define-public crate-beserial_derive-0.1 (crate (name "beserial_derive") (vers "0.1.0") (deps (list (crate-dep (name "beserial") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1137x3n5nn3fgaxy182g9s6qiwhzpbh2w65sl5lm4cv2qxm5fpi2")))

(define-public crate-beserial_derive-0.2 (crate (name "beserial_derive") (vers "0.2.0") (deps (list (crate-dep (name "beserial") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0c21296zbyc2l73wmsi3jvq0zkgshjc96jgpjd3yz7farcidvkap")))

