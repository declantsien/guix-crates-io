(define-module (crates-io be eb) #:use-module (crates-io))

(define-public crate-beebeep-0.1 (crate (name "beebeep") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "notifica") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "teloxide") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "06f4fsbgzxs5n6byviqhl9yid9lj8fbwjm2m7pn917bynrbws484")))

(define-public crate-beebox-0.1 (crate (name "beebox") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0wxrzp67giqirry3w2323f95mcx2kvfj1pwnyifsgjxwfxpipxvp")))

(define-public crate-beebox-0.1 (crate (name "beebox") (vers "0.1.1") (deps (list (crate-dep (name "cgmath") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "16vn76plr1ayfmbk92r09x0ij7wc9h4m038xlh45adj4bjb3ij23")))

