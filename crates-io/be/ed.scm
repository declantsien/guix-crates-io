(define-module (crates-io be ed) #:use-module (crates-io))

(define-public crate-beediff-0.1 (crate (name "beediff") (vers "0.1.0") (hash "0qvmd74bd7b144vm7d129nnlcim4d6gskwdj2apqyvdh396254v6")))

(define-public crate-beediff-0.1 (crate (name "beediff") (vers "0.1.1") (hash "0jzbq4ahsgk15sxip13q12maf66c1d2qknd50z578ff3kdy0qiaf")))

(define-public crate-beediff-0.1 (crate (name "beediff") (vers "0.1.2") (hash "0mikmksq8y81w605182d9j5gxjl02v4n6mb797f2al820nbqjsda")))

