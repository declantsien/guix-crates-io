(define-module (crates-io be -g) #:use-module (crates-io))

(define-public crate-be-generust-0.1 (crate (name "be-generust") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09n6xkfw1qwb7kjkgqffvxs6yr2pjj2qjny7p1wc0ayhry93iymd")))

