(define-module (crates-io be mp) #:use-module (crates-io))

(define-public crate-bempline-0.1 (crate (name "bempline") (vers "0.1.0") (hash "1n0hn14i2h8k5hffbd43pjljk7rb1ci42yxvj365bpmb41mszy78")))

(define-public crate-bempline-0.2 (crate (name "bempline") (vers "0.2.0") (hash "1rzkc0hg0yb4wh1cs048ww46agc0d1lnk3zc8cw9f5fdikalywqp")))

(define-public crate-bempline-1 (crate (name "bempline") (vers "1.0.0") (hash "07x3985bsa3yiz1y734z3dhaqyw6vsn1izgzkkyhybwvvk5a46zz") (yanked #t)))

(define-public crate-bempline-2 (crate (name "bempline") (vers "2.0.0") (hash "0ri3rpk2ivgdz1wdbpzy74g6w3jc7hp63gc4k13na57njsnz2xpl") (yanked #t)))

(define-public crate-bempline-0.3 (crate (name "bempline") (vers "0.3.0") (hash "0v7kk7fkpc0a8zzfxy62bz0g7h1jswg6n5jjp6hcd2kmp0cf240z")))

(define-public crate-bempline-0.4 (crate (name "bempline") (vers "0.4.0") (hash "1a9rnl9jyp8pg75y6w5ifvy88rg7lkicnq0x57if811l1p7i83cn")))

(define-public crate-bempline-0.4 (crate (name "bempline") (vers "0.4.1") (hash "10izyvlmi6fpyhbyn3igmxxjb49x75wql253rcygnhmkd578yd3p")))

(define-public crate-bempline-0.5 (crate (name "bempline") (vers "0.5.0") (hash "1h2ycbqqbpkgcagy2i5k6972ydpl2ajpc7k4aj1hls9cqrw57gdj")))

(define-public crate-bempline-0.6 (crate (name "bempline") (vers "0.6.0") (hash "0f5mh56w60ldn5fbiw714axv51xknqqdx4zhlv576pvhlhrnnyi8")))

(define-public crate-bempline-0.6 (crate (name "bempline") (vers "0.6.1") (hash "03bypayghlcdmpd3fd1cxkfwa7b739yb86hdjczh804aj5n1wr6y")))

(define-public crate-bempline-0.7 (crate (name "bempline") (vers "0.7.0") (hash "1074dj38cf648ygqa7hl4l6jj2b5yqa008wmwjhd2v5pr8s1rl09")))

(define-public crate-bempline-0.8 (crate (name "bempline") (vers "0.8.0") (hash "1y76gw94ij6zswpv2izk9f4bkvxkssdsgyk4jr3db6i3n6r71w8w")))

(define-public crate-bempline-0.8 (crate (name "bempline") (vers "0.8.1") (hash "0d1k8kklq6gvcx1c5l833m1yff29ypfiwlymgidr6xbb7ys506hs")))

(define-public crate-bempp-0.1 (crate (name "bempp") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "blas-src") (req "^0.10") (features (quote ("blis"))) (default-features #t) (kind 2)) (crate-dep (name "blas-src") (req "^0.10") (features (quote ("accelerate"))) (default-features #t) (target "aarch64-apple-darwin") (kind 2)) (crate-dep (name "cauchy") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "0.5.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "green-kernels") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "kifmm") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "lapack-src") (req "^0.10") (features (quote ("netlib"))) (default-features #t) (kind 2)) (crate-dep (name "lapack-src") (req "^0.10") (features (quote ("accelerate"))) (default-features #t) (target "aarch64-apple-darwin") (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mpi") (req "0.8.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "rlst") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "0fxh21v03b3dh456sa9zvm61sykna668x51sn766s36wgvfkyf8f") (features (quote (("strict")))) (v 2) (features2 (quote (("mpi" "dep:mpi"))))))

