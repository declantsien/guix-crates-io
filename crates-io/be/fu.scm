(define-module (crates-io be fu) #:use-module (crates-io))

(define-public crate-befunge-93-plus-1 (crate (name "befunge-93-plus") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0zclwyzmh1n9qrxp9zpz5pq16f3vyg4qqgpwm8pjw3hanm9s8fwr")))

(define-public crate-befunge-93-plus-1 (crate (name "befunge-93-plus") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0i72pq9622f4jqgvfbi9f14nxlgmjv0fzzgd10bjsmnfavf658d3")))

(define-public crate-befunge-93-plus-1 (crate (name "befunge-93-plus") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0li8avyf8xi3zp8gwrhfj5ldcdd7l2n86y847vzlsvj4cqi5cywj")))

(define-public crate-befunge-93-plus-1 (crate (name "befunge-93-plus") (vers "1.0.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0wbb5rni0mkq4xdk4sbwdfxac53vfr9sra4rbgbfkifin0i30m12")))

(define-public crate-befunge-93-plus-1 (crate (name "befunge-93-plus") (vers "1.0.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1k3vif6dsi4zck06ci43ycg7dks4jcmp0zrwpfp2j7kdvdlfcnyc")))

(define-public crate-befunge-93-plus-1 (crate (name "befunge-93-plus") (vers "1.0.6") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0ws6qni2k92xmv3pbgfywaa0ma9d9i1mwqbfl2c7x3zsi65dkl57")))

(define-public crate-befunge-93-plus-1 (crate (name "befunge-93-plus") (vers "1.0.7") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1w7dklr3n0vzf4y89k5anivk0xbc1nywa731fdbm4yjllrib6n1m")))

(define-public crate-befunge-colored-0.1 (crate (name "befunge-colored") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "06vrify4d4c40mx0cf76b97v0915wj2dkfgp1h4s4ybbks9d1c9j")))

(define-public crate-befunge-colored-0.1 (crate (name "befunge-colored") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "124ml9xjbw5crz9xh1grf557jqxjywd5m77w88zc4mjfg0v0ggas")))

(define-public crate-befunge-colored-0.1 (crate (name "befunge-colored") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0bb0ky5nkgpakbf7n1b4dvnk2xcd3y4xbwrzb4q8vl9k1qcv0vc0")))

