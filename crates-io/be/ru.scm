(define-module (crates-io be ru) #:use-module (crates-io))

(define-public crate-berust-0.1 (crate (name "berust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.6") (default-features #t) (kind 0)))) (hash "0bp4lppb0mjy92ikmc27hsh5gw8g6xpnlgfsvcshh6rqfwgjimn2")))

(define-public crate-berusty-0.1 (crate (name "berusty") (vers "0.1.0") (hash "1fd6bkybhpaksmiw8yrr8gryzh0z4z7n6ryqnmps6srd5mn4sp5w")))

(define-public crate-berusty-0.1 (crate (name "berusty") (vers "0.1.1") (hash "1z3clargxp6bb4s2qv9sin597mwmr1wp32a4hb17bi2grr3lcf8z")))

