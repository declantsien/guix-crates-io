(define-module (crates-io be eh) #:use-module (crates-io))

(define-public crate-beehave-0.0.1 (crate (name "beehave") (vers "0.0.1") (hash "0haj3mrcdw5dhqqww1ksg5lhqa03wspzzmkr66fyyfi9ks0lwn15")))

(define-public crate-beehave-0.0.2 (crate (name "beehave") (vers "0.0.2") (hash "022yqixh1cfw1q3b315dcbk7zvlnlxjzzian1b9da1dzygxq9als")))

(define-public crate-beehave-0.0.3 (crate (name "beehave") (vers "0.0.3") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1yiqapgd4m8mmzlqsb20vsa4x5ccagdx68s26qv6pc26c04dhxih")))

(define-public crate-beehave-0.0.4 (crate (name "beehave") (vers "0.0.4") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)))) (hash "1vv49izgcrvijhnzrhawvp6pbhapmkl52fxhim3axgk26vi2kqdj")))

(define-public crate-beehive-0.1 (crate (name "beehive") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0hl4yn29qx3jm7936xql0y1c4ggdzm6hck8arg9zh1agg7xwb8nh") (features (quote (("serde-1" "serde") ("rand-07" "rand") ("default" "serde-1" "collections") ("collections" "thiserror"))))))

(define-public crate-beehive-0.1 (crate (name "beehive") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0dr3gqfd7xqakfki4qdcxlifv9l8w0kbdxix3kn1l7v9zv60jv0n") (features (quote (("serde-1" "serde") ("rand-07" "rand") ("default" "serde-1" "collections") ("collections" "thiserror"))))))

