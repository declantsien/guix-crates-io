(define-module (crates-io be sk) #:use-module (crates-io))

(define-public crate-beskar-0.0.0 (crate (name "beskar") (vers "0.0.0") (hash "0nm5qk53n2l37f26l2i7834bb90a6lnd69x8ic48cfdmwq8mjxz3")))

(define-public crate-beskar-assets-0.0.0 (crate (name "beskar-assets") (vers "0.0.0") (hash "1gbi7cpd4i8zp6g2zl8jn1wcq76wmhxx05ky04n1n63y9miq07ix")))

(define-public crate-beskar-camera-0.0.0 (crate (name "beskar-camera") (vers "0.0.0") (hash "12zs9xsaapqm7wipkfiyyhsql78d8qyja22cgmmacpvgl33s5hll")))

(define-public crate-beskar-core-0.0.0 (crate (name "beskar-core") (vers "0.0.0") (hash "198lr1jvhjg06894xh2b0wb6yk4xa6fgrm8bznzjcjc27wr084f5")))

(define-public crate-beskar-io-0.0.0 (crate (name "beskar-io") (vers "0.0.0") (hash "1a878my5ldim0dyzxyvqv856mn9dy43d425m6ayhsbkj37j0hihf")))

(define-public crate-beskar-macros-0.0.0 (crate (name "beskar-macros") (vers "0.0.0") (hash "0d5s3vz3kh21k69r8yh46iji610672g6lh1ml83pklm1ns1d74z5")))

(define-public crate-beskar-profiler-0.0.0 (crate (name "beskar-profiler") (vers "0.0.0") (hash "1nvy8irn2ldvncn8d9623ngdk0l2l5xdk4nwia7yzvh648mc2nqs")))

(define-public crate-beskar-scenegraph-0.0.0 (crate (name "beskar-scenegraph") (vers "0.0.0") (hash "1j93iidg1qs6i8ygybd9kbqjvnzbf8dhbvl5jw86xshq6dy5ckih")))

(define-public crate-beskar-ui-0.0.0 (crate (name "beskar-ui") (vers "0.0.0") (hash "173cdmmb2x14xy6z32gh5qq2s4vypdbqcr8dl1xkdmgpy0nydnqk")))

(define-public crate-beskar-util-0.0.0 (crate (name "beskar-util") (vers "0.0.0") (hash "09pfw24jkxi2lvw3pih5mvf8pvzd8zbjclk0b5373p35l1jdg9xj")))

