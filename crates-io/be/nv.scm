(define-module (crates-io be nv) #:use-module (crates-io))

(define-public crate-benv-0.1 (crate (name "benv") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ipk34dffhvc9hzr6hf45q79ai4y3m27ppv7j03rjxmziaprxg2b")))

(define-public crate-benv-0.1 (crate (name "benv") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1v9b2lw6wgsa651na02yq939dhlq18fyza3ksby0f94m8ym6wv7m")))

(define-public crate-benv-0.1 (crate (name "benv") (vers "0.1.2") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vccxgffd2z7wg1mkbcdajy38fd2qznavz0ibc396kbjgjdm21a8")))

(define-public crate-benv-0.1 (crate (name "benv") (vers "0.1.3") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pm4j2pcq067si7m2rsn19iyjda5n4z3vigpx0024wa8vkk2id33")))

(define-public crate-benvolio-0.1 (crate (name "benvolio") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.41") (default-features #t) (kind 2)))) (hash "1s72jilynm53x5y4ljqb1awd06mjabdix73s89m5kny3nyird6vi")))

