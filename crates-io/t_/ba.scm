(define-module (crates-io t_ ba) #:use-module (crates-io))

(define-public crate-t_bang-0.1 (crate (name "t_bang") (vers "0.1.0") (hash "18dnn526vsnfwi2bgkiq41f45hjyydh87dzi1krina118lnjrgkr")))

(define-public crate-t_bang-0.1 (crate (name "t_bang") (vers "0.1.1") (hash "057w2kh9lfyjgmjmpbswry8prnzsnfrklyb7lzgcaff6qx6qmv2n")))

(define-public crate-t_bang-0.1 (crate (name "t_bang") (vers "0.1.2") (hash "092q8p1s26w6x8i192vihslf85dpqz7lw3vbprzy19lgndczs3ps")))

(define-public crate-t_bang-0.1 (crate (name "t_bang") (vers "0.1.3") (hash "128mk59yghdb5mm02q4c5vyjq8maws4hgka9h58dk3j83zyahn1r")))

(define-public crate-t_bang-0.1 (crate (name "t_bang") (vers "0.1.4") (hash "01ldscb4bhny106vmkqbv0fxx3bgmm3yy9hfabysd5cpx554w715")))

