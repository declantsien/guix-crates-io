(define-module (crates-io xa ll) #:use-module (crates-io))

(define-public crate-xalloc-0.1 (crate (name "xalloc") (vers "0.1.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0zm5sgxqws459fz1595qpy4jgxh35f2zpkykpkbq5g144n71hl76") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0kqc7qzpbcvqhfs3kpc639ilvvy6xza8792syv4ldwgid6axbmc1") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.1") (deps (list (crate-dep (name "num-integer") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1z2hcmymdq0c6j74xijxkailk9257zzidb0ixml6gbmv70qz0yhg") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.2") (deps (list (crate-dep (name "num-integer") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0zn00impg1bvnylznvg46xlvarsqbf48yia7bzyv66v33bq96gi9") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.3") (deps (list (crate-dep (name "num-integer") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "07vcnwg48l0bkx6hsys88m8dxj0ydnbr4fli2rw6jwwrh17c9702") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.4") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ar5fq5mjamg6r2ynpx6svrkl3ss26xiw54rcrni867mn7lszakw") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.5") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1zy2b0391xl7na28fbr957wiml6f173m33y7cb63zw5pvrhav3lx") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.6") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1bl6n56w8bhk5aizrpx1liys75xscas70zcqk63iy5ryi5qgdl9w") (features (quote (("nightly"))))))

(define-public crate-xalloc-0.2 (crate (name "xalloc") (vers "0.2.7") (deps (list (crate-dep (name "num") (req ">=0.2.0, <0.4.0") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16cw658j8mjskqdd1w3pp8mb4a6anwrwwjicilfzyjcaszs6ac2p") (features (quote (("nightly"))))))

