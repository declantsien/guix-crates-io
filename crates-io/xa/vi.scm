(define-module (crates-io xa vi) #:use-module (crates-io))

(define-public crate-xavier-0.1 (crate (name "xavier") (vers "0.1.1") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "xavier-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "xavier-internal") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zds1swbgnpdi706lx8pp64hzs8y3nnvsqwqrkqk0g8c2mmsm48r") (yanked #t)))

(define-public crate-xavier-0.1 (crate (name "xavier") (vers "0.1.2") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "xavier-derive") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "xavier-internal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1xnikr71ldmbfpnnzg3yd55gfficq92jlxsa14v70qxdd37q9pb6")))

(define-public crate-xavier-0.1 (crate (name "xavier") (vers "0.1.3") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "xavier-derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "xavier-internal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "03m4a4v48aiy8v3fvqn8dqpmflc59iv7kzfgi00w7ngsnnyx2gf8")))

(define-public crate-xavier-derive-0.1 (crate (name "xavier-derive") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.53") (default-features #t) (kind 0)))) (hash "1q1v1lc1ylrj69mnhymmp736wif3r2cd04y2xi2rxacbaafl88np") (yanked #t)))

(define-public crate-xavier-derive-0.1 (crate (name "xavier-derive") (vers "0.1.2") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.53") (default-features #t) (kind 0)))) (hash "0xnjl8fq92jh7dwgzxlz2kbi87ja9mfjn8hfgfglgvs14l0jicp6")))

(define-public crate-xavier-derive-0.1 (crate (name "xavier-derive") (vers "0.1.3") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.53") (default-features #t) (kind 0)))) (hash "1iljhkfkgfshidxb5fcrg12rbpm5h6mb5y2d570lyq62qjc8rws5")))

(define-public crate-xavier-internal-0.1 (crate (name "xavier-internal") (vers "0.1.1") (deps (list (crate-dep (name "html-escape") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "1byzgbgaaji4q4s0v40lp8plc5wbrpiwcfd50jxd1zcbar53x7yz") (yanked #t)))

(define-public crate-xavier-internal-0.1 (crate (name "xavier-internal") (vers "0.1.2") (deps (list (crate-dep (name "html-escape") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "1capz9fvbvk654lqas7yhlbwjgnljpii8ywxvlp0cqr5n18gj93n")))

