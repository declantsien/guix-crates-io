(define-module (crates-io xa tl) #:use-module (crates-io))

(define-public crate-xatlas-rs-0.1 (crate (name "xatlas-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.37") (default-features #t) (kind 1)))) (hash "194y87ay2jysdymj1b3mxnm7jv480dh29vbyjgfc81sd59ga6s1w") (features (quote (("generate_bindings"))))))

(define-public crate-xatlas-rs-0.1 (crate (name "xatlas-rs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.37") (default-features #t) (kind 1)))) (hash "0npsax5bj1xcn94qcvaxh6w5n9rbhcifqwwzqd2icfzvimx3g3kx") (features (quote (("generate_bindings"))))))

(define-public crate-xatlas-rs-0.1 (crate (name "xatlas-rs") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.37") (default-features #t) (kind 1)))) (hash "0v6anwa40g9nlm71x723jvsvhhb68x9dgz7k8s684dk3al3m2fsm") (features (quote (("generate_bindings"))))))

