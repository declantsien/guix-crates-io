(define-module (crates-io xa gr) #:use-module (crates-io))

(define-public crate-xagrs-0.1 (crate (name "xagrs") (vers "0.1.0") (hash "1z92h49pcrflj9vdplf6nvk4l5gx9ljim7my24pipdr8nlngd5jp")))

(define-public crate-xagrs-0.2 (crate (name "xagrs") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0padfspg5kxp4y183y9qqi1bdkrxyq06p04bk1kqkdrmjxwgqa84")))

