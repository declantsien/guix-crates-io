(define-module (crates-io xa pp) #:use-module (crates-io))

(define-public crate-xapp-0.1 (crate (name "xapp") (vers "0.1.0") (hash "1rr6msry9643cvzps54cb614jawqr0f3n88slgpc71amxkjypcvw")))

(define-public crate-xapp-frame-rust-0.1 (crate (name "xapp-frame-rust") (vers "0.1.0") (hash "1v2fgk8m66wxinhvbnncfrcwdf9scn3j95ncchdbpk9rb6wxbzc9")))

(define-public crate-xapphire-0.1 (crate (name "xapphire") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)))) (hash "0w3q28jxpfdskz7i6nq8wp1rvnd4kdqar5jwh9yzm9sisl4y5jv0")))

