(define-module (crates-io xa rc) #:use-module (crates-io))

(define-public crate-xarc-0.1 (crate (name "xarc") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0c35clf3dc65p384ra6lysmwfw4ni0saf7l0740mr7v06azmmgi0") (yanked #t)))

(define-public crate-xarc-0.2 (crate (name "xarc") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-epoch") (req ">=0.6.0, <0.9.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req ">=0.1.0, <0.3.0") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req ">=0.7.0, <0.8.0") (default-features #t) (kind 0)) (crate-dep (name "jemallocator") (req ">=0.1.8, <0.4.0") (default-features #t) (target "cfg(not(target_env = \"msvc\"))") (kind 2)) (crate-dep (name "rayon") (req ">=0.7.0, <1.5.0") (default-features #t) (kind 2)))) (hash "1zk6zz4nfvhr8qn9hwif0f1bgd3p67f448iq6hl322kfq8qf4v89")))

(define-public crate-xarc-0.3 (crate (name "xarc") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-epoch") (req ">=0.6.0, <0.10.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req ">=0.1.0, <0.4.0") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req ">=0.7.0, <0.9.0") (default-features #t) (kind 0)) (crate-dep (name "jemallocator") (req ">=0.1.8, <0.4.0") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 2)) (crate-dep (name "rayon") (req ">=0.7.0, <1.6.0") (default-features #t) (kind 2)))) (hash "0wch28i6rmjygwj5znf6vf7fdz83fksc1xkf9cyyr1j467pnh70v")))

(define-public crate-xarchive-0.1 (crate (name "xarchive") (vers "0.1.0") (hash "1ix5y498b10grq8lxrvdmpp1r8nrnx1p5c6kx4j4d6992x83wdg0")))

