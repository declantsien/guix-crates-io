(define-module (crates-io xa es) #:use-module (crates-io))

(define-public crate-xaes-gcm-0.1 (crate (name "xaes-gcm") (vers "0.1.0") (deps (list (crate-dep (name "aes-gcm") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "nonce-extension") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0by1i4z0bbf5ahnjgamn4jqhjbsb1ib5nxvc9ankgdd50mj95zh0")))

(define-public crate-xaes-gcm-0.2 (crate (name "xaes-gcm") (vers "0.2.0") (deps (list (crate-dep (name "aes-gcm") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "nonce-extension") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r95d3ah80wy0prdvb3qmwjvd9c53fvr77akmhcg1b24ayxnkssp")))

