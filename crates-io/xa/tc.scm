(define-module (crates-io xa tc) #:use-module (crates-io))

(define-public crate-xatcoder-0.1 (crate (name "xatcoder") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13") (default-features #t) (kind 0)))) (hash "06mifa9z4aad6jrla5h8mm7i2b3wld6l697irhciqkvcsmvm94rw")))

