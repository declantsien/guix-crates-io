(define-module (crates-io xa lg) #:use-module (crates-io))

(define-public crate-xalg-bin-0.1 (crate (name "xalg-bin") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "lib_xalg") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w0pri8vakvxlf9qky3rd75gxsp7rk05w3wl90wrn5mxr58baxl3")))

(define-public crate-xalg-bin-0.1 (crate (name "xalg-bin") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "lib_xalg") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xbdkfa603qicb1z46vkjfg6iqr1qxh19yb12x7pmicz1la6mvlx")))

(define-public crate-xalg-bin-0.1 (crate (name "xalg-bin") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "lib_xalg") (req "^0.1") (default-features #t) (kind 0)))) (hash "0v3kdar4ddm5aga07gg7mcd5bfah62hw8xaq8zcy7cs4bb0agvpl")))

(define-public crate-xalg-bin-0.2 (crate (name "xalg-bin") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "lib_xalg") (req "^0.2") (default-features #t) (kind 0)))) (hash "18g6apzkp8yzh34czlk3gq5zzmqadw6j6vm7j5dfj8k9whx4z4lk")))

(define-public crate-xalg-bin-0.2 (crate (name "xalg-bin") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "lib_xalg") (req "^0.3") (default-features #t) (kind 0)))) (hash "14j31hr5mppskl8a1ii2lamyvmp8pss1000da3s8ymz4vq0i9979")))

