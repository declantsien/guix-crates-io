(define-module (crates-io xa ss) #:use-module (crates-io))

(define-public crate-xassembler-0.1 (crate (name "xassembler") (vers "0.1.0") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "06inza8s37c2dd690lm60xinwxvccwarsahcm603lzlmir1hhaaz")))

(define-public crate-xassembler-0.1 (crate (name "xassembler") (vers "0.1.1") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "141qbywk9nssbl03d3ifbiv4j6pkvy4sqjaabjlp5ywa0k1hf76q")))

(define-public crate-xassembler-0.1 (crate (name "xassembler") (vers "0.1.2") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1g05fpgjs575661ys1lxdw5w1x30x33xbf3mqlhbkjiqn4xshmr0")))

(define-public crate-xassembler-0.2 (crate (name "xassembler") (vers "0.2.0") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1gx4biqpxd148jcnrl6gj02qny20rlamjpdncqdgfh51qmgw6jji")))

(define-public crate-xassembler-0.2 (crate (name "xassembler") (vers "0.2.1") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "05vbj667kz78066qhkf64xyq109dlbn56bngpx3sihs2z9m8jlxz")))

(define-public crate-xassembler-0.2 (crate (name "xassembler") (vers "0.2.2") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1hi6488970aqf76a643251mgbrbhjaz33zbpc4yg3mc9r7c8pz34")))

(define-public crate-xassembler-0.2 (crate (name "xassembler") (vers "0.2.3") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1lf76qm8g9mvmvwyc3z8ic5fa0kc13kc2ns56s57prli2y5p9qws")))

(define-public crate-xassembler-0.2 (crate (name "xassembler") (vers "0.2.4") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "17vjjmxmqczx34d22jp5zb4czgs85972pwawqgnh110sdzcqgkkv")))

(define-public crate-xassembler-0.2 (crate (name "xassembler") (vers "0.2.5") (deps (list (crate-dep (name "honeycomb") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "007gfr5ymxppch916fbicdxlj7cx14lsx6w6dc8gdwniii4lbqk3")))

(define-public crate-xassembler-0.3 (crate (name "xassembler") (vers "0.3.0") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "04pqff85v2p7yz6a66bxcm93rr1hir7a5cc1dps8xawd565zpqi2")))

(define-public crate-xassembler-0.4 (crate (name "xassembler") (vers "0.4.1") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "07j3344s5qd4v96kxicd7q3xnkkbdfa60pcji3j8yz865dh7mg72")))

(define-public crate-xassembler-0.5 (crate (name "xassembler") (vers "0.5.0") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "11n4fgbrdrqvfnz1jkf18j971b2z4814vcq4h3q0f4iv92rx2xb7")))

(define-public crate-xassembler-0.5 (crate (name "xassembler") (vers "0.5.1") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "08dsfndlgcdakw5yv9lh2ij5y4f863z8prwi7fa1yjgcfhsjzapk")))

