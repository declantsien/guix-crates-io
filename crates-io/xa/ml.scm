(define-module (crates-io xa ml) #:use-module (crates-io))

(define-public crate-xaml-island-0.1 (crate (name "xaml-island") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req ">=0.30, <=0.35") (features (quote ("alloc" "Win32_Foundation" "Win32_UI_WindowsAndMessaging" "Win32_System_WinRT_Xaml" "UI_Xaml_Hosting"))) (default-features #t) (kind 0)))) (hash "0wjng3908jdg4vvqvdjgjv068hii78l4xqvp358f1hji5q2k110z")))

