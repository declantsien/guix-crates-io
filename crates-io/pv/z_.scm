(define-module (crates-io pv z_) #:use-module (crates-io))

(define-public crate-pvz_interception_calculator-0.1 (crate (name "pvz_interception_calculator") (vers "0.1.0") (deps (list (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "0dghldhnw87hhs601ck0lv6ybidyk0lha7z1l8vhialqk8pdh0xh") (yanked #t)))

(define-public crate-pvz_interception_calculator-2 (crate (name "pvz_interception_calculator") (vers "2.0.5") (deps (list (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "0wk9nvaa1zl0nama4yzmlsbbyzd8wf1vfinsxwc712h27iidxn86")))

(define-public crate-pvz_interception_calculator-2 (crate (name "pvz_interception_calculator") (vers "2.0.6") (deps (list (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "124hgrj18jg12sp0lja551xhcb0c0fp00v75yk0zcrpigfx7g94g")))

(define-public crate-pvz_interception_calculator-2 (crate (name "pvz_interception_calculator") (vers "2.0.7") (deps (list (crate-dep (name "dyn-fmt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "1av6zpny0d6wqbh6b6r7j3j5pr2mpsb681k7yzgp6c49yndh32rg") (features (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2 (crate (name "pvz_interception_calculator") (vers "2.0.8") (deps (list (crate-dep (name "dyn-fmt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "1429k0aacgyw5s756x8bdri7k6d9lv51j7lsgzrav88472f3k5s4") (features (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2 (crate (name "pvz_interception_calculator") (vers "2.0.9") (deps (list (crate-dep (name "dyn-fmt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "00mz4znbskbk47rgam5aczziwfjpvipfln5vcggxsdxfxq58sylp") (features (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2 (crate (name "pvz_interception_calculator") (vers "2.0.10") (deps (list (crate-dep (name "dyn-fmt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "1ym1hmdppk52yk1ck6dgi2c2l73jfksanz5z784mif1hpf0513im") (features (quote (("zh") ("en"))))))

(define-public crate-pvz_interception_calculator-2 (crate (name "pvz_interception_calculator") (vers "2.0.11") (deps (list (crate-dep (name "dyn-fmt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "1dvlqysclv5hrqjxjz0fvqg0qr5ji56dqzirzqmmxw8xyp90fp4b") (features (quote (("zh") ("en"))))))

