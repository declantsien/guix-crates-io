(define-module (crates-io pv pg) #:use-module (crates-io))

(define-public crate-pvpgn-hash-rs-1 (crate (name "pvpgn-hash-rs") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "11l240hl6mgyk68m3jz7y6858fj5n5wac5ms8bh7h5fnvydv9ng2")))

