(define-module (crates-io pv oc) #:use-module (crates-io))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1v38hvm3fddp1f7hdn88gjjci9fkvaz9d5d83nc6x74b644xn3j8")))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1z1j4kay7pv5yfkq1gsq4ci7ljrnpzvwc5f4mxjr7w66x19lp4p7")))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "05v7g707viwba971bn7527hshmb7yri775zycmrhg0ghl0g34yfc")))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1xh7d4hdf23ffxj1p53xaab0x10rigkalhrzrpqm4ch5jl3620k7")))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.4") (deps (list (crate-dep (name "apodize") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1kyjdvsksgl3x48bzbk8sjh5kxs2iwm5mx5r0wlydzmj2fgm0fsn")))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.5") (deps (list (crate-dep (name "apodize") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "04ci1cpbi6ylxx2hvibbchggvnndkp7r3qr81mngynmiznyiblrp")))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.6") (deps (list (crate-dep (name "apodize") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "15y5ax7zzld7y25p3zajadvx2wfz4qi86mywh8m65ndaakx90mrj")))

(define-public crate-pvoc-0.1 (crate (name "pvoc") (vers "0.1.7") (deps (list (crate-dep (name "apodize") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "0paq84hzwa14q4vbv2rhr9ag32v0rqvxivs63lvvl40ns7p3mc8a")))

