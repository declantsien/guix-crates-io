(define-module (crates-io pv ss) #:use-module (crates-io))

(define-public crate-pvss-0.1 (crate (name "pvss") (vers "0.1.0") (deps (list (crate-dep (name "openssl") (req "^0.9.14") (default-features #t) (kind 0)))) (hash "0ymx9r559f4rqviirszr1qslkmj7bh1mm804kky2p085fjcqk282")))

(define-public crate-pvss-0.1 (crate (name "pvss") (vers "0.1.1") (deps (list (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)))) (hash "1gprym6qr2zyplb8y8bps6vcyqsm6ir1ycqx8gg02bi9lzl5b68b")))

(define-public crate-pvss-0.2 (crate (name "pvss") (vers "0.2.0") (deps (list (crate-dep (name "cryptoxide") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "eccoxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)))) (hash "007pam7l2hyb5939w8yallhc7h6sqhqkmq69yjspwbn4zpz3bkkv") (features (quote (("default" "eccoxide")))) (v 2) (features2 (quote (("openssl" "dep:openssl") ("eccoxide" "dep:eccoxide" "dep:cryptoxide" "dep:getrandom"))))))

