(define-module (crates-io w_ re) #:use-module (crates-io))

(define-public crate-w_result-0.1 (crate (name "w_result") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "~0.3.5") (default-features #t) (kind 0)))) (hash "10qnympnbx85vwjn6vf487mgacw2y4m1d2cfif9bgaf9l68qydzg")))

(define-public crate-w_result-0.1 (crate (name "w_result") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "~0.3.5") (default-features #t) (kind 0)))) (hash "1f7s7fq93fgy9mjnn6pcgzfyb7a0l3dwdzjh08qjnpzr9fp2prbd")))

(define-public crate-w_result-0.1 (crate (name "w_result") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "~0.3.5") (default-features #t) (kind 0)))) (hash "153pc3zjfxv2b3l2wi6p0q57q2a5yna6gi95cfj89wxnc2qvrb25")))

