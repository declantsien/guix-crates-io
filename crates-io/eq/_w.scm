(define-module (crates-io eq _w) #:use-module (crates-io))

(define-public crate-eq_wld-0.1 (crate (name "eq_wld") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)))) (hash "10ylckbbwq5589q8w62q30nkbc6x8bplmdi76bq6bqndxb6im0dl")))

(define-public crate-eq_wld-0.1 (crate (name "eq_wld") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)))) (hash "0kk8243ayhg639fhhjzw15i03xaxqndwyyjwbd3x5phhmaxhb1iw")))

(define-public crate-eq_wld-0.2 (crate (name "eq_wld") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)))) (hash "1pfmn2lzdbimi7xjvrqm19zf3wk6jq0ay0x8m0aaf0lmji2dslp4")))

