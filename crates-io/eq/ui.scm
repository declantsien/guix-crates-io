(define-module (crates-io eq ui) #:use-module (crates-io))

(define-public crate-equihash-0.0.0 (crate (name "equihash") (vers "0.0.0") (hash "1g0vn6vm6h507wivl24k06278glrd9a0pk4cl1sf0q19ly68visj")))

(define-public crate-equihash-0.1 (crate (name "equihash") (vers "0.1.0") (deps (list (crate-dep (name "blake2b_simd") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1042rm46jv3gzs73pi4x9whjh4hdv77v274845szbqvpc67nh9s1")))

(define-public crate-equihash-0.2 (crate (name "equihash") (vers "0.2.0") (deps (list (crate-dep (name "blake2b_simd") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1m9khpkdrp4kr9a1sw9c27bh5vq2jzwc42z80cxpfxw4yxy9smxb")))

(define-public crate-equilibrium-0.1 (crate (name "equilibrium") (vers "0.1.0-alpha") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16sqxk4iwqk64h2pi8j3j73y54hy5hhr21lzvshz4crssqfs8hyg")))

(define-public crate-equinox-0.0.0 (crate (name "equinox") (vers "0.0.0") (hash "1hdwzkmb65fawqvz4xv8skykq6r8hjm5h89ps96qjr7ilh3dg1nh") (yanked #t)))

(define-public crate-equipotent-0.0.0 (crate (name "equipotent") (vers "0.0.0") (hash "1kwki6ixaicdjy161ibm8jha6p541mwkjrqxwb6visha9m13a0cc") (yanked #t)))

(define-public crate-equiv-0.1 (crate (name "equiv") (vers "0.1.1") (hash "18w0hzp628knvky4jq333mkrrdfx3lmmqafzr5d0jzd9ga5a0vly")))

(define-public crate-equiv-0.1 (crate (name "equiv") (vers "0.1.2") (hash "10swv0c3z0g945hfmdavchrpya9d7avg85q5d2bk8ky62hw163ap")))

(define-public crate-equiv-0.1 (crate (name "equiv") (vers "0.1.3") (hash "032zn7s5bp186896w8s3ps4zw5d6dy513vcqzzjcy1xxy381l9dd")))

(define-public crate-equivalence-0.1 (crate (name "equivalence") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "equivalence-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fg2w9wvgqkaqb4pqf0v931zinzsfz4idrdgzdqbhvlh2iydcr1a") (features (quote (("derive" "equivalence-derive") ("default" "derive"))))))

(define-public crate-equivalence-0.1 (crate (name "equivalence") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "equivalence-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qddzzi4mkzqrhv19zh81kmzrbkv27cx4p949qn12igfnqh02n2k") (features (quote (("derive" "equivalence-derive") ("default" "derive"))))))

(define-public crate-equivalence-derive-0.1 (crate (name "equivalence-derive") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pp2cbcv65zi718fw47y6p5jfs7m5vvfsd9w2xl4rddg8rdcnd6h")))

(define-public crate-equivalent-0.1 (crate (name "equivalent") (vers "0.1.0") (hash "0ckdg46jvva8hhgnyjh0ywf1kir3kr0mc8ihgawa0lziswbagzad") (rust-version "1.6")))

(define-public crate-equivalent-1 (crate (name "equivalent") (vers "1.0.0") (hash "18f0q7vd4awiv9bv5mda5yv8lfhpzxspiq8f2jdjqhw0bnygxgw8") (rust-version "1.6")))

(define-public crate-equivalent-1 (crate (name "equivalent") (vers "1.0.1") (hash "1malmx5f4lkfvqasz319lq6gb3ddg19yzf9s8cykfsgzdmyq0hsl") (rust-version "1.6")))

(define-public crate-equix-0.1 (crate (name "equix") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hashx") (req "^0.1.0") (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "permutohedron") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "11nigrl01yrs6lg90qz5axrjy1srmmc90v532cl4llav50l37d6f") (features (quote (("full" "compiler" "hashx/full") ("default" "compiler") ("compiler" "hashx/compiler")))) (rust-version "1.65")))

(define-public crate-equix-0.1 (crate (name "equix") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "hashx") (req "^0.1.1") (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "permutohedron") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "visibility") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0dxwg41jmglp5kicjn5ps1xyz53nny2azx00qls7n64zdm0384pl") (features (quote (("full" "compiler" "hashx/full") ("default" "compiler") ("compiler" "hashx/compiler")))) (rust-version "1.65")))

(define-public crate-equix-0.1 (crate (name "equix") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "hashx") (req "^0.1.2") (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "permutohedron") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "visibility") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16aj0yvwnc2ijg1yylfzmfyvlga3kkydj6044qkypgd1ixgikq15") (features (quote (("full" "compiler" "hashx/full") ("default" "compiler") ("compiler" "hashx/compiler")))) (rust-version "1.65")))

(define-public crate-equix-0.1 (crate (name "equix") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "hashx") (req "^0.1.3") (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "permutohedron") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "visibility") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "122lgxi0zjvz7gw87jg36s58kk7ax4nqx1xhljprl2h7x6d6i8hq") (features (quote (("full" "compiler" "hashx/full") ("default" "compiler") ("compiler" "hashx/compiler")))) (rust-version "1.70")))

(define-public crate-equix-0.1 (crate (name "equix") (vers "0.1.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "hashx") (req "^0.1.3") (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "permutohedron") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "visibility") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fi1nkrfs3yma59ih6g2jb2vbjbwji9b7yp5r3rkpb2l4lhfs98y") (features (quote (("full" "compiler" "hashx/full") ("default" "compiler") ("compiler" "hashx/compiler")))) (rust-version "1.70")))

