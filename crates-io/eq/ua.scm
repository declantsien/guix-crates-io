(define-module (crates-io eq ua) #:use-module (crates-io))

(define-public crate-equal-0.0.0 (crate (name "equal") (vers "0.0.0") (hash "1lw5xgs7rb3ngmv5f1mgaj5z69fwz2sf3id8c0pxypv632a7n5lg") (yanked #t)))

(define-public crate-equalia-0.1 (crate (name "equalia") (vers "0.1.0") (hash "0cr80ha7z54wa3zdkcpjs164g35g7qxzxa1lisjdhfypivmrbwc9")))

(define-public crate-equalia-1 (crate (name "equalia") (vers "1.0.0") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0d556vk4wi4340rhhyvkjhmarsarks2h7ybnqmw3569alaa8p3la")))

(define-public crate-equalia-derive-0.1 (crate (name "equalia-derive") (vers "0.1.0") (hash "0qnbzsljdkfc97zmsv3pq9595h8rhb8lnj91hsabhq20p6v52r8p") (yanked #t)))

(define-public crate-equality-0.1 (crate (name "equality") (vers "0.1.0") (hash "07a7cn375fgfhx11271lgzvrkiz8hlkzsi579czjgnclg9kdvyi9") (yanked #t)))

(define-public crate-equation-0.0.0 (crate (name "equation") (vers "0.0.0") (hash "1z9lij8l1w5gdz3sx801kw7bykcwqcjvadkb6ibkv53w4q8ifsxs")))

(define-public crate-equation-0.1 (crate (name "equation") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.7.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.3") (default-features #t) (kind 0)))) (hash "0pd6c499xarjgnmxn55505rvskry32zs27wx140fv3a5bdska2bf")))

(define-public crate-equation-0.1 (crate (name "equation") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.7.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.3") (default-features #t) (kind 0)))) (hash "1ghry4ddm7s4myjayfrjnyx5c7nq0n2ksdvs74iqx23xqh0g3d1q")))

(define-public crate-equation-0.2 (crate (name "equation") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.7.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.3") (default-features #t) (kind 0)))) (hash "0wsmxb6ai3vkanh4qhzyy60b5d0npdr6ybf1ljn17dynphjfa7pf")))

(define-public crate-equation-0.3 (crate (name "equation") (vers "0.3.0") (deps (list (crate-dep (name "pest") (req "^2.7.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.3") (default-features #t) (kind 0)))) (hash "0x4hja8m4755mqxf2pxv1mihj57f92839c4zaxr8iy5kzcaz8g96")))

(define-public crate-equation-1 (crate (name "equation") (vers "1.0.0") (deps (list (crate-dep (name "pest") (req "^2.7.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.3") (default-features #t) (kind 0)))) (hash "09mqrwxjy1kjkxiqxasg3bi592n40pgrk9kx5dkgm22vcpn054ca")))

(define-public crate-equation-solver-0.1 (crate (name "equation-solver") (vers "0.1.0") (hash "1w03y8z85qryb5xg1jlfps4w19f7p5pfh54rgmxn3n9rj3ajvhhj")))

(define-public crate-equation-solver-0.1 (crate (name "equation-solver") (vers "0.1.1") (hash "0d4j6gi67s96sxcn0f9gxjw335f58k5h7h4swlgg50561crz9qxx")))

(define-public crate-equation_generator-0.1 (crate (name "equation_generator") (vers "0.1.0") (deps (list (crate-dep (name "random-number") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "18bnpm3fiypn5l2k4j7mi1p9liqxi4jx42kcmcvn2f8hhqvb14v5")))

(define-public crate-equation_generator-0.1 (crate (name "equation_generator") (vers "0.1.1") (deps (list (crate-dep (name "random-number") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "02xy9gl5z7zbdna7qdxffk6g1zkdj0plivqdhawgiwzh20vmdhlx")))

(define-public crate-equation_generator-0.1 (crate (name "equation_generator") (vers "0.1.2") (deps (list (crate-dep (name "random-number") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06niv8hqlrbnnip044px8w0px7dz87c9n1v2vmpnkjpiavg8qml0")))

(define-public crate-equationx-0.1 (crate (name "equationx") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer" "regex" "unicode"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "0ll7wgbpf0szb6bdb7p0lcbz9a11m25nvhn2driqv4r55an3h664")))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.0") (deps (list (crate-dep (name "equator-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g8hk35dg9wanrira7gfyh94ca4w38y5g6v8d6ba83lqycgdkxgs") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.1") (deps (list (crate-dep (name "equator-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06gkgvi3srrai1qn5nz69ygabi3svxaprialm5v6kn51q1zw52ps") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.2") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "178gv48jf9l1fhj37zylz0rdsfnabi106k62sspvy7sj4mi6avya") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.3") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0w0si01mfbqzpwckb9vfgf44fn79vaxlafcbgxh45zds1v0yj5s0") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.4") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0m2g0m08dcqs0z92mpplr4jmskh2rxf8y6kf9jwns6qi6q5jjh8p") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.5") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0j2b1rd7avrpq66fdbx7ygifmmnwxa4w3a6lg552sbjdb3jgp038") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.6") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0fm7liy9d20f18h30hcypi51r42ikbzrvfz325zc72fzfiwnfpca") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.7") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "07y7vmd1a53v9yp8frqhppsia272dclv2mnmh3hw1lld232bmdl6") (yanked #t)))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.8") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1rbz0fc8xn1ndh50iac2iqfvjm8zg37y687xcpjfqh6kchj9ld0c")))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.9") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "10kcv936326sp941nkdvb94lqdsk7mw6zlgkdvryhphs9xqr37dw")))

(define-public crate-equator-0.1 (crate (name "equator") (vers "0.1.10") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1s7flp2iph6j9divrnqv0pwx65qssfp7ji2fd2wx42hxm65aic53")))

(define-public crate-equator-0.2 (crate (name "equator") (vers "0.2.0") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1hhsdm419h374zxfsgbjh19xgq5vj8axyr2i6n1d764xpbiv46cw")))

(define-public crate-equator-0.2 (crate (name "equator") (vers "0.2.1") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "06v6abvs78w79ss8pb59h0vbz8zz3ckp657kw6p75x29xxvc8xyy")))

(define-public crate-equator-0.2 (crate (name "equator") (vers "0.2.2") (deps (list (crate-dep (name "assert2") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "equator-macro") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1sk1swwd76rn091gi1m26s10z11dgynb4jfcly228782b8xsapf3")))

(define-public crate-equator-macro-0.1 (crate (name "equator-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jmakba3qm779wshdp8dq9qpdb1vijy22i119g6ymqjakbc21sw5")))

(define-public crate-equator-macro-0.1 (crate (name "equator-macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "007zf37yzsjaf2ga4cwm7am0hnvn53b2agvyc0yk3q731m7vkr70")))

(define-public crate-equator-macro-0.1 (crate (name "equator-macro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i4fij9pla0689z7q5zsrdyc4dzgcxipkzwqmy8nd0dva0hpsmsw")))

(define-public crate-equator-macro-0.1 (crate (name "equator-macro") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1l1rdjskhj72v7nh8gj83a1dhd62crmbfw1fdx2ki7k1x8vw8a31")))

(define-public crate-equator-macro-0.1 (crate (name "equator-macro") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f6sggxhzv3n2wjhyib73jp8ypa5zrldq99vbb33lb1pw9cncmsa")))

(define-public crate-equator-macro-0.1 (crate (name "equator-macro") (vers "0.1.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bpkdiw8xskx3685lr53hf8q49m5wldjaksn052gpxs9k35qml30")))

(define-public crate-equator-macro-0.2 (crate (name "equator-macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c0qikxibxrlihpisrrq3g64dz2sc902z6nrxlqcqpfi9dsw7rgc")))

(define-public crate-equator-macro-0.2 (crate (name "equator-macro") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k0n5q7d6wn9g3fva9z7hr4pr3z494dsb1zja5ima8h3diwpkxiv")))

