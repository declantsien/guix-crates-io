(define-module (crates-io eq so) #:use-module (crates-io))

(define-public crate-eqsolver-0.1 (crate (name "eqsolver") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1lxr98z40km2rps17dm50frgr489g42hgwhml5s0zrnwfxjji8va")))

(define-public crate-eqsolver-0.1 (crate (name "eqsolver") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1k6238mhbcmrzg6pxm3b1qrs8qm4hv7257j8h5r53ymp8789l03m")))

(define-public crate-eqsolver-0.1 (crate (name "eqsolver") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1p10cqimmwz082qjnmwgprw6j40cicmn66a4s0z1bn43r6g43p4w")))

(define-public crate-eqsolver-0.1 (crate (name "eqsolver") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0djbn28rv7rzs1fi1a23w0cq9ac49lxbc87fk20lcbj5z8hkz42l")))

