(define-module (crates-io eq fo) #:use-module (crates-io))

(define-public crate-eqformat_map-0.1 (crate (name "eqformat_map") (vers "0.1.0") (hash "0jy0c6b3hj665scnhb4dqka436jmj5j9rbvzrbsj7zgqi25brbds")))

(define-public crate-eqformat_pfs-0.1 (crate (name "eqformat_pfs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "03w03w0jvmhs4vdpdcawzfv0xllaks87smcw8njrrmz2wqh98ghz")))

(define-public crate-eqformat_pfs-0.1 (crate (name "eqformat_pfs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "05nly30fqjsnnbs2ia2654mypps2d3h2hdihzjkqy40z3r289k3s")))

(define-public crate-eqformat_pfs-0.1 (crate (name "eqformat_pfs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "1f4djlxahgcls1phz9fzz57a0fv6izx7f5q18nhpv4lvk8bw0da7")))

(define-public crate-eqformat_pfs-0.1 (crate (name "eqformat_pfs") (vers "0.1.3") (deps (list (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "1z1jis9wckhjl3rrd4c0vfz154nwx4hxanf5ca32xcznjgr3zfvx")))

(define-public crate-eqformat_pfs-0.1 (crate (name "eqformat_pfs") (vers "0.1.4") (deps (list (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xwbgdvh5mj83i584vr24rxyfrn0pxcr0asrgd27kfbmrnjzayp0")))

(define-public crate-eqformat_pfs-0.1 (crate (name "eqformat_pfs") (vers "0.1.5") (deps (list (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dl1m1x9zb5v6xhi4crklvhz89mzsvkw57pa89q0yrkvrqfp5kyr")))

(define-public crate-eqformat_pfs-0.1 (crate (name "eqformat_pfs") (vers "0.1.6") (deps (list (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "1c4138qzcm4cprqvnzlwy8fhxcrjjl58hzfbglxqw5x3mc74mvbx")))

(define-public crate-eqformat_pfs-0.2 (crate (name "eqformat_pfs") (vers "0.2.0") (deps (list (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "036qsgmib0dfl06l1kx9abkhx83swx26ak0dmp0bj9hp32qp2wcc")))

(define-public crate-eqformat_pfs-0.2 (crate (name "eqformat_pfs") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "0vk3rvg0pixih007fb7pa2dl8y01a1bkr3bv0mb1q981ybv0dcdx")))

(define-public crate-eqformat_s3d-0.1 (crate (name "eqformat_s3d") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "14fpyfhp08cs5kz77sqjdz04ndq2g1mhhvd24q3sq5k8vsdpmka9") (yanked #t)))

