(define-module (crates-io hk vd) #:use-module (crates-io))

(define-public crate-hkvdb-0.1 (crate (name "hkvdb") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vcvpqxgc0yqyrsb520kys1la8a7gvy4p9ak080aq02vpg4phqg6")))

(define-public crate-hkvdb-0.2 (crate (name "hkvdb") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ihb1dmf1fiifnz5rd3j15h2m99vj1xy2xw3063gq2bf8vqh7x84")))

(define-public crate-hkvdb-0.2 (crate (name "hkvdb") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k8kb4i4w5sxcp27rn33ab60mfvib9zdzw0byq28z5wp4a8lx69d")))

(define-public crate-hkvdb-0.3 (crate (name "hkvdb") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "06vknqz39vi358sfflq6jp73sigvs47vj2d031phbzf3788g0qd0")))

