(define-module (crates-io hk t_) #:use-module (crates-io))

(define-public crate-hkt_macros-0.0.3 (crate (name "hkt_macros") (vers "0.0.3") (deps (list (crate-dep (name "hkt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "shoggoth") (req "*") (default-features #t) (kind 0)))) (hash "0f240swn647slf5m1cn4ga7mc8plixchkpxwmv442hignzfrj19m")))

