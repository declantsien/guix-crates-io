(define-module (crates-io hk ul) #:use-module (crates-io))

(define-public crate-hkulibrary-0.1 (crate (name "hkulibrary") (vers "0.1.0") (deps (list (crate-dep (name "authku") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1k009k8j9qnzha4pjiil083fxmc00qnbdf90vqvba7w7gq1jw7f2")))

(define-public crate-hkulibrary-0.2 (crate (name "hkulibrary") (vers "0.2.0") (deps (list (crate-dep (name "authku") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "soup") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0c70m77kkvg54b0d17h81ck7x3wl0c04nr957kjwgllj8w06h6a6")))

