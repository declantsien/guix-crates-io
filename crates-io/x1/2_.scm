(define-module (crates-io x1 #{2_}#) #:use-module (crates-io))

(define-public crate-x12_alt-0.1 (crate (name "x12_alt") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mqrav2dryp63am2h3dvhdmldny2zjs1hgsqg4gj8l72pb2yzwzb") (features (quote (("8010") ("5010"))))))

