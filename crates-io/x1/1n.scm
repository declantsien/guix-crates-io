(define-module (crates-io x1 #{1n}#) #:use-module (crates-io))

(define-public crate-x11nas-2 (crate (name "x11nas") (vers "2.19.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "19z0mwr1l13rdf913b50n983jf3rp170f8knwn836yv859919kss") (features (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("std") ("glx") ("dpms") ("dox") ("default" "std"))))))

(define-public crate-x11nas-2 (crate (name "x11nas") (vers "2.19.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "071fhskxhjgzg9vvy0zlj3sn32dzg6820cjv20xm8zq7rcwm5393") (features (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("std") ("glx") ("dpms") ("dox") ("default" "std"))))))

