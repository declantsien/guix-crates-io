(define-module (crates-io x1 #{1c}#) #:use-module (crates-io))

(define-public crate-x11cap-0.1 (crate (name "x11cap") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "*") (default-features #t) (kind 0)))) (hash "1jyc2slchd6w0y5pqrpkjishk135yl4pd17cr2vhn5zykwmbd9n9")))

(define-public crate-x11cap-0.2 (crate (name "x11cap") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "shuteye") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "x11") (req "^2.11") (default-features #t) (kind 0)))) (hash "1f6fdr8gghc9k4xnnkskb8h9akz9b7l5hwcdjyipbris3bfnm68i") (yanked #t)))

(define-public crate-x11cap-0.2 (crate (name "x11cap") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "shuteye") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "x11") (req "^2.11") (default-features #t) (kind 0)))) (hash "01vkin9ywwp82mbmyxpr6zvnpjzv067lkmz26zf0zyfx6s1rmwhk")))

(define-public crate-x11cap-0.3 (crate (name "x11cap") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "shuteye") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "x11") (req "^2.11") (default-features #t) (kind 0)))) (hash "1si2j00jdz6h9qzic1znm6yf21rg7yn51xj7db62258v4c5wwmir")))

(define-public crate-x11cap-0.3 (crate (name "x11cap") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "shuteye") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "x11") (req "^2.11") (default-features #t) (kind 0)))) (hash "1iy5wfc66kgwfd0dxmp8k6qr7yi5g37abk81ms27hdmjgbpclywi")))

(define-public crate-x11cap-0.4 (crate (name "x11cap") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "shuteye") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "x11") (req "^2.11") (default-features #t) (kind 0)))) (hash "17nai13z9ww8f84p1cwakn7wpa6hvcrr9k0spifg57f2aaafbxcm") (yanked #t)))

(define-public crate-x11cap-0.4 (crate (name "x11cap") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "shuteye") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "x11") (req "^2.11") (default-features #t) (kind 0)))) (hash "16f3cd94clj07sfl6l80gkandxq4g15g5n1d8r6ph7ybavsyvk0n")))

