(define-module (crates-io x1 #{1_}#) #:use-module (crates-io))

(define-public crate-x11_keypress_detect-0.1 (crate (name "x11_keypress_detect") (vers "0.1.0") (deps (list (crate-dep (name "x11") (req "^2.21.0") (default-features #t) (kind 0)))) (hash "08qp91xw05599rdl5r218i6vgnsa6887frvc26vk6lqv74skm9xa") (yanked #t)))

(define-public crate-x11_keypress_detect-0.1 (crate (name "x11_keypress_detect") (vers "0.1.1") (deps (list (crate-dep (name "x11") (req "^2.21.0") (default-features #t) (kind 0)))) (hash "1gdlgdjfa894f1p0gk366pjxaqzbwgc2i7gs9m6zcv96zv7xficw") (yanked #t)))

(define-public crate-x11_keypress_detect-0.2 (crate (name "x11_keypress_detect") (vers "0.2.1") (deps (list (crate-dep (name "x11") (req "^2.21.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "1richdn8lzasvqk7ckgj7srgb1x350b9qsw3dqrwd9mvc1wmmqjg") (yanked #t)))

(define-public crate-x11_keypress_detect-0.2 (crate (name "x11_keypress_detect") (vers "0.2.2") (deps (list (crate-dep (name "x11") (req "^2.21.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0smryhgwxv4347mlfkhjbyxs21gv22h1x2z7ycikfm93vys8j1lg")))

