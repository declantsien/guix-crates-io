(define-module (crates-io x1 #{1o}#) #:use-module (crates-io))

(define-public crate-x11oo-0.1 (crate (name "x11oo") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0s7wfyp1vwggdkvhdyjgxf5qzk7qnxb8hzryyjml09m4wrwxczgv") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.1 (crate (name "x11oo") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0320ysxrdp5xqvkc5p3r3k1jkhdxmihycil96xidr5pwhapjy5hd") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.1 (crate (name "x11oo") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0qq5qwpl6gzxrlnfnzkbjic1l3xkxpwp1z19x9z76r2nmhcway5y") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.1 (crate (name "x11oo") (vers "0.1.3") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "1ncyasrn0f5zy2qyagjq71xxxffax17bp87c1d004clp7jk9yqf9") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.1 (crate (name "x11oo") (vers "0.1.4") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0k4bzw5zr6iczjap8c8ncvghlnr85r57kx7fvz1n5grki1z865c8") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.2 (crate (name "x11oo") (vers "0.2.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0k6nniz6aw9yy9b0y2j8x429hq6kd2kh7w8z3g4kifrk0zrvbz8w") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.2 (crate (name "x11oo") (vers "0.2.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0m5xda6dv1wc68dmk8zb8vrlzd2i2hz4r0djw5azj5skd08hqv4s") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.2 (crate (name "x11oo") (vers "0.2.3") (deps (list (crate-dep (name "pkg-config") (req "^0.3.24") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.20.1") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0r6bbaxc3rsndvmi85ivwc0xi5x3mwrrj69s1v18krv0wf1p5vq3") (features (quote (("xfixes"))))))

(define-public crate-x11oo-0.3 (crate (name "x11oo") (vers "0.3.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.21.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "1yjmp9ciab35985y9aggmjh84nc7hsqf1f50xycvqy41dl8xw42z") (features (quote (("xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.4 (crate (name "x11oo") (vers "0.4.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.21.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "1dhcv44bnbvlv6lbbx78ji1p936l8g8ssbqg3b7dgwf424ncrvfp") (features (quote (("xfixes" "x11/xfixes")))) (yanked #t)))

(define-public crate-x11oo-0.4 (crate (name "x11oo") (vers "0.4.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.21.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0q8b3kn517w6q32d101llxbmpsa1qsk8l5vvgwqdm8g2kj5r7kx8") (features (quote (("xfixes" "x11/xfixes"))))))

(define-public crate-x11oo-0.4 (crate (name "x11oo") (vers "0.4.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.21.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0ina23gbsbckpj5lfmi1692j18yj0jfagf3lrivsmzlic78h21xv") (features (quote (("xfixes" "x11/xfixes"))))))

