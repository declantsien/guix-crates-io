(define-module (crates-io x1 #{2p}#) #:use-module (crates-io))

(define-public crate-x12pp-0.1 (crate (name "x12pp") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml" "wrap_help"))) (kind 0)) (crate-dep (name "memchr") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0nxwy64fichsb0a4fh8062r8barc0fd72ws7daz6j1krb9yyvmpw")))

(define-public crate-x12pp-0.2 (crate (name "x12pp") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "memchr") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0ggm821py3l6lc6djj04ivf6bxyfirliry31yy8inaga6jrmmdz5")))

(define-public crate-x12pp-0.3 (crate (name "x12pp") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "memchr") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1yv2m90wh5v1nvxga958zqi56r1w173vh5cv6hrhpbis2s6f4zk7")))

