(define-module (crates-io fe n4) #:use-module (crates-io))

(define-public crate-fen4-0.1 (crate (name "fen4") (vers "0.1.0") (hash "1a8m3s3plc2x1y4jy8yk5zanlr70drvq9wrlwfqir07a506ls5p7")))

(define-public crate-fen4-0.2 (crate (name "fen4") (vers "0.2.0") (hash "076f9kiqwkncz9qlizd4h6lg89zwgcc9fns9b2fri04pwdppzgab")))

(define-public crate-fen4-0.3 (crate (name "fen4") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)))) (hash "0dxsj8pj765vhl4fmvfdsrbaa0bbdhscnclizji4n55r6yncm1dq")))

(define-public crate-fen4-0.4 (crate (name "fen4") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "05vghfa8dgwbr8pyna65r694am47nh96d8r3phqljmj5aljl8fdi")))

(define-public crate-fen4-0.5 (crate (name "fen4") (vers "0.5.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0if30scz65m6fw4a4vsibvf9s853sl81phx2skm1j6ax3h1ra05l")))

(define-public crate-fen4-0.6 (crate (name "fen4") (vers "0.6.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xh06bwhhpaj7h8nrgd157a4lk1f9xc40c1ljlaqx0qiw7jpkcrv")))

(define-public crate-fen4-0.7 (crate (name "fen4") (vers "0.7.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "02n19lsyv1dy8893qg04gizkc2bgvp82mrdlk3l57zf3977a91nw")))

