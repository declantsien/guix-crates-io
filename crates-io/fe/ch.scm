(define-module (crates-io fe ch) #:use-module (crates-io))

(define-public crate-fecho-0.1 (crate (name "fecho") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sv4b2mpgjagzdkn8v05jl2ca88bp4374zy750q06x4773z8bbqx")))

(define-public crate-fecho-0.2 (crate (name "fecho") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hf2vam4sgqrz586wiipcxlgrnn0q70nxs934qgvi1nwzx5mvzdz")))

(define-public crate-fecho-0.3 (crate (name "fecho") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jg9z28i3iwghdgjvh9nblg77q8bgck4b2bn3pcbji99v9qjfcgl") (yanked #t)))

(define-public crate-fecho-0.3 (crate (name "fecho") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vl26m7x3axzywdhhqr0zx73bjkppgni335s4gb28ks67c2ppnzh") (yanked #t)))

(define-public crate-fecho-0.3 (crate (name "fecho") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1df0ig24azfib6ag9sz0aaz07m8z9aza41a07s5j2wgqsz8fxqjw")))

