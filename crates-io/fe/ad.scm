(define-module (crates-io fe ad) #:use-module (crates-io))

(define-public crate-feaders-0.2 (crate (name "feaders") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.58") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "12bgwm7jizk1fmb64frprc4qzdgfvajhcw5vvxxbz2qjxx4jdfhz")))

