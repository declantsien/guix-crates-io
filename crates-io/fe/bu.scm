(define-module (crates-io fe bu) #:use-module (crates-io))

(define-public crate-febug-0.1 (crate (name "febug") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)))) (hash "1ac9qz1rgy842lxdd177cnayaw2gm0jk6c3n0ij867l7gn3kl9pf")))

(define-public crate-febug-0.1 (crate (name "febug") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)))) (hash "1xz15sval1lzfnwmshi3b14f4y2iq4bscfmnrqi0r34h8jr2qz1k")))

(define-public crate-febug-0.1 (crate (name "febug") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)))) (hash "1w24r8wadncsdiadqfpmxs1b0wg2draq51184391mhj2dnq6wimc")))

(define-public crate-febug-0.2 (crate (name "febug") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)))) (hash "0kf17xyaa55dnyh5zrj3hkbav8nalvwndwa34sn6l195rlpsbqnk")))

(define-public crate-febug-1 (crate (name "febug") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)))) (hash "0nmsbwag28fdbzd9rbi2ka39jf0n7kkmy438qv0gfpdg0vgragm5")))

