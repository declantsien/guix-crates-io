(define-module (crates-io fe nt) #:use-module (crates-io))

(define-public crate-fentrail-0.1 (crate (name "fentrail") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "libft") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "16s3kwsg7dqpxri8r49chyy1igc7qjwxzv82vd53bh7mg6w7y9nb")))

