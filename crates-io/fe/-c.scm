(define-module (crates-io fe -c) #:use-module (crates-io))

(define-public crate-fe-cli-0.1 (crate (name "fe-cli") (vers "0.1.0") (hash "1b3gj8blpws7rj6qwx6x16skf3hb0f6d72y9dcg8nya76jq4l0xv")))

(define-public crate-fe-compiler-0.1 (crate (name "fe-compiler") (vers "0.1.0") (hash "02ywg6jagdqh3vj3c8f721dp2bxhzj0ghrdv4r3971qjadzqjy3q")))

