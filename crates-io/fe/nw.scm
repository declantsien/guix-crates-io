(define-module (crates-io fe nw) #:use-module (crates-io))

(define-public crate-fenwick-0.1 (crate (name "fenwick") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.0-pre.0") (default-features #t) (kind 2)))) (hash "0db1jy6gdnhm4a9c6igk5kvjaafrf9nf2cjwlinimqycnjrvzsy4")))

(define-public crate-fenwick-0.1 (crate (name "fenwick") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.0-pre.0") (default-features #t) (kind 2)))) (hash "0yjbzkpw41253g7fqrynz3kmxqnla22bh10q47xghj6npn6s7mk3")))

(define-public crate-fenwick-0.1 (crate (name "fenwick") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.5.0-pre.0") (default-features #t) (kind 2)))) (hash "0w9sf4gjb0kvi0g82w27rpajpf20m5lydzxnv5arr2y16974m8d6")))

(define-public crate-fenwick-0.2 (crate (name "fenwick") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5.0-pre.0") (default-features #t) (kind 2)))) (hash "1y2m12wli9cwglasdwk78m5cl34irz38cgz4bx0q3qyadp2cv920")))

(define-public crate-fenwick-1 (crate (name "fenwick") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.5.0-pre.0") (default-features #t) (kind 2)))) (hash "12zjg6jfayb0ggw275jwg32618azrafkjh03f2gn1zanjhkcgn2s")))

(define-public crate-fenwick-2 (crate (name "fenwick") (vers "2.0.0") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1xhmfk632ilb3rc6kfrpqx3ynfvvisqid3b9qx1spvmri14s25x4")))

(define-public crate-fenwick-2 (crate (name "fenwick") (vers "2.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0c0h6vimbr2vx01x6w3440ksrkwg8mr3v01xshwplhgxh6g2ba4d")))

(define-public crate-fenwick-bit-tree-0.1 (crate (name "fenwick-bit-tree") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1a4rkfc95kbfsfrrhqvmnwl2rh2ccav0dvpqmb9w040yh3js3fmz") (features (quote (("benchmarks")))) (rust-version "1.76.0")))

(define-public crate-fenwick-bit-tree-0.1 (crate (name "fenwick-bit-tree") (vers "0.1.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "099z2c37mav55k1aa3iywkf320bgs3fnpb78w0m1jc1bjwc1s0k6") (features (quote (("benchmarks")))) (rust-version "1.76.0")))

(define-public crate-fenwick-bit-tree-1 (crate (name "fenwick-bit-tree") (vers "1.0.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1jsvdk5xrkgz22c7484659rf46dfmap61w2f6ksb9zdc1w5rh52y") (features (quote (("benchmarks")))) (rust-version "1.76.0")))

(define-public crate-fenwick-bit-tree-2 (crate (name "fenwick-bit-tree") (vers "2.0.0") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.1") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m3bfqlh80pnf4f37yrw4vasca9dmcp2da8ypsxc8vpgpdaqmdwm") (features (quote (("benchmarks")))) (rust-version "1.76.0")))

(define-public crate-fenwick-bit-tree-2 (crate (name "fenwick-bit-tree") (vers "2.0.1") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.1") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zc8w2p65lzhq5r3vi3vqpz453nv89sb976a9dnrqhc8s2ixwjdb") (features (quote (("benchmarks")))) (rust-version "1.76.0")))

(define-public crate-fenwick-bit-tree-2 (crate (name "fenwick-bit-tree") (vers "2.0.2") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.1") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0l3pbjf55jfjs90inhirgf6s32w4qmmqa6l3irn65njx25qmrbhh") (features (quote (("benchmarks")))) (rust-version "1.76.0")))

(define-public crate-fenwick-tree-0.0.1 (crate (name "fenwick-tree") (vers "0.0.1") (hash "0974jzd2jlx1phr7l38nrkmwzn5ic666mrss512dh70j8zwnlkza")))

(define-public crate-fenwick-tree-0.0.2 (crate (name "fenwick-tree") (vers "0.0.2") (hash "0k4kq6m72gbjf30a3z45yrq7gbxc1cr9i7vknzji3cg793837giw")))

(define-public crate-fenwick-tree-0.1 (crate (name "fenwick-tree") (vers "0.1.0") (hash "0pqrj3fswij67f0b9pj37ypishwql9lwbrflrn3jiy6mrjy2h13b")))

