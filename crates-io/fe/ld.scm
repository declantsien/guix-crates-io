(define-module (crates-io fe ld) #:use-module (crates-io))

(define-public crate-feld-0.0.1 (crate (name "feld") (vers "0.0.1") (hash "1war4zjapyg37k6ra1xy5p00clsh6jkl27m4yyb3vpzczvmgxpl4")))

(define-public crate-feldera-0.0.1 (crate (name "feldera") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "indicatif-log-bridge") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "stream" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "similar") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "1b0ckmsvxgdhx3wx678q3522yc3khfjrqrm0n19pvkqww9yapf37")))

(define-public crate-feldspar-0.1 (crate (name "feldspar") (vers "0.1.0") (hash "1172gajqim0dgsc0pyfii8mcyjw9pkbr88z5mm2c67dip7rwninc")))

(define-public crate-feldspars-0.1 (crate (name "feldspars") (vers "0.1.0") (hash "08p08w1nj7a50k6i7qv5zvbh9j6fn5amm1lg0mwg1n54bk86jdfn")))

