(define-module (crates-io fe nv) #:use-module (crates-io))

(define-public crate-fenv-0.2 (crate (name "fenv") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "is-terminal") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0zan4wvq2fsbrikfgclbxl79gbz7wmnwahlpx1rxnvqy23yida84") (rust-version "1.64")))

(define-public crate-fenv-bind-0.0.1 (crate (name "fenv-bind") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0cpaqkgqmr5sy396p22hzg4m7qmd7bmwq9aq41h8q9ihc5gklmvk")))

(define-public crate-fenv-bind-0.0.2 (crate (name "fenv-bind") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "17g4il0lbwzgfs0ngzkz14rxqiqcqk6wb4lvgjq2ay5v2j80dkds")))

(define-public crate-fenv-bind-0.0.3 (crate (name "fenv-bind") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0w72r62xnr8zlyamngjdqrwxick176ylim5a8vqrvnnnk9bm818q")))

(define-public crate-fenv-bind-0.0.4 (crate (name "fenv-bind") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0vzcd9lhcq15gbfly06s04y5g9v56rld0mrm464d4mjxbnlg7d16")))

