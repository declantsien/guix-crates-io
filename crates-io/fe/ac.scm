(define-module (crates-io fe ac) #:use-module (crates-io))

(define-public crate-feach-0.1 (crate (name "feach") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0x90ws65f92ya1ba0p826mvib7b7c5xhdq3s4n0ra4jh9ysh4k9f")))

