(define-module (crates-io fe rl) #:use-module (crates-io))

(define-public crate-ferl-0.0.1 (crate (name "ferl") (vers "0.0.1") (hash "1jj6qk359863c6acpl1mj8ysiwk10p3yahys5ylrwg8h0sj7wwd6")))

(define-public crate-ferlet-0.0.0 (crate (name "ferlet") (vers "0.0.0") (hash "0lg8gqww5l4ky3cn05js7rqv3l0kaliwnw3zx76v72h6zmzd7w78")))

