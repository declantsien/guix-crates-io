(define-module (crates-io fe xp) #:use-module (crates-io))

(define-public crate-fexpr-0.1 (crate (name "fexpr") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "1yp0my7anp4hczxqd9r2lqnpvvzfsp5irjzhjlzi1p1x1hjzfwxr")))

(define-public crate-fexpr-0.1 (crate (name "fexpr") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0f3fyw8hrwmv9sssxknn0hxzcrl1h3382dvx9rm7d2byrg0b1lsk")))

