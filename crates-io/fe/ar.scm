(define-module (crates-io fe ar) #:use-module (crates-io))

(define-public crate-fearless-0.0.0 (crate (name "fearless") (vers "0.0.0") (hash "0bj4qxpbhh5529vpyd5930m49gi3i0y8vya5vz44v182r6rxm8d5")))

(define-public crate-fearless_simd-0.1 (crate (name "fearless_simd") (vers "0.1.0") (hash "0rcn8l0qfsgy2qfna4x2vnvz5v60j84c9b4fanfgxw9bwz7yr89k")))

(define-public crate-fearless_simd-0.1 (crate (name "fearless_simd") (vers "0.1.1") (hash "146s33kc5ficzsyz813vhb5wnayfk5asgd57jlcmpzyndclfr3hd")))

