(define-module (crates-io fe as) #:use-module (crates-io))

(define-public crate-feast-0.1 (crate (name "feast") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.3") (default-features #t) (kind 2)))) (hash "0yg8q1cyp40wzv5naz3617kpnm6rk8v2qk2lb7h2rs69ddvrjb0h")))

