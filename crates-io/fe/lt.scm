(define-module (crates-io fe lt) #:use-module (crates-io))

(define-public crate-felt-0.1 (crate (name "felt") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req ">=3.0.1, <4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">=1.0.117, <2.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req ">=0.5.0, <0.6.0") (default-features #t) (kind 0)))) (hash "1sfrzks8zdl7kkv6ldj9svv9a38jf9xq0l2yjwagh1505alsvnkd")))

(define-public crate-felt-0.1 (crate (name "felt") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req ">=3.0.1, <4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">=1.0.117, <2.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req ">=0.5.0, <0.6.0") (default-features #t) (kind 0)))) (hash "188j8c712n4w5jxv2bqmrxxbkjzxmx2mrpa0kgff2qsr99pv5nzv")))

(define-public crate-felt-0.2 (crate (name "felt") (vers "0.2.0") (deps (list (crate-dep (name "dirs") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0jichl837pynlzg7w2xcwmywnz27bgzdjq5m7jfislk3x4zjgdlz")))

