(define-module (crates-io fe bi) #:use-module (crates-io))

(define-public crate-febits-0.1 (crate (name "febits") (vers "0.1.0") (hash "1g5vv9ppg1jsar5z04ygxb4k7q8z5qkkkis4g1a6q90yncw6x7iw") (yanked #t)))

(define-public crate-febits-0.1 (crate (name "febits") (vers "0.1.1") (hash "0wp4wk1rb08m5rls5lf8dq346psw549law4nrim385qmxmyjf73x") (yanked #t)))

(define-public crate-febits-0.1 (crate (name "febits") (vers "0.1.2") (hash "02dl57vf59hnl5jg953l0fzwrphzgakwwgkpi2wlbsn0d8sfzz3n")))

(define-public crate-febits-0.1 (crate (name "febits") (vers "0.1.3") (hash "0wf73fnxr21xsv1asgf0fxar750jcfxc7v7avlg1532bmkxy02n3")))

