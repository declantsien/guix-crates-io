(define-module (crates-io fe lf) #:use-module (crates-io))

(define-public crate-felfel-0.1 (crate (name "felfel") (vers "0.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "0gmvczkwrv3r7cq4rqivdmnxcdg7iab852f6s9basj71mazzw6ri")))

(define-public crate-felfel-0.1 (crate (name "felfel") (vers "0.1.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "1xv976hq74ylk4q0gqmhhk4f0790qm1kca93jwy0px3z1qj9axpk")))

