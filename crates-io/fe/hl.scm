(define-module (crates-io fe hl) #:use-module (crates-io))

(define-public crate-fehler-1 (crate (name "fehler") (vers "1.0.0-alpha.0") (deps (list (crate-dep (name "fehler-macros") (req "^0.1.0-alpha.0") (default-features #t) (kind 0)))) (hash "1hq01fnaxlfsd15rai40f4x0sxa622a99i43ibwaqpawi3wzh7bm")))

(define-public crate-fehler-1 (crate (name "fehler") (vers "1.0.0-alpha.1") (deps (list (crate-dep (name "fehler-macros") (req "^1.0.0-alpha.1") (default-features #t) (kind 0)))) (hash "1xf3sj75n1jhq4nblis2kqil4ra0fjbdmls0lzn7y0d9bf0s3879")))

(define-public crate-fehler-1 (crate (name "fehler") (vers "1.0.0-alpha.2") (deps (list (crate-dep (name "fehler-macros") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)))) (hash "0kjkyyyl46ys0154vl7w71q4q5b4s372kx7l68d9c6c08acbw36i")))

(define-public crate-fehler-1 (crate (name "fehler") (vers "1.0.0") (deps (list (crate-dep (name "fehler-macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0d9nk0nimhrqhlwsm42kmg6bwhfqscnfddj70xawsa50kgj9ywnm")))

(define-public crate-fehler-macros-0.1 (crate (name "fehler-macros") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("default" "fold" "full"))) (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0x4a38qjm2qijw7r4z2zggmakgyq1467f0c063564c09ggwdwi6x")))

(define-public crate-fehler-macros-1 (crate (name "fehler-macros") (vers "1.0.0-alpha.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("default" "fold" "full"))) (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1x57z1d7sbf4hixcd56l7gpy9a0d2mk5l423b86z35nraxrh7gsv")))

(define-public crate-fehler-macros-1 (crate (name "fehler-macros") (vers "1.0.0-alpha.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("default" "fold" "full"))) (default-features #t) (kind 0)))) (hash "118wvakricbg3nzi1lv44439q25mawr6wbxrsz7v5192jdamapcs")))

(define-public crate-fehler-macros-1 (crate (name "fehler-macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("default" "fold" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1y808jbwbngji40zny0b0dvxsw9a76g6fl1c5qigmfsy0jqsrdfc")))

(define-public crate-fehler-more-0.1 (crate (name "fehler-more") (vers "0.1.0") (deps (list (crate-dep (name "fehler") (req "^1.0.0-alpha") (default-features #t) (kind 0)))) (hash "0azd1rkgdn5v6mj430w8g73rbmm4228lv284fdq91iw2x2x5jkz4")))

