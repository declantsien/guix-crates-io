(define-module (crates-io fe _o) #:use-module (crates-io))

(define-public crate-fe_osi-0.1 (crate (name "fe_osi") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)) (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "09sczy390wili013g8f935j6i09bbdps62fpfr8dkysadji0ycr6")))

(define-public crate-fe_osi-0.1 (crate (name "fe_osi") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)) (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "00qpmvqqx4pipiw9vrjwq3fh5cx4pm87birp49h66r5kl9v1kqi6")))

