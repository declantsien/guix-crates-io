(define-module (crates-io fe ns) #:use-module (crates-io))

(define-public crate-fenster-0.1 (crate (name "fenster") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0x1p6plcqhms957rlhg0scxxmp35a0civhhksf7kq5mal8chqc3z")))

