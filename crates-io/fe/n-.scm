(define-module (crates-io fe n-) #:use-module (crates-io))

(define-public crate-fen-rs-0.1 (crate (name "fen-rs") (vers "0.1.0") (hash "17rfzvl55liryvzpif1sfhclvjfbkaic9nzfpfmjm08ba61gfpky")))

(define-public crate-fen-rs-0.1 (crate (name "fen-rs") (vers "0.1.1") (hash "0c537kg36lj45cwbh95b3b9q0i4plk9i81xkvgkd43k9fiicc7mx")))

(define-public crate-fen-rs-0.1 (crate (name "fen-rs") (vers "0.1.2") (hash "09v252x00lplmp2zi6a0g8hxknyhia69g9q61vqrmck02s6c45bz")))

(define-public crate-fen-rs-0.1 (crate (name "fen-rs") (vers "0.1.3") (hash "16y99fjm1savs8cfrfw3lqcv3qgklg6kb9j26alfkqmjyx5hpqwl")))

(define-public crate-fen-rs-0.1 (crate (name "fen-rs") (vers "0.1.4") (hash "12yzzr15b2pfmwl181vybgcrdmwsivybc1cr31sib4crlnni96rw")))

(define-public crate-fen-rs-0.1 (crate (name "fen-rs") (vers "0.1.5") (hash "0z6626vwpq7cd4rpa44dmbn6mpzypy7s0inizbxfa7mp6p9sbp9q")))

