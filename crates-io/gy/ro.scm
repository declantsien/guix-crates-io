(define-module (crates-io gy ro) #:use-module (crates-io))

(define-public crate-gyro-0.0.1 (crate (name "gyro") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "04fxqmf7f5785m59mginwlhb22c7k6bhiql3lnc90hjn9nc324bx")))

