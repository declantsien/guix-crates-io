(define-module (crates-io gy ps) #:use-module (crates-io))

(define-public crate-gypsum-0.1 (crate (name "gypsum") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.19.0") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "uni-app") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uni-gl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p0iwn2ll4ljwmvb5q1dkdk89jqawjnnzzwpn9kjadyinwrk1x2m")))

(define-public crate-gypsy-0.0.0 (crate (name "gypsy") (vers "0.0.0") (hash "13lg4fhibibnmczdbgrngfdysg4j7ab05kv9vdxqw1k8gda6dg5i")))

