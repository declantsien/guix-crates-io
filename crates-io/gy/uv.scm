(define-module (crates-io gy uv) #:use-module (crates-io))

(define-public crate-gyuvl53l0x-0.1 (crate (name "gyuvl53l0x") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fxh1l0xqb1kf1crw0535s880whzyfgrki1q6dlxcc2ail27r772")))

(define-public crate-gyuvl53l0x-0.1 (crate (name "gyuvl53l0x") (vers "0.1.1") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fbn2fcyn69yymjs9452h8lj7fdcq5a7bzbv29ac722dnpqff542")))

(define-public crate-gyuvl53l0x-0.1 (crate (name "gyuvl53l0x") (vers "0.1.2") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0pjl2cs3acwbirbhj14z89czwacv62cjpxwy8lbbqbdmn282jsnv")))

(define-public crate-gyuvl53l0x-0.1 (crate (name "gyuvl53l0x") (vers "0.1.3") (deps (list (crate-dep (name "cast") (req "^0.2.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0hbs9c3k2mwd5cs0p6d8i9wff0f5kazdfy10hq5cvskm9rdyj38h")))

(define-public crate-gyuvl53l0x-0.2 (crate (name "gyuvl53l0x") (vers "0.2.0") (deps (list (crate-dep (name "cast") (req "^0.2.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0lwgnii8wr1mdc520lq1xrkajgm4mcxm5zx2px6g70a8vsr4xjlx")))

(define-public crate-gyuvl53l0x-0.3 (crate (name "gyuvl53l0x") (vers "0.3.0") (deps (list (crate-dep (name "cast") (req "^0.2.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0sqw6g947862jrgcmbkmd7ab2gf73a9zqnaxrn9dpialsrpahwxp")))

