(define-module (crates-io gy #{-2}#) #:use-module (crates-io))

(define-public crate-gy-21-0.2 (crate (name "gy-21") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1457jvdv6b0a7x4zaj5vn5gi958ixivmgwklkprcm0wzdgnc1hra")))

