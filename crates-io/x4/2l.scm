(define-module (crates-io x4 #{2l}#) #:use-module (crates-io))

(define-public crate-x42ltc-0.0.5 (crate (name "x42ltc") (vers "0.0.5") (deps (list (crate-dep (name "x42ltc-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "126jp4i9hgg2kz7gbvbynl4fa08v2dsps9m1rz9fpgmjqwqqr7af")))

(define-public crate-x42ltc-0.0.6 (crate (name "x42ltc") (vers "0.0.6") (deps (list (crate-dep (name "x42ltc-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0jypyw31nh3z71xxl58hdrfxkp10z98dvxlr1zwb2jhplwnhyms1")))

(define-public crate-x42ltc-sys-0.0.5 (crate (name "x42ltc-sys") (vers "0.0.5") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "10zhsdkkm14vnwvpq1fy3z2wyflgmdhs58pr2j39ysmph03larfq")))

(define-public crate-x42ltc-sys-0.0.6 (crate (name "x42ltc-sys") (vers "0.0.6") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "01afhfkcdh5354d2nb87lni7kxnwsw5xr5hpl6i60iycykz5hq2n")))

