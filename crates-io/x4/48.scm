(define-module (crates-io x4 #{48}#) #:use-module (crates-io))

(define-public crate-x448-0.1 (crate (name "x448") (vers "0.1.0") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "1vis6y1dmw2nv44kldqhsw08cndpm05kjsii9lyvmsmkw5gfhn1r")))

(define-public crate-x448-0.1 (crate (name "x448") (vers "0.1.1") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "0xr90cx2kwxwxk857nm2anw0mmdlhrj683qyssyrfnb987ikh0qp")))

(define-public crate-x448-0.2 (crate (name "x448") (vers "0.2.0") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "1cknphrnvbk292v6cqibszigyqswpla7d9papvm6a36fjjdhmjqi")))

(define-public crate-x448-0.3 (crate (name "x448") (vers "0.3.0") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "07fw61mfp1f4pr42vhgn7xa1w016gaaxj8gfbbg52zmxj0wxdm07")))

(define-public crate-x448-0.4 (crate (name "x448") (vers "0.4.0") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "08bq20976pc9wg4ffnkq6ivk1g9f96mf32shb9sjzsn3dgy3kh3l")))

(define-public crate-x448-0.4 (crate (name "x448") (vers "0.4.1") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "0mp4x7n6l62z2nl5gs3kh2qvdbh5aya7851whn82w5l9rn9944w0")))

(define-public crate-x448-0.5 (crate (name "x448") (vers "0.5.0") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "1fgpvzkq8sbqgjbmp8fhbyhirmlldzdap8jywviqa3pflghm30av")))

(define-public crate-x448-0.6 (crate (name "x448") (vers "0.6.0") (deps (list (crate-dep (name "ed448-goldilocks") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (kind 0)))) (hash "1fzy7xhcldasvnjajp0jc1vwwbfmgh3zgb5wkl40g7p2zba0gkf4")))

