(define-module (crates-io ht ab) #:use-module (crates-io))

(define-public crate-htable-0.1 (crate (name "htable") (vers "0.1.0") (hash "0kcipy394fm7z7nbcxkwxw5lnikgmd45rjc5ycf538km5hjgg61r")))

(define-public crate-htable-0.2 (crate (name "htable") (vers "0.2.0") (hash "0mzw2lmfa4cmfd0ci8d1bvi06sv4mi97gm949pvsrbdqv77kgpb2")))

(define-public crate-htable-0.2 (crate (name "htable") (vers "0.2.1") (hash "1b81sii23vxza3h7x6m0a6mmx700zhvzsh6vc9pdlm3x1pskl1lx")))

(define-public crate-htable-0.3 (crate (name "htable") (vers "0.3.1") (hash "018hiqzmdkqpa4pdhrh0ri5dh2ri5y7cq1wwhg85alkfj3s9xxj6")))

(define-public crate-htable-0.3 (crate (name "htable") (vers "0.3.2") (hash "0riwzrqj6f5xqzqrhmyll6rlhnsadvnycmyq91wmmwisviv4yw74")))

(define-public crate-htable2csv-0.1 (crate (name "htable2csv") (vers "0.1.0") (deps (list (crate-dep (name "isahc") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nipper") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0azn21kiijp2877n7fipnqirijm82dnyj20hyvsrwfcqla0vrhk0")))

(define-public crate-htable2csv-0.1 (crate (name "htable2csv") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nipper") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1mzbj17im0jpcfp3dkzl73chx3ksswv7hqlyjjks09wcqfw1ai1k")))

(define-public crate-htable2csv-0.1 (crate (name "htable2csv") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nipper") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "08n4w15jmbs44r18wwc23bk1fyrd3554s8r3z5qyldjdx8badm21")))

