(define-module (crates-io ht sc) #:use-module (crates-io))

(define-public crate-htscodecs-sys-1 (crate (name "htscodecs-sys") (vers "1.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.73") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "0j4hwlpfdrivyrgl8wc6j5yyihlbn2pm9isspwjj1z519b94v67b") (links "htscodecs")))

