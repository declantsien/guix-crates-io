(define-module (crates-io ht tt) #:use-module (crates-io))

(define-public crate-httt-0.1 (crate (name "httt") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sl80jhyrhsa5smlq95wq9c12a7wqw0d9hjqd03x1n4h48g7s6fr")))

