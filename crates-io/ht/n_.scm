(define-module (crates-io ht n_) #:use-module (crates-io))

(define-public crate-htn_planner-0.1 (crate (name "htn_planner") (vers "0.1.0") (deps (list (crate-dep (name "filename") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1lrw3napxsdcnygz1dg2cw59505iizyv68akf398dry3d1szh26p")))

