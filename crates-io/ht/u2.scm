(define-module (crates-io ht u2) #:use-module (crates-io))

(define-public crate-HTU21D-0.1 (crate (name "HTU21D") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qxfxj3z2xxj59kqhkdi92fjqihwh2nr9a08nqa5y852vrw7gvrb")))

(define-public crate-HTU21D-0.1 (crate (name "HTU21D") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0bgyfbcvcwbdqbybi94kxvk9gn2aspakkj3mncssyqgiyn5vwbwl")))

(define-public crate-htu21df-sensor-0.1 (crate (name "htu21df-sensor") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vygl8i4jp11hvvrynlsb1fxrc2km76dpx4p8dwiyjsi15xw7wfc")))

(define-public crate-htu21df-sensor-0.1 (crate (name "htu21df-sensor") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w6c4z5najlw93gcvd5rydj252hydd6dwq5chi0m1f2rlp8bq8kv")))

(define-public crate-htu21df-sensor-0.1 (crate (name "htu21df-sensor") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ar7mxhkv7g18jpzph54ga0wbmld254l6wbx0c7x5f3vfgkphicl")))

(define-public crate-htu21df-sensor-0.1 (crate (name "htu21df-sensor") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zw195rv5barsbwalvzpz49bgldkf48mwcmykk26ylnh3cnrijsw")))

(define-public crate-htu21df-sensor-0.1 (crate (name "htu21df-sensor") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v9mcbm6b0fj9aw22fwcyrh7a5zrwspzvmnfgwzq22phxld37yll") (features (quote (("std") ("default"))))))

(define-public crate-htu2xd-0.1 (crate (name "htu2xd") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0a56qipjwwph44wqd3harhglgx2qwid61y48yf3rb9wkkac87x4f")))

