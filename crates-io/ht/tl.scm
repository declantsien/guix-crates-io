(define-module (crates-io ht tl) #:use-module (crates-io))

(define-public crate-httlib-0.1 (crate (name "httlib") (vers "0.1.0") (hash "02gaf52n1af44dzs1nvzcfs9p05zav033z67hn8810s9nfblhr3s")))

(define-public crate-httlib-h1-0.0.0 (crate (name "httlib-h1") (vers "0.0.0") (hash "181rhvgcb8j396bbjg1krqpsss3qfj8mh5q4cvkb3i52cf9pgxfk")))

(define-public crate-httlib-h2-0.0.0 (crate (name "httlib-h2") (vers "0.0.0") (hash "0f4h5qqz5w0zs8ha9x3b86vhcx9nrxmr8v8rbilyjsaxckwmb4qb")))

(define-public crate-httlib-h3-0.0.0 (crate (name "httlib-h3") (vers "0.0.0") (hash "1g1hn6ckarjs8axpfdvwc3ncmk34g4bw6wck8rirfm9g5bjqkp7c")))

(define-public crate-httlib-hpack-0.0.0 (crate (name "httlib-hpack") (vers "0.0.0") (hash "18c58aj6gqk955qhw25pjf1wc3q9wgmjm7hxhrym3hjfr7rpncxm")))

(define-public crate-httlib-hpack-0.0.1 (crate (name "httlib-hpack") (vers "0.0.1") (hash "1vzr1nkp1m67iljnzinpldw5znxkhcm8cybvxlhbz18bsiyd0y9c")))

(define-public crate-httlib-hpack-0.1 (crate (name "httlib-hpack") (vers "0.1.0") (deps (list (crate-dep (name "httlib-huffman") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0s837vyf01kmglmmb5394w7xj4914vrb0qbv8jbiy8mzcv1cdv9z")))

(define-public crate-httlib-hpack-0.1 (crate (name "httlib-hpack") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "httlib-huffman") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0khj9psa9bi67pq31i17vy6ibgnz7k8wm2fln7sbp3chq79y5gvh")))

(define-public crate-httlib-hpack-0.1 (crate (name "httlib-hpack") (vers "0.1.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "httlib-huffman") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0cmg9ifhh4z89bg05366khpa6x5nzkvr72yzsbwcdfzs05wgznk4")))

(define-public crate-httlib-hpack-0.1 (crate (name "httlib-hpack") (vers "0.1.3") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "httlib-huffman") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "1qxaabq6y2jp5rb8lcv0lmvr7pi1512z34552kwnyz2nx3jn1ks0")))

(define-public crate-httlib-huffman-0.1 (crate (name "httlib-huffman") (vers "0.1.0") (hash "078d9nc9dgs1b1z9dviaxlybhya30cb8ik4h1k0mhsbnm9iqd34p") (features (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1 (crate (name "httlib-huffman") (vers "0.1.1") (hash "13f58q0mwjk9c9yfiri1cfnnfvhxflngqchwm6374wkjx5acx83m") (features (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1 (crate (name "httlib-huffman") (vers "0.1.2") (hash "1h7qciq0kf49p36zvn2x5mga23ranr4i1zpkq8xms1hvs3wfjbi9") (features (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1 (crate (name "httlib-huffman") (vers "0.1.3") (hash "057zk22ysf80l124f23lnjssqp8zffg0idaa9m0rdh2624l3k43x") (features (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.1 (crate (name "httlib-huffman") (vers "0.1.4") (hash "0nyh8z3yrgd8yk6a52krkbag083ia81rgqvxykn4lcpkjl64l74f") (features (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.2 (crate (name "httlib-huffman") (vers "0.2.0") (hash "1h48hkp4pix16c23h8fv47hlp54cnkcvl2mhjvi1nlns9af7dbjk") (features (quote (("parse") ("flatten") ("encode") ("default" "encode" "decode4" "flatten" "parse") ("decode5") ("decode4") ("decode3") ("decode2") ("decode1"))))))

(define-public crate-httlib-huffman-0.2 (crate (name "httlib-huffman") (vers "0.2.1") (hash "1y4l94782mnjhymj42h8hgd6bsc3j7sysgcf4ywbj8i9y9zj6vkx")))

(define-public crate-httlib-huffman-0.2 (crate (name "httlib-huffman") (vers "0.2.2") (hash "0x2jil2h1rixpkx5clpfl1a9vk6y5wizg446sjz9i0bbxvbhich3")))

(define-public crate-httlib-huffman-0.2 (crate (name "httlib-huffman") (vers "0.2.3") (hash "1gd8navg81cqrbjf2qc9bbjphhp43igsp7yajgrmnlsyfalhqngx")))

(define-public crate-httlib-huffman-0.3 (crate (name "httlib-huffman") (vers "0.3.0") (hash "00rgpkxc03rfsnb3dlqllw5rhwg523sxjnsfxfl82hp2j455w1ds")))

(define-public crate-httlib-huffman-0.3 (crate (name "httlib-huffman") (vers "0.3.1") (hash "1zh19i4fv41rdy72hnxxrnk0yahmr112wr318lf8vxhgjynar5dm")))

(define-public crate-httlib-huffman-0.3 (crate (name "httlib-huffman") (vers "0.3.2") (hash "1yi2bgmxfm75zaigxqwns7wda570chwypk892cmcygql787pz3hg")))

(define-public crate-httlib-huffman-0.3 (crate (name "httlib-huffman") (vers "0.3.3") (hash "006jrmflr7vyydrwxwr1q4pkilg8cc44in1fvn21p8mrilv828gq")))

(define-public crate-httlib-huffman-0.3 (crate (name "httlib-huffman") (vers "0.3.4") (hash "0xxqvqkxq7mxdb3si83szg0nfybfr3jk9mc0mg1jcmcc836cp7qs")))

(define-public crate-httlib-protos-0.1 (crate (name "httlib-protos") (vers "0.1.0") (hash "07xsb8apbqz0lwrsbq81bkj8jbjqqdvxnh9m5d2mql8jhg2w9rmf")))

(define-public crate-httlib-protos-0.1 (crate (name "httlib-protos") (vers "0.1.1") (hash "1dza90pzd90dn81llmmbhak6fppgar687nz85pk29qmr7skij6hj")))

(define-public crate-httlib-protos-0.1 (crate (name "httlib-protos") (vers "0.1.2") (hash "0bxxcyll2qb424gawv9nz7ipdy7ddgjj86wpy58yqr9074ld3c7k")))

(define-public crate-httlib-protos-0.1 (crate (name "httlib-protos") (vers "0.1.4") (hash "15k5xnpvrl6hax08f93xd7wfzcvvpw7dvijyp4xzl232k8j0gwyc")))

(define-public crate-httlib-protos-0.2 (crate (name "httlib-protos") (vers "0.2.0") (hash "0kfdyvli17w6srqfcihjqsybmr4gsydza4vyymmvagc6yx2x7k6d")))

(define-public crate-httlib-protos-0.2 (crate (name "httlib-protos") (vers "0.2.1") (hash "183i3jkqmqkzcgmswzjywb8dyxg48xwadnjwllg72lh961qx8qw2")))

(define-public crate-httlib-protos-0.3 (crate (name "httlib-protos") (vers "0.3.0") (hash "0s03b85ypgbg5k582x6bip27c8x8bw4rw0aimjy19jdrkb2cym14")))

(define-public crate-httlib-protos-0.3 (crate (name "httlib-protos") (vers "0.3.1") (hash "0i9hg78pjs4l928jgd0swhv7d24nwd6gvgbshmzndr73vrq25yv8")))

(define-public crate-httlib-protos-0.3 (crate (name "httlib-protos") (vers "0.3.2") (hash "0f6asin26ga3z1byxmyqb5vsgysbd4nzslrrpzmxrgbqfj9wsznl")))

(define-public crate-httlib-quic-0.0.0 (crate (name "httlib-quic") (vers "0.0.0") (hash "0akm2qsx64prv1xl0slasp17y2wqy6k8k9kymcvzjs1z1jj62yhv")))

