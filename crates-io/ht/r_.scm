(define-module (crates-io ht r_) #:use-module (crates-io))

(define-public crate-htr_cli-0.5 (crate (name "htr_cli") (vers "0.5.24") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "htr") (req "^0.5.24") (kind 0)))) (hash "1pnvyf3g851b9lhdik72cvz2bh432h4jl4pbpffplp97j16hw0gl")))

(define-public crate-htr_cli-0.5 (crate (name "htr_cli") (vers "0.5.25") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "htr") (req "^0.5.25") (kind 0)))) (hash "0fjpsc2r1vv2zrf5mh1njz8d6y3pv8f4wpwhvgv0p8ah0vag7ilw")))

(define-public crate-htr_cli-0.5 (crate (name "htr_cli") (vers "0.5.26") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "htr") (req "^0.5.26") (kind 0)))) (hash "00max3fppg204063mw42rj56m38w8pkzh29snqhdima3r6jkfkzr")))

(define-public crate-htr_cli-0.5 (crate (name "htr_cli") (vers "0.5.27") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "htr") (req "^0.5.26") (kind 0)))) (hash "1kawgggmv11cir3lh6iz30vfx20q3vsp851xm9m4a4kkxf8zi2i3")))

