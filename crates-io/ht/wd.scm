(define-module (crates-io ht wd) #:use-module (crates-io))

(define-public crate-htwdresden-0.1 (crate (name "htwdresden") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.3") (default-features #t) (kind 0)))) (hash "0yy744y19g1q7nfrpdhaglcvny3s306hvcq4hkbpy47qc7xc5mqs")))

(define-public crate-htwdresden-0.1 (crate (name "htwdresden") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "1y44sm0jbj04xixkf20smxzp11sfp9hlxzfvpp52mx36sydv9s2v")))

(define-public crate-htwdresden-0.2 (crate (name "htwdresden") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "04nrljd49gdlxmvbkzc662gdm7v2g2xkyhf3yya79w17ljclhzfj")))

(define-public crate-htwdresden-0.3 (crate (name "htwdresden") (vers "0.3.1") (deps (list (crate-dep (name "reqwest") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "1sn24ds78zcy0zq3wgzy69rw0gzysqsyj94n1c2h4da8i8cr22ra")))

(define-public crate-htwdresden-0.3 (crate (name "htwdresden") (vers "0.3.2") (deps (list (crate-dep (name "reqwest") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qpjhwxgql70gpxmqd4kfs2an51fy9spvi6w33ibfm7c7g4cra0k")))

(define-public crate-htwdresden-0.3 (crate (name "htwdresden") (vers "0.3.3") (deps (list (crate-dep (name "reqwest") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "13k59dvx65q6r3w292pj76z9wkrzcxni92gs6mqay19r4s86816l")))

(define-public crate-htwdresden-0.3 (crate (name "htwdresden") (vers "0.3.4") (deps (list (crate-dep (name "reqwest") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "1zj5jwa41mkanmvviqxihiw7ymgs6mnckgd8xc471cm70fc0bil9")))

