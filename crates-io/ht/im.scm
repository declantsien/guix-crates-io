(define-module (crates-io ht im) #:use-module (crates-io))

(define-public crate-htime-0.0.1 (crate (name "htime") (vers "0.0.1") (hash "1h43zcmwnyqhldkrga9y51vgl9wkb9cylap6jm2mlf50pa6fqhdc")))

(define-public crate-htime-0.0.2 (crate (name "htime") (vers "0.0.2") (hash "1089y3fggis341c052158jz7w8mkk3dm2hl45ig2wb4n6n19cfd2")))

(define-public crate-htime-0.0.3 (crate (name "htime") (vers "0.0.3") (hash "1hf8jml7gwj3avpj79jlgbywq7fqpg4dlrxmw1qcpsz6gld7s8jd")))

