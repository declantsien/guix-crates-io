(define-module (crates-io ht re) #:use-module (crates-io))

(define-public crate-htree-0.1 (crate (name "htree") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "00fgcahrdgag0cpqf835ainihgc2yvhbjs5r08h9g40gnhivcl0n")))

(define-public crate-htree-0.1 (crate (name "htree") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.4") (default-features #t) (kind 2)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "126dx7df72qz5wn8iqbl2ms2ppsmx6mmaavlim0lcm14gb3k7hhy")))

(define-public crate-htree-0.1 (crate (name "htree") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.4") (default-features #t) (kind 2)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0m24k20kyxfklfy4cisf798pz43nhf8wzlb5dfz4jgpz6zgyicis")))

(define-public crate-htree-0.1 (crate (name "htree") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.24.4") (default-features #t) (kind 2)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0l4ggrr24kjkz79hrwzvjh0z74b14fd6p1annyaasdjiqifv4zml")))

