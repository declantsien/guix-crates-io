(define-module (crates-io ht qx) #:use-module (crates-io))

(define-public crate-htqx_minigrep-0.1 (crate (name "htqx_minigrep") (vers "0.1.1") (hash "0xjkxylr51xybxmvlxg1w5yndnlcgzvy52jh02988z06qj8hxh72")))

(define-public crate-htqx_minigrep-1 (crate (name "htqx_minigrep") (vers "1.0.0") (hash "0gxj2q69gvqp9lzjldhq0ppl476pjcdgk8snh6awixaxvajcfb7f")))

