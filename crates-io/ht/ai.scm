(define-module (crates-io ht ai) #:use-module (crates-io))

(define-public crate-htai_elitist-1 (crate (name "htai_elitist") (vers "1.1.0") (deps (list (crate-dep (name "actix-rt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "1r3yrz9y8lf93hmviavrydb8kbq4i151k36ymw9zq3mhgxfsv4p4")))

