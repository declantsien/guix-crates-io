(define-module (crates-io nj id) #:use-module (crates-io))

(define-public crate-njid-0.1 (crate (name "njid") (vers "0.1.0") (deps (list (crate-dep (name "prettytable") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06828k5js7ska78rgqh376aw1pin4m32zrgqrgmr5yfxglxanikg") (yanked #t)))

