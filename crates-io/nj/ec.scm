(define-module (crates-io nj ec) #:use-module (crates-io))

(define-public crate-nject-0.1 (crate (name "nject") (vers "0.1.0") (deps (list (crate-dep (name "nject-macro") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1m0z1ayiwa4jg0wsj6gh3bhsvg96vywzx4psgqs0ln5nkhj7ax1n") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.2 (crate (name "nject") (vers "0.2.0") (deps (list (crate-dep (name "nject-macro") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0psr0zarw1v77fzwy8fp6lqd1z9g3hvwnzgyck69cdj575v6z4v8") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.2 (crate (name "nject") (vers "0.2.1") (deps (list (crate-dep (name "nject-macro") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1nnm2vbhnd85hrcaw93lzj6dl2k37y1gdk8pbjqkvnkplhvfpbqs") (features (quote (("default" "macro")))) (yanked #t) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.2 (crate (name "nject") (vers "0.2.2") (deps (list (crate-dep (name "nject-macro") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "06m6j6a5zh15jaxzv0sgxcyqp611ghhi7l4zmn3xjps0g1g2fnkw") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.2 (crate (name "nject") (vers "0.2.3") (deps (list (crate-dep (name "nject-macro") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "19p9hhny1cdm84shriqdn29fawbq488s64zpv585xyll9fg927b1") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.3 (crate (name "nject") (vers "0.3.0") (deps (list (crate-dep (name "nject-macro") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1irjk10qq8gr5hwnm2233v2n4lj2l07fzlmypd4g55fg6z5y3xjr") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.3 (crate (name "nject") (vers "0.3.1") (deps (list (crate-dep (name "nject-macro") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)))) (hash "1nfhackv54ap49yz183jbs3vxcs5ypgz3gvvhkhxkg5c43fd6hwx") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.3 (crate (name "nject") (vers "0.3.2") (deps (list (crate-dep (name "nject-macro") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)))) (hash "18bcj1dwnqx4jzhh47wmp11vbij26bl9s8v9qinwdb4xac9v1cd0") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.4 (crate (name "nject") (vers "0.4.0") (deps (list (crate-dep (name "nject-macro") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1nkivqwg5cyrs469bwvdc2lbns1njd6wam7vxps8r2a884acgp1v") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.4 (crate (name "nject") (vers "0.4.1") (deps (list (crate-dep (name "nject-macro") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1bxkw16pm6jhkivadakfany066njn1y6p5kg08gw7qih7rzzzvv0") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.4 (crate (name "nject") (vers "0.4.2") (deps (list (crate-dep (name "nject-macro") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1zpc2k8f0kbwfg3myd11bmvyvzxddvy1kdfrbwhgqp2hc7fms854") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-0.4 (crate (name "nject") (vers "0.4.3") (deps (list (crate-dep (name "nject-macro") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)))) (hash "0941z8kfj5zg3hk04d6qsna44yrhnz888rv926cqpr0xhwaiiv3x") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:nject-macro")))) (rust-version "1.60")))

(define-public crate-nject-macro-0.1 (crate (name "nject-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gb28dm8zy3jkx2kc9gcwndi787hydk7b2wxahgwlpalf3arrp1q") (rust-version "1.60")))

(define-public crate-nject-macro-0.2 (crate (name "nject-macro") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v2b95pykl179xs2pqzgfk87nk4bz1d9pzm88liw6as5cyq736hf") (rust-version "1.60")))

(define-public crate-nject-macro-0.2 (crate (name "nject-macro") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rp9nqzg0rshrfmng4z35laki2ad92zlk6yig2hs1xwzcnlyxiff") (yanked #t) (rust-version "1.60")))

(define-public crate-nject-macro-0.2 (crate (name "nject-macro") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0707gj2hxrgym0d1f6708y8aa62fap7qp9wmzxkz4cpdqr6fgf4x") (rust-version "1.60")))

(define-public crate-nject-macro-0.2 (crate (name "nject-macro") (vers "0.2.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hg2qx7nzpyv8rq660wsmyj9w1zi6r0sgxhxpqz7lwcbzvvwfyw2") (rust-version "1.60")))

(define-public crate-nject-macro-0.3 (crate (name "nject-macro") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "044vxq82vv3vav80400qmib1yrpghjslwdwblvwsbp4nyy1s6yjv") (rust-version "1.60")))

(define-public crate-nject-macro-0.3 (crate (name "nject-macro") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kzy3739aagvvzkvsnazcc4x5w1p8rfpmwc4jaqyj5p8kdh9p8zj") (rust-version "1.60")))

(define-public crate-nject-macro-0.3 (crate (name "nject-macro") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ipgdw98mizrpwr961rd910963dh0pib17j8hl04rdfyz27hn6mk") (rust-version "1.60")))

(define-public crate-nject-macro-0.4 (crate (name "nject-macro") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06x6kp794pb5n661pv8x3f31aja2il8zir6ccgqw0q2dcj62pbpr") (rust-version "1.60")))

(define-public crate-nject-macro-0.4 (crate (name "nject-macro") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fmy47naqshpmlh1jmakc9pncdvwlh4cwx870bm438a1lqrawa09") (rust-version "1.60")))

(define-public crate-nject-macro-0.4 (crate (name "nject-macro") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i2l2hmavyfl54l66qsi746zcagi3picm4n089lz2n84qma4dyhs") (rust-version "1.60")))

(define-public crate-nject-macro-0.4 (crate (name "nject-macro") (vers "0.4.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gblmg9hcdp5361g4zg7m9xmvnz683zcz887ll932h6dvqb0gn8w") (rust-version "1.60")))

