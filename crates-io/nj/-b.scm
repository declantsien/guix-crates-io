(define-module (crates-io nj -b) #:use-module (crates-io))

(define-public crate-nj-build-0.1 (crate (name "nj-build") (vers "0.1.0") (hash "0swqim1w63841yl0025cy44iflnrbm0q8qjb6bpdh3wjkh39c6g7")))

(define-public crate-nj-build-0.2 (crate (name "nj-build") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("native-tls" "blocking"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1917lf1mgril49ak8syc7b4s1wqkm9dqddn3g42n3kn040sbh5kg")))

(define-public crate-nj-build-0.2 (crate (name "nj-build") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("native-tls" "blocking"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "02mk2gdw5k4350fdv23gfivxigrx0mx1nz67nb14xvnxjaqvn3rv")))

(define-public crate-nj-build-0.2 (crate (name "nj-build") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("native-tls" "blocking"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1026h4g98yfl3h82zzk6hg169lrr8kb34729w1xarvadgdm1rshl")))

(define-public crate-nj-build-0.2 (crate (name "nj-build") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "http_req") (req "^0.7.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1wabfff73pajr21cnxh9xk49rn0g0j2v2bhv58rrl54w8ykppldp")))

(define-public crate-nj-build-0.3 (crate (name "nj-build") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "http_req") (req "^0.9.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1jp9nnj4dj97ahscm9ndrj95c5x1gsnq8sx8j32j22ia2r0g31zm")))

