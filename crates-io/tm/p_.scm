(define-module (crates-io tm p_) #:use-module (crates-io))

(define-public crate-tmp_borsh_dep_test-0.1 (crate (name "tmp_borsh_dep_test") (vers "0.1.0") (deps (list (crate-dep (name "borsh") (req ">=1.0.0, <1.1.0") (features (quote ("unstable__schema" "derive"))) (default-features #t) (kind 0)))) (hash "1m4psdy4da6r7jdiaas1ci9af047881cqqk0qbqgx8xkdpk8sgzb") (yanked #t)))

(define-public crate-tmp_env-0.1 (crate (name "tmp_env") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "08ikliqw6fifx9kak3686i2cjzxp37x3mcjbpmw354am7f01p73x")))

(define-public crate-tmp_env-0.1 (crate (name "tmp_env") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0hv8fsv4aygmg3772i9npzj56v1dmnl71sm8d854yg4clbjvjvm5")))

(define-public crate-tmp_mio-0.5 (crate (name "tmp_mio") (vers "0.5.2") (deps (list (crate-dep (name "bytes") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "miow") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2.19") (kind 0)) (crate-dep (name "nix") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.33") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "06bc7wp4g3nnglq1mdja4sjxlabx8fpb83cq4hzg25ab0av7dkli")))

(define-public crate-tmp_vec-0.0.1 (crate (name "tmp_vec") (vers "0.0.1") (hash "061ama7i7lhhpp9qfagq7ffi9p70vx77zbbx40wi6nh1i6hqzrcf")))

