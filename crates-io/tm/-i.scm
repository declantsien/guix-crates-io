(define-module (crates-io tm #{-i}#) #:use-module (crates-io))

(define-public crate-tm-interpreter-0.1 (crate (name "tm-interpreter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_scan") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "01n8bxmqvr410ll9h65m4nlhx3ac8x29rxl8491186w9payr9v6d")))

