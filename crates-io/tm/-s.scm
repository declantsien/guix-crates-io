(define-module (crates-io tm -s) #:use-module (crates-io))

(define-public crate-tm-sys-2020 (crate (name "tm-sys") (vers "2020.11.0") (hash "0ljjcn2ncv2i69cs0r2h4vzkrbyarrwyf3cz5krn8mqcl89p43a8")))

(define-public crate-tm-sys-2020 (crate (name "tm-sys") (vers "2020.11.1") (hash "0cc3rpb2m98qwg6bzfvdp2c3djcw6lrz39dqvfjf0rfdy9l91kwh")))

(define-public crate-tm-sys-2020 (crate (name "tm-sys") (vers "2020.11.2") (hash "1n2nc7h5h6xy84j2ik24rcc4hqqbzlrppxv0vad10wdbsqnv52pd")))

(define-public crate-tm-sys-2020 (crate (name "tm-sys") (vers "2020.11.3") (hash "0szpdyf80dzkics9ia91fbngk9v11nn1s1hxdf7v5zldn5lii546")))

(define-public crate-tm-sys-2020 (crate (name "tm-sys") (vers "2020.11.4") (hash "0mr6ikr1sr7bbj6prcyavvcr0i6mg9bfj3qr3lrh39kxs2fin9f1")))

(define-public crate-tm-sys-2020 (crate (name "tm-sys") (vers "2020.11.5") (hash "04asrsq3h8pisvd1nlbii4ivpgw4drdsc5nk8yjf7gm5z0k2yab6")))

(define-public crate-tm-sys-2020 (crate (name "tm-sys") (vers "2020.11.6") (hash "00cb5lx8mwm6gxf94i4kchbnmx42l7fffsb0rywqlkkb0vinjw56")))

