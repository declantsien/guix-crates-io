(define-module (crates-io tm le) #:use-module (crates-io))

(define-public crate-tmledkey-hal-drv-0.0.1 (crate (name "tmledkey-hal-drv") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "16v5islzfklq38psjaf4n5zk3drm38n09vbmjgynx4hq6jswp2hi") (yanked #t)))

(define-public crate-tmledkey-hal-drv-0.0.2 (crate (name "tmledkey-hal-drv") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0p6c95hhwh58ihsm9divjrab2yg3yps06w43gg0xilscn6b99ciz")))

(define-public crate-tmledkey-hal-drv-0.1 (crate (name "tmledkey-hal-drv") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1xkzbp4pzayclkjfbaf2ccik59bcdgxymnph2s28fvk4wlr6i9a2") (features (quote (("keys") ("galloc") ("fx" "galloc") ("demo" "clkdio" "clkdiostb" "keys" "fx") ("default" "clkdio" "clkdiostb") ("clkdiostb") ("clkdio"))))))

(define-public crate-tmledkey-hal-drv-0.1 (crate (name "tmledkey-hal-drv") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0d6529zml6xkp1fplxm0n5a798dqsz9k7ygvnab53p3i7hlgpcgg") (features (quote (("keys") ("galloc") ("fx" "galloc") ("demo" "clkdio" "clkdiostb" "keys" "fx") ("default" "clkdio" "clkdiostb") ("clkdiostb") ("clkdio"))))))

