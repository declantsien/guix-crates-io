(define-module (crates-io tm pf) #:use-module (crates-io))

(define-public crate-tmpfile-0.0.1 (crate (name "tmpfile") (vers "0.0.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 2)) (crate-dep (name "swctx") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "1gkfx90748h9zff4zwgv4mbgp1c8icr5111wa52dqgd99gszy025") (v 2) (features2 (quote (("defer-persist" "dep:swctx")))) (rust-version "1.56")))

(define-public crate-tmpfile-0.0.2 (crate (name "tmpfile") (vers "0.0.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 2)) (crate-dep (name "swctx") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "1zbs3lgwhg5f1p3zhw5ji05aklafnv567vgamsafrz0211k8f1g1") (v 2) (features2 (quote (("defer-persist" "dep:swctx")))) (rust-version "1.56")))

