(define-module (crates-io tm s9) #:use-module (crates-io))

(define-public crate-tms9918a_emu-0.2 (crate (name "tms9918a_emu") (vers "0.2.0") (deps (list (crate-dep (name "minifb") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "042rizfdac0jn2arrjqmymwzgykwfqpfas00a116z78hszkhk4kv")))

(define-public crate-tms9918a_emu-0.2 (crate (name "tms9918a_emu") (vers "0.2.1") (deps (list (crate-dep (name "minifb") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1xmgxf681d4fs2lymjdzmjkwfvhlji0s1wjgkkbzw8ab37aai5rz")))

(define-public crate-tms9918a_emu-0.3 (crate (name "tms9918a_emu") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "180br0pnshj65ybmlsqjds8pahini1q5w1bcsdxnm30l7hzz5zni")))

