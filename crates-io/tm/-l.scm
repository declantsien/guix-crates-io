(define-module (crates-io tm -l) #:use-module (crates-io))

(define-public crate-tm-language-0.0.0 (crate (name "tm-language") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "json5") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plist") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06yn5a4cdp3chhis4nymkkqmqha4z9g4cnrs1k4nvrap30p9hnvk") (features (quote (("default"))))))

