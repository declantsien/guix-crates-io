(define-module (crates-io tm l_) #:use-module (crates-io))

(define-public crate-tml_parser-1 (crate (name "tml_parser") (vers "1.0.0") (hash "1isvl8p0mx93vr8swvgkk1hl2p430m6206pch6r8n3ha777g4mjw")))

(define-public crate-tml_parser-1 (crate (name "tml_parser") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.112") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "011avcqvw45vxrny01xilzr025lpr0sr498x5ddkhk7wsvckqdh0")))

(define-public crate-tml_parser-1 (crate (name "tml_parser") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.112") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "054mi0yw8aiy6f51q0m461yvkry629mkxv1zrvpqkm65jld7j3ck")))

(define-public crate-tml_parser-1 (crate (name "tml_parser") (vers "1.0.4") (deps (list (crate-dep (name "serde") (req "^1.0.112") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0320c61g4rs9vzah29h765wgs4c55h988apmnql8jrkdglss9p1a")))

(define-public crate-tml_parser-1 (crate (name "tml_parser") (vers "1.0.5") (deps (list (crate-dep (name "serde") (req "^1.0.112") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0w4xqs785nni8qdwlana74lmizbcxvaqlin5byrv3wykcqvfhw18")))

(define-public crate-tml_parser-1 (crate (name "tml_parser") (vers "1.0.6") (deps (list (crate-dep (name "serde") (req "^1.0.112") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "01m78wb8g248p1x4yyvsnqv1wiqn811zpff2nvzpjqjlawidlxnv")))

