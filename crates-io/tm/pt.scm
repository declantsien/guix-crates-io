(define-module (crates-io tm pt) #:use-module (crates-io))

(define-public crate-tmptoml-0.1 (crate (name "tmptoml") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jlgd7h68mxc47fkfl9ildw6r81g1w052mcyijj3ksrqxhw77imf")))

(define-public crate-tmptoml-0.1 (crate (name "tmptoml") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1d2hqsgp2qhz3j97ddbgpmisbh9bjv7qd99jgkxi79ry7j490haw")))

(define-public crate-tmptoml-0.1 (crate (name "tmptoml") (vers "0.1.2") (deps (list (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0a1fwq3qz6c2m1zaz9yk9pkq3c1ykjygr3zbz1r9m4132sqj489b")))

(define-public crate-tmptoml-0.1 (crate (name "tmptoml") (vers "0.1.3") (deps (list (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1hf2y9zfjs49mhgqx96l5ww0zygbsf5q2rfbpzja7z65w02pdrng")))

