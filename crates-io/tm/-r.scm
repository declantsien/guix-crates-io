(define-module (crates-io tm -r) #:use-module (crates-io))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.0") (deps (list (crate-dep (name "tm-sys") (req ">=2020.11.0, <2021.0.0") (default-features #t) (kind 0)))) (hash "1db4i2njbhapgishibc84yar01781w5jprx6ia2j8l2lwzn9fsd4")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.1") (deps (list (crate-dep (name "tm-sys") (req ">=2020.11.0, <2021.0.0") (default-features #t) (kind 0)))) (hash "0nw11y9krcr55ggdnag020ykgk5xa10vhza61q95sdgbb360hqjg")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.3") (deps (list (crate-dep (name "anymap") (req ">=0.12.0, <0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req ">=2020.11.0, <2021.0.0") (default-features #t) (kind 0)))) (hash "1g7pfmpy6ivbp9mk3l8jx1ny9m41lvpaf0p7qj0jk8plqgsm8qjg")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.4") (deps (list (crate-dep (name "anymap") (req ">=0.12.0, <0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req ">=2020.11.0, <2021.0.0") (default-features #t) (kind 0)))) (hash "0ig4fs4b3jxd8ds4bqin92x6d4f1wvzclhf07pxyxfx1h438jpyw")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.5") (deps (list (crate-dep (name "anymap") (req ">=0.12.0, <0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req ">=2020.11.0, <2021.0.0") (default-features #t) (kind 0)))) (hash "016gbbjay0r7wxnqjldrwhg7r9hn9fnxx89mkzr5lzqrkxv2wajg")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.6") (deps (list (crate-dep (name "anymap") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req "^2020.11") (default-features #t) (kind 0)))) (hash "1dxd7m1zspqr909k0qcbdkh0naphvkb9i105jym8p97y4pv19qfw")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.7") (deps (list (crate-dep (name "anymap") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req "^2020.11") (default-features #t) (kind 0)))) (hash "1ah4qkk568m8x52qsycal8m51drksxsm2p1jkaw7kzxdlsbs06ai")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.8") (deps (list (crate-dep (name "anymap") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req "^2020.11") (default-features #t) (kind 0)))) (hash "11giafkxmn7lw3s01bbb70krj5kc23wkpj3sgsbgpigj9a8kxn7a")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.10") (deps (list (crate-dep (name "anymap") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req "^2020.11.6") (default-features #t) (kind 0)))) (hash "1wvy3khn0c6wfn5bin23za41xpckd8b3rn4ggxcnwjazy6698hp1")))

(define-public crate-tm-rs-2020 (crate (name "tm-rs") (vers "2020.11.11") (deps (list (crate-dep (name "anymap") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tm-sys") (req "^2020.11.6") (default-features #t) (kind 0)))) (hash "1fic03q2by7xnk390yym0zda18ihx5sbd4mdz7pqjk5bnnjkzz28")))

