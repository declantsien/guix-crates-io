(define-module (crates-io tm rs) #:use-module (crates-io))

(define-public crate-tmrs-0.1 (crate (name "tmrs") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "statistics") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "00j7l2a2jsyl16p9qvpwjpfmvspvs95c27va9wfwcpj4w99kykbj") (yanked #t)))

(define-public crate-tmrs-0.1 (crate (name "tmrs") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "statistics") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0lk4fh0chgjhr476q3khmk8n1gylqa2g0zxk56cpk2hynsx6nk0r") (yanked #t)))

(define-public crate-tmrs-0.1 (crate (name "tmrs") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "statistics") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1bj828qj85dk8a1362zzgqx229s2hs6dfrmsnlx634yk0v0vk489") (yanked #t)))

(define-public crate-tmrs-0.2 (crate (name "tmrs") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "statistics") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1h1apdhg7vaqdxiz6yxz0j75n64lj1zq9pnhbxm8hdka1dqjmpra") (yanked #t)))

(define-public crate-tmrs-0.3 (crate (name "tmrs") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "statistics") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1jsrfwdp5z2fbhsc91wplkihxnj4r7bcs5hla13v47mfffgd65kn")))

