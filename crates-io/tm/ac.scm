(define-module (crates-io tm ac) #:use-module (crates-io))

(define-public crate-tmac-0.1 (crate (name "tmac") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1a13jvgif2jxr3fcfch99lf3anp459v1ii2hfcbxxf5mv6c3w5zc")))

(define-public crate-tmac-0.1 (crate (name "tmac") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1l5l7pdi4m3iqqgx2sn7wscr1dyn41nny3rm3ylbdr3r93jq9hn9")))

(define-public crate-tmac-0.1 (crate (name "tmac") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0cjxn4xwpi4zkhq8z0frhc4zn0ay8x8spir88cy1rh9qibd547aa")))

(define-public crate-tmac-0.1 (crate (name "tmac") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0mlvldhykazqipn6p5r9ys9q71xnlqqxmzh6wvvrhsj4p0wrvw59")))

