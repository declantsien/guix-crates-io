(define-module (crates-io tm fl) #:use-module (crates-io))

(define-public crate-tmflib-derive-0.1 (crate (name "tmflib-derive") (vers "0.1.13") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (default-features #t) (kind 0)))) (hash "0zidhlhfljshd775y5hzdx7wwfx9gnydqmsch2v8q81l58cvwb0n") (yanked #t)))

(define-public crate-tmflib-derive-0.1 (crate (name "tmflib-derive") (vers "0.1.14") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (default-features #t) (kind 0)))) (hash "173j8rm7cn792n1kkb2243s6vc1ggwgmkn0zqifrsbgxnb3zmc4q")))

(define-public crate-tmflib-derive-0.1 (crate (name "tmflib-derive") (vers "0.1.15") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.63") (default-features #t) (kind 0)))) (hash "01wi6g5sfq3l3lkp9ss84pljmm6ms9f9yh2b8n91293rkd6c2k9h")))

(define-public crate-tmflib-derive-0.1 (crate (name "tmflib-derive") (vers "0.1.16") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.63") (default-features #t) (kind 0)))) (hash "133j75lk507gsib5171lidy3adw15yg7xgy29w1bsbkfaq78hhp6")))

