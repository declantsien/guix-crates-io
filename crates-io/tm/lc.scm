(define-module (crates-io tm lc) #:use-module (crates-io))

(define-public crate-tmlcprt-0.1 (crate (name "tmlcprt") (vers "0.1.0-pre.1") (deps (list (crate-dep (name "tendermint-light-client-verifier") (req "^0.28.0-pre.1") (default-features #t) (kind 0)))) (hash "1w1xvmrph0yb2ddzywrizd14d629gsigh8azfl1yk54hpza711fv")))

(define-public crate-tmlcprt-0.1 (crate (name "tmlcprt") (vers "0.1.0") (deps (list (crate-dep (name "tendermint-light-client-verifier") (req "^0.28.0-pre.1") (default-features #t) (kind 0)))) (hash "1w6d4s1c1a4zrgyfsxnf9d8l1c663xxaw485qyn78ydffynisvqk")))

