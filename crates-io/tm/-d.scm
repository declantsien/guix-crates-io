(define-module (crates-io tm -d) #:use-module (crates-io))

(define-public crate-tm-derive-2020 (crate (name "tm-derive") (vers "2020.11.0") (hash "05hbmpbl5xxykx6vjbampr9bjcwlizbbns25jrjjmcjmwlcxl41n")))

(define-public crate-tm-derive-2020 (crate (name "tm-derive") (vers "2020.11.1") (deps (list (crate-dep (name "Inflector") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0vwwpsgskfy097gxi6mab9ll17kk54qn3xj5w8hm32dvdvfpqg03")))

