(define-module (crates-io tm oe) #:use-module (crates-io))

(define-public crate-tmoe-0.0.1 (crate (name "tmoe") (vers "0.0.1") (deps (list (crate-dep (name "cursive") (req "^0.16.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0g1zrap1aag0s7lpb2lbpgb2d6pmghddd8rs2ws61bqqwdzfq59b") (yanked #t)))

