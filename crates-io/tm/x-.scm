(define-module (crates-io tm x-) #:use-module (crates-io))

(define-public crate-tmx-launch-0.1 (crate (name "tmx-launch") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)))) (hash "0s92wy2jyni3zjnj6v8ma1156dq4l9hj6rqk3slxrv6pwdrzflrk")))

(define-public crate-tmx-rs-0.1 (crate (name "tmx-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "17q9bidb7h5mq5v2p73gis4rmw2xl2q4b0qy2v4azf5y3w977l7s")))

