(define-module (crates-io tm -w) #:use-module (crates-io))

(define-public crate-tm-wheel-0.1 (crate (name "tm-wheel") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "08qrnaq97b8bsydg1pqwklbr3r3za4ghnq010xn5qs18n6rh1ra7") (features (quote (("default" "std")))) (yanked #t) (v 2) (features2 (quote (("std" "dep:slab"))))))

(define-public crate-tm-wheel-0.1 (crate (name "tm-wheel") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "080mkhh9s2rpyj8wwm8pjy9zw2jgrcwmmbi4zx18j9s1lf056mbp") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:slab"))))))

