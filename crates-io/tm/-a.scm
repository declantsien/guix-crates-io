(define-module (crates-io tm -a) #:use-module (crates-io))

(define-public crate-tm-abci-0.1 (crate (name "tm-abci") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "tm-protos") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pvym31flv908kmsp2ap5rs6k1gk7dz66rraizl1nr82qyr5hq81")))

(define-public crate-tm-abci-0.1 (crate (name "tm-abci") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "tm-protos") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16w0c3rk16dgyxjzhk3nba4z6g69mc0kx4lknbcdqizv0lcqi8cw")))

(define-public crate-tm-abci-0.1 (crate (name "tm-abci") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "tm-protos") (req "^0.1") (default-features #t) (kind 0)))) (hash "0np1x08xk7cr9laz2mmjjrlh9qfn30ml0s8q8phagi4g155x1f7b")))

(define-public crate-tm-anode-api-0.1 (crate (name "tm-anode-api") (vers "0.1.0") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "machinery") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "machinery-api") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "=0.19.3") (default-features #t) (kind 0)))) (hash "09wqf4q2b8z7qs39gf5zr2r0n7dahjj3fiapa0vp5g71x1hgghki")))

(define-public crate-tm-anode-api-0.2 (crate (name "tm-anode-api") (vers "0.2.0") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "machinery") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "machinery-api") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "=0.19.3") (default-features #t) (kind 0)))) (hash "086ybakpfv3m3fh2qirx9282065shisipj0vzj8wm4ai12dj328p")))

(define-public crate-tm-anode-api-0.3 (crate (name "tm-anode-api") (vers "0.3.0") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "machinery") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "machinery-api") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0as4ggc44ijkipyjqxh77w5jqfgcii4qwv3sjdsk80is5piz8szg")))

(define-public crate-tm-anode-api-0.4 (crate (name "tm-anode-api") (vers "0.4.0") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "machinery") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "machinery-api") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1hnkkb9w2hyik5j0rpw0gj83a1n86avqnjddk9p846gcypc8nqpl")))

(define-public crate-tm-anode-api-0.5 (crate (name "tm-anode-api") (vers "0.5.0") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "machinery") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "machinery-api") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "=0.19.3") (default-features #t) (kind 0)))) (hash "1sb2w7v2ib91yb0f5dxaykr9wigs7ln788lg8h2mrz8r0y83ls5b")))

(define-public crate-tm-anode-api-0.5 (crate (name "tm-anode-api") (vers "0.5.1") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "machinery") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "machinery-api") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "=0.19.3") (default-features #t) (kind 0)))) (hash "0kryz51wip20ggkzqpzcd8njdyhn70zil8irzwnkxs6kc5i3ajg0")))

(define-public crate-tm-anode-api-0.6 (crate (name "tm-anode-api") (vers "0.6.0") (deps (list (crate-dep (name "const-cstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "machinery") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "machinery-api") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "191f3bdzmjgmmd6chjwv9qg2ay5kf2yfcssg7gijf9kjh4y43isp")))

