(define-module (crates-io tm c5) #:use-module (crates-io))

(define-public crate-tmc5072-0.1 (crate (name "tmc5072") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1i8x81ys9sva49cxxkrjmbiz1rsz6zysl1pacy6dkmq5pvs6v2ix")))

(define-public crate-tmc5072-0.1 (crate (name "tmc5072") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "089wfxabhv3y35d839yw9kjv32df7k5k0ya56y7g9nka0x77lh2l")))

(define-public crate-tmc5160-0.0.1 (crate (name "tmc5160") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1cmy8v8cjwb98xing8hy6nlmyhrw1wgav67x686brd07znyi59sz")))

