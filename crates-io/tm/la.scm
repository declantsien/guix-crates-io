(define-module (crates-io tm la) #:use-module (crates-io))

(define-public crate-tmlatestbackup-0.1 (crate (name "tmlatestbackup") (vers "0.1.0") (deps (list (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "18llwqnzvwgifmf2qk462vnvhnmmrbicsf87m3nqwpwgfpala37n")))

(define-public crate-tmlatestbackup-0.1 (crate (name "tmlatestbackup") (vers "0.1.1") (deps (list (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "14vi699fdqsh6db3abwnqm4xrw3hnpli4pz3pr494wjg4n9is95z")))

(define-public crate-tmlatestbackup-0.1 (crate (name "tmlatestbackup") (vers "0.1.2") (deps (list (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "07j3znszjx37dqj9736ca0146kfx8w33d08qcr3lq81xhpl99nyg")))

