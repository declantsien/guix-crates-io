(define-module (crates-io tm cl) #:use-module (crates-io))

(define-public crate-tmcl-0.1 (crate (name "tmcl") (vers "0.1.0-alpha0") (deps (list (crate-dep (name "socketcan") (req "^1.7") (optional #t) (default-features #t) (kind 0)))) (hash "03skbvi342cqg9qkw6v4lrdrqfpk9hwzccanwl31a3cxvj2j3sdv") (features (quote (("std"))))))

(define-public crate-tmcl-0.1 (crate (name "tmcl") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "socketcan") (req "^1.7") (optional #t) (default-features #t) (kind 0)))) (hash "09aab5fvsfxk8gac74f3852r6f2nyvdnj7m1rgnbna2wy0dll0cd") (features (quote (("std"))))))

(define-public crate-tmcl-0.1 (crate (name "tmcl") (vers "0.1.0-alpha2") (deps (list (crate-dep (name "socketcan") (req "^1.7") (optional #t) (default-features #t) (kind 0)))) (hash "03rb7vzdqn5d17nnxmhdwvxzk04jzdbwrkd4yccwic3isnw0687w") (features (quote (("std"))))))

(define-public crate-tmcl-0.1 (crate (name "tmcl") (vers "0.1.0-alpha3") (deps (list (crate-dep (name "interior_mut") (req "^0.1.0-beta0") (kind 0)) (crate-dep (name "socketcan") (req "^1.7") (optional #t) (default-features #t) (kind 0)))) (hash "0yxvhb7czhimbxrh9w11ijrwkvlbci35gv6aqhyrdkdyqyrp3rxh") (features (quote (("std" "interior_mut/std"))))))

(define-public crate-tmcl-0.1 (crate (name "tmcl") (vers "0.1.0-alpha4") (deps (list (crate-dep (name "interior_mut") (req "^0.1.0-beta0") (kind 0)) (crate-dep (name "socketcan") (req "^1.7") (optional #t) (default-features #t) (kind 0)))) (hash "0391wwg3bxyqb9w3849fm9vnjcbvqwfz7fzcy3fpc5vbngbqa84d") (features (quote (("std" "interior_mut/std"))))))

(define-public crate-tmcl-0.1 (crate (name "tmcl") (vers "0.1.0-beta0") (deps (list (crate-dep (name "interior_mut") (req "^0.1") (kind 0)) (crate-dep (name "socketcan") (req "^1.7") (optional #t) (default-features #t) (kind 0)))) (hash "0a4hp1z5i74bixdayd9db7crmhxmja16wq1f5jv6qas34d1b40wj") (features (quote (("std" "interior_mut/std"))))))

