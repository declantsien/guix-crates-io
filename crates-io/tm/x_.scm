(define-module (crates-io tm x_) #:use-module (crates-io))

(define-public crate-tmx_reader-0.1 (crate (name "tmx_reader") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "1yk181ds5yzm2qgmf1rwb7zd5s9yy3xdld1yl6i811gym2z6qv3g")))

(define-public crate-tmx_utils-0.1 (crate (name "tmx_utils") (vers "0.1.0") (deps (list (crate-dep (name "cargo-watch") (req "^8.4.0") (default-features #t) (kind 0)))) (hash "15hk904gd0824q8kdlpklxffsb0wcmil160pia296693ysryxfzw")))

(define-public crate-tmx_utils-0.1 (crate (name "tmx_utils") (vers "0.1.1") (deps (list (crate-dep (name "cargo-watch") (req "^8.4.0") (default-features #t) (kind 0)))) (hash "1kwmd548ck9nd5zvg46kz0hzh4m37j2bipfqsnvak2f02g0i02vw")))

(define-public crate-tmx_utils-0.1 (crate (name "tmx_utils") (vers "0.1.2") (deps (list (crate-dep (name "cargo-watch") (req "^8.4.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1wijqbw2sbc6j9b0581a0fcqbh3aykf6krq2i7k2ajxmn797b5rj")))

(define-public crate-tmx_utils-0.1 (crate (name "tmx_utils") (vers "0.1.4") (deps (list (crate-dep (name "cargo-watch") (req "^8.4.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "11fz3rmzxcdvwqfi4i4vhcwzy4dn8aryfvhbzxxbzp90ng3dyq5q")))

