(define-module (crates-io tm p1) #:use-module (crates-io))

(define-public crate-tmp102-1 (crate (name "tmp102") (vers "1.0.0") (deps (list (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "16v1887z7drkdidicpcj20i75bg3mh116l0y368x35vv997w7jsh")))

(define-public crate-tmp117-1 (crate (name "tmp117") (vers "1.0.0") (deps (list (crate-dep (name "bilge") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "device-register") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "device-register-async") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nk43d9yh0z9np4li9sxlrh063albd6xf1v85czhykgr5511l95l") (rust-version "1.75")))

(define-public crate-tmp121212-0.1 (crate (name "tmp121212") (vers "0.1.0") (hash "04pxkvb0w8djvcvljdfr2y1r8iqil44drjqlwkb3fsk9w2kynszi")))

(define-public crate-tmp121212-0.1 (crate (name "tmp121212") (vers "0.1.1") (hash "0dg729r4cfv85phb092xvdyxh2ya4jakwv3hrx71vwynbr15qgbl")))

(define-public crate-tmp123-0.1 (crate (name "tmp123") (vers "0.1.1") (hash "19kbk0whm4hf0p2ylqi6v1ww56sng46fwp8z5890251si0595s24")))

(define-public crate-tmp123-0.2 (crate (name "tmp123") (vers "0.2.0") (hash "0q10qgw2lina2l5sax2mhcvfd8v7q467j7amfwszxy3gxsh5zsq4")))

(define-public crate-tmp123-0.1 (crate (name "tmp123") (vers "0.1.2") (hash "0sfvg47knlgbkdhkvcz1qkibvhdh0fhcsyg59npymda2j2hpa28n")))

(define-public crate-tmp1x2-0.1 (crate (name "tmp1x2") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "1d0rf9ybvbkh3ha8d9w8vis57s334f5qki8n5vxxpjrxcdmnd7hn")))

(define-public crate-tmp1x2-0.2 (crate (name "tmp1x2") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i70j52bq5g1xz78vjcyiq3m2x94cnq8fqnw0ywjp5bywa1b5aaa")))

(define-public crate-tmp1x2-0.2 (crate (name "tmp1x2") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1") (default-features #t) (kind 0)))) (hash "0lynykcvqbxfkrfbgnw1sfgydv81is2qnv0a8j9w574x85lhqgdl")))

(define-public crate-tmp1x2-1 (crate (name "tmp1x2") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (features (quote ("eh1"))) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.1") (default-features #t) (kind 0)))) (hash "1x53xi4l08pv9b8xq7180m6xxp7rsrx7688rmz0ih72qfkhcgs5f")))

