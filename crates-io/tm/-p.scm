(define-module (crates-io tm -p) #:use-module (crates-io))

(define-public crate-tm-protos-0.1 (crate (name "tm-protos") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "prost") (req "^0.8") (features (quote ("prost-derive"))) (kind 0)) (crate-dep (name "prost-types") (req "^0.8") (kind 0)))) (hash "0z0iqd50mw8ij3yvqqhd47qigfsqvakgma9aizr6hsgpn1173ivq")))

(define-public crate-tm-protos-0.1 (crate (name "tm-protos") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "prost") (req "^0.10") (features (quote ("prost-derive"))) (kind 0)) (crate-dep (name "prost-build") (req "^0.10.4") (default-features #t) (kind 1)) (crate-dep (name "prost-types") (req "^0.10") (kind 0)))) (hash "0b6lk0yx73pn3l58383k78j65q78dhr26q81krc4iyx4jrh607gh")))

(define-public crate-tm-protos-0.1 (crate (name "tm-protos") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "prost") (req "^0.11") (features (quote ("prost-derive"))) (kind 0)) (crate-dep (name "prost-build") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "prost-types") (req "^0.11") (kind 0)))) (hash "00my72w0lzccm1jb5mwzfpkkfm574spi2fcl5c3d369zg20lsmm7")))

(define-public crate-tm-protos-0.1 (crate (name "tm-protos") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "prost") (req "^0.11") (features (quote ("prost-derive"))) (kind 0)) (crate-dep (name "prost-types") (req "^0.11") (kind 0)))) (hash "1fvnf5i1ai5jqsx6amblfzay8x88zhp6qjn1vrhlczbi7wlbmijq")))

