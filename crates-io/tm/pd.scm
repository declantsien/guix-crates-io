(define-module (crates-io tm pd) #:use-module (crates-io))

(define-public crate-tmpdir-1 (crate (name "tmpdir") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.12") (features (quote ("std"))) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("fs" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0594mg6sbz5kdj52ima1gc8yxykdyl9wc36acpk42cnnpd11kmdl")))

