(define-module (crates-io ou te) #:use-module (crates-io))

(define-public crate-outer_attribute-0.1 (crate (name "outer_attribute") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.84") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "08h8znyfb5phd032gmysss2n7xwcfa439blrkds7jzc2hbisqmk2") (features (quote (("same_layout") ("different_layout") ("default" "different_layout")))) (rust-version "1.56.1")))

(define-public crate-outer_attribute-0.1 (crate (name "outer_attribute") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.84") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1r0a1a707686wb6v3xx95hjcyh1synd2rpkfnsr2yi2xxfs2ky1k") (features (quote (("same_layout") ("different_layout") ("default" "different_layout")))) (rust-version "1.56.1")))

(define-public crate-outer_cgi-0.2 (crate (name "outer_cgi") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.10") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0bbifdn9z8rsmb6zxql3lxkhnwwnrfxilb2hgb0b2dz38m3lac4k")))

(define-public crate-outer_cgi-0.2 (crate (name "outer_cgi") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.10") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0ax7vpfji4p8zvnyjy5lf8bgar6zz95j75f5fkz2pkzz93c1h6ww")))

(define-public crate-outer_cgi-0.2 (crate (name "outer_cgi") (vers "0.2.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1ws215arv1asl9kqxja0b0r26r34a2mxpk0wxlin4m1pa8fr4mdd")))

(define-public crate-outer_cgi-0.2 (crate (name "outer_cgi") (vers "0.2.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1krjmxhb9mngxy2i9gbynpmxzk87q75ys0v8c3md2bgqq9y9dl4d")))

(define-public crate-outer_cgi-0.2 (crate (name "outer_cgi") (vers "0.2.4") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1r6icidfdad0ypy70qiwd0sg7xr9fldf2kilq3pwkd6b6zvaw3d6")))

(define-public crate-outer_cgi-0.2 (crate (name "outer_cgi") (vers "0.2.5") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "07qzslz36hqgdqdwk949c76m5smml3awxaza5flnzy0w0yswa02q")))

(define-public crate-outer_cgi-0.3 (crate (name "outer_cgi") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1ccjywlpma1q77gl5w4344m8n3776swdnmm39myj3x1py28sgcr4")))

(define-public crate-outer_cgi-0.3 (crate (name "outer_cgi") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "013p4kardgmfk96mncq15z45b8xmscbp4arhg15qd3jrbrr87w33")))

(define-public crate-outerspace-0.1 (crate (name "outerspace") (vers "0.1.0") (hash "0vaywkf4na5c07i93cx45lyb027gnr5j4l0vlwbbg4cz4pj3f6qk")))

(define-public crate-outerspace-0.2 (crate (name "outerspace") (vers "0.2.0") (hash "198j8q7zjnxgh0dgkjca54yc4kbn6zhj2x3ac7n6h7czm3vbplww")))

(define-public crate-outerspace-0.2 (crate (name "outerspace") (vers "0.2.1") (hash "1zcqks1sx6648vqn8zmdvnckqw9zzz0zcvjp0m2jcw2rmkvgkwfi")))

