(define-module (crates-io ou td) #:use-module (crates-io))

(define-public crate-outdir-tempdir-0.1 (crate (name "outdir-tempdir") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "05km0aj4qg8f7wxpj7y7xhva6fnqksaa87hxm1w16hwq83db9i63")))

(define-public crate-outdir-tempdir-0.2 (crate (name "outdir-tempdir") (vers "0.2.0") (deps (list (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0bk10xwnfiv8s2xy5xxwavw3rv55b8dwn5xfrck6x3gp0ymnqchz")))

