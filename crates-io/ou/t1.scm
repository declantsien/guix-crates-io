(define-module (crates-io ou t1) #:use-module (crates-io))

(define-public crate-out123-sys-0.0.1 (crate (name "out123-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1lgpfpnn7cfammbj1prspafhf2dfy8qbdjl9i91xbncgmacvja5x")))

(define-public crate-out123-sys-0.1 (crate (name "out123-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1wjwxbvgiirnk12lx4ylb3x7cwjcvzivnyghdll7f07a0kdr69qn")))

(define-public crate-out123-sys-0.2 (crate (name "out123-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0fnw9ndj986h86w8qj36rswc3nxnflllg0s2948ay7prl0lgrdja") (features (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.2 (crate (name "out123-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "01rdrfxmxvirnzq5v2g766finh3dakiwj1pys8076gbab33kc15l") (features (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.3 (crate (name "out123-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0iqqvxygj8cdkxbaiycqbzsw8qpf10640132f0pi3rpk4g14x3sj") (features (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.3 (crate (name "out123-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0av2f6kz02dh8y7ybnv19gbxkr9ry2x620di4s04qg9056vgg9c7") (features (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.4 (crate (name "out123-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1lgh9jifgblm2qiwiwva4yr0lvn5sgh45f6h06lr7rhv8xfnx36w") (features (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.4 (crate (name "out123-sys") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0r9s7xzkzqyzlh4661llci36qv6mdm427mpnkws94wdwdk6n4vvm") (features (quote (("static" "mpg123-sys/static"))))))

(define-public crate-out123-sys-0.5 (crate (name "out123-sys") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1rfj01xbc7mp1dvn7qz65syqj6knq6prlxxy4sny4i2bgjz7gaax") (features (quote (("static" "mpg123-sys/static"))))))

