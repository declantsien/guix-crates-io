(define-module (crates-io ou tf) #:use-module (crates-io))

(define-public crate-outflux-0.1 (crate (name "outflux") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)))) (hash "11xv9v946kdyhd9y3fj8180fmsph5r04gzc9srjm17wbp73b4kg5")))

