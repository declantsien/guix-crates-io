(define-module (crates-io ou ai) #:use-module (crates-io))

(define-public crate-ouai-0.1 (crate (name "ouai") (vers "0.1.0") (deps (list (crate-dep (name "calm_io") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0pkv0jinjyv2v8s3gjkfy341xfwmq3zh54jb0d1x5vhv2cci84x7")))

