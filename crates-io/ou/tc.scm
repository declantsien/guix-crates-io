(define-module (crates-io ou tc) #:use-module (crates-io))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.0") (hash "11s7pra2vpw0608lg8vf3wafb43c9kpvj1xqviq6rwp0np5bmz5m") (yanked #t)))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.1") (hash "1aa1d5lyrx1lgnbhan08y30cly5w840jq3n0pdfk609b1wfdcgnw")))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.2") (hash "0ybm4ilm9460scy4vxs23n53alcv3kyiaxs5wi5zi3h19g2jwa1l")))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.3") (hash "1cp5q8yc3hjxyzpmhv02a40z2flv6wj6j4xmknbmvjpq0wn40wjf")))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.4") (hash "0c4qsvrvgcbdc41q6fkxiajv7map1djkrym7vy0xfipkfrlym3hh")))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.5") (hash "0cv1gy7dqnmzhbx5bnkzmvh0j66ivvbyq0j3navb7ijmdjfgdcin")))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.6") (hash "1szhjca949i9dgg3f0b0pm01mw4v6slpq0b0rgifadd0qyrwxqb5")))

(define-public crate-outcome-0.1 (crate (name "outcome") (vers "0.1.7") (hash "19vbynijr40mwppbzcs2sqgxg87km0v953zg4aixrzvygfq5kzr0")))

(define-public crate-outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c-0.0.0 (crate (name "outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c") (vers "0.0.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (optional #t) (default-features #t) (kind 0)))) (hash "1447vvfk7d2dwkcmwrpvgv7j1m4znidfh3rxvw9jczl89qacpj12") (features (quote (("std") ("report" "eyre" "std") ("default" "std")))) (yanked #t)))

(define-public crate-outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c-0.0.1 (crate (name "outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c") (vers "0.0.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 1)))) (hash "01h1pzldkmkbfzif449m75ac60jn1sbz0mkxchcfzpw29m37akfh") (features (quote (("unstable") ("std") ("report" "eyre" "std") ("nightly" "unstable") ("diagnostic" "miette" "std") ("default" "std")))) (yanked #t)))

(define-public crate-outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c-0.0.2 (crate (name "outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c") (vers "0.0.2-alpha.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 1)))) (hash "06rq4p532p5s4fdrfhmpcn5rh4cq16kar836j49w0bsfv7pkksv4") (features (quote (("unstable") ("std") ("report" "eyre" "std") ("nightly" "unstable") ("diagnostic" "miette" "std") ("default" "std")))) (yanked #t)))

(define-public crate-outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c-0.0.2 (crate (name "outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c") (vers "0.0.2") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 1)))) (hash "0lhpd6q0h1bzkxn0cc0x44v4bbibxmg03cy9qrwhrz4b0k5f26mr") (features (quote (("unstable") ("std") ("report" "eyre" "std") ("nightly" "unstable") ("diagnostic" "miette" "std") ("default" "std")))) (yanked #t)))

(define-public crate-outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c-0.1 (crate (name "outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 1)))) (hash "0kxg6g2xjg61kc48fqb5bnxs9kn7ja2dkkcbls8asak60zkxjf9r") (features (quote (("unstable") ("std") ("report" "eyre" "std") ("nightly" "unstable") ("diagnostic" "miette" "std") ("default" "std"))))))

(define-public crate-outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c-0.1 (crate (name "outcome-46f94afc-026f-5511-9d7e-7d1fd495fb5c") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req ">=3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 1)))) (hash "1mbqijfskzi6ah3dc17k3vdp28max0cjinhsvnqh9xs0y9bcbb16") (features (quote (("unstable") ("std") ("report" "eyre" "std") ("nightly" "unstable") ("diagnostic" "miette" "std") ("default" "std"))))))

(define-public crate-outcome-sim-0.1 (crate (name "outcome-sim") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0lqr9kg7xfclwzf2ai8b042s9wnq2d19h5p45njxv21mx7dsyaw4") (yanked #t)))

