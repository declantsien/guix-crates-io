(define-module (crates-io ou tr) #:use-module (crates-io))

(define-public crate-outref-0.1 (crate (name "outref") (vers "0.1.0") (hash "1x61h7dl1cc6cj2f3zsalr8d98v0cw6497sykwxf74wjmqljh8kz") (rust-version "1.62.1")))

(define-public crate-outref-0.2 (crate (name "outref") (vers "0.2.0") (hash "1kjbkpxqwrpxa93s9bizpjxwnm6qfq7c1jmcks2ksqbw9xxyjvdi") (rust-version "1.63.0")))

(define-public crate-outref-0.3 (crate (name "outref") (vers "0.3.0") (hash "14valzfjsvfmqzymgq73v819qfmi9hsxcqy29gvwnxyw6m60zbb1") (rust-version "1.63.0")))

(define-public crate-outref-0.4 (crate (name "outref") (vers "0.4.0") (hash "0v10p599fdk283lj4hhmsak8lix0y61kp8iqi7rxzpdkjmmn4j6m") (rust-version "1.63.0")))

(define-public crate-outref-0.5 (crate (name "outref") (vers "0.5.0") (hash "0h7ziagd312xqn8fn25dbbd5vhjrga01pi06h3f8cqdm1rv322s6") (rust-version "1.63.0")))

(define-public crate-outref-0.5 (crate (name "outref") (vers "0.5.1") (hash "0ynw7nb89603gkfi83f9chsf76ds3b710gxfn12yyawrzl7pcc20") (rust-version "1.63.0")))

