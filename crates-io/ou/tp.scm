(define-module (crates-io ou tp) #:use-module (crates-io))

(define-public crate-output-0.1 (crate (name "output") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "0var2lnxg95cggg42mmx12qfbrb7qa0jzdv3bqs0bzxhv0nhf47x")))

(define-public crate-output-0.1 (crate (name "output") (vers "0.1.2") (deps (list (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "164d6y7hs3gwzn2bd35al1v924l9i0547h63rw3vgfbjyi5crc04")))

(define-public crate-output-0.2 (crate (name "output") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "1v08r0w8bzjdlly22w199k1fmwn9msxpydhk974zv3mwfkg395fd")))

(define-public crate-output-0.3 (crate (name "output") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "0k3nw302akqgvp7yk882ziq16njvyljqwcizpcfb1zaz31axm7ik")))

(define-public crate-output-0.3 (crate (name "output") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "0ljhvxk1iliwy8mxalp0x6sdbxrxhk2idjhmsz2n8hjjzq3nbi4c")))

(define-public crate-output-0.4 (crate (name "output") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "1w47d683dwjncb33wbvkzfj8aqm38ggazfrpp55ajisnk57x16j9")))

(define-public crate-output-0.4 (crate (name "output") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "0wk7cwhns3x0xd2i936w90hfr57cqy4kbfapnrzn5brxgcx5aj7s")))

(define-public crate-output-0.5 (crate (name "output") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "0y7w89av5j75bf8p3mf49bsb859bbkx85i00dra1hcyf6w3x3jbh")))

(define-public crate-output-0.6 (crate (name "output") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "0ccn0ijc05rrcnnm43b7jnw4ba49y3p9lgjgss5hhqd47a6h3g73")))

(define-public crate-output-0.6 (crate (name "output") (vers "0.6.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "0jldppzip49gwnzn2dz79d352sg3xg9xgdlj1aw3dn4xsxk5fmyr")))

(define-public crate-output-0.6 (crate (name "output") (vers "0.6.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.1") (default-features #t) (kind 0)))) (hash "1x6fdwxrhk1j1kjy6m0m4gfbwhw6j9rgjh78mz2qbvn7bjqqfarv")))

(define-public crate-output-coloring-0.1 (crate (name "output-coloring") (vers "0.1.0") (hash "1v54k0qkncm4zshzq03yk9250qrp7y8wgs1zszgxmlbbqgczldwy")))

(define-public crate-output_vt100-0.1 (crate (name "output_vt100") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("winuser" "winbase" "consoleapi" "processenv"))) (default-features #t) (kind 0)))) (hash "14c5q6f6gwqrszjy4nx6sr6dzq0jy0b8x12i1w40vhb5irf3401y")))

(define-public crate-output_vt100-0.1 (crate (name "output_vt100") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("winuser" "winbase" "consoleapi" "processenv"))) (default-features #t) (kind 0)))) (hash "1xxi9wvkhzbfzs4pmfgbn69i9pgdf415346nchh0kvfl12xp380s")))

(define-public crate-output_vt100-0.1 (crate (name "output_vt100") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("winuser" "winbase" "consoleapi" "processenv"))) (default-features #t) (kind 0)))) (hash "1ygqplpxz4gg3i8f3rkan2q69pqll7gv65l2mmd8r9dphnvwbkak")))

(define-public crate-output_vt100-0.1 (crate (name "output_vt100") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("winuser" "winbase" "consoleapi" "processenv"))) (default-features #t) (kind 0)))) (hash "0rpvpiq7gkyvvwyp9sk0zxhbk99ldlrv5q3ycr03wkmbxgx270k2")))

(define-public crate-outputs-0.0.1 (crate (name "outputs") (vers "0.0.1") (hash "01fnkm9b04b6i4byixqwgz3l216fihlgx81b54hjpbk70q4734a5")))

(define-public crate-outputs-0.0.2 (crate (name "outputs") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0gms0x0ihnjzsz70xl421ri5yz27y5zvijdc7j36kpjs3gisaa4w")))

(define-public crate-outputs-0.0.3 (crate (name "outputs") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1vbhqv6mb3xmgbk2511qipxcwb40knn22y8sjhs5kp6rkgqs1fig")))

(define-public crate-outputs-0.0.4 (crate (name "outputs") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "03jlcwmyv12mbpgpkbc6qww91vq4v1lisirh8ykwzk815g0lyphg")))

(define-public crate-outputs-0.0.5 (crate (name "outputs") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0qnqdcrxj875d6pnigyj0ybny2kq0pn11i28krrdgq8j8ah6f6gg")))

(define-public crate-outputs-0.0.6 (crate (name "outputs") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1nvfq19h907b2mkpk1zsw473h3pyxd06b9y3w11n75cj3hxsvq4f")))

