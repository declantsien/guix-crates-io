(define-module (crates-io ou ti) #:use-module (crates-io))

(define-public crate-outils-0.1 (crate (name "outils") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "0cb7d3k6w7aizwjyxyhvhirxxzqz8f51ppw64061dyql6dpwd28l")))

(define-public crate-outils-0.1 (crate (name "outils") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "1p8hx7nidq5nq9wyas9n7crkfnl17709ylb0qbqcka1wca0drx9c")))

(define-public crate-outils-0.1 (crate (name "outils") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1yvlhd1r4kyrw76scv6gq943z9y95xdxkm0aqv0gjz366i147bqg")))

(define-public crate-outils-0.1 (crate (name "outils") (vers "0.1.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1njvm3xkydsb2pkmpc59rhag47m1bfdfxjjklf1hgxv4lvd4a0sj")))

(define-public crate-outils-0.2 (crate (name "outils") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0byipqxqv2vmbflv0x6siy00dzbb8rgaw5yxv5miwyj9n0lfhdjv")))

(define-public crate-outils-0.3 (crate (name "outils") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0nry4jfmpc9475zbq0j4qfzg4v8aajjpy5a80fcxsbhr69bvrwky")))

