(define-module (crates-io ou to) #:use-module (crates-io))

(define-public crate-outoforderfs-0.1 (crate (name "outoforderfs") (vers "0.1.0") (deps (list (crate-dep (name "chan-signal") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fuse") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wq7zbws2yizwfs45b8iyizmh4w1d9mf7dj3w3f6z9887bri593y")))

(define-public crate-outoforderfs-0.1 (crate (name "outoforderfs") (vers "0.1.1") (deps (list (crate-dep (name "chan-signal") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fuse") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mjw0zgx2w49vqx0q5ng9d31gxdjx8wdi8x8sr18w97vv0xhra6k")))

