(define-module (crates-io ou tm) #:use-module (crates-io))

(define-public crate-outmove-0.1 (crate (name "outmove") (vers "0.1.0") (hash "0266kvkh4ykp9k8sl24d28a9pgdpldf9krds7ic7prwvv00xyly5")))

(define-public crate-outmove-common-0.1 (crate (name "outmove-common") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bcs") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "mirai-annotations") (req "^1.10.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "01wm0qcrmadnxbaw2s0cw78jx4i543i50l9ky4crl4bzzxay4swq")))

