(define-module (crates-io ou r-) #:use-module (crates-io))

(define-public crate-our-data-0.1 (crate (name "our-data") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1nyjcyh2r2kisrmlx359wd6dp2p6p40njdihv5m9li6yy43djvx9")))

