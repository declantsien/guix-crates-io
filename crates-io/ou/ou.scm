(define-module (crates-io ou ou) #:use-module (crates-io))

(define-public crate-ouou_dictation-0.1 (crate (name "ouou_dictation") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "language-tags") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "lingua") (req "^1.4.0") (features (quote ("japanese" "english" "chinese"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tts") (req "^0.25") (default-features #t) (kind 0)))) (hash "0ijdr0khhdr3r3n2xv9j8ybjmbvspnyr2f6xwqnq7hpc446bs7zw")))

