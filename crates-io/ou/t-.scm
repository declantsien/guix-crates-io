(define-module (crates-io ou t-) #:use-module (crates-io))

(define-public crate-out-reference-0.1 (crate (name "out-reference") (vers "0.1.0") (hash "1kr1vnngvnf3a1al54zdy9gyqaw1sjffp0cjsbiz2ca5hmi6d2y6") (features (quote (("std") ("nightly") ("default" "std")))) (yanked #t)))

(define-public crate-out-reference-0.1 (crate (name "out-reference") (vers "0.1.1") (hash "1fs457jh59vym27c92gi60mrz3y5a807q3306ws15gis7n262sc5") (features (quote (("std") ("nightly") ("default" "std")))) (yanked #t)))

(define-public crate-out-reference-0.2 (crate (name "out-reference") (vers "0.2.0") (hash "0xgdadvwkg34d9jcxzljf0s7bqip00pma4hrfsr1iz6alkn5idkf") (features (quote (("std") ("nightly") ("default" "std"))))))

