(define-module (crates-io ou i-) #:use-module (crates-io))

(define-public crate-oui-data-0.1 (crate (name "oui-data") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("blocking"))) (default-features #t) (kind 1)))) (hash "0rs7pr82ka9c6vka73vrssrhp0f2ny34598z32dpcaf2g628n33s")))

(define-public crate-oui-data-0.1 (crate (name "oui-data") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("blocking"))) (default-features #t) (kind 1)))) (hash "1vvjrm4imzxk9cs7x36hzpx1y2zgram0n360d3bqd4fwl0vmgglj")))

(define-public crate-oui-data-0.1 (crate (name "oui-data") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("blocking"))) (default-features #t) (kind 1)))) (hash "0abjqbdw7056j4q2zz1z14il95w72dfpmparhrfv6m86hiyl52y7")))

(define-public crate-oui-data-0.1 (crate (name "oui-data") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0jk7sdqa4hzwrr1v5qk1999bpax2hnqlr2h061r943nqicxdw353")))

(define-public crate-oui-data-0.2 (crate (name "oui-data") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1swlmx14v5d2dx2q1jv72yhb9mcrnbs33q3h6fp1vdxk879450fs")))

