(define-module (crates-io mc co) #:use-module (crates-io))

(define-public crate-mccolors-rust-0.1 (crate (name "mccolors-rust") (vers "0.1.0") (hash "1jpicmmxipnhkb88hf2vw024pgr7ynkdklxqqnsqsw2csl1r0m3n")))

(define-public crate-mccolors-rust-0.1 (crate (name "mccolors-rust") (vers "0.1.1") (hash "1j5jfw0p54gd7r7mji9yj4dyk8737yi947asfyms9zlxnfpqc0cp")))

(define-public crate-mccolors-rust-0.1 (crate (name "mccolors-rust") (vers "0.1.2") (hash "1dsni67r6s3gxh9p7p9r0brhbrnf4nfbsl0ba7nlcmdp7crwvd0f")))

(define-public crate-mccolors-rust-0.1 (crate (name "mccolors-rust") (vers "0.1.3") (hash "1yif9sk0dpr92gmg7gkgy0jrvjl3r4aaxvs8fbqm359pw0ss8j0q")))

