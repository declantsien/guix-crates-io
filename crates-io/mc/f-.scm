(define-module (crates-io mc f-) #:use-module (crates-io))

(define-public crate-mcf-config-demo-lib-0.1 (crate (name "mcf-config-demo-lib") (vers "0.1.0") (deps (list (crate-dep (name "mcf-config-demo-settings") (req "^0.1") (default-features #t) (kind 0)))) (hash "17lzja2njprnysia8n920bhhvnfbkmbnnb4002svs43brrqrr0ng")))

(define-public crate-mcf-config-demo-settings-0.1 (crate (name "mcf-config-demo-settings") (vers "0.1.0") (hash "0f78hak65gsa74n1b4z5zmcnzzp8zfn8w6nqjifx58w7040f3v7m")))

