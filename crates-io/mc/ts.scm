(define-module (crates-io mc ts) #:use-module (crates-io))

(define-public crate-mcts-0.1 (crate (name "mcts") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "1icrsbd3wq6nv5iilgaphmg248r3z5cln568wcr1miqawszh8cjg")))

(define-public crate-mcts-0.2 (crate (name "mcts") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "0y17ilkbj7696z1bjkg276gh2l1mma14wdd22n4m2d7dh020wjng")))

(define-public crate-mcts-0.3 (crate (name "mcts") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "1qas2g3i9mfpwj41iqhcyrfzjzs2ax2mcd5xryh829w0iqh28bbx")))

(define-public crate-mctser-0.1 (crate (name "mctser") (vers "0.1.0") (hash "0w0p6i3hyk2kg2vgvjxpch57ghh7k2hwhv7w5h3i39pq7yigfqn6")))

