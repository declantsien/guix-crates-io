(define-module (crates-io mc ca) #:use-module (crates-io))

(define-public crate-mccaption-0.1 (crate (name "mccaption") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1dxq6dw51q9gnh3yjia8bknr9byybwn345mmk6pa8ixzn1mpcv3c")))

