(define-module (crates-io mc in) #:use-module (crates-io))

(define-public crate-mcinterface-0.2 (crate (name "mcinterface") (vers "0.2.0") (hash "18igx8nyiv86sd8cac117px3xycs7iwy0gfi4a3p3hix8knpwf74") (features (quote (("fmt") ("default" "fmt"))))))

(define-public crate-mcinterface-0.3 (crate (name "mcinterface") (vers "0.3.0") (hash "0fddj7fw3n2rcf5jn51bgwn979bj8jizani80sqfdb0h09hbrwl5") (features (quote (("fmt") ("default" "fmt"))))))

(define-public crate-mcinterface-0.3 (crate (name "mcinterface") (vers "0.3.1") (hash "0g7s2di29l8pv0hwqaf29jwci305qggimbpsgjgq38fsdllw872q") (features (quote (("fmt") ("default" "fmt"))))))

(define-public crate-mcinterface-0.3 (crate (name "mcinterface") (vers "0.3.2") (hash "1ahqljg15hcj64j57qkmmsfzia0yka0hq0cfdzqbz8pn0i5hx19w") (features (quote (("fmt") ("default" "fmt"))))))

