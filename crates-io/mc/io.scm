(define-module (crates-io mc io) #:use-module (crates-io))

(define-public crate-mcio-0.1 (crate (name "mcio") (vers "0.1.2") (hash "0s9r4qqs0jp36rclnkdgk587gwazw5ndnlf1ifjrqhx1f0qijpy0")))

(define-public crate-mcio-0.1 (crate (name "mcio") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mnclh1khydq13hzlv2671hv8zghgkjgh7ynnd4gmrs1bzqgdabn")))

(define-public crate-mcio-0.1 (crate (name "mcio") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x3j6z8vmgnnyly8a9sfvb39z976bbm1w5yd332s6qgi9gw1i49g")))

(define-public crate-mcio-0.2 (crate (name "mcio") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "05kzvwrrw54xmhgfzm25v71fvhpranqdsgxg9v0fyky1wb85a140")))

(define-public crate-mcio-0.3 (crate (name "mcio") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mibb3h090d6yqqa68yjp3l450sw14i19alijskzyyggdvyx4iwd")))

(define-public crate-mcio-0.3 (crate (name "mcio") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-resolver") (req "^0.9") (default-features #t) (kind 0)))) (hash "12vb5zjjvxcxihjhzhqdm0bkqbhinn542npfwpxvgc4386kb1ppl")))

