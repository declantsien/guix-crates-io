(define-module (crates-io mc mf) #:use-module (crates-io))

(define-public crate-mcmf-1 (crate (name "mcmf") (vers "1.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0fkri1c48c81ma5fm5x8p96r9mhln24qy1lsddbi8fw0lyrhi434")))

(define-public crate-mcmf-1 (crate (name "mcmf") (vers "1.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "1bnn4573qjfm9zxsj9gw7dwfl4img16i65a66h1k4azabb1484lc")))

(define-public crate-mcmf-2 (crate (name "mcmf") (vers "2.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "14wd6xn6lpinpc3va1klzd3z01qzq7hx0zad2h3dwhbs33jqkclc")))

