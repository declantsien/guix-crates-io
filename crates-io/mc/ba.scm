(define-module (crates-io mc ba) #:use-module (crates-io))

(define-public crate-mcbanner-0.1 (crate (name "mcbanner") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^8.2.0") (default-features #t) (kind 0)))) (hash "02ghdi7fsg774hirpyq7435m0w97qfyxjd27jwx60v1x0rzi5wci") (yanked #t)))

(define-public crate-mcbanner-0.1 (crate (name "mcbanner") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^8.2.0") (default-features #t) (kind 0)))) (hash "1xygii4jpf3l0fxkx6hpjg5bcad77725vjysqb7ayxh9kxljg60y")))

