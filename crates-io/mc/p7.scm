(define-module (crates-io mc p7) #:use-module (crates-io))

(define-public crate-mcp794xx-0.1 (crate (name "mcp794xx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rtcc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mbn1kfs2aarrc5l8nmak43pag5whmi636api9lak5kzskrdgf72")))

(define-public crate-mcp794xx-0.2 (crate (name "mcp794xx") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rtcc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r4md9ha01dhq0gx5gz0k5wjs2h6iai1nkd6p3bny76bz7bf448l")))

(define-public crate-mcp794xx-0.2 (crate (name "mcp794xx") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rtcc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gvchpmdk4sl8qaw09d4vj4vcgmq06iiw56p5ykcjfaavzj59hp7")))

(define-public crate-mcp794xx-0.3 (crate (name "mcp794xx") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rtcc") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kvn4k6jr8bqrfv3p70bzwqjny2lc258lj6559w50ijjga3vagfz")))

(define-public crate-mcp795xx-0.1 (crate (name "mcp795xx") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0w8kshjfjxa55pn11rar1m6bmlg4vxxm4v0l92101w2xcfimrb3p")))

(define-public crate-mcp795xx-0.1 (crate (name "mcp795xx") (vers "0.1.1") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1rcx3g05gfbg6a4y542dsnl0pm27nvcya8cx2rd0zsagc0fwbbdd")))

(define-public crate-mcp795xx-0.2 (crate (name "mcp795xx") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.10") (optional #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "11a9v018bn2jbkdadal7p8qyc37s91k360clcxbr5gi997c7jjcl") (features (quote (("default" "chrono"))))))

