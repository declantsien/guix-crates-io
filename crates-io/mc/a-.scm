(define-module (crates-io mc a-) #:use-module (crates-io))

(define-public crate-mca-cuboids-0.1 (crate (name "mca-cuboids") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "fastanvil") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "fastnbt") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "pprof") (req "^0.10.0") (features (quote ("criterion" "flamegraph"))) (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)))) (hash "0rrqdi9iza7nkyls7y4nzfkbydn09nd0n8dih8hyvnkvg7x22j56")))

(define-public crate-mca-cuboids-0.2 (crate (name "mca-cuboids") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "fastanvil") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "fastnbt") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "pprof") (req "^0.10.0") (features (quote ("criterion" "flamegraph"))) (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)))) (hash "06dwvn80j5djg6xqmnwl8yggdsbffibw8lrkwhhcg7nnaf91qd16")))

(define-public crate-mca-parser-0.0.1 (crate (name "mca-parser") (vers "0.0.1") (hash "1dhlv79dfyh054dfq0vprwqyvb79ibj7zgw9vsz2faf7z68a4svs")))

(define-public crate-mca-parser-0.0.2 (crate (name "mca-parser") (vers "0.0.2") (hash "0vhvcg05gbvylp1vjf9xh8kb37wxryfpjzmi1vyd8h8mc77qp6l7")))

(define-public crate-mca-parser-0.0.3 (crate (name "mca-parser") (vers "0.0.3") (deps (list (crate-dep (name "fastnbt") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)))) (hash "1sgf9p0z0ys8nzgg8drm42i5s3ynniipvva1968dzdxn2mmlbnkr")))

(define-public crate-mca-parser-0.1 (crate (name "mca-parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "fastnbt") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)))) (hash "0m6c4bjbfs6wblqxky9ig2700y1pc906kqj29hm68fypnb4grlc6")))

(define-public crate-mca-parser-0.2 (crate (name "mca-parser") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "fastnbt") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)))) (hash "0i1f088yks6n2y7kgi1damb6jfd3a1k0acv6b9xvfky5rmmy45x4")))

(define-public crate-mca-parser-1 (crate (name "mca-parser") (vers "1.0.0") (deps (list (crate-dep (name "fastnbt") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)))) (hash "1xpf7ri1rjp4dvc0divr2wzx0lp2rvmqs6rdhqgcb6w0czr1c02q")))

(define-public crate-mca-parser-1 (crate (name "mca-parser") (vers "1.0.1") (deps (list (crate-dep (name "fastnbt") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)))) (hash "1yj8d8jrvz8dwfhdhg5cf370sqsz8v8xz75r77rnb5gjmpwr5sgq")))

(define-public crate-mca-parser-1 (crate (name "mca-parser") (vers "1.0.2") (deps (list (crate-dep (name "fastnbt") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)))) (hash "15w600y4z304qk73l3917ibzjd1nazkv86csp113mgfw5v406v3h")))

