(define-module (crates-io mc l_) #:use-module (crates-io))

(define-public crate-mcl_derive-0.2 (crate (name "mcl_derive") (vers "0.2.3-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1glb9dnzhlxlcdax86kz3r87h7sdxqghi0wc0dlw8vmkaj58sn9m")))

(define-public crate-mcl_derive-0.3 (crate (name "mcl_derive") (vers "0.3.0-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0fk7l4q72kmr91ps0ls043fk0vay1whlz71ssxwmxfrnv87h710q")))

(define-public crate-mcl_derive-0.4 (crate (name "mcl_derive") (vers "0.4.0-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1rkvn8pwajfiln270vrl72n3ywj5l5k1lcmragwkv70z0za3gb1r")))

(define-public crate-mcl_derive-0.5 (crate (name "mcl_derive") (vers "0.5.0-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0s2wi0yyk1bb2nhy4yw9qh5rji16g7m6iyiyykfjxvhprwjkk0wr")))

(define-public crate-mcl_rust-0.0.1 (crate (name "mcl_rust") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1rqf0kj11v4sf48vz45yn5gbgl21083d1jyrp00j7kb6jydrrddp")))

(define-public crate-mcl_rust-1 (crate (name "mcl_rust") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "09n28yikla5cmzxj1c3fzfgi44v8h2jbs5z1pjjrckgn6qqbm04j")))

(define-public crate-mcl_rust-1 (crate (name "mcl_rust") (vers "1.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "05h5v7fypa183f2r3lg6sajk5ixj85v71rvx7jn0hsigfwyjw1fl")))

(define-public crate-mcl_sched-0.1 (crate (name "mcl_sched") (vers "0.1.0-rc") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libmcl-sys") (req "^0.1.2-rc") (default-features #t) (kind 0)))) (hash "1pp2i6ia9xbydh9251d7kqvpmg3qrfy5ayz0va68c9lnl4abldlh")))

(define-public crate-mcl_sched-0.1 (crate (name "mcl_sched") (vers "0.1.0-rc2") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libmcl-sys") (req "^0.1.2-rc2") (default-features #t) (kind 0)))) (hash "0ikhbkw827cwbf94cqq61s3l6hw9gi21vw5fzds700mbpr4dxvaw") (features (quote (("shared_mem" "libmcl-sys/shared_mem") ("pocl_extensions" "shared_mem" "libmcl-sys/pocl_extensions") ("mcl_debug" "libmcl-sys/mcl_debug") ("docs-rs" "libmcl-sys/docs-rs"))))))

(define-public crate-mcl_sched-0.1 (crate (name "mcl_sched") (vers "0.1.0-rc3") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libmcl-sys") (req "^0.1.2-rc3") (default-features #t) (kind 0)))) (hash "0r0w08spc1jns7cvd29rwxnj3mz9igk6sfm4hn7cl9nrki2vxk7y") (features (quote (("shared_mem" "libmcl-sys/shared_mem") ("pocl_extensions" "shared_mem" "libmcl-sys/pocl_extensions") ("mcl_debug" "libmcl-sys/mcl_debug") ("docs-rs" "libmcl-sys/docs-rs"))))))

(define-public crate-mcl_sched-0.1 (crate (name "mcl_sched") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libmcl-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "04h1vnfyjnw8d1450gyqla7ih13hx82pan15w2pzfwibp1n1bp8z") (features (quote (("shared_mem" "libmcl-sys/shared_mem") ("pocl_extensions" "shared_mem" "libmcl-sys/pocl_extensions") ("mcl_debug" "libmcl-sys/mcl_debug") ("docs-rs" "libmcl-sys/docs-rs"))))))

