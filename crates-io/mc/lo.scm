(define-module (crates-io mc lo) #:use-module (crates-io))

(define-public crate-mclockwork-utils-2 (crate (name "mclockwork-utils") (vers "2.0.19") (deps (list (crate-dep (name "anchor-lang") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "~0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "static-pubkey") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "010iyj69cd6r9zakjjy86pqbkkh07i2q7xwrv2n7zvl0bcc9wkgx")))

(define-public crate-mclog-0.0.0 (crate (name "mclog") (vers "0.0.0") (hash "0jzkiyhnac210jirvhknma22v3g2xqcllc3f2qk3skariv4zff03")))

