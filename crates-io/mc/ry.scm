(define-module (crates-io mc ry) #:use-module (crates-io))

(define-public crate-mcrypt-0.1 (crate (name "mcrypt") (vers "0.1.0") (deps (list (crate-dep (name "git-version") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "build-data") (req "^0.1.3") (default-features #t) (kind 1)))) (hash "1ih3n95hgrwvz7404xhpdpnrx7gzc0m0a1sk6rwrgn40vy9qs64s") (yanked #t)))

(define-public crate-mcrypt-0.1 (crate (name "mcrypt") (vers "0.1.1") (deps (list (crate-dep (name "build-data") (req "^0.1.3") (default-features #t) (kind 1)))) (hash "0xy1lqg7pwwac79r45r7lgpnz50fhkahlknkixd00yryrviim27l") (yanked #t)))

(define-public crate-mcrypt-0.1 (crate (name "mcrypt") (vers "0.1.2") (deps (list (crate-dep (name "build-data") (req "^0.1.3") (default-features #t) (kind 1)))) (hash "1qf4008x2aaixyhnqmj9nyijafrl1j5xby298l9mx9k1rky18wp9") (yanked #t)))

(define-public crate-mcrypt-0.1 (crate (name "mcrypt") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 1)) (crate-dep (name "vergen") (req "^7.5.0") (default-features #t) (kind 1)))) (hash "1f50wha45gkr63anbifp0hk88smzwi6zb92ng9zdfc27gx5vi73r") (yanked #t)))

(define-public crate-mcrypt-0.1 (crate (name "mcrypt") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 1)) (crate-dep (name "vergen") (req "^7.5.0") (features (quote ("build" "rustc"))) (kind 1)))) (hash "03bvnhb1pd0ahrc0xpawbaj3br676yvhzkhyg4wfrkblxa9x2gqf")))

