(define-module (crates-io mc mp) #:use-module (crates-io))

(define-public crate-mcmp-0.1 (crate (name "mcmp") (vers "0.1.2") (hash "0q4dywqrdrnsnipnmc5q8f04l8p5bgbim24rqf6bnrlcs7825r1c") (yanked #t)))

(define-public crate-mcmp-0.1 (crate (name "mcmp") (vers "0.1.3") (hash "10nr0whzyxiygv6d0m3il5pjk7kixdvx3j1fdl89ryd1yi19wilr") (yanked #t)))

(define-public crate-mcmp-0.1 (crate (name "mcmp") (vers "0.1.4") (hash "18pw2igz4nw0sn4sikhfpvxgyvkd0i38q20il4csvs0q8srqwfjq") (yanked #t)))

