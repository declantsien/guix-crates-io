(define-module (crates-io mc _b) #:use-module (crates-io))

(define-public crate-mc_bootstrap-0.1 (crate (name "mc_bootstrap") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "07707rcmdhcvgrdzvf479xbf2s47x6gh9rh1y096np3i6z92yhbr")))

