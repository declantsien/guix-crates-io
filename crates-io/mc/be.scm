(define-module (crates-io mc be) #:use-module (crates-io))

(define-public crate-mcbe-lan-advertizer-0.1 (crate (name "mcbe-lan-advertizer") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1spm0ndnw02m2c2q1d0fly17byv3qhrdr78iqpn4nr5a4bwj5d4c")))

(define-public crate-mcbe-lan-advertizer-0.1 (crate (name "mcbe-lan-advertizer") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "10ayjfyp04hfmma56mj2ygzfkwmwpnrdfw95la94v969g93p8q6j")))

