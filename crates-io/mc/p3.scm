(define-module (crates-io mc p3) #:use-module (crates-io))

(define-public crate-mcp3008-1 (crate (name "mcp3008") (vers "1.0.0") (deps (list (crate-dep (name "spidev") (req "^0.3.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1ck1a5crcfqxrw50px835xwv5jyp3fwg1plvdz0prvwslp4igxy5")))

(define-public crate-mcp3208-0.1 (crate (name "mcp3208") (vers "0.1.0") (deps (list (crate-dep (name "spidev") (req "^0.4.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0xb1azn9krn4b47snir4rnb32w820hkikpwsq9h8ymxnbwzr0p3v")))

(define-public crate-mcp320x-0.1 (crate (name "mcp320x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0gm5bq792jji3wpwl08vjmjy5didn6k69nm4722877g253mkbxiy")))

(define-public crate-mcp320x-0.1 (crate (name "mcp320x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1ww09bg89xbcvlm4g5l06ih58ynx2mqjcsgnqh9lq7jcdk38bycm")))

(define-public crate-mcp3221-0.1 (crate (name "mcp3221") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0d2bqvv604hbg412a0lp0bmf4d2d3g0cqzsyal7zh6ncy2bn53is")))

(define-public crate-mcp3425-0.1 (crate (name "mcp3425") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "103d7gxsp4mxc8yzlbcllb4czqydss07ghirsqkvl6a8f4phy8iz")))

(define-public crate-mcp3425-0.1 (crate (name "mcp3425") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "03bbfsghldlh3nkd8qcjgs4h90spx3jyni4xn969cxp2knx3cwxa")))

(define-public crate-mcp3425-0.2 (crate (name "mcp3425") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "measurements") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "0gfwn7scp57v6r8k4j7hgs8dqs8p1vflchp6fsdi5amvh9nz2qfs") (features (quote (("default"))))))

(define-public crate-mcp3425-0.2 (crate (name "mcp3425") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "measurements") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "1r8kdwba8b9hnk261vb5py9d66j48d5cix0hsbasmfnrgm9dc1sq") (features (quote (("default"))))))

(define-public crate-mcp3425-0.3 (crate (name "mcp3425") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "measurements") (req "^0.10") (optional #t) (default-features #t) (kind 0)))) (hash "0ryjpzplvdy5xjjk1zgi525993hsx13v6by4csj1x6qlwkpg61zw") (features (quote (("default"))))))

(define-public crate-mcp3425-1 (crate (name "mcp3425") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "measurements") (req "^0.11") (optional #t) (kind 0)))) (hash "1rbmlibf3v3gfilwsjww58nmpqcnfcyj3qi63vmfdsa4hn3g58hr") (features (quote (("quad_channel") ("dual_channel") ("default"))))))

(define-public crate-mcp3425-1 (crate (name "mcp3425") (vers "1.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (features (quote ("eh0"))) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "measurements") (req "^0.11") (optional #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18") (default-features #t) (kind 2)))) (hash "04cip1k8w8m7i701i5s22n2bx44i0rvx61z1k4xrnyz8myrdh3l5") (features (quote (("quad_channel") ("dual_channel") ("default"))))))

(define-public crate-mcp346x-0.1 (crate (name "mcp346x") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "measurements") (req "^0.11") (default-features #t) (kind 0)))) (hash "15q1l56bgav9vhq9k7nq4c14srbgsbwzh1x90yxdw023nxihipr3")))

(define-public crate-mcp3xxx-0.1 (crate (name "mcp3xxx") (vers "0.1.0-pre.1") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "17caq8ja1lbn9x4wkpvg83cpg0mwgm50y4iasqpb3ps8h2fmfjxf")))

