(define-module (crates-io mc -v) #:use-module (crates-io))

(define-public crate-mc-vanilla-0.1 (crate (name "mc-vanilla") (vers "0.1.0") (deps (list (crate-dep (name "mc-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "1x462q6p3vd629hqd781w4ikhznzjaz34kfapdzndhaxigixlqjx")))

(define-public crate-mc-vanilla-0.1 (crate (name "mc-vanilla") (vers "0.1.1") (deps (list (crate-dep (name "mc-core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "07ch8bi0jchb7jf4h0bi8h6a9agps6rdyw2wz8wk4gimf8rm0brx")))

(define-public crate-mc-varint-0.1 (crate (name "mc-varint") (vers "0.1.0") (hash "0ww3gsga0am6wl3mp7zd09rz220khmbz0y106zi9f0bhvis61rq1")))

(define-public crate-mc-varint-0.1 (crate (name "mc-varint") (vers "0.1.1") (hash "02dvys27qk2x3c2rqh172sd8xr1i8nyilpsa192my79p9q6xa1v4")))

