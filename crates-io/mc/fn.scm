(define-module (crates-io mc fn) #:use-module (crates-io))

(define-public crate-mcfn-0.0.1 (crate (name "mcfn") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "termstatus") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1y9rkzx4k1bc7qrjfslxx98ji7wz7ya7dydlc9nfyfwa6if56k2c") (yanked #t)))

