(define-module (crates-io mc _i) #:use-module (crates-io))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.4") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1mvgi89737n694lqprwc8lg8bi9f37zibv9x8jn071bmhn7al0lk")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.5") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "12nlsldlixiy5jqjzjb30fxax4bmc6fxcyddx3bgp5km34rf5rh0") (features (quote (("socket-handler"))))))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.6") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "00srjyb2n69k7jv1lc9rhg3nszv331y1vj4h2izlmgh6qwdpwsfi")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.7") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1w39m05s4bz54ha5alj7baf0y7m0nq59i48vhxbvq8mnx47kldyl")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.8") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1ay6wgm73bcgxfzfafwp532n1x7rmfjc57m26vfydk2vf0qxskkh")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.9") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1zhdlhfjvmj4chhbnqa56b0vrgpndmf7f717khgyd9iwgm4az5jd")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.10") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "12v08g7x3235cwgagrz1dg9wxd2r93fzyd5mkqyxjrflmdx06rkl")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.11") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "19gz5sz2g155g6wm4a701ay66n6gbshsgh0gnnw30y0pfwz1094h")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.12") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "hematite-nbt") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "14jlc0r7cl01q46rka54wdsicybnwrv05kwqfj34a7p600qb2y3n")))

(define-public crate-mc_io-0.1 (crate (name "mc_io") (vers "0.1.13") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "hematite-nbt") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "1cg6xfzkcmshlxh292cdrawm9dj02f5hdwz4zk7cinndw2im1maz")))

