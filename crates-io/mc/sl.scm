(define-module (crates-io mc sl) #:use-module (crates-io))

(define-public crate-mcslock-0.1 (crate (name "mcslock") (vers "0.1.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 2)))) (hash "0kv1rl48rcwqd39db2walzvx918xqx6yyxkrnd4jn889k1d27ps1") (features (quote (("yield") ("thread_local") ("barging")))) (v 2) (features2 (quote (("lock_api" "barging" "dep:lock_api")))) (rust-version "1.65.0")))

(define-public crate-mcslock-0.1 (crate (name "mcslock") (vers "0.1.1") (deps (list (crate-dep (name "lock_api") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 2)))) (hash "1fpb35856csm5wpml2iwkcr393mjlgig7qxm45gnbpi715q1hic0") (features (quote (("yield") ("thread_local") ("barging")))) (v 2) (features2 (quote (("lock_api" "barging" "dep:lock_api")))) (rust-version "1.65.0")))

(define-public crate-mcslock-0.1 (crate (name "mcslock") (vers "0.1.2") (deps (list (crate-dep (name "lock_api") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 2)))) (hash "0spafvkf33byb331wy9mlamhkiqfxnfp28sn1iqih1nyxyvzmssd") (features (quote (("yield") ("thread_local") ("barging")))) (v 2) (features2 (quote (("lock_api" "barging" "dep:lock_api")))) (rust-version "1.65.0")))

(define-public crate-mcslock-0.2 (crate (name "mcslock") (vers "0.2.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 2)))) (hash "1r957nrq1wvwz69vl4zdpvwfshsbp1plyab0bgw9zwmj40vjx4rv") (features (quote (("yield") ("thread_local") ("barging")))) (v 2) (features2 (quote (("lock_api" "barging" "dep:lock_api")))) (rust-version "1.65.0")))

