(define-module (crates-io mc pa) #:use-module (crates-io))

(define-public crate-mcpat-0.0.1 (crate (name "mcpat") (vers "0.0.1") (hash "0zc8h76pcnpd0pwqzsvc1rcmcp3kg9s93037fvngpar1brcr6nfw")))

(define-public crate-mcpat-0.1 (crate (name "mcpat") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0518l6k0z7j4m1qq696rl7kadf4hzjxx8hxy84ws1havx8yc2v67")))

(define-public crate-mcpat-0.1 (crate (name "mcpat") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0123q4axcvnvq2mvy1mylrg7slr8325qygb0bhvhbbxg2y13adrf")))

(define-public crate-mcpat-0.1 (crate (name "mcpat") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0wx5bzgw4hnnqiv8xckrdxx4320g618ygws7cff208mxybs3z52n")))

(define-public crate-mcpat-0.1 (crate (name "mcpat") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0xzm8dh0pn86iym015kmgql62l0hifhdsxhzb0xihagcbnvf3nl1")))

(define-public crate-mcpat-0.1 (crate (name "mcpat") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "04n0wbkq358f2gf594a2vncyvl4j06ji98ldz3wjqyqj29qrfyr0")))

(define-public crate-mcpat-0.1 (crate (name "mcpat") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "18rrwp752cmalkx7143021vakliphlcr4kwqrm4nnhlnyzr8lxgc")))

(define-public crate-mcpat-0.1 (crate (name "mcpat") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "1k9s8d1h2ghp3mqnbqfg4y60sqb1w04rk7cm5vbqa6bf2qrx7b4g")))

(define-public crate-mcpat-0.2 (crate (name "mcpat") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0mzcidfax2cp16lzwj5mrg5q0g9za1rmfi0p6j5c376fh40mlm8h")))

(define-public crate-mcpat-0.3 (crate (name "mcpat") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "106p7qahimkm4qpjwzfqv843ilfm0cb939npaq1ami28126yj1x2") (features (quote (("cache" "mcpat-sys/cache"))))))

(define-public crate-mcpat-0.4 (crate (name "mcpat") (vers "0.4.0") (deps (list (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "021vbprgmp5ba1rwkrjjz54kk4pgjxbjhhwr8n9d48zmpbq666l2") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.5 (crate (name "mcpat") (vers "0.5.0") (deps (list (crate-dep (name "fixture") (req "*") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0jq2i31didcf63vdrkpbm12yrjl3s0vcn79f0ndrnv9k4p1mvjsl") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.6 (crate (name "mcpat") (vers "0.6.0") (deps (list (crate-dep (name "fixture") (req "*") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0l94g5qymj2vq8jhqiq6kp2d8798cj1ylh32c28n5pjpcqx7r1j4") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.7 (crate (name "mcpat") (vers "0.7.0") (deps (list (crate-dep (name "fixture") (req "*") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "1i08i8lmv7rcjyh4vilfrz8xycbi2587aj8a0y82fwia4xvm6fgr") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8 (crate (name "mcpat") (vers "0.8.0") (deps (list (crate-dep (name "fixture") (req "*") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "1ra80sic0blaxncg0zzvynx1py0p448vjnvkrhl1g8dmjf69g1vm") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8 (crate (name "mcpat") (vers "0.8.1") (deps (list (crate-dep (name "fixture") (req "*") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "00vx8cnasbqnbbm6vcv89ccfp74xn20jd9n1yjx10hbykib3ppzv") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8 (crate (name "mcpat") (vers "0.8.2") (deps (list (crate-dep (name "fixture") (req "*") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "0gl6f32ha7x1763sx2lf6qam3j4y34980645hgzab233ypz81qrq") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8 (crate (name "mcpat") (vers "0.8.3") (deps (list (crate-dep (name "fixture") (req "*") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "*") (default-features #t) (kind 0)))) (hash "08g7rivfrw0i0qp0kj0vvzy3bddd9z8as8lzplw0hs3v2sx8gxd4") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.9 (crate (name "mcpat") (vers "0.9.0") (deps (list (crate-dep (name "fixture") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hiredis") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mcpat-sys") (req "^0.8") (default-features #t) (kind 0)))) (hash "11cbnq5j2lqj3mpb7k1222idc4aiy9izkzvf6wnw75swiv00a8y2") (features (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-sys-0.0.1 (crate (name "mcpat-sys") (vers "0.0.1") (hash "0p1izdl3k3v3rkn53i3bbqinnkidvs40ib0ivggd1acm7ldi61v1")))

(define-public crate-mcpat-sys-0.1 (crate (name "mcpat-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1yzdffkqfqlczv0nky5fmfqz3xrrpadycjrd5msj7r67cq7vyfbs")))

(define-public crate-mcpat-sys-0.2 (crate (name "mcpat-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "00lhjrmakyigd10yny1dd71dz261xrvmclck2qsjzg9ddhfdmvhq")))

(define-public crate-mcpat-sys-0.3 (crate (name "mcpat-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1jc3dzg0fhhk6r2nhl48av1pjxh6limrpl6hjbj8jzb8pml6lb9v")))

(define-public crate-mcpat-sys-0.3 (crate (name "mcpat-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1pk6pbxjwlyhm9r324yhzmn99l5751gj57dfdnmppbp3iql3ymwj")))

(define-public crate-mcpat-sys-0.4 (crate (name "mcpat-sys") (vers "0.4.0") (deps (list (crate-dep (name "hiredis-sys") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "13zm47fnn9y7dkx7bklpnl228qk2mc5ap7pb9dmych5lj976bzmi") (features (quote (("cache" "hiredis-sys"))))))

(define-public crate-mcpat-sys-0.4 (crate (name "mcpat-sys") (vers "0.4.1") (deps (list (crate-dep (name "hiredis-sys") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "09b5hfjc0429qm71a0ijx866426nr4ip3sfyyijz44sb59dcywnd") (features (quote (("cache" "hiredis-sys"))))))

(define-public crate-mcpat-sys-0.5 (crate (name "mcpat-sys") (vers "0.5.0") (deps (list (crate-dep (name "hiredis-sys") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "13ng40f0dj0pw8g75kfbd6l0r11z40pvw9a3a2p64dqh4qfdsp61") (features (quote (("caching" "hiredis-sys"))))))

(define-public crate-mcpat-sys-0.6 (crate (name "mcpat-sys") (vers "0.6.0") (deps (list (crate-dep (name "hiredis-sys") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "09xj4wijjyrpg0x4dkajrvcxhf1211nlsgpdh3awcqdd2fzb4fa4") (features (quote (("caching" "hiredis-sys"))))))

(define-public crate-mcpat-sys-0.7 (crate (name "mcpat-sys") (vers "0.7.0") (deps (list (crate-dep (name "hiredis-sys") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "11xscs31an7w8iddgp3gpkkykvy9g7z3jpl7mjm235mxh9pxdp9l") (features (quote (("caching" "hiredis-sys"))))))

(define-public crate-mcpat-sys-0.7 (crate (name "mcpat-sys") (vers "0.7.1") (deps (list (crate-dep (name "hiredis-sys") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0csd19q4alvg09r830rbr9lnxzn781ix0bz32dh4ms1xy19ic448") (features (quote (("caching" "hiredis-sys"))))))

(define-public crate-mcpat-sys-0.8 (crate (name "mcpat-sys") (vers "0.8.0") (deps (list (crate-dep (name "hiredis-sys") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "048mmbzjv3l6dzchsnaqixjzv5pq1jwxbl2f96rbqymbq4rbq5ll") (features (quote (("caching" "hiredis-sys"))))))

