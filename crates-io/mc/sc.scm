(define-module (crates-io mc sc) #:use-module (crates-io))

(define-public crate-mcschem-0.1 (crate (name "mcschem") (vers "0.1.0") (deps (list (crate-dep (name "quartz_nbt") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "10f71n6rvh0zkxmqav6karmm12cyn6nw5vmzpp6ll8h6yns58dzw") (rust-version "1.71.0")))

(define-public crate-mcschem-0.1 (crate (name "mcschem") (vers "0.1.1") (deps (list (crate-dep (name "quartz_nbt") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1ryv86acm3f3xxxafxbpw61i02is4bvgyfzx2fw9619s8jsgr8s0") (rust-version "1.71.0")))

