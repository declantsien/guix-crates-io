(define-module (crates-io mc rc) #:use-module (crates-io))

(define-public crate-mcrcon-0.1 (crate (name "mcrcon") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0zkr6j3i7cnk87qkasdy0k00pav8066pyz1c4xfpq1d9rhvgkl49")))

