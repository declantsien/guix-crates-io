(define-module (crates-io mc ub) #:use-module (crates-io))

(define-public crate-mcube-0.0.1 (crate (name "mcube") (vers "0.0.1") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1a9whypf15hvf5pf7z60wkm5wmmvdgniw6ma1nm2c4hrqsdfvdk3")))

