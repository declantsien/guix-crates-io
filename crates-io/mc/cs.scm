(define-module (crates-io mc cs) #:use-module (crates-io))

(define-public crate-mccs-0.0.1 (crate (name "mccs") (vers "0.0.1") (deps (list (crate-dep (name "void") (req "^1.0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1bzwpg9dki8x1np6kxgckybgh5q6p065dr42hqa8cmpwwb062wss") (features (quote (("default" "void"))))))

(define-public crate-mccs-0.0.2 (crate (name "mccs") (vers "0.0.2") (deps (list (crate-dep (name "void") (req "^1.0.2") (optional #t) (default-features #t) (kind 0)))) (hash "08z96mq3k6ja4kri0z6987wv260j9m7jk0wzfda48kqr6k1wpp03") (features (quote (("default" "void"))))))

(define-public crate-mccs-0.1 (crate (name "mccs") (vers "0.1.0") (deps (list (crate-dep (name "void") (req "^1.0.2") (optional #t) (default-features #t) (kind 0)))) (hash "08l41y7bsj2h1mln21d6xml7lrkyk9wlc6jmskh4348plinnqdkl") (features (quote (("default" "void"))))))

(define-public crate-mccs-0.1 (crate (name "mccs") (vers "0.1.3") (deps (list (crate-dep (name "void") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1kc8345bgacw5bl9hrbcxwz4z4m1vaf6n2k6icaysbylvsrxd430") (features (quote (("default" "void"))))))

(define-public crate-mccs-0.2 (crate (name "mccs") (vers "0.2.0") (hash "0q1k6bwpzmni0aimfbxzhk752g4chmgnn1wa57wdb9y9jx9anqra")))

(define-public crate-mccs-caps-0.0.1 (crate (name "mccs-caps") (vers "0.0.1") (deps (list (crate-dep (name "mccs") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)))) (hash "13yrb2by04had1czg1w5sclp0zn1snhrw5aplzr6fnf0fi02p6g1")))

(define-public crate-mccs-caps-0.0.2 (crate (name "mccs-caps") (vers "0.0.2") (deps (list (crate-dep (name "mccs") (req "~0.0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)))) (hash "1n9p3llb2pbck8hzw4zb48yj4la03h5317m167i6r81q32valvik")))

(define-public crate-mccs-caps-0.1 (crate (name "mccs-caps") (vers "0.1.0") (deps (list (crate-dep (name "mccs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)))) (hash "1kp3rcngwwl39lhk03hshx9qqyinywqpwyc12wzkzidf8l9nnxnr")))

(define-public crate-mccs-caps-0.1 (crate (name "mccs-caps") (vers "0.1.3") (deps (list (crate-dep (name "mccs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3") (default-features #t) (kind 0)))) (hash "1560krp2swrjd1lp5a3pzbzd0cccp0mbwxm2rxlpkc1v3b863fcf") (features (quote (("test-nom-errors" "nom/verbose-errors"))))))

(define-public crate-mccs-caps-0.2 (crate (name "mccs-caps") (vers "0.2.0") (deps (list (crate-dep (name "mccs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)))) (hash "124jw2fcdv25zc5c2sp7b4jwqp166iq1bczblz310pgipsfrqwj7")))

(define-public crate-mccs-db-0.0.1 (crate (name "mccs-db") (vers "0.0.1") (deps (list (crate-dep (name "mccs") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "mccs-caps") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1n56wh2371j6mw5fqwcaysysfx9bmnwc4vlpls8zlbx62z59hyd1")))

(define-public crate-mccs-db-0.0.2 (crate (name "mccs-db") (vers "0.0.2") (deps (list (crate-dep (name "mccs") (req "~0.0.1") (default-features #t) (kind 0)) (crate-dep (name "mccs-caps") (req "~0.0.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1rvnrjxl3xa4a2a9ndclv72i5jhyk93s3szs3f9qjgxahlamw7qs")))

(define-public crate-mccs-db-0.1 (crate (name "mccs-db") (vers "0.1.0") (deps (list (crate-dep (name "mccs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mccs-caps") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0s6dpvhcdccl7d8kapxq9a1835n41c4h2c6ks8gy6f6y8zx7q32p")))

(define-public crate-mccs-db-0.1 (crate (name "mccs-db") (vers "0.1.1") (deps (list (crate-dep (name "mccs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mccs-caps") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0ipwwgd1yhqhvj3l26qvlirx05vxqfhnkkigh67bnpmjcidcnd3i")))

(define-public crate-mccs-db-0.1 (crate (name "mccs-db") (vers "0.1.2") (deps (list (crate-dep (name "mccs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mccs-caps") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1l16h39ww750d3mlzk1q6fqmqshhr6vgms31qh42j7p1w6xnywlr")))

(define-public crate-mccs-db-0.1 (crate (name "mccs-db") (vers "0.1.3") (deps (list (crate-dep (name "mccs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "mccs-caps") (req "^0.1") (default-features #t) (kind 2)))) (hash "0f4xyilm4szclvh4vzqx85qvqn0hvjxwpf61xj5r38d137zainiw") (features (quote (("test-nom-errors" "nom/verbose-errors"))))))

(define-public crate-mccs-db-0.2 (crate (name "mccs-db") (vers "0.2.0") (deps (list (crate-dep (name "mccs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "mccs-caps") (req "^0.2") (default-features #t) (kind 2)))) (hash "1ibxb0mlg96c9mpnz5adsx58alx1sgn4bfwyx20ys9hfw597jh1z")))

