(define-module (crates-io mc ua) #:use-module (crates-io))

(define-public crate-mcuat-0.1 (crate (name "mcuat") (vers "0.1.0") (deps (list (crate-dep (name "serialport") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "1lchlajfzjnnb0ndr04q166f47zbyf1sycqdx7xc28xk1dm826z6")))

(define-public crate-mcuat-0.1 (crate (name "mcuat") (vers "0.1.1") (deps (list (crate-dep (name "serialport") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "1l5r3sh2iyfs9l8cfp0049nximdhxyj3fijq1r498pcbpwgv0w2f")))

(define-public crate-mcuat-0.1 (crate (name "mcuat") (vers "0.1.2") (deps (list (crate-dep (name "serial-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "1rz1y1dhb7yi10c30vcq5vmznx1lz70sv0giy51lh96h0sfp65bh")))

