(define-module (crates-io mc pr) #:use-module (crates-io))

(define-public crate-mcproto-rs-0.2 (crate (name "mcproto-rs") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.12.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "flate2") (req "^1.0.17") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive" "alloc"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "1avpjxx0hipd16n1hs1yqjcnpnaa220pdd62j0pi8y416y977nlm") (features (quote (("v1_16_3") ("v1_15_2") ("std" "rand") ("gat") ("default" "std" "bench" "v1_15_2" "v1_16_3") ("bench"))))))

