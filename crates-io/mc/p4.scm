(define-module (crates-io mc p4) #:use-module (crates-io))

(define-public crate-mcp4725-0.1 (crate (name "mcp4725") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m-rt") (req "^0.6.7") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "stm32f1xx-hal") (req "^0.3.0") (features (quote ("rt" "stm32f103"))) (default-features #t) (kind 2)))) (hash "0x0rc203954yg370cyrkmhq2yj6j2r4dw4zi40nnrk3ykskxzz9r")))

(define-public crate-mcp4725-0.1 (crate (name "mcp4725") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "18b5xkvfs8akpgkdbxqw4gm3wnzzjlxa9rsqarkhnsxnc7k9dsf3")))

(define-public crate-mcp4725-0.1 (crate (name "mcp4725") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1vic8l9j8ab0v37bq4nkpkvjsn6bbn15fr4ald53c8hf6yh9pskg")))

(define-public crate-mcp4725-0.2 (crate (name "mcp4725") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0qmml5f7bpgvxlw07m3s68kbl7sqnjp0bzzhlf23srzgw9wqy18p")))

(define-public crate-mcp4725-0.2 (crate (name "mcp4725") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1bvalkgiy34pjl28apcsb635j2db7n5kbflayq36m4cr4gry0z71")))

(define-public crate-mcp4725-0.2 (crate (name "mcp4725") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0w5knk17lnmcvjnf4cvqd9lmvadwa62k4grdplwwwsl65qckv6zw")))

(define-public crate-mcp4725-0.3 (crate (name "mcp4725") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "131bxip2jwkfckz1x0xsdl4nb7vwb70cyizr80m46cx30c5zs961")))

(define-public crate-mcp4725-0.3 (crate (name "mcp4725") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "1rb4yvbp0g8x8g3l5j6agrf7h3vszsyb3hhila1lpm4v8lqpjj10")))

(define-public crate-mcp4725-0.4 (crate (name "mcp4725") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "03b0wsssd5mykhj7dc318cvwnc112i7jfw4961ifxrynp29cbqb6")))

(define-public crate-mcp4725-0.4 (crate (name "mcp4725") (vers "0.4.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1zqvkmdfk2yswiwdia8hlgh7qmjaq8411x4a5xvir6rdcm5fgycb")))

(define-public crate-mcp4725-0.4 (crate (name "mcp4725") (vers "0.4.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1d32inhk95n23s192ch9vn3x5llc6fkfyshhaxvf9f4p8fhrkrfw")))

(define-public crate-mcp4725-async-0.1 (crate (name "mcp4725-async") (vers "0.1.0") (deps (list (crate-dep (name "defmt-03") (req "^0.3") (optional #t) (default-features #t) (kind 0) (package "defmt")) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.3.0") (default-features #t) (kind 2)))) (hash "1w6ni7sfz837yi4jsf2imxxhrim6v27mbl34msaja20a66xjfdp4") (v 2) (features2 (quote (("defmt-03" "dep:defmt-03"))))))

(define-public crate-mcp4725-rs-0.1 (crate (name "mcp4725-rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "1i502rqrlwrwmfrsymzgq0zz2w59rgmfxivx6fmxqkqpsh4mv8m4")))

(define-public crate-mcp4725-rs-0.1 (crate (name "mcp4725-rs") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0wqvr4wfi70dx7d67h7ng27bkam6gldy3f5rik7d995ny1az7k43")))

(define-public crate-mcp4728-0.1 (crate (name "mcp4728") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "num_enum") (req "^0.5.7") (kind 0)))) (hash "1x2x2vflyihp0vib4c55vljsgs88j2xkp73f6lf8fm76rbnmyini")))

(define-public crate-mcp47x6-0.1 (crate (name "mcp47x6") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)))) (hash "0xky4f0jmvwxliy68fr97z0mq1awbq830bggpm7qrydmv8wb803j")))

(define-public crate-mcp49xx-0.1 (crate (name "mcp49xx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "184fc03afs9xi0mr9xrxsnzjw1cd9ab448bh6bxipgij56wsg76r")))

(define-public crate-mcp49xx-0.2 (crate (name "mcp49xx") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1jrn9qk1b7igh536b6s7ixvkpnjp5r0sndai3hjlja3lf1kvcngn")))

(define-public crate-mcp49xx-0.3 (crate (name "mcp49xx") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "11pmq32w540aysnm8a46xylpxk8c0sj4v8s89vxn1c1lhvvwq33l")))

(define-public crate-mcp4x-0.1 (crate (name "mcp4x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0yia9ics4zklacm5c7kfmnrgrjw95a99vmzkh0bz4ssm9bs4bz4m")))

(define-public crate-mcp4x-0.2 (crate (name "mcp4x") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1m03b4v5vw2nkcmd5q7wdvidrbqg8w9mmw2cw8x8hxnd7ci26z9b")))

