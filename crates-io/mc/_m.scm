(define-module (crates-io mc _m) #:use-module (crates-io))

(define-public crate-mc_map2png-0.1 (crate (name "mc_map2png") (vers "0.1.0") (deps (list (crate-dep (name "fastnbt") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gq8zcjawp7axwcpilzig0x0piz4q396vp2x2fjrz0jsqqb09m7z")))

