(define-module (crates-io mc mc) #:use-module (crates-io))

(define-public crate-mcmc-0.0.1 (crate (name "mcmc") (vers "0.0.1") (hash "1c96glg21fbpj0h4x77f6ckkk68k95m6jcwdbfx4bzr868fbm93k")))

(define-public crate-mcmc-0.1 (crate (name "mcmc") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "arima") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "19bg2d3mrcla97i0j7y78g59cxjm673vgml0xcw1hg9ahacp9qxz")))

