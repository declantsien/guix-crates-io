(define-module (crates-io mc p9) #:use-module (crates-io))

(define-public crate-mcp9600-0.1 (crate (name "mcp9600") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1w9s4pz58n257d3fc7r67bik23cghr3f4qj503cka26hwbjxcn37")))

(define-public crate-mcp9600-0.1 (crate (name "mcp9600") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1acbgwv1v2ba6z44xpggpcs1nbf90lr154vqs7yl9d7chwvis5d9")))

(define-public crate-mcp9808-0.1 (crate (name "mcp9808") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "024fb9rx4zhgw3nbdm4ymmczl10chy7d4dmdknkk4ipgbs4z2i6h")))

(define-public crate-mcp9808-0.2 (crate (name "mcp9808") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "cast") (req "^0.3.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "1npnm4liffcldrqg71brw5k8gyihm17swrc24j1fm54cqz8k9kg5") (features (quote (("with_floating_point") ("no_floating_point") ("default" "with_floating_point"))))))

(define-public crate-mcp9808-0.3 (crate (name "mcp9808") (vers "0.3.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "cast") (req "^0.3.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "1l8xdsfmcrhqkggc3y42i3lpgijyb384w8kvzdlwkwkjnmr80af8") (features (quote (("with_floating_point") ("no_floating_point") ("default" "with_floating_point"))))))

(define-public crate-mcp9808-0.4 (crate (name "mcp9808") (vers "0.4.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "cast") (req "^0.3.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "13q684pjvas91bsd6x3dn1h02wymnzv8qw7h60b7ni052pdb41c9") (features (quote (("with_floating_point") ("no_floating_point") ("default" "with_floating_point"))))))

