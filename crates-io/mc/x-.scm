(define-module (crates-io mc x-) #:use-module (crates-io))

(define-public crate-mcx-pac-0.0.1 (crate (name "mcx-pac") (vers "0.0.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "ral-registers") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1prk8a1lz7d350kb008gjmgx2psjxvb7kfvqfrnl28pdj7p4ipv6") (features (quote (("rt" "cortex-m-rt/device") ("mcxn947") ("mcxn946") ("mcxn547") ("mcxn546") ("default" "rt"))))))

