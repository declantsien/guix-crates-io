(define-module (crates-io mc _a) #:use-module (crates-io))

(define-public crate-mc_auth-0.1 (crate (name "mc_auth") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0d32lg0zag8na4d10hbvs28mza4dzx37x3da04y5zjk6wchn9alw")))

