(define-module (crates-io mc an) #:use-module (crates-io))

(define-public crate-mcan-0.1 (crate (name "mcan") (vers "0.1.0") (deps (list (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g2xvkhrr7ld21ni7zj36vwiy35s5p7lbdakccrb0rd2373f8w1i") (yanked #t)))

(define-public crate-mcan-0.2 (crate (name "mcan") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "embedded-can") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "mcan-core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kxgrwqpw3qzp72ih0ndjvh6mqk6iqfy5bf5l2jyawzbq63nplny")))

(define-public crate-mcan-0.3 (crate (name "mcan") (vers "0.3.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "embedded-can") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "mcan-core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "0brgipwaaidaxh9qanj3vimsjbk2hw6xqa1qx2zh9f32ym4avc8x")))

(define-public crate-mcan-0.4 (crate (name "mcan") (vers "0.4.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "embedded-can") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "mcan-core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "0a5ifvxy2gyxqv45c47s7mxfz80v70cyglkgk07z68xlwi1g80h3")))

(define-public crate-mcan-0.5 (crate (name "mcan") (vers "0.5.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "embedded-can") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "mcan-core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "0innmk8l3v8j5364rkhywq0mrfxgrvvgr18hrlv4kb1xp92lf74k")))

(define-public crate-mcan-core-0.1 (crate (name "mcan-core") (vers "0.1.0") (deps (list (crate-dep (name "fugit") (req "^0.3") (default-features #t) (kind 0)))) (hash "1gzl5256kr06prhznsmn24pxzpb61niyclqy4wh0n774wvanw8cs")))

(define-public crate-mcan-core-0.1 (crate (name "mcan-core") (vers "0.1.1") (deps (list (crate-dep (name "fugit") (req "^0.3") (default-features #t) (kind 0)))) (hash "1p4vzivcwkn8h1ny5am7bgvlzlh1fhijhj5d7zg5b8smphyxvbbh")))

(define-public crate-mcan-core-0.2 (crate (name "mcan-core") (vers "0.2.0") (deps (list (crate-dep (name "fugit") (req "^0.3") (default-features #t) (kind 0)))) (hash "05nbmah1sknkhrjixxk5b7q06985zhyf85fgah0bnkgyprh49mdm")))

(define-public crate-mcan-core-0.2 (crate (name "mcan-core") (vers "0.2.1") (deps (list (crate-dep (name "fugit") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ac121nblpnjgqddcgy3vd6gv5c71rc294myqqc128p2c2wvq571")))

(define-public crate-mcan-core-0.2 (crate (name "mcan-core") (vers "0.2.2") (deps (list (crate-dep (name "fugit") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jppd4p8fqy90vq7fhrvywg6r4ff7h83fvc5rijj97i6ym7ywgcb")))

