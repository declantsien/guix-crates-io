(define-module (crates-io mc pe) #:use-module (crates-io))

(define-public crate-mcpe_motd-0.1 (crate (name "mcpe_motd") (vers "0.1.0") (hash "0h3aaqapb1010wdmji859zcgziyrxzsz1vn5m61im72207kjx5cj")))

(define-public crate-mcpe_motd-1 (crate (name "mcpe_motd") (vers "1.0.0") (hash "0n4jirhai4gma5dfsx7jmkl7495spig2hvd0gcp45w0rlpdlp549")))

(define-public crate-mcpe_query-0.1 (crate (name "mcpe_query") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1dlvpgrqmrvwq8pcjldihb17jk9s822d3y9mfg16pl0w1dq97xz5")))

(define-public crate-mcpe_query-0.1 (crate (name "mcpe_query") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "05lc3aqpakxrgw0dx9db6zqrmfxf7cav7q3zw6dsyl8mq7ngqg3h")))

(define-public crate-mcpe_query-0.1 (crate (name "mcpe_query") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dd4qqlvfz17gdxhvfygz8yqwdiq1sclaabsgr8l4g2zd8a4mzcq")))

