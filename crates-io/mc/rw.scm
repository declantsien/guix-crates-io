(define-module (crates-io mc rw) #:use-module (crates-io))

(define-public crate-mcrw-0.4 (crate (name "mcrw") (vers "0.4.0") (deps (list (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1iijvmlpi5qwvwf7xwjswyz42a3l610h3q7dbgdxsfx6s5fzf74r")))

