(define-module (crates-io mc _c) #:use-module (crates-io))

(define-public crate-mc_chat-0.1 (crate (name "mc_chat") (vers "0.1.0") (deps (list (crate-dep (name "erased-serde") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fy4xpdp8y7sqvjcl7l33q3fghw12ad3y94bzl3c19c99wxb91ih") (features (quote (("use-serde" "serde" "erased-serde") ("default" "use-serde"))))))

(define-public crate-mc_chat-0.3 (crate (name "mc_chat") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0bxvvkxdk9srjkck74d99pwkvmq37dsigr8a1s0p9rd532h506fw")))

