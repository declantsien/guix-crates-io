(define-module (crates-io mc go) #:use-module (crates-io))

(define-public crate-mcgooey-0.1 (crate (name "mcgooey") (vers "0.1.0") (deps (list (crate-dep (name "auto_impl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0635amb10kb68gaw72d5nlj8pszvkmx8h10hpi9lbq69xhib3zkq") (features (quote (("debug_draw")))) (yanked #t)))

(define-public crate-mcgooey-0.1 (crate (name "mcgooey") (vers "0.1.1") (deps (list (crate-dep (name "auto_impl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "002k75yil8agg1kpzif7fngi1ig5fyv3vr0iz55mc267ayhr2qz7") (features (quote (("debug_draw"))))))

(define-public crate-mcgooey-0.1 (crate (name "mcgooey") (vers "0.1.2") (deps (list (crate-dep (name "auto_impl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "=0.3.13") (default-features #t) (kind 0)))) (hash "1v68cj7k3xsjvzf948zsy5jzi7g38f9gdc1wy8l14mbnxbi6bvbv") (features (quote (("debug_draw"))))))

(define-public crate-mcgooey-0.1 (crate (name "mcgooey") (vers "0.1.3") (deps (list (crate-dep (name "auto_impl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "=0.3.13") (default-features #t) (kind 0)))) (hash "1k34c4kq9wz6gawzby1wyjhliklv9nbfhrs3yj6nywk2i3g4pkn6") (features (quote (("debug_draw"))))))

(define-public crate-mcgooey-0.1 (crate (name "mcgooey") (vers "0.1.4") (deps (list (crate-dep (name "auto_impl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "=0.3.13") (default-features #t) (kind 0)))) (hash "0jmk3ipmxc8cb0baimyfm6dwqn9z1iffz8pl9pkh3y7vy19swpa0") (features (quote (("debug_draw"))))))

