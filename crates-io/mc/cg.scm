(define-module (crates-io mc cg) #:use-module (crates-io))

(define-public crate-mccga-0.1 (crate (name "mccga") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0p4p3zljqif24qicw2fbb9z1gc1h76kmayvrffphz77c5f1whdaq")))

(define-public crate-mccga-0.1 (crate (name "mccga") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ryz81m6p8q1w15ag33mcv972x0af1q12vdan40qb91n01a8nsaz")))

(define-public crate-mccga-0.1 (crate (name "mccga") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0k0fxbs5zm3rxh41ml91zc1d0bb51bc92irlj8mnlzqs0s5x7ng7")))

(define-public crate-mccga-0.1 (crate (name "mccga") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ing5vfsvd06lr5hsaz6lykg3sk17d1sxi37888ag5z3il5r35nk")))

(define-public crate-mccga-0.1 (crate (name "mccga") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "13kh6rz0npbxbhfk4dcasb1iqfi99abch12q7c797zi191v8hwwa")))

(define-public crate-mccga-0.1 (crate (name "mccga") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0mlwb4xmbj8n1xa2x8x4m1wsy5nkrh6wg3dhsnavb3jwzy0np9yc")))

