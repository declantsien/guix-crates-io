(define-module (crates-io mc -n) #:use-module (crates-io))

(define-public crate-mc-network-data-types-0.1 (crate (name "mc-network-data-types") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "04zb7d6b69axwa69sza1gi69y6gjsh132mpakjkwa7cj2dhag5ky")))

(define-public crate-mc-network-data-types-0.1 (crate (name "mc-network-data-types") (vers "0.1.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1r0fv2x64dcc47agi8ijqvjc9qznss8ilv4hrafyammh6s00rwy7")))

(define-public crate-mc-network-data-types-0.1 (crate (name "mc-network-data-types") (vers "0.1.2") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "15fxgn79cxmckhkv3p3ddqwf4xaskcicxnbrmi1ayq7f62d6labv")))

(define-public crate-mc-network-data-types-0.1 (crate (name "mc-network-data-types") (vers "0.1.3") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1q6plv4qwmpg6m40yr0kcpbjaf92q6dw4ms9kxysg95rnp3qn42i")))

(define-public crate-mc-network-data-types-0.1 (crate (name "mc-network-data-types") (vers "0.1.4") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1zd1486bgy76h6qvih0q849vrwmhjipl8vj6i8jb50hxx8ibpm37")))

(define-public crate-mc-network-io-0.1 (crate (name "mc-network-io") (vers "0.1.4") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1yxa3zx1v4gvlfpp9s7i4km1kc93phrd81y3ls8k86czwrn56yr0")))

