(define-module (crates-io mc uv) #:use-module (crates-io))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.0") (hash "1niabalgyh9gx4vxzbnakmzy6v72h69x9y0jb1xlm6fcp7503q46")))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.2") (deps (list (crate-dep (name "visa-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0wbz5i41dkr3x1dlm3a9vkkzc7wknagvca8smlb0r0r0z1dfrcy6")))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.3") (deps (list (crate-dep (name "visa-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0x5g8j3jvchfcrhjwscs9gh1w3qqna5d6i1r2ybbvz830izqv3jd") (features (quote (("lte"))))))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.4") (deps (list (crate-dep (name "visa-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0zqfh411dpakqzxmw13bfi7vzgnxkbx4jhlr1byxi9hjf8sqnph1") (features (quote (("lte"))))))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.5") (deps (list (crate-dep (name "visa-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "11k04c0rxv0clwqk1m35wmdmygnxkwz3i7p1d5pcbs2bm4s92i5g")))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.6") (deps (list (crate-dep (name "visa-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qmypgipdk5qlnynz7yvyxpc8rjmphf0rsmdj4lr6zyspgl0j3m9")))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.7") (deps (list (crate-dep (name "visa-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0sjarzh6w4nalpfdsxv7yzxzg60psfdjgpdj3jdzplv64gbzv542")))

(define-public crate-mcuvisa-0.1 (crate (name "mcuvisa") (vers "0.1.8") (deps (list (crate-dep (name "visa-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "17i40n53jgca1ffqzrwpr25rly8k56lqp6708gf4hd4qz2nsh3yp")))

