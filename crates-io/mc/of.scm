(define-module (crates-io mc of) #:use-module (crates-io))

(define-public crate-mcoffin-option-ext-0.1 (crate (name "mcoffin-option-ext") (vers "0.1.0") (hash "0j8b7xxvi61daq2m1r5rwnliwhffd5ca5szf3amckh4vmwa3cwcp") (features (quote (("result") ("default" "result"))))))

(define-public crate-mcoffin-option-ext-0.2 (crate (name "mcoffin-option-ext") (vers "0.2.0") (hash "0c6gi7sbbp3ybacm05jay9wr4k7xcdiqa5kp4mszymr7392pa1jv") (features (quote (("result") ("default" "result"))))))

(define-public crate-mcoffin-time-0.1 (crate (name "mcoffin-time") (vers "0.1.0") (hash "0srr4svk9w33a4gips5sjm9dzfd5cx6bbik7pz6gq4aq7257z058")))

(define-public crate-mcoffin-tuple-ext-0.1 (crate (name "mcoffin-tuple-ext") (vers "0.1.0") (hash "1rniz1rda6dfiw93xbfgb8dcd5cdsnr06mv8gjdwj98z88zwy2sg")))

