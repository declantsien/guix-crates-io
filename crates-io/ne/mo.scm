(define-module (crates-io ne mo) #:use-module (crates-io))

(define-public crate-nemo-0.0.1 (crate (name "nemo") (vers "0.0.1") (hash "1ipnf8ndkhqisgig40kh84blk4hpj5amlx04pgwih9h3f7i8j5ys")))

(define-public crate-nemo-0.1 (crate (name "nemo") (vers "0.1.0") (hash "1lfik0mnsbvjcl1lwz2r3v3kwzshbsl1cpbanby1mkclxq39m1w5")))

(define-public crate-nemo-0.1 (crate (name "nemo") (vers "0.1.1") (hash "011pqlbf0xp12p05z3n5fj711zpgbp52iy8hl1wfsxmxc8w70qs3")))

(define-public crate-nemo-0.2 (crate (name "nemo") (vers "0.2.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "09b6fa271r66jdy653l2b1dcam9z7g420fwdzq722z9lp24qc21f")))

(define-public crate-nemo-0.2 (crate (name "nemo") (vers "0.2.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.0.10") (default-features #t) (kind 2)))) (hash "16bnfl9x4ilka56rbmx7pvs646far0b7q6n0yps0c8qah86vsbf8")))

(define-public crate-nemo-extension-0.6 (crate (name "nemo-extension") (vers "0.6.1") (deps (list (crate-dep (name "gio-sys") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "gobject-sys") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "gtk-sys") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nemo-extension-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "083y1q937pgg9qzai9wkdxc97xqh56d33nnqkpy8srcp978scwpa")))

(define-public crate-nemo-extension-sys-0.5 (crate (name "nemo-extension-sys") (vers "0.5.0") (deps (list (crate-dep (name "gio-sys") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "gobject-sys") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "gtk-sys") (req "^0.9.2") (features (quote ("v3_18"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j5afw2fv315r1sfxl65175kkx3xmp4qvh0wq6h20w1fbqxq4zba")))

(define-public crate-nemo157-0.1 (crate (name "nemo157") (vers "0.1.0") (hash "1agkpy8m324q9vmxkszkj6k259jgxjnfvcrm611sdjzdbvqfp9ks")))

(define-public crate-nemo157-0.2 (crate (name "nemo157") (vers "0.2.0") (hash "1f3z5im264zvq1l4cvm0d3bk2asw3swvqqh95ddspr3z2l44szjz")))

(define-public crate-nemo157-0.2 (crate (name "nemo157") (vers "0.2.1") (hash "0p899idnzxk1f40ypjq7ibqpaqyfrax5z805wspm7c5ymqnh4rii") (yanked #t)))

(define-public crate-nemo157-0.3 (crate (name "nemo157") (vers "0.3.0") (hash "0q66yd45pjh1196r1hf0ycdrw756xk72agxwqkdkifcw5ndm0qs8")))

(define-public crate-nemo157-0.3 (crate (name "nemo157") (vers "0.3.1-pre") (hash "0zkscv95cbc3jspvp6r0y5hg2gwh75qx9h9ays99s5bgdidbq2pl")))

(define-public crate-nemo157-0.3 (crate (name "nemo157") (vers "0.3.1-pre.1") (hash "1j175vmyvp882ffw44rk1rwfrqbg97wdd2cdq2wdpklgcw36bw78")))

(define-public crate-nemo157-0.3 (crate (name "nemo157") (vers "0.3.1-pre.3") (hash "1zzdg4k5fwfiabki72z7zb4q1lz8v21hph23by5w3pafyil5mzza")))

(define-public crate-nemo157-test-0.1 (crate (name "nemo157-test") (vers "0.1.0") (deps (list (crate-dep (name "nemo157") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12q670hvn7bhjmk3a1wsjh5xh7bsr7i1i14r1cwgay6qd06mhs1i")))

