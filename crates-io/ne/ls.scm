(define-module (crates-io ne ls) #:use-module (crates-io))

(define-public crate-nelson137-scratch-0.1 (crate (name "nelson137-scratch") (vers "0.1.0") (deps (list (crate-dep (name "bevy_app") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1xgyq27g8ki9hgzva2g3fx85q902n2zz82pqqb8z7sna0hnk392z")))

(define-public crate-nelson137-scratch-0.1 (crate (name "nelson137-scratch") (vers "0.1.1") (deps (list (crate-dep (name "bevy_app") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "02y2gan6b1q2hx96rgxxxi2bpavmhzinfijv44qxcqzcbnak9wm5")))

