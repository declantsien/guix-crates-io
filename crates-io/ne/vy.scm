(define-module (crates-io ne vy) #:use-module (crates-io))

(define-public crate-nevy-0.0.1 (crate (name "nevy") (vers "0.0.1") (deps (list (crate-dep (name "bevy") (req "^0.12") (default-features #t) (kind 0)))) (hash "0hxk1r4m93d9jfrjfipy42a7zrs9w7qsfml2mmxsgdjj9zal7md6")))

