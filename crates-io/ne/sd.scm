(define-module (crates-io ne sd) #:use-module (crates-io))

(define-public crate-nesdie-0.1 (crate (name "nesdie") (vers "0.1.0") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.5") (optional #t) (kind 0)))) (hash "18v3rb09carvyza5axci2fs3ja7vg7j8mh3v0vll0976yfddv0zr") (features (quote (("panic-message") ("oom-handler") ("default" "wee_alloc"))))))

(define-public crate-nesdie-0.2 (crate (name "nesdie") (vers "0.2.0") (deps (list (crate-dep (name "near-primitives-core") (req "=0.4.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "near-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "near-vm-logic") (req "=4.0.0-pre.1") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4.5") (optional #t) (kind 0)))) (hash "175m1xyflgg8kdw0vd5k6kmavsciqkaq9cdzi7ahsmigi5ygnka1") (features (quote (("std") ("panic-message") ("oom-handler") ("default" "wee_alloc"))))))

(define-public crate-nesdie-store-0.2 (crate (name "nesdie-store") (vers "0.2.0") (deps (list (crate-dep (name "borsh") (req "^0.9") (kind 0)) (crate-dep (name "nesdie") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)))) (hash "1gggb3mi0iw5xgbmlg8smxz3k2fiwsx44873bc3ram773cknqrn4")))

