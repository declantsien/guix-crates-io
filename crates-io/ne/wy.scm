(define-module (crates-io ne wy) #:use-module (crates-io))

(define-public crate-newyears-1 (crate (name "newyears") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "figlet-rs") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "165yk4yn8w0vw4svgyvmjx247w903388vgxs1gdagjnbw7kpksr2")))

(define-public crate-newyears-1 (crate (name "newyears") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "figlet-rs") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "01a8sfb4y3cgl6rrj26gfp3zjh73rjbb8ycnvs9qds33ya15n4zs")))

(define-public crate-newyears-1 (crate (name "newyears") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "figlet-rs") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "17h309y8cv42wr2hzjyb935g93wxnj0r56ycmcfmwlgr3jfv6qmy")))

(define-public crate-newyears-1 (crate (name "newyears") (vers "1.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "figlet-rs") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "12m72d79nc4c4mnj8ym9xvhgi3nlci02a2gm105bf79r5vncz616")))

(define-public crate-newyears-1 (crate (name "newyears") (vers "1.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "figlet-rs") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0nngmf2f1pa7aidblsp8lc5rbqgx5rxvh11s3cnr5mbdr9jd77bg")))

