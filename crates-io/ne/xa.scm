(define-module (crates-io ne xa) #:use-module (crates-io))

(define-public crate-nexa-23 (crate (name "nexa") (vers "23.1.9") (hash "1qh2iy5idnjpb1sdrgzl6j1iqp8yxyllz6z2qa0crhf7dpgbv05i")))

(define-public crate-nexa-24 (crate (name "nexa") (vers "24.4.15") (hash "1s8ncg96i7ykl192ldr7rk5k4nrvbwmcwmrgpdwq4md2f19110k5")))

(define-public crate-nexara_text_engine-0.1 (crate (name "nexara_text_engine") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1g1qb2k6i95yqlv3m3gq5dvpg018nhlc9y7bzr2v8f6fxvjav8q5")))

