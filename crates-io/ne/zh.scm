(define-module (crates-io ne zh) #:use-module (crates-io))

(define-public crate-nezha-0.1 (crate (name "nezha") (vers "0.1.0") (hash "1ylyvq2ncp9839gzjp8z1w1cqmh45vfcl4yp8vlsmxh7kgylq0j9")))

(define-public crate-nezha-lottery-0.0.1 (crate (name "nezha-lottery") (vers "0.0.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "solana-program-test") (req "^1.9.3") (default-features #t) (kind 2)) (crate-dep (name "solana-sdk") (req "^1.9.3") (default-features #t) (kind 2)) (crate-dep (name "spl-token") (req "^3.2") (features (quote ("no-entrypoint"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0fv1wwglcj93p8csmawnqmx0pj5zl4kf1q82lsbwbqkwisakm9gr") (features (quote (("test-bpf") ("no-entrypoint"))))))

