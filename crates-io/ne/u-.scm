(define-module (crates-io ne u-) #:use-module (crates-io))

(define-public crate-neu-js-0.1 (crate (name "neu-js") (vers "0.1.0") (deps (list (crate-dep (name "deno_core") (req "^0.164.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qv2rnfzph601bawjh9mpsxg5586v0nrsacblgy1qv1q3viccirj")))

