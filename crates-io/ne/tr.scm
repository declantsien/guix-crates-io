(define-module (crates-io ne tr) #:use-module (crates-io))

(define-public crate-netr-0.1 (crate (name "netr") (vers "0.1.0") (hash "09np3vvbirknqrl9l53bc00l2x7bpqqhl3mwfc2vv21c6c7l8a17")))

(define-public crate-netr-0.1 (crate (name "netr") (vers "0.1.1") (hash "1j1l27c0kb4ykddkbwr36z5l0izn5g8y29nlb5r2v84y38d0fvyh")))

(define-public crate-netr-0.1 (crate (name "netr") (vers "0.1.3") (hash "154sphhwbb2hhz49g4l86dz48wqplf16j8xrchc83l816sha0j55")))

(define-public crate-netr-0.1 (crate (name "netr") (vers "0.1.4") (hash "0zx1v6768yqhc77c5j5lr71c1j9qiwrlpnixsi5asg0zsfqc5q8z")))

(define-public crate-netr-0.1 (crate (name "netr") (vers "0.1.5") (hash "0dz9fmdrymk9mg3a8cqk9agqm6s91qf963wc5xdc2k16frgq4viz")))

(define-public crate-netr-0.1 (crate (name "netr") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(target_os = \"linux\"))") (kind 0)))) (hash "0xrf4yrz2kcm36gyxj3jyngbpnmwxhnrlar5dr3p35k1220m6had")))

(define-public crate-netraffic-0.1 (crate (name "netraffic") (vers "0.1.0") (deps (list (crate-dep (name "pcap") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0pxhhfic2svxhglwsqpdvkkkffdfg7fv7nnabkzcg0i3iarx0isg")))

(define-public crate-netrange-0.1 (crate (name "netrange") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "cidr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libnetrangemerge") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1frpx6wpczh5y53wzhnydxn96agiizm89wn1skjqflqn4ypd0hwc")))

(define-public crate-netrange-0.2 (crate (name "netrange") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "cidr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libnetrangemerge") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1s0jbcl21mnqh94pmh6wpfgcxz36996f3dfd4jidxh39n3wcyk37")))

(define-public crate-netrange-0.3 (crate (name "netrange") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "cidr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libnetrangemerge") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0jk8c2iggkbwcnx8q3gppvn82rkb00d0jfi5m4kd0d4q1749j645")))

(define-public crate-netrange-0.4 (crate (name "netrange") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "cidr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libnetrangemerge") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1js20x45w4mxnsbr35phza8jz7j71d40nhjbxq5wchfrsws0a6p5")))

(define-public crate-netrange-0.5 (crate (name "netrange") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "cidr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "libnetrangemerge") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0nj8bjy6snayss5sjzdqi519l03c6xb27iah8g5zjyjxxb9rlpxv")))

(define-public crate-netrc-0.0.1 (crate (name "netrc") (vers "0.0.1") (hash "0fby7xjn187xqggi5lp7p160wv858mcdn91nwfaanv9xvp41qd36")))

(define-public crate-netrc-0.0.2 (crate (name "netrc") (vers "0.0.2") (hash "1hd1vrm21kyvqh4y108yiwkcaj8d3nmhixfgkh9m60r68ck50ibm")))

(define-public crate-netrc-0.3 (crate (name "netrc") (vers "0.3.0") (hash "0lsij4j92z0nibg6abw39phkjz22hqvd51gy77virsk3bm9syaib")))

(define-public crate-netrc-0.4 (crate (name "netrc") (vers "0.4.0") (hash "1x80w6i0jr1cwh2xnwlbcnb8794i2xfg99f34vx8v8275v6skaa2")))

(define-public crate-netrc-0.4 (crate (name "netrc") (vers "0.4.1") (hash "1q0nf7cwgz9a846g3g8sh2b1qkids8gyrn3yf0ka5z1lchr1paf9")))

(define-public crate-netrc-rs-0.1 (crate (name "netrc-rs") (vers "0.1.0") (hash "1fnq35hn8702iglq5w5pi00flqi0i4blqflh791jc3398iha5q66")))

(define-public crate-netrc-rs-0.1 (crate (name "netrc-rs") (vers "0.1.1") (hash "0qgr2zj6l58wrqfzv80gj3q4n2k5bk7jw6ni6m2nnw50na7rq07g")))

(define-public crate-netrc-rs-0.1 (crate (name "netrc-rs") (vers "0.1.2") (hash "0dmq37j4a9d9d4vcfpm3s4gcsrpd0k87nji38s15wy4cpkxp0aga")))

(define-public crate-netrc_util-0.1 (crate (name "netrc_util") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1qnasd69y2hkrsn17palki7k9xk2wwpxxg4xl0zn7qb671295ljx")))

(define-public crate-netres-0.1 (crate (name "netres") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.1.1") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "system_shutdown") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0fx97pfrdn1zb3xz70zaksz95x5rjdrxpvspk6xdf0dyzvkh8llv")))

(define-public crate-netres-0.1 (crate (name "netres") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "system_shutdown") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "17icky1j9lizf8k5fg7vx1c9mzlrn90l8sbmzn0yp1airgcbfy49")))

(define-public crate-netres-0.1 (crate (name "netres") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "system_shutdown") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "0ixbfjwr4ga5r4c81b4nk4addrjqkz8fq8k3w0dgmdhjxzkfjjcn")))

(define-public crate-netres-0.1 (crate (name "netres") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "system_shutdown") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "1if2f5znsr9v0la2gmwi2pjcw4l7qiwvbfjmj9hv97j4knffrzjz")))

(define-public crate-netron-0.1 (crate (name "netron") (vers "0.1.0") (hash "0y00vygqb8qzqrcwyn9hams7di8fzy0pd2x8kzfrslqi29fjqr5y")))

(define-public crate-netron-0.1 (crate (name "netron") (vers "0.1.1") (hash "0ygk1wh5b8pgxfhsdizb0rb3ygz2pfak2k90113jyacdf77vcffr")))

(define-public crate-netrs-0.0.0 (crate (name "netrs") (vers "0.0.0") (hash "124n8q2plbszch0wfahryc184xr8fzmnq9xcpph6x6dd1vlnc5b9")))

(define-public crate-netrtc-0.0.0 (crate (name "netrtc") (vers "0.0.0") (hash "1mblfrf50dg5dxmj63gyq183zqg894dy9qrjjq5115gpr3cqs10j") (yanked #t)))

