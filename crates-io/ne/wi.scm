(define-module (crates-io ne wi) #:use-module (crates-io))

(define-public crate-newick-0.5 (crate (name "newick") (vers "0.5.0") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0ckp8n3yidq9w99zvafr26mdcc9xxxas1k5rsivj6knw9amnl8mv")))

(define-public crate-newick-0.6 (crate (name "newick") (vers "0.6.1") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0avmkkgmsr1wf7qzq73m57fqcamc0cvfvzdl1vmgj6xxpw79zw50")))

(define-public crate-newick-0.7 (crate (name "newick") (vers "0.7.0") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1h82ss92s7dm6875vbigjzcmncf0cwz0gpvzgvb0m65vvbz8zml6")))

(define-public crate-newick-0.7 (crate (name "newick") (vers "0.7.1") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1dzssj2n00vz7admra3m5qm181aadx7w83c1s5r70gjqdj2p2k9x")))

(define-public crate-newick-0.7 (crate (name "newick") (vers "0.7.2") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1d1h3ks2l3qcr44hwv15hbw5qnilqqwa5wvpdvi1q0sq41xxf1ns")))

(define-public crate-newick-0.8 (crate (name "newick") (vers "0.8.0") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07l6scbgn5hp9xhzf7ym1iljla6zhab11g3pxpz9dkv4rvbixv77")))

(define-public crate-newick-0.9 (crate (name "newick") (vers "0.9.0") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0c5dd5h89ydxz493gddv8d4jzdw87npsl1v5linjlw53g0r38clg")))

(define-public crate-newick-0.10 (crate (name "newick") (vers "0.10.0") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1bfvyl9f2axbnmpbyrrj4zyr0rab3ag4vklk9bhh27mfvcr9r512")))

(define-public crate-newick-0.11 (crate (name "newick") (vers "0.11.0") (deps (list (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sorbus") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "034w2qqxpssz8agxlgva2l1dd2s40j4lsl7xx0qihlxd49qad6np")))

(define-public crate-newick-rs-0.1 (crate (name "newick-rs") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1q595wk1pc9grr7acm2w7filpbg2mf97g031jywi5a3mlldcqj6x")))

(define-public crate-newick-rs-0.2 (crate (name "newick-rs") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0q5mb0a8ni85qr7vwp7yc6yip9s4rd6zadsy70v6lr4dcqkqqkqh")))

