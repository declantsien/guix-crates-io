(define-module (crates-io ne wl) #:use-module (crates-io))

(define-public crate-newlib-alloc-0.1 (crate (name "newlib-alloc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0691wiymm7capgh20lkf2mkjj98nd85aivjxm0zgcw1wsbbap8vw")))

(define-public crate-newline-converter-0.1 (crate (name "newline-converter") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0l9ab6r2q3d4n7blja5lniw827jh4g1034kqp0vyjmqs9mby76pz") (yanked #t)))

(define-public crate-newline-converter-0.2 (crate (name "newline-converter") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0r85ci77fj02rxiwrdwrda6z2nz01bvsz9iwkcjc9fzf34miry6n") (yanked #t)))

(define-public crate-newline-converter-0.2 (crate (name "newline-converter") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fancy-regex") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "lazy-regex") (req "^2.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.15") (default-features #t) (kind 2)))) (hash "0chb0xws2mqpy0p67zmdik8sqxjyqid1jq23hsla0wzhpk94fpgh") (yanked #t)))

(define-public crate-newline-converter-0.2 (crate (name "newline-converter") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fancy-regex") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "lazy-regex") (req "^2.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "unicode-segmentation") (req "^1.10") (default-features #t) (kind 0)))) (hash "03y000bbxnwzb7aipxyw7gm68b1bd8dv7illz03l4qw7bjfx0w8z")))

(define-public crate-newline-converter-0.3 (crate (name "newline-converter") (vers "0.3.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "fancy-regex") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "lazy-regex") (req "^2.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0zyw2hyjl89rj1zmp9n8fq69pbfp9zl1cbal73agxjxixjbv1dj7")))

