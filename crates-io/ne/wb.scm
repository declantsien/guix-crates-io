(define-module (crates-io ne wb) #:use-module (crates-io))

(define-public crate-newbase60-0.1 (crate (name "newbase60") (vers "0.1.0") (deps (list (crate-dep (name "rstest") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0is1lsi602sch2x02yqa5n4xxb302w7xgz7vn7y1hm5x8crrkqhw")))

(define-public crate-newbase60-0.1 (crate (name "newbase60") (vers "0.1.1") (deps (list (crate-dep (name "rstest") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "11in6f5paqbcwdc8rjb15siky4pniwfzk0xra9c8dwlhn2hhg6yd") (yanked #t)))

(define-public crate-newbase60-0.1 (crate (name "newbase60") (vers "0.1.2") (deps (list (crate-dep (name "rstest") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "0k7rfa6ykc558hfa6g3v5s68mg15a5c392p9pl5m3wxvxnfm6qf2")))

(define-public crate-newbase60-0.1 (crate (name "newbase60") (vers "0.1.3") (deps (list (crate-dep (name "rstest") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "1fmm6dm61sk9pyybbd5k02hbgiqrg0j6p8lbjsyqqn18wsfy2vsg")))

(define-public crate-newbase60-0.1 (crate (name "newbase60") (vers "0.1.4") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1h2xgl354cqlik8d2h01vgv7xvjv96plz8ipy08zmz5chzjdgm63")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.0") (deps (list (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "11ywfxnsyzzclwx777j5fvpmaf9wvi3gg9lhvbqz3rjj4xy1phlk")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.1") (deps (list (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0fvfcwkf7vab75g22dnj9vqbf3plz1flj5vb27mhsjpz5rk2rxl0")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.2") (deps (list (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1093a5lvld27mbrd5a82pra873agxr6wcnynz61nnffa9f1crfs0")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.3") (deps (list (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0dwy3c0qjpmpxm5c3zs11kzxlj2gl7093v4y24mpmi0ibpnircri")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.4") (deps (list (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1kci5r48myw917rkms1kv13fj7vpfjz45r2s0bgyqpc2czjxjzl8")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.5") (deps (list (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0xgiwv2304r9yyj34fi5b4xhgf362gx0n1kzn7bzjm4imsb42d6k")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0w5qhi9zpqaifr0q4ca2p0j4lwsccy0m29khlnak4zw20q83hmqs")))

(define-public crate-newbee-0.1 (crate (name "newbee") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0j7ldn5i72magsdhr4rhf8lmlnls5zsb97yv7znciqapjilxakc7")))

