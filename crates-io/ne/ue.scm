(define-module (crates-io ne ue) #:use-module (crates-io))

(define-public crate-neuedu-cryptos-0.1 (crate (name "neuedu-cryptos") (vers "0.1.0") (hash "1nmg9699285bqxa1lkfvbz9f5x04z658hncrzqaxk6bldzwq7jpl") (yanked #t)))

(define-public crate-neuedu-cryptos-0.1 (crate (name "neuedu-cryptos") (vers "0.1.1") (hash "17802xhhqs42iq1z07gj2zli60d4xnq3pjijpwpv3r2cm6bz80vs") (yanked #t)))

(define-public crate-neuedu-cryptos-0.1 (crate (name "neuedu-cryptos") (vers "0.1.2") (hash "0qnqsbzk2z64sq76nf33rhh05dy0v4vwkwn56qfblnab2azxzp4i") (yanked #t)))

(define-public crate-neuedu-cryptos-0.1 (crate (name "neuedu-cryptos") (vers "0.1.3") (hash "1ylb4fbx0vfh4v9saik6jbihjaypc02s8iw9varzjgz0v3wvhswx") (yanked #t)))

(define-public crate-neuedu-cryptos-0.1 (crate (name "neuedu-cryptos") (vers "0.1.4") (hash "1rqaw1364b17w3mj63az2d9li11ld6lqnsg8h3a5xc4lrf7kwjyq") (yanked #t)))

(define-public crate-neuedu-cryptos-0.1 (crate (name "neuedu-cryptos") (vers "0.1.5") (hash "079kn3h2jd1zls12iljpwdfyr1fiiw6hmgdg8ddmvmd1fqna0kxh") (yanked #t)))

(define-public crate-neuedu-cryptos-0.1 (crate (name "neuedu-cryptos") (vers "0.1.6") (hash "13kgwv1qdqk03rm7zrss86j85q8kidv5y8bjljq925xx1dzlj60h")))

(define-public crate-neuedu-cryptos-0.2 (crate (name "neuedu-cryptos") (vers "0.2.0") (hash "1wd38vv8hsi0b180zzsvrahyvgi8flwxyw3ml3wy03jj05xcbx0a")))

(define-public crate-neuedu-cryptos-0.2 (crate (name "neuedu-cryptos") (vers "0.2.1") (hash "1501f28206kfvwxa50znr61agid5a24k8gdj78sggffc335q4l9n")))

(define-public crate-neuedu-cryptos-0.2 (crate (name "neuedu-cryptos") (vers "0.2.2") (hash "0yac4li6wan86znb6v4pk0937nnw617zzvwzkyd4v6rkphxadlxn")))

(define-public crate-neuedu-cryptos-0.3 (crate (name "neuedu-cryptos") (vers "0.3.0") (hash "1pl62n4ajcgl73vi20ckj3m2hix7r19jdffqv0c7bapp23cm23sg")))

(define-public crate-neuedu-cryptos-0.4 (crate (name "neuedu-cryptos") (vers "0.4.0") (hash "0kfsvscrh9nj245j18r4yzadcbs70gl08802m0r0vbp5ypq20l0q")))

(define-public crate-neuedu-cryptos-0.5 (crate (name "neuedu-cryptos") (vers "0.5.0") (hash "17x84a5rinjqj1c7a7gdpip082rsz0z45l3an5bvja0bm8ssari4") (yanked #t)))

(define-public crate-neuedu-cryptos-0.5 (crate (name "neuedu-cryptos") (vers "0.5.1") (hash "07ngd7vbz86j4g46qcs6s17zkixrc0ifqn2qqj90m7s0svc0yz48") (yanked #t)))

(define-public crate-neuedu-cryptos-0.5 (crate (name "neuedu-cryptos") (vers "0.5.2") (hash "18jxdwbxsza0kvb00rbn4avb9n312bvsq4lzpg33hrl1jfga984w") (yanked #t)))

(define-public crate-neuedu-cryptos-0.5 (crate (name "neuedu-cryptos") (vers "0.5.3") (hash "14vyh3hjf8bm90m8an9nnjm0p5qm7ika7ifbf498nxi0clyszbw9")))

(define-public crate-neuedu-cryptos-wasm-0.5 (crate (name "neuedu-cryptos-wasm") (vers "0.5.0") (deps (list (crate-dep (name "neuedu-cryptos") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)))) (hash "0wlscwnp5apxcmbgn5lipvcm47lkl9vivcbb6kyclis7zvr09cmz")))

(define-public crate-neuedu-cryptos-wasm-0.5 (crate (name "neuedu-cryptos-wasm") (vers "0.5.1") (deps (list (crate-dep (name "neuedu-cryptos") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1003lfxhgd4w67fijmmr3pzhh00qvyw05yqjfd310ysx17j9xdz1") (yanked #t)))

(define-public crate-neuedu-cryptos-wasm-0.5 (crate (name "neuedu-cryptos-wasm") (vers "0.5.2") (deps (list (crate-dep (name "neuedu-cryptos") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)))) (hash "0zhggyad0b1kxxwzcmrc42ym11wzcgsvvfkgf4fj46sdi42ryrki") (features (quote (("stream") ("sm4") ("sm3") ("sha512_256") ("sha512_224") ("sha512") ("sha384") ("sha256") ("sha224") ("sha2") ("sha1") ("ofb") ("hmac") ("ecb") ("cfb") ("cbc") ("block"))))))

(define-public crate-neuedu-cryptos-wasm-0.5 (crate (name "neuedu-cryptos-wasm") (vers "0.5.3") (deps (list (crate-dep (name "neuedu-cryptos") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)))) (hash "1wq2jkjrflsaz5r783z8dawgigyw1vclzf6lkljd922dqljgpz4c") (features (quote (("stream") ("sm4") ("sm3") ("sha512_256") ("sha512_224") ("sha512") ("sha384") ("sha256") ("sha224") ("sha2") ("sha1") ("ofb") ("hmac") ("ecb") ("default" "sm3" "sm4") ("cfb") ("cbc") ("block")))) (yanked #t)))

(define-public crate-neuedu-cryptos-wasm-0.5 (crate (name "neuedu-cryptos-wasm") (vers "0.5.4") (deps (list (crate-dep (name "neuedu-cryptos") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)))) (hash "120kq2ssza3a87idj1lrkfh89i8anzhaj8qh6h9vd0f2292hv9fs") (features (quote (("stream") ("sm4") ("sm3") ("sha512_256") ("sha512_224") ("sha512") ("sha384") ("sha256") ("sha224") ("sha2") ("sha1") ("ofb") ("hmac") ("ecb") ("default" "sm3" "sm4" "block" "stream") ("cfb") ("cbc") ("block"))))))

