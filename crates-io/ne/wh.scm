(define-module (crates-io ne wh) #:use-module (crates-io))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.1") (default-features #t) (kind 0)))) (hash "079dlvlkh29d6vghjyvyf172kk8k76vri3vk98bpxld782apdwf7") (features (quote (("default" "api") ("api"))))))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.1") (default-features #t) (kind 0)))) (hash "1wpbh41l1l6c7wk6k670vp8id038qid03lkw4nlcm77f6wvaxvzz") (features (quote (("default" "api") ("api"))))))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.1") (default-features #t) (kind 0)))) (hash "1alybw6lykc2bn3405nbd3syl62h7nmvndnxzvbb405v7hlxcm7j") (features (quote (("default" "api") ("api"))))))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.1") (default-features #t) (kind 0)))) (hash "0jfrs88nr49lcyddhjbpqz5zrvfnkdbi11sbw70q10zj735kk449") (features (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.1") (default-features #t) (kind 0)))) (hash "13jxmrhlii2ancdxkfx1fzms5bdpwymji9cs5wfkxsa2k57ylhha") (features (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.1") (default-features #t) (kind 0)))) (hash "12nrgn1gzwc7a5ifa44j0k5fdsjnplxanddi6i8hv5cy5xfyiyr6") (features (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.1") (default-features #t) (kind 0)))) (hash "04b7p4lsl2mr2qiqxvgkhbb3qqgzgddc44f71jcdy3jxdpyq24nw") (features (quote (("tor") ("default" "api") ("api"))))))

(define-public crate-newhope-0.1 (crate (name "newhope") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.2") (default-features #t) (kind 0)))) (hash "0la1s4ysvma9fsw0sqgplfajz4az8h296y57wq0f7xbmd6l79nyi") (features (quote (("tor")))) (yanked #t)))

(define-public crate-newhope-0.2 (crate (name "newhope") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.2") (default-features #t) (kind 0)))) (hash "12r4p558cq47lpz2v93zwq7z0f84wwl3k3jpvgxv0xr0hvgsxspx") (features (quote (("tor"))))))

(define-public crate-newhope-0.3 (crate (name "newhope") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4") (default-features #t) (kind 0)))) (hash "09llyc6wlx1j362abz7qkcg7fkgylh8i2vij3j3ky5ym4ih6q8j3") (features (quote (("tor"))))))

