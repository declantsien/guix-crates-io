(define-module (crates-io ne ig) #:use-module (crates-io))

(define-public crate-neighbor-0.1 (crate (name "neighbor") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^3") (features (quote ("rustls"))) (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "sitter") (req "^0") (default-features #t) (kind 0)))) (hash "1ak5wd6l1ak5byg78yb4111nz0xsnynjcwf3ypx2xrb0b96lh03a")))

(define-public crate-neighbor-0.1 (crate (name "neighbor") (vers "0.1.4") (deps (list (crate-dep (name "actix-web") (req "^3") (features (quote ("rustls"))) (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sitter") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.4.0") (features (quote ("postgres" "uuid"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1py6mjn3mpiafb9sm9lnbwh7631m1wyzqilbs096x13vbh9mi7xi")))

(define-public crate-neighborgood-0.0.0 (crate (name "neighborgood") (vers "0.0.0") (hash "1vzjankrdp84hp0fy8hvgj3r43dzsyb9h46l1gfxz41fhlr6mha5")))

(define-public crate-neighborgrid-0.1 (crate (name "neighborgrid") (vers "0.1.0") (hash "0i9jy2b5gaph8aad8avabdff4hvpz00q8sgm49zb6nix0nppx4jb")))

(define-public crate-neighborhood-diversity-0.5 (crate (name "neighborhood-diversity") (vers "0.5.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "05nr1ivd7vk917ay3lvsp6pw947106px4q01h3yq4kq254lpdl1l")))

(define-public crate-neighborhood-diversity-0.5 (crate (name "neighborhood-diversity") (vers "0.5.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1pnazichrv6bypafhpcyc3kc823gagfkx1qirbj899hkilyz2dr7")))

(define-public crate-neighborhood-diversity-0.5 (crate (name "neighborhood-diversity") (vers "0.5.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0cmrbjja4avyxwn416p0pdrklmjxrwkhz734gqynja13fng74ba7")))

(define-public crate-neighborhood-diversity-0.5 (crate (name "neighborhood-diversity") (vers "0.5.3") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "07sq03m4kjnqjp35k3mdckpa8dfc41vfj33fdaq3bqnizgghnvkx")))

(define-public crate-neighborhood-diversity-0.5 (crate (name "neighborhood-diversity") (vers "0.5.5") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1bsv8v67iqpgmb7zw3s52xivcajk5xdlmwqmp3yzyx1s59sfd3y4")))

(define-public crate-neighborly-0.0.0 (crate (name "neighborly") (vers "0.0.0") (hash "0dkjzdzcz8khjfnj4vg73ida4kd5i3jqsfm8ciqw9a0f0jv13jvb")))

(define-public crate-neighborly-0.0.1 (crate (name "neighborly") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full" "test-util"))) (default-features #t) (kind 0)) (crate-dep (name "trait-variant") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0gc4jkhzkjynnihp9l4495hqg7vm50ri7q8kkwc0302bc5srld7m")))

