(define-module (crates-io ne s-) #:use-module (crates-io))

(define-public crate-nes-ppu-0.1 (crate (name "nes-ppu") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jps5wvlj7ghbfa2iz4iqkvr31f0xcx4q9h6qkljmjshzg0xkq4q")))

(define-public crate-nes-ppu-0.1 (crate (name "nes-ppu") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1z5126pyjchfnpbz7ag416v4j2l5f96kinfpi0y61b0pn490d52y")))

(define-public crate-nes-ppu-0.2 (crate (name "nes-ppu") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12xpkjpdmxnn4fkp0p8fxflccy6ndlkl4364syhqsagr3mv34w0k")))

(define-public crate-nes-tetris-hard-drop-patcher-0.1 (crate (name "nes-tetris-hard-drop-patcher") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ines") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mos6502_assembler") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mos6502_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "160i3x69kilgpmj8ndrdsiwjshb5a0lvwcf5z1mwpcci7fpbk4wd")))

(define-public crate-nes-tetris-hard-drop-patcher-0.1 (crate (name "nes-tetris-hard-drop-patcher") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ines") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mos6502_assembler") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mos6502_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "016sql0absyz1vfb6pgqzrkbz3ili02bv3ibcvfy4dp8wczyw44s")))

(define-public crate-nes-tetris-hard-drop-patcher-0.1 (crate (name "nes-tetris-hard-drop-patcher") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ines") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "meap") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mos6502_assembler") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mos6502_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "10d35s5sn7fcs7xsn49bv2gwb1lvnwpzc40is46md736ci317kbh")))

(define-public crate-nes-utils-0.1 (crate (name "nes-utils") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "107jgk15cix29j63jj18zbvlm2ypx4hjwp2mck5hzgz1760pchxq")))

(define-public crate-nes-yew-0.0.1 (crate (name "nes-yew") (vers "0.0.1") (deps (list (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20") (default-features #t) (kind 0)))) (hash "0jc8fjv130hxk8v2q3ncdaxjrswnyn953b0jyyh6iz81z9chmf47")))

(define-public crate-nes-yew-0.0.2 (crate (name "nes-yew") (vers "0.0.2") (deps (list (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.20") (default-features #t) (kind 0)))) (hash "11dm4wkia3jvigyxk0z8lazh3m6xj4dsd65mj6ngbpg0xnfbx3n0")))

