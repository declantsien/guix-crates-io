(define-module (crates-io ne ot) #:use-module (crates-io))

(define-public crate-neotest-docker-phpunit-0.1 (crate (name "neotest-docker-phpunit") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "04wp5w2gzcpszn96q7s4a3gm0yz13v7jsxmfp0qfig24m91cmdrf")))

(define-public crate-neotest-docker-phpunit-0.1 (crate (name "neotest-docker-phpunit") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1mcnll7v6b2807f6gfcq1i1ki5i5539blxywh2i8wp388sbb8yxr")))

(define-public crate-neotest-docker-phpunit-0.1 (crate (name "neotest-docker-phpunit") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1xliw042av845xxfdrkzba5vpi7ig509d0rrbh5jcs5zzr7yg6pz")))

(define-public crate-neotron-api-0.1 (crate (name "neotron-api") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bz86n5y7xa744nvadv1v3vxdpchdf356yp6088441kh5s20zh42")))

(define-public crate-neotron-api-0.2 (crate (name "neotron-api") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "14y4w7hqxhhbgi00z5541yc03myjvz545c5zk83frwxn0rkwkmk7")))

(define-public crate-neotron-bmc-commands-0.1 (crate (name "neotron-bmc-commands") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "1qfj7vpvwi3lvwh86sdn2iljyjrh2p2phgd9v8scb3m153rbr59h") (yanked #t)))

(define-public crate-neotron-bmc-commands-0.2 (crate (name "neotron-bmc-commands") (vers "0.2.0") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "00fwr8l091y870k3mvzw644k4yxf46gzkjzyw0l2l5z9aak4kykf")))

(define-public crate-neotron-bmc-protocol-0.1 (crate (name "neotron-bmc-protocol") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "0677f4ncm50fw7724n1cixilhx9yac8krkr5s9phbh18n3bji4pz") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

(define-public crate-neotron-common-bios-0.1 (crate (name "neotron-common-bios") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "0wyffj02lz9c9706iq26d9c7vp2li05qhqnhnyspladqhv3y8xcb")))

(define-public crate-neotron-common-bios-0.4 (crate (name "neotron-common-bios") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "0fzgabkijsshlapxxkrfxz8lwf3arddinygqrzk0szvgxlzjlzi5")))

(define-public crate-neotron-common-bios-0.5 (crate (name "neotron-common-bios") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "17whmgds453b9z6a2pv5gsrpfkg1zmbq4kjk6j2nqsmgyfy248mh")))

(define-public crate-neotron-common-bios-0.6 (crate (name "neotron-common-bios") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "1zgv43wx5xwdz0mxfd2bwhn8qmig31g9cpgrpljci0674mkbk8i8") (yanked #t)))

(define-public crate-neotron-common-bios-0.6 (crate (name "neotron-common-bios") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "0vykadvpdlhs8mndgj0112y0djx0c38m6y5x10xd8avha6rrp4cr")))

(define-public crate-neotron-common-bios-0.7 (crate (name "neotron-common-bios") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "029h48p244b03n8c4siz3la010dlflsd87pwp0v2l08qka9xm02l")))

(define-public crate-neotron-common-bios-0.8 (crate (name "neotron-common-bios") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)) (crate-dep (name "pc-keyboard") (req "^0.7") (default-features #t) (kind 0)))) (hash "0jxrc5f1khfvijg2ricr1qyn21ndm08zvfp221hvd0d8cvpl6jzs")))

(define-public crate-neotron-common-bios-0.9 (crate (name "neotron-common-bios") (vers "0.9.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pc-keyboard") (req "^0.7") (default-features #t) (kind 0)))) (hash "1qpkz69v6gl8ad4cpcycx6lwsmdirikk85ziz5pq27zzplaxrc7a")))

(define-public crate-neotron-common-bios-0.10 (crate (name "neotron-common-bios") (vers "0.10.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pc-keyboard") (req "^0.7") (default-features #t) (kind 0)))) (hash "1m5ar3x33k9ycmsk5vps3j4wgrgd0b2pmk91v22c68g5y2121zcj")))

(define-public crate-neotron-common-bios-0.11 (crate (name "neotron-common-bios") (vers "0.11.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pc-keyboard") (req "^0.7") (default-features #t) (kind 0)))) (hash "0fmd9hadha46glp37rkycdbcvq5qafgka5n544izfqf7sa6dknls")))

(define-public crate-neotron-common-bios-0.11 (crate (name "neotron-common-bios") (vers "0.11.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pc-keyboard") (req "^0.7") (default-features #t) (kind 0)))) (hash "0xn27dk07z5g2fma3dispmvi8xmhcbzrqjfvhwjgd9kbnl9nq4kq")))

(define-public crate-neotron-common-bios-0.12 (crate (name "neotron-common-bios") (vers "0.12.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pc-keyboard") (req "^0.7") (default-features #t) (kind 0)))) (hash "005dz45km68zpgz81634f8jx6l1bd81crr8pk66xdv5xlhcv7z94")))

(define-public crate-neotron-ffi-0.1 (crate (name "neotron-ffi") (vers "0.1.0") (hash "0wjkrvcmfqgph2bha3bbmxyrmmnrx9z63npmm8hj8ww77pkqcy6k")))

(define-public crate-neotron-loader-0.1 (crate (name "neotron-loader") (vers "0.1.0") (hash "13vfv5gjd67401yjqmhd1yf0pg538ppzdcwni8rmp7cb11567f5r")))

(define-public crate-neotron-os-0.1 (crate (name "neotron-os") (vers "0.1.0") (deps (list (crate-dep (name "neotron-common-bios") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "r0") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)))) (hash "0kwnx6v0bn8slfd982xalh7irq9k76mkmgxk7g71yk2xdxri162f")))

(define-public crate-neotron-sdk-0.1 (crate (name "neotron-sdk") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "neotron-api") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "05pd7cavxmk1vcl1qgsjpmwd4ckdpxfw661wr5f1dcdl0d6qmfaf") (features (quote (("fancy-panic"))))))

(define-public crate-neotron-sdk-0.2 (crate (name "neotron-sdk") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "neotron-api") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "neotron-ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cg3bnmsbrpy0apmawr8zcpxxflq2h6mmj82pznfpdj254hdchr0") (features (quote (("fancy-panic"))))))

