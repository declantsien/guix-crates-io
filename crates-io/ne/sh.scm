(define-module (crates-io ne sh) #:use-module (crates-io))

(define-public crate-neshan-rs-0.1 (crate (name "neshan-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0rankn2cw8bh07mii6hmr2vqr804r1qv4hxn7kk1bs042p92ylb5")))

(define-public crate-neshan-rs-0.2 (crate (name "neshan-rs") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "15i13mwd4iy0x7dabi13zf2aj2p98i16dy9vzsgswfnbx68w3hdx")))

