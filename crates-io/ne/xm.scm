(define-module (crates-io ne xm) #:use-module (crates-io))

(define-public crate-nexmark-0.1 (crate (name "nexmark") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0rzb8vnkb53v077zyplzvbhvrw0kgagmccap79cir1bs0vj9kpm3") (features (quote (("bin" "clap" "serde" "serde_json"))))))

(define-public crate-nexmark-0.2 (crate (name "nexmark") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "14igwl9hiw6va867mpg7id6sm7m9k4bwaf320jkm8fcnfl8l1z25") (features (quote (("bin" "clap" "serde" "serde_json"))))))

(define-public crate-NEXMemory-0.1 (crate (name "NEXMemory") (vers "0.1.0") (hash "1axzr705rj8dakac6ib955zsyglbn11dx1v5d7a52cpkp923i18g")))

(define-public crate-NEXMemory-0.1 (crate (name "NEXMemory") (vers "0.1.1") (hash "1hk0pjyiwqbf2sgv9m0il00r1hblngl5l0i18ilg9s21dpygvwnf")))

(define-public crate-NEXMemory-0.1 (crate (name "NEXMemory") (vers "0.1.2") (hash "0inih45yxzd5h5zcvjgygj1ysmd9883r5i0vaxh9ldnxz1ys3liy")))

