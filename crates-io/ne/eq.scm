(define-module (crates-io ne eq) #:use-module (crates-io))

(define-public crate-neeqaque-0.1 (crate (name "neeqaque") (vers "0.1.0") (hash "0y484na74yddkszp1v56283ca0xl5179nnl54isxmm17ip3191p6") (yanked #t)))

(define-public crate-neeqaque-0.2 (crate (name "neeqaque") (vers "0.2.0") (hash "0785qwbam2jhvw9jb2kp8spq0k03rhyy9xfi2ijcvphzil4riqxh")))

