(define-module (crates-io ne of) #:use-module (crates-io))

(define-public crate-neofetch-0.1 (crate (name "neofetch") (vers "0.1.0") (deps (list (crate-dep (name "human_bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "0fnygxy77h3r3c11i60fgajalhz8k1kdpaikdlzcsvpxg2164p4v")))

(define-public crate-neofetch-0.1 (crate (name "neofetch") (vers "0.1.1") (deps (list (crate-dep (name "human_bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1hlpxj1yllmlc332gvcbddww2bjcipg2mza2ajswrkbbp3bfk6ij")))

(define-public crate-neofetch-0.1 (crate (name "neofetch") (vers "0.1.2") (deps (list (crate-dep (name "ansi-width") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "human_bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "0ciz63qwzjiq3qbj12m93s6yz2s0frvcil2qg5aajmnnf4sg3azk")))

(define-public crate-neofetch-0.1 (crate (name "neofetch") (vers "0.1.3") (deps (list (crate-dep (name "ansi-width") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "human_bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "00hfx768jk01fp09l84si2g5p6lmgg32mlym32r82ila6h9p6zsx")))

(define-public crate-neofiglet-0.1 (crate (name "neofiglet") (vers "0.1.0") (hash "06v6cmi2j3xj6zvvxx4l0ad628vkg599ka1941azn2vlj6rznhc9")))

(define-public crate-neofiglet-0.1 (crate (name "neofiglet") (vers "0.1.1") (hash "06khlq95m5pph6k1ix7ykhddx5rfgxsrr69r7f5cg4n67hf3xn0a")))

(define-public crate-neofold-0.1 (crate (name "neofold") (vers "0.1.0") (deps (list (crate-dep (name "bio") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rna-algos") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rnafamprob") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gmsif68nmmicf3g30dqzlb5rkdwf1p34yrd68avvsvgsyja3gcr")))

