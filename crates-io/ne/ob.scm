(define-module (crates-io ne ob) #:use-module (crates-io))

(define-public crate-neobirth-0.1 (crate (name "neobirth") (vers "0.1.0") (deps (list (crate-dep (name "zengarden") (req "^0") (default-features #t) (kind 0)))) (hash "1mc39k3495dvd26jlpm9kckfcirai6x2n4sdrsb0zlm9ylq7saby")))

(define-public crate-neobirth-0.1 (crate (name "neobirth") (vers "0.1.1") (deps (list (crate-dep (name "panic-halt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "purezen") (req "^0.0.2") (kind 0)) (crate-dep (name "smart-leds") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trellis_m4") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ws2812-timer-delay") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dlmg6x6x772527w1ppa6j9liciyc0hbj1gf3vf186i7pys0rsd8")))

(define-public crate-neobmp-0.1 (crate (name "neobmp") (vers "0.1.5") (hash "1ich447nnykr8myipygxgvzhxqwlz439fk22vnf54a9i5l0g9q9k") (yanked #t)))

(define-public crate-neobmp-0.1 (crate (name "neobmp") (vers "0.1.6") (hash "1vw4c347nq8qgz21bv2kc4j8zhqylb1dakh5rnb1pzp0hll5iw5r") (yanked #t)))

(define-public crate-neobmp-0.1 (crate (name "neobmp") (vers "0.1.7") (hash "1n7y338fldmfvxz83d1w56gg8ddcibp2cpbc82sqry3z76g3xlpk") (yanked #t)))

(define-public crate-neobmp-0.2 (crate (name "neobmp") (vers "0.2.0") (hash "0hi3ndryrdv3zx3bdbqibvn6630rkjrh54cwn752azka55hplm7z") (yanked #t)))

(define-public crate-neobridge-rust-0.1 (crate (name "neobridge-rust") (vers "0.1.0") (deps (list (crate-dep (name "serialport") (req "^4.3.0") (default-features #t) (kind 0)))) (hash "13m1pban0s2sva1gqyy6g1bkdmkwkw15rc712a12llq293w9m3wz")))

(define-public crate-neobuffer-0.1 (crate (name "neobuffer") (vers "0.1.0") (hash "0gadk2slyr8iz7a7sagvik310b3lxlgsj6njx1ay8bpg7987ra84")))

