(define-module (crates-io ne di) #:use-module (crates-io))

(define-public crate-nedis-0.1 (crate (name "nedis") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "135bzc0n5gwji7rnx4wzwj75lbjdr3793zasq2zjr3mg9l1dw1p6")))

