(define-module (crates-io ne ow) #:use-module (crates-io))

(define-public crate-neowatch-0.1 (crate (name "neowatch") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vnmxzz0ihhi1v50xs9nsf1vwgflc7lwva8x7dk46ij25srn30vm")))

(define-public crate-neowatch-0.1 (crate (name "neowatch") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.5") (features (quote ("std"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "1x5rj8ps3fwbdr64qc60f1lcq7ixjcb4qvkcw892aab8j1p79nal")))

(define-public crate-neowatch-0.1 (crate (name "neowatch") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.5") (features (quote ("std"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "09xcx2fdhpyjisqzx48p539qigrr1appzpdw14q47fyyf9as453c")))

(define-public crate-neowatch-0.1 (crate (name "neowatch") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "03fa7cvhb1c27y0757gza87b5dm74si4ng4l182p0fzjkn4xymhs")))

(define-public crate-neowatch-0.2 (crate (name "neowatch") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "ctrlc") (req "^3.0") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k0kkdplx0af6pzd5xzn52wxnmi5n143vy5rmk5is7gbhvj82b7p") (rust-version "1.61")))

(define-public crate-neowatch-0.2 (crate (name "neowatch") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("std" "help"))) (kind 0)) (crate-dep (name "ctrlc") (req "^3.0") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rdp63zv86lc65w19cy28qfwd56nw82m76mz9q1lk3xy2f07ja39") (rust-version "1.61")))

