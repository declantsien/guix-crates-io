(define-module (crates-io ne wf) #:use-module (crates-io))

(define-public crate-newfile-0.1 (crate (name "newfile") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15bixhshx0nfd7zjvqimdy18py3mwcarffmpqiwcrkv5ls22ihc5")))

(define-public crate-newfl-0.1 (crate (name "newfl") (vers "0.1.0") (hash "0g1s6ycw5qkw1x9n10197p60q4fww0j2w2bfq0ckia2czbxa5zb6") (yanked #t)))

