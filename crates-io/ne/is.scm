(define-module (crates-io ne is) #:use-module (crates-io))

(define-public crate-neis_calculator-0.1 (crate (name "neis_calculator") (vers "0.1.0") (hash "1alh1ng38yzbrjdyw5hhhjjqxnx2g1jkbsgwrl89b6q928s4z4vy")))

(define-public crate-neis_calculator-0.2 (crate (name "neis_calculator") (vers "0.2.0") (hash "1669fkakmw7f2wnjnwz3is88bffh2k0msypmcpk2dzhaml4q6n3q")))

