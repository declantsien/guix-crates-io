(define-module (crates-io ne tn) #:use-module (crates-io))

(define-public crate-netneighbours-0.1 (crate (name "netneighbours") (vers "0.1.0") (deps (list (crate-dep (name "macaddr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("netioapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1wp2chif7irdahqjyc75js0z4cqlw64dp7lb8frybdhb4kvgvs05")))

(define-public crate-netneighbours-0.1 (crate (name "netneighbours") (vers "0.1.1") (deps (list (crate-dep (name "macaddr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("netioapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1r68g8fsh7g5q216h1ifwrs2mswf13snmm356cxgmsm348dq1hzc")))

(define-public crate-netneighbours-0.1 (crate (name "netneighbours") (vers "0.1.2") (deps (list (crate-dep (name "macaddr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("netioapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "13rg62v908jjnr9r7a1ckd1j472h7cvsa0zfpyvh7l1xamzk9il5")))

(define-public crate-netns-0.1 (crate (name "netns") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1hx38ba8y2dqghy4ndwswd2600kww8jkhraysn7g6z0szw3xvp8k")))

(define-public crate-netns-exec-0.1 (crate (name "netns-exec") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "08h63zkxiqvrm4npigs6246rnnvif2xnad6b8ff9qxkvl4hys06w")))

(define-public crate-netns-exec-0.1 (crate (name "netns-exec") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "0727nmvln6y5sfds6gr1g1agfyq4r4jgy5ini61kcamxv3zp5dwd")))

(define-public crate-netns-exec-0.1 (crate (name "netns-exec") (vers "0.1.2") (deps (list (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "1dli0pq371v0i5cwnm8gacx3py2ra827bjc93l5mykpkyjs064km")))

(define-public crate-netns-exec-0.2 (crate (name "netns-exec") (vers "0.2.0") (deps (list (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "08qmkf6q54qzdl0pk5jjj5y3fk92s3bn6wgq3i7v0w48s81w7bv9")))

(define-public crate-netns-exec-0.2 (crate (name "netns-exec") (vers "0.2.1") (deps (list (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "14yb3yxla41a9bkw92pnhdxn23pg8p4r79fzvjanp46ccnzm421s")))

(define-public crate-netns-exec-0.2 (crate (name "netns-exec") (vers "0.2.2") (deps (list (crate-dep (name "nix") (req "^0.18") (default-features #t) (kind 0)))) (hash "1rb8zw00jlg7z9zy5ngg1la0b7nvmwi5czny5im1z9rqbz1apdr8")))

(define-public crate-netns-rs-0.1 (crate (name "netns-rs") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0vi7slzq4qvraip7m3japcfb0j7alr916fnsl38qrlfpy6a1cm13")))

(define-public crate-netns-utils-0.1 (crate (name "netns-utils") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (features (quote ("user" "hostname" "mount" "signal" "sched" "fs"))) (default-features #t) (kind 0)))) (hash "1i74s25dhwlpn06zs2jban9rhw3ha6bfhb69bm86d3acq4b1ns2l")))

(define-public crate-netns_tcp_bridge-0.1 (crate (name "netns_tcp_bridge") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (features (quote ("async"))) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.24.2") (features (quote ("sched" "socket" "uio" "fs" "net"))) (kind 0)) (crate-dep (name "tokio") (req "^1.20.1") (features (quote ("net" "rt" "io-util"))) (default-features #t) (kind 0)))) (hash "0is32l2hw4dx07w942k7kf7ris7nq36cwrhzaacn5sr9r26jsrr5")))

(define-public crate-netnyan-0.1 (crate (name "netnyan") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full" "signal"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1cpzv8d92whlmzp2s8m6pp12qs3zjx34wdd0rkvhs8yacmrc3n7b")))

