(define-module (crates-io ne ny) #:use-module (crates-io))

(define-public crate-nenya-0.0.1 (crate (name "nenya") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 2)) (crate-dep (name "eframe") (req "^0.27.2") (default-features #t) (kind 2)) (crate-dep (name "egui") (req "^0.27.2") (default-features #t) (kind 2)) (crate-dep (name "egui_plot") (req "^0.27.2") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)))) (hash "0g90bwpphmph8sgjz95lir25x2116w6ma9if9xh9nq5mz15phvqa")))

(define-public crate-nenya-sentinel-0.0.1 (crate (name "nenya-sentinel") (vers "0.0.1") (hash "10ckns6bsvivzpppzw26rjzsj7ds14r2bdwgng0nkjgaks50payj")))

