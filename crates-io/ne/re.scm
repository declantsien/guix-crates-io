(define-module (crates-io ne re) #:use-module (crates-io))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0sphr55byldbw4das217y3cdbf6xz648dzrx76addqd2lmsxayd2")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0a4cgcj7m6ikzn3iajfdph93b74vqvrlh200krjjsvswkjrb2kh5")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1n3x6x9sfap8hzs2ds4fvq5ig9acqlamxgfam72ayz3llj2a25dy")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02ab5xnq9y8asm1d5vv8h9nk023bxgrcpvvf46hpwqb0saz6a6yc")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fjbjb8161ygjy9xra5haj36yyq0rhgzzradxnjxq7bhhihbly5m")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.5") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jfj99b2qxkc69bxfcv4803s89ryhflb0cny16xky31v742rd3c1")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.6") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "066v8gj7vmgr2zdhs2haqm5jz57pi2sgaa63adbhzak6irdfcf4g")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.7") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "03ypwa3fwpvykwbp8khz0zvh8j8ynvj9jjjvv8hvdfkinyq6x1p1")))

(define-public crate-nereon-0.1 (crate (name "nereon") (vers "0.1.8") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pcaxsdn84lv498a8qsi6n0cmkla83aa2ka8il3bb54r0kc9q2ci")))

(define-public crate-nereon-0.2 (crate (name "nereon") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "1if6q5h8hlp79a30j4rnw3d74d675dh1isdxjcv9xjqzg50l2k5g")))

(define-public crate-nereon-0.3 (crate (name "nereon") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "1n84q1qsq7pkdww6ahr1y2pq1xz76n6hqdq7xg8xc79hqnw6b7nq")))

(define-public crate-nereon-0.3 (crate (name "nereon") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "0r2qca6wqkjpa6zn89271pmwmplp41qsr1rpg6nhi46dkfqni64h")))

(define-public crate-nereon-0.3 (crate (name "nereon") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "0h3a36nvywp0dwrb5xjpvb4z6mdiqsha9m0b02ykkjv1656knk82")))

(define-public crate-nereon-0.4 (crate (name "nereon") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "1sxhby6g5dfka0ybs23yvrljyp5c0f4ihyz6yynw138iln04rm3k")))

(define-public crate-nereon-0.4 (crate (name "nereon") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "0syxv69p0r10hx7mpfa21k03bdpvyjb0kl4agmcy21ayyi5q6azk")))

(define-public crate-nereon-0.4 (crate (name "nereon") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "0wpqchnyv4avbqhhg1kyfzkh2as9al5z6iywmxfac6y62a1qsxk2")))

(define-public crate-nereon-0.4 (crate (name "nereon") (vers "0.4.3") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "0zni040rffwys7gj0s99cgq9wsjrjzmx6j6rlnsj75018lzvcbmg")))

(define-public crate-nereon-0.4 (crate (name "nereon") (vers "0.4.4") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "0ili9yg60f3q95w2i4ax36ryqzm05shn36426zy73gmwp3n98kda")))

(define-public crate-nereon-0.5 (crate (name "nereon") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "0zzg6hcgjr4yvv7823qj0f6bdj5dbrqbgsjf411fm3pbd66fypi0")))

(define-public crate-nereon-0.6 (crate (name "nereon") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 2)))) (hash "1kb06h7cpwnq8lw0fj7z8vzwg66cj0bcd2fb4ixvbc806bfdakmp")))

(define-public crate-nereon_derive-0.1 (crate (name "nereon_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0w6hii5g4rsy52clq2n374saxs1fkgkcffhwvjr29rhn6wsc6kd8")))

(define-public crate-nereon_derive-0.2 (crate (name "nereon_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "05gx31wzhnvspa9gqqgmg31wga99yryriidd9p0720i21nxvxdvd")))

(define-public crate-nereon_derive-0.2 (crate (name "nereon_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0qnp0gj9gnnlm5bhx3zzkwm2apzxnb2v5x28rrab6wyqkmhv31xl")))

(define-public crate-nereon_derive-0.4 (crate (name "nereon_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0r13hkb43mzans4yb9byxrf27iq69sh02vs1rqlbdac39c5y4vcd")))

(define-public crate-nereon_derive-0.4 (crate (name "nereon_derive") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0rxpfs0q02wxf6l32yw8bv7vil9bwan3p3r4y8b350bl6iv4n0sq")))

(define-public crate-nereon_derive-0.4 (crate (name "nereon_derive") (vers "0.4.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "163fp7b4fbx01kpnh1kaicz6783vh7g204sfn3bwpvbfd25yvaha")))

(define-public crate-nereon_derive-0.5 (crate (name "nereon_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1vix83jsbsmik8968lgmf79w598mz5zvb8wd16h0d9xz8ms46ici")))

(define-public crate-nereon_derive-0.6 (crate (name "nereon_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0lzl3wkaqca7dyvib2195i8gabh9ddh0y2i133n6akar4naryx9k")))

(define-public crate-nereond-0.1 (crate (name "nereond") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nereon") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.4") (default-features #t) (kind 0)))) (hash "0j082zy04jyqs7vmzvjyi12xi5xzabb0nlny4mczg33qy0r3v1bm")))

(define-public crate-nereond-0.2 (crate (name "nereond") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nereon") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.4") (default-features #t) (kind 0)))) (hash "10pi03awm6sqzp9a05p95qz8j2lx0ajw3zn9pv442id6ynvq8lrg")))

(define-public crate-nereond-0.3 (crate (name "nereond") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nereon") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nereon_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.4") (default-features #t) (kind 0)))) (hash "09pyy5s7h44aswgib7mhbqdr95my8p48081bph62j09xig428vy6")))

(define-public crate-nereus-0.1 (crate (name "nereus") (vers "0.1.0") (hash "0k80bbxivwvw006wzi8w7sz9isigj425i8pn100rc2bs3y0b9j04")))

(define-public crate-nereus-0.1 (crate (name "nereus") (vers "0.1.1") (hash "1pfpj38lvqfhpzijmap883rhcx6wxpa4fcgm6n6672is8aq7cq8y")))

(define-public crate-nereus-0.1 (crate (name "nereus") (vers "0.1.2") (hash "1d8ckzz3rwypikn7cqb5jj0ah9z90w6ixg0l68rrvrr48p989n48")))

(define-public crate-nereus-0.1 (crate (name "nereus") (vers "0.1.4") (hash "0i2b3cavs8bp3glbj7dg404ixi522vi0v0w17711pd8a849f70rp")))

(define-public crate-nereus-0.1 (crate (name "nereus") (vers "0.1.5") (hash "032wybp82hz1f159m1kayga9ba7c6rbmf0i343nzqs1a972jqy37")))

(define-public crate-nereus-0.1 (crate (name "nereus") (vers "0.1.6") (hash "0zrh3pcz0rx3n2fphazaizk8p9nv149kwxwnnlavvzv03i5c1h7r")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.0") (hash "1w540bqldg53bmbwqdc3ki5ijhlwj1lp73a0lhcd8bcl9x5c2m3a")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.1") (hash "1gkw7hkxrq66m5fylnl39pwg7ksgxmslj87vwlqvskaldn9119kk")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.2") (hash "0kii8l5jm7zi8wh4rnx4zcaxgca8ax7r802j0nyi87d7ipahc536")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.3") (hash "0zfnqv3h0v18x6hgn2mlk1ggvhd8igc3paslwc9461clax1zm7sf")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.4") (hash "0mcxlhgjhczbg7snr8aymdrz56qgkq6alxvsxciklbkx5h4cjjgd")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.5") (hash "0jirf6ch7v7bp4w35wjmdnvfffrjz9xawrzq85d8i5kmpd9x61x8")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.6") (hash "0y0chxm917g2g559cb4sbphvls2g979i60ahmsfy02381fpi5np4")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.7") (hash "0ax51f3154dgj0zybdvcd24rp4vpbxwdx1ylnabaq47g93lqi59d")))

(define-public crate-nereus-0.2 (crate (name "nereus") (vers "0.2.8") (hash "1azrkdccqp9ki7r6q47ab032217x5dc8xg0riq4mkxrrh2j95a3c")))

