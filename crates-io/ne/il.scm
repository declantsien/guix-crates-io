(define-module (crates-io ne il) #:use-module (crates-io))

(define-public crate-neil-0.1 (crate (name "neil") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0v38najsr0lps4wzj6gwjfjnqdk0iji7aghrzpz4cy3rypx9pw2k")))

(define-public crate-neil-0.2 (crate (name "neil") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1ygsc7dm6qhg5lms7dbr7bn8j5xkcl1f5ldza069kvd2jwhpm7wq")))

