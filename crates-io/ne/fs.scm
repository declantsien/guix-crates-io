(define-module (crates-io ne fs) #:use-module (crates-io))

(define-public crate-nefsm-0.1 (crate (name "nefsm") (vers "0.1.0") (hash "1x01x2y828jlixh8jd78m4s5gd81yy2w0r36c0g3lwlxw6lz0ydg")))

(define-public crate-nefsm-0.1 (crate (name "nefsm") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "16lrg7sk57b8vp23cl0q1396nnz5c5vlvv8dv82z4fpksyywysgq")))

(define-public crate-nefsm-0.1 (crate (name "nefsm") (vers "0.1.3") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "094v5ilqn3ggrk11ix7yby8cgccrdafgdxigxmxrw35j97w2qkqz")))

