(define-module (crates-io ne ge) #:use-module (crates-io))

(define-public crate-negentropy-0.0.0 (crate (name "negentropy") (vers "0.0.0") (hash "1wajqyrw7az74h5g8kn7n4krxs832djck7b39hkj9z2fl3yav6w6") (yanked #t)))

(define-public crate-negentropy-0.1 (crate (name "negentropy") (vers "0.1.0") (hash "10hdh8q4l9cgg2wbi2w6jbg5sf4f9iaka0sxqpnyjk4mvrjpzcgv") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-negentropy-0.2 (crate (name "negentropy") (vers "0.2.0") (hash "01ib4aaxk6d343w1b36182wxyrjmchk0vb283yfkwd8gyygq3n32") (features (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.2 (crate (name "negentropy") (vers "0.2.1") (hash "1kfvci6xikgi45953ksr3vi1dwlrs1sbipigj7aq816xmpsvh9xj") (features (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.3 (crate (name "negentropy") (vers "0.3.0") (hash "1n84xnyxah3rxkwczg7b60zs8kq9d9kglfysr967rp60a8w7nvf4") (features (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.3 (crate (name "negentropy") (vers "0.3.1") (hash "1ziaqjz8bph7xqc3fh5fk6cfhd90g1ch1qd0ywj75653g09rfr76") (features (quote (("std") ("default" "std"))))))

(define-public crate-negentropy-0.4 (crate (name "negentropy") (vers "0.4.0") (hash "0byrf88hd9x2ll7pmf29r7wsd5h8qhvdy7xcinfcdzlm73sdrhf1") (features (quote (("std") ("default" "std")))) (yanked #t)))

