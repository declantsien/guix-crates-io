(define-module (crates-io ne ga) #:use-module (crates-io))

(define-public crate-negahban-0.1 (crate (name "negahban") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "0an9cdv2lrdw5h38hwbfaivh0rmaii8i4gc4phyvs7ang2mggb6l") (yanked #t) (rust-version "1.67.0")))

(define-public crate-negahban-0.2 (crate (name "negahban") (vers "0.2.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "0nc94kj8jcwkzp71fjx49c1g0yhkf1wh4j6yvkmbr0vh5fqaznfr") (yanked #t) (rust-version "1.67.0")))

(define-public crate-negahban-0.2 (crate (name "negahban") (vers "0.2.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "0nirjwbq8cg5drdqqlvdyivba4b5m7ji4hj9zd1b6qgcyb24bzg6") (rust-version "1.67.0")))

(define-public crate-negahban-0.2 (crate (name "negahban") (vers "0.2.2") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "1hp4q0dzkcp1hwxhdic3msprg3dvc0hfjdi5giax344z59i7ygi2") (rust-version "1.67.0")))

(define-public crate-negahban-0.2 (crate (name "negahban") (vers "0.2.3") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "1593jdlpdx0skf2c3lc78g1r0iy8vi6lx4ngk07zyqgf4ardsy1f") (rust-version "1.67.0")))

(define-public crate-negahban-0.2 (crate (name "negahban") (vers "0.2.4") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "0lc7s3y4c7yf22bic3ciddlypswszjfnbxx9mjhv0r7fx41lwzm6") (rust-version "1.67.0")))

(define-public crate-negahban-0.2 (crate (name "negahban") (vers "0.2.5") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "0ljfwpw9232gm9pfdl3wylfchh5xvilpglm2gjycjjsvd1ai7w95") (rust-version "1.67.0")))

(define-public crate-negahban-0.3 (crate (name "negahban") (vers "0.3.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "1rjkib994n7mkl9s0kqywxwcn4ysy2knd146w0whgqmhjbw0braf") (yanked #t) (rust-version "1.67.0")))

(define-public crate-negahban-0.3 (crate (name "negahban") (vers "0.3.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (features (quote ("mio"))) (default-features #t) (kind 0)))) (hash "19gi9wx86rc3cipahgql24fm9wnpcxrl2va5lxrh8p8qikqrx9vn") (rust-version "1.67.0")))

(define-public crate-negatable-set-0.1 (crate (name "negatable-set") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1") (optional #t) (kind 0)) (crate-dep (name "vec-collections") (req "^0.3.3") (optional #t) (kind 0)) (crate-dep (name "vec-collections") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "0pgb9ry1mn9yhxjv1vlrmm8ml1mdc5nwz33750lifdz8yxbwd5as") (features (quote (("vc" "vec-collections" "smallvec") ("default" "serde"))))))

(define-public crate-negate-0.1 (crate (name "negate") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.75") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0ib66rmw4yb5rv82dbrazy0qvnqhjn9ssvxp9pg5drnidr9cqcb6")))

(define-public crate-negate-0.1 (crate (name "negate") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.75") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0lsffa60zk13il0f6qggxr62fa2mzin8diiqr7i4xrc4xnvsj4in")))

(define-public crate-negative-impl-0.0.0 (crate (name "negative-impl") (vers "0.0.0") (hash "0cl42vlp5c1wz88ypfz18d8llpma2jbfjbjnqwxv0i6lwxvqf27h") (yanked #t)))

(define-public crate-negative-impl-0.1 (crate (name "negative-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wn9qsm96a6ljqb9as1pzraf9mvd5vwmqcplk1l11c931qmbbvhh")))

(define-public crate-negative-impl-0.1 (crate (name "negative-impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0r1dd4l1vyw89d7zaicw90cairb7hxd1i0866y9qagqzwpf7qsl7") (rust-version "1.37")))

(define-public crate-negative-impl-0.1 (crate (name "negative-impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0d4871npz7jyyq4yl5nbv3s2xsy9w4f62rkcw6g3xva61a9glr56") (rust-version "1.37")))

(define-public crate-negative-impl-0.1 (crate (name "negative-impl") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n509fi7cndmzqbq4q9ygwk8bmp73x90p2z6zkifcjn5kcgrri2v") (rust-version "1.56")))

(define-public crate-negative-impl-0.1 (crate (name "negative-impl") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g5pbwayrbbqnrprvnyfjj0p81wliwmxq45kylnjk2akjbhac7v8") (rust-version "1.56")))

(define-public crate-negative-impl-0.1 (crate (name "negative-impl") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05hmd37c304szyxvwjni60mifzm3qby1bqwjbxlds0jhd737d4xd") (rust-version "1.56")))

(define-public crate-negative-type-bound-0.1 (crate (name "negative-type-bound") (vers "0.1.0") (hash "0ycxrysy0dndf1b9gy05wizbhl1ani8iqa9h35h9fm7mhjq4xrzp")))

