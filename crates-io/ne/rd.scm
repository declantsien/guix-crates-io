(define-module (crates-io ne rd) #:use-module (crates-io))

(define-public crate-nERD-0.1 (crate (name "nERD") (vers "0.1.0") (hash "1ri7w5m105hwag50dwfi5ar1qwqqaijbb48gzy3vnyqflji5cimn")))

(define-public crate-nerd-font-symbols-0.1 (crate (name "nerd-font-symbols") (vers "0.1.0") (hash "108vzw72y95myvjm4d91vf8g6gsfg4rv705ivd4ap00p44y2gaw2")))

(define-public crate-nerd-font-symbols-0.2 (crate (name "nerd-font-symbols") (vers "0.2.0") (hash "1jrpi1y4fiycrxylj3xnyzin8g8isqjpbldlcz5j3vgkf5h8x54c")))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dqgn4y4jq7shy5r6c3fibxls1lbh9yi0hy7nn8k2rhx72k3di22") (yanked #t)))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.1") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "10nifj4zwlnpxjq2dncdyhf5pczr3524szniz1c1zn5ncb2ncswa") (yanked #t)))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.2") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k3jz43lgb1jriy5wp4lmp7v8pgxyfg6wyq6ylw5bbs6c0bkhz09") (yanked #t)))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.3") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1c7pv28bjndp0f10dn8q1zqcq6cy1gkn56hm7yc9f9ala5i0q0pd") (yanked #t)))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.4") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x72ln4sp6mp5r3kns7c3zajp12p16v5r0zragnwrqidg7pmdhwc") (yanked #t)))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.5") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fy9dmyng2g2gg2jj06kh3j106sclhw62rbhn3sb3fxa3dxdczs7")))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.6") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0w0ddd4gdip2whpwwlzp0xdrwnhrzv1zc5n4n2zh8z9yzbr0ffws")))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.7") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "06s1ibifziwzs9jd7qfvmy27lzjp53jpdbcnz3mxdk9igr7kh7k2")))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.8") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yl1g09ck3h39wvp91d4w4zclmpjy2hgiwdcjqh951y0xjzxmbf7")))

(define-public crate-nerd_fonts-0.1 (crate (name "nerd_fonts") (vers "0.1.9") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "06gf4gi2czbw6xqwwyj89k0k4caz1myvzgxhdnvzcdzldkq2zghq")))

(define-public crate-nerdondon-hopscotch-1 (crate (name "nerdondon-hopscotch") (vers "1.0.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1mx66i5bh2qxzmkmh2wazi83yx99zjycy3njq6svqqh4zf8mj3w1")))

(define-public crate-nerdondon-hopscotch-1 (crate (name "nerdondon-hopscotch") (vers "1.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "07n4m5iparms1r4jhrjsiskf4rydin653w9qhdwa1h22cs7rkkqk")))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1g8hwr46qccs4208cdbj4b1cn1jnhqvhs2zqqzia1r5h85pc9fbw")))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.2.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "15agxpg9k7v29r40m1p1z5dh22wq994k7rw3fjshpp1wv3827i57")))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.3.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "13rp06jh2qs19kk8cg6zv3p9kq577lx5yqbny6zjzbxvz3m1ysg6") (features (quote (("default") ("concurrent"))))))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.3.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1vgnh5wzclv7hhknnygi3hyx0smxsg346brvms8irh8qplxv61iq") (features (quote (("default") ("concurrent"))))))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.4.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1m8wkw8af01csqv68z25ygwij9hm7mqdfrg16cg0pksdh1nvxpkp") (features (quote (("default") ("concurrent"))))))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.5.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1h2c3bilbgvx7dxp5yc5sfylsxrfh129yd2nql564sy5nz0zmdsn") (features (quote (("default") ("concurrent"))))))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.6.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "126w2b2v0r404nlmvjg87cq4zxjadbw0vkxzshxki7v0n5nczcbb") (features (quote (("default") ("concurrent"))))))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.6.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0jrcprz48mypak2ppzkcqpk9ibjpcpcfwvfghhdwjq8l5n5vjr3q") (features (quote (("default") ("concurrent"))))))

(define-public crate-nerdondon-hopscotch-2 (crate (name "nerdondon-hopscotch") (vers "2.7.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1m3bia0479p061x1b828i024jfz3hb6izby8i6dydcn14ibk9xp8") (features (quote (("default") ("concurrent"))))))

