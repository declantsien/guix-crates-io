(define-module (crates-io ne sc) #:use-module (crates-io))

(define-public crate-nescookie-0.1 (crate (name "nescookie") (vers "0.1.0") (deps (list (crate-dep (name "cookie") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.27") (default-features #t) (kind 0)))) (hash "0dm00pzmq9wwbk2nkk2jxbr59gy89dmbcbpp6l9mji14j35ar12q")))

(define-public crate-nescookie-0.2 (crate (name "nescookie") (vers "0.2.0") (deps (list (crate-dep (name "cookie") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.27") (default-features #t) (kind 0)))) (hash "0avr3qwxv1p59j1snpydf7kk17kxv0l5xfggbks377134xnrsk23")))

(define-public crate-nescookie-0.3 (crate (name "nescookie") (vers "0.3.0") (deps (list (crate-dep (name "cookie") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.2.27") (default-features #t) (kind 0)))) (hash "0b9w3gfxv0kjrk1dylvc9sjjnm89nwgf3n88i7fsyfhkyqp2kzaf")))

(define-public crate-nescore-0.1 (crate (name "nescore") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "19n0b677snr5q11znfy5waa6vak7f3ywma8xf9bd8snmx12wzgs5") (features (quote (("events") ("default"))))))

(define-public crate-nescore-0.2 (crate (name "nescore") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0rl4s7ylmj3np6nq08snlk1148jpir5n2xkyk4n1abh2ny0m0d1c") (features (quote (("events") ("default") ("bench-nescore"))))))

