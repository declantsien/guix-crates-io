(define-module (crates-io ne ct) #:use-module (crates-io))

(define-public crate-nectar-0.1 (crate (name "nectar") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "07a9w7kwlji2idnxkcgmwipi7ar8d4aplf9a4qq6xgy7na9ff5j7")))

(define-public crate-nectar-0.2 (crate (name "nectar") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1ydk4rirq5pknv33rcv0xp9gk8dv6jcryx5ppjn2bbr3bm4y6b9m")))

(define-public crate-nectar-0.3 (crate (name "nectar") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1aad0dsx3c74r9fibyspsxcxmjqr7hqxq99927qcinyspixmxm8p")))

(define-public crate-nectar-0.4 (crate (name "nectar") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1rhy47y5zdjivwlhly1fkjd3bqhi072c27byhdxmqrrkjhwi4iwy") (features (quote (("unicode"))))))

(define-public crate-necto-0.1 (crate (name "necto") (vers "0.1.0") (hash "050ijnw7h9khhnyd324ma5lm5gvm2yrp5wnrp5pjs8ri2ydrfbvk")))

(define-public crate-necto-0.1 (crate (name "necto") (vers "0.1.1") (hash "1a4vzx0v5c183aiysrf9cbr5qsanvgcjdcnv6vcwj10sr4dn47nx") (yanked #t)))

(define-public crate-necto-0.1 (crate (name "necto") (vers "0.1.11") (hash "1rhjg6byghhqijb6886s4yizyxhx9c2k9n5kp61a4pa6a9dqj7gh")))

