(define-module (crates-io ne ov) #:use-module (crates-io))

(define-public crate-neovide-derive-0.1 (crate (name "neovide-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x8pc52b126m9xjfw0hjjwadjgpbmq63kzn5g4kgxp6yh03fd4b3")))

(define-public crate-neovim-0.0.1 (crate (name "neovim") (vers "0.0.1") (hash "044f9gavp64w1yv64fxxv7v9mb8ywj7013x2vfbljk9g5r1l8h1f")))

(define-public crate-neovim-0.0.2 (crate (name "neovim") (vers "0.0.2") (deps (list (crate-dep (name "mpack") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1d74i74r1vmvcapg8xpxb6i0qg68p0hn54p8k6sx8f621xjs5f82")))

(define-public crate-neovim-0.1 (crate (name "neovim") (vers "0.1.0") (deps (list (crate-dep (name "mpack") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0x6f1h05zx4q2ypgfz51d6f642q7k3bgj4gc2hd89i4vyx7b7rn1")))

(define-public crate-neovim-ctrl-0.1 (crate (name "neovim-ctrl") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "neovim-lib") (req "^0.6") (default-features #t) (kind 0)))) (hash "0bh15gh12hca3cfkmka3a4xdscqvz2gn5dlwlgg39zpqmk0gmdg2")))

(define-public crate-neovim-ctrl-0.1 (crate (name "neovim-ctrl") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "neovim-lib") (req "^0.6") (default-features #t) (kind 0)))) (hash "1lj8xbahigmx8p889zwkzflbjmgv8jx8qvdvscsvhs5py12lfwkv")))

(define-public crate-neovim-ctrl-0.2 (crate (name "neovim-ctrl") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "neovim-lib") (req "^0.6") (default-features #t) (kind 0)))) (hash "0jqsxf2wx81isl3qrw90c28fwbj7j8gzfr9vqlrz6r7zsy9sbsmb")))

(define-public crate-neovim-lib-0.1 (crate (name "neovim-lib") (vers "0.1.0") (deps (list (crate-dep (name "rmp") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rmp-serialize") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0c2659sf11ay94byjabhrnmfvxif5ndc4a91lksyrlh1ip5zf4h9")))

(define-public crate-neovim-lib-0.1 (crate (name "neovim-lib") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rmp-serialize") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vbcs6z7alazxi2p6xlv71fiwn06xyl14n9f1lq4r3x2afy2yimk")))

(define-public crate-neovim-lib-0.1 (crate (name "neovim-lib") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rmp-serialize") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0aknkmwqw6l4p4kr7niy73jymxcl0qdgmshqhmazxhpjjya2lrx6")))

(define-public crate-neovim-lib-0.2 (crate (name "neovim-lib") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)))) (hash "1cjyzbp3qc51f37qkvjwh3nrkgyyki7kgarghfdhz0js5q2cxc4f")))

(define-public crate-neovim-lib-0.2 (crate (name "neovim-lib") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1k0xs2r3ynkvv9bg5xzx7vlh10fvixb6j1v64330irkjmm9493p7")))

(define-public crate-neovim-lib-0.3 (crate (name "neovim-lib") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1jpmszd2glra1f2hivz8h2qbjhbc4bq7sa0jbymrg4g92l5n4hs8")))

(define-public crate-neovim-lib-0.4 (crate (name "neovim-lib") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "06xy808pims2yddk35z3d0ycab89p1fsdy49q35m8iy63s37f15a")))

(define-public crate-neovim-lib-0.4 (crate (name "neovim-lib") (vers "0.4.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1fgi4vy5knqyzmdxiy5ja8as6kdnl3pc27w6jpg9kvidb9vczx64")))

(define-public crate-neovim-lib-0.4 (crate (name "neovim-lib") (vers "0.4.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0sm71xrnx7jk4z6scn15mvyyac073f8fbsvm2zkcp7rh80hwv0fd")))

(define-public crate-neovim-lib-0.4 (crate (name "neovim-lib") (vers "0.4.3") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0m9zhb3jnfji79l01adyjx88a6ncxb51h5brq6gx7mp4myb74fv9")))

(define-public crate-neovim-lib-0.5 (crate (name "neovim-lib") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1v1gv3nrylmrl8y88qiaz1599h4pzmpjcflx7wgv2rpcrjgvg28q")))

(define-public crate-neovim-lib-0.5 (crate (name "neovim-lib") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "15pnrr0n5q872gd5jn8c9jdwp3srlqcnabbxzpry6csy9vyphlyv")))

(define-public crate-neovim-lib-0.5 (crate (name "neovim-lib") (vers "0.5.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "05w1l5mkcrynb1yjvqk3m2vpwrfzgxv774kxilllz5xmknr6gc1r")))

(define-public crate-neovim-lib-0.5 (crate (name "neovim-lib") (vers "0.5.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0i88s7m56lg8qrjmrwf6zni3dgqab0h2c2i54zarhs4i3yrwhd7g")))

(define-public crate-neovim-lib-0.5 (crate (name "neovim-lib") (vers "0.5.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0z3k64lmvy1agxwbqpggs6cqi7p7jnkxphdxriq471bjvrc6iml9")))

(define-public crate-neovim-lib-0.6 (crate (name "neovim-lib") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1ansakda16jnzhhkbhvwpdagcbqzknis2fxpk38dpra6x3kg4vas")))

(define-public crate-neovim-lib-0.6 (crate (name "neovim-lib") (vers "0.6.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rmpv") (req "^0.4") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "11zmf0h7khbhfgyzljjxa8x6zpjalfaw9hk9nvi0q5myw6hzba6n")))

(define-public crate-neovim-rs-0.0.1 (crate (name "neovim-rs") (vers "0.0.1") (hash "1a3fazdxpnmfpbwdqlfyr06pymlzp49rlwy4zlk30lddrcrryfwi")))

(define-public crate-neovim-rs-0.0.2 (crate (name "neovim-rs") (vers "0.0.2") (hash "0gyxns519na9gzzsbv4n6w89h1xq80arigq5sd3cl1gh1vv7xifc")))

(define-public crate-neovim-rs-0.0.3 (crate (name "neovim-rs") (vers "0.0.3") (hash "1acnrg11bqpr15l8blgvcm896i9b4zawydi15yvwzal7g8hpllgx")))

(define-public crate-neovim-rs-0.0.4 (crate (name "neovim-rs") (vers "0.0.4") (hash "0fd6ivy7fspja89x4ggnbaxsz3sdckqaf6x1bi8lpqc7db98mssr")))

(define-public crate-neovim-rs-0.0.5 (crate (name "neovim-rs") (vers "0.0.5") (hash "1m891mv03cvl8kdqwisbpa4q9c3f19hzv4cld3n38km636kqzw7l")))

(define-public crate-neovim-rs-0.0.6 (crate (name "neovim-rs") (vers "0.0.6") (hash "1kk0fn276477ngv61zfjh609yxns5f9wnwmvgafb225x5q2kjc6r")))

(define-public crate-neovim-rs-0.0.7 (crate (name "neovim-rs") (vers "0.0.7") (hash "1634ba8ijxrbdv4b9cmhasndd7dy61iaxwmyl3klf30544dbi0xx")))

(define-public crate-neovim-rs-0.0.8 (crate (name "neovim-rs") (vers "0.0.8") (hash "0gdrjmg89b9pw6vdjk21phqbcipvgjfkfnsbgdl4kiqvnd9bvlrf")))

(define-public crate-neovim-rs-0.0.9 (crate (name "neovim-rs") (vers "0.0.9") (hash "0pydr10j514flpg66d2wrvydv4iwpbkv8p54vamc9q3jfy92c105")))

(define-public crate-neovim-rs-0.0.10 (crate (name "neovim-rs") (vers "0.0.10") (hash "0nzhd7wpdzaja1wv0mn2dxkpfcw68wxsjrgryx2h3yw1nykqiryb")))

(define-public crate-neovim-rs-0.0.11 (crate (name "neovim-rs") (vers "0.0.11") (hash "1hrqzqbmdicmiv80vsxknh09dwciql1pnz42d1w71lrh7vrpp1ml")))

(define-public crate-neovim-rs-0.0.12 (crate (name "neovim-rs") (vers "0.0.12") (hash "1q2fcknchzbysrhw6ch9j211y0q0i7rymln416dmg5zmca8kmbgf")))

(define-public crate-neovim-rs-0.1 (crate (name "neovim-rs") (vers "0.1.0") (hash "0v79bn8im4v3dga395wvbr0gsziz2wq4ic3axjj0ll6hqaw20fdr")))

(define-public crate-neovim-twitch-chat-1 (crate (name "neovim-twitch-chat") (vers "1.0.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "neovim-lib") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "twitch-irc") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1xvhk9dj3qkmrwn48cpf5xgwppfn6lgxkkyvhps3p4zb1dhx12x9")))

(define-public crate-neovim-twitch-chat-1 (crate (name "neovim-twitch-chat") (vers "1.0.2") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "neovim-lib") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "twitch-irc") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0kf3rw74k7fbh8n9x468h26ghsi4gqkpvn69wik6zwv80krkkal1")))

(define-public crate-neovim-twitch-chat-1 (crate (name "neovim-twitch-chat") (vers "1.0.3") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "neovim-lib") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("rt" "rt-multi-thread" "time" "sync" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "twitch-irc") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1w4ddi5ly60slb1dj5dzkf24p5rvrj3ghbri3zbvnyal6z01q0w6")))

