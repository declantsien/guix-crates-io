(define-module (crates-io ne dr) #:use-module (crates-io))

(define-public crate-nedry-0.1 (crate (name "nedry") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lang-c") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "1pha9a97dxq2mqmpva60l8bl7llg1rx66w6m62azc1qriyn1g32r")))

