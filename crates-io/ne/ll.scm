(define-module (crates-io ne ll) #:use-module (crates-io))

(define-public crate-nell-0.0.0 (crate (name "nell") (vers "0.0.0") (hash "0ahd0rscaarsnygcjilz1wgb2pxddgcmx1byhl5ic7aqbpg0vzsl")))

(define-public crate-nell-0.0.1 (crate (name "nell") (vers "0.0.1") (deps (list (crate-dep (name "errno") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)))) (hash "1g0q3hg20nr941y7drx4kilarh9mmma6qngx0y21diclqi8l6piz")))

(define-public crate-nell-0.0.2 (crate (name "nell") (vers "0.0.2") (deps (list (crate-dep (name "errno") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)))) (hash "0b61k35wkwasylaclj361faj8ww16pzk4ny99v923g4rw15wqbga")))

(define-public crate-nell-0.0.3 (crate (name "nell") (vers "0.0.3") (deps (list (crate-dep (name "errno") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)))) (hash "14476gbr5xhwl35vs38ryf2bi74iiarsssyrdkry64s4n1d8y5fr")))

(define-public crate-nell-0.1 (crate (name "nell") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.18") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "0z468fcwbw4iivkd2slsyw4b7zj2x138xx58is0y1yd84nmcmz9n") (features (quote (("sync") ("default" "sync"))))))

(define-public crate-nell-0.2 (crate (name "nell") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.19") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "078259lp714lxkqpp8snydw60s1l7nr4c7amv2frygjqs43l6h0f") (features (quote (("sync") ("default" "sync"))))))

(define-public crate-nell-0.3 (crate (name "nell") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.70") (default-features #t) (kind 0)))) (hash "1yzwfdsq9142f9vlywiabpdp08idwbza0jgp0vs8n606yq6ajjn0")))

