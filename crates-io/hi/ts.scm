(define-module (crates-io hi ts) #:use-module (crates-io))

(define-public crate-hitsound-copier-0.0.1 (crate (name "hitsound-copier") (vers "0.0.1") (deps (list (crate-dep (name "libosu") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0jygh17p3xv5p2f03l97cf12ivi1984sy16sq7vhv2jhl3cmfp1j")))

