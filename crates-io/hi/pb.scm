(define-module (crates-io hi pb) #:use-module (crates-io))

(define-public crate-hipblas-sys-0.1 (crate (name "hipblas-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "160wlk4wxh29s89gviyis15vbwjng50hz15yx1k3vfsbxlv69f1p") (links "hipblas")))

