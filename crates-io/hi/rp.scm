(define-module (crates-io hi rp) #:use-module (crates-io))

(define-public crate-hirpdag-0.1 (crate (name "hirpdag") (vers "0.1.0") (deps (list (crate-dep (name "hirpdag_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hirpdag_hashconsing") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "17c6ln48h9wxi22x4wjfdv2igwdkr5azg9wi0jrw76lwzwk2fy0m")))

(define-public crate-hirpdag-0.1 (crate (name "hirpdag") (vers "0.1.1") (deps (list (crate-dep (name "hirpdag_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "hirpdag_hashconsing") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "115ypa0xd07jc62jp6ha5q5gkffbbpf9l3d90glj2z193gi0gk7b")))

(define-public crate-hirpdag_derive-0.1 (crate (name "hirpdag_derive") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit"))) (default-features #t) (kind 0)))) (hash "1fdgvw3g2xxkkp59s7pn83q7w93vmzjmhg9q2zljcq43c8bmxaxx")))

(define-public crate-hirpdag_derive-0.1 (crate (name "hirpdag_derive") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit"))) (default-features #t) (kind 0)))) (hash "1ym24j1ksp4fwmxlbc1s20z9r7wm4psvd4kx86z34p2kg1vgbgdn")))

(define-public crate-hirpdag_hashconsing-0.1 (crate (name "hirpdag_hashconsing") (vers "0.1.0") (deps (list (crate-dep (name "array-init") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "weak-table") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "00ddi03frqcaxw2rdmr1ipmw4hji7qp07qfj9j442dj783h0fkly")))

(define-public crate-hirpdag_hashconsing-0.1 (crate (name "hirpdag_hashconsing") (vers "0.1.1") (deps (list (crate-dep (name "array-init") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "weak-table") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "07f0l1dvq6rhfqzv6km2kss9hixr1kqjrfja44bvhj1psl156waq")))

