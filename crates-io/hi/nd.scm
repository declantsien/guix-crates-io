(define-module (crates-io hi nd) #:use-module (crates-io))

(define-public crate-hindley-milner-0.1 (crate (name "hindley-milner") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0rw3a8waz08fw50h5pqv9rmnbgaxim28c7bss67sab6c0py4b2dr")))

