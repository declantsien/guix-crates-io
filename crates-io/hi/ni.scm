(define-module (crates-io hi ni) #:use-module (crates-io))

(define-public crate-hinix-0.1 (crate (name "hinix") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11") (default-features #t) (kind 0)))) (hash "0hs82hz6njcp69y57a0pi734kq5x63n5vdyr7hbcvn4nkf3w0y14")))

(define-public crate-hinix-0.2 (crate (name "hinix") (vers "0.2.0") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "1v5xw6ik8q4a695zfih47mri6x1pj8cfixdzpczcksz3ahcnklpm")))

(define-public crate-hinix-0.2 (crate (name "hinix") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.34") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 0)))) (hash "07gvqkmbfj9hhsd7d1fi8l6n5kky5kzflpgmjl9264dj68p3qmhg") (features (quote (("utils" "clap") ("default"))))))

