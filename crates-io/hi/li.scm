(define-module (crates-io hi li) #:use-module (crates-io))

(define-public crate-hilib-0.1 (crate (name "hilib") (vers "0.1.0") (deps (list (crate-dep (name "dmidecode-rs") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "smbios-lib") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (default-features #t) (kind 0)))) (hash "1sikzi2hbf0naaayc71svdvzz8gz27mq6js6byscbgqlrwyndsd4") (yanked #t)))

(define-public crate-hilite-1 (crate (name "hilite") (vers "1.0.0") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "04zy4q211fi24smfjlxnfggmm9xmzvbk48pl8p4qixhsf9jv6hpk")))

(define-public crate-hilite-1 (crate (name "hilite") (vers "1.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0h8c8v9cql3jp1sqk95cbvsmz56cwwarvb13h9x9a8bakgpyx8cl")))

