(define-module (crates-io hi me) #:use-module (crates-io))

(define-public crate-hime_compiler-4 (crate (name "hime_compiler") (vers "4.0.0") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.0.0") (features (quote ("print_errors"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1car5adxiyzsbjplrpz4xmy7bkfdgh3rks5g4abll0i01d6bzmk5")))

(define-public crate-hime_compiler-4 (crate (name "hime_compiler") (vers "4.1.0") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.1.0") (features (quote ("print_errors"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1fnplibd55qa7dcjfl95c9h76nrim3i6j72093kgaa00r2bd74r1")))

(define-public crate-hime_compiler-4 (crate (name "hime_compiler") (vers "4.2.0") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.2.0") (features (quote ("print_errors"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "09ivlwpcly6kvsmc1l1mynaz2vrh5dz9f7xsnlb8c1x1f2gf366c")))

(define-public crate-hime_compiler-4 (crate (name "hime_compiler") (vers "4.2.2") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.2.2") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.2.2") (features (quote ("print_errors"))) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.6") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1314gj6dlwpr6nqh7qx3clhg09yc0xrjvbvaaixcqwxvydmpn6bn")))

(define-public crate-hime_compiler-4 (crate (name "hime_compiler") (vers "4.2.4") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.2.4") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.2.4") (features (quote ("print_errors"))) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.6") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "162z9i678pi9ljwjkzp1l71yqfjhx5l2x8w6pzyg6ldvrkvyp6cs")))

(define-public crate-hime_compiler-4 (crate (name "hime_compiler") (vers "4.3.0") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.3.0") (features (quote ("print_errors"))) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.6") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1v84qsk8fqxms7y8pgl99x6dwqydl3g0w3df0zdbpcw296mphmjf")))

(define-public crate-hime_langserv-4 (crate (name "hime_langserv") (vers "4.2.2") (deps (list (crate-dep (name "clap") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.2.2") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.17") (default-features #t) (kind 0)))) (hash "09clvx47blfasj1ylffb3ldl8dj6dsrq32rdcishhkd4ixvsvd8p")))

(define-public crate-hime_langserv-4 (crate (name "hime_langserv") (vers "4.2.4") (deps (list (crate-dep (name "clap") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.2.4") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.2.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.17") (default-features #t) (kind 0)))) (hash "0y23q3k8ys2p1kcbyrp10m725xsv283cvdkbnl65j80gp4g0q8vw")))

(define-public crate-hime_langserv-4 (crate (name "hime_langserv") (vers "4.3.0") (deps (list (crate-dep (name "clap") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "hime_sdk") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.17") (default-features #t) (kind 0)))) (hash "0n43570s67r07nckhlsiw813bjgp6irlkpsiij7dq7927nh2rgd7")))

(define-public crate-hime_redist-3 (crate (name "hime_redist") (vers "3.3.0") (hash "19rma7mkix5rmxgizbcqsrb446j60bd39z5xkifyj0njalx8r7sg")))

(define-public crate-hime_redist-3 (crate (name "hime_redist") (vers "3.3.1") (hash "0nvywwh8rzk2ckihbi2g50zbv9fqacv6l72afklmgmx4ysnqkc19")))

(define-public crate-hime_redist-3 (crate (name "hime_redist") (vers "3.3.2") (hash "0m89s5bx77qshk1k8fg6f03mm9hzy535hp840wqr22cy18l0qzcb")))

(define-public crate-hime_redist-3 (crate (name "hime_redist") (vers "3.4.0") (hash "1a76k8dd11ar9mzri4lzbv19brbjk13z5pyp2hy1j3ybh5s67ayz")))

(define-public crate-hime_redist-3 (crate (name "hime_redist") (vers "3.4.1") (hash "15sbnrhah7piwm53s6962xxzcpwjz8884bmsqpch9q30shdps00b")))

(define-public crate-hime_redist-3 (crate (name "hime_redist") (vers "3.5.0") (hash "17w7h5fbqx4jvfqzwi7j0xrvm2fwixcwfcfflwkl9wk5jjzikwlh")))

(define-public crate-hime_redist-3 (crate (name "hime_redist") (vers "3.5.1") (hash "17fqm35kl74ymwkx7llmhc5jbkpvq8aw9yk7igzd98krxxcfnylq")))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.0.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1x8mdknwz4xzcrg0pibr45qsg28n8pnr91sfy84h281ki4wv74yc")))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.1.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "06v82wm0b8hxkb1bh059za3y26r8g4pkhli8k5a1vf34apm1r2ln") (yanked #t)))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "0ii93fd71whcv86dlc446mdycmdhlvp9h4hxcsy6h18f7kbpwiaa")))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.1.2") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1kkiqc4bkh82j0sf1sxxawdmxqb8mccpag2x0ybywczam0jzc5wr")))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.2.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "09k847vlgwhnnzrjb5bycs62vvjc4ly48zwgns7idcm07igvrkhv")))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.2.2") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1bg700jnb2mzvq3w0k4458803fvs88k47hrhdxv9kdlprmdpi4hb")))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.2.3") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1zbyvmkp3cfxxd2yjdbjbgw4h5p1447h1l9h13km47jjrzb0lns9")))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.2.4") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1j55bmibb1zpcb2r9vqxpcga94msx38sixx24z2jf898qgybvrpx") (features (quote (("default") ("debug"))))))

(define-public crate-hime_redist-4 (crate (name "hime_redist") (vers "4.3.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive" "alloc"))) (kind 0)))) (hash "05gaawdvnmdp9x5lw6rsfby510jqlaa7rfr5i3vswgiwf4gj2crf") (features (quote (("std" "serde/std") ("default" "std") ("debug"))))))

(define-public crate-hime_sdk-4 (crate (name "hime_sdk") (vers "4.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1n3l98qvjl5mdb0p5qqfxf44hrq51wj487fna9bjxjvf5fyyr1q3") (features (quote (("print_errors" "ansi_term") ("default"))))))

(define-public crate-hime_sdk-4 (crate (name "hime_sdk") (vers "4.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1kymy1na8pi5zds1qbvw93m89pbs25xxf3i55268p9ndqmf6iz9a") (features (quote (("print_errors" "ansi_term") ("default"))))))

(define-public crate-hime_sdk-4 (crate (name "hime_sdk") (vers "4.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hime_redist") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "19db00lbhjyim2xg7mj6b2dl11mnksc9s5h9r5h28x767xk07m83") (features (quote (("print_errors" "ansi_term") ("default"))))))

(define-public crate-hime_sdk-4 (crate (name "hime_sdk") (vers "4.2.2") (deps (list (crate-dep (name "hime_redist") (req "^4.2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "11ly5vmsrmzvf41h3si2gnik60fvdx6cxjc62gc97lk6cz9i47fn") (features (quote (("print_errors" "miette") ("default"))))))

(define-public crate-hime_sdk-4 (crate (name "hime_sdk") (vers "4.2.4") (deps (list (crate-dep (name "hime_redist") (req "^4.2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1cj0j0n84sh3brfpj965av3368sph7v2z5nw7pk46qq6w4dr77v6") (features (quote (("print_errors" "miette") ("default"))))))

(define-public crate-hime_sdk-4 (crate (name "hime_sdk") (vers "4.3.0") (deps (list (crate-dep (name "hime_redist") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0dppxwjn088qmdc7mxb649i2g8laan4458i8wmvw54hri3b0y5zl") (features (quote (("print_errors" "miette") ("default"))))))

(define-public crate-himetake-0.1 (crate (name "himetake") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "kusabira") (req "^0.1.0-alpha0") (default-features #t) (kind 0)) (crate-dep (name "kusabira") (req "^0.1.0-alpha0") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.1") (default-features #t) (kind 1)))) (hash "0cpbfsnvy5irv9b6lh7qlrc7swvkg74n6az97jpf59rpv6k3kc1f")))

(define-public crate-himetake-0.1 (crate (name "himetake") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "kusabira") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "kusabira") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.1") (default-features #t) (kind 1)))) (hash "1pi2h9lb18jw6nb13misa61pdqzsyidx7bzlcnv3k19gabdrm4cm")))

(define-public crate-himetake-0.1 (crate (name "himetake") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "kusabira") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "kusabira") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.1") (default-features #t) (kind 1)))) (hash "0lafd3x7ih2gsxl48y105l2ml58kwcfzaywxszv3cnppqps1zic7")))

