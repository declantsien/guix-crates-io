(define-module (crates-io hi lb) #:use-module (crates-io))

(define-public crate-hilbert-0.1 (crate (name "hilbert") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "128mzh443x3j34c6brfdqqxn36wxakbkjiwjfi23x645bgl71inr")))

(define-public crate-hilbert-0.1 (crate (name "hilbert") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "16ys6bnnlxjdndvaqgpvv6g21j6r78zddwfgdbbnk53q7b2px89b")))

(define-public crate-hilbert-0.1 (crate (name "hilbert") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1y6az4r6xr6kk6b201d98wblvzl34s7ynf2f4sm3ba832s08738p")))

(define-public crate-hilbert-c2rust-0.1 (crate (name "hilbert-c2rust") (vers "0.1.0") (hash "10fcnbbhgpmp6hq97m2f1rrbjp6c4v4xy2j4swgbcmr5l7zxrykw")))

(define-public crate-hilbert-c2rust-0.1 (crate (name "hilbert-c2rust") (vers "0.1.1") (hash "0d7h4q7wbi1k3ppr860z5jca6l1q12bchc8xfcz6rff6ih7wi662")))

(define-public crate-hilbert-c2rust-0.1 (crate (name "hilbert-c2rust") (vers "0.1.2") (hash "0h2f9c5frq9ky2175l0m8pn84bvbrvh7y3b88wv3x4jsscjjh9sz")))

(define-public crate-hilbert-c2rust-0.1 (crate (name "hilbert-c2rust") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "1wbx42idi9cczklv0hp39nrrm65dmxpvfhb91lass9d68a1la1cn")))

(define-public crate-hilbert-c2rust-0.1 (crate (name "hilbert-c2rust") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "1v423yicxmyjjymja8ppmddjhg95glhkq9pjxd3n1zmzz8lci3n3")))

(define-public crate-hilbert-c2rust-0.1 (crate (name "hilbert-c2rust") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "1gzcfms89nhbz7pdm56gidgf1rax3h5m2aiyhp0pqqccy84rw15i")))

(define-public crate-hilbert-curve-rust-0.1 (crate (name "hilbert-curve-rust") (vers "0.1.0") (hash "07m9f2w5asrgv2qzfcr3havyz4vfasickgi7044ig5xfhfxnz9za")))

(define-public crate-hilbert-curve-rust-0.1 (crate (name "hilbert-curve-rust") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "fast_hilbert") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "hilbert") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "hilbert_2d") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "hilbert_curve") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1gjh3nzkvvmja80iif1875gi9hkc2gbp1sr2ljzfqb07pdxfrlrj")))

(define-public crate-hilbert16-0.1 (crate (name "hilbert16") (vers "0.1.0") (hash "1s3r7xlar692fn01mf308ypmhgbs38hmkl39v5mnig1j4imv4pbf")))

(define-public crate-hilbert_2d-1 (crate (name "hilbert_2d") (vers "1.0.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "palette") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1xqis4rdndrsqy2hny6m5byaiqxdyrdg8lrax7jxbwj08jyg3991")))

(define-public crate-hilbert_2d-1 (crate (name "hilbert_2d") (vers "1.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "palette") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0p5yz3wjfbp7jlngl8868f59d2jznvvcf0f76npk85xi8bh82pvh")))

(define-public crate-hilbert_curve-0.1 (crate (name "hilbert_curve") (vers "0.1.0") (hash "192nnyj1dmk1hz9shq57s90dvxxzsbkhhd7ngr6fk4cc6y7kkd2m")))

(define-public crate-hilbert_curve-0.2 (crate (name "hilbert_curve") (vers "0.2.0") (hash "1nbrfwixlnhj7w4b03x7ink1nb2zycrn3mhjiarsxjdcwb4gr601")))

(define-public crate-hilbert_curve_generator-0.1 (crate (name "hilbert_curve_generator") (vers "0.1.0") (hash "0c23vpi4bhh32mm7idxlamz255sq9ap8hmll5vh2n88bwv07x2fm")))

(define-public crate-hilbert_curve_generator-0.1 (crate (name "hilbert_curve_generator") (vers "0.1.1") (hash "08jsp0l0ifhz98ka7xz016adz51lp7y13n1rs83dkpr5an81rqsn")))

(define-public crate-hilbert_curve_generator-0.1 (crate (name "hilbert_curve_generator") (vers "0.1.2") (hash "04szp2jvpkph7na1z63x11k6sqr6pxnwhh9wgfgb0a75xh3ip0v5")))

(define-public crate-hilbert_image_to_sound-0.1 (crate (name "hilbert_image_to_sound") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hilbert_curve") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "1z6nmmf1xnhdgw705hnadcp4v4fmyv88q6s3ykzj3w5kld1yyaa8")))

(define-public crate-hilbert_image_to_sound-0.1 (crate (name "hilbert_image_to_sound") (vers "0.1.1") (deps (list (crate-dep (name "cpal") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hilbert_curve") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "1khs6a1gdhz2qq8f1z713rfl31iack9z854gvf9amqi9dzkb9b88")))

(define-public crate-hilbert_index-0.1 (crate (name "hilbert_index") (vers "0.1.0") (hash "0kj12f46axj7arriqav2rlis6m468nvsp40dji86rddpa50xan1x")))

(define-public crate-hilbert_index-0.2 (crate (name "hilbert_index") (vers "0.2.0") (hash "0876zkiyr3az59g10cdg6jsh334nxgnajfi2msdlwkw8aqfa9jmh")))

(define-public crate-hilbert_transform-0.1 (crate (name "hilbert_transform") (vers "0.1.0") (deps (list (crate-dep (name "rustfft") (req "^6.1.0") (default-features #t) (kind 0)))) (hash "0s9l7gdpfv54bb3fsm8ir4kfbaahmfl5is4kcgcbdxvgqij092wn")))

(define-public crate-hilbert_transform-0.1 (crate (name "hilbert_transform") (vers "0.1.1") (deps (list (crate-dep (name "rustfft") (req "^6.1.0") (default-features #t) (kind 0)))) (hash "0a9hkcr4pj1qhpa7mx7bkfi2kak1k4n89bvsw3pln80qax5rwkw5")))

