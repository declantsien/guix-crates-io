(define-module (crates-io hi tr) #:use-module (crates-io))

(define-public crate-hitrace-0.1 (crate (name "hitrace") (vers "0.1.0") (deps (list (crate-dep (name "hitrace-sys") (req "^0.1") (default-features #t) (target "cfg(target_env = \"ohos\")") (kind 0)))) (hash "0hgvvkrbv70b5rwfks9ryzrsmrlxxn1ixj1zzrcyyz9lz09ln0qa")))

(define-public crate-hitrace-0.1 (crate (name "hitrace") (vers "0.1.1") (deps (list (crate-dep (name "hitrace-sys") (req "^0.1") (default-features #t) (target "cfg(target_env = \"ohos\")") (kind 0)))) (hash "1wa0cnjd7kpj5khxg9rb1nc043v58y7q4p0k5akhgk20mx2633qf")))

(define-public crate-hitrace-0.1 (crate (name "hitrace") (vers "0.1.2") (deps (list (crate-dep (name "hitrace-sys") (req "^0.1") (default-features #t) (target "cfg(target_env = \"ohos\")") (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1gjcvswyf154d85y9rcqx88zl4gk3rn1xra8cvdlnl0hhjnp86kb")))

(define-public crate-hitrace-0.1 (crate (name "hitrace") (vers "0.1.3") (deps (list (crate-dep (name "hitrace-sys") (req "^0.1") (default-features #t) (target "cfg(target_env = \"ohos\")") (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0abknlhfplskr9y11w9xixd2phwvx101pivm8433ipg8l1k1xfy0")))

(define-public crate-hitrace-0.1 (crate (name "hitrace") (vers "0.1.4") (deps (list (crate-dep (name "hitrace-sys") (req "^0.1") (default-features #t) (target "cfg(target_env = \"ohos\")") (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0vigmwx7p83r6yqmi576n9vz8r38nniznhqih7mylchbygk0lb7r") (features (quote (("max_level_off"))))))

(define-public crate-hitrace-macro-0.1 (crate (name "hitrace-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("parsing" "full"))) (default-features #t) (kind 0)))) (hash "0g9di1c05vsz99fpkdf5ffs54pvasqg43l1scs1j2lb524xyh7yh")))

(define-public crate-hitrace-sys-0.1 (crate (name "hitrace-sys") (vers "0.1.0") (hash "06ynfbvykvhsnpbbzh40agj1r8kap882qma338778mq7lkjmf3cx")))

(define-public crate-hitrace-sys-0.1 (crate (name "hitrace-sys") (vers "0.1.1") (hash "0fhw5cvjv4i9rm3nnvq64pvh9d9rjbkr0pfbz3ixz245wp9lyl1d")))

(define-public crate-hitree-0.1 (crate (name "hitree") (vers "0.1.0") (hash "1c0fim3zi3yx0wd2mv0gacx0qg2d5yx7igxjlmwpfqwii79qjrnm") (yanked #t) (rust-version "1.60")))

(define-public crate-hitree-0.1 (crate (name "hitree") (vers "0.1.1") (hash "104jvs1phws4wnnrm130ihk4rca19vkq4xiv8xwv2g1zc8a1x62d") (rust-version "1.60")))

(define-public crate-hitree-0.1 (crate (name "hitree") (vers "0.1.2") (hash "02m0zr7466mj3gqjvk6abp3ac9hdnmb1jvfw279rcw2albp6hjqf") (rust-version "1.60")))

(define-public crate-hitree-0.1 (crate (name "hitree") (vers "0.1.3") (hash "0wjdkzd4g2md9084ml6yr044lmvnapzg14fpw2cz1237gf7h2mvm") (rust-version "1.60")))

(define-public crate-hitree-0.1 (crate (name "hitree") (vers "0.1.4") (hash "0f2spn52i1az8ssnvqdl25z0gv079wirb0rlq4c6skcfw50sdqy7") (rust-version "1.60")))

