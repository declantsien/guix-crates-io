(define-module (crates-io hi jr) #:use-module (crates-io))

(define-public crate-hijri_date-0.1 (crate (name "hijri_date") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0h3znv6m12170kawzyxrmjp8ggc5af1y69hqpkd2k21ddby69nd2")))

(define-public crate-hijri_date-0.1 (crate (name "hijri_date") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1055i9vbj2r40k0fqd3wg61jp8s5z03b1xa844nbrnff5v6n8vfq")))

(define-public crate-hijri_date-0.1 (crate (name "hijri_date") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1255l84fq3l0ljwwb3c5r7z30vwjl70n51a89pzdv1anr5z2s07n")))

(define-public crate-hijri_date-0.1 (crate (name "hijri_date") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1x3id3wqv73j3pjh4ra75a2p28nx10rw6zb3cq8qjgz7kqa9a2g4")))

(define-public crate-hijri_date-0.1 (crate (name "hijri_date") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1cns5wkkf2m6m5h9ikhqd8iv45nmx4srnnqbig5ygnvqbxri88wy")))

(define-public crate-hijri_date-0.1 (crate (name "hijri_date") (vers "0.1.5") (deps (list (crate-dep (name "arabic_reshaper") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1xx7gghn6fkgp0bcjazrj09c4l132cmxn0w8ynz3wr9rbmsshpzn")))

(define-public crate-hijri_date-0.1 (crate (name "hijri_date") (vers "0.1.6") (deps (list (crate-dep (name "arabic_reshaper") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0al39bwkfwxv4swl4aap3d08gq572g6am3k7pgz1d7wwkdcab4f2")))

(define-public crate-hijri_date-0.2 (crate (name "hijri_date") (vers "0.2.0") (deps (list (crate-dep (name "arabic_reshaper") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "001zgry6p60sqm454n0wl2nx6b16b76dv5cvkwh6xqinhy8y8h4n")))

(define-public crate-hijri_date-0.2 (crate (name "hijri_date") (vers "0.2.1") (deps (list (crate-dep (name "arabic_reshaper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1dbwk9gnbzahmqxnzw11br2imdzv2l0gl7xpxkiwccl32kxyp51l")))

(define-public crate-hijri_date-0.2 (crate (name "hijri_date") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0x0n96pif63zak393l9l5iham0dckfry8g9qikwcza4aaxkpmbb7")))

(define-public crate-hijri_date-0.3 (crate (name "hijri_date") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0nidmjzdfhpmzvxl68b0vjcd14x0lspa18rg61dz8l7hgrzlss64")))

(define-public crate-hijri_date-0.3 (crate (name "hijri_date") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "10vhibm8vdsba6rm2yxcjzn6bbhl8r2vs1hdgxvc9kklb86bkrxn")))

(define-public crate-hijri_date-0.3 (crate (name "hijri_date") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "028hwx182zslglsm5iphrpkdqsrdailq2p1nk318c6syxiiaxj4l")))

(define-public crate-hijri_date-0.4 (crate (name "hijri_date") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0q621dsr58nrpfwasz28wfpgi5pkxjygipiza2g224z9rf3n0gf4")))

(define-public crate-hijri_date-0.4 (crate (name "hijri_date") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)))) (hash "1jc6hnyqkns9l5sjdljpc1q4m4064nlgyysi53681l8b798lr4vk")))

(define-public crate-hijri_date-0.5 (crate (name "hijri_date") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "14x33s7mm4zqsj3drk2r1021623c7mr74iw8aa2wb6qww9ck5s84")))

(define-public crate-hijri_date-0.5 (crate (name "hijri_date") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "00jn4c2lnzdhmwl9hphgpm6dcwms0lij54l4frnw1k4b1h7vp1x3")))

