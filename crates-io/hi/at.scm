(define-module (crates-io hi at) #:use-module (crates-io))

(define-public crate-hiatus-0.1 (crate (name "hiatus") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1ii964870q3xn7g87y8bk55n6ial02w3bjx05cmv561w4hvbkv2h")))

(define-public crate-hiatus-0.1 (crate (name "hiatus") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1f69m9l3mr13940ff7kzz05x0davba0gb9057wbci58wzcqggbgh")))

