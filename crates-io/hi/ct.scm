(define-module (crates-io hi ct) #:use-module (crates-io))

(define-public crate-hictl-0.1 (crate (name "hictl") (vers "0.1.0") (deps (list (crate-dep (name "hilib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("macros" "rt-multi-thread" "full"))) (default-features #t) (kind 0)))) (hash "1l2rc60wai9mh1dm3qch8kwj8wk8nh0bqp3nc5hyz4smxsssy0vp") (yanked #t)))

(define-public crate-hictor-0.1 (crate (name "hictor") (vers "0.1.0") (hash "09709lc12yprmdfy6s64hnb1hqwi3f7400h0f6677754p36amgzi")))

(define-public crate-hictor-0.1 (crate (name "hictor") (vers "0.1.1") (hash "1l9ji23bkh48jn0y19k65xa1xskcyap0q8mvdx7fwb9l19bp6xbg")))

(define-public crate-hictor-0.1 (crate (name "hictor") (vers "0.1.2") (hash "1rwz3vz6gwywf3h47x05a8l4f33swls8rlbyvyyri2jiv33v6ivi")))

(define-public crate-hictor-0.1 (crate (name "hictor") (vers "0.1.3") (hash "0d5z80bbkybznvqjf28pw30baqzcg6hrzaxji895pfh1pl85n5na")))

(define-public crate-hictor-0.1 (crate (name "hictor") (vers "0.1.4") (hash "126namz8pm5xv6dlg65wqn6yvzf1l8r2zs0734lbnsv1vbbwl2sj")))

(define-public crate-hictor-0.1 (crate (name "hictor") (vers "0.1.5") (hash "0f42ld00np5v8rkzqi2nwzg86jb95ms13yws85bgqja9kxlgqsbw")))

(define-public crate-hictor-0.1 (crate (name "hictor") (vers "0.1.6") (hash "0xz8b9zar38mzwrzyp90dxx3k4nsiyf6w4nnqkmp53nr3azr8irl")))

