(define-module (crates-io hi ll) #:use-module (crates-io))

(define-public crate-hill-0.1 (crate (name "hill") (vers "0.1.0") (hash "1n6a2y673wanhsga50a5nnrvdx0kiadsb2xak02gmar1cc6v1ln0")))

(define-public crate-hillock-0.1 (crate (name "hillock") (vers "0.1.0") (hash "127fcbp23jf1sly6f6rzf9zh6cnw5n4ac9fj73wn0m1w4jybiigv")))

