(define-module (crates-io hi cc) #:use-module (crates-io))

(define-public crate-hiccup-0.1 (crate (name "hiccup") (vers "0.1.1") (hash "18x81931m44r59s9vbbgdb7d5mdh0a49gwa43bmk71rrpyyjzyz0")))

(define-public crate-hiccup-0.1 (crate (name "hiccup") (vers "0.1.2") (hash "1ajixs14dfyhzvx3azw3rqbrkddm9s16z9zvzcwhkgvwl39s0959")))

(define-public crate-hiccup-0.1 (crate (name "hiccup") (vers "0.1.3") (hash "0bx6rq3iy9h9xifb39nilf0bm1mnry13yf9n821p7dc94v3sdmxp")))

(define-public crate-hiccup-0.1 (crate (name "hiccup") (vers "0.1.4") (hash "1724fscjwg7ydd5i03h1n985fm5kky304rja05bal0qrdklgg3vw")))

(define-public crate-hiccup-0.1 (crate (name "hiccup") (vers "0.1.5") (hash "0axrjvsx4zcj8caik2djndl3bkhxdkkh64b6r4zgs49g80hs1qy2")))

(define-public crate-hiccup-0.2 (crate (name "hiccup") (vers "0.2.0") (hash "1ffv96cr4lzypljrxqsny27wfy26fx02p2djm7zlac01nabms1bs")))

(define-public crate-hiccup-0.2 (crate (name "hiccup") (vers "0.2.1") (hash "1klilj1p1i6kfmr8xmxkakz6mc78z2wicw8i0rc3bky09jilmnwq")))

(define-public crate-hiccup-0.2 (crate (name "hiccup") (vers "0.2.2") (hash "169lpfs78xhsqjzf2vm5am7yvskjzmg8lvi0kipxacyw214zxlz9")))

(define-public crate-hiccup-0.2 (crate (name "hiccup") (vers "0.2.3") (hash "1y57k25wl6kw5rdxhyj1z5f1z6p61pfybh4s0gihvgd9gs053z0i")))

(define-public crate-hiccup-0.2 (crate (name "hiccup") (vers "0.2.4") (hash "1xld1adnmra8jbli1ygssmi4dxjkydxk9grxczg85na65xaag6g4")))

(define-public crate-hiccup-0.2 (crate (name "hiccup") (vers "0.2.5") (hash "1yhqxgacwgixrb3mgnp56w0az35svf5mwsvnzzldwbfmp1ygvbg4")))

