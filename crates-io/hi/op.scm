(define-module (crates-io hi op) #:use-module (crates-io))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.0") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 2)))) (hash "0psw0vp58s2xiiwvk73ab123hqi47zlq8zkv5k4gh6rwlpr3a39i")))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.1") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 2)))) (hash "06vjndixsb8znp919rbji4nvg6j0dc2sn4d0pxqbfz2rdcik3sbg")))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.2") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 2)))) (hash "171r8g7fdm8l87xm8nms97xjcjpzyfb8rg3cd6s9k8002hqv7djc")))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.3") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 2)))) (hash "09zf9a3xvcwmfgy4zkcmx6dc25y48hpkjxhy9zw0616yqji401ai")))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.4") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 2)))) (hash "12wva41fmfjhl7zzm4yx5gg8pgn2ihxyamhpn6nf6n9f18bl337v")))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.5") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 2)))) (hash "16jb7brqzd38v5j4na6h1nc2cpvpm5y84wzg4mfv3cgjfddga5fc")))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.6") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 2)))) (hash "0km5ndfh3lv60k50j5fpbpc7nr1kfb724n8s4hx1hhfrkrrffrqv")))

(define-public crate-hiopt-0.1 (crate (name "hiopt") (vers "0.1.7") (deps (list (crate-dep (name "hictor") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qvmk7jm7cymm6b5zhs848l9aci74ggdyrm538g7kzkbjsicyjif")))

