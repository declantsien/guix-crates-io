(define-module (crates-io hi vm) #:use-module (crates-io))

(define-public crate-hivm2-0.0.1 (crate (name "hivm2") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "~1.0.0") (default-features #t) (kind 0)))) (hash "0bnbq8v2faxnvvi8jd0cm1sdpn6x6vfhrjx88aybpa0s6fig4gcc")))

(define-public crate-hivm2-0.0.2 (crate (name "hivm2") (vers "0.0.2") (deps (list (crate-dep (name "nom") (req "~1.0.0") (default-features #t) (kind 0)))) (hash "0b4lidqdxxlmmhcc7v3jbb7qsvmya3w55nf09x1j6s6m9mgi47xj")))

