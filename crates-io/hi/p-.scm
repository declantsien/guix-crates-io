(define-module (crates-io hi p-) #:use-module (crates-io))

(define-public crate-hip-runtime-sys-0.1 (crate (name "hip-runtime-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)))) (hash "0npn0lc9safan607dkpdjbfsyl5vhcwdbqgnwvcrsygzrxa5s6lh") (links "amdhip64")))

(define-public crate-hip-sys-0.1 (crate (name "hip-sys") (vers "0.1.1") (deps (list (crate-dep (name "hip-runtime-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hipblas-sys") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "11s2l1wz2jfal3kk0g2dz0mzzqkhmadr97h323y9w95nk2gn56gk") (features (quote (("blas" "hipblas-sys")))) (v 2) (features2 (quote (("bindgen" "hip-runtime-sys/bindgen" "hipblas-sys?/bindgen")))) (rust-version "1.60")))

