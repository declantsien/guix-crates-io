(define-module (crates-io hi te) #:use-module (crates-io))

(define-public crate-hitesh_me-0.1 (crate (name "hitesh_me") (vers "0.1.0") (hash "096cm2x5v97i2mdllgg6r26zmhvnpn2vsm15mdnbg3l4pzc38yfq")))

(define-public crate-hitesh_me-0.1 (crate (name "hitesh_me") (vers "0.1.1") (hash "0f8xg3bhb13wvdjmjmhx3v1wqhcbshndch9xrx8yyfxm9n22f3y1")))

(define-public crate-hitesh_me-0.1 (crate (name "hitesh_me") (vers "0.1.2") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "1l5sxq2kxmf2p884fhniqsghnmib13xlcikg1lg72mgzhs2ql6w4")))

