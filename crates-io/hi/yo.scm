(define-module (crates-io hi yo) #:use-module (crates-io))

(define-public crate-hiyori-0.1 (crate (name "hiyori") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.7") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.131") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.73") (default-features #t) (kind 0)))) (hash "14q502zb31csfybkqh27p9q54fvcrs79kqrqz7j6anx1538ibkgf")))

