(define-module (crates-io hi lo) #:use-module (crates-io))

(define-public crate-hilog-0.1 (crate (name "hilog") (vers "0.1.0") (deps (list (crate-dep (name "env_filter") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hilog-sys") (req "^0.1.1") (features (quote ("log"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)))) (hash "1l3s5qjda5pa22240f27v2f4g5mm1fs0b3z8haf214z28nklcmhd") (rust-version "1.77.0")))

(define-public crate-hilog-binding-0.0.1 (crate (name "hilog-binding") (vers "0.0.1") (hash "1mn0y388shh8cjzpgc8qb5kq4k60cm1c4wmrwra0ag2gzikzfq9z")))

(define-public crate-hilog-binding-0.0.2 (crate (name "hilog-binding") (vers "0.0.2") (hash "1zjf1zsb088cc2i9yjslcqzzrm7hkczr32339g6rak20i34p9vbg")))

(define-public crate-hilog-binding-0.0.3 (crate (name "hilog-binding") (vers "0.0.3") (hash "0apfslpgckbxklglh5j5ln5z21wkq8qxhm643ismwb5agsir86yq")))

(define-public crate-hilog-binding-0.0.4 (crate (name "hilog-binding") (vers "0.0.4") (hash "1j0zfp6vqdh97298a0pq0md5pdh32niq3xck6w9zv36vfgqzhn5z")))

(define-public crate-hilog-sys-0.1 (crate (name "hilog-sys") (vers "0.1.0") (hash "11hmm2vgpd1kr9p765jgfhgpl3gsshsc0fayzdkixpamvhym9aph")))

(define-public crate-hilog-sys-0.1 (crate (name "hilog-sys") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "17mn8qd45mqlsngkr61b3d4gxp72lgzl7jfcymd0p9rlhmgf7q0d") (v 2) (features2 (quote (("log" "dep:log"))))))

