(define-module (crates-io hi nk) #:use-module (crates-io))

(define-public crate-hinku-0.1 (crate (name "hinku") (vers "0.1.0") (deps (list (crate-dep (name "logos") (req "^0.11.4") (optional #t) (default-features #t) (kind 0)))) (hash "1pd0zb284dj251v5nz1jrnph44dd6z45rk86cbsgs7lxvdlv5v0l") (features (quote (("logos_integration" "logos"))))))

