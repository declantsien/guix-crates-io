(define-module (crates-io hi -j) #:use-module (crates-io))

(define-public crate-hi-jira2-0.0.1 (crate (name "hi-jira2") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)))) (hash "0zjmqc0mfqqryknjw73n3hkgsq2sagn3hmlw7p0lc4aq3ridfblg")))

