(define-module (crates-io hi of) #:use-module (crates-io))

(define-public crate-hioff-0.1 (crate (name "hioff") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qy5k5jh9988g0m5qrwiqmmcv6mn143b49vl6ra82j7bvpsxhz5j")))

(define-public crate-hioff-0.1 (crate (name "hioff") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s2yypr8krrvwfylsf6g52fhxdh3sw8ckcipsl870rh6vfdgmc0v")))

