(define-module (crates-io hi eu) #:use-module (crates-io))

(define-public crate-hieu_dummy_rust_lib-0.1 (crate (name "hieu_dummy_rust_lib") (vers "0.1.0") (hash "10frz8ckfcl7rvj74v3d98c7m2wc6p556jv6qln983vc25pv4ss1")))

(define-public crate-hieu_dummy_rust_lib-0.2 (crate (name "hieu_dummy_rust_lib") (vers "0.2.0") (hash "1qbbqnr0cigkc64psyqs257wpdl07ixsvqmlbnhn2nh1rsx99q1m")))

(define-public crate-hieu_dummy_rust_lib-0.3 (crate (name "hieu_dummy_rust_lib") (vers "0.3.0") (hash "1vravd90wh5zcibdkndb671z03rm4dd6w4l0mk561ac0qzz0f6m2")))

(define-public crate-hieu_dummy_rust_lib-0.4 (crate (name "hieu_dummy_rust_lib") (vers "0.4.0") (hash "1r3vx6zv498k4amykm846d3ffdsaqzk2a0rin9phwxyg5irq67rb")))

(define-public crate-hieu_dummy_rust_lib-0.5 (crate (name "hieu_dummy_rust_lib") (vers "0.5.0") (hash "19y0s456bmcv4fxvf4bf3vqjfvrspi39cp89xcb3kkfz031h6kng")))

(define-public crate-hieu_dummy_rust_lib-0.6 (crate (name "hieu_dummy_rust_lib") (vers "0.6.0") (hash "1w825hn90msk22xynp3lp7xva6gcgfywjfjwrrvkggnriikv3zi5")))

(define-public crate-hieu_dummy_rust_lib-0.7 (crate (name "hieu_dummy_rust_lib") (vers "0.7.0") (hash "020yrilh98qmrrx2iwfpbkap559b38i7q3h8yd8dvdk4hamz4hd3")))

(define-public crate-hieu_gen-0.1 (crate (name "hieu_gen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ll63wr5fgdm4gal07s0s9sxn2x63r81gv314y7xv36rkskzhz6c")))

