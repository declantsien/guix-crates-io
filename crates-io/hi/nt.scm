(define-module (crates-io hi nt) #:use-module (crates-io))

(define-public crate-hinted-0.0.0 (crate (name "hinted") (vers "0.0.0") (hash "1wzclvak3446x09353zs6h0hfgrksvcsn1wzrrrn69ai6llcdx6l")))

(define-public crate-hinted-0.0.1 (crate (name "hinted") (vers "0.0.1") (hash "0sdsfx8by5cg2y5rqq677xly7sgw0r4sk7dmq9dpr4g97vfbnrcr") (features (quote (("nightly"))))))

(define-public crate-hinterland-0.0.0 (crate (name "hinterland") (vers "0.0.0") (hash "0a4a343j7dcfxcjvdc00w7qh0djh079r807s284q5ary6g5m7s98")))

