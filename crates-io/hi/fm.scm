(define-module (crates-io hi fm) #:use-module (crates-io))

(define-public crate-hifmt-0.1 (crate (name "hifmt") (vers "0.1.3") (deps (list (crate-dep (name "hifmt-macros") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "12nypk4n7z2rh4glb5r3vif10plxa1w8avvffwg4lh901cj90dn2")))

(define-public crate-hifmt-0.1 (crate (name "hifmt") (vers "0.1.4") (deps (list (crate-dep (name "hifmt-macros") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0492yymhbd3201lb7w2mdpdgpr0pzsmz19mh6f3rvy1g95hrw6bk")))

(define-public crate-hifmt-macros-0.2 (crate (name "hifmt-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03pdf11kx0lxi8qswc2fbig6wj903wafg155inmqyfzg8b66h2bg")))

(define-public crate-hifmt-macros-0.2 (crate (name "hifmt-macros") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ldxkdfzd8cwgrrws5899p4qk95v7wv2qxjn164af27dmcdzmxpg")))

