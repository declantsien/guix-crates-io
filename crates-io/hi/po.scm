(define-module (crates-io hi po) #:use-module (crates-io))

(define-public crate-hipool-0.1 (crate (name "hipool") (vers "0.1.0") (deps (list (crate-dep (name "hierr") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ilhg5lizajjpf3659mz43w42pzz0q4larv3c88hkiz6q2jpa478")))

(define-public crate-hipool-0.1 (crate (name "hipool") (vers "0.1.1") (deps (list (crate-dep (name "hierr") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qrfajsi3mwg0l9a2qgbmdf5s6nmv38j7w2ayfg8hw6vay8znjgi")))

