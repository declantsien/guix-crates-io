(define-module (crates-io hi bi) #:use-module (crates-io))

(define-public crate-hibiscus-0.1 (crate (name "hibiscus") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qskc84zaq9c7qz7xr3g23syv0ddsmg8k9h691cnbrx2m34y55n8") (yanked #t)))

(define-public crate-hibitgraph-0.1 (crate (name "hibitgraph") (vers "0.1.0") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hibitset") (req "^0.6") (default-features #t) (kind 0)))) (hash "1r1ny9n4p1ys1fis6f1hvd6rh1qa1ql7qhzvd0dms5c6hdfaw2rp")))

(define-public crate-hibitset-0.1 (crate (name "hibitset") (vers "0.1.0") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)))) (hash "137fsa4x7pfbdmssfmhwx2msw7aspx8xd7gqncnmg8fizhn831vv")))

(define-public crate-hibitset-0.1 (crate (name "hibitset") (vers "0.1.1") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lpw413igc27p7kyprv4vmnfbappvaxws6q34bslgvaymyn2ck5b")))

(define-public crate-hibitset-0.1 (crate (name "hibitset") (vers "0.1.2") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.7") (features (quote ("unstable"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gsxxzak2g5garyrlbmfsvijra1qlpf4fdfv00kkg10xrhc7sbnh") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.1 (crate (name "hibitset") (vers "0.1.3") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)))) (hash "09s9lrw3idsqlm8f6m3sk6xn1dzm5iycaw19j85gs9m08ri1q2ns") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.2 (crate (name "hibitset") (vers "0.2.0") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "1jm2wyi9a6icpzbwybdn0w7x7z5d6qwrnfhqh26bfqvhbxmn96n0") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.3 (crate (name "hibitset") (vers "0.3.0") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "057pdm2yaqp3zz98pm8ix07a6agbddkxsw16ypq4g521w9b99mqk") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.3 (crate (name "hibitset") (vers "0.3.1") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "12prh7dm6wvq39yifb3ycsk58zd143fckf5k9pmg4yqd2npcha6l") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.3 (crate (name "hibitset") (vers "0.3.2") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "113x63wcz0q34r6rwywh4lzp10jbzhykg3vfb4jibms3qbiri2dp") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.4 (crate (name "hibitset") (vers "0.4.0") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "0bf2rp7l0v2vnn1yq9rws8lg35kbbn26y7b0mlc537lki3awkcsi") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.4 (crate (name "hibitset") (vers "0.4.1") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "08zzqmn4bn3rgxi1g5c0a94nr002l62m9zxv0zxvb2617bmyy3aa") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5 (crate (name "hibitset") (vers "0.5.0") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "10msjhlzkismxhjpnin2b5vihv5qf9ps5mm4y7pigs8bpzpv57qy") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5 (crate (name "hibitset") (vers "0.5.1") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0n9z3k2m170x7q32pnsx94sr4g0vn6h9vbr2lzgsqivv5nr8k55q") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5 (crate (name "hibitset") (vers "0.5.2") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "03h7asdswqr3rbs5faix1x2gk3hgkqm4i95bcw97w7qmxbqcnxc8") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5 (crate (name "hibitset") (vers "0.5.3") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0japzizxdxrin858z2ghh1516hhn1yfycbgrcmkrr5dfi5fxm5d7") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5 (crate (name "hibitset") (vers "0.5.4") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1l4jb5xd5xvmrymd1b4ajwv9p88ppyr78a2pqwk3j39fyf4bq9v5") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6 (crate (name "hibitset") (vers "0.6.0") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1nxzx1y6rrzza4lxz6sgniia8ld8715gkd4d31xpddz96fd149pb") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6 (crate (name "hibitset") (vers "0.6.1") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "04k13g1anz33s590h7dz0jgajfb4ys85m333lfxgjli46hvp7n1w") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6 (crate (name "hibitset") (vers "0.6.2") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "13a0dyma3y7n316j611n4knpk6x6s3r4i0696px8kzppv4pjkrs7") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6 (crate (name "hibitset") (vers "0.6.3") (deps (list (crate-dep (name "atom") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1cb4dpjbhlg88s6ac16c4fymqffpip9c8ls2s6kmji542s1vp8ck") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6 (crate (name "hibitset") (vers "0.6.4") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "08812bg7is6cn07dhfk5l8aiw8f4vcx1cr8d6dh8x58clv7ybvgk") (features (quote (("parallel" "rayon") ("default" "parallel"))))))

