(define-module (crates-io hi be) #:use-module (crates-io))

(define-public crate-hibe-1 (crate (name "hibe") (vers "1.0.0") (deps (list (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1hwqw4w02lx5zp3sdp2di1bhd7v9n9i5xd7qbdass4c2r3ygzkhg")))

(define-public crate-hibe-1 (crate (name "hibe") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "^4.0.18") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1b47dhpzl5b6ddz59pn6qcxdgdja986j79y953ay6dbmxhhar1xn")))

