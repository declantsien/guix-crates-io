(define-module (crates-io hi dr) #:use-module (crates-io))

(define-public crate-hidraw-0.0.1 (crate (name "hidraw") (vers "0.0.1") (deps (list (crate-dep (name "ctrlc") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "hidraw-sys") (req "^0.0.1") (features (quote ("extra_traits" "wrappers"))) (default-features #t) (kind 0)))) (hash "1qi8mfpyhdwbcn5yh80sqv1r59cnjm1gmz8lp2880as4k2hag4s4")))

(define-public crate-hidraw-0.0.2 (crate (name "hidraw") (vers "0.0.2") (deps (list (crate-dep (name "hidraw-sys") (req "^0.0.2") (features (quote ("extra_traits" "wrappers"))) (default-features #t) (kind 0)))) (hash "0gm3qbhvb110nqgr4x09p8n0lyrinc7bn2zz57kbx7v01nw63lzy") (rust-version "1.64.0")))

(define-public crate-hidraw-0.0.3 (crate (name "hidraw") (vers "0.0.3") (deps (list (crate-dep (name "hidraw-sys") (req "^0.0.3") (features (quote ("extra_traits" "wrappers"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0x7hb61hk72cnk2zz51ribz8gmsi7y8lscgk5qzh1kc02nrkb3bx") (features (quote (("unsafe_ioctl") ("default")))) (rust-version "1.64.0")))

(define-public crate-hidraw-0.0.4 (crate (name "hidraw") (vers "0.0.4") (deps (list (crate-dep (name "hidraw-sys") (req "^0.0.3") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "1yp2c3sg1h6l7bx814zq4j81b5p8vxhyppzadcibysrx31px1si6") (features (quote (("unsafe_reports") ("default")))) (rust-version "1.64.0")))

(define-public crate-hidraw-0.0.5 (crate (name "hidraw") (vers "0.0.5") (deps (list (crate-dep (name "hidraw-sys") (req "^0.0.5") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "1b3726d86c5cmp5842dxidcirza8x44ypm5hqalax4rd90cmasap") (features (quote (("unsafe_reports") ("default")))) (yanked #t) (rust-version "1.64.0")))

(define-public crate-hidraw-0.0.6 (crate (name "hidraw") (vers "0.0.6") (deps (list (crate-dep (name "hidraw-sys") (req "^0.0.6") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "1b44f098az5b4na2qhjqlhjwk276h7s9aa7pg3gk07cdzc017vrm") (features (quote (("unsafe_reports") ("default")))) (yanked #t) (rust-version "1.64.0")))

(define-public crate-hidraw-0.0.7 (crate (name "hidraw") (vers "0.0.7") (deps (list (crate-dep (name "hidraw-sys") (req "^0.0.6") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "11x57calzswh6z96kwzvqj1fh6sqsdp8d1r3b3bv7gcrnkqv67cv") (features (quote (("unsafe_reports") ("default")))) (rust-version "1.64.0")))

(define-public crate-hidraw-sys-0.0.1 (crate (name "hidraw-sys") (vers "0.0.1") (deps (list (crate-dep (name "ioctl-macro") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "1w1cb039ssmrrnwgp5ngw0w0bmjw5abzibrx5pw4xfiniv2mw0pd") (features (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default"))))))

(define-public crate-hidraw-sys-0.0.2 (crate (name "hidraw-sys") (vers "0.0.2") (deps (list (crate-dep (name "ioctl-macro") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0m4bxgy3cdcl645094gab9c29agwryjd3m99xiwvav5bpx3awhzc") (features (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (rust-version "1.64.0")))

(define-public crate-hidraw-sys-0.0.3 (crate (name "hidraw-sys") (vers "0.0.3") (deps (list (crate-dep (name "ioctl-macro") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0nb060i0h0rlpcx7x96kc1kn6k38prp2sbh3gi3dl5h1c35sjras") (features (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (rust-version "1.64.0")))

(define-public crate-hidraw-sys-0.0.4 (crate (name "hidraw-sys") (vers "0.0.4") (deps (list (crate-dep (name "ioctl-macro") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0k4b93az8qwg63yw49p5qk2lrg48dxmrd5farwdyzikammm880nk") (features (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (rust-version "1.64.0")))

(define-public crate-hidraw-sys-0.0.5 (crate (name "hidraw-sys") (vers "0.0.5") (deps (list (crate-dep (name "ioctl-macro") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1sbx10dxmi7lpiww1apj73005vkm50b4wdn3ixdkvgl00q1mx0c9") (features (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (yanked #t) (rust-version "1.64.0")))

(define-public crate-hidraw-sys-0.0.6 (crate (name "hidraw-sys") (vers "0.0.6") (deps (list (crate-dep (name "ioctl-macro") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0abm8y0wdwzlj3gfy06k96xrpbrw7c54b40z5bk194rpkfgbmmpf") (features (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (rust-version "1.64.0")))

(define-public crate-hidreport-0.1 (crate (name "hidreport") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0zwvfk9b8qvkj8z6ikbrnd1j8bplxlgqw7bgpygqr3kbnkqpv72c")))

(define-public crate-hidreport-0.2 (crate (name "hidreport") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0k58qcsm1f8xbrpb1ly3qnjnfgjb5d19m7lkysjmg69c5gviknv0")))

(define-public crate-hidreport-0.3 (crate (name "hidreport") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1s9gw2168vz7dlyhp5ppgzqwc148fpg0ji9cpx3z9pnaf1qbai18")))

