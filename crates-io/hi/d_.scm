(define-module (crates-io hi d_) #:use-module (crates-io))

(define-public crate-hid_list-0.1 (crate (name "hid_list") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("hidclass" "debug" "hidsdi" "setupapi" "impl-default" "fileapi" "winnt" "winbase" "handleapi" "hidpi"))) (default-features #t) (kind 0)))) (hash "09mss38mk6lzxsafsq15m0j12bh6hakdfzzzvlkqxid6bxihqgry")))

(define-public crate-hid_list-0.1 (crate (name "hid_list") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("hidclass" "debug" "hidsdi" "setupapi" "impl-default" "fileapi" "winnt" "winbase" "handleapi" "hidpi"))) (default-features #t) (kind 0)))) (hash "01fy8ls8x9lxnnyh3drvqrsrgbppg0i2rkwzfai2vgwp53bzh6iv")))

(define-public crate-hid_list-0.1 (crate (name "hid_list") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("debug" "fileapi" "hidclass" "hidsdi" "handleapi" "hidpi" "impl-default" "setupapi" "winnt" "winbase"))) (default-features #t) (kind 0)))) (hash "1iv5bzg856dz12kp3slzqwvngs3sqrbbi24cfs1phz9f657pakd6")))

