(define-module (crates-io hi -n) #:use-module (crates-io))

(define-public crate-hi-nvim-rs-0.1 (crate (name "hi-nvim-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "palette-gamut-mapping") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0q83sd61ix1jk222p023w4wks81mwf00vq0ywcqxsdbhvygdzdsr")))

