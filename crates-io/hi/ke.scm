(define-module (crates-io hi ke) #:use-module (crates-io))

(define-public crate-hike-0.1 (crate (name "hike") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "030xnjwrwwcfglizv0nwq7k3lhqsppili8gy3xzcwdv552mba4sj")))

(define-public crate-hike-0.1 (crate (name "hike") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y5csgwdqz2vv2bvi8b9444fpqrxgg84sy871957zi87rq7f0qr8")))

(define-public crate-hike-0.1 (crate (name "hike") (vers "0.1.2") (deps (list (crate-dep (name "docopt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "10qyxj39zalp0fy5hnxxjxkr85p3a460f48a6ksdyinlmxpgpv7d")))

