(define-module (crates-io hi ka) #:use-module (crates-io))

(define-public crate-hikage-0.1 (crate (name "hikage") (vers "0.1.0") (hash "16zva1jh09n451r2kmiglg1hw2wjznd080x02ymqk7y7v7m0cvhw")))

(define-public crate-hikari-0.1 (crate (name "hikari") (vers "0.1.0") (hash "0p79g46g1ad7qw6s89i1cbl3v4zjb7fq63xnhj3ay95w85d05z1k")))

(define-public crate-hikaru-0.1 (crate (name "hikaru") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1khps7i62hz69zxjgnawq9m41r9qgya265n3kvh7f8rn5br605j5")))

(define-public crate-hikaru-0.1 (crate (name "hikaru") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "14ljp9xjmhpk36bjvrnix80y455l9567grcj624lkz15p6kdcljm")))

(define-public crate-hikaru-0.1 (crate (name "hikaru") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aypfmp56kckrgx8cpjbw545f7yzcbpd3595mfginw2alj5fcd1q")))

