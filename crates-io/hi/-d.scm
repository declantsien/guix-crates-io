(define-module (crates-io hi -d) #:use-module (crates-io))

(define-public crate-hi-doc-0.1 (crate (name "hi-doc") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "random_color") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "range-map") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.13.1") (default-features #t) (kind 0)))) (hash "1z509pdsdhvhvp84fn4wnxy4cxw8qbwm8kgryrxh5vi161zn333l")))

(define-public crate-hi-doc-0.1 (crate (name "hi-doc") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "random_color") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "range-map") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.13.2") (default-features #t) (kind 0)))) (hash "01p0nh3qrw9lf1zivxhdfbhkgyivmlxq6mwmxxl02dz1kc60lffj")))

