(define-module (crates-io hi st) #:use-module (crates-io))

(define-public crate-hist-0.1 (crate (name "hist") (vers "0.1.0") (hash "0v5xrs02fl5qb96i3nn5xzv62d77hgf956bmm792i3b6gsy627fa")))

(define-public crate-hist-cli-0.1 (crate (name "hist-cli") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0m1960dkma3sspzx0xlsmkfdny52k15k8znq3ibd1yafmmsv5dy8")))

(define-public crate-hist-cli-0.2 (crate (name "hist-cli") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1i4py4xjfz24cc2hskm14cs5h3byg8ngz1a9sca20d8l6np9ysg6")))

(define-public crate-hist-cli-0.3 (crate (name "hist-cli") (vers "0.3.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hnzyr7994wqqx6diz35cw2afdmmwh4702fnm4ggbz154b9lwg2p")))

(define-public crate-hist-cli-0.3 (crate (name "hist-cli") (vers "0.3.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c0zsf8806m8g1vcarva8xl8gfm9yjhqiwzc75rwhp6f96bfca54")))

(define-public crate-hist-cli-0.4 (crate (name "hist-cli") (vers "0.4.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hik0s1y2p3cxxzn8xgs7fp6f35zf4kpz5cgjz479jw65kyvyd6w")))

(define-public crate-hist-cli-0.4 (crate (name "hist-cli") (vers "0.4.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.8") (default-features #t) (kind 0)))) (hash "0xji5ml2lv9wd02jlphpqa7jgr09zzj8zxq4ilw8b7nkhciskx9r")))

(define-public crate-hist-cli-0.4 (crate (name "hist-cli") (vers "0.4.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.8") (default-features #t) (kind 0)))) (hash "13byxq7vm8papm3bgjbqj3khyk0r22m1ssfq6vz84f5mgy05xwdi")))

(define-public crate-hist-cli-0.4 (crate (name "hist-cli") (vers "0.4.3") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.8") (default-features #t) (kind 0)))) (hash "1ib1nc948jcanr5qs7qxwyangf5jn1w2128vbfak5gbzai1bi4w3")))

(define-public crate-hist-cli-0.4 (crate (name "hist-cli") (vers "0.4.4") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.8") (default-features #t) (kind 0)))) (hash "1n48l3331k8xn5770cy19lxqfkm946w2fmzxgwp3ffswcclmsssm")))

(define-public crate-hist-cli-0.4 (crate (name "hist-cli") (vers "0.4.5") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.8") (default-features #t) (kind 0)))) (hash "1c9c1bmjbfl9dkyhqz7j3jan5q98fkg5k9b1aqq5dw132hs0q5cx")))

(define-public crate-hist-cli-0.4 (crate (name "hist-cli") (vers "0.4.6") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.8") (default-features #t) (kind 0)))) (hash "0gw7dc7w8lg8wm9xmjn1qsjxy9g97i5qzif47vibsk03cxydx42d")))

(define-public crate-hist2d-0.1 (crate (name "hist2d") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "127jzwab0n4qazq9a4bb368qi6r8n5qakj980frsicw3vgmc3i84")))

(define-public crate-hist2d-0.1 (crate (name "hist2d") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "1l87qfcl0ir9qryx97h1zcpzbny2kpvcwi4cr69arq6d16bprsqz")))

(define-public crate-hist2d-0.1 (crate (name "hist2d") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "1478irh1vhs51872cp1g15kg8sdgkr9fwcx1nqdjy03v0bxfms1y")))

(define-public crate-histdb-rs-0.1 (crate (name "histdb-rs") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0mgq5r9kciwssbn4slhz15nnf851h39nf2kf3qhkha9wc41rnhzv")))

(define-public crate-histdb-rs-1 (crate (name "histdb-rs") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0wpz7i4wd1lchy2y3k7y64ara5gpi9vpinvsrnxswsj9zz1p6wp3") (features (quote (("histdb-import" "rusqlite") ("default" "histdb-import"))))))

(define-public crate-histdb-rs-2 (crate (name "histdb-rs") (vers "2.0.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "03024kkf1blf69nmb2npgyf6wy4sh1ggzj6z6wl3igzwjbhlbi3j") (features (quote (("histdb-import" "rusqlite") ("default" "histdb-import"))))))

(define-public crate-histdb-rs-2 (crate (name "histdb-rs") (vers "2.0.1") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1dv76wi4cn3ggg5fxnj0rnifii18kikihh5iq3f9a77gvpra7cm4") (features (quote (("histdb-import" "rusqlite") ("default" "histdb-import"))))))

(define-public crate-histlog-0.1 (crate (name "histlog") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^6") (default-features #t) (kind 0)))) (hash "1lcawcqr13cwdckzqadm4zawj78aw1wms4x96igqzk82y0ljwncq")))

(define-public crate-histlog-0.1 (crate (name "histlog") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^6") (default-features #t) (kind 0)))) (hash "0piz9ld47vfxxyjj771p9zr5f26qr7s0134qdjw40a32x6wd0j4b")))

(define-public crate-histlog-0.1 (crate (name "histlog") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^6") (default-features #t) (kind 0)))) (hash "1a6iadn3ryaz3qql6r0m8q525md578w2qdydmdfdpbkn2yfcnxa5")))

(define-public crate-histlog-0.1 (crate (name "histlog") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^6") (default-features #t) (kind 0)))) (hash "1qy9inv65i8p04hai7gdc6xbdbghv19rll7q521fnkinynssslp4")))

(define-public crate-histlog-1 (crate (name "histlog") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^6") (default-features #t) (kind 0)))) (hash "1pg2aalkxmwx9xm4gyjib00pn5ag68ljlj76bwywf5d4i2wnn85f")))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0-rc.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1bmcg4y67hjw454fiizb2hm53j52lvfyxg1mih5lvq6scp5m3vmp") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0-rc.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ikhr3yfyyhy7pnvpb47dvj6b07l1m7hyy0nvg82fkbhd7l92nlq") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0-rc.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "16xqx89bi3lf4qdvyw9475j1yflzhkpzr5aj2ivvkyxdv30ns4n7") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0-rc.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ghl8lyw1bf3ipfvsiz13gxx8irb8fh5zbfzmb9av81zms2hl0v2") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0-rc.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rpqlg3jhpkqgzyly942scny5lraaa4f513w01g9iaakiyvyf3q9") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0-rc.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0im7whmxvdllp0224isa1cgsc36xrbj3252qc1bkzyzldqv5axcw") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0-rc.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1m9b8v3vcpwxqfpyggzw4vj510mqnzc0ab2lzppbk0pnm2nfbm7v") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jlc4y9117569x278sf7vmvzb12np5kl5ib0kx4p8l63yaaiwry2") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "145aa6b6cjjkzllv53xv446nllhmaidkrmd09l1cwm9xsq1r5395") (features (quote (("default" "minstant"))))))

(define-public crate-histlog-2 (crate (name "histlog") (vers "2.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hdrhistogram") (req "^7.5.4") (default-features #t) (kind 0)) (crate-dep (name "minstant") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "105z2shqlsihw9iqvdfw01xdkkrkpcyz2carw9z90g77nsnvnamg") (features (quote (("default" "minstant"))))))

(define-public crate-histo-0.1 (crate (name "histo") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "streaming-stats") (req "^0.1.28") (default-features #t) (kind 0)))) (hash "0n724l9j6l0d3mpimi5lhzb80fvlmn1izd2k0gjyic8n9cc9ahas")))

(define-public crate-histo-1 (crate (name "histo") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "streaming-stats") (req "^0.1.28") (default-features #t) (kind 0)))) (hash "0v6znd33clam2b37rhn2pldd39l61605s1ivxzpjwdygi8f6mad6")))

(define-public crate-histo_fp-0.2 (crate (name "histo_fp") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "streaming-stats") (req "^0.1.28") (default-features #t) (kind 0)))) (hash "18r70z238nryaxdh73w73d7maml9sl5px6wng5101giv6w07ps28")))

(define-public crate-histo_fp-0.2 (crate (name "histo_fp") (vers "0.2.1") (deps (list (crate-dep (name "noisy_float") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "streaming-stats") (req "^0.1.28") (default-features #t) (kind 0)))) (hash "1csp6fjx8wixmg174j3iq6dya3bcdg45by5kmq7qv2f3z4qcgf7k")))

(define-public crate-histodu-0.1 (crate (name "histodu") (vers "0.1.0") (deps (list (crate-dep (name "bytesize") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^4") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "hdrhistogram") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "miniserde") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "scoped-tls") (req "^1") (default-features #t) (kind 0)))) (hash "0ssi2bbidy0a5236wxnya5s1c3ykz6vkr3mrgfcy78pm9zzk50pr") (features (quote (("default")))) (v 2) (features2 (quote (("completion" "dep:clap" "dep:clap_complete")))) (rust-version "1.70")))

(define-public crate-histogram-0.1 (crate (name "histogram") (vers "0.1.2") (hash "0hyxhs8x0mfyfgrcx8rg6k0h5iszmv3lpy3hy7hn57zmir36sxjh")))

(define-public crate-histogram-0.1 (crate (name "histogram") (vers "0.1.3") (hash "1va3pf2p8kgzqqnih5c0fvz0y8dddi20lxqxk0hajbjjkimad4a8")))

(define-public crate-histogram-0.1 (crate (name "histogram") (vers "0.1.4") (hash "1nqx959c3gxfvp08lm7wab9f4ggs6wamq3gqwwpbdpp83zmsv2qh")))

(define-public crate-histogram-0.1 (crate (name "histogram") (vers "0.1.5") (hash "12fnms54i5b16hc6ip2k5fqvc0i8s079ar16lrg613m2y5isj513")))

(define-public crate-histogram-0.1 (crate (name "histogram") (vers "0.1.6") (hash "147g9b7bf2hk74xj1i5m67kja6qadad833ag4g6s7padzxpjnyhh")))

(define-public crate-histogram-0.2 (crate (name "histogram") (vers "0.2.0") (hash "0hghr7zh8iixlglqdia3pip1a0xprgrfyzrq8abc4frn1spvzc4s")))

(define-public crate-histogram-0.2 (crate (name "histogram") (vers "0.2.1") (hash "1x76fzj101wqkq97pxcv7s1h1kj7xr58akf4301f4vsqxj9ifb8b")))

(define-public crate-histogram-0.2 (crate (name "histogram") (vers "0.2.2") (hash "037xb6wkldph4b0k6nd41jq0kb2wa4vpxq8r210paxns4inzd2hg")))

(define-public crate-histogram-0.3 (crate (name "histogram") (vers "0.3.0") (hash "0wsvxladny7pzzlicfn0s1lrjy5a15kzbwwj31rwali2hn2awg32")))

(define-public crate-histogram-0.3 (crate (name "histogram") (vers "0.3.1") (hash "0x4x6sdk9fxpyy8hqrr9ccsli62ib7fsnbm4l1hq7fzqqanc4qib")))

(define-public crate-histogram-0.3 (crate (name "histogram") (vers "0.3.2") (hash "0hp5fzll41dcq76n89m8zifqbjr337y79xx5izj4bjjmgc7hnkdk")))

(define-public crate-histogram-0.3 (crate (name "histogram") (vers "0.3.3") (hash "0bhshlix8h8fdfy7fflp4m8crbmx9gf8s5vlns7npcixf449rwwr")))

(define-public crate-histogram-0.3 (crate (name "histogram") (vers "0.3.4") (hash "10qaa9pbpr0l5bvjnnivc3j7m59qcqy5g0k8bixx7s36sgm94x8k")))

(define-public crate-histogram-0.3 (crate (name "histogram") (vers "0.3.5") (hash "0zirqrybgin0hkmcnngvkigxns82galvx99xcql0rkcgxvx31wl0")))

(define-public crate-histogram-0.3 (crate (name "histogram") (vers "0.3.6") (hash "1bhahvimg01l6d6pshiv4g82sxbygs9mgg7796gw25mxqcxlx089")))

(define-public crate-histogram-0.4 (crate (name "histogram") (vers "0.4.0") (hash "1wc7pwjcyrv57074rskzxgfscch0x5a6w465364wf84k9q8dvy3h")))

(define-public crate-histogram-0.5 (crate (name "histogram") (vers "0.5.0") (hash "1kykis2b06nmpc2jwqm7lsmrgncfylqpvm420wcb8d1maayw700k")))

(define-public crate-histogram-0.5 (crate (name "histogram") (vers "0.5.1") (hash "1s2b651m4jdmg0jpwjxrhsm6gmrra8p5bxp5k6jyyj714c095xyh")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.0") (hash "18yxgyc4l6s98hhzli4065n54m4zhg6s0nvgkf77fp95zq6ajvys")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.1") (hash "1b07z5q9mn0b802hj469syahsyxzn8hxfvmahw7my2gh4a3y6vzc")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.2") (hash "0q1hw8k7svclhcvpy8k61nmwbna3cvj6p4c5aww9dij6bbivrjch")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.3") (hash "1c89yk6a3mq84wrrzz0mdvgp91vlbk8ydpk85j92lj62vqsx2rg0")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.4") (hash "0n9f5wmr68dqz753gvsr0kp83kiafm7xnc0qf4j18760vqbnacwb")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.5") (hash "05xkra69ylxbr2nrcmwhq7mdkcsh88xwwvldmd3vrqzhyz97vsfi")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.6") (hash "178i7v1dpsnlsqzjn4ls2h6mapyk26z937m295gpv6pwbj6xf156")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.7") (hash "1r9dnhdkshplzqx6v307y4zw9jh8054mx41fb7ac8nf90a2dlqk1")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.8") (hash "17fcykfd47x7s5bldwxk62173xbayxxfm12kd0drdjn1ji0frp0v")))

(define-public crate-histogram-0.6 (crate (name "histogram") (vers "0.6.9") (hash "0s9660nyacb5dgb5qxzgbf6lxrki1fmmgcalwm38c2r9rcn8ijqj")))

(define-public crate-histogram-0.7 (crate (name "histogram") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "05ph937kpwaslfnmgwrzvkr2l5c1mr0g2fdm4gdqmg1pw9m0cgli")))

(define-public crate-histogram-0.7 (crate (name "histogram") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0mdx5qxlrdk9jdijrk1jlgy0wm899spanrlh4vxi621ywhgdpkgj")))

(define-public crate-histogram-0.7 (crate (name "histogram") (vers "0.7.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0xq1h8cv3dwwya8pvwhazck8ibnfwvrx2zbyhib31pgzhaczk7sc")))

(define-public crate-histogram-0.7 (crate (name "histogram") (vers "0.7.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "08q9cz7y5z3mmlx73ixjdqxl80gcf616ig1pa3nxs8bvmss8p5yh")))

(define-public crate-histogram-0.7 (crate (name "histogram") (vers "0.7.4") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "02xq0k7qryxan5k2xzqp01n66hrbhpanwfwhr31da6cn48vx2wz6") (features (quote (("serde-serialize" "serde") ("default" "serde-serialize"))))))

(define-public crate-histogram-0.8 (crate (name "histogram") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0y06zmxx5i0wdipv50h0q823x84mjhx676r6vk73qi7027459glb")))

(define-public crate-histogram-0.8 (crate (name "histogram") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0sara897s79713srwig1mrd8swjmrvmrbr12vc97sznq0dihkcl5")))

(define-public crate-histogram-0.8 (crate (name "histogram") (vers "0.8.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0c3bi5kn3n1gynbmw7hf9j9fmp1ss3mgmznk2pgnf5c2mzl3klq0") (features (quote (("serde-serialize" "serde"))))))

(define-public crate-histogram-0.8 (crate (name "histogram") (vers "0.8.3") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0i7i95sd2ixc95nbdi3w6wmmmm0gjmhjii6g4kx3z12hxz60kk3j") (features (quote (("serde-serialize" "serde"))))))

(define-public crate-histogram-0.8 (crate (name "histogram") (vers "0.8.4") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0nbi8iy4bgf0ay0dsp36dlq2x3j1rrr425w1shfiz3azmg45j3yy") (features (quote (("serde-serialize" "serde")))) (v 2) (features2 (quote (("schemars" "dep:schemars" "serde-serialize"))))))

(define-public crate-histogram-0.9 (crate (name "histogram") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0kbd1vjacm1r0ls1gnmnk38wamn1wcwrqxam24dcz24ki63r9vp5") (v 2) (features2 (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

(define-public crate-histogram-0.9 (crate (name "histogram") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0x8ixfg00nnq1zp0akhdc0qfjykdybid8dkq28p6cqwaxf846qsb") (v 2) (features2 (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

(define-public crate-histogram-0.10 (crate (name "histogram") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "1f8fydc4mgm99m12gsxn6fwmhcd3qzzjiwb2azkifax3fpfvvlzl") (v 2) (features2 (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

(define-public crate-histogram-0.10 (crate (name "histogram") (vers "0.10.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0b5ylvw5w8ncdyzma88xjy2xjpya5wrkfa89y9lail19lswirn2v") (v 2) (features2 (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

(define-public crate-histogram-sampler-0.1 (crate (name "histogram-sampler") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0xk3rqfawl12xk7xsl07rlg5i2p0g8nrym3jl5rii72m4d7ccyb5")))

(define-public crate-histogram-sampler-0.1 (crate (name "histogram-sampler") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "13rlra03r9klzz7dq9p8242502wmw4p2jm7cc1qzvbr2bq9d4yf4")))

(define-public crate-histogram-sampler-0.1 (crate (name "histogram-sampler") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0iy6fry302nyrlk8q1n45f2kxz3s7ylk43879nk2zyx00lmlkzgi")))

(define-public crate-histogram-sampler-0.1 (crate (name "histogram-sampler") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x7mk24m8np6v9qhz7lsz8f3s82zarw97f2kqcvl0dy7kqzq1cgb")))

(define-public crate-histogram-sampler-0.2 (crate (name "histogram-sampler") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "15r7s5fx9051wbs8x3kk3mwjahfb7xbsvw3s1lhcaq8zr4v2mzxh")))

(define-public crate-histogram-sampler-0.2 (crate (name "histogram-sampler") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "1bhxw6yn5xg8bzpw4d4f5kqdzwkxf4nj3pv7f1zndr4r34yhap5m")))

(define-public crate-histogram-sampler-0.3 (crate (name "histogram-sampler") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1jbbrr9z3sq3rl8q22cvvlf915iin2gadx0pdlf1rqxi2dklcpyk")))

(define-public crate-histogram-sampler-0.4 (crate (name "histogram-sampler") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0rfmmxinb6kkaijas9ssbrl4xsh97fwpsmna412sj88m5dg079h0")))

(define-public crate-histogram-sampler-0.5 (crate (name "histogram-sampler") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0yy5ik4b16avjhwvbzyiv26pz7xm9hciljls98ni0p4bq4y0ga7p")))

(define-public crate-histongram-0.1 (crate (name "histongram") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.20.0") (features (quote ("ron"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0m2rcrwdyyxjy03nhkzg13pabxdnpi0y78fdlynmn1af98yyvyyn") (features (quote (("default" "fxhash")))) (v 2) (features2 (quote (("serde" "dep:serde") ("fxhash" "dep:fxhash"))))))

(define-public crate-histongram-0.2 (crate (name "histongram") (vers "0.2.0") (deps (list (crate-dep (name "ahash") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "compact_str") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.20.0") (features (quote ("ron"))) (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "06p75lnbv65l3i62jbbg8qsmh6y8bpl3kjb3jsp5zm79lw4h08l5") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde" "hashbrown/serde"))))))

(define-public crate-historian-1 (crate (name "historian") (vers "1.0.0") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k6sf2jvhk6ri5l1alcd03m3bpvxczxvagqrnpwqhdxxa9izs8jh")))

(define-public crate-historian-1 (crate (name "historian") (vers "1.0.1") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "03kzm85bqfz68v66dimjz17a48l4vn7a1a6c0y4rlr55nmfxzgcg")))

(define-public crate-historian-2 (crate (name "historian") (vers "2.0.0") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a44zh0r00ck87lrvjk0nz6ndhr25wy12zyz8ry67avzl9mnm04f")))

(define-public crate-historian-2 (crate (name "historian") (vers "2.0.1") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wyzl3fnapqm0acnmcl7ap41qwl26b6zjinn0lbnjaggl6mi9vf2")))

(define-public crate-historian-2 (crate (name "historian") (vers "2.0.2") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x7bbp5dm6iahvyv1b6sla9q9v43s60fkwsbmd592x3y29f017hc")))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.0") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "18aff4h9q1k78q55cv58ndpyri21g267p3xqja4n3f09dikm812p")))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.2") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dnpbysk8yr6vxgqln2hy9s1rrb3ghvd50jkwbpcy98dcbsbr9ax")))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.3") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mp3blr1yjiz32yi7scchl2926vvqf87ayqkz3smsfzqk6770dw7")))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.4") (deps (list (crate-dep (name "coco") (req "^0.2") (default-features #t) (kind 0)))) (hash "01z7p96d1a003akr55m8fmsspvkbdms1wcvrzg6w4lvdg6509f66") (features (quote (("default") ("bypass"))))))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.6") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.7") (default-features #t) (kind 0)))) (hash "0lpw8jvpq1iy8zfq4nrpz1ixz5h9ixcqxa0sv2smnxs4dkln21h8") (features (quote (("default") ("bypass"))))))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.7") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.3") (default-features #t) (kind 0)))) (hash "1782y0q4mpffxrixzl3zh9bg7hqqbgn6wv0nariqs0x6j3lp1zdw") (features (quote (("default") ("bypass"))))))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.8") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.7") (default-features #t) (kind 0)))) (hash "11jphsk6lrgiv32w4933iqr48bb780na0zpyg1m4vnmqbvlfp47w") (features (quote (("default") ("bypass"))))))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.9") (deps (list (crate-dep (name "sled_sync") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dmkggymz04q8d6i24bknbnch9cf452ry588vjmybshymqbb1sfb") (features (quote (("default") ("bypass"))))))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.10") (deps (list (crate-dep (name "sled_sync") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hhm38kwmrky1d67adifhygd5gzpxa40q8bxlq0cfa1vfhahr0jf") (features (quote (("default") ("bypass"))))))

(define-public crate-historian-3 (crate (name "historian") (vers "3.0.11") (deps (list (crate-dep (name "sled_sync") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0s6vnvwvgbz090v8gh2mzjaw9ccqs3qw5ywvhpn13ipa3y5d8691") (features (quote (("default") ("bypass"))))))

(define-public crate-historian-4 (crate (name "historian") (vers "4.0.1") (hash "197nygii46madmk679fi4clmivbik6m8sjls16zx3gjr7hjlavdh")))

(define-public crate-historian-4 (crate (name "historian") (vers "4.0.2") (hash "080wzpkry2h1z4ss90w33d2cbqssh86yyn63g2faxfiyx2n1wixf")))

(define-public crate-historian-4 (crate (name "historian") (vers "4.0.3") (hash "174qvyvv3ibwq8i9d14ak44y9bqgjwbqhjgil519ddjnnxp42lzn") (features (quote (("disable") ("default"))))))

(define-public crate-historian-4 (crate (name "historian") (vers "4.0.4") (hash "0mvnj4j07f270xiblzg0z0lw2amfxcz3clckpbxjv760ngylxrya") (features (quote (("single_threaded") ("disable") ("default"))))))

(define-public crate-history-buffer-0.1 (crate (name "history-buffer") (vers "0.1.0") (hash "0symfdsgrd1mmbsncny54155203smv8zv6sa68sw06bl2yxhnh77")))

(define-public crate-history-buffer-0.1 (crate (name "history-buffer") (vers "0.1.1") (hash "0ml2il5lv6vkblvc2wnrds4n1jd0889w39qxwrygxsq0mkgc6fj0")))

(define-public crate-history-buffer-0.1 (crate (name "history-buffer") (vers "0.1.2") (hash "0a1kf383mhzj936fxy2pgl0h7gyjq6wm2bxxxqql9925an40kbcd")))

(define-public crate-history-navigation-0.1 (crate (name "history-navigation") (vers "0.1.0") (hash "16pzy60pgggr7kymi3pfn00am2ziagip6180w7wf66ks5nxb6rdb")))

(define-public crate-history_stack-0.1 (crate (name "history_stack") (vers "0.1.0") (hash "16rhysi2i6h5a8638vdn4j38drq1rv7dqdqxhzrvshadqswpvf9d") (rust-version "1.65")))

