(define-module (crates-io xq -d) #:use-module (crates-io))

(define-public crate-xq-derive-0.1 (crate (name "xq-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.63") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0qr6d4v38fikfvjzhb1v6mzihyk2kw83gjb06p9xygzjzrpdxggk")))

