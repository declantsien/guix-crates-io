(define-module (crates-io xq l-) #:use-module (crates-io))

(define-public crate-xql-derive-0.1 (crate (name "xql-derive") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.5") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "05qm0fap3z9wbyf4hkkab3dl734zqxaajngp528gxla73v0nmy2v")))

(define-public crate-xql-sqlx-mysql-0.1 (crate (name "xql-sqlx-mysql") (vers "0.1.0") (deps (list (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "mysql"))) (kind 0)))) (hash "1dj2ff1xn1322qzvgq9h5dzc3g5a7rlpmc0kxghgzr6y3vwvcy4d")))

(define-public crate-xql-sqlx-mysql-0.2 (crate (name "xql-sqlx-mysql") (vers "0.2.0") (deps (list (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "mysql"))) (kind 0)))) (hash "0f3nkal1cs5g726j35zaa95pfk38csnncbk5092hd2ggdb6vzdaz")))

(define-public crate-xql-sqlx-postgres-0.1 (crate (name "xql-sqlx-postgres") (vers "0.1.0") (deps (list (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "postgres"))) (kind 0)))) (hash "04bky7mlxj8pfhcrzwh4p5k6djkpzng9jrrlpzbvj0irynvj5l43")))

(define-public crate-xql-sqlx-postgres-0.2 (crate (name "xql-sqlx-postgres") (vers "0.2.0") (deps (list (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "postgres"))) (kind 0)))) (hash "0zvfp31icqmci315bv1vvhrzi6jnag4cnjkm1sbr97qc5vwxbx2a")))

(define-public crate-xql-sqlx-sqlite-0.1 (crate (name "xql-sqlx-sqlite") (vers "0.1.0") (deps (list (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "sqlite"))) (kind 0)))) (hash "0qif72fqaq67a02z7631xpx0c59hm2y32ryyrh8h9ihhcgnhi8md")))

(define-public crate-xql-sqlx-sqlite-0.2 (crate (name "xql-sqlx-sqlite") (vers "0.2.0") (deps (list (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "sqlite"))) (kind 0)))) (hash "0lry38zwy9bg207dsqaxi97acf1jmr46kj9zrmmq56qsp9vc5fk8")))

