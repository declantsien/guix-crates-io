(define-module (crates-io xq _n) #:use-module (crates-io))

(define-public crate-xq_notification-0.0.1 (crate (name "xq_notification") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0s7azzcqyhs9xl8v4vy8y50w08avzkjj1i2d8c845xsaimhx525b")))

