(define-module (crates-io xq -l) #:use-module (crates-io))

(define-public crate-xq-lang-0.0.1 (crate (name "xq-lang") (vers "0.0.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (default-features #t) (kind 0)) (crate-dep (name "lexgen") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lexgen_util") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.10.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "12hrihxdskz7rav3ffippckii0ld0g4big7wky1g8ddkidjid2g3")))

