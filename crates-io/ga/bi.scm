(define-module (crates-io ga bi) #:use-module (crates-io))

(define-public crate-gabi-0.1 (crate (name "gabi") (vers "0.1.0") (hash "0nr68207h7ijkclkngdip131lmzdrhj48w2h174z9hrwyr5kh7qh")))

(define-public crate-gabi-0.1 (crate (name "gabi") (vers "0.1.1") (hash "0fs0k1pzvdypmr5ndv6w7aijdan95k3fm4r44v6psd0zqhg3x288")))

(define-public crate-gabi-0.2 (crate (name "gabi") (vers "0.2.0") (hash "0szbqqdzwxzq71hmv6yzj6i62x1jr0kxh769858xj1hliwf4xhgf")))

(define-public crate-gabi-0.2 (crate (name "gabi") (vers "0.2.1") (hash "10kf051pnc7ir4h6agyw11kwa5h9cvyzfnw7i4hj449lzg56iikn")))

(define-public crate-gabi-0.2 (crate (name "gabi") (vers "0.2.2") (hash "0mqy4jxyaylnn7dxmzxpx1idql6f35jgl1xq03qyazfy516c6a86")))

(define-public crate-gabi-0.2 (crate (name "gabi") (vers "0.2.3") (hash "0knqc1afd28jwz2i0ms5b4wd1vwx16lys3yaf8fx2k5xm4q91hnc")))

(define-public crate-gabi-0.2 (crate (name "gabi") (vers "0.2.4") (hash "1aq64fmiyzwlfh4f749p2f3w5mwi578wwnx11pwjm2yjv5v5m1yz")))

(define-public crate-gabi-0.2 (crate (name "gabi") (vers "0.2.5") (hash "19sjc95w9fdm2wzsz4rnzzbdi86rk307ncv0kibrcm3p4djxkx7z")))

(define-public crate-gabi-0.2 (crate (name "gabi") (vers "0.2.6") (hash "17aycd9p4vjyjr70hcx9ymamb52dnm0sc5m32682r0bx3kvklx4y")))

(define-public crate-gabira-0.1 (crate (name "gabira") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0wv9j5qh74lpmqcmwpr6i0b6gmp4rz70hj63v40nnzhbyb0wiwch")))

(define-public crate-gabira-0.1 (crate (name "gabira") (vers "0.1.1") (deps (list (crate-dep (name "actix-web") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "08namk2pmana38xd5clcwi3kfqpsqhh57aq6vr49kbwp2gwvfikm")))

(define-public crate-gabira-0.1 (crate (name "gabira") (vers "0.1.2") (deps (list (crate-dep (name "actix-web") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1i6ckbzcgc28fdm98bqcshf9nhnl1y18glqks3nc45y86ygxma50")))

(define-public crate-gabira-0.1 (crate (name "gabira") (vers "0.1.3") (deps (list (crate-dep (name "actix-web") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "127qnf9vp3xn5cld1a8syq1j1x33vkmxdfi09jj9gh2ljmwzifks")))

