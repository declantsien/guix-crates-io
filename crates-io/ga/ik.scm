(define-module (crates-io ga ik) #:use-module (crates-io))

(define-public crate-gaiku-3d-0.1 (crate (name "gaiku-3d") (vers "0.1.0") (deps (list (crate-dep (name "gaiku-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gox") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "obj-exporter") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1bksvdwcj1i0pk04ffdwqrig9zv51dnll2k7vfk1zv545addxcq8")))

(define-public crate-gaiku-common-0.1 (crate (name "gaiku-common") (vers "0.1.0") (deps (list (crate-dep (name "decorum") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "derive-new") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "17vdlk4jkcrz421nsqvjnfbd2n181z8ibgw97r57xbi6zbx6xwav")))

