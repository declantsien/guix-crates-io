(define-module (crates-io ga ea) #:use-module (crates-io))

(define-public crate-gaea-0.3 (crate (name "gaea") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "std-logger") (req "^0.3.3") (kind 2)))) (hash "0gw8s02ns2p8k557h0j0qp4qrqlmhccv6gb3ixmm4ijvdqxh4zh4") (features (quote (("user_space") ("std") ("nightly") ("disable_test_deadline") ("default" "std"))))))

(define-public crate-gaea-0.3 (crate (name "gaea") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "std-logger") (req "^0.3.3") (kind 2)))) (hash "0m86rjpyb3kwf35lbiayf2v1glkw11k6i9xmx0l04qlhvm7j4sdh") (features (quote (("user_space") ("std") ("nightly") ("disable_test_deadline") ("default" "std"))))))

