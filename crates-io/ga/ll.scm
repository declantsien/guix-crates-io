(define-module (crates-io ga ll) #:use-module (crates-io))

(define-public crate-galleon-0.1 (crate (name "galleon") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18") (default-features #t) (kind 0)))) (hash "0vk6bs6sm9sh857j2sh1cjpll0d5df4cx41biw72x6kbwm0nklpg")))

(define-public crate-galley-0.1 (crate (name "galley") (vers "0.1.0") (hash "1fcs2nik9crwy4p3hhkfjwl881cmqj60q7gghc498r7kavrrvrng")))

(define-public crate-galley-0.1 (crate (name "galley") (vers "0.1.1") (hash "1pfysczg02vwhpzah95slzg3nbr8ms35qv92l24p5nir2lwy9jpq")))

(define-public crate-gallium-0.1 (crate (name "gallium") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "1bvkad1cd5rm4abgp6gj71lcl9f2mmx4gj8glhswsj3ks0dcnicz")))

(define-public crate-gallium-0.1 (crate (name "gallium") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.12") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "16gycfhicqryvik1l27si6h1d5bxmy5rhmyi4wkhpw4fmvd9ykd5")))

(define-public crate-gallium-0.1 (crate (name "gallium") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.12") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "01vhvafk10vj3c4nxpssc1ya7lw40ja1ax1rgzlr46l1hwpx7bqj")))

(define-public crate-gallium-0.1 (crate (name "gallium") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.12") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "1qq471pimzdpnpra4i63ci2h7q1dkyqkxf42wir2lchjkbwbnvc1")))

(define-public crate-gallium_ecs-0.1 (crate (name "gallium_ecs") (vers "0.1.0") (deps (list (crate-dep (name "ron") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "typetag") (req "^0.1") (default-features #t) (kind 0)))) (hash "0njzhp38hxxfid4p0lfhr5n84w8lck2a48hflrhgp9k8gy0clkcl")))

(define-public crate-gallium_ecs-0.1 (crate (name "gallium_ecs") (vers "0.1.1") (deps (list (crate-dep (name "ron") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "typetag") (req "^0.1") (default-features #t) (kind 0)))) (hash "07jvqs8h5a8syfgd1l3bqlw20if9hhqsnrn2jn238iqiim48mxl1")))

(define-public crate-gallium_ecs-0.1 (crate (name "gallium_ecs") (vers "0.1.2") (deps (list (crate-dep (name "ron") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "typetag") (req "^0.1") (default-features #t) (kind 0)))) (hash "0d1r26m4xpd1rsbw3nwbl7iqis3bc061l2j16xba4bl0lfjn82bj")))

(define-public crate-gallium_ecs-0.2 (crate (name "gallium_ecs") (vers "0.2.0") (deps (list (crate-dep (name "ron") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "typetag") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qw5380sbwg72dnqbw00k9pjqy3vvfl2h9l10pmw8djjq3ks5ah8")))

(define-public crate-gallium_ecs_derive-0.1 (crate (name "gallium_ecs_derive") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "08nxx638hvc13hdfyh6xmqsg81s6fn5v1lj33jk66fj2d7wgk4ky")))

(define-public crate-gallium_ecs_derive-0.1 (crate (name "gallium_ecs_derive") (vers "0.1.1") (deps (list (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "1wdjadl4b1siywfjmcdx9z41qd51ly10xml8fkg58ndfrhy6hzx9")))

(define-public crate-galloc-0.0.1 (crate (name "galloc") (vers "0.0.1") (hash "0g5s49f6s4yg2dvq6ns0plkdyfg6lvrp045pk8hkyrflvk3p34gn") (yanked #t)))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2-354d660.6") (deps (list (crate-dep (name "gear-dlmalloc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0dwhrkhn0skhfd5qz0rf52fcppxc6mnbzsn6180471w1vyvh0a7i") (features (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2-354d660.7") (deps (list (crate-dep (name "gear-dlmalloc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1063kq1lc8f54yhf859pdm11ympzlkhjpw1jfxkh5srsfassv0qw") (features (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2-354d660.8") (deps (list (crate-dep (name "gear-dlmalloc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1kl0f2xrhgcinynfsy841gwg0hj2illy5jkcwsj53n7x9gnxp3z0") (features (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2-354d660.9") (deps (list (crate-dep (name "gear-dlmalloc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "05ib1lg9m8lcyshg086mj6yf43vskdxkkr321w1ds8v55pp8j3g5") (features (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2-354d660.10") (deps (list (crate-dep (name "gear-dlmalloc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0xbiraxfsfpmkdjy3yggrd92y1ds5g05nwmr94qf6i1rq9j4a71w") (features (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2") (deps (list (crate-dep (name "dlmalloc") (req "^0.1.4") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "10wyn7hbyyl7f1k0clkz66icbjp0lq71w3szsqp9z2x0wvyhhj5k") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2-gtest-dev") (deps (list (crate-dep (name "dlmalloc") (req "^0.1.4") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "1w52fa5jmbk8m9kwpbf7zqicmw0bqkdiip1m3aag79dzgvmp83wf") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.2-dev.0") (deps (list (crate-dep (name "dlmalloc") (req "^0.1.4") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "16yd8mnc52z9vcyjsiki7bqpxnmfdky4c68qhjbriir5zc16hksk") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.3") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "13mnalrrx9kdcp8m36scsdak67ml3p5mvhxq8rvvvnd3z8cfvc37") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.4-rc.0") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "1y3x54bsiyqwspym6cwr231yh5bh8njvncyc93bnww17qncwnpia") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.4") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "05gil3dndb1r7i1k4jzb556ay36cvgn6w84d161q56bs63lhp0pz") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.0.5") (deps (list (crate-dep (name "dlmalloc") (req "^0.1.4") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "1gvq7hdx3mgi5vnv31ryi09liqx35gs3i2pr0x1b90ivk7avk9bb") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.1.0") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0iqrlwlvac2804m0ra4fyqiq6v91wfsbicgv9iidmfdi3rbnnwvk") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.1.1-rc.0") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0gca6sdb0yk6dyx15j9la3i56qk26bcjjlgwipnznq4acrqn28yr") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.1.1") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0sx9a5n5mi9rb2qbw21vsjxz6js314sr4y1bmk6019h0kqbn1iig") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.2.0") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0k2xhvgrns1pfjjmxdichc9bm7d7vlrzfwicckp7jvik0q7wj2q5") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.3.0") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0537jbjv5mhknzz7fa82v5swg5l85cvma3673ms5m67kqljskp4r") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.2.0-pre1") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "037yygjgmxr2gk842sngw2vi2hfllhq7i34ppc8zmnr7krvkbxn2") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.2.0-pre.2") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "1k67x4mx324mdcxnjjgdhw8w37nfpl1hf055r6qps8ncs2r325sk") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.3.1-pre.1") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "09gn7qlcvb0pcddjghnk92afzix0x1d1gixyzf6g07a08kg5n104") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.3.1-pre.2") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "1h5s5f9mmxlsxml5wwa1xm5mzx1prns0k6x8p30gxa7biv8pnb07") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.3.1-pre.3") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "19sxb70dv3wdjqvci4llbw47nccy8achsxa9njlx850gciqpmj5x") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.2.0-pre.3") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "14xibmn5270yrc5sq7ycajjcs7fd9a7khspsm5p0cbkrpq4m5kqw") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.2.1") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0qx31x8p1kcnxn6a8648hphpilm64ylscq40j790l0iskr9w4fs4") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.3.0-pre.1") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "1njsa485cz7cwvxvvvawzna4paz8zhzkqgdvrl4dqzd5aa3b77h1") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.3.1") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "1lgd67qnyawdflfvc7rqgr7md67sb1mnx4mf6mz4aw34ixiqrzjs") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.4.0") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0gwdkybzdiw1bk6nwhf02zj10fdq34693qzc7s2anx4v2zqas6q5") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1 (crate (name "galloc") (vers "1.4.1") (deps (list (crate-dep (name "dlmalloc") (req "^0.2.0") (default-features #t) (kind 0) (package "gear-dlmalloc")))) (hash "0gxbh4pbprl4d4pff97d8b4qcwc0qazaqbg6jzzfb2kixsx5rh7f") (features (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-gallop-0.0.1 (crate (name "gallop") (vers "0.0.1") (hash "0nr6h40shidaqq6xip0v78kpjcnqm65xksgbwaqg3flljzl68vaz")))

(define-public crate-gallop-0.0.2 (crate (name "gallop") (vers "0.0.2") (hash "0dwgw7jvdgc2c2r4sn9cnbbpy1jwzc07jbfw4laxjcqhka95vs4x")))

(define-public crate-gallop-0.0.3 (crate (name "gallop") (vers "0.0.3") (hash "0cbz4yklj8vpahiz3sh9dd6ckvk5i2jhd235yawdlifs0r1xza0r")))

(define-public crate-gallop-0.0.4 (crate (name "gallop") (vers "0.0.4") (hash "0wikncy7ww5mzx7aqlpzsy9cb5zh7wak4fxbsgvca81qw3jlnc3b")))

(define-public crate-gallop-0.0.5 (crate (name "gallop") (vers "0.0.5") (hash "1261294a5kkffwazabc1sk4d48jww2lcq7c98x73szkhmd37fmj3")))

(define-public crate-gallop-0.0.6 (crate (name "gallop") (vers "0.0.6") (hash "11ab7zz4q5b1cxkv1pams3dz8wqsf9glb7kd0jrazprlsmcwy4iv")))

(define-public crate-gallop-1 (crate (name "gallop") (vers "1.0.1") (hash "0ppysn5jni2i7jbfb0g0w0cv7a99zidgl4zm3qs8x2vdimmg5gpq")))

(define-public crate-gallop-1 (crate (name "gallop") (vers "1.0.2") (hash "0gc6h7mljrm4pn9cayrksdy1biragi4xxhm1kprvrvwkkvdv2r38")))

(define-public crate-gallop-1 (crate (name "gallop") (vers "1.0.3") (hash "1ffgy306cl719qcv4llv1k91z7pxj8bvs04hq7ybnz7jffr0qbsb")))

(define-public crate-gallop-1 (crate (name "gallop") (vers "1.0.4") (hash "1gsyf2gp2psyxr64n1rhds94sxpd7pf87z1y2n8mz1pwc73r0mfh")))

(define-public crate-gallop-1 (crate (name "gallop") (vers "1.0.5") (hash "16bha1v16vgmv8x33kcm0c5sp2avcd91dy3s3lr8h6mckinx9vaw")))

(define-public crate-gallop-1 (crate (name "gallop") (vers "1.0.6") (hash "134m3pk1v5h95zpdvwf1blqrh8z3cxj9x1wqpz95yc3dmbaam4pr")))

(define-public crate-gallop-1 (crate (name "gallop") (vers "1.0.7") (hash "1jdj20nvkb0lm11sbfi5nwgfdss9pfwfiyf139pdp8iapq69m59b")))

