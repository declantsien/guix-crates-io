(define-module (crates-io ga pb) #:use-module (crates-io))

(define-public crate-gapbuf-0.1 (crate (name "gapbuf") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.8.6") (default-features #t) (kind 2)))) (hash "1fqkh85lvgl37l2fnqbdqz9fl6794a7fl0xk3rwyl02xidi8k56g")))

(define-public crate-gapbuf-0.1 (crate (name "gapbuf") (vers "0.1.1") (deps (list (crate-dep (name "proptest") (req "^0.8.6") (default-features #t) (kind 2)))) (hash "0s7jdxpk6h7pi37cgsi129g2brfi23cwn2hna9c9q9mfmi3mp35n")))

(define-public crate-gapbuf-0.1 (crate (name "gapbuf") (vers "0.1.2") (deps (list (crate-dep (name "proptest") (req "^0.8.6") (default-features #t) (kind 2)))) (hash "1ia42w6hhlyzb1s7pbw0ai3ynm0rlr603f2zrf7mbd38r1njwgfa") (features (quote (("docs-rs"))))))

(define-public crate-gapbuf-0.1 (crate (name "gapbuf") (vers "0.1.3") (deps (list (crate-dep (name "proptest") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "03m42rql2b5ffix1p912b45354iwniavmm11hzkhdy58ib9a5q2k") (features (quote (("docs-rs"))))))

(define-public crate-gapbuf-0.1 (crate (name "gapbuf") (vers "0.1.4") (deps (list (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "14m5fzph8jc1aa797xlf4dscr9p9a9j2qkqhfnn29whycj9040ca") (features (quote (("docs-rs"))))))

(define-public crate-gapbuffer-0.1 (crate (name "gapbuffer") (vers "0.1.0") (hash "0giilpfgpy9lhgdjhbwn9r8qzbqn555q1r2qayfkm7z26kr971sy")))

(define-public crate-gapbuffer-0.1 (crate (name "gapbuffer") (vers "0.1.1") (hash "05s4mqk5v1p5kk7ai2s1nwm0bdch9026lngmwzfcqjhyy0zrp4iy")))

