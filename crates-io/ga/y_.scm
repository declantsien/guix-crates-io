(define-module (crates-io ga y_) #:use-module (crates-io))

(define-public crate-gay_panic-0.1 (crate (name "gay_panic") (vers "0.1.0") (deps (list (crate-dep (name "color_space") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "~3.4.0") (default-features #t) (kind 0)) (crate-dep (name "sashimi") (req "~0.1.1") (default-features #t) (kind 0)))) (hash "19ghbmj65r38i6z55g2manb5in7mnz6fcax68gqll4907s6pidz0")))

(define-public crate-gay_panic-1 (crate (name "gay_panic") (vers "1.0.0") (deps (list (crate-dep (name "color_space") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "~3.4.0") (default-features #t) (kind 0)) (crate-dep (name "sashimi") (req "~0.1.1") (default-features #t) (kind 0)))) (hash "14dc2jydfsk29ham1bjp59q3vbi35j3xwf1zq0swn9c5lfvpzn1d") (rust-version "1.65")))

(define-public crate-gay_panic-1 (crate (name "gay_panic") (vers "1.0.1") (deps (list (crate-dep (name "color_space") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "~3.4.0") (default-features #t) (kind 0)) (crate-dep (name "sashimi") (req "~0.1.1") (default-features #t) (kind 0)))) (hash "0cg3nk74gq57r22g246fdx6k228ijw6z8ar2ckjvfmy1dkn75d8m") (rust-version "1.65")))

