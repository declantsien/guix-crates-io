(define-module (crates-io ga ps) #:use-module (crates-io))

(define-public crate-gaps-0.0.0 (crate (name "gaps") (vers "0.0.0") (hash "18nhcxkmqg727f37v4l9s34gnmvakqr9bbgz935cwz4843nzvs9z")))

(define-public crate-gaps-0.1 (crate (name "gaps") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mhp0jwa8z3zxj84c9xxmz1zqckb6q4fcnrf22ffcm2aqsxybhp8")))

(define-public crate-gaps-0.1 (crate (name "gaps") (vers "0.1.1") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "0a7jyd5icq2xm6h3rhlpa2qi4w1fcpjrvfrfijg6id12i90pzbnm")))

(define-public crate-gaps-0.2 (crate (name "gaps") (vers "0.2.0") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "1hia6qrcdvc9qzqaqpn3fnkpa9jlq13xigz1jl0wc0z8xg192lzq")))

(define-public crate-gaps-0.2 (crate (name "gaps") (vers "0.2.1") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "06xy21q3mvhsvi0iphk46x1s8j9fl8xjc96rczvawkcxjs19c6p6")))

(define-public crate-gaps-0.2 (crate (name "gaps") (vers "0.2.2") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "0q10b3qrnad1ni7z4723wl07vcal5lq09mp8wc342nb827w24ghd")))

(define-public crate-gaps-0.2 (crate (name "gaps") (vers "0.2.3") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "19iymky9izr0as6slbrmys8psq4g73nnfd01yil788rh9a45bknb")))

(define-public crate-gaps-0.3 (crate (name "gaps") (vers "0.3.0") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "17vaxi2mrvqllwqdjaff3flnjg3yyybvjvxck2pp5j5p43vpd0fq")))

(define-public crate-gaps-0.3 (crate (name "gaps") (vers "0.3.1") (deps (list (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "08y8689jhksfryzw82276rikrqzs5l5sm26i1zx83w9nblarpk68")))

