(define-module (crates-io ga a_) #:use-module (crates-io))

(define-public crate-gaa_cupertino-0.0.1 (crate (name "gaa_cupertino") (vers "0.0.1") (deps (list (crate-dep (name "gpui") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1a1j1kimxaqllcy6qbhxg8zc86a5683i2blq63jb4jnsnc1p2dpi")))

