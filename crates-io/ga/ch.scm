(define-module (crates-io ga ch) #:use-module (crates-io))

(define-public crate-gacha-0.0.0 (crate (name "gacha") (vers "0.0.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0iciam07199n0jijhc2z1v8dyp53mv6i14b0n1jkrn85hrc49yaz") (features (quote (("default"))))))

(define-public crate-gacha-0.0.1 (crate (name "gacha") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "1p0wcdk872j33m22ha0rmz366rr8pldqazwy391d9abvw2l72k90") (features (quote (("default"))))))

(define-public crate-gachiopener_2-0.1 (crate (name "gachiopener_2") (vers "0.1.0") (deps (list (crate-dep (name "open") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "01fg1i03h7568lqw8mf98d7mq06m90315g792034y0nj4934m5zs")))

(define-public crate-gachiopener_2-0.2 (crate (name "gachiopener_2") (vers "0.2.0") (deps (list (crate-dep (name "open") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1jwj2skgw4bxyhkvd3f387nxqmddq48c7k75f7yyfrf69abdh62l")))

