(define-module (crates-io ga ng) #:use-module (crates-io))

(define-public crate-ganganonline-parser-json-0.0.0 (crate (name "ganganonline-parser-json") (vers "0.0.0") (deps (list (crate-dep (name "ganganonline-parser-lib") (req "^0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "quick-protobuf") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1mlhk6sdy0hcsrvxi08dxbhqswkqr1qkc91b0qhwdwxl4kzk7ndx")))

(define-public crate-ganganonline-parser-lib-0.0.0 (crate (name "ganganonline-parser-lib") (vers "0.0.0") (deps (list (crate-dep (name "pb-rs") (req "^0.8.2") (default-features #t) (kind 1)) (crate-dep (name "quick-protobuf") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0b727zcld01v78vv59rwxcvh8400ps4lw2xgn2xwwppb5b9fz4n9") (features (quote (("no_cow") ("default"))))))

(define-public crate-ganglion-0.1 (crate (name "ganglion") (vers "0.1.0") (hash "1cwpfa648fygnrax6njlbvzpvviamhp222h5wrrnps56xq7b2db1")))

