(define-module (crates-io ga un) #:use-module (crates-io))

(define-public crate-gaunt-0.0.0 (crate (name "gaunt") (vers "0.0.0") (deps (list (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qarhzi9i3gd34w3pi4sh6g2a370wk8ccvag4a1bgjqbjjasha6l") (yanked #t)))

(define-public crate-gaunt-0.0.1 (crate (name "gaunt") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rpckgbdmqhs2zbyqa3haqy9zq2196vxq39bw50dmqjv7609n7pf") (yanked #t)))

(define-public crate-gaunt-0.1 (crate (name "gaunt") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "1dlq68a88qqazvp3gc6pn8zmm7k8gh9kymyf2vm120ykzfsx6j03") (features (quote (("std" "alloc" "failure/std") ("logger" "slog" "std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-gauntlet-0.0.0 (crate (name "gauntlet") (vers "0.0.0") (hash "1yrfpa6khl26azd5wl2s1jb087s699azl8zz7z5whfvqchc5dph1") (yanked #t)))

