(define-module (crates-io ga mm) #:use-module (crates-io))

(define-public crate-gamma-0.1 (crate (name "gamma") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "04zblcqfcrh4zb7vdllfmcz3v8npirhscr2fha3ab8qapwfapbxb")))

(define-public crate-gamma-0.2 (crate (name "gamma") (vers "0.2.0") (hash "1wdvrqblrgv9vcvz7vjzhv9klz14dj94zf1s7fz44zwljnr9zz92")))

(define-public crate-gamma-0.3 (crate (name "gamma") (vers "0.3.0") (hash "1lyvsg084ms2niicd333h3f4g7liabaddq2xdhqrgbq411fdvhsm")))

(define-public crate-gamma-0.4 (crate (name "gamma") (vers "0.4.0") (hash "1qqhc44i19nrbg9hcdvbw3z3sxgywryav3mm0ifw0pb57x7mhjar")))

(define-public crate-gamma-0.5 (crate (name "gamma") (vers "0.5.0") (deps (list (crate-dep (name "indexmap") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1a2rkx809advzxf5a7iay96dwhcm7naddwaywlkd0x27hmaxdkli")))

(define-public crate-gamma-0.6 (crate (name "gamma") (vers "0.6.0") (hash "0cpsf4zzhwnsv2p1nwsqhjkqv54zc18abk0yvv67nx5qcfxnk6kd")))

(define-public crate-gamma-0.6 (crate (name "gamma") (vers "0.6.1") (hash "0815cgdrzxab7cpm4hqvpiqxglvsyb0s8ghv1s1cr5hnbfyi9h8y")))

(define-public crate-gamma-0.7 (crate (name "gamma") (vers "0.7.0") (hash "0jh224z4qv9ayr3rl072bzxd212jn8l6vpfrfzz0jn0b8vciciv9")))

(define-public crate-gamma-0.8 (crate (name "gamma") (vers "0.8.0") (hash "0wsbn7wh0abgh5pvjcinjvx81p0q4h70ihj09gvsbksma411a2rw")))

(define-public crate-gamma-0.8 (crate (name "gamma") (vers "0.8.1") (hash "1p0k7ylfpnrg51j6migys528pjhwmsbhsm2nxxmhhqb7dgbs72jr")))

(define-public crate-gamma-0.9 (crate (name "gamma") (vers "0.9.0") (hash "1lfvr7kwp1dsdwf3vbv2w1q9cj8vlgpf5zc8x0x7z7kv9ic7pwmq")))

(define-public crate-gamma-lut-0.1 (crate (name "gamma-lut") (vers "0.1.0") (hash "1yq9lg8qx3a62xnl8m4b2v7m4y9i6m6bbndim6rlspx9y8q6v007")))

(define-public crate-gamma-lut-0.1 (crate (name "gamma-lut") (vers "0.1.1") (hash "1jxpikj1lbbf20jk0q2zc8i3qvdm9g0wyjzqq09gvpk2m5bvc5js")))

(define-public crate-gamma-lut-0.1 (crate (name "gamma-lut") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "0166drzs1jg5g5bx0msib242vrv9q466rv958rz9nbaq3fgb61gk")))

(define-public crate-gamma-lut-0.1 (crate (name "gamma-lut") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "09vx01k39290qkg80q3z74rv5s5khq44wf0z5isw0f0r2r2drg8k")))

(define-public crate-gamma-lut-0.1 (crate (name "gamma-lut") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kci9vcmy88w7waganp9pp5pwbrqppx85ymbhpzzm9r47l1bk9jc")))

(define-public crate-gamma-lut-0.1 (crate (name "gamma-lut") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wcsn9l0581p1pzcmphl7cv870x93bi8wcycxb53086lrsyaffy8")))

(define-public crate-gamma-lut-0.1 (crate (name "gamma-lut") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m9487kf4vpyb37m6p98skkwv4bps6ymrcnbjmc16xh7jggji1yd")))

(define-public crate-gamma-lut-0.2 (crate (name "gamma-lut") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "15xjfxqydfv7lrzyrs8dmpwanrq21pkvj7in4yw9dwikx7qjmxs1")))

(define-public crate-gamma-lut-0.2 (crate (name "gamma-lut") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jcxspr34lh5x42ns7iyzycpz3gdvwm02i48p6cyz31n0d2hfrfx")))

(define-public crate-gamma-lut-0.2 (crate (name "gamma-lut") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x91hwzq6f0rgqp6h0ddydxpdkah63kq39fx567khjr1xsrvw0f2")))

(define-public crate-gamma-lut-0.2 (crate (name "gamma-lut") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mr8swk5dg60lnw4cph9nmbsqx3c3h6ac1r11hlpl1fkdaggw315")))

(define-public crate-gamma-sql-0.1 (crate (name "gamma-sql") (vers "0.1.0") (hash "0jqzi2zmj8l68q7291xg0brpirlwi72y54r20zpyzjdyw65xfkpf") (yanked #t) (rust-version "1.62")))

(define-public crate-gamma_daemon-0.1 (crate (name "gamma_daemon") (vers "0.1.0") (deps (list (crate-dep (name "battery") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "bulbb") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0q0qwc8x4br1p7i9vizcha1ksq43vkklh6bdy84l2f2iihm7nkrp")))

(define-public crate-gamma_daemon-0.2 (crate (name "gamma_daemon") (vers "0.2.0") (deps (list (crate-dep (name "battery") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "bulbb") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "1y5y7nnm5hyi1hnbl9mv33nj8h05vsr3gq70d3qf4y5aaz7xf5w8")))

(define-public crate-gamma_daemon-0.2 (crate (name "gamma_daemon") (vers "0.2.1") (deps (list (crate-dep (name "battery") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "bulbb") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "0wikkxbk870hfcq5b6hfj4cqjcarnmr73rxanh28zwkd4l10gx2r")))

(define-public crate-gamma_ray-0.1 (crate (name "gamma_ray") (vers "0.1.0") (hash "0bsvk1m3flgg8fq19nkiyyfkxfhgqxc1z9i5p89fla9n666bzxjs")))

(define-public crate-gammatest-0.1 (crate (name "gammatest") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0l3q71d5m6ay2pxk86ckdyjdz2hpl2f2qqfqdbfwp8myqg7v0pjw")))

(define-public crate-gammatest-0.1 (crate (name "gammatest") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1cnxwcpa20hfb9q6as2p83s6v0831naz0wp3sjx5gm5d65rxgfzq")))

(define-public crate-gammatest-0.1 (crate (name "gammatest") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1qmn4bgxnr2abk5p5pyp81xpjsga56cjp0knkpihz20q1l5crjww")))

(define-public crate-gammatest-0.1 (crate (name "gammatest") (vers "0.1.21") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0f24ggx1nayzhb0jkzqdm8k0h8xbysc5q38bch7w8m61ijhvvg88")))

