(define-module (crates-io ga bb) #:use-module (crates-io))

(define-public crate-gabble-0.1 (crate (name "gabble") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 0)))) (hash "1k4wjwmm2l9mx7wmv0k49yj8nggb58pgxb7ldnli9y4cm4w5al18")))

(define-public crate-gabble-0.1 (crate (name "gabble") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 0)))) (hash "12inwc67wnljymv7fkcrys0kvs0ssnl41bln1pgs2920qwxhff6v")))

(define-public crate-gabbro-0.1 (crate (name "gabbro") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (optional #t) (default-features #t) (kind 0)))) (hash "0q077824kx06p9ga5n6vc124pb7bls6pp0lrawf7d72ilha4y68y") (v 2) (features2 (quote (("sdl2" "dep:sdl2") ("logger" "dep:env_logger"))))))

(define-public crate-gabbro-0.2 (crate (name "gabbro") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("bundled"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19") (optional #t) (default-features #t) (kind 0)))) (hash "01k2z2myw9xmc07nkikkf9iq8c2jcmc18h9c7wl4apdwsijkhnkn") (features (quote (("default" "logger")))) (v 2) (features2 (quote (("sdl2" "dep:sdl2") ("logger" "dep:env_logger") ("debug" "dep:tui" "dep:crossterm"))))))

