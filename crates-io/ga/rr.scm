(define-module (crates-io ga rr) #:use-module (crates-io))

(define-public crate-garrett_art-0.1 (crate (name "garrett_art") (vers "0.1.0") (hash "1rilgmzalc3bnbp3cnw2c2w5xsnz9qknbrr2l3rs48a25s0qc36w")))

(define-public crate-garrysmod-0.0.1 (crate (name "garrysmod") (vers "0.0.1") (hash "12r7s952cj12cjpwbysdf3jnfhznapyimq6dmyj8f8241j7vwj0n")))

