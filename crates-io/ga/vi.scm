(define-module (crates-io ga vi) #:use-module (crates-io))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.36.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.15") (default-features #t) (kind 1)))) (hash "0ncx24axa2l1vikbppx7y6vncsvflj9h3g5fhq0aizadapc2dmfz")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.15") (default-features #t) (kind 1)))) (hash "0xb1rs4vjp662impcs77xxk6l86pr8nn0midacar7jhrm03i4r9g")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.40") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1i25ikn6ysgx7779wq42iqcdybsny1jh2snyn2v4ssxdlgch2grv")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1kwf0sxmv0w93ffgvzn29f3znpl1f957ch8ljh466i7jmmh7yd2s")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.44") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0865slr1nz33x4mr9w55nz6l30w81mig69xixv9yglbmp9h0m84i")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.46") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1k434216vqm1r9g6lk3cmk2cn6idcf135h6lhs0dazclqr2ywy5y")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.47") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "15zf1i4faq142x1v61p0j8ncb1fpkhkrha2rnh8kw0iswbcpxgiw")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.48") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1z557zp9agcjmjxy6p6ly178sbsfl0l1xgsg6nag6c2dz23p9vc9")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1qb0gpfn8lf4i3kgzrajc3yn91qcs0zgkdwg8gd9b5wdxxsdipr3")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.9") (deps (list (crate-dep (name "bindgen") (req "^0.50") (features (quote ("logging"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "11jw1pn0vd40dr845chdi9yc3pzpylrq3ybgb4s2s88minwy2n1y")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.10") (deps (list (crate-dep (name "bindgen") (req "^0.51") (features (quote ("logging"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0m11csmvhmp8m7f8f66gwk5yhlc779czrk6dvh02gj9jakw7a6ri")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.11") (deps (list (crate-dep (name "bindgen") (req "^0.52") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "02vjhzdy2klkfcgimc5bdv6gcfh55xczb35cvy0b75r3jcy07zjh")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.12") (deps (list (crate-dep (name "bindgen") (req "^0.53") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1dmaipyyds1czb21xb9k4py0zqarcbpkl1kk05n1x0fmpc5hyjy1")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.13") (deps (list (crate-dep (name "bindgen") (req "^0.54") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "18b2lldzzc5vaml5gg64vpdjwr1si5mdpcvi440mkrs610saj8f2")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.14") (deps (list (crate-dep (name "bindgen") (req "^0.55") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1qy2xzj96p8wsm38fmccz69y56di9gmvg0pxrl3kfkcjmm7f492x")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.15") (deps (list (crate-dep (name "bindgen") (req ">=0.56.0, <0.57.0") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 1)))) (hash "1lrwa123200wfjp3v3ljrdbm1rwvzxbvim56rmpmnjw37xgx3wc1")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.16") (deps (list (crate-dep (name "bindgen") (req "^0.57") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1vpjq6qm78f889pif627sila7fni7dgjzhsvxqldm8gj3r8nvsa5")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.17") (deps (list (crate-dep (name "bindgen") (req "^0.58") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0ihishpib7vxhlmyw4pidcdq3n494041sz1nmn4f0wbasawspl6k")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.18") (deps (list (crate-dep (name "bindgen") (req "^0.59") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "1wnms0qmli1svqpvgrv0w8643shjchblq56027jwfj73if2dj2j1")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.19") (deps (list (crate-dep (name "bindgen") (req "^0.60") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0zin41jyl6cri3041gdv9fnp0g2xk0jid9bd12rggjik55h37iyw")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.20") (deps (list (crate-dep (name "bindgen") (req "^0.60") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "1aar31whh579q0w1syf7i12w1fkq8mqd2s0vdird7abfggvnsv3v") (links "gtb")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.21") (deps (list (crate-dep (name "bindgen") (req "^0.60") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0zi1npd4fv2arbji482160cdrd0pb1jvhn2l7kcbq8avf1l3ly2h") (links "gtb")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.22") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)))) (hash "1q1fhj1fa7fbaha3d8bln8y6mk0c5ii8nprr1791yapgcamnmi6m") (links "gtb")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.23") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1qp1m855f48n2ijx0ffsfijpc53nk1m0w2kd34g42xqzlm94b13s") (links "gtb")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.24") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "09f5m58pz4dgj3khglz9gy821pbpvnc26p9kb4dbyljhgdpyfzqv") (links "gtb")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.25") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1yb6pahir6cjfh1sfk4piqwhzawdp4liqfj2nknczc9jjg4a1px9") (links "gtb")))

(define-public crate-gaviota-sys-0.1 (crate (name "gaviota-sys") (vers "0.1.26") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1frhghch9h0yg9hpsqg5nm4jl81z8b6w12br5l9vx21asnxyv9gb") (links "gtb")))

