(define-module (crates-io ga sp) #:use-module (crates-io))

(define-public crate-gaspi-reference-sys-0.0.1 (crate (name "gaspi-reference-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s2pqzgkap43w8wii0ncm5s103pbxg2p6j30lcdwz1ylp0cjbv9q")))

