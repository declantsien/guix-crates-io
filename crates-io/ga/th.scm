(define-module (crates-io ga th) #:use-module (crates-io))

(define-public crate-gather-0.0.0 (crate (name "gather") (vers "0.0.0") (hash "1mv4hpnnky56529m99qfa9z3rph310ms41ylqp5q8siwfnar1b1z")))

(define-public crate-gatherbrained-0.1 (crate (name "gatherbrained") (vers "0.1.0") (deps (list (crate-dep (name "rustyline") (req "^11.0") (default-features #t) (kind 0)))) (hash "0301ilgj3s3rv5j7niyzqn0f45ss38b5gp4ldbs8garyvx0zmlrh")))

(define-public crate-gatherr-0.1 (crate (name "gatherr") (vers "0.1.0") (hash "1q68ag7lbdrbv8spjmfaqnzjjb8ldh1ms1rhc0vlm7xjrifm0p4n")))

(define-public crate-gathery-0.0.0 (crate (name "gathery") (vers "0.0.0") (hash "0hdd21zpbdls9yhqyjk63jnflaldydcx1zj72wdp0aj4x1dk08zq")))

