(define-module (crates-io ga na) #:use-module (crates-io))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.0") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0pq67jdz9alb9cfi2h0240yrhwin8lic5pd7rnjn1zml785jn2yg") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.1") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1bcv9pw1lcv3qf4n8q196hhx1ylbnlys46v5860pbxz9hijapp3g") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.2") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "08xm0mp56zd2f46ys9gb1l6gnbf4xs4ymrg0adlhn1bqk66bj9ic") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.3") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1k39i7p5q2lizn9qv68mi5l7b36zjvx9h4isvf3k8yk0g8rip5sz") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.4") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.0-rc.2") (default-features #t) (kind 2)))) (hash "1pmwzyx9hm777vsgxhbgqyd40wg1l1z0mbqvchg8kjq68jzya1r5") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.5") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.0-rc.2") (default-features #t) (kind 2)))) (hash "1x397q9d3lmv6fkp8mbnkqivdai34nbyfmpn3dkc7rnwldfaf8fx") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.6") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.0-rc.2") (default-features #t) (kind 2)))) (hash "024dk9a2cv3g5as2iqigliwxv3a2yhzn4fzicafiqc3qwwjvwgr9") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1 (crate (name "ganache") (vers "0.1.7") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.0-rc.2") (default-features #t) (kind 2)))) (hash "14cjias8hkjhl79gh4w27i58ch670iifxx7s6df902ngz73y7ll2") (features (quote (("scalar_i32"))))))

(define-public crate-ganache-cli-0.0.1 (crate (name "ganache-cli") (vers "0.0.1") (hash "1wp5a4nsi490lha6c8c6y1pbxfszff2860hmb4yga9zd4p83mmia")))

