(define-module (crates-io ga zb) #:use-module (crates-io))

(define-public crate-gazbuzz-0.1 (crate (name "gazbuzz") (vers "0.1.0") (hash "14s8fbmgzywq0f1bcfdn01yra00sh6cxxv5ifsfld8v6ganidhwm")))

(define-public crate-gazbuzz-0.1 (crate (name "gazbuzz") (vers "0.1.1") (hash "0vmy0flzr7bw51bbm9rfnq412w8parbgm7kx0i9hmdg01gh921ym")))

(define-public crate-gazbuzz-0.1 (crate (name "gazbuzz") (vers "0.1.2") (hash "009qn0v61qgzm0f7kbgscclayvw14c0ab2qf2jjva46njdammbqi")))

(define-public crate-gazbuzz-0.1 (crate (name "gazbuzz") (vers "0.1.3") (hash "0l2rxpjyr2sazdv894f2ryjp8h8nrbmypqiha831mnxwhfk3xnm8")))

(define-public crate-gazbuzz-0.1 (crate (name "gazbuzz") (vers "0.1.4") (hash "0gbphiqx81vidhdy1lagqzx17n922m750zr6fhnih63kq6hgpaiz")))

(define-public crate-gazbuzz-0.1 (crate (name "gazbuzz") (vers "0.1.5") (hash "02vx84c2k3ifmvbyca3fwxhh0ak57vf22lsg6lnf8kha4cppfmrq")))

