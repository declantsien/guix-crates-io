(define-module (crates-io ga p-) #:use-module (crates-io))

(define-public crate-gap-buffer-0.1 (crate (name "gap-buffer") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.22") (default-features #t) (kind 0)))) (hash "1gzn8zrcr7ligz5d4cjd0cq1gznr0dmzwnarv2xfnq4civncyw5k")))

(define-public crate-gap-sys-0.2 (crate (name "gap-sys") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.65.1") (features (quote ("experimental"))) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "18xxqligfjsq1crcrrs8lph1jwg7my40is9phwpx866pbyhnqaxf")))

