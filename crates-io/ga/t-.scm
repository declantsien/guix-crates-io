(define-module (crates-io ga t-) #:use-module (crates-io))

(define-public crate-gat-lending-iterator-0.1 (crate (name "gat-lending-iterator") (vers "0.1.0") (hash "1s0g5nn2dhcx9gg7x2aa5f47dqf2cg35cprbwnnajp9wdsivbwls")))

(define-public crate-gat-lending-iterator-0.1 (crate (name "gat-lending-iterator") (vers "0.1.1") (hash "1177np61x3w8k1p085zi2m9ckz3y4s1gy2nzmd2b6ckn5lyis7q4") (rust-version "1.65")))

(define-public crate-gat-lending-iterator-0.1 (crate (name "gat-lending-iterator") (vers "0.1.2") (hash "0mnw2icb6d7ial7yxwk1krjx9bdhwk77x3lm69pp6xsiyim26s44") (rust-version "1.65")))

(define-public crate-gat-lending-iterator-0.1 (crate (name "gat-lending-iterator") (vers "0.1.3") (hash "1f9d5fkvwcdwdvkfqqh84c2n8kkfsrjjdsnw79zp5yxsgh6037c4") (rust-version "1.65")))

(define-public crate-gat-lending-iterator-0.1 (crate (name "gat-lending-iterator") (vers "0.1.4") (hash "1rj7gl36a2wzj1bd3jx5nw3jz5wq7zr6vvf60kvryf5s3c01mhqw") (rust-version "1.65")))

(define-public crate-gat-lending-iterator-0.1 (crate (name "gat-lending-iterator") (vers "0.1.5") (hash "1agx13403m08gk13sv8qhy9m4na97bm3lgpa1m0bdmdayawpncj2") (rust-version "1.65")))

(define-public crate-gat-std-0.1 (crate (name "gat-std") (vers "0.1.0") (deps (list (crate-dep (name "gat-std-proc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0py6jyzpklal1xdxqziz7sfjkk6wdaa9adhyjlpmb0482n34xkfr") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-gat-std-0.1 (crate (name "gat-std") (vers "0.1.1") (deps (list (crate-dep (name "gat-std-proc") (req "^0.1") (default-features #t) (kind 0)))) (hash "14v4ldnzi8y8zkcj2qq7rj4af5ygk0s9iklflssxpcdgqzsfp3p0") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-gat-std-proc-0.1 (crate (name "gat-std-proc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0br6d92fg4g7s81lsms6q3ayss1bl19fanqxc7v1csnic2vaw84c")))

