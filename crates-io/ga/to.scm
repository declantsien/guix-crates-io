(define-module (crates-io ga to) #:use-module (crates-io))

(define-public crate-gato-apache-cgi-0.1 (crate (name "gato-apache-cgi") (vers "0.1.0") (deps (list (crate-dep (name "gato-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bn1v6dd6grz7p1g1jm424czffd6vayqgjq30hw2vzy6fgxp0p42")))

(define-public crate-gato-apache-cgi-0.2 (crate (name "gato-apache-cgi") (vers "0.2.1") (deps (list (crate-dep (name "gato-core") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0080qm9kfkaxb63x7his7i45l034z0lbcr43m4fhgv06fqp9rpy5")))

(define-public crate-gato-apache-cgi-0.4 (crate (name "gato-apache-cgi") (vers "0.4.0") (deps (list (crate-dep (name "gato-core") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "19lznkmdz9f47jcviqqa6f66fd9wh54vdrx1p8grbr6bys4frglq")))

(define-public crate-gato-apache-cgi-0.4 (crate (name "gato-apache-cgi") (vers "0.4.1") (deps (list (crate-dep (name "gato-core") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "08s99h4rryp8h2h8gb3f2637jhykzczibilrjjbg8mblrdnqfsng")))

(define-public crate-gato-apache-cgi-0.5 (crate (name "gato-apache-cgi") (vers "0.5.0") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "1y1l5bw1xsav40y7sh9pcqrdwhcvyw1l2gyp6k6qvvz4rvi7h22y")))

(define-public crate-gato-core-0.1 (crate (name "gato-core") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hvx20nkc85451g24x88blvfnyg41sqq21bgnsfnrcmyhxfl68d5")))

(define-public crate-gato-core-0.2 (crate (name "gato-core") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r6yimjs7m90b4dc8l1jq3a5x02hkajpakw7shj0jk7pc2d5p0xx")))

(define-public crate-gato-core-0.2 (crate (name "gato-core") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b1zbf01rhp2qyhzc3fs79asgar1n0c2hq6da24854jh0ajv0aak")))

(define-public crate-gato-core-0.2 (crate (name "gato-core") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k0a0l3h96jms93bj91i4s4hlga9s4y8q5gpm0sk20fa12cp269p")))

(define-public crate-gato-core-0.3 (crate (name "gato-core") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cmsary8bvq4f283d06xhkr2a6lb0vx0yb50yn67czdwd6j515b4")))

(define-public crate-gato-core-0.4 (crate (name "gato-core") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yxsgbnf1qlqf3fdsrily9piqv9f5vid7lnizzn6a0p9wyhskc9h")))

(define-public crate-gato-core-0.4 (crate (name "gato-core") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sw4k2ck40n14vgwi8a2vahaqrdzn36f7fgmaap3hihll6xdhbfm")))

(define-public crate-gato-core-0.4 (crate (name "gato-core") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dv07ivvli9p3z0k60kf4kb2z0hqlk56my7l5bs3nbsk9ix9vjjp")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m48w92anjkdnlb23i5w08hab3l0wkq2r9q8cs6n50105s4ncaz9")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nfxjyimiwzy0a1afk3fii1bk1my9inrn9kcrglw0c1akaz3br17")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jh3shklab1vjfjjqd1rdj3wjasz2jaxlr8mmfcyjw13izkj7zz8")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qy44zmb58zziyfjq50xrsshfprx1ccyvifczaxhlawhjv6qf249")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nbdisc90c28yi85l0mfahiib2mmffmq36xcsn4rzvvccarls07r")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "102bvlyg967aaz27v1d892m41nzxkmsl82pjllr5f3x3hjlv9mqq")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "141as46xjaa3zwyvnwngaiiv4s3pcgrqzax59wy3z8d9y9hg2v11")))

(define-public crate-gato-core-0.5 (crate (name "gato-core") (vers "0.5.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ax47pnhycmlws8zv6bj7g8k1k3pwccymhl8li3sn3i57614gxgf")))

(define-public crate-gato-http-server-0.0.1 (crate (name "gato-http-server") (vers "0.0.1") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "06gaccy5cy2c9w0jdjifni7ljn3vv8mdj9i04y2rbqqp2c8qhj6j")))

(define-public crate-gato-http-server-0.0.2 (crate (name "gato-http-server") (vers "0.0.2") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "0cbnry4i5hzi2m3626h4n307b4cl6zqgpbxw36m3qkgxv1carw1j")))

(define-public crate-gato-http-server-0.0.3 (crate (name "gato-http-server") (vers "0.0.3") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "0v47vd45n09n724nb4di6s6l3kf3x856nf7xxi2ncz1wg14gxxgi")))

(define-public crate-gato-http-server-0.0.4 (crate (name "gato-http-server") (vers "0.0.4") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "08iynxjjl11xfan2izjnqiy8n5vizc3hrjw9jlhbnm33hdpcim00")))

(define-public crate-gato-lambda-0.5 (crate (name "gato-lambda") (vers "0.5.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fly509s62aa8ns8rlqsr1yyskq36qn6n6capwbisnzrwm14hp5n")))

(define-public crate-gato-lambda-0.5 (crate (name "gato-lambda") (vers "0.5.1") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vrr6r0r7hqhbhwv3x5p4m9kj0lxqv5n8qcsfq00ym9ms62vbkk8")))

(define-public crate-gato-simple-router-0.1 (crate (name "gato-simple-router") (vers "0.1.0") (deps (list (crate-dep (name "gato-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qwklg9f8kzwznli5k557x2h032bz1wi2w16g87jgqrv2k9d0p64")))

(define-public crate-gato-simple-router-0.2 (crate (name "gato-simple-router") (vers "0.2.1") (deps (list (crate-dep (name "gato-core") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w0cpyx13np5cmwify769fslnc2gdsblj4wmjk7ni3bpwkf5qhip")))

(define-public crate-gato-simple-router-0.4 (crate (name "gato-simple-router") (vers "0.4.0") (deps (list (crate-dep (name "gato-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "124sacl8ml1qy8ad8yvpvfxlm53cl5h4ckzvyjz53p9qnmkis363")))

(define-public crate-gato-simple-router-0.4 (crate (name "gato-simple-router") (vers "0.4.1") (deps (list (crate-dep (name "gato-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "01gwdn4vldgjfliczs00ykra1m71j1sr2lnjs6bm6ybbqxx93vqx")))

(define-public crate-gato-simple-router-0.4 (crate (name "gato-simple-router") (vers "0.4.2") (deps (list (crate-dep (name "gato-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xdxqnkfql6k4y2j9fzgm2a06ycpb47hf8kmdzj3mak31qhpxyp8")))

(define-public crate-gato-simple-router-0.4 (crate (name "gato-simple-router") (vers "0.4.3") (deps (list (crate-dep (name "gato-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yw8fbdxjprzsjdxq746hkwv5xz6ymz10cb8iwp9ff2ia88i0f02")))

(define-public crate-gato-simple-router-0.5 (crate (name "gato-simple-router") (vers "0.5.0") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "06cyjl58daa19ak9q969i2xv8mx7w5mb4g5m22rmz3db0v7sqc73")))

(define-public crate-gato-simple-router-0.5 (crate (name "gato-simple-router") (vers "0.5.1") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11d6rg78x6m8hdbw3c337m5n7zkdzc6h98pm5h79d89c8ppd3r0x")))

(define-public crate-gato-simple-router-0.5 (crate (name "gato-simple-router") (vers "0.5.2") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "19da1zlmzf7r1j23m07kaisrd0y69rqm8v1qb693cyswbds3bz2b")))

(define-public crate-gato-simple-router-0.5 (crate (name "gato-simple-router") (vers "0.5.3") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h65gd39f2vr9gx5m0l1irb894kir7r335b0cgglpnkglk6y8xhs")))

(define-public crate-gato-simple-router-0.5 (crate (name "gato-simple-router") (vers "0.5.4") (deps (list (crate-dep (name "gato-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "16vkgpwcipwmnx9pjg6zrw3pzvygfpqzdq9ajwza9qqr3klvpx8z")))

(define-public crate-gato-stdout-logger-0.5 (crate (name "gato-stdout-logger") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gato-core") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1zhac7b139dfjp61z85zhm8vblssi8qdd8lhi9xv02gzvza786i2")))

(define-public crate-gator-0.0.0 (crate (name "gator") (vers "0.0.0") (hash "069knpfcmzfkhnf00nvq4n7wz3khyj8a6dv1xvanhzg9rviazs3z")))

