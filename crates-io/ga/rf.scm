(define-module (crates-io ga rf) #:use-module (crates-io))

(define-public crate-garfield-0.1 (crate (name "garfield") (vers "0.1.0") (hash "172x2pqwm7fj1v310lnfjmqgc12ifl1qqsl22wvr17lhbhvdkyk0")))

(define-public crate-garfsay-0.1 (crate (name "garfsay") (vers "0.1.0") (hash "0lyzklss3ywfm2dagrhzg3zmpalgbsa1lp054xlzzgfrzrh6qyj3")))

(define-public crate-garfsay-0.1 (crate (name "garfsay") (vers "0.1.1") (hash "0ic2bsmc2nfxrp0q51wxxfg9gz5llnwzwdy70k3n33an39djy028")))

