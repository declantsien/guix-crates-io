(define-module (crates-io ga rl) #:use-module (crates-io))

(define-public crate-garlandtools-0.1 (crate (name "garlandtools") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "13dj8xpfm5p4xh7pc1f3f166p3r4y1q09iyvk8csq5x8krylifm3")))

(define-public crate-garlandtools-0.1 (crate (name "garlandtools") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1830kdcs1zdidab0v3l0n4a7pg8dcaws5axa602byxqiilqq0d6w")))

(define-public crate-garlic-0.1 (crate (name "garlic") (vers "0.1.0") (hash "184g21ikgj31q53hs2m2khc63xmv1syvxhkgn6jxlni1kp7ix8xx")))

