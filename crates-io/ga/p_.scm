(define-module (crates-io ga p_) #:use-module (crates-io))

(define-public crate-gap_query_interval_tree-0.0.1 (crate (name "gap_query_interval_tree") (vers "0.0.1") (deps (list (crate-dep (name "discrete_range_map") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)))) (hash "09g138sk7i4di7r8vwcqba8yh8362hn8b421hg1gsnspsz5jgdnq")))

(define-public crate-gap_query_interval_tree-0.1 (crate (name "gap_query_interval_tree") (vers "0.1.0") (deps (list (crate-dep (name "discrete_range_map") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)))) (hash "03gqhfnyhcqv5j6zhc1ah2431jp2wwvhin2fd19frxfnkhsxcsnn")))

(define-public crate-gap_query_interval_tree-0.2 (crate (name "gap_query_interval_tree") (vers "0.2.0") (deps (list (crate-dep (name "discrete_range_map") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("alloc"))) (kind 0)))) (hash "0xcwx92sdbk1pqs0q51qpamnwnki0sbmfn2d5zcx7hw7chridyds")))

(define-public crate-gap_solver-0.1 (crate (name "gap_solver") (vers "0.1.0") (hash "0g0xzkirvi2hswv9vv0cvc0hsw1filn7ymsp7xbc45mdb1dwdyx1")))

(define-public crate-gap_solver-0.2 (crate (name "gap_solver") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1pb4g4np7wrb4r8vb0iaiq3ijplx7v07a3nwl00hay182d2p0pp2")))

(define-public crate-gap_vec-0.1 (crate (name "gap_vec") (vers "0.1.0") (hash "1a3hqnqskr5k9yn2ipyfqd5z66nb5bi7420vz0q4q09ibr54rnff")))

