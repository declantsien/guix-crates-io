(define-module (crates-io ga mb) #:use-module (crates-io))

(define-public crate-gambero-0.1 (crate (name "gambero") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-tungstenite") (req "^0.13.0") (features (quote ("async-std-runtime" "async-tls"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "squalo") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i95yp1w64820ka0l1s1cmdybmi9zflnavlrnpwjwrb3kx4ahglq")))

(define-public crate-gambero-0.1 (crate (name "gambero") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-tungstenite") (req "^0.13.0") (features (quote ("async-std-runtime" "async-tls"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "squalo") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "017a5z0b0h6qjm4209k2vfgj6504b76dx7lbrc6ybmf153fmr9mh")))

(define-public crate-gambero-0.1 (crate (name "gambero") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-tungstenite") (req "^0.13.0") (features (quote ("async-std-runtime" "async-tls"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "squalo") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bhl94k9rmd1zlvmnahbja7gj7q6p37l5nakwx38i2vqnm26lx2v")))

(define-public crate-gambit-0.0.0 (crate (name "gambit") (vers "0.0.0") (hash "0vy1x9bk115r7z6gnmws5wkczl9llxmk1fp1nph1b8wg0pm98gm2")))

(define-public crate-gambit-parser-0.1 (crate (name "gambit-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ja27nwa3l35zw6nzxlymd14aqdjpvcqsd1bawbqcw4s803x3d03")))

(define-public crate-gambit-parser-0.2 (crate (name "gambit-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0q2a6n3qk6wakdv53yisc3i3y2hq9vzbm0x3a3bgiqjjin28yr9s")))

