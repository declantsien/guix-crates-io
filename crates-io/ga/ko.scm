(define-module (crates-io ga ko) #:use-module (crates-io))

(define-public crate-gakona-0.2 (crate (name "gakona") (vers "0.2.0") (deps (list (crate-dep (name "gulkana") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1rhgayg12283bg9yqh8k6miv3c44mfskm9h1qx9gxm8dkgdv9aqv")))

(define-public crate-gakona-0.3 (crate (name "gakona") (vers "0.3.0") (deps (list (crate-dep (name "gulkana") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0y6hivdq2f68171fpccbpg4i6qk2qz1hlc55bx1pi109wfk6fgdk")))

(define-public crate-gakona-0.4 (crate (name "gakona") (vers "0.4.0") (deps (list (crate-dep (name "gulkana") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "10kqjbs32d60pk48fqkk2il3pzski1pcl4p93ckznacpp6abm6xa")))

(define-public crate-gakona-0.5 (crate (name "gakona") (vers "0.5.0") (deps (list (crate-dep (name "gulkana") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1n7wb937f6rnwhp0jzx1invn2dgz7i3bfg424ywwb5pa7xg4jc5z")))

(define-public crate-gakona-0.5 (crate (name "gakona") (vers "0.5.1") (deps (list (crate-dep (name "gulkana") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "01ypzk3bqplqcgnfbvw29bws2jcsr8327qcayiwc4gy711r6bhwi")))

(define-public crate-gakona-0.5 (crate (name "gakona") (vers "0.5.2") (deps (list (crate-dep (name "gulkana") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0brixa2n8jrms1sysqxi0hmshxwyx4qhgwqrj738zgzz7m16c67n")))

(define-public crate-gakona-0.5 (crate (name "gakona") (vers "0.5.3") (deps (list (crate-dep (name "gulkana") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1l16xh6v1wsmspgxk4jb2apn0ngp3zrnycaqa27rm69cr9q6589a")))

(define-public crate-gakona-0.5 (crate (name "gakona") (vers "0.5.4") (deps (list (crate-dep (name "gulkana") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1n87brly1kp1hlz0653qygcp3ksxq5f3nwplgflnardlq9a7c8m8")))

(define-public crate-gakona-0.5 (crate (name "gakona") (vers "0.5.5") (deps (list (crate-dep (name "gulkana") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "00bcvg55h5s94ywgzvzwlrsy01k41k9l3jbzzy8k9l2p7bsr003g")))

(define-public crate-gakona-0.5 (crate (name "gakona") (vers "0.5.6") (deps (list (crate-dep (name "gulkana") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1sy9l4dw10hfclvdcxaawdyfwm7szwp89bx1djn5kp7r0dfixvi1")))

