(define-module (crates-io ga al) #:use-module (crates-io))

(define-public crate-gaal-0.1 (crate (name "gaal") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "synnax") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (features (quote ("crossterm"))) (default-features #t) (kind 0)))) (hash "0l7a4rijjwqsbi0xql1ia32xmk1i2kjwhhgjyzc0z9pk8x07jz6n")))

(define-public crate-gaal-0.1 (crate (name "gaal") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "synnax") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (features (quote ("crossterm"))) (default-features #t) (kind 0)))) (hash "1dglngcwy7i9kcjhw52d4yinkkilvsn31vm51f2ignqzvzw11cd5")))

