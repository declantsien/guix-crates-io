(define-module (crates-io ga ol) #:use-module (crates-io))

(define-public crate-gaol-0.1 (crate (name "gaol") (vers "0.1.0") (hash "11ly7mbs8gbwbvrq0x68hsz4ag5c550n8iczv2g64mrcibncxjff")))

(define-public crate-gaol-0.2 (crate (name "gaol") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zq7w7l6ghrndx1s0mrvzipkqjryfvm74ak4381v2n3v6z5kgi1m")))

(define-public crate-gaol-0.2 (crate (name "gaol") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "08lwja6mah80s08nzpgr1fhvvfn7lslr78xwkskkjslngb55f686")))

