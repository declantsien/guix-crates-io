(define-module (crates-io ga dj) #:use-module (crates-io))

(define-public crate-gadjid-0.0.0 (crate (name "gadjid") (vers "0.0.0-a.0") (hash "1zdcs4dmy1mgcbfdzwgw1y9yq2f8g3z4w16l7gqj2zglq3l2510g") (yanked #t)))

(define-public crate-gadjid_python-0.0.0 (crate (name "gadjid_python") (vers "0.0.0-a.0") (hash "1zf06087misr7v8sz0is6qbla36llq10nz3llhygd7wn7ggbsg52") (yanked #t)))

(define-public crate-gadjid_r-0.0.0 (crate (name "gadjid_r") (vers "0.0.0-a.0") (hash "06dr72d0iy76vh642688zl21prfwg0d3pfb1dj7jz2nc7jq1ksd5") (yanked #t)))

