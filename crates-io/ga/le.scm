(define-module (crates-io ga le) #:use-module (crates-io))

(define-public crate-gale-0.0.0 (crate (name "gale") (vers "0.0.0") (hash "1prldh946sy4gw6mmncn58ws8l83x837jz4bhqxpwdqjms7bgh1s")))

(define-public crate-gale-0.0.3 (crate (name "gale") (vers "0.0.3") (hash "0z1i4pr7wjxap7ymwar3a7p1lrva04c1d31ymms0x5db4qnpkqf0")))

(define-public crate-galemu-0.2 (crate (name "galemu") (vers "0.2.0") (hash "1ypsa1pyyw5g2bkxc5cm9wxjcpnwk01mxvgwg3x62jm78a0pf791")))

(define-public crate-galemu-0.2 (crate (name "galemu") (vers "0.2.1") (hash "01b8xb11b3913zhwc13glgmgn237w9iwx3jy3sii2dn71wdgq7iq")))

(define-public crate-galemu-0.2 (crate (name "galemu") (vers "0.2.2") (hash "0nvwcbl9vy03xfw6ch62sz8pc45vr9038lbfqryxxw1cycwp1bzc")))

(define-public crate-galemu-0.2 (crate (name "galemu") (vers "0.2.3") (hash "0ab3xdfw6l1gl25srmvmhr6qfmhd487bkr9lw4wvb8j49wpl8lxm")))

(define-public crate-galena-0.1 (crate (name "galena") (vers "0.1.0") (hash "17wmrb2fi01xvjjc265kq70la72hcqxx0dpqqnwnip94d3i7j636")))

(define-public crate-galeon-0.1 (crate (name "galeon") (vers "0.1.0") (hash "1bfz26wx4mjgk4ndgnqlfqfwnlywkwpjzwfa0wyg3y04fvb7b46r") (yanked #t)))

(define-public crate-galerio-1 (crate (name "galerio") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tera") (req "^1.5") (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "1hkk33j2f04s36djhznhrrwijh2kzh1k8rvlg4cfv883ll98nhp5")))

(define-public crate-galerio-1 (crate (name "galerio") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tera") (req "^1.5") (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "02zmwjrv2s7ps1xqm2r9dq7vc4m888fn1qlg3am7ndhga7pqyini")))

(define-public crate-galerio-1 (crate (name "galerio") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tera") (req "^1.5") (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "0wd80lqnbgc9prc9bxgffnnxjb9g87l7wvgjjy9sm9jak8qdmv57")))

(define-public crate-galerio-1 (crate (name "galerio") (vers "1.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "kamadak-exif") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tera") (req "^1.5") (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "0nqkg3jksnm3iv8rnyn6q2r7gk7jdg3zrir6yrcf6r8k8ksvqrk4")))

(define-public crate-galeshapley-0.0.2 (crate (name "galeshapley") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1vb31k4yzmgh90p94mfg09r1xys3rw9fm980716wg90yrs7g1y7d")))

(define-public crate-galeshapley-0.0.3 (crate (name "galeshapley") (vers "0.0.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1mdawn167i6q5hllkr6isz26hrxz2p45yampsz830n5gd7112r5q")))

(define-public crate-galeshapley-0.0.4 (crate (name "galeshapley") (vers "0.0.4") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0iydv8cil1yb7npkyw89112llhai7r6h8r5x9f8vxbdqylbi684r")))

(define-public crate-galette-0.3 (crate (name "galette") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1x891ba1s4x9inar82rw8bhkgrvkw31m952ayz712aqfpwqzgb2b")))

