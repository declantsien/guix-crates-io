(define-module (crates-io ga rs) #:use-module (crates-io))

(define-public crate-gars-0.0.3 (crate (name "gars") (vers "0.0.3") (deps (list (crate-dep (name "google-authenticator") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "02gn6x1m19vcvv74iviicnqa8dwqrppgvikcl2qf7485ngr2kcpk") (yanked #t)))

(define-public crate-gars-0.0.4 (crate (name "gars") (vers "0.0.4") (deps (list (crate-dep (name "google-authenticator") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "11gkwm964br34paq5q1hs4sbsz9zap9qiwbrsdwflx0jnvv29zd1")))

(define-public crate-gars-0.0.5 (crate (name "gars") (vers "0.0.5") (deps (list (crate-dep (name "google-authenticator") (req ">=0.2.1, <0.3.0") (default-features #t) (kind 0)))) (hash "0064f6hjxqf6pp4djk3jky1k2xiv4b3wv4v52jn8hznnixrk4l35")))

(define-public crate-gars-0.1 (crate (name "gars") (vers "0.1.0") (deps (list (crate-dep (name "google-authenticator") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1g374g6sap14mbacvcl3261hrfl05my65lviih377ncrshqj24kx")))

