(define-module (crates-io ga lg) #:use-module (crates-io))

(define-public crate-galgebra-0.1 (crate (name "galgebra") (vers "0.1.0") (hash "1lgxjy53j7gyjfb97ggg2ni19pr5gj8p6d4l214s3yii1gcm15s6")))

(define-public crate-galgebra-0.1 (crate (name "galgebra") (vers "0.1.1") (hash "1jpla65517lchgfs5w64qg45k6dwbn00dd5ysl8yqrayqsby29dl")))

(define-public crate-galgebra-0.2 (crate (name "galgebra") (vers "0.2.0") (deps (list (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "1n7pi7hsims80487d285zvdx27rgcgclpn33d77dvk7fylf7vdkg")))

(define-public crate-galgebra-0.2 (crate (name "galgebra") (vers "0.2.1") (deps (list (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "1gg4nw86lv3v3nbvj7c5c8cr5sdq61wx4fxza599c7b2ff9y542c")))

(define-public crate-galgebra-0.2 (crate (name "galgebra") (vers "0.2.2") (deps (list (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "0mq5qwdb05idyn92ir2vznjaq5par6zd4pyzj23j3gv6srmg8p7f")))

(define-public crate-galgebra-0.2 (crate (name "galgebra") (vers "0.2.3") (deps (list (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "1qg9gzcvfr5xs0fj6zx7hyxayz3s3a571n2zpqch27ikna9rv10m")))

(define-public crate-galgebra-0.2 (crate (name "galgebra") (vers "0.2.4") (deps (list (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "02p7h7qszd0lbaf1ly41wz8wpg1nmsql8jn7isdi5z136vp93mkv")))

