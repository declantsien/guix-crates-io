(define-module (crates-io ga br) #:use-module (crates-io))

(define-public crate-gabriel-0.0.1 (crate (name "gabriel") (vers "0.0.1") (hash "1y2z69gvpyh9rgbryk8rxprpi8fy7hq6g9k5hkh81ipwdg899y3c")))

(define-public crate-gabriel-0.0.2 (crate (name "gabriel") (vers "0.0.2") (hash "1vk3rjjsd5cbq1q9y2zaxmsr54n5wffk3b9ka17cpc00q92sc1s0")))

(define-public crate-gabriel-0.0.3 (crate (name "gabriel") (vers "0.0.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0m82hpsl4y2m51jp0kd18rsbynq9v8nw9icqlv4znpa52n21n98i")))

(define-public crate-gabriel2-0.0.1 (crate (name "gabriel2") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0ssn6amyv3x7sgj9d59z7nkx7fmg9y3j3dh25959m248vq5rwdsc")))

(define-public crate-gabriel2-1 (crate (name "gabriel2") (vers "1.0.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j74l2ajllia13b50d2cnijgpmcyp85bn65fqxxw0cs4gqaa4ga5")))

(define-public crate-gabriel2-1 (crate (name "gabriel2") (vers "1.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0aavgi7i4ifkf1rh73zm6kyhi80bg4w66aczx4aaxig5p8ibff8m")))

(define-public crate-gabriel2-1 (crate (name "gabriel2") (vers "1.0.3") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i2wadxqqagg2mvh0vzbb4lid597yppnxm8xwjzbd6ixkh2gq84d")))

(define-public crate-gabriel2-1 (crate (name "gabriel2") (vers "1.0.4") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fl8jk4j0j1fhs01m8wmi7msrlcia3iai5z8jnml7k41hhxnqv71")))

(define-public crate-gabriel2-1 (crate (name "gabriel2") (vers "1.0.5") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "075728ij9yn6ps9fhqcg4zi5nypqjxk0jymq6g83bl0xszf7gl2v")))

