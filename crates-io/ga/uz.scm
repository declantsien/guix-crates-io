(define-module (crates-io ga uz) #:use-module (crates-io))

(define-public crate-gauze-0.1 (crate (name "gauze") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.3") (default-features #t) (kind 0)))) (hash "1i7dvysw3w80hqpcfbvw427zi27kch1apqb9r48m2b4d2zm6srsb")))

(define-public crate-gauze-0.1 (crate (name "gauze") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.3") (default-features #t) (kind 0)))) (hash "1ns0yki2a4lh1xv8xqm0vh4szj22x1izrq4rkb830ipq5gbk1skv")))

(define-public crate-gauze-0.1 (crate (name "gauze") (vers "0.1.2") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.3") (default-features #t) (kind 0)))) (hash "1wyav523j778fd86k43cr2f91rfx53389z7krgb4z037b2bvmzrl")))

(define-public crate-gauze-0.1 (crate (name "gauze") (vers "0.1.3") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.3") (default-features #t) (kind 0)))) (hash "1ic53lbhak3hpy31cjbh705x2falqdfs7cw7p0vj3bkcap6g7y2z")))

(define-public crate-gauze-0.1 (crate (name "gauze") (vers "0.1.4") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.3") (default-features #t) (kind 0)))) (hash "18cqawd263g38gdw9w2cy9wj9fww4h3j1qrpqa9c4bmcfzkpj84h")))

