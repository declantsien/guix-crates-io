(define-module (crates-io ga ml) #:use-module (crates-io))

(define-public crate-gamlr-0.1 (crate (name "gamlr") (vers "0.1.0") (hash "0qajz46crl073f0z11lnyw8kizll9vxnks6hknbpyz45lr3chkv1")))

(define-public crate-gamlr-0.1 (crate (name "gamlr") (vers "0.1.1") (hash "0rp4ca06jfrwhxcbh1ka5d4i9lwijvvr79nsii8d9w9awx8yp2jn")))

(define-public crate-gamlr-0.1 (crate (name "gamlr") (vers "0.1.2") (hash "1crmgangggvd91j3fpal0p7q5lijivh1ld2cpr3zl8hq9v0h57gr")))

(define-public crate-gamlr-0.2 (crate (name "gamlr") (vers "0.2.0") (hash "1zs5h14ccnvnfns0khz1p4yz0ah0irl9d232fq3hcnczq2ffr2kp")))

(define-public crate-gamlr-0.2 (crate (name "gamlr") (vers "0.2.1") (hash "0lhph8r3gdsrpw1bhxvw95gzn9xaxj3rp2p201p8zrr7154c9arr")))

(define-public crate-gamlr-0.3 (crate (name "gamlr") (vers "0.3.0") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0y0nkikgx0zlf01pqhxhcnda3p864626mm6ksd5mi4y1528mvjsz")))

