(define-module (crates-io ga ry) #:use-module (crates-io))

(define-public crate-gary-core-0.0.1 (crate (name "gary-core") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1ly1dmdvsp3zbv7w97inhkhqq2jhxz85v8xklkkakw878d83fpsn")))

(define-public crate-gary-plugin-docker-0.0.1 (crate (name "gary-plugin-docker") (vers "0.0.1") (deps (list (crate-dep (name "bollard") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "core") (req "^0.0.1") (default-features #t) (kind 0) (package "gary-core")) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w1ln81lsq25lhwn3fl3nxg7x86mkl4bl877yk1xz51g1kwhfnck")))

(define-public crate-gary-plugin-docker-0.0.2 (crate (name "gary-plugin-docker") (vers "0.0.2") (deps (list (crate-dep (name "bollard") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "core") (req "^0.0.1") (default-features #t) (kind 0) (package "gary-core")) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zylpd78n1ddcdq4xzcqpahj5dl6y9hmab1vzixgjaj1rri7gsjd")))

(define-public crate-gary_art-0.1 (crate (name "gary_art") (vers "0.1.0") (hash "0ghvm25v58ir9nl1d36dibpmm3fqlvq9kxlc7qf74jmzcxg73sad")))

