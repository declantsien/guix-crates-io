(define-module (crates-io ga st) #:use-module (crates-io))

(define-public crate-gastimation-0.0.0 (crate (name "gastimation") (vers "0.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.8.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "166ykmcnnm4hfsvvqavamwv66vvir1dg0j4k7rghl0qr2pfjycz6")))

