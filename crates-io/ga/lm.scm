(define-module (crates-io ga lm) #:use-module (crates-io))

(define-public crate-galm-0.0.2 (crate (name "galm") (vers "0.0.2") (hash "1s6iwabn9id4al02qpb07gg8w8k7j4x995pkqhl7pj82yxmv52p9")))

(define-public crate-galm-0.0.3 (crate (name "galm") (vers "0.0.3") (hash "0v5mcnhpld9zgix3b2wvsgpgz8cxxv16fkcw5zla4g0jn3bws4p3")))

(define-public crate-galm-0.0.4 (crate (name "galm") (vers "0.0.4") (hash "1ipk9qvwl1jb5nqm20jdh02vfwjmc1gqisaqnd12zi1zsvy3iszv")))

(define-public crate-galm-0.0.5 (crate (name "galm") (vers "0.0.5") (hash "1g81ycfg0v1g6ysrgf3riyr5jy05qlf3yy7d0lvsl8gp7j2fycpq")))

(define-public crate-galm-0.0.6 (crate (name "galm") (vers "0.0.6") (hash "02dsqfzf1zb6n2k04whfv370nzl009hnv4psdhsfz7lxg3i772jn")))

(define-public crate-galm-0.0.7 (crate (name "galm") (vers "0.0.7") (hash "0mcg1v0zwyawz6y87mqm28n9n0cdpmvgpxam7w9dsd7a0sbkc397")))

(define-public crate-galm-0.0.8 (crate (name "galm") (vers "0.0.8") (hash "0a7dzvb3j2zf25wzjpp326d52wjgkx5cj3dpbvhcslc7cg16m9li")))

(define-public crate-galm-0.0.9 (crate (name "galm") (vers "0.0.9") (hash "1nbz5r3lbc3pfj12vy1lq8l9dfgjlxy3lg26mk39k5a0grm147p4")))

(define-public crate-galm-0.0.10 (crate (name "galm") (vers "0.0.10") (hash "0gyn94i5s3id42mjmc1s9a093fl2cwjvvpysqispmr5xd0ynpxh1")))

(define-public crate-galm-0.0.11 (crate (name "galm") (vers "0.0.11") (hash "1l6m6rbkmw1vxsdrkfdr99iz1gw62m6hn2z6kw8g9d4wvn9asw01")))

(define-public crate-galm-0.0.12 (crate (name "galm") (vers "0.0.12") (hash "0m70jrjl5lxg5m3fbcvrxf97ps1jrm2rd1srls41apmqnr65kjyg")))

(define-public crate-galm-0.0.13 (crate (name "galm") (vers "0.0.13") (hash "10sllhnd3hr8nrmlkz7l73v6ayabanima6szivr481b79chcyy83")))

(define-public crate-galm-0.0.14 (crate (name "galm") (vers "0.0.14") (hash "0izmizpkk44myqbijhl1755lgbgp29sydrfrpsnn9qnj565j6c9x")))

(define-public crate-galm-0.0.15 (crate (name "galm") (vers "0.0.15") (hash "0fd0pbfyd4i0d8jan2rrdhcn3x8rrdanam6r2mrrbl6b69qz2fvi")))

(define-public crate-galm-0.0.16 (crate (name "galm") (vers "0.0.16") (hash "16i9z1csz7km3dh8nshzzw7d5iyd0rwci6c0jw2lc350fjzrdsc5")))

(define-public crate-galm-0.0.17 (crate (name "galm") (vers "0.0.17") (hash "0cqbcb2wwjxhxbafd0yxl96hp3k8ri8k9kykwmsb78wwb5lkkvw1")))

(define-public crate-galm-0.0.18 (crate (name "galm") (vers "0.0.18") (hash "0vrkkq4z0fvpb36fmsa9ldgyklj6dvj1wkdam034xb70yjjfavvz")))

(define-public crate-galm-0.1 (crate (name "galm") (vers "0.1.2") (hash "13qs7hanykzyi9agpclrnfcgp0qsd372zqn9385djw0rwpvigkff")))

(define-public crate-galm-0.1 (crate (name "galm") (vers "0.1.3") (hash "00ma7slimna5pm9r7g03k564g9hfsa6fvh4xg2jb67kmfmm80s66")))

(define-public crate-galm-0.1 (crate (name "galm") (vers "0.1.4") (hash "1vjhjmdgzhnmalmgb6pncc4awfy3m28rr5daj87h9qw5sw4faanj")))

(define-public crate-galm-2 (crate (name "galm") (vers "2.0.1") (hash "0rkl7bqyfg0nd9fp0fbqzx9a9xwz4myvv9sfa0m5y0jcf45lzy80") (yanked #t)))

(define-public crate-galm-0.2 (crate (name "galm") (vers "0.2.1") (hash "0p65rga0px8wi1fknf6n6iivnhi4slkyzlr904bkijdla846vqnb")))

(define-public crate-galm-0.2 (crate (name "galm") (vers "0.2.2") (hash "039m9lkrr8fxbgbs4sf6iz8i6cq8dw6mad0dr655mgjgfs2gb536")))

(define-public crate-galm-0.2 (crate (name "galm") (vers "0.2.3") (hash "0iz4lsn2gqbq4fgjh959vr211s1fjl76r4lajrj1l0kmcpcbi4mx")))

(define-public crate-galm-0.2 (crate (name "galm") (vers "0.2.4") (hash "0zl4v9h4xbhj8wvv1k9npvlb3liv1ddh6zd2izzgvk8h25zq3pac")))

