(define-module (crates-io ga rc) #:use-module (crates-io))

(define-public crate-garcon-0.1 (crate (name "garcon") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0.3.13") (optional #t) (default-features #t) (kind 0)))) (hash "07va4cq5c7vmffvzvj3xakcmaibhsbbmh0gpzz7rpjvcb1ybwh1s") (features (quote (("async" "futures-util")))) (yanked #t)))

(define-public crate-garcon-0.1 (crate (name "garcon") (vers "0.1.1") (deps (list (crate-dep (name "futures-util") (req "^0.3.13") (optional #t) (default-features #t) (kind 0)))) (hash "0lh0qzh7g2nr5kj9811kb71jvci87jnm84hxdgiznawj79bwb0ag") (features (quote (("no_std") ("async" "futures-util"))))))

(define-public crate-garcon-0.1 (crate (name "garcon") (vers "0.1.2") (deps (list (crate-dep (name "futures-util") (req "^0.3.13") (optional #t) (default-features #t) (kind 0)))) (hash "19azf3ga41vzf9dj5q42g6ybzkqx2aa7xgrwjmlg0b28qf83gxk1") (features (quote (("no_std") ("async" "futures-util"))))))

(define-public crate-garcon-0.2 (crate (name "garcon") (vers "0.2.0") (deps (list (crate-dep (name "futures-util") (req "^0.3.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 2)))) (hash "0wg3rak353jjjzs7gmxgfd19nxhc9hrzq5l1i0726snf51l99a5y") (features (quote (("no_std") ("async" "futures-util"))))))

(define-public crate-garcon-0.2 (crate (name "garcon") (vers "0.2.1") (deps (list (crate-dep (name "futures-util") (req "^0.3.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 2)))) (hash "19r2j07ism836arcs0hvcm7gkh2bq81zjax8s7xnsi7r4kg7ydgb") (features (quote (("no_std") ("async" "futures-util"))))))

(define-public crate-garcon-0.2 (crate (name "garcon") (vers "0.2.2") (deps (list (crate-dep (name "futures-util") (req "^0.3.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 2)))) (hash "152v1f58b5zv80b1n6vxdzpa559d8bnk4cfjgwiqxxxis6j2a7hc") (features (quote (("no_std") ("async" "futures-util"))))))

(define-public crate-garcon-0.2 (crate (name "garcon") (vers "0.2.3") (deps (list (crate-dep (name "futures-util") (req "^0.3.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 2)))) (hash "0sd01qp8yi406d80pdcgmbjya1sdmqhqb6b37092cg6d3nbbhgz8") (features (quote (("no_std") ("async" "futures-util"))))))

