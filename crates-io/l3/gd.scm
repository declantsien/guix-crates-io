(define-module (crates-io l3 gd) #:use-module (crates-io))

(define-public crate-l3gd20-0.1 (crate (name "l3gd20") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zjcafk7pg45nmvz0bp7cdqqgd8096p4fclm09a73302lsi29bsh")))

(define-public crate-l3gd20-0.1 (crate (name "l3gd20") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1db4z54hwfn8v48fmz9kv8fw4gwqiy15yi9z26687ld8365hl3xv")))

(define-public crate-l3gd20-0.1 (crate (name "l3gd20") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17mcdv79q4imaskglh47b6xsp1a2hzb515021shksr8in2c132iq")))

(define-public crate-l3gd20-0.2 (crate (name "l3gd20") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1v4nbc5x3jqx0w63fnhfl0v8bi81vs4i2qznrp2lzhgl26blmnpk")))

(define-public crate-l3gd20-0.3 (crate (name "l3gd20") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "14gl1jyxa9s025bvb0v3a65s77n3y38py1d76rbbsbsl5g5ky4c6")))

