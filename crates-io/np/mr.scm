(define-module (crates-io np mr) #:use-module (crates-io))

(define-public crate-npmrc-0.1 (crate (name "npmrc") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.186") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "13hf28awh9hj2ph0s5wvnfq03m8fj9vhiv5zqhsiggllqzmg5sxq")))

(define-public crate-npmrc-0.1 (crate (name "npmrc") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.186") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "16qwqk1jmaib1a0s4695v8zji2mqc7nc7r2s5c346blgv3y9l04a")))

(define-public crate-npmrc-0.1 (crate (name "npmrc") (vers "0.1.2") (deps (list (crate-dep (name "clippy") (req "^0.0.186") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "064s46gpqxklcxy8i1izc6vd9k9y4z3r57bmc4m65rdd263841qd")))

(define-public crate-npmrc-0.2 (crate (name "npmrc") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0xfa83nscnnnnpyz3va8lmk02hzzfwkxy9wjsf42292zybbiqcf7")))

(define-public crate-npmrcbrew-0.1 (crate (name "npmrcbrew") (vers "0.1.0") (hash "0dqw6kwdpp7lcblzcciqmkf0qb8jpzh0bmhfxz1a63l74444l8ia")))

