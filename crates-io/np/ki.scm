(define-module (crates-io np ki) #:use-module (crates-io))

(define-public crate-npkill-rs-0.1 (crate (name "npkill-rs") (vers "0.1.0") (deps (list (crate-dep (name "bytesize") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.6") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0kxmvys6s4xf9vzlryf2qhplriqqwz6llv39cs0901mf97h1f2gc")))

