(define-module (crates-io np nc) #:use-module (crates-io))

(define-public crate-npnc-0.1 (crate (name "npnc") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hazard") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "queuecheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1izvq2d3lprf98scn0vmydgs2wlzj35mviglgqp6p31kcxmwcz47") (features (quote (("valgrind"))))))

(define-public crate-npnc-0.1 (crate (name "npnc") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hazard") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "queuecheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0widn43b8dnpsrw0pflj5nx9fagi8rkymjay0vfd7ka0z2rzpn9i") (features (quote (("valgrind"))))))

(define-public crate-npnc-0.2 (crate (name "npnc") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hazard") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "queuecheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "09s7clknf313wsch2r43sipqnhl76kb8by6c8iac5s99b1knqv6x") (features (quote (("valgrind"))))))

(define-public crate-npnc-0.2 (crate (name "npnc") (vers "0.2.1") (deps (list (crate-dep (name "hazard") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "queuecheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1mclrzq97p49d6lc251kd286f00j4vqsmr824l74mwn9529sy9b8") (features (quote (("valgrind"))))))

