(define-module (crates-io np y-) #:use-module (crates-io))

(define-public crate-npy-derive-0.1 (crate (name "npy-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0017fwwwp6pr171a548z83lnl200k68z2vchllcrq94r7x5q9475")))

(define-public crate-npy-derive-0.2 (crate (name "npy-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "042v5d6jqx5jfff592cfhihj2k9sqz1qdlmygfcimcaibd7rphx4")))

(define-public crate-npy-derive-0.2 (crate (name "npy-derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "18zdqip2a4a0vsg5wx6hfnijv4cys0zywq3fg7bqzppn6wwa00jy")))

(define-public crate-npy-derive-0.3 (crate (name "npy-derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0x5kflxq7g0cgfbgk76h5xpqwd8n8sr2jiip3c6xrwiih73an1sx")))

(define-public crate-npy-derive-0.3 (crate (name "npy-derive") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0234fv2gjfqq550asjs8rjnjsd86rik2iq9r6mbbni65lxsyflkl")))

(define-public crate-npy-derive-0.4 (crate (name "npy-derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (default-features #t) (kind 0)))) (hash "14w6m80jn7s184m3nngchpb0as2kmjq4ll2riq8g5bjkd3gi61b9")))

(define-public crate-npy-stream-writer-0.1 (crate (name "npy-stream-writer") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0v3kyav4zpbn6r21k2xwipzvddz1cbgg6zim5dgnlck7fk33r9gj")))

(define-public crate-npy-writer-0.1 (crate (name "npy-writer") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (optional #t) (default-features #t) (kind 0)))) (hash "0n1ki1dk1sgimpn6mqkva34xcnjg9sr6wv9h3s6azpz6s4iyp2x0") (features (quote (("full" "ndarray" "zip")))) (v 2) (features2 (quote (("zip" "dep:zip") ("ndarray" "dep:ndarray"))))))

(define-public crate-npy-writer-0.1 (crate (name "npy-writer") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (optional #t) (default-features #t) (kind 0)))) (hash "1bz58a8xswv19x7a939y4qcypqkhkny38knj8cfx4lnzfi3bqrn4") (features (quote (("full" "ndarray" "zip")))) (v 2) (features2 (quote (("zip" "dep:zip") ("ndarray" "dep:ndarray"))))))

(define-public crate-npy-writer-0.1 (crate (name "npy-writer") (vers "0.1.2") (deps (list (crate-dep (name "ndarray") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (optional #t) (default-features #t) (kind 0)))) (hash "18rhma37ks5vfkpc3wsxzlmg4nds2cm5qa7rz4qh1ywgzbmbayz7") (features (quote (("full" "ndarray" "zip")))) (v 2) (features2 (quote (("zip" "dep:zip") ("ndarray" "dep:ndarray"))))))

