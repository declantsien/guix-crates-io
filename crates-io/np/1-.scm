(define-module (crates-io np #{1-}#) #:use-module (crates-io))

(define-public crate-np1-rust-0.1 (crate (name "np1-rust") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "01qy7xqylilps8yj1k3cxadr2mkl4a0zpzqgxn8bggw7vfa3v5ph") (rust-version "1.75.0")))

