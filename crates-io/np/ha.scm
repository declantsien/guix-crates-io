(define-module (crates-io np ha) #:use-module (crates-io))

(define-public crate-nphard-0.1 (crate (name "nphard") (vers "0.1.0") (hash "0sq1806q0dsz4r1y4z94van7ss6gk9zyggmy4qn2wfz8myfgxgvs") (yanked #t)))

(define-public crate-nphard-0.1 (crate (name "nphard") (vers "0.1.1") (hash "1i509d76mw21cby8v8qgrnv4gk6hz81dv53sax82wpxzmjzhm2dw")))

