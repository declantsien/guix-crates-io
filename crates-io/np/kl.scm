(define-module (crates-io np kl) #:use-module (crates-io))

(define-public crate-npkl-1 (crate (name "npkl") (vers "1.0.0") (deps (list (crate-dep (name "byte-unit") (req "^4.0.17") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0x4makqfd6im0crz5pkg944l5h352fk1pcswiy2dsn7zfrinifw3")))

(define-public crate-npkl-1 (crate (name "npkl") (vers "1.0.1") (deps (list (crate-dep (name "byte-unit") (req "^4.0.19") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0a2haa6plrd08bywnlfz4c91jl268q812mq0b7wfhhirlsw6wcxb")))

(define-public crate-npkl-1 (crate (name "npkl") (vers "1.0.2") (deps (list (crate-dep (name "byte-unit") (req "^5.1.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "00qfick0kb29c31wy1f5a3dcsbw4vkm787jx6iw2pf2whyvmdq9i")))

