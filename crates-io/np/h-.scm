(define-module (crates-io np h-) #:use-module (crates-io))

(define-public crate-nph-ssg-0.1 (crate (name "nph-ssg") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.12") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0p41dzacz3rdv7qnriqfd2awnz9a0clhrgwrbzhn9947h6ppkpcm")))

