(define-module (crates-io np ms) #:use-module (crates-io))

(define-public crate-npms-0.1 (crate (name "npms") (vers "0.1.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "loa") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0bdcqhdw5hw0nnp4s3wq5cxyjcymwiwpprgqc0xcychpvlmrd3g4")))

(define-public crate-npms-0.1 (crate (name "npms") (vers "0.1.1") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "loa") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking" "rustls-tls" "rustls-tls-webpki-roots" "rustls-tls-manual-roots"))) (default-features #t) (kind 0)))) (hash "158qw8rswrf10spv88lj9a3756p4qvw7ni1wqn9a5ngyb0rk0fsk")))

(define-public crate-npms-0.1 (crate (name "npms") (vers "0.1.2") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "loa") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking" "rustls-tls" "rustls-tls-webpki-roots" "rustls-tls-manual-roots"))) (default-features #t) (kind 0)))) (hash "09x3ixszapnhzs36fc04lgvil3xji36dg4yqcnj3k4m5ggipqmv9")))

