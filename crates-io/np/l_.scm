(define-module (crates-io np l_) #:use-module (crates-io))

(define-public crate-npl_utils-0.1 (crate (name "npl_utils") (vers "0.1.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1vg8mybvk9r41jijs9rjwrz64f9fwm9vy182f580d4dyw73zh7vr")))

(define-public crate-npl_utils-0.2 (crate (name "npl_utils") (vers "0.2.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1a0s6d8hyy2szrqpmhfc9dkdmk5493p1ylklp4xs06h7jl249811")))

(define-public crate-npl_utils-0.2 (crate (name "npl_utils") (vers "0.2.1") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "07hf8cwailym83lkpzj7107ajrvf33h6902lx3nf11ns2h70ncg3")))

(define-public crate-npl_utils-0.3 (crate (name "npl_utils") (vers "0.3.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "12bq1h2jvy9rhh9kgb9rz2f64krx3sqixyb9axja4cvkngjgls76")))

(define-public crate-npl_utils-0.3 (crate (name "npl_utils") (vers "0.3.1") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1790496ib3jg06sdxbanvhfim0471aymv5m22ric6gq8w46rhj8c")))

