(define-module (crates-io np p-) #:use-module (crates-io))

(define-public crate-npp-rs-0.0.1 (crate (name "npp-rs") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "cuda-runtime-sys") (req "^0.3.0-alpha.1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "npp-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rustacuda") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustacuda_core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustacuda_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "181dw6w4achh4c0h089yvcpch3nii27j26awhjwkb8w0agbxlq4j")))

(define-public crate-npp-rs-sys-0.1 (crate (name "npp-rs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "102iakks4gvx32h0fgq08s88yhjlhzmf101k0gyda70rck8q82ng")))

(define-public crate-npp-sys-0.0.1 (crate (name "npp-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1wd0n7x5ldr9i9a7fx0b0g8srsc3nrnx0lnkvdqixaghzicppavx")))

