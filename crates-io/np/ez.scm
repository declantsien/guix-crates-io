(define-module (crates-io np ez) #:use-module (crates-io))

(define-public crate-npezza93-tree-sitter-haskell-0.14 (crate (name "npezza93-tree-sitter-haskell") (vers "0.14.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req "^0.20") (default-features #t) (kind 0)))) (hash "1yx7ds0qrvmsdnpl6ysbxpx2ix85p0wr8khq9fy9cbcniblnn14a")))

(define-public crate-npezza93-tree-sitter-haskell-0.15 (crate (name "npezza93-tree-sitter-haskell") (vers "0.15.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req ">=0.22") (default-features #t) (kind 0)))) (hash "1c2l7lgf3skhhdl4815dm8dgpf795dk2qqi83knzpfipkb0dyi6d")))

(define-public crate-npezza93-tree-sitter-nix-0.0.2 (crate (name "npezza93-tree-sitter-nix") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req ">=0.22") (default-features #t) (kind 0)))) (hash "0fyihqjqd3sjgzvqcn3ykzihfna71q10a6qlyb92b7z2i76s1603")))

(define-public crate-npezza93-tree-sitter-ruby-0.20 (crate (name "npezza93-tree-sitter-ruby") (vers "0.20.2") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req ">=0.22") (default-features #t) (kind 0)))) (hash "08d90r9ngnlla46v1f7p8qi7gbb6qlqrsbgd5kl93l8hifba12c9")))

(define-public crate-npezza93-tree-sitter-swift-0.4 (crate (name "npezza93-tree-sitter-swift") (vers "0.4.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req ">=0.22") (default-features #t) (kind 0)))) (hash "1j7j89nnyqklxd0c60x6rfn0gjs69ykczknf57l3q4nrm1k0kp37")))

(define-public crate-npezza93_tree-sitter-tags-0.20 (crate (name "npezza93_tree-sitter-tags") (vers "0.20.2") (deps (list (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0aw6j1xyvp5b1a1p8yyw1jggfy3xv5i90h3l9svq79xblmyjmf6c")))

(define-public crate-npezza93_tree-sitter-tags-0.21 (crate (name "npezza93_tree-sitter-tags") (vers "0.21.0") (deps (list (crate-dep (name "memchr") (req "^2.7.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0vfpl1xkp3by9srr1jlcqldn3r1k17n7zl7an6871vwh4f6ab2hh") (rust-version "1.70")))

(define-public crate-npezza93_tree-sitter-tags-0.21 (crate (name "npezza93_tree-sitter-tags") (vers "0.21.1") (deps (list (crate-dep (name "memchr") (req "^2.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0ir7fnr6ln7h7xy10hp7n3cv5irzsn1h14fqa9zmfs2palbbwl0q")))

