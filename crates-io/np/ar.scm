(define-module (crates-io np ar) #:use-module (crates-io))

(define-public crate-nparse-0.0.2 (crate (name "nparse") (vers "0.0.2") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "05jmgnjx1ygbzzjd0a0wsyagl6xkg5p077wzgqxsqi0igrdfxh2r")))

(define-public crate-nparse-0.0.3 (crate (name "nparse") (vers "0.0.3") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "18nr3f07xqbqdmk8h171hzcrcyhhh8d40k8afcqgzrxl60r88pq0")))

(define-public crate-nparse-0.0.4 (crate (name "nparse") (vers "0.0.4") (deps (list (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1x4zmp6q24yl21hlgw1n8q7aq4n3zwi556izvs313lpz0mk51r0a")))

(define-public crate-nparse-0.0.5 (crate (name "nparse") (vers "0.0.5") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1fxwchgyab5dja954brxfcnq7nbk96b8pc6ripqar43qhn7s7j6q")))

(define-public crate-nparse-0.0.6 (crate (name "nparse") (vers "0.0.6") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "15z0qybmqnrss6c8nv2b33zahcigx7wr5lsblxhl5bicnm9g3309")))

(define-public crate-nparse-0.0.7 (crate (name "nparse") (vers "0.0.7") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1jg5qgzwq78a8klq0w15rncpzwpd8s89c1jjjmwxq964z99m1hma")))

(define-public crate-nparse-0.0.8 (crate (name "nparse") (vers "0.0.8") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1m3ppr5bwwha46qp49l6sxz1c0gxq8i2pnm2jw5qd6pn31svk1bd")))

(define-public crate-nparse-0.0.9 (crate (name "nparse") (vers "0.0.9") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "13hxf7flpdyd8jc110iya10da5rg49053kk28915zbn2cm8csqk6")))

(define-public crate-nparse-0.0.10 (crate (name "nparse") (vers "0.0.10") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "10nw7cjq0i6n43wb7daiv6f36qig57kda4ralmp7cw9qwv34z9w2")))

