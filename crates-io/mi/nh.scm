(define-module (crates-io mi nh) #:use-module (crates-io))

(define-public crate-minhash-rs-0.1 (crate (name "minhash-rs") (vers "0.1.0") (deps (list (crate-dep (name "hyperloglog-rs") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "08y64crp3xylxklgq0pr0idjnyx8z4dyp8iy6982scyfc108i8zn")))

(define-public crate-minhash-rs-0.1 (crate (name "minhash-rs") (vers "0.1.1") (deps (list (crate-dep (name "hyperloglog-rs") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "0yj2m22awnncqhfh45g6g9paf6zi11w3l71j92zy4qjx0k57drrn")))

(define-public crate-minhash-rs-0.2 (crate (name "minhash-rs") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "hyperloglog-rs") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.15") (features (quote ("rayon"))) (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "01bzrcxsvp8h2brjzpqbwchccfzdsz8inz06hsfgqx88in0z7538")))

(define-public crate-minhook-0.0.0 (crate (name "minhook") (vers "0.0.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0akvridxysr2zx83jv0zpjqchy5clnsfzwls6fx1vpfq9w0yk3w4")))

(define-public crate-minhook-0.1 (crate (name "minhook") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mnd2wnic5xhh6jq8ws90afzwyz3bm6rm7d7xlvjb0bddwy7n5rv")))

(define-public crate-minhook-0.2 (crate (name "minhook") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "011ca2kj35nb7r7rbq73qw10i429hfk2g3x2kcywjq8kamvblmcs")))

(define-public crate-minhook-0.3 (crate (name "minhook") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1il7rk0hb19zxs5fd6k7rihzb9182ng4l00gkayy9a15z6mj4im3")))

(define-public crate-minhook-0.4 (crate (name "minhook") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (default-features #t) (kind 0)))) (hash "1j8ywfc9zdgiwf9n9rxszsx9dmxw8nlclkcz98ra4v6n3xgi1qwz")))

(define-public crate-minhook-0.4 (crate (name "minhook") (vers "0.4.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (default-features #t) (kind 0)))) (hash "0gdrhfb6fjpbrjnd8km0h47nmmjqr1l3rwln1ag75pdl1nyfha77")))

(define-public crate-minhook-0.4 (crate (name "minhook") (vers "0.4.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (default-features #t) (kind 0)))) (hash "1cs93ivf4z3dm6s2cpjc6c4lrn36km0d0miw20rgvxngv5yqdnmq")))

(define-public crate-minhook-0.4 (crate (name "minhook") (vers "0.4.3") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (default-features #t) (kind 0)))) (hash "128whqb5a0q35bh9xis0rbhjzmf0bzz5chw5hf5hlidaigrzm1jv")))

(define-public crate-minhook-0.5 (crate (name "minhook") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (default-features #t) (kind 0)))) (hash "0pkb508g280z453m5r4l6f5anq8rrijnabsnmlmn595afrwfi2m4")))

(define-public crate-minhook-sys-0.1 (crate (name "minhook-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "1fd0b1vsrqxv77dmrf8an4jybpnfxqnpian0gw18yd62x17sj5w9")))

(define-public crate-minhook-sys-0.1 (crate (name "minhook-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "067s024nmx41wxy2ih3l1849l8dav50w4vnqrg02xb2hkxv3spnx")))

(define-public crate-minhook_raw-0.1 (crate (name "minhook_raw") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0jifp37xd56n57s5fzj9hiw90wr1rcpkb6i0wyz9vrchl31knhwx") (yanked #t)))

(define-public crate-minhook_raw-0.1 (crate (name "minhook_raw") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0w3d81xahnlfnyv12zzn894swa4n6gisy1wn534wvdih0qbrbfs6") (yanked #t)))

(define-public crate-minhook_raw-0.1 (crate (name "minhook_raw") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "007s63cqyp7z611csq2mvjpc3wxnf4yyf89qgfss8zs9ln3bj5f1") (yanked #t)))

(define-public crate-minhook_raw-0.1 (crate (name "minhook_raw") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1s17cz8qsr4fbqplc4hh3mvji1fnjvdwp6b94vg11ymsn26spjg0") (yanked #t)))

(define-public crate-minhook_raw-0.1 (crate (name "minhook_raw") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0bmxsh1gz2h3mp79vfkl8bcdx61z87aindxgzfgm6hld1y3ajg47")))

(define-public crate-minhook_raw-0.2 (crate (name "minhook_raw") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0smsip27i0b3b0y9ma0yc0rvfln4fziq2ijijxqa4h5vcw6rm587") (yanked #t)))

(define-public crate-minhook_raw-0.2 (crate (name "minhook_raw") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1ximniw7skx7mvv632zcy7qfv8ni6bnsm6yccli1mzpjrp3d14wb")))

(define-public crate-minhook_raw-0.3 (crate (name "minhook_raw") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0.88") (default-features #t) (kind 1)))) (hash "1xyvlihjzajyclcbyiaf74pz60a70sbxjxhrg7pmwssx430x8p03")))

(define-public crate-minhtml-0.13 (crate (name "minhtml") (vers "0.13.1") (deps (list (crate-dep (name "minify-html") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1f7p1qm0ajbafl66r7g8rp8rj31j1sa152d3gg3xvikv588yzk4f")))

(define-public crate-minhtml-0.13 (crate (name "minhtml") (vers "0.13.2") (deps (list (crate-dep (name "minify-html") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dwik6p7pwv819x1am39n2yh8bsgyhl50b3gsr54jc9iv3fyybq5")))

(define-public crate-minhtml-0.13 (crate (name "minhtml") (vers "0.13.3") (deps (list (crate-dep (name "minify-html") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ppz13qwlllfdfsf84c80fib4slq7190m7hxf2sa9lq3813basjz")))

(define-public crate-minhtml-0.14 (crate (name "minhtml") (vers "0.14.0") (deps (list (crate-dep (name "minify-html") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "02yv0l7jj9w7l6fsjn0s0ma5f9mabklaysnj1j7n0md82j6w53m7")))

(define-public crate-minhtml-0.15 (crate (name "minhtml") (vers "0.15.0") (deps (list (crate-dep (name "minify-html") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "17iisyd4gggfqav21x7zalhyjc0dnwgiikaycm4ywzd6yy6imkh6")))

