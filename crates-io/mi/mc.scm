(define-module (crates-io mi mc) #:use-module (crates-io))

(define-public crate-mimc-rs-0.0.1 (crate (name "mimc-rs") (vers "0.0.1") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rustc-hex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.5") (default-features #t) (kind 0)))) (hash "01sr6xs4v1vi3xpz864p9jpvd7nk24pic4534y4w03wbfw92h717")))

(define-public crate-mimc-rs-0.0.2 (crate (name "mimc-rs") (vers "0.0.2") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rustc-hex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.5") (default-features #t) (kind 0)))) (hash "0lnq0f0nzwmy7h6mhm2crh8gwsnam99jf505awx2qk0c0jsxj9hi")))

