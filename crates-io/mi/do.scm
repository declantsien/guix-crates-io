(define-module (crates-io mi do) #:use-module (crates-io))

(define-public crate-midoli-0.1 (crate (name "midoli") (vers "0.1.0") (hash "1jncf8rqv4z64hs0nf526bx8bw1qz366g2rf70vcxymkjxbrb2d2")))

(define-public crate-midori-0.1 (crate (name "midori") (vers "0.1.0") (hash "1kzsribji3bwd8y47ad9qy945n1ah4na76hp73wnvrqvrbrijss8")))

