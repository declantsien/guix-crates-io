(define-module (crates-io mi re) #:use-module (crates-io))

(define-public crate-mireu-0.0.1 (crate (name "mireu") (vers "0.0.1") (deps (list (crate-dep (name "gpio-cdev") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0lcg3ras2z7gl04z9h5fh1rvpkvnw3zidwxayyqdz6mxj44772i3") (features (quote (("rpi_os" "gpio-cdev")))) (yanked #t)))

(define-public crate-mireu-0.0.2 (crate (name "mireu") (vers "0.0.2") (deps (list (crate-dep (name "gpio-cdev") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ccdnixh57r7ljrkvcg11qs8nd6c22p4m020ylxl48mv9vsv9cnb") (features (quote (("rpi_os" "gpio-cdev")))) (yanked #t)))

(define-public crate-mireu-0.0.3 (crate (name "mireu") (vers "0.0.3") (deps (list (crate-dep (name "gpio-cdev") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0885ysqd2sswnxpmfm908jr6xy30k1pawm4nxwlw1q9sk24ifqb4") (features (quote (("rpi_linux" "gpio-cdev")))) (yanked #t)))

