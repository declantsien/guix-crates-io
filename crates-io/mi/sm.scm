(define-module (crates-io mi sm) #:use-module (crates-io))

(define-public crate-mismatch-1 (crate (name "mismatch") (vers "1.0.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gb5sicaja1bqr8c1gj2nzzkas0dvcwyiykffycalzcya12aq0vp")))

