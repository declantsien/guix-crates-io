(define-module (crates-io mi gh) #:use-module (crates-io))

(define-public crate-might-be-minified-0.1 (crate (name "might-be-minified") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "132pssxqf96n367f07z9fynz99zb2yin0pav5vnvfx1wnqcnyvil")))

(define-public crate-might-be-minified-0.1 (crate (name "might-be-minified") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "0impvrb0lm8pnyxlr194kbh5xd068cskmrrb5vs364yw5d5icylc")))

(define-public crate-might-be-minified-0.2 (crate (name "might-be-minified") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0whp2lqf4wslmsc4rzj1sc52lylg1lb605qncbdk7qcz0ws94j8f")))

(define-public crate-might-be-minified-0.2 (crate (name "might-be-minified") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0l07gc8ag5046pj2gmkl0zp0ivrv453pb4r3spqmamrc2y1gffch")))

(define-public crate-might-be-minified-0.3 (crate (name "might-be-minified") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0sdpskdzsmphwb5yhbmxlq8znki4yzaj3mb3ir8pa5h90zqvqpw8")))

(define-public crate-might_sleep-0.1 (crate (name "might_sleep") (vers "0.1.0") (hash "0fxajm1vafg1v1g8s7d2qb3hs0g6qir4abmv7g0jxaha923iiqzk")))

(define-public crate-might_sleep-0.2 (crate (name "might_sleep") (vers "0.2.0") (hash "04nrgqv9x9514f3hm6p3aa1bkbck567j8mil24rljj82g9lbml8y")))

(define-public crate-mightrix-0.1 (crate (name "mightrix") (vers "0.1.0") (hash "15s48j7ilba95hs7aps5rny6hrskx75fyqpy0fqhdnj9b5rf39ag") (yanked #t)))

(define-public crate-mightrix-0.2 (crate (name "mightrix") (vers "0.2.0") (hash "1sr3hlqpk4185zdhr4g9l6yr6cq27nrshdb1cip6rwl8s4vqipn5")))

(define-public crate-mightrix-0.2 (crate (name "mightrix") (vers "0.2.1") (hash "0zxlcnb35plmm7dc6bi462kklfy60z9s1335yj52isxd04gj1aim")))

(define-public crate-mightrix-0.3 (crate (name "mightrix") (vers "0.3.0") (hash "1wlxb4clmgikil15jyqin2l918fxajpfyd5p8pr6j4d53ax35nfa")))

(define-public crate-mightrix-0.3 (crate (name "mightrix") (vers "0.3.1") (hash "16pgk49ckjs17b6li1a4dkgwvhw27k765dlfcpm2yfw96ixjchvc")))

(define-public crate-mightrix-0.3 (crate (name "mightrix") (vers "0.3.2") (hash "1wvqmzy5sdlsmgw94szg7xyg1b4npp7fnzvn9rd57w6v5dxk1j3d")))

(define-public crate-mighty-0.0.1 (crate (name "mighty") (vers "0.0.1") (hash "0hlnd72jradyzpxvp9rc8ql8fr14dnb65j4qhsrfzq3k2skyhxsd")))

(define-public crate-mighty-mancala-0.1 (crate (name "mighty-mancala") (vers "0.1.0") (deps (list (crate-dep (name "cursive") (req "^0.20") (kind 0)) (crate-dep (name "cursive-aligned-view") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0fvx2yw0k386fnhpikc8m69r0s29ag0kgri5l828k2iqw1q59bvw") (features (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-mighty-mancala-0.1 (crate (name "mighty-mancala") (vers "0.1.1") (deps (list (crate-dep (name "cursive") (req "^0.20") (kind 0)) (crate-dep (name "cursive-aligned-view") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1qq5457frr6aakpjx5cijn33d3awn2ml4rnix21pdhl46pjykr24") (features (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-mighty-mancala-0.1 (crate (name "mighty-mancala") (vers "0.1.2") (deps (list (crate-dep (name "cursive") (req "^0.20") (kind 0)) (crate-dep (name "cursive-aligned-view") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xgfp68szqnrhs396jjmrgb9jlkc1i8x8cphjkv4vcxbawh9h5rd") (features (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-mighty-mancala-0.1 (crate (name "mighty-mancala") (vers "0.1.3") (deps (list (crate-dep (name "cursive") (req "^0.20") (kind 0)) (crate-dep (name "cursive-aligned-view") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vkm4irr06h3ypy152pq75hnq5smc4zax5rsmkpmgd44dcg4icdg") (features (quote (("default" "cursive/crossterm-backend"))))))

