(define-module (crates-io mi dy) #:use-module (crates-io))

(define-public crate-midyakis-0.0.0 (crate (name "midyakis") (vers "0.0.0") (hash "018alfdarkn1xqzsgcmxmg5hi0zad8982grrwqwjqq6h1k3yl1v5")))

(define-public crate-midyakis-0.0.1 (crate (name "midyakis") (vers "0.0.1") (hash "1mby2wd7zjjirgbv8jnwyvvb9vfgrqj5zk9dckj55c5r95ikjxhk")))

