(define-module (crates-io mi lf) #:use-module (crates-io))

(define-public crate-milf-0.5 (crate (name "milf") (vers "0.5.8") (deps (list (crate-dep (name "indexmap") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0nn004f2kk2wsmd5flnszf5qp85aymlsjchr21qnrgicbf16h1r6") (features (quote (("preserve_order" "indexmap") ("default"))))))

