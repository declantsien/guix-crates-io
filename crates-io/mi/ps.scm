(define-module (crates-io mi ps) #:use-module (crates-io))

(define-public crate-mips-0.1 (crate (name "mips") (vers "0.1.0") (hash "152k8c38jbbdadk8xjdh6k1m9dipa30c7irybgz3z4vyzz05977p")))

(define-public crate-mips-0.2 (crate (name "mips") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)))) (hash "00n8wvqiq69gs05cg9x3g7kja2cc0r16nbb6kyppgnz4cfd49gsc")))

(define-public crate-mips-0.2 (crate (name "mips") (vers "0.2.1") (deps (list (crate-dep (name "bit_field") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yrjdj60v5qxkfallwglchr3i6ax05bvb4mzzvz8x0kvknsi8fh5")))

(define-public crate-mips-mcu-0.1 (crate (name "mips-mcu") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0r6dl23pqas44lb3721g9cgpvdspkyns1905sn0ahzrgadcq591q")))

(define-public crate-mips-mcu-0.2 (crate (name "mips-mcu") (vers "0.2.0") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ig9h7fbhj0jnhns90r971306j51jsv4g3vyqvg30idb375xzf8c")))

(define-public crate-mips-mcu-0.3 (crate (name "mips-mcu") (vers "0.3.0") (deps (list (crate-dep (name "critical-section") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pgm9yifajyrzfpf918szdil5n7picy64vc105k2m57ygp79ffph") (features (quote (("critical-section-single-core" "critical-section/restore-state-u32"))))))

(define-public crate-mips-mcu-0.3 (crate (name "mips-mcu") (vers "0.3.1") (deps (list (crate-dep (name "critical-section") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1d3v0q2xd0nk5al8diyf0jygqa7i74hgwbw9vd69gyv3lfh17q0j") (features (quote (("critical-section-single-core" "critical-section/restore-state-u32"))))))

(define-public crate-mips-mcu-alloc-0.5 (crate (name "mips-mcu-alloc") (vers "0.5.0") (deps (list (crate-dep (name "linked_list_allocator") (req "^0.10.3") (features (quote ("const_mut_refs"))) (kind 0)) (crate-dep (name "mips-mcu") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mips-rt") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1fc1qnmrzl8jzasapnnwqmp0d6xyl32cid67rw5djz6mhpcd7vmi")))

(define-public crate-mips-mcu-alloc-0.6 (crate (name "mips-mcu-alloc") (vers "0.6.0") (deps (list (crate-dep (name "critical-section") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "linked_list_allocator") (req "^0.10.3") (features (quote ("const_mut_refs"))) (kind 0)) (crate-dep (name "mips-mcu") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mips-rt") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1v88xs0rc89qafhij3lyiwyfz9mx25sgfbhk5a3f9slcn9vlzdkh")))

(define-public crate-mips-mcu-alloc-0.6 (crate (name "mips-mcu-alloc") (vers "0.6.1") (deps (list (crate-dep (name "critical-section") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "linked_list_allocator") (req "^0.10.3") (features (quote ("const_mut_refs"))) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mips-mcu") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mips-rt") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1gqbzxa2m3v3q5w4r3wb4w0fgkljcjn2j4yp1sidxpldwazyfzwd")))

(define-public crate-mips-mcu-alloc-0.6 (crate (name "mips-mcu-alloc") (vers "0.6.2") (deps (list (crate-dep (name "critical-section") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "linked_list_allocator") (req "^0.10.3") (features (quote ("const_mut_refs"))) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mips-mcu") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mips-rt") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0fyz9xpbws9gjxlqsb6bniplim1rvxw2xwnwkrilcnlsj2hi6m9r")))

(define-public crate-mips-rt-0.2 (crate (name "mips-rt") (vers "0.2.1") (deps (list (crate-dep (name "bare-metal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "mips-rt-macros") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "r0") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "19xb9xxw1jwxwdgly5ppx95bd442yay26044ak660ry4m9prpvq4")))

(define-public crate-mips-rt-0.3 (crate (name "mips-rt") (vers "0.3.0") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mips-rt-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n0qgm0ppvvmb4ypk3pf5g72a2ad4q1d89prln7vms5hxzspjyg3")))

(define-public crate-mips-rt-0.3 (crate (name "mips-rt") (vers "0.3.1") (deps (list (crate-dep (name "mips-rt-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0j9swdccqwmym3hdn2id9xz49c6c90ncjb8131ypw3j4hfn28m9x")))

(define-public crate-mips-rt-0.3 (crate (name "mips-rt") (vers "0.3.2") (deps (list (crate-dep (name "mips-rt-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0cpkn00npfw5ikgy2mj03vcrghmgvy9ddgi9q3k4r2b620n45m82")))

(define-public crate-mips-rt-0.3 (crate (name "mips-rt") (vers "0.3.4") (deps (list (crate-dep (name "mips-rt-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0q87p0v719xw3llihgiyx3h9bg64qa86lnwbl0q9rny3z9aph3nm")))

(define-public crate-mips-rt-0.3 (crate (name "mips-rt") (vers "0.3.5") (deps (list (crate-dep (name "mips-rt-macros") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "01ss2px3v1v330cs090cnz8dbxdfhphlah8116v9j3pj4km736b7")))

(define-public crate-mips-rt-0.3 (crate (name "mips-rt") (vers "0.3.6") (deps (list (crate-dep (name "mips-rt-macros") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1355hs94l088avz03r3msz3q7hlzbd52xsz0kyyc71c3v27cq78s")))

(define-public crate-mips-rt-macros-0.2 (crate (name "mips-rt-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1l999qrawnsg3ssss5g45pnqb0xmlwwfgijjfmsm2322qvxj7hp2")))

(define-public crate-mips-rt-macros-0.3 (crate (name "mips-rt-macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "10z0nviz2bavqy3mmzcygcva7hr6vrnrlbzhxv0bn2jbnc5npzs2")))

(define-public crate-mips-rt-macros-0.3 (crate (name "mips-rt-macros") (vers "0.3.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1vlrbchrlzlfv8gcqzxiv1fmdv903km6bcpg6yhi2v08wzd7yby9")))

(define-public crate-mips32-0.1 (crate (name "mips32") (vers "0.1.0") (hash "19ybbxqxhdd8qxnwbsan1w0xlwkz1qrcfvb383zgfv6ncw7ffn2j")))

(define-public crate-mipsasm-1 (crate (name "mipsasm") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1zrajwj215l3yjqcjnsi17b0b6d627xgfi5rbmp9nin2nfwf6sci")))

(define-public crate-mipsasm-2 (crate (name "mipsasm") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "02zpv541qvxmja93gxp32iqqnjn6hh2sfv59agrd5g4acmw3gi1n")))

(define-public crate-mipsasm-2 (crate (name "mipsasm") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "06dp2lj4hrfqqw0q5x34aaqj9fqn6wlwmzzdqz1zkvy01vdaq1wf")))

(define-public crate-mipsasm-rsp-1 (crate (name "mipsasm-rsp") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (kind 0)))) (hash "1hw50gjdl3xlb7vqkbqli5nwg4b6a2cfm1wqbdpq71abkp63cfvb")))

(define-public crate-mipsasm-rsp-1 (crate (name "mipsasm-rsp") (vers "1.0.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (kind 0)))) (hash "14a4lp4slvmn4migx5wdy3dkq27j086k9krr6sacfmyxr0s167r9")))

(define-public crate-mipsasm-rsp-1 (crate (name "mipsasm-rsp") (vers "1.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (kind 0)))) (hash "1x6z4khz96ja324pp749kszs2pkv743q97hvr7qczc8ymy1c8z28")))

(define-public crate-mipsasm-rsp-1 (crate (name "mipsasm-rsp") (vers "1.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (kind 0)))) (hash "1rxk2qhvgymw695ggppa6kd01z940ikqi6cix0pafyra71wzsplj")))

(define-public crate-mipsasm-rsp-1 (crate (name "mipsasm-rsp") (vers "1.1.3") (deps (list (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (kind 0)))) (hash "1qs22404c999lbi18ka08k0hrbglsmhhbfa5xybw0pdny3qrjq4w")))

(define-public crate-mipsasm-rsp-1 (crate (name "mipsasm-rsp") (vers "1.2.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (kind 0)))) (hash "1mrdh5pilmjc5yah23fj266cn7nzwjz4hvivm12m8429gj2j4k0c")))

