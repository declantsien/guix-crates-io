(define-module (crates-io mi rc) #:use-module (crates-io))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.0") (hash "17w5w8hxcakig548zkx1avzv5zm7034kgmy93qfjsq847qqisqcq")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.1") (hash "0a1jq9ig6biqfj7kpz89igc20zcgi378vhyysj3drs69rj516yba")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.2") (hash "1kdzapd4qpy3gjbl8brjyrc274lwrjrvv98sp6mcql2ybwlfhmqk")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.3") (hash "06fc1mi0izq7nm9zb2qmnkv90v53s3arpwvrsbm0s8v6c5mjxdk9")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.4") (hash "0r2yxlz6c9hzh10jv4x9zanb05crihxxi6k95z65g4jj69zrica0")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.5") (hash "05y2117n6i012p9vnayffs9z3nlrx27a3ymmih4rwz1yx3x1rjrr")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.6") (hash "0y2agq5vizp5ln7d2zy7psysin64r3g86ri3hc4r0kfkhzxkl0pn") (yanked #t)))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.7") (hash "02gdxnvr7w8gp8ahkcm4h5pa08shm284vnlb6pw0g9pbl4hz46w5")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.8") (hash "0xzcdysmyn25a7k698p79qr4qgfi2p5y76a86lsvpp1glw4hi8wh")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.9") (hash "0hy4k8inzxczyzlhd60pydp3ki0iqx87f3zri1zjycjl2bn0fk1i")))

(define-public crate-mirc-0.1 (crate (name "mirc") (vers "0.1.10") (hash "0wccd0hxghhdwh2x97h7glh47d6zys2rbabp7r8y2j3adi2vgfxl")))

