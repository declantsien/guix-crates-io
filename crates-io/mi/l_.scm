(define-module (crates-io mi l_) #:use-module (crates-io))

(define-public crate-mil_std_1553b-0.2 (crate (name "mil_std_1553b") (vers "0.2.0") (hash "02w94cdskmdds2vli6ggkmbls7mcdz8nrhi12dldlzrbmqhpxrbd")))

(define-public crate-mil_std_1553b-0.3 (crate (name "mil_std_1553b") (vers "0.3.0") (hash "1qrjffm3vrs6lp2wbzd5c7w4i380xi16c6ap147xjxg07qjjszb9")))

(define-public crate-mil_std_1553b-0.3 (crate (name "mil_std_1553b") (vers "0.3.1") (hash "06z5r3xxj65wx1wzlwkx65zjxfs6ajnp1796sn5ky7qza9glbkfr")))

(define-public crate-mil_std_1553b-0.4 (crate (name "mil_std_1553b") (vers "0.4.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "01rdb3mmzm7c64wj9kiq3inggyyxkhv4lkak87dfn69y8acb9jka")))

(define-public crate-mil_std_1553b-0.5 (crate (name "mil_std_1553b") (vers "0.5.0") (deps (list (crate-dep (name "mil_std_1553b_derive") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gdach764rxv014pb1vzxhjaynzrndms21yjlvhxg8wfcds323lp") (v 2) (features2 (quote (("derive" "dep:mil_std_1553b_derive"))))))

(define-public crate-mil_std_1553b_derive-0.5 (crate (name "mil_std_1553b_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1npq7nm02f2bhj6z0m2zgxwv8wmdx4vdngd7pcnmiqgm9j9jqk51")))

