(define-module (crates-io mi ny) #:use-module (crates-io))

(define-public crate-miny-1 (crate (name "miny") (vers "1.0.0") (hash "1i374ydxzaxnc915hlx0jh44qns782f8px1dbcjjnvbd7hm9ffid")))

(define-public crate-miny-2 (crate (name "miny") (vers "2.0.0") (hash "1yzmldxsa1428g8zry0bm672pv2r7223qs8wy2vijrsk2fa8lh9b")))

(define-public crate-miny-2 (crate (name "miny") (vers "2.0.1") (hash "0zs50fdy935x04f3d5fmlwl7cw95y0w4786m7m5b5mz3465m3z4x")))

