(define-module (crates-io mi co) #:use-module (crates-io))

(define-public crate-mico-0.1 (crate (name "mico") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0kh81yna10k9zvixvdkr26inqv1f2803ls7mrkl039vxpnvrrz3a")))

(define-public crate-mico-0.1 (crate (name "mico") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0yp1aaaalkqkfi1kwkk06i2mfkgr7qb14kn8cgk4v3bd44kbzalp")))

(define-public crate-mico-0.1 (crate (name "mico") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0ckfv5ajm8kjsh2646hq6vjwvbd5s56b77745f5iw80b61bydzfq")))

(define-public crate-mico-0.1 (crate (name "mico") (vers "0.1.3") (hash "175hpfggwsc77izsgfrkyfahdqq8vpjsik23lvd4hdp1sa9wdrp1")))

(define-public crate-mico-0.1 (crate (name "mico") (vers "0.1.4") (hash "05jr1qcfa0wyjsw16agfgl1j9j2cr6d4fz143iydq32hcybffgdl")))

(define-public crate-micon-0.1 (crate (name "micon") (vers "0.1.0") (deps (list (crate-dep (name "divider") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "tessor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tessor_viewer") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "zeno") (req "^0.3") (default-features #t) (kind 0)))) (hash "17md844fkyry4vn1yfwkxnkckykal0yqy690vbs9y6fl1npqw2xx")))

(define-public crate-micon-0.2 (crate (name "micon") (vers "0.2.0") (deps (list (crate-dep (name "divider") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "tessor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tessor_viewer") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "zeno") (req "^0.3") (default-features #t) (kind 0)))) (hash "1g0dccfap2fvqgbm65k552pz0xbkz6ldysfwjndy0s4i1wfjldzw")))

(define-public crate-micon-0.3 (crate (name "micon") (vers "0.3.0") (deps (list (crate-dep (name "divider") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "tessor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tessor_viewer") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "zeno") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gldsxymyrc08fgkq42ibh8p645kqgyj5vz7lqva2dpcwqkcjqsp")))

(define-public crate-micon-0.4 (crate (name "micon") (vers "0.4.0") (deps (list (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "tessor") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tessor_viewer") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "zeno") (req "^0.3") (default-features #t) (kind 0)))) (hash "0n2zvxczm27ss7mvnp4zmikvfdp9al22ch97np3hfl7yj0jzxmss")))

(define-public crate-micos-0.0.0 (crate (name "micos") (vers "0.0.0") (hash "0fj4kgaa7hxpp93g9fmxf5glan9b6x3j4311c4h811m1cfiknzq6") (yanked #t)))

(define-public crate-micos-0.0.1 (crate (name "micos") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.0.16") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l4wwv58q5f095b2nrwf1q6wvczhll6xvbxyvmngfdsbf8vh3n3b") (yanked #t)))

