(define-module (crates-io mi pm) #:use-module (crates-io))

(define-public crate-mipmap-1d-0.1 (crate (name "mipmap-1d") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "130mv6rhxavss2bgg928qinasi87vq2fmk5gnhw65qb1sc6zzp54")))

