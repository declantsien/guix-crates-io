(define-module (crates-io mi n-) #:use-module (crates-io))

(define-public crate-min-cancel-token-0.1 (crate (name "min-cancel-token") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "0n9x2kv2p4c7f5nka9851r844vd21d771jwm9asppx9zjradfbqi")))

(define-public crate-min-cl-0.1 (crate (name "min-cl") (vers "0.1.0") (hash "0l0699axg1xz9cjxbar84ssv71d5glci5hlxaik4g7akb9k09vxl")))

(define-public crate-min-cl-0.1 (crate (name "min-cl") (vers "0.1.1") (hash "13pgisvx5jy7qjxpx0awx0p0qxqs6n934k7b6nr94yp7smr4lwbb")))

(define-public crate-min-cl-0.1 (crate (name "min-cl") (vers "0.1.2") (hash "0rh45s3rd7lld6j46jg1yi91kc9a05qbi908zvwbqxl22ny506w3")))

(define-public crate-min-cl-0.1 (crate (name "min-cl") (vers "0.1.3") (hash "1hwq2xb3y23mpb77wj8ym1fk4gcha1w0d9gqadfh4a052amz9in4")))

(define-public crate-min-cl-0.2 (crate (name "min-cl") (vers "0.2.0") (hash "0cr5bpph7ag1a59nkp0zl6xw8pmnf2yxgdf2fcqvgfmrhzk5wfb5")))

(define-public crate-min-cl-0.3 (crate (name "min-cl") (vers "0.3.0") (hash "00m89az1ncxi0vc7f6icy508adjqcs3f9hcj5gwr8wzabwcpp1i7")))

(define-public crate-min-id-0.1 (crate (name "min-id") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "17k892rnrl7msjiz5bzr1i3rbhl8x4ami861kvwrkzkxsz8qyw0h")))

(define-public crate-min-max-0.1 (crate (name "min-max") (vers "0.1.0") (deps (list (crate-dep (name "partial-min-max") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0a07bvc6parwh2iqv5xq61l784mhgwakaq9wjsgd01mb9dlrjgfw")))

(define-public crate-min-max-0.1 (crate (name "min-max") (vers "0.1.1") (deps (list (crate-dep (name "partial-min-max") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1d01ln9ircgmpms4bxj27acyhzg93nj5dav4rmar15lxlcgi7h42")))

(define-public crate-min-max-0.1 (crate (name "min-max") (vers "0.1.2") (deps (list (crate-dep (name "partial-min-max") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "13nabs398ydm0banh8lxgpdalcfclq9534va41l3047pci9065k0")))

(define-public crate-min-max-0.1 (crate (name "min-max") (vers "0.1.3") (deps (list (crate-dep (name "partial-min-max") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ahcm0z0qlbq6q24yh8didca0f3zfibyg71bqs8hphnhrkifnm11")))

(define-public crate-min-max-0.1 (crate (name "min-max") (vers "0.1.8") (deps (list (crate-dep (name "partial-min-max") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0pw86hgbrnm53yi12p4qs60z4b8vpflvcq0g5rqpwkz349k52xgy")))

(define-public crate-min-max-heap-0.1 (crate (name "min-max-heap") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "16a85qvz9qxh2vy7izy696fhlf8z5sk5wqrxr5lsz9w6d6rzxz1m")))

(define-public crate-min-max-heap-0.1 (crate (name "min-max-heap") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0jskyj7pi7xcch3l8yvk2gdjgi4hzgmi1lrrs814disyyvvv6m71")))

(define-public crate-min-max-heap-0.1 (crate (name "min-max-heap") (vers "0.1.2") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "14libwbc7fl6l9lyfj31whxhqxcrjbq880z4v42zgl8mvlcmvjcz")))

(define-public crate-min-max-heap-0.2 (crate (name "min-max-heap") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bzzpqf09bswyy0pd8ic4pmb9pr0srszaa9ngi3qy7hp7i7s08g2")))

(define-public crate-min-max-heap-0.2 (crate (name "min-max-heap") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "19pda89a49jl8rgm83walyg7l3pwpvhrww611x8b5smydzqln8l2")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.0.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1wfs3kihny2c0vwb8j44smlq3z02lgs56jxncjgl8fy1zxzb7y0x")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mil6w57474ldsphjqm7b0w0ybb27x331g1j2r4727agylj18jhc")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0kfn4l7jz9wbfgzmh5h1wr6lslmqbcn3c95d9xc6s9dwxh4wvpq2")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wzpkfsvcd4v41675qq7sh6q3hhdcca7i7d9a28l8zz14ic9pxv0")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.0.4") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0c91snb8nl4znbjg7di0lvfpq91rmkyhgg3wv6mcrjba82v7pk57")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "118n9yprr25jmvbq51rkzd8r3lcgwjnj4v6kp5m5sy9wr8dsnn17")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1k3pkyjj8br072n15mvaqrj3p0h2ph18zh5z5hvqil5q6r9561ji")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1zpprggyvbfkia5c5dqbk9x3k3ib6k18fm8nlwb4zzqd5izyywlf")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.2.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "05n790jr9bsy7568cz6naq77wknz162al55rz8b9arw7knsawk3b")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rgdjsw7j6n99bfw4sap5zgzpn5ps05x39yrbp32fc7das7cki7l")))

(define-public crate-min-max-heap-1 (crate (name "min-max-heap") (vers "1.3.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "060bwpg7zw65yjfvyfklqwyd0hgk5rgx37yghj98xx00kk7yd1r6")))

(define-public crate-min-sqlite3-sys-0.1 (crate (name "min-sqlite3-sys") (vers "0.1.0") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.1") (default-features #t) (kind 0)))) (hash "0bg4j1kxsvll97p1fn1appxpk0k6zpy2xhrpkr077akp1j68njj1") (yanked #t)))

(define-public crate-min-sqlite3-sys-0.1 (crate (name "min-sqlite3-sys") (vers "0.1.1") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.1") (default-features #t) (kind 0)))) (hash "0v97p3613jd516y19n399ca98i0dikn74mxf3h2fq1n0gz0klk0w") (yanked #t)))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.0.0") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.2") (default-features #t) (kind 0)))) (hash "1bw6y110w116qw50kkl5gijhs6ljrdnvq9v600289xnqlfbyaig4") (yanked #t)))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.0.1") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.3") (default-features #t) (kind 0)))) (hash "0188pq6sy1423qj0f48qvrlmlmhzb0shwmg4qz0hm7sa9hzmln2w") (yanked #t)))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.1.0") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.4") (default-features #t) (kind 0)))) (hash "0pp1mhla04hsr735z9hr8caym4ndrd52xh5x1wpv154xmgfyvldq") (yanked #t)))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.1.1") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.5") (default-features #t) (kind 0)))) (hash "10mmfsdzl4asm7qfj2nv5zk4hjv5md35yv41zv4kmk7ysfncffmr")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.2.0") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.6") (default-features #t) (kind 0)))) (hash "0k5fj0rxiy6c98gmwpgnq8fkvigjnp1msgqaq86vq7hjanzsr7c4")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.3.0") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.6") (default-features #t) (kind 0)))) (hash "1xp7qnfih9s3sgqp7mrd2awcarwzzvc45jnp3nj3c7l75w1785f0")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.3.1") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.6") (default-features #t) (kind 0)))) (hash "0mzvxbsjmkz544mq3r8kgqk0daprv8hkbfy8cjr3qykl9qs9p79c")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.3.2") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.38.6") (default-features #t) (kind 0)))) (hash "1jjrw3hvc3kgbnv23fv5hzxzfyfgqpcyzdjiavkyclc0avgri7v1")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.4.0") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.39.4") (default-features #t) (kind 0)))) (hash "0mp9ykpsig3vjkv9dnv3q9cqln0liry9bli23dkpcg0aqagc10bz")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.4.1") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.39.4") (default-features #t) (kind 0)))) (hash "1mbprzmclhkf8f42c1ixzyvy6zqci808l3h858ad8hfyvlghqi5r")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.4.2") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.39.4") (default-features #t) (kind 0)))) (hash "0iby1bypjr88in8skja5rhk8gss218gix6880iyz878cmpr4a2a3")))

(define-public crate-min-sqlite3-sys-1 (crate (name "min-sqlite3-sys") (vers "1.4.3") (deps (list (crate-dep (name "sqlite3-builder") (req "^3.39.4") (default-features #t) (kind 0)))) (hash "1q1lvnb12j42i4vkn049jkr8nin4jrbncqcfcyhqfk6q25lycsky")))

(define-public crate-min-tun-0.1 (crate (name "min-tun") (vers "0.1.0") (hash "040mprw7ndnhqx5z710znga4qk74a887g9alngl1kw1qi56afn6z")))

(define-public crate-min-tun-0.1 (crate (name "min-tun") (vers "0.1.1") (hash "0smjny1y84c8pi32p9z8g7d1qbqb4b1bg1ydn848iw8ynbh88lpb")))

(define-public crate-min-tun-0.1 (crate (name "min-tun") (vers "0.1.2") (hash "1qrv39acsf4h81704dhz5ipxdxn2vp2ggyhp9h5rzmxl2mpnjf7f")))

