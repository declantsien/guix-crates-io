(define-module (crates-io mi gp) #:use-module (crates-io))

(define-public crate-migparser-0.1 (crate (name "migparser") (vers "0.1.0") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "0f55l68n3zd4jpc9pxmizd72jpv8869w0hdw71rj4224l4lcmgq3") (yanked #t)))

(define-public crate-migparser-0.1 (crate (name "migparser") (vers "0.1.1") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "0gyk0f39pv8lyh5k6lvsh6iwdiwz5w3hhp0pbdbff4a4njk7nh2c") (yanked #t)))

(define-public crate-migparser-0.1 (crate (name "migparser") (vers "0.1.2") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "136asxqiw7hf0c19ahfkzy5zib2dxdh1lp6fhwcf6p1029rn7g2b") (yanked #t)))

(define-public crate-migparser-0.1 (crate (name "migparser") (vers "0.1.3") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "050sxm9qj1mrxnpg7q0q55wr3mv443dsvi540j1mnnnp78ymp48x") (yanked #t)))

(define-public crate-migparser-0.1 (crate (name "migparser") (vers "0.1.4") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "18ss326izmfj1w4llbq8gfnacm3xrdngamq8rc1ihmp7qxv5j34d") (yanked #t)))

(define-public crate-migparser-0.1 (crate (name "migparser") (vers "0.1.5") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "0x2sbgy8jlgc3jfxw78qfrmi4lpysj9rspzf3a8a1c8fp6rnh0f3") (yanked #t)))

(define-public crate-migparser-0.1 (crate (name "migparser") (vers "0.1.6") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "1zw32g7dnk2myayl9k5mcfn7ll44pp8gsl4mjglv2gi5yinmxl33")))

(define-public crate-migparser-0.2 (crate (name "migparser") (vers "0.2.0") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "150v4k1ckinzw0l3iw4h8w2f4ggaxrjclm66h7zbzijzdrv95am6")))

(define-public crate-migparser-0.2 (crate (name "migparser") (vers "0.2.1") (deps (list (crate-dep (name "migformatting") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "19n482v0216y5laxwwv7sbxzs2jr9fhxhzngb6qfqbjbr94mf92c")))

