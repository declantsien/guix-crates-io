(define-module (crates-io mi ru) #:use-module (crates-io))

(define-public crate-miru-0.0.0 (crate (name "miru") (vers "0.0.0") (hash "0rzbwpvrivybwxrap7ix29j6a93v7hi6l49412pzldxy0vkb8kmr")))

(define-public crate-miru-gl-0.1 (crate (name "miru-gl") (vers "0.1.0") (deps (list (crate-dep (name "gl_generator") (req "^0.14") (kind 1)))) (hash "04yizjw7py23wyyvvlngnwly2yf9pfpfwgrpjzrllab53msrg3si")))

(define-public crate-miru-gl-0.1 (crate (name "miru-gl") (vers "0.1.1") (deps (list (crate-dep (name "gl_generator") (req "^0.14") (kind 1)))) (hash "1ap95lszw0prmng6qvdj5c224ashzxh8jh0n43nv5s646z7kgb0s")))

