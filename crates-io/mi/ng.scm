(define-module (crates-io mi ng) #:use-module (crates-io))

(define-public crate-ming-0.1 (crate (name "ming") (vers "0.1.0") (hash "13kwh2927h7q6j09995py0mfxy7x9x0xljhv3k92nk5phdql3chb")))

(define-public crate-ming-tools-0.1 (crate (name "ming-tools") (vers "0.1.0") (hash "1m5f3xw3fxwhyzqqv5s3rhmpfzhz025bkl9gmxbssqwcad8lc374") (yanked #t)))

(define-public crate-ming-tools-0.1 (crate (name "ming-tools") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.14") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yv8wb5ma6235bj5sqp3wph1ivnjfn0ai4gdr76xzdy0n016qhcm")))

(define-public crate-ming_test_crate-0.1 (crate (name "ming_test_crate") (vers "0.1.0") (hash "1g49v1gxp7xz1fs0pbkg2166m1981nkni8s2rdhnq1fb07n9fzmq")))

(define-public crate-ming_test_crate-0.1 (crate (name "ming_test_crate") (vers "0.1.1") (hash "0j91hyi0qvxp8wl5f39crwwwfgmwv4shpxpk6wzsyb19mry0i2xv")))

(define-public crate-ming_test_crate-0.0.1 (crate (name "ming_test_crate") (vers "0.0.1") (hash "1a4jmg8224dyvrjp8v2lpszqgjnaqsxqychhlg4p8b6b8i4ri4qz")))

(define-public crate-ming_test_crate-0.1 (crate (name "ming_test_crate") (vers "0.1.2") (hash "093m4qs2yfiqvxl9nb4rnb8s008r6jq62wgnsxbpwhrdndavlaln")))

(define-public crate-mingrep-0.1 (crate (name "mingrep") (vers "0.1.0") (hash "13nwkfvfqi0lhab920rmwxdldblx5xidz9p3pbyqmvyphwsqa4ap")))

(define-public crate-mingrep_arpitjp-0.1 (crate (name "mingrep_arpitjp") (vers "0.1.0") (hash "1p4ndjibpsxb1prw9rd9j71f04wz8wki52nb4804ic3ngc621jzj")))

(define-public crate-mingrep_cx-0.1 (crate (name "mingrep_cx") (vers "0.1.0") (hash "065jgw3h8zjqlqw92zgv3nn37klx26did5aqr1xg6zz4szy6ql9b")))

