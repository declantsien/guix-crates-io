(define-module (crates-io mi nf) #:use-module (crates-io))

(define-public crate-minfac-0.0.1 (crate (name "minfac") (vers "0.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)))) (hash "03x9i41px50qzzxdg3ppwgdn3sxv9lb4823i9m1ymw4n5mmf3i04") (features (quote (("std") ("default" "std"))))))

