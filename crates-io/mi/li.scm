(define-module (crates-io mi li) #:use-module (crates-io))

(define-public crate-milim-0.1 (crate (name "milim") (vers "0.1.0") (hash "1rm0ciljs6fvf857rsawarqc396b36p99bc3sbd28gzrwq6z9mry")))

(define-public crate-milim_vulkan-0.1 (crate (name "milim_vulkan") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 0)))) (hash "1xi14g0hdfj8cfwkridazjy6apipfw990nh494aa6gwjvc4nb3m5")))

(define-public crate-milim_vulkan-0.1 (crate (name "milim_vulkan") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 0)))) (hash "1rcs89dq6rwzvkpbrlpjkjlghlvy8vcy6fc5ah1xc417ynhxlms8")))

