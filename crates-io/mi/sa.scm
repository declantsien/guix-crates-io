(define-module (crates-io mi sa) #:use-module (crates-io))

(define-public crate-misaka-0.1 (crate (name "misaka") (vers "0.1.0") (hash "01jaylxzhgs8bnzllxw5zfyvy59kjrnh5isrl6zd1pg9ab0j5dqa")))

(define-public crate-misato-0.1 (crate (name "misato") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0xvxbqf54a123gn85bv7ngr8g0az85f0vb1l79lm81z7h7ryr41f")))

