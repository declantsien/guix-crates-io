(define-module (crates-io mi mo) #:use-module (crates-io))

(define-public crate-mimo-0.1 (crate (name "mimo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.12") (default-features #t) (kind 0)))) (hash "1w9pmcn72c18l47hrnrqf710i6fihh8kx9920762bqh6xjny68w3")))

