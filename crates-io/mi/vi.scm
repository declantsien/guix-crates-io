(define-module (crates-io mi vi) #:use-module (crates-io))

(define-public crate-mividtim_minigrep-0.1 (crate (name "mividtim_minigrep") (vers "0.1.0") (hash "1xa2wx50wk8bvi9vbz0q8m4qixgkcpci31v4jx2xziqc1g7kjgjv")))

(define-public crate-mivim-0.1 (crate (name "mivim") (vers "0.1.0") (hash "09n8p20hrsw8053945a09zsqpj59rj2d3b871rayb64ll9dxz3w6")))

(define-public crate-mivim-0.2 (crate (name "mivim") (vers "0.2.0") (hash "1sfhc03k6cisii7y353v7f9q6iqnifkns94fqpgmfcb2ajgj8q3p")))

(define-public crate-mivim-0.3 (crate (name "mivim") (vers "0.3.0") (hash "1bhkn4q4mvzxvaab86b6by2fak3gvga3ijdzacp04n5pg72la2ma")))

