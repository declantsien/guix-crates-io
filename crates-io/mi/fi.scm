(define-module (crates-io mi fi) #:use-module (crates-io))

(define-public crate-mifi-rs-0.3 (crate (name "mifi-rs") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "0503hqg46ha5w67bd75inzsigq28iqhn58r4bm3zq31rhq8l282i")))

