(define-module (crates-io mi sf) #:use-module (crates-io))

(define-public crate-misfortunate-0.1 (crate (name "misfortunate") (vers "0.1.0") (hash "1h1sz3z511yjab8nisv3q9bb5ng446vbqr815smf8cmyv1vnvn8s")))

(define-public crate-misfortunate-0.2 (crate (name "misfortunate") (vers "0.2.0") (hash "1kyzh2mxv2g1fv8xjlmzjrvdjjwdrba3nvlgmxmwqy59z87hjva3")))

(define-public crate-misfortunate-1 (crate (name "misfortunate") (vers "1.0.0") (hash "17ql332a8srqxmlfk67xb05z8vlljpli7d92yjplqj2lvxcfaf3h")))

