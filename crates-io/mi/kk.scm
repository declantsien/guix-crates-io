(define-module (crates-io mi kk) #:use-module (crates-io))

(define-public crate-mikktspace-0.1 (crate (name "mikktspace") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1mxddd86d7kj05amv2kpv4dwqh4dzsm1az56hf22lbn21fa1zy13")))

(define-public crate-mikktspace-0.1 (crate (name "mikktspace") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cgmath") (req "^0.15") (default-features #t) (kind 2)))) (hash "1aim03rs5rq0xqsz4p2ji85sq6hmhlw1hqm47x27p3rz3si1jnsw")))

(define-public crate-mikktspace-0.2 (crate (name "mikktspace") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1mp1lriqkzfhfsc549pfbrbjcmx0gjjm6y5ilhbrwdjw3dgmllp7")))

(define-public crate-mikktspace-0.3 (crate (name "mikktspace") (vers "0.3.0") (deps (list (crate-dep (name "glam") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.26.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.26") (default-features #t) (kind 2)))) (hash "1c7qcan08qxgqifrba5f3f2d9456q0nda9m65jcqy6l70fs5c2vx") (features (quote (("default" "nalgebra"))))))

(define-public crate-mikktspace-sys-0.1 (crate (name "mikktspace-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1pdi019g17yx9s7wqqb17kr2grr1r918m7c99mdvnyxkx8iary4v")))

(define-public crate-mikktspace-sys-0.1 (crate (name "mikktspace-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0p13hvvrpzv7jvh3n0sm8gan8k75k3kgh2xwbi46h754ylif9s2g")))

