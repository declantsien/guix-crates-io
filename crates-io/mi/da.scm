(define-module (crates-io mi da) #:use-module (crates-io))

(define-public crate-midas-0.4 (crate (name "midas") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fallible-iterator") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.16.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "1fkqihnypc62kqayq7cfxsq1wl4v4g7qwj59wxjg3m9pvnjsjicj")))

(define-public crate-midas-0.4 (crate (name "midas") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fallible-iterator") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.16.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "0xi60b1ybg2arxpwh2aqvdllv4b3wnsbybam8zq3vhc954nj2g7j")))

(define-public crate-midas-0.5 (crate (name "midas") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fallible-iterator") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.16.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "0bg4i59sqcyrxqdvcn6vkqyzq1bl4lqkfpmmrs5fiy8ssjaqbqxj")))

(define-public crate-midas-0.5 (crate (name "midas") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.16.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "0i08ajv7w1q1xzzblngj90ndhhw3js6f0ff1ci9iyjx1kp7jvqlm")))

(define-public crate-midas-0.5 (crate (name "midas") (vers "0.5.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.16.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "0b1qpp5cc0h9gicw5cz4fa84b1dz2j334yg9bxzhi0db269hrw5q")))

(define-public crate-midas-0.5 (crate (name "midas") (vers "0.5.4") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-attributes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (features (quote ("tracing-log"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)))) (hash "08n7wifn69vypzsm6lhrpzzx0nh5gn4hc7vs9vfp9xgkzf6mmv2n")))

(define-public crate-midas-0.5 (crate (name "midas") (vers "0.5.10") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-attributes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (features (quote ("tracing-log"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)))) (hash "1wnn2r0djq6p1g4bnwwqmfkhrli0ma2yd369485ainfyxp0r50c6")))

(define-public crate-midas-0.5 (crate (name "midas") (vers "0.5.11") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-attributes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (features (quote ("tracing-log"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)))) (hash "0z0nq38g93disgdybkpmmn48nki51lkyhf5dkdyy5m8ad3zadmaf")))

(define-public crate-midas-0.5 (crate (name "midas") (vers "0.5.12") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-attributes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (features (quote ("tracing-log"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)))) (hash "0f50iy2fya9bp2m8kqrzj4gmknwbsp1v66ayaqrn8bpxyqs2wx5d")))

(define-public crate-midas_cli-0.1 (crate (name "midas_cli") (vers "0.1.0") (deps (list (crate-dep (name "midas_rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "08aacwcq4pzvglglfvz2xk99ywnj4w9yq2565hyfrq39i93bj2bf")))

(define-public crate-midas_cli-0.1 (crate (name "midas_cli") (vers "0.1.1") (deps (list (crate-dep (name "midas_rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1h8cxydmnajvsnjwz1jxp6fkzqqabyav6sglrb015bn209x71zlx")))

(define-public crate-midas_cli-0.2 (crate (name "midas_cli") (vers "0.2.0") (deps (list (crate-dep (name "midas_rs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zd6fpc5ic16r3050m76nylhc17sd6azvda2jx6p1l7z7fgmziz4")))

(define-public crate-midas_cli-0.2 (crate (name "midas_cli") (vers "0.2.1") (deps (list (crate-dep (name "midas_rs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zz3r2mr7cdz0id0p2lbphg6c5p4sam60vrr225k2985gcw79vvh")))

(define-public crate-midas_python-0.1 (crate (name "midas_python") (vers "0.1.0") (deps (list (crate-dep (name "cpython") (req "^0.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "midas_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g8v9jwdwblfxbxxm7fkyipzrprid3izlqnmv6cj93x380xzyih8")))

(define-public crate-midas_python-0.1 (crate (name "midas_python") (vers "0.1.1") (deps (list (crate-dep (name "cpython") (req "^0.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "midas_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1phqmgp0c52zrvb2rfxqdj50blqaik8swvi44m59946nyg9hqxl6")))

(define-public crate-midas_python-0.2 (crate (name "midas_python") (vers "0.2.0") (deps (list (crate-dep (name "cpython") (req "^0.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "midas_rs") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gfcvx4sk6im6gdf0nvqhmlf6167qxwqbs430c2qvpidyjncapyj")))

(define-public crate-midas_python-0.2 (crate (name "midas_python") (vers "0.2.1") (deps (list (crate-dep (name "cpython") (req "^0.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "midas_rs") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bxv296k002im268zrykcxa89r6l7hzsvrhshp5ay3d1wx58j2hf")))

(define-public crate-midas_rs-0.1 (crate (name "midas_rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (kind 0)))) (hash "134qzbhp7j2lqmmw3c6l99r233w4qknfkvxb61lgdacx2xk9q2w3")))

(define-public crate-midas_rs-0.1 (crate (name "midas_rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (kind 0)))) (hash "12kdfvgxhqz5y09pmn150r2bnfykzsq1xcrr0d8rg685dg1gnv4i")))

(define-public crate-midas_rs-0.1 (crate (name "midas_rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (kind 0)))) (hash "0mq6l1gr2bx3jrvj93y773fzpzdkhfigajz3r27d503mi6nmh7xk")))

(define-public crate-midas_rs-0.1 (crate (name "midas_rs") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (kind 0)))) (hash "1h1dwpc07vj7gzp65wmmmp1s17zpyz5nv0x4m6r55ijv1zc45yk4")))

(define-public crate-midas_rs-0.2 (crate (name "midas_rs") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (kind 0)))) (hash "12pcmcjimms4i0110c066i8nx1xw0009zn84mdyd7swbggx045jp")))

(define-public crate-midas_rs-0.2 (crate (name "midas_rs") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (kind 0)))) (hash "1bij1crkswighl44ffx8jskvg6axbf4kl05q46vb5fgp7jfvrbhn")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.0") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "082dqna012zays1lr978lj4pfa5gh6vx1m415y6i5wfcifmg9qwg")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.1") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "014fnm38lva1cjhw8p10ssj1whigx9c9arxhzlqja82vfjwlnhxd")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.2") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "18fmxzwp5931qm8iyzx20b0rw26sd3v5kzk3157zc3i6dc3c2q6p")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.3") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0mvc1pzxzac1f9n0y10d7cl61ijwhbhfnhqlxhii0407k6zn074c")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.4") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "07j6963nf0v4spvlnsks4jip9jk083sv9y04hqvzy89n852q5hil")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.5") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1gb41i1izr8s734hjpl743r8j22m8xhv3pz930ngy6nycj8xwq48")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.6") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "19lbhfly5z9q7v2r756lclxr9wihs4l8m1dw8kb6ni8gx1zy9bg2")))

(define-public crate-midas_vga-0.1 (crate (name "midas_vga") (vers "0.1.7") (deps (list (crate-dep (name "vga") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1g4dllj1xxivhkl280ak9qgbwyz8jkh2kk7cqa44nkg6mcsgwf6q")))

(define-public crate-midasio-0.1 (crate (name "midasio") (vers "0.1.0") (hash "1mxlarm1b3qqd69kjximk0qs57mkm4j7l8ajgwb8k2c9f9f07fvq")))

(define-public crate-midasio-0.1 (crate (name "midasio") (vers "0.1.1") (hash "0c92prnza11ahwv15r5cfjcdzphp3jp856kivpa6vx6h6gh6df8w")))

(define-public crate-midasio-0.2 (crate (name "midasio") (vers "0.2.0") (hash "0zx6j4jsd35z4dxy65jsrihzb6dpvh58014lgqc1b0r68116ryr6")))

(define-public crate-midasio-0.3 (crate (name "midasio") (vers "0.3.0") (hash "0w6lwllg02gnk98z7fmvm9md3py2aidgllb9lsdfj74mckgf9wv6")))

(define-public crate-midasio-0.4 (crate (name "midasio") (vers "0.4.0") (hash "0mbz59jv34403h9wrsiz2mb560ib8k61cc8d6mymn0xyjd026s85")))

(define-public crate-midasio-0.4 (crate (name "midasio") (vers "0.4.1") (hash "045n0rh8zp46bjyyzl4xc10zkn2h0d11vw40nkbhrq8h7q0xbh2y")))

(define-public crate-midasio-0.5 (crate (name "midasio") (vers "0.5.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.5.30") (default-features #t) (kind 0)))) (hash "0in2qkz0glhjyln6mgijmkgy4sf800rgzzkl6mcp1svwkgkgg1qx")))

(define-public crate-midasio-0.5 (crate (name "midasio") (vers "0.5.1") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.5.30") (default-features #t) (kind 0)))) (hash "0v5xcbb1ksnxfw3a93fk5m76wv7p1dcpmcca6p0lrkaqa39zqa87")))

(define-public crate-midasio-0.5 (crate (name "midasio") (vers "0.5.2") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.5.30") (default-features #t) (kind 0)))) (hash "0k79anxpfisn13l3n5ac69rqh0mzdzk4c8jxk3zmgg7agiwvl4hw")))

(define-public crate-midasio-0.5 (crate (name "midasio") (vers "0.5.3") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.5.30") (default-features #t) (kind 0)))) (hash "1yxrbpj1yl6zzfsl6z267bzlhkvcxz2wj0yh6b46scldnv9w6l9j")))

(define-public crate-midasio-0.5 (crate (name "midasio") (vers "0.5.4") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1cqp1hjglnjz8fiscxpv1wlww5hvx64qvnavrpxcmym4jffs0l9z")))

(define-public crate-midasio-0.6 (crate (name "midasio") (vers "0.6.0") (deps (list (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "12hp4vsypwaj31il2dd4cb3q24nkx4csw6fjklfxc4391zzkxqxh")))

