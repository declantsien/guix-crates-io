(define-module (crates-io mi bi) #:use-module (crates-io))

(define-public crate-mibig-taxa-0.1 (crate (name "mibig-taxa") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "07gp7y14xyd54vqfwj3qy7ksr0ip6csgjkp6n8jxl064cbhi3b2x")))

