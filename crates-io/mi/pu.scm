(define-module (crates-io mi pu) #:use-module (crates-io))

(define-public crate-mipush_rs-0.1 (crate (name "mipush_rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.15") (default-features #t) (kind 0)))) (hash "1mjfipfz05v5icrwn7vvbpl6dj745a9xwybzzwszvrmmrsn7p5j2")))

(define-public crate-mipush_rs-0.1 (crate (name "mipush_rs") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.15") (default-features #t) (kind 0)))) (hash "114yrc4m6jgcgw8n6jgdmifm6q5ickq63m6516cag2b278pwvkca")))

(define-public crate-mipush_rs-0.1 (crate (name "mipush_rs") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.9.15") (default-features #t) (kind 0)))) (hash "1baiw06ll1j4v4vdlddmm05wzdqmzkgqlmhzd1rvhzk2ahdfa0h9")))

