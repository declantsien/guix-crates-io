(define-module (crates-io mi ko) #:use-module (crates-io))

(define-public crate-miko-0.0.0 (crate (name "miko") (vers "0.0.0") (deps (list (crate-dep (name "diagnostic") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "peginator") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "voml-collection") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1v2w05c1kphhlmkplxmqgp6zhi3rbhw1y68n1za57x1a1abknv77") (features (quote (("default"))))))

(define-public crate-mikoshi-0.1 (crate (name "mikoshi") (vers "0.1.0") (hash "1h4bl0i35iy8slf5kl47jgkh3rnzla20xmdm560c4h05rhibxys5")))

