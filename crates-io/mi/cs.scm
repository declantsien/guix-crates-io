(define-module (crates-io mi cs) #:use-module (crates-io))

(define-public crate-mics-vz-89te-0.1 (crate (name "mics-vz-89te") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0j6wr76cgb10xcngbcn14b66gvaj4pkg3xlg6dbw77g1mzmnhfry")))

(define-public crate-mics-vz-89te-0.1 (crate (name "mics-vz-89te") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "047k138ipwnydsdpr6hwj63h5i4iqp9z63g8nl9xi2xnyjmmwina")))

(define-public crate-mics-vz-89te-0.2 (crate (name "mics-vz-89te") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3.9") (optional #t) (default-features #t) (kind 0)))) (hash "17yxw4axgjbx7pv4jmh87q3ppl4q7ydn6s92j1sbn2nnyd39srl9") (features (quote (("unproven") ("std")))) (v 2) (features2 (quote (("time" "dep:time"))))))

(define-public crate-mics-vz-89te-0.2 (crate (name "mics-vz-89te") (vers "0.2.1") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3.9") (optional #t) (default-features #t) (kind 0)))) (hash "12fmfnilc6bb54mjdgiiq2j6kcckci6bmpznriv48y0hvgkf30w6") (features (quote (("unproven") ("std")))) (v 2) (features2 (quote (("time" "dep:time"))))))

