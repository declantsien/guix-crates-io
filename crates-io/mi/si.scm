(define-module (crates-io mi si) #:use-module (crates-io))

(define-public crate-misitebao-0.1 (crate (name "misitebao") (vers "0.1.0") (deps (list (crate-dep (name "open") (req "^3") (default-features #t) (kind 0)))) (hash "1w4gzz1xl281aplny4iz9zi1m9d0m9inh3r1091zq0r4jjxj1hpv")))

(define-public crate-misitebao-1 (crate (name "misitebao") (vers "1.0.2") (deps (list (crate-dep (name "open") (req "^3") (default-features #t) (kind 0)))) (hash "0pir6vrmhc06y3jlzc1mcfzp547bjxdp2nimqlw4aacvq85xcf6c")))

(define-public crate-misitebao-1 (crate (name "misitebao") (vers "1.0.3") (deps (list (crate-dep (name "open") (req "^3") (default-features #t) (kind 0)))) (hash "1mys3ipw5kspihp58zmq79x1z4rxbvl3fz0m9l313qrwhrlbw93p")))

(define-public crate-misitebao-1 (crate (name "misitebao") (vers "1.0.4") (deps (list (crate-dep (name "open") (req "^3") (default-features #t) (kind 0)))) (hash "1z0slfqrcs44xfr11g4rsg85a7n1x6bg5jfi8aa5xpw56kmlv26v")))

(define-public crate-misitebao-1 (crate (name "misitebao") (vers "1.1.0") (deps (list (crate-dep (name "open") (req "^3") (default-features #t) (kind 0)))) (hash "1jh9q35s0yfbrk8i3hqas5rf15xi31ianmccigjhcpvda2rs35pb")))

