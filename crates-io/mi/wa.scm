(define-module (crates-io mi wa) #:use-module (crates-io))

(define-public crate-miwa-0.0.0 (crate (name "miwa") (vers "0.0.0") (deps (list (crate-dep (name "miwa-core") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1p57wicjgf0acdccwfzygaydds27qpdqwsv82hyh1s1jrif1ml0m")))

(define-public crate-miwa-core-0.0.0 (crate (name "miwa-core") (vers "0.0.0") (deps (list (crate-dep (name "miwa-macros") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "19mx51cdhy6g217jd2494n60gsbxfwsdg9a5w2pbxyp8ab2chyxg")))

(define-public crate-miwa-macros-0.0.0 (crate (name "miwa-macros") (vers "0.0.0") (hash "1bsnd7845im33v283c5dsjani8gh296d8rahj8ng0lh31h6g3jrr")))

