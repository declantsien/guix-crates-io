(define-module (crates-io mi c_) #:use-module (crates-io))

(define-public crate-mic_impl-0.1 (crate (name "mic_impl") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0s6imv7qwcbz32xar65rfg7ahl1gd9sisf6sn5ajfmymzywnqa3z")))

