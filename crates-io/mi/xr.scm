(define-module (crates-io mi xr) #:use-module (crates-io))

(define-public crate-mixr-0.3 (crate (name "mixr") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 2)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)))) (hash "1p4ajqqrh9rph0w2ii57jk9fm5x41kbivppdz385vipcix730llk")))

(define-public crate-mixr-0.3 (crate (name "mixr") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 2)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)))) (hash "054cb14f5l4kiyfxcgb5h4l8ypkv7m7lflxkgirih3p51f7h0k3x")))

(define-public crate-mixr-0.3 (crate (name "mixr") (vers "0.3.4") (deps (list (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 2)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)))) (hash "1c9wvyys8sprj72y46plnhx7ix0mqnlx2gg7kq3ni9i0sfrwv0n2")))

(define-public crate-mixr-0.3 (crate (name "mixr") (vers "0.3.5") (deps (list (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 2)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)))) (hash "07nkr2qb6ng20npi4yikglq1d5l62vvavlddd8z24wncyy33xa7y")))

