(define-module (crates-io mi xb) #:use-module (crates-io))

(define-public crate-mixbox-0.0.1 (crate (name "mixbox") (vers "0.0.1") (hash "1x0y9bv4na31icxnrcid1ws8apwf80qk0namzs6va15a1kinncgc")))

(define-public crate-mixbox-0.0.2 (crate (name "mixbox") (vers "0.0.2") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0lxjj7n9v9hmnycwdz0cgrqlmra7hkzvhgdn7w2cfc0kdv5fyraj")))

(define-public crate-mixbox-0.0.3 (crate (name "mixbox") (vers "0.0.3") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "19g6yazha2dqssws2r5gpg7vb5dkadjn322vb26zq6nlpgwihs03")))

(define-public crate-mixbox-0.0.4 (crate (name "mixbox") (vers "0.0.4") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1r5v030w17ri8cyxqnpq8c19abjn9byhihxbsrakc1na4z62d9ic")))

(define-public crate-mixbox-0.0.5 (crate (name "mixbox") (vers "0.0.5") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "09i5l9i495548q0jps030fm7sszrmb6zck1vnbrmshb6fr45zvql")))

(define-public crate-mixbox-0.0.6 (crate (name "mixbox") (vers "0.0.6") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "05w1paii9xc4idxbfjsd42ivrqz6cn47gkd3ki096s4qizrraddf")))

(define-public crate-mixbox-0.0.7 (crate (name "mixbox") (vers "0.0.7") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1vja5sf0c3xrcih6gj59j6pi5fq3c21adbhhbrapgq5jmrk8ywck")))

(define-public crate-mixbox-0.0.8 (crate (name "mixbox") (vers "0.0.8") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1rb1wvnja39xlcs4qwzjwlbjixlnq6pyzaqkl9918zazffxj3jm4")))

(define-public crate-mixbox-2 (crate (name "mixbox") (vers "2.0.0") (deps (list (crate-dep (name "libm") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1ik0clm8kq2xj0gmmifc2s8wldg3vqf0apf5czim7y3ld4n0givx")))

