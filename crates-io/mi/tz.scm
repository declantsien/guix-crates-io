(define-module (crates-io mi tz) #:use-module (crates-io))

(define-public crate-mitzvah-0.0.0 (crate (name "mitzvah") (vers "0.0.0") (hash "15yp3nxjxfr0nfir32q12m7d3qck8asmckr8scxazk83dj5bjffb")))

(define-public crate-mitzvah_macros-0.0.0 (crate (name "mitzvah_macros") (vers "0.0.0") (hash "0nvnqav4ai2jrsnxcvkis9iri5k6n8wp3sg5p40jfkgshff7x28v")))

