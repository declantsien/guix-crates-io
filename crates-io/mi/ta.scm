(define-module (crates-io mi ta) #:use-module (crates-io))

(define-public crate-mita-0.0.0 (crate (name "mita") (vers "0.0.0") (hash "1mj98l880blbwkrw1b23x7p7lj7548zyipfjanivv468qzq2k9f6")))

(define-public crate-mita-client-0.0.0 (crate (name "mita-client") (vers "0.0.0") (hash "1xjfhc5pws3b1l2w3y8qh410alp0g7kv36j3hl1rn84zb30f60z3")))

(define-public crate-mitata-0.0.1 (crate (name "mitata") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (default-features #t) (kind 0)))) (hash "00iyknidw0r1blcbf5iz62bazihfzz0z7nhcyv07881023kka9xi")))

(define-public crate-mitata-0.0.2 (crate (name "mitata") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3.1.6") (default-features #t) (kind 0)))) (hash "0cymlx4ibd5a7jaa90j5q9gaqygfrr9f4pfwm2ms8744lg6xn610")))

(define-public crate-mitata-0.0.3 (crate (name "mitata") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std" "env" "cargo" "color"))) (kind 0)))) (hash "1d8va0c96ijg3x084r2dqcb48zpsi6v1s2ql5w973i39jdk7nj3w")))

(define-public crate-mitata-0.0.4 (crate (name "mitata") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std" "env" "cargo" "color"))) (kind 0)))) (hash "0bahpki2pwpip5g06yrhjnl522sx1v3rhdpdsnh8hghnqrkf1899")))

(define-public crate-mitata-0.0.5 (crate (name "mitata") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std" "env" "cargo" "color"))) (kind 0)))) (hash "1v2zzhdyjk7g4qgq1dc8vw7cssd6j4v14sjjkk8djzkh2r29vdpl")))

(define-public crate-mitata-0.0.6 (crate (name "mitata") (vers "0.0.6") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std" "env" "cargo" "color"))) (kind 0)))) (hash "0xqgv1rn235yk1pfs8zr7hnnilbibhc6ds41ijqyhs4f13j46pcp")))

(define-public crate-mitata-0.0.7 (crate (name "mitata") (vers "0.0.7") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std" "env" "cargo" "color"))) (kind 0)))) (hash "0ygsyxlhssjr16ng7jg290xik6gdjllrx2915ifvxib9pnilkz91")))

