(define-module (crates-io mi ur) #:use-module (crates-io))

(define-public crate-miura-0.1 (crate (name "miura") (vers "0.1.0") (hash "0crgzywwgd53cqv9z2wasv1y011imzbc8142rhnpf44qzgccxgj4") (yanked #t)))

(define-public crate-miura-0.1 (crate (name "miura") (vers "0.1.1") (hash "1z6dghsblay9djkjzpjkqc94qidsyqa6pawznkzbd28z2cgbxqyg")))

