(define-module (crates-io mi xp) #:use-module (crates-io))

(define-public crate-mixpanel-0.1 (crate (name "mixpanel") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.31") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1bfp54y9d0mpi24qxx2i0jws4nlxb1qgybqyv6wjgcx6xg08psa0")))

(define-public crate-mixpanel-0.1 (crate (name "mixpanel") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.31") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0l52m4lpp3s7sb0db3l3dnzv14jgrp9p2cwzf9hal06rb9m8zyqz")))

