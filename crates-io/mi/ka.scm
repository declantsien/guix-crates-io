(define-module (crates-io mi ka) #:use-module (crates-io))

(define-public crate-mika-0.0.0 (crate (name "mika") (vers "0.0.0") (deps (list (crate-dep (name "futures-util") (req "^0.3.0-alpha.15") (default-features #t) (kind 0) (package "futures-util-preview")) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "signals") (req "^0.3.5") (default-features #t) (kind 0) (package "futures-signals")) (crate-dep (name "simi-macros") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.45") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.22") (features (quote ("futures_0_3"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.22") (features (quote ("Window" "Document" "Node" "NodeList" "DomTokenList" "Event" "MouseEvent" "InputEvent" "FocusEvent" "KeyboardEvent" "UiEvent" "WheelEvent" "EventTarget" "Element" "HtmlElement" "HtmlInputElement" "Text"))) (default-features #t) (kind 0)))) (hash "0g4m7xwz0423zjvmfc4xnlpnqbyyf20hxkzwl18hylxxymcisz0m") (features (quote (("sub-apps" "message-like-elm") ("message-like-elm"))))))

(define-public crate-mikado-0.1 (crate (name "mikado") (vers "0.1.0") (hash "16v2q0bv7l00jbkgicfcaf2h690rra8a0rpha9gq8jczqizsdavm")))

(define-public crate-mikasa-utils-0.1 (crate (name "mikasa-utils") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00n6i1kly3nl4fz8gjxbigwgmwy5z5ng0skxnzpmgvkinmasz19f")))

