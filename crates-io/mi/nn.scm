(define-module (crates-io mi nn) #:use-module (crates-io))

(define-public crate-minnixrand-0.1 (crate (name "minnixrand") (vers "0.1.0") (hash "0n40jsxf3kmxnn8sp3yzhn6595ssnw51awzdlzkf5zgxczhia19h")))

(define-public crate-minnixrand-0.1 (crate (name "minnixrand") (vers "0.1.1") (hash "0rnawm5vwbn05z3d23vghmzphw1m201vndan9ac71zvhx4z72005")))

(define-public crate-minnixrand-0.1 (crate (name "minnixrand") (vers "0.1.2") (hash "0280wxx7mz8ybi7s3gdl3s82p9vymfgp5avg4lz09d87c29w1bcl")))

(define-public crate-minno-0.1 (crate (name "minno") (vers "0.1.0") (deps (list (crate-dep (name "hangeul") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "01mawiyrk4c3hafbk1dbp90s9d4q0zsvh2xx9gvxzpicb0gjgrqq") (yanked #t)))

(define-public crate-minnow-0.1 (crate (name "minnow") (vers "0.1.0") (deps (list (crate-dep (name "arithmetic-coding") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "minnow-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2.0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0zl7rjgrxgjra4pfmhambcxfnvngfszafbr6qsyvz08qq67g4590")))

(define-public crate-minnow-derive-0.1 (crate (name "minnow-derive") (vers "0.1.0") (deps (list (crate-dep (name "arithmetic-coding") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "darling") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.90") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "134abxw633ckriav5an48g055r22jna7g8hc94cfmh2bb2ww87k5")))

