(define-module (crates-io mi ld) #:use-module (crates-io))

(define-public crate-mild-0.0.0 (crate (name "mild") (vers "0.0.0") (hash "1rb60jilj4w5p9lpimgqk0vxcx0yzj9p9s4z38xk5xgkb6i71fbm")))

(define-public crate-mildew-0.1 (crate (name "mildew") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)))) (hash "1fx9ilylqwy9brw2fmlgw27yyp3lnip6lyylnchnmk7zg7dm87m7")))

(define-public crate-mildew-0.1 (crate (name "mildew") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1wb4mp4kdz9sv2n50zzj9gs3p029rmy2lziv9qbml7ayjjnfkvd3")))

(define-public crate-mildew-0.1 (crate (name "mildew") (vers "0.1.2") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0r0d8narsm3by6aspndfjmf8f9q9v6mrwh5ajpimx8r2pmvjgi6w")))

