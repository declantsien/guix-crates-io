(define-module (crates-io mi x-) #:use-module (crates-io))

(define-public crate-mix-distribution-0.1 (crate (name "mix-distribution") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.0-pre.0") (default-features #t) (kind 0)))) (hash "1cqaddm1w7mff94z0ncns58rn99l8mm4s2pjc1230aszrf0pzqkh")))

(define-public crate-mix-distribution-0.1 (crate (name "mix-distribution") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0hgpkm9qhz31rhil9pawppcp16xavwc3hvzkv64q0772zflqb0pa")))

(define-public crate-mix-distribution-0.2 (crate (name "mix-distribution") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0r01xvxyxpyv038xjiw9mly1jvjnksd0li57g29jlrlnpasrik98")))

(define-public crate-mix-distribution-0.2 (crate (name "mix-distribution") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0axwjq51zlh16rai7vlspdiqqhi5kzdc9mxlic30h7fbrrj42dvl")))

(define-public crate-mix-distribution-0.3 (crate (name "mix-distribution") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wlljvshn694kpii197dbj3pk5b72vwrgwbb9qm429ryfh1znqzx")))

