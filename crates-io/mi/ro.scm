(define-module (crates-io mi ro) #:use-module (crates-io))

(define-public crate-miro-0.1 (crate (name "miro") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "freetype") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "harfbuzz-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "hexdump") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "servo-fontconfig") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "vte") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0335xma2pcxvslp2xfai6ihg4hzsd013gvpwrfvy2qb6fraqnddm")))

