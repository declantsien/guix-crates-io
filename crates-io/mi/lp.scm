(define-module (crates-io mi lp) #:use-module (crates-io))

(define-public crate-milp-0.0.0 (crate (name "milp") (vers "0.0.0") (deps (list (crate-dep (name "milp-types") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1457gfmbfh6rga5c28h97x45sfq02arl2wd0n36jlpk99547v8fn") (features (quote (("default"))))))

(define-public crate-milp-types-0.0.0 (crate (name "milp-types") (vers "0.0.0") (hash "0smxba1cv8rxilvaaalffpsyman42g07ix1aw77a0az32rvj7knj") (features (quote (("default"))))))

(define-public crate-milp-types-0.0.1 (crate (name "milp-types") (vers "0.0.1") (deps (list (crate-dep (name "lp-types") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1si2in5sn9b1zdqsq4ihyd71m6w0vl67idp1f7wf5mqn8bibws38") (features (quote (("default"))))))

