(define-module (crates-io mi h-) #:use-module (crates-io))

(define-public crate-mih-rs-0.1 (crate (name "mih-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hzh71v7lxhm0siwpq29xi43daq86vp59z4b27nrnclic7jlw4jg")))

(define-public crate-mih-rs-0.1 (crate (name "mih-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "193qadw9108daka84pjf6pjrpwvwlcwqqk0k9j2dbycgf4fjsvs8")))

(define-public crate-mih-rs-0.2 (crate (name "mih-rs") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0ycc4c08xqhr5m7kxhakr335h70filvnzn8frkalqsj9q2fga3yd")))

(define-public crate-mih-rs-0.2 (crate (name "mih-rs") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1pw5amz0h4ki4w4nm7zisl8xd07d9zpqkrhi6mglbdvnyfmnq28i")))

(define-public crate-mih-rs-0.3 (crate (name "mih-rs") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0i4wxglg2gf79q5adq7208gd9c0mhgmvfacqrn1056sp4j62yl4r")))

(define-public crate-mih-rs-0.3 (crate (name "mih-rs") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "08gaqi8bizymhr46cskf6yfsb3a3qqlsn0cjiq3622fx9adnvcj8")))

