(define-module (crates-io mi r-) #:use-module (crates-io))

(define-public crate-mir-protocol-0.1 (crate (name "mir-protocol") (vers "0.1.0") (hash "10dqhampa6pc9ah9bg762r1xc88w8hgf166lbnlipm5w4wjnsfvp")))

(define-public crate-mir-rs-0.1 (crate (name "mir-rs") (vers "0.1.0") (deps (list (crate-dep (name "expect-test") (req "^1.2.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)) (crate-dep (name "mir-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jrqvffhlslah6svarc504i9gs25rj72lyfjp0msbii5hpzhmryp")))

(define-public crate-mir-sys-0.1 (crate (name "mir-sys") (vers "0.1.0+mir.0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "0l5lgrc8rqjxh29f7yw3ln6iia1ifb6gv7gdily3mrz09db0gaq4") (features (quote (("parallel-gen") ("default")))) (links "mir")))

