(define-module (crates-io mi np) #:use-module (crates-io))

(define-public crate-minparse-0.1 (crate (name "minparse") (vers "0.1.0") (hash "0a9r4p7cy3sb867v6700ddv7hm4wn4y3lv8vm6sh3dfsbry1qlkq")))

(define-public crate-minparse-0.1 (crate (name "minparse") (vers "0.1.1") (hash "1hq94iljyz88xm4bmf4z4kyjmn5paicfy59hdms6kg11j9las3i9")))

(define-public crate-minparse-0.1 (crate (name "minparse") (vers "0.1.2") (hash "01l27727016gmwxm8lm16rx21l1lw679297a9r6k95abnrxbfivh")))

(define-public crate-minpixwin-0.1 (crate (name "minpixwin") (vers "0.1.0") (deps (list (crate-dep (name "pixels") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "0jr7rb5pxk3pk3r2xic816qnmnd31x9sqc5lsl1xq2s2gdb0bxrb") (yanked #t)))

(define-public crate-minpixwin-0.1 (crate (name "minpixwin") (vers "0.1.1") (deps (list (crate-dep (name "pixels") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "1na9rmyn3wv7b8dy7m8lxysh1gchkqd3v4gkxjaglwjdf3bqs1c6") (yanked #t)))

(define-public crate-minpixwin-0.1 (crate (name "minpixwin") (vers "0.1.2") (deps (list (crate-dep (name "pixels") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "1v5yzn1psqsghnrknz199a0s36b3v6ilw6dkj6z8cyi9yfj81ri8")))

