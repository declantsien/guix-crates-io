(define-module (crates-io mi ls) #:use-module (crates-io))

(define-public crate-milstian-feedback-0.1 (crate (name "milstian-feedback") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qh52mxfwisn4s9fv5czfgngn6sdqm8qghwfcskd91y5mdvf0bh0")))

(define-public crate-milstian-feedback-0.1 (crate (name "milstian-feedback") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dqw4drdmg14qphml0jr4af9137l12d6kak7j79v34h2jvbqr0c4")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.0") (hash "1niphs2nd2fi7gbr92lx39hql78nf27r7k5i88vhbi5kxiwzrnim")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.1") (hash "0izxbjmgnkc09mjc4q9il5wasxd5d8n5idajqhgd43r5c7fdc6w3")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.2") (hash "1k9f88mk9680llk2m7sbnm3rq01sgc65hy7hc0lfp42846aqsfzk")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.3") (hash "1c9rskf9iy86sff4vkyjyxdwgbkfh1qcva2i1rxv81n90pfpncq7")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.5") (hash "1i4m8z265lmygh0dmrj3dwrr3zsvh8w4cszppsfss32rid20iqvj")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.6") (hash "16qxbpgc8m9861wrknvvqinyjnrkyni0kj4mhpcbh09xa5yijgd1")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.7") (hash "1sv1nmv3xqcj9fbrcp6nsb6bqh90j111df8mpry1xywyv89mlbdm")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.8") (hash "0v8zkp3wk1wapmj9y2y6vv40hca0sxg9cfa0an7g2c4fkx9l5gim")))

(define-public crate-milstian-http-0.1 (crate (name "milstian-http") (vers "0.1.9") (hash "1jn9jfj66dah4dmwmrh7pnsdqri20w4v6cfgn7xlns801y84mb95")))

(define-public crate-milstian-internet-framework-0.1 (crate (name "milstian-internet-framework") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1y42yck0mq69wk47ymrfmkryf67q5ai1fm08sq09hqnqzcm18p9n")))

(define-public crate-milstian-internet-framework-0.1 (crate (name "milstian-internet-framework") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0cjfljsbcinzjwcja9fz6f2n11fl50daqvxvl7708987fsyzv2d6")))

(define-public crate-milstian-internet-framework-0.1 (crate (name "milstian-internet-framework") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0ym4vdi4w526abdgxz7ig929m7h043viwr27gkiqlamn5df8ag9n")))

(define-public crate-milstian-internet-framework-0.1 (crate (name "milstian-internet-framework") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1gf0m1hmzw18l76zij0zfwk111kqmh5c8pikkqiswz63cvxdjxqm")))

(define-public crate-milstian-internet-framework-0.1 (crate (name "milstian-internet-framework") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0ixzl615dqs837v2x3rpyy2rnjlficgwx93d3s7p1xlr779bsqp7")))

(define-public crate-milstian-internet-framework-0.2 (crate (name "milstian-internet-framework") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-feedback") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0m0jhdy30zydgsa8ih0dgw8qk26vi2m6h6n0cz31fdqyfzf69y9s")))

(define-public crate-milstian-internet-framework-0.2 (crate (name "milstian-internet-framework") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-feedback") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0052v6w65pv6bdy7x6zjv6kdh2vj4kywcmcyvs7n6j9zkyg5yflf")))

(define-public crate-milstian-internet-framework-0.2 (crate (name "milstian-internet-framework") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-feedback") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1w0cbsmr35azlb6s27i4ndi8zwp8fq7y55lz30ds7jrjr0dgmbz9")))

(define-public crate-milstian-internet-framework-0.3 (crate (name "milstian-internet-framework") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-feedback") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1xag0qh9v0s9222db4kjzrhnzrjc2gyxakjpg4kps8pkkyr85qfz")))

(define-public crate-milstian-internet-framework-0.3 (crate (name "milstian-internet-framework") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "milstian-feedback") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "milstian-http") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0hnliqv7vch0cgrmhzhvxpqj9s7hkdfnflqmrbpwgzx12i4h1ijf")))

(define-public crate-milston-0.1 (crate (name "milston") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jm41wb6qm5y4ab5l3ldzv2idlj4ia0s882lb0yzapkrpjqcqyrk") (features (quote (("full" "http")))) (v 2) (features2 (quote (("http" "dep:reqwest" "dep:url"))))))

(define-public crate-milston-0.2 (crate (name "milston") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jxx1ykldcc78qg47jk1i1lmz8qgybqlzql20rlk0vvinspdz826") (features (quote (("full" "http")))) (v 2) (features2 (quote (("http" "dep:reqwest" "dep:url"))))))

