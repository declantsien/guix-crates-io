(define-module (crates-io mi ca) #:use-module (crates-io))

(define-public crate-mica-0.1 (crate (name "mica") (vers "0.1.0") (deps (list (crate-dep (name "mica-hl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mica-std") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0q77d14q25kmdxq4dspvrdawi1dxxpwjr136as2i50divgshhjql")))

(define-public crate-mica-0.2 (crate (name "mica") (vers "0.2.0") (deps (list (crate-dep (name "mica-hl") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mica-std") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "17vpifbb0i95hp4hi9cgjscv8ayjrd0lx3zbyybs6xwzqiwm370x") (features (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.3 (crate (name "mica") (vers "0.3.0") (deps (list (crate-dep (name "mica-hl") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mica-std") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0y8k3pljvscms4hsy6xyzlj7wg9fm47v7lbbnybykdy6nn0hwhnb") (features (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.4 (crate (name "mica") (vers "0.4.0") (deps (list (crate-dep (name "mica-hl") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "mica-std") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1frwf5xd8l7xgq561psgyj3a16x281jw6spx1ggg65v4qxr5ss20") (features (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.5 (crate (name "mica") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "mica-hl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "mica-std") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 2)))) (hash "128k7bnfz133csdn5rkgwli6k9sghdgrf6ls2bgchb7csg5kdh7j") (features (quote (("std-io" "mica-std/io") ("default"))))))

(define-public crate-mica-0.6 (crate (name "mica") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 2)))) (hash "0lakbz91dgs1lc7ay548frwlq0pqf7a8firvi63g9j656rpw10mv") (features (quote (("default"))))))

(define-public crate-mica-0.7 (crate (name "mica") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 2)))) (hash "18m15nnl7mhnl1dbsm7rhzljrappay6qcbk2qln5vj920030gxwd") (features (quote (("default"))))))

(define-public crate-mica-0.7 (crate (name "mica") (vers "0.7.1") (deps (list (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 2)))) (hash "1hda600x23dhamy7zjj0kznw7anim54fa3707n93dh2nfp9mql6l") (features (quote (("default"))))))

(define-public crate-mica-cli-0.1 (crate (name "mica-cli") (vers "0.1.0") (deps (list (crate-dep (name "mica") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0qbdlpsa72m5bfqd5d2gszd2gfk86rdf0b524rx6rh909l2xyc40")))

(define-public crate-mica-cli-0.2 (crate (name "mica-cli") (vers "0.2.0") (deps (list (crate-dep (name "mica") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "19n60arhkcvmvr7fjx3sb9crzijk1724xym6wh0zapiy9z2f0ric")))

(define-public crate-mica-cli-0.3 (crate (name "mica-cli") (vers "0.3.0") (deps (list (crate-dep (name "mica") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "03yvzq854n1gb5r34j890lq936764bwwb9ljqm3x8296v70mycs2")))

(define-public crate-mica-cli-0.4 (crate (name "mica-cli") (vers "0.4.0") (deps (list (crate-dep (name "mica") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "00y7d229d3xcxx0814bm85cj383nlfplnjxh5qi47nkv6kxfc9h1")))

(define-public crate-mica-cli-0.5 (crate (name "mica-cli") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mica") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)))) (hash "01lzg2xx0l002gk44jqk9x790dwdn8w15j2adwg0s6xw793awna6")))

(define-public crate-mica-cli-0.6 (crate (name "mica-cli") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mica") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)))) (hash "1awdf8h2x7hdl6l56jqfvgsji1c5r9c8yscvswzrnmx2fvx7kzr8")))

(define-public crate-mica-cli-0.7 (crate (name "mica-cli") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mica") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)))) (hash "0ndhwzyxz63r9p1kqn3lqmhpm8x9z2865ikl7qvp7qzvk4zxx1vi")))

(define-public crate-mica-cli-0.7 (crate (name "mica-cli") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mica") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (default-features #t) (kind 0)))) (hash "06hr3nkary5gcxx1fli9wl23cqbyzypmdnm0s3sjajakvi51781f")))

(define-public crate-mica-hl-0.1 (crate (name "mica-hl") (vers "0.1.0") (deps (list (crate-dep (name "mica-language") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0l9nsnmy75v7405whwxs3492ff9k6lbnpkja6k7xvm2gdjfnd170")))

(define-public crate-mica-hl-0.2 (crate (name "mica-hl") (vers "0.2.0") (deps (list (crate-dep (name "mica-language") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1clr61k61kl6wggrd1i5mwd7l8xfbrc7kfx1h69c9rxgakmlsrls")))

(define-public crate-mica-hl-0.3 (crate (name "mica-hl") (vers "0.3.0") (deps (list (crate-dep (name "mica-language") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0w1xqmigk27caida8p0zdr4xajd2qahv4ssn7n33yyqjddxxrlrl")))

(define-public crate-mica-hl-0.4 (crate (name "mica-hl") (vers "0.4.0") (deps (list (crate-dep (name "mica-language") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "07i8i03h7n8wzw6xhm4xd739n2hnp2clnf8a2p9n7x8n3qlyfvmw")))

(define-public crate-mica-hl-0.5 (crate (name "mica-hl") (vers "0.5.0") (deps (list (crate-dep (name "mica-language") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "14kqgcv52r8m2s9sp6n0hb4q1cpn0aj19mwj897z43iniw7bbj89")))

(define-public crate-mica-language-0.1 (crate (name "mica-language") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "05bs82cah86s8ggvd1dhwc1xsgkg3i2yqfpgxfmfbzr4j497l2nb")))

(define-public crate-mica-language-0.2 (crate (name "mica-language") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0ahaambrqv068748ld9ipsj14sx759w3iaczkyh2f631867p3jzi") (features (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("default"))))))

(define-public crate-mica-language-0.3 (crate (name "mica-language") (vers "0.3.0") (hash "1nmb9qdnjbfwhvvzl8v8lbdw8wvfa89pli5c2fwpdv88dnwnjxm2") (features (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("trace-vm-calls") ("trace-gc") ("default"))))))

(define-public crate-mica-language-0.4 (crate (name "mica-language") (vers "0.4.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "09gmf9qz870626jd955kmfh2fkmp3in9fglk4xn08kgc7p5805vm") (features (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("trace-vm-calls") ("trace-gc") ("default"))))))

(define-public crate-mica-language-0.5 (crate (name "mica-language") (vers "0.5.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "1mcmjax2xxafvgbb9w3jvaphfbzfickiz53awjk4ypjyqj3c6jm9") (features (quote (("trace-vm-stack-ops") ("trace-vm-opcodes") ("trace-vm-calls") ("trace-gc") ("default"))))))

(define-public crate-mica-std-0.1 (crate (name "mica-std") (vers "0.1.0") (deps (list (crate-dep (name "mica-hl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zg04wymz5fzasgp508v8j035w80b01zzi8p5l4famngb2j4wal6")))

(define-public crate-mica-std-0.2 (crate (name "mica-std") (vers "0.2.0") (deps (list (crate-dep (name "mica-hl") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1dddlf1sysl6sqg7fa8riazxixks63xj8wfhgajr4m6m0gifv79y") (features (quote (("io"))))))

(define-public crate-mica-std-0.3 (crate (name "mica-std") (vers "0.3.0") (deps (list (crate-dep (name "mica-hl") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0y48nfkck82f9775cj7ca5kykcfjzbqqvxxm8lr4w4nmasy5m8hv") (features (quote (("io"))))))

(define-public crate-mica-std-0.4 (crate (name "mica-std") (vers "0.4.0") (deps (list (crate-dep (name "mica-hl") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "06crpm3zi2hdzbaghggffv780lxa1vn493zhzhwrl40i7wsckcic") (features (quote (("io"))))))

(define-public crate-mica-std-0.5 (crate (name "mica-std") (vers "0.5.0") (deps (list (crate-dep (name "mica-hl") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "10hy9n8snr5gyw4k752cfgmad3r8mcg7irvqk3syd0fprsp8izpn") (features (quote (("io"))))))

