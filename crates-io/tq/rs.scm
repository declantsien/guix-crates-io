(define-module (crates-io tq rs) #:use-module (crates-io))

(define-public crate-tqrs-0.1 (crate (name "tqrs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.8") (default-features #t) (kind 0)) (crate-dep (name "tqdm") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0jm417y3vxk60xpgx3f05h5aip84g9pjysgjhqx126agb2gxvzhr") (yanked #t)))

