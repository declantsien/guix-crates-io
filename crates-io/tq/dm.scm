(define-module (crates-io tq dm) #:use-module (crates-io))

(define-public crate-tqdm-0.1 (crate (name "tqdm") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0ykjyn4dggsglb234mba998b4wdv9pyjnf5w95y1dzhvlji5nbbj")))

(define-public crate-tqdm-0.1 (crate (name "tqdm") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1i0j75syr65lwcq23rrvdhf2snjxm62xpwz1ylkb0lvab5jc28w4")))

(define-public crate-tqdm-0.2 (crate (name "tqdm") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0c0p2bp6nv7n5gw44c93dm2r5w47mkfap8ksp2ypw59x1nml3gk0")))

(define-public crate-tqdm-0.2 (crate (name "tqdm") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0jfsa5iarnmcpzc6g3xl2z15d8l50j9kbdgacpigb9y6apfr1w3k")))

(define-public crate-tqdm-0.2 (crate (name "tqdm") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1chrr22c86iy1khf792kahh8yss7jcfh6b5y2ycxlrxqi12rla8f")))

(define-public crate-tqdm-0.2 (crate (name "tqdm") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1fa1fgf5hf9wn1kh6r187b9dpi753ihpmsswm228v9z4qk796kjr") (yanked #t)))

(define-public crate-tqdm-0.3 (crate (name "tqdm") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1q7np8cjk9f5kyg3znxipmpy238166wf8vx388p9jb502mwx7ihq")))

(define-public crate-tqdm-0.3 (crate (name "tqdm") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1y9pdxmc4ayr4l33dm627zxvz0qrzw17bwvg4x3zvrwsj7hh7gb5")))

(define-public crate-tqdm-0.4 (crate (name "tqdm") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1pxp8np98lgs345hbpcrrv5pm8bwvp5j8v8agv32gl7wazgyf20g")))

(define-public crate-tqdm-0.4 (crate (name "tqdm") (vers "0.4.2") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)))) (hash "18zbkxxs9y77hnzd9zjzmpfln6zaiy1nszf6dfmja36ng98wh4rv")))

(define-public crate-tqdm-0.4 (crate (name "tqdm") (vers "0.4.3") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)))) (hash "1bn5g6jhjppy4vizwa7f1h8db78f2hbdq4sljgbbvy20izf8fah7")))

(define-public crate-tqdm-0.4 (crate (name "tqdm") (vers "0.4.4") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)))) (hash "08vkggsi8vq4n49a369ipmpc7n2vzjpiifiznaalbzdzwv5ns1bg") (rust-version "1.63.0")))

(define-public crate-tqdm-0.5 (crate (name "tqdm") (vers "0.5.0") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)))) (hash "1kqagvrs6f5fz1f873im1whn4hhrb608br0b2laq73c0jzkk85kh") (rust-version "1.70.0")))

(define-public crate-tqdm-0.5 (crate (name "tqdm") (vers "0.5.1") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)))) (hash "1zp6293lm0b3f9ca183r9xkj2s5l2f137wc80hbjb0hx7sxfgccd") (rust-version "1.70.0")))

(define-public crate-tqdm-0.5 (crate (name "tqdm") (vers "0.5.2") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1bpzdqwswv4zsl6374xryhzl1980zm2k6f47bmma297vczwzfipb") (rust-version "1.60")))

(define-public crate-tqdm-0.6 (crate (name "tqdm") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "07sjznak9i74d0n1fzg4msa8nccim9xn6br7pf5vc3l38vm4m7lw") (rust-version "1.60")))

(define-public crate-tqdm-0.7 (crate (name "tqdm") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.18") (features (quote ("alloc" "std"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "07v1vjv2va8p3wx0na7vr6wgp59ck5hrhpgicndsj1824hr2jbda") (rust-version "1.60")))

(define-public crate-tqdm-rs-0.0.1 (crate (name "tqdm-rs") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0x8c3sp3nwjjifzxdpcf6jwjnndd9dmfcy7gci4bazpra409wipm")))

(define-public crate-tqdm-rs-0.1 (crate (name "tqdm-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "1mz21i2grvsrgjph967ddf9lxj0s5915j77zgli6df15f75fm78g")))

(define-public crate-tqdm-rs-0.1 (crate (name "tqdm-rs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "1fn2bs5w17prm40r37drjwhvwg4b3rbk9g04yp0ji1jlsaifm7p5")))

(define-public crate-tqdm_show-0.1 (crate (name "tqdm_show") (vers "0.1.0") (deps (list (crate-dep (name "tapciify") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "tqdm") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0ryl0d0kzmxcbyhzh6ahx1vn3vwcp815aadj8siq46d0h5c0hsf0")))

