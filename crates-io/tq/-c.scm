(define-module (crates-io tq -c) #:use-module (crates-io))

(define-public crate-tq-code-generator-core-0.1 (crate (name "tq-code-generator-core") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0yxdvld444f1zr26vsbl403nasniarvdh76wm47swrsc8cr20nb7")))

(define-public crate-tq-code-generator-core-0.1 (crate (name "tq-code-generator-core") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0yb5wgrljkjgrw96yd3cp0wb9yqnf2wmd0xgd82ssigqc3m85m6g")))

