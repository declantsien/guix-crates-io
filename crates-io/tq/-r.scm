(define-module (crates-io tq -r) #:use-module (crates-io))

(define-public crate-tq-rs-0.0.0 (crate (name "tq-rs") (vers "0.0.0") (hash "1ys7shn99f3yp33jw41dxnljz7h95lrkpy4nf7pcg6m40kdclqr7")))

(define-public crate-tq-rs-0.1 (crate (name "tq-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive" "usage" "help"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1h9jjxmx95lgcmgpfpp0dvx5pcycva7f2qva9gaayd9j1wyg5rfi")))

(define-public crate-tq-rs-0.1 (crate (name "tq-rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive" "usage" "help"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hskc501dlrf0vj479wrwc20zrffhs20iqlhlj5wvwz19qhanzfs")))

(define-public crate-tq-rs-0.1 (crate (name "tq-rs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive" "usage" "help"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "16vkfiz0b4wz8avkzza9bpxzzai5l660pap6m8qml1jcrif87x37")))

