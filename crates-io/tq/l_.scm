(define-module (crates-io tq l_) #:use-module (crates-io))

(define-public crate-tql_macros-0.0.1 (crate (name "tql_macros") (vers "0.0.1") (deps (list (crate-dep (name "clippy") (req "^0.0.33") (default-features #t) (kind 0)))) (hash "1cs08my3dal6qzm8zz50f6c54rn980s1hjsysc9lggza6h8f4yhk")))

(define-public crate-tql_macros-0.1 (crate (name "tql_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("extra-traits" "printing" "full"))) (default-features #t) (kind 0)))) (hash "1114ayb2888mgrc3j7f9mamz5js47y31avqb2k9xqj04n77ishva") (features (quote (("unstable" "proc-macro2/nightly") ("rusqlite") ("postgres"))))))

