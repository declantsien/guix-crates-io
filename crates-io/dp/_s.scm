(define-module (crates-io dp _s) #:use-module (crates-io))

(define-public crate-dp_sample-0.1 (crate (name "dp_sample") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1ll5vwz9mxacab5d5nbzd5w3gig1c9zlxc9yk77p3ggg8mizx3dm")))

