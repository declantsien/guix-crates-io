(define-module (crates-io dp kg) #:use-module (crates-io))

(define-public crate-dpkg-query-json-0.1 (crate (name "dpkg-query-json") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "06vdf5rp1wnciir5vhj44fgk8jcinnfm2afg8zlk9fn3g0wiznd9")))

(define-public crate-dpkg-query-json-0.1 (crate (name "dpkg-query-json") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0lb3314pwqshydg9wz8j2wx5fcvxkb5jr0mxjns4l68n79lh0s59")))

(define-public crate-dpkg-query-json-0.1 (crate (name "dpkg-query-json") (vers "0.1.11") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0m33wfn2jnrfg0n53pn93ypr0ydql5qrgdjxwzp2dyir58kj6sfp")))

(define-public crate-dpkg-query-json-0.1 (crate (name "dpkg-query-json") (vers "0.1.12") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1rr97sp5c81wki11gdrwxfqiyyasy60rrsgbv2x8xzl08aldlbhc")))

