(define-module (crates-io dp du) #:use-module (crates-io))

(define-public crate-dpdu-rust-0.8 (crate (name "dpdu-rust") (vers "0.8.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "17s6wa7h2pcfqbkrps56cywsjv3j3hqwfaxw4wc82bpfvv7lr86l")))

(define-public crate-dpdu-rust-0.8 (crate (name "dpdu-rust") (vers "0.8.5") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "17886zzsliambiqg7ywg31lnjy3wwpha460kz3rhqamvmci3injp")))

(define-public crate-dpdu-rust-0.8 (crate (name "dpdu-rust") (vers "0.8.6") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0wshjqhzwdx73psv5mqn0rwxxh4r2fxjy7fg6lbh09gs7chj1x7b")))

