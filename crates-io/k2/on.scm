(define-module (crates-io k2 on) #:use-module (crates-io))

(define-public crate-k2on-structs-0.1 (crate (name "k2on-structs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gnn3di7vxx6030f9yq9pkj757q7kqcf2r645ln1zzy8hgrld75z")))

