(define-module (crates-io k2 ha) #:use-module (crates-io))

(define-public crate-k2hash-0.0.1 (crate (name "k2hash") (vers "0.0.1") (deps (list (crate-dep (name "k2hash-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "14g1bl09smk2ffb4j0cz629wk8ccarws88s5g5r2319dj8mqvfqk")))

(define-public crate-k2hash-sys-0.0.1 (crate (name "k2hash-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0f95rsvhqmw51kdk62899w97m5z94yj70if6b49hqflyzmwnv5hi")))

