(define-module (crates-io k2 #{2f}#) #:use-module (crates-io))

(define-public crate-k22f-0.1 (crate (name "k22f") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req ">= 0.6.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0aicir1qg936isngmb1fyjxyfjhbmzhy07xxszxhf3mpy7z3cf3a") (features (quote (("rt" "cortex-m-rt/device"))))))

