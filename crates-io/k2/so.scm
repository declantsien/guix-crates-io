(define-module (crates-io k2 so) #:use-module (crates-io))

(define-public crate-k2so-0.1 (crate (name "k2so") (vers "0.1.0") (hash "10ld5zpzy328bksrlq00zyn9gj0pjn7s6iacbsk4xnsnvsmxi61v")))

(define-public crate-k2so-2 (crate (name "k2so") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "~2.19.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "19b6ppv6422sq5qpfjlvyw2hq8aaw39a32xhjg6cz7mhcls3ykm8")))

(define-public crate-k2so-2 (crate (name "k2so") (vers "2.1.0") (deps (list (crate-dep (name "clap") (req "~2.19.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "13wcjxsjdkk1bpmb9d9h0bhx7clvwiwa5ffqpf41pbxw9d6xra16")))

