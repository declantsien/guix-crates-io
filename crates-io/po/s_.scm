(define-module (crates-io po s_) #:use-module (crates-io))

(define-public crate-pos_pso-0.1 (crate (name "pos_pso") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0s8irxlr3cck19gsb72z5hx1gggl9bsakhs0gn5pr72b5hfaipxs")))

(define-public crate-pos_pso-0.1 (crate (name "pos_pso") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1wgs6ni1cimc43ykrqm3j9hd3kxmp2xmwqpr8l2b8pmhflc7xzwq")))

(define-public crate-pos_pso-0.1 (crate (name "pos_pso") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "00jd32wxpmghsx4va2shr4jd34gadm43c5dgfq4kbjkgbxpb5pv8")))

(define-public crate-pos_pso-0.1 (crate (name "pos_pso") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0jvqfsjqrirzbbp6q28qydh0afay1c9f2lhd464bd6fx3bk5d9r6")))

(define-public crate-pos_pso-0.1 (crate (name "pos_pso") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "06x9g180jmd5fffgyd2y595dqgy84p2mskcj05dhnipg9gsgs3ds")))

(define-public crate-pos_pso-0.1 (crate (name "pos_pso") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0k610hc3a0ja9203f7q9mr4v7b81i9dhwfx7hffc7kqcmpycqp84")))

(define-public crate-pos_pso-0.1 (crate (name "pos_pso") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1bi095fc2hldygvvhj6z96v6cpf6r8g0jmd79jvknk037hgk0d19")))

