(define-module (crates-io po ch) #:use-module (crates-io))

(define-public crate-pochita-0.1 (crate (name "pochita") (vers "0.1.0") (deps (list (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "07wdv6ilvcqpx2ymmfaz03cb46b7xkkvnlhqa8fm3xhjrnn069yx") (yanked #t)))

(define-public crate-pochita-0.1 (crate (name "pochita") (vers "0.1.1") (deps (list (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v6yn4601x52h3dkjz2mcwdvd8295y1s798hxl1bqzl6knik4pd4") (yanked #t)))

(define-public crate-pochita-0.1 (crate (name "pochita") (vers "0.1.2") (deps (list (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "1q9c4mlsls73n087rqpf3a3d0zq3cb6ssz407k5pwxgfpj5y743j") (yanked #t)))

(define-public crate-pochita-0.1 (crate (name "pochita") (vers "0.1.3") (deps (list (crate-dep (name "smallvec") (req "^1.10.0") (features (quote ("union"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cksdv79rq2bgrvc7aqxbzs921hpvnh16gw8aqwibsbqahciaj73") (yanked #t) (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-pochita-0.1 (crate (name "pochita") (vers "0.1.4") (deps (list (crate-dep (name "smallvec") (req "^1.10.0") (features (quote ("union"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ssw1zmbczlbacz47zi9sp4qpzh2bbyq111kfzk6fvjl3ps1gp5i") (yanked #t) (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-pochita-0.1 (crate (name "pochita") (vers "0.1.5") (deps (list (crate-dep (name "smallvec") (req "^1.10.0") (features (quote ("union"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ix31nnijgxj3yaxaza51if2z381amdq22q8xqcj24nswdz364jn") (yanked #t) (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-pochta-0.1 (crate (name "pochta") (vers "0.1.0") (hash "0262hprarkpjpmz31jq002v2hkbr6nr5ybrypq3l7wrvg0m6m8yq")))

(define-public crate-pochta-0.1 (crate (name "pochta") (vers "0.1.1") (hash "01cgq5h4mr6vpk6795c4idvb6x7drmcfcldgvbq180p027h0zqwy")))

