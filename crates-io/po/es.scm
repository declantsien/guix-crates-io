(define-module (crates-io po es) #:use-module (crates-io))

(define-public crate-poestat_static-0.1 (crate (name "poestat_static") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 1)) (crate-dep (name "uneval_static") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "03ic10psrv4w9z78g69q5h61wvpvrzprm6fzzgk10sn3h8h1c546")))

