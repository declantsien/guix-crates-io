(define-module (crates-io po nc) #:use-module (crates-io))

(define-public crate-ponci-0.1 (crate (name "ponci") (vers "0.1.0") (hash "0rrfzv0476g94ngfvq5121db2bw08qfycpxw0sax26jfv1a1kfrs")))

(define-public crate-poncu-0.1 (crate (name "poncu") (vers "0.1.0") (hash "0yacw866jsqqg8cm4npjpmxai951690g91mib3fnwp4k7hpyrfd9")))

