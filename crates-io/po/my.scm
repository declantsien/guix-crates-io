(define-module (crates-io po my) #:use-module (crates-io))

(define-public crate-pomy-0.1 (crate (name "pomy") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.10.0") (features (quote ("vorbis"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "00n78ag8pf97c0zl48f584sr2lwkqcb2gqnqhx9h4hjydbwr7wjq")))

