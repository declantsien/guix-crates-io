(define-module (crates-io po le) #:use-module (crates-io))

(define-public crate-pole-0.1 (crate (name "pole") (vers "0.1.0") (hash "16n7fi9r1inyaww40abr86kwd2mksqiwmn39x7fs00pbqms6ymcr")))

(define-public crate-polestar-0.1 (crate (name "polestar") (vers "0.1.0") (hash "1jcw6xh7kcjs64nci8l8qaxzlfm8gby0yc8byh559ia1pac5x5fp")))

