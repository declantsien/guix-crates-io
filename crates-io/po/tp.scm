(define-module (crates-io po tp) #:use-module (crates-io))

(define-public crate-potpack-0.1 (crate (name "potpack") (vers "0.1.0") (hash "0cpfn5hpszfgmpvlq5hy5ackw1zcinakywl38g3665dxhmzc1nbw")))

(define-public crate-potpack-0.1 (crate (name "potpack") (vers "0.1.1") (hash "0xji7if2i2wzwj403j3h8fd5i0adcany1ylxlsgl8x70176cliiy")))

(define-public crate-potpack-0.1 (crate (name "potpack") (vers "0.1.2") (hash "0blwga0zwmb100l3r6s2bv3i6fjs92xyqi2qrc862ca7j73ydhc8")))

(define-public crate-potpourri-0.0.1 (crate (name "potpourri") (vers "0.0.1") (deps (list (crate-dep (name "ndarray") (req "^0.15") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ractor") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1dy6ydpbvyv3wbcj3cjjsagnaig88dksx0rrvxwzdvca19i01yha") (v 2) (features2 (quote (("ractor" "dep:ractor") ("ndarray" "dep:ndarray" "dep:ndarray-rand")))) (rust-version "1.65")))

