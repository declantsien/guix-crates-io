(define-module (crates-io po gp) #:use-module (crates-io))

(define-public crate-pogp-0.1 (crate (name "pogp") (vers "0.1.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("console"))) (default-features #t) (kind 0)))) (hash "16az98zr51mda6wvpj59w79psz7pdgvbnsr3ji0lzjapywxk87pi") (features (quote (("default" "console_error_panic_hook")))) (yanked #t)))

(define-public crate-pogp-0.0.5 (crate (name "pogp") (vers "0.0.5") (hash "1kmv8868mp5prxl9rr0qip7mkzi8wjnm0q7swr4229yyl2xsmz5w")))

(define-public crate-pogp-0.0.6 (crate (name "pogp") (vers "0.0.6") (hash "0lahq6sdf2wg7zyz4fyc1171b0r8vr5ziswncbw3fzwzvay5lmvh")))

(define-public crate-pogp-0.0.7 (crate (name "pogp") (vers "0.0.7") (hash "0npsn6wm028n9k4zfampfcsn1kx79v99qh201fv8vinzkavp5hgf")))

(define-public crate-pogp-0.0.8 (crate (name "pogp") (vers "0.0.8") (hash "03yn8qg0rp9j0b9xmhypw50xkv5f7lr170gg913n1rhsqk40crl9")))

(define-public crate-pogp-0.0.9 (crate (name "pogp") (vers "0.0.9") (hash "119z2h75f6ky7d5p8h5hxjqpw7g8y93hyjk5b3902silyk1pnnhx")))

(define-public crate-pogp-0.0.10 (crate (name "pogp") (vers "0.0.10") (hash "0bsljhhk4qkpzxvan2177g2rzxhfhdpw2vq11wbl884sqyqnjlr8")))

(define-public crate-pogp-0.0.11 (crate (name "pogp") (vers "0.0.11") (hash "02mr3ypwdb7v86n1mgdnp7f0kd8n6g1g7vzn2gwpn1pkncicyw1q")))

(define-public crate-pogp-0.0.14 (crate (name "pogp") (vers "0.0.14") (hash "0i30nvxk75n1l9qmzwbfq7m66nq0ca8lbd9wssnqc2669yr7i1vh")))

(define-public crate-pogp-0.0.16 (crate (name "pogp") (vers "0.0.16") (hash "1q7yh4q7svmg85yb6viradgcwn636dbx73zffws7khdij1np7p3s")))

(define-public crate-pogp-0.0.17 (crate (name "pogp") (vers "0.0.17") (hash "12scp2p5qnclpp51hp40y6pn9zy6dnqd6zgrd4ypdn1153pdm6vz")))

(define-public crate-pogp-0.0.18 (crate (name "pogp") (vers "0.0.18") (hash "1q7hhbw2v6a7jd2mmzsv485xrq2h48hj88vzi7h42lcnqc9sw53z")))

(define-public crate-pogp-0.0.19 (crate (name "pogp") (vers "0.0.19") (hash "1vvj5cx7damsgn862rh46d3hldggbdjhbjpwn78c0cf2yf4vm7kl")))

(define-public crate-pogpen-0.2 (crate (name "pogpen") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5") (features (quote ("serde_impl"))) (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1mq7sj33d76b5qkc7cw9srn28vwflgpzafpizfg74ja0n6yl952x")))

