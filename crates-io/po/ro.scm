(define-module (crates-io po ro) #:use-module (crates-io))

(define-public crate-porous-0.1 (crate (name "porous") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (kind 0)) (crate-dep (name "serde") (req "^1.0.185") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zybdi524va2sihmijp5x26k217akmh4nvzym6nnyy6x77zyy3k7")))

