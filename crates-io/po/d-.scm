(define-module (crates-io po d-) #:use-module (crates-io))

(define-public crate-pod-enum-0.1 (crate (name "pod-enum") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (kind 0)) (crate-dep (name "pod-enum-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ldiknz6jff1ykk7cmk1fk2cih3cckk07215z8ywv1plz13032f9")))

(define-public crate-pod-enum-macros-0.1 (crate (name "pod-enum-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1wlmd8yrs6054aihil6k2dyvi5f50272mayafa59jjr9k8rap9x2")))

