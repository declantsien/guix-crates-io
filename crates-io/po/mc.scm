(define-module (crates-io po mc) #:use-module (crates-io))

(define-public crate-pomc-1 (crate (name "pomc") (vers "1.1.2") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^3.14.1") (default-features #t) (kind 0)))) (hash "17dnm3ic8qjy83ciwk9d2kxzgxga2d19lzxd0b84sbxd5zs7qkc6")))

