(define-module (crates-io po pt) #:use-module (crates-io))

(define-public crate-poptea-0.1 (crate (name "poptea") (vers "0.1.0") (deps (list (crate-dep (name "data-encoding") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.20.1") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "x509-parser") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "02lmp4k0qlis6yx6mcilpc77k80agdiq367m2y0wspzhd28wy58m")))

