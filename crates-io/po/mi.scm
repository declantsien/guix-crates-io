(define-module (crates-io po mi) #:use-module (crates-io))

(define-public crate-pomidorka-0.1 (crate (name "pomidorka") (vers "0.1.1") (deps (list (crate-dep (name "rodio") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1k3yylgs2l93rlmdlf3kbqx8hfzhil8srlh50m82dddr54idphdr")))

(define-public crate-pomidorka-0.1 (crate (name "pomidorka") (vers "0.1.2") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1xv4p6bg8dzw8f9ipap6sp8q4xqz4jxvda38gqs923yz7rhrdyiy")))

