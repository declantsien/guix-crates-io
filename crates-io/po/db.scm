(define-module (crates-io po db) #:use-module (crates-io))

(define-public crate-podboy-0.1 (crate (name "podboy") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "1nvi818if4vwkvfj3qqrfkrqc4nf2bbymcl11pr8i6324p06f7xm")))

