(define-module (crates-io po ma) #:use-module (crates-io))

(define-public crate-poma-ci-publish-test-0.1 (crate (name "poma-ci-publish-test") (vers "0.1.8") (hash "0d3rnjjzg4f883mshd96bj2l13z76fjmi5c78pcx4jsmvsvvi41r")))

(define-public crate-poma-ci-publish-test-0.1 (crate (name "poma-ci-publish-test") (vers "0.1.9") (hash "01f5ffdp6a0pv7adj1x0lh5fn7jwmd3rxxfj66hymii0c1i5gp57")))

(define-public crate-poma-ci-publish-test-0.1 (crate (name "poma-ci-publish-test") (vers "0.1.10") (hash "0b0sa230dwhg07izh6n97fqz4r51zcyajjnklvwbd00sw0v5x0hg")))

