(define-module (crates-io po co) #:use-module (crates-io))

(define-public crate-poco-scheme-0.0.1 (crate (name "poco-scheme") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (kind 0)) (crate-dep (name "gc") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lexpr") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.7") (default-features #t) (kind 0)))) (hash "0xx8f82lxzhpnnqzqc0bnamdwx4zchf5mbc5n2py0q4308pb23wb")))

