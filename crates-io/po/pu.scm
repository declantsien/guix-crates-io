(define-module (crates-io po pu) #:use-module (crates-io))

(define-public crate-populated-0.0.1 (crate (name "populated") (vers "0.0.1") (hash "1pnh8gi93x5krib6x7yh4vak4k61irglcmp9v0p98a0q7a9x5ajh")))

(define-public crate-populated-0.0.2 (crate (name "populated") (vers "0.0.2") (hash "01grx99s6jljljhk6iwwms1cllzx47d6id28p25vznvwcfxh5xa2")))

(define-public crate-populated-0.0.3 (crate (name "populated") (vers "0.0.3") (hash "1ilwfb8swamglvqfsl0xdyvnjb5dwinr7f7n60i0argfsp9lfsm1")))

(define-public crate-populated-0.0.4 (crate (name "populated") (vers "0.0.4") (hash "1axvvpq4pahk3iwpzwr2y5fjp5mbd8xx9wb4lwwjk6gh0ysh0hsj")))

(define-public crate-popup-translation-0.2 (crate (name "popup-translation") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "x11-clipboard") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1bp1my4d8hx6mm28pbix9d9yh2crwkkp9hxliqzj1ygqd81jh8qc")))

