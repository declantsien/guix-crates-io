(define-module (crates-io po wi) #:use-module (crates-io))

(define-public crate-powi-0.1 (crate (name "powi") (vers "0.1.0") (hash "1j3kcn5wkf0bijrhpmj4f9ndvgv1zd0ab0hg1lzqydxhv3z93rh5")))

(define-public crate-powierza-coefficient-1 (crate (name "powierza-coefficient") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "strsim") (req "^0.10") (default-features #t) (kind 2)))) (hash "0hjq0vi9gnww3yypmnihcmgwk74jln0w676xcr68rksj69w47alw") (yanked #t)))

(define-public crate-powierza-coefficient-1 (crate (name "powierza-coefficient") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "strsim") (req "^0.10") (default-features #t) (kind 2)))) (hash "0zxslbnwka95wkiq82if1dv8nrci94xnn6h25ix2mbvkr8xw1qk4") (yanked #t)))

(define-public crate-powierza-coefficient-1 (crate (name "powierza-coefficient") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "strsim") (req "^0.10") (default-features #t) (kind 2)))) (hash "0nrpq1hvdnl7ddlgffrz0a36cvv7rbhyys0fzy6mc9h0fmwk04h4")))

