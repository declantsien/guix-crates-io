(define-module (crates-io po po) #:use-module (crates-io))

(define-public crate-popol-0.1 (crate (name "popol") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "012innlnwxg1kihy08x1k8hsjgvdcnm1148an4hmm7dqsdviz40n")))

(define-public crate-popol-0.2 (crate (name "popol") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "066pw6g6mqwzps5w12ih12hz7i6dkh63ab6jn9im21ljvln0djqz")))

(define-public crate-popol-0.3 (crate (name "popol") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1gzsqhsibp384p6mxic8ysabwg2r7bx3dk228jdb6va61r4nkfsk")))

(define-public crate-popol-0.4 (crate (name "popol") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0bas5bn53brsd7srhmm6rh76g36k63pcvx7k3z46a8r9pwr29k6r")))

(define-public crate-popol-0.5 (crate (name "popol") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "158l44krvmldvsqvcl91m3qzfa2c44rj8diwpwdrjbgwwq891b1d")))

(define-public crate-popol-1 (crate (name "popol") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "07m4v5pvzydl1ykp0yg58rbi2ycxyfdidq51q73k5m3fwnlc4nkw")))

(define-public crate-popol-2 (crate (name "popol") (vers "2.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "1klsnwfp1gwyfrvrkvw2ngw8apfivnq691vqri57dhzzn0376my6")))

(define-public crate-popol-2 (crate (name "popol") (vers "2.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "15zk4395n7hg4p212spk1wzpngjpbid3c8hsw1cliiyh32mz96w3")))

(define-public crate-popol-2 (crate (name "popol") (vers "2.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "0cf5yg57cmwp4bkww4pf5yd0a04pba1fciqd6h89sfxybh8xslx9")))

(define-public crate-popol-3 (crate (name "popol") (vers "3.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "1qskbisrdkpzyyn45dr55avgwl98wvarbks114jlci1fa0rnjh4k")))

(define-public crate-popoplot-0.2 (crate (name "popoplot") (vers "0.2.0") (deps (list (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "0d1nvkgprbblh8g79qx4hyl0z0iy6qx784aq13lsfbysr2cfqg17")))

(define-public crate-popoplot-0.3 (crate (name "popoplot") (vers "0.3.0") (deps (list (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1fgx71l7ybcb0yr4rjhjwgyxp8v6gpxc8xkcnrwqbya3m30fk2p2")))

(define-public crate-popoplot-0.4 (crate (name "popoplot") (vers "0.4.0") (deps (list (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "0aarka01h90kgrya6bpih10igmk4zpflspakmi4223jk9r9npf5i")))

(define-public crate-popoplot-0.5 (crate (name "popoplot") (vers "0.5.0") (deps (list (crate-dep (name "plotters") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "plotters-backend") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1lc3gvyx4hbi527x9v8xr24fbq13fdw9n8jw8bx0slvcw0vii8vi")))

(define-public crate-popoplot-0.6 (crate (name "popoplot") (vers "0.6.0") (deps (list (crate-dep (name "plotters") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "plotters-backend") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "0v0vfiskzx7xggxpi2101fhv41dmb1f5gkdcj83gxj97l7i4vx9j")))

(define-public crate-popoplot-0.7 (crate (name "popoplot") (vers "0.7.0") (deps (list (crate-dep (name "plotters") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "plotters-backend") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1447iw5xjhsihil81vzadqvm70m9fvckxv08irkgh73dggk07r9l")))

