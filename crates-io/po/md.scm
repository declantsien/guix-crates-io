(define-module (crates-io po md) #:use-module (crates-io))

(define-public crate-pomd-1 (crate (name "pomd") (vers "1.4.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "pausable_clock") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^3.14.1") (default-features #t) (kind 0)))) (hash "1pjx7hqpmndp0rjy8viv56a7vli499q4lbyhdqq6ynv084phmcmf")))

