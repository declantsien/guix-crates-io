(define-module (crates-io po ul) #:use-module (crates-io))

(define-public crate-poule-0.2 (crate (name "poule") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fdv4myqw0kj6628d6hd52lbqyr73aymi33ni3hbqhxv27s9a669")))

(define-public crate-poule-0.3 (crate (name "poule") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hcml98afjl4f95jiwnfjmmcf2xlrcqpa2c9q13g9lv8648l6dcb")))

(define-public crate-poule-0.3 (crate (name "poule") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mk4xnf92ml3rmqxfzdrylqyilrrg6clfkhkwhclabkkgx12psyz")))

(define-public crate-poule-0.3 (crate (name "poule") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wwjf9fnnvgzgg1xwzrmjzxkcbm07az7s3n0rgz1fb011lyzjc2q")))

