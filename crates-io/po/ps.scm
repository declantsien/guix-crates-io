(define-module (crates-io po ps) #:use-module (crates-io))

(define-public crate-popsicle-0.1 (crate (name "popsicle") (vers "0.1.5") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b68awyclxibc3h7l3m8rkdzv3yvzhmrzjizw3b54gr6zcy1542i")))

