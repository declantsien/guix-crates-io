(define-module (crates-io po di) #:use-module (crates-io))

(define-public crate-podio-0.0.1 (crate (name "podio") (vers "0.0.1") (hash "0wbrs0zx4mja297g1gqxsa7lknkvjin5hdh6gacwv2sx8kwz2vib")))

(define-public crate-podio-0.0.2 (crate (name "podio") (vers "0.0.2") (hash "1xfvb500bcgavvbxy3x3f30h0jl1m9a2hcwr9x7g9qr96q70sm0k")))

(define-public crate-podio-0.0.3 (crate (name "podio") (vers "0.0.3") (hash "1v5hfwdzy5dc3h758r3c43ssr0644mj87ibg6s2srrniykzyrfv9")))

(define-public crate-podio-0.0.4 (crate (name "podio") (vers "0.0.4") (hash "1hrn1cslpsyqzvvjvbgqffhn2h7h6qdzxh23xpw1wqr60m35m6w2")))

(define-public crate-podio-0.0.5 (crate (name "podio") (vers "0.0.5") (hash "012h8r7nsalgbpyv872zm42877mk4lz8cz41ddiq51rhx6azcl1s")))

(define-public crate-podio-0.0.6 (crate (name "podio") (vers "0.0.6") (hash "0zd23246v9yhsbw8bvyirlldv6m8wp18b8rdchgxaznkxb7dcy3y")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.0") (hash "04cg11wmh4r7a3qimsac9r7m6vxqwx8fh4pryw49iqjdr0rj56nd")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.1") (hash "07vszgxk23ibl3v97ghck0k33byhyhnksasdrj2gd33r9myx0n05")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.2") (hash "1addi15p79m12dfp6b80swwv5lgl6qvbq1x71n86dchq6w1a66zf")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.3") (hash "0vb2l2i953amn2ra3ri0v6p17nhiqcs7vcjchaglw1qc9rd1hxci")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.4") (hash "01zqhx1hhsh9n6y77vfajpfiys0hkfbf0rxa9my0a5ws3xnrgixm")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.5") (hash "1843adrajhrz13m3qpzx727i6n2264vh2yvimr3wqmxww4g2lhp5")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.6") (hash "1ga5arhwakj5rwrqzf9410zrbwnf24jd59af8kr9rgwbd6vb83vq")))

(define-public crate-podio-0.1 (crate (name "podio") (vers "0.1.7") (hash "06bzjxrl0h8rp5860n51dlr1g143grg2jmx4g6y1mdn2ignyz2xi")))

(define-public crate-podio-0.2 (crate (name "podio") (vers "0.2.0") (hash "1hla8fjfa8aj1w2lawjs5wz1xr2608jd70w3rmdfjg05dknpqfg8")))

(define-public crate-podium-0.1 (crate (name "podium") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive" "cargo" "unicode" "wrap_help" "env"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "k8s-openapi") (req "^0.15") (features (quote ("v1_22"))) (default-features #t) (kind 0)) (crate-dep (name "kube") (req "^0.73") (features (quote ("runtime"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "tui-logger") (req "^0.8") (default-features #t) (kind 0)))) (hash "0s9l8py074675jshnmy4yawk61ibrl4ryahs44dlhwzvw2k4rgp4")))

(define-public crate-podium-0.2 (crate (name "podium") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "cargo" "unicode" "wrap_help" "env"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "k8s-openapi") (req "^0.21") (features (quote ("v1_24"))) (default-features #t) (kind 0)) (crate-dep (name "kube") (req "^0.88") (features (quote ("runtime"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (features (quote ("all-widgets"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui-logger") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0kb1lnk3r05v80np12x5jqg00z0i2kcdfn3fdlmr0f5w3hgh7qq2") (rust-version "1.75.0")))

