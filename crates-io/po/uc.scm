(define-module (crates-io po uc) #:use-module (crates-io))

(define-public crate-pouch-0.1 (crate (name "pouch") (vers "0.1.0") (hash "082ii6jyqj076vycwihi7i61a901piq9gzlspln6rssqp96yp4gy") (yanked #t)))

(define-public crate-pouch-0.0.2 (crate (name "pouch") (vers "0.0.2") (hash "074n09na0wljrfaqbasr9p88maln0g9avsaz7yawjzhrrdqbwl9g") (yanked #t)))

(define-public crate-pouch-0.0.3 (crate (name "pouch") (vers "0.0.3") (hash "07675l2mjz0dyp7d1hk5bc206jsi0am41aknsn7aq5klbynszsjx") (yanked #t)))

(define-public crate-pouch-0.0.3 (crate (name "pouch") (vers "0.0.3-alpha") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "034606wcjhw4gx7rgbs51lh9r1yf92y9jb9imkzimssa2wgblmm7") (yanked #t)))

(define-public crate-pouch-0.0.4 (crate (name "pouch") (vers "0.0.4-alpha") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "0m0i8f7m7hr5rk33aj9dfr0mlq255c73vi9c9b7xxkb0h898f1w3") (yanked #t)))

(define-public crate-pouch-0.0.5 (crate (name "pouch") (vers "0.0.5-alpha") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "02zckp6hxa39pp0d1rr08ry73zr94q9fsri4nqvmnnjld9c53zzy")))

(define-public crate-pouch-0.0.6 (crate (name "pouch") (vers "0.0.6-alpha") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "08wr5pxsmvwykxjnghfffpjsazw94d0jp4lkjrv4h9ph6p5kc32z")))

(define-public crate-pouch-0.0.7 (crate (name "pouch") (vers "0.0.7-alpha") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "1r22naiv6zn4g01g7wlj4v32dhsmgvxri9z7bc7nnnk1rhld1xyg")))

