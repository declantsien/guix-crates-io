(define-module (crates-io po t-) #:use-module (crates-io))

(define-public crate-pot-conditioner-0.1 (crate (name "pot-conditioner") (vers "0.1.0") (deps (list (crate-dep (name "backlash") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dyn-smooth") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jqzj6q2vnj94ikgf2fsa1pfd7jdl0szwlkk2dzh5ywdisxjcp62") (rust-version "1.56")))

(define-public crate-pot-rs-0.5 (crate (name "pot-rs") (vers "0.5.0") (deps (list (crate-dep (name "ipnet") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1c6jgypwn9m9mpk3srr63svzxbnjs2ndm5ggkkg2qr0nxz5fjspc")))

