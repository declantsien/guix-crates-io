(define-module (crates-io po ny) #:use-module (crates-io))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "01wlp93f3yw551xjcwz5nhrvn8avny1h4wif27c0zz22lzczdilk") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "0w6s8ylam438ari1cpg3q94spfz19znlfnpv2yinva5r7xkcghvb") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "0dkm6fipsyda78dpbvm4a4zv0hyxr53zzg1sbqmwcpfwqhi7ykf5") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "1p55dx2n1q741hppnf1mcjfip1mwg2zapq2skljm52c6k00aa136") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.4") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "0kxf0z6qx275mhjyddmadinp2287xn7mb06s4f71hwyns93qffa8") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.5") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "18njm2z3ni58pvvvlzlzpnx0fvxnbniqmm7rylxf93na52c8k7x5") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.6") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "19f4bwmxax8r2ngqrb0yjdg73x555fyjwr8zadigh2jx2nz582j3") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.7") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1") (default-features #t) (kind 0)))) (hash "0bgyrvzb8b8drz9chpr44ivncadmglyy64wjlykkdqw495jf8r9g") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.8") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1") (default-features #t) (kind 0)))) (hash "13sb3cw87a8r2rq4ddmj8470d4hw6zm7sz1w6gq9x60gfa68yg2z") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.9") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1d3wn83i7hn1xcfvvy4mi9r7pd754wlg0nhidbwrzdx657mv0j90") (yanked #t)))

(define-public crate-pony-0.1 (crate (name "pony") (vers "0.1.10") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1gsghikhp46s1wv8h7f7zjir0clnsb67fmqh5lr1x2kjw8y8159w") (yanked #t)))

(define-public crate-pony-playground-0.1 (crate (name "pony-playground") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hubcaps") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lru-cache") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.0") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "wait-timeout") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lkninw8dni2jbypb0zbpbzbbvnljga2jysgzqxjcykszk6n8l2j")))

(define-public crate-ponyfetch-0.1 (crate (name "ponyfetch") (vers "0.1.0") (hash "0vwhfbxj5s64by4j1mr8nkgi8m1simg30v0fbhph5cnp7ab5yyyh")))

(define-public crate-ponyfetch-0.2 (crate (name "ponyfetch") (vers "0.2.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0ma13q6na0f5k4wmd7rzml1pw83yis9im51vrzza1mg1lvbzxhl2")))

(define-public crate-ponyfetch-0.2 (crate (name "ponyfetch") (vers "0.2.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0h26wz2119cs1h54r6hp423ssazb8h205i98v94ik88rxrnnwmmh")))

