(define-module (crates-io po ee) #:use-module (crates-io))

(define-public crate-poee-0.1 (crate (name "poee") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("full" "mio"))) (default-features #t) (kind 0)))) (hash "1l871fn43r3b7pfp35052jh47li9c76xalb6ggfrbyqza5qi8d8k")))

