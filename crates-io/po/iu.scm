(define-module (crates-io po iu) #:use-module (crates-io))

(define-public crate-poius-0.1 (crate (name "poius") (vers "0.1.0") (hash "1x66r6r0rpfddmvm1rw0sqz3z3swy9bwciwhqm6r6dnnpj4y9lvp") (yanked #t) (rust-version "1.77.2")))

(define-public crate-poius-0.1 (crate (name "poius") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0vm2n2xahnigrm1nvmr3ci873s7kjgcxb55pbzq6r9yaxpvkcv46") (yanked #t) (rust-version "1.77.2")))

(define-public crate-poius-0.1 (crate (name "poius") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0c844ppwdmniwc06n5rf6njjj5dhpflj1wjmv6zflr6v768r822s") (yanked #t) (rust-version "1.77.2")))

(define-public crate-poius-0.1 (crate (name "poius") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0d4xgkdqqgyxfhigjfg7p27wn3lh13vs8d7i1yjz1pyd3bk52l6x") (yanked #t) (rust-version "1.77.2")))

(define-public crate-poius-0.1 (crate (name "poius") (vers "0.1.4") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "02lvndrkj1x78cmhns1zw1kppivhv6gv7x4bi5mniq11a5qci26g") (rust-version "1.77.2")))

