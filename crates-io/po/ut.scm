(define-module (crates-io po ut) #:use-module (crates-io))

(define-public crate-pout-0.0.1 (crate (name "pout") (vers "0.0.1") (deps (list (crate-dep (name "serde-transcode") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1pakgjmhbyp9lv73s7g91wigxpbqlk0imqf1732qhdqjjis3grq7") (yanked #t)))

(define-public crate-pout-0.0.2 (crate (name "pout") (vers "0.0.2") (deps (list (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (features (quote ("paw"))) (default-features #t) (kind 0)))) (hash "0n39mrhccp91m884bxldh8ix5adykzjcbfj56ms81w7fhyhdbs3n") (yanked #t)))

