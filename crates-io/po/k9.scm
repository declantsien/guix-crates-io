(define-module (crates-io po k9) #:use-module (crates-io))

(define-public crate-pok9-0.1 (crate (name "pok9") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "01nxhrzpx8s6s5b22lalw5d6fsa19d7nhhsh2xzyjm275myqh40l")))

(define-public crate-pok9-0.1 (crate (name "pok9") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "132afkp9khkzb8jksapdm4glq73wgnxvj1ixmdlr8ag56cx3k9g8")))

