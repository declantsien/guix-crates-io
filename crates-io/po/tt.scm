(define-module (crates-io po tt) #:use-module (crates-io))

(define-public crate-potterscript-parser-0.1 (crate (name "potterscript-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vlfzzdqqvjzx5lhzssvihv59whbvaq1dyy4vbykmnjbcla5jv1x")))

(define-public crate-potterscript-parser-0.2 (crate (name "potterscript-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 2)))) (hash "16ggbqqhrbpbv3aiy38pxlm16qc8y6cmw3fvrnkzy36z6npjwywz")))

(define-public crate-potterscript-runtime-0.1 (crate (name "potterscript-runtime") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "potterscript-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0yv44m0l11qihx0kdws16p3hc2pjffc1j9rpcv8370c4dn21fr3z")))

(define-public crate-potterscript-runtime-0.2 (crate (name "potterscript-runtime") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.64") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "potterscript-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.64") (features (quote ("console"))) (optional #t) (default-features #t) (kind 0)))) (hash "14pfh954cp139qjkv2mvmsmiw7m4kp7yl00vllv8zliz547dbv5c") (features (quote (("std" "colored" "rand") ("js" "js-sys" "wasm-bindgen" "web-sys") ("default" "std"))))))

(define-public crate-potting-helper-database-0.1 (crate (name "potting-helper-database") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4.4") (features (quote ("postgres" "r2d2"))) (default-features #t) (kind 0)))) (hash "0s8qm7fn5hjc6dklk6843vrakyzcby1jbhvj59nc0llz0vyi8k5p") (yanked #t)))

(define-public crate-potting-helper-database-0.1 (crate (name "potting-helper-database") (vers "0.1.1") (deps (list (crate-dep (name "diesel") (req "^1.4.4") (features (quote ("postgres" "r2d2"))) (default-features #t) (kind 0)))) (hash "1h5kdfj12ddwc05di84bz5623kx081vanky3fyfdiirjwvcjv32h") (yanked #t)))

