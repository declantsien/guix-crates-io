(define-module (crates-io po w_) #:use-module (crates-io))

(define-public crate-pow_of_2-0.1 (crate (name "pow_of_2") (vers "0.1.0") (hash "1i77lbb475ycfs7sb8zw0awi3szaaiqfgmizqhvi8l5f1h5ma7pn")))

(define-public crate-pow_of_2-0.1 (crate (name "pow_of_2") (vers "0.1.1") (hash "0hdcl6wbzpr03iw0grlminmyqp5mihp078np2r9fnan8q4asfkb9")))

(define-public crate-pow_of_2-0.1 (crate (name "pow_of_2") (vers "0.1.2") (hash "10yk0i16r3qz5g7sx94vlpmsalwgfw25s1v6p8yb6p7k8qgfqh22")))

(define-public crate-pow_sha256-0.1 (crate (name "pow_sha256") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1zzil3906fd6xhrl5xgb10x4x95li46xn7fhqxv9mkkzsa4qnmhm")))

(define-public crate-pow_sha256-0.2 (crate (name "pow_sha256") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1fvh8nqggljh27p4bq5512xnl880a0miq01m5d4dq7f9i3npbxja")))

(define-public crate-pow_sha256-0.2 (crate (name "pow_sha256") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0bqqb60acqk4ddabpxra7kfivk6vrqsq07q1ib07zd8hisn67a2r")))

