(define-module (crates-io po m-) #:use-module (crates-io))

(define-public crate-pom-preview-3 (crate (name "pom-preview") (vers "3.0.4-alpha-1") (hash "0wnisjsxp50z3xx8wcwacn25gglzmzgqh0h353h4srj7rzid614g")))

(define-public crate-pom-rs-0.1 (crate (name "pom-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4") (default-features #t) (kind 0)))) (hash "0l8vv97jx7px62wnd086qmk4wkds311n8vf4qjz28a7bl37pkhy2")))

(define-public crate-pom-rs-0.1 (crate (name "pom-rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4") (default-features #t) (kind 0)))) (hash "1a72im6j2fjh21jxznyfqxy90hngp0rnwr92ih73rir00ysdz5z2")))

(define-public crate-pom-rs-0.1 (crate (name "pom-rs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4") (default-features #t) (kind 0)))) (hash "02d1mmvkg0pmlcyvlw6k1fm54hgrk0glw37cvvr0i817pw6sg9dh")))

(define-public crate-pom-trace-4 (crate (name "pom-trace") (vers "4.0.0") (hash "1n1fng9w321lly7hdxdwbffidw2r5k5lngdaamd9g99x9b8cv6hx")))

(define-public crate-pom-trace-4 (crate (name "pom-trace") (vers "4.0.1") (hash "1amf4wr0ljkhdlfgr4i0xx60rg8hpbzs9y11mipjpigzhjqjys7v")))

(define-public crate-pom-trace-4 (crate (name "pom-trace") (vers "4.0.2") (hash "1qa25dgw42jsfzhhz020dn9ldnc8rg59mkdql7ji52hglrizwg9f")))

(define-public crate-pom-trace-4 (crate (name "pom-trace") (vers "4.0.3") (hash "13pkm0525nfl4bnc86ylx8k48ign60dgw3slhcm0am77jl63raj9")))

