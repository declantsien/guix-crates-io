(define-module (crates-io po c_) #:use-module (crates-io))

(define-public crate-poc_lib-0.1 (crate (name "poc_lib") (vers "0.1.0") (hash "0h6k98hxamd7xgszn0j8lgyd0fjzz44q507qanicrgp49klx5wrs")))

(define-public crate-poc_lib-0.1 (crate (name "poc_lib") (vers "0.1.1") (hash "0gfilak4a6znfnm0iii4chxxh6kjfjw498q4300g9v4xqvq78s98")))

(define-public crate-poc_lib-3 (crate (name "poc_lib") (vers "3.4.5") (hash "1269xlc42hhp8k9vkasjgz757snpawdy5s6sqx83a8x1y12hvsv9") (yanked #t)))

(define-public crate-poc_lib-0.0.2 (crate (name "poc_lib") (vers "0.0.2") (hash "0sspx9blfqgi45c1yi2m4wf164v6lsr131jfv2picw6s2a6a1hl2")))

(define-public crate-poc_library-0.0.1 (crate (name "poc_library") (vers "0.0.1") (hash "08c2r2dhwlnsxd2c3vqxr9j2w0bfssxiyf43blc26cqyysxc1drd")))

(define-public crate-poc_library-0.0.4 (crate (name "poc_library") (vers "0.0.4") (hash "0pdbp7qqrkz3vk9la1kgk0f42j7x8dsjxhnlrpmsir7w4dfghk0v")))

