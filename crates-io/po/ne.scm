(define-module (crates-io po ne) #:use-module (crates-io))

(define-public crate-pone-0.1 (crate (name "pone") (vers "0.1.0") (hash "0qkizx9vjilladjp33sfklgy15zc0f0dxsgj2n3cahxxcfbv7g2v")))

(define-public crate-pone-0.1 (crate (name "pone") (vers "0.1.1") (deps (list (crate-dep (name "pone") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qc6nsf78ff10xcxlc1h02g204cmazb0mi06scalwdi72xywc3xn")))

(define-public crate-pone-0.1 (crate (name "pone") (vers "0.1.2") (hash "0l9sm90dlh730rbbrvms3drnbay5xn35wn329b4gqgbnnr7f9kqb")))

(define-public crate-pone-0.1 (crate (name "pone") (vers "0.1.3") (hash "1rpbyk4m6y9v0s7n8l0gsp5l5ic7x1k7bq4xqwzjamigqh3bnla8")))

(define-public crate-pone-0.1 (crate (name "pone") (vers "0.1.4") (hash "1gd2gvazncqg8zd74r2yp0494fyc4avnbznbpkvnhjq3zqs4l46k")))

(define-public crate-pone-0.1 (crate (name "pone") (vers "0.1.5") (hash "19qnn43njg8yq5knpkcyhalx1sxdnp4xd0gzqgk8dimvshkdbb01")))

