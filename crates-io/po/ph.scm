(define-module (crates-io po ph) #:use-module (crates-io))

(define-public crate-popher-0.1 (crate (name "popher") (vers "0.1.0") (hash "06xrkfa3dayb0cf8jfsgzwhid321p3blxfbqxq5rkxg6ydrzjzjs")))

(define-public crate-popher-0.1 (crate (name "popher") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1f6my9nkymzy9wsfrvmdxwj02nzqjg8s76ywd0r4kfawj7kkywqg")))

