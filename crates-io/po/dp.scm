(define-module (crates-io po dp) #:use-module (crates-io))

(define-public crate-podping-api-0.1 (crate (name "podping-api") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "0vv6ssx4k2adn0xzkgwih4gc8ki3qpa5kxwjh2sxp2rvnycrbnxj")))

