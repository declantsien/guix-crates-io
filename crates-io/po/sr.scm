(define-module (crates-io po sr) #:use-module (crates-io))

(define-public crate-posrel-0.1 (crate (name "posrel") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "11sq26psb6g04wbhnizxd6pg65krxr6ijhq99r4mbry3cmrpsj2a")))

