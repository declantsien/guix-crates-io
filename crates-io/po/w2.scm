(define-module (crates-io po w2) #:use-module (crates-io))

(define-public crate-pow2-0.1 (crate (name "pow2") (vers "0.1.0") (deps (list (crate-dep (name "zerocopy") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy-derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gn74sqpzavdpc4gayp1aqkg82yjxb7fpvpvhl9g9ndpvk6wxjfm") (features (quote (("default"))))))

(define-public crate-pow2-0.1 (crate (name "pow2") (vers "0.1.1") (deps (list (crate-dep (name "zerocopy") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "zerocopy-derive") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "124dax16yrl2qvxd3fhqqdkbxqfnhpcsvfaa4kpccflv7kyyzqls")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.0.0") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1yqcyv38w3rlbjx74mi5bn06msjxllxzl3zfhyrpsxy4dybr66ri")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.0.1") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1jvy5d6djp8v4l1sihfk5v580rbanwp1xviw0g2i2x811jm8yh80")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.0.2") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1xmh6isk0zhkq4q31h51ndpz1jmawrgdmp5pk57hjflw5rgzd90s")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.1.0") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "006lp11qg3133gp1va9q00sc3d1zmzcyhkif6c4c4pd0av6ij2yj")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.1.1") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "0n6j1drjrpnjhk64p56s5rhh4n6rjrs73g5x86qi6v7aywzv8xb7")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.1.2") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "0y5mcx8d8rdm059cx94zfjy2xid1i3kxc3jjhb3sf445r84197dc")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.1.3") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "0066kp67cg8gz6q60jiz4l22dbjl26d9zqfh3nr57p9rv65ym8qa")))

(define-public crate-pow2game-1 (crate (name "pow2game") (vers "1.1.4") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "0pljzdblyqsp5215ik949py1dgsdpi8rn64d8pgcraxwp1jdm5kx")))

