(define-module (crates-io po wr) #:use-module (crates-io))

(define-public crate-powr-0.3 (crate (name "powr") (vers "0.3.0") (hash "1889cbxa7ynjd8lwc2grlj0awpbwfh1sdzd3ya63s2dird4man8f")))

(define-public crate-powr-parser-0.0.1 (crate (name "powr-parser") (vers "0.0.1") (deps (list (crate-dep (name "pest") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0k1m2wcvyx0lx4nqhnv1nrqxxyxhnxzl4j1rk9x3fsdj0iinfpzj")))

(define-public crate-powr-tokenizer-0.3 (crate (name "powr-tokenizer") (vers "0.3.0") (hash "0zk6m4w23csr7aimckzs5z5a3kn7sa23x8591xvz8w5rl18a4dbm")))

