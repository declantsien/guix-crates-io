(define-module (crates-io lh li) #:use-module (crates-io))

(define-public crate-lhlist-0.1 (crate (name "lhlist") (vers "0.1.0") (deps (list (crate-dep (name "label_attribute") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0323iib271g5x5i54y82pyhrn7gi6v1sgx1lgc9l9428nfvl0693")))

(define-public crate-lhlist-0.1 (crate (name "lhlist") (vers "0.1.1") (deps (list (crate-dep (name "label_attribute") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "00dhh8n65vcjw7kpwjx6zr1q2fsa7h2ycmjxzz0wh1n232148wg8")))

