(define-module (crates-io lh as) #:use-module (crates-io))

(define-public crate-lhash-0.1 (crate (name "lhash") (vers "0.1.0") (hash "0gq18x5k9rl5l4n48hqyssgk26zidmv0q6bxm7077m0q4lgbm4mi") (features (quote (("sha1") ("md5")))) (yanked #t)))

(define-public crate-lhash-0.2 (crate (name "lhash") (vers "0.2.0") (hash "0dixwgrx1fm4kwp0wipilk4hfznp669cffkn6pbiinr5gg2pbk6g") (features (quote (("sha512") ("sha256") ("sha1") ("md5")))) (yanked #t)))

(define-public crate-lhash-0.3 (crate (name "lhash") (vers "0.3.0") (hash "0r08prkc75gimf42ccs2h2fpc2l8rx7vmn4qdn3jssf4xiy5w3c9") (features (quote (("sha512") ("sha256") ("sha1") ("md5")))) (yanked #t)))

(define-public crate-lhash-1 (crate (name "lhash") (vers "1.0.0") (hash "12vx06vyafrars8v1cc73jv5avmx1y9sil0z437inqrjcx0mxq0w") (features (quote (("sha512") ("sha256") ("sha1") ("md5"))))))

(define-public crate-lhash-1 (crate (name "lhash") (vers "1.0.1") (hash "1bipv0kh3qwgam0jg8nyxf02v812pd6bwl9wsafq90rzpn24rw3d") (features (quote (("sha512") ("sha256") ("sha1") ("md5"))))))

(define-public crate-lhash-1 (crate (name "lhash") (vers "1.1.0") (hash "1j8d7ysr60r6nqmlar2d34q3piqa0i85ybhx4k19hbjh3y44qjkl") (features (quote (("sha512") ("sha256") ("sha1") ("md5"))))))

