(define-module (crates-io st mp) #:use-module (crates-io))

(define-public crate-stmpe1600-0.1 (crate (name "stmpe1600") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iicg5b3ksci620j6cdq51lf2iqijb4bq8b5nyvn13j99ahc72p3") (yanked #t)))

(define-public crate-stmpe1600-0.2 (crate (name "stmpe1600") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "08l3w7bwhv63yzbxxg526c4zvyd474vvk6nzlna31ln2ymw1h5y4")))

(define-public crate-stmpe1600-1 (crate (name "stmpe1600") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k2mlli315nln0d9jawbj3mv3yiyg6ij60w4crnd4j93g4q0z4x2")))

(define-public crate-stmpe1600-1 (crate (name "stmpe1600") (vers "1.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "03crly04avghfsi8li7ww1jkz52kbzx3izn0wfjj54kqasbicr2i")))

(define-public crate-stmpe1600-1 (crate (name "stmpe1600") (vers "1.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "06kw48f20q1klqp0kym4hv5yc03gn9pl6nn74l3cs9iblzj7phkx")))

(define-public crate-stmpe1600-1 (crate (name "stmpe1600") (vers "1.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "1zykp5an84mrnm24ig5s927ywzlli9m1l9nvs47hicjmgab2y0rh")))

(define-public crate-stmpe1600-1 (crate (name "stmpe1600") (vers "1.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "1cvl2pw8iry8i43dwsv6hcbrqarjmq59wagz2mlfb4imm3n9kgvi")))

(define-public crate-stmpe1600-2 (crate (name "stmpe1600") (vers "2.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "1pgvw4q2xv0ww3i4crcf9hsnivizxqkm6hmva18fpknas42jv3l2")))

