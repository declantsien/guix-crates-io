(define-module (crates-io st rm) #:use-module (crates-io))

(define-public crate-strm-privacy-driver-0.1 (crate (name "strm-privacy-driver") (vers "0.1.0") (deps (list (crate-dep (name "avro-rs") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1kjjlnnwpb0s9qpal61374xdzbahc95s5lcg6ls95ckxbdilpkhc")))

(define-public crate-strm-privacy-driver-0.1 (crate (name "strm-privacy-driver") (vers "0.1.1") (deps (list (crate-dep (name "avro-rs") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "160qc5rr2bds26j2qxbzpk5bs0r45q8xvz9mds87mh0ssmza6v2n")))

(define-public crate-strm-privacy-driver-0.2 (crate (name "strm-privacy-driver") (vers "0.2.0") (deps (list (crate-dep (name "avro-rs") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "104np1xs3i8pi2dcin5zrymh39cqsjvc3a88zxmh358nf5sp01wp")))

(define-public crate-strm-privacy-driver-1 (crate (name "strm-privacy-driver") (vers "1.0.1") (deps (list (crate-dep (name "avro-rs") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1qxv5waqaplw0wp8xvq6dgwlh5zxlri1lqj5nnpik2cw84vzhh0x")))

(define-public crate-strmap-1 (crate (name "strmap") (vers "1.0.0") (deps (list (crate-dep (name "fst") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18ciiwvrfrd8jxs8g23wynwhg7r8wp72br32h7kg0cn77wmzq9ws")))

(define-public crate-strmatch-0.1 (crate (name "strmatch") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)))) (hash "0rrjcynzjwnn7fj7ca5880n70jnv08cvyhlwvq0rwkypgrnpqv4c")))

(define-public crate-strmatch-0.1 (crate (name "strmatch") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)))) (hash "1mbpk2rjrkqvy28f0y3mnx23l6q0d2r666nm6p0fxz8swwyfhmdp")))

(define-public crate-strmath-0.1 (crate (name "strmath") (vers "0.1.0") (hash "1v6fjdk3yml4qg9zbyk9n6239f3s1i6fh44135795di92n46l6nk") (yanked #t)))

(define-public crate-strmath-0.1 (crate (name "strmath") (vers "0.1.1") (hash "0zq5afq802gkd82r7hh8wz1cwzm67y07crf42c77myi5lw73n53m") (yanked #t)))

(define-public crate-strmath-0.1 (crate (name "strmath") (vers "0.1.2") (hash "134ghhr65vszn3m4zdddzb92i5djim9b8h7gwgcjb8bj6m4m57wh") (yanked #t)))

(define-public crate-strmath-0.2 (crate (name "strmath") (vers "0.2.1") (hash "07kp9q896kpszkx1yrcrcj0mv8p23fjxs29ki4sjmwafkc14098d") (yanked #t)))

(define-public crate-strmath-0.3 (crate (name "strmath") (vers "0.3.0") (hash "12njlw0wrgj7qcin1y877mc53gr9pd1a2y7fzcfm7vdwi8vb7jy7") (yanked #t)))

(define-public crate-strmath-0.3 (crate (name "strmath") (vers "0.3.1") (hash "0jxibcjl1r5kpqrm7wxckyrwfy22v1agmfqdqv3ma2hjbrw8g78v")))

(define-public crate-strmath-0.4 (crate (name "strmath") (vers "0.4.0") (hash "1jqzi1srma8mp3ka21hqp42kixq0s4mh08xr3brvvrr9lhsvym8j") (features (quote (("sub") ("rem") ("mul") ("full" "add" "sub" "mul" "div" "rem") ("div") ("add")))) (yanked #t)))

(define-public crate-strmath-0.4 (crate (name "strmath") (vers "0.4.1") (hash "0x4i99h95wfw2y33sxq0k8bnhs72q6nilgh1sd0dqrypn3dvmg0a") (features (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add")))) (yanked #t)))

(define-public crate-strmath-0.4 (crate (name "strmath") (vers "0.4.2") (hash "0c13a2f4i9x0wdkkia3xqjg865zxcybq9ggzvdr4bx6gj805qknr") (features (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add")))) (yanked #t)))

(define-public crate-strmath-0.4 (crate (name "strmath") (vers "0.4.4") (hash "1jf1szgb8jvv3kyafnbnpj4q00xdfmj78c1l9zrfix72201s54r7") (features (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add")))) (yanked #t)))

(define-public crate-strmath-0.4 (crate (name "strmath") (vers "0.4.5") (hash "1ik1q5z321gdghpn1c1ax11m82l4kk171a60ppxmzagnncxbsigw") (features (quote (("sub") ("rem") ("mul") ("math" "add" "sub" "mul" "div" "rem") ("full" "math" "display") ("div") ("display") ("add"))))))

(define-public crate-strmline-0.0.1 (crate (name "strmline") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (kind 2)))) (hash "1dcq4pi7cirvh4sr3j7y0vdsj5zizv6wmajnjrbf693vpqzwybjd") (yanked #t)))

(define-public crate-strmline-0.0.2 (crate (name "strmline") (vers "0.0.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (kind 2)))) (hash "064y4yfjmxz94965v9dqc1kc1m6bsb4q46csviba67661zvc2fxn") (yanked #t)))

(define-public crate-strmode-1 (crate (name "strmode") (vers "1.0.0") (hash "18xa287jk7mr8pq2c5pz10rc6pr910hw0hp90rnc9bdwws4dxf87")))

(define-public crate-strmprivacy_schema_strmprivacy_demo-1 (crate (name "strmprivacy_schema_strmprivacy_demo") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "strm-privacy-driver") (req "^0.1") (default-features #t) (kind 0)))) (hash "0m9ijc28l26kl6f6z6dbxn5i00fmm2dk87hfczjvwi0bysx2j0vc")))

(define-public crate-strmprivacy_schema_strmprivacy_demo-1 (crate (name "strmprivacy_schema_strmprivacy_demo") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "strm-privacy-driver") (req "^0.1") (default-features #t) (kind 0)))) (hash "095rh6hmkikhnsdf7av9vw02jxxchf1blndqpkzva8c9bsfcs02c")))

