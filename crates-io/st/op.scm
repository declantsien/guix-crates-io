(define-module (crates-io st op) #:use-module (crates-io))

(define-public crate-stop-0.1 (crate (name "stop") (vers "0.1.0") (hash "10agasl2cxl2fg1c99clwbgz0jmsy85gmk0rsz4brcvnndwqf8i2")))

(define-public crate-stop-handle-0.1 (crate (name "stop-handle") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "rt-threaded" "time"))) (default-features #t) (kind 2)))) (hash "1gibk7s77l2nlxpr35p59zryhib5w86ydcl3kixxqg9nyxpyli43")))

(define-public crate-stop-thread-0.1 (crate (name "stop-thread") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "sig") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "15ncbx19x2b5gvjkmw7i9cw2cl50qvkiyngyk186y0d483hn74a5")))

(define-public crate-stop-thread-0.1 (crate (name "stop-thread") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1hz0lipckwdib8njz76qv956s6q41g9m102cm6n4wi4hrk2c8qbk")))

(define-public crate-stop-thread-0.1 (crate (name "stop-thread") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ynh0az486azmcirfdzsnv612p9fmf3n0kdjyfld5cg54lmhsxbw")))

(define-public crate-stop-thread-0.1 (crate (name "stop-thread") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1q5w1ay0acviz61vx1789fvd2h21nbp4nkmqfr2g1npj47l3hnhb")))

(define-public crate-stop-thread-0.1 (crate (name "stop-thread") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0h0plkqnxw9bpx9475b2v1yns3mhyq95a9pvj4z55dcn2ls4jj9z")))

(define-public crate-stop-thread-0.1 (crate (name "stop-thread") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1z9ad7lz39abxz3iq513p51lvf49khd68zh8dixvcc1bcic7qxin")))

(define-public crate-stop-thread-0.2 (crate (name "stop-thread") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1cpxmjpw286liwpwrs0wbmy471wfmhaj9bnbsqmcs0dqmk4da8sc")))

(define-public crate-stop-token-0.1 (crate (name "stop-token") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05yykam1qw0fzk59il9cqzr1wn3bnak8x37iw74ssq9ff494zssa") (features (quote (("unstable" "async-std/unstable"))))))

(define-public crate-stop-token-0.1 (crate (name "stop-token") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08413r346c2r59r914qflvbvkixy5zcb2klmfl9pw0m00v8sdzng") (features (quote (("unstable" "async-std/unstable"))))))

(define-public crate-stop-token-0.1 (crate (name "stop-token") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pdrks6p97p8knkxvcdkzdcbcfx4r3gq4kbwlnryjfsdr6vmz186") (features (quote (("unstable" "async-std/unstable"))))))

(define-public crate-stop-token-0.2 (crate (name "stop-token") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0fkkr99wzxgp1cmq995lcr5h6xsc3784s7rxwccbzndx4ndhypid")))

(define-public crate-stop-token-0.3 (crate (name "stop-token") (vers "0.3.0") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "1fsdj8yi3f5d595cdfh8f2kc6vvk6yazlsnh7kav9bqk8hfnavsn") (features (quote (("docs" "async-io"))))))

(define-public crate-stop-token-0.3 (crate (name "stop-token") (vers "0.3.1") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "1x6wnlxn471rs58fyzs79sy8rhjy2dsv14rl72cl80kv1457f857") (features (quote (("docs" "async-io"))))))

(define-public crate-stop-token-0.3 (crate (name "stop-token") (vers "0.3.2") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0wi9j0mw45wqxnl3nm4q2gh4n4qq2j2vb46axwmgp71l86xfgghp") (features (quote (("docs" "async-io"))))))

(define-public crate-stop-token-0.3 (crate (name "stop-token") (vers "0.3.3") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "1s359qc5r3inp5jdqnh54ia0bpmdm21pi1bzf7mvcw3l9ijsszm4") (features (quote (("docs" "async-io"))))))

(define-public crate-stop-token-0.3 (crate (name "stop-token") (vers "0.3.4") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0chig5b8ny98nwbvbrdpzflmzj3vg4gpyz9xbhd71c307lk6pqaq") (features (quote (("docs" "async-io"))))))

(define-public crate-stop-token-0.4 (crate (name "stop-token") (vers "0.4.0") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "1wzr3j43rkifnw2l2dm6z84d5i6nbvp0mfb9rancbng9gm44q3f8") (features (quote (("docs" "async-io"))))))

(define-public crate-stop-token-0.4 (crate (name "stop-token") (vers "0.4.1") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "10p2pfid9pyw4dy9g176qs7jc4c8i8ah9ncs1a02rgknyamn9zs9") (features (quote (("docs" "async-io"))))))

(define-public crate-stop-token-0.5 (crate (name "stop-token") (vers "0.5.0") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "07jk3jsfd32wyz2rx94nd2416skk6mxkz364jdhjx99yhhzv165s") (features (quote (("docs" "async-io") ("all" "tokio" "async-io"))))))

(define-public crate-stop-token-0.5 (crate (name "stop-token") (vers "0.5.1") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0qg4r2v2wgr965iz4r63fscdrrbjzxckv578h3y1md2n2n1fq7jn") (features (quote (("docs" "async-io") ("all" "tokio" "async-io"))))))

(define-public crate-stop-token-0.6 (crate (name "stop-token") (vers "0.6.0") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-global-executor") (req "^2.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0bpz961a0wdpx9a10f60kvsfs0f4v26wlhdw5g16rvzwx8yn2aki") (features (quote (("docs" "async-io") ("all" "tokio" "async-io" "async-std"))))))

(define-public crate-stop-token-0.6 (crate (name "stop-token") (vers "0.6.1") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-global-executor") (req "^2.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0629f2lp4lhwa3gn2rx5kdh5yf4s2crqxfkj225fy69fckyg0a9q") (features (quote (("docs" "async-io") ("all" "tokio" "async-io" "async-std"))))))

(define-public crate-stop-token-0.7 (crate (name "stop-token") (vers "0.7.0") (deps (list (crate-dep (name "async-channel") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-global-executor") (req "^2.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "12vcmmm368qw38gyygwxpma5g2nh2kygsns3z3cv56l9xs0g94dg") (features (quote (("docs" "async-io") ("all" "tokio" "async-io" "async-std"))))))

(define-public crate-stop-words-0.1 (crate (name "stop-words") (vers "0.1.0") (hash "17h9mwpm9d1xn09s705rcpapwgjzc259hh30z3a2gzm3132i4a9z")))

(define-public crate-stop-words-0.1 (crate (name "stop-words") (vers "0.1.1") (hash "007sll947aym0804dydgz9qv76b8xnyhqg0rkrdjjlp9i74l5x3l")))

(define-public crate-stop-words-0.1 (crate (name "stop-words") (vers "0.1.2") (hash "1hvd46ihfmn5q01iglsz42ix7dgmakwkmjr34d5gz3rh8mb5zryk")))

(define-public crate-stop-words-0.1 (crate (name "stop-words") (vers "0.1.3") (hash "10cnrx1pi00w6cy65p70m95v8llc3b1vnkhgnalq412xv4n2d01x")))

(define-public crate-stop-words-0.1 (crate (name "stop-words") (vers "0.1.4") (hash "11zg8q8pycd8nxk864506a5m6hra7kmraihjz35z8nfrrjrsy6l5")))

(define-public crate-stop-words-0.1 (crate (name "stop-words") (vers "0.1.5") (hash "12hmcplsh87z22raxn37zr5yal4ybqai2acn73hkxsf8rrm7rgrp")))

(define-public crate-stop-words-0.2 (crate (name "stop-words") (vers "0.2.0") (hash "1v4l6kfd3l9lsmvv2j8d6msw71mlcg2jbqm51kps24x92fd7m5yq")))

(define-public crate-stop-words-0.2 (crate (name "stop-words") (vers "0.2.1") (hash "0l09g11cxmc3iww5fvq738kcnkblzkqjw5lj2ia48wgkngkjiwv5")))

(define-public crate-stop-words-0.2 (crate (name "stop-words") (vers "0.2.2") (hash "02y0r9jj3ixffnhqxb479hjjhj5w299a73yqx2xpicvfnbcm2fxl")))

(define-public crate-stop-words-0.3 (crate (name "stop-words") (vers "0.3.0") (deps (list (crate-dep (name "strum") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qcibs3hqqfn0yplz3xd6sbdjm3vsz2abm0d29im12k5c0v5p38p") (features (quote (("enum" "strum" "strum_macros") ("default"))))))

(define-public crate-stop-words-0.3 (crate (name "stop-words") (vers "0.3.1") (deps (list (crate-dep (name "strum") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)))) (hash "19djxlhhigkdrf9fda8xgyg5ncqjd4nrv7797dnj6nlhvln2qlwh") (features (quote (("enum" "strum" "strum_macros") ("default"))))))

(define-public crate-stop-words-0.3 (crate (name "stop-words") (vers "0.3.2") (deps (list (crate-dep (name "strum") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)))) (hash "0s6i64ajvaf5by1qsd44my5nx0p684n4hpqghhw3d2603v3il436") (features (quote (("enum" "strum" "strum_macros") ("default"))))))

(define-public crate-stop-words-0.4 (crate (name "stop-words") (vers "0.4.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0v3dnjqzar5mfnkxwlv43yrc2h8v6hcxfvjiiyh3i2jd1hdpicj5") (features (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.5 (crate (name "stop-words") (vers "0.5.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.68") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1jilyyn6jixwchrw1gv3kphc8c4r4y4qayqpbh5hn8q60bkkbss6") (features (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.6 (crate (name "stop-words") (vers "0.6.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.68") (optional #t) (default-features #t) (kind 0)))) (hash "1rd5454g5sk165ph1dq1p4lvglnk2qna4w40rh0mb52mfp8ky93v") (features (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.6 (crate (name "stop-words") (vers "0.6.1") (deps (list (crate-dep (name "human_regex") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.68") (optional #t) (default-features #t) (kind 0)))) (hash "1jgy3b7nlgc267vr229sbb0jzzgjd0wf9gz3370lq1rrra5w25a8") (features (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.6 (crate (name "stop-words") (vers "0.6.2") (deps (list (crate-dep (name "human_regex") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.68") (optional #t) (default-features #t) (kind 0)))) (hash "0gnciksgq1ygdvzhgjgbmmnbbglw3vnif4x0y4qpw0ccdmbwp5bs") (features (quote (("nltk") ("default" "serde_json"))))))

(define-public crate-stop-words-0.7 (crate (name "stop-words") (vers "0.7.0") (deps (list (crate-dep (name "human_regex") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)))) (hash "13fqmh1sskiwfs6p7335vn0l66a4yj511niphimylw113vzwfldj") (features (quote (("nltk") ("iso" "serde_json") ("default" "iso"))))))

(define-public crate-stop-words-0.7 (crate (name "stop-words") (vers "0.7.1") (deps (list (crate-dep (name "human_regex") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0pp1mjvd0f4pb0d4pc8n2v1wjnv9pbslyhqnpj7qz3qi8rjizirh") (features (quote (("nltk") ("iso" "serde_json") ("default" "iso"))))))

(define-public crate-stop-words-0.7 (crate (name "stop-words") (vers "0.7.2") (deps (list (crate-dep (name "human_regex") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 1)))) (hash "1rcdhfwaym48vj45rjbpm96b35qi1qkrgdq3hkdq88kmn5ahfiql") (features (quote (("nltk") ("iso") ("default" "iso") ("constructed"))))))

(define-public crate-stop-words-0.8 (crate (name "stop-words") (vers "0.8.0") (deps (list (crate-dep (name "human_regex") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 1)))) (hash "1k6x53dknivagy8nigpj1lwclkrwxmxjniwqz75jxq4xh16h4045") (features (quote (("nltk") ("iso") ("default" "iso") ("constructed"))))))

(define-public crate-stoplight-0.1 (crate (name "stoplight") (vers "0.1.0") (hash "1jwxy9w3ykq7wbd51jsp23md2c6yzhcx1wml80djmyrnyf79kygj")))

(define-public crate-stoplight-0.2 (crate (name "stoplight") (vers "0.2.0") (hash "1il79rpmrvcvqm9rr46ar835i98hcqs11hwppcwxnsiriv3jwnqa")))

(define-public crate-stoplight-0.2 (crate (name "stoplight") (vers "0.2.1") (hash "1ssnq7x5wi3ndf61aaij7im9yxhsaqdhkpv1yh6fwdw5f77wyx5v")))

(define-public crate-stoplight-0.2 (crate (name "stoplight") (vers "0.2.2") (hash "1bx2l4c638g5y73zmzgcnkgbhf8cv53ja5dhkz6zs3gcaw5by9kj")))

(define-public crate-stoplight-0.3 (crate (name "stoplight") (vers "0.3.3") (hash "15frngy0xhrmnai3ncwpvjbdkx5i8nkww5mkkri8fsc6ihlya3b7")))

(define-public crate-stoppable_listener-0.1 (crate (name "stoppable_listener") (vers "0.1.0") (deps (list (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0nvh4pl9ms3aanz4z5cmj1rka92dw59hz6rjbcn3qyv5l2yb4s89")))

(define-public crate-stoppable_thread-0.1 (crate (name "stoppable_thread") (vers "0.1.0") (hash "1sdvwyw4lp1f9yw2wv2chl1w1zw1n6zjdgkphp440iqnh8ppmljz")))

(define-public crate-stoppable_thread-0.1 (crate (name "stoppable_thread") (vers "0.1.1") (hash "0gj315977jynaslmc3cvrd7i8ybcn655chysgaaphzh7599gjyrq")))

(define-public crate-stoppable_thread-0.1 (crate (name "stoppable_thread") (vers "0.1.2") (hash "1amvqcf0hz0z28cy0wg71ihiqv3izh6i93r7sdq4zp2hgb9dg69c")))

(define-public crate-stoppable_thread-0.1 (crate (name "stoppable_thread") (vers "0.1.3") (hash "1i9pqyi06xfbmg1v1f7iwk6yc2d07xzh83z9clv8672jj3nw75ng")))

(define-public crate-stoppable_thread-0.1 (crate (name "stoppable_thread") (vers "0.1.4") (hash "1gkafw5051khgzlzwyw6riwpgnrlvjqxrwl6g9r0i3zxamscf9w2")))

(define-public crate-stoppable_thread-0.2 (crate (name "stoppable_thread") (vers "0.2.0") (hash "02ki346xl37kc84fff76xcwzr60r5al8mkcimcgccr3s2dxhviz7")))

(define-public crate-stoppable_thread-0.2 (crate (name "stoppable_thread") (vers "0.2.1") (hash "17q1cnmlr48vrr0hd0pfmmnl6mj89ak3404mzwgrralkscg0qw76")))

(define-public crate-stopper-0.1 (crate (name "stopper") (vers "0.1.0") (deps (list (crate-dep (name "futures-lite") (req "^1.11.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "waker-set") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1svxxsrsi2v4lyp2ym7dar0j8pcl0qjq0sc22mnkrj5njxcc7ibw")))

(define-public crate-stopper-0.1 (crate (name "stopper") (vers "0.1.1") (deps (list (crate-dep (name "futures-lite") (req "^1.11.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "waker-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rx5c88ca11gpmwwgk8qnbn1n5ld0kws6g99dm5w4pyigm8fdj9b")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.0") (deps (list (crate-dep (name "async-io") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "futures-lite") (req "^1.11.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "waker-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wq0pm2843xj1dg2nz3ahvl7727wr78m2waww56r6mp3aqjbyws5")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.1") (deps (list (crate-dep (name "async-io") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "1irkc18g6wknjn4giaig30zgrj44cvf5wiwcym0sif21xx2dmvln")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.2") (deps (list (crate-dep (name "async-io") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "1yv6nhxwkd2zpljrrr7cl4cvdj0bx5zrssv0zzvlzah9d54smjy3")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.3") (deps (list (crate-dep (name "async-io") (req "^2.2.0") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "1rnr2x2148356xqvzkn4ybl5k5rb57qx4jf95ixs43h28xmcy3l1")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.4") (deps (list (crate-dep (name "async-io") (req "^2.2.0") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "1lvvazij475ismz5d8d1vpppcxkcjcjs45yzfvxssw1xmd9gz2zd")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.5") (deps (list (crate-dep (name "event-listener") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "test-harness") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1va2sm03ic42knn6b7wd59v0hrkpy52rm5x4ly52abg3bzf3dyj8")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.6") (deps (list (crate-dep (name "event-listener") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "test-harness") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1qcxrhd5wbiqv20d1kh70dmq5899jby8wp4l5j4060vbfvbdqagb")))

(define-public crate-stopper-0.2 (crate (name "stopper") (vers "0.2.7") (deps (list (crate-dep (name "event-listener") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "test-harness") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "06bh6fksy3b0qm27vda863w00hxwwhn7hy79aqbmj7kas41jvj36")))

(define-public crate-stopwatch-0.0.2 (crate (name "stopwatch") (vers "0.0.2") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1ifywzadrn94wds87m3bvvy7h6l37lgqav9fyjkqlczxx3qkn48d")))

(define-public crate-stopwatch-0.0.3 (crate (name "stopwatch") (vers "0.0.3") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0iw92p8581vi455w3zz5r77n0yad8svp6qi0aa6fqjz0c2blgww5")))

(define-public crate-stopwatch-0.0.4 (crate (name "stopwatch") (vers "0.0.4") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0ggjn1m5zng79awbq0jsz5p724dm4wk4ry4nwc0pw0f1bn2prrnc")))

(define-public crate-stopwatch-0.0.5 (crate (name "stopwatch") (vers "0.0.5") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "169axf1dmqw25wwv1rqyg3zkryyv75avajryl724fghr8a6g86i4")))

(define-public crate-stopwatch-0.0.6 (crate (name "stopwatch") (vers "0.0.6") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0xz2bm5aicgwfvj2bdk461jiyi7a6fc3xzvj03cq38hsyvnqmskx")))

(define-public crate-stopwatch-0.0.7 (crate (name "stopwatch") (vers "0.0.7") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0a7p7gcznjpmqd6wpdfbgk6dirw34yydh6b38lx4v94dqzmva11x")))

(define-public crate-stopwatch-rs-0.1 (crate (name "stopwatch-rs") (vers "0.1.0") (deps (list (crate-dep (name "quanta") (req "^0.10") (default-features #t) (kind 0)))) (hash "1wxpgvc73ik1icfcxp5qf9rcd53jar6j87rsy9awrjc69znkgkic")))

(define-public crate-stopwatch2-2 (crate (name "stopwatch2") (vers "2.0.0") (hash "1d7v0avq2s3a8kv5a0pfld4n42s673q1vq4rgx0liyla708cw7li")))

(define-public crate-stopwords-0.1 (crate (name "stopwords") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0zllny0838irrs79gqxddypa41jnvfa9nvfvq13fwg8k1r84sf4n")))

(define-public crate-stopwords-0.1 (crate (name "stopwords") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gki3g46cxld57nqw4assh6lc9wryb8jxm0jj4aymrijw6k0hi8f")))

