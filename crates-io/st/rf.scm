(define-module (crates-io st rf) #:use-module (crates-io))

(define-public crate-strfile-0.1 (crate (name "strfile") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0kcg5gz5fdqydg93a1rr9rn56xigwkll0lnkdv3h39j5jdy44y75")))

(define-public crate-strfile-0.1 (crate (name "strfile") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1s97iz1jmywjv81hqxhnf3z4ad7sw0ib98vv6mh8cq8a8cfif8yj") (yanked #t)))

(define-public crate-strfile-0.1 (crate (name "strfile") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0wkkfdbdrxrw145f13w4vrlq5v8y03bwsdsag1f157ky3nmijr5l")))

(define-public crate-strflags-0.1 (crate (name "strflags") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ecow") (req "^0.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "identconv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11") (features (quote ("serde" "const_new"))) (default-features #t) (kind 0)))) (hash "0yspg4592qh0cyvrc9hkbipvh34hb657k9n1x0ki4z3xamk58n2c") (features (quote (("debug" "levenshtein" "log"))))))

(define-public crate-strflags-0.1 (crate (name "strflags") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ecow") (req "^0.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "identconv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11") (features (quote ("serde" "const_new"))) (default-features #t) (kind 0)))) (hash "0085mq1bbm76id3lxhndmhvxg6w0vhnpyqhn8frsdq25lkp0rki4") (features (quote (("debug" "levenshtein" "log"))))))

(define-public crate-strflags-0.2 (crate (name "strflags") (vers "0.2.0") (deps (list (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ecow") (req "^0.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "identconv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11") (features (quote ("serde" "const_new"))) (default-features #t) (kind 0)))) (hash "0brivjgv12f3q9ig9ycx0vq07m6r2zdxfgyqnh7pylcn07r16rfx") (features (quote (("debug" "levenshtein" "log")))) (yanked #t)))

(define-public crate-strflags-0.2 (crate (name "strflags") (vers "0.2.1") (deps (list (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ecow") (req "^0.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "identconv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11") (features (quote ("serde" "const_new"))) (default-features #t) (kind 0)))) (hash "1a2apni3xal15izjp45irl97ab0pk5v1rcbmm7l882rbf5kzls82") (features (quote (("debug" "levenshtein" "log"))))))

(define-public crate-strflags-0.2 (crate (name "strflags") (vers "0.2.2") (deps (list (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ecow") (req "^0.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "identconv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11") (features (quote ("serde" "const_new"))) (default-features #t) (kind 0)))) (hash "0q4z784l8ngr2mclff4aqn3sgxlpk57ws78kk95wda9w6j47w449") (features (quote (("debug" "levenshtein" "log"))))))

(define-public crate-strflags-0.3 (crate (name "strflags") (vers "0.3.0") (deps (list (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ecow") (req "^0.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "identconv") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11") (features (quote ("serde" "const_new"))) (default-features #t) (kind 0)))) (hash "1fivixc95xnwd2nk7qyr03vm9ix9hrjd5d8hy7m45349cwm4bbbi") (features (quote (("debug" "levenshtein" "log"))))))

(define-public crate-strflags-0.3 (crate (name "strflags") (vers "0.3.1") (deps (list (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ecow") (req "^0.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "identconv") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11") (features (quote ("serde" "const_new"))) (default-features #t) (kind 0)))) (hash "1kzqj6jbqjh7b69ybbz56dlzra69v5chslma065srd8nx8zf2j20") (features (quote (("debug" "levenshtein" "log"))))))

(define-public crate-strfmt-0.1 (crate (name "strfmt") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j4fvbir4pnpf7qzcwmnf1k4dmc5sx6i0igzrpsmbybjlaf6120h")))

(define-public crate-strfmt-0.1 (crate (name "strfmt") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "19890a202kifyw0dcjiffpabshm1chmyw5phjav43f1pwq29hrcy")))

(define-public crate-strfmt-0.1 (crate (name "strfmt") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "1589mjbm7mv85jpdqdcdbx5r27mm6syba95l0zj4gx92qhql4z8n")))

(define-public crate-strfmt-0.1 (crate (name "strfmt") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "15a51q1759fqnnddd2yz6lkjz1ahkkhlg02cy1p9wcm6ki5bi029")))

(define-public crate-strfmt-0.1 (crate (name "strfmt") (vers "0.1.4") (hash "13i9p1xv96szdprwwafrpq0r69001vb8kjjkwdzkhp04y8qlksiz")))

(define-public crate-strfmt-0.1 (crate (name "strfmt") (vers "0.1.5") (hash "085jgpdvddybv2g2380p1288fk4y4xq73fm3743hikjzf389gjn6")))

(define-public crate-strfmt-0.1 (crate (name "strfmt") (vers "0.1.6") (hash "03x34w92aqyymncxdxp136ci0fmcdh6dslkz4wmqb9bsxx2b4y5j")))

(define-public crate-strfmt-0.2 (crate (name "strfmt") (vers "0.2.0") (hash "1bdk15ccapm7rd3qxly72pizzdj25zf8pbh4543yrzdgzj202ni0")))

(define-public crate-strfmt-0.2 (crate (name "strfmt") (vers "0.2.1") (hash "1hhkwgrzqwcs2wj7dca5rpsdfrwd55h1djm8rmj1bgwrflkdhxlh")))

(define-public crate-strfmt-0.2 (crate (name "strfmt") (vers "0.2.2") (hash "13w2likqqw9jslbmwchh7sz8prw3xhbyjq0imk1fi9vdmg6spk96")))

(define-public crate-strfmt-0.2 (crate (name "strfmt") (vers "0.2.3") (hash "1d0z1whm8m8ri52c542pan86gx35wz9l2c7ag5slq777wmcy2gdp")))

(define-public crate-strfmt-0.2 (crate (name "strfmt") (vers "0.2.4") (hash "0raysrg8giw52nwbb9334d5zamifvgcdkf1khy62bhwz5npli0vs")))

(define-public crate-strfn-0.1 (crate (name "strfn") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "siesta") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0ycqfgaipmb6c7pzq9sfh9ibvrymwff7g1723x22slg43bvwna0a")))

(define-public crate-strfry-0.0.0 (crate (name "strfry") (vers "0.0.0") (hash "0r0g5sgr9grqs8qrrdvd8gxxmnbqvafgmcdfr854k6fyi2sbdqr1")))

(define-public crate-strfry-0.1 (crate (name "strfry") (vers "0.1.0") (hash "0p571dysgmqwqfwffqr3wm2x1v5xyjmmyjapn2ivcrd2wn8qmy7m")))

(define-public crate-strftime-ruby-1 (crate (name "strftime-ruby") (vers "1.0.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.3") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "01f92m9kz25zvc5w1rjym6d9f3csiwvi07g0l7f7a12q151x40kb") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.58.0")))

(define-public crate-strftime-ruby-1 (crate (name "strftime-ruby") (vers "1.0.1") (deps (list (crate-dep (name "version-sync") (req "^0.9.3") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "0y2a1dq99n95h7c11xkrq876k8iipc4ai3ns804kp306wkh1sm8y") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.58.0")))

