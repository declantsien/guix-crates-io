(define-module (crates-io st sp) #:use-module (crates-io))

(define-public crate-stspin220-0.1 (crate (name "stspin220") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-time") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0qrkccasanqnv8xiakc6lm76a7ijbxa67lylwjiq75prvlkwib7w")))

(define-public crate-stspin220-0.2 (crate (name "stspin220") (vers "0.2.1") (deps (list (crate-dep (name "step-dir") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lz0m0wmrlf5a86xic095r2v9xx59asy1hdnz12vxsflmk914p9a")))

(define-public crate-stspin220-0.3 (crate (name "stspin220") (vers "0.3.0") (deps (list (crate-dep (name "step-dir") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0wrdlbiapsg5zpryg8r6aymia9c3283j922s169szy4m7f668w0c")))

(define-public crate-stspin220-0.4 (crate (name "stspin220") (vers "0.4.0") (deps (list (crate-dep (name "step-dir") (req "^0.4.0") (features (quote ("stspin220"))) (kind 0)))) (hash "00p5nksn0sw6mds17fs6a7l418xzzs9ip4k53i9p5ziskhkd6xbg")))

(define-public crate-stspin220-0.4 (crate (name "stspin220") (vers "0.4.1") (deps (list (crate-dep (name "step-dir") (req "^0.4.1") (features (quote ("stspin220"))) (kind 0)))) (hash "186kmmkhhhlqjsm53pvz4j7x0ivk67x8pnfw59yfj42b2m3gjr6z")))

(define-public crate-stspin220-0.5 (crate (name "stspin220") (vers "0.5.0") (deps (list (crate-dep (name "stepper") (req "^0.5.0") (features (quote ("stspin220"))) (kind 0)))) (hash "124a0i1w98mfwr5hh5065p0dyjf01d757qlrskbrk1skb3xlgiv4")))

(define-public crate-stspin220-0.6 (crate (name "stspin220") (vers "0.6.0") (deps (list (crate-dep (name "stepper") (req "^0.6.0") (features (quote ("stspin220"))) (kind 0)))) (hash "0nmp028lp892963y8kc3pvhghh8aww7ds1ps048y7s3g3009p42m")))

