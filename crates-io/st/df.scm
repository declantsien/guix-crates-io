(define-module (crates-io st df) #:use-module (crates-io))

(define-public crate-stdf-0.1 (crate (name "stdf") (vers "0.1.0") (deps (list (crate-dep (name "byte") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stdf-record-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02z3s903y6yz09ajvl98jrq00yjvigy53crk8canilccrw8whc83")))

(define-public crate-stdf-0.1 (crate (name "stdf") (vers "0.1.1") (deps (list (crate-dep (name "byte") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "stdf-record-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "06hb4hmww68ic0liw00akl0x5v9z1mpm4752d4fhm62gz5ndakpv")))

(define-public crate-stdf-0.1 (crate (name "stdf") (vers "0.1.2") (deps (list (crate-dep (name "byte") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "stdf-record-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0asg66arn2gllmqz4p9adkd8x9ygzy7z8lfa6as5fkk12w725a9r")))

(define-public crate-stdf-0.2 (crate (name "stdf") (vers "0.2.0") (deps (list (crate-dep (name "byte") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "stdf-record-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1aiw8cpj1bj32pk153i6nk7y3ddwv4kbb6qf6gdxmjyp2y1fqiag")))

(define-public crate-stdf-record-derive-0.1 (crate (name "stdf-record-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.21") (default-features #t) (kind 0)))) (hash "09p7r1wfqsm7k9ml3gmg8xmh7j131dgfk9jhj9c4dlh6mv1k4r5d")))

(define-public crate-stdf-record-derive-0.1 (crate (name "stdf-record-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.21") (default-features #t) (kind 0)))) (hash "0vwwcsn0s7p6ryxyp3x64arsin52ivsavp4yl5sjy86w9kp8r8vm")))

(define-public crate-stdf-record-derive-0.2 (crate (name "stdf-record-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("printing"))) (default-features #t) (kind 0)))) (hash "06j7libkp20fq1wr654irhxdfn8izncykz9ywsdx67lqx6mhsggj")))

