(define-module (crates-io st im) #:use-module (crates-io))

(define-public crate-stimmgabel-0.1 (crate (name "stimmgabel") (vers "0.1.0") (hash "1iiysz9zlfp75w1250sxxkvhr096nr7yh3r6m37vwsiv9cp5sdd1")))

(define-public crate-stimmgabel-1 (crate (name "stimmgabel") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1") (features (quote ("rand_core"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "polyproto") (req "^0.9.0-alpha.3") (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36") (features (quote ("rt-multi-thread" "macros" "net"))) (default-features #t) (kind 0)))) (hash "00lzbh31dqjdnw59h4aga3chaxyqm0p0hlygb126l8l98srfh02c")))

