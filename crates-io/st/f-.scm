(define-module (crates-io st f-) #:use-module (crates-io))

(define-public crate-stf-macro-0.1 (crate (name "stf-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.102") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02ac337m83zingc0sy84fvvlzyarcbs667rizhlhfv15pr0xhcgf")))

(define-public crate-stf-runner-0.1 (crate (name "stf-runner") (vers "0.1.0") (hash "0lh5kwf4xc4zn5mrkalag34kqf4r3dn27lx3n5ys06ij7n993i1n")))

