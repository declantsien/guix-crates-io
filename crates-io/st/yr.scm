(define-module (crates-io st yr) #:use-module (crates-io))

(define-public crate-styrus-0.1 (crate (name "styrus") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0v3s9bmnbd087li8hxayfp1rmsf9gdpkr032kgmc8zynlmqbc74s")))

(define-public crate-styrus-0.1 (crate (name "styrus") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0h2vxllary8ppscvva6a5qv0g2rsglclmcjknx016fz0qs63rmvb")))

(define-public crate-styrus-0.1 (crate (name "styrus") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "02xwd92f2b53vn3p4nb6vkwljqw3wfp2s0ipymbl1gm4cmf4zsmc")))

