(define-module (crates-io st l-) #:use-module (crates-io))

(define-public crate-stl-bin-parser-0.0.0 (crate (name "stl-bin-parser") (vers "0.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0jcw3fh8rkx7faba4g33549bk8gwc6k34n7qf6m07vxm3rv1jy3s")))

(define-public crate-stl-bin-parser-0.0.1 (crate (name "stl-bin-parser") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "095yj7npvbx1mjkjyqgfnwm75jglzhdmj40f7sdbnvl6nkm84739")))

(define-public crate-stl-rs-0.1 (crate (name "stl-rs") (vers "0.1.0") (hash "08vd1qp93cij8sl1kizjqzqq4qixa8l5bva82sg2np2lb4wl0jsc")))

(define-public crate-stl-thumb-0.5 (crate (name "stl-thumb") (vers "0.5.0") (deps (list (crate-dep (name "cbindgen") (req "0.23.*") (default-features #t) (kind 1)) (crate-dep (name "cgmath") (req "^0.18.0") (features (quote ("mint"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "glium") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "stl_io") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0p4iq03crl3y72a0kccw9vh3p8ywhznkswc2269jws6d8i3ld53q")))

