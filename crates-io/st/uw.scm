(define-module (crates-io st uw) #:use-module (crates-io))

(define-public crate-stuw81300-0.1 (crate (name "stuw81300") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "micromath") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1jfcbar7nqvh8fdfngmc1w7yi3b7sfpss5n6xpsc9i5y5c4h9q8j")))

(define-public crate-stuw81300-0.2 (crate (name "stuw81300") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "micromath") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "00h3gnb8b8bi4fxyl8w41y9q1vjdjmq7ps3sd5079qs1km07wvcm")))

