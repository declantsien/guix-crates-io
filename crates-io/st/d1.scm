(define-module (crates-io st d1) #:use-module (crates-io))

(define-public crate-std1-0.1 (crate (name "std1") (vers "0.1.0") (hash "0lkv8ksq1yc0ndvy6l3qj2sx41zqsxh28312cx8gwjcrhcjfi92l") (yanked #t)))

(define-public crate-std1-0.2 (crate (name "std1") (vers "0.2.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1pammkv2vnfpm95sa5wzpfzananxwf7r98fx7y2g2bymssk1dhf4")))

(define-public crate-std140-0.1 (crate (name "std140") (vers "0.1.0") (deps (list (crate-dep (name "std140-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "111fy12xjmwr6lzcqavhkf0fsh62d3znzl12vw29r2f4kc5140br")))

(define-public crate-std140-0.1 (crate (name "std140") (vers "0.1.1") (deps (list (crate-dep (name "std140-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bmbcxy5szj1qxv7c8wkgmvm5r2q19fq48nxg8vrvyva0q1yk4m6")))

(define-public crate-std140-0.1 (crate (name "std140") (vers "0.1.2") (deps (list (crate-dep (name "std140-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wdsxvjg3qr5mhsh5r9b51dqa073zhf136ydskk8dyhnmlq7aixi")))

(define-public crate-std140-0.2 (crate (name "std140") (vers "0.2.0") (deps (list (crate-dep (name "std140-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ijiiwzkp1269h5sbk9sg9mc2h44w2cg0v5mizzwix5knvjyl9mc")))

(define-public crate-std140-0.2 (crate (name "std140") (vers "0.2.1") (deps (list (crate-dep (name "std140-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1f7vinm8yx1s4y78di9s2hs1vkhr3pk8xpim7rclq5h4dc3ydjhw")))

(define-public crate-std140-0.2 (crate (name "std140") (vers "0.2.2") (deps (list (crate-dep (name "std140-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1b9li5547qgyafaqn1rvmw4m45bn5k1gjm9wmj3kpg1lr7iijklr")))

(define-public crate-std140-0.2 (crate (name "std140") (vers "0.2.3") (deps (list (crate-dep (name "std140-macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13hxd7zhjwjyxc9kpcx1aiynp0wi3din15l9hdgvdb7jbn7fylk4")))

(define-public crate-std140-0.2 (crate (name "std140") (vers "0.2.4") (deps (list (crate-dep (name "std140-macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "127ajrjij61cci6ypd0rv5whzpvjyn647w6qmnaqgqz6y3l2h70h")))

(define-public crate-std140-0.2 (crate (name "std140") (vers "0.2.5") (deps (list (crate-dep (name "std140-macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "19brnaa58mmnkas0zkqy7qr2vgk5s0kvqsqhjhhr3vpljzi6wb6m")))

(define-public crate-std140-0.2 (crate (name "std140") (vers "0.2.6") (deps (list (crate-dep (name "std140-macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0m9mpc3kjj3qzigzjhzv7kq7qr35k6kq5zvwa1891bd5avc3qagp")))

(define-public crate-std140-macros-0.1 (crate (name "std140-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fdyizw3ymr4wcrkx493lig5xak23dzs04p4b99hz2rlxz1aldfq")))

(define-public crate-std140-macros-0.1 (crate (name "std140-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lc3c2jlbw6n5z04p7b4dkxlsych9wfh0lq9ih02pch7avv2f9mb")))

(define-public crate-std140-macros-0.1 (crate (name "std140-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07aqmvndbzawrdpm5v7ra69ndg7aaa1mkikcngarsdp8q3341ryi")))

