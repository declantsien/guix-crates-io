(define-module (crates-io st pl) #:use-module (crates-io))

(define-public crate-stpl-0.1 (crate (name "stpl") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0c3ry51z6xmcgylsg23bn3lqm05kf2w7ibwi99kr94fbjxgjz44j")))

(define-public crate-stpl-0.2 (crate (name "stpl") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "03p0gg95swcr5dq11r6xsrvsxnxflfhnmjsdbipgzwgz93gr8av2")))

(define-public crate-stpl-0.3 (crate (name "stpl") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0r2swjgdyw42bgaiqbv9bhsqcrrd5fw46895gsfxvlp3i92pcadf")))

(define-public crate-stpl-0.3 (crate (name "stpl") (vers "0.3.1") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "10wgpsq90pfj39vi5sakyzwmh6a4q0db17rs1jbmidmwhjaa9532")))

(define-public crate-stpl-0.4 (crate (name "stpl") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "19h83yl9nsgh9xc9ppak21cy8gfzkgnaaymsa0c84asc97505ivm")))

(define-public crate-stpl-0.5 (crate (name "stpl") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0n75jbz0qy7q9av0b4p49dl03ax26ha3sbylyf9bfmhn0709q7g5")))

