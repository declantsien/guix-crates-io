(define-module (crates-io st tp) #:use-module (crates-io))

(define-public crate-sttp-0.1 (crate (name "sttp") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1x80pq6sd5rbdnyndb6i57i79r80qngxc5k10m0vxd8wsjcyj4z9")))

