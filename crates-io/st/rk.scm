(define-module (crates-io st rk) #:use-module (crates-io))

(define-public crate-strkey-0.1 (crate (name "strkey") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m6n674k7i7n7q7cygx9h669w6z1pxzz3xc1j2ijisaizcs19bc4")))

