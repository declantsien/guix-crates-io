(define-module (crates-io st #{70}#) #:use-module (crates-io))

(define-public crate-st7032i-0.0.1 (crate (name "st7032i") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "12mmnk4r4pkj5iscrxvfymyr03afb2lalmrjj91m9vvphh0nmj36")))

(define-public crate-st7032i-0.0.2 (crate (name "st7032i") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "01ir016pc27x1y4fzjdp2rkq36b60i30imqskr03ybrwdca4k5dj")))

(define-public crate-st7032i-0.0.3 (crate (name "st7032i") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "03n6a63l11nf1ig1lhjsl9kakqjac3cm9r7zbbm2zqjg9f6cg2w8")))

(define-public crate-st7032i-0.0.4 (crate (name "st7032i") (vers "0.0.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0x6grbd4m3v4zc93g4d5s92v51r66cy9alwkq4d57bxjhbzqw99z")))

