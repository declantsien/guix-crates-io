(define-module (crates-io st rc) #:use-module (crates-io))

(define-public crate-strchunk-0.1 (crate (name "strchunk") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "range-split") (req "^0.1.0-beta.1") (features (quote ("bytes"))) (default-features #t) (kind 0)))) (hash "0wxb1n52a8mfzahj0ibi5jkaydpmxvy2wvs9r56ww646avpdazrg") (features (quote (("specialization"))))))

(define-public crate-strchunk-0.1 (crate (name "strchunk") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "range-split") (req "^0.1") (features (quote ("bytes"))) (default-features #t) (kind 0)))) (hash "1f02zlrl9qnbjh9fmkxaqwqpib8ad0i8j5q1lfsamvn28dwpkjg2") (features (quote (("specialization"))))))

(define-public crate-strchunk-0.2 (crate (name "strchunk") (vers "0.2.0-beta.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "range-split") (req "^0.2.0-beta.2") (features (quote ("bytes"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-std" "io-util"))) (default-features #t) (kind 2)))) (hash "0y94c8pbniswy2jpw6apvynwvih10s2mpxqhvxfdca49ajbmkyvw") (features (quote (("specialization"))))))

(define-public crate-strchunk-0.2 (crate (name "strchunk") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "range-split") (req "^0.2") (features (quote ("bytes"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-std" "io-util"))) (default-features #t) (kind 2)))) (hash "17snc3c71fxlvdwbylj79y2hzvpqs7dg354a514q45nk7hzfnq89") (features (quote (("specialization"))))))

(define-public crate-strchunk-0.3 (crate (name "strchunk") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "range-split") (req "^0.3") (features (quote ("bytes"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.3") (features (quote ("io-std" "io-util" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1r3066h9lapcpcl8g4phylgzychcfhzhpnfvr6isi9jxwyhb126p") (features (quote (("unstable" "specialization") ("specialization"))))))

(define-public crate-strchunk-0.4 (crate (name "strchunk") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "range-split") (req "^0.4") (features (quote ("bytes"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.1") (features (quote ("io-std" "io-util" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1hmlijnwr4x88yfszk5zr2cbxs0lzpwh7qmqpr1klcd58pm71n48") (features (quote (("unstable" "specialization") ("specialization"))))))

(define-public crate-strchunk-0.4 (crate (name "strchunk") (vers "0.4.1") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "range-split") (req "^0.4") (features (quote ("bytes"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.1") (features (quote ("io-std" "io-util" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0jgl06sxrmj58xrzcqjbirifvb0yfis97l1cs8bsb5pas94gjg5f") (features (quote (("unstable" "specialization") ("specialization"))))))

(define-public crate-strck-0.1 (crate (name "strck") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)) (crate-dep (name "smol_str") (req "^0.1.23") (default-features #t) (kind 2)))) (hash "0hrmwxn7i7nb8lk0assz9bwqs0jrsixgga0gp55s4hkcbhbc6v2a")))

(define-public crate-strck-0.1 (crate (name "strck") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "smol_str") (req "^0.1") (default-features #t) (kind 2)))) (hash "1balb9585hm4h3zqswi3qpnlqcxaxrxf8n45m63dxp7majq9pfa2")))

(define-public crate-strck-0.1 (crate (name "strck") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "smol_str") (req "^0.1") (default-features #t) (kind 2)))) (hash "05z981r28f3s7a29cwkb1fg7cznk89rpf8g9kyfrg3wxxl6hk4dy")))

(define-public crate-strck_ident-0.1 (crate (name "strck_ident") (vers "0.1.0") (deps (list (crate-dep (name "strck") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-ident") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pj9cvdd8mb7avvayjs9d0lqmg38rh0qx9078zcnxg6vjszvrin1") (features (quote (("serde" "strck/serde") ("rust"))))))

(define-public crate-strck_ident-0.1 (crate (name "strck_ident") (vers "0.1.1") (deps (list (crate-dep (name "strck") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-ident") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yzjiz7sman2kwka51wjnyywqqlcwlrcr9grw5b167hny4ld3wks") (features (quote (("serde" "strck/serde") ("rust"))))))

(define-public crate-strck_ident-0.1 (crate (name "strck_ident") (vers "0.1.2") (deps (list (crate-dep (name "strck") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-ident") (req "^1.0") (default-features #t) (kind 0)))) (hash "00kjyy5avywpzhkajvlk35h6w4xkl34j3wk78sj5hf4v2qmq1hz1") (features (quote (("serde" "strck/serde") ("rust"))))))

(define-public crate-strconv-0.1 (crate (name "strconv") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)))) (hash "1i7qjl8wd8b8qa93q3y48iy8zpsjyk76hiz2qhdw7hpf107z2bgk")))

(define-public crate-strconv-0.1 (crate (name "strconv") (vers "0.1.1") (deps (list (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)))) (hash "1w73qxpfkjpsy4wy06xfzahxl8saip6h0yym2fcp677dlbylzlza") (yanked #t)))

(define-public crate-strcursor-0.1 (crate (name "strcursor") (vers "0.1.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0q20awdciz3abmk6ghm6dvwki8fkyw52axwmb061h2f89jk9yhqm")))

(define-public crate-strcursor-0.2 (crate (name "strcursor") (vers "0.2.0") (deps (list (crate-dep (name "debug_unreachable") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hmhrwcdx1vcb6xka6k2fk517dvpflwcd8kgyvsyc9in5ypgaxzg")))

(define-public crate-strcursor-0.2 (crate (name "strcursor") (vers "0.2.1") (deps (list (crate-dep (name "debug_unreachable") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10dd3i4kir0i7mq477csl8j2b57x8r873i4h7nzxf6z0l045b28k")))

(define-public crate-strcursor-0.2 (crate (name "strcursor") (vers "0.2.2") (deps (list (crate-dep (name "debug_unreachable") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "unicode-segmentation") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qn50fqh3bndyfidbp7ip1vqfipd0gh86yqb2l20c1w3ras91xw2")))

(define-public crate-strcursor-0.2 (crate (name "strcursor") (vers "0.2.3") (deps (list (crate-dep (name "rustc_version") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "unicode-segmentation") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gnk9zn318xgz7zp4s2gprgcbsnvcnp567d2kzskcc0vmmq5qc38")))

(define-public crate-strcursor-0.2 (crate (name "strcursor") (vers "0.2.4") (deps (list (crate-dep (name "rustc_version") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "unicode-segmentation") (req "^0.1.0, < 0.1.3") (default-features #t) (kind 0)))) (hash "1gd2pchxrcbxh98zdfig8bim7c45smmv3bcs4isnnidy0q4413cl")))

(define-public crate-strcursor-0.2 (crate (name "strcursor") (vers "0.2.5") (deps (list (crate-dep (name "rustc_version") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "unicode-segmentation") (req "^0.1.0, < 0.1.3") (default-features #t) (kind 0)))) (hash "0nj6kfka15za5yigdn7xi149mv6v1rvxgqln2axdpl7nw4hvfg55")))

