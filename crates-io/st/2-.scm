(define-module (crates-io st #{2-}#) #:use-module (crates-io))

(define-public crate-st2-logformat-0.1 (crate (name "st2-logformat") (vers "0.1.0") (deps (list (crate-dep (name "abomonation") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "abomonation_derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "differential-dataflow") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "timely") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "00mk9ycwpff74ci853zyk272s9bdqmy260n32r7blam9jqqm7nf8")))

