(define-module (crates-io st dr) #:use-module (crates-io))

(define-public crate-stdr-0.1 (crate (name "stdr") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "mpsc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "thread") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0h8n3sik5j9wy3pr1k7mz41dydi2a35mf3fb24kgnk77dpw18k60") (yanked #t)))

(define-public crate-stdr-0.1 (crate (name "stdr") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "mpsc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "thread") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mrn02wnj3k465vwnfj44jy601gci9jjxawzsczya48r4706p9sq") (yanked #t)))

(define-public crate-stdrename-1 (crate (name "stdrename") (vers "1.0.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "0kfzs4r048vkpdcfz8cs2mx1x917r48s794rz8lvwv0xa74kbvck")))

(define-public crate-stdrename-1 (crate (name "stdrename") (vers "1.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "0i15svqxyb3shdbapf5nqqlx6mhcxl24f5lkqz2kd7f1g2rjwbsl")))

(define-public crate-stdrename-1 (crate (name "stdrename") (vers "1.2.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "0q9fzawx6dyyv0yajc2hqb27782vx6dvx3iwmz9gcfj2pkqsbwj8")))

(define-public crate-stdrename-1 (crate (name "stdrename") (vers "1.3.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "1qb7m19szw2qh9zakxhxawiajwf1c5rg948svgpymy2z7mgkkkpm")))

