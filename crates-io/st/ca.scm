(define-module (crates-io st ca) #:use-module (crates-io))

(define-public crate-stcalc-0.1 (crate (name "stcalc") (vers "0.1.0") (hash "1wmchh0hr1hn4n35rkr8wk38qw0khxcw29myi5133hwkgl89mmgs") (yanked #t)))

(define-public crate-stcalc-0.1 (crate (name "stcalc") (vers "0.1.1") (hash "07m2c8jbi803w2giw9987jn80ihv1m7hygk02p5b3bg6n1815vxx")))

(define-public crate-stcalc-0.1 (crate (name "stcalc") (vers "0.1.2") (hash "1l0aydzan1qk8arhhw4kg0cnzhkmzhk8325lksr4xfqpkmy2q4xm")))

(define-public crate-stcalc-1 (crate (name "stcalc") (vers "1.0.0") (hash "0mi9zg1sr5b9jylscb7zk57cxwwbqfap1i8wrfkwqdyjg9kb7rd2")))

(define-public crate-stcalc-1 (crate (name "stcalc") (vers "1.0.1") (hash "0kj4pn4n0m2ra7xa1qgmmdx1zw2fw8n1b6ry6qzqvpp5m5ym1zip")))

(define-public crate-stcat-0.1 (crate (name "stcat") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.24.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ml33b321kjl5byhhb2mhg5rlbdga14jbzfaw0gl6z5mpz66ww3q")))

(define-public crate-stcat-0.2 (crate (name "stcat") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.3.3") (features (quote ("max_level_trace" "release_max_level_trace"))) (default-features #t) (kind 0)) (crate-dep (name "slog-async") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sloggers") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0rpbh34bvw403yabfv6kvqvw607bpsbkwnynx2xs12agqfidq3q6") (yanked #t)))

(define-public crate-stcat-0.2 (crate (name "stcat") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.3.3") (features (quote ("max_level_trace" "release_max_level_trace"))) (default-features #t) (kind 0)) (crate-dep (name "slog-async") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sloggers") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0a71qk08z6pyfxxcjb7dbhxaxlij86yvgv7idzgabw0gn8zas8pw") (yanked #t)))

(define-public crate-stcat-0.2 (crate (name "stcat") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.3.3") (features (quote ("max_level_trace" "release_max_level_trace"))) (default-features #t) (kind 0)) (crate-dep (name "slog-async") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sloggers") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1bnykckdfvd5mxnz0rpzb896l841y7r472i9w9cjsbxhgij7mbis")))

(define-public crate-stcat-0.2 (crate (name "stcat") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.3.3") (features (quote ("max_level_trace" "release_max_level_trace"))) (default-features #t) (kind 0)) (crate-dep (name "slog-async") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "sloggers") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1kddbggd8slkikxgr11m8chlx23r06hkqn4gl3cp8wb939wnv3bm")))

