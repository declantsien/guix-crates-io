(define-module (crates-io st iv) #:use-module (crates-io))

(define-public crate-stivale-0.1 (crate (name "stivale") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0s51xxm8hlf7hndwbzyrhhbax5i7a1dgw603zzlkn5i8wzzb2qi2")))

(define-public crate-stivale-0.2 (crate (name "stivale") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1q5676skc2zpc2nq30d9ld9s8lm668pw5fgjarr8qqnjsn40ws5d")))

(define-public crate-stivale-0.2 (crate (name "stivale") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0wp711zjdqv2q3k1h7nawrhcdd9ba311v9dq0ll00qq08j5ggi7c")))

(define-public crate-stivale-0.2 (crate (name "stivale") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "07yjsqbs56kz37zam9vc83f8cklrdkg8kqpr55jvac3pxivnr1s2")))

(define-public crate-stivale-boot-0.2 (crate (name "stivale-boot") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1lq4m0r46wzfqr1r02cy4dgzrg3n03zhclrixkx0kqd02xj8nczn")))

(define-public crate-stivale-boot-0.2 (crate (name "stivale-boot") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1f6pfrrbymi27p20d2ww1qcpn1pbnkfwi2a3q4lpxridm0svnsm8")))

(define-public crate-stivale-boot-0.2 (crate (name "stivale-boot") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1ifaqyiq39nqk8di3b5ziz6w0kkwnfwim02mfp07qx40akawjd0g")))

(define-public crate-stivale-boot-0.2 (crate (name "stivale-boot") (vers "0.2.4") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "187mlgxyd38g31vi98xbnb7rys9inzfy77bfh122m55ljkbiak7q")))

(define-public crate-stivale-boot-0.2 (crate (name "stivale-boot") (vers "0.2.5") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1ph3igvyc4x91nh174za4x1b71xb2x6jg33n93wgbx9crliqzqvy")))

(define-public crate-stivale-boot-0.2 (crate (name "stivale-boot") (vers "0.2.6") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0vc3a37nl22bh5y4d4a38jd0zg25iv7gygd1dh91acbbn2gcz11r")))

(define-public crate-stivale-boot-0.3 (crate (name "stivale-boot") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1shc937vlyly590fv4mm3wklg3n5jd3ribirrv07yhi0d0zwwp9g")))

(define-public crate-stivale-boot-0.3 (crate (name "stivale-boot") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "stivale-proc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (optional #t) (kind 0)))) (hash "1a15rwk5g9vnwf12lqxaa6xjyi997j13b3mq0n6dlyqn6glqprxb") (features (quote (("helper-macros" "stivale-proc") ("default")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

(define-public crate-stivale-proc-0.1 (crate (name "stivale-proc") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "052b6y5srfp1ybbkaq6sl8dvxz8pqzpl062z50xww5yfqk5as08h")))

(define-public crate-stivale2-0.1 (crate (name "stivale2") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1rxis9hihjblx5mindlyv6d9mcr7i0q7c32ih0zx96lrby0yk064") (yanked #t)))

(define-public crate-stivale2-0.1 (crate (name "stivale2") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1srym74sg47lbi6924ckr65al9fbfrndwvy0iz0wlijn464swci7")))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.0") (hash "05ql73qnqjb39wkz1vka7vs7byjrhz0sav1xbf8rvzk4c6kf69s7") (features (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.1") (hash "1i7q8xyacg0ffm1j1zcr7pza6ixqxccn2mf7ccnyd2kmbs278zy4") (features (quote (("v2") ("v1") ("default" "v2")))) (yanked #t)))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.2") (hash "131v1q2bjz1gr36d721hd7gbhjbwi71pvr1azmj3awmh4i83rj08") (features (quote (("v2") ("v1") ("default" "v2")))) (yanked #t)))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.3") (hash "18287pwzvw1np3pyi0z3vj74rg2bir9hzsdz6ra897r344az2pra") (features (quote (("v2") ("v1") ("default" "v2")))) (yanked #t)))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.4") (hash "0v24ylxxrfjvvz8zc4q9qm1xqnf8lxszh3qm6l4rs52w7xs8pn35") (features (quote (("v2") ("v1") ("default" "v1"))))))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.5") (hash "107gdqvfjqd85syi8pay1yllnmd3kizs1zx1z1g51d0q0zv8drv2") (features (quote (("v2") ("v1") ("default" "v1"))))))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.6") (hash "06sqrcnd94rl58nhgq6r120ma4pjnvrakrda57wqj9smbwxbp49w") (features (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.7") (hash "12x85wx3bf7skw2mjzzczhwy2ph4whgfqdlv8fwxfxg4g85s2d4d") (features (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.8") (hash "0dgixvar8s6yj99ag2ygc4gbvzsnrp4iamvxf6314h0b8fdpj08q") (features (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.1 (crate (name "stivale_rs") (vers "0.1.9") (hash "12jd65h757khb2hjq849bhqfi679nhz2yf07yp1l9jiar7xl40cw") (features (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.2 (crate (name "stivale_rs") (vers "0.2.0") (hash "1lxphaadvfgqmri0rqry1sxzfa3g97nvy1pbg364iw4wjr06nga5") (features (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.2 (crate (name "stivale_rs") (vers "0.2.1") (hash "1y0mk03i7mpsbk8kjyla673cabqclib9blb29hmz65pfq052mbqy") (features (quote (("v2") ("v1") ("default" "v2"))))))

(define-public crate-stivale_rs-0.2 (crate (name "stivale_rs") (vers "0.2.2") (hash "15map27jpzmqr1ygfhx25ma2naig92az37z10ycnqpdhci6sab3j") (features (quote (("v2") ("v1") ("default" "v2"))))))

