(define-module (crates-io st lr) #:use-module (crates-io))

(define-public crate-stlrs-0.1 (crate (name "stlrs") (vers "0.1.0") (hash "0ig4bj5410yi4gi1g1ad9cpiz4d8qvpwx3cvi3k0xwadijcxsxn1")))

(define-public crate-stlrs-0.2 (crate (name "stlrs") (vers "0.2.0") (hash "06hkxpnqma1xl3f6wfs623lr8259yfi5ig7x4hvn97vpvbq2yrjs")))

(define-public crate-stlrs-0.2 (crate (name "stlrs") (vers "0.2.1") (hash "13icab3w7vndj1k7hrc4rvncisr6d66hmy1m5mdp466xq3xd586v")))

(define-public crate-stlrs-0.2 (crate (name "stlrs") (vers "0.2.2") (hash "1yqbhvqkrm6rh08w5md328sn07h77d84cpchnx2v847ysa3sc4iq")))

(define-public crate-stlrs-0.3 (crate (name "stlrs") (vers "0.3.0") (hash "0b975c1akag6kvqgfirpc3a6h3yh2ayhz47pd5gj8zr3jphi3gn5") (rust-version "1.56.0")))

