(define-module (crates-io st ut) #:use-module (crates-io))

(define-public crate-stutter-0.1 (crate (name "stutter") (vers "0.1.0") (hash "0ky0aj1xycnvdmkcc6n0m7z5ilii85zyf7dvb5v643kn57d9zyff")))

(define-public crate-stutter-rs-0.1 (crate (name "stutter-rs") (vers "0.1.0") (deps (list (crate-dep (name "cactus") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "0v0f9qas1zfmgc86jhy64qimzrlwjwijap2wfgscr91m6jcs7m3h")))

(define-public crate-stutter-rs-0.1 (crate (name "stutter-rs") (vers "0.1.1") (deps (list (crate-dep (name "cactus") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "0836w51yr6r1app57cm2gzkwy24r3rjzs8paajpsjpwyn0i5yw4b")))

