(define-module (crates-io st mc) #:use-module (crates-io))

(define-public crate-stmc-0.1 (crate (name "stmc") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qka5p4m17rc3zjc1wnyfs78jfzng7y16akpvmld0x24yxcvdzvs")))

(define-public crate-stmc-0.1 (crate (name "stmc") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19jrc4pdkqb6fqlnam4wh74i9scmcmj3p1k5qrkhzms6zzfd1914")))

(define-public crate-stmc-0.2 (crate (name "stmc") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15xlxl6fw9db8ax52z1yka4a3wsfk3fiphh4jrnhnbmcb8sppk4c")))

(define-public crate-stmc-0.2 (crate (name "stmc") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0k78byhkg84i0l5842bpym8sy896jd0q90nds1cfyplsn0va658h")))

(define-public crate-stmc-0.2 (crate (name "stmc") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1yhskpy0xb3fnf9vc864sfhfr6dvhx9bycapvicmc7q1av0l5bjq")))

(define-public crate-stmc-0.2 (crate (name "stmc") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sna9s97p2k27rm29k3aywvh7bab1hqmjq8zm42fb3cm3pjxdiln") (features (quote (("big_endian"))))))

