(define-module (crates-io st lo) #:use-module (crates-io))

(define-public crate-stlog-0.1 (crate (name "stlog") (vers "0.1.0") (hash "0dyxr8098yhikglnhizws4rbgxscq7x7nfp6fdi3f2028d28qbpz")))

(define-public crate-stlog-0.2 (crate (name "stlog") (vers "0.2.0") (hash "00g3y6fwbl2i274frcjqlbj0vfncbf71z8pbkh2kkhnwcbjcnqrz")))

(define-public crate-stlog-0.3 (crate (name "stlog") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "stlog-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vni668ppayrpwgvsd0qx5gdzj68014iapf1brib87kq8knqmphk") (features (quote (("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug")))) (yanked #t)))

(define-public crate-stlog-0.3 (crate (name "stlog") (vers "0.3.1") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "stlog-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1068v1qgbn9hvn59jn0my3vj7fmhr7vnck1kpmz8jw6vrc36kbj5") (features (quote (("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug")))) (yanked #t)))

(define-public crate-stlog-0.3 (crate (name "stlog") (vers "0.3.2") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "stlog-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hkkpnxz08clvkl6rgg3aa0hdhxjlk3klg30xc5w819ljiwk097w") (features (quote (("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug"))))))

(define-public crate-stlog-0.3 (crate (name "stlog") (vers "0.3.3") (deps (list (crate-dep (name "stlog-macros") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "1m7f7dvzzrjvi7l0rdygr342j0kkcnxiwg0nb5nqpbg1mq0axyq5") (features (quote (("spanned" "stlog-macros/spanned") ("release-max-level-warning") ("release-max-level-trace") ("release-max-level-off") ("release-max-level-info") ("release-max-level-error") ("release-max-level-debug") ("max-level-warning") ("max-level-trace") ("max-level-off") ("max-level-info") ("max-level-error") ("max-level-debug"))))))

(define-public crate-stlog-macros-0.1 (crate (name "stlog-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0w1dlrlyjh9a7dgd7bbp7d2ngl6rjjllcjaz7s243r76ndw6nbaq")))

(define-public crate-stlog-macros-0.1 (crate (name "stlog-macros") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03wrvxnf3y58z4r7rszsyziyvqgbadqlyf5d6ndn15kx21zc2ypb")))

(define-public crate-stlog-macros-0.1 (crate (name "stlog-macros") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jc82jnkfncfr1bh52brczs1rzi9879mn2f17fmmy0mpfmgmzh4l") (features (quote (("spanned"))))))

