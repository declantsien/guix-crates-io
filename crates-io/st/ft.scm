(define-module (crates-io st ft) #:use-module (crates-io))

(define-public crate-stft-0.1 (crate (name "stft") (vers "0.1.0") (deps (list (crate-dep (name "apodize") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "strider") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0957gbf01lmkfwm73amag3r5833vmfx3vk5vvg3w251p0z44a72m")))

(define-public crate-stft-0.1 (crate (name "stft") (vers "0.1.1") (deps (list (crate-dep (name "apodize") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "strider") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1xnzkz97vxn7cpr2ws6w6k18cls1ibva52wi4f22c5h0kakx8ad3")))

(define-public crate-stft-0.1 (crate (name "stft") (vers "0.1.2") (deps (list (crate-dep (name "apodize") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "strider") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1rnj73c56vzml23lb0qhsi3gs77rk2zanwj7lphdds7xa7xbg4pr")))

(define-public crate-stft-0.1 (crate (name "stft") (vers "0.1.3") (deps (list (crate-dep (name "apodize") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "strider") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1lg1j84vx94g138k2blvxs4wymjxsi2wbjwmm3pyddg6y1gqm97c")))

(define-public crate-stft-0.2 (crate (name "stft") (vers "0.2.0") (deps (list (crate-dep (name "apodize") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "strider") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0maw4y7xjag9d1bi39aa2pqx5rww8fzqxy53qdgk6nf219x1169c")))

