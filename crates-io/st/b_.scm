(define-module (crates-io st b_) #:use-module (crates-io))

(define-public crate-stb_dxt-0.1 (crate (name "stb_dxt") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.18") (default-features #t) (kind 1)))) (hash "0hqvci06ihmv80vi2w6g50gv19rjbzbfkjkr2iw16l7hb96gksh8")))

(define-public crate-stb_image-0.1 (crate (name "stb_image") (vers "0.1.0") (hash "15sc8l2jf21449fpq77cx9krq2swpmw4w06gqrsg1rd6m553y91a")))

(define-public crate-stb_image-0.2 (crate (name "stb_image") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1dbbzpvjrwmhlbhq1c2hk3y2550m155iq16rkqag13fxlld6493a")))

(define-public crate-stb_image-0.2 (crate (name "stb_image") (vers "0.2.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14knxr1v1pw763nyqz6fg87yb1z9ypgfvdds3x2yxpq0lra8x8px")))

(define-public crate-stb_image-0.2 (crate (name "stb_image") (vers "0.2.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "07hy1mhlgmhr9mpv6zqq8mzj33pi7ysb7xg65chm4pxhxx1dwfma")))

(define-public crate-stb_image-0.2 (crate (name "stb_image") (vers "0.2.4") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qdffq2a1c7n1l99bhlgbvmg95vakh3c0wd3y0shsr243m8kl2nl")))

(define-public crate-stb_image-0.2 (crate (name "stb_image") (vers "0.2.5") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r608cmql9r5r01b7inm9zm82zlz0jawp601k9cnglqlv1izwrg8")))

(define-public crate-stb_image-0.3 (crate (name "stb_image") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lzwykh1sbsgf40gyf1bi1anfy04ry54lv5imgj64cyxnygainmz")))

(define-public crate-stb_image_rust-2 (crate (name "stb_image_rust") (vers "2.27.0") (hash "0vqf4apvnjfiy6jar39qirrpm556ha3ba3n80q6h1bfkgah3707n")))

(define-public crate-stb_image_rust-2 (crate (name "stb_image_rust") (vers "2.27.1") (hash "0gby0b6azxq7hrz1mnd945ciyvad6g6nvnfxdgwsxnrhfwv18pkd")))

(define-public crate-stb_image_rust-2 (crate (name "stb_image_rust") (vers "2.27.2") (hash "1amnc11vrhxqr3b8f3svxzd1qid8am5gwb6j8ygy1m81mc1a10nn")))

(define-public crate-stb_image_write_rust-1 (crate (name "stb_image_write_rust") (vers "1.16.0") (hash "1mwsg1msdd0avaygpfd1if79c6n4d0pi56gqpllrvn8mmgngdhq7")))

(define-public crate-stb_image_write_rust-1 (crate (name "stb_image_write_rust") (vers "1.16.1") (hash "1d33n8avk41psqx84sard0ahn8sa62xdlw776sygfgr1sm28py5f")))

(define-public crate-stb_rect_pack-0.1 (crate (name "stb_rect_pack") (vers "0.1.0") (deps (list (crate-dep (name "stb_rect_pack_sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "13lsp2ii9cywy7axdk4bi4hs6icyvw58892p99knmyv8prymaf8y")))

(define-public crate-stb_rect_pack-0.2 (crate (name "stb_rect_pack") (vers "0.2.0") (deps (list (crate-dep (name "stb_rect_pack_sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g87iwajyihpnax8pqp70qqm110n4bpywmmsnc6fsd3zl90ldimw")))

(define-public crate-stb_rect_pack-0.3 (crate (name "stb_rect_pack") (vers "0.3.0") (deps (list (crate-dep (name "stb_rect_pack_sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h3i1bz7n9264wyfjz725b8n04wb02agym4aq2bg0iyf6xml7d9d")))

(define-public crate-stb_rect_pack_sys-0.1 (crate (name "stb_rect_pack_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.61") (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "1qdkqqdx6d72n5by6pqyb0wv15g8d0adl97lxfs67fg1s9kb7a4l")))

(define-public crate-stb_rect_pack_sys-0.1 (crate (name "stb_rect_pack_sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.61") (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0cm9i783pvn5dxw0g0v10w8n68bssdjg4bmqhcyxl0nmf3kl9i8g")))

(define-public crate-stb_rect_pack_sys-0.1 (crate (name "stb_rect_pack_sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.61") (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0wvh0vh76zvl4drln4pbb5qy8a101p29ggh57hk74r2nha61nc7m")))

(define-public crate-stb_rect_pack_sys-0.1 (crate (name "stb_rect_pack_sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.61") (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0c5pfkknx4rfir74jaxc7ks5f6jhppd09y631vp69pnlx32l9q8q")))

(define-public crate-stb_rect_pack_sys-0.1 (crate (name "stb_rect_pack_sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.63") (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0y8zc8v5jj5gvv90w411ryb83gdk2fm540d6imjsv8c6flx06z20")))

(define-public crate-stb_rect_pack_sys-0.2 (crate (name "stb_rect_pack_sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.64") (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "1d65z5j777pxkxmf4d0n6yb70imzkbv3mvc2ii0y51dwjdsz9q4z")))

(define-public crate-stb_rect_pack_sys-0.3 (crate (name "stb_rect_pack_sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "12s4xh34j000a6818rv2imy83a0f8s3lmjx80qw14325sjk84isl")))

(define-public crate-stb_truetype-0.1 (crate (name "stb_truetype") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ljsqbbj3bbqjshxmzi0ra8qckn3hpqyp95n20kgnbi1pj92v8bm")))

(define-public crate-stb_truetype-0.1 (crate (name "stb_truetype") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jbb9mkakxnqbrkr9rca8x01dcrbga3kgca4gn23brs6xqryfxkw")))

(define-public crate-stb_truetype-0.1 (crate (name "stb_truetype") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "026sdzxhbmfbnbql4bskwxwm7f7bznrnx0rysq4f57gw8042gwzw")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ysig67ll41s7b64cmmkfbj6jn4in790fggvzp2g04mlrwamr5d0")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "01c8jnqb1d76hbcbv94vbxi2fdhv17p6k5yrw1vs94x4i2sw7d91")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f2m5npaf6yw0jj8i6j8x8zh1pg090182a49qvxwy4fxmcw2pkjj")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.3") (deps (list (crate-dep (name "approx") (req "^0.3") (kind 2)) (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gfsqdrhmbrs11xpz8fjidb46mrw17w6p9z82bmdfxi5q1zndhg5")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.4") (deps (list (crate-dep (name "approx") (req "^0.3") (kind 2)) (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wcavb4nnwxf77y0ikjslxbk1i3c86spwz0zvq4mjr6q6qqpvyj8")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.5") (deps (list (crate-dep (name "approx") (req "^0.3") (kind 2)) (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w5dr5gcr07k4lkf7ds8gak4q11il8cbwhf35ni2jq9vnihd59vi")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.6") (deps (list (crate-dep (name "approx") (req "^0.3") (kind 2)) (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)))) (hash "1fk8ar6wn7vnxfcqvg8lhbh47dg544s6kr4bzxa1vs5qbm8dzdv9")))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.7") (deps (list (crate-dep (name "approx") (req "^0.3") (kind 2)) (crate-dep (name "byteorder") (req "^1.1") (kind 0)) (crate-dep (name "libm") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "15p0sx3hwzi2kg7d1g07hd07nbpp87n6ysjqgvy4nlahidhry41m") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-stb_truetype-0.3 (crate (name "stb_truetype") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.3") (kind 2)) (crate-dep (name "byteorder") (req "^1.1") (kind 0)) (crate-dep (name "libm") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0ykggf9iaifsg2q441j5lld33iv0d2nhw9s54nnw7jsjzgb10hl2") (features (quote (("std") ("default" "std"))))))

(define-public crate-stb_truetype-0.2 (crate (name "stb_truetype") (vers "0.2.8") (deps (list (crate-dep (name "stb_truetype_next") (req "^0.3") (default-features #t) (kind 0) (package "stb_truetype")))) (hash "0pa2rfqjlkh015lny0ls8w28r38y8pw2kgff1xl5lk19h91yq6wx")))

(define-public crate-stb_truetype-0.3 (crate (name "stb_truetype") (vers "0.3.1") (deps (list (crate-dep (name "approx") (req "^0.3") (kind 2)) (crate-dep (name "byteorder") (req "^1.1") (kind 0)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "0lgvnh3ma6cz811bk8imj45djz76zs47b8327sgnmik2x03nnyzp") (features (quote (("std") ("default" "std"))))))

(define-public crate-stb_truetype_bugfix_19072017-0.2 (crate (name "stb_truetype_bugfix_19072017") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mr3a6hv9fz3szywyw5khqpsl8mqpwbwillsha8mxpx59wnz19lh")))

(define-public crate-stb_truetype_rust-2 (crate (name "stb_truetype_rust") (vers "2.27.0") (hash "050va8s8qmbk675gyc7lpm8dg9hldgy90njgnq0a383hrs4h1ab5") (yanked #t)))

(define-public crate-stb_truetype_rust-1 (crate (name "stb_truetype_rust") (vers "1.26.1") (hash "03pzfw31vm5ikhf9753sp7v2790dqklmgrn8ikd9xx26qm79ywf9")))

