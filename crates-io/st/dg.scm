(define-module (crates-io st dg) #:use-module (crates-io))

(define-public crate-stdg-0.1 (crate (name "stdg") (vers "0.1.0") (deps (list (crate-dep (name "euclid") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "font-kit") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0vvpz4s0pjzj7h4kn2yza4rw82j7yvgmk3d3zha8iw2inc28pqf9")))

(define-public crate-stdg-0.2 (crate (name "stdg") (vers "0.2.0") (deps (list (crate-dep (name "euclid") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "font-kit") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0zfsr0iswqvsn5cp9fimnlm65bsp78i87n4rzz3d8pnhp9zgfmfn")))

