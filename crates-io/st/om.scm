(define-module (crates-io st om) #:use-module (crates-io))

(define-public crate-stomp-0.3 (crate (name "stomp") (vers "0.3.3") (hash "16vj9cn4wy70ll0flb1b8dd2g0fd261mjjwr9981zwd7wk2d32n4")))

(define-public crate-stomp-0.3 (crate (name "stomp") (vers "0.3.4") (hash "0pdv80c5ir2jjvq6l7szkkqkqbn5qmplfmvkwpjsd5nl68zqxh74")))

(define-public crate-stomp-0.3 (crate (name "stomp") (vers "0.3.5") (hash "187av6zdgkv5q32j1acslpc42mppqn501zhj0w0n0081nr7finb7")))

(define-public crate-stomp-0.3 (crate (name "stomp") (vers "0.3.6") (hash "0y2l2arvvmg1sjimg8jb4gbp3lnn5r6d29d2dka9irdrf0p9a11m")))

(define-public crate-stomp-0.3 (crate (name "stomp") (vers "0.3.7") (hash "1bb5zddya8h59m0k31np9nis6rrl9756bhj0i0iw0kkmji5gqld4")))

(define-public crate-stomp-0.4 (crate (name "stomp") (vers "0.4.0") (hash "01q5qacz1w5fyrfj4mpn85r4jv25mapdd78wk6ygvw8bvjpkqqlz")))

(define-public crate-stomp-0.5 (crate (name "stomp") (vers "0.5.0") (hash "049q97q1v2l209381f4l368bhzhrphdrv3lf613kjsaqw32lmzsd")))

(define-public crate-stomp-0.5 (crate (name "stomp") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0v8n2g5x3s112pjkmla450pad68341n125nzgb4vnhkyqcr3xqkz")))

(define-public crate-stomp-0.5 (crate (name "stomp") (vers "0.5.2") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "07fa0gq6xrnj18frj9a6869zzwig3l67k54ahijvqk37j0dxxc4h")))

(define-public crate-stomp-0.6 (crate (name "stomp") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "10ikhx7jkp8mwsrk9xlvr4fk80vmxhrm8ci01rlr06kfa9k6p3gk")))

(define-public crate-stomp-0.7 (crate (name "stomp") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1dzvq4x37bm5309akwpd2qi3q3pdmx2rym11wr71pilrzxawn7j2")))

(define-public crate-stomp-0.8 (crate (name "stomp") (vers "0.8.0") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "094fv3x15avgsz3fb4vkssgy7fsfh4na5ipxqlzbd3snw2dy6dxx")))

(define-public crate-stomp-0.8 (crate (name "stomp") (vers "0.8.1") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1mmpinp2rndwb57n11ahv5j6i90vfqxysmaxy6hzln4994b346lr")))

(define-public crate-stomp-0.8 (crate (name "stomp") (vers "0.8.2") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0n7hn1kp8446hih674jrdmy8swn3abjrlya80v03d044129gxwqr")))

(define-public crate-stomp-0.8 (crate (name "stomp") (vers "0.8.3") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "01cmy83wlbad8pnnyffiwnhviy7fns02k7g6g9gis2ibp4ncgsj3")))

(define-public crate-stomp-0.8 (crate (name "stomp") (vers "0.8.4") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0y3zlin83j7mclsxx9in6293lrq95wr8gh8453cmfcik87frhirq")))

(define-public crate-stomp-0.9 (crate (name "stomp") (vers "0.9.0") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "*") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "*") (default-features #t) (kind 0)))) (hash "0faqkcbg0q38asw5347wcfmkka156nwnvk35fc90bzj9ij0gn8rn")))

(define-public crate-stomp-0.10 (crate (name "stomp") (vers "0.10.0") (deps (list (crate-dep (name "lifeguard") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "*") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "*") (default-features #t) (kind 0)))) (hash "0mqwmwg0rjl3pa2kaaijg71jwm3byc6d96zqd73wk25svcyszjpf")))

(define-public crate-stomp-0.10 (crate (name "stomp") (vers "0.10.1") (deps (list (crate-dep (name "lifeguard") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "*") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "*") (default-features #t) (kind 0)))) (hash "027pksksgfyw2yw4midj15h6m9h1jnxgc2hxxmwyspi9l3xb84jv")))

(define-public crate-stomp-0.10 (crate (name "stomp") (vers "0.10.2") (deps (list (crate-dep (name "lifeguard") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zq0wakid7zck4zl9lavh6aj8yhx91ixgh8lhysx8j8rchn448qa")))

(define-public crate-stomp-0.11 (crate (name "stomp") (vers "0.11.0") (deps (list (crate-dep (name "lifeguard") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jl718k1gz2a0gdyn9vgrg2pm42rx2r4j8dy63z1rbf3cj70yljz")))

(define-public crate-stomp-client-0.1 (crate (name "stomp-client") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "stomp-parser") (req "^0.5") (default-features #t) (kind 0)))) (hash "0fqwh3laindigibhjpbl4x64jbb3g510ipqzd6fgkqpvwrb9f9wn")))

(define-public crate-stomp-parser-0.2 (crate (name "stomp-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^6.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "nom-trace") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "15nay1rwizvshci6pdaakl1kf2dg5j8c7ysmkkqkrh1wg65h1pvy")))

(define-public crate-stomp-parser-0.3 (crate (name "stomp-parser") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^6.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "nom-trace") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1nggj2vz5zaix4nkgg742knhnrb5wcmnfixq9n7slwc1z2yavilp")))

(define-public crate-stomp-parser-0.3 (crate (name "stomp-parser") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^6.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1ilzda5nqh9npas8xqc7isrdhxigry0jxkmlihharcwx9affrszg")))

(define-public crate-stomp-parser-0.3 (crate (name "stomp-parser") (vers "0.3.3") (deps (list (crate-dep (name "nom") (req "^6.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1f5d1rw13a1a5nwclkh1k9v9dnpcbwsg6q70ksaczyha5dblkdxb")))

(define-public crate-stomp-parser-0.4 (crate (name "stomp-parser") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^6.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0a7gpd33hypw3ydsriks5kz4mvwkpsg40mv12by89aiwmhpb53qj")))

(define-public crate-stomp-parser-0.4 (crate (name "stomp-parser") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^6.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "11p90zavnqr1m9lk58ccirxy3sb3gn30iq2jxcm3i7kq8gxil6jq")))

(define-public crate-stomp-parser-0.5 (crate (name "stomp-parser") (vers "0.5.0") (deps (list (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0gk4g6521rx60c43rh4k7sgsi1afdr0zb6zkrphc8kcpci1fr69i")))

(define-public crate-stomp-parser-0.6 (crate (name "stomp-parser") (vers "0.6.0") (deps (list (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "13vhac3m958ls5y8a5ms0db6z0jj1hpddrhkd282bx6v3xdhwkdd")))

(define-public crate-stomp-rs-0.0.1 (crate (name "stomp-rs") (vers "0.0.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05dys2b4sx5ris1w1wwmdsx8jh0bdp8vh01j69za9wdnwr6ix1l2")))

(define-public crate-stomp-rs-0.0.2 (crate (name "stomp-rs") (vers "0.0.2") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0bwxqg2nqacy9kf09iwxc8k0rg0zpp2fzgkl55hqlfzqig3x6759")))

(define-public crate-stomp-rs-0.0.3 (crate (name "stomp-rs") (vers "0.0.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "16mvnwcgjdmasd1434825i9icgzm6wz940hwpdi7ljbpvf2dslan")))

(define-public crate-stomp-rs-0.0.4 (crate (name "stomp-rs") (vers "0.0.4") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0nsi2nk7v3bg6kphyls03mxs96qlf4gbjmn5m5mjs8rpjm9w49nx") (yanked #t)))

(define-public crate-stomp-rs-0.0.5 (crate (name "stomp-rs") (vers "0.0.5") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1nby08nm9xi65hsxn9qfyx7pz9mhm0fkym88d4m66xqx0sdb9l4f")))

(define-public crate-stomp-rs-0.0.6 (crate (name "stomp-rs") (vers "0.0.6") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1pmc89xhkl0k0a22n71czwplf47dkrrqz9j2dvjprnsdlhvqv8ld")))

(define-public crate-stomp-rs-0.0.7 (crate (name "stomp-rs") (vers "0.0.7") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0v0hl8fffln5m4n94rfmfm2yfc86589jddn38prfnz8fiqhn59wa")))

(define-public crate-stomp-rs-0.0.8 (crate (name "stomp-rs") (vers "0.0.8") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1j5h2xb6wwc9y00v2vv0lccqf0xyy0cz4lwd8ccgdgr1xl67b6hf")))

(define-public crate-stomp-test-utils-0.1 (crate (name "stomp-test-utils") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync" "time" "test-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "03jksylp5aclxai3qm3h26a1w21gxgkd6nmyigrx6bp88bqi4qh9")))

(define-public crate-stomp-test-utils-0.2 (crate (name "stomp-test-utils") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync" "time" "test-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "0r83yh6pk71bzsg3900ipv47hrbf49c1ws0a0k88xzi5g0zf36hv")))

(define-public crate-stomp-test-utils-0.2 (crate (name "stomp-test-utils") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync" "time" "test-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "04n6vmcz0q8bxbvd4a9rhixsdasb8n046iz74dpfiybzqym7wafy")))

(define-public crate-stomper-0.1 (crate (name "stomper") (vers "0.1.0") (deps (list (crate-dep (name "libstomper") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "08jqnxyhy3ilycq82gvyi3v4skx04bz5y6s5an4gcgfxqav4nzzm")))

(define-public crate-stomper-0.2 (crate (name "stomper") (vers "0.2.0") (deps (list (crate-dep (name "libstomper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "11g2rb1ql7naas6qr0xn3ibvrg775wmcckqazbzminwlq2j4nsz4")))

