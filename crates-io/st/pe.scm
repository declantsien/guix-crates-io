(define-module (crates-io st pe) #:use-module (crates-io))

(define-public crate-stperf-0.1 (crate (name "stperf") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qkiw584s6a2ds0hbi8fjr4gb9cb9gv9j248cyzhmqjqvgm25z1k") (features (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1 (crate (name "stperf") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gxlgw3sf6chz5vzf1nvvrw14vdb8pkss55f1if10nx6xwvsqina") (features (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1 (crate (name "stperf") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)))) (hash "15yj0ilq04scpnxgdxh0api7kwv7cpf5cjlbx09fk5slzg7xdz7p") (features (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1 (crate (name "stperf") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)))) (hash "1difmpmjrink9b4v8m0pl6a2dwal3737mbn6si5spdrnqs2ympmj") (features (quote (("disabled") ("default"))))))

(define-public crate-stperf-0.1 (crate (name "stperf") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jyj9hyixq1ybm0bg58nakb04sz87ah6a8a8m14z4ikm2dg7c6r0") (features (quote (("disabled") ("default"))))))

