(define-module (crates-io st oo) #:use-module (crates-io))

(define-public crate-stooge-0.1 (crate (name "stooge") (vers "0.1.0") (hash "0iq9si19801krq780n72qjxbs5jyh8y22cy9rvnq1890js7ggklp")))

(define-public crate-stoogesort-0.1 (crate (name "stoogesort") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0sgd7zsqxqaflq7535njk0ikhrhhyl7c6pwfk6x74yh31s4ka29g") (rust-version "1.56.1")))

(define-public crate-stoogesort-0.1 (crate (name "stoogesort") (vers "0.1.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0346ixqzsvrqv1l6spxjrl4rwh9rsx6m26q2qw5zmzy7g5fzxyrr") (rust-version "1.56.1")))

(define-public crate-stoogesort-0.2 (crate (name "stoogesort") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "04igj19qa8a7f3sbm375h8rwbhpbwz767ijzpndvcqs8dywhkjcs") (rust-version "1.56.1")))

(define-public crate-stools-0.1 (crate (name "stools") (vers "0.1.0") (hash "0nifyg7b4r3bhs0kcp616grmpwq236il6n8l2y8fh9nw24vyydr7") (yanked #t)))

(define-public crate-stools-0.1 (crate (name "stools") (vers "0.1.1") (hash "13g1kikwlr4a29kziqh0lzsz4899myf29mjap2l184160xrqrax6") (yanked #t)))

(define-public crate-stools-0.1 (crate (name "stools") (vers "0.1.3") (hash "0da3aqs3n0vjk196sgfcwsqpc1wrp9inais5ydaw07v7x4177r48") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.0.0") (hash "0i5n2zabq8i0660x9lpdb2s4qr1nfr52lymicsrjbjh54chqhf2f") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.0.1") (hash "026whzmm0xlqs77fd6qcws3za81incd4r3jfy52hq3pn3ighghcb") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.0.3") (hash "0v1ginbsh6x42ly7lylrkb2gyszh98wml2351zyp2mw9xipqh7ka") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.0.4") (hash "0wsfpcpipbr3104m7wg1fx1jfgf1b1f2jx1cg4016rk1szbr1323") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.0.5") (hash "1dkwpx8i666xx3ij25cwm8h8xdwfdz991gcc32s4ygn8lj9d0n35") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.0.7") (hash "10sb0v90xfzm63qgc3mfriib4wkbkpfqk2y7b1p753wziw82qj6g") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.0.8") (hash "063am7ly6cq9cxaabyv800bv3y0vcgpp01613n5b4475q3vhvadi") (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.1.0") (hash "0nncczzhrpy30lsmv0hs3zyvdw6kvkvjxck78rpnn1h5vgjgjlwx") (features (quote (("hook_key")))) (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.1.1") (hash "025acwgg2kizyc4438d688kz78w71qxgg6cfppl6fg1c151zrilf") (features (quote (("hook_key")))) (yanked #t)))

(define-public crate-stools-1 (crate (name "stools") (vers "1.1.2") (hash "0b8kcgxpgkw7xrp0fhhy5z13jinybyp247crh0j3m4rcxjqh6n9c") (features (quote (("hook_key"))))))

(define-public crate-stools-1 (crate (name "stools") (vers "1.1.4") (hash "05968zx4spxm9brga3g4x9m2i0p8y1yyjj59m4qz23a3347wa2l8") (features (quote (("hook_key"))))))

(define-public crate-stools-1 (crate (name "stools") (vers "1.1.6") (hash "1qn9603cxls7gq2r50ymlwas9zkx4yyrlb9mqgzp3prcyy85awgd") (features (quote (("hook_key"))))))

