(define-module (crates-io st av) #:use-module (crates-io))

(define-public crate-stava-0.1 (crate (name "stava") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "15y3l77p79pnjaaqbzxsav3y257jrxj6ba8sf7vq8zp19a0x4j5z")))

(define-public crate-stava-0.2 (crate (name "stava") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "19k4cs4xvrj60il7bm5v1r2n05qzspf3knxrjlg173d4sbypv719")))

(define-public crate-stava-0.3 (crate (name "stava") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "perf"))) (kind 0)))) (hash "1gy5xz9l5b19ay5x53a1cb5ck4c255zsr08m0mpjc0ri5lzphva4")))

(define-public crate-stava-0.3 (crate (name "stava") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.31.2") (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "perf"))) (kind 0)))) (hash "1jpg8nay3f1mcrj1cbw30xz8n5mj0sphhrdzyss5sk481cw1jrbv")))

(define-public crate-stava-0.4 (crate (name "stava") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (kind 0)) (crate-dep (name "include_dir") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "perf"))) (kind 0)))) (hash "1k3zhsixzkxy4rz6wgim9js1hp14pzam84fz80c4c9jq3009h4yh")))

(define-public crate-stava-0.4 (crate (name "stava") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.31.2") (kind 0)) (crate-dep (name "include_dir") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "perf"))) (kind 0)))) (hash "1lnkv80hblmbg04bq7nbq525ycybnbdakqqywpar121gaan42rfc")))

(define-public crate-stava-0.5 (crate (name "stava") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (kind 0)) (crate-dep (name "include_dir") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "perf"))) (kind 0)))) (hash "0fbrnh6s9p015iml8fmr2306w52l8l1pfgrw01hm76hzsgl6c4zn")))

(define-public crate-stava-0.6 (crate (name "stava") (vers "0.6.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.31.2") (kind 0)) (crate-dep (name "include_dir") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "perf"))) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "13z5bw22hy72nw3lhsgbb2ik1679z01yaznhaq8q1ikgxp7g4hdh")))

(define-public crate-stava-0.6 (crate (name "stava") (vers "0.6.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.31.2") (kind 0)) (crate-dep (name "include_dir") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "perf"))) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1nifka0cpm7nazxqnwrzwwpiw4rp56glsngia69wwkl963ynnlng")))

(define-public crate-stava-0.6 (crate (name "stava") (vers "0.6.2") (deps (list (crate-dep (name "assert_cmd") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.1") (kind 0)) (crate-dep (name "include_dir") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.4.2") (features (quote ("std" "perf"))) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "15yv7c96gz8nl1rh1d59aizh6j0zqc1pm575x82iqlwdnmq42496")))

(define-public crate-stave-0.0.0 (crate (name "stave") (vers "0.0.0") (hash "0g4nj3bbjfhrlq65wlpqavhmr0z1dv8q9d3afrss9ryb2lwfqcdh")))

(define-public crate-stavec-0.1 (crate (name "stavec") (vers "0.1.0") (hash "0drg6qpmcd2pq5h6amrn7xp50sjgw3a4aav26iky1zs0n2k02wa5") (features (quote (("std") ("default" "std"))))))

(define-public crate-stavec-0.1 (crate (name "stavec") (vers "0.1.1") (hash "0vrpa1lhkywvn5cqqrl3ixqsvn7p78siadn95n47wc6kif2k8fzc") (features (quote (("std") ("default" "std"))))))

(define-public crate-stavec-0.1 (crate (name "stavec") (vers "0.1.2") (hash "07s475lxyvk0r16i28zdzxms1l88j2h4bnsp6d2590zrdxz3fypj") (features (quote (("std") ("default" "std"))))))

(define-public crate-stavec-0.2 (crate (name "stavec") (vers "0.2.0-rc.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1lj8h7kcd7qg6l15zd23h9phq5vrjzmwixs5h2j37inwivfl57j7") (features (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

(define-public crate-stavec-0.2 (crate (name "stavec") (vers "0.2.0-rc.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0pj35bsgc6cb2wlsa91bhap2r1bv1rqa2c7mswil1nxam8r3ppbc") (features (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

(define-public crate-stavec-0.2 (crate (name "stavec") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1r2wk7gg0921mk4s4nc5py9n41vdhzgwgdcd2qrpnj7zbdq96xbw") (features (quote (("std" "num-traits/std") ("repr-c") ("default" "std")))) (yanked #t)))

(define-public crate-stavec-0.2 (crate (name "stavec") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0wv52770labmcjclxyb225vp7hjmv552l4pbnjf6mi2ca3bdpdfh") (features (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

(define-public crate-stavec-0.3 (crate (name "stavec") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0az0ixwll4h03rh3mmbqfj0yd1mblx8x4hrzij85ncvi4n0gjqcs") (features (quote (("std" "num-traits/std") ("repr-c") ("default" "std"))))))

