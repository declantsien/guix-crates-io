(define-module (crates-io st as) #:use-module (crates-io))

(define-public crate-stash-0.0.1 (crate (name "stash") (vers "0.0.1") (hash "060cmx6lz92d61aklgl0r5bd013pz66sgajiqcagsqy2mwxymjp1")))

(define-public crate-stash-0.1 (crate (name "stash") (vers "0.1.0") (hash "19a5bja8aykny4wdnz27pa6qy4q1i9s9nxv31wni5wg072ch2n6b")))

(define-public crate-stash-0.1 (crate (name "stash") (vers "0.1.1") (deps (list (crate-dep (name "unreachable") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0iwygws3yh2ap0j0mxvsj9maxbnd1h3lwpsybjk4pk8dcz14ikh0")))

(define-public crate-stash-0.1 (crate (name "stash") (vers "0.1.2") (deps (list (crate-dep (name "unreachable") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "165m3kr72sdyzis1s163dqlrbq4x8gc9f4lqw5c6xh36v6h2f7yc")))

(define-public crate-stash-0.1 (crate (name "stash") (vers "0.1.3") (deps (list (crate-dep (name "unreachable") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1rgm1azfymncwqdnibdvdv9vvvvrdi3gzsvhhfdwn6kdig5l51ib")))

(define-public crate-stash-0.1 (crate (name "stash") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)))) (hash "1wsflx656x7dc7n92lnc2csfxasqnhc9bzmb3hnsgiqsw0iqr458") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-stash-0.1 (crate (name "stash") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 2)))) (hash "06n9gmmnsrd1621q09kxnaswkvg69clasakbw0cni5snj3jh35p7") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-stash-cli-0.1 (crate (name "stash-cli") (vers "0.1.0") (hash "08shrwf7lvr3fasvpgwf0pl5sxaikf66gzpp71lc07k9kzbffb3q")))

(define-public crate-stash-dynamodb-0.1 (crate (name "stash-dynamodb") (vers "0.1.0") (hash "0cbyyd7a8n73dbc5rg30fgkhbqkx22fdq2kcc2n1vxghnvg1rscp")))

(define-public crate-stash-sdk-0.1 (crate (name "stash-sdk") (vers "0.1.0") (hash "139nzmvcx1zjlbrj7wy6h41iaq4x3zx2czlfhw7m32j96diagqin")))

(define-public crate-stashr-0.1 (crate (name "stashr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1hk1781g5c3gd2yf6z8fagssc0l4n8mfpswah47cc7nq65r80d6v")))

(define-public crate-stashr-0.2 (crate (name "stashr") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0p64k2drlrqrz1z46klcfmvfmij006xkfb25xsv0ah5j3kqygin1")))

(define-public crate-stashr-0.3 (crate (name "stashr") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0r5adw49kiv24r5fwf7nv2bx6ghqql611dscxzm950dszzvdgr7b")))

(define-public crate-stashr-0.3 (crate (name "stashr") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1liv962gcq5kcpqllhz4bl5add269i17xrwvmiwm83v252r0nw5n")))

(define-public crate-stashr-0.3 (crate (name "stashr") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0a0g1w96chd27b5kqsylj0mn31yd94km0z8p4q91xd7skbv3p7ia")))

(define-public crate-stashr-0.3 (crate (name "stashr") (vers "0.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1ar5ckl4c6gfrj537yqnn4gyfmla17jpvi3j1mmnsmh9j2dgglws")))

(define-public crate-stashr-0.4 (crate (name "stashr") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1nhnvz486fw0mnlbra7bp9m2vz7igb60582hys87m4p3b2vghvgp")))

