(define-module (crates-io st m-) #:use-module (crates-io))

(define-public crate-stm-core-0.4 (crate (name "stm-core") (vers "0.4.0") (deps (list (crate-dep (name "parking_lot") (req "^0.5") (kind 0)))) (hash "0fb1wy022x8ai4q251vbbaqgspxvady5w43p6h8gpdzk7j9i65jl") (features (quote (("default"))))))

