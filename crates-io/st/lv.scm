(define-module (crates-io st lv) #:use-module (crates-io))

(define-public crate-stlv-0.1 (crate (name "stlv") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0dsc3sdp46aar78z0smz9nwal5h51n8wgyjbysi9352yvgpfspc8")))

(define-public crate-stlv-0.1 (crate (name "stlv") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1jg8l4lp09w5d50x53rsd3k364034nz9dz5sdf7rgcxmdfsjn2jf")))

(define-public crate-stlv-0.1 (crate (name "stlv") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1cbgxm3cbbil4s74v07s0p2p9kawmyvx8n218y1z8azhh9xs1drc")))

(define-public crate-stlv-0.1 (crate (name "stlv") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yapcs87gzr0hwj5y0gn845niv6li4ankwm1sdcdfh6c1mdbzv3x")))

