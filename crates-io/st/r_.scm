(define-module (crates-io st r_) #:use-module (crates-io))

(define-public crate-str_assert-0.1 (crate (name "str_assert") (vers "0.1.0") (deps (list (crate-dep (name "dissimilar") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m2dw4zfa5qfch89sg7ll5kpv4y6ndwyd164mih6ni2kygjjfsfm")))

(define-public crate-str_assert-0.2 (crate (name "str_assert") (vers "0.2.0") (deps (list (crate-dep (name "dissimilar") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fvvg82rc5bsbxsap1a81wiwr3h3g1ga2nvbz4p1ajcamjqm97q8")))

(define-public crate-str_case_conv-0.1 (crate (name "str_case_conv") (vers "0.1.0") (hash "1c8ypc91gsmyfzkl1yyd01bbgh4irs9y01nmy4pn0m4vpcfikvkp")))

(define-public crate-str_crypter-1 (crate (name "str_crypter") (vers "1.0.0") (hash "1925qyigim27ssiciv37zv4622kcaqnqzha76cjnqms7ghhl0ygk")))

(define-public crate-str_crypter-1 (crate (name "str_crypter") (vers "1.0.1") (hash "00p68fvmpxgapw9iz2vwddc4ll1a09g74k4zhzq1d81ck7w2aqsa")))

(define-public crate-str_crypter-1 (crate (name "str_crypter") (vers "1.0.2") (hash "1wm55wpmcqvh0rbas6li1pda2vmp6jga820nnh559gfbs24pi1f5")))

(define-public crate-str_edit_distance-0.1 (crate (name "str_edit_distance") (vers "0.1.0") (hash "1y3nvag9vmaa8qchx753nhjp373mz6vzyhmqhs40z7hzzk0cssdc")))

(define-public crate-str_indices-0.1 (crate (name "str_indices") (vers "0.1.0") (hash "18wf2zfc2vabim38lp4gsfv1x1h1v9a2afdjcr8vgr2l22qvcwhz")))

(define-public crate-str_indices-0.2 (crate (name "str_indices") (vers "0.2.0") (hash "1pq0v28y92g9qzc3yl4s0nbwkwm8ya1vzl2a6pdalwdwix2crx7z")))

(define-public crate-str_indices-0.3 (crate (name "str_indices") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0r67ayxpncfpr772vlwhcxhijjyf33vj79hxnmz9ry491gb157fa")))

(define-public crate-str_indices-0.3 (crate (name "str_indices") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1am47wn0wmwk69gankd76v1fmb4yvcsw94jdggicbr46qi4alfr8")))

(define-public crate-str_indices-0.3 (crate (name "str_indices") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1y3kp1hc92fz4pzlz09zy0692qlsjnr8ayk6rl0i35a73cxddymd")))

(define-public crate-str_indices-0.4 (crate (name "str_indices") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1h7jl3j0q1ip9gbmiwrkr2x2w1i0lms47s0bc9sf05y8h3x9k4cx") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-str_indices-0.4 (crate (name "str_indices") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1pfnfjmkandxqmq04nqa5nwgsggq8jp8z4xivr9fqhk8j9j620jz") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-str_indices-0.4 (crate (name "str_indices") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1r1b7qk6wbz8dk93l5qnbv74dwcd52dgmjbq2lrql3g5x3fsxvmq") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-str_indices-0.4 (crate (name "str_indices") (vers "0.4.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0p6kggjax1mx0niq22dsm5xq2jvg6l4nyrm8a6f0138yaav7qmg9") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-str_inflector-0.12 (crate (name "str_inflector") (vers "0.12.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1xsfxyfckrjrj7zjb8vqj9ffi7q77i5gh05ys4xb75bnba6q82zc") (features (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-str_iter-1 (crate (name "str_iter") (vers "1.0.0") (hash "1zi9b9469hraw3jh43v9nxhvaj6cj5i32pphjxxp8alxq7has5qz")))

(define-public crate-str_overlap-0.1 (crate (name "str_overlap") (vers "0.1.0") (hash "06jpaaw25k9j08zj6c1ynmgcyaglz4flf2cd21qfyz6f8dfsligw")))

(define-public crate-str_overlap-0.1 (crate (name "str_overlap") (vers "0.1.1") (hash "1ppmg8r57wlwzchmvnmin2anprczqa308rsh88z363gnjxl9xhkn")))

(define-public crate-str_overlap-0.2 (crate (name "str_overlap") (vers "0.2.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.1") (default-features #t) (kind 1)))) (hash "1h1xgghn9v99jarpdwhaa69f94h884xjpb59aqh9k8whi5w7r171")))

(define-public crate-str_overlap-0.3 (crate (name "str_overlap") (vers "0.3.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.1") (default-features #t) (kind 1)))) (hash "0c6qnxdl20ql60r2n1wvmwz4f620vcdvj8l6yrqbngr4kk1vm9n8")))

(define-public crate-str_overlap-0.4 (crate (name "str_overlap") (vers "0.4.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.1") (default-features #t) (kind 1)))) (hash "1ynmd3b9707d17mcxf8wl73yd5fmy7qljrqz8anb98iibyrm9kcb")))

(define-public crate-str_overlap-0.4 (crate (name "str_overlap") (vers "0.4.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.1") (default-features #t) (kind 1)))) (hash "0wgw6mvg9inhv0n3fb78hf5wll8rkjw5j6pzfz8w8fnfdahfsr44") (yanked #t)))

(define-public crate-str_overlap-0.4 (crate (name "str_overlap") (vers "0.4.2") (deps (list (crate-dep (name "autocfg") (req "^1.0.1") (default-features #t) (kind 1)))) (hash "0znm3ri5asxk5v35mmmbiylf16v1d0crsb3hji9r4fh742lh63xs")))

(define-public crate-str_overlap-0.4 (crate (name "str_overlap") (vers "0.4.3") (deps (list (crate-dep (name "autocfg") (req "^1.0.1") (default-features #t) (kind 1)))) (hash "120biddcy49japy39bc0z345yz8g20nhn25xmyn36i52fm4dk8kh")))

(define-public crate-str_rev-0.1 (crate (name "str_rev") (vers "0.1.0") (hash "0k4i34k00gjsfww3gzxwlsxsvir8dxqnckj89ijx417w17y0d9qf")))

(define-public crate-str_sim-0.1 (crate (name "str_sim") (vers "0.1.0") (hash "1hnrlapka56aqb0h1z6jgk8xgwlifvx2b74h98lcqd9bdbzvr3lb")))

(define-public crate-str_sim-0.1 (crate (name "str_sim") (vers "0.1.1") (hash "0lyq250hm2ymlkv58yncp7cfd1a1sp0r0p91qmk6kdzyrmxhh64h")))

(define-public crate-str_sim-0.1 (crate (name "str_sim") (vers "0.1.2") (hash "07hn2388ik39x72csppk3dlkdfxnbgjh2arcpwp81sii0v0yh9bj")))

(define-public crate-str_slug-0.1 (crate (name "str_slug") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "deunicode") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "17y7yaq158v9s5ipwsd4kn13836nxv3rkgbbna0s58bjr14wvb7m")))

(define-public crate-str_slug-0.1 (crate (name "str_slug") (vers "0.1.1") (deps (list (crate-dep (name "blake3") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "deunicode") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0vy8gi5vqa8fi5gfz4l9hzgbqms48j8ky34kb4cw30ag5a60jr87") (features (quote (("hash" "blake3"))))))

(define-public crate-str_slug-0.1 (crate (name "str_slug") (vers "0.1.2") (deps (list (crate-dep (name "blake3") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "deunicode") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "005v0sjxlvhmb3yscysg0yhks3hnc3kzl7ayvk9gvbpscqa5drc2") (features (quote (("hash" "blake3"))))))

(define-public crate-str_slug-0.1 (crate (name "str_slug") (vers "0.1.3") (deps (list (crate-dep (name "blake3") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "deunicode") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1yl5vlw6iq9v3a0xv0nwaj6nbq87xw09ylx0axr9mncj1gm97nl4") (features (quote (("hash" "blake3"))))))

(define-public crate-str_split_mut-0.1 (crate (name "str_split_mut") (vers "0.1.0") (hash "0453z5id8a6c0fnl3y08p022q29w91vhgchkbwdyd7ramrn59jwn") (yanked #t)))

(define-public crate-str_split_mut-0.1 (crate (name "str_split_mut") (vers "0.1.1") (hash "0swdsi802i26vcxd88ng1kffcmxlqgrcwngl1g6k8906w5hkpkzc")))

(define-public crate-str_split_mut-0.1 (crate (name "str_split_mut") (vers "0.1.2") (hash "16413id4accp23d9nwz7axl04xw57bwgrzaqac1fqvhfb75lba85")))

(define-public crate-str_split_mut-0.2 (crate (name "str_split_mut") (vers "0.2.0") (hash "0rr7vs1lglqakgjcypm3p3f25zbzmjhd9lyf6wvq0rka6i9ly2rb")))

(define-public crate-str_split_mut-0.2 (crate (name "str_split_mut") (vers "0.2.1") (hash "0n2yccdnviggafmbary958j8jivzv5ainjir470nxzzx839phz1p")))

(define-public crate-str_split_mut-0.3 (crate (name "str_split_mut") (vers "0.3.0") (hash "1spwah7yb1xw81gn7qzdxspvf25a07pc9q4rjsa7ac9m9nbcppwr") (yanked #t)))

(define-public crate-str_split_mut-0.3 (crate (name "str_split_mut") (vers "0.3.1") (hash "1v0wf4dkzk9d17i0cvmmy2xmyc7xy7y7026vnd8p0jxm15xfssv4") (yanked #t)))

(define-public crate-str_split_mut-0.4 (crate (name "str_split_mut") (vers "0.4.0") (hash "0c9nq04r07b6jzwqfkm701yhzpplz8mcsk8zfadflqa4if3i65q9")))

(define-public crate-str_split_mut-0.4 (crate (name "str_split_mut") (vers "0.4.1") (hash "0jbg7ad8xy6ngz8q3rnq9bdhhd16spl378jf82x4pir94bqb3anf")))

(define-public crate-str_splitter-0.1 (crate (name "str_splitter") (vers "0.1.0") (hash "1xa8cjhc41s26ps047q10rb8qgq288h7vg5dgf700kimifblbiv2")))

(define-public crate-str_splitter-0.1 (crate (name "str_splitter") (vers "0.1.1") (hash "18229847r82y59iky50b55hv7687kq375cjd1hyikzfqyaaf00pc")))

(define-public crate-str_stack-0.1 (crate (name "str_stack") (vers "0.1.0") (hash "1sxl8xd8kiaffsryqpfwcb02lnd3djfin7gf38ag5980908vd4ch")))

(define-public crate-str_strings-0.1 (crate (name "str_strings") (vers "0.1.0") (hash "1c5zp49zwby6b9z6qihigdayz2lsv1xnm9rcvwbmjzjfnp8nhzsa")))

(define-public crate-str_to_bytes-0.1 (crate (name "str_to_bytes") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jmychz69kzy0d08w1zx170rys87q8i0w7fsgiydnba8p4qap7if")))

(define-public crate-str_to_bytes-0.2 (crate (name "str_to_bytes") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jb9xapzw718wqfnadj41lbs1b9wr3vlvd6ps9a7ndblqjva504n")))

(define-public crate-str_to_bytes-0.1 (crate (name "str_to_bytes") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1h7vw60f58dja2hqq296a4f8v7a5cz90yhwyhg024l7sxf7kjqsf")))

(define-public crate-str_to_bytes-0.2 (crate (name "str_to_bytes") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "04ppd8zy5zj3k91518izhkh2m76a61macvmn51k9fhc1pp5w8h5j")))

(define-public crate-str_to_bytes-0.2 (crate (name "str_to_bytes") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1vy6iz7yncnv0ysdcxn77hxim6mbnfhdyak3piwwv3jpzcfcfl6s")))

(define-public crate-str_to_bytes-0.2 (crate (name "str_to_bytes") (vers "0.2.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1nncbnq36jbxs2z3j7gi3b0bvppj9y3zxhnpdav7d5b43m4gwggf")))

(define-public crate-str_to_enum_derive-0.1 (crate (name "str_to_enum_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0r2rgqrx3wjd76cyzs2vck3mlnibg7j4b7l78rrl1j49z347lhip")))

(define-public crate-str_to_enum_derive-0.2 (crate (name "str_to_enum_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (default-features #t) (kind 0)))) (hash "0p931pq80nmfi8glr2xml736xg1i3vvizm6mya2xxhvwp9q3dq8i")))

(define-public crate-str_tools-0.1 (crate (name "str_tools") (vers "0.1.0") (hash "1ynzx6qyimqh7m5ds7z1vsr6cqjrcap0w1x03hw6vkzjkrx5b4hs")))

(define-public crate-str_windows-0.1 (crate (name "str_windows") (vers "0.1.0") (hash "1ylsf6l8lkxn7xdlv3cvvkdv4z23bqgzv8cqsih0lnwlvkkqrph7")))

