(define-module (crates-io st k-) #:use-module (crates-io))

(define-public crate-stk-http-0.2 (crate (name "stk-http") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "stk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "stk-json") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1h2a9rwfj91j812d272ynddvcip005hw3fbfh9h79fjzzd79rqav")))

(define-public crate-stk-http-0.2 (crate (name "stk-http") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "stk") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "stk-json") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "0srijz3fhb2327iihjpbqvxcnapw2py9h9dygd015zfyxpsnh5wm")))

(define-public crate-stk-json-0.2 (crate (name "stk-json") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "stk") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14016axki2lvn6mna6ii6sb47z7qjdda4v5i0gycnwhjf50nfigi")))

(define-public crate-stk-json-0.2 (crate (name "stk-json") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "stk") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0aznz2lygwbi3ji4a5h8kv35kllx9v8v4a4ibaqla45xcwxbb6j5")))

