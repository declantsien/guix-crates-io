(define-module (crates-io st dn) #:use-module (crates-io))

(define-public crate-stdnet-0.0.0 (crate (name "stdnet") (vers "0.0.0") (hash "0jpv13mxvla0wp9vgjpvz4ky3fd1q0msb1fy32qqs6rkajfl6wsz")))

(define-public crate-stdng-0.1 (crate (name "stdng") (vers "0.1.3") (hash "1qnky6806mvvxbfi4bi399zbp38p5sixdr7vnsk33mgnvjzlq1gb")))

(define-public crate-stdng-0.1 (crate (name "stdng") (vers "0.1.4") (hash "0zh5h3fqsbjb3wz6x7dcrldxs3hw124hz6lmni8h1w6jd8ymschs")))

(define-public crate-stdng-0.1 (crate (name "stdng") (vers "0.1.5") (hash "19n2z0wxnnwxj6z85qnfvb9h5f04ca36kwkxvzqr2lchxm7cmg5v")))

(define-public crate-stdng-0.1 (crate (name "stdng") (vers "0.1.6") (deps (list (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a9pwxyan8whj88b8mi8rhiwa265v2r10w1d0dcvqhp59zs2vxlv")))

