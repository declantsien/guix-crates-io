(define-module (crates-io st ei) #:use-module (crates-io))

(define-public crate-steiner-0.0.0 (crate (name "steiner") (vers "0.0.0") (hash "1facsnq0gfwda65gd0klbgqbnzd1bc2xs64zmwmd47acxa7aynby") (yanked #t)))

(define-public crate-steiner-tree-0.0.1 (crate (name "steiner-tree") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "1.8.*") (default-features #t) (kind 0)))) (hash "0zs86a9vnzcj9z9fq7grsvy9vy11d8x2kyf495wrdfp42zcid5ba")))

(define-public crate-steiner-tree-0.0.2 (crate (name "steiner-tree") (vers "0.0.2") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.10") (default-features #t) (kind 0)))) (hash "15xpf751svmmg0kzz5mjcmyyqan6v8ss44l6xkpnpca7gyy3m6gh")))

(define-public crate-steiner-tree-0.0.3 (crate (name "steiner-tree") (vers "0.0.3") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.11") (default-features #t) (kind 0)))) (hash "0z7yfwbsja9vy3wbslp5v17ldfnj4rm6p9kq13njnhcgr1sd6m5c")))

