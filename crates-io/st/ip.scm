(define-module (crates-io st ip) #:use-module (crates-io))

(define-public crate-stipulate-0.0.0 (crate (name "stipulate") (vers "0.0.0") (hash "1wcllal1icmxkxac7kbszc04d1ylfba7f567k2zs1ms37vjrxhgf")))

(define-public crate-stipulate-0.0.1 (crate (name "stipulate") (vers "0.0.1") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "wait-timeout") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ppl1ky58ld1ncypv2wxnqnb36591wx5skqhc2vfyrqphzqmyl6x")))

(define-public crate-stipulate-0.0.2 (crate (name "stipulate") (vers "0.0.2") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "wait-timeout") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dgs1fs1xw4y8j61msx1r1p4v116qgyps4pn53cc7qwxzrfgscvs")))

