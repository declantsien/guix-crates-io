(define-module (crates-io st tx) #:use-module (crates-io))

(define-public crate-sttx-0.1 (crate (name "sttx") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1br7cf8iaswcgmx8cscbb50gmfwbiq9kykrxkmilhcx3wb8w4l43")))

