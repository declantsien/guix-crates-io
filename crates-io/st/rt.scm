(define-module (crates-io st rt) #:use-module (crates-io))

(define-public crate-strtod-0.0.1 (crate (name "strtod") (vers "0.0.1") (hash "1p7n14mn8gkxali9xd3f0d3mdh92ki3fac5k841m6pcin1527j17")))

(define-public crate-strtod2-0.0.1 (crate (name "strtod2") (vers "0.0.1") (hash "1p3nk3clgj53vbalizhnyji9skgxrb367wf6dyfb7mnq7k8yw7q7")))

(define-public crate-strtoint-0.1 (crate (name "strtoint") (vers "0.1.0-alpha") (deps (list (crate-dep (name "test-case") (req "^2.2.2") (default-features #t) (kind 2)))) (hash "1zyr62gk7sw04siav4a04qrjybckq0bzygpjwjqbjb0fnckzyy4x") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-strtoint-0.1 (crate (name "strtoint") (vers "0.1.0") (deps (list (crate-dep (name "test-case") (req "^2.2.2") (default-features #t) (kind 2)))) (hash "1x7jlyi0lj95dyfj4f4rga5hdxdhwisp4nlmnq52wilqn1r1024i") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-strtools-0.1 (crate (name "strtools") (vers "0.1.0") (hash "0pg5jy9w8gbz7ww9sc6rmw7c7ip35imq9ml85g7f02rjr40cs55a")))

(define-public crate-strtools-0.1 (crate (name "strtools") (vers "0.1.1") (hash "1fhv10hjriysnnzbwj25rd1fdj1fg5q5c0x153al2gyrg02mm6js")))

(define-public crate-strtools-0.2 (crate (name "strtools") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0i3bg1qlvb8dz5j2b6q831gr5n982jwx0sdy8iqnc8hjhg58sp1g")))

(define-public crate-strtools-0.3 (crate (name "strtools") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lnqlcb3pgfmxz48vysfalzklhhki9nw5kzq8qb3fkb9p3584xmb")))

(define-public crate-strtools-0.3 (crate (name "strtools") (vers "0.3.1") (deps (list (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dl04n98craz7aji292f4d6jzlavhic5kypq02k583dnk6x43acy")))

