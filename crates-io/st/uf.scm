(define-module (crates-io st uf) #:use-module (crates-io))

(define-public crate-stuff-0.1 (crate (name "stuff") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "sptr") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0q3z4qw5lr2i0bnyl25bcj82mbgr1mzg0cghnca3bnwriy7pjcg2")))

(define-public crate-stuff-0.1 (crate (name "stuff") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "sptr") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1ii7r28rcqgq5gvakd16acb5aybrx3dyih4a70hgb5a5h0vhyv7j")))

(define-public crate-stuff-0.1 (crate (name "stuff") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "sptr") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1gc87fg5kks5mxab3jaaxddwiv97va7qcq459pvwnks6mr7lm761")))

(define-public crate-stuff-0.1 (crate (name "stuff") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "sptr") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "19rhz3312dkaq0nfr6wz1ljnx6b3n19is6pgw1y1sh08z9bw45kg")))

(define-public crate-stuff-0.1 (crate (name "stuff") (vers "0.1.4") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "sptr") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0qyi78g8j2hylk33avkmgfz2vad3yjhh6l40vd1b29vdqbwl46xg")))

(define-public crate-stuff-0.2 (crate (name "stuff") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "sptr") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "15vzg5aixnzx7x821vasijzq385yqx5kryzxfx0sfm70fpl4w3mn") (rust-version "1.34.2")))

