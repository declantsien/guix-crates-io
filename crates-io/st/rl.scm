(define-module (crates-io st rl) #:use-module (crates-io))

(define-public crate-strloin-0.1 (crate (name "strloin") (vers "0.1.0") (hash "10hhij46ijklb5242czn4qwv3ig4qxf2y1r7qdg1llvnvc94vwr2") (rust-version "1.56")))

(define-public crate-strloin-0.1 (crate (name "strloin") (vers "0.1.1") (hash "0nfy5qy1qmj2bjmkn8w3k0ini4xzif6p9wkvali5nnanbqfsj4m9") (rust-version "1.56")))

(define-public crate-strloin-0.1 (crate (name "strloin") (vers "0.1.2") (deps (list (crate-dep (name "beef") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1072k9qpqx9pvkjf940maykphzskhs18nsw3xmcdb96qb71zv9iz") (rust-version "1.56")))

(define-public crate-strloin-0.1 (crate (name "strloin") (vers "0.1.3") (deps (list (crate-dep (name "beef") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "16imv0bwcx82n80n0xrgfmbsmnwy7zssmw8dp8y74qmj1ain2l0x") (rust-version "1.56")))

(define-public crate-strloin-0.1 (crate (name "strloin") (vers "0.1.4") (deps (list (crate-dep (name "beef") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0wwxhl9w8dwmdsp9l88l46y50ix28rxg8nl00kvx8d0py13bwwz6") (rust-version "1.56")))

