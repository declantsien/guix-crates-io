(define-module (crates-io st ow) #:use-module (crates-io))

(define-public crate-stow-0.1 (crate (name "stow") (vers "0.1.0") (hash "0gyr49fj0krrd8zqc9a9dvmkaf9vdpbym1qdhr0m4xkar80ravq9")))

(define-public crate-stow-0.2 (crate (name "stow") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("fs" "io-util" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "1fx76lqqjbk3crgmmnfxx1vd63mblcnhhw9wxn1dl1h7g1rv59bi")))

(define-public crate-stow-squid-0.1 (crate (name "stow-squid") (vers "0.1.1") (deps (list (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1z57yy50cmx0rq13ddq7phi5k5z7h3w6b4sxnxkj2zs90l6dhwjj")))

(define-public crate-stowaway-0.1 (crate (name "stowaway") (vers "0.1.0") (hash "1p9fbmg6pa46dnys3y9riiaspdwlzxbgpn57v5r21ry4bq1avnc8") (yanked #t)))

(define-public crate-stowaway-0.1 (crate (name "stowaway") (vers "0.1.1") (hash "13dcfbg70076fgq7ay5sf4c0ckjzc0vd7q82a6l6rqywbjx4am75") (yanked #t)))

(define-public crate-stowaway-1 (crate (name "stowaway") (vers "1.0.0") (hash "1mwvk2igaqcwcyd39zq89yib8iykmg459y6k4907103qsmzis2r1") (yanked #t)))

(define-public crate-stowaway-1 (crate (name "stowaway") (vers "1.1.0") (hash "158sjvw5z0jd2nl0rgwvq2rxmm0rzqspxnv503cy568m113k59dq") (yanked #t)))

(define-public crate-stowaway-1 (crate (name "stowaway") (vers "1.1.1") (hash "06sqcrdlx267dv6mfhv3h8mv8c01sw4v8ggnnbwl09xgyzzksizk") (yanked #t)))

(define-public crate-stowaway-1 (crate (name "stowaway") (vers "1.1.2") (hash "14f806bdp4jy7svjzzaib1i6m5fwh3jyixzamq73r1l2wxhkvc9j") (yanked #t)))

(define-public crate-stowaway-2 (crate (name "stowaway") (vers "2.0.0") (deps (list (crate-dep (name "rustversion") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1dlzg7gxlfcwxikgqvlfpbnhqd3gmwm4avyvhfj7q2p2kncp5yxa") (yanked #t)))

(define-public crate-stowaway-2 (crate (name "stowaway") (vers "2.1.0") (deps (list (crate-dep (name "rustversion") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "stowaway-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1c1rran5s7vgz7mkh33b4i3y1n9g1niyg83f69r6914ak7g33bif") (features (quote (("derive" "stowaway-derive")))) (yanked #t)))

(define-public crate-stowaway-2 (crate (name "stowaway") (vers "2.2.0") (deps (list (crate-dep (name "rustversion") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "stowaway-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0yinj01v4cl4pkr6928p2b1fwd01qv59h6h3kxmqcd6msl6xaj1l") (features (quote (("derive" "stowaway-derive") ("default" "derive")))) (yanked #t)))

(define-public crate-stowaway-2 (crate (name "stowaway") (vers "2.3.0") (deps (list (crate-dep (name "rustversion") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "stowaway-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1aniklblfxww785j4g1pmbaj625av4gyp0qphq5938bdws010ygf") (features (quote (("derive" "stowaway-derive")))) (yanked #t)))

(define-public crate-stowaway-2 (crate (name "stowaway") (vers "2.3.1") (deps (list (crate-dep (name "rustversion") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "stowaway-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0k70ni4kcq4nvm3bvd9i1ihh4kcip373dy8jq4dfgrgzjililmaq") (features (quote (("derive" "stowaway-derive")))) (yanked #t)))

(define-public crate-stowaway-derive-0.1 (crate (name "stowaway-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "stowaway") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1n0dp4hy492j423x2z4aq94y4p9g9srigvmchcd7c5mcsm6adiyp")))

