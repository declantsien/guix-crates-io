(define-module (crates-io st -m) #:use-module (crates-io))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n59gm60s87mqdx2i18cy83kr1ly4x5ival9mmg7cyrqg9z00rqi")))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0iagwv9hdk63aagd2yk22qzysjjpw6jgvhayxi7f0adz58pyil2w")))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0fbwnwg3ly7qwjkpklmjz9lvkj572jpnwwlmg62dnbbwik44rhcz")))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "193nywlr20pcs0dv3nks8kd6ybcmj53kcckx2zjcb1nplwmd0nhs")))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.4") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1l820pisfi134v3wy0na480wl7rf69kgxzvmgc560ngqb0xb3biw")))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.5") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "17w94hkwhlrgq4mr0rghhigw3is6avxiilvpc3bwnxkcxy0h838c") (yanked #t)))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.6") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0kqc1jgkh12rzl928a7h4s8sshggiw8775xxj9rkpxzm3lx9z75w")))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.7") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1v3wxv76pwfy2wrg07n5wz073mn2723dd216hmmqkvlpymc69p9d") (yanked #t)))

(define-public crate-st-map-0.1 (crate (name "st-map") (vers "0.1.8") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1vdmg8sr3iynkblcd97pl4yslisdnn7lgm4dlpab0xph6lc8k7gh")))

(define-public crate-st-map-0.2 (crate (name "st-map") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0iwhbb0cf8zh9x5hmwc0l2iam1rl1h3ciq3asxbgk8g59g8xalpk")))

(define-public crate-st-map-0.2 (crate (name "st-map") (vers "0.2.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0j4l0s7yhrvk05p5h1i068dbf9f377gil8135i0f4gdia52v6n5s")))

(define-public crate-st-map-0.2 (crate (name "st-map") (vers "0.2.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0is2zfzfp0djbgmvsak0px07gz8fby3rvz080b3va09q791kkd89")))

(define-public crate-st-map-0.2 (crate (name "st-map") (vers "0.2.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static-map-macro") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "18c9d9x6kaa70mnn30zssyprqj37vyr70ls98s9l0ff8bi75r2la")))

