(define-module (crates-io st #{73}#) #:use-module (crates-io))

(define-public crate-st7306-0.8 (crate (name "st7306") (vers "0.8.2") (deps (list (crate-dep (name "embedded-graphics") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pmzag9g4lb75zyf8qylv2w0z69ynhjndsa5za6sdlhy40h2ldba") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

