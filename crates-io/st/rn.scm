(define-module (crates-io st rn) #:use-module (crates-io))

(define-public crate-strng-0.0.0 (crate (name "strng") (vers "0.0.0") (hash "0qs6wd97lf23hr4fc0bn9q4fdpwk93jy5j1rdf4mxz6kgaw1nz54")))

(define-public crate-strng-0.1 (crate (name "strng") (vers "0.1.0") (deps (list (crate-dep (name "containers") (req "^0.9") (default-features #t) (kind 0)))) (hash "1fx52l7sf6gixfkdxm0419iwgb5rrg4cvqgi9zwjj388pf2v86q3")))

(define-public crate-strng-0.1 (crate (name "strng") (vers "0.1.1") (deps (list (crate-dep (name "containers") (req "^0.9") (default-features #t) (kind 0)))) (hash "17x34p41aj9nncrmv2l6zqrdj0i5lmraqa0cvm2l4b7f858lkmbm")))

(define-public crate-strng-0.2 (crate (name "strng") (vers "0.2.0") (deps (list (crate-dep (name "containers") (req "^0.9") (default-features #t) (kind 0)))) (hash "0wf5h5wmi9bzliq9f5h1zw7r346skx3rivr839yv3wq2bdbmfr6b")))

(define-public crate-strng-0.2 (crate (name "strng") (vers "0.2.1") (deps (list (crate-dep (name "containers") (req "^0.9") (default-features #t) (kind 0)))) (hash "0j8mszdagzpys79fi1qcq4y7x2p3ja3rjba36f5ifldd67k9isdj")))

(define-public crate-strng-0.2 (crate (name "strng") (vers "0.2.2") (deps (list (crate-dep (name "containers") (req "^0.9") (default-features #t) (kind 0)))) (hash "0r36sv3246sm1blghrrai05bzldj361sdijabxl09qvc9zbpc56r")))

(define-public crate-strnum_bitwidth-0.1 (crate (name "strnum_bitwidth") (vers "0.1.1") (hash "0vxvnkwnxacv1pha4c4nv3y7sblv9nkwmrc2hcdl6x7mz8iajixs")))

(define-public crate-strnum_bitwidth-0.1 (crate (name "strnum_bitwidth") (vers "0.1.2") (hash "02d5f60sxbs0h3z50zy81c680axif01i7hpflhdh03682vfcns2x")))

