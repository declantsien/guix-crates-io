(define-module (crates-io st c-) #:use-module (crates-io))

(define-public crate-stc-s-0.1 (crate (name "stc-s") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mcvhd50qqysqvd5jqd5ydjyqs3rp2x6irk518lrqn7xlx3cz04p")))

