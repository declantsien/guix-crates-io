(define-module (crates-io st dl) #:use-module (crates-io))

(define-public crate-stdlib-rs-0.0.1 (crate (name "stdlib-rs") (vers "0.0.1") (deps (list (crate-dep (name "man") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ls7pb2m6dfmgvwb1bqxmv1xn25m5dvikw2kc2pi792r6l2rklh9") (features (quote (("build_deps" "man"))))))

(define-public crate-stdlib-rs-0.0.2 (crate (name "stdlib-rs") (vers "0.0.2") (deps (list (crate-dep (name "man") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zyvys18d6j4h3v0fksk0gg3v7a9zb0ncnl9am4cbxrgh9c89m1l") (features (quote (("build_deps" "man"))))))

