(define-module (crates-io st l_) #:use-module (crates-io))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0yb9qf8snx1rvlwdn825lh1ya23h66000x4cxaqlxlqqxvmzn0ry")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1fbfgbyqcfxq3bprppdwr64q3dp423lflgmvdswdhr7xazwf59kr")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0d7s1cf5ri15bmqg1dwcm01v152fs4ga70v1ppx2fcd8aikp63yd")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1lkzfn5zclx2gs30qdkr32mvmbfh3w6kpsqxx7kskfrywdc6xwnv")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.4") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "18nrqicwqdvmkc0mzy2xryr1nm2h9g4b86qdsw7xj3h89jdaqngc")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.5") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1xvnlsr8cii2fbscvlsdkfcw988qqa04a4jvv80zww4yb39br6k5")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.6") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0dchqxg919rg913zbx9kcigga5q7b8dvzx36x715aily74246lnr")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.7") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0l38bmwv471p9k0v9ryfgfr1m0dcc1mfsindni5qfdng8nqqj8bq")))

(define-public crate-stl_io-0.3 (crate (name "stl_io") (vers "0.3.8") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "17ywip9nxnrg6v7pqm0qcbj932cblibqfb5wcp7svlzkci7lh41q")))

(define-public crate-stl_io-0.4 (crate (name "stl_io") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1pkdb8nbsd1mqvxn8vqxd1dpi5pv8blzq410k23ff9ysbgl7n2nk")))

(define-public crate-stl_io-0.4 (crate (name "stl_io") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1wry22amgv2m65fl1991h29h6kp4b9s0br7c7l199bbgig70cbv6")))

(define-public crate-stl_io-0.4 (crate (name "stl_io") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0v6h7hmr36mfyspzx0lcl45qqwmhw22rdq3l0b869549c84i72fw")))

(define-public crate-stl_io-0.5 (crate (name "stl_io") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1s7fhl9ssy29dhbnpi30gqnbz8a3nzr0nn6y3by1jdrai2wz7mpa")))

(define-public crate-stl_io-0.5 (crate (name "stl_io") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.8") (default-features #t) (kind 0)))) (hash "0akvddicpwcmqxlhscqv1s3n4czcw7av4qwk8w95vknnvkpzw7c2")))

(define-public crate-stl_io-0.5 (crate (name "stl_io") (vers "0.5.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.8") (default-features #t) (kind 0)))) (hash "1lfprplskbza8gfdxk1hn7v7zdnwh0y696vsldzgb8r1g9v7ian9")))

(define-public crate-stl_io-0.6 (crate (name "stl_io") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.8") (default-features #t) (kind 0)))) (hash "0p86bwld7mw6vxhy8aldys916pyidkv6miawi2fjdv5k108y8s2w")))

(define-public crate-stl_io-0.7 (crate (name "stl_io") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.9") (default-features #t) (kind 0)))) (hash "1xvw9zs9dn726w4l5q8bmcsxbp66i80wzh7k6an6drijwq1yn86m")))

(define-public crate-stl_parser-0.1 (crate (name "stl_parser") (vers "0.1.0") (hash "1z45lfqwfwryf6kvsqiqb4c6zs5nki7ifx7x4v160c2kv4vv661g")))

(define-public crate-stl_rotate-0.1 (crate (name "stl_rotate") (vers "0.1.0") (hash "1qc6cs1qr84y550bx6qd01d0nzqnc67frkdq6l4b2abi5qm518cx")))

(define-public crate-stl_rotate-0.2 (crate (name "stl_rotate") (vers "0.2.0") (hash "02da2pppfx3bx7qp9bc30vxxhfmdp2lyj175gvhjfhp29a7ah375")))

