(define-module (crates-io st rs) #:use-module (crates-io))

(define-public crate-strs-0.1 (crate (name "strs") (vers "0.1.0") (hash "1fa3dxxmg27xm7xva3kbf4dn3b8j16mzplnblak4azq5bgs885mz")))

(define-public crate-strs-0.1 (crate (name "strs") (vers "0.1.1") (hash "17v4sl192v1bfrwr3bd42fy9d901f52j035cznayms469byk8n2a")))

(define-public crate-strs-0.1 (crate (name "strs") (vers "0.1.2") (deps (list (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0hpiiq8ca8m8vjysplaz56jh35qyc8y1wazf3manv5g2aq7l0d2c")))

(define-public crate-strs-0.1 (crate (name "strs") (vers "0.1.3") (deps (list (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "16si2x2yf8583jmrl63mr46cm72ip9f4h5gli800k27k9b5gsdq1")))

(define-public crate-strs_tools-0.1 (crate (name "strs_tools") (vers "0.1.0") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1ajjq85ykw6yf9v9fv2na5i0qfvgihclsb2pslkbkh5ldj8vpdpg") (features (quote (("use_std") ("use_alloc") ("split") ("parse" "split") ("number_paraser" "lexical") ("indentation") ("full" "use_std" "indentation" "parse" "split" "number_paraser") ("default" "use_std" "indentation" "parse" "split" "number_paraser"))))))

(define-public crate-strs_tools-0.1 (crate (name "strs_tools") (vers "0.1.5") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "07l41q95j5nxr6zj9b2n6clfcnb3xcfzrf38jmszd689463mvhm7") (features (quote (("use_std") ("use_alloc") ("split") ("parse_number" "lexical") ("parse" "split" "parse_number") ("indentation") ("full" "use_std" "indentation" "parse" "split" "parse_number") ("default" "use_std" "indentation" "parse" "split" "parse_number"))))))

(define-public crate-strs_tools-0.1 (crate (name "strs_tools") (vers "0.1.6") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1s306d3k1slcq6ci2lm095y9y1dx041r720r2ml4gpc58spwmi0z") (features (quote (("use_std") ("use_alloc") ("split") ("parse_number" "lexical") ("parse" "split" "parse_number") ("indentation") ("full" "use_std" "indentation" "parse" "split" "parse_number") ("default" "use_std" "indentation" "parse" "split" "parse_number"))))))

(define-public crate-strs_tools-0.1 (crate (name "strs_tools") (vers "0.1.7") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0mdvdwvq9bkf89414qjs1lsjgblyp49a0kz4j5skwfplnhy6nid7") (features (quote (("use_std") ("use_alloc") ("split") ("parse_request" "split" "isolate") ("parse_number" "lexical") ("isolate") ("indentation") ("full" "use_std" "use_alloc" "indentation" "isolate" "parse_request" "parse_number" "split") ("default" "use_std" "indentation" "isolate" "parse_request" "parse_number" "split"))))))

(define-public crate-strs_tools-0.1 (crate (name "strs_tools") (vers "0.1.8") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0814sgg9n80r3fdyahszpqryrisnqhl43b01kg7zna3vlk753kz6") (features (quote (("use_std") ("use_alloc") ("split") ("parse_request" "split" "isolate") ("parse_number" "lexical") ("isolate") ("indentation") ("full" "use_std" "use_alloc" "indentation" "isolate" "parse_request" "parse_number" "split") ("default" "use_std" "indentation" "isolate" "parse_request" "parse_number" "split"))))))

(define-public crate-strs_tools-0.2 (crate (name "strs_tools") (vers "0.2.0") (deps (list (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "15wch8as2c1c13p98iyqz0wkfn6nfga6v100gsv3prwx0xr2bvw6") (features (quote (("use_alloc") ("string_split") ("string_parse_request" "string_split" "string_isolate") ("string_parse_number" "lexical") ("string_isolate") ("string_indentation") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_parse_number") ("enabled") ("default" "enabled" "string_indentation" "string_parse_number"))))))

(define-public crate-strs_tools-0.3 (crate (name "strs_tools") (vers "0.3.0") (deps (list (crate-dep (name "former") (req "~0.2.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.4.0") (default-features #t) (kind 2)))) (hash "11s3r0xjc1d49wgralm58w8n6z4wf80dw607m7zs13a0ckmh5dnp") (features (quote (("use_alloc") ("string_split") ("string_parse_request" "string_split" "string_isolate") ("string_parse_number" "lexical") ("string_isolate") ("string_indentation") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.4 (crate (name "strs_tools") (vers "0.4.0") (deps (list (crate-dep (name "former") (req "~0.3.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.5.0") (default-features #t) (kind 2)))) (hash "1x6q7zcncpifzq5hny1fxw6f4kiw0mmzk8zyfdsakcv9ijly6jzg") (features (quote (("use_alloc") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.5 (crate (name "strs_tools") (vers "0.5.0") (deps (list (crate-dep (name "former") (req "~0.4.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "1w90g2jjjnam2n47rjx9jy10c542smmnbr9a147ry3a077ih66rb") (features (quote (("use_alloc") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.6 (crate (name "strs_tools") (vers "0.6.0") (deps (list (crate-dep (name "former") (req "~0.5.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "1m74n5y17s53xb794csqwnpz8584h9rqsxxvgs8wnqrp0k6qf0wj") (features (quote (("use_alloc") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.7 (crate (name "strs_tools") (vers "0.7.0") (deps (list (crate-dep (name "former") (req "~0.8.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "17amvjcvfhsp6gzab4sr9mrhlwxkin3naazig0mm81hc314nvs9b") (features (quote (("use_alloc") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.8 (crate (name "strs_tools") (vers "0.8.0") (deps (list (crate-dep (name "former") (req "~0.10.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "1whvp21iy4g3afwsaq6q7i00sgiwmcm9y3zmf7x6q35rzwzyzzdw") (features (quote (("use_alloc") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.9 (crate (name "strs_tools") (vers "0.9.0") (deps (list (crate-dep (name "former") (req "~0.11.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "1clbdagcvk1ffryy5k187kqrkzivr657qv2viddahqasibswjvvd") (features (quote (("use_alloc") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.10 (crate (name "strs_tools") (vers "0.10.0") (deps (list (crate-dep (name "former") (req "~0.12.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "1l0vnk3bm3l93x89fckkdfpi8yp1lkm8x1ak7hnv3zdmkh7dj5kz") (features (quote (("use_alloc") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.11 (crate (name "strs_tools") (vers "0.11.0") (deps (list (crate-dep (name "former") (req "~0.16.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "19x2ldsx6cd5x51vf45w6dgaismrv805ghx3xsr9ljligirz38li") (features (quote (("use_alloc" "no_std") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strs_tools-0.12 (crate (name "strs_tools") (vers "0.12.0") (deps (list (crate-dep (name "former") (req "~2.0.0") (features (quote ("default"))) (kind 0)) (crate-dep (name "lexical") (req "~6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.9.0") (default-features #t) (kind 2)))) (hash "0720hq9z67fyp0xy7bq0hcpggwd9ax56f2cklf8nybjx8pvffkjk") (features (quote (("use_alloc" "no_std") ("string_split" "string_parse_request" "enabled") ("string_parse_request" "string_split" "string_isolate" "enabled") ("string_parse_number" "lexical" "enabled") ("string_isolate" "enabled") ("string_indentation" "enabled") ("no_std") ("full" "enabled" "use_alloc" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split") ("enabled") ("default" "enabled" "string_indentation" "string_isolate" "string_parse_request" "string_parse_number" "string_split"))))))

(define-public crate-strscan-0.1 (crate (name "strscan") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0d6d81xi829gqar9cfvwp81q71iqgkkfgkk698n2vx05q262m34b")))

(define-public crate-strscan-0.1 (crate (name "strscan") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0gzppkfq9f3g50gy4f9bjrc633s6m2m2cmnyhdz8giyywjbdn41i")))

(define-public crate-strseq-0.1 (crate (name "strseq") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)))) (hash "0isc2qfdl5hv05qb16qa4lxndxvwwj917k5fmnni87i3pyci0w4k") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strseq-0.1 (crate (name "strseq") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)))) (hash "10xnx1l06ipcl4zlycm8rwvp5ghpbaglnm6lgv2by81cy8cjgax7") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strsim-0.1 (crate (name "strsim") (vers "0.1.0") (hash "1728jqm5gi65m6g30ms33zlbs1r0bfhbgpjjanq9l0i0x7lqdr1j")))

(define-public crate-strsim-0.1 (crate (name "strsim") (vers "0.1.1") (hash "15bvbxqip56l8g5vr14clmixnr09r5cmx7m9gj5zy7zgl3qgahn1")))

(define-public crate-strsim-0.2 (crate (name "strsim") (vers "0.2.0") (hash "0733bxbcrp40gf5cbkfcv1plgavakm8xar3g3kahp5qigzwj6wp8")))

(define-public crate-strsim-0.2 (crate (name "strsim") (vers "0.2.1") (hash "1mrni0k41hz3lfcwwzip7k7sraqngb0pbnj616whqj27wriznz4b")))

(define-public crate-strsim-0.2 (crate (name "strsim") (vers "0.2.2") (hash "1vkwdlikc1gmljq9zh7ll269j0d305swg0q9pfzlm3x2r52flqnk")))

(define-public crate-strsim-0.2 (crate (name "strsim") (vers "0.2.3") (hash "1il0b0zlmdp6mjvb05m4x88sr18y16sjlk3xsy8lf6iqjxj0fy2m")))

(define-public crate-strsim-0.2 (crate (name "strsim") (vers "0.2.4") (hash "0xcd92iy91d59p9cqz7s0ldayl13zsgcxzaqnhhhnfam0garhbzm")))

(define-public crate-strsim-0.2 (crate (name "strsim") (vers "0.2.5") (hash "1y9fw4nzs3m7krf2v49ai076gwpsf4aa3bpdhpb0x6kgddf46xni")))

(define-public crate-strsim-0.3 (crate (name "strsim") (vers "0.3.0") (hash "09dq9a22cx3myjrq4ykz8irk31jrq7mcpxbd3bnrbl546qn3mmz4")))

(define-public crate-strsim-0.4 (crate (name "strsim") (vers "0.4.0") (hash "1nc941gspbswrh95vbd4xs3410dvgiwrxiqjlwhj5as90cha5mmi")))

(define-public crate-strsim-0.4 (crate (name "strsim") (vers "0.4.1") (hash "11g9rdaqh6yqhb28vcj8j5zdq1xrv9ij316bqjjk8rpdbifmfpqd")))

(define-public crate-strsim-0.5 (crate (name "strsim") (vers "0.5.0") (hash "0a9ib4f4lyavy4yc6ma10g2x2whd2vh9fk96y8c013m4qipjxap9")))

(define-public crate-strsim-0.5 (crate (name "strsim") (vers "0.5.1") (hash "0bj4fsm1l2yqbfpspyvjf9m3m50pskapcddzm0ji9c74jbgnkh2h")))

(define-public crate-strsim-0.5 (crate (name "strsim") (vers "0.5.2") (hash "0z3zzvmilfldp4xw42qbkjf901dcnbk58igrzsvivydjzd24ry37")))

(define-public crate-strsim-0.6 (crate (name "strsim") (vers "0.6.0") (hash "151ngha649cyybr3j50qg331b206zrinxqz7fzw1ra8r0n0mrldl")))

(define-public crate-strsim-0.7 (crate (name "strsim") (vers "0.7.0") (hash "0l7mkwvdk4vgnml67b85mczk466074aj8yf25gjrjslj4l0khkxv")))

(define-public crate-strsim-0.8 (crate (name "strsim") (vers "0.8.0") (hash "0sjsm7hrvjdifz661pjxq5w4hf190hx53fra8dfvamacvff139cf")))

(define-public crate-strsim-0.9 (crate (name "strsim") (vers "0.9.0") (hash "08an22v4r62aj3qinl8ydjcbdxdswyzbc8xlnjkhrww7hm3j7gm7")))

(define-public crate-strsim-0.9 (crate (name "strsim") (vers "0.9.1") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0hvnnpaywxiphafrb2z2yinq8nnvc9p7pm2cplxsjfj2n5m6db1l")))

(define-public crate-strsim-0.9 (crate (name "strsim") (vers "0.9.2") (hash "1xphwhf86yxxmcpvm4mikj8ls41f6nf7gqyjm98b74mfk81h6b03")))

(define-public crate-strsim-0.9 (crate (name "strsim") (vers "0.9.3") (hash "0k497pv882qn3q977ckznm13vxx927g8s1swvcv68j3c1pccwik4")))

(define-public crate-strsim-0.10 (crate (name "strsim") (vers "0.10.0") (hash "08s69r4rcrahwnickvi0kq49z524ci50capybln83mg6b473qivk")))

(define-public crate-strsim-0.10 (crate (name "strsim") (vers "0.10.1") (hash "13f0bnp7428g6bm817ixyh9p3sig8k4v5xl3xvdpisrl8prsdg6c") (yanked #t)))

(define-public crate-strsim-0.11 (crate (name "strsim") (vers "0.11.0") (hash "00gsdp2x1gkkxsbjxgrjyil2hsbdg49bwv8q2y1f406dwk4p7q2y")))

(define-public crate-strsim-0.11 (crate (name "strsim") (vers "0.11.1") (hash "0kzvqlw8hxqb7y598w1s0hxlnmi84sg5vsipp3yg5na5d1rvba3x") (rust-version "1.56")))

(define-public crate-strsplit-0.1 (crate (name "strsplit") (vers "0.1.0") (hash "0lh8rahhzs5ddzsiajxnkf8j9wnra3ha1b9kdfhpqspn554q96fh")))

(define-public crate-strsplit-0.1 (crate (name "strsplit") (vers "0.1.1") (hash "1yvgkg23nq8i0baqj2kls54k9sy9sxh0bwanjlmsb673zl7fxb9g")))

(define-public crate-strstr-0.0.0 (crate (name "strstr") (vers "0.0.0") (deps (list (crate-dep (name "constptr") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cowstr") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "19f6agkq9163zsq6a67qhl3viys45g19109b5xgpvx0cjn11ly2m")))

