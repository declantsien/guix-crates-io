(define-module (crates-io st _r) #:use-module (crates-io))

(define-public crate-st_ring_buffer-0.1 (crate (name "st_ring_buffer") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)))) (hash "0havwsgavc83dzkbdbq3x7fkgyli5di2sdirpiqmyxy0nw628xhl")))

(define-public crate-st_ring_buffer-0.2 (crate (name "st_ring_buffer") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)))) (hash "1ah7l6z1bh5l72h8cscgrli0gdd624fx3z4bi3lrrxdzs06n1pn5") (features (quote (("std") ("default"))))))

(define-public crate-st_ring_buffer-1 (crate (name "st_ring_buffer") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2") (default-features #t) (kind 2)))) (hash "1sl6x2ch54d4w77jdf80763pydxafszmk9fxi8rpnrfivmcx4zbg") (features (quote (("std") ("default"))))))

