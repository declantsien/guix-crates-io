(define-module (crates-io st dm) #:use-module (crates-io))

(define-public crate-stdm-0.1 (crate (name "stdm") (vers "0.1.0") (deps (list (crate-dep (name "semicolon") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0aydsdw4nrj0j7ak9gbz3a33ry84msyc12kbv8p256nx1b37cfjy")))

(define-public crate-stdmacros-0.1 (crate (name "stdmacros") (vers "0.1.0") (hash "0cgv4jwzvg1czsfhpg8pkcvfcr21pa12s12ilmbgz4759p0x74hh")))

(define-public crate-stdmacros-0.1 (crate (name "stdmacros") (vers "0.1.1") (hash "1l5j02ajkfcw0zb6gkz9ikcn0is83lav39qqr84dhl75vnj4yc77")))

