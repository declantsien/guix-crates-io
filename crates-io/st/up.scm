(define-module (crates-io st up) #:use-module (crates-io))

(define-public crate-stupid-add-0.1 (crate (name "stupid-add") (vers "0.1.0") (hash "1l44fzlvidv44sfjg8ix42zdi2dg93wxlb21ccybi8gnjxzslmah")))

(define-public crate-stupid-from-num-0.1 (crate (name "stupid-from-num") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.27") (default-features #t) (kind 0)))) (hash "0qjpgqh97mwk5dyaibprrw0m66bnn9anb519cybzb85n2kj1bp29")))

(define-public crate-stupid_simple_dotenv-0.1 (crate (name "stupid_simple_dotenv") (vers "0.1.0") (hash "0r9r1hc32im0pvq6mqn8kikzs8jbc6yvs773pl5kkg996fi7j4va")))

(define-public crate-stupid_simple_dotenv-0.1 (crate (name "stupid_simple_dotenv") (vers "0.1.1") (hash "0m91d1xaj90ww5iq73fnrlxsnldg4cgwdlyhqnjxwmaz1l1cgapk")))

(define-public crate-stupid_simple_dotenv-0.2 (crate (name "stupid_simple_dotenv") (vers "0.2.0") (hash "171hbz3nz2d69lwf6g8q8r5mq3dg3rc93xvyb27q2x7bylba25jz")))

(define-public crate-stupid_simple_dotenv-0.2 (crate (name "stupid_simple_dotenv") (vers "0.2.1") (hash "07spv7dfnghyy2mnjrbifwf4gv3md8kfpxg6vz65nv9zwp9192df")))

(define-public crate-stupid_simple_dotenv-0.2 (crate (name "stupid_simple_dotenv") (vers "0.2.3") (hash "0kmvac28s7pm30ccnypyjb0b5cfza23y2dgf0lf31g74zqlb4w0r")))

(define-public crate-stupid_simple_dotenv-0.2 (crate (name "stupid_simple_dotenv") (vers "0.2.4") (hash "16yy4cymnpxdcmm0g31iksjyjl8ri7qlmxqbshzl0a753wdfjh6n")))

(define-public crate-stupid_stuff-0.1 (crate (name "stupid_stuff") (vers "0.1.0") (hash "1x3rx5kl4sx7lv6b48amsin6771i33z9k0vjnf4q7lj213k3kcmm") (yanked #t)))

(define-public crate-stupid_utils-0.1 (crate (name "stupid_utils") (vers "0.1.0") (hash "0rxl8xj4l9qcn5hklhwhxbnf83nd2v7psxq8lf440xknn22lyz2f")))

(define-public crate-stupid_utils-0.1 (crate (name "stupid_utils") (vers "0.1.1") (hash "0f5a48q06cl74mdhfiiy2g9v1wkkz5vl4wjvgrbpscy2wsf2vxwi")))

(define-public crate-stupid_utils-0.2 (crate (name "stupid_utils") (vers "0.2.1") (hash "0asd5mrgk76ny4spd3pncgn99br0nz076ma6sch607klwypyjcc8")))

(define-public crate-stupid_utils-0.2 (crate (name "stupid_utils") (vers "0.2.2") (hash "19nj31z8i5i8spn70yncrilamz2rxc47s9k1y1is2c5cgqzl3zqv")))

(define-public crate-stupid_utils-0.2 (crate (name "stupid_utils") (vers "0.2.3") (hash "1xarcqywh8mn4i3jf7g7caqz47yc3i7jxkx0d7y58nzilmrw66if")))

(define-public crate-stupid_utils-0.2 (crate (name "stupid_utils") (vers "0.2.4") (hash "0srw1j7y1f9yxsnxzdh4yjwnr28ylk2njz8gmbd190m52bgxqlc3")))

(define-public crate-stupid_utils-0.2 (crate (name "stupid_utils") (vers "0.2.5") (hash "0j97smdql3r0ls019rqh4a3w3vxrsim8zszy6v20hr4byb55cpks")))

(define-public crate-stupid_utils-0.2 (crate (name "stupid_utils") (vers "0.2.6") (hash "0xx4816djg6c1q0sm117xxnd57vsr83ynqnbkn0p6zh7pi027fji")))

(define-public crate-stupidalloc-0.1 (crate (name "stupidalloc") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.6.4") (features (quote ("windows_dpi_awareness" "windows_visual_styles"))) (optional #t) (default-features #t) (kind 0)))) (hash "0n6lc4nabbl59dfajjwzm9mxf1wwy94pxqmkm679ksjjvfy15csb") (features (quote (("interactive" "native-dialog"))))))

(define-public crate-stupidalloc-0.2 (crate (name "stupidalloc") (vers "0.2.0") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (features (quote ("ahash" "inline-more"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.6.4") (features (quote ("windows_dpi_awareness" "windows_visual_styles"))) (optional #t) (default-features #t) (kind 0)))) (hash "175lgkpqxasr5kkjahyphng975dhlazpgfndck4zhfjqrij9z47v") (features (quote (("logging") ("interactive" "native-dialog") ("graphics" "minifb") ("always-graphics" "graphics"))))))

(define-public crate-stupidalloc-0.2 (crate (name "stupidalloc") (vers "0.2.1") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (features (quote ("ahash" "inline-more"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.6.4") (features (quote ("windows_dpi_awareness" "windows_visual_styles"))) (optional #t) (default-features #t) (kind 0)))) (hash "18zrfpqql2lks3dgin4r0mb9vwqpnkhy3w8zjbbx01i3cbm4g3b0") (features (quote (("logging") ("interactive" "native-dialog") ("graphics" "minifb") ("always-graphics" "graphics"))))))

(define-public crate-stupiderators-0.1 (crate (name "stupiderators") (vers "0.1.0") (hash "1yqfka6z3npcb8cld1yiqc2z9k14sbqbzinyydrgc1cf5qjxafd4")))

(define-public crate-stupiderators-0.2 (crate (name "stupiderators") (vers "0.2.0") (hash "0lplpw7l0kffid6ik30fdfckbm6wb17rs6x8r5ib6bbqcpzn429v")))

(define-public crate-stupiderators-0.2 (crate (name "stupiderators") (vers "0.2.1") (hash "0836ljghm8qlhfrgql5cqdsbj4nsill4lvhibyaw4plz5cff4g9x")))

