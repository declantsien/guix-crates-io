(define-module (crates-io st rp) #:use-module (crates-io))

(define-public crate-strp-0.1 (crate (name "strp") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "strp_macros") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "042drxmjcknq97wis5hvwsghxw4vhn7sz8hy5m39k8n46qz34j3l") (features (quote (("no_std"))))))

(define-public crate-strp-0.1 (crate (name "strp") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "strp_macros") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1szs33f9p2c39z8s6x0xxw5g7nayf8qgs8g1djgbvrsjbzjsf2zl") (features (quote (("no_std"))))))

(define-public crate-strp-0.2 (crate (name "strp") (vers "0.2.0") (deps (list (crate-dep (name "strp_macros") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0diazccvcsayq88a3jh2i8rqjgiqay3f88jpy85rpcmihf7fk9jw") (features (quote (("no_std"))))))

(define-public crate-strp-0.2 (crate (name "strp") (vers "0.2.1") (deps (list (crate-dep (name "strp_macros") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1cx4acrx7x9zsckyyqx2i6a86q64fcs2slr1dkag23fjlnixjw9p") (features (quote (("no_std"))))))

(define-public crate-strp-0.2 (crate (name "strp") (vers "0.2.2") (deps (list (crate-dep (name "strp_macros") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "19nfkhnfjh4xgp3ikncdpy8y643h6m8kcazcks2j6qda64szk8sr") (features (quote (("no_std"))))))

(define-public crate-strp-0.3 (crate (name "strp") (vers "0.3.0") (deps (list (crate-dep (name "strp_macros") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "17qbyqxf9rzsjjll2m2apsva6b1j4pcc27jqcgp6mnrdbvz7hqna") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-0.3 (crate (name "strp") (vers "0.3.1") (deps (list (crate-dep (name "strp_macros") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0p9ymnzsis2fcq6gxazxchxiqzls7za2b0m765mbvmchcjsd7f8g") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-0.3 (crate (name "strp") (vers "0.3.2") (deps (list (crate-dep (name "strp_macros") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1z391538nc2vjsxdfz6r1lj35v6w2ydzyzij7cqzbl0klrsxsv8m") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-0.3 (crate (name "strp") (vers "0.3.3") (deps (list (crate-dep (name "strp_macros") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0d295rpbby1mdbm48a2nqvcny6v9amfqlvqw2kj35cl3q6nh6iw8") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-0.3 (crate (name "strp") (vers "0.3.4") (deps (list (crate-dep (name "strp_macros") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "12pd2zdl70v02xlbrffmghb2i2q9q7x3l6zxhz7xncd62xli6068") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-0.3 (crate (name "strp") (vers "0.3.5") (deps (list (crate-dep (name "strp_macros") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0kyzcly6f4gpbwsjc4xj696f513v9d1vx6c433vcqnn3dl9dbfia") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-0.3 (crate (name "strp") (vers "0.3.6") (deps (list (crate-dep (name "strp_macros") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0q9m72sq87201wa531ankf5y3wg4habai1hyzdz933yyidcwnwr8") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-1 (crate (name "strp") (vers "1.0.0") (deps (list (crate-dep (name "strp_macros") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1lnl5mad4ad83liqsfc859951mhdxbf7zvx1vwn2irm5di7588c5") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-1 (crate (name "strp") (vers "1.0.1") (deps (list (crate-dep (name "strp_macros") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0fs1smgz91lzizzavpzxq4iy1h2s5j6xhjlfyhglla9ra3yj9pds") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-1 (crate (name "strp") (vers "1.1.0") (deps (list (crate-dep (name "strp_macros") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1xffpgzianj171f2cabprkwan5nhw7m06dn5b6q7v5chvs4140xk") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-1 (crate (name "strp") (vers "1.1.1") (deps (list (crate-dep (name "strp_macros") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1r0kpg1lbj99nq8ig3x048qipg5m30syqkph1zsk02dijryr29iw") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp-1 (crate (name "strp") (vers "1.1.2") (deps (list (crate-dep (name "strp_macros") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "07c84alazdc9917qkqagxr5min38gf7jypqmbhy4llycwd6pbg6i") (features (quote (("std" "strp_macros/std") ("default" "std"))))))

(define-public crate-strp_macros-1 (crate (name "strp_macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f8ah6lb092qxvazbclgq33dpqhan7w7ask72ff94qsfnfqx21dc")))

(define-public crate-strp_macros-1 (crate (name "strp_macros") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qi5faihmr32v7p36vani92ndbvi4qm1dhnnrh2lxpnvnpybds8f")))

(define-public crate-strp_macros-2 (crate (name "strp_macros") (vers "2.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18s7h7i829y6x7a667hq9jjgwfnchfsknnzhjzlr9bn1bjlpld3f") (features (quote (("no_std"))))))

(define-public crate-strp_macros-2 (crate (name "strp_macros") (vers "2.0.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i73aqgjkb53z9jr5czzq19iqdz70r2zs4891nq760avyika55kg") (features (quote (("no_std"))))))

(define-public crate-strp_macros-2 (crate (name "strp_macros") (vers "2.0.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bc2kmjl63w92nlhmhq8n3gima33vfihfvvdg40g1xry6al3gsn3") (features (quote (("no_std"))))))

(define-public crate-strp_macros-2 (crate (name "strp_macros") (vers "2.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vhwdchm04b7mfaq4x4z6a9c6b80rd8pcfcwy9mbifd0bhdsvlfi") (features (quote (("std"))))))

(define-public crate-strp_macros-2 (crate (name "strp_macros") (vers "2.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05lr0lkxzjdf3y1wmk4jdzxymphys8v6b734qdzbf0kvxpifinmb") (features (quote (("std"))))))

(define-public crate-strp_macros-3 (crate (name "strp_macros") (vers "3.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fy9k2l84mv1p4kgyxa0dwwxpqfv2q8p17hnrfqzyx6k9f63r3f0") (features (quote (("std"))))))

(define-public crate-strp_macros-3 (crate (name "strp_macros") (vers "3.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ia1s84kmv3cb8vkc669s24s3cypnbqzlk3zsayvlf4cdn7pxmb1") (features (quote (("std"))))))

(define-public crate-strpool-1 (crate (name "strpool") (vers "1.0.0") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 1)))) (hash "0zv23dd1khakjjr34z2549pn5pyhdaj9877738jw3cp9nmbjwkck")))

(define-public crate-strpool-1 (crate (name "strpool") (vers "1.0.1") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 1)))) (hash "13y86s3qapbkahdzdps4g1y3hqjdz6nlk1b1w3k718kcmj3prnn5")))

(define-public crate-strpool-1 (crate (name "strpool") (vers "1.0.2") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 1)))) (hash "1z55p8yxr5gbqn05779mvn1927zi3d3jpvinxs8glcivbzs923fy")))

(define-public crate-strpool-1 (crate (name "strpool") (vers "1.0.3") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 1)))) (hash "1ccy7dn25wmfmj96hma5jd0q08b2z1py1hvbspsr8xil29qhprna")))

(define-public crate-strpool-1 (crate (name "strpool") (vers "1.0.4") (deps (list (crate-dep (name "ahash") (req "^0.8") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 1)))) (hash "1vwn0272b3aa2ksr09j1v9g2b2xws5vvvi650pna5fpqzh84q738")))

(define-public crate-strpool-1 (crate (name "strpool") (vers "1.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 1)))) (hash "1km8j7l6m3nk5mb8zalkz1bcdk9xbj7kzhbj98h0kyh8xh554y88")))

(define-public crate-strpool-1 (crate (name "strpool") (vers "1.1.1") (deps (list (crate-dep (name "cityhasher") (req "^0.1") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1hj0qq9diivddkmz30x8qgyk2d3gczgsqxc3rmqx0qm95b6blf3q") (features (quote (("std")))) (v 2) (features2 (quote (("serde" "dep:serde" "std"))))))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "02nmnv2704k0s5hx43aqkm5i4lvwk5iz2jyf23f3lj90si95wbgl")))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0varxvljxnshfb7vid5d4g33b90dg3xl45v807i58fnf1ds0gpr6")))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0dm07074j5masplsbcmycfzjzlqwn9par033k58hkadnv3d47wdk")))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ql14vgc9b0i7fmalyhf6jyzq0jlqszlmjrd2cb928vaa8xfhxcc")))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "09z1vbaarbi2gmh10csvq2knv1hk2acclhli9fv056ksxnmr8xrx") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qimy0fpvphr4h4ap3whgxyr30pamcmw0n41f3jhp0z3385102n7") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0m6d83fd4zlmyflw6pzzxln2iz2dgwmbiclfcpnj7xgk6pcz27z1") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1rkj4jm74rn18lbxah6sjgzzsh1v5s1a3r757zji1b85zw3z4jy7") (features (quote (("wasm" "wasm-bindgen")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0hrcihvsnw76yppf1935p1y4i8aqxfi19dj6z6ginv9wngs9zq41") (features (quote (("wasm" "wasm-bindgen")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.9") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "04va3fh11nlnci5q0d4jz9jbqz87ay6xbgqmrjk0xid7m6rpq8ky") (features (quote (("wasm" "wasm-bindgen")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.1 (crate (name "strprox") (vers "0.1.10") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "07d39s5nb3cg9cb3vis0kj5d597jlrzmi3b2yzni72h1g74f7919") (features (quote (("wasm" "wasm-bindgen")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.2 (crate (name "strprox") (vers "0.2.0") (deps (list (crate-dep (name "js-sys") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "12nfmzi8z1qx76x3038rwpw3hkzv88kd82gjn3krnkqbgk043i4x") (features (quote (("wasm" "wasm-bindgen" "js-sys")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3 (crate (name "strprox") (vers "0.3.0") (deps (list (crate-dep (name "js-sys") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1nd4qqap0d1nnl0wzymikw9m4larfznxlw19hrfly8zfnc7z12bb") (features (quote (("wasm" "wasm-bindgen" "js-sys")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3 (crate (name "strprox") (vers "0.3.1") (deps (list (crate-dep (name "js-sys") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "08js35zqw3k66hsrvd4m92pxxmgp3mykzmchc3dm2al8k6cicx5c") (features (quote (("wasm" "wasm-bindgen" "js-sys")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3 (crate (name "strprox") (vers "0.3.2") (deps (list (crate-dep (name "js-sys") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0ja4pf4hnyk7l62l9ajrr8jvvn7lvj7yd4gz75d16zjq1iyjzzlx") (features (quote (("wasm" "wasm-bindgen" "js-sys")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strprox-0.3 (crate (name "strprox") (vers "0.3.3") (deps (list (crate-dep (name "js-sys") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1ryzjw70clzxnhnxy47xnzlzm87rl66sgm4hm020shab7bb4h060") (features (quote (("wasm" "wasm-bindgen" "js-sys")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-strptime-0.1 (crate (name "strptime") (vers "0.1.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0j7aaxs5l3swmzl5kipc4fkpgzmp22d92c23pj20zqirxry1bwr0") (rust-version "1.70")))

(define-public crate-strptime-0.2 (crate (name "strptime") (vers "0.2.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0dcip8lhj9p26n0bk2mkkv4clxxip0wh1j306r9449cv7621zzsy") (rust-version "1.70")))

(define-public crate-strptime-0.2 (crate (name "strptime") (vers "0.2.1") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1dv09mqap6533vb62di4d5f9pciqsh9zsbqm4p1l6zq92fyn5qlx") (rust-version "1.70")))

(define-public crate-strptime-0.2 (crate (name "strptime") (vers "0.2.2") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "18vpnlakf1hvnrcm00in2rl7sdvbbi1z8hy9wmshkxl7xz5lcj6j") (rust-version "1.70")))

(define-public crate-strptime-0.2 (crate (name "strptime") (vers "0.2.3") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0fn4ib3isz37l65yym6zvvfv7zc0ypp60avnjq3dr6w9lb4457rw") (rust-version "1.70")))

(define-public crate-strptime-1 (crate (name "strptime") (vers "1.0.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bxpidlscbdj9zmcwkszakx5gyqvjj1r3bq9dg79ngg46a4akvik") (rust-version "1.70")))

(define-public crate-strptime-1 (crate (name "strptime") (vers "1.1.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1z13nl8fdgfi22j3ijpdaw0f38fw4blmrk2h42s3mmzp6lzh0kv9") (yanked #t) (rust-version "1.70")))

(define-public crate-strptime-1 (crate (name "strptime") (vers "1.1.1") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1q3ygna50vg51rica2qmfl3zh3kpg13vhjd5zqfhjsa830cx3zx7") (rust-version "1.70")))

(define-public crate-strptime-1 (crate (name "strptime") (vers "1.1.2") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "06qsn92rbbdvq2isw6lphlnmzg3m99pmglsg4glvnvjafpqlkyhi") (rust-version "1.70")))

(define-public crate-strptime-1 (crate (name "strptime") (vers "1.1.3") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)))) (hash "0z2lxr1baagwysxa97bjj8ixsrqfmsk0g040mgam9k71rjbwr45f") (rust-version "1.70")))

