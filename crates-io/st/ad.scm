(define-module (crates-io st ad) #:use-module (crates-io))

(define-public crate-stadium-0.1 (crate (name "stadium") (vers "0.1.0") (hash "09587lwigyg3b13z857bnkn9gz7n5bnlimwyik56m7rz692mlzxc")))

(define-public crate-stadium-0.1 (crate (name "stadium") (vers "0.1.2") (hash "02xz8avj591ngpbkic9gw22a9pd91mwnhfw9c8j4xg06p29g7r3l")))

(define-public crate-stadium-0.1 (crate (name "stadium") (vers "0.1.3") (hash "07kz07fzm5v77cbhv4d4azfmwk42zmwsf2swmylbv0d19qpbvm7y")))

(define-public crate-stadium-0.1 (crate (name "stadium") (vers "0.1.4") (hash "115mp9prlc0f458d0vhv2512am7rvmab01czi0dhcj191r6fifaa")))

(define-public crate-stadium-0.1 (crate (name "stadium") (vers "0.1.5") (hash "153ij7civpr1cxlwhr917059jc8q55h5pdpfhj7jvwz89m6p5br5")))

(define-public crate-stadium-0.1 (crate (name "stadium") (vers "0.1.6") (hash "0cgvc9pk7vp79l1f6n3v2rihgyh2xc90lr9in8hninp69c5hbad7")))

