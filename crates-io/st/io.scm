(define-module (crates-io st io) #:use-module (crates-io))

(define-public crate-stio-0.0.1 (crate (name "stio") (vers "0.0.1") (deps (list (crate-dep (name "http-body-util") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "hyper") (req "^1.2") (features (quote ("http1" "server"))) (default-features #t) (kind 2)) (crate-dep (name "mio") (req "^0.8.11") (features (quote ("net" "os-poll"))) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1c0il6mw77v328finhqdlxs0pwsfwgb9aicg8cllw9hnsnnp37kj")))

