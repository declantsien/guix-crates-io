(define-module (crates-io st in) #:use-module (crates-io))

(define-public crate-stingray-0.1 (crate (name "stingray") (vers "0.1.0") (hash "1ak58fzh6raxa9gp23nwjw1g7d7rvrbw84nkah6b654cfkswl5d6")))

(define-public crate-stinkage-0.0.0 (crate (name "stinkage") (vers "0.0.0") (hash "1nrqk6hy5id0gp1z2qn3z0wpqi9f6qagmz92814bz7rzm0f12jhy") (yanked #t)))

(define-public crate-stinkage-0.0.1 (crate (name "stinkage") (vers "0.0.1") (deps (list (crate-dep (name "ring") (req "^0.13") (default-features #t) (kind 0)))) (hash "0324axl5rg7c77cqsc31d53sck7l7x5wrwdslm7bxpvdp6r96hvb") (yanked #t)))

(define-public crate-stint-0.0.0 (crate (name "stint") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sp71cg0jz8c22w22kxg8ibq8j6j5bsf2f294z95vy3966jcnldq")))

