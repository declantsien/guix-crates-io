(define-module (crates-io st #{3-}#) #:use-module (crates-io))

(define-public crate-st3-cursor-color-0.1 (crate (name "st3-cursor-color") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "08q8q5swvw0hm4w2vm0jywi8p69zs0199j8wwybpsz8i1kjrrh9c")))

