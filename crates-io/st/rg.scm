(define-module (crates-io st rg) #:use-module (crates-io))

(define-public crate-strg-0.1 (crate (name "strg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0acfjjm35qv0m017r8vfwmks4klbm1fcxcmsx8rx51g4fakhq2js")))

