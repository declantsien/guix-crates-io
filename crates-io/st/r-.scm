(define-module (crates-io st r-) #:use-module (crates-io))

(define-public crate-str-block-0.1 (crate (name "str-block") (vers "0.1.0") (deps (list (crate-dep (name "proclet") (req "^0.1") (features (quote ("proc-macro" "literal-value"))) (kind 0)))) (hash "0wbh51m6za3nxl7qzl996714zw7wf35j225cp10f84fyzsrphnfw")))

(define-public crate-str-block-0.1 (crate (name "str-block") (vers "0.1.1") (deps (list (crate-dep (name "proclet") (req "^0.2") (features (quote ("proc-macro" "literal-value"))) (kind 0)))) (hash "1mlk1g16zbjadkfw61gmyfpf3n843k7v3zbvhznkx0xc2i7l658k")))

(define-public crate-str-block-0.1 (crate (name "str-block") (vers "0.1.2") (deps (list (crate-dep (name "proclet") (req "^0.3") (features (quote ("proc-macro" "literal-value"))) (kind 0)))) (hash "1jn4rkw9v1xvdf92jwpdrn9sfds920lf28yd3xcpns8kjb23zfy3")))

(define-public crate-str-buf-1 (crate (name "str-buf") (vers "1.0.0") (hash "1ypzpzmx0lgdjipnpqm8kby7hbwrvf01zzhsnc7xq8xnps09xp49")))

(define-public crate-str-buf-1 (crate (name "str-buf") (vers "1.0.1") (hash "16rij62p2nkim0p9rpwssg2yg1ra60vj1g0jxzrskrx6grhq2py4")))

(define-public crate-str-buf-1 (crate (name "str-buf") (vers "1.0.2") (hash "1qqj5aqa7hflnjgpmlhiqsj4bzpxwjpkp8a2svl4l8fr1q6xgi6f")))

(define-public crate-str-buf-1 (crate (name "str-buf") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1hz0n7jvw488ba3h6dpc7xb5y3srcmyjw2lz75mjwhwmnq415kjw")))

(define-public crate-str-buf-1 (crate (name "str-buf") (vers "1.0.4") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0ylm42disghsjc8sz8ibj70gvamgpzhqbaxkgqak5ykipwcf8iqa")))

(define-public crate-str-buf-1 (crate (name "str-buf") (vers "1.0.5") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0shprf95kywspn4vbn706n8kvh6n473c5sffmdbsz77zni1kcjnl")))

(define-public crate-str-buf-2 (crate (name "str-buf") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "07ykbnhzlfsbaqwzb7gszv9dvpdqmlmbdwdkgiq9f34krnkfcsm6") (yanked #t)))

(define-public crate-str-buf-2 (crate (name "str-buf") (vers "2.0.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0bnb70z88bd12hrlpmmxq8mxammrzza87ncjsl7d7qhvayvbsw29") (yanked #t)))

(define-public crate-str-buf-2 (crate (name "str-buf") (vers "2.0.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0r3g699758s21024g691dy1xkf3xgxxns7szq88cn8nbj8d3y9zm") (yanked #t)))

(define-public crate-str-buf-2 (crate (name "str-buf") (vers "2.0.3") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "12sgmh5ygwy6g9prb1v6hlr626hgcychsjwpfcfk0sli17z8mgk0") (yanked #t)))

(define-public crate-str-buf-2 (crate (name "str-buf") (vers "2.0.4") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1cpg3q4px2f55cm2nln7bcqpfrqg16hxlbxdqr8wicjgmr36fgsx")))

(define-public crate-str-buf-3 (crate (name "str-buf") (vers "3.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0zndxi15vwir82nz0h314m6imv2akavcjj1bhvxafjq1myb613ag")))

(define-public crate-str-buf-2 (crate (name "str-buf") (vers "2.0.5") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "07mf7mzrrji0g6d44y0phdw7x49xl7sv3l3qrh6l89hi40lwnwq8")))

(define-public crate-str-buf-3 (crate (name "str-buf") (vers "3.0.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0jpc18a0s0spgq854x3yr1lzr1kmf1cxzshdaq2d42nw1j8f4cj6")))

(define-public crate-str-buf-1 (crate (name "str-buf") (vers "1.0.6") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1l7q4nha7wpsr0970bfqm773vhmpwr9l6rr8r4gwgrh46wvdh24y")))

(define-public crate-str-buf-3 (crate (name "str-buf") (vers "3.0.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "ufmt-write") (req "^0.1") (optional #t) (kind 0)))) (hash "0cagpgn8q21yvr1vffkp4mgfx4z85cb6ch1ml4z3ryg2akp74nz7")))

(define-public crate-str-buf-3 (crate (name "str-buf") (vers "3.0.3") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "ufmt-write") (req "^0.1") (optional #t) (kind 0)))) (hash "0d4c1lql6kgd6kvc603rnd729arwdg5m60fvshpkqwaw4avrgsqc")))

(define-public crate-str-cat-0.1 (crate (name "str-cat") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0bfajai1f748qcph8y39z7dy6gy470nylc5cm0c7l1aarjjmydwl")))

(define-public crate-str-cat-0.1 (crate (name "str-cat") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "15nhy8fciz49mhfx7c9whl4hliy4x7lgfl8dvmii4h5723fddqll")))

(define-public crate-str-cat-0.1 (crate (name "str-cat") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0vgh884rm2fgq89783c1pya9i0b11204ribzxpw35z7irq03wbhf")))

(define-public crate-str-cat-0.1 (crate (name "str-cat") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1vvsmcf6lsgz41bhhb58l6zys5k5b77jydqsd5jvggmflmgv2irf")))

(define-public crate-str-cat-0.2 (crate (name "str-cat") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1nxrvgpp82jra68921jswl71xgkw6jp8sh10gay46yj6hhl27bb9")))

(define-public crate-str-concat-0.1 (crate (name "str-concat") (vers "0.1.0") (hash "1hp2nrd6yxydg1n4xsc0c2hi8qmfk7gi5jjpsbnnb3n9b3bz6pgw") (yanked #t)))

(define-public crate-str-concat-0.1 (crate (name "str-concat") (vers "0.1.1") (hash "0lchmhf6mdgya3gn7vpd35vi0f4rz2n1d9rhalwb72zwizcgdnmp") (yanked #t)))

(define-public crate-str-concat-0.1 (crate (name "str-concat") (vers "0.1.2") (hash "1m99hvk1n9a5bm2i1wn74j9znnj4a2zanfvsxkcrq7921q9r1f17") (yanked #t)))

(define-public crate-str-concat-0.1 (crate (name "str-concat") (vers "0.1.4") (hash "07pzmli97gj4h80kqljka792xsxlc8vdjqkw2yqhwq15y96cbzb4") (yanked #t)))

(define-public crate-str-concat-0.2 (crate (name "str-concat") (vers "0.2.0") (hash "0d16c35iz7r4vrzldla9sn781hji17swxrgmrpily72092g96s1l")))

(define-public crate-str-distance-0.1 (crate (name "str-distance") (vers "0.1.0") (deps (list (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "0myg06m8jcqssiclj4xwy78qq7jcnl1jwxaymias3ly3gy0aq5l4")))

(define-public crate-str-intern-0.0.1 (crate (name "str-intern") (vers "0.0.1") (hash "1inmpfnzbqfq3lxi6fjqh01a5fybx5cvn7lpyyyg3j34d8azbrn2") (features (quote (("global") ("default" "global"))))))

(define-public crate-str-intern-0.0.2 (crate (name "str-intern") (vers "0.0.2") (hash "1xsj5h2amycbbgip4zdmji2q04j1ykk90mi0za7ak9347cdxg13i") (features (quote (("global") ("default" "global"))))))

(define-public crate-str-macro-0.1 (crate (name "str-macro") (vers "0.1.0") (hash "1p207walfsvzhhxlz5sgr2zx33cya3k7h890w6apismnfjj8rcjp")))

(define-public crate-str-macro-0.1 (crate (name "str-macro") (vers "0.1.1") (hash "14z0qp334rr2201qk6xg5bvq616lib0sl16gqcwskqjj6wisn4wn")))

(define-public crate-str-macro-0.1 (crate (name "str-macro") (vers "0.1.2") (hash "1kafccn4ja35ygqws8vr5dw6iqzgdnfr82wjs5l4dbclnx8aqkp3")))

(define-public crate-str-macro-0.1 (crate (name "str-macro") (vers "0.1.3") (hash "060wlgcj765n8cvic8mqd01yzbnsvl6bflbdi78wmziar477rrpr")))

(define-public crate-str-macro-0.1 (crate (name "str-macro") (vers "0.1.4") (hash "1p966jx6sfg1lzrl6i8m4k11r04pjrfsblxbqpkmmgps2sfk6509")))

(define-public crate-str-macro-1 (crate (name "str-macro") (vers "1.0.0") (hash "0r28asfn2kdx3g41g0hf4d5hdkviiz3f4dnx0dxiqx3hca318xcv")))

(define-public crate-str-match-0.1 (crate (name "str-match") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xv4df7p3svahfgsmdl476qnm72a5ka8va05d0zjipakdmwxik93") (features (quote (("default") ("attribute"))))))

(define-public crate-str-match-0.1 (crate (name "str-match") (vers "0.1.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c2l1kkf8knqg7b6ai7p0r62nnvi2ycxcdiq4yr68x3x8g0ifpmr") (features (quote (("default") ("attribute"))))))

(define-public crate-str-match-0.1 (crate (name "str-match") (vers "0.1.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pllaagn9vagv5cdhmp2diiplqswjs491zpajhnl6vjmjj49fp4d") (features (quote (("default") ("attribute"))))))

(define-public crate-str-queue-0.0.1 (crate (name "str-queue") (vers "0.0.1") (deps (list (crate-dep (name "memchr") (req "^2.4.0") (optional #t) (kind 0)))) (hash "0nv7qydk3y5j0j1lr3vllfnww99ih2bb7wq4nfhkifvf069hxws0") (features (quote (("std_with_memchr" "std" "memchr/std") ("std") ("default" "std"))))))

(define-public crate-str-reader-0.1 (crate (name "str-reader") (vers "0.1.0") (hash "09agszs9x799ccjc1775w2kgil2c12wdks2ina11bcs43h8davny")))

(define-public crate-str-reader-0.1 (crate (name "str-reader") (vers "0.1.1") (hash "0fwxnvjs2i8hyiy9m48hdd8ww4cfxvxj84k0ngpj7n1w1d6w26yp")))

(define-public crate-str-reader-0.1 (crate (name "str-reader") (vers "0.1.2") (hash "045bpm4g04j2wdyndl2kxjxl6k7bkc46wxdqzfzy0ipckaw21am6")))

(define-public crate-str-similarity-0.1 (crate (name "str-similarity") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0pp6n7760anyp52rx5cd91h58f5fy1pn5majryjgfwfi999swf60") (yanked #t)))

(define-public crate-str-similarity-1 (crate (name "str-similarity") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "04wp2kp4y7q93nw8pq5d6sq4b5x4av0lcvpzkzq1pir0x2xhzfhx") (yanked #t)))

(define-public crate-str-similarity-1 (crate (name "str-similarity") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "03hnq18bm656m541145b959rm822jiaa0lgv7kykjd62k02r57lb") (yanked #t)))

(define-public crate-str-similarity-1 (crate (name "str-similarity") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1mjkmwc9r2clv7nbl3v8rmlfm5dq24f1zvg60pmziaac6j45liyz") (yanked #t)))

(define-public crate-str-similarity-1 (crate (name "str-similarity") (vers "1.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "03xbag706k6cpnqzidh62rc56j9jwgq421yz2yl8z6xgw2hh976d") (yanked #t)))

(define-public crate-str-similarity-1 (crate (name "str-similarity") (vers "1.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1a0xazxxljk4jcin55vir6m133gz42nv98sd1j57abm0y66wfhgr") (yanked #t)))

(define-public crate-str-similarity-2 (crate (name "str-similarity") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "07y8zz1p66nl5i1mkbqj7d7p2fwa8da0g7d280jkrvnpi8dbyv39") (yanked #t)))

(define-public crate-str-similarity-3 (crate (name "str-similarity") (vers "3.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "10dswlzjgnpinbiwmnlfakkrq0bdlfnrrxlc7z9blwghggjgjq7p")))

(define-public crate-str-similarity-3 (crate (name "str-similarity") (vers "3.0.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1nk0agmy1b10n23wsi41g02ndsfgy7jdhdhlmk6hcbk1k8g9y9k1")))

(define-public crate-str-similarity-3 (crate (name "str-similarity") (vers "3.0.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "0dzzqq0j1crnaaijza7qzblmr19d5qq4r3j61jzjcql0va012ymm")))

(define-public crate-str-util-0.0.1 (crate (name "str-util") (vers "0.0.1") (hash "1bpz4l3qbppc1yj42jfkh4fgy2b62qcpgkmsadv1gc3fnyd5ip2c")))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "1bzxgl70naf7cyy4pi7lhnzvwzh4g6yzk5jbq0i3k37xfw5wb673") (features (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "11da0xlp76l2wmya2grhk7c4pd8iffiq11ij4gy43fnarj1sp4za") (features (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "0vfmm6mjry7mhwfqg50b36aycydb67w4hgqhw9c5a7lq8r0sx0rc") (features (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "0qzlhyd0lwlv1vrq90cwzl8lkbxwivfqs3jxq3ix97kdc4zci6pj") (features (quote (("std") ("default"))))))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.4") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "0zya3prif95wbhl8m6cgxq9mw08kxnyh71ymccd871x0llnq9ay4")))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.5") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "0az1qa5cs5y84rw4im50zd3hwssv4kh0vmaq9dya8r74k2z5ycmi")))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.6") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "02b7jjvm7gcnhjxp2awm3pakzml5jxzwgfd2yig0gim4i4mfa1dm")))

(define-public crate-str-utils-0.1 (crate (name "str-utils") (vers "0.1.7") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "cow-utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2") (default-features #t) (kind 0)))) (hash "15xmm1ba3h7lry9pl26mmwyi0p95gpr25q5ri58lbzd887av7g30") (rust-version "1.60")))

