(define-module (crates-io st ds) #:use-module (crates-io))

(define-public crate-stdshout-0.1 (crate (name "stdshout") (vers "0.1.0") (hash "1ldiqhbz78n6crqj497i6r1cdgfb9lcwnms3h8dkpl665y446127")))

(define-public crate-stdsimd-0.0.1 (crate (name "stdsimd") (vers "0.0.1") (hash "0kvrp1nzdxas1nbx011apafyma4pl7kaapz467cafvzj3sswjr55")))

(define-public crate-stdsimd-0.0.2 (crate (name "stdsimd") (vers "0.0.2") (hash "1gvhir9lnsf2xnvh700iaczfr80dl2h5agy1n4h5ac1ncvr7m2c8")))

(define-public crate-stdsimd-0.0.3 (crate (name "stdsimd") (vers "0.0.3") (deps (list (crate-dep (name "cupid") (req "^0.3") (default-features #t) (kind 2)))) (hash "1q9vzs9hfnp04wfbwiwmc0qdi9a7y494h0v36sb0ms6qljafz13a") (features (quote (("strict"))))))

(define-public crate-stdsimd-0.0.4 (crate (name "stdsimd") (vers "0.0.4") (deps (list (crate-dep (name "auxv") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "coresimd") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "04hpss7s5xk8dbq9h3cd0gzb16816w0kahrahm45wk6i8409wmkk") (features (quote (("strict" "coresimd/strict") ("intel_sde" "coresimd/intel_sde"))))))

(define-public crate-stdsimd-0.1 (crate (name "stdsimd") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "coresimd") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ga5y5d5j78x39ffjshqc35zqjgfxms9a050j8v6l4qh8i1ipwm2") (features (quote (("wasm_simd128" "coresimd/wasm_simd128") ("default"))))))

(define-public crate-stdsimd-0.1 (crate (name "stdsimd") (vers "0.1.1") (deps (list (crate-dep (name "auxv") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "coresimd") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cupid") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0qzcxckylsdxa6wdfl821zccad2srbvzyrg1hxwc2pz0wk7vylx1") (features (quote (("wasm_simd128" "coresimd/wasm_simd128") ("default"))))))

(define-public crate-stdsimd-0.1 (crate (name "stdsimd") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "coresimd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06mijlf0mvagbm30zy4xgm4nb830mciwbvik8afqkld7grgy287i") (features (quote (("wasm_simd128" "coresimd/wasm_simd128") ("default"))))))

