(define-module (crates-io st di) #:use-module (crates-io))

(define-public crate-stdin-readlines-0.1 (crate (name "stdin-readlines") (vers "0.1.0") (hash "0ahq7ia1wzcnkdp05mffbw5hp90dw83q02qqr4viprqhgi8m78ga") (yanked #t)))

(define-public crate-stdin-readlines-0.1 (crate (name "stdin-readlines") (vers "0.1.1") (hash "0qrn0z5dgh0pqpnj10mq135nd8kxpsghzj93d04451px7y43v4iv")))

(define-public crate-stdin-should-be-tty-0.1 (crate (name "stdin-should-be-tty") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1kp4qhg9bhf6sqhr6shm0ijbm6adp2qcd48l3csbf7q88585dgdy")))

(define-public crate-stdin-to-cloudwatch-0.1 (crate (name "stdin-to-cloudwatch") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^1.1.5") (features (quote ("behavior-version-latest"))) (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-cloudwatchlogs") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.6") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g0gmim6zsjqz51iwj0jm7nprrvga011xss624s2cm3v7q39i304")))

(define-public crate-stdin-to-cloudwatch-0.2 (crate (name "stdin-to-cloudwatch") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^1.1.5") (features (quote ("behavior-version-latest"))) (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-cloudwatchlogs") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.6") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r1nv9s3awi4qhld4ny1g92fgbpw73alhli8xahc0z6h4m6y9vvx")))

(define-public crate-stdin-to-cloudwatch-0.3 (crate (name "stdin-to-cloudwatch") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^1.1.5") (features (quote ("behavior-version-latest"))) (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-cloudwatchlogs") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.6") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zl3m84z807pxlfb8m1l6svy8zhffalhbm8f6n8b215qz0jgfs63")))

(define-public crate-stdin_parser-0.1 (crate (name "stdin_parser") (vers "0.1.0") (hash "1b7ingn61qjpm6v99vfbn1lrvisy8j74kkdjkki8dizyiak07cmj")))

(define-public crate-stdin_parser_derive-0.1 (crate (name "stdin_parser_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "stdin_parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.33") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1mb2zsajjv029axy9c5s9svmz2gqpqagh8dbrylz3ywp7asapgb0")))

(define-public crate-stdin_receiver-0.1 (crate (name "stdin_receiver") (vers "0.1.0") (hash "1168wb2lswvfzsk5nf05w9xsiznnrmhd3hqb8marybzhi5cid8ln")))

(define-public crate-stdinix-0.1 (crate (name "stdinix") (vers "0.1.0") (hash "02x2ccbp3nz7nhvqdwj5nkf8ib2w78a88zlpdl4xzyzylkvib8ha")))

(define-public crate-stdinix-0.2 (crate (name "stdinix") (vers "0.2.0") (hash "1f5q8dpng8gribp3vx0xaccdpz3v8lwh4xan3i90rg2s74v0vqsl")))

(define-public crate-stdinman-0.1 (crate (name "stdinman") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.11") (features (quote ("voice"))) (default-features #t) (kind 0)) (crate-dep (name "songbird") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "13zf906lfifgwl0ddk9yvqx6b3miyi0fqimhvrg0328zibvamcsn")))

(define-public crate-stdinout-0.1 (crate (name "stdinout") (vers "0.1.0") (hash "1fwdylp0wjkx7d781q52nrnxvm5fbwamxwp36xlmvgcim7fy34as")))

(define-public crate-stdinout-0.2 (crate (name "stdinout") (vers "0.2.0") (hash "13fm134nbbqffnk7z2k0aivf6hl6nxn8n88awhhyy5ciz2fvbv3m")))

(define-public crate-stdinout-0.3 (crate (name "stdinout") (vers "0.3.0") (hash "1y7ywj260b5la77hi8n1xw5lsmyjgdg2sh8r86312kh6mpgaxi1p")))

(define-public crate-stdinout-0.3 (crate (name "stdinout") (vers "0.3.1") (hash "0vl0vg5bx1mqmm0xggi0pdbhkfgmdmmkignv8d7mpwgs6qnj93xq")))

(define-public crate-stdinout-0.4 (crate (name "stdinout") (vers "0.4.0") (hash "102np0d8hr3r4gh1w7m5nvsvfj7i2ng8026padjdbpmjyxp05w99")))

(define-public crate-stdinout-0.4 (crate (name "stdinout") (vers "0.4.1") (hash "18gybqz7q38w69mcjrlpqj34pg10wg2bcqxlml4iakkjkamkl7bk")))

(define-public crate-stdint-0.1 (crate (name "stdint") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0v0cw4jsz7drhk09w9h57av1i2m5h50xn49cp7wdmqk2mlhqk11g")))

(define-public crate-stdint-0.2 (crate (name "stdint") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0z8cp17m69732rf3g947cp8rqk5bzxi9rjbyr797jljzqwrqggqd") (features (quote (("std") ("default" "std"))))))

(define-public crate-stdio-override-0.1 (crate (name "stdio-override") (vers "0.1.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ky5ibdhfdj0r7r77wirypm1srkkii25nwini82n9gw4yvw0005r") (features (quote (("test-readme" "doc-comment"))))))

(define-public crate-stdio-override-0.1 (crate (name "stdio-override") (vers "0.1.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "01wxhnjckwlilx3g90cppkvnw5pd5jzh73n5j9x9p22p2q2yzb7w") (features (quote (("test-readme" "doc-comment"))))))

(define-public crate-stdio-override-0.1 (crate (name "stdio-override") (vers "0.1.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1jf5j4ship1l6lzhndhm13hvs6lfcnsayzra51baambsdx0gm1sz") (features (quote (("test-readme" "doc-comment"))))))

(define-public crate-stdio-override-0.1 (crate (name "stdio-override") (vers "0.1.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1j1hq3f6y1n7dxry125r50gql1w501f9ak6s2yksjnp9s892bhm9") (features (quote (("test-readme" "doc-comment"))))))

(define-public crate-stdio2-0.1 (crate (name "stdio2") (vers "0.1.0") (hash "0bcg755i9v6hvg9qg7l14if8p31y2897y6y5ijilnq6017617rrx")))

(define-public crate-stdio2-0.0.1 (crate (name "stdio2") (vers "0.0.1") (hash "1w4bsdzzn3kp56761agsa56l85mq8fx06xnly5l9hqggx8radqgk")))

(define-public crate-stdio_logger-0.1 (crate (name "stdio_logger") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "term-painter") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0c30bm96vdpxvngnkw41qrzlwy6hhks42wjyf5mp4xa33ihg3jcw")))

