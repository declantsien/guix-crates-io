(define-module (crates-io st ev) #:use-module (crates-io))

(define-public crate-stevedore-0.0.0 (crate (name "stevedore") (vers "0.0.0") (hash "0m8dq44w9i1nmxbk4jh505rhxr98qd620n992qah02vwpprsq825")))

(define-public crate-stevia-0.0.0 (crate (name "stevia") (vers "0.0.0") (hash "1qccyx7fnb3y0k8v65p98sw0k0r8i97a3rp3c5s804bpf83z2j9s")))

