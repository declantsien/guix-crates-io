(define-module (crates-io st te) #:use-module (crates-io))

(define-public crate-stter-0.1 (crate (name "stter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pdf-extract") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "0nnz2j2ddv2jllwcabq28j99rwqjmprnsfnkwx7fd4m63x1hmwy4") (yanked #t)))

