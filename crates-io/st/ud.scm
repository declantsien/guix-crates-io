(define-module (crates-io st ud) #:use-module (crates-io))

(define-public crate-stud-0.1 (crate (name "stud") (vers "0.1.0") (hash "161swc3vgydvad1v2qihvqs2hnkjqfv2vgr0rz04wp3ah27yz552")))

(define-public crate-stud-0.1 (crate (name "stud") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "stud-macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "signal"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1blhd1q9r4sip7g8nbc5j71gj0kyq1r5h4gcvwrkzhgq2ay54dgj") (features (quote (("default" "bin")))) (v 2) (features2 (quote (("bin" "stud-macros/bin" "dep:clap" "dep:tracing-subscriber" "dep:yansi" "dep:tokio"))))))

(define-public crate-stud-macros-0.1 (crate (name "stud-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "06vh04n13c0fdn940b86f6rgvr5qqcbiakbzfidz17s43bkxkhw5") (features (quote (("bin"))))))

(define-public crate-stud-macros-0.1 (crate (name "stud-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1vcvbcdjm9kv84b2fpa6axw5szlrk7fmnnj40p8aqzaah9zldjcd") (features (quote (("bin"))))))

(define-public crate-stud_ip_scraper-0.1 (crate (name "stud_ip_scraper") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5") (default-features #t) (kind 0)))) (hash "0sw0wfiam4hl4q6lqzfw8p7dxgix3l16jq2jx1hp8idzgpvjirxd") (features (quote (("verbose") ("rate_limiting") ("default" "rate_limiting"))))))

(define-public crate-stud_ip_scraper-0.1 (crate (name "stud_ip_scraper") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5") (default-features #t) (kind 0)))) (hash "0lfyp5nzxqzb3lvjipq25hjz5rjiz8ri0jq9bl69q28vlihvn6rw") (features (quote (("verbose") ("rate_limiting") ("default" "rate_limiting"))))))

(define-public crate-stud_ip_scraper-0.2 (crate (name "stud_ip_scraper") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5") (default-features #t) (kind 0)))) (hash "0ilh727av15y2y05192mcip26nlczq3kxl9sfb9vwpi8k3ynsv8q") (features (quote (("verbose") ("rate_limiting") ("default" "rate_limiting"))))))

(define-public crate-studarc-0.1 (crate (name "studarc") (vers "0.1.0") (hash "0al5bcr7nyhqzp06g78xbd34c1wip1ykmm6fisb6g4nxl2d0wmmm")))

(define-public crate-studentvue-0.1 (crate (name "studentvue") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.17.2") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.11") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zvfb18bp01dbgrln9mcxrdqm09r1p85q9hv1ip7dqjim6bjbchj")))

(define-public crate-studier-0.1 (crate (name "studier") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zvrjx14bxfnbkjq7av7v7vgjq2gakda2vb2rgrqjpryfqr1i2ii")))

(define-public crate-studier-1 (crate (name "studier") (vers "1.0.0") (deps (list (crate-dep (name "enum-as-inner") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sbvkbipr3ahvyfgfw5c7irlhvp36j5k1m0h6dgr4j7slj2r0ynk")))

(define-public crate-studier-1 (crate (name "studier") (vers "1.0.1") (deps (list (crate-dep (name "enum-as-inner") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ghxigpx17nfsvbqcaml37jb02q1jw5aaa2rhwjrnzl9yg9jmzq6")))

(define-public crate-studier-1 (crate (name "studier") (vers "1.0.2") (deps (list (crate-dep (name "enum-as-inner") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0adwhiak13ijcr857rbgjcvv49grvcakr19asmfy01h8zv3wgfv3")))

(define-public crate-studier-1 (crate (name "studier") (vers "1.1.0") (deps (list (crate-dep (name "enum-as-inner") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j1x6416wbg7h5f1na4z8nnag3hbf7dc4njm03dm0lyisqc9crz4")))

(define-public crate-studio-0.0.0 (crate (name "studio") (vers "0.0.0") (hash "0shacyan0gn0b785ixl2xdy2fxn50bc8dh2smfk5m33cg013ccn1")))

(define-public crate-studs-0.1 (crate (name "studs") (vers "0.1.0") (deps (list (crate-dep (name "studs-macro") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0661qdnk0r1f1qv5wfvysziag541hhqipaq7pd734cahxzpimf3l") (features (quote (("no-std" "studs-macro/no-std") ("macro" "studs-macro") ("default" "macro") ("async"))))))

(define-public crate-studs-0.1 (crate (name "studs") (vers "0.1.1") (deps (list (crate-dep (name "studs-macro") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "16rpgp7w82zpczaiv62ai8nb4h3xkyg8mif7j9grx5gn2aycwf45") (features (quote (("no-std" "studs-macro/no-std") ("macro" "studs-macro") ("default" "macro") ("async"))))))

(define-public crate-studs-0.1 (crate (name "studs") (vers "0.1.2") (deps (list (crate-dep (name "studs-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0waxdskwaisn35bkpyzrs738jgahd92r882y2lxiak8dyrgrh8pk") (features (quote (("macros" "studs-macros") ("default" "macros"))))))

(define-public crate-studs-0.1 (crate (name "studs") (vers "0.1.3") (deps (list (crate-dep (name "studs-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0znj0ygs97kkb9f1vrzgw5hl3j28ljr18xf4ygnmzl4fl5akn1v0") (features (quote (("reflect") ("macros" "studs-macros") ("default" "macros"))))))

(define-public crate-studs-lib-0.1 (crate (name "studs-lib") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n9d11sa23qlbw52kxxdfxsrdyc644m85hw3g5mqjjqdm0zal0sl") (features (quote (("no-std") ("default"))))))

(define-public crate-studs-lib-0.1 (crate (name "studs-lib") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08ml7a9s0giv25rjv5xqfqix79y2665f0n8ahixi6hnf8awlxi7k") (features (quote (("no-std") ("default"))))))

(define-public crate-studs-macro-0.1 (crate (name "studs-macro") (vers "0.1.0") (deps (list (crate-dep (name "studs-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r13manwyypxqs9m8gkxbqqf6w4qi9cwz30qg0il985g45hzynsz") (features (quote (("no-std"))))))

(define-public crate-studs-macro-0.1 (crate (name "studs-macro") (vers "0.1.1") (deps (list (crate-dep (name "studs-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "120s5c5g89847h0ip922ryiaia2c5s4bg61in25fc8xbxk4yz5vi") (features (quote (("no-std") ("from"))))))

(define-public crate-studs-macros-0.1 (crate (name "studs-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "studs-macros-lib") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07px6ql9pa6rnaazy6yd5dlan1rcfi5f6g547lhzz5pyxvlscmvq")))

(define-public crate-studs-macros-lib-0.1 (crate (name "studs-macros-lib") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12rz3g7kkm7kfzc6vpmi5pl5z39hlfq6hpi0mkcx09rmg6jrq1k1") (features (quote (("default"))))))

(define-public crate-study-example-0.2 (crate (name "study-example") (vers "0.2.0") (deps (list (crate-dep (name "c-kzg") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0y82539f9q7d3hc6ckm3frbbds073rvnz2l8c11qhrgn5s7443ll")))

(define-public crate-study-example-0.2 (crate (name "study-example") (vers "0.2.1") (deps (list (crate-dep (name "c-kzg") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hello_macro_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "098gqypkknw77riyx2ywjkgny4k71k2ww57zppghwqr5681r78w2")))

(define-public crate-study-rust-0.1 (crate (name "study-rust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1blqc2lv1k7s337lkqrm99qf4a2113vidfby8prmxz53c0a15h8w") (yanked #t)))

(define-public crate-study-rust-example-0.1 (crate (name "study-rust-example") (vers "0.1.0") (deps (list (crate-dep (name "c-kzg") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18w0mpjsvk7hgnl0fi9013df2qg2icwrl5ks698nrsi2k5fg30g9")))

(define-public crate-study13-0.1 (crate (name "study13") (vers "0.1.0") (hash "0c35lwd64si01r2c8dwxpg46v8q1alb3vzq05inw83fhmyfy7711")))

(define-public crate-study_blocker-0.1 (crate (name "study_blocker") (vers "0.1.0") (deps (list (crate-dep (name "eframe") (req "^0.20.1") (features (quote ("persistence" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "01wd7vihiq6297gnrp17mrgam4mjq515d34rhfqfd4d11jswmz02")))

(define-public crate-study_blocker-0.2 (crate (name "study_blocker") (vers "0.2.0") (deps (list (crate-dep (name "eframe") (req "^0.20.1") (features (quote ("persistence" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08ws1yh7g66l4nq4cbdyg9fakl9bagsnci2dmldm9gcgsfm6x2sr")))

(define-public crate-study_blocker-0.2 (crate (name "study_blocker") (vers "0.2.1") (deps (list (crate-dep (name "eframe") (req "^0.20.1") (features (quote ("persistence" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1m00ml7kp9am86zncwfdmzbin13j2s6pg259l1d62v2rc8yplmmd")))

(define-public crate-study_blocker-0.2 (crate (name "study_blocker") (vers "0.2.2") (deps (list (crate-dep (name "eframe") (req "^0.20.1") (features (quote ("persistence" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0465ap7l6ym3889c7f730zkdilf8l4lgjp8xdq1lbfrk9dpbgx2s")))

(define-public crate-study_cargo_profile_and_publish_crates_io-0.1 (crate (name "study_cargo_profile_and_publish_crates_io") (vers "0.1.0") (hash "0nnqzqb61sq5imbwp41mfvz7nbz3zv4rgzj8w38xxwazrmqwh71h")))

(define-public crate-study_rpn_calc_2022_04_07-0.1 (crate (name "study_rpn_calc_2022_04_07") (vers "0.1.0") (hash "1pzlsx55z295vzbdmghbh3k3618adfvaaynxhii41rxzxvinrjnz") (yanked #t)))

(define-public crate-study_rpn_calc_2022_04_07-0.1 (crate (name "study_rpn_calc_2022_04_07") (vers "0.1.1") (hash "0dxsc0hvh2af5i5a0h04asd825k88a1f6yc9khhcfsiycdschdz8") (yanked #t)))

(define-public crate-study_rpn_calc_2022_04_07-0.1 (crate (name "study_rpn_calc_2022_04_07") (vers "0.1.2") (hash "00m7fnhqc7kszrzywldy11v8h9k7rg18yz3h894dy6dwvsfgfkwc") (yanked #t)))

(define-public crate-studyhall-0.0.1 (crate (name "studyhall") (vers "0.0.1") (hash "0i9daxdmkydigwrvggi6xgbvy7f2apf73ngv4iaw9vsn75sxi8gk") (yanked #t)))

(define-public crate-studyhall-0.1 (crate (name "studyhall") (vers "0.1.1") (hash "06f1jw90rph1jalv7rghxsrg3fvby8q7k8409krbhdsxs2fx4f30") (yanked #t)))

(define-public crate-studyhall-0.1 (crate (name "studyhall") (vers "0.1.3") (hash "1scpx981kkb5bvkfdmqdnqqdcs2w9kxg2q47vg88fs9my05z2h4q") (yanked #t)))

(define-public crate-studyhall-0.1 (crate (name "studyhall") (vers "0.1.4") (hash "1vl4rc7cvi1wi7prkaccj9qzgchkqxskp1irz7f98h8k43v6qbya") (yanked #t)))

(define-public crate-studyhall-0.1 (crate (name "studyhall") (vers "0.1.77") (hash "1113l3j01rdwqm0qr4143x6fra3axn0pi02wyrd5bmmpmap7xk59") (yanked #t)))

(define-public crate-studyhall-0.77 (crate (name "studyhall") (vers "0.77.0") (hash "03q2p15c37va9wljbxm84ds0y4fxiaaqpgpdqzass0cmzr341jnd") (yanked #t)))

(define-public crate-studyhall-7 (crate (name "studyhall") (vers "7.7.18") (hash "0cjllcyqbdqp6861l83mv7yrsgvdff1n1i2cpgxanjffhcsasvw5") (yanked #t)))

(define-public crate-studyhall-2018 (crate (name "studyhall") (vers "2018.7.7") (hash "0dgikx0g28qrlcbi83vdg4h3hnr37lci7c73fwchiz41zrayjsrw") (yanked #t)))

(define-public crate-studyhall-2019 (crate (name "studyhall") (vers "2019.12.13") (hash "0y8vrm6q62j6s3zhxpl9shc3myw7xh56bj5xwxm1h2h942ckdh7a") (yanked #t)))

(define-public crate-studyhall-9999 (crate (name "studyhall") (vers "9999.999.99") (hash "0r3lzxjx665jdpmypl6swm0580jjza9q987r1b65h7r4g38lxzay") (yanked #t)))

(define-public crate-studyhall-9 (crate (name "studyhall") (vers "9.9.9") (hash "0f2xmcvc8q8qjagwqp76najfqz80mqqk9rg7wb65faz3x7jzrpqg") (yanked #t)))

(define-public crate-studyhall-99999 (crate (name "studyhall") (vers "99999.99999.99999") (hash "1jyi5hkvxa2js3g3f58l9qj7xpa1pas74pbq9ky8gackr6vvd5zs") (yanked #t)))

(define-public crate-studyhall-9999999 (crate (name "studyhall") (vers "9999999.9999999.9999999") (hash "0xazqg5hmvy7vi3q60sarvclrm4672yhbjq895fxgipaf1yb4h5i") (yanked #t)))

(define-public crate-studyhall-999999999 (crate (name "studyhall") (vers "999999999.999999999.999999999") (hash "1jbr8x4wykkkrj7nwid3w4csxfr17ihkh2lsl8iaqz5wqbpimhd8")))

