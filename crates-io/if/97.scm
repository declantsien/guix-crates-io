(define-module (crates-io if #{97}#) #:use-module (crates-io))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1xgi1mqqlv64japdkr8sk82kniadkpdbx90nj76lasn1kz68qmd2") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "14b7r7yx03m6dk0r8g1vx7g2jfb9zhcrxw03yr121kbj0xyflljj") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0qlhi66np4kghk2c5nk4kcpdzdfkfig47k51zlpssncqy2177329") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0dijdmgbhkkvqxxgkxi47a2gk80nrwljrj3i6jcdlz9hp4x1sqvk") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "08gssvwyal9cp88rgw6sks04zmgzwbb11a6ka40p9ds19lv1wpj2") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "14pv0ln0mjk2arcz8sifc9dgfbrxdqwckzvpxggb22jiwd11bvy3") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0dk6j12y3kq5932ncyxgfpw499bsbf5j2annb9g17wss8li8aziz") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1cnz4r415x6rj9g1xgyppcfcy0m6zldgkrwjhr2ns5ap6gwskhig") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.8") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0sasn27rrn90r3as1xnlz30qiwa6y2bjii250dl917ysc5a899h7") (yanked #t)))

(define-public crate-if97-0.1 (crate (name "if97") (vers "0.1.9") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1izkxm4a4n5753km59vcwdj14ppsd6x79y6ngjxg9cyrc9k87mqj") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1igsiwv6qa5379qbaahs0gnf06k93sfhx6hahhc4zmxi4s4j3cv9") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0600x86gcd9rp3lpiq7kg1r0c88ci0phfqdv0q40wh6j61bmzx88") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "02lg21ax98qfvyl9423711mkmyh1c5fqbvgkyhgqjhfpj9b02261") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "073lqpxsbixqvla3z82k47hw9r7n2b3a14m8fjpsmsq2j1dls0sc") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1xgp6pisndka81wx0ci7dzi5dlmwkwsqh1hc0xrknmv04zmhcpp0") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "07m8lc0ddjwmnw3v6pwi042j225dn94hds3k1pf0wf3p5flk4bk8") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0r7adjhfap3mz9v4gb3rpz4s3d7f1inni1a911xcwn8bbyygc3zb") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0bqjkcf3x4dz18qlalah2cp9x8j984yrv1mp6kvmn4y6j2yqa9ad") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.8") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1yjpzpgjqzypy8n3zyqa02j1xmrgi3shqhhvq6s778fc2lcgfr1z") (yanked #t)))

(define-public crate-if97-0.2 (crate (name "if97") (vers "0.2.9") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0wwafj2sqwfx7c81bmb3qzm65dl96p2h0m3p6s7ndnkgh0n7z5gb") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0amgdy5rcl0lxjz013fwa89j8dnvdw2a9cyd4rzxb8gsgdki9v1s") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "10fgfjk8xm7zzmqs6nl2cjlmpq9pv2dbwjb0glfd62ha3x0hxzhn") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0qv5lqm92crdnqw1kmjhpnhzkf0bsn6gphqdmj9v46h8l3gbzl39") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1s2dj8x3pc07nwlfj7iy7z3nv7yydhzl792nv9gkpqshcdgrnhjy") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "11vc16gabssak8m643cziv1w2qw7vy5xrv3fapa3xsjyhn8chih1") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1l825855k4cn9nkjwf9yj601jwmx5158hjxhyhs4q4c1yls4rvq8") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0gzx6ii7wydqsx7wjg775bzayw1z9rj4l4bx01zc75gxpj1zfza3") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0iig6nrim3gs5894vyjznxxga8ycgp5fcxnzdcnbcyv4xbpz86g9") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.8") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1j3rbh86ss3h3f7v0gnb9q3jnrmpprfdbrjq8ldviip8vkbpka2l") (yanked #t)))

(define-public crate-if97-0.3 (crate (name "if97") (vers "0.3.9") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0k9134v9sa2dy51sgmrjn31s4nlfs1khl1j0zzbfz1027jw0xrx4") (yanked #t)))

(define-public crate-if97-0.4 (crate (name "if97") (vers "0.4.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "196xcn7lyr6yas2biw4hjjb9qhfzssh9p3sy9hrs423blm668qsf") (yanked #t)))

(define-public crate-if97-0.4 (crate (name "if97") (vers "0.4.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "09vsng8h2n2alxfmrs6c32kjdk6pg1yrdi123fzh53lih7n8wr0x") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "172nlmjjgs0h2l4hqqja5z44wpmm1d6d6d9vs6sxsiw0d4jfa6qx") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "16pyfs2jqk4c80f0vxwnkskax6jvzlx2i8lkyfjbcjb7iqn8512a") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "19j8m9rgkj92966055idphznqzp0hnazsdfk02hbg0ndbs2pmkls") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1q6ipghibq4kf6y3i9y3vlc55hi74afh6b6jh53vm2g374d37h95") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1djmrxqchm1hlxj7im55i7rd5ckxhm5b4s5hrs6ximqnx325mgmq") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1hdqrziqy43l7b4095ykgmv0pvqf7d9ypjph278a4h95mzbjhwaj") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1fiwirjii5wvlwknhjqpba1zmmq0lnf9hckxwrbmwnihinz7lz38") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "08jvgi5dsgs1bji7z5c59l7agmbhff3dwmdcdwjli1pjdivqv9xk") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.8") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0rz9djv7lk7dp7c2b0c94my93hvpzcv3s3qlk5ai7vr7w9dq4cn5") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.0.9") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1492d6gks0vbzchiplc74jr55n6ln8v2n2r1gw9wnjk535ad0rz9") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1g5hcl8d8qgrwjbpw3n1h8dd6sd8fd351k35jzza2jr1bgcxwdq3") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "007rfzrxw46zxs46l8fcpqnx1yw6jckpsnl7rsvzs9xxvl69vc7c") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0ddzmyrbbsqy92sm4gzj9p624vc3cpjzgqg6qagaqpn4gvy598gv") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "013d9y5z6jyi597fdmri0hr48gvnvz83n0k51kp19aq57d4hf0ww") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0mslxd31zr4z34sgllf3hs4bcfpgd8zmwhgylkxwmh635hi2fkpd") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1qmpisk9cim25134krg66r4jf02s685lcw9wm1g4845rmjmldcnb") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "17wn0j9nhgjqc0bkgc47rp2v0ixyzgx6b3fzymda3a2n8gkdgjyr") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1q885jl63xmp5q1fc45hcb6nz29n1palmdcwhscbgy2zklshnv23") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.8") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0iy7l819c7ql1y7x9660z9rhc2hz352xk806xyz6algmhrzsv5vx") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.1.9") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1w2sjl9cgnlkhbd001g8birjxjd0zjd7gpgfr4ccirgrnfind9sj") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0zjrxslr3sv8inahvvcdhfjh0i3gymfs84ww993asnqmwmcsi53m") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.2.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1nxp5kz8q24d2ij8dk10g3lf736k0g15cvniain3lcyfqlbwl3lr") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1av6x9yrmr12wxs2zbh55jyq3ysq9pkg50bl8rngsdd6iykvlsfv") (features (quote (("default") ("c_api")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1y9b62wixqzrkdmj0mpnzs02350d0plmfslnjjfjw442lb6fnvqz") (features (quote (("default") ("c_api")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0c7xw7gpfigfkl45pbmlfihjawj2w9w9m6z6pnijdkrpj1cqq2rp") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0pyyfchlfp9gq1yq5slmgqzlbpflcavsrai9nq4d2cxrc4zfq8w7") (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1jidalnxpnhgz9bjnxn0fc10v7v93mikk1p8xagvagwg6nnhxv5q") (features (quote (("stdcall") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1gmwm1rds7zs99wpg4drc9zsap1cbmdqcdrx504nxdf2m0jn1pk9") (features (quote (("stdcall") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0kqwp5h9xhy8vmg8d7gm12ayfar03g3hfn0imd1945bpmzp5p8c4") (features (quote (("stdcall") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "01szxdhwb7pff2fs9967q56k5lpw3g228y9v0n8gfkbsq5vqsgf5") (features (quote (("stdcall") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.8") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0f2pp8f6spg41b19qvgl3qfkhjjifhh9zj2k3xhw6g499l3g759j") (features (quote (("stdcall") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.3.9") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1hzn555bxc1rh94r355c35y489qa83s3lh74v81ysy90hkxscc6l") (features (quote (("stdcall") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.4.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gjlhdhv5dy2gk5v8dzlm2lbzcplj0qjv79r6r7nsxvr5lqz6ww0") (features (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.4.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "07ciiydyzp7xhms13vblg60ahxipvxka5qm85h5q1zjfdnbcwr0h") (features (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.4.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "0r8lzs8y2a9flsk4qw7jgryz65fy9b5xj8hnkqz89fdpgr4mxf6y") (features (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.4.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "131b4zbl9i1bc08vbvlwjdfav07rpj8bi7jb40rn9w7qz50g5hha") (features (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (yanked #t)))

(define-public crate-if97-1 (crate (name "if97") (vers "1.4.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "15axnzbhibgnhq144if36x8w5aznmlbdkccrlnvpnnjymvjxv5m9") (features (quote (("stdcall") ("python" "pyo3") ("cdecl")))) (yanked #t)))

