(define-module (crates-io if ac) #:use-module (crates-io))

(define-public crate-ifaces-0.0.3 (crate (name "ifaces") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0mgnyw5bjm3v0p30bmq6qk0vr92dlnpzzavgbg8gnllkil2x3862")))

(define-public crate-ifaces-0.1 (crate (name "ifaces") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.8") (default-features #t) (kind 0)))) (hash "1wc19gwj402321wiw170vm4hjq0rpr943czgi8vr4rxf9imwwp7s")))

(define-public crate-ifacialmocap-0.1 (crate (name "ifacialmocap") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1vvsxy82dvys7b7g0m9jngzc0s4n1b2ixznygj58vx0frf2drvcz")))

