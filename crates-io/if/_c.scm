(define-module (crates-io if _c) #:use-module (crates-io))

(define-public crate-if_chain-0.1 (crate (name "if_chain") (vers "0.1.0") (hash "1rbw8zacvkdiiydf4vmcimwdzynpw1lh72zvdrcn9nxq3gmdm10w")))

(define-public crate-if_chain-0.1 (crate (name "if_chain") (vers "0.1.1") (hash "0irndp2yaaka3js2y6c2jx81mxjf29g5m5x5kma5sipy00hhwqp8")))

(define-public crate-if_chain-0.1 (crate (name "if_chain") (vers "0.1.2") (hash "1f28pnnkf7a0sjyvkys0y0mk7mkc1w9wdpvj06dzcflysfyr1fv1")))

(define-public crate-if_chain-0.1 (crate (name "if_chain") (vers "0.1.3") (hash "1v2phdgq9i313svbrcaldygivd0jn25gpml7h6vyf906mbcrbb2b")))

(define-public crate-if_chain-1 (crate (name "if_chain") (vers "1.0.0") (hash "0zgcn31bahnsmsjc0cgk0cy38p8sfjs79yvi6rjs5zz5b5xhqdn3")))

(define-public crate-if_chain-1 (crate (name "if_chain") (vers "1.0.1") (hash "122hvv1kby9qzibijl04rcx94xhk9360rj0f113zrqmjbz3q0whz")))

(define-public crate-if_chain-1 (crate (name "if_chain") (vers "1.0.2") (hash "1vdrylnqw8vd99j20jgalh1vp1vh7dwnkdzsmlx4yjsvfsmf2mnb")))

