(define-module (crates-io if ly) #:use-module (crates-io))

(define-public crate-iflytek-sys-0.0.1 (crate (name "iflytek-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "090s3qwf77nrx0wsmhd9ghhah8mmp9hdagpzrhs4iwrirj3p3rri") (yanked #t) (rust-version "1.56")))

(define-public crate-iflytek-sys-0.0.2 (crate (name "iflytek-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "10y61nvhqqcjpc25gpml9g9klpsvv7n6gzy2mycadkgjxb95g3ks") (yanked #t) (rust-version "1.56")))

(define-public crate-iflytek-sys-0.0.3 (crate (name "iflytek-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "1z1vghqy10pi4lqldlcgr0z6x1c2nav5iq944j3wy5c87r5yvr3d") (yanked #t) (rust-version "1.56")))

(define-public crate-iflytek-sys-0.0.4 (crate (name "iflytek-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "023034k93km7f1wi4xx9mgjhljhagy8q8yf1adaa6awqv9adchky") (rust-version "1.56")))

