(define-module (crates-io if -t) #:use-module (crates-io))

(define-public crate-if-to-let-chain-1 (crate (name "if-to-let-chain") (vers "1.0.0") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.14.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.39") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "visit" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1g0hhx7v9ywwyl8gym8ji0pad64lsbcbg9f9bgmvc5d2wq2ki6br")))

