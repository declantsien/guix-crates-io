(define-module (crates-io if fy) #:use-module (crates-io))

(define-public crate-iffy-0.1 (crate (name "iffy") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "191gcb3niswxppvq9k7x67p7dn452r9qv458g512w8ji8pfh4dv9")))

(define-public crate-iffy-0.1 (crate (name "iffy") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qsibzgyfmncbv20jlm0pxbafv5fdjk44l22sq4z1j0zk7hz0fx1")))

(define-public crate-iffy-0.1 (crate (name "iffy") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "04pf9hchxsp42i2cpk8908zp53prfzg029hm7m32n83kixdp7n3v")))

