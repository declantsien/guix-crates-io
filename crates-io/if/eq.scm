(define-module (crates-io if eq) #:use-module (crates-io))

(define-public crate-ifeq-0.1 (crate (name "ifeq") (vers "0.1.0") (hash "1hi9fwsdh2nyzqwkzkf03cjkxy3fdjfmvighcwz1zsxn8sdc7yxd")))

(define-public crate-ifeq-0.1 (crate (name "ifeq") (vers "0.1.1") (hash "0lsa20f6swhxbivgn933xvzfnj924yvnzzpmhfxzhcgvkxih848k")))

(define-public crate-ifeq-0.1 (crate (name "ifeq") (vers "0.1.2") (hash "0i59cy69rddhz3rcdqzkf0nyxil35by3wii2dhssqmzy609hv93b")))

(define-public crate-ifeq-0.1 (crate (name "ifeq") (vers "0.1.3") (hash "1g2h9yfkjpddk1p0mp9580c9mkg7pkdgcib264h22pl024ijfgwf")))

