(define-module (crates-io if _r) #:use-module (crates-io))

(define-public crate-if_rust_version-0.1 (crate (name "if_rust_version") (vers "0.1.0") (hash "08b49dca32g05rqll07bqmf86f1xqphzn8bdxbq6f98p0100mkgx")))

(define-public crate-if_rust_version-1 (crate (name "if_rust_version") (vers "1.0.0") (hash "1v6mj3vqy5g0x7gpyg5kiivm42qqgras69cxb0hrg4w67qrwpns6")))

