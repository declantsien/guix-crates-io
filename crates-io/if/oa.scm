(define-module (crates-io if oa) #:use-module (crates-io))

(define-public crate-ifoam-0.0.0 (crate (name "ifoam") (vers "0.0.0") (deps (list (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "01rv8i1da9swd9qcvmkpf12fwnhrmkbb2h2bkjd1i00wpjd00sy9")))

(define-public crate-ifoam-0.0.1 (crate (name "ifoam") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)))) (hash "1q2hsnirai93ajwd466d7jl55xf9hwyy19753vls4mvninafx1fs")))

