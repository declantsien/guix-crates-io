(define-module (crates-io if _e) #:use-module (crates-io))

(define-public crate-if_empty-0.1 (crate (name "if_empty") (vers "0.1.0") (hash "0jx5z86wzcpyvz2ysbhg0bk5f35lj3ajv4j2sw05363r6sdvr9db")))

(define-public crate-if_empty-0.2 (crate (name "if_empty") (vers "0.2.0") (hash "1klqyah28m5abq7q6aivs9iib1rm0xirq89q4bjfkxnlrf9q0b27")))

(define-public crate-if_empty-0.3 (crate (name "if_empty") (vers "0.3.0") (deps (list (crate-dep (name "if_empty_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0disyknnlazsdqkg3hxkvir8gda1a331g0p4zldimq6a75mw0b1g")))

(define-public crate-if_empty_derive-0.1 (crate (name "if_empty_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a5c2mnxjjd0q287456hfr0n7id3h231ikci9vcjj4f283b14ksd")))

