(define-module (crates-io if -d) #:use-module (crates-io))

(define-public crate-if-decompiler-0.1 (crate (name "if-decompiler") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1zapsy85mwsjlscck2zz6vsljshl7xynqym5q2m3gr77qlaz1xyk")))

