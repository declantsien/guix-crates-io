(define-module (crates-io if -n) #:use-module (crates-io))

(define-public crate-if-newer-0.0.0 (crate (name "if-newer") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 2)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "cradle") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "executable-path") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0r5gclk2a2jh0d4slljvbkv9ix5cyysfbad9y363j4kh2nqxksyn")))

(define-public crate-if-none-0.0.1 (crate (name "if-none") (vers "0.0.1") (hash "0cnh16sr8mlg4sp3ki6jz4w61lwjzd8i3zf340yssavbmbs9qhf4")))

(define-public crate-if-none-0.0.2 (crate (name "if-none") (vers "0.0.2") (hash "08mx4qxsjlc7w7i6f2097lnf4cbxsmjmprza5x9455x5ip2817zm") (yanked #t)))

(define-public crate-if-none-0.0.3 (crate (name "if-none") (vers "0.0.3") (hash "0cb92cwyg20vx2qsrj0n3nlqdk9vpd00i7cplzpijmsiljnin8c8") (yanked #t)))

(define-public crate-if-none-0.0.5 (crate (name "if-none") (vers "0.0.5") (hash "1j9hch7y4jnzmrfhd133nqpj40adbb6vvc8ff8rxy1yr56nj12nl")))

