(define-module (crates-io if st) #:use-module (crates-io))

(define-public crate-ifstructs-0.1 (crate (name "ifstructs") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11") (default-features #t) (kind 2)))) (hash "02gab6pd7n0pzl080hvjx5gq02dwmmrpyv5kmp2c2pgzv82ms93z")))

(define-public crate-ifstructs-0.1 (crate (name "ifstructs") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11") (default-features #t) (kind 2)))) (hash "10cmbra7bbcnyphygs3z7sw5p6117xaic6w56dm8gsm5j87pfkdj")))

