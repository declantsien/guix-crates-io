(define-module (crates-io if un) #:use-module (crates-io))

(define-public crate-ifun-grep-0.1 (crate (name "ifun-grep") (vers "0.1.0") (hash "0f56w5jc2wk2n1l95a2k6chd0383p6gqmmx1y6spz12i8vzldj24")))

(define-public crate-ifun-grep-0.2 (crate (name "ifun-grep") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.71") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "037ai0cvy7vffl03n1pqn6ywwkcm54f8p101w0spvx6bz1vspi0m")))

(define-public crate-ifunky-0.1 (crate (name "ifunky") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1kap3m3b88anpnk8nmnbl2ckhqsppnr6vc65k9ym46ix80xj03b1")))

(define-public crate-ifunky-0.1 (crate (name "ifunky") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0k21jncsqi2j5fdjf9548bfna234x65fkj06qpr848xvsm7jia9v")))

(define-public crate-ifunny-basic-0.1 (crate (name "ifunny-basic") (vers "0.1.0") (deps (list (crate-dep (name "easy_base64") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "sha1_smol") (req "^1.0.0") (kind 0)) (crate-dep (name "sha256") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (kind 0)))) (hash "0vy6ffmljnd70b458ma31zv8hfsknv73fm3lj629d5808arc086h")))

