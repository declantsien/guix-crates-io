(define-module (crates-io if le) #:use-module (crates-io))

(define-public crate-iflet-0.1 (crate (name "iflet") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1xryr5g49f9pjqq7379hghr1fh7n5f3ck9bn5k27jiwmyri70pbv")))

