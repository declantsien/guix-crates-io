(define-module (crates-io if fc) #:use-module (crates-io))

(define-public crate-iffc-0.1 (crate (name "iffc") (vers "0.1.0") (hash "1mv6x2xfwx2v0f7xqd483mybyfmaffbhi0ysa0d2an59gs7vqd4d")))

(define-public crate-iffc-0.1 (crate (name "iffc") (vers "0.1.1") (hash "03ic4z1yngb875v6m4kc9r1j9sqcj2fbma38dfzydgsgn0ih8nf2")))

(define-public crate-iffc-0.1 (crate (name "iffc") (vers "0.1.2") (hash "16v4wk94j40fjbzh8i7hmz690s02c8vqjz4bmy6knzd622h1h22g")))

