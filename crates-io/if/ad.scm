(define-module (crates-io if ad) #:use-module (crates-io))

(define-public crate-ifaddrs-0.1 (crate (name "ifaddrs") (vers "0.1.0") (deps (list (crate-dep (name "c_linked_list") (req "~1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ifaddrs-sys") (req "^0.1.0") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "~0.2.28") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "~1.1.0") (default-features #t) (kind 2)))) (hash "1kr3vflbd7sg3vcqmcv5wgl9pm3qwk06vdkjpw106a282vrrfz4z") (yanked #t)))

(define-public crate-ifaddrs-sys-0.1 (crate (name "ifaddrs-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.2.28") (default-features #t) (kind 0)))) (hash "070b9lc0lp0y1dfipf4f0i5xi2jccj6hc890yi6qfq79py6fw76z") (yanked #t)))

