(define-module (crates-io if sc) #:use-module (crates-io))

(define-public crate-ifsc-calendar-api-0.1 (crate (name "ifsc-calendar-api") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19rq32zfz4pkmw1ifqsvwmcwmg7sqlf9ip97gbbsyxsji3ii7z0x")))

(define-public crate-ifsc-calendar-api-0.1 (crate (name "ifsc-calendar-api") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0szx0xakiqx8di00fwjv7c7kw87pihmbfd2042hh5bn52cmnsgq8")))

(define-public crate-ifsc-calendar-api-0.2 (crate (name "ifsc-calendar-api") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0n3jhmlxgfkck4vb9hy314nnykr0qqrwj1643x9z0216qlkq72g5")))

