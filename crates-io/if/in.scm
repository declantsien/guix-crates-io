(define-module (crates-io if in) #:use-module (crates-io))

(define-public crate-ifinfo-0.1 (crate (name "ifinfo") (vers "0.1.0") (deps (list (crate-dep (name "interfaces2") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "14rmwcjz4xp57rn7nz0h37iizgj3d53zxg14f7wdbz3sj82z3jfp")))

