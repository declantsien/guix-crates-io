(define-module (crates-io kd ri) #:use-module (crates-io))

(define-public crate-kdri-0.4 (crate (name "kdri") (vers "0.4.0") (deps (list (crate-dep (name "bluetooth-serial-port") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "mio") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ix9j74ayg3pda6xpq54nxwf6gidpcarj39x47l1bc0q4z3pbjls")))

(define-public crate-kdri-0.4 (crate (name "kdri") (vers "0.4.1") (deps (list (crate-dep (name "bluetooth-serial-port") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "mio") (req "^0.5") (default-features #t) (kind 0)))) (hash "1h5f1dx26p8i9mfsbxsip46gn2i3j5hpabcikin0wi60x8jhwwbs")))

(define-public crate-kdri-0.4 (crate (name "kdri") (vers "0.4.2") (deps (list (crate-dep (name "bluetooth-serial-port") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ksidivyjk9vgk8ik632rr1slrckmjfclm537x5pnmbzs9wanb6s")))

(define-public crate-kdri-0.4 (crate (name "kdri") (vers "0.4.3") (deps (list (crate-dep (name "bluetooth-serial-port") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)))) (hash "120sci5kwh2jfcrgq7lfqg2wya6fb4df6aqp96pfyffgqj2ssl1g")))

