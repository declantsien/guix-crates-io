(define-module (crates-io kd tr) #:use-module (crates-io))

(define-public crate-kdtree-0.0.0 (crate (name "kdtree") (vers "0.0.0") (hash "1c6dlilg4d12hq848pg2a35h68qxxlsp5awm4c4ma01cprlwjm68")))

(define-public crate-kdtree-0.1 (crate (name "kdtree") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "~0.3.9") (default-features #t) (kind 2)))) (hash "0iqyz1yrqb8av0fscnyqmfw8dcyyb93931pjqz2vb8zanjxgx54y")))

(define-public crate-kdtree-0.2 (crate (name "kdtree") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "~0.3.9") (default-features #t) (kind 2)))) (hash "1b15k9z23y26fh8j8zdd4azqiginhn3kn76974y6g784312dpwsx")))

(define-public crate-kdtree-0.2 (crate (name "kdtree") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "~0.3.9") (default-features #t) (kind 2)))) (hash "1x8zzs5rlkk87794kbpiynwgylnibalcmbpd7pjp39h825hanpjx")))

(define-public crate-kdtree-0.3 (crate (name "kdtree") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "~0.3.9") (default-features #t) (kind 2)))) (hash "16zr47y3n9k7f2dxsirbzivv405q096py1a8706rk4ibv667rr2i")))

(define-public crate-kdtree-0.3 (crate (name "kdtree") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "~0.3.9") (default-features #t) (kind 2)))) (hash "1xnlmy370h01jh1m6s7q5if81var4dl5avlhi8ssgihg11bb3ml5")))

(define-public crate-kdtree-0.3 (crate (name "kdtree") (vers "0.3.2") (deps (list (crate-dep (name "rand") (req "~0.3.9") (default-features #t) (kind 2)))) (hash "0s9yc1f927lq9ldx289mgkvkim2v6bx9mbzdxv2csblaqrpjs7y7")))

(define-public crate-kdtree-0.3 (crate (name "kdtree") (vers "0.3.3") (deps (list (crate-dep (name "rand") (req "~0.3.9") (default-features #t) (kind 2)))) (hash "05ac8xv0hx1h3qxpj3wgxrh36c74frx3sxhi2gw8b4cbwvca9c6j")))

(define-public crate-kdtree-0.4 (crate (name "kdtree") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 2)))) (hash "13mgcl61j4g1ax4g876cfkacn3lwfcq4qxr8x9c8njig32vmbid6")))

(define-public crate-kdtree-0.5 (crate (name "kdtree") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0nhay5p4s23276qhsmfw3q75p7bcllgl9lid1ngrzbzgr3mwxpwg") (features (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-kdtree-0.5 (crate (name "kdtree") (vers "0.5.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "011v3fixz84qhk9fhmsf358695c3j1alk5dgzclf8zvih686agdn") (features (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-kdtree-0.6 (crate (name "kdtree") (vers "0.6.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "18mip12jillxil7agmi7l5lyq9yxqikla2pwwglqg47w529kbvl0") (features (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-kdtree-0.7 (crate (name "kdtree") (vers "0.7.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fj7bq8w58sx73dgc1zdzyg33idmgak9f3yhmb4vlr8bfyghw2hg") (features (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-kdtree-ray-0.1 (crate (name "kdtree-ray") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0qi7n32xcxclwd1f6phkskpfvss113yzgmi1bg7zwwwhmabq1gn4")))

(define-public crate-kdtree-ray-0.1 (crate (name "kdtree-ray") (vers "0.1.1") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "03zbyblq1lghc647aj8dlcc2dw66a6xn0fxcny719cn98xgl2jki")))

(define-public crate-kdtree-ray-0.1 (crate (name "kdtree-ray") (vers "0.1.2") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0dbsy9gh5crzzrdq2xqwmaysxg8z51wxj9k8mbkjl73h8ivfnsw9")))

(define-public crate-kdtree-ray-1 (crate (name "kdtree-ray") (vers "1.0.0") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.4.2") (default-features #t) (kind 0)))) (hash "1g13zps1abngnqwi644k14i5ycq4vxy5ag88gh7z0sppf449c6aq")))

(define-public crate-kdtree-ray-1 (crate (name "kdtree-ray") (vers "1.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.4.2") (default-features #t) (kind 0)))) (hash "0dq07kvflg0bhwfdga6zg7cijgrb8hf60h7ryc6vac8wj87a4kn2")))

(define-public crate-kdtree-ray-1 (crate (name "kdtree-ray") (vers "1.2.0") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1a1yiq5s0rsa2bi6qdrrjjpldkmgs7ji2wk8ajlh7s1yh4p91ya5")))

(define-public crate-kdtree-ray-1 (crate (name "kdtree-ray") (vers "1.2.1") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.7.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "07zsxirnpd3d3i457fycrpfd66dbghz0pzaq81fxi9hfk067v7nx")))

(define-public crate-kdtree-rust-0.1 (crate (name "kdtree-rust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0mlrk7wd2py956z2j9yz0nl1ghvil70nsyj1wvljx763g63k2nmy")))

(define-public crate-kdtree-rust-0.1 (crate (name "kdtree-rust") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1mm3zz1l71fmi312jbx5rczgzrh2laypvpzkhavsnqqls1myhp54")))

(define-public crate-kdtree-rust-0.1 (crate (name "kdtree-rust") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0fz5rll4wi1s77adkrdlwgrabrznlq616a5slkflph09qh2k6k22")))

(define-public crate-kdtree-simd-0.6 (crate (name "kdtree-simd") (vers "0.6.1-alpha.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0kvxs38i2gy4bng43w1wd0q17rmhdb8qzg3p1adj61kljx1g0rlc") (features (quote (("serialize" "serde" "serde_derive"))))))

