(define-module (crates-io kd l2) #:use-module (crates-io))

(define-public crate-kdl2-0.0.0 (crate (name "kdl2") (vers "0.0.0") (deps (list (crate-dep (name "kii") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (default-features #t) (kind 0)))) (hash "0xp31w96rsvl2m8ad2x16v4dr6jgbpqvqzrpy34xm36x5vsjgadg")))

(define-public crate-kdl2xml-0.1 (crate (name "kdl2xml") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "kdl") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0l16aynva34h9jvpa7x2n0ca68nz9r4a6m03l8qnv0lsdxx41rg7") (rust-version "1.62")))

