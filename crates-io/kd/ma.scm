(define-module (crates-io kd ma) #:use-module (crates-io))

(define-public crate-kdmapi-0.1 (crate (name "kdmapi") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi" "errhandlingapi" "sysinfoapi"))) (default-features #t) (kind 0)))) (hash "1vp7zqhjd2wli2hqf86av6vs809c517fkyc309fcwi5nhlrykxy9")))

