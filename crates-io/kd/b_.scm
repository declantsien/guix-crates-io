(define-module (crates-io kd b_) #:use-module (crates-io))

(define-public crate-kdb_c_api-0.1 (crate (name "kdb_c_api") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "0x407b1hhw76j839sgsbwyksfp9hqm3xsbw39f1rmldnhspvrgjg")))

(define-public crate-kdb_c_api-0.1 (crate (name "kdb_c_api") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "0y1v448fgk00mnm217la2p3ciql4sdalzdrn628h3a97skws22fa") (yanked #t)))

(define-public crate-kdb_c_api-0.1 (crate (name "kdb_c_api") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "083pdjck90vi70j5gb6aciy6lz52wf7lhdsk91xrx8c6crqp7fzv") (yanked #t)))

(define-public crate-kdb_c_api-0.1 (crate (name "kdb_c_api") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1wplzabmkimisvj5awx98mbnblprg2gza0xd6y6wkbskwxi7djan") (yanked #t)))

(define-public crate-kdb_c_api-0.1 (crate (name "kdb_c_api") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "18a5mql2vv98972ifdyr84a6apx3chcpabnr85icfc35l9bn4jyb")))

(define-public crate-kdb_c_api-0.1 (crate (name "kdb_c_api") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1km15sm67nvsnnmfdvv90bqh5hk2z96ldm3vdl785gba9w7knjnd")))

(define-public crate-kdb_c_api-0.1 (crate (name "kdb_c_api") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "0901lhzngy4hfd7p2rg8aw64yyr0pczp15cvlnf3j029419ry69j")))

