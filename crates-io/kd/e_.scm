(define-module (crates-io kd e_) #:use-module (crates-io))

(define-public crate-kde_frameworks-0.1 (crate (name "kde_frameworks") (vers "0.1.0") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 1)))) (hash "0xqgkxqsina17c19i5y1dil5dqhx541qzxwl7ah64j69g8kskyps")))

(define-public crate-kde_frameworks-0.1 (crate (name "kde_frameworks") (vers "0.1.1") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 1)))) (hash "0jkdpzgcz6livcqjfqgy6lmmzvxaxnn8337vcasy5gkmycggfl15")))

(define-public crate-kde_frameworks-0.1 (crate (name "kde_frameworks") (vers "0.1.2") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 1)))) (hash "19i0jc0fl4l820rxj1rf4rkkbf6i8c50iqsvqqc9bvxkirix7zdg")))

(define-public crate-kde_frameworks-0.1 (crate (name "kde_frameworks") (vers "0.1.3") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 1)))) (hash "13b9l04y4vg9zgzlrn714rs30bbffl1knzv9l6rbzc0vm861c3qk")))

(define-public crate-kde_frameworks-0.1 (crate (name "kde_frameworks") (vers "0.1.4") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 1)))) (hash "00gs3zxadq8cy96h1syqjcfnvrkrcxva8biifn3mp2zysznyfkvi")))

(define-public crate-kde_frameworks-0.2 (crate (name "kde_frameworks") (vers "0.2.0") (deps (list (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wbdvj9sankhpdmhqkr1h2d15lgqqkql6yl66ahmx6gmw759h0zi")))

