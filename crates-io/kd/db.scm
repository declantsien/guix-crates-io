(define-module (crates-io kd db) #:use-module (crates-io))

(define-public crate-kddbscan-0.1 (crate (name "kddbscan") (vers "0.1.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1qzwx07lcii05dd2pqawavwg9qhv0z40cmd4ma4bl8llrzz0frci")))

