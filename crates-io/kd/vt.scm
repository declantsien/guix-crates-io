(define-module (crates-io kd vt) #:use-module (crates-io))

(define-public crate-kdvtree-0.5 (crate (name "kdvtree") (vers "0.5.0") (hash "0lq75ixkaf2m32i8gxwrxa40h43faq0g45kcb36aq0pg7hmwdqss")))

(define-public crate-kdvtree-0.6 (crate (name "kdvtree") (vers "0.6.0") (hash "1dyra4wjdjh4k9x6acrn9wjc63izcfid79a6xajynxs7v7z08n84")))

(define-public crate-kdvtree-0.7 (crate (name "kdvtree") (vers "0.7.5") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1jq6lhy36vv0ykmmdlg480g51vj28pxd2qk011ynd6c8mmbkxyic")))

(define-public crate-kdvtree-0.8 (crate (name "kdvtree") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0y3wnsg1zfj0hjf4ly9n6a0sz8mlgz8jrm0qi5cjaz1drsnydqb0")))

