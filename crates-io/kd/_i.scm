(define-module (crates-io kd _i) #:use-module (crates-io))

(define-public crate-kd_interval_tree-0.1 (crate (name "kd_interval_tree") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "18kip9cbp1wlckvcfr0dpsl5xw6wf5w4svp5khjlnwj5m4f2gj2z")))

(define-public crate-kd_interval_tree-0.1 (crate (name "kd_interval_tree") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1dwjs5dnd9mr4nilalqn39q3fxmkby777vbq8mk4wgyb59sc8a5z")))

