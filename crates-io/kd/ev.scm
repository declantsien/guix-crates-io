(define-module (crates-io kd ev) #:use-module (crates-io))

(define-public crate-kdev-0.0.0 (crate (name "kdev") (vers "0.0.0") (hash "0kxjxb8p63dfzdc69j4cacws3pkynjia3z809xs4z4b97iw7zrrb") (yanked #t)))

(define-public crate-kdev-0.0.1 (crate (name "kdev") (vers "0.0.1") (hash "0q1fxmy0wrgwbfvcn09bdplgrrlrkx1cd7rw4naclrzq8z4rnhl4") (yanked #t)))

(define-public crate-kdev-0.0.2 (crate (name "kdev") (vers "0.0.2") (hash "12dn3ic9rwfznyn8m60vma7c9hrhpv93yvl73d9wsm6w7gqa6xiw") (yanked #t)))

