(define-module (crates-io bt ru) #:use-module (crates-io))

(define-public crate-btrup-0.1 (crate (name "btrup") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "docopt_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0ycqh4arm7cis5m70q0mgb99ac9xhpamgrjp31c9vsx7zxlbjvbs")))

(define-public crate-btrup-0.1 (crate (name "btrup") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "docopt_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1v0j6mbnb49vz8d492axznp3kqwmm6dx9njllga47cjk6w6a12ki")))

