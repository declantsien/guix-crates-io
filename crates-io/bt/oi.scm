(define-module (crates-io bt oi) #:use-module (crates-io))

(define-public crate-btoi-0.1 (crate (name "btoi") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "1hx38cxp99jicis7wydvkk55h4fasfdqri75b04271vkfy84w1hl")))

(define-public crate-btoi-0.1 (crate (name "btoi") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "1qbsqx76rynmx9b5pl0mazxy17n4w7xphhxgznd26r9pfgwwxcgv")))

(define-public crate-btoi-0.1 (crate (name "btoi") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.5") (default-features #t) (kind 2)))) (hash "1kgcqqic1krng42g1p59hxwily86dhwb339856f9cy60hz3cxii4")))

(define-public crate-btoi-0.1 (crate (name "btoi") (vers "0.1.3") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0ah9379rmdh4gvzjcp1i7agwa58v1ykdr35lyy4yvdsqdcnipa3x")))

(define-public crate-btoi-0.2 (crate (name "btoi") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0p88d7nmzv49lblikcjgd9ivkhpngppgd8xz6ip63a0xcbn4x6nw")))

(define-public crate-btoi-0.3 (crate (name "btoi") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "16c3v220x6fnj0czss923vxyrb21j4kl2q2jxqnbdz4ya3zfq5cd") (features (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4 (crate (name "btoi") (vers "0.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)))) (hash "0n3n93nm2qcdspl7f4p7x070ws1gj7jnc4w1vcjdwsd3q4q8cxk3") (features (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4 (crate (name "btoi") (vers "0.4.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)))) (hash "0w8lhvwjpz3liza1hwsr6dhzl22qfgnkvm88s19ybnbhbqhdikp4") (features (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4 (crate (name "btoi") (vers "0.4.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "04ba4j96icaan10c613s2rwpn2kdbl8728qhz2xzi0dakyd8dh4p") (features (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-btoi-0.4 (crate (name "btoi") (vers "0.4.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1bg02zgsv5njbhng9sq2b70przbazsczjbla5lbbdf59fdzl1mlx") (features (quote (("std" "num-traits/std") ("default" "std"))))))

