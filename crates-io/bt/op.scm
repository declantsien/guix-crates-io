(define-module (crates-io bt op) #:use-module (crates-io))

(define-public crate-btop-0.1 (crate (name "btop") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.23.0") (features (quote ("all-widgets"))) (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.29.10") (default-features #t) (kind 0)))) (hash "0djchikn05j3ayppwxcv8qjlljl325y86rg0yxl29g2gx8qbbjai")))

