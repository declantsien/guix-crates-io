(define-module (crates-io bt wn) #:use-module (crates-io))

(define-public crate-btwn-0.2 (crate (name "btwn") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0w0m2xib5br9anz36jwdbh9s3mvm400iq5dns15vcfp72xaqiy33")))

(define-public crate-btwn-0.2 (crate (name "btwn") (vers "0.2.1") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vlb4ji40y2rxhaqyzjkkvbib414sjavdb0svwnvlrpg3fbz5ngm")))

(define-public crate-btwn-0.3 (crate (name "btwn") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0k87f6gazjm706r85mg1v7yk2gpqxj8lhmw717w983amw1fdc4qh")))

(define-public crate-btwn-0.4 (crate (name "btwn") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pmmqqyrjs73462hxcfa56cg387b2bvv6xxii1inw56a40fgx4z8")))

(define-public crate-btwn-0.5 (crate (name "btwn") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vhxhj2iw8ivq1ihgnql9mvfmffykahcvi245zw457h7yklzck6k")))

(define-public crate-btwn-0.5 (crate (name "btwn") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "16bg9jww8x6qsl8in66dhzw1ipgdhnwg430qb72nb4s8py0m9hxd")))

(define-public crate-btwn-0.6 (crate (name "btwn") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1qz5gaxdqy0scljxz404xjifwk896bib9z8szj8rrxlqdc2wjxpv")))

