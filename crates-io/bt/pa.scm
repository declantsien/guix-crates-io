(define-module (crates-io bt pa) #:use-module (crates-io))

(define-public crate-btparse-0.1 (crate (name "btparse") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.3.8") (default-features #t) (kind 2)))) (hash "03xql0731mvmg1qay9z61jpx07rsx34cnav43rkiwnk367cl2mxk")))

(define-public crate-btparse-0.1 (crate (name "btparse") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.3.8") (default-features #t) (kind 2)))) (hash "19i8zy9zdg4bxgfwx0qh0w00rv0fmlmnqb9675ywpxhf9pqb73ya")))

(define-public crate-btparse-stable-0.1 (crate (name "btparse-stable") (vers "0.1.2") (deps (list (crate-dep (name "eyre") (req "^0.3.8") (default-features #t) (kind 2)))) (hash "17zqyicg63y5p831jg0qx1pdym1q7jp84i6ws60zhlnj5qjvhx8d")))

