(define-module (crates-io bt sd) #:use-module (crates-io))

(define-public crate-btsdu-0.1 (crate (name "btsdu") (vers "0.1.0") (deps (list (crate-dep (name "btrfs-send-parse") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "1ig0diram5n1phxqyg6kbadssh9nk0mcasbwanwdrhkglqjlvr9j")))

