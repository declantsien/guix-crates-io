(define-module (crates-io bt or) #:use-module (crates-io))

(define-public crate-btor2rs-0.1 (crate (name "btor2rs") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1w16l0dnv6wr1zx56x9a9mvaii0csghcvgjx3rmqf02gwfkjnw17") (rust-version "1.70")))

(define-public crate-btor2rs-0.1 (crate (name "btor2rs") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1zhqjx3jdc85aj500l9jxhmapvmj3p1i8dkhaddim8s9cwsbqigr") (rust-version "1.70")))

(define-public crate-btor2rs-0.1 (crate (name "btor2rs") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0qm5f0m4n7ajnpnib183rycivjb3wzlk5xmgk809k7cmxhkb3rfm") (rust-version "1.70")))

(define-public crate-btor2rs-0.1 (crate (name "btor2rs") (vers "0.1.0") (deps (list (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0bimq4cr7whfzvjsqg1sgc4bpvdnir31f8za0rv6q6sffji2sr9n") (rust-version "1.70")))

(define-public crate-btor2tools-1 (crate (name "btor2tools") (vers "1.0.0") (deps (list (crate-dep (name "btor2tools-sys") (req "~1.0.0") (default-features #t) (kind 0)))) (hash "0blxa6jzdgajfycvdhql42xa8iv5arbak32nm35b6yjr2spp42n9")))

(define-public crate-btor2tools-1 (crate (name "btor2tools") (vers "1.1.0") (deps (list (crate-dep (name "btor2tools-sys") (req "~1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1.0.22") (default-features #t) (kind 0)))) (hash "16rn66y2fy8rlx1qnv1h4zfh8q0ww9vjcwkw41a13y02qaiyqds3")))

(define-public crate-btor2tools-sys-1 (crate (name "btor2tools-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "~0.55.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "~0.1.2") (default-features #t) (kind 1)))) (hash "0xqx0kfswgfhygd1xb3zdrskyshach2fv66yman6rpvbximlrz0d") (links "btor2parser")))

(define-public crate-btor2tools-sys-1 (crate (name "btor2tools-sys") (vers "1.1.0") (deps (list (crate-dep (name "bindgen") (req "~0.68.0") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "~0.1.2") (default-features #t) (kind 1)))) (hash "0pr8krcdyzbp1xwbm8gvz8xj3gbmhskkycxyz6sw8ja0f37rl4ys") (links "btor2parser")))

