(define-module (crates-io bt w-) #:use-module (crates-io))

(define-public crate-btw-nl-0.1 (crate (name "btw-nl") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0b7n58fh8w6vv47n7r273b9v6z0sbipixk2rn86pgjazi8d13rxn")))

(define-public crate-btw-nl-0.1 (crate (name "btw-nl") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "150vz0z0468vnmszna59mvz49s8z9bxlra09ml8ls1vh12vgy3za")))

(define-public crate-btw-nl-0.2 (crate (name "btw-nl") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "16z3pwg2ka3p8q0j28hslss1fx9dnx5m0mcg3gdr79661z3yjbki")))

(define-public crate-btw-nl-0.2 (crate (name "btw-nl") (vers "0.2.1") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "16qzs0sjx7423k56iqj2kf0i5hln6r508bhkix6r05axicqy4dib")))

