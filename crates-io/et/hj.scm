(define-module (crates-io et hj) #:use-module (crates-io))

(define-public crate-ethjson-0.1 (crate (name "ethjson") (vers "0.1.0") (deps (list (crate-dep (name "ethereum-types") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m5hwwmxryjjbcmhfkns4mywbgdkwqmvsa3jr0sgmkwdshrfqqkq") (features (quote (("test-helpers"))))))

