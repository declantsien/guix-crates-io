(define-module (crates-io et ch) #:use-module (crates-io))

(define-public crate-etch-0.1 (crate (name "etch") (vers "0.1.1") (deps (list (crate-dep (name "glue") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (default-features #t) (kind 0)))) (hash "0mp0pb07pwvk3br80knd6xfk56dj8rkzw555lr0ifvc00swk03mf")))

(define-public crate-etch-0.2 (crate (name "etch") (vers "0.2.1") (deps (list (crate-dep (name "glue") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (optional #t) (default-features #t) (kind 0)))) (hash "16n57ixlvxa72n39165ssawxbs9c6042pk6s8rczyb4a80s5vq0g") (features (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.3 (crate (name "etch") (vers "0.3.1") (deps (list (crate-dep (name "glue") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (optional #t) (default-features #t) (kind 0)))) (hash "1v567rj8f5knqxlyynsmbmp3p327fhnzgz6ws186dp2x92l6rsig") (features (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.4 (crate (name "etch") (vers "0.4.1") (deps (list (crate-dep (name "bork") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glue") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (optional #t) (default-features #t) (kind 0)))) (hash "0nswm7jzg30a7qd3n56h8jfiqagilxn7klsy5c7cvpdq71nsixnw") (features (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.1 (crate (name "etch") (vers "0.1.2") (deps (list (crate-dep (name "glue") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (default-features #t) (kind 0)))) (hash "174z43kks3mf7pr4c2kv4i6kb22vkmaxr3n7mvh6lqzibn64z0bc")))

(define-public crate-etch-0.2 (crate (name "etch") (vers "0.2.2") (deps (list (crate-dep (name "glue") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (optional #t) (default-features #t) (kind 0)))) (hash "05wlfihhah9zq49dihcp5pqxqg20659qxbwjcrlfijm4rixq84ph") (features (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.3 (crate (name "etch") (vers "0.3.2") (deps (list (crate-dep (name "glue") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (optional #t) (default-features #t) (kind 0)))) (hash "0cfhb1jgkdc092k4fdh8wi8kmmdfr91r22aczwl7zawlzf42187h") (features (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etch-0.4 (crate (name "etch") (vers "0.4.2") (deps (list (crate-dep (name "bork") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glue") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^3") (optional #t) (default-features #t) (kind 0)))) (hash "044hwrd01v1yjj8djpk3c01jbjsc0z039kkcf60980wklbvm9fdy") (features (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

(define-public crate-etcher-0.0.0 (crate (name "etcher") (vers "0.0.0") (hash "17gaj581y2c62bzksyz3ar2xkmw6qkvz2zlhdwb1yl3fvag3k0bn") (rust-version "1.74")))

(define-public crate-etchlord-0.1 (crate (name "etchlord") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "005imjw31f2c0i4aqy1gsb198849fadib8wxvygkz9p52bwhn7a4")))

