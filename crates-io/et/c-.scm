(define-module (crates-io et c-) #:use-module (crates-io))

(define-public crate-etc-express-midi-0.2 (crate (name "etc-express-midi") (vers "0.2.1") (deps (list (crate-dep (name "midir") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ahidv0dc4g1ppmjnf1494bji4xillkjk6w0bzyxdhi4yyck5x84")))

(define-public crate-etc-os-release-0.1 (crate (name "etc-os-release") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (kind 0)))) (hash "0aqagz5hi7k1ap423sszk6qhws2iw39qm4jn5bdmavz01ib3payk") (features (quote (("default")))) (v 2) (features2 (quote (("url" "dep:url") ("date" "dep:chrono")))) (rust-version "1.70.0")))

(define-public crate-etc-passwd-0.1 (crate (name "etc-passwd") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0n3pgnga3gdi25xv4xa4l3zpp4ggh483af81gfy49pjqrdknv4il")))

(define-public crate-etc-passwd-0.1 (crate (name "etc-passwd") (vers "0.1.1") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.73") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "169kr4b9x7r44bgaw4ph080s7mbpxpl0agp7jmsp88xxqpl2vqvk")))

(define-public crate-etc-passwd-0.2 (crate (name "etc-passwd") (vers "0.2.0") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0sfahsmljgxw7b9cz2k7jaa3f8l7ipwyyb38l21nrcgf070agg0a") (rust-version "1.56.1")))

