(define-module (crates-io et ce) #:use-module (crates-io))

(define-public crate-etcetera-0.1 (crate (name "etcetera") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0zjn0n1xim2v59rpk04abi66ajv487f6c8w8ygp6r33yjsvzjksl")))

(define-public crate-etcetera-0.2 (crate (name "etcetera") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ygkh8dvb8zql86vjc9kpidkmfardg1sk3p1ghab7dbvk2flk334")))

(define-public crate-etcetera-0.2 (crate (name "etcetera") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1naw0crxkp1pfhcbcdi6ns4lk202kanwhi2hr7hi7ijcqzqq357h")))

(define-public crate-etcetera-0.3 (crate (name "etcetera") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0vrjw5p32n6hx32gl1pfkr0hlay1bhxy58w1rsz03jwmmr1kw7k8")))

(define-public crate-etcetera-0.3 (crate (name "etcetera") (vers "0.3.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0jb0ax1fvbpq6mm4971hqh21r25qaanmchdzxnb1bfqdf00fi7kr")))

(define-public crate-etcetera-0.3 (crate (name "etcetera") (vers "0.3.2") (deps (list (crate-dep (name "cfg-if") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req ">=2.0.0, <3.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req ">=1.0.22, <2.0.0") (default-features #t) (kind 0)))) (hash "1v1d2z2h1ay3skks0s4n833z275nkdf28d168cyq7ywl3vyh8sq1")))

(define-public crate-etcetera-0.4 (crate (name "etcetera") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "1zb2x5p9ajj78r5b42gmgn53zgc78jgz8lfvw5szm6sfivhzq5yh")))

(define-public crate-etcetera-0.5 (crate (name "etcetera") (vers "0.5.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "0zcycp4hgsx586ncn18glpn38502r1gcxpkpjbgl34f9z7gry2l7")))

(define-public crate-etcetera-0.6 (crate (name "etcetera") (vers "0.6.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "06wnpzpnd443i5hd0ikpgmny3073pa2xylz6sxzrkla1kyh9yfa4")))

(define-public crate-etcetera-0.7 (crate (name "etcetera") (vers "0.7.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1p38yzs2dbiaklnqqdr97q7n4k478sl10bdipgb0haldaq2db5p7")))

(define-public crate-etcetera-0.7 (crate (name "etcetera") (vers "0.7.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1827gydakhp0jaw9zkn98gwq7sc5nxb6vj6fjr6qr78jqvnjx0ji")))

(define-public crate-etcetera-0.8 (crate (name "etcetera") (vers "0.8.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "windows-sys") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_UI_Shell"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0hxrsn75dirbjhwgkdkh0pnpqrnq17ypyhjpjaypgax1hd91nv8k")))

