(define-module (crates-io et hd) #:use-module (crates-io))

(define-public crate-ethdigest-0.1 (crate (name "ethdigest") (vers "0.1.0") (deps (list (crate-dep (name "ethdigest-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "12gcx916c8nh5qqiqsq2w1y78d784hv9vzfvvf2n858ckanhrmj4") (features (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.1 (crate (name "ethdigest") (vers "0.1.1") (deps (list (crate-dep (name "ethdigest-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "14vfs8zkqca24r3fr3lc5s41nlk266d4vdlap0hz8hqfkjpbapli") (features (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.2 (crate (name "ethdigest") (vers "0.2.0") (deps (list (crate-dep (name "ethdigest-macros") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "11vcfb8kaq11525l247b0iqfc5zipnd34wxy27x8brvzy6ml0yas") (features (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.2 (crate (name "ethdigest") (vers "0.2.1") (deps (list (crate-dep (name "ethdigest-macros") (req "=0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "06rqckls523xcj3rdd2gxcmaipxnrlpkdiqcjgznzs2g90ydbf6p") (features (quote (("macros" "ethdigest-macros") ("keccak" "sha3") ("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.3 (crate (name "ethdigest") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "036s5v2z96iyykmq6xwilxrkzpsb63rmfxpls0l2fgcjw5q27gfw") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.3 (crate (name "ethdigest") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "1a96sizwar7kkzsrb69lpdp38sa4l6ysqfyh42pacqkizv95rkw6") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.3 (crate (name "ethdigest") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "0qmkg9kx3q62c8gj33dhc5zagpmvl237igb224f22kmvayvhhyd3") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-0.4 (crate (name "ethdigest") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (kind 0)))) (hash "1k0imqkxg40pkbwarhck8d59phb6js77xa441ybwzy8zpqisvb9w") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std" "sha3?/std"))))))

(define-public crate-ethdigest-macros-0.1 (crate (name "ethdigest-macros") (vers "0.1.0") (deps (list (crate-dep (name "sha3") (req "^0.10") (kind 0)))) (hash "0w22wg194735k5b6zmlf2b86y9fbmvanv3hw5k67r949i9fg5dzb")))

(define-public crate-ethdigest-macros-0.1 (crate (name "ethdigest-macros") (vers "0.1.1") (deps (list (crate-dep (name "sha3") (req "^0.10") (kind 0)))) (hash "1w64k9rwhff59wb5cin056vvzdn729wkwdxsl7jaid1kmd8k0lmz")))

(define-public crate-ethdigest-macros-0.2 (crate (name "ethdigest-macros") (vers "0.2.0") (deps (list (crate-dep (name "sha3") (req "^0.10") (kind 0)))) (hash "19k3fmjg0pgw4k07c6wgbwp4s1r4gmswvbm1r4k9h8yg3w1ffbrw")))

(define-public crate-ethdigest-macros-0.2 (crate (name "ethdigest-macros") (vers "0.2.1") (deps (list (crate-dep (name "sha3") (req "^0.10") (kind 0)))) (hash "1a0m9sci4kw8yzj9lvb1vznw1m1jvmv9pbwy58g76l09k204hhxx")))

