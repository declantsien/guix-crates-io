(define-module (crates-io et ho) #:use-module (crates-io))

(define-public crate-ethock-0.0.3 (crate (name "ethock") (vers "0.0.3") (deps (list (crate-dep (name "rouille") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vg0fdnv809lbrhnb0cwi9g94hm03ba61sixmiq80gh3yfz8pbsi")))

(define-public crate-ethock-0.1 (crate (name "ethock") (vers "0.1.0") (deps (list (crate-dep (name "jsonrpc-core") (req "^17.0.0") (default-features #t) (kind 0)) (crate-dep (name "jsonrpc-http-server") (req "^17.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.0") (features (quote ("blocking" "json"))) (default-features #t) (kind 2)))) (hash "03d46wwk31k87fbbmr24hybf0vjsxrz32h392283a55r21gnlpx7")))

(define-public crate-ethox-0.0.1 (crate (name "ethox") (vers "0.0.1-wip") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)))) (hash "05j4s8h2v6w3w9h6x98fsdb90wv55mb3ykgnlgs7jqvrjs4kgxly")))

(define-public crate-ethox-0.0.1 (crate (name "ethox") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (kind 2)))) (hash "1kdsxl06r1c14x40vy2yfpwfxz1a4z2vc0gndlmi99fwc4ni6b0j") (features (quote (("sys" "libc") ("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-ethox-0.0.2 (crate (name "ethox") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (kind 2)))) (hash "1n77vpl1mz1vw85mf965sff20yr8w2mvwr0d8mf2rg0kawyc9rcg") (features (quote (("sys" "libc") ("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-ethox-io-uring-0.0.2 (crate (name "ethox-io-uring") (vers "0.0.2") (deps (list (crate-dep (name "ethox") (req "^0.0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "io-uring") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (kind 0)))) (hash "1x478zq1m8dv51nhiw55ghiigbprxsgbkb0h7810fbj0invqkm33")))

