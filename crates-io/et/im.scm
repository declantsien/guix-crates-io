(define-module (crates-io et im) #:use-module (crates-io))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.0") (hash "1f6izngafhzmk7rzqhz5yr9cvk4sfw1z54s4mc9p7c7bry5rj6l5")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.1") (hash "0ki44lq9ic3fzqhh57bkl4gjbdfxx2d8nvamk8xfi573c9h3ikbz")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.2") (hash "06nf6n3a7m4smfk06pm71pzj7n975a8lvi7sssmkvywg2xrhk406")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.3") (hash "0lcsgxr8pvk7ra9ilwgsr6dzhkjxzljg0bnwxb4p7a2m74x0vd2g")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.4") (hash "1vaichsqbk7wx34ih0mgiqdg2ni36xxwn26dq9afm9w31wvzvi2c")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.5") (deps (list (crate-dep (name "clock_source") (req "^0.1") (default-features #t) (kind 0)))) (hash "0b1663kwnb31z8dxdj75iivnxrj40knbx5bwx6mm8xb3bi0ybv7v")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.6") (deps (list (crate-dep (name "clock_source") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wqm0ixx9frlzjmi1q053a4sx3hmz2mh07f854vs2576jcxzl8y3")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.7") (deps (list (crate-dep (name "clock_source") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0fsg4sa7ij8j3nkky2xkcq31b2g7136x2j9jad0kawq4rcwzj4j1")))

(define-public crate-etime-0.1 (crate (name "etime") (vers "0.1.8") (deps (list (crate-dep (name "clock_source") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1vjrg105kfdmp5yl99cqzbpnpw74yw6j0ziadwi5l0kb45j2fc7a")))

