(define-module (crates-io et hi) #:use-module (crates-io))

(define-public crate-ethiopic-calendar-0.1 (crate (name "ethiopic-calendar") (vers "0.1.0") (hash "1mhb3qn4yvw0wdqah6nlmj3yn8rx1m6r2cvqxwh3sdx6nl3ild5i")))

(define-public crate-ethiopic-calendar-0.1 (crate (name "ethiopic-calendar") (vers "0.1.1") (hash "1xbkyiqx41dssf37ng7mdkfb9j6g65gq59kbz1xl53q33szj57qb")))

(define-public crate-ethiopic-calendar-0.1 (crate (name "ethiopic-calendar") (vers "0.1.2") (hash "0w14v018qa6vxsh5s8ybn9vs6kvzbs0nyqygmly8x5z1q0k8scfv")))

(define-public crate-ethiopic-calendar-0.1 (crate (name "ethiopic-calendar") (vers "0.1.3") (hash "0j16vd3lc2j2wypad4r9n50xqvdmzdmhjdlhq5kmnd5qan363zxz")))

(define-public crate-ethiopic-calendar-0.1 (crate (name "ethiopic-calendar") (vers "0.1.4") (deps (list (crate-dep (name "hifitime") (req "^3.8.0") (default-features #t) (kind 0)))) (hash "0smxdayjpa78hf2ifrvf31yz8wgq261776rsgizdslkk299gc9qp")))

(define-public crate-ethiopic-numerals-0.1 (crate (name "ethiopic-numerals") (vers "0.1.0") (hash "0rlf26lygbybah865rm3xawj5iv35vddx0y0yc3c2fldlwhjsfzw")))

