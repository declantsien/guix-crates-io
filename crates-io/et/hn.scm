(define-module (crates-io et hn) #:use-module (crates-io))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.0.0") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ibg50fx3fp09iicvnpiqj2kccr0bdifwil3fz3z94nxzciffrf1") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.0.1") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zvzlnj4wqjrrq3z78jsy5xbcczy08hd2z3ycd87m209x5yabhbm") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.0.2") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gp08ywpj5x7f10ad587nphq58wmzz282kxqkyvhyzqkivhq8fci") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.0.3") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "12p7f256qavq62844j2vqns2098ac771k7k4aan2bvzrw6qwczc8") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.0.4") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "08j8dv8zmmihrp17gdvpqlsc8rpw9r83ydqznm6g3j0zd8cxln0g") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.1.0") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1fhad33a8pyps96dmzzb9w5wssbi6sw1ff3rqlcy3wqjp2k5nil0") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.1.1") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1liylhsw0kb1fidsbnznvpd1cd2h90fc8rcpxggv94mdvi3h7d33") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.2.0") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "11jxz7jvbf6pdf2pm3z9d5dr0yhgbm2dif39n51x64gfncxxjqn3") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.2.1") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0iycphsq0r2d508y3m8r8wr5l3q1igzwnq40d0mh9m8bfhsfmx7n") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.2.2") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1lwa4h1rj3dnrmqm1b059cx73c9gs9s79pdlivnnmvg9vnvsgr47") (features (quote (("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.3.0") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ethnum-macros") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "049k6y7ndvbgxypra9vdlq1d24127sxc0hmvbqjpbsx6kw5krb1f") (features (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.3.1") (deps (list (crate-dep (name "ethnum-intrinsics") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ethnum-macros") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1vfi5pcqiq906ixi8mr0v0xqs8v7knks82dni25b8dwgpivyfzbd") (features (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.3.2") (deps (list (crate-dep (name "ethnum-intrinsics") (req "=1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ethnum-macros") (req "=1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "010w5k5ri2k5500igvp3zb48z0sfjwfb5jvsvkg303wf0z8bk601") (features (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.4.0") (deps (list (crate-dep (name "ethnum-intrinsics") (req "=1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ethnum-macros") (req "=1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1b5d4j4al7f40sq717bbrkiifgxvbgyfpvh6zfvpylpsna1g73vc") (features (quote (("macros" "ethnum-macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-1 (crate (name "ethnum") (vers "1.5.0") (deps (list (crate-dep (name "ethnum-intrinsics") (req "=1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0b68ngvisb0d40vc6h30zlhghbb3mc8wlxjbf8gnmavk1dca435r") (features (quote (("macros") ("llvm-intrinsics" "ethnum-intrinsics"))))))

(define-public crate-ethnum-intrinsics-1 (crate (name "ethnum-intrinsics") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.55") (default-features #t) (kind 1)))) (hash "1zjdqql6p8kwikwp0jw2jmsgfckgcl28ydz8ksk5ifrg2xlmm4yg") (links "ethnum")))

(define-public crate-ethnum-intrinsics-1 (crate (name "ethnum-intrinsics") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)))) (hash "1vjndp4wcrzsn597z2qdhbhsq4krhzy8g36cnqwhj07qaxmamrl7") (links "ethnum")))

(define-public crate-ethnum-intrinsics-1 (crate (name "ethnum-intrinsics") (vers "1.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0q54arblnkanigjxynl52zn4rhss7drx5hn9vsm93glc1742qnc2") (links "ethnum")))

(define-public crate-ethnum-intrinsics-1 (crate (name "ethnum-intrinsics") (vers "1.2.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "1qvb1r3vmnk5nplz6x1014rn6b9nfnig2qmlj8hi3jpq75j8cgh9") (links "ethnum")))

(define-public crate-ethnum-macros-1 (crate (name "ethnum-macros") (vers "1.0.0") (hash "0wn53nxc5sfb93x9dqs3zzg1qz99wm9lnb1mhljp7z8b0shlsiaj")))

(define-public crate-ethnum-macros-1 (crate (name "ethnum-macros") (vers "1.1.0") (hash "0haq14b9jzs7qxi7mj6d7vx80rmd62b1jsmldfa2z786mc8vlsds")))

