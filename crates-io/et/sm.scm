(define-module (crates-io et sm) #:use-module (crates-io))

(define-public crate-etsm-0.1 (crate (name "etsm") (vers "0.1.0") (hash "14gcxf3c8jaxfy0zmjb0qvz50myvyvr5wyz860gs980b3j5ryx1j")))

(define-public crate-etsm-0.1 (crate (name "etsm") (vers "0.1.1") (hash "1azzj1nvsxgiafyn1r2mfrk6hrn1qlnsnm5gd5higynxshjwz4wi")))

(define-public crate-etsm-0.1 (crate (name "etsm") (vers "0.1.2") (hash "0c8h6bpzxvbs739dz9wy26v7r4sp0vig25xamiizarsl109bgv6l")))

(define-public crate-etsm-0.2 (crate (name "etsm") (vers "0.2.0") (hash "0nd03vzjgzd81rqadbj8qkc5whp5gsrrwfslp9844s8hz3bqcwgp")))

(define-public crate-etsm-0.5 (crate (name "etsm") (vers "0.5.0") (hash "1lldmcwqbanvpqh5lh8jd4954r2big3fwbld0daqd0fh8yva2bwj")))

