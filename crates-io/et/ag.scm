(define-module (crates-io et ag) #:use-module (crates-io))

(define-public crate-etag-0.1 (crate (name "etag") (vers "0.1.0") (hash "17hc09qqvilvhafbxaw8hg4l3fnjmjlib5q7hcj3myws95k3nxg9")))

(define-public crate-etag-0.2 (crate (name "etag") (vers "0.2.0") (hash "0jry4b8b4g6k36c91wayzx56snb0b0sqhl09sgmw31h7vfv836v6")))

(define-public crate-etag-1 (crate (name "etag") (vers "1.0.0") (hash "0v06lrv9is7s89ylicdz05kzdcqwkdazdmasxj23msfbsfkkc6xg")))

(define-public crate-etag-1 (crate (name "etag") (vers "1.0.1") (hash "0i7gbyvqi14zz8whfpmk5p9njpbp4yg59wv7l15d7g1hyada0q6j")))

(define-public crate-etag-1 (crate (name "etag") (vers "1.0.2") (hash "1c7xgcimz48q0nrcncc0h1v232ljjf951mll6ld8viihz5cpk1s7")))

(define-public crate-etag-1 (crate (name "etag") (vers "1.0.3") (hash "02ffy0cgp7f9aphlpvd2qcy0nsq5g1kw6xpyh5z8r0062f50agck")))

(define-public crate-etag-2 (crate (name "etag") (vers "2.0.0") (deps (list (crate-dep (name "wy") (req "^1") (default-features #t) (kind 0)))) (hash "0vcma93a0nv9y4zm4y3cgpk50afbqp5agglk11cgr0b9p03kw4xy") (features (quote (("std"))))))

(define-public crate-etag-2 (crate (name "etag") (vers "2.0.1") (deps (list (crate-dep (name "wy") (req "^1") (default-features #t) (kind 0)))) (hash "0dymr1dhb1rc4s3yp50pr39gy5gkk34larr95pw2rbqdq7j1qvif") (features (quote (("std"))))))

(define-public crate-etag-2 (crate (name "etag") (vers "2.0.2") (deps (list (crate-dep (name "wy") (req "^1") (default-features #t) (kind 0)))) (hash "1568qb4py1iw82shlpwwjjbvmpwlb34pl3rq7v7dxrr4ng88k2ih") (features (quote (("std"))))))

(define-public crate-etag-2 (crate (name "etag") (vers "2.0.3") (deps (list (crate-dep (name "wy") (req "^1") (default-features #t) (kind 0)))) (hash "0r3nzxs62fhzjm3wk3qpb2kn7jvjh2z03mjl1abdh5s6kk4kfd2y") (features (quote (("std"))))))

(define-public crate-etag-2 (crate (name "etag") (vers "2.0.4") (deps (list (crate-dep (name "str-buf") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (default-features #t) (kind 0)))) (hash "0j67mc7hvwn9ra2idi4bmp89z5bdskwm55agihx3idqrfm7i7iw2") (features (quote (("std")))) (yanked #t)))

(define-public crate-etag-2 (crate (name "etag") (vers "2.0.5") (deps (list (crate-dep (name "str-buf") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (default-features #t) (kind 0)))) (hash "0dnh0ily8nxy91nbf3rhfvxxk3hgzlwmh9gi0q1kg0hyb6dzs2bc") (features (quote (("std"))))))

(define-public crate-etag-2 (crate (name "etag") (vers "2.0.6") (deps (list (crate-dep (name "str-buf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (default-features #t) (kind 0)))) (hash "0a00mn0nnlm5w45hzh3mmvddzylzxq66va735yvqk6q4qm41q3nb") (features (quote (("std"))))))

(define-public crate-etag-3 (crate (name "etag") (vers "3.0.0") (deps (list (crate-dep (name "str-buf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("const_xxh3" "xxh3"))) (default-features #t) (kind 0)))) (hash "13yqr7pgwvxmj01cpakvian9b8kj3fh3vqv861gx66y9is8zijdf") (features (quote (("std"))))))

(define-public crate-etag-4 (crate (name "etag") (vers "4.0.0") (deps (list (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.6") (features (quote ("const_xxh3" "xxh3"))) (default-features #t) (kind 0)))) (hash "0i5zfnl71harw6g15ivnvmpysnbrf6dlx0qbp9nc5pfcl9hhcgab") (features (quote (("std"))))))

(define-public crate-etagere-0.1 (crate (name "etagere") (vers "0.1.0") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "1vicw04bic1whmbw3kwmdh40yd20zhifihkghw6nnwp28fb6fqi6") (features (quote (("serialization" "serde" "euclid/serde"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.0") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gk3a3qgg17xdmphylhkif8h6024gygvsgj2jjzwr82kcp7lghzi") (features (quote (("serialization" "serde" "euclid/serde"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.1") (deps (list (crate-dep (name "euclid") (req ">=0.22.0, <0.23.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">=1.0.0, <2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)))) (hash "0c9a3vix6m2x7zvmfjdvhx718iy5r5d0mfdqcczvdfbi37bsd9rp") (features (quote (("serialization" "serde" "euclid/serde"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.2") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sp6z3573g1fa7mfk5yh2vxdijq65h5xmk4gyw1yxqci7af4gwsp") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.3") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0yzvns4jgz9zip6fpj2p82wpwi0pbja32yg3ssj0829jcwd0r2y0") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.4") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ml3xy4903d69x2kyxmas4vzzr3w1balffn026dx0kwh83jps3aj") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.5") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0b7skbnh6xbqxk15v726vdpc61sa4k8lyrm9152lysrvh6axkc3s") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.6") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wk2amzr0pq6wv3pnqiy8kb134q7i1lxnm14l6s2lsxvsv1nvdjy") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.7") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "128wc2wjfjrsvfq1skf4mgcwr9gv3kmmnf9i5hwpydlg64d1a0b3") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.8") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0i4ngn8gbnp8nmlszg32bm54b4pf6n9h7q42w0c2jdalhxs2zwpw") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.9") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sdk0vfqb2cvn1giyk42zsr4ybayh5a1hj2g5d1sfdd2lfg0pxsv") (features (quote (("serialization" "serde" "euclid/serde") ("checks")))) (yanked #t)))

(define-public crate-etagere-0.2 (crate (name "etagere") (vers "0.2.10") (deps (list (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0kiy6k3683qb126kvrnks048qha1li179w5psq6vsikc3n460s9h") (features (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

