(define-module (crates-io oy -d) #:use-module (crates-io))

(define-public crate-oy-derive-0.1 (crate (name "oy-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yrcv4j10037238rvavl2x4rw5xif4d6r3w2q64m7r1978wp0cgg") (features (quote (("std") ("default" "std"))))))

