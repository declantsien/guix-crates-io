(define-module (crates-io oy as) #:use-module (crates-io))

(define-public crate-oyashio-0.0.4 (crate (name "oyashio") (vers "0.0.4") (deps (list (crate-dep (name "promiser") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "15hdskfdkmc93f4b5v18rwqdk0md95ggr76rd51vrg809zam2vir")))

(define-public crate-oyasumi-0.1 (crate (name "oyasumi") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pv6grg9qhsiqllbncap80xswl6grwh3bvd8pr79mrr7csx3a8hb")))

(define-public crate-oyasumi-0.1 (crate (name "oyasumi") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "signal" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0q9rhdb5khv3bc7w6xxaam26f4gq5wpjnrfcvxji6bvcmjvjkk5v")))

