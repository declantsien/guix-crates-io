(define-module (crates-io r4 _g) #:use-module (crates-io))

(define-public crate-r4_grrs-0.1 (crate (name "r4_grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)))) (hash "18hlayvswy2zs09p3h3vma75lh3ad9wfzhr0hwi1i3vzcy7lm3q0")))

(define-public crate-r4_grrs-0.1 (crate (name "r4_grrs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)))) (hash "01c3c97w272ixwbw8vpiiqpmhagry13bwm8y059rypa695kggck3")))

