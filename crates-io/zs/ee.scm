(define-module (crates-io zs ee) #:use-module (crates-io))

(define-public crate-zsee-0.0.1 (crate (name "zsee") (vers "0.0.1") (deps (list (crate-dep (name "bson") (req "^2.9.0") (default-features #t) (kind 0)) (crate-dep (name "dioxus") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "gloo-timers") (req "^0.3.0") (features (quote ("futures"))) (default-features #t) (kind 0)))) (hash "1bp5gw6q0dqdbajrz1dp569pz1kv4lckzb27s8p9243q6mdm3k8l")))

(define-public crate-zsee-0.0.2 (crate (name "zsee") (vers "0.0.2") (deps (list (crate-dep (name "bson") (req "^2.9.0") (default-features #t) (kind 0)) (crate-dep (name "dioxus") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "gloo-timers") (req "^0.3.0") (features (quote ("futures"))) (default-features #t) (kind 0)))) (hash "1vxfxscgz00cfps6g8mh3xnb8hrf4my7l2rc80yragaqy0gxhl3w")))

