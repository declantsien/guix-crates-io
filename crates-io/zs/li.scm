(define-module (crates-io zs li) #:use-module (crates-io))

(define-public crate-zsling-0.0.1 (crate (name "zsling") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "zigc") (req "^0.0.0-alpha0.2") (default-features #t) (kind 1)))) (hash "0wq7g1gvnb0r7rdvygnnk3m6q405v8ichi7jgs7y2cjb6i9rdj43")))

(define-public crate-zsling-0.1 (crate (name "zsling") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "zigc") (req "^0.0.0-alpha0.2") (default-features #t) (kind 1)))) (hash "0l8y8cwpxx3zm3a2k8d85d6gsv800bkwzzny8az63yrhay7qw4ff")))

(define-public crate-zsling-0.1 (crate (name "zsling") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "zigc") (req "^0.0.3") (default-features #t) (kind 1)))) (hash "1plk4vqc441cbwjq9ajhn2d8r24xdszipspirwyrvd4004k9xdxb")))

(define-public crate-zsling-0.1 (crate (name "zsling") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "zigc") (req "^0.0.3") (default-features #t) (kind 1)))) (hash "133yy5ba6svcy94rfsnx68sy91z00y9kkpx370rqm796gy8zf09s")))

