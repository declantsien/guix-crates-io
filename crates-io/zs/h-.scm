(define-module (crates-io zs h-) #:use-module (crates-io))

(define-public crate-zsh-module-0.1 (crate (name "zsh-module") (vers "0.1.0") (deps (list (crate-dep (name "zsh-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wxb9rn3hdhcqr6nlki76k8rpqqss8chv5d2siizyc7k2wn62kjg")))

(define-public crate-zsh-module-0.1 (crate (name "zsh-module") (vers "0.1.1") (deps (list (crate-dep (name "zsh-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nk8akc40qd1v8znpm1naqi1wz30ibjl8chrkywgx5721sm69n8g")))

(define-public crate-zsh-module-0.2 (crate (name "zsh-module") (vers "0.2.0") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "zsh-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1j4iyksjkjcksw8jbysv3j2mrgibgm45xcvnkhwlv7sls093mrrh") (features (quote (("export_module") ("default" "export_module"))))))

(define-public crate-zsh-module-0.2 (crate (name "zsh-module") (vers "0.2.1") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "zsh-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1j5mqnyd653yaykvsfcf2m92951a201zp821azqr64r18wdjb6w7") (features (quote (("export_module") ("default" "export_module"))))))

(define-public crate-zsh-module-0.2 (crate (name "zsh-module") (vers "0.2.2") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "zsh-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "131rs4l4vw76prpkkkqcg5qrv498ffd28lbxrfkq4b3a92nqsp44") (features (quote (("export_module") ("default" "export_module"))))))

(define-public crate-zsh-module-0.3 (crate (name "zsh-module") (vers "0.3.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "zsh-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xcqysmyh8hdz8z7h67adygcgz9arbpwpwd8i4c170bh6y4a9n66") (features (quote (("export_module") ("default" "export_module"))))))

(define-public crate-zsh-sys-0.1 (crate (name "zsh-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)))) (hash "0x0wil0s41vc0grzhmvvkh5fnl5bwff0h2vpd0v2f8fzf1hsnjmy")))

(define-public crate-zsh-sys-0.1 (crate (name "zsh-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)))) (hash "0sdlbf4r5s0p3mzq7mz39wskgmgbdhscn8dchmy70q7arnf0y9br")))

