(define-module (crates-io zs se) #:use-module (crates-io))

(define-public crate-zssetmgr-0.1 (crate (name "zssetmgr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "readfilez") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1x4lwppgcg3wnmil6cdchp68gc2ryrlyrrnshjzpwanbv8b7cv66") (yanked #t)))

(define-public crate-zssetmgr-0.1 (crate (name "zssetmgr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "readfilez") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1ybna3qf6jqjc1px39z3qjy4x26qcxwjnss95wzfkdl3r09ljg9d") (yanked #t)))

(define-public crate-zssetmgr-0.1 (crate (name "zssetmgr") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "readfilez") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "04vhws5x6hs6dkv2f1hkanxlck17zjr7qbxm309bgqkpvjxcdmif") (yanked #t)))

