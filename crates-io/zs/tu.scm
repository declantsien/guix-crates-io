(define-module (crates-io zs tu) #:use-module (crates-io))

(define-public crate-zstud-sys-0.1 (crate (name "zstud-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "1bbclqrbzn235niggcwd9n96sm8h2qsbida6xwzp2rbhfscfmf81")))

(define-public crate-zstud-sys-0.1 (crate (name "zstud-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.66.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "0knzgyh7q5sxhilsqym09rzp0988hhjlwrnrr3iv466km449qv3k")))

(define-public crate-zstud-sys-0.1 (crate (name "zstud-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.66.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "0dlkna47a88dp9an5y1k4fw4bbjkcfcmawscczb7bjd9a71l81hn")))

(define-public crate-zstud-sys-0.1 (crate (name "zstud-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.66.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "0fklhr3rn5qa39h0p2drrkvkzx5g1pbmmpl8p8g7af20ljb82cz0")))

