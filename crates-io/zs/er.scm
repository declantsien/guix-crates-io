(define-module (crates-io zs er) #:use-module (crates-io))

(define-public crate-zser-0.0.0 (crate (name "zser") (vers "0.0.0") (hash "04zwazszgaz303cf2awn187rqf1vig9rwcnk3gz3bd43kvr0gjxc")))

(define-public crate-zser-0.0.1 (crate (name "zser") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (kind 0)) (crate-dep (name "data-encoding") (req "^2.0.0-rc.1") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1wnlrvyygq431v1wxim95lzqrk2k8ydj2ja29dl86b3229yygwfz") (features (quote (("std") ("default" "std") ("bench"))))))

