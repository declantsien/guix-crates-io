(define-module (crates-io zs tr) #:use-module (crates-io))

(define-public crate-zstr-0.1 (crate (name "zstr") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("proc-macro" "parsing" "printing"))) (kind 0)))) (hash "1rnd3mdxfkwk87l8hgc0fyqa9fk3saxchs4g0wxk87n6hg2m4g4d")))

(define-public crate-zstr-0.1 (crate (name "zstr") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (features (quote ("proc-macro" "parsing" "printing"))) (kind 0)))) (hash "0gj5acsdl042zr1sifpdc0h9pz21gypd4mxxv153mjmlaa5z6cdz")))

(define-public crate-zstring-0.0.1 (crate (name "zstring") (vers "0.0.1") (hash "01a3lmpqkdya2d99cljbj3j1g0sky38mf9as0j4djg321dbj3fxb") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.0.2 (crate (name "zstring") (vers "0.0.2") (hash "12zv650zda8hgqr82502wvf3wm31cbzcpsxzvbg0d05bzcxz3rq2") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.0.3 (crate (name "zstring") (vers "0.0.3") (hash "027wbya7xvxl291630db805db675c8xkw60l30x6qxnxk3mq31fq") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1 (crate (name "zstring") (vers "0.1.0") (hash "19ay4fkbhjwzgwn2kljizbl3jjd6kpjpccyn39jgjf67ihmalsp0") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1 (crate (name "zstring") (vers "0.1.1") (hash "0pja4w8i8blg2384y33qv1z3ws5q6d2imn38rkai35w6yfc8yaf9") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1 (crate (name "zstring") (vers "0.1.2") (hash "1jcnbg0pf0yjjs4yfwzj6a8d60xy7gbr5sgl3bb19i05a6kiiynk") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1 (crate (name "zstring") (vers "0.1.3") (hash "1aw996d1w99xm1bmnb5jfs789h8ds7dzvvvqg3sr8rjv8qhrsdbk") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2 (crate (name "zstring") (vers "0.2.0") (deps (list (crate-dep (name "ptr_iter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jfk1qbjh5mjpqzqqwxrxgmrl6rc6lb6swqgcqn2kp4fw87r3mpv") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2 (crate (name "zstring") (vers "0.2.1") (deps (list (crate-dep (name "ptr_iter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1y0spvcmlqw56yag880ldhc51qhcdlyfhahby77rvnk86bsjng4j") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2 (crate (name "zstring") (vers "0.2.2") (deps (list (crate-dep (name "ptr_iter") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 2)))) (hash "1kn7phd9h1kh9mj9khn0mh360sxgmbawv8dizr0x1hcmjd1c8gyd") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2 (crate (name "zstring") (vers "0.2.3") (deps (list (crate-dep (name "ptr_iter") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 2)))) (hash "1771lwdic15ixkdmkvirbf1gc3sjrhag5jfpmqcrryp2r3jw9l24") (features (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2 (crate (name "zstring") (vers "0.2.4") (deps (list (crate-dep (name "ptr_iter") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 2)))) (hash "0s5w4mg357wkdxzk6ff3qq7r97yw11qm52n98ncv5kizg1gywr1d") (features (quote (("default") ("alloc"))))))

