(define-module (crates-io gz -m) #:use-module (crates-io))

(define-public crate-gz-msgs-0.1 (crate (name "gz-msgs") (vers "0.1.0") (deps (list (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 1)))) (hash "13k5wg0l3hhvd94kdc5d0p5l3s30c4v17w6w66kg30rx9mli2qgv") (yanked #t) (rust-version "1.60.0")))

(define-public crate-gz-msgs-0.2 (crate (name "gz-msgs") (vers "0.2.0") (deps (list (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 1)))) (hash "1y4z7r1gxavv4wcm0k9yh7kivd5a5vkkvzdfgdhs37m4kw7pacgv") (yanked #t) (rust-version "1.60.0")))

(define-public crate-gz-msgs-0.2 (crate (name "gz-msgs") (vers "0.2.1") (deps (list (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 1)))) (hash "08bfv8njhslcb6c3s8gcfalcq379s3g4flpv9x9sqzgl36979ly4") (yanked #t) (rust-version "1.60.0")))

(define-public crate-gz-msgs-0.3 (crate (name "gz-msgs") (vers "0.3.0") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 1)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 1)))) (hash "0xcgrv56dp1d3d5899pg7h4f75n2gji8xd84kdrj3mfm2dr0yyfi") (rust-version "1.60.0")))

(define-public crate-gz-msgs-0.3 (crate (name "gz-msgs") (vers "0.3.1") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 1)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 1)))) (hash "09z5y2xwm7lhr8kl94vxw7x3hf0sq5yq93i3dnfl23lwhz9py1d3") (rust-version "1.60.0")))

(define-public crate-gz-msgs-0.4 (crate (name "gz-msgs") (vers "0.4.0") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 1)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 1)))) (hash "1cblwkl72jx6g2995z5rym37w5fckfp8ll38d43mnzq1i81nzzhk") (features (quote (("ignition")))) (rust-version "1.60.0")))

(define-public crate-gz-msgs-0.6 (crate (name "gz-msgs") (vers "0.6.0") (deps (list (crate-dep (name "gz-msgs10") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gz-msgs8") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gz-msgs9") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "06agy9np59x7mcyr9v2vsklxmz1imqci90139vph7dsdxs55caw9") (features (quote (("harmonic" "gz-msgs10") ("garden" "gz-msgs9") ("fortress" "gz-msgs8") ("default" "gz-msgs9")))) (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs-0.7 (crate (name "gz-msgs") (vers "0.7.0") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.7.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "0nllk3a5xqpq2lmasj2n8x68sc34g0mxdni1yv04lw3z0zzqv569") (features (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (rust-version "1.65.0")))

(define-public crate-gz-msgs-0.7 (crate (name "gz-msgs") (vers "0.7.1") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.7.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "1x4a2p2kbnrjr13fp000qnfyk0j3alxsh9nfln4fyr6qqv5a51b7") (features (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (rust-version "1.65.0")))

(define-public crate-gz-msgs-0.7 (crate (name "gz-msgs") (vers "0.7.2") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.7.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "0h6h7pfanzw69ghp1xzkl84yc59br1c6sav88ywhngczll28v01h") (features (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (rust-version "1.65.0")))

(define-public crate-gz-msgs-0.7 (crate (name "gz-msgs") (vers "0.7.3") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.7.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "1s2brz6a297s3kz165rz99117yg0xchzyf06314dl4l8y7g160qb") (features (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (rust-version "1.65.0")))

(define-public crate-gz-msgs-0.7 (crate (name "gz-msgs") (vers "0.7.4") (deps (list (crate-dep (name "gz-msgs-build") (req "=0.7.4") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "=0.7.4") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "1ckj922xx0z2xp43hv2z5yig2dandzkdkfcc2sy2apjk3d5bnp11") (features (quote (("harmonic") ("generate" "gz-msgs-build") ("garden") ("fortress")))) (rust-version "1.65.0")))

(define-public crate-gz-msgs-build-0.1 (crate (name "gz-msgs-build") (vers "0.1.0") (deps (list (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 0)))) (hash "0wnjqw7fjzl7xw80firw0ci9rhxmwxml7fnwwy110jxxcrxh64rv") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs-build-0.2 (crate (name "gz-msgs-build") (vers "0.2.0") (deps (list (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "^3") (default-features #t) (kind 0)))) (hash "0nvk3y2d33bl6yjk735i802dxl64xwjkzkayjmjjg3sykhcr9nhq") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs-build-0.7 (crate (name "gz-msgs-build") (vers "0.7.0") (deps (list (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "0wl1rvp0ngkqn5xh3d7z24g7wpl7y2daasd4al6vfngjdmbz41iy") (rust-version "1.65.0")))

(define-public crate-gz-msgs-build-0.7 (crate (name "gz-msgs-build") (vers "0.7.1") (deps (list (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "1r8s7j0w83zw7ya91rzmrhqn4iwk21527mhh2ck6q163acmn1nwh") (rust-version "1.65.0")))

(define-public crate-gz-msgs-build-0.7 (crate (name "gz-msgs-build") (vers "0.7.2") (deps (list (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "0l9mibz6nx224nmkz9bsm3a6n4cp0h69h38179vcdpf7nsjlkanw") (rust-version "1.65.0")))

(define-public crate-gz-msgs-build-0.7 (crate (name "gz-msgs-build") (vers "0.7.3") (deps (list (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "13b34svhy583bgfb0c825asi88ycfcdypk8fi6877layl25z8vk9") (rust-version "1.65.0")))

(define-public crate-gz-msgs-build-0.7 (crate (name "gz-msgs-build") (vers "0.7.4") (deps (list (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "01ifk58ygpdzaq0iap8xyr50mi9w22zcjvz42l85pwgspy3k9j93") (rust-version "1.65.0")))

(define-public crate-gz-msgs-common-0.1 (crate (name "gz-msgs-common") (vers "0.1.0") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)))) (hash "1nfk2xlbfn54zqdg4kzgdpbc6kwid70rhs1by96qmk5z7jfmm2wd") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs-common-0.7 (crate (name "gz-msgs-common") (vers "0.7.0") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "0k99xzmjfwgkx0jck8g8c6ixhhg9v3mrdh34q8s9pwzfnwshavz4") (rust-version "1.65.0")))

(define-public crate-gz-msgs-common-0.7 (crate (name "gz-msgs-common") (vers "0.7.1") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "123zhm5byqlhbk37sp38k4b85nxapm45qn87l5ks38vp8s05kyh4") (rust-version "1.65.0")))

(define-public crate-gz-msgs-common-0.7 (crate (name "gz-msgs-common") (vers "0.7.2") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "1bd3l094j96nisl9jafjdhkc5z9rfrj65zfsmphm8wzf3n01l2vm") (rust-version "1.65.0")))

(define-public crate-gz-msgs-common-0.7 (crate (name "gz-msgs-common") (vers "0.7.3") (deps (list (crate-dep (name "gz-msgs-derive") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "02gfpwvdrqccf4748989i8q4mw2dlgd4d9x58hw25swgq9ybbqmd") (rust-version "1.65.0")))

(define-public crate-gz-msgs-common-0.7 (crate (name "gz-msgs-common") (vers "0.7.4") (deps (list (crate-dep (name "gz-msgs-derive") (req "=0.7.4") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "=3.3.0") (default-features #t) (kind 0)))) (hash "0gmlxqnm8k3zkygf3m9xn79l12mibr3jlyw2zk8rm909p9fpxy67") (rust-version "1.65.0")))

(define-public crate-gz-msgs-derive-0.1 (crate (name "gz-msgs-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "03wzwq4cgrz6g18gyi0izfwr6ckj8jqjbqkj804096n5y4gkwljg") (rust-version "1.60.0")))

(define-public crate-gz-msgs-derive-0.2 (crate (name "gz-msgs-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1nz30dp433w9n54vrxw1i8430xs9d8a6c1nh3g5c09gbnlpgi7x4") (rust-version "1.60.0")))

(define-public crate-gz-msgs-derive-0.3 (crate (name "gz-msgs-derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1bdhwlzlynd0bw47rjdycfrg9sfbz9bpxdziniw956z4w6w2xrkr") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs-derive-0.4 (crate (name "gz-msgs-derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "05s47nwswm6p25mx1pqmihjs3g31jrfkdjljyma8n9wv1x7g2ihg") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs-derive-0.7 (crate (name "gz-msgs-derive") (vers "0.7.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "03r9xs90mjcrbqqy51nn7z6h8h167sn21iy38mc49d6vdircf3gp") (rust-version "1.65.0")))

(define-public crate-gz-msgs-derive-0.7 (crate (name "gz-msgs-derive") (vers "0.7.1") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "0mjkndjjmcg72qy0vcdknrrn1yvj7bd5k93l9q0f6w61m51fjjh4") (rust-version "1.65.0")))

(define-public crate-gz-msgs-derive-0.7 (crate (name "gz-msgs-derive") (vers "0.7.2") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "188p4zhylx8x8617rhsabafjgji3d3nb6a0w55z94mqbmr1qambl") (rust-version "1.65.0")))

(define-public crate-gz-msgs-derive-0.7 (crate (name "gz-msgs-derive") (vers "0.7.3") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "1fza6scwgxlc20n9cb357k3gh9jrmmrfkgazk49j539371xvwvy3") (rust-version "1.65.0")))

(define-public crate-gz-msgs-derive-0.7 (crate (name "gz-msgs-derive") (vers "0.7.4") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "0g8nn0dxc6zy5sqjd5y9zkz3wsmkd37lmns303l2n0l3jz72712m") (rust-version "1.65.0")))

(define-public crate-gz-msgs10-0.5 (crate (name "gz-msgs10") (vers "0.5.0") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)))) (hash "015c3ihvvgngsmmjfd1jzqxbd8fxksg604j06qwhnf2bknb3jcdh") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs10-0.6 (crate (name "gz-msgs10") (vers "0.6.0") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.2.0") (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)))) (hash "1gd4lwq8sjcbj0fxh6zi3wd5fij23cc6aw2x0zdx029p5mss9i6r") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs10-0.0.0 (crate (name "gz-msgs10") (vers "0.0.0") (hash "01549qbw4ghlw1lcq483cd61kfq380znkpjq8cnmzcv54nsq0i2g") (yanked #t)))

(define-public crate-gz-msgs8-0.5 (crate (name "gz-msgs8") (vers "0.5.0") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)))) (hash "0lp3dxdqbw2ydd4iy43q8j8x620gkzgpxn5mmq4f53cvi9kn37ya") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs8-0.6 (crate (name "gz-msgs8") (vers "0.6.0") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.2.0") (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)))) (hash "0w961319r2kk6yhbjca23kj0lkr7iqkni9hvgdz1s1q0vb103y2n") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs8-0.0.0 (crate (name "gz-msgs8") (vers "0.0.0") (hash "02va3skbw96q58dkq3703s1zdlrljh6c5afdgq670749vafma8im") (yanked #t)))

(define-public crate-gz-msgs9-0.5 (crate (name "gz-msgs9") (vers "0.5.0") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)))) (hash "170r9id2kpb0qgambbd1hm9xf763na34d7pvinms6wz12f49h7nw") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs9-0.6 (crate (name "gz-msgs9") (vers "0.6.0") (deps (list (crate-dep (name "gz-msgs-build") (req "^0.2.0") (default-features #t) (kind 1)) (crate-dep (name "gz-msgs-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^3") (default-features #t) (kind 0)))) (hash "0r6cani2fc1izzdaa9795w0q8l5x0i40sq2d321yvdfirr8yyi8q") (yanked #t) (rust-version "1.64.0")))

(define-public crate-gz-msgs9-0.0.0 (crate (name "gz-msgs9") (vers "0.0.0") (hash "1v6bq19jszqbh7z139a4bn5y70r6k4xr2rk6yl5dls4rl2fwmxq3") (yanked #t)))

