(define-module (crates-io gz ip) #:use-module (crates-io))

(define-public crate-gzip-0.1 (crate (name "gzip") (vers "0.1.1") (hash "0c8da71ygrws3l6zr9fakrkpzgnzqj8slhhycb90wjyxvn2jzilm")))

(define-public crate-gzip-0.1 (crate (name "gzip") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "deflate") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "enumflags2") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1c3dsh1cnlvfph1wawznrac8ap8pwj971gspvc78vz3ird24azs1")))

(define-public crate-gzip-cmp-0.1 (crate (name "gzip-cmp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "08l518r1mkrpmgvnaw9cwkfs59lc7p30chxi33sl54zpaha5p1yw")))

(define-public crate-gzip-header-0.1 (crate (name "gzip-header") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "129gkww02yssla5phagszj0xfjn1lv79gxh9r3ar909bi74bl5fn")))

(define-public crate-gzip-header-0.1 (crate (name "gzip-header") (vers "0.1.1") (deps (list (crate-dep (name "crc") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07jqc263zc0c128i6zdd6hh6i6sxqzgriaiq01c8b4lbq70qsh6k")))

(define-public crate-gzip-header-0.1 (crate (name "gzip-header") (vers "0.1.2") (deps (list (crate-dep (name "crc") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0h6dbv6g9rnfp7njgbpw4hhwppxr56y6f92v6m1564pfr7hwz7qa")))

(define-public crate-gzip-header-0.2 (crate (name "gzip-header") (vers "0.2.0") (deps (list (crate-dep (name "crc") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "11j5i17xkq6ad16bz5m4hwl7gxpj4wgwzwn6ns5yk7wh9995k7ig")))

(define-public crate-gzip-header-0.3 (crate (name "gzip-header") (vers "0.3.0") (deps (list (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0fg6vm8sgsm69szwqyz7abfbyziv6pv0jkcailimlamvsfrzwc81")))

(define-public crate-gzip-header-1 (crate (name "gzip-header") (vers "1.0.0") (deps (list (crate-dep (name "crc32fast") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "18lm2y96mahkmcd76pzyam2sl3v6lsl9mn8ajri9l0p6j9xm5k4m")))

(define-public crate-gzip_cli-0.1 (crate (name "gzip_cli") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "gzip") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1yhln5nnq64q5ddgrqh38q0fxwqa5d5vxqlhmax3x8iv8gn2qg2n")))

