(define-module (crates-io gz bb) #:use-module (crates-io))

(define-public crate-gzbbinarydoc-0.1 (crate (name "gzbbinarydoc") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0qicbynnjycbxd95d5il2j8hadbfdslwb51nl8jmzfaaryssllal")))

