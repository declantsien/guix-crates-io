(define-module (crates-io rd k-) #:use-module (crates-io))

(define-public crate-rdk-rs-0.1 (crate (name "rdk-rs") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rdk-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "09j1jm97q2y00nf7jhnvd91g9zcvg0p7ina88rifr2r0dm36djrc")))

(define-public crate-rdk-sys-0.1 (crate (name "rdk-sys") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 1)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 1)))) (hash "04x1srdp9xhz417mx3af5hw64xk943g951vb9709m7blqqbvai5v")))

(define-public crate-rdk-sys-0.1 (crate (name "rdk-sys") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 1)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 1)))) (hash "0qwyn6a7gpyqh42bkfxb1vm191h2vbwvy8f2a193hcqzkvdap7wc") (features (quote (("conda"))))))

