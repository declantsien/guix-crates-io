(define-module (crates-io rd ir) #:use-module (crates-io))

(define-public crate-rdir-encoding-0.1 (crate (name "rdir-encoding") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lnabwywwhl5i3195x8qdcw1f8c9h89p3x2cl7yyn4zr0ld7j7m7")))

