(define-module (crates-io rd gr) #:use-module (crates-io))

(define-public crate-rdgrep-0.1 (crate (name "rdgrep") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zz3x36v6a7ardzbymqhzx9jh3xg0wr0g781pqvhi5p9vbrmpgsx")))

