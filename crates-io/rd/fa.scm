(define-module (crates-io rd fa) #:use-module (crates-io))

(define-public crate-rdfa-wasm-0.1 (crate (name "rdfa-wasm") (vers "0.1.1") (deps (list (crate-dep (name "wasm-pack") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1hpc023xfjyplk7ymi7vn0f4q496q0p8j06kkyhjl4mprwfcv76a") (rust-version "1.73")))

(define-public crate-rdfa-wasm-0.1 (crate (name "rdfa-wasm") (vers "0.1.2") (deps (list (crate-dep (name "graph-rdfa-processor") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.88") (default-features #t) (kind 0)))) (hash "11cv5vrpggbr4492js0ql9gxc7r3f40w69kdamp40g0wjml7808r") (rust-version "1.73")))

(define-public crate-rdfa-wasm-0.1 (crate (name "rdfa-wasm") (vers "0.1.3") (deps (list (crate-dep (name "getrandom") (req "^0.2.10") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "graph-rdfa-processor") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.90") (default-features #t) (kind 0)))) (hash "0pcafi8pk5zf4qrx04qhkl3ppik2ydc67n0fpsy06r4a2g14brjd") (rust-version "1.73")))

