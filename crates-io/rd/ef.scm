(define-module (crates-io rd ef) #:use-module (crates-io))

(define-public crate-rdefer-1 (crate (name "rdefer") (vers "1.0.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "19hfq5vxsni2qjackylxyi0ngp8ylwi6pn7rv7dw1znhnqz2p187") (features (quote (("default") ("async" "tokio"))))))

