(define-module (crates-io rd m3) #:use-module (crates-io))

(define-public crate-rdm3600-0.1 (crate (name "rdm3600") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "176jxqfjvhkdki74s65mzibja2znj6fs0905ww753kn6al4svgja")))

