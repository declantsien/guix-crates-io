(define-module (crates-io rd c-) #:use-module (crates-io))

(define-public crate-rdc-macros-0.1 (crate (name "rdc-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0j0xlaawc214xzk7l3n5vw2xyana2azzyvcdjdnpbc3zqllssy8g")))

