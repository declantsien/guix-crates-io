(define-module (crates-io rd et) #:use-module (crates-io))

(define-public crate-rdetect-0.1 (crate (name "rdetect") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "0ls9gsk96fidnhmk2yn7xk985d9b61ln71l9043mlbmj6fwsfmx8")))

(define-public crate-rdetect-0.1 (crate (name "rdetect") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "023hfsxdp3p0nwvzsm1g4wvrx6k52fk0ly6r0qh3d6b9086zzn0h")))

(define-public crate-rdetect-0.1 (crate (name "rdetect") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "1msgf71s4rpk4q6ygi5sgydanl6q58v5hkmfzyp5kq31yws3hmqc")))

(define-public crate-rdetect-0.1 (crate (name "rdetect") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "1nv3i1nrhhli5z4m0gy49r5d8n0sgw0ccbkdr5iglq26dv5xkmy0")))

(define-public crate-rdetect-0.2 (crate (name "rdetect") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "1vwdk5gw888lqjhd67zdcnnlf4xdxrn22633m32wrxl8l147jdk8")))

(define-public crate-rdetect-0.2 (crate (name "rdetect") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "1iqskl9ggsnzk7x48bvadbcaa6jh2k5x22nxvxrd75yy1mlwsb8m")))

