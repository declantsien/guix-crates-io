(define-module (crates-io rd og) #:use-module (crates-io))

(define-public crate-rdog-0.1 (crate (name "rdog") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1xrah88bsdsrlpkzg3xim1xpvdryzf1a86p47zrvnzlbjq2f2rcy")))

