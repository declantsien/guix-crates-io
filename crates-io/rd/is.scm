(define-module (crates-io rd is) #:use-module (crates-io))

(define-public crate-rdis-0.0.0 (crate (name "rdis") (vers "0.0.0") (hash "1ly7gynvhlcjycx8c471bhpdjpbg73hnfx9kd7smdg0c7gq2xi70")))

(define-public crate-rdisk-0.1 (crate (name "rdisk") (vers "0.1.0") (hash "0zpf8v63k5sq79p96sl3b9w2il0a0h856gfjpx71x3v18c8xkvqw")))

(define-public crate-rdisk_shared-0.1 (crate (name "rdisk_shared") (vers "0.1.0") (hash "1gvqs9c4l8gpzjpx6gd61psfca38z1dyh97p1v3kdy3qb8b5j37c") (features (quote (("std") ("default" "std"))))))

(define-public crate-rdispatcher-0.0.3 (crate (name "rdispatcher") (vers "0.0.3") (hash "0b6760q9shhakn414ah4ghgl6r1iz20jmpigs40spxskbfhgwp5w")))

(define-public crate-rdispatcher-0.0.4 (crate (name "rdispatcher") (vers "0.0.4") (hash "0vgr5qn78srj2rfh5hyp41lrj56a33qdgq65v10d5lh4y7fvr2s3")))

(define-public crate-rdispatcher-0.0.5 (crate (name "rdispatcher") (vers "0.0.5") (hash "0q4znbgqw1023yd79c89bxi9pf8n1gplp7lxn2n4m4b50k0rfdv8")))

(define-public crate-rdispatcher-0.1 (crate (name "rdispatcher") (vers "0.1.0") (hash "0i30k5v794vz6p2hmmygvd1wxrf6lppx6myv4mz03vskb1vci6w1")))

(define-public crate-rdist-0.0.0 (crate (name "rdist") (vers "0.0.0") (hash "02ml26i9wq11g1y246cyv3k6kfx69jfj4lpnh338i35khchwbd4p")))

