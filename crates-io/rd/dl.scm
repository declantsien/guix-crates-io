(define-module (crates-io rd dl) #:use-module (crates-io))

(define-public crate-rddl-0.1 (crate (name "rddl") (vers "0.1.0") (deps (list (crate-dep (name "pom") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1l0nil6s2plgvdxg8ybqxcq0i1d9k3fp596vd33qhrjvq73fvqlk")))

(define-public crate-rddl-0.1 (crate (name "rddl") (vers "0.1.1") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "09gph8n2a43idqjp9yh8nixcxzv8lbjzz9lnrsfdy9vipfh1scib")))

