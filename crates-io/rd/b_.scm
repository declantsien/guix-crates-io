(define-module (crates-io rd b_) #:use-module (crates-io))

(define-public crate-rdb_atelier-0.0.0 (crate (name "rdb_atelier") (vers "0.0.0") (hash "04n1pz259gdr739mkbc5wv9796clidyvk5v2j42myb6iwyclhi1k")))

(define-public crate-rdb_core-0.0.0 (crate (name "rdb_core") (vers "0.0.0") (hash "1pn3ammcw5929lafdsvxsvv30gyy8pf9kj0ffhzs64zpbnp4qlzj")))

(define-public crate-rdb_hasher-0.0.0 (crate (name "rdb_hasher") (vers "0.0.0") (hash "1ygdsjyza8ix9zag5p4fcv3icfq08k7f0a166x8rjddk1ww67as2")))

(define-public crate-rdb_tool-0.1 (crate (name "rdb_tool") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "012vy62v9yigd9vxw0pbbmgypdq4fikdz6z9iyyq8px2n4klb4l0") (yanked #t)))

(define-public crate-rdb_xortool-0.0.0 (crate (name "rdb_xortool") (vers "0.0.0") (hash "1zkx1bq2qhjbdbhrwl8hb3s69ykpzyccnhwpciv6h263hzbw5n8m")))

