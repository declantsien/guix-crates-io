(define-module (crates-io rd if) #:use-module (crates-io))

(define-public crate-rdiff-0.1 (crate (name "rdiff") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^2.6.1") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gp4mqcm5p6v4aywmq6ph0qc92q4vvajfy0x69fzpkf5gsraldrk")))

(define-public crate-rdiff-0.1 (crate (name "rdiff") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^2.6.1") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "04cqdpyfzfqby08px32nrsyrnlg6pis52fmn94dxldlm3sycq6f4")))

(define-public crate-rdiff-0.1 (crate (name "rdiff") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^2.6.1") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w8gfi3zq789nra8hkl8sikbn6gxq0cf5474f1dwn47k9x4r1j1h")))

