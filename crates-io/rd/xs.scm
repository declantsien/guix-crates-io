(define-module (crates-io rd xs) #:use-module (crates-io))

(define-public crate-rdxsort-0.1 (crate (name "rdxsort") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "08bpvmqcw54gffahr9rhsgwdhca0gzmhyrjjqs89ysgkfb2bxbl8")))

(define-public crate-rdxsort-0.2 (crate (name "rdxsort") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0xam91zdqxj99nvkrm66yp0ah8k3yfh5j6dvqkhva77awb52a1br") (features (quote (("unstable"))))))

(define-public crate-rdxsort-0.3 (crate (name "rdxsort") (vers "0.3.0") (deps (list (crate-dep (name "quicksort") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0sf1a0vk8b32fscn0637rb0q4gzw55j98w6g5flbg6gbf1gkm5km") (features (quote (("unstable"))))))

