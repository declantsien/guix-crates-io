(define-module (crates-io rd ra) #:use-module (crates-io))

(define-public crate-rdrand-0.0.1 (crate (name "rdrand") (vers "0.0.1") (hash "06dvga06xfas2n3d2vkkc1f7nig0iqq9w8mpf6cb37jb7d6103y4")))

(define-public crate-rdrand-0.0.2 (crate (name "rdrand") (vers "0.0.2") (hash "14ik6sk1lnxsyl68pnqnh8qj3vycsbvczq5q977dmvwih389a7h4")))

(define-public crate-rdrand-0.0.3 (crate (name "rdrand") (vers "0.0.3") (hash "0fdchw70rc4gkr3r4a1zv1ywaxcpg7s8ymj41ybhhk8m7smbq788")))

(define-public crate-rdrand-0.0.4 (crate (name "rdrand") (vers "0.0.4") (hash "1m4faw7bn6aylxprz0qfc77sk8kmqagfb5d7f4pip85zx67qzk18")))

(define-public crate-rdrand-0.0.5 (crate (name "rdrand") (vers "0.0.5") (hash "11i5dhp6fnjvhk4pijmi1pfqpvklszpp63xfkzmbf6wjh56jzxf4")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "0.1.*") (default-features #t) (kind 0)))) (hash "130h2wmlxc01147iv6zw302y70bf2727vj081h3i1mhqsv70xkv1")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ygi5mwqw2nzdg82n7hg8j0xks37s5bbc4x35jkw4sx4l2nn19yx")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0095dzghb6n0aql0av6idn7769myx62c2gvnvdy3wj09r3amaqwv")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0x15lvmmn800vgdim02gm3v2lk7l9g29zmrxafr7jc145mh2zrc0")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "193scxwfh4f4g39cj18x8lwvzp6jh8xxxxj1aph8a99cb8psw8vv")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "10ydhiczivj8xk26pgajyb8h33c823dqb3xi6jd53p5bl6dbkjpx")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0a1gskcw270bb00b8scj70hmk2qxvbzkx15psp32xdgj84cpyq0m")))

(define-public crate-rdrand-0.1 (crate (name "rdrand") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "054cd80mpg6m07ya0d9wz85mcl2m5pgr76v5nqj4d43nw2q8mayp")))

(define-public crate-rdrand-0.2 (crate (name "rdrand") (vers "0.2.0") (deps (list (crate-dep (name "rand_core") (req "^0.2") (kind 0)))) (hash "1mzy7c0vwxfixsj74hc679cscfh4xfnn2dfkb3ll205g6avd4bcv")))

(define-public crate-rdrand-0.3 (crate (name "rdrand") (vers "0.3.0") (deps (list (crate-dep (name "rand_core") (req "^0.3") (kind 0)))) (hash "14yzlr8l1r93100xlzpz7ny607x0dkvpd0sp4kywx02w4vq0xrqn")))

(define-public crate-rdrand-0.4 (crate (name "rdrand") (vers "0.4.0") (deps (list (crate-dep (name "rand_core") (req "^0.3") (kind 0)))) (hash "1cjq0kwx1bk7jx3kzyciiish5gqsj7620dm43dc52sr8fzmm9037") (features (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.5 (crate (name "rdrand") (vers "0.5.0") (deps (list (crate-dep (name "rand_core") (req "^0.4") (kind 0)))) (hash "0biqbnpgkg3ji9sad04nzhgzwihzr2lfjp1wmwar6117p3s4aac3") (features (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.5 (crate (name "rdrand") (vers "0.5.1") (deps (list (crate-dep (name "rand_core") (req "^0.4") (kind 0)))) (hash "02dsyibvl1iygkmljr0ld9vjyp525q4mjy5j9yazrrkyvhvw7gvp") (features (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.6 (crate (name "rdrand") (vers "0.6.0") (deps (list (crate-dep (name "rand_core") (req "^0.4") (kind 0)))) (hash "1ir1d2xwknd9r2dpygpphdc7h7wmfy23kjivbp0n0psinm1gh52h") (features (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.5 (crate (name "rdrand") (vers "0.5.2") (deps (list (crate-dep (name "rand_core") (req "^0.4") (kind 0)))) (hash "192j1viv70mpz8x4k2kh6j58839vyvx4k8nsrkz4sg6knj2p1pjp") (features (quote (("std") ("default" "std"))))))

(define-public crate-rdrand-0.7 (crate (name "rdrand") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5.1") (kind 0)))) (hash "0q5k8wby76vhcy183sjddj32hh7pabkyd5fvc0d300lpvp6hc6gx") (features (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8 (crate (name "rdrand") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (kind 0)))) (hash "057jgl6bzzrcp1yr7xdx78v1v2nn4ma9p9p8vzn629pki8w55pha") (features (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8 (crate (name "rdrand") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (kind 0)))) (hash "0xm5260sih9bp3lil22kymy44r5029xw56akh3jy8h6b0p5xbq64") (features (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8 (crate (name "rdrand") (vers "0.8.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (kind 0)))) (hash "1fv6w7lzxfrfmw16dymn27s454qkqr1q99zp3ymc2m852r1bccz2") (features (quote (("std" "rand_core/std") ("default" "std"))))))

(define-public crate-rdrand-0.8 (crate (name "rdrand") (vers "0.8.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (kind 0)))) (hash "0m9nacxrckfh5dpmn5wqrd63llafy382xg5d8znqxb0jhqi9a8fr") (features (quote (("std" "rand_core/std") ("default" "std"))))))

