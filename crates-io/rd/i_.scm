(define-module (crates-io rd i_) #:use-module (crates-io))

(define-public crate-rdi_macros-0.1 (crate (name "rdi_macros") (vers "0.1.0") (deps (list (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("parsing" "full"))) (default-features #t) (kind 0)))) (hash "1vdikjkpf9bsrbkia9hwilj6qxpc9h9q1px785zd8f2przy6ra9r")))

