(define-module (crates-io rd ot) #:use-module (crates-io))

(define-public crate-rdot-0.1 (crate (name "rdot") (vers "0.1.0") (hash "1dgaick6zllc4hp95789ahhgvv0lwalg4vvizlil521l9lia46ia") (yanked #t)))

(define-public crate-rdotenv-0.1 (crate (name "rdotenv") (vers "0.1.0") (hash "17hrx5s5b3a9gy9h3r71v7awyh2g14a6kvvz083s33gylm1xc6nm")))

(define-public crate-rdotenv-0.1 (crate (name "rdotenv") (vers "0.1.1") (hash "1ajl4h8fy07dsccib6iagi6xasq3cgq49jsw0516cmi6a4rzwjir")))

(define-public crate-rdotenv-0.1 (crate (name "rdotenv") (vers "0.1.2") (hash "18fk6i3krw41zhn9y0im8cf0y831rn1chvyaf40s1yrdcsv68k8y")))

(define-public crate-rdotenv-0.1 (crate (name "rdotenv") (vers "0.1.3") (hash "1b8x2c5i47vg434p0jnzl5nh09s1kyfkzcrycc715vsj14llf1ss")))

(define-public crate-rdotenv-0.1 (crate (name "rdotenv") (vers "0.1.4") (hash "11pbdmkh2cx57nxs3sbrz1z31xxljyx4k3hwyjppbvfidigsq41c")))

