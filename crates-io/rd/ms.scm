(define-module (crates-io rd ms) #:use-module (crates-io))

(define-public crate-rdms-0.0.1 (crate (name "rdms") (vers "0.0.1") (deps (list (crate-dep (name "jsondata") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (kind 0)))) (hash "0nh2p7qywhd2sa7cslifa84ra0qwa60iq9v5p8x7dn28wwcl0yss")))

