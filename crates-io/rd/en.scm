(define-module (crates-io rd en) #:use-module (crates-io))

(define-public crate-rdenticon-0.1 (crate (name "rdenticon") (vers "0.1.0") (deps (list (crate-dep (name "ril") (req "^0.9") (kind 0)) (crate-dep (name "sha1_smol") (req "^1") (default-features #t) (kind 0)))) (hash "1zcbzna71aavwd6vn1w24r1sgsikf55x9vwa2qxpsp4viby9z5cv") (features (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1 (crate (name "rdenticon") (vers "0.1.1") (deps (list (crate-dep (name "ril") (req ">=0.9, <0.11") (kind 0)) (crate-dep (name "sha1_smol") (req "^1") (default-features #t) (kind 0)))) (hash "1p2hqbv9bsk3fj7iym1zr3ci7i58aicgs0f3s15h5d4p55k2q8w7") (features (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1 (crate (name "rdenticon") (vers "0.1.2") (deps (list (crate-dep (name "ril") (req ">=0.9, <0.11") (kind 0)) (crate-dep (name "sha1_smol") (req "^1") (default-features #t) (kind 0)))) (hash "0dzvjyr0wih4lsm5b8smlgq1ws9xn6xag8rs2y10q70nzqsccwzn") (features (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1 (crate (name "rdenticon") (vers "0.1.3") (deps (list (crate-dep (name "ril") (req ">=0.9, <0.11") (kind 0)) (crate-dep (name "sha1_smol") (req "^1") (default-features #t) (kind 0)))) (hash "07dv5jc1ckf89l0jrf67x88l28rnd93yn8nbsajdxkwif1ihkd8r") (features (quote (("default" "ril/png"))))))

(define-public crate-rdenticon-0.1 (crate (name "rdenticon") (vers "0.1.4") (deps (list (crate-dep (name "ril") (req ">=0.9, <0.11") (kind 0)) (crate-dep (name "sha1_smol") (req "^1") (default-features #t) (kind 0)))) (hash "0vgbmdiv00qfgnrq2gxch6hlxlqrfvb18hbk0c621rdwb6nadmb3") (features (quote (("default" "ril/png"))))))

