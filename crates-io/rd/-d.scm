(define-module (crates-io rd -d) #:use-module (crates-io))

(define-public crate-rd-derive-0.1 (crate (name "rd-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0l0k0f8pmd07fw8dd2q83gbbi9v3pkrwkd64x7zw9ipvfyx6cy9q")))

(define-public crate-rd-dir-0.1 (crate (name "rd-dir") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0zwn3nnavawwpwvdl6jhkw1qklv9qfnih7sccmw678hr84x045j5")))

