(define-module (crates-io rd b-) #:use-module (crates-io))

(define-public crate-rdb-conn-0.0.1 (crate (name "rdb-conn") (vers "0.0.1") (hash "16nrgi4j1qlljqgqsimzap54z7zpvpkavrbb79a6dhallc058a46")))

(define-public crate-rdb-pagination-0.1 (crate (name "rdb-pagination") (vers "0.1.1") (deps (list (crate-dep (name "educe") (req "^0.5") (features (quote ("default"))) (kind 2)) (crate-dep (name "rdb-pagination-core") (req "^0.1") (kind 0)) (crate-dep (name "rdb-pagination-derive") (req "^0.1") (optional #t) (kind 0)))) (hash "0bxlvs7r1cfdgq5xwxv5cw09rgxfxknc1cpgc75s6v93c7g9kv8s") (features (quote (("serde" "rdb-pagination-core/serde") ("mysql" "rdb-pagination-core/mysql") ("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:rdb-pagination-derive")))) (rust-version "1.61")))

(define-public crate-rdb-pagination-core-0.1 (crate (name "rdb-pagination-core") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "150q6yzhx8rn8svmc3bs00xv3sqbdab7gqxhsy46ixx7qvy69hz2") (features (quote (("mysql")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.61")))

(define-public crate-rdb-pagination-core-0.1 (crate (name "rdb-pagination-core") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1a5ddkmz47qlj2qckbvg8n48v41q55dwwsf31xhckkrhdmbxdhf9") (features (quote (("mysql")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.61")))

(define-public crate-rdb-pagination-derive-0.1 (crate (name "rdb-pagination-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rdb-pagination-core") (req "^0.1") (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rkcjfnbnpr3krh3fz330gq8x63n11fxscq39lh455169pjxzdnb") (rust-version "1.61")))

