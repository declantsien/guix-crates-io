(define-module (crates-io rd cl) #:use-module (crates-io))

(define-public crate-rdcl-number-analyzer-0.1 (crate (name "rdcl-number-analyzer") (vers "0.1.0") (hash "06p79c04wrycggw9ny903v44q48qbzm3zc1asvriyrhgrd3rdlj6")))

(define-public crate-rdcl_aoc_helpers-0.1 (crate (name "rdcl_aoc_helpers") (vers "0.1.0") (hash "01csdc13v089pnklgbhw6z7v8176vpd7wv957c8jacm04i0m32f8")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.0") (hash "1b62k068kgx74gqnkipf2kwjywb9y0pnf20j828xxcz6racjp5ld")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.1") (hash "1cm7cqni66fasv04fa14cq1vg116g6vx81gl8m7rsqfqwzbnbxkq")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.2") (hash "1lfyml7lmalazfv3a7l9z7fzk1mndbb79bgw7nr724nxda1mqjqj")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.3") (hash "0hc5dfcn5r78h1wi4b5kn4ai4xppjv009w441fc406xwmm74xywi")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.4") (hash "0ky1rdc8l3f74c9w35bk76zxh9q642q7v97jwc0jijaqa4ys004n")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.5") (hash "1l5bfazv2idj4y2a3cw4d1fl47kvrkam0d3sknfdxjcyzc4rqpi5")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.6") (hash "0zbhcv0mcynzmsmk8m2c4jrzmrwbfki673mw2nxh266ghnq7x7vg")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.7") (hash "0b4jgq7sf58hfjklyhb177gsbw2bc6hkv8w4cnykkdkx8wvmz4v0")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.8") (hash "1p84i3072i5k5ikid7lm1m7hdpblnmqipfdsrnpx4hcc98sf05xd")))

(define-public crate-rdcl_aoc_helpers-0.2 (crate (name "rdcl_aoc_helpers") (vers "0.2.9") (hash "0xwha1r21h8wj5h4pr2dwc9zjv0lys5zvz7afr0fp2qw4hhdgdg2")))

(define-public crate-rdcl_aoc_helpers-0.3 (crate (name "rdcl_aoc_helpers") (vers "0.3.0") (hash "0kqj6qqp2g95fr7yn7hjlkaki7yaba5y3dvyhr7j7bya0srf1yx5")))

(define-public crate-rdcl_aoc_helpers-0.4 (crate (name "rdcl_aoc_helpers") (vers "0.4.0") (hash "01ql3y0861pyrv8cxi9iy552z89lrca8p5wi810cfzr2zaiydmlg")))

(define-public crate-rdcl_aoc_helpers-0.5 (crate (name "rdcl_aoc_helpers") (vers "0.5.0") (hash "10ralzqq6j69dy0xssgzrblszsfzjf23vinbmcdf8qwk9lz8a5g3")))

(define-public crate-rdcl_aoc_helpers-0.5 (crate (name "rdcl_aoc_helpers") (vers "0.5.1") (hash "0m1jpmsn0dlg91n86h9n7d1hpa7qr40skyldx6mn3xbz7sa7kbd0")))

(define-public crate-rdcl_aoc_helpers-0.5 (crate (name "rdcl_aoc_helpers") (vers "0.5.2") (hash "0fhq4r7wni402q279g432m6jinsbhklrbr4p30xa8lpkc09wp4kx")))

(define-public crate-rdcl_aoc_helpers-0.5 (crate (name "rdcl_aoc_helpers") (vers "0.5.3") (hash "0dn4r7daj3cf0rhpiswmqr8knqyfdwf99hpqvqklasd432kf0ldk")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.0") (hash "1h0gh7rd2jzf3319a7q0j13rp5lyp899dlqxzw2gi2ygmw41h7wx")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.1") (hash "1al7xdvv5mhndx36qzq7xmrv845qgxcz5mk6d81l3k5xpcs0ma2b")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.2") (hash "12p66ah2x9sidki9mb0wbm1kn835xfyg0bl9g4amk7jfbai59kp6")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.3") (hash "0vpcb4fzayrmixrbl17rc57mk0rsmiqgv0l9v93nw9ni28sg52vv")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.4") (hash "02gkj3jba1rjgrn4cipyfd14y6d5v7w52xkiy0g3b10wwb1qvrf4")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.5") (hash "05z3x6kblq64ypj6bgnp8npgp2kwfg1iwi7hc0c3kfnmc78x15f5")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.6") (hash "13v9x2cxyvwkr367xpfr15089yq6gz3zrzp44j432rq0djrrgmgd")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.7") (hash "15r05044xpzqgzvdsy90x3qphsmjgrp6frnvmsan68y1grg5d3rl")))

(define-public crate-rdcl_aoc_helpers-0.6 (crate (name "rdcl_aoc_helpers") (vers "0.6.8") (hash "1qdrjbhr8rrwd8d7lvyqj13dkgkhvl269j5sxhr1lq345qw5lrdc")))

(define-public crate-rdcl_aoc_helpers-0.7 (crate (name "rdcl_aoc_helpers") (vers "0.7.0") (hash "07ljd4cqqmv93lgj8mf5izz2i9mh621gkv748hp5cxiljazjqg7y")))

(define-public crate-rdcl_aoc_helpers-0.8 (crate (name "rdcl_aoc_helpers") (vers "0.8.0") (hash "0nd96sjf6y98y1kngkfrvnmrkqcr6xngj7zwlg05xdqjanyi8ca4")))

(define-public crate-rdcl_aoc_helpers-0.8 (crate (name "rdcl_aoc_helpers") (vers "0.8.1") (hash "0xy78s0zidw597d735vhbw6bqpm3ga91idz4sa8wch38gfcq46ln")))

(define-public crate-rdcl_aoc_helpers-0.9 (crate (name "rdcl_aoc_helpers") (vers "0.9.0") (deps (list (crate-dep (name "grid") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "11gyp4v73mk7xxsncwx2nx01is2c3z7yj70icsdkgpmvkyl3dff3")))

(define-public crate-rdcl_aoc_helpers-0.9 (crate (name "rdcl_aoc_helpers") (vers "0.9.1") (deps (list (crate-dep (name "grid") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "1svzz54fah27n3gkydrwpqwhgsm47j3dg05rzn5ql8ph73mjh3lc")))

(define-public crate-rdcl_aoc_helpers-0.10 (crate (name "rdcl_aoc_helpers") (vers "0.10.0") (deps (list (crate-dep (name "grid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "15ymj0chc5zisyhigncg5lyg994s3jzg2rr5zi4gfklsjb9y2am3")))

