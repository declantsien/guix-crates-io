(define-module (crates-io rd bg) #:use-module (crates-io))

(define-public crate-rdbg-0.1 (crate (name "rdbg") (vers "0.1.0") (hash "0n2p2nlwakpi6ycyydc49n0ajn818qcgqai89b0gjygc4rx9b0yh") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1 (crate (name "rdbg") (vers "0.1.1") (hash "1546xps3n183m6naqcq8mavlhl8gxlq6569lvfjc7lm9fxaqdcpp") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1 (crate (name "rdbg") (vers "0.1.2") (hash "0zx3dc9lj7a3aq2xdi8z26fj6x944y8y4a57326hk2in24f9n1hc") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1 (crate (name "rdbg") (vers "0.1.3") (hash "17d8pi303i1216j2sxrn6h1v3nvag07s4b68ka6jzvkqf737pqjx") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1 (crate (name "rdbg") (vers "0.1.4") (hash "12fqlifqbhawaqivw6m1v9vw0y1kv8izaq563bmpb75fswsqp66w") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.1 (crate (name "rdbg") (vers "0.1.5") (hash "0zckcl0am2ikprsavkbp71vsnm3ffvgfxs15nk7gx19xrc8mqar2") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.2 (crate (name "rdbg") (vers "0.2.0") (hash "1xsyzn1y5v9hlfvvljafbl2ihq3l67zq87icl5qndk7f3cyl6cy7") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-0.2 (crate (name "rdbg") (vers "0.2.1") (hash "0wqix44mskpp2mgm4akw2p6lsvxqlp7l35zxq4m5hddvs4w6qr5q") (features (quote (("insecure-remote") ("enabled") ("default" "enabled"))))))

(define-public crate-rdbg-client-0.1 (crate (name "rdbg-client") (vers "0.1.0") (hash "09mljv212my96ncx5bal64lphdm13f4yka4i2n4pmcm762s61jds")))

(define-public crate-rdbg-client-0.1 (crate (name "rdbg-client") (vers "0.1.2") (deps (list (crate-dep (name "rdbg") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "148sa490icgy54sshil2k0is234cxw02rqhxnpska6qypm3jby01")))

(define-public crate-rdbg-client-0.1 (crate (name "rdbg-client") (vers "0.1.3") (deps (list (crate-dep (name "rdbg") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1kr60dlh0fbh09xmrvwav26bzhnq38rlb6hcq16f183yg2vnxh4r")))

(define-public crate-rdbg-client-0.1 (crate (name "rdbg-client") (vers "0.1.4") (deps (list (crate-dep (name "rdbg") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0pd0gb65yrxjx4kgnxafgyyadl7gpyczz88x5fw2xx37h18wnxiq")))

(define-public crate-rdbg-client-0.1 (crate (name "rdbg-client") (vers "0.1.5") (deps (list (crate-dep (name "rdbg") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "07m7jhzmzyfryc6w1q7gqdiba63zad81sl0yppr8a63ljzrjm52c")))

(define-public crate-rdbg-view-0.1 (crate (name "rdbg-view") (vers "0.1.0") (deps (list (crate-dep (name "rdbg-client") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1f8jixni4vchr4a40i05xqvc9qr9p8bjw2xjy0zprwf2h2w3vyz6")))

(define-public crate-rdbg-view-0.1 (crate (name "rdbg-view") (vers "0.1.2") (deps (list (crate-dep (name "rdbg-client") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1d6vf405h2x8ybkqc6vjpwh432nbzgg1lxwnj1s0ksdrn3fxb676")))

(define-public crate-rdbg-view-0.1 (crate (name "rdbg-view") (vers "0.1.3") (deps (list (crate-dep (name "rdbg-client") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "013dv5sl2rflc3gqndrz67rdaa23y0amndrp58jk2yjd3fkni9kq")))

(define-public crate-rdbg-view-0.2 (crate (name "rdbg-view") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rdbg-client") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "17kavhxjjhsw87qkx5v45s6gn218hz9ya7h9c0sh0911awjd8lhg")))

(define-public crate-rdbg-view-0.2 (crate (name "rdbg-view") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rdbg-client") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1m0ry951xsy43ndglbjr1n0jbpi2c6mq4bnv8fqdj1vkdlv2zrix")))

(define-public crate-rdbg-view-0.2 (crate (name "rdbg-view") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rdbg-client") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0m9c0rjqjjn7yw7xnnqvzpcfmbykgxi93bwbfky73jqrb3krcnys")))

(define-public crate-rdbg-view-0.2 (crate (name "rdbg-view") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rdbg-client") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "13ddgc6m7pl5d76h2806r60cj8rqd2h18mqgdsvmilrl15khmxlk")))

