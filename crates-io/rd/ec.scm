(define-module (crates-io rd ec) #:use-module (crates-io))

(define-public crate-rdeck-0.2 (crate (name "rdeck") (vers "0.2.0") (deps (list (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (default-features #t) (kind 2)))) (hash "13dr77kxsbki4yy34cfh6hv158rh9cz4zdvjyz0c2v93qj9nnin8")))

