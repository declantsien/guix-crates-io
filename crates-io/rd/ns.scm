(define-module (crates-io rd ns) #:use-module (crates-io))

(define-public crate-rDNS-0.0.1 (crate (name "rDNS") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0x0hr2rfdz7rq663n6y0d6vxmv13yks7d8rv3mf84kjl1qqy568y")))

