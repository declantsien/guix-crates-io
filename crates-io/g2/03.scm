(define-module (crates-io g2 #{03}#) #:use-module (crates-io))

(define-public crate-g203_rs-0.1 (crate (name "g203_rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)))) (hash "1xgmcpw3rjl3lyyscagr34cd26y6w7gjazgqc39qvkqwhbgwg3hs")))

(define-public crate-g203_rs-0.1 (crate (name "g203_rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)))) (hash "1jwm4jjp6z6n571068xjnd04dbmpblaxha8i7i3gl0amxj9ma97j")))

