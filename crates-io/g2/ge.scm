(define-module (crates-io g2 ge) #:use-module (crates-io))

(define-public crate-g2gen-0.1 (crate (name "g2gen") (vers "0.1.0") (deps (list (crate-dep (name "g2poly") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1mvybkl6il20b7zznd89w4jb3r57q06c0n9bxvhk39z88ly6b0p3")))

(define-public crate-g2gen-0.2 (crate (name "g2gen") (vers "0.2.0") (deps (list (crate-dep (name "g2poly") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0i8n5b2sqmazyjhljd63m4r2359bc8q9j6hfy6zj93cs0r23vp9s")))

(define-public crate-g2gen-0.2 (crate (name "g2gen") (vers "0.2.1") (deps (list (crate-dep (name "g2poly") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0nxib3wyvsg9bnmdw6qf9j0l3n0rxajq8hywl7y8iv354vry5g0s")))

(define-public crate-g2gen-0.4 (crate (name "g2gen") (vers "0.4.0") (deps (list (crate-dep (name "g2poly") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0m8590wqm57p4dik77s5x8lcydas9kwkzllc71f8r033djqh1h9g")))

(define-public crate-g2gen-1 (crate (name "g2gen") (vers "1.0.0") (deps (list (crate-dep (name "g2poly") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "057zd470k24rp9k7s9sbrmk0zy8m63jgnvz8q6z51pw4hii3jdrb")))

(define-public crate-g2gen-1 (crate (name "g2gen") (vers "1.0.1") (deps (list (crate-dep (name "g2poly") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "03l243pnj9pabd0pi0jcf4fyrxxhddx8ixv31gchs9gwn8jpcb7w")))

