(define-module (crates-io g2 -u) #:use-module (crates-io))

(define-public crate-g2-unicode-jp-0.4 (crate (name "g2-unicode-jp") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)))) (hash "0qjfmmpsgf11jm61wqa1wrqk4z1hl8ci28kza0fvzbqh3n3f51g6")))

(define-public crate-g2-unicode-jp-0.4 (crate (name "g2-unicode-jp") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)))) (hash "1fsh282515368n7zph9lr0vwlw4ss1crynhl5cd4kw9zmhdazjd2")))

