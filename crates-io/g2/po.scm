(define-module (crates-io g2 po) #:use-module (crates-io))

(define-public crate-g2poly-0.1 (crate (name "g2poly") (vers "0.1.0") (hash "08l9lk7lp14jqb838mz28mln06i2v0b6hnx1q8ipyf4snfskv8hn")))

(define-public crate-g2poly-0.1 (crate (name "g2poly") (vers "0.1.1") (hash "0p5zmls41slw79v7pmlngvjsq0zybyfk2am1y2yhpppsryiqiml5")))

(define-public crate-g2poly-0.2 (crate (name "g2poly") (vers "0.2.0") (hash "1nhi4zz9pwyrc1bllgx3c8577byp8xpcfc1l9cjzj1ifyj6r7wfs")))

(define-public crate-g2poly-0.4 (crate (name "g2poly") (vers "0.4.0") (hash "0ys5r96dr6ywam37mxkhj6wbp9qd6l5hxjc9gvq0g9gwi1w7cdz8")))

(define-public crate-g2poly-1 (crate (name "g2poly") (vers "1.0.0") (hash "04l2ncnp86mgdzcq277y3y6q9zf0j7ciihkns64nh8fksll77gd9")))

(define-public crate-g2poly-1 (crate (name "g2poly") (vers "1.0.1") (hash "04lfc0j6169ljcfh5837ra3izmlcwnzw1c8l5km071ika3kqcsmg")))

