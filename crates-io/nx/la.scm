(define-module (crates-io nx la) #:use-module (crates-io))

(define-public crate-nxlang-0.1 (crate (name "nxlang") (vers "0.1.0") (hash "1my6iqkbx5ch8vw05kqdd42dyvqa1s826qvys9j801m613lqi1yv") (yanked #t)))

(define-public crate-nxlang-core-0.1 (crate (name "nxlang-core") (vers "0.1.0") (hash "0vh6ykd32n6y564x8kdskd5yi47x96ki9mcyx2m3gxj4q2snqg21")))

(define-public crate-nxlang-extra-0.1 (crate (name "nxlang-extra") (vers "0.1.0") (hash "0i03vyaqbq9hqiyf1mn9pbcq4xz0xb2xjj3lbq6q3sbsw9scvr9l")))

(define-public crate-nxlang-script-0.1 (crate (name "nxlang-script") (vers "0.1.0") (hash "07zh9w6c4lxjwhv5q7671pfz7ldrnxkf3syx8g88w585dq9xgsjq")))

(define-public crate-nxlang-server-0.1 (crate (name "nxlang-server") (vers "0.1.0") (hash "0i5b4l414b7d2rfxi53p9vimb3jc42p4bxva71fhah9jqrsmxnzh")))

