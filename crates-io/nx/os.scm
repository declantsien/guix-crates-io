(define-module (crates-io nx os) #:use-module (crates-io))

(define-public crate-nxos-0.0.1 (crate (name "nxos") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "03vp44lf425sfq1znl2h9sfishzn65c59smpj7jw7vhkh14x1xll")))

