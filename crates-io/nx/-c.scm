(define-module (crates-io nx -c) #:use-module (crates-io))

(define-public crate-nx-c-0.1 (crate (name "nx-c") (vers "0.1.0") (hash "0fmd6d0i4d4ymhhicjx0s0l4ngzpcwibkykql4qzijgpb05536vi")))

(define-public crate-nx-cs-0.1 (crate (name "nx-cs") (vers "0.1.0") (hash "0jb25p3a8jwx19afrlw84lr1a1h7v4xpzyqlq0abx0mhxb4yrm1z")))

