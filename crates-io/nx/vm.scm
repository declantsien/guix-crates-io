(define-module (crates-io nx vm) #:use-module (crates-io))

(define-public crate-nxvm-0.1 (crate (name "nxvm") (vers "0.1.0") (deps (list (crate-dep (name "nanovm") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0k2m21kzz2sj0kr94al3wabjx7xlnxwdgcwapbcv95x2y4qjzs8v")))

