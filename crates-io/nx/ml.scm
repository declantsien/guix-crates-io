(define-module (crates-io nx ml) #:use-module (crates-io))

(define-public crate-nxml-0.1 (crate (name "nxml") (vers "0.1.0") (deps (list (crate-dep (name "print_return_values") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16rqcgbqzwy3r0dccbkagnbwjiaqaac9m9jdkavyzkniam02j7yc")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.0.0") (hash "126xm9m027vq35hpdvlpkq3p1vdh995d9xap1w5r9mdxzip0d7ph")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.0.1") (hash "1khddvb1y6nmav2dks4mjm07v3zrzh5m45gjjn1f4xc4d9v04n3n")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.0.2") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "03ci0jg60lx5z5c0qlvncnfjwpixhi4dggi0g67nvvfz439p2lv3")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.0.3") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1b2ls4whxmzvh6gsx89qz8v3dla3zicrnf54i7icpjb0403ivx54")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.0.4") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10s7my0grs2gziz50irn7nki5i4d5rwyrc584mpdp8khh4smq7v3")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.1.0") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1rwcpj0d0sk6ncnwhlgcdhjqbaqj34cb2ky25fy66kj4if8iiyxp")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.2.0") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "067ii4f6khyc3q5dbksyila6ri8xh325h6fwalnsl7h1h33krr4d")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.3.0") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1lw0r6x25ryb4ssic3avpmkll6g6q4cq42gbc3j7l23jr3di1d7n")))

(define-public crate-nxml-1 (crate (name "nxml") (vers "1.4.0") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "00w4n3bjp0rnd4djy0p46q4lfavjx5a8kir8db7b6lic24abxy1r")))

(define-public crate-nxml-2 (crate (name "nxml") (vers "2.0.0") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1didji4c2wz0287ynkc12bdhnqc0h9pjps3a78nmkxz4j7f1dqp9")))

(define-public crate-nxml-2 (crate (name "nxml") (vers "2.0.1") (deps (list (crate-dep (name "parco") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1zzrr5gh0afpy073bc0lwy52p7dr5zagl4rsmr1mny1143rn2d8x")))

(define-public crate-nxml-2 (crate (name "nxml") (vers "2.0.2") (deps (list (crate-dep (name "parco") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1xbk6k9lwnphzfk66n5n25gnzx9cr13i96s7q4d5r5n5viac47hv")))

(define-public crate-nxml-2 (crate (name "nxml") (vers "2.0.3") (deps (list (crate-dep (name "parco") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "05wn1yw5vvgr11r54jr4cf8p100c34ih52yd8dpvrh6cdhbfcdc3")))

(define-public crate-nxml-3 (crate (name "nxml") (vers "3.0.0") (deps (list (crate-dep (name "parco") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0qqv1g773x4y5j5wb8rzpbsa3r2rgypl0jbg658m8mghas45v847")))

