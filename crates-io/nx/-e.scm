(define-module (crates-io nx -e) #:use-module (crates-io))

(define-public crate-nx-emulator-0.1 (crate (name "nx-emulator") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "pixels-u32") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "07abkr6wls1z39bk5f1s5g58bs9fz6nsxxphds4xy2xkiizgv8wn")))

