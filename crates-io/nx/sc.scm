(define-module (crates-io nx sc) #:use-module (crates-io))

(define-public crate-nxscript-0.1 (crate (name "nxscript") (vers "0.1.0") (deps (list (crate-dep (name "ns") (req "^0.1.0") (default-features #t) (kind 0) (package "nxlang-script")) (crate-dep (name "swc_common") (req "^0.11.6") (default-features #t) (kind 0)) (crate-dep (name "swc_ecma_ast") (req "^0.49.4") (default-features #t) (kind 0)) (crate-dep (name "swc_ecma_parser") (req "^0.67.0") (default-features #t) (kind 0)))) (hash "0057jqr97fm5jjilikxvcpkhv5gw9agq4kxinx71h2v5pv08zn2x")))

