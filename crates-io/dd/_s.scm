(define-module (crates-io dd _s) #:use-module (crates-io))

(define-public crate-dd_statechart-0.3 (crate (name "dd_statechart") (vers "0.3.2") (deps (list (crate-dep (name "roxmltree") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0mdh4p5fp5kfhkc12wwzgcvr3441gzrfp4271m0bpqyl69fzv90c")))

(define-public crate-dd_statechart-0.4 (crate (name "dd_statechart") (vers "0.4.0") (deps (list (crate-dep (name "roxmltree") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0j0ylwsxyx2qihpc82ysx9y9zskxkqa79clgvhpsamlmrq8wyi54")))

(define-public crate-dd_statechart-0.4 (crate (name "dd_statechart") (vers "0.4.1") (deps (list (crate-dep (name "roxmltree") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0y0crck50f3aragy059z3vvbsyyns784msriv7hy9zzm40slcpbw")))

(define-public crate-dd_statechart-0.7 (crate (name "dd_statechart") (vers "0.7.0") (deps (list (crate-dep (name "roxmltree") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1r0kjsawswwswj2sfd7np40bw1cxflpmc2x8l5gwjvnpmnyj6r9i")))

