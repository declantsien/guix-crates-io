(define-module (crates-io dd aa) #:use-module (crates-io))

(define-public crate-ddaa_protocol-0.1 (crate (name "ddaa_protocol") (vers "0.1.0") (hash "1r6c0fzhri42z777v9bamwsi0wycqqnzd1k6fp9w5r3xfhvji0sp")))

(define-public crate-ddaa_protocol-0.2 (crate (name "ddaa_protocol") (vers "0.2.0") (hash "0ja9nv66wrmnscaf40py4xyvxm989671i80771qjwcpxqcwxx8l6")))

