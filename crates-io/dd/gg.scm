(define-module (crates-io dd gg) #:use-module (crates-io))

(define-public crate-ddgg-0.1 (crate (name "ddgg") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)))) (hash "0v2igak2w1vb4l1a6glcc2ixpg62dsfhgsj0hlqx5891kb7jdhr1") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ddgg-0.2 (crate (name "ddgg") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)))) (hash "0apdhzr1sb2bki7asssvv7f7hndyn9bqfc882ipcvksxlnmss3cn") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ddgg-0.3 (crate (name "ddgg") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)))) (hash "0ajz98fs4j1jp1pvfs3ardlgm9sbx8s4qh9ajb3jnqzzla33iv68") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ddgg-0.4 (crate (name "ddgg") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)))) (hash "1bz868ig4mpgqrv0m6b9lb0ahraaldnh74pjsc6309n5bxh49ji3") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ddgg-0.8 (crate (name "ddgg") (vers "0.8.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7") (default-features #t) (kind 0)))) (hash "04ynkdr111firck5kjqvh0vcvfczkh3h22fyvx6vh0z0n9ng9iwa") (features (quote (("js_names") ("default" "serde")))) (v 2) (features2 (quote (("serde_string_indexes" "dep:serde") ("serde" "dep:serde"))))))

(define-public crate-ddgg-0.8 (crate (name "ddgg") (vers "0.8.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7") (default-features #t) (kind 0)))) (hash "05f06n8xnwa9dw17b3p55h9z2q6i6ds09flgz18vzcjc5jmj40ik") (features (quote (("js_names") ("default" "serde")))) (v 2) (features2 (quote (("serde_string_indexes" "dep:serde") ("serde" "dep:serde"))))))

(define-public crate-ddgg-0.9 (crate (name "ddgg") (vers "0.9.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7") (default-features #t) (kind 0)))) (hash "149q6dmq6223xsn55w8kxvs4jqs4pzngpcw6gr9c445y5rxgwdc1") (features (quote (("js_names") ("default" "serde")))) (v 2) (features2 (quote (("serde_string_indexes" "dep:serde") ("serde" "dep:serde"))))))

(define-public crate-ddgg-0.10 (crate (name "ddgg") (vers "0.10.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7") (default-features #t) (kind 0)))) (hash "1bq01jj6c82057g31krvln1b4cmx4iss6hvpqik09572j5510ljr") (features (quote (("js_names") ("default" "serde")))) (v 2) (features2 (quote (("serde_string_indexes" "dep:serde") ("serde" "dep:serde"))))))

