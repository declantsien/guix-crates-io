(define-module (crates-io dd dk) #:use-module (crates-io))

(define-public crate-dddk_core-0.1 (crate (name "dddk_core") (vers "0.1.0") (hash "03dbqmkbffgcqvgsg0044rpbw6mqfjcrf3z1cqw6g8si76l380fk")))

(define-public crate-dddk_core-0.2 (crate (name "dddk_core") (vers "0.2.0") (deps (list (crate-dep (name "dddk_macro") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bghz482lbrj0ska359sc5r6ck4z5xp8ph827mwxcvq8hqhimkdb")))

(define-public crate-dddk_core-0.3 (crate (name "dddk_core") (vers "0.3.0") (deps (list (crate-dep (name "dddk_macro") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0l0v643y21w374kx2jdzwj4yf10n4y622s7yvwb1g7lacf3isvli")))

(define-public crate-dddk_core-0.4 (crate (name "dddk_core") (vers "0.4.0") (deps (list (crate-dep (name "dddk_macro") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0x9v3gy83xcx877sr2nwb4mj5n7qvfpigj6kyn4fgpya7iladc7r")))

(define-public crate-dddk_core-0.6 (crate (name "dddk_core") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "12a0ix17gizlgfqabdlkw4xhicj9swnkvrmrbpbfg0f3wfmbh5in")))

(define-public crate-dddk_macro-0.1 (crate (name "dddk_macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13wjk159r9vkpl2y2n86f49g4qbaq96k1ahml5jgrlijrqv0hwzm")))

(define-public crate-dddk_macro-0.2 (crate (name "dddk_macro") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "14y6knp1fq7qx981168nc99glv6hv39y384gahf1kqfm8h28bm8n")))

(define-public crate-dddk_macro-0.3 (crate (name "dddk_macro") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y2rnv9138zn95pv1m9330jirhb5x35smn471jjd2kvhls3gczfk")))

(define-public crate-dddk_macro-0.4 (crate (name "dddk_macro") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xnqlpn4b4hzxwv4dzlad1ha1ivx2aha045pd0f1iqgmp00k6xx8")))

(define-public crate-dddk_macro-0.5 (crate (name "dddk_macro") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13pmb18gsyf4xbhq451v8wqkzbnwlbk2brbln5hmmvps3jg70gfs")))

(define-public crate-dddk_macro-0.6 (crate (name "dddk_macro") (vers "0.6.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (default-features #t) (kind 0)))) (hash "063ik8hm132rp9brx1y4mjbm185jbzlrn75qhv4ypw40q43dyfvy")))

(define-public crate-dddk_security-0.1 (crate (name "dddk_security") (vers "0.1.0") (deps (list (crate-dep (name "dddk_core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11dh1p5x871zfisq6vhmbhchqga9g32fpj5mxphnb90bn3lrrv5h") (yanked #t)))

(define-public crate-dddk_security-0.1 (crate (name "dddk_security") (vers "0.1.1") (deps (list (crate-dep (name "dddk_core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0520j06ljvqv82m7mr60x9gbirbd2snn32fg5fgi7gflhrl3aqrf")))

(define-public crate-dddk_security-0.2 (crate (name "dddk_security") (vers "0.2.0") (deps (list (crate-dep (name "dddk_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "dddk_macro") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "180pg26593rr9if9wigkabklhy6zf0yln8qpwrbq6sgaf01f26k8")))

(define-public crate-dddk_security-0.3 (crate (name "dddk_security") (vers "0.3.0") (deps (list (crate-dep (name "dddk_core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "dddk_macro") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "00pdj4s0pph44v6aqqlr5fb3sbinxkkhcjmrvzgknkrfld0xj3z4")))

(define-public crate-dddk_security-0.4 (crate (name "dddk_security") (vers "0.4.0") (deps (list (crate-dep (name "dddk_core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "dddk_macro") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0sjir3zbfr3x5ya9vssivzmk6fc26fm6vzw07wjra7pkiznvn3cy")))

(define-public crate-dddk_security-0.6 (crate (name "dddk_security") (vers "0.6.0") (deps (list (crate-dep (name "dddk_core") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "dddk_macro") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "0bf0draij9d96dbwkj2d8ggxywji26as0nszvxqd83dq5qx2n4k7")))

