(define-module (crates-io dd -l) #:use-module (crates-io))

(define-public crate-dd-lib-0.1 (crate (name "dd-lib") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding8") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "12gpsd7kziwdmvvakk3pw9rai4bh10i4kh7lrwka5mnnwhl9av57")))

(define-public crate-dd-lib-0.1 (crate (name "dd-lib") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding8") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "08yh210d9yg2j0845sb3cndxgr5nglw7zxza0fshabl56qbmj5jw")))

(define-public crate-dd-lib-0.2 (crate (name "dd-lib") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding8") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0a5lmdimw6qlv5h10k0f8dhyw9nxpny4sy3942p1f2v968bh5sqp")))

(define-public crate-dd-lib-0.2 (crate (name "dd-lib") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding8") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0mk058pliv0xdyjahh824gsv3xk39gk6mj6dl1kgiqnbkj0wlrm8")))

