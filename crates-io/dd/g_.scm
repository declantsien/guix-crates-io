(define-module (crates-io dd g_) #:use-module (crates-io))

(define-public crate-ddg_cli-0.1 (crate (name "ddg_cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0l73rqgnr5hlab2cfakcgi8sipzyqsf41533ycvbhkbf26jy8qwd")))

