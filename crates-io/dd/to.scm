(define-module (crates-io dd to) #:use-module (crates-io))

(define-public crate-ddtools-0.0.0 (crate (name "ddtools") (vers "0.0.0") (deps (list (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0ia28cgr9kzjdwrpcgb2fwsn8yqi054rnf7md4mmmxn3yb8fvih1")))

