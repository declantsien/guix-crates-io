(define-module (crates-io dd sc) #:use-module (crates-io))

(define-public crate-ddsc-0.1 (crate (name "ddsc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (kind 0)) (crate-dep (name "libddsc-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1r0hvq8q1cn70szwnz3p96hqpndydnjmg8jyinwq035swlryppnv")))

(define-public crate-ddsc-0.1 (crate (name "ddsc") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (kind 0)) (crate-dep (name "libddsc-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0rs9lip466n3anyswkm9wrmmj4flj213wkxm0qi1fvsh9c12x9rr")))

