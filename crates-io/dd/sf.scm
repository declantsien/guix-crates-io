(define-module (crates-io dd sf) #:use-module (crates-io))

(define-public crate-ddsfile-0.2 (crate (name "ddsfile") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "10ymkijxvwbcxjkkylil1blhgpzdmzpig1rzdbjj4blfw8aj9cq3")))

(define-public crate-ddsfile-0.2 (crate (name "ddsfile") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "1x8j92iwkfawmqdi6kgwr1hqvsmy6a9mq7p3l9xm4aw3y3mry2m9")))

(define-public crate-ddsfile-0.2 (crate (name "ddsfile") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "19wmyhzf5mjyyix4xfvpdf2ybdfm8kr3wdsmhvpgr3avhhyv2ryy")))

(define-public crate-ddsfile-0.2 (crate (name "ddsfile") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "139a398ka1f0l3893zama7yiqai4qvwpsd1bgwip9ziql8ivig1y")))

(define-public crate-ddsfile-0.4 (crate (name "ddsfile") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ffalziivn52hn5xc0czz0317b2nm6sz5x5n32227d25cklmgwrl")))

(define-public crate-ddsfile-0.5 (crate (name "ddsfile") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w1cz3iciss77bg7la2lx9wxypx4lb2bw7z0v455mf6mfdwz1hws")))

(define-public crate-ddsfile-0.5 (crate (name "ddsfile") (vers "0.5.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)))) (hash "11rf0j02xp7nc8wpq74m1q1zxhazjxbc794dvrfxnh1ggjbcskjr")))

(define-public crate-ddsfile-0.5 (crate (name "ddsfile") (vers "0.5.2") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "enum-primitive-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i36igx6mmrr8xs14b1z72l2vhy4kml6jyxcqsb9xaipcwggx7a7")))

