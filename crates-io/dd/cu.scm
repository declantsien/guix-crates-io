(define-module (crates-io dd cu) #:use-module (crates-io))

(define-public crate-ddcutil-0.0.1 (crate (name "ddcutil") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "ddcutil-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1i2ahcbdvbsf5xdhs57q9j7crw5a4lr7zapc5qbx75g9k5m06a4d")))

(define-public crate-ddcutil-0.0.2 (crate (name "ddcutil") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "ddcutil-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "00q1isgs610zirc46sfw5bmcjvvcxczmn092xqq2pwj2vdxyj05c")))

(define-public crate-ddcutil-0.0.3 (crate (name "ddcutil") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "ddcutil-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0zynvbrf0kw4zlmgjvr5f0sgakshrnv61vyx2cjpaa2f002r2vzm")))

(define-public crate-ddcutil-sys-0.0.1 (crate (name "ddcutil-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0kw2fpv6nfnnfvrwh6gsl8ny4p57xvygiraialg2pkgs7my8zbqj")))

(define-public crate-ddcutil-sys-0.0.2 (crate (name "ddcutil-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1zmska5bm57fwyr40j0niilmx5c51ik7m8j13mf4dfdk3zv2a1aa")))

(define-public crate-ddcutil-sys-0.0.3 (crate (name "ddcutil-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1rlqh3rrflzdrrc0sgdfbaf51586j1ln13x1jbg1a5d3rnp55nx7")))

