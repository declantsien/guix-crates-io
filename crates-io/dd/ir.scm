(define-module (crates-io dd ir) #:use-module (crates-io))

(define-public crate-ddir-0.0.1 (crate (name "ddir") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "1n6cypdipy2zps50qma5fvjglw281nz92jw3q1vava2b6sdfn27x")))

(define-public crate-ddir-0.0.2 (crate (name "ddir") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "1k030mzl85xg3pp88agrj9n1m7lzrkvc64wymw70cqmam2z5xxfg")))

