(define-module (crates-io lv #{2r}#) #:use-module (crates-io))

(define-public crate-lv2rs-0.1 (crate (name "lv2rs") (vers "0.1.0") (deps (list (crate-dep (name "lv2rs-core") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gxyy06z8p0z18zyrihrd6zc294wdqqk1qdh3sli4qbl41n9p70x")))

(define-public crate-lv2rs-0.1 (crate (name "lv2rs") (vers "0.1.1") (deps (list (crate-dep (name "lv2rs-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1xmliw7ydg58hnd3s54yl7lx1aad4l4q4zpk3sj3df3hw2cv7pvb")))

(define-public crate-lv2rs-0.2 (crate (name "lv2rs") (vers "0.2.0") (deps (list (crate-dep (name "lv2rs-core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-urid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0nap6h5zdpbxrbkrm62y4r149nbhg0kb1w8bygxskc7dqj0hqraw")))

(define-public crate-lv2rs-0.2 (crate (name "lv2rs") (vers "0.2.1") (deps (list (crate-dep (name "lv2rs-core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-urid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12fr58ma0ynlfdf7llahnz71rq43y171zgrpxbj8jxy0ciq06vp9")))

(define-public crate-lv2rs-0.3 (crate (name "lv2rs") (vers "0.3.0") (deps (list (crate-dep (name "lv2rs-atom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-midi") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0sm52w8fddp8bzdd6m5mp3hv23rr0kw1fh105i3x74gbidg21km9")))

(define-public crate-lv2rs-0.3 (crate (name "lv2rs") (vers "0.3.1") (deps (list (crate-dep (name "lv2rs-atom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-midi") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02dkcsfvzqpn72pj5b2ahm4kpqllnkj4bvjialcrzl4sc1qqyi8c")))

(define-public crate-lv2rs-0.3 (crate (name "lv2rs") (vers "0.3.2") (deps (list (crate-dep (name "lv2rs-atom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-midi") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0cq6mjwr6sjxhrviv1a38snwmdal2ghhrglahj98906lq9bw2ka8")))

(define-public crate-lv2rs-atom-0.0.0 (crate (name "lv2rs-atom") (vers "0.0.0") (hash "1pfbncl61n69d8maij9bnnnmm8ll9277z10x7k8yikgh9my4qyrb")))

(define-public crate-lv2rs-atom-0.1 (crate (name "lv2rs-atom") (vers "0.1.0") (deps (list (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vjfam9d9ww3cnyj00j7sz6lzjd6i0h4drxwyn2k4fgwkyknb7rq")))

(define-public crate-lv2rs-atom-0.1 (crate (name "lv2rs-atom") (vers "0.1.1") (deps (list (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1kn1nimyf4z6qfabr759aid2yvzgpnq59x9d1hibwd16xa2m5p0q")))

(define-public crate-lv2rs-atom-0.1 (crate (name "lv2rs-atom") (vers "0.1.2") (deps (list (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dp3gsv837mgdbi2jjh42ks9yyh0psjxq0ryv3wln4j0mjvv4j37")))

(define-public crate-lv2rs-bufsz-0.0.0 (crate (name "lv2rs-bufsz") (vers "0.0.0") (hash "1nx36dv743zans2xbqifawx4prhxpxqyi52xj0y93jlj0x650wky")))

(define-public crate-lv2rs-core-0.1 (crate (name "lv2rs-core") (vers "0.1.0") (hash "133nfzhcc6caj6yxg19a8bfdhm3iqb4amfw5yknnkalhy87d0hfj")))

(define-public crate-lv2rs-core-0.1 (crate (name "lv2rs-core") (vers "0.1.1") (hash "0rfc15vj8s4b6k7jffh4clghcaaf6kqnghxjdbwvyaav8fzna8ak")))

(define-public crate-lv2rs-core-0.1 (crate (name "lv2rs-core") (vers "0.1.2") (hash "0c9f1xvpxr1gllrrag9i1adm71q8x6yix9a16a8z3pd2nzj5f1s5")))

(define-public crate-lv2rs-core-0.2 (crate (name "lv2rs-core") (vers "0.2.0") (hash "1lqcmh4rpkg9mjr4mw959hg304bcrkrz20lgx3pz00zhlk7cakka")))

(define-public crate-lv2rs-core-0.3 (crate (name "lv2rs-core") (vers "0.3.0") (hash "1ssxjd77jzvgj0dnbb17a8129prwpayx3d2lkfrxlxn64frf0ds6")))

(define-public crate-lv2rs-core-0.3 (crate (name "lv2rs-core") (vers "0.3.1") (hash "19g8j2liqhi92jiy245q57ai6ln4cr03a1haj8mq117lq9rh63d6")))

(define-public crate-lv2rs-core-0.3 (crate (name "lv2rs-core") (vers "0.3.2") (hash "1bsbw7am7ammmza9aawdbz308p6w0lz57xyb7jz5z0y7gfrvq3df")))

(define-public crate-lv2rs-core-0.3 (crate (name "lv2rs-core") (vers "0.3.3") (hash "0xh9slgnkwnyrdxc3jwns3facjdx84d5130pvl9f7bpdfi70hr4x")))

(define-public crate-lv2rs-dynmanifest-0.0.0 (crate (name "lv2rs-dynmanifest") (vers "0.0.0") (hash "19aylk09cck7dbf5wkb1mp7v1mg21s8zv2xpxrf8pkir8501dia8")))

(define-public crate-lv2rs-log-0.0.0 (crate (name "lv2rs-log") (vers "0.0.0") (hash "0yjj1f971hyhsw4sba7kyimxapp1ds3647knzjmfax00gqzjdrjm")))

(define-public crate-lv2rs-midi-0.0.0 (crate (name "lv2rs-midi") (vers "0.0.0") (hash "09j7iq7w325ivb58xzyis1v70daqszh1zz2rhhhgjak6v243353k")))

(define-public crate-lv2rs-midi-0.0.1 (crate (name "lv2rs-midi") (vers "0.0.1") (hash "0g6c0pqg5hdxi4szgswz7zvgd1js8p9qv8sph08hnm2bs3hdah5m")))

(define-public crate-lv2rs-midi-0.1 (crate (name "lv2rs-midi") (vers "0.1.0") (deps (list (crate-dep (name "lv2rs-atom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0v6b8cmbdfsk5dw3q6vl82fmwcb05pjafkbd70q56fadmdgig1in")))

(define-public crate-lv2rs-midi-0.1 (crate (name "lv2rs-midi") (vers "0.1.2") (deps (list (crate-dep (name "lv2rs-atom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lv2rs-urid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0k8qkb43aswccnnzqrh5dxpp3kqrjqg67rcpb46m7q33szh8pw8c")))

(define-public crate-lv2rs-morph-0.0.0 (crate (name "lv2rs-morph") (vers "0.0.0") (hash "15ff2lpsf7n1n1jsb8j9az86id1pgm7lhdgh2b9sj5fg2nlrl994")))

(define-public crate-lv2rs-morph-0.0.1 (crate (name "lv2rs-morph") (vers "0.0.1") (hash "0cwgvy7fx2k8k5zn6fjnmq3cjzb8z7n1wyvqvhrfksvsyq1jcwja")))

(define-public crate-lv2rs-options-0.0.0 (crate (name "lv2rs-options") (vers "0.0.0") (hash "0fjc8labp669y41762blmgq4h5fvmxlqffh20562yp1qv602l0h0")))

(define-public crate-lv2rs-options-0.0.1 (crate (name "lv2rs-options") (vers "0.0.1") (hash "1mvbl4p5lxnx16yd72w6v0hjmp69yxllyjqb0mf5lj9qvdmypdd4")))

(define-public crate-lv2rs-patch-0.0.0 (crate (name "lv2rs-patch") (vers "0.0.0") (hash "1nvwr2hg7250szfmrzgvrv2qns9kx1vnwbk8q2cmwnngy5yclwpz")))

(define-public crate-lv2rs-patch-0.0.1 (crate (name "lv2rs-patch") (vers "0.0.1") (hash "17alp2xf424rw0bsfyl2i7j07wlr240n67al2pyjc9l8964pgj02")))

(define-public crate-lv2rs-resize-0.0.0 (crate (name "lv2rs-resize") (vers "0.0.0") (hash "0rh60428r6zyzaly8r53idb0y3fvvcr90ks5rbad41ljbajrsca6")))

(define-public crate-lv2rs-resize-0.0.1 (crate (name "lv2rs-resize") (vers "0.0.1") (hash "0wi3i0hl6q6z33frf5g8rqnqk4bn4h0kq1n7ryaynzbxi2vhvxfd")))

(define-public crate-lv2rs-state-0.0.0 (crate (name "lv2rs-state") (vers "0.0.0") (hash "0fv2mw1q7mk56kg3900wj091rfbcy67js8vflhlks3xyra4z8ch7")))

(define-public crate-lv2rs-state-0.0.1 (crate (name "lv2rs-state") (vers "0.0.1") (hash "0i4p3yjik9kgx97qzxqjh34gshkj5wm7fk435hgns90wmm63i473")))

(define-public crate-lv2rs-time-0.0.0 (crate (name "lv2rs-time") (vers "0.0.0") (hash "0066g5fhhv9gvwn9gvh6fwiggb8jxlcmzbb7j5wgnvrv8xip624p")))

(define-public crate-lv2rs-time-0.0.1 (crate (name "lv2rs-time") (vers "0.0.1") (hash "10hjqaily51lj5ikils41pczj389xf43lxiwk7ripsh2cwsbln9s")))

(define-public crate-lv2rs-ui-0.0.0 (crate (name "lv2rs-ui") (vers "0.0.0") (hash "12giyc7vvij355mj6nicn77mfnyvkld6v55wp2kfz0jca2w3pfpw")))

(define-public crate-lv2rs-ui-0.0.1 (crate (name "lv2rs-ui") (vers "0.0.1") (hash "03xmrmjajiarw9f6gr204jvbaswyy2mkapmx0bwz2vd3zr773nf1")))

(define-public crate-lv2rs-units-0.0.0 (crate (name "lv2rs-units") (vers "0.0.0") (hash "0zgb72x1h8fqjp4ahswmlg69fbilnfx0p5xq05v0inlm9r16arj0")))

(define-public crate-lv2rs-units-0.0.1 (crate (name "lv2rs-units") (vers "0.0.1") (hash "120yls3lqn653nvq6ccq66br5jw99cdlxvvx3xc7a6ibf0glf0g2")))

(define-public crate-lv2rs-urid-0.0.0 (crate (name "lv2rs-urid") (vers "0.0.0") (hash "1hxx3vmx8dlaqbdnr289ddw2fl16d7cbc4w0nwxa3ci8jbjm29v2")))

(define-public crate-lv2rs-urid-0.1 (crate (name "lv2rs-urid") (vers "0.1.0") (deps (list (crate-dep (name "lv2rs-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "113rvsac4l6b733fxpkb1k3spx1z9fzwm4phx9l41xicn9mw8g0v")))

(define-public crate-lv2rs-urid-0.2 (crate (name "lv2rs-urid") (vers "0.2.0") (deps (list (crate-dep (name "lv2rs-core") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "15ilba3abxm8f7d0mpm6rlzhfxa28z2ab893wb23fdd7gn8il6n4")))

(define-public crate-lv2rs-urid-0.2 (crate (name "lv2rs-urid") (vers "0.2.1") (deps (list (crate-dep (name "lv2rs-core") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "11rsfmgizcd83miq0p1q251cd07kvn9lai257rni1z2xy83fbj3m")))

(define-public crate-lv2rs-urid-0.2 (crate (name "lv2rs-urid") (vers "0.2.2") (deps (list (crate-dep (name "lv2rs-core") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0hlqdnc5qhcns8ac84yxhn3dix56hmhqz3dkk9cd253bh66f2jk5")))

(define-public crate-lv2rs-worker-0.0.0 (crate (name "lv2rs-worker") (vers "0.0.0") (hash "1czx5qn8d9krmicfrwzbbpaiaw0v8qb6m4dj4dj6phq840d3f9nv")))

(define-public crate-lv2rs-worker-0.0.1 (crate (name "lv2rs-worker") (vers "0.0.1") (hash "08fwjh3vpm993yhhw5d864d6h4ac6p2avz03az4z4r591xhqpkhi")))

