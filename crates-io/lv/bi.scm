(define-module (crates-io lv bi) #:use-module (crates-io))

(define-public crate-lvbitfile2rust-0.1 (crate (name "lvbitfile2rust") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.23") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1jn6nqcnc9w64v93i9w3nbkraxnhp7qxm6ib39qgvaqcigzdpkz5")))

(define-public crate-lvbitfile2rust-0.1 (crate (name "lvbitfile2rust") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.23") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0ldj9nx9bfdqpax63rgk44f22qlqjzf9xnfbw7msikm8qiiwcvam")))

(define-public crate-lvbitfile2rust-cli-0.1 (crate (name "lvbitfile2rust-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "lvbitfile2rust") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18shkgm9ilk58jwq3a3jl115z6w0rq7fi82y0hdbxjc02vqr1kfc")))

(define-public crate-lvbitfile2rust-cli-0.1 (crate (name "lvbitfile2rust-cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "lvbitfile2rust") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19apzhp0k29lc782d421sfgrnrbi2v827g68qs7jqhk3jgjaidfz")))

(define-public crate-lvbitfile2rust-macros-0.1 (crate (name "lvbitfile2rust-macros") (vers "0.1.0") (deps (list (crate-dep (name "lvbitfile2rust") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0d77cchxcn4jv47hsa5b59wa1vvdb8by2agngy2hnpnha9b8bwxl")))

(define-public crate-lvbitfile2rust-macros-0.1 (crate (name "lvbitfile2rust-macros") (vers "0.1.1") (deps (list (crate-dep (name "lvbitfile2rust") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0qhhxzy685n7ycyc9ac75svwb61hhcnhn4iw9wfwplc6m81zdawi")))

