(define-module (crates-io lv #{2_}#) #:use-module (crates-io))

(define-public crate-lv2_raw-0.1 (crate (name "lv2_raw") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)))) (hash "0c8j9j2xgrazhlnpdsbgl7jnd1g6iwbc9wk01a7vc9bw4rdsa8i4")))

(define-public crate-lv2_raw-0.2 (crate (name "lv2_raw") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j3apz55c2k99pannzsja3szgrzji0zr2d01v1d7kzk43wiyfg2p")))

