(define-module (crates-io lv m-) #:use-module (crates-io))

(define-public crate-lvm-sys-0.1 (crate (name "lvm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.23") (default-features #t) (kind 1)))) (hash "0y4l0lhbvh838xzv6jnflajzzij5y6vk94gs9pn3c0nwhl94qml6")))

(define-public crate-lvm-sys-0.1 (crate (name "lvm-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "~0.40") (default-features #t) (kind 1)))) (hash "06arzngmq9pjcf9ib1im22qp20a7ccznl21gfsbs801wc1d26jqx")))

