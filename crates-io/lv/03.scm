(define-module (crates-io lv #{03}#) #:use-module (crates-io))

(define-public crate-lv03-0.0.1 (crate (name "lv03") (vers "0.0.1") (hash "1mqb9dmq6f3jmvjpsmr77i09a6cb58ypyblwjv1fz2gk2ap10q19")))

(define-public crate-lv03-0.0.2 (crate (name "lv03") (vers "0.0.2") (hash "1rqfyl4061clyk0gl48f81nws07p51jylryi296c7slk7yhsa1mm")))

(define-public crate-lv03-0.0.3 (crate (name "lv03") (vers "0.0.3") (deps (list (crate-dep (name "nav-types") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0lami1xs0a3fcl75in18bx04117aq5vf830g277y6m8dzdks0pnr") (features (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.0.4 (crate (name "lv03") (vers "0.0.4") (deps (list (crate-dep (name "bmp") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "nav-types") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.2") (default-features #t) (kind 0)))) (hash "17alwpg801wrf33cfzdymffzcccgglwl9bsgm0nas3vvm0mbyajj") (features (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.0.5 (crate (name "lv03") (vers "0.0.5") (deps (list (crate-dep (name "bmp") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "nav-types") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1.2") (default-features #t) (kind 2)))) (hash "0zbnlfrz8hk8fcvwzqsxl42v8ldgiz4qjv4r6gy6nzsngpqrc31p") (features (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.1 (crate (name "lv03") (vers "0.1.0") (deps (list (crate-dep (name "bmp") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nav-types") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)))) (hash "1rscnn41hwn10sc0ai6j8c2n4dpzdngmrf4ijmwdxklmvcf1bsi0") (features (quote (("nav-types-conversion" "nav-types") ("default"))))))

(define-public crate-lv03-0.1 (crate (name "lv03") (vers "0.1.1") (deps (list (crate-dep (name "bmp") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nav-types") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "oorandom") (req "^11.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)))) (hash "1jlx1q298rbgn43522c5nfh6c06q2gplfyz7rzxch7glja6y0ix7") (features (quote (("nav-types-conversion" "nav-types") ("default"))))))

