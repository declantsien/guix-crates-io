(define-module (crates-io n1 #{8t}#) #:use-module (crates-io))

(define-public crate-n18tile-0.1 (crate (name "n18tile") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.14") (features (quote ("png" "pdf" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "n18hex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12317h6v38rqq1vapdp0v1fyzwx8wj5jvvzh0sg96f7pbi0dny71")))

(define-public crate-n18token-0.1 (crate (name "n18token") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.14") (features (quote ("png" "pdf" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "n18hex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01yknzim67979gvwras5vd2m85hx8bh9x6imvzs1llzwbzdf1sdf")))

