(define-module (crates-io n1 #{8g}#) #:use-module (crates-io))

(define-public crate-n18game-0.1 (crate (name "n18game") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "n18catalogue") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18hex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18map") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18route") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18tile") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18token") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q0wnlfy08hihdhg42wb692z1aggv8k3lgdr7775qsrnj12ig2ml")))

