(define-module (crates-io n1 #{8c}#) #:use-module (crates-io))

(define-public crate-n18catalogue-0.1 (crate (name "n18catalogue") (vers "0.1.0") (deps (list (crate-dep (name "n18hex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18tile") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03z9nbilwc7qhh6f303xzf22ids9rjwq3rbglgdg5qbjm3kfcppw")))

