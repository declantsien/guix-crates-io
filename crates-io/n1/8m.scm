(define-module (crates-io n1 #{8m}#) #:use-module (crates-io))

(define-public crate-n18map-0.1 (crate (name "n18map") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.14") (features (quote ("png" "pdf" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "n18catalogue") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18hex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18tile") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18token") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1m1vb9nzlax4gzxvfw28049s1fgs03qv2byf4pc1p24w0k1ic5d3")))

