(define-module (crates-io n1 #{8b}#) #:use-module (crates-io))

(define-public crate-n18brush-0.1 (crate (name "n18brush") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.14") (features (quote ("png" "pdf" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "n18hex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18map") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18route") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18tile") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n18token") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b3hs04jc54vqp85hg4l9x00mx6d9vpiq9v1wywir4pgyzmjccsi")))

