(define-module (crates-io n1 #{8h}#) #:use-module (crates-io))

(define-public crate-n18hex-0.1 (crate (name "n18hex") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.14") (features (quote ("png" "pdf" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "pango") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "pangocairo") (req "^0.14") (default-features #t) (kind 0)))) (hash "1s9kvlb52plifakia12gjwzpbamb520zz0gpf9qcgm5x7ahxc4mv")))

