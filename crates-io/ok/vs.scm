(define-module (crates-io ok vs) #:use-module (crates-io))

(define-public crate-okvs-0.1 (crate (name "okvs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qgh2n8gvnxla8fqbp8wr53y4xxychsw41japr0gg9lhfwa2nf0q")))

(define-public crate-okvs-0.1 (crate (name "okvs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1483x0ff2khjizlsz8gd31vw03pc98h3x427fjs2kfawgmw78z0z")))

(define-public crate-okvs-0.2 (crate (name "okvs") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (default-features #t) (kind 0)))) (hash "04khlv1qjjd9ry1sskydjb4m5pdljafhbjyam333sirql6vs9dlv")))

