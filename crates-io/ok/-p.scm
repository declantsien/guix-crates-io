(define-module (crates-io ok -p) #:use-module (crates-io))

(define-public crate-ok-picker-0.0.1 (crate (name "ok-picker") (vers "0.0.1") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "03x6fkpx72snfj8c98ii10b88kd5mx6x354jwaij3dms4gibkw8w")))

(define-public crate-ok-picker-0.0.2 (crate (name "ok-picker") (vers "0.0.2") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0b7gcky67psxgax8wgq6fqg8mz9d38g9dqcl6m151v1j1xldc11v")))

(define-public crate-ok-picker-0.0.3 (crate (name "ok-picker") (vers "0.0.3") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0dqrirfw03dkqdc03snf5gy1r6gxx55mc4f5502aply1rc5ywqja")))

