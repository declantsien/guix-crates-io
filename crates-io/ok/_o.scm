(define-module (crates-io ok _o) #:use-module (crates-io))

(define-public crate-ok_or-0.1 (crate (name "ok_or") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0h5mybagjr4kldqdyf3l4wkij7c5yjlpaj2z6799y78rl3738sfp")))

