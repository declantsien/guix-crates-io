(define-module (crates-io ok th) #:use-module (crates-io))

(define-public crate-okthief-0.1 (crate (name "okthief") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "ok-picker") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "00f8j8sz9fclqidkygdl884fvcxsmazdqybfi4gqzhhdfkzi0a2i")))

