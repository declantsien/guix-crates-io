(define-module (crates-io ok la) #:use-module (crates-io))

(define-public crate-oklab-1 (crate (name "oklab") (vers "1.0.0") (deps (list (crate-dep (name "rgb") (req "^0.8.25") (default-features #t) (kind 0)))) (hash "04cz3dy3jh2c8zhixws1b2hnq1xmc9w71qqrj2qvl4qdlnnl0zj6")))

(define-public crate-oklab-1 (crate (name "oklab") (vers "1.0.1") (deps (list (crate-dep (name "rgb") (req "^0.8.36") (default-features #t) (kind 0)))) (hash "0hpcha8knid3qqr08049y2r5m7f39f9fkk09lhq03w57akgdb1iy")))

