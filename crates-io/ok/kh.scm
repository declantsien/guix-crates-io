(define-module (crates-io ok kh) #:use-module (crates-io))

(define-public crate-okkhor-0.4 (crate (name "okkhor") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rupantor") (req "^0.3") (default-features #t) (kind 2)))) (hash "1a537w0h670ydrhngkqa6d4jnci4kjf9cbb1jvkq7ypq8l59acl4") (features (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.4 (crate (name "okkhor") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rupantor") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bnh0gsgpj19ivq8hlxsx0k2bdclvc95h9cdnd2h3cisdlnm3vys") (features (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.5 (crate (name "okkhor") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rupantor") (req "^0.3") (default-features #t) (kind 2)))) (hash "196z9jv1hjwwin5508ci74chl3ym2wpcam8ic7d9aaq70h591ldc") (features (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.5 (crate (name "okkhor") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rupantor") (req "^0.3") (default-features #t) (kind 2)))) (hash "10rikb96vv0g58v26v2jsjgn6mkrdmmc4jbhmf5v4byx93aa0vds") (features (quote (("regex") ("editor"))))))

(define-public crate-okkhor-0.5 (crate (name "okkhor") (vers "0.5.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rupantor") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bqhbjs7ydchxqakspkrq8df0wcjpy2299a2i2z39yy9g0h4bvz6") (features (quote (("regex") ("editor"))))))

