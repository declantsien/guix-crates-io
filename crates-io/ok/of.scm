(define-module (crates-io ok of) #:use-module (crates-io))

(define-public crate-okofdb-0.1 (crate (name "okofdb") (vers "0.1.0") (deps (list (crate-dep (name "snap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "023fbnbkdf3yjc8blgb6i69kg9qll7vfv9klj8hnxibmqsk9n0hg")))

(define-public crate-okofdb-0.1 (crate (name "okofdb") (vers "0.1.1") (deps (list (crate-dep (name "snap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "0bj26sryqzznzmzcp7xh48b3b3xb021q2bq5ml55n0v77v55xw1y")))

(define-public crate-okofdb-0.1 (crate (name "okofdb") (vers "0.1.2") (deps (list (crate-dep (name "snap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "1ixdsishpdy4whjrvkwb4j8zl1zp0qcl58fxicd7c684a5gc4j5d")))

(define-public crate-okofdb-0.1 (crate (name "okofdb") (vers "0.1.3") (deps (list (crate-dep (name "snap") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "1b8j8mlskai5s22bk20ncigcy11cs7p9v007a0xvn3d5wlvdlkwc")))

