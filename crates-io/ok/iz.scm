(define-module (crates-io ok iz) #:use-module (crates-io))

(define-public crate-okizeme-0.1 (crate (name "okizeme") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "bevy-inspector-egui") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m0gfmzkmsm6lf9xb0wzchgbp520sh9lh1wj2v9l58mbhkg3v7fa")))

(define-public crate-okizeme_animation-0.1 (crate (name "okizeme_animation") (vers "0.1.0") (hash "1w8glrbrwvlh77rdwxvzzi9aah563wfas10hqpfqlqmnb3qiqvzh")))

(define-public crate-okizeme_core-0.1 (crate (name "okizeme_core") (vers "0.1.0") (deps (list (crate-dep (name "okizeme_animation") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_defense") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_input") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_offense") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_player") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1017pgl822np17zlrpycw1ja9wwbjayhcy5jfl17qzc03azk4j6v")))

(define-public crate-okizeme_defense-0.1 (crate (name "okizeme_defense") (vers "0.1.0") (hash "0ly1ya5wzl4gs16bwc79c6iv0ww8qdy90zrrldr3qw84ivxhkyqc")))

(define-public crate-okizeme_input-0.1 (crate (name "okizeme_input") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.6") (default-features #t) (kind 0)))) (hash "1sxcqqyj4wixbfavk9lxinf92308sl7wcg2vy2rsdnijnmx97b4l")))

(define-public crate-okizeme_offense-0.1 (crate (name "okizeme_offense") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "bevy-inspector-egui") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "okizeme_defense") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ysfcyswchp1bbpnx9hvj129gj23zs89f17jf32lwl7mn27ffv56")))

(define-public crate-okizeme_player-0.1 (crate (name "okizeme_player") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "okizeme_input") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okizeme_types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0h9kcixrmcfq5y1hh7m5040ygxgz9rnk7ifsgkjq1h538zq93rnm")))

(define-public crate-okizeme_types-0.1 (crate (name "okizeme_types") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.7") (default-features #t) (kind 0)))) (hash "0h10z2myb2gwslgya6zqixb31wj1fk9zbb625f27a2qfvwc4jijg")))

(define-public crate-okizeme_utils-0.1 (crate (name "okizeme_utils") (vers "0.1.0") (hash "1giyk29iyqyjindha62vamk3wzllvyzy5s4ffsb23d6nsznw5x94")))

