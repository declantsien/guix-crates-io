(define-module (crates-io ct fr) #:use-module (crates-io))

(define-public crate-ctfr-0.1 (crate (name "ctfr") (vers "0.1.0") (deps (list (crate-dep (name "axum") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13gndklnjj0hcc6pdrqvwys1y12kmj4pj6prmijdl8whf85rf8nf")))

