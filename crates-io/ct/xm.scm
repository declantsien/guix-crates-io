(define-module (crates-io ct xm) #:use-module (crates-io))

(define-public crate-ctxmap-0.1 (crate (name "ctxmap") (vers "0.1.0") (deps (list (crate-dep (name "ctor") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "inventory") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "00wbyb2w7jm7vwnpjmd2vc8rd77p6g0y44nj7kpi60wkm8ngmzyj") (yanked #t)))

(define-public crate-ctxmap-0.2 (crate (name "ctxmap") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "1saqg3z0w5ks8h3kxjn4wlpfvlqavd04d8cinvm9hjf40vyps389") (yanked #t)))

(define-public crate-ctxmap-0.3 (crate (name "ctxmap") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "169ccrxz2vyphv3xr65i00i10p1y2j5q2b2vbsdask07l6bmpqv6")))

(define-public crate-ctxmap-0.4 (crate (name "ctxmap") (vers "0.4.0") (deps (list (crate-dep (name "once_cell") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "0b9sn9rajkv4w7im6rkjw7ykxnb513zr6w3b1b10v72ciyn635md")))

(define-public crate-ctxmap-0.5 (crate (name "ctxmap") (vers "0.5.0") (deps (list (crate-dep (name "once_cell") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "15w9a79y4mng317angdf5avqzzb7p96wgxb9p1fjld8xcdjlrz26")))

