(define-module (crates-io ct sh) #:use-module (crates-io))

(define-public crate-ctsh-0.0.1 (crate (name "ctsh") (vers "0.0.1") (deps (list (crate-dep (name "ctsh-proc") (req "=0.0.1") (default-features #t) (kind 0)))) (hash "11dyr78f5l63wxg0ixbiqyl18h2x3j7i4nj8asvpllsnb2522yf6") (features (quote (("default"))))))

(define-public crate-ctsh-proc-0.0.1 (crate (name "ctsh-proc") (vers "0.0.1") (deps (list (crate-dep (name "duct") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0a3lm0hvqk81zkpfdzlps2hv5dgzs22xw8818r04ngbzp7jawih2") (features (quote (("default"))))))

