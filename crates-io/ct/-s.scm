(define-module (crates-io ct -s) #:use-module (crates-io))

(define-public crate-ct-sct-0.1 (crate (name "ct-sct") (vers "0.1.0") (deps (list (crate-dep (name "const-oid") (req "^0.9.2") (features (quote ("db"))) (default-features #t) (kind 0)) (crate-dep (name "der") (req "^0.7.6") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "x509-cert") (req "^0.2") (default-features #t) (kind 0)))) (hash "120xcwql5vhv2nw3dyz6l58i6s8a7bznswvc43lap7vih9n8yxih") (rust-version "1.65")))

