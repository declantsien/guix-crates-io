(define-module (crates-io ct -p) #:use-module (crates-io))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.0") (deps (list (crate-dep (name "inline-python-macros") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "194kp188gfyyqmbcswnrcn4s11ca8h9b91y1sc01pnbbnr81246l")))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.1") (deps (list (crate-dep (name "inline-python-macros") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1xzq86kyaip3yd09z45jgnx97zi54iyshl7grg3mka4jcznmnwp3")))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.2") (deps (list (crate-dep (name "inline-python-macros") (req "=0.7.0") (default-features #t) (kind 0)))) (hash "1jfzc91gl3zxyid6lgwxlwm3k211wxjj2216afc8l9q4rrsz0cl6")))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.3") (deps (list (crate-dep (name "inline-python-macros") (req "=0.7.1") (default-features #t) (kind 0)))) (hash "0d33d478h4gx9hlyq3aqhcwl8zls0m6y0kaz85raxy8a3fg0kj2s")))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.4") (deps (list (crate-dep (name "inline-python-macros") (req "=0.8.0") (default-features #t) (kind 0)))) (hash "1fs0n8fmnj4rgdb9kd8xmdk36kw4l0m3lbqsmss2ayg53jwy7jlf")))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.5") (deps (list (crate-dep (name "inline-python-macros") (req "=0.10.0") (default-features #t) (kind 0)))) (hash "01m0nlrpnwgp1jyvsar82q89kh653iqz0bfgdzcw1zpxd5n7vasq")))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.6") (deps (list (crate-dep (name "inline-python-macros") (req "=0.11.0") (default-features #t) (kind 0)))) (hash "18ccvvxk9r4g6ni4hqzajl6jc5vj92b1b26yf12s84ymy6qz753s")))

(define-public crate-ct-python-0.5 (crate (name "ct-python") (vers "0.5.7") (deps (list (crate-dep (name "inline-python-macros") (req "=0.12.0") (default-features #t) (kind 0)))) (hash "09rpb9pdqzj9sb2f0bl4albs9ycyp5gc8rimrgyzvlfnr1z1snis")))

