(define-module (crates-io ct ch) #:use-module (crates-io))

(define-public crate-ctchi-0.1 (crate (name "ctchi") (vers "0.1.0") (hash "1p8wd6l9nyjgz5vw6ri44rlm02289dnn377cyavcbmkr0p28cv8s")))

(define-public crate-ctchi-0.2 (crate (name "ctchi") (vers "0.2.0") (hash "0spgn1v4ql7w0cjfic7c4gqg9cb9hp0d6v09yfampqya89h6y2hx")))

(define-public crate-ctchi-0.3 (crate (name "ctchi") (vers "0.3.0") (hash "1qxsf5hb8125q7624fgm5m1sh38l3p74wmy9nyd5c62nl73z3z2x")))

(define-public crate-ctchi-0.4 (crate (name "ctchi") (vers "0.4.0") (hash "15fvhy646zbzzk5y0473g5xvqyh9fggawxfs2idpdabyv0v0ckn9")))

(define-public crate-ctchi-0.4 (crate (name "ctchi") (vers "0.4.1") (hash "05dfsawkpkhyf7mc212cdbc9zm6f8ixy2cd0xy8sm5qqxfzcp2my")))

(define-public crate-ctchi-0.5 (crate (name "ctchi") (vers "0.5.0") (hash "1giqr5w25pad04rsc6l5551bwmk1lkrxcl7mlcjpwqiyha0vl98v")))

(define-public crate-ctchi-0.6 (crate (name "ctchi") (vers "0.6.0") (hash "04df3xma8dl2pvfalc1v3afb0732fj88hyah6gs17xd3ysd7mgh7")))

(define-public crate-ctchi-0.7 (crate (name "ctchi") (vers "0.7.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zwg62vdf9h83ag6npmm8mc1qi5j5hib5888ajws3xx5077jp068")))

(define-public crate-ctchi-0.8 (crate (name "ctchi") (vers "0.8.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vv05ikrkpc4xp871yff6p7wc89p8jiim9pyf5qigs1zcfjl27ma")))

(define-public crate-ctchi-0.8 (crate (name "ctchi") (vers "0.8.1") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1l4yzx454xv5xwvwry7wgcgk0k8lmqvb3y16gl0yxqb3m1pdlgja")))

(define-public crate-ctchi-0.9 (crate (name "ctchi") (vers "0.9.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02gjpwdypl9y9275gzir4584vgps74jwfgg8klkm93mr6rn8qxqs")))

(define-public crate-ctchi-0.9 (crate (name "ctchi") (vers "0.9.1") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bbmj0wbvk9mz489gxldrjlff8wvvqjy0rs046n4gna5zpisb1xl")))

(define-public crate-ctchi-0.10 (crate (name "ctchi") (vers "0.10.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fd8q7a9rwvfj3brpzmc5p5k5qwskdgq4sc7x184njwlp9wi7k2f")))

(define-public crate-ctchi-0.11 (crate (name "ctchi") (vers "0.11.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02p12dzwjrrs7khcgnczxwk9ib2zavdmazm1ydfb0lixwdyvpzp4")))

(define-public crate-ctchi-0.11 (crate (name "ctchi") (vers "0.11.1") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1n3fam8x3cizx6zf8knxwh572icbln9qq1zdna6s6cwnfx51kngm")))

(define-public crate-ctchi-0.11 (crate (name "ctchi") (vers "0.11.2") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vl265vbjihjl788iknaz2bca83f5sg8pyrm3igz98ly8nb7jhgk")))

(define-public crate-ctchi-0.12 (crate (name "ctchi") (vers "0.12.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0a3mq5vi9lh9cr5v871rbs477l19c13h15r5zf5v8smgsmrk0sw9")))

(define-public crate-ctchi-0.13 (crate (name "ctchi") (vers "0.13.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vj93vkm9nvvqmd5md7cq5zhgj3dazwx01ayzbx7mx1zcq3palkp")))

(define-public crate-ctchi-0.14 (crate (name "ctchi") (vers "0.14.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "014w4ds4isbkb65c4v8417ipaqmf1bn5wigsrhav5ggcmli701z8")))

(define-public crate-ctchi-0.17 (crate (name "ctchi") (vers "0.17.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "06q679yd37ybddx3gn2908yg53l3n9znnalgha03prz3mhc3gvq1")))

(define-public crate-ctchi-0.18 (crate (name "ctchi") (vers "0.18.0") (deps (list (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hlvhsxyk3iix8cjj8zyrkj5z505yj5s7gz72daiw7r8i89wlwlk")))

(define-public crate-ctchi-0.19 (crate (name "ctchi") (vers "0.19.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "14b912z9q35yds2c96ml7qp3lrh63044qjvgl7lal9lwjspf3snd")))

(define-public crate-ctchi-0.19 (crate (name "ctchi") (vers "0.19.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ctchi_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hjxda5xp24ljzsv8m8b7hls9dg0b7yxxnr5qkwbbqz8hnk6b748")))

(define-public crate-ctchi_codegen-0.1 (crate (name "ctchi_codegen") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "parsing" "proc-macro"))) (default-features #t) (kind 0)))) (hash "05li99x94hjk57af5ajbyxhrv447gy4yayr310v6190db9anlhnl")))

(define-public crate-ctchi_codegen-0.2 (crate (name "ctchi_codegen") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "parsing" "proc-macro"))) (default-features #t) (kind 0)))) (hash "0lysl5ds5amn47smyk15l3naamh4qp7w35v0k3dy9r84hd50srzn")))

