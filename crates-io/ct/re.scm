(define-module (crates-io ct re) #:use-module (crates-io))

(define-public crate-ctre-0.6 (crate (name "ctre") (vers "0.6.1") (deps (list (crate-dep (name "ctre-sys") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "wpilib-sys") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "0l20ilfsp8ikbvxlfgjzli3lhy6sjiflfqvj76cn42anx4w90066") (features (quote (("usage-reporting" "wpilib-sys") ("serde" "ctre-sys/serde") ("default" "usage-reporting"))))))

(define-public crate-ctre-sys-5 (crate (name "ctre-sys") (vers "5.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1z737rnvc7w2j6qa1yi8k80pwjj7xp2q80jk6r4x5h28bawr5zdf") (links "CTRE_PhoenixCCI")))

(define-public crate-ctreg-1 (crate (name "ctreg") (vers "1.0.0") (deps (list (crate-dep (name "cool_asserts") (req "^2.0.3") (default-features #t) (kind 2)) (crate-dep (name "ctreg-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex-automata") (req "^0.4.6") (features (quote ("meta"))) (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0k6i9y2j4djhy1k4xdicaa2ys749in6ri6yav8wr6fqxmwmwxrhy")))

(define-public crate-ctreg-1 (crate (name "ctreg") (vers "1.0.1") (deps (list (crate-dep (name "cool_asserts") (req "^2.0.3") (default-features #t) (kind 2)) (crate-dep (name "ctreg-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex-automata") (req "^0.4.6") (features (quote ("meta"))) (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1zbajbb4x53k4fgna1d14sv6izahrclgq852dckl2rfs5gynh4w3")))

(define-public crate-ctreg-1 (crate (name "ctreg") (vers "1.0.2") (deps (list (crate-dep (name "cool_asserts") (req "^2.0.3") (default-features #t) (kind 2)) (crate-dep (name "ctreg-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex-automata") (req "^0.4.6") (features (quote ("meta"))) (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0vy6fn9jla238xzgpdbdkxcm19xkz1mgdlg2wb9g8dibnlrzqr8l")))

(define-public crate-ctreg-macro-1 (crate (name "ctreg-macro") (vers "1.0.0") (deps (list (crate-dep (name "lazy_format") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "regex-automata") (req "^0.4.6") (features (quote ("meta"))) (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("parsing" "proc-macro"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1ll3nfgyw4xxnqjdmssqzbnanfp99l7n03bzcwdmx137bi3cw8xc")))

(define-public crate-ctrem-0.1 (crate (name "ctrem") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "088y2y9z5vf70x6b4zk8pfjb2jbh6fpxkbfd02grb3w1mfary0p5")))

(define-public crate-ctrem-0.1 (crate (name "ctrem") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1bsrxrknsa8phi8lsrc6hj1cajdzxyms9ydb1q2h47j1yzyb2hdv")))

(define-public crate-ctrem-0.1 (crate (name "ctrem") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1p7qwhv3q5ljxvmx4f5p8lh27qpqa049jbblvn11dblglhkz4hgh")))

(define-public crate-ctrem-0.1 (crate (name "ctrem") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0v01yqx2sr2sb3x0cp7s0llbbz1xsymimj3ryx588fr7zhkflz5k")))

(define-public crate-ctrem-0.1 (crate (name "ctrem") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0q5yv35r4d45irmkiliffczb7v31b2zlpdvr2ryj1vh6ynj0wrrb")))

