(define-module (crates-io ct es) #:use-module (crates-io))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "1sxxbzqw0v0a3hasajygasbs5pkkp2s66f0rp4sshlgvp528x3hd")))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "0wlsw2ab36jqdcdzwxd58pir782449b937kljhxl49y7jzf2ssqm")))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "0zbg1lhfci68xr7dp44jww29v7s7bahzz2crp30qv6v1ypncj9sd")))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.3") (deps (list (crate-dep (name "gcc") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "08ky2xfy674zbj01h68y6vzfa4fa1j0p5xyr1d08g95ma6idw527")))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.4") (deps (list (crate-dep (name "gcc") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "149mywl382gz1hskwygnx4dvfh1vx3zywk43a5y6p7bx1ihnv9kc")))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "1sjwvqgqi80znjjzhs73nv0wvvlf42haq5fnjqvpcj3a90razfk7")))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "0rc3yb5xdzqh7zgndy149a1c4k4plz87si2qnirqchyfd4g2cqdd")))

(define-public crate-ctest-0.1 (crate (name "ctest") (vers "0.1.7") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "0id4qd5z3ndszsypbvbiz43d23ixq99bi7pwp7rdqanywnk9szz5")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "0308n4dk21qwxllcylszr9j16k870s2kyg8sn8faag5sicnbdddz")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "1f1w3b2qkhq7xhysblcbsh89n7jv2pqknnirbz9i7pvf2s572jvz") (yanked #t)))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "0zq0a1f2wnbj58gvad09xdqdcd53jni1x5p8g99pswi4xzjy8hl6")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "1lp7r4pq5gvqfx94qjxbqm2iwxn7hpbvli9ig8lm0pqckqdqdblz")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.4") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "01xv2z63839jswmlmbdwpw89583hzf14i19dvfzdlgsfiq8fkap2")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.5") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "1p2ydws88h7pb9rs43iynhbwmipvkp3d50d25l8y5gy0gcvyh6ig")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.6") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "13cias43lgwkqyybbrg4x9bx7xxnzihqy730cjk7pfp0fs7y6dr1")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.7") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "0yi8r33h59yj3ncd2yr48y98gvnzr6drkh9mnk751mh4ns7qjjhj")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.8") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "1f5m9rmx9sml3z1zwazpmnzb0l96mqsqy8z4s58cn98wr7nw0x1m")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.10") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "0awhcmr1jqx3sp6iaam8k37np2i8lr3jpiyx6lxphm73v6085mmx")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.11") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "0hif067w09qcfigyv43ki0rpmmg32hh6qglwda1bisr3ym669f68")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.12") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "1zmfmaappagwyc72cdh2jjcbnr9807ynica4lj6gxywcf8zch3b6")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.13") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "1fq1nik51cqdcq3wmglwhyx89s31c52sraz0qaw3pnzby0569pgv")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.14") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.59.1") (default-features #t) (kind 0)))) (hash "19g3qzln43v6w98yr3gkchzikpzvccl3433h5jpl4s0vnnil063z")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.15") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0n8w8jgbsq7xx3bqz719vr455sdqha2pn5q0sj3wmwpnydfbzql2")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.16") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1c15cl58i991q78181glmb4z34bsffbbcrkb5710pniyr6ha93ia")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.17") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1rfhmn6f059nw6sg2r3g9qvbwqa05niik14dnbvhrwhkhcyfsd53")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.18") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1nf5a2jsh3lg9vx5aaf60kdfh2l669f3g4ppqrzslimqixlqh7wc")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.19") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "04k4scaygdvaqikj0sqg1gw8rhx54skpkp250d84p84brmxgjf2j")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.20") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "083kxk810zhqyk3dddrl9nlms28hfbbm5dl6zwphsbdqn1iffn5h")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.21") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0l0cli2py89rv2af72wbd10w0hjvrn1qvcv6dqvm8k3bzdmnjd4z")))

(define-public crate-ctest-0.2 (crate (name "ctest") (vers "0.2.22") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax2") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1w5rwf78zmk29bsd8h8ir3qh7ynbkpwy4jhrknzaj9v015r7b1i8")))

(define-public crate-ctest2-0.3 (crate (name "ctest2") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 0)))) (hash "07n9h2adv67z78c29jv52k2811nyasax4grv1cfd8743h9ny2rkn")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0cddv8sr6523p0r4m87cq4djr4gcbih0im0mj8k4rr3krd566nph")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.1") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1gm09bgnkja911l0p4c7hgfzdcrqq6iq7s7pcndbc48i376bf84x")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.2") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 0)))) (hash "17f5h71h1vbih84vq1j4zgpivcqssspm2rkr4pcgjwbszax0chgf")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.3") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 0)))) (hash "0b2d8gfhnp17dq2l8s2prfxfmvfcdlj13a62xqmb08nkdhxc025s")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.4") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 0)))) (hash "162svcck20whfvbs0v4vwrbfdq31nqbx80vjr0v32kkavqkh1l9p")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.5") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 0)))) (hash "1iqmfd3smlwrfpai1b1i1l553jh18w3iiqh8ll67ga9yykg674wa")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.6") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 0)))) (hash "1lasaj89gcd0g5b9d6h038ak4rqlyykkx4kyi7ljbm7n94sysskw") (rust-version "1.56.0")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.7") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 0)))) (hash "112xzvzfhl9ddrshi679jxnfhvi1w860is9qzppvzcchwjkgmbgj") (rust-version "1.56.0")))

(define-public crate-ctest2-0.4 (crate (name "ctest2") (vers "0.4.8") (deps (list (crate-dep (name "cc") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "garando_syntax") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 0)))) (hash "15760ijsmq7wj9bc21lkd1v6bnykfii4z7r949pfmxbq0y01li75") (rust-version "1.56.0")))

