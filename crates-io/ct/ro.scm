(define-module (crates-io ct ro) #:use-module (crates-io))

(define-public crate-ctron-0.1 (crate (name "ctron") (vers "0.1.0") (hash "061rbgnj016jg0mxky3f29w58d819bvp8adlnnv2axw6pmgiii0y")))

(define-public crate-ctron-0.2 (crate (name "ctron") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0glq2nccdybg1k2508fs8zlvl477kkc15x9v88rfib3kv3wc043y")))

