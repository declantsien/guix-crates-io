(define-module (crates-io ct ae) #:use-module (crates-io))

(define-public crate-ctaes-sys-0.1 (crate (name "ctaes-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.9") (default-features #t) (kind 0)))) (hash "1x0v0jiq9p0w8b17yjwyy6q2ln1ry7jh2swv9w31rlnkyna6n2k7")))

