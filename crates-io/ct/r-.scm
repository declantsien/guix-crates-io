(define-module (crates-io ct r-) #:use-module (crates-io))

(define-public crate-ctr-drbg-0.0.0 (crate (name "ctr-drbg") (vers "0.0.0") (hash "1sxflzpfynb3r24wf2qgla8q9nchn6j2wssfwhcyyiflwpwa9i6q")))

(define-public crate-ctr-mode-0.0.0 (crate (name "ctr-mode") (vers "0.0.0") (hash "075wqw5q0505a7m3nq0lc8by1ghqmr0knlv0wf1g7ch2cnwblh43")))

