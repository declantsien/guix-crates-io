(define-module (crates-io ct ti) #:use-module (crates-io))

(define-public crate-ctti-0.0.0 (crate (name "ctti") (vers "0.0.0") (hash "030jlsq70i4nifh97h63yzsqc16iyjijwcndh22jzr34vb0r64zy")))

(define-public crate-ctti-0.1 (crate (name "ctti") (vers "0.1.0") (hash "046kvrc6pdmns7dbcm16lcfknjy5k5i9fd0q3l0al8gbbld0772a")))

(define-public crate-ctti-0.2 (crate (name "ctti") (vers "0.2.0") (hash "1mg20ps72cvhqvrzlgf9fcizda5k2skk44bqrcsyj9vhak77v9s6")))

