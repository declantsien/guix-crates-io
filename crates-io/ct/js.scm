(define-module (crates-io ct js) #:use-module (crates-io))

(define-public crate-ctjs-0.0.1 (crate (name "ctjs") (vers "0.0.1") (deps (list (crate-dep (name "ctjs_macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0x3c5rfgig14fs0l6m0wignz800jyvx05q7ckjxfccac1hlnnglj")))

(define-public crate-ctjs-0.0.2 (crate (name "ctjs") (vers "0.0.2") (deps (list (crate-dep (name "ctjs_macros") (req "=0.0.2") (default-features #t) (kind 0)))) (hash "1340iyr5zdv4y719q7q8rlprs58v1ifkl33idy34xd24ffbzhxcv")))

(define-public crate-ctjs_macros-0.0.1 (crate (name "ctjs_macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-js") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-serde") (req "^0.2") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0gvknwimg0bwcm1lvranc9y10pz14z1qcngzh8nwri20f7j0za66")))

(define-public crate-ctjs_macros-0.0.2 (crate (name "ctjs_macros") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-js") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-serde") (req "^0.2") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "013z71y71rif5k398zabrblqg9ikii98njzva9h4cs1cwikj1g6x")))

