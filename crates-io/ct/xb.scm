(define-module (crates-io ct xb) #:use-module (crates-io))

(define-public crate-ctxbuilder-0.1 (crate (name "ctxbuilder") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wbq6nl5jjgsfpcqgvvzfdqj32wnjy1jj1fiya4nkg6dx0irylcv") (features (quote (("default" "uuid")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

(define-public crate-ctxbuilder-0.2 (crate (name "ctxbuilder") (vers "0.2.0") (deps (list (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "1pijbjnydryyngxqdb22fpasqdg35riz46dgrckdc371qsff0h2j") (features (quote (("default" "uuid")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

