(define-module (crates-io ct -l) #:use-module (crates-io))

(define-public crate-ct-logs-0.1 (crate (name "ct-logs") (vers "0.1.0") (deps (list (crate-dep (name "sct") (req "^0.1") (default-features #t) (kind 0)))) (hash "048gih7aa5mz9a5zd9iyn54yj4kb238l50nhgl4yhvwjszn0w6xj")))

(define-public crate-ct-logs-0.2 (crate (name "ct-logs") (vers "0.2.0") (deps (list (crate-dep (name "sct") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z4sxvsmv24nabipic9fppl953jl2k35a62kyj4ziv1g4bxi3kb1")))

(define-public crate-ct-logs-0.3 (crate (name "ct-logs") (vers "0.3.0") (deps (list (crate-dep (name "sct") (req "^0.3") (default-features #t) (kind 0)))) (hash "1f0885ws3p49xh6dfgnhh7zjw9h4rhs9ljs8i9cnkhifzz98784f")))

(define-public crate-ct-logs-0.4 (crate (name "ct-logs") (vers "0.4.0") (deps (list (crate-dep (name "sct") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ancm8s8f6a1g88l4rrbjj4cqynncr83l6p3djzi4zk60x8vz94m")))

(define-public crate-ct-logs-0.5 (crate (name "ct-logs") (vers "0.5.0") (deps (list (crate-dep (name "sct") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "045jfvn82icvd4rq2m3xw0vpwh44a9d5z4rqwj8p4x0alzsjhnqg")))

(define-public crate-ct-logs-0.5 (crate (name "ct-logs") (vers "0.5.1") (deps (list (crate-dep (name "sct") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "07xya6m5aq8q6grb1llmwij5s3czvdp2hxidq240lmksn3w60ihv")))

(define-public crate-ct-logs-0.6 (crate (name "ct-logs") (vers "0.6.0") (deps (list (crate-dep (name "sct") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "04wiwiv4ghni3x2vni3z711mlz0ndqvh04vmdkbw3nr7zbsqcdjd")))

(define-public crate-ct-logs-0.7 (crate (name "ct-logs") (vers "0.7.0") (deps (list (crate-dep (name "sct") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0bk7pbmkjm18cgccm4a76vyn3wkaf2z4bh0jy9fk3dl4188i73lc")))

(define-public crate-ct-logs-0.8 (crate (name "ct-logs") (vers "0.8.0") (deps (list (crate-dep (name "sct") (req ">=0.6.0, <0.7.0") (default-features #t) (kind 0)))) (hash "1j5as2h789c2gazq3drl5i58xk8zzx6sxd1wdr19x3d6dwc1da61")))

(define-public crate-ct-logs-0.9 (crate (name "ct-logs") (vers "0.9.0") (deps (list (crate-dep (name "sct") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0z8f2g4cw9ndrg5zwabacaq8fqh2qpn57wp4sx6p0f6i3ps2narn")))

