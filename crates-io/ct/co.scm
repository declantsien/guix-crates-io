(define-module (crates-io ct co) #:use-module (crates-io))

(define-public crate-ctcore-0.1 (crate (name "ctcore") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)))) (hash "18hfqimcbwk0gac45jc5bmqgrvli619yc4lljdqk7qbmmf71y5n7")))

(define-public crate-ctcore-0.2 (crate (name "ctcore") (vers "0.2.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)))) (hash "0hg9ry28cs15rx8m5q7pv58wfrblyx6z624vgzllbi96wwd70mf8")))

