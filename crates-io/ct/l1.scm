(define-module (crates-io ct l1) #:use-module (crates-io))

(define-public crate-ctl10n-0.1 (crate (name "ctl10n") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1nbs986xqmhckidjk8z28haqsalnx69fxadvscfg29vp3q967hqq")))

(define-public crate-ctl10n-0.2 (crate (name "ctl10n") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "03r9g6xbd7ji5hrarh2hgq2dfpqjkn8zvwgdzfqghyw22wfdncmq")))

