(define-module (crates-io ct fl) #:use-module (crates-io))

(define-public crate-ctflag-0.1 (crate (name "ctflag") (vers "0.1.0") (deps (list (crate-dep (name "ctflag_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pnjvvyr296pzkzm2jx3xipbrda63yflkw3swq3aars00iwg1p8v")))

(define-public crate-ctflag-0.1 (crate (name "ctflag") (vers "0.1.1") (deps (list (crate-dep (name "ctflag_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ivdpj8s8d74dx6kc7mvgarnzbs6yxrpm4815a1bbv3ll8cqz2ap")))

(define-public crate-ctflag-0.1 (crate (name "ctflag") (vers "0.1.2") (deps (list (crate-dep (name "ctflag_derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "00j6a2plxd4isflryajsx0q8pdm8i1f9agifdfk27as4zmbkbapy")))

(define-public crate-ctflag_derive-0.1 (crate (name "ctflag_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.3") (default-features #t) (kind 0)))) (hash "12g6zb9mppihjd0yryjc7nrypiaj0vndxacx22k5agskvn1bm8nf")))

(define-public crate-ctflag_derive-0.1 (crate (name "ctflag_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.3") (default-features #t) (kind 0)))) (hash "0831ihh9rqy45m9nrdfp32py301dlg2g9d8fvsv9jl1zqwbzsn3y")))

(define-public crate-ctflag_derive-0.1 (crate (name "ctflag_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.3") (default-features #t) (kind 0)))) (hash "1vmphnsbrjxk9jgmbqrm0av4z8hrmiw3q96rjvkr7kmh4cywpfq8")))

