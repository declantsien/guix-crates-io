(define-module (crates-io ct rs) #:use-module (crates-io))

(define-public crate-ctrs-1 (crate (name "ctrs") (vers "1.0.0") (hash "167nhqk1igr0l0wpm0s68zqayrlq4a083sci0508lkbwb73h9x8r")))

(define-public crate-ctrs-1 (crate (name "ctrs") (vers "1.0.1") (hash "10xha2vga1y7qs6zz2x6xnqpra9is7r7czwk3j5grkmjg1ws0cgx")))

(define-public crate-ctrs-1 (crate (name "ctrs") (vers "1.0.2") (hash "0jg43gg7jxr4i3w2hq17y7p0grl7bw9i1ixc5md3z7prfq1xkv9f")))

