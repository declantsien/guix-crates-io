(define-module (crates-io ct -u) #:use-module (crates-io))

(define-public crate-ct-utils-0.0.1 (crate (name "ct-utils") (vers "0.0.1") (deps (list (crate-dep (name "typenum") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0ck1abrjnsi4zp5vxkjdgwkbh4igpc3rbwfdsx2vjvlbj5fcyx91")))

