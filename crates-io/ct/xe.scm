(define-module (crates-io ct xe) #:use-module (crates-io))

(define-public crate-ctxerr-0.1 (crate (name "ctxerr") (vers "0.1.0") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "02fm6kr0cgaksr03nnl4ln9gifsv9zmzjkklnj05xy4w5rky5rav")))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.0") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "11yfj04rm3d5pl9fi6ns6k67x0gqbq3v4dh7bk7c9s4mcwiz8i1b")))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.1") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1lqmsy0fc0mqxlvnwrnpbca61drjz5z4726qdxyyzld0kskmzgii")))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.2") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "10avp0amd1bharrkvq1fm2g7d7i5m8cprxi1151hsj95yky3r1rm")))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.3") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1s3d82w6civnyjr37wk5y7mn2xaysan5ik8v24vl4az656f1flqk")))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.4") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0g74kyf5dci5vmxxsmc9rqgldj44fxm70k82jmb8fnv1i5w0kxk7")))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.5") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "07kssbpxbfvyb0qvgmwsacaka576s8nmzsk9mjshqv3rg7fwcbnq")))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.6") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1zr5l34sl2c9axji2dc8bs9mlj1yc6ln2sq6rg9pj01maa523j1n") (yanked #t)))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.7") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "122w28p7i2b3pwrfs3ihfxwajwv8jdl0g1d00wvxgir1vwja5v9v") (yanked #t)))

(define-public crate-ctxerr-0.2 (crate (name "ctxerr") (vers "0.2.8") (deps (list (crate-dep (name "ctxerr_derive") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "13l7q1lhkzdng088379n20blgpgp3786ymj37yjp6krsa8wz4c6w")))

(define-public crate-ctxerr_derive-0.1 (crate (name "ctxerr_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rpznl3z32pvmbc20wc5lkpcmbbyxk0chzzxd9k0q52ckq7dnalv")))

(define-public crate-ctxerr_derive-0.2 (crate (name "ctxerr_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b67kfi97km1sazlq38wd00122mqwsvwd1im8z2zc9smnljgvdqs")))

(define-public crate-ctxerr_derive-0.3 (crate (name "ctxerr_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vdc2hqyb7ijw4wx3bd8cwkm82qqw5rrbr0y8hyv4i8vdkpmcwm1")))

(define-public crate-ctxerr_derive-0.4 (crate (name "ctxerr_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "079m75xclyyy09mazq7nvxcb1sqvpnmcg5w9c28467b8dkagbg4m")))

(define-public crate-ctxerr_derive-0.5 (crate (name "ctxerr_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dilbrm0a1rnyca5m1ycp4jf28abm3sbd1z5vasrvfwi31bbmdld")))

(define-public crate-ctxerr_derive-0.5 (crate (name "ctxerr_derive") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "140k2v6m1pw1pzjk0wr9nw80vyab9jldhhd8j8wqi8kmjhck0l5m")))

(define-public crate-ctxerr_derive-0.5 (crate (name "ctxerr_derive") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pddxg5pvy31rxb60zndj40hyam3sa5r829pzx8j0lk3d2cdnm6f") (yanked #t)))

(define-public crate-ctxerr_derive-0.5 (crate (name "ctxerr_derive") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n5hrfakpav11g4x1cjjp6c3d6j34i9di4dk8w4r3xg6c7g8iwns")))

(define-public crate-ctxerr_derive-0.5 (crate (name "ctxerr_derive") (vers "0.5.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z6184x56d0akbvrpsi7wc4x7ilcyl8kkl859ndd45shjyzi16ay")))

