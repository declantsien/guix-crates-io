(define-module (crates-io ct ty) #:use-module (crates-io))

(define-public crate-ctty-0.1 (crate (name "ctty") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (target "cfg(any(target_os = \"freebsd\", target_os = \"macos\"))") (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(any(target_os = \"freebsd\", target_os = \"macos\"))") (kind 0)) (crate-dep (name "nix") (req "^0.19") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0csgya5xvpi01d8gizb8xky09ab1kafvmx0mja0hwgsiqldwg4hq")))

