(define-module (crates-io ct ow) #:use-module (crates-io))

(define-public crate-ctow-0.1 (crate (name "ctow") (vers "0.1.0") (hash "02s9zf10x9gv16379p3wxjv1s3dxj1h94gqinj0hcch9gkyv7y3j")))

(define-public crate-ctow-0.1 (crate (name "ctow") (vers "0.1.1") (hash "01lwdkj6lmjc881mm28l14pl8knm9k6aflgffn4zcgk6nz214s0d")))

(define-public crate-ctow-0.1 (crate (name "ctow") (vers "0.1.2") (hash "0r8qmn71nc5lb27gl0kcnfmr606lwjxg2zqww566g4zclqz49fjg")))

(define-public crate-ctow-0.1 (crate (name "ctow") (vers "0.1.3") (hash "10a62qk60p2l1j9hx60q9q11l7dn6v6l0rwxjaxhbiv9qviy8v4j")))

(define-public crate-ctow-0.1 (crate (name "ctow") (vers "0.1.4") (hash "0brj38q3qd8zf7adn8d833y4ps8i0ahxx0vmwz723lvklpgdzi92")))

(define-public crate-ctow-0.1 (crate (name "ctow") (vers "0.1.5") (hash "13h3r7fx0006gf34sxn7hyrz64q6ys80l2igizs02vlz7llanyms")))

