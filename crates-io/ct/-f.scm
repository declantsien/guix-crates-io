(define-module (crates-io ct -f) #:use-module (crates-io))

(define-public crate-ct-for-0.1 (crate (name "ct-for") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "0hg2mgd28r0qj64hp57a6fadb7xf41jmxkf4zwvpwz6cqfijal5r")))

(define-public crate-ct-for-0.1 (crate (name "ct-for") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "06gys4fl7v5lm8lfp98l0m7c0fx8dpp8fr0rbfdg9qvgkpbld9wg")))

