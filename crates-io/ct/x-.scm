(define-module (crates-io ct x-) #:use-module (crates-io))

(define-public crate-ctx-thread-0.1 (crate (name "ctx-thread") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)))) (hash "0rmkv8ikxmlarv610ps4rib82h5p4q4dc3rwa59zjq0pkvaybv8j")))

(define-public crate-ctx-thread-0.1 (crate (name "ctx-thread") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)))) (hash "0rx5wvn0zc7qwlbk300nlxdcnqdrwk0dnmwykyx8by9vwswjnzls")))

