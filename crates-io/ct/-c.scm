(define-module (crates-io ct -c) #:use-module (crates-io))

(define-public crate-ct-codecs-0.1 (crate (name "ct-codecs") (vers "0.1.0") (hash "1jvk0bbbrvfq8s3b95mzs4pm9rmncnkxnmh49x8wr63adk32541z") (features (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-0.1 (crate (name "ct-codecs") (vers "0.1.1") (hash "0fk7c2d5ca25rvpyk85jk0iysmwqbig0j20mr1fxjk9zi2lw4hz9") (features (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-1 (crate (name "ct-codecs") (vers "1.0.0") (hash "0mi39i5lzql8vkfr5dda70iv06xjgh4cgwsbl5vnz0imxji32cr7") (features (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-1 (crate (name "ct-codecs") (vers "1.1.0") (hash "1qz5ayqfp1zr0m898wmcqzg6d277gh8mk3f39zlw5xq7zrn43gpp") (features (quote (("std") ("default" "std"))))))

(define-public crate-ct-codecs-1 (crate (name "ct-codecs") (vers "1.1.1") (hash "1pvmrkk95jadmhhd5mn88mq2dfnq0yng8mk3pfd5l6dq0i2fpdzk") (features (quote (("std") ("default" "std"))))))

