(define-module (crates-io ct tw) #:use-module (crates-io))

(define-public crate-cttw-0.0.8 (crate (name "cttw") (vers "0.0.8") (deps (list (crate-dep (name "oauth-client") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "twitter-api") (req "*") (default-features #t) (kind 0)))) (hash "0f6xw3v8yip3vrw31zhw6figbrnsjklkjd5j8rmpphygsn39gmny")))

(define-public crate-cttw-0.0.10 (crate (name "cttw") (vers "0.0.10") (deps (list (crate-dep (name "oauth-client") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "twitter-api") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h55mdwpvws5wqy9f3rq1m1xg83bs50qp8gl84yp2q7bbz112v8w")))

