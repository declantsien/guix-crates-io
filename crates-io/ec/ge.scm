(define-module (crates-io ec ge) #:use-module (crates-io))

(define-public crate-ecgen-rs-0.1 (crate (name "ecgen-rs") (vers "0.1.0") (deps (list (crate-dep (name "genawaiter") (req "^0.99.1") (features (quote ("futures03"))) (default-features #t) (kind 0)))) (hash "0y690hlpzc0abzd1jz9076pjcvdba89cvpfx1zk6hc9qlxlbq1fc")))

(define-public crate-ecgen-rs-0.1 (crate (name "ecgen-rs") (vers "0.1.1") (deps (list (crate-dep (name "genawaiter") (req "^0.99.1") (features (quote ("futures03"))) (default-features #t) (kind 0)))) (hash "08fg10ma35iia013b93pm4a59r7qynhi8nfanyiqphgi233yf4bf")))

