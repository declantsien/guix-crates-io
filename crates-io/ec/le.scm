(define-module (crates-io ec le) #:use-module (crates-io))

(define-public crate-eclectic-0.0.1 (crate (name "eclectic") (vers "0.0.1") (deps (list (crate-dep (name "collect") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "compare") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trie") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "19k5zf5acxjh9w8zgpbjqs0y9k5m68al7lf9rkkm7xfn7997ipb1") (features (quote (("default" "collect_impls" "trie") ("collect_impls" "collect" "compare"))))))

(define-public crate-eclectic-0.0.2 (crate (name "eclectic") (vers "0.0.2") (hash "0dd0ahl5lahbljqr2wkjlw1mk9d8a0xhhk8mknjdr15xcmr4r205")))

(define-public crate-eclectic-0.0.3 (crate (name "eclectic") (vers "0.0.3") (hash "1yqsc8xbbd8c89d8rsbw974sfyjsxk871vqki7vjldf9pp9qwzy5") (features (quote (("nightly"))))))

(define-public crate-eclectic-0.1 (crate (name "eclectic") (vers "0.1.0") (deps (list (crate-dep (name "linear-map") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "02zrjw4sm2yrgr1vxfzzbf74sk5vpavqx7hrk6zssbp07mmhc8q3")))

(define-public crate-eclectic-0.3 (crate (name "eclectic") (vers "0.3.0") (deps (list (crate-dep (name "linear-map") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trie") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1l2qxxi21p7zzc4xmsmn59lan1dwsgjq465h3p72iiqnh86lrzxx") (features (quote (("std_impls") ("nightly") ("default" "std_impls"))))))

(define-public crate-eclectic-0.4 (crate (name "eclectic") (vers "0.4.0") (deps (list (crate-dep (name "linear-map") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trie") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0b4czvyn68zd3fmgcp5jihqzcg32vwcav89m52jhzc59r9d43mac") (features (quote (("std_impls") ("nightly") ("default" "std_impls"))))))

(define-public crate-eclectic-0.5 (crate (name "eclectic") (vers "0.5.0") (hash "0npb2365iga3zdkgq3xywgxnjpf0cwsld09yy2gs7np0k2adfax2")))

(define-public crate-eclectic-0.6 (crate (name "eclectic") (vers "0.6.0") (hash "16pan5zzdh19icnhwqj64577npms7p6gxvqg1kgq5ihca17nygb0")))

(define-public crate-eclectic-0.7 (crate (name "eclectic") (vers "0.7.0") (hash "09r70f4cbwzr9pkjdi87i6l1xv5w43nl3k726lcrg550fdwn1j76")))

(define-public crate-eclectic-0.8 (crate (name "eclectic") (vers "0.8.0") (hash "1snawd7pkd1v5d9yjf657jvdy9b6i855skmg565mnva0qbn2jlqi")))

(define-public crate-eclectic-0.9 (crate (name "eclectic") (vers "0.9.0") (hash "0kwk8bcl8r7vs3dw6y657f33nz94nrf71ygf7k0ipmyz994gj0zn")))

(define-public crate-eclectic-0.10 (crate (name "eclectic") (vers "0.10.0") (hash "0p0s03wjz1gqbc1jc6lpbdv7adx46w5j0z57z255zn6hfh9x095m") (features (quote (("nightly"))))))

(define-public crate-eclectic-0.11 (crate (name "eclectic") (vers "0.11.0") (hash "0zhxx587bx23il6qazh7rmrrvh3q58jmpw3bafjdjf82sh01jvm0") (features (quote (("nightly"))))))

(define-public crate-eclectic-0.12 (crate (name "eclectic") (vers "0.12.0") (hash "0r2gw1nrysjnyrgy34h03nnzj8fpbysray0y4d7nwymhkcy1kafq") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-eclectica-0.0.1 (crate (name "eclectica") (vers "0.0.1") (hash "18dd75as5f1s5hsk7c55psxjnb8h6yrncl828xkpkp2hzv74s5j8")))

(define-public crate-eclectica-0.0.2 (crate (name "eclectica") (vers "0.0.2") (hash "04hy4assc6kf9ja4a2829nahl35ksbka2m3pi2hv9dvgl125g223")))

(define-public crate-eclectica-0.0.6 (crate (name "eclectica") (vers "0.0.6") (hash "0sa5mc09v4f1r2s6l1q4yrsg2pk4sin72r3vlr4bss09rc8g5fmi")))

(define-public crate-eclectica-0.0.7 (crate (name "eclectica") (vers "0.0.7") (hash "1xdwgw07byf0iclrsp9smx1nwrlcm0s7wxcqb4l4rn3jp4y6g0g6")))

(define-public crate-eclectica-0.0.8 (crate (name "eclectica") (vers "0.0.8") (hash "1pm4x27jhvjav4n0f6v5nbzvc2xgz3jyc9cvpzgk395lxyq77363")))

(define-public crate-eclectica-0.0.9 (crate (name "eclectica") (vers "0.0.9") (hash "1dijcy49ykls8npwq0p5i0z3p56f911vk2p36dz2g6kn4vd87ixb")))

(define-public crate-eclectica-0.0.12 (crate (name "eclectica") (vers "0.0.12") (hash "0vb4d34sgnfcah7qj29bsdgl749jrlaq4dfgjlrrcrjgxfr29d7d")))

(define-public crate-eclectica-0.0.13 (crate (name "eclectica") (vers "0.0.13") (hash "1k4lr0qi35jhm25lx3bw963wh7xil51zx3p4mcfaxj4x66f87m47")))

(define-public crate-eclectica-0.0.14 (crate (name "eclectica") (vers "0.0.14") (hash "09bs4l1hgcm4y35fvdd80al3biy1d82bhb54k6wi3553080zk8gp")))

(define-public crate-eclectica-0.0.15 (crate (name "eclectica") (vers "0.0.15") (hash "1sr7mb4campzd9n860v6q88vmpssqgdgrg267hl702g2fnjgkj8j")))

(define-public crate-eclectica-0.1 (crate (name "eclectica") (vers "0.1.0") (hash "02jpa15rrvmv19rz2iwdrjm8f67sw2vhcjjrj99sjjh71rd8nb9a")))

(define-public crate-eclectica-0.1 (crate (name "eclectica") (vers "0.1.1") (hash "0ikybh3yl0jvr8jza1fazgi3wzmrlq7fvi85vzak2x3j3iqzgd8l")))

(define-public crate-eclectica-0.2 (crate (name "eclectica") (vers "0.2.0") (hash "1ks7ql83hv5b50d55xsbllmswf5znqxja7akxi4crd09wy29ij78")))

(define-public crate-eclectica-0.2 (crate (name "eclectica") (vers "0.2.1") (hash "0xv2281fmas6i43m02fq7xkvfqykpnsh1ki63fa4vl3424dbg3q4")))

(define-public crate-eclectica-0.3 (crate (name "eclectica") (vers "0.3.0") (hash "1gr240bc272brlwkgz7cbl0j33ivhb22xqxb4hcvs7kk0layrsq1")))

(define-public crate-eclectica-0.3 (crate (name "eclectica") (vers "0.3.1") (hash "1afjv4nadb4xick7h8mlax0mnh24y1m8bq1a90v52rzd8i9igbrc")))

(define-public crate-eclectica-0.3 (crate (name "eclectica") (vers "0.3.2") (hash "10bhy3jnkw85h6jwb9pl5yimwgfli8myvsaqhxj4cdkzhx5y1ikp")))

(define-public crate-eclectica-0.3 (crate (name "eclectica") (vers "0.3.3") (hash "150ncaykc71rkqvslhw1wg8w6hdq3lacdavcsn245pfv53nzlil2")))

(define-public crate-eclectica-0.4 (crate (name "eclectica") (vers "0.4.0") (hash "0ll4zn96qa833iq2kr51bbkm58ww5356jdq50b14840k6zlzipri")))

(define-public crate-eclectica-0.4 (crate (name "eclectica") (vers "0.4.1") (hash "0ilxc1imgm2qr84sk1vcq3q6rf46c16h198yz6p56ldz7l8c7ay1")))

(define-public crate-eclectica-0.5 (crate (name "eclectica") (vers "0.5.0") (hash "0dj876ggrpaqldk8giagbx4a1l2jxsjbq2p4qq1xsfx8nfcnb6wr")))

(define-public crate-eclectica-0.6 (crate (name "eclectica") (vers "0.6.0") (hash "0v5cxc6xvyxdpvky0p1a91n0440kapr17ri6dp1rqwzrg84nq90c")))

(define-public crate-eclectica-0.7 (crate (name "eclectica") (vers "0.7.0") (hash "0xlgm01h7jvf4xd495v0b5qrg01y9zbdvs03400miny2q8gf9hpw")))

(define-public crate-eclectica-0.7 (crate (name "eclectica") (vers "0.7.1") (hash "18yvw9nasaqqgfr9scjffkgcd5v6739w0wdfff9qs5bmd6xkqb17")))

(define-public crate-eclectica-0.8 (crate (name "eclectica") (vers "0.8.0") (hash "0c9y2ha68cwfj7dd1kdh1b87i7c22ppk4rikkg1bdbrb1hvr6icy")))

(define-public crate-eclectica-0.8 (crate (name "eclectica") (vers "0.8.2") (hash "1wwvs48dsx95ww6pi9dwfk6q2hayr50f8y3flqj8xln8b9j3kp3v")))

(define-public crate-eclectica-0.8 (crate (name "eclectica") (vers "0.8.3") (hash "1g39dninhhmlzwcng6nbyf6k0din6y1kvbwsdxzviqbm81n8i1i9")))

(define-public crate-eclectica-0.8 (crate (name "eclectica") (vers "0.8.4") (hash "10jl4fr3zca6wc3y6zw1bdr04h6kmpqmms2lycgjkznn76l4rrjp")))

(define-public crate-eclectica-0.8 (crate (name "eclectica") (vers "0.8.5") (hash "0gqpvk2v38gi8z74kfri9x7lvwv7p7j16rdaibilns7kigaiw8ha")))

(define-public crate-eclectica-0.8 (crate (name "eclectica") (vers "0.8.6") (hash "0r032bzd8vb9jajfl829h966c8zy9wybzy3ys5mn8mmsw1mawd5h")))

