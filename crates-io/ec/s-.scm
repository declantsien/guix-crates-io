(define-module (crates-io ec s-) #:use-module (crates-io))

(define-public crate-ecs-lib-rs-0.1 (crate (name "ecs-lib-rs") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "0sh6m3ffgh6fz3fnk0w96hm6zxw6klrjq3vhyz1fjx9khni5xasf")))

(define-public crate-ecs-lib-rs-0.1 (crate (name "ecs-lib-rs") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "11caq0jxf55qpzxn1ikp2qrc2anzwmby27m6jh6mrcg8j8wcb9z5")))

(define-public crate-ecs-logger-1 (crate (name "ecs-logger") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1dndwp5fn849bfkiirkvj9pizda2p8hgqpfm3292vc2g6ym95wjk")))

(define-public crate-ecs-logger-1 (crate (name "ecs-logger") (vers "1.0.0-rc.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "01hbndnxl40bvw50kwcbdx40s5xhcf70xcxa9sj3q7fdlfdj41f7")))

(define-public crate-ecs-logger-1 (crate (name "ecs-logger") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "09skin3kw15ydq5ymp8y1qrww4l1axislx44398vfqbxr337wv5v")))

(define-public crate-ecs-logger-1 (crate (name "ecs-logger") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "04adasc5l8xga4cy8rqawzl3w4hajkig8lq1llmf7qr1npnazrb4")))

(define-public crate-ecs-rs-0.1 (crate (name "ecs-rs") (vers "0.1.0") (hash "1dkbs29s6zgmnj67wygfgvpfipcdvsmxc10rsj0w8vfyzb9dw1x1")))

(define-public crate-ecs-rs-0.1 (crate (name "ecs-rs") (vers "0.1.1") (hash "0qkwlg9zi650qh5jhx9brhkrcphs5cqwa0l1ih8yrwl4z92nyfpl")))

(define-public crate-ecs-rs-0.1 (crate (name "ecs-rs") (vers "0.1.2") (hash "07nx341hlnph9g2p22f55hiv0jj071x1b173y5d7cbqh8pxgljln")))

(define-public crate-ecs-tiny-0.1 (crate (name "ecs-tiny") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "12rdfc18b47wcljcai4102kmpshwc4dbqz8gl3ayn3671sw9w3lp")))

(define-public crate-ecs-tiny-0.1 (crate (name "ecs-tiny") (vers "0.1.1") (deps (list (crate-dep (name "ahash") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "0srmj884vxqry8bwmlhmrsdr4m37dcnan2012m1z9a499qasr4kr")))

