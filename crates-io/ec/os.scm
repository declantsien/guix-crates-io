(define-module (crates-io ec os) #:use-module (crates-io))

(define-public crate-ecos-rs-0.1 (crate (name "ecos-rs") (vers "0.1.0") (hash "07rfjqkwkagrn5hmdq60n566zll094688819rsybix48grwl23aw")))

(define-public crate-ecosystem-0.1 (crate (name "ecosystem") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)))) (hash "0vy1ryc35pblmybzlbp1bzhx15jq90zrjia89c6rpw5f50a8cpmg")))

