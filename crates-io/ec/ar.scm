(define-module (crates-io ec ar) #:use-module (crates-io))

(define-public crate-ecargo-0.1 (crate (name "ecargo") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "cargo-platform") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.27.2") (default-features #t) (kind 0)) (crate-dep (name "egui-modal") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1qlqs16r047awb8ghv4dxwz783p8lkifv9xkqfffk340j4av578j")))

