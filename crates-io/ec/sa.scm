(define-module (crates-io ec sa) #:use-module (crates-io))

(define-public crate-ecsact-0.1 (crate (name "ecsact") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "11psfvgf8my03wjmhvsz34lz5g55dmay3i8743vvchbxfj4jfzd2")))

(define-public crate-ecsact_dylib_runtime-0.1 (crate (name "ecsact_dylib_runtime") (vers "0.1.0") (deps (list (crate-dep (name "ecsact") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.78") (default-features #t) (kind 1)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "1v1hrjr81iihav6yyk04gk5qljxx38cz0zis3xc55jxlph3qzwv9")))

(define-public crate-ecsact_macro-0.1 (crate (name "ecsact_macro") (vers "0.1.0") (deps (list (crate-dep (name "ecsact") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i2fmmzn664bbqr2l8gnx3w0xk6s024hvl2a9r3sp11jj0f6d349")))

(define-public crate-ecsact_rust_codegen-0.1 (crate (name "ecsact_rust_codegen") (vers "0.1.0") (deps (list (crate-dep (name "ecsact") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ecsact_dylib_runtime") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ecsact_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "indented") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "1hsdr5r0bmylr4rn5g15i4x37bxl3c1yzysxgib81lsaypi6c9kb")))

