(define-module (crates-io ec db) #:use-module (crates-io))

(define-public crate-ecdb-0.0.1 (crate (name "ecdb") (vers "0.0.1") (hash "0dx5d5smp673jx9jhc6nf3mramacs7pc9c32clrpzqnfr3iywdqj")))

(define-public crate-ecdb-0.0.2 (crate (name "ecdb") (vers "0.0.2") (deps (list (crate-dep (name "protobuf") (req "^2.8.1") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen-pure") (req "^2.3") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "0hlsibvh2xfc49xs63b7b3phh36sifdn8afs48i1j09sn48nn9b4") (features (quote (("with-serde"))))))

