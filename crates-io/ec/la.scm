(define-module (crates-io ec la) #:use-module (crates-io))

(define-public crate-ecla-0.1 (crate (name "ecla") (vers "0.1.0") (deps (list (crate-dep (name "elog") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cn43jgkialig7ahs8giz8sc4g2w9z64qix5hnc06x4fxzilqh78")))

(define-public crate-ecla-0.1 (crate (name "ecla") (vers "0.1.1") (deps (list (crate-dep (name "elog") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qypdhs9dm1136m1hl7hm0c87a4jb07cgzwqwr655f8b0qv0b4d2")))

(define-public crate-ecla-1 (crate (name "ecla") (vers "1.0.0") (deps (list (crate-dep (name "elog") (req "^0.1") (default-features #t) (kind 0)))) (hash "04v5ia81xxhj0zq93a2nqvfklw3hdh50k2bqvnxh2q61b7irxd9p")))

(define-public crate-eclair-0.0.0 (crate (name "eclair") (vers "0.0.0") (hash "01pfza7f6l67y6q5a2nvnar67v3nyfx1a7kwkx8irjqxmcymhrd2")))

(define-public crate-eclair-builder-0.1 (crate (name "eclair-builder") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "0f1vqrb11sxcyjdqlsydiqvhla9c70zxdjywfad6vsj419yvz240")))

(define-public crate-eclair_bindings-0.0.1 (crate (name "eclair_bindings") (vers "0.0.1") (deps (list (crate-dep (name "ffi-opaque") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1pmmknff5dv1y74yrqlqlpqm810k6912byc3chydbbgawsapj80m")))

(define-public crate-eclair_bindings-0.1 (crate (name "eclair_bindings") (vers "0.1.0") (deps (list (crate-dep (name "ffi-opaque") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "19lmakmafd17kyk5ip9g5fy33kmbqjcapblfk932s3f5hb4h06sd")))

(define-public crate-eclair_bindings_derive-0.0.1 (crate (name "eclair_bindings_derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1gi1sqq3f1myfs3lxlbh19lh0aiplzx06al9n038smdizvarsgq3")))

(define-public crate-eclair_bindings_derive-0.1 (crate (name "eclair_bindings_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "09aip3cill3kj7pq0xjxajknrihjcjinhv85x57wc3c849w8zy76")))

