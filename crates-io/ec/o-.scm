(define-module (crates-io ec o-) #:use-module (crates-io))

(define-public crate-eco-rs-0.1 (crate (name "eco-rs") (vers "0.1.0") (hash "02k3a99p4wv0543z7l0c989z7djjg12168a2a15wdjdcjr3kmqv3")))

(define-public crate-eco-rs-0.1 (crate (name "eco-rs") (vers "0.1.1") (hash "19nxacfzr8rkb9hc17sy0hym8b9yydml4rqxvvfqhgnfjaql01lj")))

(define-public crate-eco-rs-0.1 (crate (name "eco-rs") (vers "0.1.2") (hash "1jk9fjgzlnl1gxc5j37npvk6nly7i4c8sxm0jgbsrs6mv62g5jyi")))

(define-public crate-eco-rs-0.1 (crate (name "eco-rs") (vers "0.1.3") (deps (list (crate-dep (name "console") (req "^0.15.8") (features (quote ("windows-console-colors"))) (default-features #t) (kind 0)))) (hash "0dfllkmc861x7gk8mzsx7gd66cqlzzxm8jjs2z59m8b25mzg7lla")))

(define-public crate-eco-rs-0.1 (crate (name "eco-rs") (vers "0.1.4") (deps (list (crate-dep (name "console") (req "^0.15.8") (features (quote ("windows-console-colors"))) (default-features #t) (kind 0)))) (hash "1bfdi45n9v50jp8k3flnbdarilcywbscibxvcfch32zc03qy27r3")))

(define-public crate-eco-rs-0.1 (crate (name "eco-rs") (vers "0.1.5") (deps (list (crate-dep (name "console") (req "^0.15.8") (features (quote ("windows-console-colors"))) (default-features #t) (kind 0)))) (hash "08ygg4by64g06vlf9qc0fdh21z7pzm75ykpp2b6qk34ik4fwrqs9")))

(define-public crate-eco-rs-0.1 (crate (name "eco-rs") (vers "0.1.6") (deps (list (crate-dep (name "console") (req "^0.15.8") (features (quote ("windows-console-colors"))) (default-features #t) (kind 0)))) (hash "17mqaryb7m3li97pnskz7hgc83rvd80w3w6qyich2j1amw4iplfp")))

