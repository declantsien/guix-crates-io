(define-module (crates-io ec e_) #:use-module (crates-io))

(define-public crate-ece_421_sam_cynthia_aditya_trees-0.1 (crate (name "ece_421_sam_cynthia_aditya_trees") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1xn5jl21dqg9pphplnbcck1xll8br6bn04i1yams8dk49rs5nsk0")))

(define-public crate-ece_421_sam_cynthia_aditya_trees-0.1 (crate (name "ece_421_sam_cynthia_aditya_trees") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0gvacan53nli524fjl6qxjz8mczcxzc2hagmq9lncypqpmif8kqs")))

