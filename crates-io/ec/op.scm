(define-module (crates-io ec op) #:use-module (crates-io))

(define-public crate-ecopay2-0.1 (crate (name "ecopay2") (vers "0.1.0") (deps (list (crate-dep (name "cosmwasm") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-vm") (req "^0.7.2") (kind 2)) (crate-dep (name "cw-storage") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "= 0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "= 1.0.103") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "= 0.5.0") (features (quote ("rust_1_30"))) (kind 0)))) (hash "0crxrxszj62hm0jzd2bsgbmhyf252dgjq7hfmgccp0jgkx5vslz4") (features (quote (("singlepass" "cosmwasm-vm/default-singlepass") ("default" "cranelift") ("cranelift" "cosmwasm-vm/default-cranelift") ("backtraces" "cosmwasm/backtraces" "cosmwasm-vm/backtraces"))))))

(define-public crate-ecopy-0.1 (crate (name "ecopy") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5") (default-features #t) (kind 0)))) (hash "1qrq9vcg3nnmblcbqinp009x6s0vrwm5d3d46xmw2awr2sqwijh8")))

