(define-module (crates-io ec fu) #:use-module (crates-io))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "1dh8szib7l9n5iq76swdbgracihzwaa41xmk51x3vkpaldjl9ffi")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "1j786vq0rjbpqj0vk6snwv16x3agq5fgsq12vymyp2fpcbf6s48w")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "15d7wrp2zvz0mwa8mwsivkb9zm72ij9bwawmzw19qbl8vgqx3fjj")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "1xr108055fgsnwgygckj3ks18l815hx81yrfrx3xbcvgibcd1z2r")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "1hsa5421884ivfd45xmhkrc7dn4hphp975prdffazkj86szfwrj7")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "1lqjs3kspksca345qlj2m6k7lx69w1cj4bm9m6s2mg4dsxf7kyy8")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "0p5msgc1ay67bklg9dzhdgmkf0k4jz8g30qjs9y2a5ddhxbsrwr1")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "0pg7s9p5ykp1cy7hp0aj0cmvfav1cc8nflddbhaky14kdnq5q50c")))

(define-public crate-ecfuzz-0.1 (crate (name "ecfuzz") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "087rbbvr7hc436d6xld5wi0a4n742kczzmm3kwwckd8hd59n75kq")))

(define-public crate-ecfuzz-0.2 (crate (name "ecfuzz") (vers "0.2.0") (deps (list (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "0x940lkb5ign9gyk46xz7vsxi3yncaxyhazd79ridd9a70raj75x")))

(define-public crate-ecfuzz-0.2 (crate (name "ecfuzz") (vers "0.2.3") (deps (list (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "05n3w2040y80kn6fm60hyphy1ppya6mkrsic1mj5cfcilqa17r7y")))

(define-public crate-ecfuzz-0.2 (crate (name "ecfuzz") (vers "0.2.4") (deps (list (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "1a513y34nj3wwzgfyvymp5fjdys12pi4jsznidr6hn4z3zsiw4pn")))

