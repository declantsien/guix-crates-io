(define-module (crates-io ec ro) #:use-module (crates-io))

(define-public crate-ecrou-0.0.0 (crate (name "ecrou") (vers "0.0.0") (hash "0rr60iz0ar0j8nlfh514drb8lwq64ikx3yp5g0gf5mzv7d61pqvi")))

(define-public crate-ecrous-0.0.1 (crate (name "ecrous") (vers "0.0.1") (hash "1m3b2hmnh7d8srkgxz66wg4l6l849z04xbdk5iy9p3acydkia3iv")))

