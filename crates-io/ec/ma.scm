(define-module (crates-io ec ma) #:use-module (crates-io))

(define-public crate-ecma-spec-0.1 (crate (name "ecma-spec") (vers "0.1.0") (hash "1j8lv3d9lsjr7ks14iha2zg3rcjahrn2qbq40w4yg2kqik70s9v4")))

(define-public crate-ecma402_traits-0.1 (crate (name "ecma402_traits") (vers "0.1.0") (hash "11a11h73qka0canayqb0wg4aspw0xdq64jg8csgr3jgbxapclkxs")))

(define-public crate-ecma402_traits-0.2 (crate (name "ecma402_traits") (vers "0.2.0") (hash "1k7vfdqggai42nvkwh1qv1y88cr8qj31q3yr85ia8nm8ikd2vilq")))

(define-public crate-ecma402_traits-1 (crate (name "ecma402_traits") (vers "1.0.0") (hash "102l6kcn8srjaqlp5khr0hb73bqfgn9ck99bng9mix7y6zjdg6p8")))

(define-public crate-ecma402_traits-1 (crate (name "ecma402_traits") (vers "1.0.1") (hash "0rg6n66pp3r2h9xkg61rpcl9wv8qsw1sdlf01swy18bq4hqa1czj")))

(define-public crate-ecma402_traits-1 (crate (name "ecma402_traits") (vers "1.0.3") (hash "0wc9jvi9r1d1kaks33x30n2pgxqdq4in9iwkkiyz0362zh6p9acx")))

(define-public crate-ecma402_traits-2 (crate (name "ecma402_traits") (vers "2.0.0") (hash "1ldi2zcycbicn55fc7nam1z6j6dqdc8k3w0gd7sdwm9hbhadxmpl")))

(define-public crate-ecma402_traits-2 (crate (name "ecma402_traits") (vers "2.0.1") (hash "021c3azcp0pdln5lx1nfxw6b8hv8d7q2q7rmg4fmkfmhlg8am2ax")))

(define-public crate-ecma402_traits-2 (crate (name "ecma402_traits") (vers "2.0.2") (hash "1yw0qhin4zsr2dlvwpl7psmkl8ih6xmzmmnxibgh9n6d2d8wv0g0")))

(define-public crate-ecma402_traits-2 (crate (name "ecma402_traits") (vers "2.0.3") (hash "0jz0zyzjgy0gaf6xms2ncjihsj8n14bfnnwvb4cl3gbr0hzaqimb")))

(define-public crate-ecma402_traits-3 (crate (name "ecma402_traits") (vers "3.0.0") (hash "1jgbajxl6g2hciafpqfs9ywqwi31v3hx4njnwrwjvix66j9flvjk")))

(define-public crate-ecma402_traits-4 (crate (name "ecma402_traits") (vers "4.0.0") (hash "1r04hfd8ff4753l4d826688hmnqb4fz67lnw2ldc8nb2l972l31b")))

(define-public crate-ecma402_traits-4 (crate (name "ecma402_traits") (vers "4.2.0") (hash "081jwj359flfj1gqk0djhq5jgriy3jhgqwvxxjjcqv9jqkknsg0n")))

(define-public crate-ecma402_traits-4 (crate (name "ecma402_traits") (vers "4.2.1") (hash "1amind48snkqf47rib27xiad2abfmcl107hjibfafqibbmwdc087")))

(define-public crate-ecma402_traits-4 (crate (name "ecma402_traits") (vers "4.2.2") (hash "1y5zfhc2y1pcllmp6hwy0h3z5yxm2xclzqqn461rh00dlrfyvgf0")))

(define-public crate-ecma402_traits-4 (crate (name "ecma402_traits") (vers "4.2.3") (hash "0pm1ccyvrn4cgfk383ff60mw9zlhynqb7wg2p3s91y50cy6z3y8w")))

(define-public crate-ecma402_traits-5 (crate (name "ecma402_traits") (vers "5.0.0") (hash "18vkcvmxk92xaq7dnx8y3ylg3ihslnpxfl6jmjl3nj1jgb0d5dxx")))

(define-public crate-ecma_regex-0.0.1 (crate (name "ecma_regex") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "libregexp-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 2)))) (hash "0fnxrhyfh2xzi1jwclijiis3nfcxnc67zqynjn022rlywjlrfyfp")))

(define-public crate-ecma_regex-0.0.2 (crate (name "ecma_regex") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "libregexp-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rs_regex") (req "^1.7.1") (default-features #t) (kind 2) (package "regex")))) (hash "0i577cs07sxba56wdy1dfj1yqmnqj7jaqn81ivwxd21n3qgi2nhh")))

(define-public crate-ecmascript-0.0.1 (crate (name "ecmascript") (vers "0.0.1") (deps (list (crate-dep (name "combine") (req "^3.3.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0c5l5xmfkbc6dpip3kqyw3afznn6lb8g29gq3rrkgkf2s74243hz")))

(define-public crate-ecmascript-0.1 (crate (name "ecmascript") (vers "0.1.0") (deps (list (crate-dep (name "combine") (req "^3.3.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0557rnvxb9ay4gg61mqk68fd21kbcvlyjjzqj3gycd8bhsskinj2")))

(define-public crate-ecmascript-0.2 (crate (name "ecmascript") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^3.3.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zbdc9rq754ffhzrrb4hdmi484cvkiqg4qnfa3la6sb0xmnbbq8i")))

