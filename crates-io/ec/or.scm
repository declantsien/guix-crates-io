(define-module (crates-io ec or) #:use-module (crates-io))

(define-public crate-ecore_rs-0.1 (crate (name "ecore_rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "safe_index") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^2.3") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.10") (default-features #t) (kind 0)))) (hash "1jpmhhv470kq1zb2biba20lsqpjm1irab9ck3k35lypawzpzqwa5")))

