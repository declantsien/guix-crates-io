(define-module (crates-io ec -c) #:use-module (crates-io))

(define-public crate-ec-client-0.1 (crate (name "ec-client") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.90") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0sqkdxdn3xhggbch1c31q0g0ks8qd2n8brxqclfswy17yq7ivb3w")))

(define-public crate-ec-cryptography-0.1 (crate (name "ec-cryptography") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "066y9dkvcwhixsr1way1s6224w7w23sv3yd0bkmfb7sjl90ahf0a") (yanked #t)))

