(define-module (crates-io ec ha) #:use-module (crates-io))

(define-public crate-echannel-0.0.1 (crate (name "echannel") (vers "0.0.1") (deps (list (crate-dep (name "async-channel") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "14ybswpbfzy60yrqc47vwmgsj6fzfp4g6r8sm97b7nx7s601rr4v")))

(define-public crate-echannel-0.0.2 (crate (name "echannel") (vers "0.0.2") (deps (list (crate-dep (name "async-channel") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0dk3q9b1kx8vasgixpp70z93yp44n4zvmnlkia2rkdrm5dcc1k4y")))

(define-public crate-echannel-0.0.3 (crate (name "echannel") (vers "0.0.3") (deps (list (crate-dep (name "async-channel") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0840nmjlx221lzidb971fwx5vh3zfca6ka2ygpshx00h3xrjnfj2")))

(define-public crate-echarts-0.1 (crate (name "echarts") (vers "0.1.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.61") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("Element" "HtmlCanvasElement"))) (default-features #t) (kind 0)))) (hash "0jqkphl62axk3zh69h5h09lyghg7q80441xa04vcwviyx7cqjigs")))

(define-public crate-echarts-0.1 (crate (name "echarts") (vers "0.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("Element" "HtmlCanvasElement"))) (default-features #t) (kind 0)))) (hash "0lfjknwv6n7np04jq5vsjly32mm0j15kzbxgvblbjqvsv5x8pk51")))

(define-public crate-echarts-0.1 (crate (name "echarts") (vers "0.1.2") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("Element" "HtmlCanvasElement"))) (default-features #t) (kind 0)))) (hash "06jzfmlkga2jki191j2i26h14qfd19vn71y3i9p3d7ixg0a7d0ns")))

