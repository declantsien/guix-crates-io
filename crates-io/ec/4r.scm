(define-module (crates-io ec #{4r}#) #:use-module (crates-io))

(define-public crate-ec4rs-1 (crate (name "ec4rs") (vers "1.0.0-rc.1") (hash "198xxlpqiw9qqrq0gy5wchp3kv0fdwpxvnsnlv482k3lc6qwsm82") (rust-version "1.59")))

(define-public crate-ec4rs-1 (crate (name "ec4rs") (vers "1.0.0") (hash "1iqiyszz8my3618sa9v8y03nl89marihyy104g0gspiinmczrzmk") (rust-version "1.59")))

(define-public crate-ec4rs-1 (crate (name "ec4rs") (vers "1.0.1") (hash "1srl1c8dl4rad07rrmx4a05bwnvi6835aavsc5h2jvjdb1n0ym6v") (rust-version "1.56")))

(define-public crate-ec4rs-1 (crate (name "ec4rs") (vers "1.0.2") (hash "0dwlw73v0lyschcfx94fj196jdgn8311rp6gb3y1zajqshcpiryb") (rust-version "1.56")))

(define-public crate-ec4rs-1 (crate (name "ec4rs") (vers "1.1.0") (deps (list (crate-dep (name "language-tags") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)))) (hash "0b4njh898zcnqvlhpar30s1iv5l5zysqjri064g2474spmmycdyc") (features (quote (("allow-empty-values")))) (rust-version "1.56")))

