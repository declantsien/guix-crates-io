(define-module (crates-io ec cl) #:use-module (crates-io))

(define-public crate-eccles-0.1 (crate (name "eccles") (vers "0.1.2") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0dbxpbk7v90zrc8jwjs7l2xhmwcf70xwdaa02smm0md19zibvv7x")))

(define-public crate-eccles-0.1 (crate (name "eccles") (vers "0.1.3") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "06h67zfjhwxc616vydxskhsdvxdip44322zyhsfr5qwrzpdkxmnx")))

(define-public crate-eccles-0.2 (crate (name "eccles") (vers "0.2.0") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1v0kgawk16s464ds75dkwrzmz67qnq98r2x88b71x7mb7pf04qzl")))

(define-public crate-eccles-0.2 (crate (name "eccles") (vers "0.2.1") (deps (list (crate-dep (name "time") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0zrd7msvbj2nzq4wi5ci5nqk0z5zyafjd674wk8ikkhh8sh6ns7s") (features (quote (("default" "time"))))))

(define-public crate-eccles-0.2 (crate (name "eccles") (vers "0.2.2") (deps (list (crate-dep (name "time") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "02w5mmzkm7lqazk0wrnb3qzfzkabjvk53xq46pmblfz7lv8h1p7j") (features (quote (("default" "time"))))))

(define-public crate-eccles-0.2 (crate (name "eccles") (vers "0.2.3") (deps (list (crate-dep (name "clock_ticks") (req "*") (default-features #t) (kind 0)))) (hash "0ay8v8yn128k9g3yly2shn45hsjagn9z0q8jv638rdzhd8idhd83")))

