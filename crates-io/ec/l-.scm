(define-module (crates-io ec l-) #:use-module (crates-io))

(define-public crate-ecl-sys-0.1 (crate (name "ecl-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.0") (default-features #t) (kind 1)))) (hash "0x82ryp2qa3qxndc4z9ncy7kd03ad25bp1gs413dwjadgir7j99q") (links "ecl")))

