(define-module (crates-io ec oj) #:use-module (crates-io))

(define-public crate-ecoji-1 (crate (name "ecoji") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.31.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7.21") (default-features #t) (kind 1)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "02yh2b9bapcmbclvsiisn7jd48khyvb2khg36bm9gk4993ld0idl") (features (quote (("build-binary" "clap"))))))

