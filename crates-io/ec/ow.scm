(define-module (crates-io ec ow) #:use-module (crates-io))

(define-public crate-ecow-0.1 (crate (name "ecow") (vers "0.1.0") (deps (list (crate-dep (name "loom") (req "^0.5") (optional #t) (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1nii9315lv2z7hlizqy13j94h0lsv5mnw6mif9wpqfgwzd02h3p6")))

(define-public crate-ecow-0.1 (crate (name "ecow") (vers "0.1.1") (deps (list (crate-dep (name "loom") (req "^0.5") (optional #t) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "051s4hqh88nm7r26nih9sbp34x4a6nsi68v5r119lkf54lchbif5")))

(define-public crate-ecow-0.1 (crate (name "ecow") (vers "0.1.2") (deps (list (crate-dep (name "loom") (req "^0.5") (optional #t) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1s18cxys3n354w3qc9mvz80yyrgbn2ixp0h60czxyvngag89068x") (features (quote (("std") ("default" "std"))))))

(define-public crate-ecow-0.2 (crate (name "ecow") (vers "0.2.0") (deps (list (crate-dep (name "loom") (req "^0.5") (optional #t) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "00if0126540hlakir3p9655m8pbq55d8s6lxv8qn8wnskhzmxsp6") (features (quote (("std") ("default" "std"))))))

(define-public crate-ecow-0.2 (crate (name "ecow") (vers "0.2.1") (deps (list (crate-dep (name "loom") (req "^0.7") (optional #t) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "157pv7xhiffamnap32w0n9nkmr11gxf6k3a6c1ggyhkwf8q1m8yv") (features (quote (("std") ("default" "std"))))))

(define-public crate-ecow-0.2 (crate (name "ecow") (vers "0.2.2") (deps (list (crate-dep (name "loom") (req "^0.7") (optional #t) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0jvg2jrjgczy05mbsnirqqh3rxghxbdbwkbc18cj71lq10bvpgsl") (features (quote (("std") ("default" "std"))))))

