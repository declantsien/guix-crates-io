(define-module (crates-io bd di) #:use-module (crates-io))

(define-public crate-bddisasm-0.1 (crate (name "bddisasm") (vers "0.1.0") (deps (list (crate-dep (name "bddisasm-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xmpbx9arfx4ssxs0ji6pyxvv2hscz8mzbbnk2bc941vd85w7s1a")))

(define-public crate-bddisasm-0.2 (crate (name "bddisasm") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bddisasm-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 2)))) (hash "1rdykkq5n5nihwv48ng6s4c9940lcs51d87wi3z5mm1br45mwc3s") (features (quote (("std"))))))

(define-public crate-bddisasm-0.2 (crate (name "bddisasm") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bddisasm-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 2)))) (hash "1fn8gcw67c5bcw9j84nbvps1hpkpy35i6z16vyjki0l72gl4i66k") (features (quote (("std"))))))

(define-public crate-bddisasm-0.3 (crate (name "bddisasm") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bddisasm-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 2)))) (hash "1an2i14azblmlddy5iafqh43i4ir8f0ybxjmandghyx89z2nzgjw") (features (quote (("std"))))))

(define-public crate-bddisasm-0.2 (crate (name "bddisasm") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "bddisasm-sys") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 2)))) (hash "0qrlibqnvcifsq1w8cv1167pwpq2195ydr5xb1l51lkl6gdbncal") (features (quote (("std")))) (yanked #t)))

(define-public crate-bddisasm-0.4 (crate (name "bddisasm") (vers "0.4.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "bddisasm-sys") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 2)))) (hash "17f8khdw4hnbjfbq39rg6szdgazz0d60b7bj2nysxw406jxqm193") (features (quote (("std"))))))

(define-public crate-bddisasm-sys-0.1 (crate (name "bddisasm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)))) (hash "0vlx20z6jzq8qws3gkd28hnyvmv68q3hcwb1pql8h382bb3yna3a") (links "bddisasm")))

(define-public crate-bddisasm-sys-0.2 (crate (name "bddisasm-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0fghld48g8i67g3dbdqb8xr7n43b42lhz1xchgxv68j1ch80imjl") (links "bddisasm")))

(define-public crate-bddisasm-sys-0.2 (crate (name "bddisasm-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0zz1vdxq80nf4177ll64hdar6v2m21d8ncds96w1lwsvgjlfrsxv") (links "bddisasm")))

(define-public crate-bddisasm-sys-0.3 (crate (name "bddisasm-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1c6172mbf5j55jpj8750p2nq1ip2qrvj8wd3xbg4z6j7jmkdpi3z") (links "bddisasm")))

(define-public crate-bddisasm-sys-0.2 (crate (name "bddisasm-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.62.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1f9ignfbxgd6s73jmnh17zg07wr8hm3vvqldfiyr181ddn0hh7kb") (yanked #t) (links "bddisasm")))

(define-public crate-bddisasm-sys-0.4 (crate (name "bddisasm-sys") (vers "0.4.2") (deps (list (crate-dep (name "bindgen") (req "^0.62.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "16q9pdvml5snqika2l4z9abx9dizckzhfykqlqvlpyvshlg3l7vq") (links "bddisasm")))

