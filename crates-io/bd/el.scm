(define-module (crates-io bd el) #:use-module (crates-io))

(define-public crate-bdel-0.1 (crate (name "bdel") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1vqr3h2r8py7dp6bfy3cmnmdzgc7ynmfwai31yvkm1liahfma5jn")))

