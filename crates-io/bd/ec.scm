(define-module (crates-io bd ec) #:use-module (crates-io))

(define-public crate-bdecode-0.1 (crate (name "bdecode") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "criterion-cycles-per-byte") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 2)))) (hash "1gpr9i069pjz4q1h3wcl6b80lpcpma0jy693yr2wmav7zvxvyfdr")))

