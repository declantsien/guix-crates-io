(define-module (crates-io bd as) #:use-module (crates-io))

(define-public crate-bdasm-0.5 (crate (name "bdasm") (vers "0.5.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (features (quote ("chrono"))) (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "cpclib-asm") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "cpclib-disc") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.27") (default-features #t) (kind 0)))) (hash "0mq9j613mnaisrnvpqcjlxxyg4n7r8vcm84dp3hy39mhk2py20xy")))

