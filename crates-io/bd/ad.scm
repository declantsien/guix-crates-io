(define-module (crates-io bd ad) #:use-module (crates-io))

(define-public crate-bdaddr-0.1 (crate (name "bdaddr") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1crfyjbg0pxw2r27qrf22xijhlj3gap1l6dkjr48v67jarn6jkrl") (features (quote (("matches" "generic-array" "aes") ("default"))))))

(define-public crate-bdaddr-0.1 (crate (name "bdaddr") (vers "0.1.1") (deps (list (crate-dep (name "aes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d4drab628yjip9307yvafclsccklyrlgvzi4bvl6xwcl13si30l") (features (quote (("matches" "aes") ("default"))))))

(define-public crate-bdaddr-0.1 (crate (name "bdaddr") (vers "0.1.2") (deps (list (crate-dep (name "aes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0js6k81fblinfb59v5ax7r0549wxnhcnvl96pajlkprm3660amg4") (features (quote (("matches" "aes") ("default"))))))

(define-public crate-bdaddr-0.2 (crate (name "bdaddr") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "aes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d992h1ciy6sh0sks0zsk8wigl41bn0nqm4l138yyi7zlpfvykbq") (features (quote (("matches" "aes") ("default")))) (rust-version "1.56")))

(define-public crate-bdaddr-0.2 (crate (name "bdaddr") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "aes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "13kj2rw6hfpyq4wg4b88811bgvawb6w5lp51x9z8sghigz85qc60") (features (quote (("matches" "aes") ("default")))) (rust-version "1.56")))

(define-public crate-bdaddr-0.2 (crate (name "bdaddr") (vers "0.2.0-alpha.3") (deps (list (crate-dep (name "aes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xzxcr1526sr8l2qbdg1ikch61fcjz55m88mwm3qwwnfy4ss3rhk") (features (quote (("matches" "aes") ("default")))) (rust-version "1.56")))

(define-public crate-bdaddr-0.2 (crate (name "bdaddr") (vers "0.2.0-alpha.4") (deps (list (crate-dep (name "aes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g7xsn1lmppy52vcn9cxkbp67m47kh5g074s1r3mpv9r6lk2m953") (features (quote (("matches" "aes") ("default")))) (rust-version "1.56")))

