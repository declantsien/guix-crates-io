(define-module (crates-io bd rk) #:use-module (crates-io))

(define-public crate-bdrk_geometry-0.0.1 (crate (name "bdrk_geometry") (vers "0.0.1") (hash "13g35j5r56i438gcfn0zrj5a0kxqly3l2q6lya161f10z353m3is")))

(define-public crate-bdrk_geometry-0.0.2 (crate (name "bdrk_geometry") (vers "0.0.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0pw9w9v0qc01fc8alcsd2wxjs3v0gvk49iz4j87r35wp97wdvi0i")))

(define-public crate-bdrk_geometry-0.0.3 (crate (name "bdrk_geometry") (vers "0.0.3") (hash "0037632clqgh1y7qyi5fvnrhpjqms4s4nj9y10lc7hgwi8qy6py0")))

(define-public crate-bdrk_geometry-0.0.4 (crate (name "bdrk_geometry") (vers "0.0.4") (hash "0hg9sp9gw3y3mwxn1ydvqim2b81szw1glyc4ikqk88fi76zilpll")))

