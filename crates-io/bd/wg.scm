(define-module (crates-io bd wg) #:use-module (crates-io))

(define-public crate-bdwgc-alloc-0.1 (crate (name "bdwgc-alloc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cvwbhdrdbig3m8z93vxvkg4pjvvikb29866jdd2iscmmhldvqva")))

(define-public crate-bdwgc-alloc-0.1 (crate (name "bdwgc-alloc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "177wjp86f3w8rn0807i0b730yycif21i5y1z7h9pfpkbsgvq96wg")))

(define-public crate-bdwgc-alloc-0.2 (crate (name "bdwgc-alloc") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y4pl1kv8drd52g3j1gbzg8sd9kspwc7cbpjmgnkbh57libvklig")))

(define-public crate-bdwgc-alloc-0.2 (crate (name "bdwgc-alloc") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1aqnwfm08ifn95yjgcjv58gqgbky7ggn4fnawp3gjksw72s40866")))

(define-public crate-bdwgc-alloc-0.3 (crate (name "bdwgc-alloc") (vers "0.3.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08jqsjkz5dzp5vgc35zwblrznvpmxwnf40s8d7w7qs24bg4fp009")))

(define-public crate-bdwgc-alloc-0.4 (crate (name "bdwgc-alloc") (vers "0.4.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ahb6p0m6vh5fa520hrgjd55gbqdzcsk8dy518dl55f7jnncfgpp")))

(define-public crate-bdwgc-alloc-0.4 (crate (name "bdwgc-alloc") (vers "0.4.1") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nz3zcy6czwmrgicnbnbwpxkfdi4wsia7c0sy7vszwg8dxg00qjm")))

(define-public crate-bdwgc-alloc-0.4 (crate (name "bdwgc-alloc") (vers "0.4.2") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i2fpslfpb45fkjvvdjcfnqban40qhrswn8591z8yd8d651k70gv")))

(define-public crate-bdwgc-alloc-0.5 (crate (name "bdwgc-alloc") (vers "0.5.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14lr8l14pbmd3vbyb790x6dc51m9llx958hg5lgin1finjsdla1s") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.5 (crate (name "bdwgc-alloc") (vers "0.5.1") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ljq6may5j3wiv16qydb516pqqx16gyj5hawrcl9z3k5cjq8dfwg") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.5 (crate (name "bdwgc-alloc") (vers "0.5.2") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gmf77i1xvyjdwwcx3vni7brxg5zd1iv24baw6fjja8zdm3xffr1") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "095af1rng8zz04wff4fgyx7s04in7na7yyx0d1rhxxivn1xkivff") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.1") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "004gdvg7sxr16fprg0xlqmv2i79qc7s3d9m1zp1f7drsqq1rrlw8") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.2") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a0rkndwqcarq6w7j6cf4cg37llcznm74cslzah9wagykijqsjng") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.3") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "01bisfixcr33ljm8aldymmwjc84pcyzmjbrv181jnri4f0273cj6") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.4") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "024c9hl8imvqx5dyxcw3bpbc9fqi7bwzxhv814kvk00zm3m5cli6") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.5") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p76bg1v0nkdgjljzzpmrk7rmi4nr9kghwnsgh5fqz1igpwpb6fy") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.6") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f21wg8cplczwv08r3n4sljnmk8r3sflm1v4mk5j1rnyfh8bfdyb") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.7") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fd7ra2d6knrlf8qzgadgkwv7zc7pm08c0s9k2k77whf91ijylsp") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-alloc-0.6 (crate (name "bdwgc-alloc") (vers "0.6.8") (deps (list (crate-dep (name "autotools") (req "^0.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s42wp351fzw1csknql0qbisqjlw8gnhdajz8qkgl26gnm4phmw9") (features (quote (("default" "autotools"))))))

(define-public crate-bdwgc-sys-0.1 (crate (name "bdwgc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.48") (default-features #t) (kind 1)))) (hash "1xyq98gnc21yd7s9psmnlppn4b8a9dibhag4qkfqqga2cn4khk3g") (yanked #t)))

(define-public crate-bdwgc-sys-0.1 (crate (name "bdwgc-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req ">=0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req ">=0.1.48") (default-features #t) (kind 1)))) (hash "0l0hdkmprcczwwz48qs3xny13f50fllckzqz4mpvgqldkv2bpqzc")))

