(define-module (crates-io pw nr) #:use-module (crates-io))

(define-public crate-pwnr-0.1 (crate (name "pwnr") (vers "0.1.0") (deps (list (crate-dep (name "checksec") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "ropr") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "05c8cigfrd622hy2665xlmmdl1z041jf2gjkimk82m27qgzfqb6z")))

(define-public crate-pwnr-0.1 (crate (name "pwnr") (vers "0.1.1") (deps (list (crate-dep (name "checksec") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "ropr") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "03mfcrkz8gas958qkw469j5ddn6r07vpl7jmdfmf79cf508w2b23")))

(define-public crate-pwnr-0.1 (crate (name "pwnr") (vers "0.1.2") (deps (list (crate-dep (name "checksec") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "ropr") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0fsivz2q9cjdd27n78zzc3mf4jbxyiqsdsnk2p3ads6ib2g9k2iy")))

