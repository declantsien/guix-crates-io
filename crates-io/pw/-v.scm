(define-module (crates-io pw -v) #:use-module (crates-io))

(define-public crate-pw-volume-0.4 (crate (name "pw-volume") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "0f2ry45356lfp4ykiinzc1c3pfqp0j6ckmk731mp7mr9sh4nm4d5")))

