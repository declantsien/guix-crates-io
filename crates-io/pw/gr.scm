(define-module (crates-io pw gr) #:use-module (crates-io))

(define-public crate-pwgraster-0.1 (crate (name "pwgraster") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "0bxwkwg6c20x41fdafpc4qdg0bdvivyqw592nqvwbybxfidvrrx1") (features (quote (("default"))))))

