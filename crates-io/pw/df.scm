(define-module (crates-io pw df) #:use-module (crates-io))

(define-public crate-pwdf-0.1 (crate (name "pwdf") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (features (quote ("backtrace"))) (default-features #t) (kind 0)))) (hash "1h6npb3hrsx1zayfvkli21jy8d9j0gh118xj8nfgj761jmc9iyjl")))

(define-public crate-pwdf-0.1 (crate (name "pwdf") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (features (quote ("backtrace"))) (default-features #t) (kind 0)))) (hash "1lp0291x37yyalwnimr5vid2xrhj2wipwwsab8y13m2aiwkl7dfr")))

