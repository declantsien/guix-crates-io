(define-module (crates-io pw wi) #:use-module (crates-io))

(define-public crate-pwwizard-0.1 (crate (name "pwwizard") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "09j5bn2y9jwn6cfn4k6g9yh923zcy7dkzcg0vblrx51hzb04rw87")))

(define-public crate-pwwizard-0.1 (crate (name "pwwizard") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0bz3s5mqzx7k4pwvj2in37slrh4zq4nbdl0fxp2cq94djhhgarxb")))

(define-public crate-pwwizard-0.1 (crate (name "pwwizard") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "divrem") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ib6llc1pcr6aj4nj9jv70arfd8296xnkqdh101pw2zyh166vaq4")))

