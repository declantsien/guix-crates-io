(define-module (crates-io pw gn) #:use-module (crates-io))

(define-public crate-pwgn-0.1 (crate (name "pwgn") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "password-gen") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1rxxbw1pp7sa0a95ygfs0zqrnf45l67kkglx8jdggasbvi6zc2wa")))

