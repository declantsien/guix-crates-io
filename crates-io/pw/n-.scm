(define-module (crates-io pw n-) #:use-module (crates-io))

(define-public crate-pwn-helper-0.1 (crate (name "pwn-helper") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pwn-helper-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0h3k274h6mqpminc4k2gi2y3c9aqvgb7a8kcjbjzhxsq9419l6x3")))

(define-public crate-pwn-helper-macros-0.1 (crate (name "pwn-helper-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.84") (default-features #t) (kind 0)))) (hash "06xfja8xy64l6wwqi09k9ciw9f9ykaa7fswmynabk0dq1jjm00ar")))

