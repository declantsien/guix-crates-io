(define-module (crates-io pw r_) #:use-module (crates-io))

(define-public crate-pwr_airly-0.0.1 (crate (name "pwr_airly") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "06avqj3bfgq017r7yhjqhnvxk3rrsj7dbd8z1xnnqykkkfdrzsgf")))

(define-public crate-pwr_airly-0.1 (crate (name "pwr_airly") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gz79c61ys0i8dij7y9519g3c5pl1pbjzwkfiy6nwlgrwfp9j6rg")))

(define-public crate-pwr_airly-0.2 (crate (name "pwr_airly") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "1bdj5gsykiykkv0jmy9xa6n043h5znmclcpmfx54ircw4adqdjf2")))

