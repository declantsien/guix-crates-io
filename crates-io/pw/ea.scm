(define-module (crates-io pw ea) #:use-module (crates-io))

(define-public crate-pwease-0.1 (crate (name "pwease") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)))) (hash "10rbv7li7djl0zsh52gvw2614sn78kq1fgry0wfj6yr8x89wpn7n") (yanked #t)))

