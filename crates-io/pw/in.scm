(define-module (crates-io pw in) #:use-module (crates-io))

(define-public crate-pwin-0.1 (crate (name "pwin") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0mi73i59lamsvfs4qar73sdnns9jcbrq143ia9a5ww0agmznp0k2") (yanked #t)))

(define-public crate-pwin-0.1 (crate (name "pwin") (vers "0.1.1") (deps (list (crate-dep (name "termion") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1s7l2f9m5ch20a227xxgxf0s465971ky0899z170s1qdvyvpxh73") (yanked #t)))

(define-public crate-pwin-0.1 (crate (name "pwin") (vers "0.1.2") (deps (list (crate-dep (name "termion") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1y3y5gv4kpdah66wdnnan6br9sx5dv8sv1cn653nc3ixg5isdjm2") (yanked #t)))

(define-public crate-pwin-0.1 (crate (name "pwin") (vers "0.1.3") (deps (list (crate-dep (name "termion") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "19d9skwx3zkxshnh540ya44mg0hz5dmjalflv257zjswlmdcf6ah")))

(define-public crate-pwin-1 (crate (name "pwin") (vers "1.0.0") (deps (list (crate-dep (name "termion") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1qn207p9sad8qz085hdkqsjgdh754s8nmfjbz1zf07avh3r7q12c")))

(define-public crate-pwin-1 (crate (name "pwin") (vers "1.0.1") (deps (list (crate-dep (name "termion") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "15ccim2acyy14adakqld021maqp1w4bsdd8ypqz73g0vdsx1blkg")))

(define-public crate-pwintln-0.1 (crate (name "pwintln") (vers "0.1.3") (deps (list (crate-dep (name "backtrace") (req "^0.3.55") (default-features #t) (kind 0)) (crate-dep (name "cstr") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "goblin") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)) (crate-dep (name "owoify") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "05d6wg4i97cmnz0mb6c0zpq0xijww0ig090b5a9wnnw3fa8w38q2")))

