(define-module (crates-io pw fu) #:use-module (crates-io))

(define-public crate-pwfu-0.2 (crate (name "pwfu") (vers "0.2.0") (hash "09lcfbsyqckrv1ags59q3xya1lakzpz72qsvy0vac62hd42a48yz")))

(define-public crate-pwfuzz-rs-0.1 (crate (name "pwfuzz-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "01c0l3aw3z647hzp1gj7g2k1dc5922kvm4vi5p47dpiwidjws39x")))

(define-public crate-pwfuzz-rs-0.1 (crate (name "pwfuzz-rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xh0w6jhn4v43arb045pna6bcsah8dyq9mv7xbp4w7sjc04shxay")))

(define-public crate-pwfuzz-rs-0.2 (crate (name "pwfuzz-rs") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "06fjc5g89v0q87fv2v6gcm8ksq14b8v8pd5n4qnbj0l24dkn9rbc")))

