(define-module (crates-io pw d1) #:use-module (crates-io))

(define-public crate-pwd123-0.1 (crate (name "pwd123") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1r23vn1clwdl1k6vvax1y1xffamdb4l1335yn5ifw14mq5rh545c")))

(define-public crate-pwd123-0.1 (crate (name "pwd123") (vers "0.1.1") (deps (list (crate-dep (name "blake3") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1nr98cmbf598p9h2102vac14klks0ssyhzjzf5zi49fnfkvlr1ns")))

