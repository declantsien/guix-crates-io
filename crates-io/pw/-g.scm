(define-module (crates-io pw -g) #:use-module (crates-io))

(define-public crate-pw-gen-0.1 (crate (name "pw-gen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.30") (default-features #t) (kind 0)) (crate-dep (name "tracing-test") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1vv8znv88jh06qbb34894hx516k5d92v4zjzyx4gn8p38ci464cv")))

(define-public crate-pw-gen-0.1 (crate (name "pw-gen") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.30") (default-features #t) (kind 0)) (crate-dep (name "tracing-test") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "17v9l804q10cym5ikz55ap6g1scgkw2i1ziq5w1gbjzb6pyn4w8n")))

(define-public crate-pw-gen-0.1 (crate (name "pw-gen") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.30") (default-features #t) (kind 0)) (crate-dep (name "tracing-test") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0ww8y2glq6id0piynh3zy9g0c6ghmg1rfb61hhmn5d23sra865fx")))

