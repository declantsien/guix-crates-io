(define-module (crates-io pw or) #:use-module (crates-io))

(define-public crate-pword-0.1 (crate (name "pword") (vers "0.1.0") (deps (list (crate-dep (name "chbs") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "08zdmy8zj4rlh4dkmrg9bl58mx485m35yr4ljmyy18fa0wc0x59k")))

(define-public crate-pword-0.1 (crate (name "pword") (vers "0.1.1") (deps (list (crate-dep (name "chbs") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "01ms3q156lpy2qg2sj3xqfap197hj23qiljdj4hr7fd2d3sfdwz3")))

(define-public crate-pword-0.1 (crate (name "pword") (vers "0.1.2") (deps (list (crate-dep (name "chbs") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1aafvkzda42mgrhljl0wi6dk9z1slz03lr181j5nmgkmjyv14dg4")))

(define-public crate-pword-0.1 (crate (name "pword") (vers "0.1.3") (deps (list (crate-dep (name "chbs") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1fmmjrq15jskfrpmzdh7i14hzybi5xcb1w4lm4lkq5xmb5ixzjif")))

