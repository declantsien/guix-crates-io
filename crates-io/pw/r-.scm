(define-module (crates-io pw r-) #:use-module (crates-io))

(define-public crate-pwr-hd44780-0.0.1 (crate (name "pwr-hd44780") (vers "0.0.1") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "03gmmidl0vk8hmiw16ikf2w5j1fbp346jxsza55arizgmdkvai7b") (yanked #t)))

(define-public crate-pwr-hd44780-0.0.2 (crate (name "pwr-hd44780") (vers "0.0.2") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yappcgk3zxyyqkd0nqdqjn0k9pdd8jabprxprgqpnkmv68d5y8s")))

(define-public crate-pwr-hd44780-0.0.3 (crate (name "pwr-hd44780") (vers "0.0.3") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mfsacsln40pmiqazqv702fv24s873if5hj08vnbz4l17f3l8v8m")))

(define-public crate-pwr-hd44780-0.0.4 (crate (name "pwr-hd44780") (vers "0.0.4") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v5wf1bcd0wr38xyy1vn52ifl2a9kwpzgahyfkhc2qvq79g1nmcm")))

(define-public crate-pwr-hd44780-0.0.5 (crate (name "pwr-hd44780") (vers "0.0.5") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "19sv092gyi9wnxbidxjmy1f08whhcq10w1gw15vg3qs4mp6fqfrm")))

(define-public crate-pwr-hd44780-0.1 (crate (name "pwr-hd44780") (vers "0.1.0") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1223pfn61gw1dixdh584ljk9lh1qzcwvmdagx0s9mhjvar4lcf5b")))

(define-public crate-pwr-hd44780-0.1 (crate (name "pwr-hd44780") (vers "0.1.1") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "15zp562zaci8xkgypac4z3k3ymqrf2w4g63yl7xhr1gzfmf26x3k")))

(define-public crate-pwr-hd44780-0.1 (crate (name "pwr-hd44780") (vers "0.1.2") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0082kwh4v12yd8kcnizwhjkbpcp9aic37wk3wkz4qkdxa4mdd1wb")))

(define-public crate-pwr-hd44780-0.1 (crate (name "pwr-hd44780") (vers "0.1.3") (deps (list (crate-dep (name "i2cdev") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kzpzn379d6caq0l1s33vhagmlskxbyv36zpd978rg3vjilwzz79")))

(define-public crate-pwr-rs-0.1 (crate (name "pwr-rs") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.25.0") (features (quote ("rand" "recovery"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1rhlwd2fq7fp4mzsav196jbyiwk95anxdfv1kgihlikgcw1l12sy") (yanked #t) (rust-version "1.63.0")))

(define-public crate-pwr-rs-0.1 (crate (name "pwr-rs") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.25.0") (features (quote ("rand" "recovery"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1iz8vkf61ldzbg3bmv7y2vl1nwgi0k3j7i765db0wzhg95kph6xx") (rust-version "1.63.0")))

