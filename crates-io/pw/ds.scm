(define-module (crates-io pw ds) #:use-module (crates-io))

(define-public crate-pwds-0.1 (crate (name "pwds") (vers "0.1.0") (hash "1bgwchj7x8i6jna3inn85kwnbg4v7gvibbjkcyg70h33zmvnpnrn")))

(define-public crate-pwds-0.2 (crate (name "pwds") (vers "0.2.0") (hash "1mjgjlv8v1d3smld0d79n1zipn93f74hwax82bb1ih5xih5q9vg1")))

