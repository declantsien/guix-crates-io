(define-module (crates-io pw nb) #:use-module (crates-io))

(define-public crate-pwnboard-rs-1 (crate (name "pwnboard-rs") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("serde_json" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "11007gvdahrxrnjbpffxprq1md7nikv9rw1pbdz3f7j17dza4f7f")))

(define-public crate-pwnboard-rs-1 (crate (name "pwnboard-rs") (vers "1.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("serde_json" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0vwdy9gcnhb3b59hcfxgrjl0m3q3q4drq36iz64plz3lavx3pqpm")))

