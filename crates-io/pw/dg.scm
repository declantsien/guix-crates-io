(define-module (crates-io pw dg) #:use-module (crates-io))

(define-public crate-pwdg-0.1 (crate (name "pwdg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1iln8rkj22s5pydrak7rn75p2mhz0qkb3dpcwl7hhlmrv5ib41fm")))

(define-public crate-pwdgen-1 (crate (name "pwdgen") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4") (features (quote ("colors"))) (kind 0)))) (hash "149aliqsq9d2l1kxsim6zh37bl6wlhw4wj2wi1wssgi7cjsyvaim") (yanked #t)))

(define-public crate-pwdgen-1 (crate (name "pwdgen") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4") (features (quote ("colors"))) (kind 0)))) (hash "0ynaflsdl3b0qjjixyrisw4b7dffd2kq3islaqwk2jvdmk7ff3x6") (yanked #t)))

(define-public crate-pwdgen-1 (crate (name "pwdgen") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4") (features (quote ("colors"))) (kind 0)))) (hash "0sg9bd305aywm7pmc2ljiq65rg68ff122r7q8wczn53892a7cwmf") (yanked #t)))

(define-public crate-pwdgen-1 (crate (name "pwdgen") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4") (features (quote ("colors"))) (kind 0)))) (hash "1ficaqb4d1062frib2jngq83ihgcw3mlhm8f2dix0xwdw4ymglqz") (yanked #t)))

(define-public crate-pwdgen-1 (crate (name "pwdgen") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4") (features (quote ("colors"))) (kind 0)))) (hash "17p9d2wn7ffm2chpgs34jj4zayb8jjqxw9pxj70nylnna3z4m1nc")))

