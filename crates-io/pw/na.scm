(define-module (crates-io pw na) #:use-module (crates-io))

(define-public crate-pwnage-0.0.1 (crate (name "pwnage") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.8") (default-features #t) (kind 0)))) (hash "02s9p51xx7l2z01rhi8xa6h6mrmhl2qlq5ipj2y8vhfybmz5yk76")))

