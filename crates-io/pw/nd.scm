(define-module (crates-io pw nd) #:use-module (crates-io))

(define-public crate-pwnd-0.1 (crate (name "pwnd") (vers "0.1.0") (hash "06qrw6way4d6llcxrirklvq4v3y02lkmpjdq7sn1hsncpilj68pk")))

(define-public crate-pwnd-0.1 (crate (name "pwnd") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.51") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13.0") (features (quote ("io-util" "rt-multi-thread" "process" "net" "macros" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1iaaimx0yqdf8j5za2ka2b3sx1jvi20qi44nhjjdvksm3c7ysk7k")))

(define-public crate-pwndar-0.1 (crate (name "pwndar") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "18gs4d0bz7iz5hk0l18mmk8m5m9c6hcl9jlihn3bh6cvqzn72hk8")))

