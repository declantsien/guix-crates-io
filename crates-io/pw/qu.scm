(define-module (crates-io pw qu) #:use-module (crates-io))

(define-public crate-pwquality-0.1 (crate (name "pwquality") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r4qv1srb9hmzafh7y48j7cvl2a4z47pdwgwb1qk0i5nnip9275y")))

(define-public crate-pwquality-0.2 (crate (name "pwquality") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pwquality-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w675kqr15jdzmrgal67vzvz00iiy39iks2rfimp5wzrxyb9km45")))

(define-public crate-pwquality-sys-0.2 (crate (name "pwquality-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1pk2wc6fp3x0yhkp41zpqpnkry0lmzs7gz5nhl9fbyhcjrp1wxny")))

