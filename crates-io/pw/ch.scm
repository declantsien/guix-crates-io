(define-module (crates-io pw ch) #:use-module (crates-io))

(define-public crate-pwcheck-0.1 (crate (name "pwcheck") (vers "0.1.0") (deps (list (crate-dep (name "portable-pty") (req "^0.8.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Security"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0fwk7pxwx50qmj3v7bqgb5y44w94lq5kx3yk63nw5zyz83fd7rin") (rust-version "1.64")))

(define-public crate-pwcheck-0.2 (crate (name "pwcheck") (vers "0.2.0") (deps (list (crate-dep (name "pam-client") (req "^0.5.0") (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "portable-pty") (req "^0.8.1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Security"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1v1ikaq7024967kfvni0b1jg6gf0wn8h5arrz2lfzp45ka8qnlk2") (rust-version "1.64")))

(define-public crate-pwcheck-0.2 (crate (name "pwcheck") (vers "0.2.1") (deps (list (crate-dep (name "pam-client") (req "^0.5.0") (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "portable-pty") (req "^0.8.1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Security"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0cp73ma4adcdryizic9qrmy8rdcrh7x6rj4md9xz0xnbm7b99lhw") (rust-version "1.64")))

(define-public crate-pwchecker-rs-0.1 (crate (name "pwchecker-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.1") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "02zmcj93nk9h45ydnqpzq73jm3qk5p67h9rk75gs6z3nsgns91af")))

(define-public crate-pwchecker-rs-0.1 (crate (name "pwchecker-rs") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.1") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "09abdx1y00zaj3fn7abs8q1qj0krf2yigwyl5svys33rw68k6kgd")))

(define-public crate-pwchecker-rs-0.1 (crate (name "pwchecker-rs") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.1") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0qj32j89vrz347qcz0vzv0vwr67n3948hvb7g8cjph11lajslm7j")))

(define-public crate-pwchecker-rs-0.1 (crate (name "pwchecker-rs") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dnv4qlwc19sqnqvnjjfvh33z5rww4hmqkqr93rafcbhj0q6s27n")))

(define-public crate-pwchecker-rs-0.1 (crate (name "pwchecker-rs") (vers "0.1.4") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gs6wfi7czmfgzmxpgdfl7568vpfa9bm8gxrz21zl4p809f8pnm2")))

(define-public crate-pwchecker-rs-0.1 (crate (name "pwchecker-rs") (vers "0.1.5") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sc0sr62gvlvi0bv7v7y5k5288v03h6vyfz9kbxbyfni8l9b2lx9")))

