(define-module (crates-io z_ ta) #:use-module (crates-io))

(define-public crate-z_table-0.1 (crate (name "z_table") (vers "0.1.0") (hash "06akv0v3kjm25h63gqvnvzc4r7pdlchx792zl2sj731zmjmd5ss2")))

(define-public crate-z_table-0.1 (crate (name "z_table") (vers "0.1.1") (hash "17azflckiydsn14abh9j56b6rnqb5jvll7d1gsdp7i5z2nyjjq15")))

(define-public crate-z_table-0.1 (crate (name "z_table") (vers "0.1.2") (hash "153rf0ja32qn74grz76pah2f9yjq1jdda3syj4hqgw6c8a9q7hnz")))

(define-public crate-z_table-0.1 (crate (name "z_table") (vers "0.1.3") (hash "0himb6pn496kvv26592966l8gv6lh8bkd23ii2wrfjp4sa96cdyz")))

(define-public crate-z_table-0.1 (crate (name "z_table") (vers "0.1.4") (hash "1lbbks4snv4njks3vrhshwvz7qksfqbkz4wfsqgw0c470rlv735l")))

(define-public crate-z_table-0.1 (crate (name "z_table") (vers "0.1.5") (hash "0gz35dnfs4qrpp3rka4469523jqq500lh7cp8ipsjfg9pilx2hgr")))

