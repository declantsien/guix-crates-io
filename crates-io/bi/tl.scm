(define-module (crates-io bi tl) #:use-module (crates-io))

(define-public crate-bitlab-0.0.1 (crate (name "bitlab") (vers "0.0.1") (hash "1jhg7rph0gmkhc3l8bbld654zpgdgx5jcgigb2x0qswr2q51bz8d")))

(define-public crate-bitlab-0.0.2 (crate (name "bitlab") (vers "0.0.2") (hash "1p25336hgk3gs7h0q86f6rpkmc7mwpjs5x7cwc5sl7qdk0hdc7yr")))

(define-public crate-bitlab-0.0.3 (crate (name "bitlab") (vers "0.0.3") (hash "1zzki3z1yjq9nn33cyk1cblnsxrqhmasb93izqdyr4q4xq07bfnm")))

(define-public crate-bitlab-0.1 (crate (name "bitlab") (vers "0.1.0") (hash "1n5x0z0kx8isj0lmsq09q2ynwzg8y8b289qpamxm5d5wr73wpii9")))

(define-public crate-bitlab-0.1 (crate (name "bitlab") (vers "0.1.1") (hash "0i3drrm48vd67s3pqqgd1nviydcrdvvzn5s2bm8df8bbkwd8kwfc")))

(define-public crate-bitlab-0.1 (crate (name "bitlab") (vers "0.1.2") (hash "13zzyhcskzv4w7s8xdradslx867j1id6ci0rqmvlpw9sdqb2nqqm")))

(define-public crate-bitlab-0.1 (crate (name "bitlab") (vers "0.1.3") (hash "1y03sj03ankbxm54mydc5izs5phhn5nf19s4w6m9ypqp1faa6zby")))

(define-public crate-bitlab-0.1 (crate (name "bitlab") (vers "0.1.4") (hash "0l9872gn420yr6vgyrawnsimc3jqmarjg5gzw0lbb7yhwgv0i7qg")))

(define-public crate-bitlab-0.2 (crate (name "bitlab") (vers "0.2.0") (hash "1d179g82jahznwjvs0aaprs8qdf60x5sh9cm95q6vpzh3z3gfhxr")))

(define-public crate-bitlab-0.2 (crate (name "bitlab") (vers "0.2.1") (hash "0mzrhp3s9d1h2c21w5ydwkg4dja1x4il917sk7ak8j9mggjd2b9w")))

(define-public crate-bitlab-0.3 (crate (name "bitlab") (vers "0.3.0") (hash "1dgyrjsqw4347ajkcdsdgx17d7ay7k0x3wb7r3crr4cr6j2g1jis")))

(define-public crate-bitlab-0.4 (crate (name "bitlab") (vers "0.4.1") (hash "0kl5z96km9m498ckl4flsks0nzp9rhfmsx4br6z73zac45x4a60k")))

(define-public crate-bitlab-0.4 (crate (name "bitlab") (vers "0.4.2") (hash "12fb489z97y3yh0px1l0n3l2vqxmv1wii3xsjvzm2kjyknlilbkb")))

(define-public crate-bitlab-0.4 (crate (name "bitlab") (vers "0.4.5") (hash "01nvs3z7kqi19dnl2rr7vhkq96fs2l993882zk68in0y17pv39bc")))

(define-public crate-bitlab-0.5 (crate (name "bitlab") (vers "0.5.0") (hash "0vy71gw4xnnp75463wsyjxwcp60k7amyh1k3p09m50xq5kbiv7fc")))

(define-public crate-bitlab-0.5 (crate (name "bitlab") (vers "0.5.2") (hash "1vp3rbpd42w5x6ygng2lybcky2d6x2baf3mgqv9an2pxm29hsk6p")))

(define-public crate-bitlab-0.6 (crate (name "bitlab") (vers "0.6.0") (hash "1i2bdd2yc030kcqxha41h2wzp4w7m0m38ka69i1kkv3bpvk57r7h")))

(define-public crate-bitlab-0.6 (crate (name "bitlab") (vers "0.6.2") (hash "1cz4qlxyg0p76qjdai6znvmksdn7vjq1k7q0qw0hf2nvg01fmlzn")))

(define-public crate-bitlab-0.7 (crate (name "bitlab") (vers "0.7.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v4x1d182kg8rd7bjscfpbq0zs09sdb1arjnq6l4n1lb7dxxahj5")))

(define-public crate-bitlab-0.8 (crate (name "bitlab") (vers "0.8.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "08657rvga36xsn54khxcx4jk861dksskgcbgxwv7jlbiy23mkc5m")))

(define-public crate-bitlab-0.8 (crate (name "bitlab") (vers "0.8.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qp9c5yzcpz1c0r46j8nd2l3mfasr6kfdfnzbwxbjai9k2ziyz4r")))

(define-public crate-bitlab-0.8 (crate (name "bitlab") (vers "0.8.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rzcbdadywh811pqz6ry6rq5vq28f5qbl085scv68zlfm28wyly0")))

(define-public crate-bitlab-1 (crate (name "bitlab") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "18qa7napfb5bynj68bqygbnrpy7vc5lbd4yzjwxmym52yjjd0src")))

(define-public crate-bitlab-1 (crate (name "bitlab") (vers "1.1.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0f9g9riv9l3d1ynvi439j9k8s40ln6wwhnnp8v6dvfjcbsj9phvn")))

(define-public crate-bitless-0.1 (crate (name "bitless") (vers "0.1.0") (deps (list (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-journald") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1l5617097pzhvlgsp8j29znaiwzvi7dnc1sqlzvz65mkbfqxhxrj")))

(define-public crate-bitless-0.1 (crate (name "bitless") (vers "0.1.1") (deps (list (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-journald") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "10zsllgwz0s0xwqfkdnwm2yd9r0qrz9ac9n8fkisnnqkzqfn2q4y")))

(define-public crate-bitless-0.2 (crate (name "bitless") (vers "0.2.0") (deps (list (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-journald") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1yaywaz6jm4x62h6ad85rrpv716fnbqd5fbng1qgjkd4rdws7491")))

(define-public crate-bitless-0.3 (crate (name "bitless") (vers "0.3.0") (deps (list (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-journald") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "16c6kd4s0ds0bma6jrywznc2zg4hypyi7059z6s0rz81m76l975h")))

(define-public crate-bitlight-core-derive-0.1 (crate (name "bitlight-core-derive") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1bfddi2y8s8x7rmwzczd2mzm7asxwnas23qx9bmr4d9k8x2i1d34")))

(define-public crate-bitly-urlshortener-0.1 (crate (name "bitly-urlshortener") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "jabba-lib") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.83") (default-features #t) (kind 0)))) (hash "10mpvixwn7smsw482x82b27689x2md989hqxi8bbgdis8chkysfa")))

(define-public crate-bitly-urlshortener-0.1 (crate (name "bitly-urlshortener") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "jabba-lib") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.83") (default-features #t) (kind 0)))) (hash "1w8b4ijlsq2kp6d9n6ppwi8fqw3yvl3cxjjgdhnjpi3qi50pvw2x")))

