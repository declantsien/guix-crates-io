(define-module (crates-io bi t_) #:use-module (crates-io))

(define-public crate-bit_blend-1 (crate (name "bit_blend") (vers "1.0.0") (hash "1nn7q23cc5fgrx6jflj2wbzxbpg9lm31ajdcnn7nh3gvbwr74ygg")))

(define-public crate-bit_blend-1 (crate (name "bit_blend") (vers "1.1.0") (hash "1mfrn0sm8q9g8ax495kd5gwwz8cwls89rxkwhpl4y3wn7r3742ic")))

(define-public crate-bit_bool-0.1 (crate (name "bit_bool") (vers "0.1.0") (hash "136h5fdnlaz4wiycsv93i4qlmgaj3rm0win7mkvw9jv5rcj5j4f1")))

(define-public crate-bit_buffers-0.1 (crate (name "bit_buffers") (vers "0.1.0") (hash "1lzlh4ghip5syk4ikyyzj4xy67kbph4h943nf5bp1nsaqwwxhb56")))

(define-public crate-bit_buffers-0.1 (crate (name "bit_buffers") (vers "0.1.1") (hash "1lm6dj7c4avghzscrcjixm61z6hain7r4ib4fh74xmqxn06wnin8")))

(define-public crate-bit_buffers-0.1 (crate (name "bit_buffers") (vers "0.1.2") (hash "0j9i1ikj6xa98nshybi9mx9bc5v3a42d7d4bw6vzhzvpxc6lr34p")))

(define-public crate-bit_collection-0.1 (crate (name "bit_collection") (vers "0.1.0") (deps (list (crate-dep (name "bit_collection_derive") (req "= 0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 2)))) (hash "1r8a3xp76pdwzpy60c0p0iig0x0ky2vwny965v1dkjq6rh8pwwcg")))

(define-public crate-bit_collection-0.1 (crate (name "bit_collection") (vers "0.1.1") (deps (list (crate-dep (name "bit_collection_derive") (req "= 0.1.1") (kind 0)))) (hash "0ixm9nqy336i5r4bnwgvycn4svf4058ym2rg6sl23mn01wcv04y6") (features (quote (("std" "bit_collection_derive/std") ("default" "std"))))))

(define-public crate-bit_collection-0.2 (crate (name "bit_collection") (vers "0.2.0") (deps (list (crate-dep (name "bit_collection_derive") (req "= 0.2.0") (kind 0)))) (hash "14clcbacxlra0al6dym0d0hhbsrnbqbgisadrwdignfpcygvrrzp") (features (quote (("std" "bit_collection_derive/std") ("default" "std"))))))

(define-public crate-bit_collection-0.2 (crate (name "bit_collection") (vers "0.2.1") (deps (list (crate-dep (name "bit_collection_derive") (req "= 0.2.1") (kind 0)))) (hash "10klmwk0lvv4308fp5syjzz9i6ysh5nfl9zk2gikjv38fjrg7mzp") (features (quote (("std" "bit_collection_derive/std") ("default" "std"))))))

(define-public crate-bit_collection-0.2 (crate (name "bit_collection") (vers "0.2.2") (deps (list (crate-dep (name "bit_collection_derive") (req "= 0.2.2") (kind 0)))) (hash "0297aks2q2l8ywndfjr8npi7nfbi0r026r7w6xvjv0wd7npcbfig") (features (quote (("std" "bit_collection_derive/std") ("default" "std"))))))

(define-public crate-bit_collection-0.2 (crate (name "bit_collection") (vers "0.2.3") (deps (list (crate-dep (name "bit_collection_derive") (req "^0.2.2") (kind 0)))) (hash "0inx59k36qr6xsshlyxg5sr2a02s6g2hpmsn1d56wwp8ishvhdj0") (features (quote (("std" "bit_collection_derive/std") ("default" "std"))))))

(define-public crate-bit_collection_derive-0.1 (crate (name "bit_collection_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1iv1w7nfsd2d0j2drw4pzj8p6bm7zv423ybz2j71l9rii9rnp7dx")))

(define-public crate-bit_collection_derive-0.1 (crate (name "bit_collection_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "10d24ic6asqif5f4vss543w8pgjgvgy35gc8jlpxzcqgy6ilqbng") (features (quote (("std") ("default" "std"))))))

(define-public crate-bit_collection_derive-0.2 (crate (name "bit_collection_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1a59hwzhj1haqb0pdqp8bnk9w4yjpci04qk1b5sq4czsrnsd83sj") (features (quote (("std") ("default" "std"))))))

(define-public crate-bit_collection_derive-0.2 (crate (name "bit_collection_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0xp0n3d8bjf53957m3jkcf20adfbhv1gsrg20320zhf8kg16h6q4") (features (quote (("std") ("default" "std"))))))

(define-public crate-bit_collection_derive-0.2 (crate (name "bit_collection_derive") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1spjfxwxair9j6b3qkv2j2mi685625z76xin3fbhf6wysqd816vr") (features (quote (("std") ("default" "std"))))))

(define-public crate-bit_combi_iter-0.1 (crate (name "bit_combi_iter") (vers "0.1.0") (hash "0rklv0zcp7ynban1c2rrjgczvckavqx8h2zq1n48k1d553076yqj")))

(define-public crate-bit_combi_iter-1 (crate (name "bit_combi_iter") (vers "1.0.0") (hash "0xj1shnxjnvx1wiggahr3f0xy4056p6nkxha9ymj525jcnnng1i7")))

(define-public crate-bit_combi_iter-1 (crate (name "bit_combi_iter") (vers "1.0.1") (hash "0qgpk8y61i21lxpgkxm5nxfipa8adnsvdaddf439ki67z2jjzpm7")))

(define-public crate-bit_combi_iter-1 (crate (name "bit_combi_iter") (vers "1.0.2") (hash "1ybpl1p9qshr3sy69r2rayqszjxcgbljh1rqwlw0p6xxg6hfzj35")))

(define-public crate-bit_crusher-0.0.1 (crate (name "bit_crusher") (vers "0.0.1") (deps (list (crate-dep (name "dsp-chain") (req "*") (default-features #t) (kind 0)))) (hash "0c784mzj6lhgbzya6fw80iw424mr60a66j5mkfhi01vil0inz13y")))

(define-public crate-bit_crusher-0.0.2 (crate (name "bit_crusher") (vers "0.0.2") (deps (list (crate-dep (name "dsp-chain") (req "*") (default-features #t) (kind 0)))) (hash "0j7mvnjm3vg03wp1dzvmjbx2rj2mkm9ig4xbmckk98xg8p547aj5")))

(define-public crate-bit_crusher-0.0.3 (crate (name "bit_crusher") (vers "0.0.3") (deps (list (crate-dep (name "dsp-chain") (req "*") (default-features #t) (kind 0)))) (hash "190n17ds8w5bn6z0bxvq77n4hib19mviavl8vl736i09xjblzvs5")))

(define-public crate-bit_crusher-0.1 (crate (name "bit_crusher") (vers "0.1.0") (deps (list (crate-dep (name "dsp-chain") (req "*") (default-features #t) (kind 0)))) (hash "1cc1pln170bgwsj17qa5lclngm379hm5czkcvy47hk55bin9s5gj")))

(define-public crate-bit_crusher-0.1 (crate (name "bit_crusher") (vers "0.1.1") (deps (list (crate-dep (name "dsp-chain") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 2)))) (hash "0mvy7yw167k1l3s575ixccchsh2bidsg26qwsnm0s213zby71x9b")))

(define-public crate-bit_crusher-0.1 (crate (name "bit_crusher") (vers "0.1.2") (deps (list (crate-dep (name "dsp-chain") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 2)))) (hash "0zb4ak7khv0rna8p9n6gma09jykvd17cvg561d9ifk5yi3v2vlr1")))

(define-public crate-bit_crusher-0.2 (crate (name "bit_crusher") (vers "0.2.0") (deps (list (crate-dep (name "dsp-chain") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 2)))) (hash "17j4ymrsq3ch3s7n7prmlj517x9npy1f6p84nlzms65jw5lsfpm6")))

(define-public crate-bit_fiddler-0.1 (crate (name "bit_fiddler") (vers "0.1.0") (hash "16x81s5x64f5ahkj2wig265kpbzfc7dzws8cv9k0syki3bv6lmgi")))

(define-public crate-bit_fiddler-1 (crate (name "bit_fiddler") (vers "1.0.0") (hash "17vdkqk8asanj4y1fbcp447qxbn06kfnvhi6kdxg9il6bgg9qz9i")))

(define-public crate-bit_fiddler-2 (crate (name "bit_fiddler") (vers "2.0.0") (hash "11ckj4i2fbh3snvyhmky16pa9zhk71ls95yckcwamig899fi3s7i")))

(define-public crate-bit_fiddler-2 (crate (name "bit_fiddler") (vers "2.1.0") (hash "04jw89z18cra6gcsichpwvfj6npxccsk0kp5z7wiv3s94g90zfl5")))

(define-public crate-bit_fiddler-2 (crate (name "bit_fiddler") (vers "2.1.1") (hash "077kqzdnlby2kiwi0593gs2isw3c1vj13w7x3shpmqwjc3nnd9n3")))

(define-public crate-bit_field-0.1 (crate (name "bit_field") (vers "0.1.0") (hash "1df6hz5irscav4r3hv3nx9mkj0xabnkchvlsx66143cvw6xfb21z")))

(define-public crate-bit_field-0.2 (crate (name "bit_field") (vers "0.2.0") (hash "0rz0ixhxqpl6s7dhqzp85iblmfh2l6508qmdrg36r7g6f5f0dsjs")))

(define-public crate-bit_field-0.2 (crate (name "bit_field") (vers "0.2.1") (hash "1pbjkscyylp5wsrw1vysawr2fra27w2yn1b35wj470876i7bc3l8")))

(define-public crate-bit_field-0.4 (crate (name "bit_field") (vers "0.4.0") (hash "1xk161c93r5mlpcpp7wrlwrpv6xcz0im5cw3sgw7n8k4q0k2czwc")))

(define-public crate-bit_field-0.5 (crate (name "bit_field") (vers "0.5.0") (hash "1fyyc3xg8xr03xi2xpsnshh5xbgpkiz1g95qhv4pkid0sfb555cw")))

(define-public crate-bit_field-0.6 (crate (name "bit_field") (vers "0.6.0") (hash "1yigf79qx1xxj7z7bxdcmgfwa1y7lw9w4sc4mvxg116nfd209k9q")))

(define-public crate-bit_field-0.6 (crate (name "bit_field") (vers "0.6.1") (hash "0p9ga9pc6d8w3kckms0h1fk5hx5164ks6dv9v027h1givg10h9ig")))

(define-public crate-bit_field-0.7 (crate (name "bit_field") (vers "0.7.0") (hash "1ba9d0kfjl757rgjjj095bcq02azmg4z481rcjzm7g712i0ad4gz")))

(define-public crate-bit_field-0.8 (crate (name "bit_field") (vers "0.8.0") (hash "0jg6327ck6vccwf2rrqwg1dcks20a4rn6dn5ji7r4fyg7lf9i0jz")))

(define-public crate-bit_field-0.9 (crate (name "bit_field") (vers "0.9.0") (hash "0mjxkfcz2biq469iwsrj7jrj1cr54qrpssxbfiwn22chky86b1zd")))

(define-public crate-bit_field-0.9 (crate (name "bit_field") (vers "0.9.1") (hash "108qfwjrykz85v8ahvi4z38vrrsxdj2nfbmw4r37j8fg7d3a7lxf") (yanked #t)))

(define-public crate-bit_field-0.10 (crate (name "bit_field") (vers "0.10.0") (hash "1h0x72mv5c6xryc916fdyqqvvc0ykdpgna1smka42iq8rw3dcrd1")))

(define-public crate-bit_field-0.10 (crate (name "bit_field") (vers "0.10.1") (hash "192rsg8g3ki85gj8rzslblnwr53yw5q4l8vfg6bf1lkn4cfdvdnw")))

(define-public crate-bit_field-0.10 (crate (name "bit_field") (vers "0.10.2") (hash "0qav5rpm4hqc33vmf4vc4r0mh51yjx5vmd9zhih26n9yjs3730nw")))

(define-public crate-bit_manager-0.1 (crate (name "bit_manager") (vers "0.1.0") (hash "1yf5l3ivpv3n2spa9mfdqfknk1k9pywb9bq25ays84dlrgh6dq2k")))

(define-public crate-bit_manager-0.1 (crate (name "bit_manager") (vers "0.1.1") (hash "05axmpxqdyh0i3llq0zjnkwklfnngfhca1wmzld2jq1g1d3c669l")))

(define-public crate-bit_manager-0.2 (crate (name "bit_manager") (vers "0.2.0") (hash "1x7hkx8qszqz332a84h3gpcaf39wckl00f3pr6xv4181lpiqawrv")))

(define-public crate-bit_manager-0.3 (crate (name "bit_manager") (vers "0.3.0") (hash "0g1xp59q0hrx0gra9pw6fsyfs7dynbvm94j443ckzq3smzgaskkm")))

(define-public crate-bit_manager-0.4 (crate (name "bit_manager") (vers "0.4.0") (hash "1dijhmxhbsgx9yc06zmdn07varcvjda18v7gs9ph2rrls7l70q0v")))

(define-public crate-bit_manager-0.5 (crate (name "bit_manager") (vers "0.5.0") (hash "02dh1aphkygp0f6n8bfrlfp3rcc7fh8rzqjxikkl3qcxqwpj1hsx")))

(define-public crate-bit_manager-0.5 (crate (name "bit_manager") (vers "0.5.1") (hash "1xvfw6v3qw76v7hiygj55gr1s5lrwwsjzbh5f18mfgldl19r2hgs")))

(define-public crate-bit_manager-0.5 (crate (name "bit_manager") (vers "0.5.2") (hash "0p048hh438f7mbs10dnlqaknba8xvj5lnc30fffp3h759x3ic746")))

(define-public crate-bit_manager-0.5 (crate (name "bit_manager") (vers "0.5.3") (hash "0ncvvf6lfi8lr04i64shv4id4a9hks69h8zjr5zhdly71dpkzh0z")))

(define-public crate-bit_manager_derive-0.1 (crate (name "bit_manager_derive") (vers "0.1.0") (deps (list (crate-dep (name "bit_manager") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0gzxir29bxpk51dk2nb91vxhxd4cmgvpks174197z0g3zyl193bb")))

(define-public crate-bit_manager_derive-0.1 (crate (name "bit_manager_derive") (vers "0.1.1") (deps (list (crate-dep (name "bit_manager") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "02ds9blr4dsi2mh2cln0z8iakvwdci3ipg1ycgp1mi3dpg0ny6yq")))

(define-public crate-bit_manipulation-0.1 (crate (name "bit_manipulation") (vers "0.1.0") (hash "18q6wjgi8l1cab1vpsrd2g8651cw6j9scyfryk18f1h01p1zs466")))

(define-public crate-bit_manipulation-0.1 (crate (name "bit_manipulation") (vers "0.1.1") (hash "1fj30cp2vcimwdh46lnm95wf1gkwd2h1h7y2bprmizl7l4rdqll8")))

(define-public crate-bit_manipulation-0.1 (crate (name "bit_manipulation") (vers "0.1.2") (hash "1r57h5shr2hyxqlx6nknpzld6zpy2jj5micxxqsk4wrka64pc687")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.0") (hash "14mz9fip8gyhmfymbxizzpcaxjgjjl4iii6891fd852alqscrvh6")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.1") (hash "04i9hvjx0mb5rvggby0plpsx94a1kfhq02ay244xbhnxify5crp1")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.15") (hash "1b2ik8jr2hpssbr49zhryycfs82khcmiyv6yn1zlrisxzd8y725a")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.2") (hash "0gp85il3vnr88vh0kx0km69azj9mciw1dmn5gi46l0vzzy0fy65y")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.25") (hash "070wqz83ni2d7gqydy7k9a5ak285x0yp4ynfcvi604gl7f1qrnbn")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.3") (hash "1ki9sickybkgqzjbahrrhb21jag7ap0l72qaca34fkxs0w2frw2g")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.35") (hash "07qfnwj1f5cz8yiyqyb1vxxbaxf0ky0mnxgd3smsyc0nzz4k9q1a")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.4") (hash "0nibg6pdipzcqs3skh6wqk3nf4vnqidrrb03wr362nai136r22ra")))

(define-public crate-bit_manipulation-0.2 (crate (name "bit_manipulation") (vers "0.2.40") (hash "0f6vi16anja91zhj40b3zgpx0c32bkyfwj7xfrjdwpxjjzi04gml")))

(define-public crate-bit_mask_ring_buf-0.1 (crate (name "bit_mask_ring_buf") (vers "0.1.0") (hash "0shpzim6bai2c19aa4smg3vjzglg2gigbjq43f21nchr25b8p4k8")))

(define-public crate-bit_mask_ring_buf-0.1 (crate (name "bit_mask_ring_buf") (vers "0.1.1") (hash "1x9dlswxxj59xg4dnaijzw4z8clh18vq498isgy2w351x4gbg0mv")))

(define-public crate-bit_mask_ring_buf-0.1 (crate (name "bit_mask_ring_buf") (vers "0.1.2") (hash "0xpg0mi0kmid66hhcqqpch0dissyjw9dn99xd8drwgwbwdjx4v9m")))

(define-public crate-bit_mask_ring_buf-0.1 (crate (name "bit_mask_ring_buf") (vers "0.1.3") (hash "14mi6fkv02rcakw0ghs3nyap1jcffhvdifryj6p2ivfyarp22v7s")))

(define-public crate-bit_mask_ring_buf-0.1 (crate (name "bit_mask_ring_buf") (vers "0.1.4") (hash "14arjhd27mcnar3bjfvnzn1ah7idc78cvwrw6l8niagcd674sdy2")))

(define-public crate-bit_mask_ring_buf-0.1 (crate (name "bit_mask_ring_buf") (vers "0.1.5") (hash "1x79ibq5calsq78xgz38wiyv3kh6aqbal3b9hfhx0w68bqiqllw5")))

(define-public crate-bit_mask_ring_buf-0.1 (crate (name "bit_mask_ring_buf") (vers "0.1.6") (hash "0zvai5039w191r3rf71grrqk7k7gy0ymx0nddgbs55kv61lbm4lx")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.0") (hash "133h47m07cjjkjdaiqfqnm9wsvrvd95skcxlpjndpyinqjq45a7b")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.1") (hash "10hy0hbpniwx89k4lyzaa6hcvzz0kmzrzwywdm1ysy7n14acx84v")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.2") (hash "1wb1w80fsg7s1s11a7zxvsrj2kmzncs824xhgwljlhx97rx464nz")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.3") (hash "0qbw71371zpgkwhd4q3zj5xf42vwx163b8s3q920cz4v80myh177")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.4") (hash "161zrj30wb075mszidrirsn5i0xm45nh3yfhq5i3gfik81wwssg6")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.5") (hash "11a9wamp673hjw7iybhprxyn4a910c5qg2fn9nwsmfzl2fqb83b9")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.6") (hash "0p6z2xyw8xfalcn1myj8yrghhqax7khb07bsvv31bjp2lhrmfm3h")))

(define-public crate-bit_mask_ring_buf-0.2 (crate (name "bit_mask_ring_buf") (vers "0.2.7") (hash "1hyiabci68sf5xrm26ck9s6blbjfi191082bl135144wxykvwp5b")))

(define-public crate-bit_mask_ring_buf-0.3 (crate (name "bit_mask_ring_buf") (vers "0.3.0") (hash "1zara8rxlr4ncnkwz99xi0dymcphkqc2iabwn2f2yaciib54wslm")))

(define-public crate-bit_mask_ring_buf-0.3 (crate (name "bit_mask_ring_buf") (vers "0.3.1") (hash "0rkrryalzgnlijl2d11p7v1j5xg86qbnz633w9xy8pk0bairx6bq")))

(define-public crate-bit_mask_ring_buf-0.4 (crate (name "bit_mask_ring_buf") (vers "0.4.0") (hash "15409zbgdjrfy7r2nxv36i99w1752mcgwd5bapqafikwc1z8661x")))

(define-public crate-bit_mask_ring_buf-0.4 (crate (name "bit_mask_ring_buf") (vers "0.4.1") (hash "0lz6hjbdkp17aqgynp3clah5c5y8lmys15vpciy6bjkhn3g9n9bq")))

(define-public crate-bit_mask_ring_buf-0.4 (crate (name "bit_mask_ring_buf") (vers "0.4.2") (hash "1739nfqd5r5qzpbyavjcz7fq7dkld054bsjzjs37xdri1rfw3zvk")))

(define-public crate-bit_mask_ring_buf-0.4 (crate (name "bit_mask_ring_buf") (vers "0.4.3") (hash "1b58znmqrvqmrfmhcdq9a2ldjr3fj12kn8z7rnrk3cxfl9ga342q")))

(define-public crate-bit_mask_ring_buf-0.4 (crate (name "bit_mask_ring_buf") (vers "0.4.4") (hash "02kg4vpp5abyfqdlh0l50graqw88ic8z9fya8bq6ld619pxn07q6")))

(define-public crate-bit_mask_ring_buf-0.4 (crate (name "bit_mask_ring_buf") (vers "0.4.5") (hash "0s52aa39lk71134d67ivlh4fjqvspa5a002b1y5j1f4ifm2r5zyj")))

(define-public crate-bit_mask_ring_buf-0.4 (crate (name "bit_mask_ring_buf") (vers "0.4.6") (hash "0aah9i8i0j56zpw5ai1gsapgwq9mv883ib4rc02ai01f8fc7bbr7")))

(define-public crate-bit_mask_ring_buf-0.5 (crate (name "bit_mask_ring_buf") (vers "0.5.0") (hash "0yqv7562k9c9id8ab38bdm8j3110pgnml985kslihnjldr0wn3k1")))

(define-public crate-bit_mask_ring_buf-0.5 (crate (name "bit_mask_ring_buf") (vers "0.5.1") (hash "1v9rvwndc2acbsgsi6p9j0yz749grngszzaz5wz9sblgz2356gxl")))

(define-public crate-bit_mask_ring_buf-0.5 (crate (name "bit_mask_ring_buf") (vers "0.5.2") (hash "02zbh4k4qqiv3hnrlk1sparch38iw41brqljypc3a0x71hn34kc1")))

(define-public crate-bit_mask_ring_buf-0.5 (crate (name "bit_mask_ring_buf") (vers "0.5.3") (hash "0rfkm2sqc11nl29vkhc39yfb6ijavx2w4v5w213672sjmpk59vmk")))

(define-public crate-bit_mutator-0.1 (crate (name "bit_mutator") (vers "0.1.0") (hash "13cjyascvdra0rqs7kfigl7356kh80wwifalw1k8pqirn3j9z8yr") (yanked #t)))

(define-public crate-bit_mutator-0.1 (crate (name "bit_mutator") (vers "0.1.1") (hash "1qncxlamyjkgcs0vi0667pv819z4r3nbmd0dmzrgbfy5scdgckcq")))

(define-public crate-bit_op-0.1 (crate (name "bit_op") (vers "0.1.0") (hash "1iz4xm1gv08ml0dsvzmh9c1k2mcvmpvqrjykqd9y97x3m9j9f9vg")))

(define-public crate-bit_op-0.1 (crate (name "bit_op") (vers "0.1.1") (hash "0g5x1qh8yif3521p5q6f6il22z5pi1pn2ybz0pls3sxcbj2ip435")))

(define-public crate-bit_range-0.1 (crate (name "bit_range") (vers "0.1.0") (hash "0lrvjnwqqw4cx31hxrhzzz5c259kllcb3q1jw90vwa4sfikr4m27")))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.0") (hash "0zydkicj909viza71yvvvx9maw1mi5drn5lvzi5gxfwhfrd4k54f")))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.1") (hash "0rqyysai9f2iv51q2xb9lv60i8p4g0gvnx8baqkr8qabhax8d2yp")))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.2") (hash "16g02b7lp3s1pl2740kc6r0gnxnc2ivx400xsaiyi47vv8y6abb1")))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.3") (hash "1ll826byi7jah2f66jxn2q7s72ywwzc2sj4nqcw5j317z64phlwq")))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.4") (hash "0kd6prg2vvrypja601al0rjw7shmshc6m7k9kj01j4gp1vbisfwb") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.5") (hash "1plns8k071kg510xzkm0c821wb5d8fjbwp302k5h0c5wqp0p88sw") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.6") (hash "1yjk4js4bvlpsdhpdg8lshpjcg55a6d1f8y0v40bhmmjiddv9s4n") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.7") (hash "135282g1prcxd7nzdgksnji6qad8blfh7mpkfw1rr2d2nlny15sy") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_reverse-0.1 (crate (name "bit_reverse") (vers "0.1.8") (hash "15kj151hy2s7zq4jj3zw80hccmxjfwqprgq6w73rb55v1aiqqllr") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bit_roles-0.1 (crate (name "bit_roles") (vers "0.1.0") (deps (list (crate-dep (name "bit_roles_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0rhcdizg2sxxjzlqx62hsw8cwxqj095wd3vls744a783dclx7fqd")))

(define-public crate-bit_roles-0.2 (crate (name "bit_roles") (vers "0.2.0") (deps (list (crate-dep (name "bit_roles_macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0d9b1jfiiqv8nfffw70cipm5rcq229swi6cs8ifzfvwv9hskgs1w")))

(define-public crate-bit_roles_macros-0.1 (crate (name "bit_roles_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "17ikgnivh6rj6l1sbg61cb3xjrdlc3x87lqbnxppr7c4f7jhc19i")))

(define-public crate-bit_roles_macros-0.2 (crate (name "bit_roles_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "1b9bi78igbkm9h9lcjs2769kdbfbgwndfjknskkgh6q6i7pkmx69")))

(define-public crate-bit_seq-0.1 (crate (name "bit_seq") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "1g014i5mamhrd69w4lfmbfdqsx0df4pr4ixb5k58vajpliabil6x")))

(define-public crate-bit_seq-0.1 (crate (name "bit_seq") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "1c0dw1jsp0bzzpya6pb779s8gnjpimpczwgd5kyfjcvakahgcymk")))

(define-public crate-bit_seq-0.1 (crate (name "bit_seq") (vers "0.1.2") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "0wdc9j41jdkyg4005zbqvr85kzbhcbc5n7kv5rl0a4vyjsas4ac1")))

(define-public crate-bit_seq-0.2 (crate (name "bit_seq") (vers "0.2.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "10khqy6d4z7n2vvzky19li5fp1xc30zwhx8d461q2pp55vz3872y")))

(define-public crate-bit_seq-0.2 (crate (name "bit_seq") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "0zczkcap0mmx2pdzw87as9b4w6n4dcifgdp014574v8w1qz8harx")))

(define-public crate-bit_serializer-0.0.1 (crate (name "bit_serializer") (vers "0.0.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "10xzzpwvc19h1bwcw1j27kkxx4wj41yb5chg5sj5nglpqrxnjkch")))

(define-public crate-bit_streamer-0.1 (crate (name "bit_streamer") (vers "0.1.0") (hash "1s42al00lgjchvh59d53d8vv7y6sh6ncxa1z3ss4q13dgb6ji26f")))

(define-public crate-bit_utils-0.0.0 (crate (name "bit_utils") (vers "0.0.0") (hash "0kb9m17glfzk0gbwiibnrci3h8rbr9hf1f1rm574j8q1p3k7pd6i")))

(define-public crate-bit_utils-0.1 (crate (name "bit_utils") (vers "0.1.0") (hash "1qlsyhhm2jkda5mgagsfh9fi8q42cvf5bs315f2mvkx090dc0sgn")))

(define-public crate-bit_utils-0.1 (crate (name "bit_utils") (vers "0.1.1") (hash "0vf25jgilhydhhfrfzvm4x49bkri65m9gs9xy3d4njm08p54s4gq")))

