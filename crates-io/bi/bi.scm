(define-module (crates-io bi bi) #:use-module (crates-io))

(define-public crate-bibicode-0.1 (crate (name "bibicode") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "1dndqx5v2fi4df5582jxajhs02l954mc4w6g78cqnki3rmgn77b7")))

(define-public crate-bibicode-0.1 (crate (name "bibicode") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "002rj30jkn212mjpi48yz7i6g1madg6hnfjz964qflkdpilg05zs")))

(define-public crate-bibicode-0.1 (crate (name "bibicode") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0fs8abnaclivvjx407j0v1v5m3jskb3kxc0lciiqc14g86s0n74z")))

(define-public crate-bibicode-0.1 (crate (name "bibicode") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0cjd6s1sijg06g52as11hd8is37qmpwsfk61mrmnwwvj193nzqwh")))

(define-public crate-bibicode-0.1 (crate (name "bibicode") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0mv6xvjfg112dk44vkz1rrmcjii00minv89lw4ds4swasmffx23z")))

(define-public crate-bibicode-0.1 (crate (name "bibicode") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "1i27ylbl4w243yppd8m6fn2yhn637jp2g7k3vrjmza7fbq67cznj")))

(define-public crate-bibicode-0.2 (crate (name "bibicode") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zsy380rkqdhibsryhvrfkqc9s6b6dd4m8ra3bgd92qmps9hrs92")))

(define-public crate-bibicode-0.2 (crate (name "bibicode") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zjpaxdssf1fpkdammmfz241gbgaghrd1232sz050rnvv6wq5f7g")))

(define-public crate-bibicode-0.2 (crate (name "bibicode") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1f3lsgc2isv8n2x5wwwn6krf01bh40z0xdhvz47wpjnjblglllx4")))

(define-public crate-bibicode-0.3 (crate (name "bibicode") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0a5k9zkg5zgbar0bsqb2amypajz72sbv515x4z10n3yfkdnnwxmx")))

(define-public crate-bibicode-0.3 (crate (name "bibicode") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1axdy2xlabm67cy9cl2y0z0lzgnk58kgjiwpb4z8wnxvfhxazs4g")))

