(define-module (crates-io bi ol) #:use-module (crates-io))

(define-public crate-biolib-0.1 (crate (name "biolib") (vers "0.1.0") (hash "1pgsnygv0lmiviamaszy8d1i4296jsvzadghna8smkswhh0wf0m9")))

(define-public crate-bioluminescence-0.0.1 (crate (name "bioluminescence") (vers "0.0.1") (hash "13916xqqxd3kaixjl5yjfbnzrqjy8ap49niybd3zjdkzsvgkd8nr")))

