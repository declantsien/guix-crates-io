(define-module (crates-io bi tu) #:use-module (crates-io))

(define-public crate-bitumen-0.1 (crate (name "bitumen") (vers "0.1.0") (hash "05m6brypsqsv8vrizj3nbykv8sx3chcf5f3wnlc6y0l9jkb9yhmj")))

(define-public crate-bitut-0.1 (crate (name "bitut") (vers "0.1.0") (hash "1ndlqc9c8aqsd5s96fdi11v8f95s09bfi2j5324z3fwgp93iqmdk") (features (quote (("default") ("const_impl") ("assertions"))))))

(define-public crate-bitutils-2 (crate (name "bitutils") (vers "2.0.0") (hash "0yhg2s6gys4b27yv5lm0aqsj78hp2l598lw6705q3an0nvqzn213") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bitutils-2 (crate (name "bitutils") (vers "2.0.1") (hash "1iapmlgn3maqxs06bvl31lmrzsdziws0jyy7qdksp7aagpridqn5") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-bitutils-3 (crate (name "bitutils") (vers "3.0.0") (deps (list (crate-dep (name "bf-impl") (req "^1.0") (default-features #t) (kind 0)))) (hash "07jx5azn1wm7ws4xxwqjb7b8nclj97frv2h94ga67pgpcmp54jg9") (features (quote (("use_std" "bf-impl/use_std") ("default" "use_std"))))))

(define-public crate-bitutils-3 (crate (name "bitutils") (vers "3.0.1") (deps (list (crate-dep (name "bf-impl") (req "^1.0") (kind 0)))) (hash "1nqclzh5xdk5s1lmy4hsryj4s8lb0zgd4vhy8r2jxk9z9qw1v8a9") (features (quote (("use_std" "bf-impl/use_std") ("default" "use_std"))))))

(define-public crate-bitutils2-0.1 (crate (name "bitutils2") (vers "0.1.0") (hash "10jx13dl7pr95k4fkd9r2fspzzympml9vxqjbwdrkn9qiq93jsq2")))

(define-public crate-bitutils2-0.1 (crate (name "bitutils2") (vers "0.1.1") (hash "0lk16sap59g7q14az9iinh0dm9ypiyd1sq6gr1bv3jksasgylpb3")))

