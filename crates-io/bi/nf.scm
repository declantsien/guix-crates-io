(define-module (crates-io bi nf) #:use-module (crates-io))

(define-public crate-binf-0.1 (crate (name "binf") (vers "0.1.0") (hash "12mpdx1mh8arl373y40naksqmnb8lrxfdz6lkdgfl0hqf36v3r6l") (yanked #t)))

(define-public crate-binf-0.1 (crate (name "binf") (vers "0.1.1") (hash "17ffgxfpiv60m56zfch4fx4haa4k7dzrqh3cdjm35qzvngzn7i5i")))

(define-public crate-binf-0.1 (crate (name "binf") (vers "0.1.2") (hash "0lbkm2jg5g440ix450w1kcca559xr5pisp8l6q8r0n3gps1b8rk0")))

(define-public crate-binf-0.2 (crate (name "binf") (vers "0.2.0") (hash "0mihh1nprn362n8wznrbkhlxvxdqi30jyi902d0f05acf8ynv9hg")))

(define-public crate-binf-0.2 (crate (name "binf") (vers "0.2.1") (hash "0lfszfqjwwl4yf0fdwyzkmgw8mh3r0y5shin8npgga96d6cvqkqh")))

(define-public crate-binf-0.2 (crate (name "binf") (vers "0.2.2") (hash "0zli869nfggnz91fl0n4y7s7lwz3k08r1zjklk1vswbpd5sw9w8z")))

(define-public crate-binf-0.2 (crate (name "binf") (vers "0.2.3") (hash "1884llglr0j6b7bd7c6s83zaadnk0bjpv3hgxwfrwfvadpbbks7y")))

(define-public crate-binf-1 (crate (name "binf") (vers "1.0.0-rc1") (deps (list (crate-dep (name "binf_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18055kh3gcvm2s32wkhkvplwckcidczf5gc1nqqqkl3w4mj4za2i")))

(define-public crate-binf-1 (crate (name "binf") (vers "1.0.0") (deps (list (crate-dep (name "binf_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13sabzkc90fbjvvd3m0cpnr6dx1c3k033rd99lj2jjp8ryc4swkd")))

(define-public crate-binf-1 (crate (name "binf") (vers "1.0.1") (deps (list (crate-dep (name "binf_macros") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "12aqlpwrbwc18y3jj2bi7v244y7ld4jn577n3csw1h9za8y45643")))

(define-public crate-binf-1 (crate (name "binf") (vers "1.0.2") (deps (list (crate-dep (name "binf_macros") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1afp7nm4yfxy6357sn4jvhcpyjs9i3a7qnbki05zhj6i8ic5jlzw")))

(define-public crate-binf-1 (crate (name "binf") (vers "1.1.0") (deps (list (crate-dep (name "binf_macros") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1h545vg4jcgb0whwav3qclbll2wmlcvkjs0lsjjirawihdy0zk6f")))

(define-public crate-binf-1 (crate (name "binf") (vers "1.1.1") (deps (list (crate-dep (name "binf_macros") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1d277zj48dwnzx2ndqx2l60bl34mm9y0ka6mihw3fk5mhgk5wm6k")))

(define-public crate-binf_macros-0.1 (crate (name "binf_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jgd6pz7lxgvwg6775a1vs2rvh2d4l8c8jsjrbs20gry1wlrc9rj")))

(define-public crate-binf_macros-0.1 (crate (name "binf_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n63acl9jm0y7rw5gxmlv5kwqvvl6f8igcfcg8fkq2nnzxxwyrfb")))

(define-public crate-binf_macros-1 (crate (name "binf_macros") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dgprk4fbp6bvsz37swwy4cza87dfc2k6k8ga3l586xv1n2h60x4")))

(define-public crate-binf_macros-1 (crate (name "binf_macros") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sbzk518zs3c46m1c0ixi4frdlf39r6jiz6ppp8qqsj676ad7kpa")))

(define-public crate-binf_macros-1 (crate (name "binf_macros") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c4xn1xg0qci018yvcfii9bk0hg4npxd4km1w887m2hqzh706527")))

(define-public crate-binf_macros-1 (crate (name "binf_macros") (vers "1.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19dlkyhdm3ii1bx3kdc8hkd1v4fxl0mk577cpwl3115177x0aqpr")))

(define-public crate-binfarce-0.0.1 (crate (name "binfarce") (vers "0.0.1") (hash "0rcqqc2zqb8iz88wl0hicibynp1c637krsylafrf30ajhn6x4zbm")))

(define-public crate-binfarce-0.1 (crate (name "binfarce") (vers "0.1.0-rc1") (hash "0v40fjlvwjj7f0lyhqs4ah27kfn3aq0788a3iy3qd58g9jd2gb19")))

(define-public crate-binfarce-0.1 (crate (name "binfarce") (vers "0.1.0") (hash "1wx3h7mzkbpa98ip97cw2i52jrn3i6zrhd5rx63h3gw1dx72qfid")))

(define-public crate-binfarce-0.1 (crate (name "binfarce") (vers "0.1.1") (hash "04f7a8grg66ada4gfl2z9k0bl653zy14a1n14xcjp89rgplj31m3")))

(define-public crate-binfarce-0.2 (crate (name "binfarce") (vers "0.2.0") (hash "1zxqg43xqz6xmyv68wc6xzigczhrq3hfcg2nx5jh4db468lkin9r")))

(define-public crate-binfarce-0.2 (crate (name "binfarce") (vers "0.2.1") (hash "18hnqqir3gk5sx1mlndzgpxs0l4rviv7dk3h1piyspayp35lqihq")))

(define-public crate-binfetch-wasm-1 (crate (name "binfetch-wasm") (vers "1.0.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.67") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.90") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.67") (features (quote ("XmlHttpRequest"))) (default-features #t) (kind 0)))) (hash "1yf28f6wb2zc94n0mkifgs4kllp8vr48kx2bzlc1qinwx3grca3q")))

(define-public crate-binfetch-wasm-1 (crate (name "binfetch-wasm") (vers "1.0.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.67") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.90") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.67") (features (quote ("XmlHttpRequest"))) (default-features #t) (kind 0)))) (hash "1fnbmnljbpj6my0izn3yibh6cvxks4yzciqhx1v0vijqr793kwgh")))

(define-public crate-binfield_matrix-0.1 (crate (name "binfield_matrix") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "1wbs26wxkiamqps5wg5ypxbf05qvb8km97nz5frch6jzxqc82axa")))

(define-public crate-binfield_matrix-0.1 (crate (name "binfield_matrix") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "1za4rqw42nb414yj5f56by4a67c5hk67mdjxcbx903v80007cj4d")))

(define-public crate-binfield_matrix-0.1 (crate (name "binfield_matrix") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "1djfqfijg01xwndzcdwb4g7zg7d1bis2vcy6h2wjb5bzbxr8p40w")))

(define-public crate-binfield_matrix-0.2 (crate (name "binfield_matrix") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0l0b4fzcpglshayfkj9vk4rxwq2yym7kzn5k8z491g88m83z3087")))

(define-public crate-binfire-0.0.0 (crate (name "binfire") (vers "0.0.0") (deps (list (crate-dep (name "binfire-lib") (req "^0.0") (kind 0)) (crate-dep (name "regex") (req "^1.6") (features (quote ("std"))) (kind 0)))) (hash "07p6vwaj8lrj0qx6fyrr1gij9hzi9xj5vr65w8b57cwshbg0dgqd")))

(define-public crate-binfire-lib-0.0.0 (crate (name "binfire-lib") (vers "0.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "18b93rrznq17ampr7nz1wffapivjr1xpsq85vh410fz6mxkk4lqd")))

(define-public crate-binfmt-0.1 (crate (name "binfmt") (vers "0.1.0") (deps (list (crate-dep (name "arch-ops") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fake-enum") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "target-tuples") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1bavay7fw39829c8f2dc9arnw4ipr4drjbrmcwsax8l3llnf2ipc") (features (quote (("z80") ("xo65") ("xir") ("x86") ("w65") ("riscv") ("pe" "coff") ("o65") ("macho") ("m68k") ("m6502") ("llir") ("elf64" "elf") ("elf32" "elf") ("elf") ("default-formats" "coff" "elf32" "elf64" "macho" "pe" "llir" "xir" "ar") ("default-archs" "w65" "x86" "arm" "riscv" "m68k" "clever") ("coff") ("clever") ("arm") ("ar") ("aout") ("all-formats" "default-formats" "aout" "xo65" "o65") ("all-archs" "default-archs" "z80" "m6502") ("aarch64"))))))

(define-public crate-binfmt-0.1 (crate (name "binfmt") (vers "0.1.1") (deps (list (crate-dep (name "arch-ops") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fake-enum") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "target-tuples") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0265h7hyq7g3sds5c17343rl0lzviy1zgndsrkq6srm0qlzagkkf") (features (quote (("z80") ("xo65") ("xir") ("x86") ("w65") ("riscv") ("pe" "coff") ("o65") ("macho") ("m68k") ("m6502") ("llir") ("elf64" "elf") ("elf32" "elf") ("elf") ("default-formats" "coff" "elf32" "elf64" "macho" "pe" "llir" "xir" "ar") ("default-archs" "w65" "x86" "arm" "riscv" "m68k" "clever") ("coff") ("clever") ("arm") ("ar") ("aout") ("all-formats" "default-formats" "aout" "xo65" "o65") ("all-archs" "default-archs" "z80" "m6502") ("aarch64"))))))

