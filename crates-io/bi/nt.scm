(define-module (crates-io bi nt) #:use-module (crates-io))

(define-public crate-bint-0.1 (crate (name "bint") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.302") (optional #t) (default-features #t) (kind 0)))) (hash "1sx1l5vvjrrwbqr470lf2q5qi8bfzdrv10ciqviw1jsf6n0fn13x") (features (quote (("default"))))))

(define-public crate-bint-0.1 (crate (name "bint") (vers "0.1.2") (hash "1rh8n4qswdsrprl2vasnhq33fkbq5krlzd7ndbm84v2n8ww3dfdq")))

(define-public crate-bint-0.1 (crate (name "bint") (vers "0.1.3") (hash "13dp4dryqp0xz7zgqvyhhnriifvpanf7fcnjaaprz85ahf2h41k7")))

(define-public crate-bint-0.1 (crate (name "bint") (vers "0.1.4") (hash "0bipk89qhvcg0pdd9pmspx7lpc2rbdl6vpzgnahh78qx4jh8akph") (rust-version "1.34")))

(define-public crate-bint-0.1 (crate (name "bint") (vers "0.1.5") (hash "1aby4030rjp5824d2fjfcn299w9888ym3i7pncin7zk5bc43rr1s") (rust-version "1.34")))

(define-public crate-binter-1 (crate (name "binter") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1b7x7h1jvz04yxhq5fnz9qv05qw7fw4d8iszah0b8cnvd3czyxgq")))

(define-public crate-bintest-0.0.0 (crate (name "bintest") (vers "0.0.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.13") (default-features #t) (kind 0)))) (hash "18rlskc4ycfq5095swpmdmadz4dng83bgm183q7ycg5zmylagdq5")))

(define-public crate-bintest-0.0.1 (crate (name "bintest") (vers "0.0.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.13") (default-features #t) (kind 0)))) (hash "1nwbvyzn9zzd5i1lwk3c5rdbaxniiaxjlr87nh0c9azixz8p3lv7")))

(define-public crate-bintest-0.0.2 (crate (name "bintest") (vers "0.0.2") (deps (list (crate-dep (name "cargo_metadata") (req "^0.13") (default-features #t) (kind 0)))) (hash "0wq6mgv88jv79jcyli4l2yn56vkbza61m0vzr8iwaj3cl4k0v6nz")))

(define-public crate-bintest-0.1 (crate (name "bintest") (vers "0.1.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.13") (default-features #t) (kind 0)))) (hash "1yqqbmw25njj7xr0lr4vv6n1ig2hkq4aij380z5r5z9g8d0gxiwk")))

(define-public crate-bintest-0.1 (crate (name "bintest") (vers "0.1.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.13") (default-features #t) (kind 0)))) (hash "0jzh9cqka56frsp3qigqlicwdbkz9nfim4vhxnz8h02pm0v72raa")))

(define-public crate-bintest-1 (crate (name "bintest") (vers "1.0.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.13") (default-features #t) (kind 0)))) (hash "0058mp33wkf8ws0gw0wnr6gmzwniba7q0dkmwnmv9fs8kwgr1xx5")))

(define-public crate-bintest-1 (crate (name "bintest") (vers "1.0.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.13") (default-features #t) (kind 0)))) (hash "1nm24h7lvsc0sy8yjk59sqjdwwcl7b452krwqw557wvg6z972cb4")))

(define-public crate-bintest-1 (crate (name "bintest") (vers "1.0.2") (deps (list (crate-dep (name "cargo_metadata") (req ">=0.13, <=0.15") (default-features #t) (kind 0)))) (hash "0zbnlg8sn917g2n6l3kf7iqxlp5zsgm9aj5y8gcny7sj59insj69")))

(define-public crate-bintest-1 (crate (name "bintest") (vers "1.0.3") (deps (list (crate-dep (name "cargo_metadata") (req ">=0.13, <=0.16") (default-features #t) (kind 0)))) (hash "1dbw1ap8xkkfbgqhdb26xg7blwx91d1x9nwfvd35lnryh5b7z7jn")))

(define-public crate-bintest-2 (crate (name "bintest") (vers "2.0.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "1av5vvqhcx8617brfpgqg40ziww3klalchgflsnqd4j2s51mrk7j") (rust-version "1.70.0")))

(define-public crate-bintest_helper-0.1 (crate (name "bintest_helper") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0p17y31179gc7kk15hmwpxcx1gkjl1r2njqb78ynz4xhqdkripj0")))

(define-public crate-bintex-0.1 (crate (name "bintex") (vers "0.1.0") (deps (list (crate-dep (name "bintex_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "deku") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "09jrkqj6x8gya5qasxmf3a4b087bq8b5d33df3i1lbiq470q69nc")))

(define-public crate-bintex_derive-0.1 (crate (name "bintex_derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "deku") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "04fdgwldil9lvdfj5d98cykav9qxfnz4ls2126ikyggjshswbnv4")))

(define-public crate-bintext-0.1 (crate (name "bintext") (vers "0.1.0") (deps (list (crate-dep (name "base16") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 2)) (crate-dep (name "core_affinity") (req "^0.5.10") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "radix64") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0696l6rd4mg9q1zk2k528cw4b2k3pi9rwh2x1dzy6aa3klknjm0j") (yanked #t)))

(define-public crate-bintext-0.1 (crate (name "bintext") (vers "0.1.1") (deps (list (crate-dep (name "base16") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 2)) (crate-dep (name "core_affinity") (req "^0.5.10") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "radix64") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "09vy6s5in6mnhc2i8md67yy60lhsgmisd41h2rl2qxiyilgqpj52")))

(define-public crate-bintext-0.1 (crate (name "bintext") (vers "0.1.2") (deps (list (crate-dep (name "base16") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 2)) (crate-dep (name "core_affinity") (req "^0.5.10") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "radix64") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "05sir9rimx52rd5hmgb2zxp8n3r904aygdifl3cywcd2rls2qicn")))

(define-public crate-bintext-0.1 (crate (name "bintext") (vers "0.1.3") (deps (list (crate-dep (name "base16") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 2)) (crate-dep (name "core_affinity") (req "^0.5.10") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "radix64") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0j6ivlbx5yb0z6k910zlkw2fpds57917qnkwy0vkc4gbhpmqry7k")))

(define-public crate-bintree-0.1 (crate (name "bintree") (vers "0.1.0") (hash "1n8px1kxscqsbh589c1ybq25x72banw805v17kbq3657vi5f415x")))

(define-public crate-bintrie-0.1 (crate (name "bintrie") (vers "0.1.0") (hash "0aj0q60wq00h297lqzs81ippaizpgfrfmmy8nmwv69nlixr9i0cj")))

(define-public crate-bintrie-0.2 (crate (name "bintrie") (vers "0.2.0") (hash "01j9yx0835aq4ps3yix6yccdyvp0mf9ylx32sfvcr6vv11bwjpyk")))

