(define-module (crates-io bi sy) #:use-module (crates-io))

(define-public crate-bisync-0.1 (crate (name "bisync") (vers "0.1.0") (hash "1yxzmrv1nc56mrraka3r1g4674ply5588r1lpy3avndr87zf99ya")))

(define-public crate-bisync-0.2 (crate (name "bisync") (vers "0.2.0") (deps (list (crate-dep (name "bisync_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16lsij82gg5vrj657nlnilgc7wc8lzlwwi8977hlw13kl57k009z")))

(define-public crate-bisync-0.2 (crate (name "bisync") (vers "0.2.1") (deps (list (crate-dep (name "bisync_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1d9xj3r0fvlhyy9kbgciah6qqp019nfzjvf267vfp7qg5gb59djp")))

(define-public crate-bisync-0.2 (crate (name "bisync") (vers "0.2.2") (deps (list (crate-dep (name "bisync_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "13gd049l5i83f35k91izp1kl4w9ydaazbkvphqpyvnma9ky7i9hn")))

(define-public crate-bisync_macros-0.2 (crate (name "bisync_macros") (vers "0.2.0") (hash "1bgginlzpjw4ki9d3jjs7kplc6dsmg1g3b8j1wvj6m97gigqbyiq")))

