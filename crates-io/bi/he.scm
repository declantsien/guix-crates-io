(define-module (crates-io bi he) #:use-module (crates-io))

(define-public crate-biheap-0.1 (crate (name "biheap") (vers "0.1.0") (hash "1z1rdwsz4rzpmhnvi9wjs8bhyc0xi1i26fnygdzl6s3hbq0gsz9s")))

(define-public crate-biheap-0.1 (crate (name "biheap") (vers "0.1.1") (hash "1zivzd554v51jddjrbv4f0b2blgf88y751a31vw8cr4a9h90xbjv")))

(define-public crate-biheap-0.2 (crate (name "biheap") (vers "0.2.1") (hash "1rcwxm4gai603kwsmgb4fl5p9dymbnjnrlv4siinbmjr7zsrrnpm")))

(define-public crate-biheap-0.2 (crate (name "biheap") (vers "0.2.2") (hash "13smsn7hj5a9b62zfggyr0xddg1if8rj94s0ym86jysyq46yx9rj")))

(define-public crate-biheap-0.3 (crate (name "biheap") (vers "0.3.1") (hash "0vmv2pi11mpvhhzf635wmvridxxnxjsqnk5bsb7ll7120mz827y4") (features (quote (("threadsafe"))))))

(define-public crate-biheap-0.3 (crate (name "biheap") (vers "0.3.2") (hash "0bba1g60dacgrxgq34jydf3qvszwmyk8s96nn1qbvmb7k0j9dfg3") (features (quote (("threadsafe"))))))

(define-public crate-biheap-0.3 (crate (name "biheap") (vers "0.3.3") (hash "087nv9l1wccx8c0rk3ck67kdrzdd1vjkj5k2h3gy9ir9imj445l8") (features (quote (("threadsafe"))))))

(define-public crate-biheap-0.3 (crate (name "biheap") (vers "0.3.4") (hash "14311m9l0vy20cdqvh4x4070bhybhlzkxd3qk5m4d31rrvk4ldf1")))

(define-public crate-biheap-0.3 (crate (name "biheap") (vers "0.3.5") (hash "1cl9nyzx2fq5zhpz0jnzagx3h11lyclm2a7zkkqkjlnllayfz73q")))

(define-public crate-biheap-0.4 (crate (name "biheap") (vers "0.4.1") (hash "19wj5phd8hn1715q8kgg5pgcs8ylq310l3p10kksgp4psm8zv1p1")))

(define-public crate-biheap-0.4 (crate (name "biheap") (vers "0.4.2") (hash "08qdjyyr7v44dy66i9njzk684jh4x0afx1y940vr48g95nvgiw2a")))

(define-public crate-biheap-0.5 (crate (name "biheap") (vers "0.5.1") (hash "00abzmpjgim2prk8j2liv9r0j172dsi0fvygvikwggx82qchv6p3") (features (quote (("threadsafe"))))))

