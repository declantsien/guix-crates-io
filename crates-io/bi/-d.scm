(define-module (crates-io bi -d) #:use-module (crates-io))

(define-public crate-bi-directional-pipe-0.1 (crate (name "bi-directional-pipe") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1arsk3qxwpqjk105jvfdfqaxii7w49y76bv9vm6gxk9v5a2rmrm8")))

(define-public crate-bi-directional-pipe-0.1 (crate (name "bi-directional-pipe") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0g0ddhcvywrl5q82zqbpcmn9sg3wvy5cb8sxlk653dfdzmnxx9si")))

(define-public crate-bi-directional-pipe-0.1 (crate (name "bi-directional-pipe") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1w6yhrjs3ljilqdj0a0za7x12lyl9f9ach9y39mavbgsz22phd19")))

(define-public crate-bi-directional-pipe-0.1 (crate (name "bi-directional-pipe") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "175ha2vchx04p6injm6hrcid33zgsbv6hfr4xnkhwjchf0j23rzd")))

(define-public crate-bi-directional-pipe-0.1 (crate (name "bi-directional-pipe") (vers "0.1.4") (deps (list (crate-dep (name "atomic-waker") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0wjrh3a3d6ig1nyxvvwn8gc46z0mwn4mqinpvriik4ijmj0z00g5")))

