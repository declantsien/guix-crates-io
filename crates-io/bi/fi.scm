(define-module (crates-io bi fi) #:use-module (crates-io))

(define-public crate-bifid-1 (crate (name "bifid") (vers "1.0.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wsx80x45ynwablcrbhzz28c0m56w26xzngpkl8d20vkb2gcwqac")))

