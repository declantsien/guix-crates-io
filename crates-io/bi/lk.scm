(define-module (crates-io bi lk) #:use-module (crates-io))

(define-public crate-bilk-0.0.1 (crate (name "bilk") (vers "0.0.1") (hash "0ssjx1h2v2bnsy87cqix7h7vazgdhis7fy1kqprl3q5i60k4m5z8")))

(define-public crate-bilk-0.0.2 (crate (name "bilk") (vers "0.0.2") (hash "12a0j722i13gh44133ac27vdigyg2ndvxjqmpi6bg6jr3sxah5cv")))

