(define-module (crates-io bi gc) #:use-module (crates-io))

(define-public crate-bigcat-0.1 (crate (name "bigcat") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "09wn3pcb3q5hrkphwk8sxvpxpnzr97wclzli6d16v25zcgkriza0")))

(define-public crate-bigcat-0.1 (crate (name "bigcat") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0vcvr2722qa57mypvlpfcdggaz6lw4zlyndf93rb8az365mzjlgs")))

(define-public crate-bigcat-0.1 (crate (name "bigcat") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0sw362nhpficbywgx3hd39x085wiqb6q7d5zqilf42i33rq6aa66")))

(define-public crate-bigcollatz-0.1 (crate (name "bigcollatz") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.3") (features (quote ("rand" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1gr5lkg7gq91wqkrvxcmhamv1v6kzhbkdqq6c20rq4l2m9g5hszb")))

(define-public crate-bigcollatz-0.2 (crate (name "bigcollatz") (vers "0.2.0") (deps (list (crate-dep (name "num-bigint") (req "^0.3") (features (quote ("rand" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "155vmim1djmjy243i9bkklvbbblx7k95m3dkfzghgcadja3vdcp6")))

(define-public crate-bigcollatz-0.2 (crate (name "bigcollatz") (vers "0.2.1") (deps (list (crate-dep (name "num-bigint") (req "^0.3") (features (quote ("rand" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1nk5g1cd3w9m0sx60f3sd2bhycncsl96fj2svjvbpif9fq5hb36p")))

(define-public crate-bigcollatz-0.3 (crate (name "bigcollatz") (vers "0.3.0") (deps (list (crate-dep (name "num-bigint") (req "^0.3") (features (quote ("rand" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0m6hsaswsa3j9k753wj935js2hg1pm8w7mx7ms779qrybzh0qhr1")))

