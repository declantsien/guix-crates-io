(define-module (crates-io bi qu) #:use-module (crates-io))

(define-public crate-biquad-0.1 (crate (name "biquad") (vers "0.1.0") (hash "1cvjl3dqhr66ilgwcs43mvmdil9hn49lyp2bf9kzci37qswkyydb")))

(define-public crate-biquad-0.2 (crate (name "biquad") (vers "0.2.0") (hash "19w5pbzhr3sl0dsc8hi8df4nicpsfl50bbk2ddxy10aqasn8gz4a")))

(define-public crate-biquad-0.3 (crate (name "biquad") (vers "0.3.0") (deps (list (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0ia44nz025zxm7z2hqnx3xzh768157lq9l5zhgz95lxmib4w35qs")))

(define-public crate-biquad-0.3 (crate (name "biquad") (vers "0.3.1") (deps (list (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0kz1rrcriyq4v3grllj0r5q974nbybrjqid17y9daxd1hd95qd0m")))

(define-public crate-biquad-0.4 (crate (name "biquad") (vers "0.4.0") (deps (list (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0q4khlxqi1nk2n328czqpqcyx1qy6hg4hkqzxdcl153n00w4zba0")))

(define-public crate-biquad-0.4 (crate (name "biquad") (vers "0.4.1") (deps (list (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1sps9irdmpyzw1lhrrj8a86790ysg8w8j4g33n4r28sn53w70xq8")))

(define-public crate-biquad-0.4 (crate (name "biquad") (vers "0.4.2") (deps (list (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0gpc13lag439nmq077wfwz055qbjaxbpk7znvnbddbg3wgsj81c2")))

