(define-module (crates-io bi ku) #:use-module (crates-io))

(define-public crate-bikuna-0.1 (crate (name "bikuna") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dp7h3j1cm72jfzs3f5pag32m0g5vbrff7d2py07d6zs6sivji03")))

(define-public crate-bikuna-0.1 (crate (name "bikuna") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c14dgir7a1ri5jiiibdr215ph5hswgi157hkpy3vzy71l764gh9")))

