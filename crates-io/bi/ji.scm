(define-module (crates-io bi ji) #:use-module (crates-io))

(define-public crate-biji-ui-0.1 (crate (name "biji-ui") (vers "0.1.0") (deps (list (crate-dep (name "leptos") (req "^0.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "leptos-use") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "=0.2.92") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (default-features #t) (kind 0)))) (hash "0xl60j76sg538p2bvmi6b4haw0d53r4dj2sjd6gq5i634znwk3wv")))

(define-public crate-biji-ui-0.1 (crate (name "biji-ui") (vers "0.1.1") (deps (list (crate-dep (name "leptos") (req "^0.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "leptos-use") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "=0.2.92") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (default-features #t) (kind 0)))) (hash "1pgr7zr25i2g6falx8lv16z74dq7hmhi46hzyf33xbm6wkkqglpf")))

(define-public crate-biji-ui-0.1 (crate (name "biji-ui") (vers "0.1.2") (deps (list (crate-dep (name "leptos") (req "^0.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "leptos-use") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (default-features #t) (kind 0)))) (hash "0wd844ayi2bzsa766pas7x3x1mvxxcr87cz61dprvxjw58lpjf9s")))

