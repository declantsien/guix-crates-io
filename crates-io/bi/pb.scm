(define-module (crates-io bi pb) #:use-module (crates-io))

(define-public crate-bipbuffer-0.1 (crate (name "bipbuffer") (vers "0.1.0") (hash "1dghi85klb20h0fq8yf3ck6fjxxs3gcgqh0rmpxpgq8qk4bdlvxk")))

(define-public crate-bipbuffer-0.1 (crate (name "bipbuffer") (vers "0.1.1") (hash "0hzcxskwq4ynpjnw3zm86kybwbxkbqranxkibcvrzfafgsj390xa")))

(define-public crate-bipbuffer-0.1 (crate (name "bipbuffer") (vers "0.1.2") (hash "0c9v6plpibbc924zqbyh091m6r8jb8y496pgqxjmm08avi5f6cwf")))

