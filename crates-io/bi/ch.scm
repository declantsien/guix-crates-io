(define-module (crates-io bi ch) #:use-module (crates-io))

(define-public crate-bichannel-0.0.1 (crate (name "bichannel") (vers "0.0.1") (hash "03nv1mdhy0107q7m4qzmx6aayfmjdxcw6in5k2rxcaykkh3prcf1")))

(define-public crate-bichannel-0.0.2 (crate (name "bichannel") (vers "0.0.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0hpnv3ry6z8wyd6n6zak4x0jx5b79g87a0brp985ryp02ijvzr76") (features (quote (("crossbeam" "crossbeam-channel"))))))

(define-public crate-bichannel-0.0.3 (crate (name "bichannel") (vers "0.0.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "11s9fjshbaxmff5wg5a5z2jmnnkc84lbkzxw2ji6y02fdsv81bck") (features (quote (("crossbeam" "crossbeam-channel"))))))

(define-public crate-bichannel-0.0.4 (crate (name "bichannel") (vers "0.0.4") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0fr19i46q0qs5vmidpfjhyl3n4mdhjmyvyyy9n9ndl9n7m3zbgd4") (features (quote (("crossbeam" "crossbeam-channel"))))))

