(define-module (crates-io bi ma) #:use-module (crates-io))

(define-public crate-bimap-0.1 (crate (name "bimap") (vers "0.1.0") (hash "1sh432p0717r54kx31hg38gv6l7iy8hyi42gpq2w2wa2bwbgzhp4")))

(define-public crate-bimap-0.1 (crate (name "bimap") (vers "0.1.1") (hash "0dgs1ff3hsfpqydwq3ym48sh231q28s5dsmzifd0zczv6cy11fy0")))

(define-public crate-bimap-0.1 (crate (name "bimap") (vers "0.1.2") (hash "1n4vykhz4bsrlgyg5040ylqqkybxkz9l5k93adnpa1q988448mgg")))

(define-public crate-bimap-0.1 (crate (name "bimap") (vers "0.1.3") (hash "01ifbwgr1l4jygmna56axw6l4ki19vhid6xb24si5l750smimamh")))

(define-public crate-bimap-0.1 (crate (name "bimap") (vers "0.1.4") (hash "0l7sh25586cjyrbl2bjmh7yimgrchnd80x8c5ndgvkgwxikq5in7")))

(define-public crate-bimap-0.1 (crate (name "bimap") (vers "0.1.5") (hash "0g9xj8qzv5hbnyafg9mrm3par1gik0hqm50sqvx8n1rp4ac2na3b")))

(define-public crate-bimap-0.2 (crate (name "bimap") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rljijs8cxhgxw7v3s1c5c164gnqybnqbs4gaksn450amyz4apjh") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.3 (crate (name "bimap") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1dcfl3jd9k73gwbgpi6aycbl4ckmzcm8g1ni8pp9zcgwgi5a73jj") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.3 (crate (name "bimap") (vers "0.3.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "07wpz69nw2fa1x4nmxayhmm3yv8snp798qcj4wm3kjifqzjpfqa4") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.4 (crate (name "bimap") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "06srhxkzbxnzm9l1lmmhgc21hq3azaf629ykfji4wwnp9zr08ckq") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5 (crate (name "bimap") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1z5d3l25q7s6wm4ag7aprbxs2jlhan47926b08hp6lsvi8cv4767") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5 (crate (name "bimap") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "12gysa3hfx755sznzy2f6631b9a6rv7s90drpz7y58x6yrh8j8la") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5 (crate (name "bimap") (vers "0.5.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "10g765qns7hr14xgqgwiia6fwnh7n2kkb7y19bw8xv1xrpb6m023") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.5 (crate (name "bimap") (vers "0.5.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "175kym82ibwazg69s3dg98dyrjpmbgf4vnh46pfsmchyz2vc8a1m") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6 (crate (name "bimap") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1grj1hwsyisz7691z9ir5jhx5wq5ja5l2x5zg0r7fa1iy2w74azr") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6 (crate (name "bimap") (vers "0.6.1") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "01vb8id3lpc6ipx9c22dq4nnf2cs9rk6l6jc7qg8m8y8pg51gbjh") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6 (crate (name "bimap") (vers "0.6.2") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0jyl53bi572kxaayc2py797vx8g12p4bmn258m69dimm9qjma15w") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap-0.6 (crate (name "bimap") (vers "0.6.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1xx4dns6hj0mf1sl47lh3r0z4jcvmhqhsr7qacjs69d3lqf5y313") (features (quote (("std") ("default" "std"))))))

(define-public crate-bimap_plus_map-0.1 (crate (name "bimap_plus_map") (vers "0.1.0") (deps (list (crate-dep (name "bimap") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0brd62xxd1xxlpr03h6jh76m5v395r2y8lansfqaw4k6rv6mb2vy")))

(define-public crate-bimap_plus_map-0.1 (crate (name "bimap_plus_map") (vers "0.1.1") (deps (list (crate-dep (name "bimap") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "001q3sji4wh8k5fb96vqc4gvmp37hnni3sgdyzl8z84csy2qryyn")))

