(define-module (crates-io bi n_) #:use-module (crates-io))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.0") (hash "1zncq17h35vi3d0plfr80gqp27mxk083b29w9lky2b7s4v4kdi8q") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.1") (hash "0v09l9i3gzjk78058ymlcshcsaji9ykssx90bdssadygsfii0kig") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.2") (hash "058njn5g2zhcjwnl8xqf5y5bggvrsy0y000q3b5zn9hcdadn3ral") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.3") (hash "04y6af2il794qwjccdw8p6nma364sqikv40hfxjxay05v6370721") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.4") (hash "0l0v8wq8p4dbg717mx9rqfndxs5lvi9n8mnplqxwrss9m39ch3h7") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.5") (hash "1ndy1fjprir3jil00ljx3v624nvkmj3s27yqnb50n6357rb91rv9") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.6") (hash "07ki8zx9lw6zlbpw7ylb2s254bpb9n8hzv6xxcyrdz38k0lnca1m") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.7") (deps (list (crate-dep (name "fnrs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1kdlfrsyk9mlqvhjjfs8cm3r9bb6k9jg450dzjdqhdyk3jilb73c") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.8") (deps (list (crate-dep (name "fnrs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "13n2xpr64r246m8d19czbggsrqyf6zmcc2mb2120qk55lsjcd1bc") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.9") (deps (list (crate-dep (name "fnrs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1wclmg19hpp93l03x5lmi79p8xfxl7pnrddykx534qkp83pqc93p") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.10") (hash "1b1kpz8f62zpasmn9gc3hnll572mrpxqqfb8hdvp1rnwkbiny8kc") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.11") (hash "1kcv5nl0r06h58zgd9j2az0q3rfyc8j2597q4kr2siylhhb2vja6") (yanked #t)))

(define-public crate-bin_buffer-0.1 (crate (name "bin_buffer") (vers "0.1.12") (hash "0wgim7sp2daxh6yjshkpbg5w1lrydibb751dnl52cs3n86hws0gd")))

(define-public crate-bin_chicken-0.1 (crate (name "bin_chicken") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^2.1.5") (default-features #t) (kind 0)))) (hash "1m2mms43f5i4c1pp4vzrwy2n7mj55wmzvbn67zavy6ggbr0ifydw")))

(define-public crate-bin_codec-0.1 (crate (name "bin_codec") (vers "0.1.0") (hash "1ja12f8yv6nlmakgadhgmi5xrr9jhrlclqf770c5q9ib1x1dhdg6")))

(define-public crate-bin_codec_derive-0.1 (crate (name "bin_codec_derive") (vers "0.1.0") (deps (list (crate-dep (name "bin_codec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0qbrzf6kf8awvmf16f9wdqlsdawi16bkhd2askwh15q5wbcgmycc")))

(define-public crate-bin_common-0.2 (crate (name "bin_common") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (features (quote ("cpp_demangle"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6") (features (quote ("colored"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1j2q90cz049yfr5vh6rqki9hfbmavp6am0d9ny4f99c8m8p4xrff") (features (quote (("panic_handler" "backtrace") ("log_rotation" "xz2") ("default" "log_rotation" "panic_handler"))))))

(define-public crate-bin_crate-0.1 (crate (name "bin_crate") (vers "0.1.0") (hash "1djrqwklcxnsv73a0qnz87dw9d7cfi7h8z3mnzkfw4wip9vcnaxy")))

(define-public crate-bin_crate-0.1 (crate (name "bin_crate") (vers "0.1.1") (hash "1b9wm8xv9x9wkjkd24b6vpf0ryd0lmnc8q1mdmf1fsrfkv925295")))

(define-public crate-bin_file-0.1 (crate (name "bin_file") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1fvq8nisl5v7x1xx1mklb1hydvzksjiir71dwfvx23laf259y01g") (features (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-bin_file-0.1 (crate (name "bin_file") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qb3s201nkwkzjigzxb50psp0bf9jmylxavnn6np4hwvv1rai4k9") (features (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-bin_io-0.1 (crate (name "bin_io") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)))) (hash "1p7cb765r2179gizjllhyg0sxbls4i6nmnmwvp55ppn8991p24ig")))

(define-public crate-bin_io-0.1 (crate (name "bin_io") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)))) (hash "11xi53l9m61ipyjsa3hn1zdj3p0g1mm92646y6vnjy64gplckdhj")))

(define-public crate-bin_io-0.1 (crate (name "bin_io") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)))) (hash "049sxvzw1ry19n8y6a9371zcr4a3zs6zyph0aabb4a1xd06xxwav")))

(define-public crate-bin_io-0.2 (crate (name "bin_io") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)))) (hash "16yppfhln6bjml7c5ki8drf95176hj6x233b00p8l4g89py6anwh")))

(define-public crate-bin_packer_3d-0.1 (crate (name "bin_packer_3d") (vers "0.1.0") (hash "1na9phygc4890f4d670fnwhj313i4gap1j8z3np9cqslar7hdyjf")))

(define-public crate-bin_packer_3d-0.1 (crate (name "bin_packer_3d") (vers "0.1.1") (hash "1ir6bvvnxb8khynzbl88p8yxrhx15w2y7gs82hnl9npk2wamxa45")))

(define-public crate-bin_packer_3d-0.1 (crate (name "bin_packer_3d") (vers "0.1.2") (hash "0lgmzcxlhsjnflvx8pynkp5264m7d6mn4qfxww0jp0s0qf7y58fd")))

(define-public crate-bin_packer_3d-1 (crate (name "bin_packer_3d") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "09xgxxq354kav1ljir3dglp7gbmi94vy70m0mdlmv20pv4f553cr")))

(define-public crate-bin_packer_3d-2 (crate (name "bin_packer_3d") (vers "2.0.0-beta-1") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0sqdkzix79s0h1w7rrf6q8wib26xxddcffa7im1n18103p6lcagx")))

