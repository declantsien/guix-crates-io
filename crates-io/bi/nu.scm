(define-module (crates-io bi nu) #:use-module (crates-io))

(define-public crate-binutils-0.1 (crate (name "binutils") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "1q7zm4a6k213b1lkf2aqdyxsyrsdxdvrbz796lisj13zkvgwq73n")))

(define-public crate-binutils-0.1 (crate (name "binutils") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "030fvv6grh7c7chdwn78377jnk3nq3cs4wwm4cvcx06f5j1yrxyi")))

