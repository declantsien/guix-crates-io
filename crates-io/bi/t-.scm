(define-module (crates-io bi t-) #:use-module (crates-io))

(define-public crate-bit-array-0.4 (crate (name "bit-array") (vers "0.4.3") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.3") (default-features #t) (kind 0)))) (hash "1jx3mwmqiqzb33hgg6xrx3n1mgy6yr7ml6pqh4pagl6diazghdvx") (features (quote (("nightly"))))))

(define-public crate-bit-array-0.4 (crate (name "bit-array") (vers "0.4.4") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.3") (default-features #t) (kind 0)))) (hash "068asaggf0n00577rvcd3lffah8vg8iw1zw56jqnk8xssj3ls4l7") (features (quote (("nightly"))))))

(define-public crate-bit-bounds-0.1 (crate (name "bit-bounds") (vers "0.1.0") (deps (list (crate-dep (name "const-assert") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0pnxg0iccxsp6f7c0nq8pilxnbngg8rzx71f8n82mbi7dbwdl3ya")))

(define-public crate-bit-bounds-1 (crate (name "bit-bounds") (vers "1.0.0") (deps (list (crate-dep (name "const-assert") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "089y5badd4524kmdijryqfysd0vg7ymbd6b239vzb5b6na6ir9f8")))

(define-public crate-bit-by-bit-0.1 (crate (name "bit-by-bit") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0irsj8aw3xkggcz9m3mks0prs89l75xzbypvrv60zqhmzn72ss59")))

(define-public crate-bit-byte-structs-0.0.3 (crate (name "bit-byte-structs") (vers "0.0.3") (deps (list (crate-dep (name "cortex-m") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "051qy7qhsp2j0vq826gvvsps3vqrhg2rs4fk48lfq2xac8lfylq4") (features (quote (("cortex-m-debuging" "cortex-m-semihosting" "cortex-m"))))))

(define-public crate-bit-cursor-0.1 (crate (name "bit-cursor") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0iqhjrnd73z9kxic4znvl21zkdzh5jw0a8r1lgm1plr50wpfjwxa")))

(define-public crate-bit-index-0.1 (crate (name "bit-index") (vers "0.1.0") (hash "1mcbn2mcbl1jjwwrja4bgaa9gaj2bgzhhrg3kl8r34yvaij0l100") (yanked #t)))

(define-public crate-bit-index-0.2 (crate (name "bit-index") (vers "0.2.0") (hash "12664n52z4y25znacipw0z7byxhwzcqw4m5zknxchl4ccg0x9m3l")))

(define-public crate-bit-io-0.1 (crate (name "bit-io") (vers "0.1.0") (hash "0njwz0b14b8sda5ia378kz2ccb264vfwawy1jaq85m8mlfsc887w")))

(define-public crate-bit-iter-0.1 (crate (name "bit-iter") (vers "0.1.0") (hash "1h1idx8s70r9hlm478924ib3w2hihx5g58qblrdfdljhr2a5ghh3") (yanked #t)))

(define-public crate-bit-iter-0.1 (crate (name "bit-iter") (vers "0.1.1") (hash "0q2v2dawmg437iw0dzsnk9jykq4di1w663jdkz6v0c0qs01sg4vk") (yanked #t)))

(define-public crate-bit-iter-0.1 (crate (name "bit-iter") (vers "0.1.2") (hash "0c7bn0gj602jlpai37b632lfpgr6jivhiz2qihmqn7jnfca9y9rs") (yanked #t)))

(define-public crate-bit-iter-0.1 (crate (name "bit-iter") (vers "0.1.3") (hash "1qgb421gg0ikld5ri636vhb90jpf6z4ivcn19zxl0fvhfls36r3q") (yanked #t)))

(define-public crate-bit-iter-0.1 (crate (name "bit-iter") (vers "0.1.4") (hash "184iii48d7rzgsh0brmkr6flhfskrc7c6r6x9vbv0zyhkwhmmhdj") (yanked #t)))

(define-public crate-bit-iter-0.1 (crate (name "bit-iter") (vers "0.1.5") (hash "0lvrvxr9cv81apch33blj94x1pajzrl8c6qqr2knmrgpsdkvsjji")))

(define-public crate-bit-iter-0.1 (crate (name "bit-iter") (vers "0.1.6") (hash "05v1nnvgdrf14bqdc7f0rvavn19jdlhqxqfqyh5xkss144n7rgnp")))

(define-public crate-bit-iter-1 (crate (name "bit-iter") (vers "1.0.0") (hash "18p3zadpkb7rgz82aqpsbmvqfxfm9prmy2inb84p776d56wj17j9")))

(define-public crate-bit-iter-1 (crate (name "bit-iter") (vers "1.1.0") (hash "0mi3k8c24w4r1829ax9pkv8hrnrdrw4zfysa4vk9rzhhhwlya9j8")))

(define-public crate-bit-iter-1 (crate (name "bit-iter") (vers "1.1.1") (hash "14gf41fcxdsgw9rnkgshk8zmlnaygy36a676fvbcac8611vpl1i3") (rust-version "1.33.0")))

(define-public crate-bit-iter-1 (crate (name "bit-iter") (vers "1.2.0") (hash "11zbpqlwg6p39cx966z49gqkrqbxpbfzfkjq7i7whc9rxsbc7hw0") (rust-version "1.53.0")))

(define-public crate-bit-list-0.1 (crate (name "bit-list") (vers "0.1.0") (deps (list (crate-dep (name "list-fn") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1yjglyg4jhb4vldldhm4vpa9xlw45zb80iinpagvg9vaa6c47awj")))

(define-public crate-bit-list-0.1 (crate (name "bit-list") (vers "0.1.1") (deps (list (crate-dep (name "list-fn") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0k7zbnlbjjcb3q7465z16hkj9babc529sgds6jrh8p3p21c15ahy")))

(define-public crate-bit-list-0.2 (crate (name "bit-list") (vers "0.2.0") (deps (list (crate-dep (name "list-fn") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04y4b1py07096qxvjzlc9k11jkdz4302j1v1a9pncv0wcf7cr24p")))

(define-public crate-bit-list-0.2 (crate (name "bit-list") (vers "0.2.1") (deps (list (crate-dep (name "list-fn") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1f3x8z86lh199sjf55zdn9r9rp0gx6pnk950adqc0768g74rhwq1")))

(define-public crate-bit-list-0.2 (crate (name "bit-list") (vers "0.2.2") (deps (list (crate-dep (name "list-fn") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0jdhl853hl30kjgzlz433nskramqx5z1sd536vypnmcka51s0v34")))

(define-public crate-bit-list-0.3 (crate (name "bit-list") (vers "0.3.0") (deps (list (crate-dep (name "list-fn") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "17m5md9vc2qbzz2rcxhzf7gqkfcq51y13a68kag4hjqszx2frhsb")))

(define-public crate-bit-list-0.4 (crate (name "bit-list") (vers "0.4.0") (deps (list (crate-dep (name "list-fn") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0qm4vvj89533cm40hnjsrmmnl3h1rx3yxq2r4bkflnw8gkw3261b")))

(define-public crate-bit-list-0.5 (crate (name "bit-list") (vers "0.5.0") (deps (list (crate-dep (name "list-fn") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1jwyixf69y1xvx72i9nyabkkir4nqyp81wr3h8182vwxz8vky80k")))

(define-public crate-bit-list-0.6 (crate (name "bit-list") (vers "0.6.0") (deps (list (crate-dep (name "list-fn") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1k3qdybbqm1xg1bkq9vd1l8cgnc3m5rpggaxlrn3qi4kx5dzqrr4")))

(define-public crate-bit-list-0.7 (crate (name "bit-list") (vers "0.7.0") (deps (list (crate-dep (name "list-fn") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0jkyar5hisy9q0dc3cpi8s0fvh5vfc45n6sl863vifc5dlx75pg6")))

(define-public crate-bit-list-0.8 (crate (name "bit-list") (vers "0.8.0") (deps (list (crate-dep (name "list-fn") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0b310pmfsaq4hgmwhb939jar7bk15v4zz5zjqgbgndrhs86kwv4y")))

(define-public crate-bit-list-0.9 (crate (name "bit-list") (vers "0.9.0") (deps (list (crate-dep (name "list-fn") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1y8q834lddjknapxjfan6xf2vxgsp73pbnf8jqj3mfb6w559mp6m")))

(define-public crate-bit-list-0.10 (crate (name "bit-list") (vers "0.10.0") (deps (list (crate-dep (name "list-fn") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0aqgnizb8bkg9rrm99vrh13vgk37d6s796aipy79ag5rlsh5b28g")))

(define-public crate-bit-list-0.10 (crate (name "bit-list") (vers "0.10.1") (deps (list (crate-dep (name "list-fn") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0z5qc5z9v0x5mb71vsm5y5vp28cxgyfgsmqd7mipwmpfcfmrr217")))

(define-public crate-bit-list-0.11 (crate (name "bit-list") (vers "0.11.0") (deps (list (crate-dep (name "list-fn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0zmp4s8m4ghpjnxzzcljv2rc102swcs4w5jhx1fwj4nrswfr637p")))

(define-public crate-bit-list-0.11 (crate (name "bit-list") (vers "0.11.1") (deps (list (crate-dep (name "list-fn") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ibl4i6l98srwi1vvpvawl5cn9wa4867c52fn46r2y2lragrks8g")))

(define-public crate-bit-list-0.12 (crate (name "bit-list") (vers "0.12.0") (deps (list (crate-dep (name "list-fn") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0zb06038539p86bida4z2h4h29azyys32gpjl85q4g7cj0r0n1v0")))

(define-public crate-bit-list-0.12 (crate (name "bit-list") (vers "0.12.2") (deps (list (crate-dep (name "list-fn") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "16s990hg8dyl5wnqd0lxcp6qkdj48dkiwywrr8jjf8z3zfqcbybv")))

(define-public crate-bit-list-0.12 (crate (name "bit-list") (vers "0.12.3") (deps (list (crate-dep (name "list-fn") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "055qxscrng4al4j6m79j6jz4q0vs8k27bls9hrg1p464nhl5q93s")))

(define-public crate-bit-list-0.12 (crate (name "bit-list") (vers "0.12.4") (deps (list (crate-dep (name "list-fn") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "131i4kxi1vqf1wl6yn0c1j3av0ij676nnh697x3sp7irjc1xdgs5")))

(define-public crate-bit-list-0.12 (crate (name "bit-list") (vers "0.12.5") (deps (list (crate-dep (name "list-fn") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1mi3iv5h0x617fppfx3m9l8nzmjdv2zwwi887ram2yzvhdbn0fw3")))

(define-public crate-bit-list-0.12 (crate (name "bit-list") (vers "0.12.6") (deps (list (crate-dep (name "list-fn") (req "^0.11.5") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0cq3j9qdx395zfh0ymkxwb69vfvgad3dcvff219yry8kbbi2zwi2")))

(define-public crate-bit-list-0.13 (crate (name "bit-list") (vers "0.13.0") (deps (list (crate-dep (name "list-fn") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1k3iv5n52is9f673vgpw3lrgc4kj2d4hx63cmvgkdj62b2195haj")))

(define-public crate-bit-list-0.14 (crate (name "bit-list") (vers "0.14.0") (deps (list (crate-dep (name "list-fn") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1px0rla222i465ylxs0w25y6izp8miia1c959mmxbbpxhnz8x50q")))

(define-public crate-bit-list-0.15 (crate (name "bit-list") (vers "0.15.0") (deps (list (crate-dep (name "list-fn") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "16jf5fli6cppzh30k9cgcgqj3him30d87j9alpn2hnpqlpm09in1")))

(define-public crate-bit-list-0.15 (crate (name "bit-list") (vers "0.15.1") (deps (list (crate-dep (name "list-fn") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "11nkrgzyrws2zsr63f7qgpvgys9jzjrs75kpyxncfakr9p61mhlr")))

(define-public crate-bit-list-0.16 (crate (name "bit-list") (vers "0.16.0") (deps (list (crate-dep (name "list-fn") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0fz6cxnfcvjknxdbrwk7vqpq4ckvxjdbjjwnbfvnrjxdspb14prx")))

(define-public crate-bit-list-0.17 (crate (name "bit-list") (vers "0.17.0") (deps (list (crate-dep (name "list-fn") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1gmpm3n5swj7fy7qldk907zcp2g9wacz05npfyg98a5mgw2w3qll")))

(define-public crate-bit-list-0.18 (crate (name "bit-list") (vers "0.18.0") (deps (list (crate-dep (name "list-fn") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "11cg5v96jydysc8lxnbnjw9d76497zqhlfw59wysg13f9yhvkkas")))

(define-public crate-bit-list-0.19 (crate (name "bit-list") (vers "0.19.0") (deps (list (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0501594mds19mvwpl1clhlpzir71p2ccgbrahafbny0az4igp634")))

(define-public crate-bit-list-0.19 (crate (name "bit-list") (vers "0.19.1") (deps (list (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "15hnn1s5n37y7wksa2ybrvlk9pbh7683vpay6wm32ggqyjf9ji0b")))

(define-public crate-bit-list-0.20 (crate (name "bit-list") (vers "0.20.0") (deps (list (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0k9151xw5y88gggw6yfv2yvgczf9lm4sv32j63sxziqy7jwjg5n6")))

(define-public crate-bit-list-0.20 (crate (name "bit-list") (vers "0.20.1") (deps (list (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "05djlbri254r650ja6glsag37llp54rrcisl8r9qlad5mad4sqar")))

(define-public crate-bit-list-0.21 (crate (name "bit-list") (vers "0.21.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0iwaaxgfvqwn1f235b8z69mnxvjc6h3d6n4v5jpx5nm536w6akwa")))

(define-public crate-bit-list-0.21 (crate (name "bit-list") (vers "0.21.1") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "16xyca3aa9r8hs5478gahknphx5z75ybrbdn055700gx60993nz0")))

(define-public crate-bit-list-0.21 (crate (name "bit-list") (vers "0.21.2") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "042dgrq3pxfb72flx3a05xppnjqgb1q4yx2g78ypcg8y2l4ssp3y")))

(define-public crate-bit-list-0.22 (crate (name "bit-list") (vers "0.22.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "105chfgxgpprv70kfwn7xp87iz6597d8c5mb03i1mly253s74fws")))

(define-public crate-bit-list-0.23 (crate (name "bit-list") (vers "0.23.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0gdvpa3kb5m2dqgqshlhrpkhq3c5n7ndakqwbq5sfgf0mcfqvak4")))

(define-public crate-bit-list-0.23 (crate (name "bit-list") (vers "0.23.1") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1cwrc9dljxx63lxjc08ldqpj3rvwyzhcbwny38lmqaz3945dvlzk")))

(define-public crate-bit-list-0.24 (crate (name "bit-list") (vers "0.24.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "14ywmc8jfvrml4sirh3sb6as0ahkcf7rw0cympkqkg3p9wyb69h2")))

(define-public crate-bit-list-0.24 (crate (name "bit-list") (vers "0.24.1") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "03y4g33gxg8m0bz9g8nmblbk10dyryk19la68cdiw0cmdzb84wx0")))

(define-public crate-bit-list-0.25 (crate (name "bit-list") (vers "0.25.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1hn3jd68f1pnazv93yqavj7hv80p2fmz3dvk1s7375brj0w17l36")))

(define-public crate-bit-list-0.25 (crate (name "bit-list") (vers "0.25.1") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "13k2b7n9p292m0rbgzdhqqgn5jsr3hmxn0cz9hssrs72bkg6q98g")))

(define-public crate-bit-list-0.26 (crate (name "bit-list") (vers "0.26.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0v420czap6ya5bg1skx5q9hnks44p8c6nnmp008sf3p4vm4xrvbr")))

(define-public crate-bit-list-0.27 (crate (name "bit-list") (vers "0.27.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0sjfg9sa425704i1bd5xknwpw3lbh05yqfcf2rsln6ili5wz7g7s")))

(define-public crate-bit-list-0.28 (crate (name "bit-list") (vers "0.28.0") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1z3yr7494iwir5ri1qr35c9kfrw1njr3rviwgw6jqbibp06x07vs")))

(define-public crate-bit-list-0.28 (crate (name "bit-list") (vers "0.28.1") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0p1m16by8604jiqdhy976skc20ak5qwmmxb0bs4ng8dkld494h6p")))

(define-public crate-bit-list-0.28 (crate (name "bit-list") (vers "0.28.2") (deps (list (crate-dep (name "lim-bit-vec") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "list-fn") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0qrki2wd5ggq5w0i6wc17lyvdv87i13hffn385mk6a1zrib3mirf")))

(define-public crate-bit-long-vec-0.1 (crate (name "bit-long-vec") (vers "0.1.0") (hash "14n20izjvbiqfv936hpbq0f72127zs2lvjx3frb4jhsqc4zz9xj8")))

(define-public crate-bit-long-vec-0.2 (crate (name "bit-long-vec") (vers "0.2.0") (hash "192f2ihy20glz1rjvb02nwavmzcf612z20l5iaff400n5j5h9c4k")))

(define-public crate-bit-long-vec-0.2 (crate (name "bit-long-vec") (vers "0.2.1") (hash "0a89d18kpp907xx66agijxx2bvx4cafvjdlna9xm43r3cwjiwbl9")))

(define-public crate-bit-matrix-0.0.1 (crate (name "bit-matrix") (vers "0.0.1") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)))) (hash "0caw86j1l28ygrgl0jy053xcgvq7ard51gycpr9912s9vgr1n2qf")))

(define-public crate-bit-matrix-0.1 (crate (name "bit-matrix") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fs8r28brqi4wlsqrzr9hv06mv6y4xlpn99fddzxq13s34x3g8qq")))

(define-public crate-bit-matrix-0.2 (crate (name "bit-matrix") (vers "0.2.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)))) (hash "04ghm7qjkl171bdbh6cmxgdg2g6f3frl5qy6chd7z91asw634kyn")))

(define-public crate-bit-matrix-0.3 (crate (name "bit-matrix") (vers "0.3.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "073ikl01mavgay19m0w8lz3jpnnl6dkcbr1ahdyarj618qvkdplf") (features (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.4 (crate (name "bit-matrix") (vers "0.4.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1q4474m1g8p4iycddxdff6p8zv4v0agi7y9mwkycgd57b24yigr7") (features (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.5 (crate (name "bit-matrix") (vers "0.5.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1bzpr53y7hkly27cn6gjvrnhl274mixg114c883lk7hmsf0dx82j") (features (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.6 (crate (name "bit-matrix") (vers "0.6.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "11p28qj3qydjvi1qkqrccl45x9a2aciddb88q0wvmikg5d9hkmfs") (features (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.6 (crate (name "bit-matrix") (vers "0.6.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "19l0m8x9xp1qnfidfm1nbsq5fqap728laffhxwn5clsid8iqbxjr") (features (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.7 (crate (name "bit-matrix") (vers "0.7.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "memusage") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0gscn70djavgalyn2agprqgfjrxv4dkhdaji6yz4h1sqhg63f2rr") (features (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-matrix-0.7 (crate (name "bit-matrix") (vers "0.7.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "memusage") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "004w3cpzbdrys86m4bibrzyiijrlszjd95rph75hqpfn65blhh4w") (features (quote (("std" "bit-vec/std") ("serialize" "serde" "bit-vec/serde") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-parallelism-0.1 (crate (name "bit-parallelism") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1ywwilim5f4npy65vifiiw3hck9mcsjlxkgb1csjxkvy65x9ynwc") (rust-version "1.64")))

(define-public crate-bit-parallelism-0.1 (crate (name "bit-parallelism") (vers "0.1.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "15aad3i27kj6iv5sz94kz4cl0lzk2m5ifmdaw5b3s46p6pcw9kvj") (rust-version "1.64")))

(define-public crate-bit-parallelism-0.1 (crate (name "bit-parallelism") (vers "0.1.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0r8xy6rllj55xk80r0na4qhbpxp9yx63g70dxiqkpvsvwqqvz89k") (rust-version "1.64")))

(define-public crate-bit-parallelism-0.1 (crate (name "bit-parallelism") (vers "0.1.3") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "182arbqnx2z4k7pyn9rv7fzbmvnw1lvbn23d6hjz8dqp5hsvx9qq") (rust-version "1.64")))

(define-public crate-bit-set-0.1 (crate (name "bit-set") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "*") (default-features #t) (kind 0)))) (hash "19kbi6zlsxczclmmr0ybnva4syy29g0napdgrhlf6pjmkvi881hs")))

(define-public crate-bit-set-0.2 (crate (name "bit-set") (vers "0.2.0") (deps (list (crate-dep (name "bit-vec") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1nl9igldhlc28cb3mw925bsl2gm0x9s6l8apxk6nygcy3kxydqg6") (features (quote (("nightly"))))))

(define-public crate-bit-set-0.3 (crate (name "bit-set") (vers "0.3.0") (deps (list (crate-dep (name "bit-vec") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "13a29n67gcshal1pif985cfmcdd4cv9p43h1ri2jbwjj0ixpqll4") (features (quote (("nightly"))))))

(define-public crate-bit-set-0.4 (crate (name "bit-set") (vers "0.4.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0320hhcbr73yzjpj2237vw2zq728yg7vmzb8dardg04ff4263gyr") (features (quote (("nightly"))))))

(define-public crate-bit-set-0.5 (crate (name "bit-set") (vers "0.5.0") (deps (list (crate-dep (name "bit-vec") (req "^0.5.0") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "02kkjpv4hdynkrp11wrls1cklvgia335rk4gqdlml90qdk2gq7kg") (features (quote (("std" "bit-vec/std") ("nightly" "bit-vec/nightly") ("default" "std"))))))

(define-public crate-bit-set-0.5 (crate (name "bit-set") (vers "0.5.1") (deps (list (crate-dep (name "bit-vec") (req "^0.5.0") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "100ac8867bvbx9kv634w4xjk98b71i8nq4wdcvpf3cf4ha4j6k78") (features (quote (("std" "bit-vec/std") ("nightly" "bit-vec/nightly") ("default" "std"))))))

(define-public crate-bit-set-0.5 (crate (name "bit-set") (vers "0.5.2") (deps (list (crate-dep (name "bit-vec") (req "^0.6.1") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1pjrmpsnad5ipwjsv8ppi0qrhqvgpyn3wfbvk7jy8dga6mhf24bf") (features (quote (("std" "bit-vec/std") ("default" "std"))))))

(define-public crate-bit-set-0.5 (crate (name "bit-set") (vers "0.5.3") (deps (list (crate-dep (name "bit-vec") (req "^0.6.1") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1wcm9vxi00ma4rcxkl3pzzjli6ihrpn9cfdi0c5b4cvga2mxs007") (features (quote (("std" "bit-vec/std") ("default" "std"))))))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1ga9bvx9iz6jdpk6mfxpdbv44wik7di99gxbscmfkyhsk5nlyjjx")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0fk4b5n6n5q38wivdadiq3r8klp3nii7l8pp3rcjjg9gi3vky5vf")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0a15756q32rlkfzl5w1igq79jh76khnzb0kyzfbbs9yizbabfk4r")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0af3hq8vab5m8rbwqk7rkjp791r4j5f0c49shsdg21cdmgn004sf")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1yfl161587c9pbih94gpi93qfkyzg9zjp06jrr9i6haiznpyx9lp")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.5") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0knb6qzjrh22l8drp4ihb5c0vkfi6lmlli519f9xzb7v71zl494x")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.6") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1j9gljz6y1ai7iz3vanykfjkl1qy3jvnfdijdmkswacwkzs553zi")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.7") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "102a2a06c293qgkcr3v6lmin4znnnzxh9k99czi1vw7cscfzkg5j")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.8") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0xd2xsrhpxl4ifkflc876mdafr6lbx7sasqzw96gpc4v658ncd8b")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.9") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0pnzc35f69jfpkfblzbwd0m0vfwc8kirsaxslbzrydhcxxb92nf9")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.10") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1yh6frjfln5xdrjg581lwdv9kd4dknq0z78lh79m8lw8x2khaph5")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.11") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "17p6cb1i2ywasj4mhgwx3wf4y1n4j6gx6kxlib2dpf65r6mkmn45")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.12") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "06wb28j1fxxld234vddrijkz9bpcd72r679x13fhbf54qbwzc8jj")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.13") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fckyrrjzhrr1dyl0y30j6f146h80a5c22pg03rj412jsr09xyr8")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.14") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0i83rx3fmvyhk8g1jzvg4m7iw6l6xls5yzn6n9zcqgrh10s03fjv")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.15") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0yscnqffi3r4sxqn7wk5vz5dxsfgjf14ic4zrrk1ziris8zjl4p8")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.16") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "07vx698wid3k418hycsi3n2k2lsc8df8888ypcwwlfjfv9ys9q4y")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.17") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fl6hpbf7mnm1618p8nd8ss79xqkn27anm16n7s3ljv5qm3ajwia")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.18") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "02z5zldwjwqfmwsf1ziaz74rlhw58d1gh5si0w748bhjim8kq0d8")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.19") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1v6yq8rs5qzq0hwxhvmal0sxa2mq40bbaqf4j1459qh1swkbwn26")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.20") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1dm0w70ncfvdxjflkqyv0k8x1jkib2sg0da55w8m0sjs41cfv7ff")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.21") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "02yc06qcznnm5f968ch5k35r394mcps5m2kr6dxcfxkmr7i1q59i")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.22") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "00m3r34hlz0bk5fm6csi7hw3f8ih9ac043q9r5chrnh39asnd5zq")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.23") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "08syymsxzvy1l62bl09wn7qy4scac8g434bi31qj3ivypy24a6r5")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.24") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1w2r4v5m6xpailk622i1lhanqfxl758pcpjyg6jqbfvj31ixf9zr")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.25") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1r150srzp6mngqp3pj52fkg73hyx9njh9cd252iaz9wv9znvh8vr")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.26") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "13dz2jil188mdn4ykdyphi0ivbnlwm5qly40z9pzgsw5x7hmksja")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.27") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1whznczdkhvvww0xcp45isdwkshhjzz4xa0arykgm8mkymrj4isc")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.28") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0k71g62ih5w0snafjkdcr6pfazvy2wrbcdqm6bxb3ca5qfpsxjwh")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.29") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "05lc2jpbbdizbgfp5wk3z0rpxj6im48my0p7rj6xsnkgnfgbf4hj")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.30") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ag8dhsl8swhx44rr2b4zk0lkzmgidkdy6pyf249hcal8qlrayah")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.31") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1lnnk303q3qvd83xdm2pwnxl641adpffq2r7nnml9jrxkwb7dhbc")))

(define-public crate-bit-struct-0.1 (crate (name "bit-struct") (vers "0.1.32") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0hcxckri292pnk03q4r54b6ydaj9iajfw14fz8ahcalgg8v6cpp3")))

(define-public crate-bit-struct-0.2 (crate (name "bit-struct") (vers "0.2.0") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1w37zzrd7jhlyaf6ffkc93ssy15m03gbha50c9xm8ls3h379903p") (rust-version "1.62.1")))

(define-public crate-bit-struct-0.3 (crate (name "bit-struct") (vers "0.3.0") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "17xd2972ixk9hhwxrbb0wln6ni30740gygsrllzh48jfji605b48") (rust-version "1.62.1")))

(define-public crate-bit-struct-0.3 (crate (name "bit-struct") (vers "0.3.1") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1yhzsg6pjx4m6qxgikg7fkk2i85h53yrvxbpk5wca89z66g4g926") (rust-version "1.62.1")))

(define-public crate-bit-struct-0.4 (crate (name "bit-struct") (vers "0.4.0") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "03dw81h99bjf21br0dfx4xl2w3wrdp6nl24x1qygp5sk99pklcsh") (yanked #t) (rust-version "1.62.1")))

(define-public crate-bit-struct-0.3 (crate (name "bit-struct") (vers "0.3.2") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0885b4g61baspg1vl61g8ny5qz85fh5zmcf9lbvnfx202jqbn3r7") (features (quote (("default" "serde")))) (rust-version "1.62.1")))

(define-public crate-bit-vec-0.1 (crate (name "bit-vec") (vers "0.1.0") (hash "0zqqjnld6ji869rpc8vmn52gfj0kxfvndr7nzarsr23lnsxc1k65")))

(define-public crate-bit-vec-0.1 (crate (name "bit-vec") (vers "0.1.1") (hash "0fg8m826psqy5c4jf75h3l634vclys44a57bayyg91a6zb96k11q")))

(define-public crate-bit-vec-0.1 (crate (name "bit-vec") (vers "0.1.3") (hash "115hb6dmi475xkjbzbvqqnhnp6rh71cmsn9qg4w2pnivarhxdgw1")))

(define-public crate-bit-vec-0.1 (crate (name "bit-vec") (vers "0.1.4") (hash "1jbj1lq3d2k5vrjga7kmlqv4lq8y346wwh91fr7wlgi9hspppr96")))

(define-public crate-bit-vec-0.1 (crate (name "bit-vec") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "074c7rwzmyl137jc6yyxdvz819j790pb7464prkbzyp8zbqm4z62") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.2 (crate (name "bit-vec") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "15vn48hnyfnk5f3n0dhdybcg9xsm71f912a6gqp98xwsal1fzad4") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.3 (crate (name "bit-vec") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0dn3xbj2cqh1yw9f4adn5xzmy633z1nkalfd064l4d6kfy2qrddc") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.4 (crate (name "bit-vec") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0hcgj0j0vdk4ybxi5f7b0jz0lh2fv698dqs3xfv3xgfa9d44r4kr") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.4 (crate (name "bit-vec") (vers "0.4.1") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1kxdjmvysn8i2ia3n8bxm7w5l9mdk42i0kgwgq8rirxy5cjdaz05") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.4 (crate (name "bit-vec") (vers "0.4.2") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "19zkzhx38a0cd1jvzcc076av3lpidr7n2nvysmciwkd0gkzxpxrp") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.4 (crate (name "bit-vec") (vers "0.4.3") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1788cljz3kbnln29003x7j2n9cigya5dyngmahbjbd5vx34c55sv") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.4 (crate (name "bit-vec") (vers "0.4.4") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "0pw902a8ail0k64a7092a8vngfzsq7xkj2r22hz6q1z62s5zzd02") (features (quote (("nightly"))))))

(define-public crate-bit-vec-0.5 (crate (name "bit-vec") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "1kv7brj661psiccsxqzqc2c9c48iqsv0pv3zw853kdrvcb5xah24") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-bit-vec-0.5 (crate (name "bit-vec") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "1fyh8221s6cxlmng01v8v2ljhavzawqqs8r1xjc66ap5sjavx6zm") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-bit-vec-0.6 (crate (name "bit-vec") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0fla8gn8ffay69674cqzsp1f5s48m4f8iahk73jr5fpargsk7y2w") (features (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-bit-vec-0.6 (crate (name "bit-vec") (vers "0.6.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1xs6yba2jqlmzcm9ahzggrlb933c08ik9ah8zdsybylzhc83llm4") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-vec-0.6 (crate (name "bit-vec") (vers "0.6.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1lwws9l5x4j2m1aw9qvr7pbhsk0v02xmhy6419jqa6la5mgwa3az") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-bit-vec-0.6 (crate (name "bit-vec") (vers "0.6.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ywqjnv60cdh1slhz67psnp422md6jdliji6alq0gmly2xm9p7rl") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

