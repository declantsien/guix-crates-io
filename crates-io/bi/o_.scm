(define-module (crates-io bi o_) #:use-module (crates-io))

(define-public crate-bio_edit-0.1 (crate (name "bio_edit") (vers "0.1.1") (deps (list (crate-dep (name "bio-types") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "bit-set") (req "^0.5") (default-features #t) (kind 0)))) (hash "1fn0jv0rkiw42wvnvrhfp302vd3dypa3rfrzsvd1ksv1iivb1d6n")))

