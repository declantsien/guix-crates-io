(define-module (crates-io bi n-) #:use-module (crates-io))

(define-public crate-bin-cpuflags-x86-1 (crate (name "bin-cpuflags-x86") (vers "1.0.0") (deps (list (crate-dep (name "iced-x86") (req "^1.20") (features (quote ("std" "decoder" "instr_info"))) (kind 0)) (crate-dep (name "object") (req "^0.32") (default-features #t) (kind 0)))) (hash "1zfhdk8blpp105qa4daa8nc47vxlzcfa9sxm5a3y31m7vv04wshw")))

(define-public crate-bin-cpuflags-x86-1 (crate (name "bin-cpuflags-x86") (vers "1.0.1") (deps (list (crate-dep (name "iced-x86") (req "^1.20") (features (quote ("std" "decoder" "instr_info"))) (kind 0)) (crate-dep (name "object") (req "^0.32") (default-features #t) (kind 0)))) (hash "1j259z2jk43sj03ybflw1kg0mvbqwywfvnmwxramf9530is39nfa")))

(define-public crate-bin-cpuflags-x86-1 (crate (name "bin-cpuflags-x86") (vers "1.0.2") (deps (list (crate-dep (name "iced-x86") (req "^1.20") (features (quote ("std" "decoder" "instr_info"))) (kind 0)) (crate-dep (name "object") (req "^0.34") (default-features #t) (kind 0)))) (hash "1zrzkywgmrp663p4lfd3nry0rhs9qg97myrkpgxr1hplfjij7xfh")))

(define-public crate-bin-crates-test-0.1 (crate (name "bin-crates-test") (vers "0.1.0") (hash "00ajssr0dlr7fig3rm3aj7255i43f6q160ddmbyqjk6kdv4vybdf") (yanked #t)))

(define-public crate-bin-crates-test-0.1 (crate (name "bin-crates-test") (vers "0.1.1") (hash "19518imdz5iy1k0vr6g4dgl750wlgz8qad4c0zvdkrlar8s1xcmm") (yanked #t)))

(define-public crate-bin-crates-test-0.1 (crate (name "bin-crates-test") (vers "0.1.11") (hash "1lf95ldagf851wjn4ahrsyv0cgd07cx5fgdy9gr0anlp9fnb7jbk") (yanked #t)))

(define-public crate-bin-layout-0.1 (crate (name "bin-layout") (vers "0.1.0") (deps (list (crate-dep (name "data-view") (req "^4") (default-features #t) (kind 0)))) (hash "0fcl3gkz5nsk6p1mmphhmgks7rg4ln4l72pvc542zyn03y964s25") (features (quote (("nightly") ("NE" "data-view/NE") ("BE" "data-view/BE"))))))

(define-public crate-bin-layout-0.2 (crate (name "bin-layout") (vers "0.2.0") (deps (list (crate-dep (name "data-view") (req "^4") (default-features #t) (kind 0)))) (hash "0cgrjbx9fpiai1lg995gqp0km237fqbg9spz6qcjryr7wg2n83hg") (features (quote (("nightly") ("NE" "data-view/NE") ("BE" "data-view/BE"))))))

(define-public crate-bin-layout-1 (crate (name "bin-layout") (vers "1.0.0") (deps (list (crate-dep (name "data-view") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w5g93z9rnyd63hz158f58rhz0br72lrc8ky7basfa8q4w534hy6") (features (quote (("nightly") ("NE" "data-view/NE") ("BE" "data-view/BE"))))))

(define-public crate-bin-layout-2 (crate (name "bin-layout") (vers "2.0.0") (deps (list (crate-dep (name "derive") (req "^1") (default-features #t) (kind 0)))) (hash "1s6a5m78gmwj0mddardw79fhcf3jy6sscg52s4h55lwm6s3q1ihq") (features (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-2 (crate (name "bin-layout") (vers "2.1.0") (deps (list (crate-dep (name "derive") (req "^1") (default-features #t) (kind 0)))) (hash "1lalavcrvj0vw6ph69kywqd24c0rcsbfwaqvgndkwxh8ag37y5x1") (features (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-3 (crate (name "bin-layout") (vers "3.0.0") (deps (list (crate-dep (name "derive") (req "^1") (default-features #t) (kind 0)))) (hash "1npyzki9nxbkz2vsbxbg1nj0szy73kzvqzijll85kwr7bdjhji2b") (features (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-3 (crate (name "bin-layout") (vers "3.0.1") (deps (list (crate-dep (name "derive") (req "^1") (default-features #t) (kind 0)))) (hash "073wn91cxrz01biwkqp2rl4bgy0q3aagm6mipp89dp20vr4c8x1s") (features (quote (("nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-3 (crate (name "bin-layout") (vers "3.1.0") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "09352xjrlar8axz9j8p89bw4xg3wp67nx80cm3l8j86ryhnwivw4") (features (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-4 (crate (name "bin-layout") (vers "4.0.0") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kp6m8izd5sdn3mrf19w6fpk7xwm3hhl4dg1xmg1jsh22nq0b5n5") (features (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE")))) (yanked #t)))

(define-public crate-bin-layout-4 (crate (name "bin-layout") (vers "4.0.1") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gmvgnrqskbvhja25zqsqr0bzsh4924vvz1a1k21q60jhmvwfzpj") (features (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE")))) (yanked #t)))

(define-public crate-bin-layout-5 (crate (name "bin-layout") (vers "5.0.0") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "stack-array") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "util-cursor") (req "^0.1") (default-features #t) (kind 0)))) (hash "0y0fqg09xlyan81gm9sfvqbaa9gjp7y8ha53aslg1ida9x0pm839") (features (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-5 (crate (name "bin-layout") (vers "5.1.0") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "stack-array") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "util-cursor") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hc0sm6d7p3c7ibslh5j576msl0fh4ll7d62zmw6khl3csxlbzqf") (features (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-6 (crate (name "bin-layout") (vers "6.0.0") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stack-array") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "util-cursor") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fliq4nsaip0q9s06cxw2r1pwg6vx5mnq1kncss3cqdnz19yl211") (features (quote (("nightly") ("auto_traits" "nightly") ("NE") ("L3") ("BE"))))))

(define-public crate-bin-layout-7 (crate (name "bin-layout") (vers "7.0.0") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.7") (default-features #t) (kind 0)))) (hash "152jbws91zs3c04w7dm0vb07mdpivwiik2shsx5rnmrjf2bp4jvl") (features (quote (("NE") ("L2") ("BE"))))))

(define-public crate-bin-layout-7 (crate (name "bin-layout") (vers "7.1.0") (deps (list (crate-dep (name "bin-layout-derive") (req "^0.7") (default-features #t) (kind 0)))) (hash "1dicb1bifyap33f09df63ip5qmqwrmcmk44312kbf2d7arjl7152") (features (quote (("NE") ("L2") ("BE"))))))

(define-public crate-bin-layout-derive-0.1 (crate (name "bin-layout-derive") (vers "0.1.0") (deps (list (crate-dep (name "virtue") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0vmkij3qff2jj0kpbg7fc4n44jqb100lr39k60bfwnhdxrgfsn7m")))

(define-public crate-bin-layout-derive-0.2 (crate (name "bin-layout-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1qlqcxq7ij8m98d89f79d8jc9hwplb89m50qjggmpn5avm6c17dx")))

(define-public crate-bin-layout-derive-0.3 (crate (name "bin-layout-derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1sbx9w3pvcwpcj60a8jlr4c3w70nbwydax6iz5xrkfvhi2rp0ff3")))

(define-public crate-bin-layout-derive-0.4 (crate (name "bin-layout-derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1ka7b6yqfh2hqx3r32lyy07z2k8q2yv3klkizz9409cwfy672340")))

(define-public crate-bin-layout-derive-0.7 (crate (name "bin-layout-derive") (vers "0.7.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0jxk7xa1jqzalyi7lpjidriy8b0cy79zzaqp1gsdpxq2xpycpifl") (features (quote (("sizehint"))))))

(define-public crate-bin-pool-0.1 (crate (name "bin-pool") (vers "0.1.0") (deps (list (crate-dep (name "no-std-compat") (req "^0.4.1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "12w8im60v4razb9w05k4rmhypykn6rabdrhgr2856zxyc9ki011n") (features (quote (("std" "no-std-compat/std") ("default" "std"))))))

(define-public crate-bin-pool-0.1 (crate (name "bin-pool") (vers "0.1.1") (hash "1cilac4983qc8vr80maxn911r06zj5kwjaffwvyinj6y04vmg1z2")))

(define-public crate-bin-proto-0.1 (crate (name "bin-proto") (vers "0.1.0") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "0000lh8yc1l97kh1sx1whvcjw5xsc71szffcmnxzjs3284v1f8h9") (features (quote (("derive" "bin-proto-derive") ("default" "derive" "uuid"))))))

(define-public crate-bin-proto-0.2 (crate (name "bin-proto") (vers "0.2.0") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fq9yviifj37x8bl03hrr013xq3rsvi3l2g3pps8cpy8akbhri48") (features (quote (("derive" "bin-proto-derive") ("default" "derive" "uuid")))) (yanked #t)))

(define-public crate-bin-proto-0.2 (crate (name "bin-proto") (vers "0.2.1") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "11ay36br2x96wi0na96wg8dzzw39xvh4m5gj4632x38r5708bjc6") (features (quote (("derive" "bin-proto-derive") ("default" "derive" "uuid"))))))

(define-public crate-bin-proto-0.3 (crate (name "bin-proto") (vers "0.3.0") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "02pi462fqqgm549aw9sh0z3mdpc6hmib8i46hvqhyc7fr6zn9q8n") (features (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3 (crate (name "bin-proto") (vers "0.3.1") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "14jdjjzjfsvp7n60dzd0lm5zn2rczm7i2mly1i6l39kv9v6hfq5r") (features (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3 (crate (name "bin-proto") (vers "0.3.2") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1inyp6120yx3d17lc5wh2ginlynnpp79ww973p6n21ncf0qk916m") (features (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3 (crate (name "bin-proto") (vers "0.3.3") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0xfdm1qh6iimg2680hm22d5vi22ljs7pcgyqcw6qm4q24r65wwvk") (features (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.3 (crate (name "bin-proto") (vers "0.3.4") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "00ws64f9ailpr6l7z31kydhsqwbcfirf4hs2hy2gnx2hkj2b09y0") (features (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-0.4 (crate (name "bin-proto") (vers "0.4.0") (deps (list (crate-dep (name "bin-proto-derive") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "01qfgq2hfd267vplxnpcrnvbr83gp74390hagxg5ya390kibaz5b") (features (quote (("derive" "bin-proto-derive") ("default" "derive"))))))

(define-public crate-bin-proto-derive-0.1 (crate (name "bin-proto-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.60") (features (quote ("default" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0v703m3jk4fmms1mnngj61xk8qi2qxhgabqgaspgsxj5iv80b1qj")))

(define-public crate-bin-proto-derive-0.2 (crate (name "bin-proto-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("default" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0r7qmchvllbfd51lb0cyx71bh24ixnwgx4c5x58am2f2fnki3lr8")))

(define-public crate-bin-proto-derive-0.3 (crate (name "bin-proto-derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("default" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1qvcvs0sxadl6z892k9w4gvh4aghr5bcri29rr9kwchlax2m1c1k")))

(define-public crate-bin-proto-derive-0.3 (crate (name "bin-proto-derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("default" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0j4p04mrdfzka8cd5v9ridzgmyayfd50v19k2hj0g7mbinl671fq")))

(define-public crate-bin-proto-derive-0.3 (crate (name "bin-proto-derive") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("default" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1mi9dz100fzyi4xj0m45mch9gv9afwlbg7b29gi0y304z6a8wv46")))

(define-public crate-bin-proto-derive-0.4 (crate (name "bin-proto-derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("default" "extra-traits" "parsing"))) (default-features #t) (kind 0)))) (hash "1il8k789zl00imc27ny9r25j3l914a6rmdz53w5ynwb4p0yp89mp")))

(define-public crate-bin-rs-0.0.1 (crate (name "bin-rs") (vers "0.0.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0j0zn00jgv0kfxhh5s0xkrgmar71z65xby86p9j6lxbbfiiaj08n") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (yanked #t) (rust-version "1.58")))

(define-public crate-bin-rs-0.0.2 (crate (name "bin-rs") (vers "0.0.2") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "16i4jxjg9mr0p4h3zb3mj3l2limaxqs1gg7ksyafj5f71znd1d9x") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (yanked #t) (rust-version "1.58")))

(define-public crate-bin-rs-0.0.3 (crate (name "bin-rs") (vers "0.0.3") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ji4cgfpzvi5cp8z9w8vrw34jgf9n2lrnzsxzzgw74002818k4kn") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (yanked #t) (rust-version "1.58")))

(define-public crate-bin-rs-0.0.4 (crate (name "bin-rs") (vers "0.0.4") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "1m1238bb4avpkmi2s6qw6wxm2cdjnqbpmfmwqjhv72mshl27sd90") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (rust-version "1.58")))

(define-public crate-bin-rs-0.0.5 (crate (name "bin-rs") (vers "0.0.5") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gqj6ngd27axwyny8nh66ghadsq4142zgc643m4350z6ddr97c9x") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (rust-version "1.58")))

(define-public crate-bin-rs-0.0.6 (crate (name "bin-rs") (vers "0.0.6") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0m8kfp92nr9hwhi3fg755v4pj21aw8h0pkkr3pak09i11q4y56xh") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (rust-version "1.58")))

(define-public crate-bin-rs-0.0.7 (crate (name "bin-rs") (vers "0.0.7") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0gcw7nrpxyg9496arfwa70ga0031m14z4psvxqp1a9wiw7d14bp6") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (rust-version "1.58")))

(define-public crate-bin-rs-0.0.8 (crate (name "bin-rs") (vers "0.0.8") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0i05jgp1mmfqajz6qia1ghajkp5sxxdyp59spi1imqsdsmk46dsv") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (yanked #t) (rust-version "1.71")))

(define-public crate-bin-rs-0.0.9 (crate (name "bin-rs") (vers "0.0.9") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "macros" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0q94gq21pr2br43z28idlgfiiyq4d4anfkvyj1rm9whwsmz3km0v") (features (quote (("util") ("stream") ("default") ("codec" "encoding_rs") ("async" "tokio")))) (rust-version "1.71")))

(define-public crate-bin-stl-0.1 (crate (name "bin-stl") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "1d32s8p50xfd1g9l3q5k99yxf21ixrgf1pgsggy1hpjrzhfi3ybd")))

(define-public crate-bin-stl-0.1 (crate (name "bin-stl") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "1qj2xcgjlnzgmqh435qj14dijgjz63r8a09j73an26vf4nz8rjdf")))

(define-public crate-bin-tree-0.1 (crate (name "bin-tree") (vers "0.1.0") (hash "1m4sf6hh1z4l8wpj7isz16s10nf2vi2sj1v2sqxwprmns3x50ciw")))

(define-public crate-bin-tree-0.2 (crate (name "bin-tree") (vers "0.2.0") (hash "0bl74ldlshsax646qxdm6zhgyjgzagx3085f40c8hifsz33s7nqf")))

(define-public crate-bin-tree-0.2 (crate (name "bin-tree") (vers "0.2.1") (hash "0065zmm49f6lmphrpj7jj8cwvdxgpw92wfxdm6893pni7wh902jf")))

(define-public crate-bin-tree-0.3 (crate (name "bin-tree") (vers "0.3.0") (hash "1simknp200c1np1vnkmjblyrg3galii30s7gfca9983af7kz83da")))

(define-public crate-bin-tree-0.4 (crate (name "bin-tree") (vers "0.4.0") (hash "0dv8x7lvqxb94l7m5ym5znlk6ihp2h9i190010xs2lmdh1yv467y")))

(define-public crate-bin-tree-0.5 (crate (name "bin-tree") (vers "0.5.0") (hash "1fphqfz7bcn4494xiqfgkkk683i16p65vbr1j3i0mmhrz4nhzkg3")))

(define-public crate-bin-tree-0.6 (crate (name "bin-tree") (vers "0.6.0") (deps (list (crate-dep (name "uints") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0291axgfyn4k8mh004bnnr49hvg5lry6g6ns3sprx5a92y0x4lfq")))

(define-public crate-bin-tree-0.6 (crate (name "bin-tree") (vers "0.6.1") (deps (list (crate-dep (name "uints") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1hcbl5kczx1lhaz6y3g29yjdgx6q55zca5jw3155d1qvcn8a5vag")))

(define-public crate-bin-tree-0.6 (crate (name "bin-tree") (vers "0.6.2") (deps (list (crate-dep (name "uints") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "07y7r0my2g4kccwva9i1963z26s2wmkk4fc5vlw9sfkvsw01bclb")))

(define-public crate-bin-tree-0.7 (crate (name "bin-tree") (vers "0.7.0") (deps (list (crate-dep (name "uints") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "14hx99rniklwdq5l3582qprrflxqad56qfzic22q37bjg0dlfacp")))

(define-public crate-bin-tree-0.7 (crate (name "bin-tree") (vers "0.7.1") (deps (list (crate-dep (name "uints") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0q6jhj0zk5gz190cna7hdbf2r0549r71f12y0zkysr920y1npdii")))

(define-public crate-bin-tree-0.8 (crate (name "bin-tree") (vers "0.8.0") (deps (list (crate-dep (name "uints") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1a9vp9gbir51w22s86sm4y4pm4w021nnfvvsy22ysf96ybvw90s9")))

(define-public crate-bin-tree-0.9 (crate (name "bin-tree") (vers "0.9.0") (deps (list (crate-dep (name "uints") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0rfzg3hn0sgd328x54ynr894cx5lygc3ky65whdpk4g3m9nkaixk")))

(define-public crate-bin-tree-0.9 (crate (name "bin-tree") (vers "0.9.1") (deps (list (crate-dep (name "uints") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1wslmdgk0ih4s1qsagvjrah2l52p11p9hxvd0jcyaswf4j6b18px")))

(define-public crate-bin-tree-0.9 (crate (name "bin-tree") (vers "0.9.2") (deps (list (crate-dep (name "uints") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1rlbfixy8yf28q474zgdjz1j2yz17sbip6klyb8z4dyg3jlh303b")))

(define-public crate-bin-tree-0.10 (crate (name "bin-tree") (vers "0.10.0") (deps (list (crate-dep (name "uints") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "07gh78wvrnlpg3rvbwa8j71ik5q3xnfgyhci1pyv79vbawfcwbxs")))

(define-public crate-bin-tree-0.10 (crate (name "bin-tree") (vers "0.10.1") (deps (list (crate-dep (name "uints") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1jyz4sg2yjw3zr039gh6vla5kvvzl5ggw8bcr7xwzbji1ww0n7gg")))

(define-public crate-bin-utils-0.1 (crate (name "bin-utils") (vers "0.1.0") (hash "07jjzqmv2slrn4avw415zaah7v4a37nvi59fzk40ybzd7h1f0l2k") (features (quote (("non_fixed") ("default" "non_fixed"))))))

(define-public crate-bin-utils-0.1 (crate (name "bin-utils") (vers "0.1.1") (hash "0fz109h1myqn2jzijyqcxczif93gvx6yd9yyc6f41zg1pzibmxsr") (features (quote (("numeric_rw") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.1 (crate (name "bin-utils") (vers "0.1.2") (hash "1zy7hfbksrk8hkaadp4kk0b503hsybq7iiiwykvyjsjbf5sd2dbi") (features (quote (("numeric_rw") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.1 (crate (name "bin-utils") (vers "0.1.3") (hash "1q7y8sz7cpg92z19mdwyv1kwb000cgy9xcw7w64macb7jw9910ic") (features (quote (("numeric_rw" "non_fixed") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.1 (crate (name "bin-utils") (vers "0.1.4") (hash "0fm5lpwbiyygi002m6v8985pjpx1rqiid863kmlrashgkjlqcvhy") (features (quote (("numeric_rw" "non_fixed") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.1 (crate (name "bin-utils") (vers "0.1.5") (hash "1qs3j3y4y8phhijrjbmyhcihgqdzkk90igdrqlbnwry2lprdl92f") (features (quote (("numeric_rw" "non_fixed") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.1 (crate (name "bin-utils") (vers "0.1.6") (deps (list (crate-dep (name "try_take") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lj5c7n5rkcmrdihfq1l083hfar6ca91zavykwx3nxniwnkk7554") (features (quote (("numeric_rw" "non_fixed") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.2 (crate (name "bin-utils") (vers "0.2.0") (deps (list (crate-dep (name "try_take") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0sfcfqizvach5fb57d7fy4llfgvczf0d9pkz1i34w93px3m29ibc") (features (quote (("numeric_rw" "non_fixed") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.2 (crate (name "bin-utils") (vers "0.2.1") (deps (list (crate-dep (name "try_take") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0s0jpczlvgcgj7zmdjp8daymmbq68a6rc1y29fh7fzg87shilxc6") (features (quote (("numeric_rw" "non_fixed") ("non_fixed") ("default" "non_fixed" "numeric_rw"))))))

(define-public crate-bin-utils-0.2 (crate (name "bin-utils") (vers "0.2.3") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "try_take") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wq76pvy1dmlmac5r2whhgjayhyibwa7w9mnhf7ad13dg3ryd0dx") (features (quote (("numeric_rw") ("non_fixed") ("default" "non_fixed" "numeric_rw" "capped") ("capped" "heapless")))) (yanked #t)))

(define-public crate-bin-utils-0.2 (crate (name "bin-utils") (vers "0.2.4") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "try_take") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wc3i75w9m44vf92gsg75s061y3lgg59pb1mwyjpjjy5ld14wh9i") (features (quote (("numeric_rw") ("non_fixed") ("default" "non_fixed" "numeric_rw" "capped") ("capped" "heapless")))) (yanked #t)))

(define-public crate-bin-utils-0.2 (crate (name "bin-utils") (vers "0.2.5") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "try_take") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r9argkcdnw5nwsc3knj1ffrlrwh6lc6m50zymqkpdf390vmscqx") (features (quote (("numeric_rw") ("non_fixed") ("default" "non_fixed" "numeric_rw" "capped") ("capped" "heapless"))))))

