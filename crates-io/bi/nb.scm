(define-module (crates-io bi nb) #:use-module (crates-io))

(define-public crate-binbin-0.1 (crate (name "binbin") (vers "0.1.0") (hash "17js99d6rbjbz6fhpivpm3b6rvc75n973fxzyib6ijhhksdhlsrp")))

(define-public crate-binbin-0.2 (crate (name "binbin") (vers "0.2.0") (hash "0k29gg3vq98y2sw1r6f1v43prb0zk43jdnrrhklh0l0kygqchhqk")))

(define-public crate-binbox-0.0.1 (crate (name "binbox") (vers "0.0.1") (hash "1ss8agn1np2ljhad29h1gdqdi13d4ynisrz21n76xyskx1hmji1s") (yanked #t)))

