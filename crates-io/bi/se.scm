(define-module (crates-io bi se) #:use-module (crates-io))

(define-public crate-bisect-0.1 (crate (name "bisect") (vers "0.1.0") (hash "0rf7wxq1x9dbpxldqhklwhnkrjy24vz4s6ycmpfqv4iwxv6h79z3")))

(define-public crate-bisect-0.2 (crate (name "bisect") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0nmvi7lyl0ha7mfm6jlvb4wgdi2ml587b32asbn239vq31vf0xrh")))

(define-public crate-bisection-0.1 (crate (name "bisection") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "1hx80j7lmj3mg093psrnf5llmyksqg078jsbrzkcq3mb3fd0f7h2")))

(define-public crate-bisection_key-0.0.1 (crate (name "bisection_key") (vers "0.0.1") (hash "0bxynijhb16l07lhsgn90ld2zdsv3b9nw2l91ylmyx6azp9abs7h")))

(define-public crate-bisector-0.1 (crate (name "bisector") (vers "0.1.0") (deps (list (crate-dep (name "semver") (req "^1") (default-features #t) (kind 2)))) (hash "0l5nkjqi2q155giyc0hgfhq0grgwcr25synk782wp2m8awd6z0ih")))

(define-public crate-bisector-0.2 (crate (name "bisector") (vers "0.2.0") (deps (list (crate-dep (name "semver") (req "^1") (default-features #t) (kind 2)))) (hash "1yp338q736frik8jz72c3pa06gimf3a60jwbz1ysvwp11wmvdxdf")))

(define-public crate-bisector-0.3 (crate (name "bisector") (vers "0.3.0") (deps (list (crate-dep (name "semver") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "yare") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "0fl450j6y549prv7d80qyia5ifqkj66pza0czvmgc887dgpg1fx2") (features (quote (("testing_external_program_ewc"))))))

(define-public crate-bisector-0.4 (crate (name "bisector") (vers "0.4.0") (deps (list (crate-dep (name "semver") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "yare") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "0vrxz3w3nw1w9f5ds1nymhbqdypl0pdzgh2ssrzc0vppglag3vnz") (features (quote (("testing_external_program_ewc"))))))

(define-public crate-bisetmap-0.1 (crate (name "bisetmap") (vers "0.1.0") (hash "1gv3mbvskd5b3sxfxni81hwx75fiyfx6i4sysqdxcdhmiw148pn3")))

(define-public crate-bisetmap-0.1 (crate (name "bisetmap") (vers "0.1.1") (hash "01nnn7jdf0jqv4n0faj6nzp2fpcvil2k2z32h0kb1w55dwcxvw29")))

(define-public crate-bisetmap-0.1 (crate (name "bisetmap") (vers "0.1.2") (hash "0fjj15g3gbmbwfk8vjnyp09mx9nmhr2ijlvgnvbl8c3137wkiz40")))

(define-public crate-bisetmap-0.1 (crate (name "bisetmap") (vers "0.1.3") (hash "1m6ha6ni8xzk2280fn6ba8z23yclk98dfrp81zppa36z7phpvq1y")))

(define-public crate-bisetmap-0.1 (crate (name "bisetmap") (vers "0.1.4") (hash "0wfihpknrlxjjwxqwjbgxvwxj13g60f2mh8qlda2pc25h92c32cp")))

(define-public crate-bisetmap-0.1 (crate (name "bisetmap") (vers "0.1.5") (hash "0rirw39jark9y92ayllk13cw0w0ihrl14rgblrq61fa6zk3rvzii")))

(define-public crate-bisetmap-0.1 (crate (name "bisetmap") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "06nav7bh8sd3c2sy2wx0d92jy2h57zikzbmh75jvv4ppivfp9j92")))

