(define-module (crates-io bi te) #:use-module (crates-io))

(define-public crate-bite-0.0.1 (crate (name "bite") (vers "0.0.1") (hash "12frcj0xf6vk949z77vs960h0981dbfjr9csj0qa07x4jbxk2anh")))

(define-public crate-bite-0.0.2 (crate (name "bite") (vers "0.0.2") (hash "0lp3ldddj70243chba01nvjqz6rgxqd8hv2saqy2d0hnl825k3dq")))

(define-public crate-bite-0.0.3 (crate (name "bite") (vers "0.0.3") (hash "0cnffzk6v7d3flfabnd0is6y9djrpp7gxx0hj6wyyvckr5qyrx04")))

(define-public crate-bite-0.0.4 (crate (name "bite") (vers "0.0.4") (hash "1gm0rqql1i6m8igcx7m352353jn0ixbzx44fxa94amwjqdrycqj1")))

(define-public crate-bite-0.0.5 (crate (name "bite") (vers "0.0.5") (hash "1n323n63qnkm14finnypvcg0jp22bj02g0ngmwmzpgx117w1dfb7")))

(define-public crate-bitende-0.0.1 (crate (name "bitende") (vers "0.0.1") (hash "1g5x5ci3339120qiidc7sjy8a7ggq6q7nzy3ppc0yxrfi58gxi7a")))

(define-public crate-bitendian-0.1 (crate (name "bitendian") (vers "0.1.0") (deps (list (crate-dep (name "async-fs") (req "^2.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)) (crate-dep (name "futures-io") (req "^0.3.29") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("io-std"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("rt" "fs" "io-util"))) (default-features #t) (kind 2)))) (hash "0qx75qvvpm0vid0mfpcn1ml29y58hd05nk46z04zakabzfwd2gc1") (features (quote (("std") ("full" "std" "futures" "tokio") ("default" "std")))) (v 2) (features2 (quote (("tokio" "dep:tokio" "dep:pin-project" "std") ("futures" "dep:futures-io" "dep:pin-project" "std"))))))

(define-public crate-bitendian-0.2 (crate (name "bitendian") (vers "0.2.0") (deps (list (crate-dep (name "async-fs") (req "^2.1.0") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)) (crate-dep (name "futures-io") (req "^0.3.29") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("io-std"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("rt" "fs" "io-util"))) (default-features #t) (kind 2)))) (hash "03kp73r4b6nn3rxrc844hykz52bkcrs70g51am31i7rhdqwm5my1") (features (quote (("std") ("full" "std" "futures" "tokio") ("default" "std")))) (v 2) (features2 (quote (("tokio" "dep:tokio" "dep:pin-project" "std") ("futures" "dep:futures-io" "dep:pin-project" "std"))))))

(define-public crate-bitenum-0.1 (crate (name "bitenum") (vers "0.1.0") (hash "1r1pf94ky9fkcgyvrgn42j31p11bxipxh01hx3mqhix9snjisgzk") (yanked #t)))

(define-public crate-bitenum-0.2 (crate (name "bitenum") (vers "0.2.0") (deps (list (crate-dep (name "bitenum_macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0mwg9idr2al27578hk99ax39i0hbspw326ca8rhq0vld277igr83")))

(define-public crate-bitenum-0.2 (crate (name "bitenum") (vers "0.2.1") (deps (list (crate-dep (name "bitenum_macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0cz9bh1qwfnd3lj3b01aafz2z5m82cqh6vqip74lxsbqgsayy15f")))

(define-public crate-bitenum-0.2 (crate (name "bitenum") (vers "0.2.2") (deps (list (crate-dep (name "bitenum_macro") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mamplvfp6dz036pzgga8yvcsppjy4bkxl09dfv754m0217jfjkb")))

(define-public crate-bitenum_macro-0.2 (crate (name "bitenum_macro") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0yihwv0j4h4nivcx0a9q5a8ry4fpwkhfkqi877659j5cfrbm9bh4")))

(define-public crate-bitenum_macro-0.2 (crate (name "bitenum_macro") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0j39kcnckrwcn2d4pwygssqf6cjhvy67ywc7kwzkf7c3drd6zik2")))

(define-public crate-biterate-1 (crate (name "biterate") (vers "1.0.0") (hash "19napl64m8cw3krjlb51cp8ll86gp8zk4ala5qyj6mf28hmhya0k")))

(define-public crate-biterator-0.1 (crate (name "biterator") (vers "0.1.0") (hash "12jkkynqhh7b9x91qgf492hy20msi3ip2bhzkhj7rxiy4p03w140")))

(define-public crate-biterator-0.2 (crate (name "biterator") (vers "0.2.0") (hash "0h3kgldl2f69wsjjn162c2vdb5p6bxjlhsfz6422vnjxll0gvnwh")))

(define-public crate-biterator-0.3 (crate (name "biterator") (vers "0.3.0") (hash "0pc66hmm47pkbbhgfhwyq2r8zpk28d5k9s2i0ws3wb84dv7qnai5")))

(define-public crate-bites-0.0.0 (crate (name "bites") (vers "0.0.0") (hash "0hp0i8j8fv7lxjhpxx4mhfsb43imqnx1nkcnr7z04j3m9qlacpvi")))

(define-public crate-bitex-0.1 (crate (name "bitex") (vers "0.1.1") (deps (list (crate-dep (name "curs") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "http_stub") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "mime_guess") (req "= 1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0kqjxm0skgz67c5a3xq5bazayy19h96jwz4q7xmpvwbyr8c1mw8d")))

