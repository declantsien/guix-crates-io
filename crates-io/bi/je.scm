(define-module (crates-io bi je) #:use-module (crates-io))

(define-public crate-bijection-0.1 (crate (name "bijection") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1n3kfn0vzij05nqb12175cp9p00fa8gry0039yfxl35c0fbzsv7j")))

(define-public crate-bijection-0.1 (crate (name "bijection") (vers "0.1.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "16pxqfp44gx3q37c0f91vzg1742hd0md8wrnjpifla1qqd8w13kq")))

(define-public crate-bijection-0.1 (crate (name "bijection") (vers "0.1.2") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0zvzbji1wa320qal34a19jyq40q8yigzs3dz74ycjc8p3g0k4clr")))

