(define-module (crates-io bi bu) #:use-module (crates-io))

(define-public crate-bibutils-sys-0.1 (crate (name "bibutils-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "05f5x5rn70vcwi6p05qcxfv68rl8b8hd1zd22s2dranl9ma9d7l1") (links "bibutils")))

(define-public crate-bibutils-sys-0.1 (crate (name "bibutils-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)) (crate-dep (name "indoc") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "0p2gzcxa96j2vmiw57zhnd4r9di13kzzwaa7gyw1llj7n7gph4bh") (links "bibutils")))

