(define-module (crates-io bi ni) #:use-module (crates-io))

(define-public crate-binify-0.1 (crate (name "binify") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1dxh0isydyl23qad3q9brm7ss296zvkhzv4il53l70zkclnd9sd4")))

(define-public crate-binio-0.1 (crate (name "binio") (vers "0.1.0") (hash "082dj3fsira239cjy5yqfvh7rai3l6329x7hpjmp3b4hbbswsyj8") (yanked #t)))

(define-public crate-binio-0.1 (crate (name "binio") (vers "0.1.1") (hash "0h72qqpf3g3cxphwg1r8j03s761w4dwfpba8cr2509sign3wi7r9") (yanked #t)))

(define-public crate-binio-0.1 (crate (name "binio") (vers "0.1.2") (hash "0d90m0zzqanpwa9zg1xwl96fpxnwk154sdpfrjsplxrdi84mgss5") (yanked #t)))

(define-public crate-binio-0.1 (crate (name "binio") (vers "0.1.3") (hash "03yfzrqpfh761p9j4yipw5fqmj9r6w6glx4syjwqpnfnyifp62y9") (yanked #t)))

(define-public crate-binio-0.1 (crate (name "binio") (vers "0.1.4") (hash "05ca0f4f825frwhab0fwj58c6f4zrf4d8jvp9qm621d9692afvl2") (yanked #t)))

(define-public crate-binio-0.1 (crate (name "binio") (vers "0.1.5") (hash "04asysmqz379768scn0bijivad7mv5rpabhqbka0y6r183mzsvbv") (yanked #t)))

(define-public crate-binio-0.1 (crate (name "binio") (vers "0.1.6") (hash "1n6bf4g8r7ya85vfzwadbp5lydhjnvm0d951q27wf6qa0x0xv56p")))

