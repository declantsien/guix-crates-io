(define-module (crates-io bi ti) #:use-module (crates-io))

(define-public crate-bitinfo-0.1 (crate (name "bitinfo") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "parse_int") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "010xpiqj3ym6k73mnw25xkk7v2m7f4r99dp7khq9v658pcg5s9ya")))

(define-public crate-bitinfo-0.1 (crate (name "bitinfo") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "parse_int") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "10jrcdv1pqnfl7dn8nhbzc2z2p7akn1g9lx19alhj6fg93ylir1i")))

(define-public crate-bitint-0.0.0 (crate (name "bitint") (vers "0.0.0") (hash "1vgrdqhpg1gmxjykrqq0xhga5i9ys19yznjg6lav1s791a962zha")))

(define-public crate-bitint-0.1 (crate (name "bitint") (vers "0.1.0") (deps (list (crate-dep (name "assume") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bitint-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0y3rf81kqwy49p921x7cnlf45z7pnx5r2pap3gjlqh4dspck1dc0") (features (quote (("unchecked_math") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-bitint-0.1 (crate (name "bitint") (vers "0.1.1") (deps (list (crate-dep (name "assume") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bitint-macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0ppp5z4xbq9fiizr4css1g7n42f1xlzcf1cny5h6kzkykxkm3bym") (features (quote (("unchecked_math") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-bitint-macros-0.1 (crate (name "bitint-macros") (vers "0.1.0") (deps (list (crate-dep (name "litrs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1alpmnpkcmi1ayv19cwvcc60c5y08qqm7yax52ryvvysjrzrvzwq")))

(define-public crate-bitint-macros-0.1 (crate (name "bitint-macros") (vers "0.1.1") (deps (list (crate-dep (name "litrs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1jrg6bscxa30pqg7b6fyc00by41w5aa55jdxrr44czdh9f1zl2pr")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.0") (hash "1dcc7khpxfn6iicxwf7bspxf211wda5jc19585qvc80f98nh9wxm")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.1") (hash "01r9wqafpn5j5azywkgkz21lymf6xq6k8xakcgxk6077s05c92k1")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.2") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "07x8nmcxfx89cqrsifay5vpq5ahczl7lw8v4bd2qq7p3clcjrnjn")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.3") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "09p7fnn6cva76cxsypfcmdfckm97hcnd9sifzvd8nr4nbczvf7vh")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.4") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "0xpnqkdx9gibv8l61y8rf3nky09mlzm5s4r8if7zjyqf6sir5kvl")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.5") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "06azn0k8pzaxd5crkwf5ryn8v52mmr5mv3fyrx8l33y5z7j8zsl6")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.6") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "1n091asmrzb7ib0csq18agm1hl0lf8a41yi3qvjsiq045svzkyx0")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.7") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "1981xfv8g8kld6ff29wxg801hddssshgsmxp6i1s73a5aqakik08")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.8") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "137jx4nq4gbvphpb9jwrd80wd206zsg1nnvy7rljk46n8bb2d83a")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.9") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "0c1j86rhr40c1ihzqn8hlq1gkd4d8iqwrhhgj3k4pv30nbxcgb06")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.10") (deps (list (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "0y5q0q32fs3yhzwv5gj264c3z3hz0niiiiwl3a8myhw41r7vvq23")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.11") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "1ky9mka8gla4f1nd58w883p92gl6yz6aqrzyyxy1vlch7ya0ah8k")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.12") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "0hhy9r8jybb51jqmylblhmhyshn86xz2z4y2c23lnfinhrmj65q9")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.13") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "01j0v94knxczq0akgyy5j4xlv2kd17i96c6g5437k9ibnrpc6bq1")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.14") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "1cqfml5mhj66fqpib1rk0j50pv3g9s51f59r7y56iilgai84w8wq")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.15") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "1n15qk45h3fair0ghx5qix49swxy376a89mj0cpzgfvwjy9mgg36")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.16") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "053vzhf32nbv04hp0qr88qfwba47nv6s52r6bbcmhm77ka320j6p")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.17") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "0njwapbi4byijdwkdscahh7f5lhmnadggs04k2iplwqxkdllzw2k")))

(define-public crate-bitintr-0.1 (crate (name "bitintr") (vers "0.1.18") (deps (list (crate-dep (name "bencher") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "0.1.*") (default-features #t) (kind 1)))) (hash "00r75phz87l8vjkjb7f6cpiil398gf1ahsamrd75cw6mpihigrj1")))

(define-public crate-bitintr-0.2 (crate (name "bitintr") (vers "0.2.0") (hash "0id1rdbns1rmpv6aqj9v25ssfc8yzhizpfrl85av81zgjm27a37q") (features (quote (("unstable"))))))

(define-public crate-bitintr-0.3 (crate (name "bitintr") (vers "0.3.0") (hash "1pbkn59pb6kbyvmianidybbm6f6fy4z493v948zngj4avz2ab9bv")))

(define-public crate-bitio-0.0.1 (crate (name "bitio") (vers "0.0.1-alpha") (hash "0a67x1kjpwpmhhag1dn351v9f5hyxc7islvgfj6yf8krvixvfzyx")))

(define-public crate-bitio-0.0.1 (crate (name "bitio") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1b3gri60aywggiv4lcaby644z0lldp3148aq1yklzxd357dpqhrb")))

(define-public crate-bitio-0.0.2 (crate (name "bitio") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1xlw5psv7rfzabm5d8ncjw1jm9iapb31qwjps06wkzjdnaw9gb1x")))

(define-public crate-bitio-0.0.3 (crate (name "bitio") (vers "0.0.3") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "064pgml45n6b0dav8jbxwzfbc6hz3dp8631if466rbjvsa4v5zzk")))

(define-public crate-bitio-0.0.4 (crate (name "bitio") (vers "0.0.4") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1g9z1csn1v18h5czh8wsd4hzp83jnlrzlccz2mkirpdm3la1avn7")))

