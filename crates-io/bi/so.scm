(define-module (crates-io bi so) #:use-module (crates-io))

(define-public crate-bison-0.0.0 (crate (name "bison") (vers "0.0.0") (deps (list (crate-dep (name "bison-core") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "bison-http") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "bison-orm") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1m4nb6qn58pbph2893w5sjzvjyi1xbi8vxmb8ymdxm0ggln34rkw")))

(define-public crate-bison-core-0.0.0 (crate (name "bison-core") (vers "0.0.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bison-http") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cqycfsik71n0fjd3cfpmc9q6xp1ycmcnrw706kdl7dpi9l3c60f")))

(define-public crate-bison-http-0.0.0 (crate (name "bison-http") (vers "0.0.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cookie") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("http1" "http2" "runtime" "server"))) (default-features #t) (kind 0)))) (hash "0b31q13ang7mgk662ailz5v1j62m5d2fj2fygb8ha3q6nbxsa7pa")))

(define-public crate-bison-orm-0.0.0 (crate (name "bison-orm") (vers "0.0.0") (hash "0wbdxsikmjfjqicxbzr96qdcqhzx9sw04j9fv2vdm7vr74by5flf")))

(define-public crate-bisonn-0.1 (crate (name "bisonn") (vers "0.1.0") (deps (list (crate-dep (name "bson") (req "^2.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1cl3f9ws5zl8b99zp5i6r6xvq1l4vny3axn96bxxba1qc4h1467d")))

(define-public crate-bisonn-1 (crate (name "bisonn") (vers "1.0.0") (deps (list (crate-dep (name "bson") (req "^2.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1xi49kc9wy3jycx1ymz6nhjm9wgyjzv43aslb36k3r7682sn45qv")))

(define-public crate-bisonn-1 (crate (name "bisonn") (vers "1.0.1") (deps (list (crate-dep (name "bson") (req "^2.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0y3qwbqm7agyq3ffhzzs0vxrrn7612ashnzy8h5zvi0yhkcx43hf")))

