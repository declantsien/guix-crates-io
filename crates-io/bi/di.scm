(define-module (crates-io bi di) #:use-module (crates-io))

(define-public crate-bidi-0.1 (crate (name "bidi") (vers "0.1.0") (hash "1qsswv6j93q2jiymvq2041j9jj8w76v34xmc80wdpzkgy9wpyizb")))

(define-public crate-bidi-0.1 (crate (name "bidi") (vers "0.1.1") (hash "00lfaix1s842yv878hhnnb0743862l35r52qwv5pzxqy62zgdjbx")))

(define-public crate-bidiff-0.1 (crate (name "bidiff") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "divsufsort") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "sacabase") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sacapart") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1k3wh3p405glqr2cblsqvji25xmqbj13brbvrvwhziaz8mpmywnd") (features (quote (("instructions") ("enc" "byteorder" "integer-encoding") ("default" "enc"))))))

(define-public crate-bidiff-1 (crate (name "bidiff") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "divsufsort") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^2.0.0") (optional #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "sacabase") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sacapart") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1gm9v0aqwg87dn1jl26ahcl3aad183sdkss4xl8sv64rg4rbsn5f") (features (quote (("instructions") ("enc" "byteorder" "integer-encoding") ("default" "enc"))))))

(define-public crate-bidimensional-0.1 (crate (name "bidimensional") (vers "0.1.0") (hash "0gqd603ksk7l4ph41wysmyphmc6casg158dpqcp0hx3rkw9vhzh8")))

(define-public crate-bidimensional-0.1 (crate (name "bidimensional") (vers "0.1.1") (hash "0qb858vjjpmx47b5r72ghs6lccva7nl0s5iv5fpcvdpib5iykv9v")))

(define-public crate-bidimensional-0.1 (crate (name "bidimensional") (vers "0.1.2") (hash "0yanmf26f97g8axy1fqvh4c7xni1qb50bfqpfq8f0ha9xhiy3044")))

(define-public crate-bidimensional-0.1 (crate (name "bidimensional") (vers "0.1.3") (hash "098k11q3lxfdwkm49b9nbqxi1b9rkwx0fkzp9inbpmvn7vmrmij6")))

(define-public crate-bidimensional-0.2 (crate (name "bidimensional") (vers "0.2.0") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1q9hsgj2hz3chsx32hf5mavgzmv76f1jjjj3y3b27528qc52882h")))

(define-public crate-bidimensional-0.2 (crate (name "bidimensional") (vers "0.2.1") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0x3igixgflaki3ag1alga1s1nm2x05mzxjdnzk8nrx7x6i6lf90w")))

(define-public crate-bidimensional-0.3 (crate (name "bidimensional") (vers "0.3.0") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1ixr7hr5finv1rrgjhwbjx6p9hwi8y3npzbrz49l0aa0374si4s0")))

(define-public crate-bidimensional-0.3 (crate (name "bidimensional") (vers "0.3.1") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1h4n5nxi5ssgs7nv6h0ijz2yycrrvmns5nyl78mkjgiin9qjxhcm")))

(define-public crate-bidimensional-0.3 (crate (name "bidimensional") (vers "0.3.2") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "18dcp0fllfz3ll19i29kj48l5apwhmdd841wyhch2spz5bs724za")))

(define-public crate-bidimensional-0.3 (crate (name "bidimensional") (vers "0.3.3") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1mxrax9k7fzpac22awsk132xfvdcnm85yklz169sj1vq1lgnc6fa")))

(define-public crate-bidimensional-0.3 (crate (name "bidimensional") (vers "0.3.4") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1133c8vf0zxdq9f8lk3fwv08v10bkvfl5wqxqq259alv14dpydjd")))

(define-public crate-bidimensional-0.3 (crate (name "bidimensional") (vers "0.3.5") (deps (list (crate-dep (name "doc") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "12s3h0wn77aiaq6yibbw5490bqhnz876nkzi61bdw7ay98ckk6ig")))

(define-public crate-bidir-map-0.1 (crate (name "bidir-map") (vers "0.1.0") (hash "1f11847pam33wbl4xzmvvvklb2ks5j468ygrbdha3ngvb091wak1")))

(define-public crate-bidir-map-0.2 (crate (name "bidir-map") (vers "0.2.0") (hash "1mql8hl4kcf340xqcafwbnhwp8qqw86dic02fxpvn68ix5bxshd8")))

(define-public crate-bidir-map-0.2 (crate (name "bidir-map") (vers "0.2.1") (hash "1faw5ilhvw5jdi0ym8022z8f8x9mw66jfrmg75r9nml4lyr7bl1g")))

(define-public crate-bidir-map-0.3 (crate (name "bidir-map") (vers "0.3.0") (hash "1rs8g1gyp0ak4gasjkqxz6qddnixbwiwsz5n8vghna5nl8bz6d16")))

(define-public crate-bidir-map-0.3 (crate (name "bidir-map") (vers "0.3.1") (hash "111y8rm2rgpjxndh7gss7719z3xh42pzhrcizfpc83mqzkmdp3n2")))

(define-public crate-bidir-map-0.3 (crate (name "bidir-map") (vers "0.3.2") (hash "12izwm2j85bvh3cx0s5qq1bilnjx9qmjn04x2cgw4lslpy1h334k")))

(define-public crate-bidir-map-0.3 (crate (name "bidir-map") (vers "0.3.3") (hash "0231wpn8ik000r7hsv3dp7q808862iqap5zx83xz7pw6p8svb6cw")))

(define-public crate-bidir-map-0.4 (crate (name "bidir-map") (vers "0.4.0") (hash "14pyfg0xhzriiqs6j6klqb236m89brk497gwj40sbx4yp7dln04d")))

(define-public crate-bidir-map-0.5 (crate (name "bidir-map") (vers "0.5.0") (hash "19x0qr73cy4v25yinr5c96qm1njgm3hw62rj2w60rm6icgi20ldd")))

(define-public crate-bidir-map-0.6 (crate (name "bidir-map") (vers "0.6.0") (hash "0wwwms9c93jqw64q73vsvjs3rphrvw2qanpmxq9l95spgq1hagbc")))

(define-public crate-bidir-map-1 (crate (name "bidir-map") (vers "1.0.0") (hash "00xlk9gws0d45yb014lzlf7hd40v41hpr9cb8pc07s059v7hbgvm")))

(define-public crate-bidir_iter-0.1 (crate (name "bidir_iter") (vers "0.1.0") (hash "0mdgp351lc39c8ra63xgb9ln48nl37lr1m2k3nndgv32whsswpsz")))

(define-public crate-bidir_iter-0.2 (crate (name "bidir_iter") (vers "0.2.0") (hash "0yni7hda0dczhd0sbic5zajlkr71sk5my9d0rkd8ak7iv6mv99b7")))

(define-public crate-bidir_iter-0.2 (crate (name "bidir_iter") (vers "0.2.1") (hash "1y5lzycl8wl7bplckdp3vywp3r206aycxck5crwwcfyiyynh2v58")))

(define-public crate-bidir_termtree-0.1 (crate (name "bidir_termtree") (vers "0.1.0") (hash "03wv7j7dlnk5jvx9b1qsm8bh4d3msbk7jda92qmm454jlx95x85k")))

(define-public crate-bidir_termtree-0.1 (crate (name "bidir_termtree") (vers "0.1.1") (hash "1hlyvfi71035p0cklcnn2qwi9b5gbv8wspxn5x8vyfl99g66dlbd")))

(define-public crate-bidirectional-channel-0.1 (crate (name "bidirectional-channel") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "06my5a36yiqls2jvl2zmlkb1xwagiy3x4j4w9vspc0yiwq12j580")))

(define-public crate-bidirectional-channel-0.2 (crate (name "bidirectional-channel") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.14") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "ntest") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1rvs1a4ngafwsqakrckn13jcdyp8l3475n4klvmx27jzbwfzkdjc")))

(define-public crate-bidirectional-channel-0.2 (crate (name "bidirectional-channel") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.14") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "ntest") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0mkny5i1k95lvzsj31ss6jbqvvpswbwhabvvr8n79anjj0f9iwsd")))

(define-public crate-bidirectional-channel-0.2 (crate (name "bidirectional-channel") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.14") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "ntest") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1ny4vd9ani32fbfcb6qsg6l905bx3yykb4kg5m60diaj9lr31l3z")))

(define-public crate-bidirectional-channel-0.2 (crate (name "bidirectional-channel") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.14") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "ntest") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0rzipc3qmc0prhsn5031zwrsw6ki4vqwgwx7m8pj3jnaxxp7kx6s")))

(define-public crate-bidirectional-channel-0.3 (crate (name "bidirectional-channel") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.14") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "ntest") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "11cyp3yn06d0apx8pcdn5f3ayn24khr30bqkw7skjad4jf0gvz9z")))

(define-public crate-bidirectional-channel-0.3 (crate (name "bidirectional-channel") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.14") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "ntest") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1sql9ac27h4s0plr505y6ck4j3yfcw8wp7x0iqi3gf81nbjyjz9g")))

(define-public crate-bidirectional-map-0.1 (crate (name "bidirectional-map") (vers "0.1.0") (hash "0f75h13fcifl72b7phq8w6xmvyrqmk1aa971a5z5iny174m9ivh7")))

(define-public crate-bidirectional-map-0.1 (crate (name "bidirectional-map") (vers "0.1.1") (hash "00s60rmlvzq60la28plyi6i2cc81m6jpx4bi7ymhibvcs3pd1igc")))

(define-public crate-bidirectional-map-0.1 (crate (name "bidirectional-map") (vers "0.1.2") (hash "1bhxygc5kl5mfnzi27wf58xcixg57662mznzmyc7fg7lg9drx240")))

(define-public crate-bidirectional-map-0.1 (crate (name "bidirectional-map") (vers "0.1.3") (hash "1rk29240zfplsy5p8naz87wym223mnydcbz3xjkcqj643spc9zv8")))

(define-public crate-bidirectional-map-0.1 (crate (name "bidirectional-map") (vers "0.1.4") (hash "0ij333wm3xk0irw2bx3i6dykqr935alx777hvw88bfasvf4shrmc")))

(define-public crate-bidirectional_enum-0.1 (crate (name "bidirectional_enum") (vers "0.1.0") (hash "02lkprjkc4rd28vxrkgp45kc4p15fkcplf13g266iczpbygyxxyg")))

(define-public crate-bidirectional_enum-0.2 (crate (name "bidirectional_enum") (vers "0.2.0") (hash "0rqkzr0cvwdsc6ygjipsndcqrrqm82a01qfg6l8m6k6py6l5ap6n")))

(define-public crate-bidivec-0.1 (crate (name "bidivec") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f4nn053vs7qm9j2231jzs3sq0m6rbv815iza8qzkypfinasa76r") (rust-version "1.40")))

