(define-module (crates-io bi yi) #:use-module (crates-io))

(define-public crate-biying_cli-0.1 (crate (name "biying_cli") (vers "0.1.0") (deps (list (crate-dep (name "doe") (req "^0.1.85") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "soup") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0w9kqn65zv7r7jsw670gn35rsdjq24anjw2vcn03j2ksys6w047c")))

