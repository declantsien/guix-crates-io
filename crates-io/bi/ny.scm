(define-module (crates-io bi ny) #:use-module (crates-io))

(define-public crate-binyl-0.1 (crate (name "binyl") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "065s302jafswwmf2vpxfx2i4pd7jqhsk49a2gr2mpyympflajckb")))

(define-public crate-binyl-1 (crate (name "binyl") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1rzqr1rq7ml5jhfqn1hr260jf78j5c1rsa43a71wq67k4sz9nf07")))

