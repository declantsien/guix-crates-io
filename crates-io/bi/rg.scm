(define-module (crates-io bi rg) #:use-module (crates-io))

(define-public crate-birgitte_fibonacci-0.1 (crate (name "birgitte_fibonacci") (vers "0.1.0") (deps (list (crate-dep (name "memoize") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1mfwwh4prbcrwds9d7141c6igapymfg7p0zic3fwgr5fv6cdh9cx")))

(define-public crate-birgitte_fibonacci-0.2 (crate (name "birgitte_fibonacci") (vers "0.2.0") (deps (list (crate-dep (name "memoize") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0ahgw7xqc5f1l9ckk8svf3ir5sgsp6j1ag4r6gyy1v0fr7djdg3h")))

