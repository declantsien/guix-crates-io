(define-module (crates-io bi co) #:use-module (crates-io))

(define-public crate-biconnected-components-0.1 (crate (name "biconnected-components") (vers "0.1.0") (deps (list (crate-dep (name "petgraph") (req "^0.6") (default-features #t) (kind 0)))) (hash "0blprwfa7mgn4ifcmjj8c8q57dv9ap0c77fkfwib7sjfyi2rbyip")))

(define-public crate-biconnected-components-0.2 (crate (name "biconnected-components") (vers "0.2.0") (deps (list (crate-dep (name "petgraph") (req "^0.6") (default-features #t) (kind 0)))) (hash "002mfhqlk656zlpralmwhp28wmmmmh2x1jskd9y1bjnr9nvnd8l6")))

(define-public crate-biconnected-components-0.3 (crate (name "biconnected-components") (vers "0.3.0") (deps (list (crate-dep (name "petgraph") (req "^0.6") (default-features #t) (kind 0)))) (hash "0s7102k34b5iz1vsr6wv7viwvgn2gsb8cb1qx7kiyr9z05h4nqya")))

(define-public crate-bicoro-0.1 (crate (name "bicoro") (vers "0.1.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0lzzkf03sc434c06qmx4isj745yrqd9a9scska3dzkm47s0gh5ni")))

(define-public crate-bicoro-0.1 (crate (name "bicoro") (vers "0.1.1") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "112vnb4a07mxhfzjv9z13nylf570gcmydac4dfmz5m87pb72nmzy")))

(define-public crate-bicoro-0.1 (crate (name "bicoro") (vers "0.1.2") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "02kxgzhnmxdz16jcxm7b3v17j37nhjhfc0jckh01hq6fn6yi9rwc")))

(define-public crate-bicoro-0.2 (crate (name "bicoro") (vers "0.2.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1f90lrgsb2q0pcycg0fy62376bf2fxi3707bmq6as7hgxc74jk14")))

(define-public crate-bicoro-0.2 (crate (name "bicoro") (vers "0.2.1") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0i2gni57qh75kyrpl646ysdns8phapxkdqb1zdypx2wn2k7g5795")))

(define-public crate-bicoro-0.2 (crate (name "bicoro") (vers "0.2.2") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1yqjq81940z1v7yvnc461r9j8b0v4rk8m6j8gh4aw9fzlsl1h9n2")))

(define-public crate-bicoro-0.2 (crate (name "bicoro") (vers "0.2.3") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1mkh11psihnplqdffvw9lprnyah342a7kw7jgic8yg12rkc6rcdn")))

(define-public crate-bicoro-0.3 (crate (name "bicoro") (vers "0.3.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "16lx18vjy5va5p3r7wm7fx2frmk39cvcnr74k9gy4dbk1v3772bk")))

(define-public crate-bicoro-0.4 (crate (name "bicoro") (vers "0.4.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "14fjjfwll4ginbz32r0bgcz1cd48xvwikdsipgffikxd4c0avfsv")))

(define-public crate-bicoro-0.5 (crate (name "bicoro") (vers "0.5.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "026gxjc7kka25dpz59lfm6d41lssgxk4l1q8kh2gb6cxsw8nqq79")))

(define-public crate-bicoro-0.6 (crate (name "bicoro") (vers "0.6.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0baysvgi8wj5l6i1bcidfyzn4sigj4irrj1zb7mv7r2zfmc3finm")))

(define-public crate-bicoro-0.7 (crate (name "bicoro") (vers "0.7.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0ii7rhsf0dzsh4xr6g43di0ks4lr3048dfrddzdggsab57h7a161")))

(define-public crate-bicoro-0.8 (crate (name "bicoro") (vers "0.8.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1l6w80cachi0hps48hg8iy3vpy4p7msda3zga97w0i0z780433h5")))

(define-public crate-bicoro-0.8 (crate (name "bicoro") (vers "0.8.1") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1jc13lx9qlq1ixpgbklc7hh0fwz3h82jycfs9crdqhhk7lkw08hh")))

(define-public crate-bicoro-0.8 (crate (name "bicoro") (vers "0.8.2") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "01n6ambkk4xd5910mz8zcb6vlca31wjl29qxv0m9p56vrplscm9i")))

(define-public crate-bicoro-0.9 (crate (name "bicoro") (vers "0.9.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1wzqi72gy97inm4gdf0sfw25s4mdn66gwk7cdjghqd50cx545zcl")))

(define-public crate-bicoro-0.9 (crate (name "bicoro") (vers "0.9.1") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0jqn7gc4nxg9yfds8c67f1hby5fbm2xvfn2ay716k6bxi14j74ps")))

(define-public crate-bicoro-0.9 (crate (name "bicoro") (vers "0.9.2") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1jbs7s4jxjyhwy9f1248krd4ddxb5y3s36va03dzjzjj4zj21qj0")))

(define-public crate-bicoro-0.9 (crate (name "bicoro") (vers "0.9.3") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1k7i358md3gyr933kb8fkcn57dmmwbd3vcfx8jz89szx3bi9gasy")))

(define-public crate-bicoro-0.10 (crate (name "bicoro") (vers "0.10.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1f94zyg004pic0crys68yplyz2416vik3ayl6bq256myv0x5zqjv")))

(define-public crate-bicoro-0.11 (crate (name "bicoro") (vers "0.11.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "02ajl525n3p8bvqh42ynqi2idqkaazrs14mv8q6w0a67c8jzb6xd")))

(define-public crate-bicoro-0.12 (crate (name "bicoro") (vers "0.12.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0y2g5as24wlz6clz2xzxk7iwwwnf6qfycxikz59bs6s745p7rzwm")))

(define-public crate-bicoro-0.13 (crate (name "bicoro") (vers "0.13.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "10c31sih2r04zil9102857kmrmlwa04rmmm162cjp8k1g19i8fch")))

(define-public crate-bicoro-0.14 (crate (name "bicoro") (vers "0.14.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0xhihlm4vjzd79qyahg9ccmxzbca0xfqn5pznd63nbfahijbi58y")))

(define-public crate-bicoro-0.15 (crate (name "bicoro") (vers "0.15.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1m7hlwscxnm2wnp6grpmcb0sfn9c5fk1367dh9kqg6shj9q384dv")))

(define-public crate-bicoro-0.16 (crate (name "bicoro") (vers "0.16.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "108rarxj35f1038gvhvmmlxdi05xk9w7b37l7bhqw3g97zj69vgw")))

(define-public crate-bicoro-0.17 (crate (name "bicoro") (vers "0.17.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1ss3dadk5psjjvd85bx043bk7a9dp3vyyziw23nkszicdz8386dh")))

(define-public crate-bicoro-0.18 (crate (name "bicoro") (vers "0.18.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "15fz5za7742vb9hm1f69i078i8wcs9fk654x8398p1cb423796ww")))

(define-public crate-bicoro-0.19 (crate (name "bicoro") (vers "0.19.0") (deps (list (crate-dep (name "do-notation") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "023yrwdgr3qij0rahx18wahjpxsskr071h6ga7244xhp01a1v5lb")))

