(define-module (crates-io bi ff) #:use-module (crates-io))

(define-public crate-biff-0.0.1 (crate (name "biff") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0binaakdjrz5vfc17gil6kmm5cyn1xr0n287arvwdy3fdky05nf1")))

(define-public crate-biff-0.0.2 (crate (name "biff") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0lnnfh2ryr8yirzl1dhp3w530gslyp8zpmf8d7im4h6f4v0xgg5h")))

(define-public crate-biff-0.0.3 (crate (name "biff") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0xjxm0fic6vm410qsjax2camw2xcydsl96lnknrz09q3447xfl9c")))

(define-public crate-biff-0.0.4 (crate (name "biff") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1slk2x33ql9gsmfy63l0azzdk7ff5bgkvx1df47qb9l12bls6w3b")))

(define-public crate-biff-0.0.5 (crate (name "biff") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0ngz55rjs96iv6j0nx5r845nd6k12q52r9j8j6ggdr4d2c9vxigg")))

(define-public crate-biff-0.1 (crate (name "biff") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "0390m7zks15ik3s4ian2bgc90ws3ym7kc0h1m6l9awwq5li3rnnb")))

(define-public crate-biff-0.1 (crate (name "biff") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "0wjcqng7w17wzj1kvbg98c62vgdix5fgfs9cr4i4skxdi9z3vm9z")))

(define-public crate-biff-0.1 (crate (name "biff") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "0d4xngm1vq3bl0bd35h2is11j7nkl69ihb5sgg22dc8hdb3b0vvs")))

(define-public crate-biff-0.1 (crate (name "biff") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "1vjqfyr3d7fv7m7rrh03appaay7i2m6biac0hdmk4lv2z5xqfl3a")))

(define-public crate-biff-0.1 (crate (name "biff") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "1y9hpn1k16lajsixg5hkn8pvk0dbva4rq87jrgwlbxhkiscr571r")))

(define-public crate-biff-0.1 (crate (name "biff") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "0fp0bp8b82jv6gksj6k77ij3abfqfax02swbn95dllxz4yh83qh1")))

(define-public crate-biff-0.1 (crate (name "biff") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "0azws5m4mg3qnkzhx1va1rvdw9ym80qpsy1i2g2s2h8y5k7bkl1l")))

