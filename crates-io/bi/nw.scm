(define-module (crates-io bi nw) #:use-module (crates-io))

(define-public crate-binwrite-0.1 (crate (name "binwrite") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0ggnf4yz73kkv4fakz4kza0jsaqwdm8xf89f5v0cv36lqkq7m5rf")))

(define-public crate-binwrite-0.1 (crate (name "binwrite") (vers "0.1.1") (deps (list (crate-dep (name "binwrite_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0qc8cc1n9fzjy2p34aggqi4f0618gm6cks3xz1fkba43xdda6h1h")))

(define-public crate-binwrite-0.1 (crate (name "binwrite") (vers "0.1.2") (deps (list (crate-dep (name "binwrite_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1xs03shxfqa2i54bcymh2crjgxin32rcwznlwzfjw970a5xkxc0i")))

(define-public crate-binwrite-0.1 (crate (name "binwrite") (vers "0.1.3") (deps (list (crate-dep (name "binwrite_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1nbzl06q2w7y943lwhzijywf3szvsjijr4vf7qn5svi6fj86ackj")))

(define-public crate-binwrite-0.1 (crate (name "binwrite") (vers "0.1.4") (deps (list (crate-dep (name "binwrite_derive") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1sja9c9zl8g16inawmicdiqb8qvhqsndbkr366kh86ximv7yzgy8")))

(define-public crate-binwrite-0.1 (crate (name "binwrite") (vers "0.1.5") (deps (list (crate-dep (name "binwrite_derive") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "01p86x7nv2yl2l93kz9q1sg9p4wa08fb4gq4rhrniqswl9sjgyir")))

(define-public crate-binwrite-0.2 (crate (name "binwrite") (vers "0.2.0") (deps (list (crate-dep (name "binwrite_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0hwszkkkgygc84j80gms0x14ny8cjd3cw0l8kfqlqv517q4d1nrj")))

(define-public crate-binwrite-0.2 (crate (name "binwrite") (vers "0.2.1") (deps (list (crate-dep (name "binwrite_derive") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1j3llkblbzirbkcqvvrvfpc7qalgy2j6pjr77rjav17kn4fxmhs2")))

(define-public crate-binwrite_derive-0.1 (crate (name "binwrite_derive") (vers "0.1.0") (deps (list (crate-dep (name "binwrite") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0wrz4cz3x9pps21rycpgpwyfzrsxczdbh0gyllr21igxcbgm4yjf")))

(define-public crate-binwrite_derive-0.1 (crate (name "binwrite_derive") (vers "0.1.1") (deps (list (crate-dep (name "binwrite") (req ">= 0.1.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1rc125msrnm5f64gfv78n48jiknjqsdxfdpvj5a1xxzcy8xl70ap")))

(define-public crate-binwrite_derive-0.1 (crate (name "binwrite_derive") (vers "0.1.2") (deps (list (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1dcqdmzazsxv9x0nyizk6cla3xmcsihfvkk93l5cahxp54ry0n1s")))

(define-public crate-binwrite_derive-0.2 (crate (name "binwrite_derive") (vers "0.2.0") (deps (list (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "100gizfipqdi543ksy2fii39gbkbzxq3b6ig9hdsdi6wr4831y6l")))

(define-public crate-binwrite_derive-0.2 (crate (name "binwrite_derive") (vers "0.2.1") (deps (list (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0lz3m8izfdfzv28yqi871qffzslnzgl9q8kwk6x2chp0kk0ifvs7")))

