(define-module (crates-io bi p4) #:use-module (crates-io))

(define-public crate-bip47-0.1 (crate (name "bip47") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin") (req "^0.27") (default-features #t) (kind 0)))) (hash "0ys0avf9az6by72y77kwgnxlp8512gq9m6i2cwp9873x9hika2nk")))

(define-public crate-bip47-0.2 (crate (name "bip47") (vers "0.2.0") (deps (list (crate-dep (name "bitcoin") (req "^0.27") (default-features #t) (kind 0)))) (hash "05wmsc5j8isz1l3n007zbzsbasazpy5cf5b18v869gy4yqjj2581")))

(define-public crate-bip47-0.3 (crate (name "bip47") (vers "0.3.0") (deps (list (crate-dep (name "bitcoin") (req "^0.28") (default-features #t) (kind 0)))) (hash "0dk3j7dbxy67an6jsylmrmsyjjrbf3rbdiqc28pwyym142iakln8")))

