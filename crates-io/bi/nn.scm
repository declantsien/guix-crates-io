(define-module (crates-io bi nn) #:use-module (crates-io))

(define-public crate-binn-ir-0.0.2 (crate (name "binn-ir") (vers "0.0.2") (hash "0vg506vnaiglvf3dajh1nrxwyh01mwi1v6wn19z0f0jqja4pvndn")))

(define-public crate-binn-ir-0.1 (crate (name "binn-ir") (vers "0.1.0") (hash "11kn7sfzms10gz007rgqy1wmx4csvv99wyzpx5i2w6gpcrdsid25")))

(define-public crate-binn-ir-0.2 (crate (name "binn-ir") (vers "0.2.0") (hash "0qbpivx9d0hirbb253icgvdmz4r07534a09hw33xg52a9bywv8k1")))

(define-public crate-binn-ir-0.3 (crate (name "binn-ir") (vers "0.3.0") (hash "0wp930ri1zjg53raimp2pm9hld8bp0zbnvd0fwpr6ipwpkpjwxpk")))

(define-public crate-binn-ir-0.4 (crate (name "binn-ir") (vers "0.4.0") (hash "00p25280aj1xiva1g709q9w3qgarc41cfchnfkanx3kyrxfp9fv2")))

(define-public crate-binn-ir-0.5 (crate (name "binn-ir") (vers "0.5.0") (hash "0yz3gdw4azn12hk0prx514nvdgf2x917m1622wkzgm3hx5iqfdbp")))

(define-public crate-binn-ir-0.5 (crate (name "binn-ir") (vers "0.5.1") (hash "0a2003vrwhv3sgdgcsv4cb9zqz617fk3lrj14swshxdgws2h6sgx")))

(define-public crate-binn-ir-0.6 (crate (name "binn-ir") (vers "0.6.0") (hash "0gdsqgyxy4yzqxwy5p3x0q5fs71dlws4jqz7mqvks6mzwkgsbgcq")))

(define-public crate-binn-ir-0.7 (crate (name "binn-ir") (vers "0.7.0") (hash "1pw2yaqax18yzxb2lzddd14s060q1hi3w3g74j0f9vn14x64qc7b")))

(define-public crate-binn-ir-0.8 (crate (name "binn-ir") (vers "0.8.0") (hash "033ymz0danzvwllgfvnxpyqap2zbkywnzzzs4389xxrm5y53ma9q")))

(define-public crate-binn-ir-0.9 (crate (name "binn-ir") (vers "0.9.0") (hash "0sr94kas5hpk2hra7cd15lrzym6wzlvn2njd4h8490b8d76s47pl")))

(define-public crate-binn-ir-0.10 (crate (name "binn-ir") (vers "0.10.0") (hash "18k9jqf4q9bm3bppm2diqb03k9ra83phpqmm870nfxy7ynj9bm1q")))

(define-public crate-binn-ir-0.11 (crate (name "binn-ir") (vers "0.11.0") (hash "0hk37aad6mgwxjd5c7kxxs3xnm73r4zjvcddq8ks7bicfg5671hv")))

(define-public crate-binn-ir-0.12 (crate (name "binn-ir") (vers "0.12.0") (hash "1r5068mpc0gxfs3bzryrgm72yys6a9iq9l61nyg395mws6gdwaw5")))

(define-public crate-binn-ir-0.13 (crate (name "binn-ir") (vers "0.13.0") (hash "01bckp5m6v7z0wgyqprl6ras342bxskzqp9nn2py99xqpw0jzh6q")))

(define-public crate-binn-ir-0.13 (crate (name "binn-ir") (vers "0.13.1") (hash "1n1wgjlrlqkfx11zyjaa7c26nnqc1wc92qqjkd6n29dpniznh2aq")))

(define-public crate-binn-ir-0.14 (crate (name "binn-ir") (vers "0.14.0") (hash "1dndj35mcygphkbbyx612pg08ksyw6a925w5rdg04srw9bk1ikjv") (features (quote (("std"))))))

(define-public crate-binn-ir-0.14 (crate (name "binn-ir") (vers "0.14.1") (hash "1xlx1hhmwbqnd45l5mjp58w4vm2fapbv75m7wq81a0vm0zxhfb4q") (features (quote (("std"))))))

(define-public crate-binn-ir-0.14 (crate (name "binn-ir") (vers "0.14.2") (deps (list (crate-dep (name "kib") (req "^3") (default-features #t) (kind 2)))) (hash "1jblxsdln063xzsqvyi6hkvk984l1vbdl6jgi3zwv9mvig8a4g2l") (features (quote (("std"))))))

(define-public crate-binn-ir-0.14 (crate (name "binn-ir") (vers "0.14.3") (deps (list (crate-dep (name "kib") (req "^4") (default-features #t) (kind 2)))) (hash "1vrqk2pn5jbk301gibkjpxrdqf0vi2vfwmc6z2fa1s923ar10bf3") (features (quote (("std"))))))

(define-public crate-binn-ir-0.15 (crate (name "binn-ir") (vers "0.15.0") (deps (list (crate-dep (name "kib") (req "^4") (default-features #t) (kind 2)))) (hash "0c19jfnzygg7hhnz263kh3hmadqajpilj318nxk26z7i6is91q9a") (features (quote (("std"))))))

(define-public crate-binn-ir-0.16 (crate (name "binn-ir") (vers "0.16.0") (deps (list (crate-dep (name "kib") (req "^4") (default-features #t) (kind 2)))) (hash "1xqy5qs8idvxvcl9s520pls221xmz7iymrnkymxnrxlh14iqjb5b") (features (quote (("std"))))))

(define-public crate-binn-ir-0.17 (crate (name "binn-ir") (vers "0.17.0") (deps (list (crate-dep (name "kib") (req ">=5, <6") (default-features #t) (kind 2)))) (hash "1c2gpk17sw1rd1c4ps85d2z7kcpnkmwc3h7ii2capabrmfrdm60q") (features (quote (("std"))))))

(define-public crate-binn-ir-0.17 (crate (name "binn-ir") (vers "0.17.1") (deps (list (crate-dep (name "kib") (req ">=7.0.1, <8") (default-features #t) (kind 2)))) (hash "1zj332q4f2fy2hl5pp65sv5rv0asainf31ryfqmk8xnsvnwzhb89") (features (quote (("std"))))))

(define-public crate-binn-ir-0.17 (crate (name "binn-ir") (vers "0.17.2") (deps (list (crate-dep (name "kib") (req ">=7.0.1, <8") (default-features #t) (kind 2)))) (hash "0gnhdd3wbqc87zrqplpxf8zlrlchxc8ikd336wpwn9nf2baji13j") (features (quote (("std"))))))

(define-public crate-binn-ir-0.17 (crate (name "binn-ir") (vers "0.17.3") (deps (list (crate-dep (name "kib") (req ">=7.0.1, <8") (default-features #t) (kind 2)))) (hash "1srwnnaf624jdqrikwbx9j5zd0wyv1fffl99q5sjjc65x1137fsw") (features (quote (("std"))))))

(define-public crate-binn-rs-0.1 (crate (name "binn-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "12r0iqjs1xgxg75r5ymlbhnryg9q227z5gn8fgphszpmj5zjy7ls")))

(define-public crate-binny-0.1 (crate (name "binny") (vers "0.1.0") (hash "02p5q7rqqwwz8ywrc30nqbnw6nmmqfrnc91khbimbz57n6qzh6pw")))

(define-public crate-binny-0.1 (crate (name "binny") (vers "0.1.1") (hash "0wcvqb85p8jaa1m24pv77jyw0zyq2swyf02s7qvp098sj9hj2lmr")))

