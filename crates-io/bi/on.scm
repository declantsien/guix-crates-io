(define-module (crates-io bi on) #:use-module (crates-io))

(define-public crate-bioneer-0.1 (crate (name "bioneer") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0w1ghnr9lz42cmw5dpx70zdw81mynciv4wvc8xkbv00id0glzq4j")))

(define-public crate-bioneer-0.1 (crate (name "bioneer") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0l7b50rpia3paqyvcl46459i20y7nbb0c8clvl61nmavs354xnnm")))

(define-public crate-bionic-0.0.0 (crate (name "bionic") (vers "0.0.0") (hash "1sf69dc1ai3c7hsxma3wss8y1q8yhhypsck46vhnl5iwahjrha3w")))

(define-public crate-bionic-ebooks-0.1 (crate (name "bionic-ebooks") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "1ziivl1miy1k8y0cckskiw9sl1vi64fymn3x5gjd005y120mhfcj")))

(define-public crate-bionic-ebooks-0.1 (crate (name "bionic-ebooks") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (features (quote ("deflate"))) (kind 0)))) (hash "1zz5ync9nahqhlk1n7z09f7bkn66k8cxhs670mlp1gnzap6ikwsd")))

(define-public crate-bionic_reading_api-0.1 (crate (name "bionic_reading_api") (vers "0.1.0") (deps (list (crate-dep (name "html2md") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "visible") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "06nn9qgqyhl5vswj13872pr8bci8jm93vmj5qwqwz9k0lnv75a26") (features (quote (("doc-tests" "visible"))))))

(define-public crate-bionic_reading_api-0.1 (crate (name "bionic_reading_api") (vers "0.1.1") (deps (list (crate-dep (name "html2md") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "visible") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1nm3xr00748c8ml9gfnsvbm945c21qbwwabqvnpr4rvv2y586gi2") (features (quote (("doc-tests" "visible"))))))

