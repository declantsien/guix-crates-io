(define-module (crates-io bi pp) #:use-module (crates-io))

(define-public crate-bippy-0.1 (crate (name "bippy") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 0)))) (hash "0yzpvibn7hj5si32im4mafkkp7skqbsxprgwxdyiyyj2xapw2mpf")))

(define-public crate-bippy-0.1 (crate (name "bippy") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 0)))) (hash "15gi83mbvva8fza8d16f14jdfq7nxlidvl6h0kvgdpqc2ig79vn2")))

