(define-module (crates-io bi to) #:use-module (crates-io))

(define-public crate-bitoku-bitoku-sdk-agent-0.2 (crate (name "bitoku-bitoku-sdk-agent") (vers "0.2.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "05yhk63sg81q99ynq5va9v3kqmn5zl8k2k10mrd9k5hcnjc94nrk") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-0.1 (crate (name "bitoku-sdk-agent") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1n3k8w5irryvxf78hb70jvbpvgi04qy7ayx1wsll4pza4chgl842") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-0.1 (crate (name "bitoku-sdk-agent") (vers "0.1.1") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1zbkyyb5c5x4icwci9yb65grkvhwwfvfd8f8zh7gf3r5mjxaadq8") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (yanked #t)))

(define-public crate-bitoku-sdk-agent-0.1 (crate (name "bitoku-sdk-agent") (vers "0.1.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1xjakwrsfc0qn0fqk4zihwmadd93qwxwrbyhygzq7hqzpxi8v68h") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (yanked #t)))

(define-public crate-bitoku-sdk-agent-0.1 (crate (name "bitoku-sdk-agent") (vers "0.1.3") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0n3vdw3g1qk3vafdpr4rkdg0az60246fd5igr0q0ya3gwddq89j9") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (yanked #t)))

(define-public crate-bitoku-sdk-agent-0.1 (crate (name "bitoku-sdk-agent") (vers "0.1.4") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0580icrwsmmj7h8wvscwn7zghwkmh16qipc3n0fjp4m1271qpzlh") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (yanked #t)))

(define-public crate-bitoku-sdk-agent-0.1 (crate (name "bitoku-sdk-agent") (vers "0.1.5") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1mjc6qxdn74k6mpdxxi8mm9l1r8mzpzzsl93l6cfb584f5wdff9x") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (yanked #t)))

(define-public crate-bitoku-sdk-agent-0.1 (crate (name "bitoku-sdk-agent") (vers "0.1.6") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1mim7fs852f88gb26a8iym00llarvpk8lapqzva1l847p4wpgdhl") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-native-0.1 (crate (name "bitoku-sdk-agent-native") (vers "0.1.0") (deps (list (crate-dep (name "borsh") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.14.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0ri22xw31xdimxs2zqjyazbqmvkbkz0hx90q6yb2sql5h8d2sdsx")))

(define-public crate-bitoku-sdk-agent-native-0.1 (crate (name "bitoku-sdk-agent-native") (vers "0.1.1") (deps (list (crate-dep (name "borsh") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.14.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "127fs6glna48mdjy40yr23hq0xhjgn72fx6gshiyjqjgxdbsi9wy") (features (quote (("no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-native-0.1 (crate (name "bitoku-sdk-agent-native") (vers "0.1.2") (deps (list (crate-dep (name "borsh") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.14.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0cig1pr3swy34xcmk7cpj2g9cqr1k45pfdnqw7g7ibdc003yd1la") (features (quote (("no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-native-0.1 (crate (name "bitoku-sdk-agent-native") (vers "0.1.9") (deps (list (crate-dep (name "borsh") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.13.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1flc2qa63hbcki0df6hnw6djlj1lbndj59nn4q9ddiadxyx594na") (features (quote (("no-entrypoint"))))))

(define-public crate-bitoku-sdk-agent-native-0.1 (crate (name "bitoku-sdk-agent-native") (vers "0.1.8") (deps (list (crate-dep (name "borsh") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.10.29") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1wcpm894gsqdw1ywxp1xjjp9yp842ghmv9rzwjpnylwmqh90x2vg") (features (quote (("no-entrypoint"))))))

(define-public crate-bitonic-0.1 (crate (name "bitonic") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "087y5v58ipw8l76cxlah4kcdbp2gj4phrdy9qa6q95n1iahhq90v")))

(define-public crate-bitonic-0.2 (crate (name "bitonic") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1dib1kqgkdysa83vc8iglzyc6i1rychn0wd554s1rpp128vf4w15")))

(define-public crate-bitops-0.1 (crate (name "bitops") (vers "0.1.0") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zm5sk168v7q3m8k3nw17y6xvap7qy6x4pf93gz258zvn4y746pl")))

(define-public crate-bitorder-0.0.1 (crate (name "bitorder") (vers "0.0.1") (hash "038bnb9kvd62l1xg4dx7rrw859wgkl4bhx8s00xvlm0pd2d7x0gd")))

(define-public crate-bitorder-0.0.2 (crate (name "bitorder") (vers "0.0.2") (deps (list (crate-dep (name "deku") (req "^0.12.5") (default-features #t) (kind 2)))) (hash "0n3whmzkh5ng6idl914xj8c2g7nl3cxd08wg8sl7frc1hcwxrmmz")))

(define-public crate-bitou-sdk-agent-0.0.1 (crate (name "bitou-sdk-agent") (vers "0.0.1") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1kvqnpzk3mrrqsc1k39c31h5j3lnl15bnq9y65kdz2rkmpwgz5rs") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

