(define-module (crates-io bi nv) #:use-module (crates-io))

(define-public crate-binver-0.1 (crate (name "binver") (vers "0.1.0") (deps (list (crate-dep (name "binver_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (kind 0)) (crate-dep (name "semver") (req "^1.0") (kind 0)))) (hash "0qh0sxhhx1ry2sjz5pb5vkls7x0kki2ka10ra3rhqj9x9rpwbn8b") (features (quote (("std") ("default" "std"))))))

(define-public crate-binver-0.1 (crate (name "binver") (vers "0.1.1") (deps (list (crate-dep (name "binver_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (kind 0)) (crate-dep (name "semver") (req "^1.0") (kind 0)))) (hash "1sjgw5j2kw0icbdr4zxk213wlr226ivm7s7726kw9xrbwnfkbcc6") (features (quote (("std") ("default" "std"))))))

(define-public crate-binver_derive-0.0.1 (crate (name "binver_derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing" "printing" "extra-traits"))) (kind 0)))) (hash "1vz7pklswslmrp3mwl8x9qyqbwp5c1nq5rz9cfhjd1dllzyganfy")))

(define-public crate-binver_derive-0.1 (crate (name "binver_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing" "printing" "extra-traits"))) (kind 0)))) (hash "0359rbwi4l4rg8s0myfza48ia0ylqlmisr3svk9z8cmhi1afj4rw")))

(define-public crate-binver_derive-0.1 (crate (name "binver_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing" "printing" "extra-traits"))) (kind 0)))) (hash "1wrk12sspibvz45f5rfwsyv2pmwi0jwckmkpnr9ay3w7gpwhrrnd")))

(define-public crate-binverse-0.1 (crate (name "binverse") (vers "0.1.0") (hash "08wn2vnakiqqjgga5s5f0nr9msidijvnlxdds6dvqc8bs24cc821")))

(define-public crate-binverse-0.2 (crate (name "binverse") (vers "0.2.0") (hash "096887c872pcy7i2j7k76l0dg8fkrvaywfirvhdz0k40gdhln2q4")))

(define-public crate-binverse-0.3 (crate (name "binverse") (vers "0.3.0") (deps (list (crate-dep (name "binverse_derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zfkv5zjjbkxsphy1pdzc5h1cz0kpql0lihgyalif397hj6qh8p3") (features (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.4 (crate (name "binverse") (vers "0.4.0") (deps (list (crate-dep (name "binverse_derive") (req "=0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "00byp5afikvxfim42r9l84d8qm2vqxx3h4bidbdwbglaq0j6wjdr") (features (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.4 (crate (name "binverse") (vers "0.4.1") (deps (list (crate-dep (name "binverse_derive") (req "=0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1h6qgn18vz1li6rjbv3df6xabkw7bjb2j3mzvyy7vjfl48yz02dr") (features (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.5 (crate (name "binverse") (vers "0.5.0") (deps (list (crate-dep (name "binverse_derive") (req "=0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "13mm86axcfv829ldngl590kgmijx0f1f36vizixw9bm5k20qabkq") (features (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.5 (crate (name "binverse") (vers "0.5.1") (deps (list (crate-dep (name "binverse_derive") (req "=0.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "05rw93df9228py2l619y7glbr719x9qnkhr7bxf7zz7366wnzynr") (features (quote (("derive" "binverse_derive") ("default_features" "binverse_derive"))))))

(define-public crate-binverse-0.5 (crate (name "binverse") (vers "0.5.2") (deps (list (crate-dep (name "binverse_derive") (req "=0.5.2") (default-features #t) (kind 0)))) (hash "1dahzilin2fmjq1b3wpr64xzcrd9i28kf5bczfkf5gj69g2fc0bq") (features (quote (("inline"))))))

(define-public crate-binverse-0.5 (crate (name "binverse") (vers "0.5.3") (deps (list (crate-dep (name "binverse_derive") (req "=0.5.3") (default-features #t) (kind 0)))) (hash "0v4h0sls1j8s76l09sz3pws689wsv14xraxn6yj7i47i0wnw21cq") (features (quote (("inline"))))))

(define-public crate-binverse-0.5 (crate (name "binverse") (vers "0.5.4") (deps (list (crate-dep (name "binverse_derive") (req "=0.5.4") (default-features #t) (kind 0)))) (hash "004d2yvqrkkwh0v5a0mxqs5na3vdadcqw61nvk6najyb40pxgxh4") (features (quote (("inline"))))))

(define-public crate-binverse-0.5 (crate (name "binverse") (vers "0.5.5") (deps (list (crate-dep (name "binverse_derive") (req "=0.5.5") (default-features #t) (kind 0)))) (hash "0wpg3r6vwwcbbrr3xz89ig21yn57xjs4ki0knza21wylhax1kcrk") (features (quote (("inline"))))))

(define-public crate-binverse-0.6 (crate (name "binverse") (vers "0.6.0") (deps (list (crate-dep (name "binverse_derive") (req "=0.6.0") (default-features #t) (kind 0)))) (hash "02n12a5gk10pk0sqknppnwn63rmlllk2rap2jcmx9gq4bkqqjwji")))

(define-public crate-binverse-0.6 (crate (name "binverse") (vers "0.6.1") (deps (list (crate-dep (name "binverse_derive") (req "=0.6.1") (default-features #t) (kind 0)))) (hash "0m78ih4hlzfpkl8s091is854cjsn4xcb8mcjk6bw2arc4wawvzqp")))

(define-public crate-binverse-0.6 (crate (name "binverse") (vers "0.6.2") (deps (list (crate-dep (name "binverse_derive") (req "=0.6.2") (default-features #t) (kind 0)))) (hash "1f5wg7k7jb629cv3ii3iyd9flw54kdkhzcym4x8jldv2rgjxnmyn")))

(define-public crate-binverse_derive-0.3 (crate (name "binverse_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0k30bry7qsy8gqpk2ndsafhlav4lj2jm59qwmkgqa7hhkylmnczq")))

(define-public crate-binverse_derive-0.4 (crate (name "binverse_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00il13zqb42955a2xdnn1di2jj8rmwdgmxcsjl54v1cfjslnywng")))

(define-public crate-binverse_derive-0.4 (crate (name "binverse_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0k6ygl3cwy49wmp4j15v9aq28g0xk152xl7jhp353hpygjrdn7bf")))

(define-public crate-binverse_derive-0.5 (crate (name "binverse_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1rbqh4cp32f5av8cbxbxs2xzqqgf2d0v26dsxd697n9mxcp1dm8f")))

(define-public crate-binverse_derive-0.5 (crate (name "binverse_derive") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "07jrqllx7478j2b1wzbw8386zwqv789zlkxpggj68xq3y0a3rscj")))

(define-public crate-binverse_derive-0.5 (crate (name "binverse_derive") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1w3lz1w9b5qpxmqs0gbg7m2j6z2yfnilcd32cwdbgk0zcq18lxay")))

(define-public crate-binverse_derive-0.5 (crate (name "binverse_derive") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0nny2n1n3m624bf2kqhxikr78zj5jdvbbzg4krdf04809jn9wnw5")))

(define-public crate-binverse_derive-0.5 (crate (name "binverse_derive") (vers "0.5.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1dgnc2ivigdqbb9xsz6qbdz35lym1pp8jwnyngnyfc1xlnwcvr19")))

(define-public crate-binverse_derive-0.5 (crate (name "binverse_derive") (vers "0.5.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1my8z59xbm44s7w26188xxfflzprab51fzhp1d2gzxakq30gl8cg")))

(define-public crate-binverse_derive-0.6 (crate (name "binverse_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "08qmjzias947y3ml930yhvf5fjinfr90yb62p0gl3fz9n3wq7l0l")))

(define-public crate-binverse_derive-0.6 (crate (name "binverse_derive") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1sya5had9j5vqnx2xjy3nabxdzf4x0scg1y58h2i6zbqw64a1ji5")))

(define-public crate-binverse_derive-0.6 (crate (name "binverse_derive") (vers "0.6.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1zzl9majz2a0lfn925c665g4n4md6b9p29hc9rrq6m357qqg7s27")))

(define-public crate-binview-0.1 (crate (name "binview") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vxsacgajyi8j5fcp6rm9w1vv3n39k9hx0m8w692rwp04g652fhj") (yanked #t)))

(define-public crate-binview-0.1 (crate (name "binview") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1din0wrqcr1wylw3cmyqdmpz9s1xz7y0h34zn304g3kxca5h8rgk")))

(define-public crate-binvis-0.1 (crate (name "binvis") (vers "0.1.0") (hash "06ykb4r21v2dgpkjqdsc8rmbiy4n53cl9ara6vwvx379qyh7gdyl")))

