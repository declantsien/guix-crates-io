(define-module (crates-io bi p7) #:use-module (crates-io))

(define-public crate-bip78-0.1 (crate (name "bip78") (vers "0.1.0-preview") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "bitcoin") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (optional #t) (default-features #t) (kind 0)))) (hash "1n856zjy35h48lggg7s8qhczmvq5bx6vp8hda1anbb7ghcgal6fg") (features (quote (("sender") ("receiver" "rand"))))))

