(define-module (crates-io bi tr) #:use-module (crates-io))

(define-public crate-bitr-0.1 (crate (name "bitr") (vers "0.1.0") (hash "1zn5vmy62ligqgrrri8ghv2jasr6mznfjh2jky198n42a9jkf5nd")))

(define-public crate-bitr-0.1 (crate (name "bitr") (vers "0.1.1") (hash "1is85j9z04024m1rvprsinl9js4r22h5f480an9dhwj1jkhghfq0")))

(define-public crate-bitrange-0.1 (crate (name "bitrange") (vers "0.1.0") (deps (list (crate-dep (name "bitrange_plugin") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wizia69ffxrcginni25c0hjnj2hqpxyiqqs7102b36kfhmbi6p8")))

(define-public crate-bitrange-0.1 (crate (name "bitrange") (vers "0.1.1") (deps (list (crate-dep (name "bitrange_plugin") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06bzsgcjyh3sfxvpb8c0aykdiw3iwka1gjg0vvbngcssalbx90sp")))

(define-public crate-bitrange-0.1 (crate (name "bitrange") (vers "0.1.2") (deps (list (crate-dep (name "bitrange_plugin") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ab316dl8rv160crsfdscjjp94is9njrd2hk0kw0hiy0p9kb6jq8")))

(define-public crate-bitrange-0.2 (crate (name "bitrange") (vers "0.2.0") (deps (list (crate-dep (name "bitrange_plugin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "19hbv3yai8pg6a4kk9nrcjwzaid2mr6zsd5ic30v7kczjcylv7ll") (features (quote (("std") ("panic") ("default" "std"))))))

(define-public crate-bitrange-0.3 (crate (name "bitrange") (vers "0.3.0") (deps (list (crate-dep (name "bitrange_plugin") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0pl600d6p1a5q3anrriznadjwi81q6fr1qf6jr33ifz2lk4i463c") (features (quote (("std") ("panic") ("default" "std"))))))

(define-public crate-bitrange_plugin-0.1 (crate (name "bitrange_plugin") (vers "0.1.0") (hash "04g4ilg3infnhsqqz7ffqd7a4sdv66aa95c7s2qg4kfk6vn1ynwq")))

(define-public crate-bitrange_plugin-0.1 (crate (name "bitrange_plugin") (vers "0.1.1") (hash "1hb1ri5inimdwmcgg5bw2l0rw3igs5gybcw5drlnbvzmi12z1xbj")))

(define-public crate-bitrange_plugin-0.1 (crate (name "bitrange_plugin") (vers "0.1.2") (hash "16ixn8zsm2r31i109ksnix0dkfpr9p13b5pgjdpfdz8lz74001hb")))

(define-public crate-bitrange_plugin-0.3 (crate (name "bitrange_plugin") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1iazf93x1bddqiw33bs6yg648y9cghaa2imjg7xhbch3879rywf6")))

(define-public crate-bitranslit-0.1 (crate (name "bitranslit") (vers "0.1.0") (deps (list (crate-dep (name "bitranslit_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 1)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0wpn08m70an6s2vi4ybp24qkgjhg3cckh8vm2sssqrbhdsz9lcn2")))

(define-public crate-bitranslit-0.2 (crate (name "bitranslit") (vers "0.2.0") (deps (list (crate-dep (name "bitranslit_derive") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 1)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0vcn9h5nw9xxxsir99zw31vf1lsmmy1valch7p9q586ln42s18gh")))

(define-public crate-bitranslit-0.3 (crate (name "bitranslit") (vers "0.3.0") (deps (list (crate-dep (name "bitranslit_derive") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 1)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "10srrlw70kppg2aibqc54x1dkkfm23rkra5hi4p5jvgzy5r6hrd9")))

(define-public crate-bitranslit-0.3 (crate (name "bitranslit") (vers "0.3.1") (deps (list (crate-dep (name "bitranslit_derive") (req "=0.3.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 1)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0n76zi64jbmnp91459x3f4swbnxyzkl9ygm4x20xpfwmwxmnp6nh")))

(define-public crate-bitranslit_derive-0.1 (crate (name "bitranslit_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0fs95ybvr19jb6kgflnlkggriqgcvjry0d23skm63mxwz2aspj4h")))

(define-public crate-bitranslit_derive-0.2 (crate (name "bitranslit_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "05i1q48khxqv26pbrzq6j34dvcpii5xc5w87hl5abmwv5l5n9l65")))

(define-public crate-bitranslit_derive-0.3 (crate (name "bitranslit_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "09g382hvkdrx1y4gbcjbpqip4jpzz3wza0ch9gvaqb4iyrz36xz3")))

(define-public crate-bitranslit_derive-0.3 (crate (name "bitranslit_derive") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0cd4iv3c6p1j09nzxs332k0cr2iingx19f74ki1pssjgyqk3z40n")))

(define-public crate-bitrate-0.1 (crate (name "bitrate") (vers "0.1.0") (hash "0k9svk6q7z23pqm5dyqn6qhh6w83yvlp4nm5sqjny2k6b4q3m9vw")))

(define-public crate-bitrate-0.1 (crate (name "bitrate") (vers "0.1.1") (hash "0ga7j6m2d2nmm08s72vdlhlida3clxlsgz98g1rfyjyh29lxhiy1")))

(define-public crate-bitread-0.1 (crate (name "bitread") (vers "0.1.6") (deps (list (crate-dep (name "bitread_macro") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0slhmmzxsf3r837y5qs4y6pbzz076wm9jb9mss1asnrpwp0472vn")))

(define-public crate-bitread-0.1 (crate (name "bitread") (vers "0.1.9") (deps (list (crate-dep (name "bitread_macro") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "15w72p4f2h8j8bqv1nzd1bdl61pdhbc1raglh27l5h2lia9fhx8q")))

(define-public crate-bitread_macro-0.1 (crate (name "bitread_macro") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1pgr4vd1rldfjjf2bk317lfn28lk8gyv4vf8brqgwq3kjpbjn81s")))

(define-public crate-bitread_macro-0.1 (crate (name "bitread_macro") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "04xhfpcxf1a812v71d57nsa11nisrpmpk6zhl14jd13wyzh8n1nz")))

(define-public crate-bitreader-0.1 (crate (name "bitreader") (vers "0.1.0") (hash "13cfjys502z8c7aiayh5fb8k8vddz9m5npgsicdha6mxvng06idj")))

(define-public crate-bitreader-0.2 (crate (name "bitreader") (vers "0.2.0") (hash "0yns8s2r92r914janyyjv57a6c671p3rw01ml4cww364i1jsl6c3")))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.0") (hash "01qpnpm67wk7ikwza8cqi4vxq8lq7x9zy7mzpnh3mzv4n0m3xcc0")))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.1") (hash "1f6mrrvvsrh7p8sar7vdq8p38ams13hgbaqkm43ybw04dqpp2gm1")))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "16z0csk5a251yvz4rpqh7n90m261i6zg87wajwn4gmbwyfnz19sz") (features (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.3") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "19licyksbjfsa3jvz39cm3xkjrsqla2lc1g6z8jr3j2z06c7m9bh") (features (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.4") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "06wlzf8ra7k8ya0m71dq0kxbaalaan48dymairn9q8s4gld1hy4i") (features (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.5") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "1xcp1pxyd3si180w7x9wx485dgxmyhk957lzd1sx81nwgjhzhnxx") (features (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.6") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "0hcrn2s483si9j6v3gykn3029g4m2s5rifd9czz9iznihlfafknq") (features (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.7") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "16w68lcdax243ibv4937jj9ssdl60723x6bz35zmx5sdhvj4607i") (features (quote (("std") ("default" "std"))))))

(define-public crate-bitreader-0.3 (crate (name "bitreader") (vers "0.3.8") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "1zxf5qkjhqzsqilnkvlh3sxvs4n4rjp3anvraa14cz3zv74mkn5x") (features (quote (("std") ("default" "std"))))))

(define-public crate-bitreader_async-0.1 (crate (name "bitreader_async") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.2.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1swv8dh8ywdszv85vkk3vgjirvms4dmk6qgpwpm64xz63z8163m0")))

(define-public crate-bitreader_async-0.2 (crate (name "bitreader_async") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.2.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "bit_reverse") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0xpa0ay11biciscvgkxlxa4rdknq4ypwvwjghg8bil4b5y947xq5")))

(define-public crate-bitreel-0.1 (crate (name "bitreel") (vers "0.1.0") (deps (list (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0a9385namkw2lkmywjjlxv3mmmqw9ha54y2hfrn0x6j94iz47qmi")))

(define-public crate-bitreel-0.1 (crate (name "bitreel") (vers "0.1.1") (deps (list (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0ksq06h8as3c2i5xwlcvysm60rk95gdcsal2dhfaf9j34vfy4hza")))

(define-public crate-bitreel-0.2 (crate (name "bitreel") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0gf4cad3rg0dcwqwjzq4yafac26af519f03k22lbnvdpbc49s5cf") (features (quote (("default-connector" "reqwest") ("default" "default-connector"))))))

(define-public crate-bitregions-0.1 (crate (name "bitregions") (vers "0.1.0") (deps (list (crate-dep (name "bitregions-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "1ym0fmmw81dngna0fmj0njpjflagrmha10shs573lp7skgiykx8g")))

(define-public crate-bitregions-0.1 (crate (name "bitregions") (vers "0.1.1") (deps (list (crate-dep (name "bitregions-impl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0vl9a1mwb3gy94nlw3hcskw7v91n4syy1gav25q0fj9ks63ixj4q")))

(define-public crate-bitregions-0.1 (crate (name "bitregions") (vers "0.1.2") (deps (list (crate-dep (name "bitregions-impl") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "19mycz9av2siikh8mzkzia2y8vfklk7jj3syd1hxysf7jyyrq82b")))

(define-public crate-bitregions-0.1 (crate (name "bitregions") (vers "0.1.3") (deps (list (crate-dep (name "bitregions-impl") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "1m95nhxfkqjm7lckzknl8wqpyh71p0inmf6698k76zb9kv6sipq0")))

(define-public crate-bitregions-0.2 (crate (name "bitregions") (vers "0.2.0") (deps (list (crate-dep (name "bitregions-impl") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0dhk1m8hc9x9wmhpgbnznmk9slgy191jkm95llr19p0hj62r9m6x")))

(define-public crate-bitregions-0.2 (crate (name "bitregions") (vers "0.2.1") (deps (list (crate-dep (name "bitregions-impl") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "154bf46lvgcyb6gy13by20g7cisacp1csb8vh230z43maz22mpfh")))

(define-public crate-bitregions-0.2 (crate (name "bitregions") (vers "0.2.2") (deps (list (crate-dep (name "bitregions-impl") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "19787198vr0wn1607plawb1jf7mlp4m0a1ph36kk6b5a4vz7cfiw")))

(define-public crate-bitregions-0.2 (crate (name "bitregions") (vers "0.2.3") (deps (list (crate-dep (name "bitregions-impl") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "05z3iqr0808w6klmfvs6cfrwwds7czr93qdbwv4q0hkzm9f9ji75")))

(define-public crate-bitregions-0.2 (crate (name "bitregions") (vers "0.2.4") (deps (list (crate-dep (name "bitregions-impl") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "074lzn7ry0g9pdzwxi5pdi4jidsbhiipqvnpgp6z6hjfv3p4748g")))

(define-public crate-bitregions-0.2 (crate (name "bitregions") (vers "0.2.5") (deps (list (crate-dep (name "bitregions-impl") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "150klrkab891x1r0v59sbhhl864b3gr6awc3dvgd1xav8q0qn8sv")))

(define-public crate-bitregions-impl-0.1 (crate (name "bitregions-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0a2xqqyx1xhdi2qfk1k7bf4swlnfzdzc741cwxxkwvmankn6r5r4")))

(define-public crate-bitregions-impl-0.1 (crate (name "bitregions-impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n15mm867lq5gp0vk57rikab23619wv138rsmbirfzdx57mnqkiv")))

(define-public crate-bitregions-impl-0.1 (crate (name "bitregions-impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xx0xfqrp0ybpnsvhvxkr7z619ckh0scnpr5jn42zrp5al1rmv85")))

(define-public crate-bitregions-impl-0.1 (crate (name "bitregions-impl") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wway44g31v0gdhbj7q7gyvzwdikllxh2cfrchsvn4ndf4ypdzd2")))

(define-public crate-bitregions-impl-0.2 (crate (name "bitregions-impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0waawkch2vbxcn0bgc8q66142gjl32vizs28l4irxkqflzzs7rs1")))

(define-public crate-bitregions-impl-0.2 (crate (name "bitregions-impl") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0q907zv2cqgjk5q33rrf8rgmm7hr3x9n77md5vfjgfx5d97v1hmx")))

(define-public crate-bitregions-impl-0.2 (crate (name "bitregions-impl") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mq2gy5wyn3bia5q1fnjmg9m3kfww5mdzlvb6wbxsrgfx72v4vvv")))

(define-public crate-bitregions-impl-0.2 (crate (name "bitregions-impl") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1m51mdch63wb5q3wyw0m3iblbrpa2awj3n518468mni2mk6d5awl")))

(define-public crate-bitregions-impl-0.2 (crate (name "bitregions-impl") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "017889pp791ablyy4fn2d01hd018kbh2583iqvrilamxvnc62zsj")))

(define-public crate-bitregions-impl-0.2 (crate (name "bitregions-impl") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jl8m6vf73pa662pg91shijawqqqfn3xvpbnicqjkzcz6iz49xcy")))

(define-public crate-bitrot-0.1 (crate (name "bitrot") (vers "0.1.0") (hash "0l1biqip4wf0ap84bz375xmnbkqlxs6xbk9ciiip5nri40gggc46")))

(define-public crate-bitrush-index-0.1 (crate (name "bitrush-index") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1w6jdzmjw5s5hni5yb6263rlijb619vi1k7jzllpq30aq3a6g6w6")))

(define-public crate-bitrush-index-0.1 (crate (name "bitrush-index") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1yf4vf6x5bdswbrsnfc4c88q4n4g0av8bdg642bb17f01q00vwr9")))

(define-public crate-bitrust-0.1 (crate (name "bitrust") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.0") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "fs2") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "isatty") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^2.14.0") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2.8.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1a72gvdkmylqbvlm2myn1flip3rrmknsh5538vrf6bm6k8mjc9py")))

(define-public crate-bitrw-0.1 (crate (name "bitrw") (vers "0.1.0") (deps (list (crate-dep (name "base2") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rpr3qwr670625p1ra6yy61zrwn2057qd0ki98k6lj4l9xq4bg53")))

(define-public crate-bitrw-0.1 (crate (name "bitrw") (vers "0.1.1") (deps (list (crate-dep (name "base2") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1n7vas5fbaws5s9y20xbpkh043kj4n2bqipwwc1sjl09gzd3xgyj")))

(define-public crate-bitrw-0.1 (crate (name "bitrw") (vers "0.1.2") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1cx04mr2lfdh8nj0vq4vh58macpm2n4xw3sb8kpdd6z5qr8i1yky")))

(define-public crate-bitrw-0.2 (crate (name "bitrw") (vers "0.2.0") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "07xnpd41ibk76hcr13q1xzwmr8xrsc80zfjh5y74nl62mfny3zx8")))

(define-public crate-bitrw-0.2 (crate (name "bitrw") (vers "0.2.1") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "061827kj8dmqb8nxd8avjiqa8g1w9l6pwdif3fv87pbadj3464ai")))

(define-public crate-bitrw-0.3 (crate (name "bitrw") (vers "0.3.0") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1xfxyxxcknjpslnkl9xj69zds2alm9r0id68a995r33pcmwzk1dh")))

(define-public crate-bitrw-0.4 (crate (name "bitrw") (vers "0.4.0") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0bgjhlf7bysmyhddzh8ifmzv84ny32bpac84gab7gvzcd8qrmvai")))

(define-public crate-bitrw-0.4 (crate (name "bitrw") (vers "0.4.1") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0q2z9nhgmy4whxr02anbh77hra1lr49cczhjdl165yim9fxdyjxa")))

(define-public crate-bitrw-0.5 (crate (name "bitrw") (vers "0.5.0") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0r2wxc659sbv8yqhiknavkfpv5dgbgipp0569anzrlpwpssbgcjz")))

(define-public crate-bitrw-0.6 (crate (name "bitrw") (vers "0.6.0") (deps (list (crate-dep (name "base2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "13mi07dhsxwbgajj350k8l45y32ydjrhhp3azha1wizlqlig97zk")))

(define-public crate-bitrw-0.7 (crate (name "bitrw") (vers "0.7.0") (deps (list (crate-dep (name "base2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0hpzfsinwixq2wsqh43miw1ypfg85pga2rl85k5fxfk234y868s2")))

(define-public crate-bitrw-0.7 (crate (name "bitrw") (vers "0.7.1") (deps (list (crate-dep (name "base2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1vbwcn7q2rmrjdkkk1b60hgdmib40lm3s9bicrsmr8y4z9f4g9m9")))

(define-public crate-bitrw-0.8 (crate (name "bitrw") (vers "0.8.0") (deps (list (crate-dep (name "base2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1xk9miciq8vxvazy0kkmm8dglgw62mmq1jx8gzl9vhjpl4xbpb2w")))

(define-public crate-bitrw-0.8 (crate (name "bitrw") (vers "0.8.1") (deps (list (crate-dep (name "base2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "07nx0pb31rmhs1rl4v7pxam8b6bbxah71gby53h8vw1prqg72pa7")))

(define-public crate-bitrw-0.8 (crate (name "bitrw") (vers "0.8.2") (deps (list (crate-dep (name "base2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "168h8rzh3f8b7p8x912ragvi53inm60lh8xg61k9j2avi58wzgm9")))

(define-public crate-bitrw-0.8 (crate (name "bitrw") (vers "0.8.3") (deps (list (crate-dep (name "base2") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1z9vylh5qmaidpb1k2x6c0a1xw8yzib9hssdvdcrhvafhggrl0m5")))

