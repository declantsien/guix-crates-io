(define-module (crates-io bi pa) #:use-module (crates-io))

(define-public crate-bipack_ru-0.1 (crate (name "bipack_ru") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1hiv88sfkv6gb12c31kijgavp4xkgprzaifvg3li5gbsivya42r7")))

(define-public crate-bipack_ru-0.2 (crate (name "bipack_ru") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0209vspiyfvki7b69ck1bsd3b10jsf8k66481w4xz322d1lhq3dz")))

(define-public crate-bipack_ru-0.2 (crate (name "bipack_ru") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1nzsd9drg62zp1daf68s8dw8w1pzvlqzcsjkwm6pb4iz573pxwng")))

(define-public crate-bipack_ru-0.2 (crate (name "bipack_ru") (vers "0.2.2") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1gl706k4wjkv4rzgrxh3f1w8g754yr6ybv25bw3k6x60hsq9j8yb")))

(define-public crate-bipack_ru-0.3 (crate (name "bipack_ru") (vers "0.3.1") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0y36gjf6znsay1r8cxgj9y900y15k2cd1d1aw9frkxrf7khvfrl1")))

(define-public crate-bipack_ru-0.3 (crate (name "bipack_ru") (vers "0.3.3") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0g1594jlvz0pbhd66w7ll5fmclvfkhnx727zjl60mcmvj2xzdkiq")))

(define-public crate-bipack_ru-0.4 (crate (name "bipack_ru") (vers "0.4.1") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lnbwf0bm8jb1jwvxxxcgm55mf5q0hk1b0n5amjm078yj9a6zvr7")))

(define-public crate-bipack_ru-0.4 (crate (name "bipack_ru") (vers "0.4.2") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05h8lj9xq6j9pl7nklggga5208fslmk01ql3kdsjwy0i37yl1b4s")))

(define-public crate-bipack_ru-0.4 (crate (name "bipack_ru") (vers "0.4.3") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09hv00lzhj2n1gx1jg5id5zpiiyjrf4i2zxckzmsqhd2fwmwasai")))

(define-public crate-bipack_ru-0.4 (crate (name "bipack_ru") (vers "0.4.4") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09y05r2bc8y09xmnm0vjqcdd8xb67lrj3sc1k1lnfbddqqqrvb8n")))

(define-public crate-bipatch-0.1 (crate (name "bipatch") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1r8jxs4kvkiks70kslsp6m4lg3rzvb7s9s21y2qhkjin42nb5xh5")))

(define-public crate-bipatch-0.1 (crate (name "bipatch") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0.7") (kind 0)))) (hash "1c14g89513c5q03fwd1f3js13w5ixr7zyw6qf11gxzhwvp1rq6yb")))

(define-public crate-bipatch-1 (crate (name "bipatch") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^2.0.0") (kind 0)))) (hash "0mr26nwvir8fjvmqw8q17x2awm123bgxsxznlln8rwils70n779g")))

