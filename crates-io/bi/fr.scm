(define-module (crates-io bi fr) #:use-module (crates-io))

(define-public crate-bifrost-0.1 (crate (name "bifrost") (vers "0.1.0") (hash "1glx1kxbd9ik6fd3m7zjri9cb39v6yd67gp1pd2c5q3ipzhv4fy4") (yanked #t)))

(define-public crate-bifrost-0.1 (crate (name "bifrost") (vers "0.1.1") (hash "06agz9y6mssgb2za46420x00k4pd44vxax3r9p0y41ih23mbn0kq") (yanked #t)))

(define-public crate-bifrost-0.1 (crate (name "bifrost") (vers "0.1.2") (hash "0jifbhh95grp1z96wzlnvg15in20pzvc0nvchdq06lynar88r7ws") (yanked #t)))

(define-public crate-bifrost-0.2 (crate (name "bifrost") (vers "0.2.0") (hash "16a166g3ggxj6y918mq3y9gy4krjds56s1k1ps7g6kw0x25sd3s1") (yanked #t)))

(define-public crate-bifrost-0.3 (crate (name "bifrost") (vers "0.3.0") (hash "0flmdy3n259w7nm0bfgvxg59l1vx7ah573np718qiw8ckp9lnqks") (yanked #t)))

(define-public crate-bifrost-0.1 (crate (name "bifrost") (vers "0.1.0-alpha") (hash "0305yhbikqvgc26vzkn4qlksmg520k05bn0h1cykr38nyw0zmddq")))

(define-public crate-bifrost-0.4 (crate (name "bifrost") (vers "0.4.0-alpha") (deps (list (crate-dep (name "bifrost-ice") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "bifrost-sdp") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "bifrost-stun") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "0wln9zl93qg7bid6sw8w0zka8b9vxmcbf84l3sdvvyjkw1kdlmi5")))

(define-public crate-bifrost-0.4 (crate (name "bifrost") (vers "0.4.0-alpha.1") (deps (list (crate-dep (name "bifrost-ice") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "bifrost-sdp") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "bifrost-stun") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "1vahlqrpzyxl0zkafq68hylycngmhdgvrgcay4kkzi8rdxw37px6")))

(define-public crate-bifrost-0.4 (crate (name "bifrost") (vers "0.4.0-alpha.2") (deps (list (crate-dep (name "bifrost-ice") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "bifrost-sdp") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "bifrost-stun") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "1bn1v7s4gxrjnlszvn24g5y0q89grl5y4dx3dwk0223qcraj3rvr")))

(define-public crate-bifrost-ice-0.1 (crate (name "bifrost-ice") (vers "0.1.0-alpha") (hash "0wnljvmfd567xw8rxg231zy6gbmzpj30h4k4vvfm7wz44ljv6nvc")))

(define-public crate-bifrost-sdp-0.1 (crate (name "bifrost-sdp") (vers "0.1.0-alpha") (deps (list (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "1xjx5g069bq91v8k43rwh334ghp8d9jg3c4n0smxg7m9f0v0fq89")))

(define-public crate-bifrost-sdp-0.1 (crate (name "bifrost-sdp") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "vec1") (req "^1.4") (default-features #t) (kind 0)))) (hash "0rsxgqk1a6jnzyzj7br00gnnn9wvk2s2a7p3khvbmnfp500sxada")))

(define-public crate-bifrost-stun-0.1 (crate (name "bifrost-stun") (vers "0.1.0-alpha") (hash "1qrf13f38mfzak0fhy141hr6x2ng1x3ybcww6wd37sgx9h4a15lv")))

(define-public crate-bifrost-turn-0.1 (crate (name "bifrost-turn") (vers "0.1.0-alpha") (hash "0nc3ipd51ch5plxbmkam1b39sk661cvdvbm4d4q7piizjdy45fqz")))

(define-public crate-bifrost-webrtc-0.1 (crate (name "bifrost-webrtc") (vers "0.1.0-alpha") (hash "1ibrfn2sncnskzqy0wmkjdi9r2r9y48l1fkhpdwaapwrh1jd0pjq")))

(define-public crate-bifrostlink-0.1 (crate (name "bifrostlink") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.77") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("rt" "io-util" "process" "sync" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.3") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0fj67wj8diln40vdb2vikib6gz3wjcd5vxkga4dk9wyk9xfwdzj2")))

