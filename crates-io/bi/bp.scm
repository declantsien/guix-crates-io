(define-module (crates-io bi bp) #:use-module (crates-io))

(define-public crate-bibparser-0.3 (crate (name "bibparser") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hr3r0d8bcysqlbgaiwbm55q1asczx319n0q1jjrwjp674awkm3v")))

(define-public crate-bibparser-0.3 (crate (name "bibparser") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bjq6xa95zgh7k9a21wcpvyqvs2nvawshflh4x2x61lqxc5dvzm8")))

(define-public crate-bibparser-0.3 (crate (name "bibparser") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0z1gb7gdq2mv4y5imcxzizkm1ar60fskdh9q3gpg83qjk5yaia9y")))

(define-public crate-bibparser-0.4 (crate (name "bibparser") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fi5yhqs4g8qv059ajc005bfv4ga8g7cq80dc5m9nf3s3xrdcy53")))

