(define-module (crates-io bi ne) #:use-module (crates-io))

(define-public crate-binexp-0.1 (crate (name "binexp") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "1lla70d4hyhpy8irwbjdk32zam56dbmm4hpyylmjm8lp7izgxigv") (rust-version "1.68")))

(define-public crate-binexp-0.1 (crate (name "binexp") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "1k4j4jbszg5yj7ildz6yss2ca0pshizhg74cr11f9kd1hj6s84lm") (rust-version "1.68")))

(define-public crate-binext-1 (crate (name "binext") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("min_const_gen"))) (default-features #t) (kind 2)))) (hash "15kw9fijg851y7w4j3jmh82qyk16kv2jncgfp7z81qph5p2w10y0")))

