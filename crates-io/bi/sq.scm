(define-module (crates-io bi sq) #:use-module (crates-io))

(define-public crate-bisq_client-0.0.1 (crate (name "bisq_client") (vers "0.0.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "ra_common") (req "^0.0.43") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "03sycv2k591zrlxqvjc4kxrc19fbnx90bbrrlqnxfyjglj50mkys")))

