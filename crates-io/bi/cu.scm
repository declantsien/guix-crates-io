(define-module (crates-io bi cu) #:use-module (crates-io))

(define-public crate-bicubic-0.1 (crate (name "bicubic") (vers "0.1.0") (hash "0aiz7spm4b4xik222cxfbfshdbb2701j506kd0ld3j64l4k73a3x") (yanked #t)))

(define-public crate-bicubic-0.1 (crate (name "bicubic") (vers "0.1.1") (hash "1sz1wli467ylrnafvxjzfhjcqqvs7a9frrplaflyzi8n0rkm842c")))

(define-public crate-bicubic-0.1 (crate (name "bicubic") (vers "0.1.2") (hash "10xf9brx9fidwc037s80c01izw9xab8bp4b0ijivlf1f575rc4sq")))

