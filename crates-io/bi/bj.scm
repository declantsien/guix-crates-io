(define-module (crates-io bi bj) #:use-module (crates-io))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "1l8rddz7ayy27wjmaj0rm9hysq066s2cw46c9a0np4v9kpg1gni5") (yanked #t)))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "15a2xj86dhwv8b9v66pdvkra80h4xcalwhx2wgr48qxyc071ixiw") (yanked #t)))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "0ja68hmwlylag5w3viwl39f7d4n1ymq7dla1b82ihz182r8i1lxj") (yanked #t)))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "1066yxynmzvz928r4s8x3is9n80x9sjfbqmxmk52wz9xkayg138l") (yanked #t)))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "00zn6vdhmj6ss4vd5sp8f72wwvzb6dx4wi4ckgp0kafbsxz5hfyc") (yanked #t)))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "11a3jxpxfb1hifqjgb25v8h471794fbpvalgnblbihkqh9p6nda7") (yanked #t)))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "16m817kymh0lh9zzyxwi36lzkq103p78y6wpflcz51pvz1dxjy6g")))

(define-public crate-bibjoin-0.1 (crate (name "bibjoin") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "04fb4cxalyqaxng95jfy9fca505489ilr8v6yr4gr44gc6g0qr4q")))

(define-public crate-bibjoin-0.2 (crate (name "bibjoin") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "polars-io") (req "^0.15.1") (features (quote ("decompress"))) (default-features #t) (kind 0)))) (hash "1vly2z6ckfwpz12bayxqbyii2znp1skr42cg0mcyw6h38c5ani1s")))

(define-public crate-bibjoin-0.2 (crate (name "bibjoin") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "polars-io") (req "^0.15.1") (features (quote ("decompress"))) (default-features #t) (kind 0)))) (hash "1g8lvn106w1mcqb8bbv3jvn178sjxz5qdkn2j41w75b95g46q5q6")))

(define-public crate-bibjoin-0.2 (crate (name "bibjoin") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "polars-io") (req "^0.16") (features (quote ("decompress"))) (default-features #t) (kind 0)))) (hash "1icdq7z6bg7hrin2sn3yar9c0yhbcf1a2m5l2mirwxgqpnwmsqq7")))

(define-public crate-bibjoin-0.3 (crate (name "bibjoin") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "polars-io") (req "^0.16") (features (quote ("decompress"))) (default-features #t) (kind 0)))) (hash "1nsbmszxbdf1madyhicdplzg12psza85parfwal7vlwdv1sl0239")))

