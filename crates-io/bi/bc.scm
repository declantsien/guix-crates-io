(define-module (crates-io bi bc) #:use-module (crates-io))

(define-public crate-bibcompiler-0.1 (crate (name "bibcompiler") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mll7dl6b0qjg8g4s66mnznhgkv0mn3pd0mrp1z8nd6jvwa5p03d") (yanked #t)))

(define-public crate-bibcompiler-0.1 (crate (name "bibcompiler") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sxh0d4d884gkj2lfk2lwrvdscafiwddrsqbib2hrp0fvljzgnyn") (yanked #t)))

(define-public crate-bibcompiler-0.1 (crate (name "bibcompiler") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11xgk95g08x5rgi2yylj56dfqj8rsi5r1ba3gyz7ykzbnbwjphq4") (yanked #t)))

(define-public crate-bibcompiler-0.1 (crate (name "bibcompiler") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hamgw069g073bxw15v6c4h1nkhj45ra6c6xlixs8y4bvpm84rrd") (yanked #t)))

