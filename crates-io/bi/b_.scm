(define-module (crates-io bi b_) #:use-module (crates-io))

(define-public crate-bib_unifier-0.1 (crate (name "bib_unifier") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "biblatex") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bunt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "read_input") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1nhra2nlhx8c77mfckc6p2h533hqrmzfk3rx2ii0iz0maxqix7rf")))

