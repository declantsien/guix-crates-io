(define-module (crates-io bi n2) #:use-module (crates-io))

(define-public crate-bin2bcd-0.0.1 (crate (name "bin2bcd") (vers "0.0.1") (hash "0l9ggzpj56ac44wpjxvsmrb845phw0x4vrhqf9hhhqyyypw5i6fw")))

(define-public crate-bin2src-0.0.56 (crate (name "bin2src") (vers "0.0.56") (hash "0m2i5ild6i9nwhaz81lxkrkiajfhq36qzmanvj5xfg7l7z5s1asn") (yanked #t)))

(define-public crate-bin2src-0.0.57 (crate (name "bin2src") (vers "0.0.57") (hash "1k6lqq04ksf5ymbsp5ps4n2phnmdvqailp40313g9ib9sdka7m47")))

(define-public crate-bin2src-0.0.58 (crate (name "bin2src") (vers "0.0.58") (hash "1fj6lym6qipxqdp9dmrci05k3d1l9jsq09s4csh9cp9vpjh0gghs")))

