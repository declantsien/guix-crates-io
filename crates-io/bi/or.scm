(define-module (crates-io bi or) #:use-module (crates-io))

(define-public crate-biorust-0.1 (crate (name "biorust") (vers "0.1.0") (hash "0fancjnr6riwi2bz0mw3968j4miwmlm7ibn7gngq8zcj8q7j7ihc") (yanked #t)))

(define-public crate-biorustlings-0.0.2 (crate (name "biorustlings") (vers "0.0.2") (deps (list (crate-dep (name "xml-rs") (req "^0.6") (default-features #t) (kind 0)))) (hash "0r5v4rb2xg0ld9lpimvrnwzfn9vh9413l125cz0sh55n4nw4qqd2")))

