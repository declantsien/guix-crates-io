(define-module (crates-io bi no) #:use-module (crates-io))

(define-public crate-binocle-0.1 (crate (name "binocle") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "colorgrad") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "egui_wgpu_backend") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "egui_winit_platform") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "1c1hlgbdz44w3qzziky2fjd6350335309hq2xnkmlnnb3gs8m72c") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize"))))))

(define-public crate-binocle-0.1 (crate (name "binocle") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "colorgrad") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "egui_wgpu_backend") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "egui_winit_platform") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "0kw06q7dfby3as7n576gpi2radgsbqnk1vjj0bh6c4ds7xdhp6ng") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize"))))))

(define-public crate-binocle-0.1 (crate (name "binocle") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "colorgrad") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "egui_wgpu_backend") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "egui_winit_platform") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "1g7mri6h6cj6cw6wy3xnwad2dsl7gysyid0x5kkk8y4qf36hp543") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize"))))))

(define-public crate-binocle-0.2 (crate (name "binocle") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "colorgrad") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "egui_wgpu_backend") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "egui_winit_platform") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "1234rfprsrhcqj9gfmbwjip07bd48ng86f10d3k99qvfp48d7q8j") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize")))) (rust-version "1.56")))

(define-public crate-binocle-0.3 (crate (name "binocle") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorgrad") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "egui_wgpu_backend") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "egui_winit_platform") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 0)))) (hash "0c0lkp67xvzap979h1k3hbp0h1smqk0kd1y92rfns49pcl9jxbjc") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize")))) (rust-version "1.56")))

(define-public crate-binocle-0.3 (crate (name "binocle") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorgrad") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "egui_wgpu_backend") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "egui_winit_platform") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.11") (default-features #t) (kind 0)))) (hash "131ni7lzbz1m5jlal1r1226skr3d0pasqa67hdq1l1wjlcc2kk6n") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize")))) (rust-version "1.66")))

(define-public crate-binocle-0.3 (crate (name "binocle") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorgrad") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "egui-wgpu") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "egui-winit") (req "^0.22") (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.14") (default-features #t) (kind 0)))) (hash "0p18y39q0j53rqql3fll3i3jvbyw5kpk55m10aqmnfhw5przdxig") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize")))) (rust-version "1.66")))

(define-public crate-binocular-0.0.0 (crate (name "binocular") (vers "0.0.0") (hash "1b57m2jwrzflgv36jiv0zm18lw68nzffqwz8w7mwc30amd5v7h1j")))

(define-public crate-binocular-cli-0.1 (crate (name "binocular-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1nkrq3qsa7cq0j2q3id87bm72r3czpylzzfh660gng74njbjqq43") (yanked #t)))

(define-public crate-binocular-cli-0.1 (crate (name "binocular-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wffpbsncbiv0fhz5yypfw2agyc31qpj4crprf8imb1gcd8mizy2")))

(define-public crate-binod-0.1 (crate (name "binod") (vers "0.1.0") (hash "0qlkwmdr8mr4ghvy5sk838dbnzbni3qdaz8cywhxvn0jpgbn512h")))

(define-public crate-binod-0.1 (crate (name "binod") (vers "0.1.1") (hash "14c4ak1hv5ay8cqhwpa50pjhhjk2v8gbpfr8a2k90rg4w67vfyff")))

(define-public crate-binod-0.1 (crate (name "binod") (vers "0.1.2") (hash "1zcbp8hxdg06zaasshrybaphi5zmdxn15p6fzpbcx2jnh2c94ppx")))

(define-public crate-binomial-heap-0.1 (crate (name "binomial-heap") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0q2y437b41wjd6cdzwznmxg3npg6fi2954hdfsnmwaadz5fkxl39") (features (quote (("binary_heap_extras"))))))

(define-public crate-binomial-heap-0.2 (crate (name "binomial-heap") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "18k1k53zl6kkapg1i871wqm7biwp1x42209365mp735y2pn7x9j4") (features (quote (("binary_heap_extras"))))))

(define-public crate-binomial-iter-0.1 (crate (name "binomial-iter") (vers "0.1.0") (hash "1w2rwmq8g5gynx849631rsj9l5liryzja9bk84a7xz2ln9jazjz3")))

(define-public crate-binomial_tree-0.1 (crate (name "binomial_tree") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "048q0x2kjkg3m7x6q6kcwc5qcbivs7xnvdm8q1spw008h8354p01")))

(define-public crate-binomial_tree-0.2 (crate (name "binomial_tree") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1chdwrsn42g1959gixck6vxgx34cc21nd67240lwp7yg5kw6d7la")))

(define-public crate-binomial_tree-0.3 (crate (name "binomial_tree") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1frw4nqmxq19s33v5daf8b1y6212w3qi3ig1m7a6i40hmhr9hxy6")))

(define-public crate-binomial_tree-0.3 (crate (name "binomial_tree") (vers "0.3.1") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0irln3a69caxgv0m0img3zvwfqkqs36f1fw50js4p291a6x8kafr")))

(define-public crate-binomial_tree-0.3 (crate (name "binomial_tree") (vers "0.3.2") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1mw8lkb5k9l2jm41gbh7r270wia5ywzqnhibrlliqksl1ypp98yv")))

(define-public crate-binomial_tree-0.3 (crate (name "binomial_tree") (vers "0.3.3") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1mrz5pj8sm9w5z1q655f05zhlxdgxyzpb4gbrzg11gkdc607k16p")))

(define-public crate-binomial_tree-0.3 (crate (name "binomial_tree") (vers "0.3.4") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "141jdyl55gg93vcln1q5xdjrn52jdw2khw4739ifzya10r4s7mxf")))

(define-public crate-binomial_tree-0.3 (crate (name "binomial_tree") (vers "0.3.5") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1faf030hlkayyjb9dmsryygg3x7zvca1l3ff0diqwwwbk79v3pw3")))

(define-public crate-binomial_tree-0.3 (crate (name "binomial_tree") (vers "0.3.6") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0c29z95fb75md40jk88flx7q5nbf6x6nnfmr26fqa1zn59p0pj4a")))

(define-public crate-binomial_tree-0.4 (crate (name "binomial_tree") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "14mx926acz1qik5yzma1as8rxakgr9c528kr17lxv09hvi1d8v2n")))

(define-public crate-binomial_tree-0.5 (crate (name "binomial_tree") (vers "0.5.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "black_scholes") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ywq19ma186h0knpnixy4yavhwks07bfm4f2l70v2lq87iax4iwv")))

(define-public crate-binout-0.1 (crate (name "binout") (vers "0.1.0") (hash "1ra10ak5y9bvx7b764p8f2sfblw09apd0klwdrfd43wwfrw5lps9")))

(define-public crate-binout-0.1 (crate (name "binout") (vers "0.1.1") (hash "0i27g4f05rvllfdwjnjqk5i2bcd3kvwrgcs5li1i6jlh48qb2l2x")))

(define-public crate-binout-0.2 (crate (name "binout") (vers "0.2.0") (hash "0pqf627jny8mjn1wdz51sg8fsc2amnnxh8n8gnxmjsam00f7p318")))

(define-public crate-binout-0.2 (crate (name "binout") (vers "0.2.1") (hash "1spj4hh4xk23f3si8xpm1zw18a65bdrpvbi4wigwm3sqibw1l2xn")))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1r43pkgm8k5qa2mgjs67vlp9apaf677l41knyjsych5rci3g47ds")))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0b6rd8gmfg8c6a5zz62nk4afclzxm1d614jfw3qyh2m6v1j3q8rc")))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h3wk9cyc1y8f523xb58dmyjl3kfaj5scm6qkp0l7wn3hxkppv60")))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "161ws9fvzlyq2scj7bh7rzl1hy7mbw7jj9pdx5h6hn4hibdcyl79")))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0n4vgslayr59lyj6v97mi701nhgvyar163dcq2ylqry0wyc6nmqb")))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0si6zqjzgha9165sqb2ljq66xdgb665wmnmgprza10bkw33gsj1a") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1nd861djsl3li14mf4dx6ngi0hc1885fr6y17zail4yyad09m6h4") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "167hgga6gbsy80wghfyw3qk705g6j226m82z1zfbzzl5bhckgzgn") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.1 (crate (name "binoxxo") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1ag6gpcsxwhy5lk04fpinbcxv10lnrj1mvb4grn7m9gm89jgi1vm") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.2 (crate (name "binoxxo") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0gsnraxvzrfg6fq8wd13503mbipbv5vsg5p9kxlzhc5w09hmjixl") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.2 (crate (name "binoxxo") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "09j1lxw7b38jjh2jg10cmbimkwbwlff7yb7321pv48s3ql5pzilg") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.3 (crate (name "binoxxo") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "09vm6k6ip3ah02hwva2sl6ivi0kh13chigx6b4ccljhxrdj2apa6") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.4 (crate (name "binoxxo") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0lh2gczp8zppxqc9zf8qb2fqcyjqd0xvnmq28wy2lmg972sgylhx") (features (quote (("wasm-bindgen" "rand/wasm-bindgen") ("default"))))))

(define-public crate-binoxxo-0.5 (crate (name "binoxxo") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1kibn51mqv5026nd565lqqkb9lq38x1bp6cz9k8vclnmdgaalb93")))

