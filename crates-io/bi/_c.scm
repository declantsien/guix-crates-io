(define-module (crates-io bi _c) #:use-module (crates-io))

(define-public crate-bi_channel-0.1 (crate (name "bi_channel") (vers "0.1.0") (hash "0dm0wkcl9iri7gvapkqwiziy98adyqk6iwcjcgjls7zli0x0jww5")))

(define-public crate-bi_channel-0.1 (crate (name "bi_channel") (vers "0.1.1") (hash "025fgr7kija2gdr59nyrbrbpnsvqxahnzwwli1wb413fy27wz9zd")))

(define-public crate-bi_channel-0.1 (crate (name "bi_channel") (vers "0.1.2") (hash "065sm330zds91yrc3yqiqsmf0a4dp819fgd1kipg31q2vs958gnq")))

