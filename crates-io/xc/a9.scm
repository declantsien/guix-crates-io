(define-module (crates-io xc a9) #:use-module (crates-io))

(define-public crate-xca9548a-0.1 (crate (name "xca9548a") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0n9kdwmkgnh5wdcw8p033ag6y60sh59p9wj0qvziybzwb6wjz3xp") (features (quote (("default"))))))

(define-public crate-xca9548a-0.2 (crate (name "xca9548a") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "10nyh43d8mjvnc6nzsw3mdc953byd8inkwcf00h93v08gp3a0240") (features (quote (("default"))))))

(define-public crate-xca9548a-0.2 (crate (name "xca9548a") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0271dj9qj3r0z1y0jqg2ykv3kz5qfdi7icpd3c8cqvhf01jcwmlw") (features (quote (("default"))))))

