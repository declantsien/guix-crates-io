(define-module (crates-io xc ha) #:use-module (crates-io))

(define-public crate-xchange-rs-0.1 (crate (name "xchange-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jabxr9vxh3blgykg62b9grv8cv8f6cr7cpn1y5ykyjfhnxiamg4") (yanked #t)))

(define-public crate-xchange-rs-0.1 (crate (name "xchange-rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h1hrqbrm4vwwlqxrfr86gsg31q2xg9gmpbp6y32imzai0kir43w")))

