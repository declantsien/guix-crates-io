(define-module (crates-io xc h_) #:use-module (crates-io))

(define-public crate-xch_ceb_lib-0.1 (crate (name "xch_ceb_lib") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mjgrxg63byajfvikbld49dgw621gyzi4qra3v2cmx231vdbzi1z") (yanked #t)))

