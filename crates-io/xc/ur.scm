(define-module (crates-io xc ur) #:use-module (crates-io))

(define-public crate-xcur-0.1 (crate (name "xcur") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "1chbfnh2a6wivwg45yv5ghc4bw67mafkvl4rhn4fnxl8dgrgl5sn")))

(define-public crate-xcur-0.1 (crate (name "xcur") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "0s47ybkmjxlrnpja4jfbj72dab8k4xb9v63nrx4mknaa51qagn47") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-xcur-0.1 (crate (name "xcur") (vers "0.1.2") (deps (list (crate-dep (name "clippy") (req "^0.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "1d7lyi7vv9n9qrirjski3wag7px72jyi6npkq0hcx9cs7nnmgvrf") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-xcur-0.1 (crate (name "xcur") (vers "0.1.3") (deps (list (crate-dep (name "clippy") (req "^0.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "12qy40ckdibdh8pzlrcn9sb11hir991n18niyf6gbj42dq07j65g") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-xcursor-0.1 (crate (name "xcursor") (vers "0.1.0") (deps (list (crate-dep (name "rust-ini") (req "^0.15") (default-features #t) (kind 0)))) (hash "04w6kdkq95i273r8w8pshp7abw3n0m8kyx5s83l3r1j9n9gw2wpq")))

(define-public crate-xcursor-0.1 (crate (name "xcursor") (vers "0.1.1") (deps (list (crate-dep (name "rust-ini") (req "^0.15") (default-features #t) (kind 0)))) (hash "18fqjajwrx87wd31gmzl5qnbrn33h2n4mnrc8zrqn3w7ysrg6gxd")))

(define-public crate-xcursor-0.1 (crate (name "xcursor") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1cczzjxz5slv8rxd50pk0q1sbpv1laws40bb0ci7nhcwi8ypw8ix")))

(define-public crate-xcursor-0.2 (crate (name "xcursor") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vwzn4s2ysi12cc7d5162rdcqnnzazbk0lqch6gs3nc4m7ivf2ab")))

(define-public crate-xcursor-0.3 (crate (name "xcursor") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "05yxfrdimm2akfxg14rpif1cr50kjhx6szmpiynfp9c6271qqmhs")))

(define-public crate-xcursor-0.3 (crate (name "xcursor") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "0d8lxz3yyxdnalc8cv6pycv4d5bl6d5gvww925nz9l22nf8frvan")))

(define-public crate-xcursor-0.3 (crate (name "xcursor") (vers "0.3.2") (deps (list (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "008d3cqp3pf5bngnai4m0fc5qs8d02l37bkk032y2dgxvv7q396k")))

(define-public crate-xcursor-0.3 (crate (name "xcursor") (vers "0.3.3") (deps (list (crate-dep (name "nom") (req "^6.0") (kind 0)))) (hash "022x7jm71dyqrxwsjkqfgj8bx57y7g8yyz318qb80y5ffhaj76is")))

(define-public crate-xcursor-0.3 (crate (name "xcursor") (vers "0.3.4") (deps (list (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)))) (hash "1dwh8vkll79i2vr56dmn9fdpw2ig0klcb0a3300l7k8k6fk0ads6")))

(define-public crate-xcursor-0.3 (crate (name "xcursor") (vers "0.3.5") (hash "0499ff2gy9hfb9dvndn5zyc7gzz9lhc5fly3s3yfsiak99xws33a")))

(define-public crate-xcursorlocate-0.1 (crate (name "xcursorlocate") (vers "0.1.1") (deps (list (crate-dep (name "confy") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^0.9") (features (quote ("shape"))) (default-features #t) (kind 0)))) (hash "1wx00ay1kxs3yc0qg4zwp0l7pynklpxvkvjckb6yzx1dqxif0qy7")))

