(define-module (crates-io xc #{2b}#) #:use-module (crates-io))

(define-public crate-xc2bit-0.0.0 (crate (name "xc2bit") (vers "0.0.0") (hash "0ppxn88hcacfbd4z9y1kn4xl9dz2cfn5ni135x3527f79m43v4bl")))

(define-public crate-xc2bit-0.0.1 (crate (name "xc2bit") (vers "0.0.1") (hash "0f3ncrpbiaza1ydvr28dr2a44wrm7ig013qw50zwiy26qmx35idp")))

(define-public crate-xc2bit-0.0.2 (crate (name "xc2bit") (vers "0.0.2") (hash "1zr4sxn38g3hd23alaxdkaw1q2dymvkkx28zqq5f4c7q244y7bdg")))

(define-public crate-xc2bit-0.0.3 (crate (name "xc2bit") (vers "0.0.3") (deps (list (crate-dep (name "bittwiddler") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "jedec") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yosys-netlist-json") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0bdlalzxpp5lvkm98zwd8f3aljh62axyxbs95g4hyd013drmffw2")))

(define-public crate-xc2bit-0.0.4 (crate (name "xc2bit") (vers "0.0.4") (deps (list (crate-dep (name "bittwiddler") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "jedec") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yosys-netlist-json") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "19y03p9ybhr4wvzfmybd3g4zcnyh69xlhjxspbm78sj5pa4y7dk5")))

