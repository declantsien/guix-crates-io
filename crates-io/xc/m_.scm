(define-module (crates-io xc m_) #:use-module (crates-io))

(define-public crate-xcm_grpc_lib-0.1 (crate (name "xcm_grpc_lib") (vers "0.1.0") (hash "1vs7ca6wizn5wryqg2zf64551cyvaxswvh92zsl19cpg8g1hz533")))

(define-public crate-xcm_grpc_lib-0.1 (crate (name "xcm_grpc_lib") (vers "0.1.1") (deps (list (crate-dep (name "async-stream") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.6") (default-features #t) (kind 1)))) (hash "14n1lrqpn7hkv31zi53ih3mpyzs73l0z4m7i9dm135liqxhpfip3")))

