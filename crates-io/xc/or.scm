(define-module (crates-io xc or) #:use-module (crates-io))

(define-public crate-xcore-0.1 (crate (name "xcore") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "110219zb1ybhldsfygkhlkp0y2nl2z2rs9lhymfp8kmfp8kc3dba")))

(define-public crate-xcore-0.1 (crate (name "xcore") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0shhfkzw91r80jrsqp8szsrivqbnlvanvbvslmzkgsqdddm0sxil")))

(define-public crate-xcore-0.1 (crate (name "xcore") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1339l152dyc22pn33xj5vp4rcqan5lwchdswa2iq472gzk1vpbs1")))

(define-public crate-xcore-0.1 (crate (name "xcore") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0l3xafx5gjp2axk21kp9rnkjs0bg1vsdplshn129mksynmgr4r2s")))

