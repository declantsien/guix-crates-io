(define-module (crates-io xc sp) #:use-module (crates-io))

(define-public crate-xcsp3-rust-0.1 (crate (name "xcsp3-rust") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.29.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)))) (hash "1xdpam10dklksan96iv29jiw9ndn3ilxmgcy1gzvp3ndzmymzx5d") (rust-version "1.70")))

