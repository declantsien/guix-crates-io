(define-module (crates-io xc ms) #:use-module (crates-io))

(define-public crate-xcmsg-0.0.0 (crate (name "xcmsg") (vers "0.0.0") (hash "1kvphh7la1n25awyfgmdbnfhw809fiss0sk9k0cr0h2q6g407dd3") (yanked #t)))

(define-public crate-xcmsg-builder-0.0.0 (crate (name "xcmsg-builder") (vers "0.0.0") (hash "0a3i0xazlii7k35xhz0kbmsiqps3y5d23vnfxmdmqa138nb0bxr8") (yanked #t)))

(define-public crate-xcmsg-executor-0.0.0 (crate (name "xcmsg-executor") (vers "0.0.0") (hash "0za48srbvc7dyx7vvcpqxasps6h421g8b3433f2yza7dvqr29pyp") (yanked #t)))

