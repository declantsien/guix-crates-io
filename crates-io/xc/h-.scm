(define-module (crates-io xc h-) #:use-module (crates-io))

(define-public crate-xch-ceb-0.1 (crate (name "xch-ceb") (vers "0.1.0") (hash "1d7n3wcr0kiixjdmciqb13khib6p6sh1np6czzj7nwmgn75g50pj")))

(define-public crate-xch-ceb-0.1 (crate (name "xch-ceb") (vers "0.1.1") (hash "1mzb1xa6lhqbfvvdxql830kp7lhspk1w9gk1hv0q6d02flvvvpvq")))

(define-public crate-xch-ceb-0.1 (crate (name "xch-ceb") (vers "0.1.2") (hash "0m8cagissbf5wdmjk29zq89x4z4qnxr9mhan2nygldz66c4iy5gi")))

(define-public crate-xch-ceb-0.1 (crate (name "xch-ceb") (vers "0.1.3") (hash "1c09hcava02lgnpn3dzli1v1aa6v24f0bb1a3ym1qwc100h0gihs")))

(define-public crate-xch-ceb-0.1 (crate (name "xch-ceb") (vers "0.1.4") (hash "0jz98y4y3j0lpv8gdp7plkhxpzrsbcmgcl6hylghxpxyhfgnn3h5")))

(define-public crate-xch-ceb-0.1 (crate (name "xch-ceb") (vers "0.1.5") (hash "193sgjcj0imql83q610m0g15c664gs6ngghhb19jyrjn6gjvb8lx")))

(define-public crate-xch-ceb-1 (crate (name "xch-ceb") (vers "1.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "029m6a6jvxr4m7wcsfxjhgcm3h7g08p30nwhhp2jxv26np8rxy3k") (yanked #t)))

(define-public crate-xch-ceb-1 (crate (name "xch-ceb") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fmair8c11vsnvrg5dj0nnh4sfvh5mydm0v2g4ydlwqsgvrsvicj") (yanked #t)))

(define-public crate-xch-ceb-1 (crate (name "xch-ceb") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y3aw8s3mkmr4afprb8bdwpsndfaprn0lq46zbywyvszh624784f") (yanked #t)))

(define-public crate-xch-ceb-0.2 (crate (name "xch-ceb") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dk9bld77bsl94lpqk71q4y4mngg7gcbkll5xs7dbzh5i39cv36s")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.0") (deps (list (crate-dep (name "lib_xch") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0rkipqwgfjpy78y93zd51h07v373hri3f9jsb3r7z6dyg8m1n1b7")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.1") (deps (list (crate-dep (name "lib_xch") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0mrcci3b8wgsf40wnphxsha8pw1mn0lhgxwc9pa5rhkr779pvf2d")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.2") (deps (list (crate-dep (name "lib_xch") (req "^0.2") (default-features #t) (kind 0)))) (hash "04l31fx57fpcf415ylg4m8kax4cmzfq8f1fwywsvr817j69xpba0")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.3") (deps (list (crate-dep (name "lib_xch") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y0p5pa6j2plp6fyy5hs4m6z724d3yi2zxl7wsazv2i4f2p19i6p")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.4") (deps (list (crate-dep (name "lib_xch") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k6svjfgjpy9h9fgs31bp7y2gcgmv4j6ldgdazgb5ryhhxs2k8qp")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.5") (deps (list (crate-dep (name "lib_xch") (req "^0.3") (default-features #t) (kind 0)))) (hash "0iqkjr3qbsijpmg7hxadyiqjf7lq970agcn2zg76h2d98bs3a283")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.6") (deps (list (crate-dep (name "lib_xch") (req "^0.3") (default-features #t) (kind 0)))) (hash "10qq029wpb51fh6kl110djj6z7whpqws0jsibks79188krg31gqc")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.7") (deps (list (crate-dep (name "lib_xch") (req "^0.6") (default-features #t) (kind 0)))) (hash "06p5drgn3zcqnwvfdisnm5w065rf3jrhhf8zq7xbbzaqkwnzs3r5")))

(define-public crate-xch-ceb-0.3 (crate (name "xch-ceb") (vers "0.3.8") (deps (list (crate-dep (name "lib_xch") (req "^0.7") (default-features #t) (kind 0)))) (hash "1flqs3pzijvnss16aq6zpqv2q8caqx4x9wq8yavs2svcfkzlkp4h")))

(define-public crate-xch-ceb-0.4 (crate (name "xch-ceb") (vers "0.4.0") (deps (list (crate-dep (name "lib_xch") (req "^0.10") (default-features #t) (kind 0)))) (hash "0i5rkpih07vsa9xnshkfidz6zgw40bqxr2z41j3jycvp1wbd5498")))

(define-public crate-xch-ceb-0.4 (crate (name "xch-ceb") (vers "0.4.1") (deps (list (crate-dep (name "lib_xch") (req "^0.10") (default-features #t) (kind 0)))) (hash "12nfszlpgdrff11s2p54v94qz6jiyfgc027p4lnk4nw75gpdkmb6")))

(define-public crate-xch-ceb-0.4 (crate (name "xch-ceb") (vers "0.4.2") (deps (list (crate-dep (name "lib_xch") (req "^0.10") (default-features #t) (kind 0)))) (hash "1v159amzq21r2mm8a72wbh9wgkv18f0k9pzi0dscn2h67q0madmb")))

(define-public crate-xch-ceb-0.4 (crate (name "xch-ceb") (vers "0.4.3") (deps (list (crate-dep (name "lib_xch") (req "^0.11") (default-features #t) (kind 0)))) (hash "1ikf3pxrckw15z7bqvkh0lf4v4ylaa9jdm5f4mvpll49xi75b4a0")))

(define-public crate-xch-ceb-0.4 (crate (name "xch-ceb") (vers "0.4.4") (deps (list (crate-dep (name "lib_xch") (req "^0.11") (default-features #t) (kind 0)))) (hash "1363k546ksrlcy3gqp0rwljbg7536jbrz0gmh5gr5amf5pj0hsxz")))

(define-public crate-xch-ceb-0.5 (crate (name "xch-ceb") (vers "0.5.0") (deps (list (crate-dep (name "lib_xch") (req "^0.12") (default-features #t) (kind 0)))) (hash "0vrbgmcl3dli6jrkl3dyad3zl7gsjqn37mc223acppzbadi9kim9")))

(define-public crate-xch-ceb-0.5 (crate (name "xch-ceb") (vers "0.5.1") (deps (list (crate-dep (name "lib_xch") (req "^0.12") (default-features #t) (kind 0)))) (hash "0cqmlq89qcn22z8sysaj8p3spir64j9v7q7b1ynmasyq1qaz7rn7")))

(define-public crate-xch-ceb-0.5 (crate (name "xch-ceb") (vers "0.5.2") (deps (list (crate-dep (name "lib_xch") (req "^0.13") (default-features #t) (kind 0)))) (hash "08d64sfaz7l0a1g4m5lxjgc10l5g44fbxss5vzh8wrvbw731yl8b")))

