(define-module (crates-io xc ru) #:use-module (crates-io))

(define-public crate-xcrun-1 (crate (name "xcrun") (vers "1.0.0") (hash "1hnv5w6jg8ihvw8q4dvq8g36i6dq8vwsyn93c3xnzz7nl7mr5qpz")))

(define-public crate-xcrun-1 (crate (name "xcrun") (vers "1.0.1") (hash "1rrkwv4ga014dvfh33b28w1w26cajgdx20pshkwvcw44dbwg978a")))

(define-public crate-xcrun-1 (crate (name "xcrun") (vers "1.0.2") (hash "1dr7k6k661ggs2y6bv09hnaw6s0hi9vi1d2yxb1q5zxap22yfhr4")))

(define-public crate-xcrun-1 (crate (name "xcrun") (vers "1.0.3") (hash "0bhmxxj65zainrpvbipcszscdqqair0r43sn229b3786m4x1gs2j")))

(define-public crate-xcrun-1 (crate (name "xcrun") (vers "1.0.4") (hash "1w4l8y8r33w13aacmwdifpgz5ycc56znh23li9za31r8d0js7yhr")))

