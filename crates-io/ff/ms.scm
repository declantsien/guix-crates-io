(define-module (crates-io ff ms) #:use-module (crates-io))

(define-public crate-ffms2-0.1 (crate (name "ffms2") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ffms2-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "0rbn5mf7sn7z5g463yw6hfdjgvkc4r927yrmznja1x43k8psh5ra")))

(define-public crate-ffms2-0.2 (crate (name "ffms2") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ffms2-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "1cyw6jg5sb04ldsscwxj9pwc35xs58ycp7za5d4g0fky17mhijmn")))

(define-public crate-ffms2-sys-0.1 (crate (name "ffms2-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1") (default-features #t) (kind 1)))) (hash "07iggscnqkrxr20j1sb5dfwnjla91cwbgl34c6lh36nkisn3xfx5")))

(define-public crate-ffms2-sys-0.2 (crate (name "ffms2-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1") (default-features #t) (kind 1)))) (hash "0jmbwc2dfn7r54q69ryqfislrm7m2g20v677kp6dbrw56ygssr59")))

