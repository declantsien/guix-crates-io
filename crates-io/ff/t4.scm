(define-module (crates-io ff t4) #:use-module (crates-io))

(define-public crate-fft4r-0.1 (crate (name "fft4r") (vers "0.1.0") (deps (list (crate-dep (name "kissfft-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.3") (default-features #t) (kind 0)))) (hash "09girwvzrwpdy9zmxycwmw3b7ra0mxkf8d2607s852shwzp2q7jk")))

