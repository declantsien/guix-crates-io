(define-module (crates-io ff th) #:use-module (crates-io))

(define-public crate-ffthumb-0.1 (crate (name "ffthumb") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.26") (default-features #t) (kind 1)))) (hash "0vlm3jdbjv1nf2wc2pdm5nq2apxdwjcv118caym8mzsccsvv2j2f") (v 2) (features2 (quote (("bindgen" "dep:bindgen"))))))

(define-public crate-ffthumb-0.2 (crate (name "ffthumb") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.26") (default-features #t) (kind 1)))) (hash "0ni5h369z3nzk4lsf6n73p7zldy70b53s0fzibczvnfxxkjjwy8c")))

(define-public crate-ffthumb-0.2 (crate (name "ffthumb") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.26") (default-features #t) (kind 1)))) (hash "10zzycb97pfmrld1p85zjd1jsiynkc4p112szdg8df2w7gvs4hr9")))

