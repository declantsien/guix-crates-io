(define-module (crates-io ff ts) #:use-module (crates-io))

(define-public crate-ffts-0.1 (crate (name "ffts") (vers "0.1.0") (deps (list (crate-dep (name "ffts-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "115cmq8qyw32d2nhkpmvbwxm69n8cs904m6k2f6r1k381sjfa94k")))

(define-public crate-ffts-0.1 (crate (name "ffts") (vers "0.1.1") (deps (list (crate-dep (name "ffts-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "09yfx0s7k24zik2h93nnaq6vmgjs1rg408a469rlwqhm1yzy3dv1")))

(define-public crate-ffts-0.1 (crate (name "ffts") (vers "0.1.3") (deps (list (crate-dep (name "ffts-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "0fp7dldnkichpck4rhmb87f5lbvjf9g1j30vri94v1l0dbjxx6s2")))

(define-public crate-ffts-sys-0.1 (crate (name "ffts-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 2)))) (hash "0j1bmxlz1ijxmcgijlpl82c82hsgq1p5wpfa4zkbc7lhh6acv32q")))

(define-public crate-ffts-sys-0.1 (crate (name "ffts-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 2)))) (hash "1hxr18843rrsf9wwhr05qb71pl9s8ffx0i6ikw5ajh60gsim1xd4")))

(define-public crate-ffts-sys-0.1 (crate (name "ffts-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 2)))) (hash "10pvmbxywaka84kmipqgp587qyslwrbf8wq7np45jiikgijvf279")))

(define-public crate-ffts-sys-0.1 (crate (name "ffts-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 2)))) (hash "0zh1mcxi6xabq5vba4s35adyfwqbnzpvh5jlbf4cq67412slii96")))

