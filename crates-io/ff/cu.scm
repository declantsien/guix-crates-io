(define-module (crates-io ff cu) #:use-module (crates-io))

(define-public crate-ffcutter-0.1 (crate (name "ffcutter") (vers "0.1.0") (deps (list (crate-dep (name "shell-words") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n6j33j80xrf4j8dfq20k9jqkxqhn9g57d6bvjbbnpqi70a1fl06")))

