(define-module (crates-io ff me) #:use-module (crates-io))

(define-public crate-ffmetadata-0.1 (crate (name "ffmetadata") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1r09znfc01csn454ijhqav293vr8li3lr22ap497nqijvb5xv0yx")))

(define-public crate-ffmetadata-0.1 (crate (name "ffmetadata") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "11w36qbk4sd6dbhfk8d9kx80zkqpgg43d5c7zz7b2prvgykcbixb")))

(define-public crate-ffmetadata-0.1 (crate (name "ffmetadata") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "13w0li8v3dsi4n38sz4bcx0ldv10xwilcjznxlvcn0y7r7hlisas")))

