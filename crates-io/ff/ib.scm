(define-module (crates-io ff ib) #:use-module (crates-io))

(define-public crate-ffiber-0.1 (crate (name "ffiber") (vers "0.1.0") (deps (list (crate-dep (name "color-eyre") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1lwajfd905rxx20cszdrkfj06bd5a0rndp9hbm39mg37ckip3nf2")))

