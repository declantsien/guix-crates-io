(define-module (crates-io ff _k) #:use-module (crates-io))

(define-public crate-ff_k_center-1 (crate (name "ff_k_center") (vers "1.2.2") (deps (list (crate-dep (name "num_cpus") (req "^1.16") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.14.4") (features (quote ("extension-module" "abi3-py36"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1k8yijf5kb1kbb6swqpxy6zhx2m8cfga8h2f11qwlwiywvph17zl")))

