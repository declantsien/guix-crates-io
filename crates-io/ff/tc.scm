(define-module (crates-io ff tc) #:use-module (crates-io))

(define-public crate-fftconvolve-0.1 (crate (name "fftconvolve") (vers "0.1.0") (deps (list (crate-dep (name "easyfft") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0yll627xm19qdl19ddk7ih0vshpcpsc1xvzgmby04ffnmgxb6l0f")))

(define-public crate-fftconvolve-0.1 (crate (name "fftconvolve") (vers "0.1.1") (deps (list (crate-dep (name "easyfft") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "132krq0728mzghdi35dg8g5mllvvbcp04043wkwbzb78wnpw7m2b")))

