(define-module (crates-io ff in) #:use-module (crates-io))

(define-public crate-ffind-0.1 (crate (name "ffind") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "jwalk") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1lv5prcmkq3s4av4kfmq2rx1kbbmvrjk7h28xk3cn903c0pcj7kg")))

