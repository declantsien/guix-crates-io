(define-module (crates-io ff -d) #:use-module (crates-io))

(define-public crate-ff-derive-num-0.1 (crate (name "ff-derive-num") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0y1sxpqmmmkzkn278bms92ngmbways1jajap1pzapmb0wslm87q2")))

(define-public crate-ff-derive-num-0.1 (crate (name "ff-derive-num") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "084l96rqyy9hgmfpjlifigmzkgw0n0251irgqxd9hqci4il7bf9z")))

(define-public crate-ff-derive-num-0.1 (crate (name "ff-derive-num") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0bwssklvfr2jijif1356ravpizpd2mwgi2d6qiqp9frd5a6d041g")))

(define-public crate-ff-derive-num-0.2 (crate (name "ff-derive-num") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1x2cxnc4dqdzrs1cpir6shx10wi3q5g2fylj3r40dgl3p4d4a5g7")))

