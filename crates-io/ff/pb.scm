(define-module (crates-io ff pb) #:use-module (crates-io))

(define-public crate-ffpb-0.1 (crate (name "ffpb") (vers "0.1.0") (deps (list (crate-dep (name "kdam") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1sq3k0sqsp3ng7vn5wwjhx8jbpw90vs4k1y9lnkb0qam0mf1j09r")))

(define-public crate-ffpb-0.1 (crate (name "ffpb") (vers "0.1.1") (deps (list (crate-dep (name "kdam") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "16fj76c81cz8lmb3g7mnzd244whjbbgah5z8ik9fv7m3aakq9654")))

(define-public crate-ffpb-0.1 (crate (name "ffpb") (vers "0.1.2") (deps (list (crate-dep (name "kdam") (req "^0.5") (features (quote ("rich"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)))) (hash "1fppcifwv2ry6zzyx9vi1j9x57060rr5xzqvbwwpa010rxw9wqpf")))

