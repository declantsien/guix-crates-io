(define-module (crates-io ff cn) #:use-module (crates-io))

(define-public crate-ffcnt-0.1 (crate (name "ffcnt") (vers "0.1.0") (deps (list (crate-dep (name "btrfs") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0igjvagvsl6zcsjzcz7xabz08snmw891px9if4037x8rl6vg1ziy")))

(define-public crate-ffcnt-0.1 (crate (name "ffcnt") (vers "0.1.1") (deps (list (crate-dep (name "btrfs") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0zym2m1jfxwz4y77nspbkfr3c4jqkgia7yqcxv3bvzanhy81l86x")))

(define-public crate-ffcnt-0.1 (crate (name "ffcnt") (vers "0.1.2") (deps (list (crate-dep (name "btrfs") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0xv87gcnb4yxrwm26jnz4zilzfd19k3cqq9yb8g6yaipgbaq4x2l")))

(define-public crate-ffcnt-0.1 (crate (name "ffcnt") (vers "0.1.3") (deps (list (crate-dep (name "btrfs") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1asda82r9xgbq6li8bf60yzvh6x88dgqv9svmrpb0abgzm3s72h5")))

(define-public crate-ffcnt-0.2 (crate (name "ffcnt") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "platter-walk") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15hnls0h3g88m7hn1m3xns507s4j0947sgxavdv5vxyi9im6dibd")))

(define-public crate-ffcnt-0.2 (crate (name "ffcnt") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "platter-walk") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0d9mrf7gi5zyq0lj0zmfcyycs7y9b75qdkgv7f5cfpj7a54rf003") (features (quote (("system_alloc"))))))

(define-public crate-ffcnt-0.2 (crate (name "ffcnt") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "platter-walk") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0axl4nz1dn73r7wr4jqy5lhg4waw6x6yyyyjyasxfdsbasx3pkfv") (features (quote (("system_alloc"))))))

(define-public crate-ffcnt-0.3 (crate (name "ffcnt") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "platter-walk") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "12afykbykzy0wwh3n6jb7d0hc74w3qkklx2pvsqdxgvd86cmda95")))

(define-public crate-ffcnt-0.3 (crate (name "ffcnt") (vers "0.3.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "platter-walk") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "18qc8jlk2xi41bz1ynsybdj2k85lq080smmvfxcyp18mwknf1clb")))

(define-public crate-ffcnt-0.3 (crate (name "ffcnt") (vers "0.3.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "platter-walk") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0iyqamg05lhxmixyp4k12k5gjr6mal5wi82ibr5ghxxwjf5gcrqy")))

