(define-module (crates-io ff _b) #:use-module (crates-io))

(define-public crate-ff_buffer-0.1 (crate (name "ff_buffer") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 1)))) (hash "03mj00mqry0hb5lp9382ixp3hvzhp023r3wx3gndrf0ahbmxa4bk") (features (quote (("crosslto")))) (links "libffbuffer")))

