(define-module (crates-io ff t-) #:use-module (crates-io))

(define-public crate-fft-convolver-0.1 (crate (name "fft-convolver") (vers "0.1.0") (deps (list (crate-dep (name "realfft") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "15vs8y62v6zn0m0r2wnj7kvnhrcspb4rsw2s7wl8p61hbxp5xm27")))

(define-public crate-fft-convolver-0.2 (crate (name "fft-convolver") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "realfft") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1z74p8x9li18xikm8z8vw0l5j1l8zz4arwppfc0p2bcmsdrh9kxx")))

(define-public crate-fft-lab-0.0.0 (crate (name "fft-lab") (vers "0.0.0") (hash "0d4zdnxjjyf74d8am4a7hxr2jqb8sf13jp7ss8kyj7rjq6sgbw7f") (rust-version "1.64")))

(define-public crate-fft-shop-0.1 (crate (name "fft-shop") (vers "0.1.0") (hash "0kbc6wjrlkx1jgpbhir2wwcfgmgrs3dv7hh4vfkymlm8fbr0yzwp")))

(define-public crate-fft-shop-0.1 (crate (name "fft-shop") (vers "0.1.1") (hash "0jr04haja776k7n6q9svvw34yi385qhhl7p5xlfxpvsgjrm01cx8")))

