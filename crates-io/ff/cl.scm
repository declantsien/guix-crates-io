(define-module (crates-io ff cl) #:use-module (crates-io))

(define-public crate-ffcli-0.1 (crate (name "ffcli") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.7") (default-features #t) (kind 0)))) (hash "1fh3hjwpd5gpkknj70c4d9mdm9qqi5m8lqbngjygdxnvh0by688j")))

(define-public crate-ffcli-0.1 (crate (name "ffcli") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0fhnllbwwam43w6g1gx8nril1fvxy25wdqvgim9wjq7wqvkg7khs")))

(define-public crate-ffcli-0.1 (crate (name "ffcli") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0nzcd4njxymmv7ss8azczqh1vmvif7llnkdgi73bhy6iijz6kx7y")))

(define-public crate-ffcli-0.1 (crate (name "ffcli") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1jfd093g5cx9lllrk2vvc0sna24pfbcdl9zk3cblbp8frhg09zav")))

(define-public crate-ffcli-0.1 (crate (name "ffcli") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0z72vxx6wn266znxs5dg8z5a9ldimd4irhgv6jx54qfns2238dqy")))

(define-public crate-ffcli-0.1 (crate (name "ffcli") (vers "0.1.5") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1ag61wrp1jyl9acg6y4987h47f050bzz2gpwhjs3fwmrpkn91jbw")))

(define-public crate-ffcli-0.1 (crate (name "ffcli") (vers "0.1.6") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0x31c731p48ax09x50bhdpxv2xdjsz1chnw60w41ry1mw7xxx42z")))

