(define-module (crates-io ff nn) #:use-module (crates-io))

(define-public crate-ffnn-0.1 (crate (name "ffnn") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.30.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1c7s610lj0s28bwpfkx7cb5wly9ps59dxczydkdwh1jv94h1hnqd")))

