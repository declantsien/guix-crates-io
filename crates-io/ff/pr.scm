(define-module (crates-io ff pr) #:use-module (crates-io))

(define-public crate-ffprobe-0.1 (crate (name "ffprobe") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "1wjlw7f6362i947hx7dpc8zqc7cxjqvnlnwlpdgz42ld2avhfxyc") (features (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.2 (crate (name "ffprobe") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "195265rcq3bxnv52c5zqy1lgghnn9d6cas942i510ihkvipvgy3a") (features (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3 (crate (name "ffprobe") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0si1zilg30j4wlypj0y2p2qrdak06fbqpygrppjr7752f1qdafgl") (features (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3 (crate (name "ffprobe") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0yhhqzx13944dcbk9f12z8bfk8k091c3x7ry71vhy78xk70yfbz5") (features (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3 (crate (name "ffprobe") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "1b0lnjgd75p5fqqm9s72sglgsbgh4j1ri8dc9864173hldjd6la1") (features (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.3 (crate (name "ffprobe") (vers "0.3.3") (deps (list (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "190hd7bwpmlzd9glyzv5zfk4jvwwdjhby05zrrszqadh9abh7mmm") (features (quote (("__internal_deny_unknown_fields"))))))

(define-public crate-ffprobe-0.4 (crate (name "ffprobe") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0b55x6s5bmlwv0fh2zwqbwqizzlxbjwxnamvnlfibb7rw4szizlg") (features (quote (("__internal_deny_unknown_fields"))))))

