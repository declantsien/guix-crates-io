(define-module (crates-io ff la) #:use-module (crates-io))

(define-public crate-fflazy-0.1 (crate (name "fflazy") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "0n2xlhkqfysjzc2gps0pw1asw7waz92b6m7v9qyji054s2lwsxlj")))

