(define-module (crates-io ff ix) #:use-module (crates-io))

(define-public crate-ffix-0.1 (crate (name "ffix") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f3r3h202q65iwddsq5ywaxv811izsavnf54lh1vjk4gf1d69pha") (yanked #t)))

