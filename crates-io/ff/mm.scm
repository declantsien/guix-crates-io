(define-module (crates-io ff mm) #:use-module (crates-io))

(define-public crate-ffmml-0.1 (crate (name "ffmml") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "textparse") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w49hx8q6m8i9zckdqwlj314n9ad40rs5jxfqzbsijmd9phryh75")))

(define-public crate-ffmml-0.1 (crate (name "ffmml") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "textparse") (req "^0.1") (default-features #t) (kind 0)))) (hash "13ba11g39gjr4q07q3nl43lm4q0iw3lr92nwr616x5y7wli2dyfh") (features (quote (("wav" "byteorder"))))))

(define-public crate-ffmml-0.1 (crate (name "ffmml") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "textparse") (req "^0.1") (default-features #t) (kind 0)))) (hash "1r5i3cl9lnm28yz2njcy16ax4axb8wyriwjbiqlgnk0j1j62vriz") (features (quote (("wav" "byteorder"))))))

(define-public crate-ffmmlc-0.1 (crate (name "ffmmlc") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ffmml") (req "^0.1") (default-features #t) (kind 0)))) (hash "152f2wxwkbr7cihm7fkpkg8fvlzvgqcw1a4nhbkda0fnw9sby1mv")))

