(define-module (crates-io ff -p) #:use-module (crates-io))

(define-public crate-ff-particles-0.1 (crate (name "ff-particles") (vers "0.1.0") (deps (list (crate-dep (name "macroquad") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nanoserde") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bdimyicp4k1c6shwy21jrlzzggldl1mmkqn2nia8c2rlklfx6d0")))

(define-public crate-ff-particles-0.1 (crate (name "ff-particles") (vers "0.1.1") (deps (list (crate-dep (name "macroquad") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nanoserde") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0csql8v5i8sv09xpxksk40jk8kxn25cnyxxw1flabf1bcw19cmvk")))

(define-public crate-ff-particles-0.1 (crate (name "ff-particles") (vers "0.1.2") (deps (list (crate-dep (name "macroquad") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nanoserde") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1h2522gq90r32m09aj70nfms157ssm68j9wv6rl54g5p6g20aq25")))

(define-public crate-ff-profile-0.1 (crate (name "ff-profile") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1rp1ymvhksrrx44islay76x00bf8h153jaxafcf4l8rhhivq1lfm")))

