(define-module (crates-io ff ig) #:use-module (crates-io))

(define-public crate-ffigen-0.0.1 (crate (name "ffigen") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "*") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "*") (default-features #t) (kind 0)))) (hash "0qj4dafwy221j3h2nfvdnm69210c0axvy7adysd0h80slkwvyn2y")))

