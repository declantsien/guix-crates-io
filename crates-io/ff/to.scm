(define-module (crates-io ff to) #:use-module (crates-io))

(define-public crate-fftools-1 (crate (name "fftools") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5.0") (features (quote ("eq-separator" "short-space-opt" "combined-flags"))) (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1pfl1rr3ik61zvmzch5iaavvc8b025hjmq02wxizb1gq4v3bvi18")))

