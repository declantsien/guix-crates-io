(define-module (crates-io c0 sv) #:use-module (crates-io))

(define-public crate-c0sv-0.1 (crate (name "c0sv") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^6") (kind 0)))) (hash "07ykka07jqi4vr03h30rcvpwl3sdzdldqvs1dqcijc7mp2aisi9m")))

(define-public crate-c0sv-0.1 (crate (name "c0sv") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^6") (kind 0)))) (hash "0x6cvs37si0cmy5d14kbnnhpwf3qi3wljn2j414jmf9spjqa0gw2")))

(define-public crate-c0sv-0.2 (crate (name "c0sv") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^6") (kind 0)))) (hash "1dxzl383299x0g8sqz5a78j3087znkrj26q6mqwmin6cz2b0yfjm") (features (quote (("std") ("default" "std"))))))

