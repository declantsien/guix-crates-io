(define-module (crates-io wi lo) #:use-module (crates-io))

(define-public crate-wilo-0.1 (crate (name "wilo") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "termwiz") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0aialc2ay26ax0ksjc4qxmf0jmchxy10nnbihwqdadzy3q6nqign")))

(define-public crate-wilo-0.2 (crate (name "wilo") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "termwiz") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1x5mdsp3ycn43lv734qxyx80qvk62h4rys1qpxc78mvjcrz6wq3h")))

(define-public crate-wilo-0.3 (crate (name "wilo") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "termwiz") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "16gpiry0q796583krgqc5x2pk9ybpypjvahjj95pwcx7kkwl8f4m")))

