(define-module (crates-io wi ms) #:use-module (crates-io))

(define-public crate-wimshurst-0.1 (crate (name "wimshurst") (vers "0.1.0") (deps (list (crate-dep (name "topologic") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0m5j3a2qfhpnd3rsmr9gg6ran5ncbr2g0qkd9hhfrdkcbqa1xdqv")))

(define-public crate-wimshurst-1 (crate (name "wimshurst") (vers "1.0.0") (deps (list (crate-dep (name "topologic") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 2)))) (hash "0yrpnr7a11vilbggddmdqw82lazsr6zmy2kmza2i466n27l5srnd")))

