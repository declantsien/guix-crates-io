(define-module (crates-io wi lt) #:use-module (crates-io))

(define-public crate-wilton_rust-0.1 (crate (name "wilton_rust") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mfpkiwq5vb1f749az0vab2vjvc7q7qaj6kldycxzs1fp1g8wxqi")))

(define-public crate-wilton_rust-0.2 (crate (name "wilton_rust") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10pq5dr45zlvq1vkphkrwbaz1skz7sgz35a41w1kadiq54dg0vwv")))

(define-public crate-wilton_rusty-0.3 (crate (name "wilton_rusty") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0macmaz5w4mn58qs8aqjn4dg6l2w7bv3jk6dh4cbqh55ny9qvvkf")))

(define-public crate-wilton_rusty-0.3 (crate (name "wilton_rusty") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "16gslpav698b93rkqjawrz8lxcczsvmsxdb60y6h4x1kvy8lnr2i")))

