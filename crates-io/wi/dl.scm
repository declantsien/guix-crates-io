(define-module (crates-io wi dl) #:use-module (crates-io))

(define-public crate-widl-0.1 (crate (name "widl") (vers "0.1.0") (hash "090fxp0901zw2d5rh5cnbrqaq5qq0bv5vxfmdnvn1wb6nf8i8s45")))

(define-public crate-widl-codegen-0.1 (crate (name "widl-codegen") (vers "0.1.0") (hash "0r0p3b4i8aaihz1k8cn1dz9pznqjc2acqsw5a6bgkxv1y54mwl4z")))

