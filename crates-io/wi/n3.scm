(define-module (crates-io wi n3) #:use-module (crates-io))

(define-public crate-win32-0.1 (crate (name "win32") (vers "0.1.0") (hash "1rqbhmfrf6wg8rv6fi52aszhhy5rwh4kx328x2bbasxyqzpx2yxp") (yanked #t)))

(define-public crate-win32-ai-0.0.2 (crate (name "win32-ai") (vers "0.0.2") (hash "1smrv2799i521qgk1i0zlkvz740z694vk43iq94cb5drrj06yssq") (rust-version "1.46")))

(define-public crate-win32-ai-0.1 (crate (name "win32-ai") (vers "0.1.0") (hash "1qs0lp6r7ngq4az4114q6p1dxapv0abjnagg0hssa1mqcrnlbij6")))

(define-public crate-win32-ai-sys-0.0.1 (crate (name "win32-ai-sys") (vers "0.0.1") (hash "0zc71llsricj0d7vawgn7jgrkinxhiw552mgayp4lyiklfkmyx6l") (rust-version "1.46")))

(define-public crate-win32-ai-sys-0.0.2 (crate (name "win32-ai-sys") (vers "0.0.2") (hash "0xkq2jfc6i89hjh17d2kgcylic8srdb6pvgrf2lzw5fyjsni67ww") (rust-version "1.46")))

(define-public crate-win32-ai-sys-0.1 (crate (name "win32-ai-sys") (vers "0.1.0") (hash "0fhd7779qnxi49cm7w5hahcl2sx7v4pdr0kyyja3ckgn7mx2may2")))

(define-public crate-win32-data-0.0.2 (crate (name "win32-data") (vers "0.0.2") (hash "1dx9ywsysx2rfh7prz2cbcrwrbjjzxfw930qwjmsc8hz84bcc3hk") (rust-version "1.46")))

(define-public crate-win32-data-0.1 (crate (name "win32-data") (vers "0.1.0") (hash "0vzqqj9aq9x6mby2827j8cvn1hnylvf2qnh2syg08kka2ccmma3z")))

(define-public crate-win32-data-sys-0.0.2 (crate (name "win32-data-sys") (vers "0.0.2") (hash "1msv5c1nzjm2c3sv7chprbd1lczd1asz628i739yvyksi9sv1isx") (rust-version "1.46")))

(define-public crate-win32-data-sys-0.1 (crate (name "win32-data-sys") (vers "0.1.0") (hash "1jyn6xmjvkn9qigc5nv8nvv69n2l071zpzzyli3sz2hakziilc7f")))

(define-public crate-win32-devices-0.0.2 (crate (name "win32-devices") (vers "0.0.2") (hash "0hdjwmlphd5xwfg2k9jp4gz2z0ncj88qm8q5h5y72r9gv1wighb0") (rust-version "1.46")))

(define-public crate-win32-devices-0.1 (crate (name "win32-devices") (vers "0.1.0") (hash "0h500g12i1x7hq4vlwxvdrgvqlcc7sdd0zd23k2qyjzrw6gnxg58")))

(define-public crate-win32-devices-sys-0.0.2 (crate (name "win32-devices-sys") (vers "0.0.2") (hash "07m7q3rxpijpazh6jdjhv3cpzaaqqml711rv514l8p1gh1r090b2") (rust-version "1.46")))

(define-public crate-win32-devices-sys-0.1 (crate (name "win32-devices-sys") (vers "0.1.0") (hash "048166ach4ql6xf48bm7h3c8irnadz40gzr6knzxri49ardh5r65")))

(define-public crate-win32-error-0.9 (crate (name "win32-error") (vers "0.9.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "17izf3zx6279hm8vkr4mrslxh6n487fdbhdv42mn4i4nri7221wv")))

(define-public crate-win32-foundation-0.0.2 (crate (name "win32-foundation") (vers "0.0.2") (hash "1z5k14caa05hrz1zs08jsn8cia3j1kr1mp4vr0k8rfzb44nkbzg6") (rust-version "1.46")))

(define-public crate-win32-foundation-0.1 (crate (name "win32-foundation") (vers "0.1.0") (hash "1xgi95fsh5bbgkfxp7yidzxynmgha9748l20lcn6kpfsk2bfk0vx")))

(define-public crate-win32-foundation-sys-0.0.2 (crate (name "win32-foundation-sys") (vers "0.0.2") (hash "14mmipy78r7mlhrmxxy4i5dllaygsavrhwb8sy4ln84s1a5cadsp") (rust-version "1.46")))

(define-public crate-win32-foundation-sys-0.1 (crate (name "win32-foundation-sys") (vers "0.1.0") (hash "1fnzv0mm1mdci7awm80iqrp8sxmfr5vv021yiljzzf6nq7xprc1p")))

(define-public crate-win32-gaming-0.0.2 (crate (name "win32-gaming") (vers "0.0.2") (hash "05pk9ri7l9qrmdq9837464b6wflp07lmwcfiiqbyrv66yzbkijks") (rust-version "1.46")))

(define-public crate-win32-gaming-0.1 (crate (name "win32-gaming") (vers "0.1.0") (hash "1q10qdk96cnq02b1v2hsdqz8s0933k0nxh767vn3sl93lpq72j48")))

(define-public crate-win32-gaming-sys-0.0.2 (crate (name "win32-gaming-sys") (vers "0.0.2") (hash "0ky2xk3wy01jd4gxa27y827409nibcz710yhs5xqqn4sxmfv9w4v") (rust-version "1.46")))

(define-public crate-win32-gaming-sys-0.1 (crate (name "win32-gaming-sys") (vers "0.1.0") (hash "0dvzj5b72lj4lcbkki1iz02gaz10ily4ndqk55gvw4mqmjgacvzs")))

(define-public crate-win32-globalization-0.0.2 (crate (name "win32-globalization") (vers "0.0.2") (hash "0vwn4qhd3yz8z5a6mv8psbpy4px6s8wj2ll6kmb2anbfki9shg3h") (rust-version "1.46")))

(define-public crate-win32-globalization-0.1 (crate (name "win32-globalization") (vers "0.1.0") (hash "1dk5rfgfk1pwgfh1w7l2r766innjnpn91wkmfgbgj3jnqc3kyqi8")))

(define-public crate-win32-globalization-sys-0.0.2 (crate (name "win32-globalization-sys") (vers "0.0.2") (hash "0qqvjvhgpx1j735hmnk4vxbfcjjchkckpzalkxk0qiw2jg7a7gfv") (rust-version "1.46")))

(define-public crate-win32-globalization-sys-0.1 (crate (name "win32-globalization-sys") (vers "0.1.0") (hash "0vsml69kfx4dks21js2i22q8rfr861y4k8mjgq44fwns0pv6r3q8")))

(define-public crate-win32-graphics-0.0.2 (crate (name "win32-graphics") (vers "0.0.2") (hash "14bv0ncs23bzy872j5665gipzrs14r2kdbxvm7rbs09chv2ppa5y") (rust-version "1.46")))

(define-public crate-win32-graphics-0.1 (crate (name "win32-graphics") (vers "0.1.0") (hash "1bicrnq4n0nb6mnp2hgmdg8552qbg3kr2nq9618968pqm5lg8bja")))

(define-public crate-win32-graphics-sys-0.0.2 (crate (name "win32-graphics-sys") (vers "0.0.2") (hash "1316z95c2x54z9qq5ldn85rg0ww63yhf47szakf68pzirqj2y52g") (rust-version "1.46")))

(define-public crate-win32-graphics-sys-0.1 (crate (name "win32-graphics-sys") (vers "0.1.0") (hash "167zijnja8yra1bx6sd97wlqvclwll7hribg1hw7hbp7ysfx8gpc")))

(define-public crate-win32-management-0.0.2 (crate (name "win32-management") (vers "0.0.2") (hash "0172ac6hm81y6l0v5jv03ksw8l4r9nv76fzcxvrk97ai5ggcdclm") (rust-version "1.46")))

(define-public crate-win32-management-0.1 (crate (name "win32-management") (vers "0.1.0") (hash "0qq9d6ddpqrwj59dhrflwr56g13pm6i9vq0rsc4n9rvzk7f003dk")))

(define-public crate-win32-management-sys-0.0.2 (crate (name "win32-management-sys") (vers "0.0.2") (hash "029i0bwzdvj1si37bq46a05g8q7ylxd0bl2px4pii0m1sssqfp64") (rust-version "1.46")))

(define-public crate-win32-management-sys-0.1 (crate (name "win32-management-sys") (vers "0.1.0") (hash "10yngf88x9g5v7nw74vlh7lcq229dv7ckv0hjip5ss8k9zyk51c0")))

(define-public crate-win32-media-0.0.2 (crate (name "win32-media") (vers "0.0.2") (hash "1aqlamxn6mmykmi3vg4sxm17w9z13m0mx07jqmkivkh6wv378cdm") (rust-version "1.46")))

(define-public crate-win32-media-0.1 (crate (name "win32-media") (vers "0.1.0") (hash "0pjgxz48klfxza94g1gs36369i1ls2imxyym5vybk15zwvlh66vr")))

(define-public crate-win32-media-sys-0.0.2 (crate (name "win32-media-sys") (vers "0.0.2") (hash "17x4i5i6syh2w0p2zw05yr7qama87w04955fwxf5gp5l9gnwp30h") (rust-version "1.46")))

(define-public crate-win32-media-sys-0.1 (crate (name "win32-media-sys") (vers "0.1.0") (hash "04vp965l0bacc8bl30g15y4m24sfq8l9yphic8zg6b75cw6wazf6")))

(define-public crate-win32-networking-0.0.2 (crate (name "win32-networking") (vers "0.0.2") (hash "0h7sr8fg4lpl6gf1wsazv3ww20l3ap650a7chvgd22fz5lj6n6ir") (rust-version "1.46")))

(define-public crate-win32-networking-0.1 (crate (name "win32-networking") (vers "0.1.0") (hash "0g81gfy2sx4p7yf2v8jppxspx0s7rnbhlypp4p3wxh32lwy3dddy")))

(define-public crate-win32-networking-sys-0.0.2 (crate (name "win32-networking-sys") (vers "0.0.2") (hash "1mxf1pmhr1y259h8kkm7h32rlin986520lpjx46vapr1jyxwr2n9") (rust-version "1.46")))

(define-public crate-win32-networking-sys-0.1 (crate (name "win32-networking-sys") (vers "0.1.0") (hash "008cgnnnwdrziqjdpgb3lzwjrxv7ydqgx1w4i2zncbwnk1dp50hz")))

(define-public crate-win32-networkmanagement-0.0.2 (crate (name "win32-networkmanagement") (vers "0.0.2") (hash "0nyhjxy958zzwvrd6pwicvppxwsqwsgxsrh51qkl5cwc4fpzy46q") (rust-version "1.46")))

(define-public crate-win32-networkmanagement-0.1 (crate (name "win32-networkmanagement") (vers "0.1.0") (hash "10ynycyhhdarpv99mmawbnhrs5rk1aklsxim1089fjcy8ygdk78g")))

(define-public crate-win32-networkmanagement-sys-0.0.2 (crate (name "win32-networkmanagement-sys") (vers "0.0.2") (hash "1m6hn4rr1ph9yw6k9swvmz8rzrgsrfg89vmmlird1m4hl94516pc") (rust-version "1.46")))

(define-public crate-win32-networkmanagement-sys-0.1 (crate (name "win32-networkmanagement-sys") (vers "0.1.0") (hash "09x0nm405h03r8bmv2vxlvn9m4zw4ayir8rdng3q5zibm6dvvmqa")))

(define-public crate-win32-remove-dir-all-0.1 (crate (name "win32-remove-dir-all") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (target "cfg(windows)") (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("shellapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "05ah7jxjsp3pkbij3b978m42gb9cpas5in6g2rc8mrj9hmwmc647") (features (quote (("symlink_tests") ("property_system_api" "winapi/libloaderapi" "winapi/objbase" "winapi/shobjidl_core" "winapi/winerror") ("default" "property_system_api"))))))

(define-public crate-win32-security-0.0.2 (crate (name "win32-security") (vers "0.0.2") (hash "0vk7mv4vqbrfhs3swiaysyri67hcq19yg69fcfxq5gns591n3abs") (rust-version "1.46")))

(define-public crate-win32-security-0.1 (crate (name "win32-security") (vers "0.1.0") (hash "0wka2r8sf87cx3pbb7sypb2wjkbdx923fc0w3bif8qxhlw2i684g")))

(define-public crate-win32-security-sys-0.0.2 (crate (name "win32-security-sys") (vers "0.0.2") (hash "0w5kria4xgs7gnx31gnr5g8jfaiyd57fa4pf4xj6n68804iahyfh") (rust-version "1.46")))

(define-public crate-win32-security-sys-0.1 (crate (name "win32-security-sys") (vers "0.1.0") (hash "0djwgqkrrcb8qv4hygadsccw6ixk6ifhbd6g7rqcdvrg52g7dm7k")))

(define-public crate-win32-storage-0.0.2 (crate (name "win32-storage") (vers "0.0.2") (hash "0sjvqajsvazsg2kq3sa0afzifh0d5yhqzrxg7g6zbnwwkmivfvkf") (rust-version "1.46")))

(define-public crate-win32-storage-0.1 (crate (name "win32-storage") (vers "0.1.0") (hash "10q4j1zp5mrlqi2svsdaapci7gskvgn7xr89q8xylfzbjkc4qciv")))

(define-public crate-win32-storage-sys-0.0.2 (crate (name "win32-storage-sys") (vers "0.0.2") (hash "01vr8zsrmm6w82mgh6qrxp5gr3zk41nafixlh0mbf26bj5s2i1pc") (rust-version "1.46")))

(define-public crate-win32-storage-sys-0.1 (crate (name "win32-storage-sys") (vers "0.1.0") (hash "1m1q7ck0rzihdfsvign6yjp0sbishhs3gwa2ni05w23hyksy11sb")))

(define-public crate-win32-synthetic-pointer-0.1 (crate (name "win32-synthetic-pointer") (vers "0.1.0") (deps (list (crate-dep (name "enumflags2") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_UI_Controls" "Win32_UI_Input_Pointer" "Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "168zn1w6rrymh65hqj6hg0gwhbjm0kzlpbwipaqk4jm008csbj4r") (yanked #t)))

(define-public crate-win32-system-0.0.2 (crate (name "win32-system") (vers "0.0.2") (hash "1qxi9irmzgznbcmp8xcbfnb06ay8sn2as1jn2yqg84xsz4w6ig71") (rust-version "1.46")))

(define-public crate-win32-system-0.1 (crate (name "win32-system") (vers "0.1.0") (hash "0afhvbd0aka01259mfc9968cwr5y2cvl8ysasyjbbk85078sd2mq")))

(define-public crate-win32-system-sys-0.0.2 (crate (name "win32-system-sys") (vers "0.0.2") (hash "0lnzqlv94j56pn3rc17q3nmc92z84xwl89fbvss3m81dbv60f3yw") (rust-version "1.46")))

(define-public crate-win32-system-sys-0.1 (crate (name "win32-system-sys") (vers "0.1.0") (hash "06yqdimas3f2gjxnf77817yalv470pmsa5hrixj2k63685gfvgvm")))

(define-public crate-win32-ui-0.0.2 (crate (name "win32-ui") (vers "0.0.2") (hash "030j7znhjwhfgqz34ipia190kfdcz5wrwqrvvablvcx1c4bv7l0m") (rust-version "1.46")))

(define-public crate-win32-ui-0.1 (crate (name "win32-ui") (vers "0.1.0") (hash "0frnfab6a53clwiiqr7wcw0cdhhfgq5s0r5mm26nbc4r0h8jd573")))

(define-public crate-win32-ui-sys-0.0.2 (crate (name "win32-ui-sys") (vers "0.0.2") (hash "1ss106csrsm3k68rdk6qn46anrrypnbn1j52rm4i85mnpp6ikamb") (rust-version "1.46")))

(define-public crate-win32-ui-sys-0.1 (crate (name "win32-ui-sys") (vers "0.1.0") (hash "0fqmlnca055lp2lpk049w7r8rzzdfxjxam1xrbcyp4mfh4hlahys")))

(define-public crate-win32-web-0.0.2 (crate (name "win32-web") (vers "0.0.2") (hash "182y6anamhjkq8534frja8ly872dx68lgq1i0j2v5fpng7m06pic") (rust-version "1.46")))

(define-public crate-win32-web-0.1 (crate (name "win32-web") (vers "0.1.0") (hash "0310hh4ii7r64x6z8g17sqrvkw1hpfmzhpm5542s5z6a1fqjgwk3")))

(define-public crate-win32-web-sys-0.0.2 (crate (name "win32-web-sys") (vers "0.0.2") (hash "183xp60kpyikk8vwc400lwnvbpmfwl7dgwbggcisaqjqcp4dvwm1") (rust-version "1.46")))

(define-public crate-win32-web-sys-0.1 (crate (name "win32-web-sys") (vers "0.1.0") (hash "0wwndc2f3q1zph07snxwwmmnrsf6ksvvs10qb761mqwj0rval6pl")))

(define-public crate-win32-wlan-0.1 (crate (name "win32-wlan") (vers "0.1.0") (deps (list (crate-dep (name "get-last-error") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Networking_Connectivity" "Devices" "Devices_WiFi" "Foundation" "Foundation_Collections" "Win32_Foundation" "Win32_Networking_WinHttp" "Win32_NetworkManagement_WiFi"))) (default-features #t) (kind 0)))) (hash "117n5zmffja08qlsqjr13vmi2xwql90ij4pgwp7g0jzjnp8ji2sr") (features (quote (("default"))))))

(define-public crate-win32_filetime_utils-0.2 (crate (name "win32_filetime_utils") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0690bgycg77wj6x7nqcddzcf5zljkwskhiav2kbshivhlvqq0h7x")))

(define-public crate-win32_filetime_utils-0.2 (crate (name "win32_filetime_utils") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "win32_filetime_utils") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "168nsn1i1wscnnl5mirq27d4sh9mx1if8va5z97hkri9jm14x3yq")))

(define-public crate-win32_notification-0.1 (crate (name "win32_notification") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("shellapi" "combaseapi" "errhandlingapi" "impl-default" "winerror" "impl-debug"))) (default-features #t) (kind 0)))) (hash "1vki49klspwrbdri3kvin8p32lwnzn42fmv50dip9lkpmm8xnk1n")))

(define-public crate-win32api-rs-sys-0.1 (crate (name "win32api-rs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1g6ljpxnw909ygqc6z04n6iff39hgki3nihifchx3pd5js9922b8")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "1j15p17a7apd8c2zzxvkzyah47y7dq5xan4gdif5mlr596clpd21")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "1wllfhy30gpi0s8046qnz078fkbbfbabdq907mbh8jw8fjggspxg")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "0i2w710sxrwl7anz08zrd4dvr21w88yzng4pd3y3ljh75kjdbpim")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.2-pre1") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "0a0jpc9msa99fkk3smn5q5gzl1c79mixd1y3m4v9914biz7sjy33")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.2-pre1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "091d9q6di5hp6lqj9h1gfaviicsbw9sv3vjh186ss6f0n5qlfxpg")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.2-a1") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "0sfv3nd3ad06n8xgx4as3hinx9r4cndbjh58sbywf4cg9qjl5gqs")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.3-a1") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "1i530xlxwk7dlv2hvq6vlms29266vfyhcl3iyk8xb86s8gichc7k")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.3-a2") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "194bn9dxgnhvcb3dlgkpxkiqm5hiyfq7ajvvm1bxdxr63ng04wb8")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "09jx8gfa3vrharb79i72jyb4c9xmli23vnz4l7pmsnixxv9h66s7")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.4") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "0fj66vap60c1wzkirq4sm4dj520sy6vmxsgvwblmjz379f9gfr3w")))

(define-public crate-win32console-0.1 (crate (name "win32console") (vers "0.1.5") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winnt" "handleapi" "processenv" "winbase" "fileapi" "wincon" "minwindef" "wingdi" "wincontypes" "consoleapi" "windef" "winuser" "utilapiset"))) (default-features #t) (kind 0)))) (hash "0wzn0rngnkigdgkgi8haw8xwcc6szrklcz2sky5kw3cr07gvnscy")))

(define-public crate-win32job-1 (crate (name "win32job") (vers "1.0.0") (deps (list (crate-dep (name "rusty-fork") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (default-features #t) (kind 0)))) (hash "1x4kardq4l4g4y0kwgvapjjhydgp5swcipkqh73rppis55si74if")))

(define-public crate-win32job-1 (crate (name "win32job") (vers "1.0.1") (deps (list (crate-dep (name "rusty-fork") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (default-features #t) (kind 0)))) (hash "0da2sqxndfranjhv86akk3kr7jrjmds5fd66d8afk2vcdw9i8alv")))

(define-public crate-win32job-1 (crate (name "win32job") (vers "1.0.2") (deps (list (crate-dep (name "rusty-fork") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (default-features #t) (kind 0)))) (hash "0hkx14gdv02vmsa58l71w8hmwf4z3g3qfi0wcg5sl0bkkkp6v6cj")))

(define-public crate-win32job-1 (crate (name "win32job") (vers "1.0.3") (deps (list (crate-dep (name "rusty-fork") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.52") (features (quote ("Win32_Foundation" "Win32_Security" "Win32_System_JobObjects" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (default-features #t) (kind 0)))) (hash "172gg0inlxc4k71zlhmxg4408i5wzm7i8nsw2bi88vr2gq8dspbi") (yanked #t)))

(define-public crate-win32job-1 (crate (name "win32job") (vers "1.0.4") (deps (list (crate-dep (name "rusty-fork") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("handleapi" "winbase" "psapi" "processthreadsapi" "jobapi2" "basetsd"))) (default-features #t) (kind 0)))) (hash "1pam2l6rjxgzh30fzrcv0ypcicb3w5cqscbssbx37fcg8vd1bsdi")))

(define-public crate-win32job-2 (crate (name "win32job") (vers "2.0.0") (deps (list (crate-dep (name "rusty-fork") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.52") (features (quote ("Win32_Foundation" "Win32_Security" "Win32_System_JobObjects" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (default-features #t) (kind 0)))) (hash "0g1blsb7ixrqjicykx82rvrymcydlsdgfwzb61x88iyrazsinasv")))

(define-public crate-win32yoink-0.1 (crate (name "win32yoink") (vers "0.1.0") (deps (list (crate-dep (name "clipboard-win") (req "^5.3.0") (default-features #t) (kind 0)))) (hash "1aw8sa3z0vc5hdklfx5zy3zn6v1nddqn3kwm4698lk3yyzhnjq9v")))

