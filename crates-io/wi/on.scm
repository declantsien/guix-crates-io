(define-module (crates-io wi on) #:use-module (crates-io))

(define-public crate-wion-0.0.0 (crate (name "wion") (vers "0.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (optional #t) (default-features #t) (kind 0)))) (hash "1g6ncgxl1j17c4kxhi3l9j1x7hgq3g5dhji19driv4dafl4j44rn") (features (quote (("default"))))))

(define-public crate-wion-0.1 (crate (name "wion") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^2.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (optional #t) (default-features #t) (kind 0)))) (hash "0p3244gzvxbr4pnnspn3zqrys4zbp39qw3frcdm6837b957d2l5r") (features (quote (("default"))))))

