(define-module (crates-io wi st) #:use-module (crates-io))

(define-public crate-wisteria-0.1 (crate (name "wisteria") (vers "0.1.0") (hash "0z3vfr3pm3cdmmrsmvnhqcijfmd628q1rc50bz563l1n4cmmmlck") (yanked #t)))

(define-public crate-wistime-0.1 (crate (name "wistime") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1zlb2aly8zmff7zx3zr3nxir2xiszmy7yz7wziq0ly18d23d84sk")))

