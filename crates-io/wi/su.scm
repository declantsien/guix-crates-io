(define-module (crates-io wi su) #:use-module (crates-io))

(define-public crate-wisual-logger-0.1 (crate (name "wisual-logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "13m4bvhny70nfmnf2xkx89q6kpmvwddr2pq03r53dcmiy95rzp89")))

(define-public crate-wisual-logger-0.1 (crate (name "wisual-logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0f6f85h2ykfwa49d5xi1kj77r4dhipvq9k0y416r1v0qhwgj4hwj")))

(define-public crate-wisual-logger-0.1 (crate (name "wisual-logger") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "01qq6lgr99fxc0l30bvk335i1skp8gk9ggm7432qwxxxn68y83ny")))

(define-public crate-wisual-logger-0.1 (crate (name "wisual-logger") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0j6vj0kgxdzy1ivc4qd4gbhpk3md2vklk947in6ksg5z63hnj87v")))

(define-public crate-wisual-logger-0.1 (crate (name "wisual-logger") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "02q1d7xdbasr26mahfc9i2094vzy8xdbylqwcr4y0q6zizjwdl79")))

