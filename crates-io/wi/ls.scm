(define-module (crates-io wi ls) #:use-module (crates-io))

(define-public crate-wilson-0.1 (crate (name "wilson") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1naq4waz60x2yww50l90a1ihlfcrjhcfxl4wlyhq8l8n8v5g8azk") (features (quote (("f64") ("f32") ("default" "f64"))))))

(define-public crate-wilson-0.1 (crate (name "wilson") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1kzyalm5c0fag6k8rsqpwa7al1d629yjv6g9kgpih8rzp7kmgd9h") (features (quote (("f64") ("f32") ("default" "f64"))))))

