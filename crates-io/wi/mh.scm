(define-module (crates-io wi mh) #:use-module (crates-io))

(define-public crate-wimhrst-0.1 (crate (name "wimhrst") (vers "0.1.0") (deps (list (crate-dep (name "topologic") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0yy3vj44s677ag2yp9dkf8b2qdhclp4q9xwsn451aabbda1jg7pn")))

(define-public crate-wimhrst-0.1 (crate (name "wimhrst") (vers "0.1.1") (deps (list (crate-dep (name "topologic") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 2)))) (hash "0nmwf1xzgxdbq684bp6pkq3si99ggyizb997hj9y5zzdck1sl60a") (yanked #t)))

(define-public crate-wimhrst-0.1 (crate (name "wimhrst") (vers "0.1.2") (deps (list (crate-dep (name "topologic") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 2)))) (hash "1j2rhkzc45bd3fkg0qrwd0hi6520x0mvys96dzaaby41xz3g5sy6")))

(define-public crate-wimhrst-1 (crate (name "wimhrst") (vers "1.0.0") (deps (list (crate-dep (name "topologic") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 2)))) (hash "1v4yf4nhxaljfww529849zmy9ar25qm8002jkq6mpph3ddcnksh2")))

