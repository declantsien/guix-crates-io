(define-module (crates-io wi zd) #:use-module (crates-io))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "vek") (req "^0.15.8") (kind 0)))) (hash "1aab1yib9wl93x82xdrrr69246pfdzjq8634hlm02fgb47ch45yw")))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (kind 0)))) (hash "067dl9j1gzc73j5cvf9cgzlqvaqv4ivvmgrcvc8iy3zdp4zwwpcv")))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (kind 0)))) (hash "0kpmbx5qq2jvyszz3lg7g0sw5z5pbwl3354rff2f3f9pk0g2i3fc")))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (kind 0)))) (hash "1ar5a1q7fd08ia7yifywj0i88g53byibas7ljqwvjbpyz28b8nvy")))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (kind 0)))) (hash "0zfryr0r6xcnxbzzf266gzw1lxccws2bvb36i11snxayshrn5w2f")))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (kind 0)))) (hash "0yibjzhdvz937bili8maxisfx4acb79sr9qazd5ibr5wnp9b50ss")))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.1.5") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (kind 0)))) (hash "0cd007z91nk6vjc9qbm6s0bffkkv0njq0bhs9br752zki09hdbrd")))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.2.0") (deps (list (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (features (quote ("libm"))) (kind 0)))) (hash "11h31pvlca5xyvy82q1khym9hrn5fcr99nl0pfsq3jd3avs95ip6") (features (quote (("simd") ("f64") ("default"))))))

(define-public crate-wizdraw-1 (crate (name "wizdraw") (vers "1.2.1") (deps (list (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15.8") (features (quote ("libm"))) (kind 0)))) (hash "07mh5903y5l5yrlxw7vgwrm7gkznc6zwafshq2b3bffwwnd7gm02") (features (quote (("simd") ("f64") ("default"))))))

(define-public crate-wizdraw-2 (crate (name "wizdraw") (vers "2.0.0") (deps (list (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.16") (features (quote ("libm"))) (kind 0)))) (hash "1pj1wlr2bkygf0mdw24i4674971a0j3q4vfj4q446whg3d0xj16j") (features (quote (("stroke") ("simd") ("default" "stroke"))))))

