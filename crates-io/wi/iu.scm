(define-module (crates-io wi iu) #:use-module (crates-io))

(define-public crate-wiiu_swizzle-0.1 (crate (name "wiiu_swizzle") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0m3p0g9s9cynnvkl19x3w64za1709h761lipn56fblr0j85kvhc0")))

(define-public crate-wiiu_swizzle-0.2 (crate (name "wiiu_swizzle") (vers "0.2.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "085jdj7mbk2y0c7dh1fn1jgpplgd9q3bpq9m6rjm46z1n184av7m")))

(define-public crate-wiiunhide-0.1 (crate (name "wiiunhide") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "0pkcirkx2sb0mpfg3dwqjfpxicybmbsd9qmsmp9jpa4jyykfzr2z")))

(define-public crate-wiiuse-sys-0.1 (crate (name "wiiuse-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.33.1") (default-features #t) (kind 1)))) (hash "02b4f68gfxvpm46viwiybw4062hhk3qdd5c65q79pgh5adinfr1q")))

(define-public crate-wiiuse-sys-0.2 (crate (name "wiiuse-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.33.1") (default-features #t) (kind 1)))) (hash "0wdssnl4kc1c240qkph85gzi27lma8l66028b6p944dmdf4i2k5q")))

(define-public crate-wiiuse-sys-0.3 (crate (name "wiiuse-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.33.1") (default-features #t) (kind 1)))) (hash "1xa6s56x945j9zncj6gyj5z4mw91yg0aagihfmlfbqjl00l69880")))

