(define-module (crates-io wi tr) #:use-module (crates-io))

(define-public crate-witransfer-0.1 (crate (name "witransfer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13v7f1jp9778jqyw0wcjmgzsr18daw1b07s5ykmwxnqh628wlcka") (yanked #t)))

(define-public crate-witransfer-0.1 (crate (name "witransfer") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jipnca04h27gn5zyi94jkbzjg6ld3n10rb9cf7rdjzqmwnlsivd") (yanked #t)))

