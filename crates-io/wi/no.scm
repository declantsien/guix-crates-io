(define-module (crates-io wi no) #:use-module (crates-io))

(define-public crate-wino-0.0.1 (crate (name "wino") (vers "0.0.1") (hash "13n01wspws900m6bjvnxy4w5rp2l45rnaarip0w9qa7acrrj30yn") (rust-version "1.75")))

(define-public crate-wino-core-0.0.1 (crate (name "wino-core") (vers "0.0.1") (hash "06h2sq0pisxvc331shjdkwv7vwh10q4xwy2kwpclpbxa9m6fbv24") (rust-version "1.75")))

(define-public crate-winos-0.1 (crate (name "winos") (vers "0.1.0") (hash "1l66ja2jgaf0kdv71ip70bq50phsmrp7f9yr60a9wppjvk7kwgqb")))

(define-public crate-winos-0.1 (crate (name "winos") (vers "0.1.1") (hash "0rvc28xbsvhbsipmzv4qxggrhlh5pkcniks7h0knwh9k6hfgdng5")))

(define-public crate-winos-0.1 (crate (name "winos") (vers "0.1.2") (hash "031i16k1j6cvipwjd69ssm4sq6319mjj884a785sv4a1m2gq0pl1")))

