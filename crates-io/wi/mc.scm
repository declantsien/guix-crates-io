(define-module (crates-io wi mc) #:use-module (crates-io))

(define-public crate-wimc-0.0.0 (crate (name "wimc") (vers "0.0.0") (deps (list (crate-dep (name "aul") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "wbsl") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "whdp") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1s64nrkwmvi16w5jjz5v6z2dsw8ii4f86k9m22c8q0pfwc4a3msa")))

(define-public crate-wimc-0.0.1 (crate (name "wimc") (vers "0.0.1") (deps (list (crate-dep (name "aul") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "wbdl") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wbsl") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "whdp") (req "^1.1.11") (default-features #t) (kind 0)) (crate-dep (name "wimcm") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1gpcr2rsnrfsfslx8raabldl467l52q65s81zavy5vxj57jvrm2q")))

(define-public crate-wimc-0.1 (crate (name "wimc") (vers "0.1.0") (deps (list (crate-dep (name "aul") (req "^1.3.2") (features (quote ("no-color"))) (default-features #t) (kind 0)) (crate-dep (name "wbdl") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "wbsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "whdp") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "wimcm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "133gramidgn2g6f6xny6c8n41mm4n5p6dacdisc03a2yxmlnlpjc")))

(define-public crate-wimcm-0.0.0 (crate (name "wimcm") (vers "0.0.0") (deps (list (crate-dep (name "wjp") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "08zvzbj9z9svd7a5lhwn63mzn48wswgk5d7hd9k3qwi006xjcwwk")))

(define-public crate-wimcm-0.1 (crate (name "wimcm") (vers "0.1.0") (deps (list (crate-dep (name "wbdl") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0mb0czqjyf4887bgknybhid8nr16zhi40a5g6w6saqszcaykd0gn")))

(define-public crate-wimcm-0.0.1 (crate (name "wimcm") (vers "0.0.1") (deps (list (crate-dep (name "wbdl") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1fill4siqypbhzhnb7xzvv8mgwd4xxcbvysvkl477724q4v78k7i")))

(define-public crate-wimcm-0.1 (crate (name "wimcm") (vers "0.1.2") (deps (list (crate-dep (name "wbdl") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0dghnrrrlfs4ak5xyywqk2c972h6y2lz8mi38dcxnlhivzj8bipd")))

(define-public crate-wimcm-0.2 (crate (name "wimcm") (vers "0.2.0") (deps (list (crate-dep (name "wbdl") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1b3r03k4s9l2nw9sxp81fx32jbmyphhzq0n5nkqyccxz5l51b7ih")))

(define-public crate-wimcm-0.1 (crate (name "wimcm") (vers "0.1.3") (deps (list (crate-dep (name "wbdl") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0d3cx18xy3dzvvc0j8rjl4ijlvrrvvqkyrv28cw5rz1mx9ghksnd")))

(define-public crate-wimcm-0.2 (crate (name "wimcm") (vers "0.2.1") (deps (list (crate-dep (name "wbdl") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "03wcf2igbqnwmy6r74z7x32izx68c4j277shv6p7kgknarbqh2ja")))

(define-public crate-wimcm-0.2 (crate (name "wimcm") (vers "0.2.2") (deps (list (crate-dep (name "wbdl") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1fr7nys5j2hvz4hv5npzvpgkg16ygsigpabdzrpjvpf496nqfcyk")))

(define-public crate-wimcm-0.2 (crate (name "wimcm") (vers "0.2.3") (deps (list (crate-dep (name "wbdl") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "156l8dihw351awycnaziwmdyappwf0pgda9cxm3a9zayg9yyixrg")))

(define-public crate-wimcm-0.2 (crate (name "wimcm") (vers "0.2.4") (deps (list (crate-dep (name "wbdl") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1qb3xf8bzviqdxdi2mnp1nzx942a74bhqz1vz9d1hd6scbrrg4kw")))

(define-public crate-wimcm-0.2 (crate (name "wimcm") (vers "0.2.5") (deps (list (crate-dep (name "wbdl") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "0qmryvvg5r7pkrnbm6khlakkiiskymjjj10cy627xwclkb0adx24")))

(define-public crate-wimcm-0.2 (crate (name "wimcm") (vers "0.2.6") (deps (list (crate-dep (name "wbdl") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "1s6hv8q7kjrs9ssnmdzpwp851m0f8xy2rnkkw6i3vli2hrv0jqv1")))

(define-public crate-wimcm-0.3 (crate (name "wimcm") (vers "0.3.0") (deps (list (crate-dep (name "wbdl") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "0pklbsi1ydwsfvpq65p2nlnsrmlj262plix4f78nrqildi0l189l")))

(define-public crate-wimcp-0.0.0 (crate (name "wimcp") (vers "0.0.0") (deps (list (crate-dep (name "aul") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "wbsl") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "whdp") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0l0ykkx8a04s63ksai3bwm5ffq6xdcq3rxx7vwzab0lnsv3awym0")))

(define-public crate-wimcp-0.0.1 (crate (name "wimcp") (vers "0.0.1") (deps (list (crate-dep (name "wbdl") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "wimcm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "0bfn61iar55z4da71jdhppb7qvci3hz3irrz374kcx87mjiqygnd")))

(define-public crate-wimcp-0.1 (crate (name "wimcp") (vers "0.1.0") (deps (list (crate-dep (name "wbdl") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "wimcm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "wjp") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "0x3q5alq2bg3kivxn27sandxwllmffyawfylfhzk9nxw2ixq7hj6")))

