(define-module (crates-io wi ml) #:use-module (crates-io))

(define-public crate-wimlib-sys-0.1 (crate (name "wimlib-sys") (vers "0.1.13") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "1avarb4zyk2g3hwqcmjykbn5f9icksdv5pvx7zqmxb378rlma2rj") (links "wim")))

(define-public crate-wimlib-sys-0.1 (crate (name "wimlib-sys") (vers "0.1.13-1") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "08q68gcp3dsxxgf1m065hhlicldq5nj7g3945rps15xlw6mx5sbh") (yanked #t) (links "wim")))

(define-public crate-wimlib-sys-1 (crate (name "wimlib-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "09f11l6q2vzfnbnqfmcl9gid5kasayqqxmz2psmxhhcrkbh3hicz") (links "wim")))

(define-public crate-wimlib-z-1 (crate (name "wimlib-z") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ggn60acgi6yv07rjnvxw2pknr6x34nhwdjd70pmp2makm9a1sbj") (yanked #t) (links "wim")))

