(define-module (crates-io wi it) #:use-module (crates-io))

(define-public crate-wiitrig-0.0.1 (crate (name "wiitrig") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gn3h3gkj9kvhwnm7r037ysyhbzhrgg6bcwy408k3bbws09bp4zz")))

