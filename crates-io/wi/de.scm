(define-module (crates-io wi de) #:use-module (crates-io))

(define-public crate-wide-0.0.0 (crate (name "wide") (vers "0.0.0") (hash "1w97n940ipfxd0scb9w3swdijdi95zklcx035zyg13xcrhjn06cz")))

(define-public crate-wide-0.1 (crate (name "wide") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "lokacore") (req "^0.3") (default-features #t) (kind 0)))) (hash "1gm3nmwfd7fpk4v5cxlvgq9z078ijaycjsz7i9as2jvfknp3d4bw") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.1 (crate (name "wide") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "lokacore") (req "^0.3") (default-features #t) (kind 0)))) (hash "14b827wy58g54g1qqc40kspfqi7faimnhg59fckdhkggxbflh808") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.1 (crate (name "wide") (vers "0.1.2") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "lokacore") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xdgw9jd409rnrzqdnb2v67qm5ny3lxy981l2vmapx0g3q298jc6") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2 (crate (name "wide") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "lokacore") (req "^0.3") (default-features #t) (kind 0)))) (hash "08s57bccr5h40sm533hpiwp3plfgsjbwyq6yf64ayj74lz6d56s4") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2 (crate (name "wide") (vers "0.2.1") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "lokacore") (req "^0.3") (default-features #t) (kind 0)))) (hash "1yhwkpiczx4g2c2gmcja3x0scrd0883j0dv2fczjajxz9lhrbpqq") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2 (crate (name "wide") (vers "0.2.2") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1lmy951gfb0xx804j43l30gyzlqw0d0g2pzi2i36p2m5kzd1rqql") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2 (crate (name "wide") (vers "0.2.3") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1f7m9pqfv780ygd3iw0lli9vkpixpcg7v1kbj3l3mybrxyk7a0c5") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2 (crate (name "wide") (vers "0.2.4") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0szvadwcf2crvnf86s036czhzwl9ifnwgbk4d6fkc77fkqr7fnck") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2 (crate (name "wide") (vers "0.2.5") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1w1f4kxm2dhlwb0m36c7y4dk4imdqs0jpyxi1437bgbykipqrv0r") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.2 (crate (name "wide") (vers "0.2.6") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1vrnad41grx4lf2jl0bzpc5pjd90yr3aysww8m5jkyxiqc7wf9n4") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.3 (crate (name "wide") (vers "0.3.0") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "02mgdhni5cbgnzjx8iv7mwvn5rb9yzxz1v2f57vg0hyp84k4a1xy") (features (quote (("default") ("always_use_stable")))) (yanked #t)))

(define-public crate-wide-0.3 (crate (name "wide") (vers "0.3.1") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0hjmy7mggvpyl451dsmdfb3gh9rafmwljy9dgcb4aq8s5p3p4snz") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.3 (crate (name "wide") (vers "0.3.2") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "0qc96phigpkix50r0y5jkc4qx4n7m90dk01a6hmx6kbqa0nbvkd3") (features (quote (("default") ("always_use_stable"))))))

(define-public crate-wide-0.4 (crate (name "wide") (vers "0.4.0") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "1pqw4gcrcmc8anqzzyx964rxjrq3sdr50p8nbclffprnid4kb5sc") (features (quote (("extern_crate_std") ("default") ("always_use_stable")))) (yanked #t)))

(define-public crate-wide-0.4 (crate (name "wide") (vers "0.4.1") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "04vxgwj0kx0xjfcv2mb29gxmhd12gxbl69451pwp3nhq9s73waby") (features (quote (("extern_crate_std") ("default") ("always_use_stable")))) (yanked #t)))

(define-public crate-wide-0.4 (crate (name "wide") (vers "0.4.2") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "0119wjllqbh5chfq46ddv95liqrrczxc6wy3dj5vvif49cyz18xr") (features (quote (("extern_crate_std") ("default") ("always_use_stable")))) (yanked #t)))

(define-public crate-wide-0.4 (crate (name "wide") (vers "0.4.3") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "1s289cpc60q42d510lj3cvcfazf9c11llqw79mni2z9vjwb473sj") (features (quote (("extern_crate_std") ("default") ("always_use_stable")))) (yanked #t)))

(define-public crate-wide-0.4 (crate (name "wide") (vers "0.4.4") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "1wcj8197sznaam4m8985ralhv76irrcrg5r1rqaj1b2l9qfx8xr5") (features (quote (("extern_crate_std") ("default") ("always_use_stable")))) (yanked #t)))

(define-public crate-wide-0.4 (crate (name "wide") (vers "0.4.5") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "19f2ajp4aa2x72jscl78j5kin35yadk943jzz6zg76b30dl40d59") (features (quote (("extern_crate_std") ("default") ("always_use_stable"))))))

(define-public crate-wide-0.4 (crate (name "wide") (vers "0.4.6") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "0ad75vnzygj8qfcl1l9n4wi93xmqzvhqlpqn4hfayrwbn6wa69aq") (features (quote (("extern_crate_std") ("default") ("always_use_stable"))))))

(define-public crate-wide-0.5 (crate (name "wide") (vers "0.5.0") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "17z3b6qapd7z0ppcrcmp2cpkwsgyg55aadxh9yqfhkghm308qxcw") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.5 (crate (name "wide") (vers "0.5.1") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0c8gn6yy0pm4k47msir74igaz0ccy5y30dfjw8lm32v74fc24yz9") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.5 (crate (name "wide") (vers "0.5.2") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1id2l5rnygg1wk1fh58gib8y8sr55lli9p3ilh6cg9dnm5gi8427") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.5 (crate (name "wide") (vers "0.5.3") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0ardwlqfg8294lmpkqm7yppph5ayg69s2wfd6nvmjdgs6hcs9gy7") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.5 (crate (name "wide") (vers "0.5.4") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1h400rcsmbhprkp637paacyc7qg8czyfh9bpkdbwqrzw9mclnpyg") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.5 (crate (name "wide") (vers "0.5.5") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "04cj5vlw8mwgc3j5w428f1xjs00yr3mcknc1wqgxhay2xiv7rcl0") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.5 (crate (name "wide") (vers "0.5.6") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "12qq6w9g44vgmkq4dwyjwnfxf4za4f63mhz2qqzv8x1ckihky1qh") (features (quote (("std") ("default")))) (yanked #t)))

(define-public crate-wide-0.6 (crate (name "wide") (vers "0.6.0") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "17ksq40bhpkzivzjm0xsrfhalcn9s7my4w0zsznr08gm56n4dxhq") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.6 (crate (name "wide") (vers "0.6.1") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "07qsxxqxwxg0cjak1741v0mvi5nh0ivyny254ki71442p6619yc7") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.6 (crate (name "wide") (vers "0.6.2") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1k1nnlr8nyh1hfgh0wkxkwi64vzqn0cs0v9ibfdsx196dgm8y74k") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.6 (crate (name "wide") (vers "0.6.3") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0s1gj6cmkw3609ixamzmaxdwpb8g19gj4rgyw1ijyir0cmk81w42") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.6 (crate (name "wide") (vers "0.6.4") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1vp1w74jrkhs19z3gg163bfli4a41l0s4cbxmmpr0cv3xxhg2l9c") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.6 (crate (name "wide") (vers "0.6.5") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.5") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "16yqf8xsvns21clg3rk8nljq4qnsfqg240smbjqa0z520k3fgfs6") (features (quote (("std") ("default"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.0") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1rr23wi8p96xynywl0vgjzgrs3m21vzhsz7hhkp57ybi8i4cz2fd") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.1") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "14y8js69x0hb4pkg249ahz5a4ncyx4hd0h614vd1jrjgjn759wnj") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.2") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1vdgmx6drry5rgm5k4g2gkhnc2k7bj3cxwzpnm68sylz2f571klz") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.3") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0qlb9w0j8xnaxybba2lvw83kkh2bkabzyf76zwgjnqr5sbqs4va7") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.4") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0psw31mh21cn4w7i0klsz7law8p8iddsqir8x35cf6n3vb8s5axk") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.5") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0p2jsbvir4x0f8slm54mqyhc04xin1298j7qirf6b8c94jnyqhdf") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.6") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0v1nv46z2irdbw6qkscm5pahdaqdikbchv3vfwip5mll510hmzzy") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.7") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1ln04lvrifvdf752ckgg2slkmicq54n3s0h15yj9v7p8vwm1vhdk") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.8") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "08xjlgzpyl47v3llg8dp7iln7krqjbrv1rj4z55l6jb5kp2bd2dn") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.9") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.6") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0rgzd485mp3265k7x0497plrjl3a89317n8fpz26pk7kf5m4kl2w") (features (quote (("std") ("default" "std"))))))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.10") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0a27g3w3bdwdja1inml69fzxxszj6f6vmylhjwm618nvw8iqc0a0") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.11") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0bynfx8ibdmpb0ivkbiizm9l51cvp2bkc634y6h0nzpgcpx9yima") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.12") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1615c1qk39kzrngx5w0w0r35gbv3zaxm0xf49c5qdwcgqgpypv7b") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.13") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0hl2974x5f34vrpibqngm6dd97kwqdgzqb21jlqkdnikgfski2f6") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.14") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "04qn4xcn294k37g2whfwxkrbvr12qgxjfpvinsgpi8gb8kb9265k") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.15") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "071cl37d9ni0a4r8y1hjyr5vq0w0w3wf742lqagnfvi49xafrgl9") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.16") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1ijv1m6pxk212iyc8yid2j6dw03c5z3ia3m4zv9d24czf4d8b8c1") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.17") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "07z16ipyiqwn0z12skrgxlvvkq1l3z7mid0j9d8c1z83qv93j3hg") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.18") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1dhbsfc66jry48qgnfy1av5qanrnnbg1vcgdddvnx7mghnggh9ar") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.19") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0rbhdgflm9wwah74hmkhg41qjbxn39wgmifvllcdf1nyj10mkdma") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.20") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "0g7g2n6cbp9nipyi7wd7yqz3ailwk8pb5773m61l2y1mrjj0bq11") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-0.7 (crate (name "wide") (vers "0.7.21") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (features (quote ("bytemuck"))) (default-features #t) (kind 0)))) (hash "1z6pfk6pkxribm9d9ikbgjdx0ppfq3ss8r1hb8jkqgxhl54wg3fd") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-wide-literals-0.1 (crate (name "wide-literals") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1qr4k1i74g79g1p6pv2vdcr2fd2ab0qcbj5qll3pnym6wazf7bi3")))

(define-public crate-wide-literals-0.2 (crate (name "wide-literals") (vers "0.2.0") (deps (list (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1qq647f6nj4ml8z9l6r6y4apif3mhm68mp9k4snrzjq89g5d0r3h")))

(define-public crate-wide-str-0.1 (crate (name "wide-str") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "wide-str-impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ac3988fzmyrz10iqrvax9v9zzzm96a6g27wxyzjvv9bp184m29f")))

(define-public crate-wide-str-impl-0.1 (crate (name "wide-str-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0l7hd1d1w16bd7kh4misq0bfchxjjwwazrixghhi9668qamz4n76")))

(define-public crate-wider_primitives-0.0.5 (crate (name "wider_primitives") (vers "0.0.5") (deps (list (crate-dep (name "cfg_aliases") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "12z1c8pngq90fzjkcscqmh4r5ssb95b30hjypg6qhpxr25nzp6hh") (features (quote (("unstable") ("internals-doc-visible"))))))

(define-public crate-wider_primitives-0.0.6 (crate (name "wider_primitives") (vers "0.0.6") (deps (list (crate-dep (name "cfg_aliases") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "16271zyimbrqa8ykijl74mbgfzxhgygrvg65v1ac5jyp394kdp2f") (features (quote (("unstable") ("internals-doc-visible"))))))

(define-public crate-wider_primitives-0.0.7 (crate (name "wider_primitives") (vers "0.0.7") (deps (list (crate-dep (name "cfg_aliases") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1c0b3ygn262six7p0w9y9ch1a7hfsd4gxn6kglydhww9klzwh4l4") (features (quote (("unstable") ("internals-doc-visible"))))))

(define-public crate-widerror-0.0.0 (crate (name "widerror") (vers "0.0.0") (hash "1hy87qbxb2lclqr4852lk5w35nwvvnhyjmhvnfb1cid3fshshj60")))

(define-public crate-widerwlock-0.5 (crate (name "widerwlock") (vers "0.5.0") (deps (list (crate-dep (name "parking_lot") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "parking_lot_core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 2)) (crate-dep (name "xoshiro") (req "^0.0.4") (default-features #t) (kind 2)))) (hash "19ici71qc1fs4sm9f0k5v5xzwcv0ww0fvn2rcf065ipjs5fp1hck")))

(define-public crate-widestring-0.1 (crate (name "widestring") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "1zg3qwhpg8k0ac72lswxzvkbb04ghgaspyyzb9ff3qz6vhzjxvcj")))

(define-public crate-widestring-0.2 (crate (name "widestring") (vers "0.2.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "11brniysddgxccgzrz8n9jh3pnj99w1jipdryczikndyhbmj8hfg")))

(define-public crate-widestring-0.2 (crate (name "widestring") (vers "0.2.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "0cf9h6qhx6mrlql1rad75a3aaf91ph0llfw0fcyp5cdjga1p7cfm")))

(define-public crate-widestring-0.2 (crate (name "widestring") (vers "0.2.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "1nyacxk2cbyrmn8vlra4z9ns2818qn178yshkhcd5qqj5r670mvi")))

(define-public crate-widestring-0.3 (crate (name "widestring") (vers "0.3.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "108phf854bzmqg04cxgxy68gh7r8qr7slqrnz1251gwgllp944m2")))

(define-public crate-widestring-0.4 (crate (name "widestring") (vers "0.4.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "1dhx6dndjsz1y7c9w06922412kdxyrrkqblvggm76mh8z17hxz7g")))

(define-public crate-widestring-0.4 (crate (name "widestring") (vers "0.4.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "06j64bv2b5g07khvzysx9x4kwazvbg2m9nz8hf4nl7zygp337ic9") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-widestring-0.4 (crate (name "widestring") (vers "0.4.2") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "13565qy4jhpg4x0xw8mwxzzsh0p8c93p5208lh6kpwp0q01y6qx7") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-0.4 (crate (name "widestring") (vers "0.4.3") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "0z3ba8qrxb62vpfgk7n2xs2grm9kdaj1cz4q8s0gs8fx8h0r8s61") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-0.5 (crate (name "widestring") (vers "0.5.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "1dkm41c8a3zszqszljr062x05imqc5smp5zfzqgf9r0v0nan7ghm") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-0.5 (crate (name "widestring") (vers "0.5.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "10qrilijh1qzw362mvd4nsz3vv32dxx530vk41hkcx8hah22z20p") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-widestring-1 (crate (name "widestring") (vers "1.0.0-beta.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "13yfb89c84az4ggg9z2c4jfin75c6hbm194lb7wfs1x753lfzwg6") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.56")))

(define-public crate-widestring-1 (crate (name "widestring") (vers "1.0.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "1a1pmaj7m9csh8c9ffhp8rf6y5c4fr1kxy9ff7wvqa6d7wnvvfmm") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.58")))

(define-public crate-widestring-1 (crate (name "widestring") (vers "1.0.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "0hz4z7ynzmgkg75y74s5ywx7vilzdj2n8j7174c63apipymv3sx5") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.58")))

(define-public crate-widestring-1 (crate (name "widestring") (vers "1.0.2") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "1a11qxmqf8jhh0vbyb6cc614d9qdqsh01r5bqnivn5pc74gi8gv5") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.58")))

(define-public crate-widestring-1 (crate (name "widestring") (vers "1.1.0") (deps (list (crate-dep (name "debugger_test") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "debugger_test_parser") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "048kxd6iykzi5la9nikpc5hvpp77hmjf1sw43sl3z2dcdrmx66bj") (features (quote (("std" "alloc") ("default" "std") ("debugger_visualizer" "alloc") ("alloc")))) (rust-version "1.58")))

