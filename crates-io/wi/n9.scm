(define-module (crates-io wi n9) #:use-module (crates-io))

(define-public crate-win95-keygen-0.1 (crate (name "win95-keygen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1v1j1llvhlvgvj0iigfb04ys9wi90n4bkv7x7rdzbdbsqsi5y9y7")))

(define-public crate-win95-keygen-0.1 (crate (name "win95-keygen") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0vhdc3m6131w05jqlbqz7rcy5jsl1s8zmvrkdsyl0fpccng806cj")))

(define-public crate-win95-keygen-0.2 (crate (name "win95-keygen") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1lldhpia7ryxmws4nj21k7jqgxnv9d8l5fx9mjfl620gwblrzjb7")))

(define-public crate-win9x-sync-0.1 (crate (name "win9x-sync") (vers "0.1.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "vc6-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zv0jy8qxb8q6dq7azswapy6zywrsaxd3vscnf5a8r2j6kcrclx0") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std" "vc6-sys/rustc-dep-of-std"))))))

(define-public crate-win9x-sync-0.1 (crate (name "win9x-sync") (vers "0.1.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "vc6-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "02nvzmia02xix4lnfancngsgq021mgidr7q2g9wd7f4y37s5ar0x") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std" "vc6-sys/rustc-dep-of-std"))))))

(define-public crate-win9x-sync-0.2 (crate (name "win9x-sync") (vers "0.2.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "vc6-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "16xlvf43ig1f2l92sr0drz8kcra7ibm6nfs8rslvdiilb4vgrqj0") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std" "vc6-sys/rustc-dep-of-std"))))))

