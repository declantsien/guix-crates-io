(define-module (crates-io wi nr) #:use-module (crates-io))

(define-public crate-winr-0.0.0 (crate (name "winr") (vers "0.0.0") (deps (list (crate-dep (name "eventify") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.52.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_WindowsAndMessaging" "Win32_UI_Input_KeyboardAndMouse"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1iczpcw5ya6vb8r558c4x50vpg4yshdc57vypb197h17dc6258xa")))

(define-public crate-winreader-0.1 (crate (name "winreader") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser" "memoryapi" "handleapi" "processthreadsapi" "psapi"))) (default-features #t) (kind 0)))) (hash "0ikrrwx0iihgmsngw1h27090ww4zvcjcppp7s69psav88cvybc95")))

(define-public crate-winreg-0.1 (crate (name "winreg") (vers "0.1.0") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "1ggpmkpamclski6vsajxpbbdhcnarznz13vvbwzs2qxmpbvjx2gl")))

(define-public crate-winreg-0.1 (crate (name "winreg") (vers "0.1.1") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "05gjxcvinl2krznmghfinc2qwj45sm2hvyr2m4wqs90pc94v0fqf")))

(define-public crate-winreg-0.2 (crate (name "winreg") (vers "0.2.0") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "0pw07y793qg47a8s5yjc9dkqkfcvzzvib1vl6yllki7vsqr245ww")))

(define-public crate-winreg-0.2 (crate (name "winreg") (vers "0.2.5") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "0m8sjy5wkpl92cn0665x8b3vswl6wgxxd9391sin415ywnf8ca2n")))

(define-public crate-winreg-0.2 (crate (name "winreg") (vers "0.2.6") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "1rhcwf2szim13ylwi6377gf5ii516mgmars0bm02pf2db6v8xspb")))

(define-public crate-winreg-0.3 (crate (name "winreg") (vers "0.3.0") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ktmw32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "0z8zndhmcpgy6rkj8vnk61bhxjv7j1r0q8j21xn6y4dvv2fjisq5")))

(define-public crate-winreg-0.3 (crate (name "winreg") (vers "0.3.1") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ktmw32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16m0vkgcc4y63lpyvq5vsqyfng0pgicf52kf06n0nz3j9szfavff")))

(define-public crate-winreg-0.3 (crate (name "winreg") (vers "0.3.2") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ktmw32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ynrg651q8bzdh7623cy819jyj7d4f3761yb77gndv9z737dbh2c")))

(define-public crate-winreg-0.3 (crate (name "winreg") (vers "0.3.3") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ktmw32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0rrzy3xk2ghqw8hg0qwm742p3pg3aakc0jwnrv2kpqlzd6yvlcf7")))

(define-public crate-winreg-0.3 (crate (name "winreg") (vers "0.3.4") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ktmw32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0jkkxylvvwd6ripbd0fnngb1014bzc86vb18z04nsv4kfd93w2md") (features (quote (("default"))))))

(define-public crate-winreg-0.3 (crate (name "winreg") (vers "0.3.5") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ktmw32-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1sggzsxnvywjgsb78x3ymim7cxjw51cb31pz9x69nq9z47xmff76") (features (quote (("default"))))))

(define-public crate-winreg-0.4 (crate (name "winreg") (vers "0.4.0") (deps (list (crate-dep (name "advapi32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ktmw32-sys") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bwnbx7n1w51madjjdfkm6vimlz9ddb9l3crpmmg5mr0kwvqbwnb") (features (quote (("transactions" "ktmw32-sys") ("serialization-rustc" "transactions" "rustc-serialize") ("default" "transactions" "serialization-rustc"))))))

(define-public crate-winreg-0.5 (crate (name "winreg") (vers "0.5.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "0b5xfhl43prv9f171dr3qj3jxrm2gyvxpm5apqw9m207p9x0cf4k") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.5 (crate (name "winreg") (vers "0.5.1") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "0jkh4jj2g8g0bl7r1xvq9vv9hr4gdzphg9ndqm65q6f1jn9paym2") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.6 (crate (name "winreg") (vers "0.6.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "1q5n8a4lqqbb763fr7hnfycp38qc25iqs10i9wf45gxis2appxns") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.6 (crate (name "winreg") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "17gcjv5jx3s59av7b272043r5ayal9vc95hvalcb3jnkqk3g7wbk") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.6 (crate (name "winreg") (vers "0.6.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "1jdcqr6zmvwyrp87h48miasfdvv16gjsb60rc8dy2kqwb3mnv65j") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.7 (crate (name "winreg") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "0sdxcyvda4v1v6a0k1j2v1400z3ng323k9a56gxvkq51x21dn801") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.8 (crate (name "winreg") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "1364vyx4kh170pxfg8iwlvv8xskvry53xfya0565q8qnx73gh1yi") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.9 (crate (name "winreg") (vers "0.9.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "15rmb4rnddxgs19yrmbpanyfxypakdk4hj999ii7zkwphf4v7k8n") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.10 (crate (name "winreg") (vers "0.10.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "09vjwrlcr83vammgg9gwjnca7a6fh157bfi4zdvfxgij5azb9ba2") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde")))) (yanked #t)))

(define-public crate-winreg-0.10 (crate (name "winreg") (vers "0.10.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "17c6h02z88ijjba02bnxi5k94q5cz490nf3njh9yypf8fbig9l40") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.11 (crate (name "winreg") (vers "0.11.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "12gixkw8gz38c2q7kkd2na77z07jara7vyg8662416qfymzsb8bn") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.50 (crate (name "winreg") (vers "0.50.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_System_Time" "Win32_System_Registry" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_Diagnostics_Debug"))) (default-features #t) (kind 0)))) (hash "1cddmp929k882mdh6i9f2as848f13qqna6czwsqzkh1pqnr5fkjj") (features (quote (("transactions") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.12 (crate (name "winreg") (vers "0.12.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "09fs2svsysfn37xbfdbw3hrybrhkp5ab5n9mvgyjp23xd8b6mvsy") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.51 (crate (name "winreg") (vers "0.51.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_System_Time" "Win32_System_Registry" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_Diagnostics_Debug"))) (default-features #t) (kind 0)))) (hash "1z48nmaskwsiyyq9576sgf8ya3fvf1xg3kma8q7n8ml1jkvkszwk") (features (quote (("transactions") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.13 (crate (name "winreg") (vers "0.13.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "1827blb21k6qamkf3j1zmalpsi9jkpgyv0d681xhakl2001bmvdx") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.14 (crate (name "winreg") (vers "0.14.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("impl-default" "impl-debug" "minwindef" "minwinbase" "timezoneapi" "winerror" "winnt" "winreg" "handleapi"))) (default-features #t) (kind 0)))) (hash "118swsnqpqcla0wijz7dpjjk56hpzcgjh04l38z1509rvydk1dx1") (features (quote (("transactions" "winapi/ktmw32") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winreg-0.52 (crate (name "winreg") (vers "0.52.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "~3.0") (default-features #t) (kind 2)) (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_System_Time" "Win32_System_Registry" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_Diagnostics_Debug"))) (default-features #t) (kind 0)))) (hash "19gh9vp7mp1ab84kc3ag48nm9y7xgjhh3xa4vxss1gylk1rsaxx2") (features (quote (("transactions") ("serialization-serde" "transactions" "serde"))))))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.0") (deps (list (crate-dep (name "toml") (req "^0.1") (default-features #t) (kind 0)))) (hash "0sp4ji5yb45hdkypahsv4rg8bn0wn8r9slaldmk77p3fd7257cc2")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.1") (deps (list (crate-dep (name "toml") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "1ppw0v2jcpd4xgvrzmni6c9d2di27l0hgig1q93gy9qwcnk2mrfq")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.3") (deps (list (crate-dep (name "toml") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "0xi4amzpwwympic7815v91kwp1gdnz6md6bhqbx136b6msyjgpzh")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.4") (deps (list (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 2)))) (hash "0fhal45h6mq2zwrw5z2fbwq52v2blmi1wm5kn9mpmgq7macd1nqq")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.5") (deps (list (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "0pgjnhilck4zydl9j15nck7l6xdn0ranrv2x83lfzmsnccnikn97")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.6") (deps (list (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "13gji0fn3n6rij39ya4xkz25vn6k98is5javqv53whbr9vdanzgh")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.7") (deps (list (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "0p9wz1r2s24ckaq6mp0qd4hwklfg7h16zi4cld6wp280c220n6nj")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.8") (deps (list (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "1kcy3wvn5jrggb4b2pak4z1kpkinfyrdy3r5qxgikfhm9c5qv95f")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.9") (deps (list (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "0gpxf7f2x0kqkkrwsm6xyy0gr29y1hz0pvwd2r7mm5gymcf1il93")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.10") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "1rv01sfq1hffbnq6kcvcfg08fzs8nfcg7xb0w0hi12qc9q8920il")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.11") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "1p55gj03m5j41w6a1vghg0l0dkz6dwp7mxqmzy98jnzypc8bakzz")))

(define-public crate-winres-0.1 (crate (name "winres") (vers "0.1.12") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "0v2gvqnd8iwwvb6fs69nv0mmk1z96430527n0qlfbsarxxhv53dn")))

(define-public crate-winres-edit-0.0.0 (crate (name "winres-edit") (vers "0.0.0") (hash "19nv37nw5kl2s8hplvyirp5m4ndr13843qlsrjb23scac9296q50")))

(define-public crate-winres-edit-0.1 (crate (name "winres-edit") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "manual-serializer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Diagnostics_Debug" "Win32_System" "Win32_Security" "Win32_Graphics_Gdi" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "10v3qvazjayrjgzfs718pb305gmbfhflg3zlmmhpjm2bmqfb9lny")))

(define-public crate-winres-edit-0.2 (crate (name "winres-edit") (vers "0.2.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "manual-serializer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_System_LibraryLoader" "Win32_System_Diagnostics_Debug" "Win32_System" "Win32_Security" "Win32_Graphics_Gdi" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "1134c54g7yr72bi4559n094pfq7xykwzskbh5a9ijxphmbh70gqh")))

(define-public crate-winresource-0.1 (crate (name "winresource") (vers "0.1.12") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "1zx333r2q9kp62yjpmq90n29gl2mp9m7x90khhylagzfszzc3krp")))

(define-public crate-winresource-0.1 (crate (name "winresource") (vers "0.1.13") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "11k3f0mc2a4q2rmz0m55wwihxzsyciq76d838zbva8ns3hlig6am")))

(define-public crate-winresource-0.1 (crate (name "winresource") (vers "0.1.14") (deps (list (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "0sb9slva788hshpdh46n4pmhsa046dalkmxhsl4pir0kfzr7dp6z")))

(define-public crate-winresource-0.1 (crate (name "winresource") (vers "0.1.15") (deps (list (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "0c5ws4cmpwhyq91k3y39n15rqfrhp1l30rpf0fyw0137mvqqrlsw")))

(define-public crate-winresource-0.1 (crate (name "winresource") (vers "0.1.16") (deps (list (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "08ww4h28b9ykixrldbmqv5rfas1hvhxs19acpc45rfnyr2lgk84b")))

(define-public crate-winresource-0.1 (crate (name "winresource") (vers "0.1.17") (deps (list (crate-dep (name "toml") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt"))) (default-features #t) (kind 2)))) (hash "0aakwh8llq2zvm7qihkrg7sz50hzccyl4x831j60g4psijpsmqkp") (features (quote (("default" "toml"))))))

(define-public crate-winresult-0.1 (crate (name "winresult") (vers "0.1.0") (deps (list (crate-dep (name "winresult-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xa4ga0kpgyk5rmczlpwjkaabhcims9w3xqig9vp4p7yhcipvny2")))

(define-public crate-winresult-0.1 (crate (name "winresult") (vers "0.1.1") (deps (list (crate-dep (name "winresult-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rc1rbb0dwd8lqg6j4h1dpr82qhanz7j9a21mwk3aiwf5wihz43y")))

(define-public crate-winresult-0.1 (crate (name "winresult") (vers "0.1.2") (deps (list (crate-dep (name "winresult-types") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1z4q7h4shf4m42qpmb8ng09hs9cc5ycvl3wh4c28r2rxlm71n3zv")))

(define-public crate-winresult-0.1 (crate (name "winresult") (vers "0.1.3") (deps (list (crate-dep (name "winresult-types") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "13gm27f6bpdpagah6b8a7frnc59cssawdk3k9q8xzwr1ac0x9q8z")))

(define-public crate-winresult-types-0.1 (crate (name "winresult-types") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cy5q188jxkhy3qdpv2ag2db228gr6xfpmwxq6nxfc88zzxkz73n")))

(define-public crate-winresult-types-0.1 (crate (name "winresult-types") (vers "0.1.2") (deps (list (crate-dep (name "bytemuck") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cxgqc04lyqn45m4w7nqa1sss93qjpqj2j7kmlm46qbdm4wkpk4x")))

(define-public crate-winresult-types-0.1 (crate (name "winresult-types") (vers "0.1.3") (deps (list (crate-dep (name "bytemuck") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1s71alcxsaypwz4qnd9d73kng6cifb8ga00l110wrdwrxpa4ycf7")))

(define-public crate-winroute-0.1 (crate (name "winroute") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("iphlpapi" "netioapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1cs4gnasrx6wnx034k7flj07gc06wyg58219hk18si4sb5x6qn3x") (yanked #t)))

(define-public crate-winroute-0.1 (crate (name "winroute") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("iphlpapi" "netioapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1zgy291pzcinhaqqxl4jv0nqldijafh3cj0a3v412nfybggwj40z")))

(define-public crate-winroute-0.1 (crate (name "winroute") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("iphlpapi" "netioapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1b1gkadip3vhz20wr88f3wyac15wal1zvbg6a24mdb5gxsy7npcb")))

(define-public crate-winroute-0.2 (crate (name "winroute") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("iphlpapi" "netioapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1z3b16fags8sjjmjvi32f1hl0a42qzmybng493p1784lqqcj2bmn") (features (quote (("serializable" "serde") ("default" "serializable"))))))

(define-public crate-winrs-0.1 (crate (name "winrs") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("ntdef" "errhandlingapi"))) (default-features #t) (kind 0)))) (hash "1w8b0iihh41anzljyxw9a909vjkxrxwdnc4l4xdhhdf50xgg8fhh") (features (quote (("default" "clipboard") ("console" "winapi/consoleapi" "winapi/wincon") ("clipboard" "winapi/winuser"))))))

(define-public crate-winrt-0.0.1 (crate (name "winrt") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "0srbk7x2sa9chqcfcckds0dd16sspv6hv7p6yn377ccxbv7qbaz0") (yanked #t)))

(define-public crate-winrt-0.1 (crate (name "winrt") (vers "0.1.0") (deps (list (crate-dep (name "ole32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "oleaut32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "runtimeobject-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0dzaaaglhm8m9514y9s0290z6gf28kfaq5q967pya2jndfc4msap") (features (quote (("windows_web") ("windows_ui_xaml" "windows_ui") ("windows_ui") ("windows_system") ("windows_storage") ("windows_services") ("windows_security") ("windows_perception") ("windows_networking") ("windows_media") ("windows_management") ("windows_graphics") ("windows_globalization") ("windows_gaming") ("windows_devices") ("windows_data") ("windows_applicationmodel") ("nightly") ("all" "windows_applicationmodel" "windows_data" "windows_devices" "windows_gaming" "windows_globalization" "windows_graphics" "windows_management" "windows_media" "windows_networking" "windows_perception" "windows_security" "windows_services" "windows_storage" "windows_system" "windows_ui" "windows_ui_xaml" "windows_web"))))))

(define-public crate-winrt-0.2 (crate (name "winrt") (vers "0.2.0") (deps (list (crate-dep (name "ole32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "oleaut32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "runtimeobject-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "118w59dqj9db5x40dvi2v16q3a2fa275qhz7qc7vz2r1yzjg9dil") (features (quote (("windows-web") ("windows-ui-xaml" "windows-ui") ("windows-ui") ("windows-system") ("windows-storage") ("windows-services") ("windows-security") ("windows-perception") ("windows-networking") ("windows-media") ("windows-management") ("windows-graphics") ("windows-globalization") ("windows-gaming") ("windows-devices") ("windows-data") ("windows-applicationmodel") ("nightly") ("all" "windows-applicationmodel" "windows-data" "windows-devices" "windows-gaming" "windows-globalization" "windows-graphics" "windows-management" "windows-media" "windows-networking" "windows-perception" "windows-security" "windows-services" "windows-storage" "windows-system" "windows-ui" "windows-ui-xaml" "windows-web")))) (yanked #t)))

(define-public crate-winrt-0.2 (crate (name "winrt") (vers "0.2.1") (deps (list (crate-dep (name "ole32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "oleaut32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "runtimeobject-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0h8qrrf9p85aqd9db8wm277sp46pw5wsc5lcmzdymqx9yqij94pw") (features (quote (("windows-web") ("windows-ui-xaml" "windows-ui") ("windows-ui") ("windows-system") ("windows-storage") ("windows-services") ("windows-security") ("windows-perception") ("windows-networking") ("windows-media") ("windows-management") ("windows-graphics") ("windows-globalization") ("windows-gaming") ("windows-devices") ("windows-data") ("windows-applicationmodel") ("nightly") ("all" "windows-applicationmodel" "windows-data" "windows-devices" "windows-gaming" "windows-globalization" "windows-graphics" "windows-management" "windows-media" "windows-networking" "windows-perception" "windows-security" "windows-services" "windows-storage" "windows-system" "windows-ui" "windows-ui-xaml" "windows-web"))))))

(define-public crate-winrt-0.3 (crate (name "winrt") (vers "0.3.0") (deps (list (crate-dep (name "ole32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "oleaut32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "runtimeobject-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "02l7n2xs872fic4w6da9jhva6cxlf3xpz1pwa2nmb1ylqja26cnf") (features (quote (("windows-web") ("windows-ui-xaml" "windows-ui") ("windows-ui") ("windows-system") ("windows-storage") ("windows-services") ("windows-security") ("windows-perception") ("windows-networking") ("windows-media") ("windows-management") ("windows-graphics") ("windows-globalization") ("windows-gaming") ("windows-devices") ("windows-data") ("windows-applicationmodel") ("nightly") ("all" "windows-applicationmodel" "windows-data" "windows-devices" "windows-gaming" "windows-globalization" "windows-graphics" "windows-management" "windows-media" "windows-networking" "windows-perception" "windows-security" "windows-services" "windows-storage" "windows-system" "windows-ui" "windows-ui-xaml" "windows-web"))))))

(define-public crate-winrt-0.4 (crate (name "winrt") (vers "0.4.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt" "combaseapi" "oleauto" "roapi" "roerrorapi" "hstring" "winstring" "winerror" "restrictederrorinfo"))) (default-features #t) (kind 0)))) (hash "13dl7i2w2mqc6s0wj38s4wkpkp10gvkjwb22bbf87c125slcnc3y") (features (quote (("windows-web") ("windows-ui-xaml" "windows-ui") ("windows-ui") ("windows-system") ("windows-storage") ("windows-services") ("windows-security") ("windows-perception") ("windows-networking") ("windows-media") ("windows-management") ("windows-graphics") ("windows-globalization") ("windows-gaming") ("windows-devices") ("windows-data") ("windows-applicationmodel") ("nightly") ("lang-compat") ("all" "windows-applicationmodel" "windows-data" "windows-devices" "windows-gaming" "windows-globalization" "windows-graphics" "windows-management" "windows-media" "windows-networking" "windows-perception" "windows-security" "windows-services" "windows-storage" "windows-system" "windows-ui" "windows-ui-xaml" "windows-web"))))))

(define-public crate-winrt-0.5 (crate (name "winrt") (vers "0.5.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt" "combaseapi" "oleauto" "roapi" "roerrorapi" "hstring" "winstring" "winerror" "restrictederrorinfo"))) (default-features #t) (kind 0)))) (hash "1qkh747bmk5pyxncihrmvn15ld4g83rzcik1lvq4mlh1l3ccd3mv") (features (quote (("windows-web") ("windows-ui-xaml" "windows-ui") ("windows-ui") ("windows-system") ("windows-storage") ("windows-services") ("windows-security") ("windows-perception") ("windows-networking") ("windows-media") ("windows-management") ("windows-graphics") ("windows-globalization") ("windows-gaming") ("windows-devices") ("windows-data") ("windows-applicationmodel") ("nightly") ("lang-compat") ("all" "windows-applicationmodel" "windows-data" "windows-devices" "windows-gaming" "windows-globalization" "windows-graphics" "windows-management" "windows-media" "windows-networking" "windows-perception" "windows-security" "windows-services" "windows-storage" "windows-system" "windows-ui" "windows-ui-xaml" "windows-web"))))))

(define-public crate-winrt-0.5 (crate (name "winrt") (vers "0.5.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt" "combaseapi" "oleauto" "roapi" "roerrorapi" "hstring" "winstring" "winerror" "restrictederrorinfo"))) (default-features #t) (kind 0)))) (hash "153zx6kr5ndh87w1yn4bjp40w2zzaxi8hiyv675lypx4qpm02k0c") (features (quote (("windows-web") ("windows-ui-xaml" "windows-ui") ("windows-ui") ("windows-system") ("windows-storage") ("windows-services") ("windows-security") ("windows-perception") ("windows-networking") ("windows-media") ("windows-management") ("windows-graphics") ("windows-globalization") ("windows-gaming") ("windows-devices") ("windows-data") ("windows-applicationmodel") ("windows-ai") ("nightly") ("lang-compat") ("all" "windows-ai" "windows-applicationmodel" "windows-data" "windows-devices" "windows-gaming" "windows-globalization" "windows-graphics" "windows-management" "windows-media" "windows-networking" "windows-perception" "windows-security" "windows-services" "windows-storage" "windows-system" "windows-ui" "windows-ui-xaml" "windows-web"))))))

(define-public crate-winrt-0.6 (crate (name "winrt") (vers "0.6.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt" "combaseapi" "oleauto" "roapi" "roerrorapi" "hstring" "winstring" "winerror" "restrictederrorinfo"))) (default-features #t) (kind 0)))) (hash "1j8n3lc3ghsk4js4c7m503gakh688g8yjid30p9q3pypmlvz8j62") (features (quote (("windows-web") ("windows-ui-xaml" "windows-ui") ("windows-ui") ("windows-system") ("windows-storage") ("windows-services") ("windows-security") ("windows-perception") ("windows-networking") ("windows-media") ("windows-management") ("windows-graphics") ("windows-globalization") ("windows-gaming") ("windows-devices") ("windows-data") ("windows-applicationmodel") ("windows-ai") ("nightly") ("all" "windows-ai" "windows-applicationmodel" "windows-data" "windows-devices" "windows-gaming" "windows-globalization" "windows-graphics" "windows-management" "windows-media" "windows-networking" "windows-perception" "windows-security" "windows-services" "windows-storage" "windows-system" "windows-ui" "windows-ui-xaml" "windows-web"))))))

(define-public crate-winrt-0.7 (crate (name "winrt") (vers "0.7.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_macros") (req "^0.7") (default-features #t) (kind 0)))) (hash "09xv1zp04000znzhxd8198c0cgn0j68y0zcfysipg0nlm83nfykf")))

(define-public crate-winrt-0.7 (crate (name "winrt") (vers "0.7.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_macros") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "18gyar9zd7nqn2fi64xsxjd5hq5ardanh0k31xhsw4kjz7ffvl1s")))

(define-public crate-winrt-0.7 (crate (name "winrt") (vers "0.7.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_macros") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "194mvdl5pf2l61ys4a6ws31mpznr1m2m782rxk5ddvx9aqg2pxns")))

(define-public crate-winrt-0.7 (crate (name "winrt") (vers "0.7.3") (hash "1zjlf1iq5wnb0jyr93ivf0i62wgif0im0n4bnfx18fyx1km0bzb0") (yanked #t)))

(define-public crate-winrt-0.8 (crate (name "winrt") (vers "0.8.0") (hash "166gi1nqi889z19gv3526dimp7zv9kjg5yva7b0103j0mg5visyr")))

(define-public crate-winrt-ai-0.0.0 (crate (name "winrt-ai") (vers "0.0.0") (hash "05lkil5y13407baiqz6zscak1findxvjj0gjfq78n3qil8h47ybi") (rust-version "1.46")))

(define-public crate-winrt-ai-0.0.1 (crate (name "winrt-ai") (vers "0.0.1") (hash "0kcdkja4mg2f9jfff87ilhs7v4vqqlmb979ccwjcscygvvs9l6g5") (rust-version "1.46")))

(define-public crate-winrt-ai-0.0.2 (crate (name "winrt-ai") (vers "0.0.2") (hash "1v5nk5m0f72x1nvz4ckgx2p631icms0dq3qxmvmc77afg93253wi") (rust-version "1.46")))

(define-public crate-winrt-ai-0.23 (crate (name "winrt-ai") (vers "0.23.0") (hash "0b6bhsclpmafa2d202237gl1iz77pw1f7l3xw2bh1rk0pci52n57")))

(define-public crate-winrt-ai-sys-0.0.0 (crate (name "winrt-ai-sys") (vers "0.0.0") (hash "18vhibx6wv4k612kjq7sdl553s4pgk5pqmiad1hkk1zmxig6dh3z") (rust-version "1.46")))

(define-public crate-winrt-ai-sys-0.0.1 (crate (name "winrt-ai-sys") (vers "0.0.1") (hash "0jw9ffc1nyknw5qbv2jh83sakzxq4adlmhm5yrj3xryysv229d75") (rust-version "1.46")))

(define-public crate-winrt-ai-sys-0.0.2 (crate (name "winrt-ai-sys") (vers "0.0.2") (hash "0ysp8s4qma2q8di0iz9xhk7xki4c7vfcly0iisd0rch8ibpcin59") (rust-version "1.46")))

(define-public crate-winrt-ai-sys-0.23 (crate (name "winrt-ai-sys") (vers "0.23.0") (hash "1fcmzknvd35fascm1djxksj77bw8jcflm5jcs9lvrk6g2wv2c68l")))

(define-public crate-winrt-applicationmodel-0.0.0 (crate (name "winrt-applicationmodel") (vers "0.0.0") (hash "018fjxh8xwkn4ajnl8k8jw0pyy2pjm3fv4s8pxknxz87kixjg4lv") (rust-version "1.46")))

(define-public crate-winrt-applicationmodel-0.0.1 (crate (name "winrt-applicationmodel") (vers "0.0.1") (hash "075nhy114rdarxfcy7ic3ram66w4pa47d08nbyabxqlqm6lnvixv") (rust-version "1.46")))

(define-public crate-winrt-applicationmodel-0.0.2 (crate (name "winrt-applicationmodel") (vers "0.0.2") (hash "0bs4k11vh4dsjqm0v3m7ahi4vgs6hp6lxk2bgmh129ajksnd5ayy") (rust-version "1.46")))

(define-public crate-winrt-applicationmodel-0.23 (crate (name "winrt-applicationmodel") (vers "0.23.0") (hash "0n54k83lzpplm5cwlc21zz7gw384ww2sx19ih06n1cal9axwbd47")))

(define-public crate-winrt-applicationmodel-sys-0.0.0 (crate (name "winrt-applicationmodel-sys") (vers "0.0.0") (hash "0x1r2kq328zk74cdsgz77cwnq284nc19kl2v0vymmh4dcpa3l8ky") (rust-version "1.46")))

(define-public crate-winrt-applicationmodel-sys-0.0.1 (crate (name "winrt-applicationmodel-sys") (vers "0.0.1") (hash "082hpx8a9c1q6nmda1pfp30dqfmh1aaj5ynjml5r4v0i129xh1bg") (rust-version "1.46")))

(define-public crate-winrt-applicationmodel-sys-0.0.2 (crate (name "winrt-applicationmodel-sys") (vers "0.0.2") (hash "1jddkkxrz0lv9sf9qgj0w988f32hc9sd9q719mzcgrv2ar6k9gpg") (rust-version "1.46")))

(define-public crate-winrt-applicationmodel-sys-0.23 (crate (name "winrt-applicationmodel-sys") (vers "0.23.0") (hash "1f3yc2sz1m53vn46qggkgzkavky161jzyxgqhd305ga4af1skwgp")))

(define-public crate-winrt-core-app-0.4 (crate (name "winrt-core-app") (vers "0.4.0") (deps (list (crate-dep (name "atomic") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.1") (default-features #t) (kind 1)) (crate-dep (name "winrt") (req "^0.6.0") (features (quote ("windows-applicationmodel" "windows-ui"))) (default-features #t) (kind 0)))) (hash "1da836b80irpib2jnc2qnjzjvdasgzhh48gzqzjbggqr9i342s4x")))

(define-public crate-winrt-data-0.0.0 (crate (name "winrt-data") (vers "0.0.0") (hash "1wryqp6ch23n1d9cks9rabk5sv1gxy01cr8g3054pdg7nby30pz9") (rust-version "1.46")))

(define-public crate-winrt-data-0.0.1 (crate (name "winrt-data") (vers "0.0.1") (hash "0mp21gbcnxpd3h6zp2w66ljk7gp7fwqswxinwmbviqznssx14gpw") (rust-version "1.46")))

(define-public crate-winrt-data-0.0.2 (crate (name "winrt-data") (vers "0.0.2") (hash "0dk6mac3i6sqlav3944vbzy3s0q117m1zdk5xand39j2j2nrri18") (rust-version "1.46")))

(define-public crate-winrt-data-0.23 (crate (name "winrt-data") (vers "0.23.0") (hash "1jgmzriw9mr23k7fjq5xkf3qsfnfj073k06cab1lnwxryrx12zx5")))

(define-public crate-winrt-data-sys-0.0.0 (crate (name "winrt-data-sys") (vers "0.0.0") (hash "07h7aac4qw84rpzgwbymwflqiqg5jrdga88fw1gsf6fff7n2x1c5") (rust-version "1.46")))

(define-public crate-winrt-data-sys-0.0.1 (crate (name "winrt-data-sys") (vers "0.0.1") (hash "0dfwk6b7kynv1fsxp81swjv3kmpq1km8ikwv5gl75sf7ga10lwq5") (rust-version "1.46")))

(define-public crate-winrt-data-sys-0.0.2 (crate (name "winrt-data-sys") (vers "0.0.2") (hash "065is6v22zinpbk5kf5fpj3b72lsjjh25qy6p3bln0136j99lisa") (rust-version "1.46")))

(define-public crate-winrt-data-sys-0.23 (crate (name "winrt-data-sys") (vers "0.23.0") (hash "0sf2z9l9hjhminpzsy2irhnr56rafmqahig63kf6j3rv5w2vw70c")))

(define-public crate-winrt-devices-0.0.0 (crate (name "winrt-devices") (vers "0.0.0") (hash "1q0635g3mnzb8xkvakqggn37ikwkvagk5jkam0hwsxsw7rsmc8qd") (rust-version "1.46")))

(define-public crate-winrt-devices-0.0.1 (crate (name "winrt-devices") (vers "0.0.1") (hash "0c30ir4vjm266rdfzp33rzx8psapzrpbw6243vb91pcdhn33fxcz") (rust-version "1.46")))

(define-public crate-winrt-devices-0.0.2 (crate (name "winrt-devices") (vers "0.0.2") (hash "1wavynxpyklnml6fxqcrdzdv72xpigl15f79i9h8gplrpg0j3j0m") (rust-version "1.46")))

(define-public crate-winrt-devices-0.23 (crate (name "winrt-devices") (vers "0.23.0") (hash "0aqy6difcinvg99lgbyidbbs6f319zqgar89hhxq61b1x8km73id")))

(define-public crate-winrt-devices-sys-0.0.0 (crate (name "winrt-devices-sys") (vers "0.0.0") (hash "0pm3hxik88xcnic1xy40l14ar93b6g1bbsmcnz37dfqrp3jz65ss") (rust-version "1.46")))

(define-public crate-winrt-devices-sys-0.0.1 (crate (name "winrt-devices-sys") (vers "0.0.1") (hash "03d3km1raw3w1lrwp1iiiwh4n2jadd9187w95awz52l8l2lyly7z") (rust-version "1.46")))

(define-public crate-winrt-devices-sys-0.0.2 (crate (name "winrt-devices-sys") (vers "0.0.2") (hash "1hakfgw0pzc8d1cn722cgkkgzwc5q85vcrmvz4kxbfvx10d1s2f1") (rust-version "1.46")))

(define-public crate-winrt-devices-sys-0.23 (crate (name "winrt-devices-sys") (vers "0.23.0") (hash "0xn8vv5ls97j0nz41jvryaw478y14j593b5hkihr3dqx4vkwkvwr")))

(define-public crate-winrt-embedded-0.0.0 (crate (name "winrt-embedded") (vers "0.0.0") (hash "1cqzzy4sqr3ly2g8zi2awd8nzp0gpn9p583rg42hf759w642ndb5") (rust-version "1.46")))

(define-public crate-winrt-embedded-0.0.1 (crate (name "winrt-embedded") (vers "0.0.1") (hash "110b8gs61nywyd4dbaz1j363f8lby1shih5w60a93x6mrimqp3zz") (rust-version "1.46")))

(define-public crate-winrt-embedded-0.0.2 (crate (name "winrt-embedded") (vers "0.0.2") (hash "126ydan4lkm7pvg140l0y21n1whq0wsh2wcmjc1x2d9jr9ymmlx7") (rust-version "1.46")))

(define-public crate-winrt-embedded-0.23 (crate (name "winrt-embedded") (vers "0.23.0") (hash "16nc111fmd9h5lrivzwmwfjr5bs6a56hcd3097ig2ckaxs1470zm")))

(define-public crate-winrt-embedded-sys-0.0.0 (crate (name "winrt-embedded-sys") (vers "0.0.0") (hash "094y0zly65517jcakmvpsk6cxb96i4pjxj5mbszgizg4p0d06spz") (rust-version "1.46")))

(define-public crate-winrt-embedded-sys-0.0.1 (crate (name "winrt-embedded-sys") (vers "0.0.1") (hash "077p9a9vfnyq0z3i6lbs9hhxxwv2xic53cjrizb268sv799mcjpp") (rust-version "1.46")))

(define-public crate-winrt-embedded-sys-0.0.2 (crate (name "winrt-embedded-sys") (vers "0.0.2") (hash "1m927hj2pd2pzy1ipacp0rlxzfq8c6qf08844d63zl8y1ql1ij6g") (rust-version "1.46")))

(define-public crate-winrt-embedded-sys-0.23 (crate (name "winrt-embedded-sys") (vers "0.23.0") (hash "1ynhz5n0pgv7a86n71s721i8a8k86dq6lj22jnshm11492y385kz")))

(define-public crate-winrt-foundation-0.0.1 (crate (name "winrt-foundation") (vers "0.0.1") (hash "1m70sfw2p3zv39nvqnbjnj8rx9gpahkxvyyffqn38k7c58cdm4sh") (rust-version "1.46")))

(define-public crate-winrt-foundation-0.0.2 (crate (name "winrt-foundation") (vers "0.0.2") (hash "1997dj2001lyk2zcbdd6aqzpj372qybwfwb84wv1x1lspig6lmmr") (rust-version "1.46")))

(define-public crate-winrt-foundation-0.23 (crate (name "winrt-foundation") (vers "0.23.0") (hash "0nyjzd9vl22pjkl5kxh8yl3gy2h5rgfd93ic1dwffm1r6mk7ajms")))

(define-public crate-winrt-foundation-sys-0.0.1 (crate (name "winrt-foundation-sys") (vers "0.0.1") (hash "0vh4c4jbagpr267f645z3w0m83agp3jqm2z5r8gry4s5xnlnwcy8") (rust-version "1.46")))

(define-public crate-winrt-foundation-sys-0.0.2 (crate (name "winrt-foundation-sys") (vers "0.0.2") (hash "0jv33xkf4y4l8klwd9vl36663xls8fwyxcv0zy81f7xf94vwj6aq") (rust-version "1.46")))

(define-public crate-winrt-foundation-sys-0.23 (crate (name "winrt-foundation-sys") (vers "0.23.0") (hash "0rjykpjxb9lf8rxdi9hjdzp57vi1s4l6k7b0cfh3n0sdhg96bad0")))

(define-public crate-winrt-gaming-0.0.1 (crate (name "winrt-gaming") (vers "0.0.1") (hash "0pq05wsj0wfmm9djpcs7hmrbqpl3qj313ibc4vb2hkgb81qzi5i0") (rust-version "1.46")))

(define-public crate-winrt-gaming-0.0.2 (crate (name "winrt-gaming") (vers "0.0.2") (hash "057v65ky4v0fpb0h8rrkz1r4djlywyph1fi40wfxghjqqjbhxbwm") (rust-version "1.46")))

(define-public crate-winrt-gaming-0.23 (crate (name "winrt-gaming") (vers "0.23.0") (hash "0r5x6m7di12j92sqja6clxpwra025c9yyp78r57jh4dia8w7ib7z")))

(define-public crate-winrt-gaming-sys-0.0.1 (crate (name "winrt-gaming-sys") (vers "0.0.1") (hash "0g6plxzbm07nnfwsc7yqp3j3d5n4rgghi8csbyyvrypg1jcakbzb") (rust-version "1.46")))

(define-public crate-winrt-gaming-sys-0.0.2 (crate (name "winrt-gaming-sys") (vers "0.0.2") (hash "1y3p3zdbm2dirhxxa8a8zj3wr4whw896drrl4bddbyhg8aj513iy") (rust-version "1.46")))

(define-public crate-winrt-gaming-sys-0.23 (crate (name "winrt-gaming-sys") (vers "0.23.0") (hash "1xlrq70qkxxx7m8n1awh0iicah2c2fx706zc7pl8d7jhjjnqmibb")))

(define-public crate-winrt-globalization-0.0.1 (crate (name "winrt-globalization") (vers "0.0.1") (hash "1mdi12ysxdbi8p78rci2fiqfvy89yhk58gq247jv64rdkbfihn1b") (rust-version "1.46")))

(define-public crate-winrt-globalization-0.0.2 (crate (name "winrt-globalization") (vers "0.0.2") (hash "04i5bncnim6z0bm91si7s5nygd390wdyrs3c7szij8z5s4cpfkri") (rust-version "1.46")))

(define-public crate-winrt-globalization-0.23 (crate (name "winrt-globalization") (vers "0.23.0") (hash "0i7vnpn3mjvwxk9cgfz2gyjzfki2h5cnhdxkw14gign59v94lbb8")))

(define-public crate-winrt-globalization-sys-0.0.1 (crate (name "winrt-globalization-sys") (vers "0.0.1") (hash "00pq7j7fvg8112kkiliylw48fcnv3zlz7c6kifydf373rilrldzx") (rust-version "1.46")))

(define-public crate-winrt-globalization-sys-0.0.2 (crate (name "winrt-globalization-sys") (vers "0.0.2") (hash "17h6l8gwm62qlpwm9h1h3ix62vdrskw4r03qxz17q4al10l13vzx") (rust-version "1.46")))

(define-public crate-winrt-globalization-sys-0.23 (crate (name "winrt-globalization-sys") (vers "0.23.0") (hash "06sh5frb8028imqlr2pw3a2kzsi0sls326plmmad1f1vk8qzy2kq")))

(define-public crate-winrt-graphics-0.0.1 (crate (name "winrt-graphics") (vers "0.0.1") (hash "1hzh8q6jdp1dff9sr73ci43gqbv72w386mkgdy2fxc3y83a5i76v") (rust-version "1.46")))

(define-public crate-winrt-graphics-0.0.2 (crate (name "winrt-graphics") (vers "0.0.2") (hash "1ws0fkj1zr0ashmm7qcbxjdy3ncb8q2bh0gxmnqyla1mvhqqv8sv") (rust-version "1.46")))

(define-public crate-winrt-graphics-0.23 (crate (name "winrt-graphics") (vers "0.23.0") (hash "1h0ll3krc2v8bn730dsrwwbx0vxskgybbbjzmkn0ml7lg1v10g5a")))

(define-public crate-winrt-graphics-sys-0.0.1 (crate (name "winrt-graphics-sys") (vers "0.0.1") (hash "17d942kmrmv6kagfkjmjpig7dbqryp1f1pvim57gpd4rb3igzgs7") (rust-version "1.46")))

(define-public crate-winrt-graphics-sys-0.0.2 (crate (name "winrt-graphics-sys") (vers "0.0.2") (hash "1xdhij9y5r57xvzd91lq6b0zmmnsp5bk39psr44nb7isp69nyi6z") (rust-version "1.46")))

(define-public crate-winrt-graphics-sys-0.23 (crate (name "winrt-graphics-sys") (vers "0.23.0") (hash "15c5z0klrr99xgj0gw8xbi44l3jhcxiqh6g8fjmzbhn3za9z6yqf")))

(define-public crate-winrt-management-0.0.1 (crate (name "winrt-management") (vers "0.0.1") (hash "05jgafq667shxk4vcmm1pc7grxgm2gqvwncyabid0y602i7kc8l7") (rust-version "1.46")))

(define-public crate-winrt-management-0.0.2 (crate (name "winrt-management") (vers "0.0.2") (hash "011hv1pg932vlmcpl1qh1w5vi3d5li0z3jnxsa95m7fc6wa91m6r") (rust-version "1.46")))

(define-public crate-winrt-management-0.23 (crate (name "winrt-management") (vers "0.23.0") (hash "0r63z1i0vbw2kajsgpxkjxln40cgvhah89dbpiz7wajqv97sw40s")))

(define-public crate-winrt-management-sys-0.0.1 (crate (name "winrt-management-sys") (vers "0.0.1") (hash "06l9b09gkqy2g6q51fh5ql1znis72javrsxxlwj7slmxnwnvv9h9") (rust-version "1.46")))

(define-public crate-winrt-management-sys-0.0.2 (crate (name "winrt-management-sys") (vers "0.0.2") (hash "11l1hksd7lh91syr29ajhjqkjrcykmgf2fbi4zhd81slhq8krw6w") (rust-version "1.46")))

(define-public crate-winrt-management-sys-0.23 (crate (name "winrt-management-sys") (vers "0.23.0") (hash "1bnalv014spgn14lcvdaxyami0i2ljh3m4qbzqz2fa8i77wrwm4r")))

(define-public crate-winrt-media-0.0.1 (crate (name "winrt-media") (vers "0.0.1") (hash "0iaiq71m6p9png3lmy51gwy97rnjbc8lkrlv21b1bq4kb1iknvnh") (rust-version "1.46")))

(define-public crate-winrt-media-0.0.2 (crate (name "winrt-media") (vers "0.0.2") (hash "0k2vvlib61yfn1q41va7vq2ij5gj07f7n6zm1ssd9n9pclci4qwr") (rust-version "1.46")))

(define-public crate-winrt-media-0.23 (crate (name "winrt-media") (vers "0.23.0") (hash "1rgkrc976xmppya93fkg1jzr6hh66c5rjn66anijfwh29qcmmngy")))

(define-public crate-winrt-media-sys-0.0.1 (crate (name "winrt-media-sys") (vers "0.0.1") (hash "0bqp6bwcsmc4ym76a7yk9dbzvj5bs7ifmrf7sz9gs5f85vzlmcpp") (rust-version "1.46")))

(define-public crate-winrt-media-sys-0.0.2 (crate (name "winrt-media-sys") (vers "0.0.2") (hash "07iyfag126fvyabgqhkkz6xddrkfwz2n890ikfn4dvnzvr3mcais") (rust-version "1.46")))

(define-public crate-winrt-media-sys-0.23 (crate (name "winrt-media-sys") (vers "0.23.0") (hash "1w8vw3grbmqmvfgy0gnz1sjilxvs1mxmxa1004dfwihajf5hn7v0")))

(define-public crate-winrt-networking-0.0.1 (crate (name "winrt-networking") (vers "0.0.1") (hash "0b51735f7k7n6qj6kkppzrpr56qkr83krbg4w7kjlganhacr44c2") (rust-version "1.46")))

(define-public crate-winrt-networking-0.0.2 (crate (name "winrt-networking") (vers "0.0.2") (hash "1gmbfkvydpshh02m3nlab2vdypymsfll03jd4r2j4mm3q3grpz1j") (rust-version "1.46")))

(define-public crate-winrt-networking-0.23 (crate (name "winrt-networking") (vers "0.23.0") (hash "18m1pxc0svvykzapshjkimqargh2654yamf4sa6zqylw51a7lr0y")))

(define-public crate-winrt-networking-sys-0.0.1 (crate (name "winrt-networking-sys") (vers "0.0.1") (hash "0y0wmsp3nn2iwpw5csihcrblw9dfbk6bx8srnmd01l55yzs2gnc6") (rust-version "1.46")))

(define-public crate-winrt-networking-sys-0.0.2 (crate (name "winrt-networking-sys") (vers "0.0.2") (hash "0hswywf7wlw68c5czf9ibsh6j39akph8jzxgfmii2xhg7hyh3xrq") (rust-version "1.46")))

(define-public crate-winrt-networking-sys-0.23 (crate (name "winrt-networking-sys") (vers "0.23.0") (hash "07j3y3xa3iq7zy0bqva27kbaw64c18sgqh7ihw796yyg85h3y0i0")))

(define-public crate-winrt-notification-0.1 (crate (name "winrt-notification") (vers "0.1.0") (deps (list (crate-dep (name "winrt") (req "^0.3.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0nyxm20qyymgc4p0mxz0q0qjaji1bhwnp3rqyfy8haxy2gdxlb72") (yanked #t)))

(define-public crate-winrt-notification-0.1 (crate (name "winrt-notification") (vers "0.1.1") (deps (list (crate-dep (name "winrt") (req "^0.3.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1av3d2wnydyyfvnz7fm5wd1hyhk0ws1i44s7nfbz6737ihx63hnx")))

(define-public crate-winrt-notification-0.1 (crate (name "winrt-notification") (vers "0.1.2") (deps (list (crate-dep (name "winrt") (req "^0.3.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0hf0i8wbfn943rqv9yskap907vcmwhrd1gh1hyc2mxnsmknn3kia")))

(define-public crate-winrt-notification-0.1 (crate (name "winrt-notification") (vers "0.1.3") (deps (list (crate-dep (name "winrt") (req "^0.3.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1ckrghq6i6mkszdzzmf7g8mj6913mpmazd524kk7d12qkh2vrdy0")))

(define-public crate-winrt-notification-0.1 (crate (name "winrt-notification") (vers "0.1.4") (deps (list (crate-dep (name "winrt") (req "^0.3.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1b9ms85g9310gz3ylrn19ap7syj7mn86ikbn6gaiawmm8qaggqi9")))

(define-public crate-winrt-notification-0.1 (crate (name "winrt-notification") (vers "0.1.5") (deps (list (crate-dep (name "winrt") (req "^0.3.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0fkb6j2pcwhhn3c7lavjv7pbd2xxy3k5b5dv5gb2c6s2gxjpi284")))

(define-public crate-winrt-notification-0.2 (crate (name "winrt-notification") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "winrt") (req "^0.4.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "07affgvl5lhvfjyc8l32yg24s554p549a8wmskpps4pkjcvkdbpb")))

(define-public crate-winrt-notification-0.2 (crate (name "winrt-notification") (vers "0.2.1") (deps (list (crate-dep (name "strum") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "winrt") (req "^0.4.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "06smq5j4zvb6ggi78w2y1pyjrxv5bx6bw32sxg948qg0h5bph1i8")))

(define-public crate-winrt-notification-0.2 (crate (name "winrt-notification") (vers "0.2.2") (deps (list (crate-dep (name "strum") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "winrt") (req "^0.4.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0hm2c8zyklk65myrnkyksaqlyv2n95i23qyjkdpjqy8dlmfsccbc")))

(define-public crate-winrt-notification-0.2 (crate (name "winrt-notification") (vers "0.2.3") (deps (list (crate-dep (name "strum") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "winrt") (req "^0.4.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1cf620mcbas8035iiv1dggi14xnf845pv4fyay03adp6w4xbxdyv")))

(define-public crate-winrt-notification-0.3 (crate (name "winrt-notification") (vers "0.3.0") (deps (list (crate-dep (name "strum") (req "^0.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0h3mpgphrshs2xp1jmrvgaq91gwvz9l80nicmipd90h3ydl54a0w")))

(define-public crate-winrt-notification-0.2 (crate (name "winrt-notification") (vers "0.2.4") (deps (list (crate-dep (name "strum") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "winrt") (req "^0.4.0") (features (quote ("windows-data" "windows-ui"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1i013b5zwx9y3hlwnwdx6cr3fy4bpahz4kdlva14d2k8h6r0wyap")))

(define-public crate-winrt-notification-0.3 (crate (name "winrt-notification") (vers "0.3.1") (deps (list (crate-dep (name "strum") (req "^0.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1jh731n9dnrvm2jk1dcildh0xb3i9znbm4rz04rm0rnsmi2rivp5")))

(define-public crate-winrt-notification-0.4 (crate (name "winrt-notification") (vers "0.4.0") (deps (list (crate-dep (name "strum") (req "^0.21.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.21.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.21.1") (default-features #t) (kind 1)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1nkjvfzw9s96i6r1iv1czgvky2rdzqjc29dnwqz2pi36ihmq7k7h")))

(define-public crate-winrt-notification-0.5 (crate (name "winrt-notification") (vers "0.5.0") (deps (list (crate-dep (name "strum") (req "^0.22.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.24.0") (features (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Data_Xml_Dom" "UI_Notifications"))) (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "windows") (req "^0.24.0") (features (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Win32_System_LibraryLoader" "Data_Xml_Dom" "UI_Notifications"))) (default-features #t) (target "cfg(target_env = \"gnu\")") (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0smyrjq0s141dmzl4mxwqx7ll8sgrbbi81qasprjajh3ivxh38gd")))

(define-public crate-winrt-notification-0.5 (crate (name "winrt-notification") (vers "0.5.1") (deps (list (crate-dep (name "strum") (req "^0.22.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.24.0") (features (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Data_Xml_Dom" "UI_Notifications"))) (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "windows") (req "^0.24.0") (features (quote ("Win32_Foundation" "Foundation_Collections" "Win32_System_SystemInformation" "Win32_System_LibraryLoader" "Data_Xml_Dom" "UI_Notifications"))) (default-features #t) (target "cfg(target_env = \"gnu\")") (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1x7djvadifkgzvkkrzyfr5mpzvaqzxibkrbkvk3f08qbhi9h6yh0")))

(define-public crate-winrt-perception-0.0.1 (crate (name "winrt-perception") (vers "0.0.1") (hash "0zxidllpqml3zkq31mmn13801d6fw9wrr3lgysmajnshv6k4jn7m") (rust-version "1.46")))

(define-public crate-winrt-perception-0.0.2 (crate (name "winrt-perception") (vers "0.0.2") (hash "0kgh914146rp7h5a9jwlapxw4xnnhsy5d4095zbgwjd2v2q5nw1r") (rust-version "1.46")))

(define-public crate-winrt-perception-0.23 (crate (name "winrt-perception") (vers "0.23.0") (hash "0bwzjhzqsgws093nx8b9zi7m6xyydc0ys56ii573rpj3d8mjggiq")))

(define-public crate-winrt-perception-sys-0.0.1 (crate (name "winrt-perception-sys") (vers "0.0.1") (hash "0qh2138bzjxnbj3904y41940svfs5ggvgbjrgz8hma88yknrhanq") (rust-version "1.46")))

(define-public crate-winrt-perception-sys-0.0.2 (crate (name "winrt-perception-sys") (vers "0.0.2") (hash "0dys3g52ix4sdhiyv4rcy6p9fga2k3w1cxa0h2qj2zn0a8lh6lav") (rust-version "1.46")))

(define-public crate-winrt-perception-sys-0.23 (crate (name "winrt-perception-sys") (vers "0.23.0") (hash "1a2jxdmz5k25w8yhiy7jjbbpjqvs07pl7fd5cw1f438mklyfx0ck")))

(define-public crate-winrt-phone-0.0.1 (crate (name "winrt-phone") (vers "0.0.1") (hash "173887b08ik7iami2hpc91znf3ljc3xblsgzwk568bfn7w5qwx52") (rust-version "1.46")))

(define-public crate-winrt-phone-0.0.2 (crate (name "winrt-phone") (vers "0.0.2") (hash "05h6a4cgsm0zmw30qg7nkbjanx05nwf44g8pznijhr4dggkkshdr") (rust-version "1.46")))

(define-public crate-winrt-phone-0.23 (crate (name "winrt-phone") (vers "0.23.0") (hash "0hqnqbb3ijyqhckmnxkxxabp49hjdcb3nb37jq8hwvxsm8kzc22m")))

(define-public crate-winrt-phone-sys-0.0.1 (crate (name "winrt-phone-sys") (vers "0.0.1") (hash "1w03r38d46fd6azdi8mlwdra6qh767siqyhrrpz8jj8ha6vs4033") (rust-version "1.46")))

(define-public crate-winrt-phone-sys-0.0.2 (crate (name "winrt-phone-sys") (vers "0.0.2") (hash "0w8kglk4lph33wy7y57pk01jbfywa744ck6g7ncs9hzzd8y96ich") (rust-version "1.46")))

(define-public crate-winrt-phone-sys-0.23 (crate (name "winrt-phone-sys") (vers "0.23.0") (hash "0kjwhcz1bdfkaq3rgdxcwfxblvaw1bp4k2502l9hizxfkdzdn3fm")))

(define-public crate-winrt-security-0.0.1 (crate (name "winrt-security") (vers "0.0.1") (hash "0j7ak3ss6kknxsfmqgk1hdcy8q0sj9p5d0ja2rvayalsg7kmh1sy") (rust-version "1.46")))

(define-public crate-winrt-security-0.0.2 (crate (name "winrt-security") (vers "0.0.2") (hash "1jm27cwwdcablmzvjla77qrcxbh70a7v39qwki3snly40cd4q4hk") (rust-version "1.46")))

(define-public crate-winrt-security-0.23 (crate (name "winrt-security") (vers "0.23.0") (hash "011vi8qz2cpmi8r9sqr1szvy98y2i1xhkbfyd44v3r28kqjs2xdn")))

(define-public crate-winrt-security-sys-0.0.1 (crate (name "winrt-security-sys") (vers "0.0.1") (hash "1smg2a5k2szj5hwzc2cx3iybiwk3yxscs65978wg6qsgii4pa64g") (rust-version "1.46")))

(define-public crate-winrt-security-sys-0.0.2 (crate (name "winrt-security-sys") (vers "0.0.2") (hash "1rb7py8j2mcsjgrvg5p59b4n1ss7c34gn87b1dlqvvx02fylysr1") (rust-version "1.46")))

(define-public crate-winrt-security-sys-0.23 (crate (name "winrt-security-sys") (vers "0.23.0") (hash "052n3m16z8cwixadhsjldjsj88kn48vfpvi468syffp8fhc1ca9x")))

(define-public crate-winrt-services-0.0.1 (crate (name "winrt-services") (vers "0.0.1") (hash "1vbzzipg0wfkmmjh5liw3jzzf8nzsiawnv4bpwdyr5z1m8dq5rgn") (rust-version "1.46")))

(define-public crate-winrt-services-0.0.2 (crate (name "winrt-services") (vers "0.0.2") (hash "0dyq9f0db0s26si3pjiab4gqvax3c78l2m8v62jqdjg3kgz46dlw") (rust-version "1.46")))

(define-public crate-winrt-services-0.23 (crate (name "winrt-services") (vers "0.23.0") (hash "0wlijjk26l2dzsdnnmvi78gb1rzvq4hl9j81zswgbc84q88ffk2b")))

(define-public crate-winrt-services-sys-0.0.1 (crate (name "winrt-services-sys") (vers "0.0.1") (hash "02fxh2qkzj6i4gmky4wf8s0gsy86jfb59rjlm4afxyl3302f8zd3") (rust-version "1.46")))

(define-public crate-winrt-services-sys-0.0.2 (crate (name "winrt-services-sys") (vers "0.0.2") (hash "1hkyj1nmih584dahx05mhrb9a4dmj24l8v7wiyx0xpp86qdb4grj") (rust-version "1.46")))

(define-public crate-winrt-services-sys-0.23 (crate (name "winrt-services-sys") (vers "0.23.0") (hash "03dxx50dfljqp2j9mn32557knqpqk4s3h0wfg8phf8idkalbigsl")))

(define-public crate-winrt-storage-0.0.1 (crate (name "winrt-storage") (vers "0.0.1") (hash "01gph2ax8y2lhl153zkw58c0xz0da5a3302zs8q009wzc7w7nc3x") (rust-version "1.46")))

(define-public crate-winrt-storage-0.0.2 (crate (name "winrt-storage") (vers "0.0.2") (hash "0qx8g0nkmqcr2vj8s81x4dgnnsfn39d5qwwzhjixw2jmjlrw5s39") (rust-version "1.46")))

(define-public crate-winrt-storage-0.23 (crate (name "winrt-storage") (vers "0.23.0") (hash "1bih4fcsma3ck47idn2ywa321zdj04bs1vsw2j96i2rwsd8xlbi4")))

(define-public crate-winrt-storage-sys-0.0.1 (crate (name "winrt-storage-sys") (vers "0.0.1") (hash "0bh3cby3b3dhks9a3n8nzqqkglynpxawb2s4brdmwg7gsivvd747") (rust-version "1.46")))

(define-public crate-winrt-storage-sys-0.0.2 (crate (name "winrt-storage-sys") (vers "0.0.2") (hash "13dn2vwkr1p3kvvx93ca1fz7v3nfpv0kfcmk6f7jcvalzbj3010c") (rust-version "1.46")))

(define-public crate-winrt-storage-sys-0.23 (crate (name "winrt-storage-sys") (vers "0.23.0") (hash "038vjsrvfp4sx07f7alkrlg5n2x5qg1j739ld5fpc4lbkj1x56cl")))

(define-public crate-winrt-system-0.0.1 (crate (name "winrt-system") (vers "0.0.1") (hash "0j5lnjmy2gpbn9cvak907wd5lz502r2in7nzsjp6yrcy6njjvzvk") (rust-version "1.46")))

(define-public crate-winrt-system-0.0.2 (crate (name "winrt-system") (vers "0.0.2") (hash "0lprcjfm2zrmr70s1qw51qy2y5as5i1rdqjc74jp2jrv160j07xv") (rust-version "1.46")))

(define-public crate-winrt-system-0.23 (crate (name "winrt-system") (vers "0.23.0") (hash "0wnl330pl7zjh2cgq5qlp2zkcjs12d29ykf4ix08kv4qcwmd9y95")))

(define-public crate-winrt-system-sys-0.0.1 (crate (name "winrt-system-sys") (vers "0.0.1") (hash "02av0f30zjlnvd9fnwb3nz2lkjdhfh94k9c7yn0gjyin9b786p0y") (rust-version "1.46")))

(define-public crate-winrt-system-sys-0.0.2 (crate (name "winrt-system-sys") (vers "0.0.2") (hash "0snl7zi51rzkxpgwx2yrw1xqm8b5lnzwmsl34krvv0fzc56akhkx") (rust-version "1.46")))

(define-public crate-winrt-system-sys-0.23 (crate (name "winrt-system-sys") (vers "0.23.0") (hash "14ss8x0v0wj1p6qldvks5r4bmf84rbr110f5r7vx5nwk9anavfvh")))

(define-public crate-winrt-toast-0.1 (crate (name "winrt-toast") (vers "0.1.0") (deps (list (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Data_Xml_Dom" "Foundation" "Globalization" "UI_Notifications" "Win32_System_Registry" "Win32_Storage_FileSystem" "Win32_Security" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "1ba2m76w4vb7biklk4i9x0c4jd58h087rx4wd4y3d1zdhc7vhiw9")))

(define-public crate-winrt-toast-0.1 (crate (name "winrt-toast") (vers "0.1.1") (deps (list (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Data_Xml_Dom" "Foundation" "Globalization" "UI_Notifications" "Win32_System_Registry" "Win32_Storage_FileSystem" "Win32_Security" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "1i1l0h462x5xqsjlwwc402241zl261kn05sf6d0sxghccg61iqd5")))

(define-public crate-winrt-ui-0.0.1 (crate (name "winrt-ui") (vers "0.0.1") (hash "1vjgpmwjv918zcniqs09356gwhn6p73992g5mxlh8g6fws5dkkbs") (rust-version "1.46")))

(define-public crate-winrt-ui-0.0.2 (crate (name "winrt-ui") (vers "0.0.2") (hash "19k67jnc6zi7lnxj8ba69xajzwvp1ql1lza1ygdrdb8wngxq5j5j") (rust-version "1.46")))

(define-public crate-winrt-ui-0.23 (crate (name "winrt-ui") (vers "0.23.0") (hash "19fyj62svcndc6573nr9zwg5kqpvg5gl32va7s2mmsq8ync70l1d")))

(define-public crate-winrt-ui-sys-0.0.1 (crate (name "winrt-ui-sys") (vers "0.0.1") (hash "0af9m9b93df72l6wcfi1w5y33d1w80lp55zslmd4bhw8ha1ypjx9") (rust-version "1.46")))

(define-public crate-winrt-ui-sys-0.0.2 (crate (name "winrt-ui-sys") (vers "0.0.2") (hash "0j5ck2y68yakrw4b9309c21nqc7fjvdahpa47p1ccsf3kwjibfjm") (rust-version "1.46")))

(define-public crate-winrt-ui-sys-0.23 (crate (name "winrt-ui-sys") (vers "0.23.0") (hash "16nxn3ciysbaqmzw206wd88kdglqsnqy2a6ydlcvzs7hj906z8k7")))

(define-public crate-winrt-web-0.0.1 (crate (name "winrt-web") (vers "0.0.1") (hash "1bbyx69wrn86q1jl1g07s3ll2wvxns74xk54v4b0i8ggz6mczxbg") (rust-version "1.46")))

(define-public crate-winrt-web-0.0.2 (crate (name "winrt-web") (vers "0.0.2") (hash "0v1llc3f4kz7p2adg785s885hwpvwq96j695rwjciwayn24bw0kb") (rust-version "1.46")))

(define-public crate-winrt-web-0.23 (crate (name "winrt-web") (vers "0.23.0") (hash "08hp8n743rg7hy82rsy8q7zaryg8a69kw5750awg69i7qqz9lff0")))

(define-public crate-winrt-web-sys-0.0.1 (crate (name "winrt-web-sys") (vers "0.0.1") (hash "1fdvb5w2x4gs453h9il6d05n8xr5s2k9hb7kpxbqwm8f33vv952m") (rust-version "1.46")))

(define-public crate-winrt-web-sys-0.0.2 (crate (name "winrt-web-sys") (vers "0.0.2") (hash "0vdvl0ns21gz9s9g496s116gr8zrkcifs9bq6jh8v34rwa6ab55s") (rust-version "1.46")))

(define-public crate-winrt-web-sys-0.23 (crate (name "winrt-web-sys") (vers "0.23.0") (hash "1igp0dnrsd0yr3zvjxsi55wr2p84cbfpvmnmbyavqgq6w8ighprj")))

(define-public crate-winrt_bluetooth_bindings-0.0.1 (crate (name "winrt_bluetooth_bindings") (vers "0.0.1") (deps (list (crate-dep (name "winrt") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1q9gzw6c226bmqmxr86pkb8ld96fzmzz37va8hdjcr5iar509p6r")))

(define-public crate-winrt_bluetooth_bindings-0.0.2 (crate (name "winrt_bluetooth_bindings") (vers "0.0.2") (deps (list (crate-dep (name "winrt") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "06axxfh1qhp8q68mr6g4r4j1zwvkw0c6cfmcp8iijs2340vdz8sc")))

(define-public crate-winrt_bluetooth_bindings-0.0.3 (crate (name "winrt_bluetooth_bindings") (vers "0.0.3") (deps (list (crate-dep (name "winrt") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "02w9j7plmx4wlkd1p68h1krvqz98bhs0irbfg9j1qin3yhb8j6mn")))

(define-public crate-winrt_bluetooth_bindings-0.0.4 (crate (name "winrt_bluetooth_bindings") (vers "0.0.4") (deps (list (crate-dep (name "winrt") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1z3yc7nkfaxkppymrjdy9fp93f5skb82zy09d1yk9531sk3vdc2s")))

(define-public crate-winrt_gen-0.7 (crate (name "winrt_gen") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_gen_macros") (req "^0.7") (default-features #t) (kind 0)))) (hash "12bsayi668pzz709vpd6c18skha0hdb1i6v3dzxq5bwkm6jdv8vp")))

(define-public crate-winrt_gen-0.7 (crate (name "winrt_gen") (vers "0.7.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_gen_macros") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "08ir8w2q2fsjivgan6289hc4vggwmq624sc6bkmjznfvnifsjfnb")))

(define-public crate-winrt_gen-0.7 (crate (name "winrt_gen") (vers "0.7.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_gen_macros") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1bmr7fnj6da916ka6qhh64i6i680fzivxmf3jyrprm1hbwnlk5zl")))

(define-public crate-winrt_gen_macros-0.7 (crate (name "winrt_gen_macros") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zwi1mxxcpyw9113dis7yw0hdzhh1qd1amnydyia0dbwx7cf5gkq")))

(define-public crate-winrt_gen_macros-0.7 (crate (name "winrt_gen_macros") (vers "0.7.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jyz80zpwfqjnhp5d0yg2b23v8hf081aska084idqxahc7i82ysy")))

(define-public crate-winrt_gen_macros-0.7 (crate (name "winrt_gen_macros") (vers "0.7.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n5pg0ndfxsqdq56lqrz7v6086dcmyvp4ckzkcymzwzhngmpikc5")))

(define-public crate-winrt_macros-0.7 (crate (name "winrt_macros") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_gen") (req "^0.7") (default-features #t) (kind 0)))) (hash "1a2pw9mym27ma156hyzz2kdxf3105h8az3zbkikbpl71pk3s0y7w")))

(define-public crate-winrt_macros-0.7 (crate (name "winrt_macros") (vers "0.7.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_gen") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "154mgv55cdl5978iqm4wb4jbn43j42y9wdl0p518p4rbyxxwckp5")))

(define-public crate-winrt_macros-0.7 (crate (name "winrt_macros") (vers "0.7.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winrt_gen") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0qnn90kv3m5v9yvq3kkk2jg7yn7qzs5ss36jll4lks7fg3w991fq")))

