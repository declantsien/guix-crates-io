(define-module (crates-io wi nm) #:use-module (crates-io))

(define-public crate-winmd-0.1 (crate (name "winmd") (vers "0.1.0") (hash "0wj5cxg9x24sh6pqaw0a2290js80pgrfz590x62slplq37mxns5f") (yanked #t)))

(define-public crate-winmd-0.2 (crate (name "winmd") (vers "0.2.0") (deps (list (crate-dep (name "winmd-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1kli4cvy4zl1cgfhdknjwq70k1hn0rhcdpcj7c2wnbx4ck892b01")))

(define-public crate-winmd-0.3 (crate (name "winmd") (vers "0.3.0") (deps (list (crate-dep (name "winmd-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "13vgdnn05j4w7zylp68hljm78szmppmh6ly2hiyprznk57083705")))

(define-public crate-winmd-0.4 (crate (name "winmd") (vers "0.4.0") (deps (list (crate-dep (name "winmd-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0nacnynbkcap572kycscxqjv4ivpm1b9j6qf8vg1bhvf4zmnx5pw")))

(define-public crate-winmd-0.5 (crate (name "winmd") (vers "0.5.0") (deps (list (crate-dep (name "winmd-macros") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1pzz2hvx6qv8q91445x89z8h5j4ngz88z083r4cdj9z8rxbcag26")))

(define-public crate-winmd-0.6 (crate (name "winmd") (vers "0.6.0") (deps (list (crate-dep (name "winmd-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "00qnw4wy872syb2a6l9i9yb4z7n82gs5cx5bqf4xmzh9pz8lvdqr")))

(define-public crate-winmd-macros-0.2 (crate (name "winmd-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sbk5pcq3330nihmhjxn8dfffds6lxfw48a70c4x5mccl4130sf2")))

(define-public crate-winmd-macros-0.3 (crate (name "winmd-macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h7nz6d43ph853abvs80pfqj4xzcv19xgq6r9h063f7xv0fzk11k")))

(define-public crate-winmd-macros-0.4 (crate (name "winmd-macros") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14bp0libir8a25x81nqckp4rs2bl6wzv1m6il55qg72arjcx8c0y")))

(define-public crate-winmd-macros-0.5 (crate (name "winmd-macros") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "134ysjz5bqhbkb2li2x2yvpbpw2zhmf5gmbknk61q53vm620dd5k")))

(define-public crate-winmd-macros-0.6 (crate (name "winmd-macros") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0x3rb165l39wzd5yb2bkqil55yfs0afw45cqnzsgklalhnc8iq24")))

(define-public crate-winmix-0.1 (crate (name "winmix") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.56.0") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_System_Com" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (default-features #t) (kind 0)) (crate-dep (name "windows-result") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ndz48nqjfq4vl959bi6s6mvp6h3bn6yaavqpm8yalhypagh7xjb")))

(define-public crate-winmix-0.1 (crate (name "winmix") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.56.0") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_System_Com" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (default-features #t) (kind 0)) (crate-dep (name "windows-result") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0zc1pd58mj1rryglc9aj7780h6ivhqc3yxbhqpm2nbrly4vk8i05")))

(define-public crate-winmix-0.1 (crate (name "winmix") (vers "0.1.2") (deps (list (crate-dep (name "windows") (req "^0.56.0") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_System_Com" "Win32_System_Threading" "Win32_System_ProcessStatus"))) (default-features #t) (kind 0)) (crate-dep (name "windows-result") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1bczz1mp1fvr6d9c2b9rals4g0hrlpfcy4vy59smhhb3l3k3a579")))

(define-public crate-winmm-sys-0.0.1 (crate (name "winmm-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "143kb47n94vhqrk9gzvy1v438l00kp2l6jw9yb4dainswndxwlbf")))

(define-public crate-winmm-sys-0.0.2 (crate (name "winmm-sys") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "08hy1bhx7hd4rlgq4jhxac805my78hy36dmq9ip102b8rx72z6p3")))

(define-public crate-winmm-sys-0.1 (crate (name "winmm-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0ns0w2ds2bkjaidgcs5d54kmhd9ki57cy69p5z3djccrmvygi6im")))

(define-public crate-winmm-sys-0.1 (crate (name "winmm-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "1ains62pflbn7gjfyhn27jxi7538lyv89r2gq20wij2ihpa1gm9p")))

(define-public crate-winmm-sys-0.2 (crate (name "winmm-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0db0hqkic11mc5a5parh7avw84xy6k1v8w7c3brlpjk3df0pm990")))

(define-public crate-winmsg-0.1 (crate (name "winmsg") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "02xnhm2ww6b554n4fqi0gi3q8m2b31yy38blqjac3ar8jidpkff7")))

(define-public crate-winmsg-0.1 (crate (name "winmsg") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (kind 0)))) (hash "0b5f79xhhl89k263cxp222qj7nr372nxxcqsc6ng3634hvfiac08")))

(define-public crate-winmsg-0.1 (crate (name "winmsg") (vers "0.1.2") (deps (list (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "1qdprpg26mjr1r7iklk0d996401b6850an4gm67qz9hazzkhh0hl")))

(define-public crate-winmtp-0.1 (crate (name "winmtp") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.44") (features (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "0pn137wkpz0lv1lfqqz52ph85c1k9w2953dxils27ww5m15v6yzb")))

(define-public crate-winmtp-0.2 (crate (name "winmtp") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.44") (features (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "15ks8v2ldsy6kz6ajyjaqpjjrawwin64w797czmrgbl31yr2i9v2")))

(define-public crate-winmtp-0.2 (crate (name "winmtp") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.44") (features (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "1ra155k90044qjdpxx39bd5v905fx3n2wwzvzc9yj89c9912dqmx")))

(define-public crate-winmtp-0.3 (crate (name "winmtp") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.52") (features (quote ("Win32_System_Com" "Win32_Devices_PortableDevices" "Win32_UI_Shell_PropertiesSystem" "Win32_Storage_FileSystem" "Win32_System_Com_StructuredStorage" "Win32_System_Variant" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "0i04bky13bwa7d78jjzayly84lv8gg736kj27kfa0l5ydl1nqmag")))

