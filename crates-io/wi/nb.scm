(define-module (crates-io wi nb) #:use-module (crates-io))

(define-public crate-winbluetooth-0.1 (crate (name "winbluetooth") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("impl-default" "guiddef" "handleapi" "processthreadsapi" "winbase" "winioctl" "winerror" "winnt" "winsock2" "ws2def"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1vskayad3y8qxmxjcw9lw5sik1ckyixcqvjs33nym4g1vpmbvsz7") (features (quote (("impl-default") ("impl-debug") ("debug" "impl-debug"))))))

(define-public crate-winbuild-0.0.0 (crate (name "winbuild") (vers "0.0.0") (hash "1nw6w7q4gmfzgq117i4v5v3gfj1vka01ygi4i3p8805yz9nilzcb") (rust-version "1.60")))

