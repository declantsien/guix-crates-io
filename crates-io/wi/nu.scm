(define-module (crates-io wi nu) #:use-module (crates-io))

(define-public crate-winui-0.0.0 (crate (name "winui") (vers "0.0.0") (hash "1cflfgi0w3cim2b269yxk5gfzmwqqlf3p7g3ibfqs38c8gg6ddcp")))

(define-public crate-winutil-0.1 (crate (name "winutil") (vers "0.1.0") (deps (list (crate-dep (name "advapi32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1igy4s5ahwywzm9nvm21wgxc1wh5cm3nyznkgvwaypwf0lfzi1ll") (yanked #t)))

(define-public crate-winutil-0.1 (crate (name "winutil") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wow64apiset" "processthreadsapi" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0vkyl3fbbf05n5ph5yz8sfaccrk9x3qsr25560w6w68ldf5i7bvx")))

(define-public crate-winutils-rs-0.1 (crate (name "winutils-rs") (vers "0.1.0") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "1w9g7jx5604l1vq812k3f7yzfvgc53r7g363b7p9xwmlaqpvk3w3") (yanked #t)))

(define-public crate-winutils-rs-0.1 (crate (name "winutils-rs") (vers "0.1.1") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "179i75fx06h3xnp264r9kh31a55c5d01cs0dnmfbh27x30d26kp7") (yanked #t)))

(define-public crate-winutils-rs-0.1 (crate (name "winutils-rs") (vers "0.1.2") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "1vf1mp4aryk8sl8hqm0x1srph39k5nz6a9hjwx8qq8p2gj61fjy1") (yanked #t)))

(define-public crate-winutils-rs-0.1 (crate (name "winutils-rs") (vers "0.1.3") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "1whncm3li5p9dcbz03vvxsx3x500qhjx8az8sw73f48aqykc0mfz") (yanked #t)))

(define-public crate-winutils-rs-0.1 (crate (name "winutils-rs") (vers "0.1.4") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "0b9pq13zkm330lhdnmn624b7kmivdccvysi3zxklnpkc02b64wwk") (yanked #t)))

(define-public crate-winutils-rs-0.1 (crate (name "winutils-rs") (vers "0.1.5") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "1q00cqybxq319vw6mr6y4jhdysg3dyj28q9h3nqmw6jgzkicycai") (yanked #t)))

(define-public crate-winutils-rs-0.1 (crate (name "winutils-rs") (vers "0.1.6") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "14l6hrn36a1mrw25dwsqjkh802ws7ihxlln6kw9jzkb7dz5pvv96")))

(define-public crate-winutils-rs-0.2 (crate (name "winutils-rs") (vers "0.2.0") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "1iyz5lk3qs4whiihv6hv30vwyn05ij0s2rz8xhgs90mii23nczsa")))

(define-public crate-winutils-rs-0.2 (crate (name "winutils-rs") (vers "0.2.1") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "15nbywir2w54a5aw0d39cg86q781mp7nxmi4c1dc30a1ggcf6a9d")))

(define-public crate-winutils-rs-0.2 (crate (name "winutils-rs") (vers "0.2.2") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("accctrl" "aclapi" "combaseapi" "errhandlingapi" "handleapi" "ioapiset" "rpc" "rpcdce" "sddl" "securitybaseapi" "synchapi" "winbase" "winerror" "winioctl"))) (default-features #t) (kind 0)))) (hash "0v597lr94sdbp0l2gqn2ch93l7wrqcaans12dzg33qzs0drg0arl")))

