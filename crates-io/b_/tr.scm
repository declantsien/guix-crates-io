(define-module (crates-io b_ tr) #:use-module (crates-io))

(define-public crate-b_trees-0.0.1 (crate (name "b_trees") (vers "0.0.1") (hash "0crmxkhg3pc9r65im2wzmgski4npzxl9lxhbrxgy8j8l4s44lbiq")))

(define-public crate-b_trees-0.0.11 (crate (name "b_trees") (vers "0.0.11") (hash "0vfpb5aw9pb62af8w9hhxwixdnmp76n8gx9zwwf0gxb9bqqlp9gi")))

(define-public crate-b_trees-0.0.12 (crate (name "b_trees") (vers "0.0.12") (hash "18xiiymszqiq7hh7ll68wfaz8wj7k2ldpmqylzh69717k9j27k2y")))

