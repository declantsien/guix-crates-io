(define-module (crates-io vl oc) #:use-module (crates-io))

(define-public crate-vlock-0.1 (crate (name "vlock") (vers "0.1.0") (hash "1b9grhbg47ia8xibkc7qjy1jshwpk3db5j9yqzw9ahgn2h315my9")))

(define-public crate-vlock-0.2 (crate (name "vlock") (vers "0.2.0") (hash "1znln1kwnyiqsbvv5a03bh5hizfvsrazld8rpqxjc3g4l01sgpd5")))

(define-public crate-vlock-0.2 (crate (name "vlock") (vers "0.2.1") (hash "047clxn1zgcwil85yclk2zci7apn7pg86risk1hwxisrf397ya5r")))

