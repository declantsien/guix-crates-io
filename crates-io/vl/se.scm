(define-module (crates-io vl se) #:use-module (crates-io))

(define-public crate-vlseqid-0.1 (crate (name "vlseqid") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)))) (hash "1nx2hd7ayjvkxrdlsg93d9kdlwc3biivwv7008fwi5l6wr8v4r36")))

(define-public crate-vlseqid-0.2 (crate (name "vlseqid") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)))) (hash "0v55jfkw3y78lxi8af79r9vyvj966xn4gzffmhn5fnvmz1jjr11c")))

(define-public crate-vlseqid-0.3 (crate (name "vlseqid") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)))) (hash "0zhi06lkqh7vs1r34wzds1vsl16bnl14xm3s3gq2h1gszmcqn3c8")))

(define-public crate-vlseqid-0.4 (crate (name "vlseqid") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)))) (hash "1rxz6klp7f7bqmix7f7wasgckbmk2gvyg8rk6fkf50w59i6fn93c")))

