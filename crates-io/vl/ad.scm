(define-module (crates-io vl ad) #:use-module (crates-io))

(define-public crate-vlados-0.0.1 (crate (name "vlados") (vers "0.0.1") (hash "1yzs4j6082yzxmaaz0464335p7s3jd4cwgwcfxssmfrxx4d3fvmp") (yanked #t)))

(define-public crate-vladyslav-art-0.1 (crate (name "vladyslav-art") (vers "0.1.0") (hash "1b368r9xg8sp63x8m8xrdrx0w8pkjp8wi15k8g5xz2l7bikylzrf") (yanked #t)))

