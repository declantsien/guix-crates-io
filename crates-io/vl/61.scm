(define-module (crates-io vl #{61}#) #:use-module (crates-io))

(define-public crate-vl6180x-0.1 (crate (name "vl6180x") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.2.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1zvgd56724i09xd35fx4409hkwph2j2rkndlm0yisaybh23csbz4")))

(define-public crate-vl6180x-0.1 (crate (name "vl6180x") (vers "0.1.3") (deps (list (crate-dep (name "cast") (req "^0.2.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0v4mz0fd6cj7z57v050a9mhvl1x3hzb60d3jw01n6yjdsrydvv08")))

