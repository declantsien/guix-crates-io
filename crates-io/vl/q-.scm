(define-module (crates-io vl q-) #:use-module (crates-io))

(define-public crate-vlq-bij-0.1 (crate (name "vlq-bij") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "truncate") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n56by9270i7szawvl0y3vndj6bzhrnmacp0jhn2xvacvgns2b62")))

(define-public crate-vlq-bij-0.2 (crate (name "vlq-bij") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1x2rijyj8rswhf0nrxl0q44x08kaxvi75g1zf2kmkd9gvv2r9ffc")))

(define-public crate-vlq-rust-0.1 (crate (name "vlq-rust") (vers "0.1.0") (hash "02fr6z1fl0l995lyavm0myj87nl5gsnbqd32zzidlkplrqkplnda")))

(define-public crate-vlq-rust-0.1 (crate (name "vlq-rust") (vers "0.1.1") (hash "18h46qka7yww7xzzwsq8idm4jfb06h2fpdc3h62d8y4wp5n6ad7l")))

(define-public crate-vlq-rust-0.2 (crate (name "vlq-rust") (vers "0.2.0") (hash "10r06dqvn53zmzfk3nj0kmw3d6z2b3jy4g0w6c8r82j0gjbj8bqw")))

(define-public crate-vlq-rust-0.3 (crate (name "vlq-rust") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1f0pzia8bh9gw1p9ljikwbb2yzavrwda5pdxikwydhxk9lq9zpwj")))

(define-public crate-vlq-rust-0.4 (crate (name "vlq-rust") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "16vvg08nkh7yil5gpzmc3fc4bh43wdichp2mss5lwslzd3wvk7q8")))

