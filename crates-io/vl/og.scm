(define-module (crates-io vl og) #:use-module (crates-io))

(define-public crate-vlog-0.1 (crate (name "vlog") (vers "0.1.0") (hash "1h5d40wibjvz549vcbb8af8aniax6al2q8ka18mkzxdm00s8mqlg")))

(define-public crate-vlog-0.1 (crate (name "vlog") (vers "0.1.1") (hash "0lzmgs5xjhnwv16db0jrini1nv1y8p9rzwn2khdhzzdqvrvmpy14")))

(define-public crate-vlog-0.1 (crate (name "vlog") (vers "0.1.2") (hash "102yffr31dip6ac4mmym7l7r7fr4qimms3p74wdpnq0cwqavya37")))

(define-public crate-vlog-0.1 (crate (name "vlog") (vers "0.1.3") (hash "1mizzqnd12kihygpph83fmqp0rxapqr7k6qsvfb4pc92p5ypff69")))

(define-public crate-vlog-0.1 (crate (name "vlog") (vers "0.1.4") (hash "0waxa9qzp428igbwkx11ig7rwqhm2wd7b6jcv9zv23y50jwzqri2")))

