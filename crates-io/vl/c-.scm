(define-module (crates-io vl c-) #:use-module (crates-io))

(define-public crate-vlc-exec-0.1 (crate (name "vlc-exec") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "winres") (req "^0.1.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0wzwkjpfiqkrcmh3p2cbkfkz6q5n7nhzikm0hfayylgs9q3044bl")))

(define-public crate-vlc-rc-0.1 (crate (name "vlc-rc") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1r5xxxmvkzxi1z64xpkdwjx9s39kk3a3g4dsixfsm5gd0pzbv8i6")))

(define-public crate-vlc-rc-0.1 (crate (name "vlc-rc") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0rq48z8q471ngysgnbck90fxlri6xs500jbk1s6wfy8w4ir9wrlx")))

(define-public crate-vlc-rs-0.1 (crate (name "vlc-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rfy1b1xa72s8dap6wcchgpyhwbm4ir7df59ajw60ya7jk8vfn82")))

(define-public crate-vlc-rs-0.2 (crate (name "vlc-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k1zrbsyb37cirykyy6751125nsyd832c76ggcla4x3b1vqds0fi")))

(define-public crate-vlc-rs-0.3 (crate (name "vlc-rs") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vm9b6n3f9wagyj3sng43pwkcwpk1nq1rzybb03px5xjlzab48v2")))

