(define-module (crates-io vl fe) #:use-module (crates-io))

(define-public crate-vlfeat-sys-0.1 (crate (name "vlfeat-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "~0.2") (default-features #t) (kind 0)))) (hash "1b75kbndnsq4c4jvlryjmq44dydxmq80pdwkfhg83bdnf0z2p9n9")))

