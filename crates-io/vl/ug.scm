(define-module (crates-io vl ug) #:use-module (crates-io))

(define-public crate-vlugger-0.1 (crate (name "vlugger") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)))) (hash "0xng0bzkxs3dq86lnhazhvp4zy37syf81pbkcx84izc7vnpwlkm1")))

(define-public crate-vlugger-0.2 (crate (name "vlugger") (vers "0.2.0") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)))) (hash "1cdxk20q2y78jvbk12y1l3zjxywbvygqw3xvs7gy87lam1c7ryqv")))

(define-public crate-vlugger-0.3 (crate (name "vlugger") (vers "0.3.0") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1p5252cx6nhh36w7qnh8vlyb5x3dgw4868a4z028xrmljb0bdj10")))

