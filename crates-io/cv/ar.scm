(define-module (crates-io cv ar) #:use-module (crates-io))

(define-public crate-cvar-0.1 (crate (name "cvar") (vers "0.1.0") (deps (list (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)))) (hash "1ir0ylw10zi7z8a899iy6hx567kzmp0idp9aj1b3rh8jzlfz4zak")))

(define-public crate-cvar-0.2 (crate (name "cvar") (vers "0.2.0") (hash "0gmiygbb5hin14qvgh4yamkivdqbjs0jyxyjvlnah0r13fpkdz7i")))

(define-public crate-cvar-0.2 (crate (name "cvar") (vers "0.2.1") (hash "06985a5bqnfnbzw2yw6p605ljm2y6ygvbd8p3s50irfcnzrri82k") (features (quote (("type_name")))) (yanked #t)))

(define-public crate-cvar-0.2 (crate (name "cvar") (vers "0.2.2") (hash "0893p7xcbw2xa04b4g7wswcy8474ndfpcllyshs7whsriz6f8vqv") (features (quote (("type_name"))))))

(define-public crate-cvar-0.3 (crate (name "cvar") (vers "0.3.0") (hash "1n0vxpl6aspg91gmcrcibcdvin2qxh4yprrflcxb1z41a3jy8i27") (features (quote (("type_name"))))))

(define-public crate-cvar-0.3 (crate (name "cvar") (vers "0.3.1") (hash "1lp6vqx2m511zzldw5y83x2nbkdsfadbf0b5m8624mfgx7wwv0bp") (features (quote (("type_name"))))))

(define-public crate-cvar-0.4 (crate (name "cvar") (vers "0.4.0") (hash "1igdwjh3ylg7s2bxfdzw68fhp57vixybqgz3y5ka334ld9jyr4mf") (features (quote (("type_name"))))))

(define-public crate-cvar-0.4 (crate (name "cvar") (vers "0.4.1") (hash "0ihwyak5a5x0d9kknr0k1j1jvsqdhwn3m1kz3p4n0cx7gvlxbhk3") (features (quote (("type_name"))))))

(define-public crate-cvars-0.0.0 (crate (name "cvars") (vers "0.0.0") (hash "0aqs263rf51dzp3gh41ck5apc3apwl24s291kjz2pphd5zs1cgv7")))

(define-public crate-cvars-0.1 (crate (name "cvars") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.21.0") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.21.1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0lvaga3wyb669ksp0ziah42sjl4x4k57dwmf524180qi3mjf0f0z")))

(define-public crate-cvars-0.2 (crate (name "cvars") (vers "0.2.0") (deps (list (crate-dep (name "cvars-macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "044x8xdpqyvads79inm9sqg6gx3j45xd83mn0dd6b1c878mblbdc")))

(define-public crate-cvars-0.3 (crate (name "cvars") (vers "0.3.0") (deps (list (crate-dep (name "cvars-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0idbmshkyn2rhyffdml6aqgnhh7fafri19j1x9ibv4apb7wlcpsm")))

(define-public crate-cvars-0.3 (crate (name "cvars") (vers "0.3.1") (deps (list (crate-dep (name "cvars-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1cva3brlb8cc3s755brsrgjszknv3w9cqaqjkfsniqvcn0qmwwqw")))

(define-public crate-cvars-0.3 (crate (name "cvars") (vers "0.3.2") (deps (list (crate-dep (name "cvars-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "16yx8z7px69w4apjfnxxkc9qkvinha6s83gdd4ladvp48kl4lhy7")))

(define-public crate-cvars-0.4 (crate (name "cvars") (vers "0.4.0") (deps (list (crate-dep (name "cvars-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.25.0") (default-features #t) (kind 2)))) (hash "0l1880wkpzngb61nghbl8a09jryhx20j3jrgln5mbymaminpaf7s")))

(define-public crate-cvars-0.4 (crate (name "cvars") (vers "0.4.2") (deps (list (crate-dep (name "cvars-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.25.0") (default-features #t) (kind 2)))) (hash "1yvyzcflwvi4g0l03vidf7ns9f4aks9lafh2x8j9ql8diasjiaq5")))

(define-public crate-cvars-console-0.1 (crate (name "cvars-console") (vers "0.1.0") (deps (list (crate-dep (name "cvars") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "10rhpacpfq87rx01yih6pvy9q58dcvkz63dzxl3qhbli35z83a3b")))

(define-public crate-cvars-console-0.2 (crate (name "cvars-console") (vers "0.2.0") (deps (list (crate-dep (name "cvars") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1acfjp25rhz23ihv3mq0q8slbkc4diivb94723sjbqraz7x7zc2f")))

(define-public crate-cvars-console-fyrox-0.1 (crate (name "cvars-console-fyrox") (vers "0.1.0") (deps (list (crate-dep (name "cvars") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fyrox-ui") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0x7rygnvdv45ld5vqbai3nipsbrhwfa842y1hk3fhxz7rw61xzz3")))

(define-public crate-cvars-console-fyrox-0.2 (crate (name "cvars-console-fyrox") (vers "0.2.0") (deps (list (crate-dep (name "cvars") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "fyrox-ui") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1y0fn7yngkd2v6kfy6vvsj7y2crd7iifs98h5k01d7cc2l9wb9xj")))

(define-public crate-cvars-console-fyrox-0.3 (crate (name "cvars-console-fyrox") (vers "0.3.0") (deps (list (crate-dep (name "cvars") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "fyrox-ui") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "07bmq485xcihnzm897w8yx36kc3rz2y66s6zkzcx3k3afagmspip")))

(define-public crate-cvars-console-fyrox-0.4 (crate (name "cvars-console-fyrox") (vers "0.4.0") (deps (list (crate-dep (name "cvars") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "fyrox-ui") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "1jr9177s088lvxhxdsw7gddfsvdzy23110a5wqlbgyqx0dx3b1mw")))

(define-public crate-cvars-console-fyrox-0.5 (crate (name "cvars-console-fyrox") (vers "0.5.0") (deps (list (crate-dep (name "cvars") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "fyrox-ui") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "1zqjrm4kbz5afdi1j22j9kzzm1r0138i3dk43cdg2xzbf417z2yi")))

(define-public crate-cvars-console-macroquad-0.1 (crate (name "cvars-console-macroquad") (vers "0.1.0") (deps (list (crate-dep (name "cvars") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "09gk23xsm9kq66llds1hf4pqvbfm3f83jrnal5aw56aasavri47f")))

(define-public crate-cvars-console-macroquad-0.2 (crate (name "cvars-console-macroquad") (vers "0.2.0") (deps (list (crate-dep (name "cvars") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "1gpj4yvm9ciz2x7jdv7cxgrqwq3sdax2vsi5lwj38d0qg9zssbgl")))

(define-public crate-cvars-console-macroquad-0.3 (crate (name "cvars-console-macroquad") (vers "0.3.0") (deps (list (crate-dep (name "cvars") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "cvars-console") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "macroquad") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ak1gdqhhfhgq8pgsc580zxgiq3igv2sjxfpsxl2i7if035bxgm1")))

(define-public crate-cvars-macros-0.1 (crate (name "cvars-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "10szckg2ih8drchl79qxv5l4y2kzj225s2pjjdygfz92diqmck5q")))

(define-public crate-cvars-macros-0.1 (crate (name "cvars-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0k630909r937kkpa2vmcmdn6cn7fr9flya199n47p986jqjxx6rv")))

(define-public crate-cvars-macros-0.2 (crate (name "cvars-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1dx2ndwx5mzwrardfc0r09zkn0xw446fzzciwjm7ak27h6j49p69")))

(define-public crate-cvars-macros-0.2 (crate (name "cvars-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "18rqap1zg6y0pxrndxfhd13nvxc2wjng9qsxyir8syxs1gq9lqsk")))

(define-public crate-cvars-macros-0.4 (crate (name "cvars-macros") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.16") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "188zr4dp8kxbilyb5l659v45qy383sigfm919z93clbf6r9i18ac")))

(define-public crate-cvars-macros-0.4 (crate (name "cvars-macros") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.16") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0z0rlrdhi1ihkpif8g6059jc1n9kai8phbmmgp822dwfx7pbrqdc")))

(define-public crate-cvars-macros-0.4 (crate (name "cvars-macros") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "strum_macros") (req "^0.25.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.16") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0b688cf3a58h8a64n2qv73vs8hwfbhwhqr9nqygvqgns56m4n69b")))

