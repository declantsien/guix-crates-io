(define-module (crates-io cv -d) #:use-module (crates-io))

(define-public crate-cv-decoder-0.1 (crate (name "cv-decoder") (vers "0.1.0") (deps (list (crate-dep (name "c_str_macro") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "ofps") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.62") (features (quote ("clang-runtime"))) (default-features #t) (kind 0)))) (hash "0q4azk6zvg11c2nwrd7dixkmn188vnw779h9dyvlqs9rcsflsq9l")))

