(define-module (crates-io cv ct) #:use-module (crates-io))

(define-public crate-cvctl-0.1 (crate (name "cvctl") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-core") (req "^0.2.15") (features (quote ("trans"))) (default-features #t) (kind 0)))) (hash "19rskly21qcn6v7vb7vs0rscndwmqczpbn1jy9x7bvvvq1gbj51v")))

