(define-module (crates-io cv i-) #:use-module (crates-io))

(define-public crate-cvi-rs-0.1 (crate (name "cvi-rs") (vers "0.1.0") (deps (list (crate-dep (name "ema-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1yz2j19icp0vyjzg6xgw28xcrgxifhnrmx38qzmc3is66504ga3q")))

