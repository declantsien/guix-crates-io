(define-module (crates-io cv -g) #:use-module (crates-io))

(define-public crate-cv-geom-0.1 (crate (name "cv-geom") (vers "0.1.0") (deps (list (crate-dep (name "cv-core") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rfi83i2iphrzmx1pnsqik60aa7vw7m6vsalfq698sx9f60z7bw9")))

(define-public crate-cv-geom-0.1 (crate (name "cv-geom") (vers "0.1.1") (deps (list (crate-dep (name "cv-core") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1x1qmp227d37wwwm4rpk9qnlrq1dq3q9h635xzv0k8dia9yj65qv")))

(define-public crate-cv-geom-0.2 (crate (name "cv-geom") (vers "0.2.0") (deps (list (crate-dep (name "cv-core") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0i79ifcw0d456ns5aapcvn9292slsa2646jfkysfc9d0wd8a56ni")))

(define-public crate-cv-geom-0.2 (crate (name "cv-geom") (vers "0.2.1") (deps (list (crate-dep (name "cv-core") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0krjfar1xili4fh8i4k53x7lxcba3cvhg1pg1nayx7gw6dxz22rm")))

(define-public crate-cv-geom-0.3 (crate (name "cv-geom") (vers "0.3.0") (deps (list (crate-dep (name "cv-core") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ihnfzqd4hlkffpkiip2s9ynln8pqjdk5q0xps1wd4ylj36bdjqd")))

(define-public crate-cv-geom-0.4 (crate (name "cv-geom") (vers "0.4.0") (deps (list (crate-dep (name "cv-core") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0rs1j8wdl60l9wnsawm330vv9h9c2jiishpp8s5q9950pnn268ia")))

(define-public crate-cv-geom-0.5 (crate (name "cv-geom") (vers "0.5.0") (deps (list (crate-dep (name "cv-core") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05f0q1fkgj7xzsxjxiba7h008x9hgnahdp8zv2i36779ay3v49h2")))

(define-public crate-cv-geom-0.5 (crate (name "cv-geom") (vers "0.5.1") (deps (list (crate-dep (name "cv-core") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0vhpmr3dyd8zwzb31bds06mnkj28b67hc7i8l8g8lv2rdphgap6i")))

(define-public crate-cv-geom-0.6 (crate (name "cv-geom") (vers "0.6.0") (deps (list (crate-dep (name "cv-core") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0an92q54qa82pqrv18fq61pr135hlr58287v0a6far9v83ifkphq")))

(define-public crate-cv-geom-0.7 (crate (name "cv-geom") (vers "0.7.0") (deps (list (crate-dep (name "cv-core") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04whcs95kh0dv11rp9pv9w7rq8m2rsfa77q1ibxam01lhwnybj8b")))

