(define-module (crates-io cv -b) #:use-module (crates-io))

(define-public crate-cv-bridge-0.1 (crate (name "cv-bridge") (vers "0.1.0") (hash "0xdnid835h9kf0p75kp0vmxln0qyf6xsg4zx3kqq5mj1yx0d9fpl") (yanked #t)))

(define-public crate-cv-bridge-0.1 (crate (name "cv-bridge") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.74.2") (default-features #t) (kind 0)) (crate-dep (name "rosrust_msg") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "17fhfswm4w513h4bawijg970xgyk7r6bzc2mk5pv8bfwz7268dzk") (yanked #t)))

(define-public crate-cv-bridge-0.2 (crate (name "cv-bridge") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.74.2") (default-features #t) (kind 0)) (crate-dep (name "rosrust_msg") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0v2zrwak78r1hhvdwgpsjamhind04p95x78ihjyavwgq1x5lax5i") (yanked #t)))

(define-public crate-cv-bridge-0.3 (crate (name "cv-bridge") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.74.2") (default-features #t) (kind 0)) (crate-dep (name "rosrust_msg") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "12zsm9mfa853y6wgvd67sgfg8z99ijvf881022g81yx9yzqwdh8v")))

(define-public crate-cv-bridge-0.3 (crate (name "cv-bridge") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.74.2") (default-features #t) (kind 0)) (crate-dep (name "rosrust_msg") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "08fdlp6fr8965c0lijvmwcswlbj5wk1djcyg9fr6r8clmm8710jp")))

(define-public crate-cv-bridge-0.3 (crate (name "cv-bridge") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.74.2") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9.10") (default-features #t) (kind 0)))) (hash "0mfqpr50dpih2gjklqbb4zld46l5cgml7hsp9g8y1z2z44f9iklj")))

(define-public crate-cv-bridge-0.3 (crate (name "cv-bridge") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.76.4") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9.10") (default-features #t) (kind 0)))) (hash "0yxqhbvi7fwjy6hnnpmdiaqyqwa49fmsa4m7fbkz738gzklyv9bd")))

