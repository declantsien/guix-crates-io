(define-module (crates-io cv t-) #:use-module (crates-io))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.0") (hash "0038z4nj6cyjg02znsn5rvx15pi3b56addxf47rrim7fadj2fq2n") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.1") (hash "01rj3plz1rcm8saiism635nhjd16w4wc1lv2akdlm9igz0r3vsnl") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.2") (hash "0mgrpp6cpqqlsiv3w7zvxxiln898wfca3a9qyzrq9k4v8b8r0x8j") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.3") (hash "0ybf5lsyvl8bn57angvf1rr41x8mcvdyrkym0blrxpliih4n02s0") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.4") (hash "19fqh0513fiah64z6rrc1g5m319q2xrgv0b56r0f49nnc36amq9m") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.5") (hash "0w5mvyklvx9ypa5v47k4wqkfx0hvjlrjkxs37naica9xlcphpw69") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.6") (hash "0pqp6g84jql97m0b5ky4r3jd0y00x4xhy9gs0cnindbfvk7y4329") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.7") (hash "1n15dx96dxa8ibqn79x3ynsi8vczdqdrj0davl5b8q86qhprz48z") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.8") (hash "1d3c8f7mrq96x66lydykwqic0kdkrwi9pxqn1b7v2i24hdi86hbg") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.9") (hash "1jj20nfp456sv6ijqxz06hc4fbg2iw5s9d1fawq3s1w93pcmc38r") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.10") (hash "11dq8jn65hddyzad1w8zvvszgxabln3x6nyj006jrny5a8n4wnyn") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.11") (hash "0sgw8p6bak808aicsx6crqdipwrmmdcms2cnafbml3f30lp5w5j6") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.12") (hash "0fjhn24b1gcrrjry1gry00wz869vizxczrqsj4jhyi0r7bfd804p") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.13") (hash "02z07zb8aw5340id0gjymnjk61xk0iapqjylzmknkmf7m4syaa2v") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.14") (hash "0a6mhab1ch0s30cl5wlk6j2lna1kxpd9p3asz4cd066gg3xnp8sg") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.15") (hash "00jgc599b4pk0zyvsm7imj5m58vk8vzlpvmr6h640pkz7gciqs05") (yanked #t)))

(define-public crate-cvt-trait-0.1 (crate (name "cvt-trait") (vers "0.1.16") (hash "1dyvq7xbbsnlqpfb0ldczja9wd8yf12k0d4yqk3b94f6iql3k76d") (yanked #t)))

(define-public crate-cvt-trait-1 (crate (name "cvt-trait") (vers "1.0.0") (hash "0zfa3nyf8937a0xvy7albnc9v2d61pr9rjly9yxvm2yjhfrrgbbf") (yanked #t)))

(define-public crate-cvt-trait-1 (crate (name "cvt-trait") (vers "1.0.1") (hash "0ccfzs8b1awhiwmlf01xlpvcknajxm79yshvcsznm53im3rr943a") (yanked #t)))

(define-public crate-cvt-trait-1 (crate (name "cvt-trait") (vers "1.1.0") (hash "0z0rs39bsk5wya0hkf4908fkn34clwzav90f5g633l2wzxs3rnfr") (yanked #t)))

(define-public crate-cvt-trait-1 (crate (name "cvt-trait") (vers "1.1.1") (hash "11pw9fkksg243xwvzyj7h1wfrd6rhhbbmp12mjxbdgls1lcfn7zb") (yanked #t)))

(define-public crate-cvt-trait-1 (crate (name "cvt-trait") (vers "1.1.2") (hash "0dcrplaz59cpr65is9a7mi45pjlxpm21mrwkcljnq42qx3v4m25z") (yanked #t)))

(define-public crate-cvt-trait-1 (crate (name "cvt-trait") (vers "1.2.0") (hash "0z1xlndnq0af320ikdxxrvrz82m5vy1pfvj7h6ayjacf3kwqf13b") (yanked #t)))

(define-public crate-cvt-utils-0.1 (crate (name "cvt-utils") (vers "0.1.0") (hash "1f0qirdb5xcyvn6ww2wxz7njxvk12hvi41s4k9z25g96vrm36h1c")))

(define-public crate-cvt-utils-0.1 (crate (name "cvt-utils") (vers "0.1.1") (hash "1f4mk7aq9qhim4rphi618q2f7dh4pi887aab8ydm77rwl71ls0bc")))

(define-public crate-cvt-utils-0.1 (crate (name "cvt-utils") (vers "0.1.2") (hash "1bnmm27hql0i2dvcnvcliiwvyv4dsihabp6z15jx7r3jr4az9376")))

