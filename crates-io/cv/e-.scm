(define-module (crates-io cv e-) #:use-module (crates-io))

(define-public crate-cve-rs-0.1 (crate (name "cve-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02mnzf8nd7iplnrrw3g63nnqsyqd65b67rnrnz0ylklzq6zpfdap")))

(define-public crate-cve-rs-0.2 (crate (name "cve-rs") (vers "0.2.0") (hash "0lsashjjv64gj9ihfp4989d5n0vgv9q689rlz63rkv0ma7lkkpk6")))

(define-public crate-cve-rs-0.3 (crate (name "cve-rs") (vers "0.3.0") (hash "1szp0isci8h2y99z9xni9gdnhvdy44abfnljhsgrrw6aa10nppyc")))

(define-public crate-cve-rs-0.4 (crate (name "cve-rs") (vers "0.4.0") (deps (list (crate-dep (name "oorandom") (req "^11.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "00x1c41assw2wd2l34f5qbncjxqfk24jq3a4ad59i0gzy3z6avvq") (features (quote (("give-up" "oorandom") ("default"))))))

(define-public crate-cve-rs-0.5 (crate (name "cve-rs") (vers "0.5.0") (deps (list (crate-dep (name "oorandom") (req "^11.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (optional #t) (default-features #t) (kind 0)))) (hash "107kmd5w7n6v5csy21lk0csyd5ik6czmzrnpznshx4q44n7j3x71") (features (quote (("give-up" "oorandom") ("download-more-ram" "reqwest") ("default"))))))

(define-public crate-cve-rs-0.6 (crate (name "cve-rs") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "oorandom") (req "^11.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.6") (optional #t) (kind 0)))) (hash "1q72c889g8sb19b4g4pkp8vdiy9cs22hzvfksh0zvm2l4j12x4hi") (features (quote (("give-up" "oorandom") ("download-more-ram" "ureq") ("default")))) (rust-version "1.69.0")))

