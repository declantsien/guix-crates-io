(define-module (crates-io cv -u) #:use-module (crates-io))

(define-public crate-cv-utils-0.0.1 (crate (name "cv-utils") (vers "0.0.1") (deps (list (crate-dep (name "lettre") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "199d8s3p2q3sx93465xp9n7r63ika68pz0jf646c4bmzna7g3z7f")))

(define-public crate-cv-utils-0.0.2 (crate (name "cv-utils") (vers "0.0.2") (deps (list (crate-dep (name "lettre") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0wkx5iyrwd9azh2v32hz9v3ll29ndr0rgxkqvarkj5g663sav3ql")))

(define-public crate-cv-utils-0.0.3 (crate (name "cv-utils") (vers "0.0.3") (deps (list (crate-dep (name "lettre") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0vqm7qwx7wdf4ra5p8mn53basmfqkl22k7qa2f67d2lbsqlcw9ny")))

