(define-module (crates-io cv da) #:use-module (crates-io))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.0") (hash "07b78fww5j5w88xpm63ssaswjlsg3xr4vigghvg693zfw6hqfdsk")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.1") (hash "09hqkdpm4khxbjh31hw5syicjr1xahnakjpnilp6gkclpcxmrq51")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.2") (hash "0bfbs7qn3r5xy474qdvs5z5ng2nda57izxvnhsbb2d0cjny7dnsy")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.3") (hash "1k0vgm5pv5zi86mjlz703wlg49fm0r5alpmz94pcy4fhlfdd4jrh")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.4") (hash "1f15h5f16qn27hs8c9ldxscm0rd8k7n7as6p9534jjbzk377cva7")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.5") (hash "11n6j46c4vz84vlw1g3l5mqr6824dixz7ibk1ky5xghfmfl6c0wq")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.6") (hash "08prx58qm4ssczs5sh87bn5xbg6rpr9y25h7gis3lgq9bibq034f")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.7") (hash "107pg4fwlvjyv3h5gc98w3flbagls67v2kbww29x0ky04b5bsjxf")))

(define-public crate-cvdate-1 (crate (name "cvdate") (vers "1.1.8") (hash "1kgblrr6074mf5bqqnd1pcn28acsl06qjchrlgds0sc7n0kzp9y3")))

