(define-module (crates-io cv es) #:use-module (crates-io))

(define-public crate-cvesearch-0.1 (crate (name "cvesearch") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "04q0nml5xawn7rvfn13in6s495r3bxldphf8fapfm29k1crrjkdd")))

(define-public crate-cvesearch-0.1 (crate (name "cvesearch") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "0l6lx8qg5r0yqph66822ik02hfqc39gq6ccizsgh0jm9nwqmkhhs")))

