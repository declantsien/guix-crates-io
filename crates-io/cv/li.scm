(define-module (crates-io cv li) #:use-module (crates-io))

(define-public crate-cvlib-0.1 (crate (name "cvlib") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1bg0h1h1gvwms9l81x3sz3iv3s2f8v6xcs9s257b0yinasr8aaln")))

(define-public crate-cvlib-0.1 (crate (name "cvlib") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1m6pdc984zs56kdpjkprwkh3iqfb51ay96ncbhrmgqk0c7biqm83")))

(define-public crate-cvlib-0.1 (crate (name "cvlib") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1s96lg32pm3q2vnpbwg01vb10439lp09plnn1w45h742mdmaa3bm")))

