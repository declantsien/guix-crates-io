(define-module (crates-io cv ss) #:use-module (crates-io))

(define-public crate-cvss-0.1 (crate (name "cvss") (vers "0.1.0") (hash "0n5mysxfxzm4mqrzqzb7zhpv6vgjsfqkkv0865n7mm80jipz6jdb") (features (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-0.2 (crate (name "cvss") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "19xvqivrplyylqjnnhf4nrg4bs65j9kvv37q7051qrvd0yjbswy5") (features (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-0.3 (crate (name "cvss") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ic1ppvgvz9bi0q3grmwi3hss5fb8b7fm2hmsv985gpjx30rg929") (features (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-1 (crate (name "cvss") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "078nwyhnd6agk7dm6hqxm6f2jr46sx5iq8c28ihwrsgimrcw09f4") (features (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-1 (crate (name "cvss") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0n4gvadiim38krhb00szz3jwg0n22miq08fqa8dk11fa5crdiv5j") (features (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-1 (crate (name "cvss") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0gxxzimyxwf6ka011n6cd296ax6qiwnx8n6mxzh2l55bpvd65642") (features (quote (("v3") ("default" "v3"))))))

(define-public crate-cvss-2 (crate (name "cvss") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "03q1nh4jy0cvgckji1jr1kz3j7gf2zg74240j8qi1qxhk7vs5iky") (features (quote (("v3") ("std") ("default" "std" "v3")))) (rust-version "1.56")))

(define-public crate-cvss_tools-0.1 (crate (name "cvss_tools") (vers "0.1.0") (hash "079lnc23049sppq7ga8yv6k5rbkwjw74p286lw189d67g7qsg00y")))

(define-public crate-cvss_tools-0.1 (crate (name "cvss_tools") (vers "0.1.1") (hash "1737aihg4bzjm0a9nlr25niiawpskr846ykks7sfvh37r8bqiayk")))

(define-public crate-cvss_tools-0.1 (crate (name "cvss_tools") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "108bi4rpj06h97p1gm8291blj95w4m8qrm64rp91zf2fi4vlgd1g")))

(define-public crate-cvss_tools-0.2 (crate (name "cvss_tools") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "01klcnwv1wwsilbcyji9g7pavayv1aqzdhlws5bria2bmn0krpy4")))

(define-public crate-cvss_tools-0.3 (crate (name "cvss_tools") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1fdrk78nizaxw1jq0dpsjmlxx5nbmqkzy5asv97lna47350qwg2g")))

(define-public crate-cvssrust-0.1 (crate (name "cvssrust") (vers "0.1.0") (hash "09iqp6ddrr9zwx5s0v0314jjkvqfm1gwb7jpnihks5x8n50y5x0z")))

(define-public crate-cvssrust-0.1 (crate (name "cvssrust") (vers "0.1.1") (hash "0l61z78cj61lbbj9wm8v4ihrkkafla8dcvp1hslhnwc6dadpc7cn")))

(define-public crate-cvssrust-1 (crate (name "cvssrust") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mhdld6z6xr9lkywk5cfd57947qrkrh8v76b6zpphj53189riwfp")))

(define-public crate-cvssrust-1 (crate (name "cvssrust") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bxfig0qifi8cycdl2l10vkbips4a9a9shbm2asm2ybhf4bb61kn")))

