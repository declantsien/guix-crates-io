(define-module (crates-io cv od) #:use-module (crates-io))

(define-public crate-cvode-wrap-0.1 (crate (name "cvode-wrap") (vers "0.1.0") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "sundials-sys") (req "^0.2.1") (features (quote ("cvodes"))) (kind 0)))) (hash "1812sbh5aclmhw51fp9yil53dl68p9rpzim24j7021d0bp6y5pyw")))

(define-public crate-cvode-wrap-0.1 (crate (name "cvode-wrap") (vers "0.1.1") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "sundials-sys") (req "^0.2.1") (features (quote ("cvodes"))) (kind 0)))) (hash "132w12d7qy7fkxpn14xrmjzxvzz0pyjrzdam15k4l1yfab0wccxg")))

(define-public crate-cvode-wrap-0.1 (crate (name "cvode-wrap") (vers "0.1.2") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "sundials-sys") (req "^0.2.3") (features (quote ("cvodes"))) (kind 0)))) (hash "019ndz04l8axk6v6l4hj7hylivp2n4xv7wbffi9ydjm8gy5sv3rb")))

(define-public crate-cvode-wrap-0.1 (crate (name "cvode-wrap") (vers "0.1.3") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "sundials-sys") (req "^0.2.3") (features (quote ("cvodes"))) (kind 0)))) (hash "027dpvhs8aczzaxsvwxrz2rw0j5prlf1ywxqzjw43hp00aw40dv6")))

