(define-module (crates-io cv #{2-}#) #:use-module (crates-io))

(define-public crate-cv2-crate-0.1 (crate (name "cv2-crate") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "sort") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0k11yh9s9cfp7dbip26bhrnnnn3nqcva0w746h95cl1knj4ir8hw")))

(define-public crate-cv2-crate-0.1 (crate (name "cv2-crate") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "sort") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "08g8kklkp42pb24xvv8abrkqgflx8c8f831jq8d5k3ymipk6245k")))

(define-public crate-cv2-crate-0.1 (crate (name "cv2-crate") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "sort") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m9x2g04pil4vb9gm5k0jg92zj3wc7dkgpq8ls0m2p6zr26x1ci1")))

(define-public crate-cv2-crate-0.1 (crate (name "cv2-crate") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "sort") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "05mwjl6apb0k1z1lh5ar3ng68ga1mlbbja8rpgzr6961fkrqrw9r")))

