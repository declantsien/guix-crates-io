(define-module (crates-io cv rd) #:use-module (crates-io))

(define-public crate-cvrdt-exposition-0.1 (crate (name "cvrdt-exposition") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.10.1") (default-features #t) (kind 2)))) (hash "1x622nik7ib1dymphgsrysknhw2wsssrrisl8d275yxssd7q3wwb")))

(define-public crate-cvrdt-exposition-0.1 (crate (name "cvrdt-exposition") (vers "0.1.1") (deps (list (crate-dep (name "proptest") (req "^0.10.1") (default-features #t) (kind 2)))) (hash "0wmhycy24skwq9cw4633s144na0ar6ljjnh4510vz6qhh3fsbsyc")))

(define-public crate-cvrdt-exposition-0.1 (crate (name "cvrdt-exposition") (vers "0.1.2") (deps (list (crate-dep (name "proptest") (req "^0.10.1") (default-features #t) (kind 2)))) (hash "18hz77lww1h9bbpl9j1153q8z066ij415lrf77vnldg0xkwa23ld")))

(define-public crate-cvrdt-exposition-0.1 (crate (name "cvrdt-exposition") (vers "0.1.3") (deps (list (crate-dep (name "proptest") (req "^0.10.1") (default-features #t) (kind 2)))) (hash "0bywwnw8r9dk8dkylc44fdyxjgcm2kcw7qq292bry30pfdkrbbag")))

(define-public crate-cvrdt-exposition-0.1 (crate (name "cvrdt-exposition") (vers "0.1.4") (deps (list (crate-dep (name "proptest") (req "^0.10.1") (default-features #t) (kind 2)))) (hash "0glnar8rghhisq137jjhpw8nz7x3jgbdpzcc4jrgbs5q4s6frm4g")))

(define-public crate-cvrdt-exposition-0.1 (crate (name "cvrdt-exposition") (vers "0.1.5") (deps (list (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "15nca72wq5x9yvssnhl8igji07zw85dl3yka27qpb12ch4pvqgr1")))

(define-public crate-cvrdt-exposition-0.1 (crate (name "cvrdt-exposition") (vers "0.1.6") (deps (list (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1jdcghdlb5lypnl1qdbfa8v61n2wh8hxczpsqsny5qa12z8lkfhq")))

(define-public crate-cvrdt-exposition-0.2 (crate (name "cvrdt-exposition") (vers "0.2.0") (deps (list (crate-dep (name "proptest") (req "^1.3.1") (default-features #t) (kind 2)))) (hash "1ccmqk28iw1yl2v8nnc4pma6b2liignsb9hqcgcbl1xy0il897jy")))

