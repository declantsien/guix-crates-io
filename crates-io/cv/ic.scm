(define-module (crates-io cv ic) #:use-module (crates-io))

(define-public crate-cvicenie_2-0.1 (crate (name "cvicenie_2") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "050ayy5i7q64xpbp04l7jsl248cih3rq4ivbvc3nc04w614mcys7")))

(define-public crate-cvicko2ak-0.1 (crate (name "cvicko2ak") (vers "0.1.0") (hash "1c5wz1c0f6zjl5ahbv9f9mkn843saypl3c4qf0nyv1avph9zdx8h")))

