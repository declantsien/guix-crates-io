(define-module (crates-io cv ec) #:use-module (crates-io))

(define-public crate-cvec-0.1 (crate (name "cvec") (vers "0.1.0") (deps (list (crate-dep (name "coption") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "self-rust-tokenize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "1rs08ncf69h18zp47myk2a86vk46d7cf822r2i4amha1zccls0sj") (features (quote (("self_rust_tokenize") ("default" "self_rust_tokenize"))))))

(define-public crate-cvec-0.1 (crate (name "cvec") (vers "0.1.1") (deps (list (crate-dep (name "coption") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "self-rust-tokenize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0627xgjqj3m3p6wvipj25pzxbsg3p8gnv7w54h525l33vsgprm6i") (features (quote (("self_rust_tokenize") ("default" "self_rust_tokenize"))))))

