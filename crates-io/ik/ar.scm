(define-module (crates-io ik ar) #:use-module (crates-io))

(define-public crate-ikari_hello_cargo-0.1 (crate (name "ikari_hello_cargo") (vers "0.1.0") (hash "0vy4bwfn8hfpih6xb2lxhwz8aymf786dqa4iw9k3ax56x6q605fa")))

(define-public crate-ikarusdef-0.1 (crate (name "ikarusdef") (vers "0.1.0") (deps (list (crate-dep (name "indoc") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "00ijkpmqgc0wkya0378d7vl7r13crzcks7p5mm6ajscs4h9lypri")))

(define-public crate-ikarusdef-0.1 (crate (name "ikarusdef") (vers "0.1.1") (deps (list (crate-dep (name "indoc") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0xp1prbbswx8yrgwi6sbmw00nmp3b08ch1gy08j1adfvvg3k4a3a")))

(define-public crate-ikarusdef-0.1 (crate (name "ikarusdef") (vers "0.1.2") (deps (list (crate-dep (name "indoc") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1kk4k2i6a4wgwwrjlnvg6mhc0dshrvw4vwvkb41fdj00xwcplws7")))

(define-public crate-ikarusdef-0.1 (crate (name "ikarusdef") (vers "0.1.3") (deps (list (crate-dep (name "indoc") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1bgaapk3x6szjdvcbh09g2sr4i51lg9bixazpgifib9g1in34nl6")))

(define-public crate-ikarusdef-0.1 (crate (name "ikarusdef") (vers "0.1.4") (deps (list (crate-dep (name "indoc") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "03q1f1c9nlxmdhsfc57pgip7lkzmmzr20m061zajbpzjnqc84sjp")))

