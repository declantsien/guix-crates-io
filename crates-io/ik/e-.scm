(define-module (crates-io ik e-) #:use-module (crates-io))

(define-public crate-ike-derive-0.0.0 (crate (name "ike-derive") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wp0x4n1bycz7anpb76xmcn1jsh2w8bbz17cb442g2kid02rfv62")))

