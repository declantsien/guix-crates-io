(define-module (crates-io ik al) #:use-module (crates-io))

(define-public crate-ikal-0.1 (crate (name "ikal") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock" "std"))) (kind 0)) (crate-dep (name "ikal-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1c0xfh22xdx3v7ql3gndb11kvy6icq9hk4rllhv4dzg39137py74")))

(define-public crate-ikal-0.2 (crate (name "ikal") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (features (quote ("clock" "std"))) (kind 0)) (crate-dep (name "ikal-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "similar-asserts") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1liwqy8sr4ws9agv6g63ll0lc1g51ayc4rzcajanvsgr8fwd5m6s")))

(define-public crate-ikal-derive-0.1 (crate (name "ikal-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1y2zyqlgf9cadcq8smlc2wq35irf4f66kqcbcask5nbjz254kb20")))

(define-public crate-ikal-derive-0.2 (crate (name "ikal-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0dmy65sl9g971lkzk2fn4nqkf6zdnkxk9pkrql9sdn072kchcprn")))

