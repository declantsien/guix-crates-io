(define-module (crates-io ik on) #:use-module (crates-io))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "0r37ix8nbbj709ivb7mi22k1n8c7s06amscbgmkzrqy9v125rrrr") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "1h4fijrz9s1nbgr6c7kins0hrc1nr933wz4i6vl501v3a1gym3sp") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "1i4hfj8as63dw9d22dncj6mvfqmmvcmlgqxqa9z9ysblb5q7ybcw") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.3") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "1h35yjyhnrcqw6xnzkxdfyj3g1njsxr99fy4lg75709abwnhhvsi") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.4") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "1gm8plk1ck2nh6kd0dh5s070c11cc175jhq6c8dpvb3njr0sych5") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.5") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "17qj4bqp9nrdxq7fvgn841nn6z2bc20890svg4illzgkdsl8zb8s") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.6") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "02vqywwn8702shnx6hv2s9spwp61ddif87y8hm7g170113756kdq") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.7") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "0y5zws4bfh7vjy74slnbyryrca51451bis1bxz9xkp805792xbih") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.8") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "0jz26wq1nbkn1bkcqranxiki9030x8hrmm4nn2yfiyyxhpjv75np") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.9") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "1r69hhhs2qjxbmp7hcvvwjfwmgala3ri91h1mhybvjjl9dmvw0id") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.10") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "10qj8pzwi14wh6i7r98h3np6a3b8qd615wc58b5cap9r9b9lghz3") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.11") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "1ahnnagbjg8yl8j6r4vxpf5yfn0dv2iygvvlz5vg9xcl1h8h5h8v") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.12") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "0w404s78rpwg51c17m6085qq79vn9f1pvzkvm3fsmyk1xq50p7if") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.13") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "14rgdkr6a2pfhsayrjyik2xhl34shalr7rd4n1vd8pkf3kc1ksdv") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.14") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "08wpn437533qjb0sqd98q0abx8h8ygcz7c9vsdm75bjppawyq1ya") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.15") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "18qpavaa120yps15mj1cwvgi1l4snm9j5gcq9ykm9dqkz261396h") (yanked #t)))

(define-public crate-ikon-0.1 (crate (name "ikon") (vers "0.1.0-beta.16") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.8.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)))) (hash "1mf46xpk3cafsjlki1ph3z6g51qgw6w70i1b9m51bw8k8kichms8") (yanked #t)))

