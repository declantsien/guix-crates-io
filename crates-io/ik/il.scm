(define-module (crates-io ik il) #:use-module (crates-io))

(define-public crate-ikill-1 (crate (name "ikill") (vers "1.0.0") (deps (list (crate-dep (name "heim") (req "^0.1.0-alpha.1") (features (quote ("process" "runtime-tokio"))) (kind 0)) (crate-dep (name "skim") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (features (quote ("full" "macros"))) (default-features #t) (kind 0)))) (hash "1pr07g9995c6khpd1xq16ibcckjpypjw5w4c8i2dj79bk9fmd4ni")))

(define-public crate-ikill-1 (crate (name "ikill") (vers "1.1.0") (deps (list (crate-dep (name "heim") (req "^0.1.0-alpha.1") (features (quote ("process" "runtime-tokio"))) (kind 0)) (crate-dep (name "skim") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (features (quote ("full" "macros"))) (default-features #t) (kind 0)))) (hash "13pp3w3wp20nl3hnjx2jr9pwgg1zv1sc97133qb0crng7jdi25sk")))

(define-public crate-ikill-1 (crate (name "ikill") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "heim") (req "^0.1.0-alpha.1") (features (quote ("process" "runtime-tokio"))) (kind 0)) (crate-dep (name "skim") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (features (quote ("full" "macros"))) (default-features #t) (kind 0)))) (hash "1nfx34bzccqy4bxq8vb97pvj95lcna9xv2nkarar1hgrzjsgla4i")))

(define-public crate-ikill-1 (crate (name "ikill") (vers "1.3.0") (deps (list (crate-dep (name "heim") (req "^0.1.0-rc.1") (features (quote ("process"))) (kind 0)) (crate-dep (name "skim") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 0)))) (hash "0pyzxq6r3nf04xn5rzlvv4rjqdaxbgacldr68s6nj28flx1jrrr9")))

(define-public crate-ikill-1 (crate (name "ikill") (vers "1.4.0") (deps (list (crate-dep (name "heim") (req "^0.1.0-rc.1") (features (quote ("process"))) (kind 0)) (crate-dep (name "skim") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 0)))) (hash "0pl6s7vrb68jdyx1sic8lprh6cip0kh4wkfy323cnqgdgg2c4s2z")))

(define-public crate-ikill-1 (crate (name "ikill") (vers "1.5.0") (deps (list (crate-dep (name "heim") (req "^0.1.0-rc.1") (features (quote ("process"))) (kind 0)) (crate-dep (name "skim") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 0)))) (hash "0nm8zpgqcbyp0026xnmgx8664rqd5zv01knisrd09bymq4bjqlnc")))

