(define-module (crates-io yi pp) #:use-module (crates-io))

(define-public crate-yippie-0.1 (crate (name "yippie") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "show-image") (req "^0.13") (features (quote ("image"))) (default-features #t) (kind 0)))) (hash "0rf3dx867ickkzfkphvp20sw4xabc5wpx3g3d4w4zz6hkfasbb5n")))

