(define-module (crates-io yi el) #:use-module (crates-io))

(define-public crate-yield-closures-0.1 (crate (name "yield-closures") (vers "0.1.0") (deps (list (crate-dep (name "waker-fn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "yield-closures-impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0j79vg35nk251lr6a4f6cy3w3bl9f4ir26pa32ac8wd2561c70kc")))

(define-public crate-yield-closures-impl-0.1 (crate (name "yield-closures-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1igbvkzn5kvw9f97d07pq450h38jrdzbb2fgrrnizqydr7skx8m8")))

(define-public crate-yield-iter-0.1 (crate (name "yield-iter") (vers "0.1.0") (hash "085vkpngfjf5fcn0h93w13c5dxs6jndcx8pz1srbf42sj52h9l2m")))

(define-public crate-yield-iter-0.2 (crate (name "yield-iter") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)))) (hash "1z6fy63wx31m01xm2i5jbwdjvqsr5j99572gcnlbpww64gyyrpsw")))

(define-public crate-yield-iter-0.2 (crate (name "yield-iter") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)))) (hash "16hzqypc7pgpvqaq5fll97k6qxjpxinavgs48pggb3r0yr6ix4nl")))

(define-public crate-yield-progress-0.1 (crate (name "yield-progress") (vers "0.1.0") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt" "sync" "macros"))) (kind 2)))) (hash "0a0nbik38r2da53kk7sfx71lyri8q9zvjrf8aiklgxz4wdihhj25") (rust-version "1.63.0")))

(define-public crate-yield-progress-0.1 (crate (name "yield-progress") (vers "0.1.1") (deps (list (crate-dep (name "instant") (req "^0.1.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt" "sync" "macros" "signal" "time"))) (kind 2)))) (hash "14yfhhsz0nd22j7v0izlbadcyz1mm58rpp2ph05wwi5s37zi1kd1") (features (quote (("sync") ("default" "sync")))) (yanked #t) (v 2) (features2 (quote (("log_hiccups" "dep:log" "dep:instant")))) (rust-version "1.63.0")))

(define-public crate-yield-progress-0.1 (crate (name "yield-progress") (vers "0.1.2") (deps (list (crate-dep (name "instant") (req "^0.1.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt" "sync" "macros" "signal" "time"))) (kind 2)))) (hash "0dxnbsasb3aqhi3iygpxz86vh5scvl41qz8z4ci6gvrjx1rk0kq0") (features (quote (("sync") ("default" "sync")))) (yanked #t) (v 2) (features2 (quote (("log_hiccups" "dep:log" "dep:instant")))) (rust-version "1.63.0")))

(define-public crate-yield-progress-0.1 (crate (name "yield-progress") (vers "0.1.3") (deps (list (crate-dep (name "instant") (req "^0.1.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt" "sync" "macros" "signal" "time"))) (kind 2)))) (hash "1vvmgblk8a7agriv5sx9narqgj5r0casydflsplvv025d58qj3hk") (features (quote (("sync") ("default" "sync")))) (v 2) (features2 (quote (("log_hiccups" "dep:log" "dep:instant")))) (rust-version "1.63.0")))

(define-public crate-yield-progress-0.1 (crate (name "yield-progress") (vers "0.1.4") (deps (list (crate-dep (name "instant") (req "^0.1.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt" "sync" "macros" "signal" "time"))) (kind 2)))) (hash "1kzn11yn8snx12q7lx4fkyw0myifq4gnapb88gma3spnmiishwcc") (features (quote (("sync") ("default" "sync")))) (v 2) (features2 (quote (("log_hiccups" "dep:log" "dep:instant")))) (rust-version "1.63.0")))

(define-public crate-yield-progress-0.1 (crate (name "yield-progress") (vers "0.1.5") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt" "sync" "macros" "signal" "time"))) (kind 2)) (crate-dep (name "web-time") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "1wl9wqhnfpqxhdfnmgd4aaiw6fdr3mbbgax6iq02vlnmw0ypsqa7") (features (quote (("sync") ("default" "sync")))) (v 2) (features2 (quote (("log_hiccups" "dep:log" "dep:web-time")))) (rust-version "1.63.0")))

(define-public crate-yield-progress-0.1 (crate (name "yield-progress") (vers "0.1.6") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt" "sync" "macros" "signal" "time"))) (kind 2)) (crate-dep (name "web-time") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1mm7ycbnd53p5rsg8gp8d5x5n0m3xs8w3sfv4w35a3fy3l3x91v7") (features (quote (("sync") ("default" "sync")))) (v 2) (features2 (quote (("log_hiccups" "dep:log" "dep:web-time")))) (rust-version "1.63.0")))

(define-public crate-yield-return-0.1 (crate (name "yield-return") (vers "0.1.0") (hash "049yfgsn82jp1fbbj325kx55ngq8rwz34rqfc8sl0wd592kcb5rf")))

(define-public crate-yielding-executor-0.10 (crate (name "yielding-executor") (vers "0.10.0") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.1.1") (features (quote ("sync" "rt"))) (default-features #t) (kind 2)))) (hash "0pahr38p5rmyjps8c4hf8c0q4a7g4vvr9nfp94iga20fxmqr9628") (features (quote (("default") ("debug"))))))

(define-public crate-YieldSimRust-0.1 (crate (name "YieldSimRust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "17sbnhyqqz3c4xkmz8izdj2an4cc8wfxpsqjy0inabancn2qwprh")))

(define-public crate-YieldSimRust-0.1 (crate (name "YieldSimRust") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1a5fzvqv7470dknycp36qgl9i94jrqljj40zfi21192y1wvvp6i4")))

(define-public crate-YieldSimRust-0.1 (crate (name "YieldSimRust") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "11a7gxqfqlq47755ws1q9bifmpp4rviajz9mfv7krp77sla4rfa8")))

