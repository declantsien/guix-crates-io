(define-module (crates-io id co) #:use-module (crates-io))

(define-public crate-idcontain-0.1 (crate (name "idcontain") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 2)))) (hash "0dziqlpb44gqc09wp1sbmkkml3vcshb9swd5yqjk72mq7ld78jfg")))

(define-public crate-idcontain-0.2 (crate (name "idcontain") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 2)))) (hash "0arwfzh7s46vw7786kdr89zq12qc9zx84vkj0li2afjlpk2b8hsf")))

(define-public crate-idcontain-0.3 (crate (name "idcontain") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 2)))) (hash "0iysbd0200s3h7pi5wv8jj43s2n70nl0fn2j1qcvw6iykcqi99b2")))

(define-public crate-idcontain-0.4 (crate (name "idcontain") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 2)))) (hash "193y2y91afmx6nkw1ng3dh4czm1dpysiskki598vsvmsnzcx2lsb")))

(define-public crate-idcontain-0.6 (crate (name "idcontain") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.5") (default-features #t) (kind 2)))) (hash "1vjka0gzckrlc5z4225b8j0i7m2rcb3l91878xnsd3f8za5hczkn")))

(define-public crate-idcontain-0.7 (crate (name "idcontain") (vers "0.7.1") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "1ad0qq28y089k0xjw9mns6inj4bx5x1b5mx5piz4mazn77k1h5dp")))

(define-public crate-idcontain-0.7 (crate (name "idcontain") (vers "0.7.2") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "05apaf5f5c6gvsgvaa74037hrd4qk2jby4083rdwmm8328a67cln")))

(define-public crate-idcontain-0.7 (crate (name "idcontain") (vers "0.7.3") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "10v5wnczcc2kvvbagzp4vqrc62qpdm63kz422ggdqj6ba3s6aadb")))

(define-public crate-idcontain-0.7 (crate (name "idcontain") (vers "0.7.4") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1cd6z78cq06a5znvvgd977i5vdfbsigw2as05fhlkjzg3gyz62dm")))

(define-public crate-idcontain-0.7 (crate (name "idcontain") (vers "0.7.5") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1m55cblvyd6kvd2bbn82c054q052zr2f0wdm3wsvaxrzf7c0f2ld")))

(define-public crate-idcontain-0.7 (crate (name "idcontain") (vers "0.7.6") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "19ymipymmbciw88z1zbsp9sq5dmm68k4v4dxzrcxfpih1pn8b1dc")))

