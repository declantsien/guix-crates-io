(define-module (crates-io id _t) #:use-module (crates-io))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.3") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "05rsb0kc52sqbbz8frkj6h23jlscpxfx39g1zqwr4wpmc2fas6hr")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.4") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1j7ah7j0rxrk01b3nyrr3pymkl0krwldis9nzqgp5g3izmj26fkb")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.5") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1jnvhi5q2q9b4mw9gm1h2w8wxjvzn5hyq1jf0pq7laa4v1z286dr")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.6") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0yzab8f9dsvhb74qv7g20gd9pdwjkhmj3206wk0yr8zrg576mpkr")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.7") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1zk2l6v9lsjazi5xf3a4ms9kynka24mvxnnbhr5mc9fmycz1i16y")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.8") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "00njmsyqsy0p4acgh3cxgsms206j139rdb9rda8g88ikw7767si6")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.9") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0r9dc5c65cykjg53r3wr89qji5v5qpagfkq11rz4gc4c3k4xmdlr")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.10") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1z0apzbv87fa6n92hflrrikkcayfd4gr95pn552ygn165400vksb")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.11") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0jl4xvxgmhc58l5a8rq92ygw96wy74qmjsll0l6gpdabfhgiysj1")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.12") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1kwwmjy3rdqv7a42w7pqf9kx4w1d5bsi0d8anf8hvaxans9339qa")))

(define-public crate-id_tree-0.2 (crate (name "id_tree") (vers "0.2.13") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0w1y2iyds4gb0yjrc6fyv5barhd21bfc0kgwcb4r07l1fg133bgm")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.0.0") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "028xg0nvd0b0s6zyxhmp48q528y3hm1s9g2yaxl6ybdf0mnhn6js")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.0.1") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1pq8nbcahn15qfdiw19gi9bw563nzbxyhys5x8ckcydw2wim0sfi")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.0.2") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1zbgw9hw87manyj0qrik2jx3kc1hzgxhhk1wmlaipd7pg3hkq8hr")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.1.0") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0qmgvjqsr17sb7978hpfy3jh49sddsaxkylpg5s3kvqys7d642i5")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.1.1") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "029850isd9lsqffmi6aihy824vh90c0si18sjs724wxmppc2jc3a")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.1.2") (deps (list (crate-dep (name "snowflake") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0w8z76cdjac5yi3af47lpd4pp3h2p2hzvz5s2s1nkvrxgbabzx2n")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.1.3") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1q81lv1wc8ymn4br7x7fiylmc0ydipglr6wj0n77pgdiljgvdz7c")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.2.0") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0zkkrkziwd4m6wf1lnyvdip40xdc03napy9vmlrzkbkzwf0rnpm3")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.3.0") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0vqldpslzvj295dknbjiy6jqm18556nj7sdm497aqx3i7575l9hc")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.4.0") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1ms9mli1ing2a27cbi3nn40x5vabb75bdwvgj9drrmmgmk0l0a0y")))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0mxrrarn5m2fws1z7iijkby81rj006h0ksd6ckz9p9bp5inivd8v") (features (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1sl7snkx6fjmczyq4a127nbdz17ih8pxn3qmb23nw6v9n3qcvbm8") (features (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "006x9ll56zg5xbzipx5srz7k0r3qkjq1gjb3mxk4gjzz87wbwcw2") (features (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

(define-public crate-id_tree-1 (crate (name "id_tree") (vers "1.8.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "06h7c55k9jnk1yac9fcyk7kqydklzcnlpv944rddx2xysn6xpndw") (features (quote (("serde_support" "serde" "serde_derive" "snowflake/serde_support"))))))

(define-public crate-id_tree_layout-1 (crate (name "id_tree_layout") (vers "1.0.0") (deps (list (crate-dep (name "id_tree") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml_writer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0j6bzjv4dj4l9snc6mwdxyq97ypx2nr26p31il5sx18m5bvdc9qj") (features (quote (("serde_support" "serde" "serde_derive" "serde_json" "id_tree/serde_support") ("default" "serde_support"))))))

(define-public crate-id_tree_layout-1 (crate (name "id_tree_layout") (vers "1.0.1") (deps (list (crate-dep (name "id_tree") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml_writer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0rwfdxi1v5h04y0yxxn3vimyd47llax8w1la49ggiwp2gimb1znl") (features (quote (("serde_support" "serde" "serde_derive" "serde_json" "id_tree/serde_support") ("default" "serde_support"))))))

(define-public crate-id_tree_layout-2 (crate (name "id_tree_layout") (vers "2.0.0") (deps (list (crate-dep (name "id_tree") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml_writer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "17j1c5dqin60bllqbxxk3d7bxwxa8vjw2jaqkc3yzb79l6l47h0p") (features (quote (("serde_support" "serde" "serde_derive" "serde_json" "id_tree/serde_support") ("default" "serde_support"))))))

(define-public crate-id_tree_layout-2 (crate (name "id_tree_layout") (vers "2.0.1") (deps (list (crate-dep (name "id_tree") (req ">=1.7.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">=1.0.115, <2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">=1.0.115, <2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req ">=1.0.57, <2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml_writer") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)))) (hash "0gkmpq5kfwjgqcnv0xjk8911z8yysg62afzfm17aqgiyyfcacghl") (features (quote (("serde_support" "serde" "serde_derive" "serde_json" "id_tree/serde_support") ("default" "serde_support"))))))

(define-public crate-id_tree_layout-2 (crate (name "id_tree_layout") (vers "2.0.2") (deps (list (crate-dep (name "id_tree") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml_writer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1pli2byqc03f72lksvsf7313178yl2s45rw6j47ag6aq4yk06p9k") (features (quote (("serde_support" "serde" "serde_derive" "serde_json" "id_tree/serde_support") ("default" "serde_support"))))))

(define-public crate-id_tree_layout-2 (crate (name "id_tree_layout") (vers "2.0.3") (deps (list (crate-dep (name "id_tree") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml_writer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0fvp9bzwzw9xganxlcx6wan8xpvsg3qaffq2mryf4sghgapaixka") (features (quote (("serde_support" "serde" "serde_derive" "serde_json" "id_tree/serde_support") ("default" "serde_support"))))))

(define-public crate-id_tree_layout-2 (crate (name "id_tree_layout") (vers "2.0.4") (deps (list (crate-dep (name "id_tree") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml_writer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1qyx6zvpmr76f8jskbdvd75ij5a958qj445y09xra5fpq4dlqbql") (features (quote (("serde_support" "serde" "serde_derive" "serde_json" "id_tree/serde_support") ("default" "serde_support"))))))

