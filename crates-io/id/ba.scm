(define-module (crates-io id ba) #:use-module (crates-io))

(define-public crate-idbag-0.1 (crate (name "idbag") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0v7l9fnjx7v8im4fsr9m37jxhli9d11il1qv7d0irqd6hhrvxf6r") (features (quote (("dev-docs")))) (rust-version "1.56")))

(define-public crate-idbag-0.1 (crate (name "idbag") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0zq81qik8rz23np013j1gf1z4xybh7sd7hcxwclbyrw4xkpj7ri5") (features (quote (("dev-docs")))) (rust-version "1.56")))

(define-public crate-idbag-0.1 (crate (name "idbag") (vers "0.1.2") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0x33dw1fglbrjz6la3r68yw0jl4pwfispqlkl4zcdyiy0rk8b3kr") (rust-version "1.56")))

