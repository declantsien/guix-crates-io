(define-module (crates-io id #{-i}#) #:use-module (crates-io))

(define-public crate-id-ish-0.0.1 (crate (name "id-ish") (vers "0.0.1") (deps (list (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0fmlxi9vm9y27bjkfhl1vfwp3lyy1iznm35d480dga0wg4advpgc") (features (quote (("u128") ("i128"))))))

(define-public crate-id-ish-0.0.2 (crate (name "id-ish") (vers "0.0.2") (deps (list (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1c0608za14qq0gc414akiqa55220875zpwqqfb8ys7328v8jil7g") (features (quote (("with-take-id") ("u128") ("i128") ("default" "i128" "u128" "with-take-id"))))))

(define-public crate-id-ish-0.0.3 (crate (name "id-ish") (vers "0.0.3") (deps (list (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "02q4yrpm914lfb27xcwl0mvp60ppdxnpdqwa9jfw7aq73fimrqyc") (features (quote (("with-take-id") ("u128") ("default" "u128" "with-take-id"))))))

