(define-module (crates-io id -p) #:use-module (crates-io))

(define-public crate-id-pool-0.1 (crate (name "id-pool") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.114") (optional #t) (default-features #t) (kind 0)))) (hash "120isjz3i3wcm27ibzd148y3r4dqyvzig1sb8bx2aviqqk71x83n") (features (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

(define-public crate-id-pool-0.2 (crate (name "id-pool") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.117") (optional #t) (default-features #t) (kind 0)))) (hash "0qvnc40jdjb84jgbgpl412yhmcprvxr8cskgvq0ahibpn71pmda2") (features (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

(define-public crate-id-pool-0.2 (crate (name "id-pool") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.117") (optional #t) (default-features #t) (kind 0)))) (hash "1c4wlzqq7r6j9d3c2isjikxbyn3dbr5670bpyvimkhq060p86csy") (features (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

(define-public crate-id-pool-0.2 (crate (name "id-pool") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.138") (optional #t) (default-features #t) (kind 0)))) (hash "0i5kzpcgifwfbgwxnfp1y234ap0jcwzkbq529bp2323ni96xzl58") (features (quote (("usize") ("u64") ("u32") ("u16") ("default" "usize"))))))

