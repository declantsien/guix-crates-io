(define-module (crates-io id -g) #:use-module (crates-io))

(define-public crate-id-generator-0.1 (crate (name "id-generator") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "100ih8bjx73dm5xy9abrk891nivk4yfk542241qqmpkifjwfkza7")))

(define-public crate-id-generator-0.2 (crate (name "id-generator") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "1q4kanb9q2c4kam1vrvy0lwx7zimzjidxcd68jqnx2ylw1xidib3")))

(define-public crate-id-generator-0.3 (crate (name "id-generator") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1ngb9qxdrwrpp5f38s6bdzmsva5m69441yyl0ypd2ivah99sqnma")))

