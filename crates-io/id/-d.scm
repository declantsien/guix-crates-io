(define-module (crates-io id -d) #:use-module (crates-io))

(define-public crate-id-derive-0.1 (crate (name "id-derive") (vers "0.1.0") (deps (list (crate-dep (name "macrotest") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "1131wwrckv9qf01xix2np5l8kpwyl958kgs007gc2zpcaz66lhvi")))

