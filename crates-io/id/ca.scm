(define-module (crates-io id ca) #:use-module (crates-io))

(define-public crate-idcard-0.1 (crate (name "idcard") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "13s4hymqvfs3dkkynwxi5m8pjbrq3gk23wjqhg00hzw87y97fdwn")))

(define-public crate-idcard-0.1 (crate (name "idcard") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "044xsfwlq7ngd5cvj4jc1f42gk21rg3x5nqnnw8r0rhiy5lyv90v")))

(define-public crate-idcard-0.1 (crate (name "idcard") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "15096kmi1aw63xkavhlbc9ss9hihj0rrq45iljvy1m09pp4dzqsl")))

(define-public crate-idcard-0.1 (crate (name "idcard") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0apbjb8gsrslbw1r5wg4vyb6qn5cckdw6naksqbnhakvdwb9wgfp")))

(define-public crate-idcard-0.2 (crate (name "idcard") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0f3d3v901yil3hqp2pwh8m4x6g1fdbxn59xf0pzz2gg173pz45by")))

(define-public crate-idcard-0.3 (crate (name "idcard") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "08simx1fnv1p96rkh0659yqdcdrkl05qchdadaqpdrl4c8xiaa94")))

(define-public crate-idcard-cn-0.0.1 (crate (name "idcard-cn") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gb2260") (req "^0.1") (default-features #t) (kind 0)))) (hash "12f32bypj861nxjcaz5xx24fm2y9gdw7xccic851y1zzd939ik31")))

