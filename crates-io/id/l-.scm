(define-module (crates-io id l-) #:use-module (crates-io))

(define-public crate-idl-gen-0.1 (crate (name "idl-gen") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k6n31gyhf47gv5y8mm8cdal2qsnfwqm7w14r31v3xbvlnimz7yd")))

(define-public crate-idl-macro-0.1 (crate (name "idl-macro") (vers "0.1.0") (deps (list (crate-dep (name "anchor-syn") (req "^0.24.2") (features (quote ("idl"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "000ikks2crzzvbfckazn7ip2nzwvaianxahh9kvb5mcmz7ayk7p6")))

(define-public crate-idl-parser-0.1 (crate (name "idl-parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "07p9dl25z78lpicg1zk3bn1pr5f91wh05c262g17m4yiqlydddlv")))

