(define-module (crates-io id -c) #:use-module (crates-io))

(define-public crate-id-card-0.1 (crate (name "id-card") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0fz2307wwkkj03xjy7i89hagli2bxhya7brsnq6f92dkvfhz9pzq")))

(define-public crate-id-card-0.1 (crate (name "id-card") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1lyn5wj2163z42p626nz58k0wspc28jfnb0qljxh0m4mm9lwszng")))

(define-public crate-id-card-0.1 (crate (name "id-card") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ydjnjc1g9060x3kl20hzs2r8h54daqs41xx74c23lvxpm0l7b9i")))

(define-public crate-id-card-0.1 (crate (name "id-card") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "05d08xh052iq7wvnm089q62vw4hg1agb9ndpdlsqpk84zyd5dxma")))

