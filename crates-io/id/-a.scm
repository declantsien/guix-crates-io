(define-module (crates-io id -a) #:use-module (crates-io))

(define-public crate-id-arena-1 (crate (name "id-arena") (vers "1.0.0") (hash "0rjbwv66dxi87gdd5sbxjvx4wfcm0amrz9lzfasf338ms1nykm7r") (features (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-1 (crate (name "id-arena") (vers "1.0.1") (hash "01v1rxdbs5vqij3bk2zjj2rb35ip9dib7gzz2sixzb3bv7frw9k2") (features (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-1 (crate (name "id-arena") (vers "1.0.2") (hash "0kgb6145as6jf6j43giiczs7n1sb227nlrprb3fddl5knc8fwab8") (features (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2 (crate (name "id-arena") (vers "2.0.0") (hash "0gl48a2p55yjppi9m37kck8c3bk4d9hw5lpc3imf9zpa7w1m0wis") (features (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2 (crate (name "id-arena") (vers "2.0.1") (hash "1i168jw9fix471y751ksni63fs5qwx5xssa20snamc9prc5p6xzp") (features (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2 (crate (name "id-arena") (vers "2.1.0") (deps (list (crate-dep (name "rayon") (req "^1.0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1qvsbv08wlrmw2ksix2q5ff9776lnk0kj95mgjw4a6qljgmanpqq") (features (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2 (crate (name "id-arena") (vers "2.2.0") (deps (list (crate-dep (name "rayon") (req "^1.0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0vnvjf7221bj1g895fb210npjl5vxnxzqx6wri59l4q4y2sqj3gh") (features (quote (("std") ("default" "std"))))))

(define-public crate-id-arena-2 (crate (name "id-arena") (vers "2.2.1") (deps (list (crate-dep (name "rayon") (req "^1.0.3") (optional #t) (default-features #t) (kind 0)))) (hash "01ch8jhpgnih8sawqs44fqsqpc7bzwgy0xpi6j0f4j0i5mkvr8i5") (features (quote (("std") ("default" "std"))))))

