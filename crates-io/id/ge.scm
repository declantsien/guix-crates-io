(define-module (crates-io id ge) #:use-module (crates-io))

(define-public crate-idgen-0.1 (crate (name "idgen") (vers "0.1.1") (hash "00hq3mgwimclxmgmmkfajpx5fk4cffkngyf8crqsgqbdy0lpn3v2")))

(define-public crate-idgen-0.1 (crate (name "idgen") (vers "0.1.2") (hash "0jv0ibjbnpandgdbb7rnaibm8llfvm0fwynycbqgb5n7iaw2drfg")))

(define-public crate-idgend-0.1 (crate (name "idgend") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.13.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0i1clmgkixhsf6y19c2nvyps318gi7cl6q5yv07i9fah4mkd48j2")))

(define-public crate-idgenerator-1 (crate (name "idgenerator") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0mxhi55g62vmcpsjnqc0dhm3cvc0lxm0xsqpjnh9i62bnnsx679a")))

(define-public crate-idgenerator-1 (crate (name "idgenerator") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1p1f9wazrz87nl8z2ya4pl66xnny23gfrslwja73w5szcgkzdfxh")))

(define-public crate-idgenerator-1 (crate (name "idgenerator") (vers "1.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0mlgv6s5c42z9gjvpc2myvb26j2jims0g3vsr7l697xsdjkynl7d")))

(define-public crate-idgenerator-1 (crate (name "idgenerator") (vers "1.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0r1aiwkzyml6rri8avidcqzr70ag7djpwscshannn5jcx9ma9cym")))

(define-public crate-idgenerator-2 (crate (name "idgenerator") (vers "2.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sjvvmdvzmb4dhbdxsz1dg3jdbki773dlm9hg1gpp247w9l2zcwa")))

(define-public crate-idgenerator-thin-0.1 (crate (name "idgenerator-thin") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "179vknnf6nbp1gim6vdv660spgn27k0r4iygliqdajf6d4j2w5zy")))

(define-public crate-idgenerator-thin-0.1 (crate (name "idgenerator-thin") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kb3j93xdb0jjv0w002vdjfl0rmwb30572dall6avld9jwxi7k4h")))

(define-public crate-idgenerator-thin-0.1 (crate (name "idgenerator-thin") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "17hcl9m9q71vfn3sccfnlg3vqwq877psvr823l9gknibaxq4nag2")))

(define-public crate-idgenerator-thin-0.2 (crate (name "idgenerator-thin") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "16ngk05d7b3jipw1qjxmrys88s64qsw8s6x9lxzibk129lc8j8n0")))

(define-public crate-idgenerator-thin-0.2 (crate (name "idgenerator-thin") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0") (default-features #t) (kind 0)))) (hash "066751q67zzidkrqwfj29lssm1xvm4yw1afq7clgy24rv4j9j5ga")))

