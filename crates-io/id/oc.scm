(define-module (crates-io id oc) #:use-module (crates-io))

(define-public crate-idocker-0.1 (crate (name "idocker") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "shiplift") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0157xqzfhrr9ci7yigdh3sdddbx9nxi7wbh66ky7wi00fpx8hn7g")))

