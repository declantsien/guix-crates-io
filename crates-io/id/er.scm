(define-module (crates-io id er) #:use-module (crates-io))

(define-public crate-iderive-0.1 (crate (name "iderive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "1x194zhkq6dgyqx70wxz5mmfgkksk8nqg9hp7wn76kkjw0spfz9q")))

(define-public crate-iderive-1 (crate (name "iderive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "0ld8c3mbyjjy9799xxj7qsxywh534n0bq4dfjga420zkln42rsqi")))

(define-public crate-iderive-1 (crate (name "iderive") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "149nkc3fybrablna594kifqn5wxw9xi5102mrh65lk1l7jdylaim")))

(define-public crate-iderive-1 (crate (name "iderive") (vers "1.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "14jdwsswn48ad8rfllaz9f40klrhf19bhpqqp4gk0parlk0yp6mm")))

(define-public crate-iderive-1 (crate (name "iderive") (vers "1.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "0hp62268qyw1ps588mamnm7n76fh7cz5bj3yrn0i9in4r3hnnih6")))

