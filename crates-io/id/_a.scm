(define-module (crates-io id _a) #:use-module (crates-io))

(define-public crate-id_allocator-0.1 (crate (name "id_allocator") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "17jmidpzwr2167njiipgjhkknp52zh4b773f6j7jch45qa885a6p") (v 2) (features2 (quote (("serde" "dep:serde"))))))

