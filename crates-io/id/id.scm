(define-module (crates-io id id) #:use-module (crates-io))

(define-public crate-idid-0.1 (crate (name "idid") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rev_lines") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 0)))) (hash "0p59agn8b53jk7qiw1l9n7z6vdfgzkhx72aaapkvfddp8p00jnr1")))

