(define-module (crates-io id l_) #:use-module (crates-io))

(define-public crate-idl_builder-0.1 (crate (name "idl_builder") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "0lknrd82fznjjika2ccwwgpv73d4s8gf9fz9vgx8cazkrxbwwza0")))

(define-public crate-idl_builder-0.1 (crate (name "idl_builder") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "1afn55sj9cqv5v64hy395kjxhy44y0n57bw495ygvf94gdgbc1lr")))

(define-public crate-idl_builder-0.1 (crate (name "idl_builder") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "1bv5lpsf0c5hv50p775dx6kdwkq5wx4x3pqagb47zxd9xjwl38wr")))

(define-public crate-idl_builder-0.1 (crate (name "idl_builder") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "1isw236za9c47pcmr34jv7c4pbrdd6yhpq2c75ih0krj4znw00mf")))

(define-public crate-idl_builder-0.1 (crate (name "idl_builder") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "0l2sa74cfgb1xc2kl6n5v1fdymdazypkzg5d5c9m18dsscf7vfrw")))

(define-public crate-idl_builder-0.1 (crate (name "idl_builder") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "0xb6mh5bgnbh6izp0syr93k5dgivfnz7cfnc8l1g21sqynji0f8p")))

(define-public crate-idl_builder-0.1 (crate (name "idl_builder") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "0b0gp6hpci7jxazz1a5gjwk228zf04jswa8cd3j1b65y7hv6v7rx")))

(define-public crate-idl_internal-0.1 (crate (name "idl_internal") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wk9c7qpb1j1wv44azn6z7v9qhaprpyv8rmcvkn04bp4h7qk4xrs") (yanked #t)))

