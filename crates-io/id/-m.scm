(define-module (crates-io id -m) #:use-module (crates-io))

(define-public crate-id-manager-0.0.1 (crate (name "id-manager") (vers "0.0.1") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)) (crate-dep (name "random-manager") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "ripemd") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0bxn786875f2b8ichv2svdxyrnqw1gsrf73nhj1j5i3sa6dsrxpl") (rust-version "1.62")))

(define-public crate-id-manager-0.0.2 (crate (name "id-manager") (vers "0.0.2") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)) (crate-dep (name "random-manager") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "ripemd") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0c76v99wiwzq48crldn0pigzxilxvnsqjlzyw3327v966lh0ijsv") (rust-version "1.66")))

(define-public crate-id-manager-0.0.3 (crate (name "id-manager") (vers "0.0.3") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)) (crate-dep (name "random-manager") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "ripemd") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1581gi519fqnsg2fxnql0lkvqn0bzdisiilgbv97qhqafqg1baxx") (rust-version "1.67")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.0") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "12vz99q3ay3nllsmklchd1g021xsbpqn5bjqljz35h1yx88fnc70")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.1") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0l0mm9vp0pky7b3azbbkwwvgvfqclxjpd1wn8aqvq3z0yrawsm5z")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.2") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0dbkbygnxbdilih3824x8lxkbnqw73da5lm98gbr92spxw8ddqky")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.3") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1x1i1zzcrx292vzccwmvwixb4gxdndvm1ba7807kvy0y6sb7j8iw")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.4") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "12fsxbrfhcz243njh40hbqaikzqndc08bqccymzanz6xh5rscz36")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.5") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ckfd74373i7wmzakq81i6j7pgq3ny6hzkjs4nywq2b6l102qnwg")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.6") (deps (list (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1m5d1ingwl1ry18jdabxd05vdh8wl36xjy07ar2z9z9635jm4ipv")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.8") (deps (list (crate-dep (name "id-set") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0isl10zqgm551xyvasp257w3zn2f78gqzi3pshzwknymcgqq1zk4")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.9") (deps (list (crate-dep (name "id-set") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1n4p80sw6rrym2a1frs1yd60hlb74lkpddq7dxby3y13cifcvriy")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.10") (deps (list (crate-dep (name "id-set") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "03a2c3ljf5rcyj599fdhw3dpkfsib4s7g920i7x9b8xbq5sk1kpp")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.11") (deps (list (crate-dep (name "id-set") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "13mwp91n9s37x03cy1zjncji3skisbmiyyxj122ikajjwxzb7wdq")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.12") (deps (list (crate-dep (name "id-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1h392vhfkjnrz7zbzxbpwazisiqm9p08x2w4nrvs759pigy225w2")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.13") (deps (list (crate-dep (name "id-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "08whk4a28036jkynbc72rz9ydkwad06cnnfbs8gpli8h2yvnyn82")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.14") (deps (list (crate-dep (name "id-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1hrn84f5ps0h62fpi17z3z6h5jav4j9h9rv69ihbjnss4jaxdkvm")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.15") (deps (list (crate-dep (name "id-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "004sxcgsjypr9lryrdji069nsjdbhxb6farrk6f8sklv9dzr4na4")))

(define-public crate-id-map-0.1 (crate (name "id-map") (vers "0.1.16") (deps (list (crate-dep (name "id-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0502vxrhaldjq4s8z9pgc7h853qmzskz4qcygxlj09kkfba65qcc")))

(define-public crate-id-map-0.2 (crate (name "id-map") (vers "0.2.0") (deps (list (crate-dep (name "id-set") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "12gsyv9h5acf1sa2mh477gf9vi04kyxva0354nph3v72a1igrxlv")))

(define-public crate-id-map-0.2 (crate (name "id-map") (vers "0.2.1") (deps (list (crate-dep (name "id-set") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1lf6yrsn39l14la0g0g007zvaz2yqfp5rf1wv69mnnlxh5lmb0qb")))

