(define-module (crates-io id cu) #:use-module (crates-io))

(define-public crate-idcurl-0.1 (crate (name "idcurl") (vers "0.1.0") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)))) (hash "0zj1a55jdzyf2mgpn6adyc9rf1qx94bsqibl670cmn8ym36wvchs")))

(define-public crate-idcurl-0.1 (crate (name "idcurl") (vers "0.1.1") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)))) (hash "1x5bb36f0ji0a77nm4cscqw49p47n8bv031j00qpb0clgaaf8z5l")))

(define-public crate-idcurl-0.2 (crate (name "idcurl") (vers "0.2.0") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)))) (hash "1886j0xnkabim66qcihsa1nwvqx9y33gh3i4q21r0cg1g37zxw8w")))

(define-public crate-idcurl-0.2 (crate (name "idcurl") (vers "0.2.1") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)))) (hash "1an8zbp4hzj6fxnfb1jmvznz62wp3lnk0n8jy05pjdpy91sikhgl")))

(define-public crate-idcurl-0.2 (crate (name "idcurl") (vers "0.2.2") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)))) (hash "0cir9vc3shqflz3967lqarhhmdl5y7wcy34lmnyxyvyc9aqin2dv")))

(define-public crate-idcurl-0.2 (crate (name "idcurl") (vers "0.2.3") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)))) (hash "15a57zgcalp0kmhcgwf9g2bb7y9wkm5yk750kzzawpxm0c2w3fvg")))

(define-public crate-idcurl-0.3 (crate (name "idcurl") (vers "0.3.0") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0696lvllz8y4hzyqn45qjl9fpnk24vk6x14q2lgi1yynb775c8sk")))

(define-public crate-idcurl-0.3 (crate (name "idcurl") (vers "0.3.1") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0np0a3fmzd3yx9iawynbb41x31qr72nc7a6kmrvsa5wkdm1ya92r")))

(define-public crate-idcurl-0.3 (crate (name "idcurl") (vers "0.3.2") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "02ngw8sf873hdc6qzsn2khaldxvi6afrl0hs51lfz99fkz6km9yf")))

(define-public crate-idcurl-0.4 (crate (name "idcurl") (vers "0.4.0") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "1wq7mvvg29ddj16wl1qxv42xs77s8bh92hk6azjqqdy3d218ks2y")))

(define-public crate-idcurl-0.4 (crate (name "idcurl") (vers "0.4.1") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "12gy2v70k05947dfj5jnkjn91pixdh0ijhsk42i7fdrb2hnyn4dx")))

(define-public crate-idcurl-0.4 (crate (name "idcurl") (vers "0.4.2") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0s2yvadlm69xdrzp8gjiwkvvpjb9b2va6rqw26904y094s1rl94y")))

(define-public crate-idcurl-0.4 (crate (name "idcurl") (vers "0.4.3") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0gaczbprq0zx91kax9c1qsi1kfv7g7x9qy0p063gla91bjx0bvk8")))

(define-public crate-idcurl-0.5 (crate (name "idcurl") (vers "0.5.0") (deps (list (crate-dep (name "curl-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0awkijfwb9a5lryxj94z57fbdzrykr90gs864p5mrim0iy8bh6z8")))

