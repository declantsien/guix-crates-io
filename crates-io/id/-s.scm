(define-module (crates-io id -s) #:use-module (crates-io))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.0") (hash "1vdj3q6dhrr5d42n8yylsb15z1day59y9h368q6xc3rc8wkkax6p")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.1") (hash "0b836ifyr4sgpl80m354a8c55a161iiaab3jwgzi49sw17g18hp4")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.2") (hash "0fhpikf2nravl6mgn64ad2jkynfwzkgm4i3inn8w4hn9cpgp2f55")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.3") (hash "0h02b5p3vz3w238fgli2p8pa0p8mh1lyazm8b6iwlb8yy8by17dm")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.4") (hash "04szx3a6ii2x89pxwy7pg1aila85rkj88aqwhcl8dg5w6kh9qjay")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.5") (hash "0nr975i7i94x3nk0nfn4dlkx0qcmn5v3rsp59p6hby9l69w2996q")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.6") (hash "1iigd7qcyb5912x7dhnihxd6sdhwqmzqfxw86fcnip0cs750i8hc")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.7") (hash "1m3zajl1kikxss8mxhhw1gr8brzw5h4d6hla072qg7q5dsdjazfi")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.8") (hash "0xcq02bkhxzqk0qj3fsdfq3fxabg969yr1grzpkllvnjc7iadw97")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.9") (hash "1np7pcb6nc76xfbszbwx751h4iy9zc963rrnhfhfsqq97qcys27x")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.10") (hash "1qzrzhgmwkhyx1c5cy1m8xq02n246n67hr74m4d6h9nm03z6l42b")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.11") (hash "0mg5m2wa0gmf6l4zm0j5rl0kw69shanny0qpkppnb3q2rj80h5bx")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.12") (hash "192my6ayckqnr5djpf7slcl1qa9cw58rpkfgkgby39wbffb1c4in")))

(define-public crate-id-set-0.1 (crate (name "id-set") (vers "0.1.13") (hash "1lznic77aijcvm4wvcy3mmra2c5932zadcclgzsw1yfipigz3v9b")))

(define-public crate-id-set-0.2 (crate (name "id-set") (vers "0.2.0") (hash "0333gngb48wg5q7qhsqp8yr21lnqnvcd948xwp6d93vxyrgvlfq9")))

(define-public crate-id-set-0.2 (crate (name "id-set") (vers "0.2.1") (hash "15fspfkaq0sm7y7fngvvrpsm43hiy0ivckvnkipb9sr7y4sxx3h7")))

(define-public crate-id-set-0.2 (crate (name "id-set") (vers "0.2.2") (hash "0r4jshsamlkd2999hkg89jn84vdw714bl68iagw6qia6cggzlcwn")))

