(define-module (crates-io id le) #:use-module (crates-io))

(define-public crate-idle-0.1 (crate (name "idle") (vers "0.1.0") (hash "0jkai9ir373r847sx138chrprhbpjawqql1vjwh7a87ds1ybfy0h") (rust-version "1.56.1")))

(define-public crate-idle-0.2 (crate (name "idle") (vers "0.2.0") (hash "1lwxmpjzjrl9xkl2q04g6vhwpvrjqrpgknb1jka57nbhvdb4rzp3") (rust-version "1.56.1")))

