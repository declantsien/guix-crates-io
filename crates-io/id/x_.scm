(define-module (crates-io id x_) #:use-module (crates-io))

(define-public crate-idx_binary-0.1 (crate (name "idx_binary") (vers "0.1.0") (deps (list (crate-dep (name "idx_sized") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zm4906n8dxqryajpld8kb2fhbg6cxw28r4x6ipsnc87jsgzv4c6") (yanked #t)))

(define-public crate-idx_binary-0.2 (crate (name "idx_binary") (vers "0.2.0") (deps (list (crate-dep (name "idx_sized") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mn814043lkzw6mjhzp9358x4yz0xmx758ij8xh5lcfc8slggack") (yanked #t)))

(define-public crate-idx_binary-0.2 (crate (name "idx_binary") (vers "0.2.1") (deps (list (crate-dep (name "idx_sized") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.2") (default-features #t) (kind 0)))) (hash "02mlkd7nvcw5vdvidal543mwcwv7x10bkk7ifsxhvv8ch9mgxv0p") (yanked #t)))

(define-public crate-idx_binary-0.3 (crate (name "idx_binary") (vers "0.3.0") (deps (list (crate-dep (name "idx_sized") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.2") (default-features #t) (kind 0)))) (hash "012w7yd1qzw8hihvjx6zq6n3yrzqgc128kmhqxl3mgxyhfwvlq51") (yanked #t)))

(define-public crate-idx_binary-0.3 (crate (name "idx_binary") (vers "0.3.1") (deps (list (crate-dep (name "idx_sized") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.3") (default-features #t) (kind 0)))) (hash "00wlh7bcn9kz6m9hq9d03vnb1q8wiqr7rkm2rxar9gggylf3rm04") (yanked #t)))

(define-public crate-idx_binary-0.3 (crate (name "idx_binary") (vers "0.3.2") (deps (list (crate-dep (name "idx_sized") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kkarlg4wp056x9hgzb1ny2cd8jmhnp817y7lrngi8iw2hkh1a78") (yanked #t)))

(define-public crate-idx_binary-0.3 (crate (name "idx_binary") (vers "0.3.3") (deps (list (crate-dep (name "idx_sized") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.3") (default-features #t) (kind 0)))) (hash "0k8lmsiyl2xxzw5j54fyvrvd9b7xglfcpqiyv0qmix19ab8jq9b6") (yanked #t)))

(define-public crate-idx_binary-0.3 (crate (name "idx_binary") (vers "0.3.4") (deps (list (crate-dep (name "idx_sized") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jm0gg7p4almwh1mcbafhw629a1wlc1sv2qzxdwn9gj2r6zdk5g0") (yanked #t)))

(define-public crate-idx_binary-0.3 (crate (name "idx_binary") (vers "0.3.6") (deps (list (crate-dep (name "idx_sized") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jx78d6g10y613d064gvkmr9gxy4i16x3hf4g4lqx2rzcllirx63") (yanked #t)))

(define-public crate-idx_binary-0.3 (crate (name "idx_binary") (vers "0.3.7") (deps (list (crate-dep (name "idx_sized") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.3") (default-features #t) (kind 0)))) (hash "1d2rf59bnpkys94h22haqmkz2qw06cx1r29q2q136n2lwblijsn3") (yanked #t)))

(define-public crate-idx_binary-0.4 (crate (name "idx_binary") (vers "0.4.0") (deps (list (crate-dep (name "idx_sized") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.3") (default-features #t) (kind 0)))) (hash "17l46v2l37m8gv4p89vws4ry7m3kaf2bjyk46d5wfia1l9gka93f") (yanked #t)))

(define-public crate-idx_binary-0.5 (crate (name "idx_binary") (vers "0.5.0") (deps (list (crate-dep (name "idx_sized") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gljgdcw7w3f51vsmfifjk2jka9xrm39prilb7y7bv39cyyaicaj") (yanked #t)))

(define-public crate-idx_binary-0.6 (crate (name "idx_binary") (vers "0.6.0") (deps (list (crate-dep (name "idx_sized") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.4") (default-features #t) (kind 0)))) (hash "0033j21y91wskfr3pv3r6mz2q28cgg6sm853l3q9gbrk8adrd6jg") (yanked #t)))

(define-public crate-idx_binary-0.7 (crate (name "idx_binary") (vers "0.7.0") (deps (list (crate-dep (name "idx_sized") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.5") (default-features #t) (kind 0)))) (hash "0qagk6xkdlnhpsv8nfgwfgldyp2vcick8b519pahfjkdl2390qfj") (yanked #t)))

(define-public crate-idx_binary-0.7 (crate (name "idx_binary") (vers "0.7.1") (deps (list (crate-dep (name "idx_sized") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.6") (default-features #t) (kind 0)))) (hash "1n60vfmg4ncqpkmjgbga07dp9lqg57s2c6skd8ldcdl1p56f27g9") (yanked #t)))

(define-public crate-idx_binary-0.8 (crate (name "idx_binary") (vers "0.8.0") (deps (list (crate-dep (name "idx_sized") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.7") (default-features #t) (kind 0)))) (hash "0dgpd1n8nsvr64db7ni5fz08gs9b24ss860hz75cdx2v09gmrbki") (yanked #t)))

(define-public crate-idx_binary-0.8 (crate (name "idx_binary") (vers "0.8.1") (deps (list (crate-dep (name "idx_sized") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.7") (default-features #t) (kind 0)))) (hash "1pqkrfvg77vpv3w49xy0pg60vpq0apr7ya0bazdn0qi62vfwms0n") (yanked #t)))

(define-public crate-idx_binary-0.8 (crate (name "idx_binary") (vers "0.8.2") (deps (list (crate-dep (name "idx_sized") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.7") (default-features #t) (kind 0)))) (hash "0abvjk0a67lm51pbnrnm8yxm7wsp7bjwb3cb6jb7sgjajqwhkfj0") (yanked #t)))

(define-public crate-idx_binary-0.8 (crate (name "idx_binary") (vers "0.8.3") (deps (list (crate-dep (name "idx_sized") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.7") (default-features #t) (kind 0)))) (hash "0jgpwsx3vrwjfcy797qay7kpdcq7yp52fwsfk456r18758p89lg1") (yanked #t)))

(define-public crate-idx_binary-0.8 (crate (name "idx_binary") (vers "0.8.4") (deps (list (crate-dep (name "idx_sized") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.7") (default-features #t) (kind 0)))) (hash "1wmc5dh0d508q4afzfisl7ak7q5bvhh993r804b02wwgambvm114") (yanked #t)))

(define-public crate-idx_binary-0.8 (crate (name "idx_binary") (vers "0.8.5") (deps (list (crate-dep (name "idx_sized") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.7") (default-features #t) (kind 0)))) (hash "161prwbrgph7qvfavxhsy889qfkb07dag9m6k72ngxs64m6cg468") (yanked #t)))

(define-public crate-idx_binary-0.8 (crate (name "idx_binary") (vers "0.8.6") (deps (list (crate-dep (name "idx_sized") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.7") (default-features #t) (kind 0)))) (hash "16rxy1x0z21wlddhc9h6ams108ira0m457fqsb51fwnn3m5kvwl1") (yanked #t)))

(define-public crate-idx_binary-0.9 (crate (name "idx_binary") (vers "0.9.0") (deps (list (crate-dep (name "idx_sized") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ym4iv6755n6if95kdx63040ym667ws4zwyz0cdcvrcnn9fg5bc6") (yanked #t)))

(define-public crate-idx_binary-0.10 (crate (name "idx_binary") (vers "0.10.0") (deps (list (crate-dep (name "idx_sized") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ndlbhk4yf1j2b92bxrl5sxq2cf6a6nn406qrw623in5c7if80g0") (yanked #t)))

(define-public crate-idx_binary-0.10 (crate (name "idx_binary") (vers "0.10.1") (deps (list (crate-dep (name "idx_sized") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.8") (default-features #t) (kind 0)))) (hash "0x0ifyikhp95br42wdyx2fz0vgbgmazd25ql6ffw1w9hf4982n2g") (yanked #t)))

(define-public crate-idx_binary-0.10 (crate (name "idx_binary") (vers "0.10.2") (deps (list (crate-dep (name "idx_sized") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.9") (default-features #t) (kind 0)))) (hash "068kc699c93vagv2q4a25whjgvyj5xcj5ms2d2h185k3z4hfix5n") (yanked #t)))

(define-public crate-idx_binary-0.10 (crate (name "idx_binary") (vers "0.10.3") (deps (list (crate-dep (name "idx_sized") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.9") (default-features #t) (kind 0)))) (hash "0gcbhcw89yy44rp29v4g50hdd9aj6nv04fvxrbm22k2jr1vrh82f") (yanked #t)))

(define-public crate-idx_binary-0.10 (crate (name "idx_binary") (vers "0.10.4") (deps (list (crate-dep (name "idx_sized") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.9") (default-features #t) (kind 0)))) (hash "0cqf4cfl3djbx7sjbyrwyr5vjbwv4ccd21vifrsxj9fqdzhy2yj2") (yanked #t)))

(define-public crate-idx_binary-0.10 (crate (name "idx_binary") (vers "0.10.5") (deps (list (crate-dep (name "idx_sized") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.9") (default-features #t) (kind 0)))) (hash "02l3wym595yzbwan5insxhaa9m10zwa48v7slqanki5shpq06z4g") (yanked #t)))

(define-public crate-idx_binary-0.11 (crate (name "idx_binary") (vers "0.11.0") (deps (list (crate-dep (name "idx_sized") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.9") (default-features #t) (kind 0)))) (hash "1cl00fqlck1fjj752lz2800vdbl2rzqvx7vq794hnx1c6nfdbiw8") (yanked #t)))

(define-public crate-idx_binary-0.11 (crate (name "idx_binary") (vers "0.11.1") (deps (list (crate-dep (name "idx_sized") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.10") (default-features #t) (kind 0)))) (hash "0gnia30h0czbm1v0ra3a4rdk4gs9957m65hcpafq27vzk60cwsyr") (yanked #t)))

(define-public crate-idx_binary-0.11 (crate (name "idx_binary") (vers "0.11.2") (deps (list (crate-dep (name "idx_sized") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.10") (default-features #t) (kind 0)))) (hash "1fsqdyyklgpfxylgzci77anfn34s8vr6gygg0jcvw2cpscnhnd4j") (yanked #t)))

(define-public crate-idx_binary-0.11 (crate (name "idx_binary") (vers "0.11.3") (deps (list (crate-dep (name "idx_sized") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.10") (default-features #t) (kind 0)))) (hash "0skcbnqb6da2029smx2xmk0bnm2c8p0rhjd4hrkllw3pkxzwbfqp") (yanked #t)))

(define-public crate-idx_binary-0.12 (crate (name "idx_binary") (vers "0.12.0") (deps (list (crate-dep (name "idx_sized") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.10") (default-features #t) (kind 0)))) (hash "10kd6a50mw9m3xc7gaijcgq8dfxzmljjhp6h39mjqbzvc9wm6c57") (yanked #t)))

(define-public crate-idx_binary-0.13 (crate (name "idx_binary") (vers "0.13.0") (deps (list (crate-dep (name "idx_sized") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.10") (default-features #t) (kind 0)))) (hash "06cb92sas0qp6pfxk3ngvyyd78ziblr8k99h6f6jdy2qpzks3qhr") (yanked #t)))

(define-public crate-idx_binary-0.13 (crate (name "idx_binary") (vers "0.13.1") (deps (list (crate-dep (name "idx_sized") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.10") (default-features #t) (kind 0)))) (hash "01ygdlya7gfc42lqfdf7qivf2kgcnb9xj117fh570y3d173fg6w9") (yanked #t)))

(define-public crate-idx_binary-0.13 (crate (name "idx_binary") (vers "0.13.2") (deps (list (crate-dep (name "idx_sized") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "00wrzxl10say6pzkldz3dilqf7svm3pamvibnxj37zk9abixnqbg") (yanked #t)))

(define-public crate-idx_binary-0.13 (crate (name "idx_binary") (vers "0.13.3") (deps (list (crate-dep (name "idx_sized") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "0kfjcqhmx50bsijpnviqrd2wdkmn6csdiglf4nyhrwaj5k61gfli") (yanked #t)))

(define-public crate-idx_binary-0.13 (crate (name "idx_binary") (vers "0.13.4") (deps (list (crate-dep (name "idx_file") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "09bynm2vbaaiaj1b5lh5k555vscsfxwkfaq89wxxc2mldnpnbnca") (yanked #t)))

(define-public crate-idx_binary-0.13 (crate (name "idx_binary") (vers "0.13.5") (deps (list (crate-dep (name "idx_file") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "02gz65jkssrl6iz6x5ifg2rplv7knj7zgrb7wx4fclxhqq0a8fpr") (yanked #t)))

(define-public crate-idx_binary-0.14 (crate (name "idx_binary") (vers "0.14.0") (deps (list (crate-dep (name "idx_file") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "0d4w9r7rfy9965v6m294dqajlhdzcchari5wp3y8wsimgdzbxjbp") (yanked #t)))

(define-public crate-idx_binary-0.14 (crate (name "idx_binary") (vers "0.14.1") (deps (list (crate-dep (name "idx_file") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "1va8p7jd1vvcvgyfgmx72sh51q8qk0kv1fnyckblb0r4dff44ac7") (yanked #t)))

(define-public crate-idx_binary-0.15 (crate (name "idx_binary") (vers "0.15.0") (deps (list (crate-dep (name "idx_file") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "164ag6cgs5ii6x51hfzafmk78hzmayxfwbfp61rhkbzkl5pn0xx3") (yanked #t)))

(define-public crate-idx_binary-0.15 (crate (name "idx_binary") (vers "0.15.1") (deps (list (crate-dep (name "idx_file") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "0b795y3cv0k861d1k5fjhd28g0z1zq1xknghd2b8qx6y0cssvliw") (yanked #t)))

(define-public crate-idx_binary-0.15 (crate (name "idx_binary") (vers "0.15.2") (deps (list (crate-dep (name "idx_file") (req "^0.33") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "0gdxalxx9jx9vr043igcgc7vykcvm83wlnjzlswkk9bk72rfyc0l") (yanked #t)))

(define-public crate-idx_binary-0.16 (crate (name "idx_binary") (vers "0.16.0") (deps (list (crate-dep (name "idx_file") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "0vkznwknm8alw23197czp4c37gdiwv9a6bla3nc0s49mk163sb9i") (yanked #t)))

(define-public crate-idx_binary-0.17 (crate (name "idx_binary") (vers "0.17.0") (deps (list (crate-dep (name "human-sort") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "0fqd6ahqqqnrwybwmkwqi8c3gpapipc2nixaw3sqac831d34qr1v") (yanked #t)))

(define-public crate-idx_binary-0.18 (crate (name "idx_binary") (vers "0.18.0") (deps (list (crate-dep (name "idx_file") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "natord") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "1gs8852vdy94irq9dn654iklcdmdvjdl9406flpfr8lfsidjpcak") (yanked #t)))

(define-public crate-idx_binary-0.18 (crate (name "idx_binary") (vers "0.18.1") (deps (list (crate-dep (name "idx_file") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.11") (default-features #t) (kind 0)))) (hash "1qyfw7smm8ifgaji2xgihdv13zmiiipmg4bprcn7k0x4zxx9siqh") (yanked #t)))

(define-public crate-idx_binary-0.18 (crate (name "idx_binary") (vers "0.18.2") (deps (list (crate-dep (name "idx_file") (req "^0.35") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "0rjbjg7bzxvqbq2sim9sjzvryg6dfhq7ihrarhg56pvjg0mm5v1h") (yanked #t)))

(define-public crate-idx_binary-0.18 (crate (name "idx_binary") (vers "0.18.3") (deps (list (crate-dep (name "idx_file") (req "^0.36") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1jj2a1sisfz9yqwzpnrbcm87z14w2ikfz85chqa3mf2w0x6ldgkx")))

(define-public crate-idx_binary-0.19 (crate (name "idx_binary") (vers "0.19.0") (deps (list (crate-dep (name "idx_file") (req "^0.37") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1rc3jixkjdsgid674f7w0w0ma9j9g9yaywmhwj3bl99gc75c2d9w")))

(define-public crate-idx_binary-0.19 (crate (name "idx_binary") (vers "0.19.1") (deps (list (crate-dep (name "idx_file") (req "^0.37") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1wapwmib4fhai1x2l6xjx3m63cp510frkqpxbaryj4si7y4zgryi")))

(define-public crate-idx_binary-0.19 (crate (name "idx_binary") (vers "0.19.2") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.38") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "0sf7kignqs347419ygxgipsmhw9y43mnvpdw6m3hr9hx24fj681y")))

(define-public crate-idx_binary-0.19 (crate (name "idx_binary") (vers "0.19.3") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.38") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "0lzpqp4rzwzq5cdq6bcami541x8v6w58qk591kn9kgsdbpr60w94")))

(define-public crate-idx_binary-0.20 (crate (name "idx_binary") (vers "0.20.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.38") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1xcj9ylhyz4g8p9x4g7jrs2nyy5wajgbqa292w360z2zfy44yssp")))

(define-public crate-idx_binary-0.21 (crate (name "idx_binary") (vers "0.21.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.39") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "02q1z5ihagw41vs1gfwqa10a4zv5bbfj23bjp65nk2kswk0swn7h")))

(define-public crate-idx_binary-0.22 (crate (name "idx_binary") (vers "0.22.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.40") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1jfbyv6x37xk1bwj6cjvwl1pjx0dg0dbrrdawdpi48k4rfcqrqgz")))

(define-public crate-idx_binary-0.22 (crate (name "idx_binary") (vers "0.22.1") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.41") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "06991x1i0h9fzjv6vmd2jcpw3rz7a8zjavsz8250pgnlifxhf16p")))

(define-public crate-idx_binary-0.23 (crate (name "idx_binary") (vers "0.23.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.42") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1j4awf54dq5m8rsdz2jasaa5xbbhqp8f8c50j42m48sv1qzsh6as")))

(define-public crate-idx_binary-0.24 (crate (name "idx_binary") (vers "0.24.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.43") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1qvfq9dvs6rb931nz1qa1is6q6r7p93ahznl4hb32c5ggc1vlg45")))

(define-public crate-idx_binary-0.25 (crate (name "idx_binary") (vers "0.25.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.44") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "1vgi3xgi5jg4sn1nisb4q3yc2f9sn4qz0wi8zfg08y4y7xvgnq0i")))

(define-public crate-idx_binary-0.26 (crate (name "idx_binary") (vers "0.26.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.45") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.12") (default-features #t) (kind 0)))) (hash "0i5142gd4x8x8h6f287gz8i0igy4r26pgx2wyc8h74d9wpphli0l")))

(define-public crate-idx_binary-0.27 (crate (name "idx_binary") (vers "0.27.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.13") (default-features #t) (kind 0)))) (hash "01pxwar4lk0xv2p0pp22ksv4v38q4dac71arflgimsxgpsihd9s1")))

(define-public crate-idx_binary-0.27 (crate (name "idx_binary") (vers "0.27.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.13") (default-features #t) (kind 0)))) (hash "04b5kskqqh1xjs6cnvq6994fk7z6avqy0qq5vvfzpba8ylhfhyys")))

(define-public crate-idx_binary-0.27 (crate (name "idx_binary") (vers "0.27.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.13") (default-features #t) (kind 0)))) (hash "1vain55pn0rm5fizzrw6y71clhz56zkkaafm2x8y8ckim1hz6iim")))

(define-public crate-idx_binary-0.27 (crate (name "idx_binary") (vers "0.27.3") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.13") (default-features #t) (kind 0)))) (hash "1f3zp0bjlbx6fj4g1a91lgqcin4rvciai6g6lnr03r1hgmh79iv1")))

(define-public crate-idx_binary-0.27 (crate (name "idx_binary") (vers "0.27.4") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.13") (default-features #t) (kind 0)))) (hash "0nn2yd3lmbbdpwqf05iizdlx3jyw8bqqwzyifb78qd7zhhi6spq5")))

(define-public crate-idx_binary-0.27 (crate (name "idx_binary") (vers "0.27.5") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.13") (default-features #t) (kind 0)))) (hash "1dcdbkl9k464558nfmxxyvpg4m2nln2nllsvki1s9q7mvhxwyy6a")))

(define-public crate-idx_binary-0.28 (crate (name "idx_binary") (vers "0.28.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.47.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "13xwm47a19ziccxl441cs192f2iirbyhqk40xzpi44i38mhyh6ij")))

(define-public crate-idx_binary-0.28 (crate (name "idx_binary") (vers "0.28.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.47.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0y11mjx8702wcfxmv5s9alm78fqi0l5jvg0621pn9rl840862z41")))

(define-public crate-idx_binary-0.28 (crate (name "idx_binary") (vers "0.28.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.48.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1a7aa656bqxg3nqpkn4lw3s8agnm786iz3n07dgiqiwh0qr14bpl")))

(define-public crate-idx_binary-0.29 (crate (name "idx_binary") (vers "0.29.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.49.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0ynj0r2bdd6b8n536qbg83aqaz4mayq26g7q3hzzq7vj2vvxxm1k")))

(define-public crate-idx_binary-0.29 (crate (name "idx_binary") (vers "0.29.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.49.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1zn925qj976qrz8v65vgicn0x5wgsljyydic16ziavdfzm1k5kda")))

(define-public crate-idx_binary-0.30 (crate (name "idx_binary") (vers "0.30.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.50.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1x8wj2b5xybiz62hz3j5alfrbgzw2g8k29g03xb6inc7y4g36c7s")))

(define-public crate-idx_binary-0.31 (crate (name "idx_binary") (vers "0.31.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.51.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1cxcci7qhlhhsf1slx0dcl303rjdayzican4wjakvyzb4z9r9iyi")))

(define-public crate-idx_binary-0.32 (crate (name "idx_binary") (vers "0.32.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.52.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "1ijd2r1dwbicxr7s3yha04bjf1rnx0ir4qda47d7srlbwxk5vi3z")))

(define-public crate-idx_binary-0.33 (crate (name "idx_binary") (vers "0.33.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.53.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "04hp55kvmy28i375rpvw2ygv6y785nxjjgkp5gvhnjri8xpkkqix")))

(define-public crate-idx_binary-0.33 (crate (name "idx_binary") (vers "0.33.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.53.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0yc91mgr99dycb2vhrxhqx8p2i03m4zlww5i49k16p6209qns8wj")))

(define-public crate-idx_binary-0.33 (crate (name "idx_binary") (vers "0.33.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.54.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "150lfg06is9ipq2ixngnc15nmwazk8qhs51cpazfpjpidx7301fi")))

(define-public crate-idx_binary-0.34 (crate (name "idx_binary") (vers "0.34.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.55.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "18hafzzbf4cnwvgxxk91pwhblm0y3hjfmc365ard27z92lbpblrx")))

(define-public crate-idx_binary-0.34 (crate (name "idx_binary") (vers "0.34.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.55.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1r3l3m7gbh14m4fq6f3xz44zhcirrw4hgfzg3z4xsh93clxpnk09")))

(define-public crate-idx_binary-0.35 (crate (name "idx_binary") (vers "0.35.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.56.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0nc8agbk7yjnxk0dcrk7xfz1d0f16ab6sbf8n08wflanymb2knhf")))

(define-public crate-idx_binary-0.36 (crate (name "idx_binary") (vers "0.36.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.57.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0mf2x1aq2zpybs5i7jywaa0r06xjv9z1vn5zbqn0v3wgg0m5in0a")))

(define-public crate-idx_binary-0.36 (crate (name "idx_binary") (vers "0.36.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.58.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0611zghn5bwf8rgf5v2jdrd5cgj6adh99dm0cs28pciv45m8qxv6")))

(define-public crate-idx_binary-0.37 (crate (name "idx_binary") (vers "0.37.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.59.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "168gh9p8q8yv4y07zjvx37d0li1l99x2lys1y8xra0qgvlf31a8x")))

(define-public crate-idx_binary-0.37 (crate (name "idx_binary") (vers "0.37.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.59.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0jpw2sjpgdl6ynnv0nsw6hmc672shrsn88jwxkc7jp63zrpb9z1c")))

(define-public crate-idx_binary-0.38 (crate (name "idx_binary") (vers "0.38.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.60.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1zihdp5v7a0yfvh0c31647ap8f4if4d8nybkhkyqdlr63wiqs8cc")))

(define-public crate-idx_binary-0.38 (crate (name "idx_binary") (vers "0.38.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.61.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1r8k8nw6shn50djhy876pwm7bjvf1nxlx4080w72p8xv373h5saj")))

(define-public crate-idx_binary-0.38 (crate (name "idx_binary") (vers "0.38.2") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.62.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0k652z6wnb5kh7023kzqz5h19j61h3cpdyngrds4k66i1xakn9a1")))

(define-public crate-idx_binary-0.38 (crate (name "idx_binary") (vers "0.38.3") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.63.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "12y4kzxaasg2q74d14kgkvy6yskcjs93nwcid0jkbbjj52n4xb5r")))

(define-public crate-idx_binary-0.38 (crate (name "idx_binary") (vers "0.38.4") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.64.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1n6k7lak1qfa4i9cid3qp2q9lyw6j7aiz4gi87f29smgzxf7lz6y")))

(define-public crate-idx_binary-0.38 (crate (name "idx_binary") (vers "0.38.5") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "idx_file") (req "^0.64.0") (default-features #t) (kind 0)) (crate-dep (name "various_data_file") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "18gv75ic5p6n4zqc1rynqajjspvbgr4435l4gcrngwklqcxrgy5m")))

(define-public crate-idx_file-0.29 (crate (name "idx_file") (vers "0.29.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.42") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1xb9pw16qsmp2v1r1ji63jf9dqmindmjixyh9ylv6gs42xprpdhx") (yanked #t)))

(define-public crate-idx_file-0.30 (crate (name "idx_file") (vers "0.30.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.43") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "14zr92c55kb4npn2mj5r1n3whnwfybwm4s8sg4c5ggvm1fcymwvf") (yanked #t)))

(define-public crate-idx_file-0.30 (crate (name "idx_file") (vers "0.30.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.43") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "06jmwnl2ip5fhvi7l116npkglasdbsgydymss7fspi22jsw9yr5a") (yanked #t)))

(define-public crate-idx_file-0.30 (crate (name "idx_file") (vers "0.30.2") (deps (list (crate-dep (name "avltriee") (req "^0.43") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1vziizwmw4bjq9kk9fbcz1cmfz22s0476k8vp450zhcfw4bfw2pv") (yanked #t)))

(define-public crate-idx_file-0.31 (crate (name "idx_file") (vers "0.31.0") (deps (list (crate-dep (name "avltriee") (req "^0.44") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1nm7fph3m50w44maa6rzjh8nybwpsp04dsckr0nrd69qk9dj8aqi") (yanked #t)))

(define-public crate-idx_file-0.32 (crate (name "idx_file") (vers "0.32.0") (deps (list (crate-dep (name "avltriee") (req "^0.45") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "07n176xmzldvz7pf30z56bvmw9c37gvphdmj73bkxd0przy0aqsf") (yanked #t)))

(define-public crate-idx_file-0.33 (crate (name "idx_file") (vers "0.33.0") (deps (list (crate-dep (name "avltriee") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "175k1g5nn3xf4a6wq17kr5zf3k8ihjd5hgscwv6wzjr713x17k5r") (yanked #t)))

(define-public crate-idx_file-0.34 (crate (name "idx_file") (vers "0.34.0") (deps (list (crate-dep (name "avltriee") (req "^0.47") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "0ghin7vwlrvsrj0sg996yp3f6c6hwrwm0lnqw3bf9cn6007cfbia") (yanked #t)))

(define-public crate-idx_file-0.35 (crate (name "idx_file") (vers "0.35.0") (deps (list (crate-dep (name "avltriee") (req "^0.47") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.16") (default-features #t) (kind 0)))) (hash "0j61llrs9zzi0pirzbv55idlgsgxhkyi1352cv99f46mnkisjj7q") (yanked #t)))

(define-public crate-idx_file-0.35 (crate (name "idx_file") (vers "0.35.1") (deps (list (crate-dep (name "avltriee") (req "^0.48") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.16") (default-features #t) (kind 0)))) (hash "0xxjyx8g4lf5xkw75sd001xs0bgp5bd76hka4ik2ia4qlykkr0hq") (yanked #t)))

(define-public crate-idx_file-0.36 (crate (name "idx_file") (vers "0.36.0") (deps (list (crate-dep (name "avltriee") (req "^0.49") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.16") (default-features #t) (kind 0)))) (hash "03j16zn53ag6wnmgyj4f9c0mxighi934dpwd1ms38k3a0kzqzrxh")))

(define-public crate-idx_file-0.37 (crate (name "idx_file") (vers "0.37.0") (deps (list (crate-dep (name "avltriee") (req "^0.50") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.16") (default-features #t) (kind 0)))) (hash "0xcqx4mviragclfnfbz05xrn4swwrf5i0lizv1bqh5by0dn45s2q")))

(define-public crate-idx_file-0.37 (crate (name "idx_file") (vers "0.37.1") (deps (list (crate-dep (name "avltriee") (req "^0.50") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.16") (default-features #t) (kind 0)))) (hash "0sdz2699c659fgyamra2g2f4c48xz22yy1mcsq6rma4dk4zdldq0")))

(define-public crate-idx_file-0.37 (crate (name "idx_file") (vers "0.37.2") (deps (list (crate-dep (name "avltriee") (req "^0.50") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.17") (default-features #t) (kind 0)))) (hash "1lr3447kabg1p51jmgqr664vlwkv7almvnp1578y6j3v7jz6207z")))

(define-public crate-idx_file-0.38 (crate (name "idx_file") (vers "0.38.0") (deps (list (crate-dep (name "avltriee") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.17") (default-features #t) (kind 0)))) (hash "04jdqygx5my776fhc3lpm1kbswfc3gahy9988yhc08i9gnkxcy2a")))

(define-public crate-idx_file-0.38 (crate (name "idx_file") (vers "0.38.1") (deps (list (crate-dep (name "avltriee") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.18") (default-features #t) (kind 0)))) (hash "1v6zgl0k557vrcad960mgd2l9zk3spdhm5g44s5vjg73rcgr5b5b")))

(define-public crate-idx_file-0.38 (crate (name "idx_file") (vers "0.38.2") (deps (list (crate-dep (name "avltriee") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.18") (default-features #t) (kind 0)))) (hash "1ihaj0cd972xks4ir4yn1nibgcf1yikh64ivlb77a7pc76hin9hc")))

(define-public crate-idx_file-0.38 (crate (name "idx_file") (vers "0.38.3") (deps (list (crate-dep (name "avltriee") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)))) (hash "02f42jcpgnbav0xcx1hghim47fypax7hwvda3kys398rlx4m4x45")))

(define-public crate-idx_file-0.38 (crate (name "idx_file") (vers "0.38.4") (deps (list (crate-dep (name "avltriee") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)))) (hash "1v37av5vzbcr01byrnpdvbl9w6d2xjvjgcl92dcxv2bq8gwl63c6")))

(define-public crate-idx_file-0.39 (crate (name "idx_file") (vers "0.39.0") (deps (list (crate-dep (name "avltriee") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)))) (hash "1y6ijsmk2mfmpfh4zjbcj905maqh87fg6ivazv47r2r1cmy26bbq")))

(define-public crate-idx_file-0.40 (crate (name "idx_file") (vers "0.40.0") (deps (list (crate-dep (name "avltriee") (req "^0.52") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)))) (hash "1jg73m9n097c3pa1a5xm432l51lz3p9063yd9kihpr6r67kdiav0")))

(define-public crate-idx_file-0.41 (crate (name "idx_file") (vers "0.41.0") (deps (list (crate-dep (name "avltriee") (req "^0.53") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)))) (hash "0abk4k8b67k43bmv678jvmbvd0ja7n9bai38s8lx283n5kk4a6bc")))

(define-public crate-idx_file-0.42 (crate (name "idx_file") (vers "0.42.0") (deps (list (crate-dep (name "avltriee") (req "^0.54") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)))) (hash "0bfm8wnia35skf9dkjs5hgiza1nvgcx8sg170sganfrdndg2416v")))

(define-public crate-idx_file-0.43 (crate (name "idx_file") (vers "0.43.0") (deps (list (crate-dep (name "avltriee") (req "^0.55") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "1hv1v1vy8qgkx08q3li42hlzk4m8ivb03fn9pfzlklfrify4fjjp")))

(define-public crate-idx_file-0.44 (crate (name "idx_file") (vers "0.44.0") (deps (list (crate-dep (name "avltriee") (req "^0.56") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "024ng9fk43wv0mmz5mpnrn50b3k6yhvyx4g8azj140sc36894z92")))

(define-public crate-idx_file-0.45 (crate (name "idx_file") (vers "0.45.0") (deps (list (crate-dep (name "avltriee") (req "^0.56") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "0a67ykmxiwsq0dlc9mkpcpkmpkjrhijq15sfdyk7bcp2gxfkhjhj")))

(define-public crate-idx_file-0.46 (crate (name "idx_file") (vers "0.46.0") (deps (list (crate-dep (name "avltriee") (req "^0.57") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "0j5h27xi25yzbgwv277zblwm5ad2yrm4yr7dl87lq5x71y1pmwb0")))

(define-public crate-idx_file-0.46 (crate (name "idx_file") (vers "0.46.1") (deps (list (crate-dep (name "avltriee") (req "^0.57") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "0zy882p2w122mgxpkbhg9xzr4vl0xbb3ryh9hzcvq4zz012v1dif")))

(define-public crate-idx_file-0.46 (crate (name "idx_file") (vers "0.46.2") (deps (list (crate-dep (name "avltriee") (req "^0.57") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "01rzdzjy32d1shfw8ijx9r97jv02lb5lnqn0qpvyvng7da9hjkrn")))

(define-public crate-idx_file-0.46 (crate (name "idx_file") (vers "0.46.3") (deps (list (crate-dep (name "avltriee") (req "^0.57") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "1djmk1hri9fslbbb18zjgsbbsbiw4mx450z3crhm639xac48w1d6")))

(define-public crate-idx_file-0.46 (crate (name "idx_file") (vers "0.46.4") (deps (list (crate-dep (name "avltriee") (req "^0.57") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0a2s5sa8319fimsfg0y46gcs8ip5lqg3hy0cik28z2nivgww2fay")))

(define-public crate-idx_file-0.47 (crate (name "idx_file") (vers "0.47.0") (deps (list (crate-dep (name "avltriee") (req "^0.58.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0zn87syyd64jgclfrs2m4c5bnl8vfz5sgqcqk47h2k6xfpy8xbpr")))

(define-public crate-idx_file-0.47 (crate (name "idx_file") (vers "0.47.1") (deps (list (crate-dep (name "avltriee") (req "^0.58.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "18jl469lmylcs91fx7w6b5w9x8rsd9kgb21mwy4qrg1mn9m9nkcw")))

(define-public crate-idx_file-0.48 (crate (name "idx_file") (vers "0.48.0") (deps (list (crate-dep (name "avltriee") (req "^0.60.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0m1n7yybskc0pf1c7xbybv4x72cqpbb8xf84p6wvr7cys8pp1s6k")))

(define-public crate-idx_file-0.48 (crate (name "idx_file") (vers "0.48.1") (deps (list (crate-dep (name "avltriee") (req "^0.61.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "177ggv2a8102kwf01bky0z1zzc8amsjmkk5h89vlp8cgax2fdk8c")))

(define-public crate-idx_file-0.49 (crate (name "idx_file") (vers "0.49.0") (deps (list (crate-dep (name "avltriee") (req "^0.61.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "1rw7w0g51hvim95i2kc5y3kd6lr4cz13b3vwsbkwl9b70xi2ky13")))

(define-public crate-idx_file-0.50 (crate (name "idx_file") (vers "0.50.0") (deps (list (crate-dep (name "avltriee") (req "^0.62.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "1k9gsf2h7s6rpfk4z40dhcqm3cdgm5nww7nk5p9azdirj842p8hl")))

(define-public crate-idx_file-0.50 (crate (name "idx_file") (vers "0.50.2") (deps (list (crate-dep (name "avltriee") (req "^0.63.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "19yy3h67wgamwxlsa9v56cdnrsb5f6bf6hqhz6nyhz6l3gk0fsjj")))

(define-public crate-idx_file-0.51 (crate (name "idx_file") (vers "0.51.0") (deps (list (crate-dep (name "avltriee") (req "^0.64.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0116knrf6vcdrwm948i37iy62y87njz2c8sz0ij9rk8x3bkbqi19")))

(define-public crate-idx_file-0.51 (crate (name "idx_file") (vers "0.51.1") (deps (list (crate-dep (name "avltriee") (req "^0.65.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0vhljpvhz9gz7xs3vnxl546s0nxm7lvcc2afbriz1kcg9vy58ld8")))

(define-public crate-idx_file-0.52 (crate (name "idx_file") (vers "0.52.0") (deps (list (crate-dep (name "avltriee") (req "^0.66.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0krs307jv05lrcyvvmqzdfs57z2xnvq8bgjcd9rybbb7pbr3psms")))

(define-public crate-idx_file-0.53 (crate (name "idx_file") (vers "0.53.0") (deps (list (crate-dep (name "avltriee") (req "^0.66.1") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0ff1kxw10m3vnghijy1sa58fgb66n6pwlgpkl654i3f9nffkwdvd")))

(define-public crate-idx_file-0.54 (crate (name "idx_file") (vers "0.54.0") (deps (list (crate-dep (name "avltriee") (req "^0.67.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0w8n93hvwc45lybcm0fgdf7ph7hv6xkdrh2s0ymrxhsdx2vxi0nh")))

(define-public crate-idx_file-0.55 (crate (name "idx_file") (vers "0.55.0") (deps (list (crate-dep (name "avltriee") (req "^0.68.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0wjkp8a0p4gqpkjd3a8m2jd2zh5cqaxn6vgvn2y9dc4g34y2f7db")))

(define-public crate-idx_file-0.56 (crate (name "idx_file") (vers "0.56.0") (deps (list (crate-dep (name "avltriee") (req "^0.69.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0mv702qmz82vazl4nv8fjk95qr2hzbnv5qps9ml2hcsmfhy71vys")))

(define-public crate-idx_file-0.57 (crate (name "idx_file") (vers "0.57.0") (deps (list (crate-dep (name "avltriee") (req "^0.70.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "1b7p18lfaz3cs4z1hgddbhapgvvpaf15hz765vlnnzy6las2pkdi")))

(define-public crate-idx_file-0.58 (crate (name "idx_file") (vers "0.58.0") (deps (list (crate-dep (name "avltriee") (req "^0.71.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0z2ds3pdkldzmfv6cfplj4m8l07vhnx99qz88z95xharbcncnpjw")))

(define-public crate-idx_file-0.59 (crate (name "idx_file") (vers "0.59.0") (deps (list (crate-dep (name "avltriee") (req "^0.72.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "07wmx9vlgzr8vgh32g9r8vflai5xdiglihm7yhx9kqz4rqp3jf0q")))

(define-public crate-idx_file-0.60 (crate (name "idx_file") (vers "0.60.0") (deps (list (crate-dep (name "avltriee") (req "^0.73.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "120b1mnsiab7csmia4q3hgdwff5agxnpjyqm3agqphkyz2wf8yja")))

(define-public crate-idx_file-0.61 (crate (name "idx_file") (vers "0.61.0") (deps (list (crate-dep (name "avltriee") (req "^0.74.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0rigrxmrdzaq6l2ssdb1g4iczmqzm3pgq4srwc65w737ngv948di")))

(define-public crate-idx_file-0.62 (crate (name "idx_file") (vers "0.62.0") (deps (list (crate-dep (name "avltriee") (req "^0.75.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "1lfg73gc8f4v9p8d0q95d19dcq7qy16pls8w29iaj2pcli4wv21b")))

(define-public crate-idx_file-0.63 (crate (name "idx_file") (vers "0.63.0") (deps (list (crate-dep (name "avltriee") (req "^0.76.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "1xdjm7zsiv0qxwv6nhvsq2g9gwij0acfs3cngiczxsmcr72wqwn8")))

(define-public crate-idx_file-0.64 (crate (name "idx_file") (vers "0.64.0") (deps (list (crate-dep (name "avltriee") (req "^0.77.0") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 2)))) (hash "0v26y5g0s6a877drnka0zvcssy4sihzh8iph2q9s5f7ljxrwxj39")))

(define-public crate-idx_parser-0.1 (crate (name "idx_parser") (vers "0.1.0") (hash "19wsa0mrkad6dfk200g41lsxn6qiai9762pr0l47nfvyn9mph2rv") (yanked #t)))

(define-public crate-idx_parser-0.1 (crate (name "idx_parser") (vers "0.1.1") (hash "07f5bcfiqhw3hqd02jrzc0vmffs4xl7642y5xndyvjwn55d0g16b") (yanked #t)))

(define-public crate-idx_parser-0.2 (crate (name "idx_parser") (vers "0.2.0") (hash "1jz2diimr7wryd2p3ax52534z46y0iiwwx3kysmdrjyznv346g0p")))

(define-public crate-idx_parser-0.2 (crate (name "idx_parser") (vers "0.2.1") (hash "0i0v0dp0bnvf7zh8rnra6632csgl2wkwriak7mhlrrfw7hs4ypam")))

(define-public crate-idx_parser-0.3 (crate (name "idx_parser") (vers "0.3.0") (hash "1shinbpp8azplsdrhkxlck0a51z5a15hbv5x98vz7jdpqh2w6b66")))

(define-public crate-idx_sized-0.1 (crate (name "idx_sized") (vers "0.1.0") (deps (list (crate-dep (name "avltriee") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bpq01af63nsgggl1c3rwviwcy5widn642r4s280wwdvl0f77m7r") (yanked #t)))

(define-public crate-idx_sized-0.7 (crate (name "idx_sized") (vers "0.7.0") (deps (list (crate-dep (name "avltriee") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wgcj1s1sd2ypf6qbrfwvqrzzqvh56iik53fj9hjgs5p2ql8hmm6") (yanked #t)))

(define-public crate-idx_sized-0.8 (crate (name "idx_sized") (vers "0.8.0") (deps (list (crate-dep (name "avltriee") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bxi5gka9yfy5m21918kkfjr9npi8ilqbv9x18r81s68mwhkxlnv") (yanked #t)))

(define-public crate-idx_sized-0.8 (crate (name "idx_sized") (vers "0.8.2") (deps (list (crate-dep (name "avltriee") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gf5735f54ya7mqqswidwps5v3vkzlhm60a5kc8dracq3lqqchck") (yanked #t)))

(define-public crate-idx_sized-0.9 (crate (name "idx_sized") (vers "0.9.0") (deps (list (crate-dep (name "avltriee") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "16kzc71ff0341pq26r3nlmzn4f8lwszharwhjdkcyqi7j03gy83a") (yanked #t)))

(define-public crate-idx_sized-0.9 (crate (name "idx_sized") (vers "0.9.1") (deps (list (crate-dep (name "avltriee") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "02lqjnxp5qy1b5vhpv3gqb8v8nf81hn8r3jrbncn0yjgbhgsijx0") (yanked #t)))

(define-public crate-idx_sized-0.9 (crate (name "idx_sized") (vers "0.9.2") (deps (list (crate-dep (name "avltriee") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kbwppn8rx9pyi516wgr7jsxg7kn0hld10lm7q9s2hv1ym8b4pn4") (yanked #t)))

(define-public crate-idx_sized-0.9 (crate (name "idx_sized") (vers "0.9.3") (deps (list (crate-dep (name "avltriee") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kax06hwac6r7yr5qqfigcs7njrx8jl801fa6a6lxf203s9jya8y") (yanked #t)))

(define-public crate-idx_sized-0.9 (crate (name "idx_sized") (vers "0.9.4") (deps (list (crate-dep (name "avltriee") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "0c7bb24l0g322bvakkhql99pn5vaikar0r7hnbsp37bhczghr55z") (yanked #t)))

(define-public crate-idx_sized-0.9 (crate (name "idx_sized") (vers "0.9.5") (deps (list (crate-dep (name "avltriee") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1fw4vc5p9a5jij9wwsyhyrkd4am3ps5aq9im96b87rj4bd1m7d8s") (yanked #t)))

(define-public crate-idx_sized-0.9 (crate (name "idx_sized") (vers "0.9.6") (deps (list (crate-dep (name "avltriee") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1fgn6f60954p06fk7hj9kvlgb8asmj5iisrfqrpvvs3px02sqdwk") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.0") (deps (list (crate-dep (name "avltriee") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "01k3pia7dkxa5x15zj3cyr491jgijj0mqfq10yips47f9mq4qmcq") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.1") (deps (list (crate-dep (name "avltriee") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "17qjqg2hsl0iy8wrwsx1740ia0x5jmddd7fmkvf76zdnk9ds3pk0") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.2") (deps (list (crate-dep (name "avltriee") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0r9r8xmwqm0lvl15abqpj8bwldb857pw65max77jkqpbf11rfblp") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.3") (deps (list (crate-dep (name "avltriee") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "11xivkls7scb98hkwmh4q74696nq4ca0va0j5mdfsq0dxybnafna") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.4") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.3") (default-features #t) (kind 0)))) (hash "16xvfsz8xf6ypv60illc34vz4f5dl1gka9zrdy9khjs8lryx1wfv") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.5") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "0749210wxa6ra9xvjswxr3a2yfznnzsi3kybmnjzj1vqgqij1f1y") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.6") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yvjbpl7jjmxk8lln4kzf95cdza4g4srziwakbyfjnv4l7l9pnrl") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.7") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ln8sncyphcg8xza7dj99gb4nbpnqsy0b825825w9z64yixla117") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.8") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "171kq18wmkb246n297xk7skl93vws3dlg06jhh4pp7b60lbxjjr2") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.9") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "0x7xjpz4aaravsjix2x0nqdclmbxbdk4909k4px2bvsg8117bmql") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.10") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "1a2c8364yqhv0p6ygafkanqjawmm01b0gvalyw9q5vinzshdifc3") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.11") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ixv0r9fbchrf5gavd03f4rjlb0zlwlw4kyqiy7yf499zp8m2qi6") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.12") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "11qs94cz7hcmmg2g9qv89ky298gbrvk3ddx18k9df4ay0jkgzgfs") (yanked #t)))

(define-public crate-idx_sized-0.10 (crate (name "idx_sized") (vers "0.10.13") (deps (list (crate-dep (name "avltriee") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "02m41skp849097vgf0hj03r4n3lk4yn86sf0j17rmcg06jcas9gv") (yanked #t)))

(define-public crate-idx_sized-0.11 (crate (name "idx_sized") (vers "0.11.0") (deps (list (crate-dep (name "avltriee") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bn0bb9hm6ng7vqvaxky786rjv3lgvxnwnj72v4578yh4zqjdbfm") (yanked #t)))

(define-public crate-idx_sized-0.11 (crate (name "idx_sized") (vers "0.11.1") (deps (list (crate-dep (name "avltriee") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yplrn52gzw58cqssq6qbj73iickwv649nd32n7zz6ri8ar6x5m5") (yanked #t)))

(define-public crate-idx_sized-0.11 (crate (name "idx_sized") (vers "0.11.2") (deps (list (crate-dep (name "avltriee") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "040qj7l4idbyaas1605iglb0c1ib64sa3r4cgsp3dnfs0g3iqpa2") (yanked #t)))

(define-public crate-idx_sized-0.11 (crate (name "idx_sized") (vers "0.11.3") (deps (list (crate-dep (name "avltriee") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nh087fxn7bq5zw7m9zzsfmkjilsq2mkckllh5v5zki30vlbrz2s") (yanked #t)))

(define-public crate-idx_sized-0.11 (crate (name "idx_sized") (vers "0.11.4") (deps (list (crate-dep (name "avltriee") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "0q9117ckjsf434dqys9y8lspws7b8q25rd5980p8m5rwa3mxgayv") (yanked #t)))

(define-public crate-idx_sized-0.12 (crate (name "idx_sized") (vers "0.12.0") (deps (list (crate-dep (name "avltriee") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.4") (default-features #t) (kind 0)))) (hash "10j3nhvg7ajsgj449xjbbhfn87fd5ks1zp70jr1yzwigs3hlyfc0") (yanked #t)))

(define-public crate-idx_sized-0.12 (crate (name "idx_sized") (vers "0.12.1") (deps (list (crate-dep (name "avltriee") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.5") (default-features #t) (kind 0)))) (hash "10sla8sg49s6965a16lywagbhy7d13v6n7fdcagmv7mkisans2j9") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.0") (deps (list (crate-dep (name "avltriee") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.6") (default-features #t) (kind 0)))) (hash "1ac28m6c48ncd5i97m1w3jbmfs4h5yagn1l53anavynn81lzc2gw") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.1") (deps (list (crate-dep (name "avltriee") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "15vwv8a9w3c8n8zlymy4nmy0jqbpkc73m98hc66c5zvz8p0pfiv6") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.2") (deps (list (crate-dep (name "avltriee") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0kj2z0xrf25z7sslqfkm3wmrpffv1sarvprnxxhbjkbrhp5y5rx5") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.3") (deps (list (crate-dep (name "avltriee") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "17lhxyrpaadsr2cd8izjp1b3kghfwjddanb3wvnk4sr7x79lx6gv") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.4") (deps (list (crate-dep (name "avltriee") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0a9h2bh54jl8vi62slgpdwm72kwywliadiajiivh5kanj6ipy47c") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.5") (deps (list (crate-dep (name "avltriee") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ms0im0mwyb3jnhhnzywm9blg6vwjzjivnljykj85y242fmf85d2") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.6") (deps (list (crate-dep (name "avltriee") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "1pcim6r7pdm52gyn7drm6x2h00nvz0yqvams9nqlabdbqg9l4sf5") (yanked #t)))

(define-public crate-idx_sized-0.13 (crate (name "idx_sized") (vers "0.13.7") (deps (list (crate-dep (name "avltriee") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "03y9s3s72dkyvfbykjz3yd511jdg9wnjsspahdsykz47a403xpsw") (yanked #t)))

(define-public crate-idx_sized-0.14 (crate (name "idx_sized") (vers "0.14.0") (deps (list (crate-dep (name "avltriee") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "1n2rcrmasmfxg3cvj9lgw7jsanw656iflbjms0wbfn9fqgzyzza5") (yanked #t)))

(define-public crate-idx_sized-0.14 (crate (name "idx_sized") (vers "0.14.1") (deps (list (crate-dep (name "avltriee") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "14d1szbzjjly81llj9gw269lsgfx2as45pbhz8iy9wqz5vqwg6bm") (yanked #t)))

(define-public crate-idx_sized-0.15 (crate (name "idx_sized") (vers "0.15.0") (deps (list (crate-dep (name "avltriee") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "12aajcbzhj9k0m5nr4kznsvzr3v44s43y03lw1ay3cnhpqdfna58") (yanked #t)))

(define-public crate-idx_sized-0.16 (crate (name "idx_sized") (vers "0.16.0") (deps (list (crate-dep (name "avltriee") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "11lac9g9nw0vr9rqbxm7jpvz15igsdlwhjjvc461yn61as98wcc8") (yanked #t)))

(define-public crate-idx_sized-0.16 (crate (name "idx_sized") (vers "0.16.1") (deps (list (crate-dep (name "avltriee") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "1wxc978z5lp57q7y71h0r0vc42b9wk2q1phdjjs5qq506wn5076c") (yanked #t)))

(define-public crate-idx_sized-0.16 (crate (name "idx_sized") (vers "0.16.2") (deps (list (crate-dep (name "avltriee") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "006fwwiwz2ihqq2f2akbbvrgb1k85h2xikpahf4afimmk0jcffb5") (yanked #t)))

(define-public crate-idx_sized-0.17 (crate (name "idx_sized") (vers "0.17.0") (deps (list (crate-dep (name "avltriee") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "001wxd4cniwdbgcr06pfh96pw0pvn60i75lax8fqgh71y2mqw6nk") (yanked #t)))

(define-public crate-idx_sized-0.18 (crate (name "idx_sized") (vers "0.18.0") (deps (list (crate-dep (name "avltriee") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "0i1rdbs9ykfqahx98d5n13bcvhb1janm9sbxm9mgbc087ndwg9rq") (yanked #t)))

(define-public crate-idx_sized-0.18 (crate (name "idx_sized") (vers "0.18.1") (deps (list (crate-dep (name "avltriee") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "10yd81j6mg80ak1vc71jqk2s1vhywb412axfdpys68rrmmz2swf5") (yanked #t)))

(define-public crate-idx_sized-0.18 (crate (name "idx_sized") (vers "0.18.2") (deps (list (crate-dep (name "avltriee") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "0gc8dl49d257piwzx8g6i852igrkxj3a1dybjdr4mxxrxzsbga9m") (yanked #t)))

(define-public crate-idx_sized-0.19 (crate (name "idx_sized") (vers "0.19.0") (deps (list (crate-dep (name "avltriee") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "102bs25sjcdhn4gav2m5ai8m74lkvl1h4llvl8gkscc1l4swjyh3") (yanked #t)))

(define-public crate-idx_sized-0.19 (crate (name "idx_sized") (vers "0.19.1") (deps (list (crate-dep (name "avltriee") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "1rm3c51gsnnlcx6wv2g8vpqaikhqmkhghxjv40m4rpjkzfq0lzb8") (yanked #t)))

(define-public crate-idx_sized-0.19 (crate (name "idx_sized") (vers "0.19.2") (deps (list (crate-dep (name "avltriee") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qlv4cnrmql5xy4xjv31va8dx6mi93fik144qyvfyjrf7d30fvqw") (yanked #t)))

(define-public crate-idx_sized-0.20 (crate (name "idx_sized") (vers "0.20.0") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "0chm5gn05pyb3fj9080l37f49aqis2hsjsih92ck1y402z5hv85j") (yanked #t)))

(define-public crate-idx_sized-0.20 (crate (name "idx_sized") (vers "0.20.1") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.8") (default-features #t) (kind 0)))) (hash "080j1bq05833m8gdd8a36b2h3s23zsm551j8wanzkm3nsg7m9a98") (yanked #t)))

(define-public crate-idx_sized-0.20 (crate (name "idx_sized") (vers "0.20.2") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.9") (default-features #t) (kind 0)))) (hash "148i6x4y2fp04z3zhbiaz32kkrfhvm2cbq7m92ll9vp73qn13bmf") (yanked #t)))

(define-public crate-idx_sized-0.20 (crate (name "idx_sized") (vers "0.20.3") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.10") (default-features #t) (kind 0)))) (hash "009k15ii2c97rrpp93wxignz0c5w7c3cnalssk6kzpb2vr23w6f9") (yanked #t)))

(define-public crate-idx_sized-0.20 (crate (name "idx_sized") (vers "0.20.4") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.11") (default-features #t) (kind 0)))) (hash "1n456xjn7ap5yvbp48a6s4d0khb20rdlff0d8mdg8bxfac86x93p") (yanked #t)))

(define-public crate-idx_sized-0.21 (crate (name "idx_sized") (vers "0.21.0") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.12") (default-features #t) (kind 0)))) (hash "1fx476l9iab91y0b35j7kdkwg6m3l1j8s1rwx7n3nlsrb9km0wab") (yanked #t)))

(define-public crate-idx_sized-0.21 (crate (name "idx_sized") (vers "0.21.1") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.12") (default-features #t) (kind 0)))) (hash "1608q13pq445qwb2g6dcmlabfxpdz16v192dglw8ryhvzwxxblga") (yanked #t)))

(define-public crate-idx_sized-0.21 (crate (name "idx_sized") (vers "0.21.2") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.12") (default-features #t) (kind 0)))) (hash "1564rq8qv23l1nnfb4g0rzp9w536qdpf82zqjzaapd1sk2kaj9pc") (yanked #t)))

(define-public crate-idx_sized-0.21 (crate (name "idx_sized") (vers "0.21.3") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.13") (default-features #t) (kind 0)))) (hash "1ziassdhks6y7x3kqkdnzyxzjfsk6cdzaj8ad2h6qb0dwhbbfmrl") (yanked #t)))

(define-public crate-idx_sized-0.21 (crate (name "idx_sized") (vers "0.21.4") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "0v9gprcgm22qs37jq3izsb0h5ji7x1cvsj3flvyaxm9ql7f79yif") (yanked #t)))

(define-public crate-idx_sized-0.21 (crate (name "idx_sized") (vers "0.21.5") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "04plq757rijpf597f2wss74fqpa2994sdn2r3zzmifxnlm46ccx4") (yanked #t)))

(define-public crate-idx_sized-0.22 (crate (name "idx_sized") (vers "0.22.0") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "1p9v5mf11b8i5li2b47aq1yi2l4ldg7q4pmq07qbn1yl6sx9vifp") (yanked #t)))

(define-public crate-idx_sized-0.22 (crate (name "idx_sized") (vers "0.22.1") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "1ahqzabdr45n7x4pcb18jrzgziyscfpzg7zyx7im6yg0fc2904rl") (yanked #t)))

(define-public crate-idx_sized-0.23 (crate (name "idx_sized") (vers "0.23.0") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "00zw9fjxwqqs231xmxjg07p4j5269jj7qig83dzmc2w7ngb7dwqm") (yanked #t)))

(define-public crate-idx_sized-0.23 (crate (name "idx_sized") (vers "0.23.1") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "09caw3p5yhm9hnirfax1wr3bllmiygsb1f5c6sz0ij38adn5prli") (yanked #t)))

(define-public crate-idx_sized-0.23 (crate (name "idx_sized") (vers "0.23.2") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "05kw37p0c5s1bg76hr42klh6l5myb8wlaljzygjc56sc298j4992") (yanked #t)))

(define-public crate-idx_sized-0.23 (crate (name "idx_sized") (vers "0.23.3") (deps (list (crate-dep (name "avltriee") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "1y10lfj8nky0kzhj5vj75dip9720m7nvc42dfgdrwz9qrb33xd3w") (yanked #t)))

(define-public crate-idx_sized-0.23 (crate (name "idx_sized") (vers "0.23.4") (deps (list (crate-dep (name "avltriee") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.14") (default-features #t) (kind 0)))) (hash "1sxx9cw6bwmbkgz9bz17gib9sn3alcl03kzhfbp2b3rka0f0z2wr") (yanked #t)))

(define-public crate-idx_sized-0.23 (crate (name "idx_sized") (vers "0.23.5") (deps (list (crate-dep (name "avltriee") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "0yqnfjsqq4ql0mvi88igigfgnwxwh2d8gkp4mz5s4cijw321a60q") (yanked #t)))

(define-public crate-idx_sized-0.23 (crate (name "idx_sized") (vers "0.23.6") (deps (list (crate-dep (name "avltriee") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "0kva5c6hw7p6jdh414w4kbndp15q7z2896wvdbd7g7cq8wfkl6iq") (yanked #t)))

(define-public crate-idx_sized-0.24 (crate (name "idx_sized") (vers "0.24.0") (deps (list (crate-dep (name "avltriee") (req "^0.33") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1gwyq7ka91pa2gws2wq4sac821gnw697zbnp96280svqxvbjivdx") (yanked #t)))

(define-public crate-idx_sized-0.25 (crate (name "idx_sized") (vers "0.25.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.37") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1d0w56f9iv9pj9bydzc4wlna7z4x7sjsliapfs6z1m91lqqbfx0q") (yanked #t)))

(define-public crate-idx_sized-0.25 (crate (name "idx_sized") (vers "0.25.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.37") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1wdlhp11k4gyn85wsizch91fa57gkw9kxl5c7s2r7rk3h5bxh6a9") (yanked #t)))

(define-public crate-idx_sized-0.25 (crate (name "idx_sized") (vers "0.25.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.37") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1fmddl64kxi3sh04rkkid2c9rc165hv4y4yyvzarr302jrcrvini") (yanked #t)))

(define-public crate-idx_sized-0.26 (crate (name "idx_sized") (vers "0.26.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.38") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "05r2x5jp8khmchfhf0v1kr87chzvcnfgrsfdsgn0cxwc6b38y967") (yanked #t)))

(define-public crate-idx_sized-0.26 (crate (name "idx_sized") (vers "0.26.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.39") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "09rz83mqxrysa4svpbhgs2w2ji8yyhw0j4pb6db5kx7n4f7lvd3g") (yanked #t)))

(define-public crate-idx_sized-0.27 (crate (name "idx_sized") (vers "0.27.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.40") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1km4jbmvrk43ip939mx94nig1i8r349kkcjvwbyba7dm810f30kw") (yanked #t)))

(define-public crate-idx_sized-0.28 (crate (name "idx_sized") (vers "0.28.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "avltriee") (req "^0.41") (default-features #t) (kind 0)) (crate-dep (name "file_mmap") (req "^0.15") (default-features #t) (kind 0)))) (hash "1501gv66wlmqzhiqr3zlizijl8lx32gr8kj2xq15mpmhnria9456") (yanked #t)))

