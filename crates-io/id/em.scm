(define-module (crates-io id em) #:use-module (crates-io))

(define-public crate-idem-0.1 (crate (name "idem") (vers "0.1.0") (hash "01ny188nm6l419x38c7j20qbhrxwfanw8kvz447r5smslszr30zh")))

(define-public crate-idem-0.2 (crate (name "idem") (vers "0.2.0") (hash "0fhp47n9ap2f6v1fhfrv8hgxiv4h3j70smf45cnaxrfxrxdv6pyj")))

(define-public crate-idem-0.3 (crate (name "idem") (vers "0.3.0") (hash "0mpbpc2bbw3ly2nvav1jpnaij803w4ws92ddf7da7wn6vc9y8qxl")))

(define-public crate-idem-0.3 (crate (name "idem") (vers "0.3.1") (hash "18h9g0k82949y9shcgsv47fdq72d9pvfz9zavqbaa580xxh442rb")))

(define-public crate-idem-0.4 (crate (name "idem") (vers "0.4.0") (hash "0qd6zksc51yfvn33mqq1mnz0q4mlmjgvq6as657b5fdj3msjdjs9")))

(define-public crate-idem-0.1 (crate (name "idem") (vers "0.1.1") (deps (list (crate-dep (name "idem") (req "^0.4") (default-features #t) (kind 0)))) (hash "17hdbx59i6hay5f1l3qldqawbpd1qkgar1w8xh2aa5vmylp2p9kd")))

(define-public crate-idempotent-resource-ops-0.0.1 (crate (name "idempotent-resource-ops") (vers "0.0.1") (deps (list (crate-dep (name "time") (req "^0.3.17") (features (quote ("std"))) (kind 2)))) (hash "0mnvd8rm1b4l63ag8b1qxbfywdzkf72m39rwkll6yqh37c6h5ydm") (features (quote (("std") ("default")))) (rust-version "1.60")))

