(define-module (crates-io id _f) #:use-module (crates-io))

(define-public crate-id_factory-0.1 (crate (name "id_factory") (vers "0.1.0") (hash "1dad61wx88bc500b036ac08yjd1f691w2ldq4rpmyk4hdd7jad1g")))

(define-public crate-id_factory-1 (crate (name "id_factory") (vers "1.0.0") (hash "0jh4kvzavbldmgcznp4nfjwhl20w32ddx6c2iyvvh8gbpmx4k4w1")))

