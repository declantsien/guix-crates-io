(define-module (crates-io id ea) #:use-module (crates-io))

(define-public crate-idea-0.0.0 (crate (name "idea") (vers "0.0.0") (hash "0lvxwv438l15qmrl5qh7mxczpyr93wbw9vm6f85v83ydmym5sbhr") (yanked #t)))

(define-public crate-idea-0.0.1 (crate (name "idea") (vers "0.0.1") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sjps4xv929lisk5k78jlh4r986rzipfpxzn7dirz370il8w55sl")))

(define-public crate-idea-0.1 (crate (name "idea") (vers "0.1.0") (deps (list (crate-dep (name "block-cipher") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.7") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qgwdn1mbfksyndlymmf9g23fhap1kn2mmb15k1wwzmcl0nwzp1d")))

(define-public crate-idea-0.2 (crate (name "idea") (vers "0.2.0") (deps (list (crate-dep (name "block-cipher") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.8") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "0l3w5k5bz65qrk6jdiqksgbwinckglaizzkn69pw1kx7bzyiq7n8")))

(define-public crate-idea-0.3 (crate (name "idea") (vers "0.3.0") (deps (list (crate-dep (name "cipher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.2") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cxcr8a7war3np96f4hlvzxl78wnkyik4pfwpcim29pj9h8lppgw")))

(define-public crate-idea-0.4 (crate (name "idea") (vers "0.4.0") (deps (list (crate-dep (name "cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "035msmqxzy68j824nbfbzi6sxdhfvimc5ccnjgkynjva85q9fsvi")))

(define-public crate-idea-0.5 (crate (name "idea") (vers "0.5.0") (deps (list (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4") (features (quote ("dev"))) (default-features #t) (kind 2)))) (hash "0444ncqf5k7r5aj9n2dgm9mys30dw1bvy9j2wl1qnlixjshzc0lf") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-idea-0.5 (crate (name "idea") (vers "0.5.1") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 2)))) (hash "0xv4hd9mgrwgzfl7cc5nlwyahm9yni5z9dwb3c1z5mqr8h05fm87") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-idea_algorithm-0.1 (crate (name "idea_algorithm") (vers "0.1.0") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)))) (hash "0yhyk00vq87ncgjbq5ppj497wrzjrzaxnd1wcahl8q3841zi4ggg") (yanked #t)))

(define-public crate-idea_algorithm-0.1 (crate (name "idea_algorithm") (vers "0.1.1") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)))) (hash "06hvcfxzvfx98q9kdg3dp4pim4qfbazw6bvxv5av719qpg2b3z6r") (yanked #t)))

(define-public crate-idea_algorithm-0.1 (crate (name "idea_algorithm") (vers "0.1.2") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)))) (hash "1aw1n7b5xd70kgr5mq641h6gidgssv6wbfqqh0mhyf0cpmq4rm0v") (yanked #t)))

(define-public crate-idea_crypto-0.1 (crate (name "idea_crypto") (vers "0.1.0") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0dfin8h0z5n38i1x1vk1g22i6s9cfq0gmr87lhznpwiv52f87p3n")))

(define-public crate-idea_crypto-0.1 (crate (name "idea_crypto") (vers "0.1.1") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0px1h2pfj4jnir7nhrn66v11ln7gvv3szm1iifjnncinp7ln5c41")))

(define-public crate-idea_crypto-0.1 (crate (name "idea_crypto") (vers "0.1.2") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1brgj4rk6jvbadmmn6xmrwgwq4dcq4dd80v9q9f9mahya35lgq63")))

(define-public crate-idea_crypto-0.1 (crate (name "idea_crypto") (vers "0.1.3") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "139m8qn3z11zrjsn2c76qf89rprx9zip36a9pvmz0ahfadcz37f4")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.0") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "10zjkdx4zd6l0pf9b86yfj6c2pi5dqnfjinm8brj30nhyxmn6gpr")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.2") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0najflg99qfy9hs1ga8j2pahdvfxdx4469cf18dxpq44bka54ca2")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.3") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0l6kz10ka12yicb364i0iiivqis84k8asmjp37d4vzb4bq4bn9l9")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.4") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "03arpk3fz2mbfdqd8gg75037j0dwplp5c44z7jqz0fgd7lqpk15b")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.5") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0rwaqfb1ri97gfwqh22h7a3ibggdvys5x7hpgl7skjd9v2fs6l7p")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.6") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1qxgcd73zljspg569npj2blc3wqdrp7zirj4hx7ciddpwnpd7brn")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.7") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1zm9q91p4y8aim42i3pns0gsa34a0fp5g364gphi08sdna0id2h8")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.8") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0gh6x233xl50xb9jh4l354phaxx8h52yh0ndpcgsfcg5yfwaf97n")))

(define-public crate-idea_crypto-0.2 (crate (name "idea_crypto") (vers "0.2.9") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 0)) (crate-dep (name "useful_macro") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "16s2crwhghhccmffry1d3idsiglvpwamn6csc6fsf54a1mapxg3f")))

(define-public crate-ideal-0.1 (crate (name "ideal") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "15s47r863r9w7g23c2cv0ssid7w6q080vl3qih0alq4xxx36k7rf")))

(define-public crate-ideal-0.2 (crate (name "ideal") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)))) (hash "1gf0ziv74j19z00kvnqbg01z4i548pvhb3da9493xrb9czgllpmx")))

(define-public crate-ideal-0.3 (crate (name "ideal") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)))) (hash "19b6b17r9f1fi4fs1z41vdz880hk6q5hsm7vilnv09vhcswf3m2n")))

(define-public crate-ideal-0.4 (crate (name "ideal") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z898qlcrg4ra1m9f6ng92r6w6qjdq6id7gc48l9n4n5i2hn9z2i")))

(define-public crate-ideal-0.5 (crate (name "ideal") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dyjki9zhf1yf088mv4wkj65226w1qcc75idnrq3dqf1kywzsw59")))

(define-public crate-ideal-0.6 (crate (name "ideal") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xy138kw7mwqzv8rnkcshgdqrh9w7v2z5ldz45j64f8vx0rapji1")))

(define-public crate-ideal-0.7 (crate (name "ideal") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vx28fkvi65n06ibcx63zhvimra86hnmjwm383fhyh0kvcklc7l3")))

(define-public crate-ideal-0.7 (crate (name "ideal") (vers "0.7.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1spql92vis1wpzgs1fjsbxxvmwya3ncsckyy7g63kd74q33kn13j")))

(define-public crate-ideal-0.7 (crate (name "ideal") (vers "0.7.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "15c4lb5nsjgd0xrvd2kw7k6gws71nrgjz6vpb7r8rw5b8hlplzhi")))

(define-public crate-ideal-0.7 (crate (name "ideal") (vers "0.7.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jvgqxgqs576linkn6wjh07nk4x67mvib1dr58mmvvy4iw0bfcva")))

(define-public crate-ideal-0.7 (crate (name "ideal") (vers "0.7.4") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dipqvr1f9fk7wwcfjczp21as8y3wlf1h22696adx9y5dw3yppap")))

(define-public crate-idealist-0.1 (crate (name "idealist") (vers "0.1.0") (hash "0wkhnhl5r9ynalxlgmvzv8mfrjf9aikql2xwfd8hv9wvgz7c3fz9") (yanked #t)))

(define-public crate-idealist-0.1 (crate (name "idealist") (vers "0.1.1") (hash "0mslf5750j22zgz2fcks7d0hy37469brmnbg1l7pwrss6a1lf5dn")))

(define-public crate-ideas-0.1 (crate (name "ideas") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "defer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0n121bh25v5dy3inzksfgqh3llhndcii13qxkk0pn154fknbilkv")))

(define-public crate-ideas-0.2 (crate (name "ideas") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "defer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1g487q4mzwds1mi3qwrx2a2nzf24ir81x0my127sgzj9x6fczqd2")))

(define-public crate-ideas-0.2 (crate (name "ideas") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "defer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1dy2vgg0jbmf6wrqp0flrqzdk5bc24rpfqf2gdirsh6c27c3f2pv")))

(define-public crate-ideas-0.2 (crate (name "ideas") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "defer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0j7paw39frcqyr2690c6zqbjs2kmln1xag9aq2mhbs90gwknpjx2")))

