(define-module (crates-io id _g) #:use-module (crates-io))

(define-public crate-id_graph_sccs-0.1 (crate (name "id_graph_sccs") (vers "0.1.0") (deps (list (crate-dep (name "id_collections") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wrzfkdk5p45613hpp2j9gnfbibp5rpkab410r63msrc4sspkjl3")))

(define-public crate-id_graph_sccs-0.1 (crate (name "id_graph_sccs") (vers "0.1.1") (deps (list (crate-dep (name "id_collections") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "08x6ysr6nkfl8jqcf22sh885cg203vl54cpv7zg1lwja5w05msmn")))

