(define-module (crates-io id po) #:use-module (crates-io))

(define-public crate-idpool-0.9 (crate (name "idpool") (vers "0.9.9") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1m33x52z883cskb3k5n14pmz54izsvzy6f3gdhnar28n58vyisnm") (yanked #t)))

(define-public crate-idpool-1 (crate (name "idpool") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (kind 0)))) (hash "0sw500gqs1a6iyypig5c9jznjjji42b2mrv8lcr05mlpwm235v2b") (yanked #t)))

