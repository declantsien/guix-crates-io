(define-module (crates-io yd ev) #:use-module (crates-io))

(define-public crate-ydev-0.1 (crate (name "ydev") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "jlogger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "0r0zd81h6cv6ym8cq3qc6g2x3as10xzpfqbaypa8y3h0l6xx9bpz")))

(define-public crate-ydev-0.1 (crate (name "ydev") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "jlogger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0kfq6bc4i26s64bqwbdmzld3212rmd8wd5d2gmv9r7v7f0fqzw9j")))

