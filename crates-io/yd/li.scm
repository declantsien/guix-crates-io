(define-module (crates-io yd li) #:use-module (crates-io))

(define-public crate-ydlidar_driver-0.1 (crate (name "ydlidar_driver") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.16") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.120.0") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 0)))) (hash "1b51rf87rm76j3gshqvl6dlm6syz23pblrbfs758z5mgxw44jnfd")))

