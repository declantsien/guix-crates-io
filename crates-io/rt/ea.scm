(define-module (crates-io rt ea) #:use-module (crates-io))

(define-public crate-rtea-0.1 (crate (name "rtea") (vers "0.1.0") (deps (list (crate-dep (name "rtea-proc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13ycnv130dpf9fcd1yqphqbfplmcrbagwxmj49sqac4c0kpj3qr8")))

(define-public crate-rtea-0.1 (crate (name "rtea") (vers "0.1.1") (deps (list (crate-dep (name "rtea-proc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ww5j19c5p9qb73c7f8v8824p2pghnh9lj7mnik183bsfxv70v7g")))

(define-public crate-rtea-0.2 (crate (name "rtea") (vers "0.2.0") (deps (list (crate-dep (name "rtea-proc") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0if4bf1iy6901gzdg8cd0cdh2953xrp0c27gn9azdjw3j041pa3i")))

(define-public crate-rtea-proc-1 (crate (name "rtea-proc") (vers "1.0.0") (hash "09p4qfbrqfdraz03l29gzc7xgq2wzm8m2jky12jsl8grci664hin")))

(define-public crate-rtea-proc-1 (crate (name "rtea-proc") (vers "1.0.1") (hash "0ghs4vsq1r587xs1zdjkkj2c2j15811nxzpk8a5275npdbpfvnmv")))

(define-public crate-rtea-proc-2 (crate (name "rtea-proc") (vers "2.0.0") (hash "1bnmbm8gsi5jsgrj0jncpcg5lf3cy14jigk5lmrn1c8bf3nvm5lb")))

