(define-module (crates-io rt or) #:use-module (crates-io))

(define-public crate-rtorch-0.0.1 (crate (name "rtorch") (vers "0.0.1") (hash "0707vyd7n6p28zx8nahf275fdwnlzc9s5n04yf98fls3sgi6vyij")))

(define-public crate-rtorrent-utils-1 (crate (name "rtorrent-utils") (vers "1.0.0") (deps (list (crate-dep (name "rtorrent-xmlrpc-bindings") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1f2ic72vngz87cy5d03wvgs5qv4c86g4nkk5y62x58q9vdhnylcq")))

(define-public crate-rtorrent-utils-1 (crate (name "rtorrent-utils") (vers "1.0.1") (deps (list (crate-dep (name "rtorrent-xmlrpc-bindings") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0jbzzdl8f1q8yggdqjq7i7pwzzq2zkjlimn49w25dvc5m0jcnij0")))

(define-public crate-rtorrent-utils-1 (crate (name "rtorrent-utils") (vers "1.0.2") (deps (list (crate-dep (name "rtorrent-xmlrpc-bindings") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0xil66h3zzpf4zvmjzl9f0v5vr66hbgkh5j29jwxwqs82p8cvwjj")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.1.0") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1mgbyxhdab19vl2qabayziqfx54b3apjfiz065igj9aaa1yz0xgj")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.1.1") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0gaz7vkyzac957h4bj972xn8s2z6bcp85a0j5378gyy5mzsc7sbc")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.1.2") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0x6qca7f77zdsfhhqmr0a490jdn9fykp6fqac3zzw3bc0yz4wqvy")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.1.3") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0aky1gpplyxsaf7qbwvdwiy40irkpyx9azgvhm5973v29bxbfi4l")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.1.4") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1li0w9ljb8fbxglbhmymydrmvqwh49fnz7fldharwmpzjpk9br3m")))

(define-public crate-rtorrent-xmlrpc-bindings-0.1 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.1.5") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0kxp1v3296qdi92z3m5402w4wfqjfs64jb6jb1dch413zjpnxv6g")))

(define-public crate-rtorrent-xmlrpc-bindings-0.2 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.2.0") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "00pi5ydbzkf9mxwlyi5p4in4zs05aahykcyiyyd8br4wa52y4m7z")))

(define-public crate-rtorrent-xmlrpc-bindings-0.2 (crate (name "rtorrent-xmlrpc-bindings") (vers "0.2.1") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0yfkaqxkk16ihvn9l7zc1sf1a509vqghhbv78hamzzak2k1drpvm")))

(define-public crate-rtorrent-xmlrpc-bindings-1 (crate (name "rtorrent-xmlrpc-bindings") (vers "1.0.0") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1rby1c5s3c4fmk3g3vnf6ic273w8wxl4hlp967zd0fk390h0z4iw")))

(define-public crate-rtorrent-xmlrpc-bindings-1 (crate (name "rtorrent-xmlrpc-bindings") (vers "1.0.1") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1hxrk2544w8zscknmjpd666l0jfc951dw76vglmhqg30yvvwvsxp")))

(define-public crate-rtorrent-xmlrpc-bindings-1 (crate (name "rtorrent-xmlrpc-bindings") (vers "1.0.2") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "02a3q0lw2skqjjnmxcm449fx740n9s76c18vk413slwy6k75k317")))

(define-public crate-rtorrent-xmlrpc-bindings-1 (crate (name "rtorrent-xmlrpc-bindings") (vers "1.0.3") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1fdx5qx5rindffnpg9qd4dvjixffajvxm8ak3a3l03nyb9kmjwfr")))

(define-public crate-rtorrent-xmlrpc-bindings-1 (crate (name "rtorrent-xmlrpc-bindings") (vers "1.0.4") (deps (list (crate-dep (name "xmlrpc") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0id5a3kx18zg2f6z6ga5dg2pg9ywc8afl1rcccxd2p4xcxfgrfpl")))

(define-public crate-rtorrent_xml_rpc-0.1 (crate (name "rtorrent_xml_rpc") (vers "0.1.0") (hash "1vh94qzlqbid3n020b629wm0zyk1aj1yqiqm56z3ffdmr4nmn3ji")))

(define-public crate-rtorrent_xml_rpc-0.1 (crate (name "rtorrent_xml_rpc") (vers "0.1.1") (hash "1slnl84bqkb5ay1367b4s31p6g6byhnxmwkmfbs3fligkxyx8zi0")))

(define-public crate-rtorrent_xml_rpc-0.1 (crate (name "rtorrent_xml_rpc") (vers "0.1.2") (hash "1vzcm9cjpvaihqi8rfq2a8idiwl7v1v4rc4q4m3pq5rl042x04cb")))

(define-public crate-rtorrent_xml_rpc-0.1 (crate (name "rtorrent_xml_rpc") (vers "0.1.3") (hash "18cwldglnskvim6kf368zm61s4dcp3k4dqqc9f7ckwgybfzrcclj")))

(define-public crate-rtorrent_xml_rpc-0.1 (crate (name "rtorrent_xml_rpc") (vers "0.1.4") (hash "0qa315adwfz94g1745pd0x51q9l5n63agvfmjarp3fbxh5vcfsvb")))

