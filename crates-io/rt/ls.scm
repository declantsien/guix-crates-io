(define-module (crates-io rt ls) #:use-module (crates-io))

(define-public crate-rtls-0.0.0 (crate (name "rtls") (vers "0.0.0") (hash "0db3l664pzhfpy885mcvirnnvvsqf95gpq263m6m081ycglvyca1")))

(define-public crate-rtlsdr-0.0.1 (crate (name "rtlsdr") (vers "0.0.1") (hash "13v8ynx0marqxfs82rkzvirlxkjg54dv53p6if6y52l03jnjza4z")))

(define-public crate-rtlsdr-0.1 (crate (name "rtlsdr") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "04pf941xcwc6nckvgyqd5p4qppvvjpsmpk22qhsi1pn4zkbs42dn")))

(define-public crate-rtlsdr-0.1 (crate (name "rtlsdr") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xh4fl43zmifwg9z4qfm38ggqjdjrx4nzwy9m2rdh669gdg2s8v9")))

(define-public crate-rtlsdr-0.1 (crate (name "rtlsdr") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qbqp998ydfxj2s53bjrgg031jp2vfqpfqkl6s6dwi79rgnd2gc2")))

(define-public crate-rtlsdr-0.1 (crate (name "rtlsdr") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fi566gk22dsg0fx743xyxkvhg5r6qpf0gm44p6ya43k28sxf28a")))

(define-public crate-rtlsdr-0.1 (crate (name "rtlsdr") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gpvrp3pjkr464wf0jgc48383xm7ypcgw5kr4y71ip8fdgjaasqk")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.0") (hash "10mhiwh3wqp9nlqhh9rm21ykx2x9z1m1fn9pbdkdrdcmg9i1pgi0")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.1") (hash "107wvgf99wqb9gr3kfg84q10ffqchjz94xrgg2k98sw2a5nw8gmr")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.2") (hash "0dz477mx851s860llw64n5ya6qwxgvkb2h0avx6749k5d53xg5lh")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.3") (hash "0x298ny3zwbsrxrwmip4xm5k7wcix3k7ndqffgp6d4kimrjn6szl")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.4") (hash "085mhq736fvjfn6xl3jrmsdh5ssh4hqn28i0x5wwyzmd0slvfm0a")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.5") (hash "0k13hax3m2x03jp6r60crbhayxzqv7lbq6l316b2762j6rn21mrq")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.6") (hash "15rhkdjznwjphbx3830jywmkjcg9cqgkpap20y88h1s0402b88j8")))

(define-public crate-rtlsdr-full-0.1 (crate (name "rtlsdr-full") (vers "0.1.7") (hash "1c7rw22qjz6lbxpb8fp8m5msn0nn5mr94brwz3jqry3mj7f0mbpd")))

(define-public crate-rtlsdr-rs-0.1 (crate (name "rtlsdr-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.42.2") (default-features #t) (kind 1)))) (hash "0ka76ish4fslah5xqpgqqrdcdpgyn39za4c13zpby9b4n7fn9fph")))

(define-public crate-rtlsdr_iq-0.1 (crate (name "rtlsdr_iq") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "0pqsa11c4faf6ajcfs7yall7p8lhnf1imfhgiiis8bjqwsfb3655")))

(define-public crate-rtlsdr_mt-2 (crate (name "rtlsdr_mt") (vers "2.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rtlsdr_sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "10bxg6n4bvzzwvpkd7j7vmy5v354h4z6h89wbxyf7rrmnnc49l1j")))

(define-public crate-rtlsdr_mt-2 (crate (name "rtlsdr_mt") (vers "2.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rtlsdr_sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1l35qszccxvpdazkw48v1ggxgj6zr8z60xdyan00spcm4grngk6c")))

(define-public crate-rtlsdr_mt-2 (crate (name "rtlsdr_mt") (vers "2.1.0-rc0") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rtlsdr_sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0wjzp1l13lkfc28h020cgvb1zmxw2ryzc6wjykls1prgi9x0d4f9")))

(define-public crate-rtlsdr_mt-2 (crate (name "rtlsdr_mt") (vers "2.1.0-rc1") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rtlsdr_sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1r32sb37fnw7v1b217rlbb79z8492sqx0b12bb0c7x6hi3n8bsry")))

(define-public crate-rtlsdr_mt-2 (crate (name "rtlsdr_mt") (vers "2.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rtlsdr_sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0d00bqlakbz59ww753gsgly07f1ryn52viisbj6im0360d5lpsv2")))

(define-public crate-rtlsdr_mt-2 (crate (name "rtlsdr_mt") (vers "2.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rtlsdr_sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0njy848rq9l43pglvz64k8zbara4snr5vfrwva85315wyy5l5bby")))

(define-public crate-rtlsdr_sys-1 (crate (name "rtlsdr_sys") (vers "1.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "14zib6mr1d2a4swh2dgxzplmp4kgan3fh366pc2ps6rzhc8p7skg")))

(define-public crate-rtlsdr_sys-1 (crate (name "rtlsdr_sys") (vers "1.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "0rss4pnb8gxhi1j4wm28zlkj9kp3n5g0slz7wh4x6rzbjdy7c42v")))

