(define-module (crates-io rt mi) #:use-module (crates-io))

(define-public crate-rtmidi-0.1 (crate (name "rtmidi") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "1ppqx4404f243csv92g2cryddvpir1da59b4j3sh7d4iccv9k0yl")))

(define-public crate-rtmidi-0.2 (crate (name "rtmidi") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "043hg0p8g14y6f093m6fyikg4nbkbk8prd9v6fxcwarsf1iny6ry")))

