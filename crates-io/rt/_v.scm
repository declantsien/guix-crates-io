(define-module (crates-io rt _v) #:use-module (crates-io))

(define-public crate-rt_vec-0.1 (crate (name "rt_vec") (vers "0.1.0") (deps (list (crate-dep (name "rt_ref") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0098jlmcgxppyyaz3lp161v7sf0x40hpqdrkzmrh9rcxfm73i9b4")))

(define-public crate-rt_vec-0.1 (crate (name "rt_vec") (vers "0.1.1") (deps (list (crate-dep (name "rt_ref") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "00jbbkiv15n9h4b40198b44q3qbjwd360npm3ngwmxc17cj7iicc") (features (quote (("unsafe_debug" "rt_ref/unsafe_debug"))))))

