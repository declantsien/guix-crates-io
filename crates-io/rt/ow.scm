(define-module (crates-io rt ow) #:use-module (crates-io))

(define-public crate-rtow-0.1 (crate (name "rtow") (vers "0.1.0") (deps (list (crate-dep (name "chan") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1h9ir7k3pz5jg9k9mzp13wrkd5jfhrp0vpnrbfrl4mplqf56dlbp")))

