(define-module (crates-io rt ra) #:use-module (crates-io))

(define-public crate-rtrace-0.1 (crate (name "rtrace") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "docopt_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "109jxdarczxxqsbs87dysw4m0jcinqjg3v9w66i8wd81s78lx5qx")))

(define-public crate-rtrace-0.2 (crate (name "rtrace") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1shg9a3sfjw0ckg5cwgq2q4rvhn3jx2914d9gvq1f64n3c29z8k6")))

(define-public crate-rtrace-1 (crate (name "rtrace") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "1ihia8sag6pv778lgxkkxskv1hqmrqswbh4ik8p0103yx4ykkb15")))

(define-public crate-rtrade-0.0.0 (crate (name "rtrade") (vers "0.0.0") (hash "0vvlfgm2v412m09b5l0qi0z5xxfg1zinfzsf7rdyak3hg11kbjb6")))

(define-public crate-rtran-0.1 (crate (name "rtran") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13i7p4fd2szg5bzw7hg1ikxiqg4bhqmwfhy0pms3y5v3y1zrb9fl")))

(define-public crate-rtrans-0.1 (crate (name "rtrans") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^0.7.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0qkqwm0za59rxc4z920dfwshp1140svj377dw9qci56f39l4fs41")))

