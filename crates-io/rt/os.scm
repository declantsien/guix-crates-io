(define-module (crates-io rt os) #:use-module (crates-io))

(define-public crate-rtos-0.0.1 (crate (name "rtos") (vers "0.0.1") (hash "1ixf7i73cj9xf4p8495q6ql01ks2r52f5hjfgnnmx8jpr92k4jbv")))

(define-public crate-rtos-trace-0.1 (crate (name "rtos-trace") (vers "0.1.0") (hash "1l57raxlg4v55kngpfr7bw628ckl4r9rap8y44s75s4jkcqzam3n") (features (quote (("trace_impl") ("default" "trace_impl")))) (yanked #t)))

(define-public crate-rtos-trace-0.1 (crate (name "rtos-trace") (vers "0.1.1") (hash "1wzxi60x8ydy0ijs2p2dxnp6ckz7j9phixhxlyqhgl0qp05idmgk") (features (quote (("trace_impl") ("default" "trace_impl")))) (yanked #t)))

(define-public crate-rtos-trace-0.1 (crate (name "rtos-trace") (vers "0.1.2") (hash "18xbkjpmkfd9ynk6kg0rvikvsrcirfpzvfhdq2mzw2p9dp43clb3") (features (quote (("trace_impl") ("default" "trace_impl"))))))

(define-public crate-rtos-trace-0.1 (crate (name "rtos-trace") (vers "0.1.3") (hash "15w0zcqnb9vakisvkscdy0vjm6y4wsi2y4br76dh9wkl6gnbrdnd") (features (quote (("trace_impl") ("default" "trace_impl"))))))

