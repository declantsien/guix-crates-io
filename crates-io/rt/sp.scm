(define-module (crates-io rt sp) #:use-module (crates-io))

(define-public crate-rtsp-0.1 (crate (name "rtsp") (vers "0.1.0") (hash "1gxgrs8lhyb2yv02q0hisazkzlpkn9qsxr0v3sqx60lx4sk78jly")))

(define-public crate-rtsp-mock-0.2 (crate (name "rtsp-mock") (vers "0.2.0") (deps (list (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "gstreamer") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "gstreamer-rtsp-server") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0lmbgajzi8ydzsr8kqcznfak9nd99krvzc08zs1xkd2msf701a3i")))

(define-public crate-rtsp-rtp-rs-0.1 (crate (name "rtsp-rtp-rs") (vers "0.1.33") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "openh264") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1bvqb7dknln66hzrc6dbk68ncs31kjawvri96xi14iwi9lg4rx82")))

(define-public crate-rtsp-rtp-rs-0.1 (crate (name "rtsp-rtp-rs") (vers "0.1.34") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "openh264") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "01ac6l8pw9l7p1ib9sbddd6r75hamxbfrfqclhqcfnhpx21y51a4")))

(define-public crate-rtsp-rtp-rs-0.1 (crate (name "rtsp-rtp-rs") (vers "0.1.35") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "openh264") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0nrbv3njjxcnkcakw3ckib6gpnx6qfyjsq4xd8ajcxij8a7zi80b")))

(define-public crate-rtsp-server-0.0.0 (crate (name "rtsp-server") (vers "0.0.0") (hash "0sgiada9awcqs14agj0mkwsw7q9zp01c1hfpwlpqvhqc9nfjdgkz")))

(define-public crate-rtsp-types-0.0.0 (crate (name "rtsp-types") (vers "0.0.0") (hash "06lnsq3h861i75gq8ymlyq22xcdb601paiypcjigz0h8yzvhjpa7")))

(define-public crate-rtsp-types-0.0.1 (crate (name "rtsp-types") (vers "0.0.1") (deps (list (crate-dep (name "cookie-factory") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "11lfgi51iz9p814hi3bpvpvw95agvvbhh56x4f66mik7l0ppilxw")))

(define-public crate-rtsp-types-0.0.2 (crate (name "rtsp-types") (vers "0.0.2") (deps (list (crate-dep (name "cookie-factory") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "0v8x57d0zq2pdv5ipfmzb3hzrmzg045rp7xih48xc25jiii7grlh")))

(define-public crate-rtsp-types-0.0.3 (crate (name "rtsp-types") (vers "0.0.3") (deps (list (crate-dep (name "cookie-factory") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "0xagmf455fwrdcm8706arh32d0v96qjhlapv12f4f36hshi82rkv")))

(define-public crate-rtsp-types-0.0.4 (crate (name "rtsp-types") (vers "0.0.4") (deps (list (crate-dep (name "cookie-factory") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "0d8dmz0sayhamqq7k1ig9nd3qhdxfmzy11sxvpsdci15p95f1jyh")))

(define-public crate-rtsp-types-0.0.5 (crate (name "rtsp-types") (vers "0.0.5") (deps (list (crate-dep (name "cookie-factory") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "1byy5lm4ikj5cwb2y6v5n6n3j540f1bqhcj0q62yr3axvj8fq6ia")))

(define-public crate-rtsp-types-0.1 (crate (name "rtsp-types") (vers "0.1.0") (deps (list (crate-dep (name "cookie-factory") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "1f91rqg9w7jl2z8c0mizm3av3dxls0ly1agskd2vfkwg7cri874r")))

(define-public crate-rtsp-types-0.1 (crate (name "rtsp-types") (vers "0.1.1") (deps (list (crate-dep (name "cookie-factory") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "0k27b6gv5j15prsryi0ywnnpnrqwvxyry0praxharwb3mzh12p3c") (rust-version "1.58")))

(define-public crate-rtsparse-1 (crate (name "rtsparse") (vers "1.0.0") (deps (list (crate-dep (name "pico-sys") (req "^0.0") (default-features #t) (kind 2)))) (hash "1vxq800p3579r03n93lqmx8c1iziiwvii7kal3368s314vixbbr7") (features (quote (("std") ("default" "std"))))))

