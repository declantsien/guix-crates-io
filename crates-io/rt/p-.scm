(define-module (crates-io rt p-) #:use-module (crates-io))

(define-public crate-rtp-parser-0.1 (crate (name "rtp-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "0i0xj4zdjxplcl7r3if021hg4yb5r1c3xgk70hl55lidnqikddxb")))

(define-public crate-rtp-rs-0.1 (crate (name "rtp-rs") (vers "0.1.0") (hash "0i45i0lqjmj694kzfa4crl08wy2jfvm2b3avkm8bvs5nzkc08kq5")))

(define-public crate-rtp-rs-0.2 (crate (name "rtp-rs") (vers "0.2.0") (hash "0qc1dqr03n4bhzq0gbll6d581n570mviak5xbhwx8shbk9fg71mw")))

(define-public crate-rtp-rs-0.3 (crate (name "rtp-rs") (vers "0.3.0") (hash "132r7ib2j1s0pk4k113k388cqxz1ry0kw725fzii0xi0kwbv33g7")))

(define-public crate-rtp-rs-0.4 (crate (name "rtp-rs") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1fr0fwi9sglb01503hvfd71j7060v1kb5zib1lic21prwxxlhpyp")))

(define-public crate-rtp-rs-0.5 (crate (name "rtp-rs") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1x8pgqbwwqrqnp2ykzdva7inmn7jzcgr42fy07lldm4ka5lhs4g1")))

(define-public crate-rtp-rs-0.6 (crate (name "rtp-rs") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ma365x07n8k5nkwzc96wd5fchrgnkhlsspz9i1w8dixbd52gvfl")))

(define-public crate-rtp-types-0.0.1 (crate (name "rtp-types") (vers "0.0.1") (deps (list (crate-dep (name "smallvec") (req "^1") (features (quote ("union"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0s52881y5mwvwwgb2wl6bv6pj31nn90vjsf3z8nd5l40p1jsl97w")))

(define-public crate-rtp-types-0.1 (crate (name "rtp-types") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1") (features (quote ("union"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0xq28wxm42z1gqqy1aydm9ipg1a7hlxqlkk7bhlpd6gfphdx7aip")))

(define-public crate-rtp-types-0.1 (crate (name "rtp-types") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^1") (features (quote ("union"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1bzr6d6mbrl36l3lz00576v5rv44vr5971k7hwl5chnrgyxkh6z0") (rust-version "1.57.0")))

