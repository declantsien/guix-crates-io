(define-module (crates-io rt ft) #:use-module (crates-io))

(define-public crate-rtftp-1 (crate (name "rtftp") (vers "1.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "16x7048jc1aq9jzw6gjzds55ixk76l0vf458xdq7mqb3pdl8ig37")))

(define-public crate-rtftp-1 (crate (name "rtftp") (vers "1.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cq9l2c1jm8r15m23rhlmg63sqypi6qsb867bgc66m3q58anky61")))

