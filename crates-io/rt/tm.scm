(define-module (crates-io rt tm) #:use-module (crates-io))

(define-public crate-rttmon-0.1 (crate (name "rttmon") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "0bqc6sfx5hmfd8yhg94wmv38w9x4dmdl3grjw8lhj3m2pkzvcc15")))

(define-public crate-rttmon-0.1 (crate (name "rttmon") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "1qvpmrz188xfrwsl613p674li98s5fq3hfrn8bfd1wzmjrfxaiff")))

(define-public crate-rttmon-0.1 (crate (name "rttmon") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "11j62z678xnxkxf1agdhpifa4g7ylf362fybm1qvlrf6k4dm0fgc")))

(define-public crate-rttmon-0.1 (crate (name "rttmon") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "032dsm5xryqn14dgmnvxhn124pv3ncycc8gd9isfwps88hddqrk5")))

(define-public crate-rttmon-0.2 (crate (name "rttmon") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "134kpg2nv0hk96bnyvfkb8sn0h1vj4k7wh5xfrc7ks3v4yh5l5vn")))

(define-public crate-rttmon-0.2 (crate (name "rttmon") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "0fc9ghsndkb4gsz4sk8hy251i929fyw80yznpm74y19fdh0wvw9y")))

(define-public crate-rttmon-0.2 (crate (name "rttmon") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "0sy5chnhzmn1475y00wgc70xz38mqf34v4sfvv9sqmkiwcshb5cp")))

(define-public crate-rttmon-0.2 (crate (name "rttmon") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "04anckip8rrjy77paj3f3shv4qqpjxvpcx5hi4x27xpawylbb4da")))

