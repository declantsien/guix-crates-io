(define-module (crates-io rt -w) #:use-module (crates-io))

(define-public crate-rt-watchdog-0.1 (crate (name "rt-watchdog") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pklsy9rmmh30llh1wcfhzar2wjbbamcj5l4nw42ywskkq3zh699")))

