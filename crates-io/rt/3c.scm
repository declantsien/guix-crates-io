(define-module (crates-io rt #{3c}#) #:use-module (crates-io))

(define-public crate-rt3conf-0.1 (crate (name "rt3conf") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-big-array") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0ns6vjwwj4cd6cw8lc1py498fya6q8117jri41skr994ih1vm3c6")))

