(define-module (crates-io rt f-) #:use-module (crates-io))

(define-public crate-rtf-grimoire-0.1 (crate (name "rtf-grimoire") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.2") (default-features #t) (kind 0)))) (hash "1s5ri8wgd6jfmf7qd1y7ssb1y4d1a8zi85m23dbpswqf3h5av7r8")))

(define-public crate-rtf-grimoire-0.1 (crate (name "rtf-grimoire") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^4.2") (default-features #t) (kind 0)))) (hash "1sidp6ink6xz83ljgiq6mmgqibd9dhcf7wjf87hjm42lhzia8hng")))

(define-public crate-rtf-grimoire-0.2 (crate (name "rtf-grimoire") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1hc30nd1ksky822m3vmdwbgrsdqsnsm8ycfmlv2mlh12nksgny7k") (rust-version "1.58")))

(define-public crate-rtf-grimoire-0.2 (crate (name "rtf-grimoire") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1ld4vcd1qjxwgqdfj0wysrx2q1zhfk0mbdmzz6sayd9wpkp6rmly") (rust-version "1.58")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.0") (hash "0klz508xbxlzr1062jls8hqzp3byimb66p5l11y2p3ac2vzw7b5s")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.1") (hash "00k478xv877inkkxqv42g3mq5igw05az2mr2yh74vdgrp1njr1q2")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.2") (hash "05rv7yfhlynq6lq5bzl6anffin1knnfna4b7bxabc8abl00xwibw")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.3") (hash "028vigxcjfbyi21150767y9i9y3mrdsvm2zykjdsy2bjrvvbmpmf")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.4") (hash "0rbw0n1n1r8ylvzgsrbg6a3jrgi4ml3sgs40kqqwr487qbamy5jw")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.5") (hash "0hymdl0faz9yy311v5i80v0mydzn6sczhmigx05pp0rr98pqq10d")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.6") (hash "05xcqnvaym0p93k8631zvkrl0z3a5yc5cl75g3b2s9wnl7gj818r")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.7") (hash "0cm8zg69vdfmakq2ajjrvdyf7jnaiax6rqn9zxgxkjk64bpppvmy")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.8") (hash "1sb7j2z6jxf1inhmzhlfr56d166p3019pdy2plym0r85a359vji9")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.9") (hash "1j65c70fwrskyhkhqb5prmrvf49pzgqc6njd7dga1xyhzw4si88a")))

(define-public crate-rtf-parser-0.1 (crate (name "rtf-parser") (vers "0.1.10") (hash "0q7k09fqprfd29gm0pg0ia0x9iaak49ylzxin38qjr573kp9pl9l")))

(define-public crate-rtf-parser-0.2 (crate (name "rtf-parser") (vers "0.2.0") (hash "16rgx85wgbr17jd16f8yqychk1c1h4516pzxw0rscglkysad1bg0")))

(define-public crate-rtf-parser-0.2 (crate (name "rtf-parser") (vers "0.2.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1x5m3sszkc77jpa4mv57748m370hdvpz66v2jb2b6j2bzwx1iwx5") (features (quote (("serde_support" "serde"))))))

(define-public crate-rtf-parser-0.2 (crate (name "rtf-parser") (vers "0.2.2") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "086ci513frajk7waq1dpc7z0wlkpwwlrwd09dk88s9p3f41jijr2") (features (quote (("serde_support" "serde"))))))

(define-public crate-rtf-parser-0.3 (crate (name "rtf-parser") (vers "0.3.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1i29av8xkxv05kvhbg8afjx9a9vlr5id3gl2jbbs6whsyk4xylzk")))

