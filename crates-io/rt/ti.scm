(define-module (crates-io rt ti) #:use-module (crates-io))

(define-public crate-rtti-0.1 (crate (name "rtti") (vers "0.1.0") (hash "0365idc184cg7h1y4z72xa8vyxrwx0zm6dhgp55mv5h9kbwhvq68")))

(define-public crate-rtti-0.2 (crate (name "rtti") (vers "0.2.0") (hash "1k3ys1f337sdip9cac0kf8n97hjszv2vj9if8gwxf2c5qvk9zyyp")))

(define-public crate-rtti-0.3 (crate (name "rtti") (vers "0.3.0") (hash "1jcfr3v5asyznjg8mv93qv2qyapaxarbkcmb8qanclgxcqn8dvzp") (yanked #t)))

(define-public crate-rtti-0.3 (crate (name "rtti") (vers "0.3.1") (hash "02d64m3sv8n2wf77332fbyjd9v37zcc1d8k9sr9kz5rmydpxs34x")))

(define-public crate-rtti-0.3 (crate (name "rtti") (vers "0.3.2") (hash "0b6ylxxhmvfwz15xpsfqnn9plnfks0ypc5sqhyggyyb5pdbpw9hm")))

(define-public crate-rtti-0.4 (crate (name "rtti") (vers "0.4.0") (deps (list (crate-dep (name "rtti-derive") (req "^0.4") (default-features #t) (kind 2)))) (hash "1wdm14r6yi0mk1pnzba4qfvyl4003fbahnkig3xiix5q7pslmqa1")))

(define-public crate-rtti-derive-0.1 (crate (name "rtti-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.13") (default-features #t) (kind 0)))) (hash "0xqi7cpaqbk4k7ba0k348vyg35yp604a585fv9axc7qjf7r983lx")))

(define-public crate-rtti-derive-0.2 (crate (name "rtti-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.13") (default-features #t) (kind 0)))) (hash "1krj6grp2x30gynizvkxj2jnvc6wcppkbb82n542gkhdzww02mgk")))

(define-public crate-rtti-derive-0.3 (crate (name "rtti-derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.13") (default-features #t) (kind 0)))) (hash "060awfzx82clqxyyn7mrx7bplivnhf4ri1mg9c8ncqz0h6zmp1m6")))

(define-public crate-rtti-derive-0.4 (crate (name "rtti-derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.13") (default-features #t) (kind 0)))) (hash "1xy5wijrb8yvwgm72yx7sgn1yh2ij8pcyphhxjg17vsb4fzli157")))

