(define-module (crates-io rt l_) #:use-module (crates-io))

(define-public crate-rtl_reshaper_rs-0.1 (crate (name "rtl_reshaper_rs") (vers "0.1.0") (deps (list (crate-dep (name "arabic_reshaper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "unic-bidi") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0c8d3yv3j017jjfxcpj99ymcvdw1405rls97kmwzkharsnakqz1r")))

