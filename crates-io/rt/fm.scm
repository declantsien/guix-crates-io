(define-module (crates-io rt fm) #:use-module (crates-io))

(define-public crate-rtfm-0.1 (crate (name "rtfm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "opener") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "04n2yfsznsxjfcapx1n12x0yd85hxg5jzwjsc3yyq9dbqgx7lmn9")))

(define-public crate-rtfm-core-0.1 (crate (name "rtfm-core") (vers "0.1.0") (deps (list (crate-dep (name "static-ref") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "17dwryw9c8flf9m854nanx597naah6aa8953gr4yvw7wcivn6yqs")))

(define-public crate-rtfm-core-0.2 (crate (name "rtfm-core") (vers "0.2.0") (hash "0hdlpr04kpzpk6xkda307khnxqrkc4qkk7257qmpinwmm06l9fhi")))

(define-public crate-rtfm-core-0.3 (crate (name "rtfm-core") (vers "0.3.0-beta.1") (hash "1hgkgh54fqh55cywy10kkjkz1q98hibk1wcak9lcbfvfz3sgzdyl") (yanked #t)))

(define-public crate-rtfm-core-0.3 (crate (name "rtfm-core") (vers "0.3.0-beta.2") (hash "190znx9d297x6npin4nx03zgj2dgrapz8fvkm8928v45dbpa1g9b") (yanked #t)))

(define-public crate-rtfm-core-0.3 (crate (name "rtfm-core") (vers "0.3.0") (hash "14rllrjshq2vrmkvnhprycfbnzialbz6z2cl1cr70nxanbnr7j4y")))

(define-public crate-rtfm-syntax-0.1 (crate (name "rtfm-syntax") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "1fvwrdrzhbayyc7av0sc4x94nx678ddj746hia3i7mc3729sbm9n")))

(define-public crate-rtfm-syntax-0.2 (crate (name "rtfm-syntax") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "18ff07dc7v7dlxhn748vfh04nwsyrpvwpg9v4vwdfsssmxww09rw")))

(define-public crate-rtfm-syntax-0.2 (crate (name "rtfm-syntax") (vers "0.2.1") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "16fn35vy6chazlin541ca5zsw8rpgassj5cisp9lw9292d2k0yy8")))

(define-public crate-rtfm-syntax-0.3 (crate (name "rtfm-syntax") (vers "0.3.0") (deps (list (crate-dep (name "either") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.3.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1ksywg2cc0gyf4dykizj1nz4i5p4wb3vdfb12m86lb7c6pwacs9j")))

(define-public crate-rtfm-syntax-0.3 (crate (name "rtfm-syntax") (vers "0.3.1") (deps (list (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.3.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0ckxlfwppswy6cgb8c8rrf92d3xzm0a2hywh23cv73gnsklzh6bz")))

(define-public crate-rtfm-syntax-0.3 (crate (name "rtfm-syntax") (vers "0.3.2") (deps (list (crate-dep (name "either") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.3.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1gvmclars87gaw227lcrlk43z4rk4hrsm6jbf5x1793khlrzy8bj")))

(define-public crate-rtfm-syntax-0.3 (crate (name "rtfm-syntax") (vers "0.3.3") (deps (list (crate-dep (name "either") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.3.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "07pmcb6k4zm57lh79jaifjpnl9crqw6p8z1bsd1r7al0036xdjcw")))

(define-public crate-rtfm-syntax-0.3 (crate (name "rtfm-syntax") (vers "0.3.4") (deps (list (crate-dep (name "either") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.6") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.2") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1gg6r81891wi0y50fdkzlj6015xfdr9zik7avam3bnrsimj4h142")))

(define-public crate-rtfm-syntax-0.4 (crate (name "rtfm-syntax") (vers "0.4.0-beta.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "18qj5nccvjw29ld72fxjw3zc0dfd7zqj69iz8hpji334gnzlmdf5") (yanked #t)))

(define-public crate-rtfm-syntax-0.4 (crate (name "rtfm-syntax") (vers "0.4.0-beta.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1x6xqg3xcn9vim5g448vzbv0vypjhibnzqq9cq8cydqrid5pixlm") (yanked #t)))

(define-public crate-rtfm-syntax-0.4 (crate (name "rtfm-syntax") (vers "0.4.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0vhiqk742cs62kk6p02as0hk7y57fr6ql8by9r2ncgfz6hyf4ma4")))

(define-public crate-rtfmt-0.1 (crate (name "rtfmt") (vers "0.1.0") (hash "0gpmpmdzb2wddhjvlxw0i7lsidgzdzla071r15piq1gcpgzk0fif")))

(define-public crate-rtfmt-0.1 (crate (name "rtfmt") (vers "0.1.1") (hash "1ry3fqzjmkshrjq5bip0inlivlvbxwgkzfghps4r8kv2xjm06mm5")))

