(define-module (crates-io rt id) #:use-module (crates-io))

(define-public crate-rtid-cli-0.1 (crate (name "rtid-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "009zydjwf9s87dkkh6q0s7rs1vbpzrvprnfga40lgpzzbxzm6wgb")))

