(define-module (crates-io rt _r) #:use-module (crates-io))

(define-public crate-rt_ref-0.1 (crate (name "rt_ref") (vers "0.1.0") (hash "1s4g5pfw457pqgyviamnc6zk2sm8qz2xb9fwk7lxarh7p7n2n85a")))

(define-public crate-rt_ref-0.1 (crate (name "rt_ref") (vers "0.1.1") (hash "1camslx780b7avggdshyzvcj1455cg6d96f17dh03xp75szr40aq")))

(define-public crate-rt_ref-0.1 (crate (name "rt_ref") (vers "0.1.2") (hash "06bzsrgi5yw9rsqxh37j4k9rzzxh9s79x49prw3j5sgdiil1hgid")))

(define-public crate-rt_ref-0.1 (crate (name "rt_ref") (vers "0.1.3") (hash "1y37q0rjn0ff2jnz1w5kbv68inim25p6dfi114r5j33m8jzrmwkn") (features (quote (("unsafe_debug"))))))

(define-public crate-rt_ref-0.2 (crate (name "rt_ref") (vers "0.2.0") (hash "1w8859kdi1n2ph7pw3zsfan8i7l0c73x1z7m5ijs9x855zj8ri45") (features (quote (("unsafe_debug"))))))

(define-public crate-rt_ref-0.2 (crate (name "rt_ref") (vers "0.2.1") (hash "0my34x8vpp8krwjq7kgb0gh2g2hccfdr3n3j7qqcfxkqza46d8g2") (features (quote (("unsafe_debug"))))))

