(define-module (crates-io rt r_) #:use-module (crates-io))

(define-public crate-rtr_translator-0.1 (crate (name "rtr_translator") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1i3n1ylwi0hryg5kgsnrkbnmnsbpnji0wv33l7ra6dzp28bcppwd")))

(define-public crate-rtr_translator-0.1 (crate (name "rtr_translator") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "05n164y20jsd4hsbjbxnc0ygk4vw16w5m1p2f6hlkkjlfd10xkbs")))

(define-public crate-rtr_translator-0.1 (crate (name "rtr_translator") (vers "0.1.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "string-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "092zw46118p94vfjr2pi50z3xnmzsq9va3avmixgdk1lv1cagrn3")))

