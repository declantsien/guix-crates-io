(define-module (crates-io rt la) #:use-module (crates-io))

(define-public crate-rtlambda-0.0.1 (crate (name "rtlambda") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4") (optional #t) (default-features #t) (kind 0)))) (hash "10z6i9lxj56fnj8xvja4m5iibyn0jkaifyxqchvd60mlf0d0sf5g") (features (quote (("default" "ureq")))) (v 2) (features2 (quote (("ureq" "dep:ureq"))))))

