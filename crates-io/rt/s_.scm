(define-module (crates-io rt s_) #:use-module (crates-io))

(define-public crate-rts_derive-0.1 (crate (name "rts_derive") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full" "extra-traits" "visit"))) (default-features #t) (kind 0)))) (hash "1blk1qmhipwvr9xglfms49z0dn18kiwvsmpbpj9y88fljl7sl9m0")))

(define-public crate-rts_proc-0.1 (crate (name "rts_proc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "05nsnjnmsf8mafvciarr8hirnnr5abqd1vils9z5c52fkjjdp6sg") (yanked #t)))

