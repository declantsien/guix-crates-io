(define-module (crates-io rt _m) #:use-module (crates-io))

(define-public crate-rt_map-0.1 (crate (name "rt_map") (vers "0.1.0") (hash "0wacjmcgd3w71ri4336vn9l6aqsibs9m67hwd84gjzvbbvvsqhwc")))

(define-public crate-rt_map-0.2 (crate (name "rt_map") (vers "0.2.0") (hash "02b2c90m4r45v7ybzri062pxlzrp32zc9r5578ibnr128rkxzic9")))

(define-public crate-rt_map-0.3 (crate (name "rt_map") (vers "0.3.0") (hash "186vxph6jgapkgiv4lg3wbng0qp459jnpq8q0jsfwzff38ykjmb6")))

(define-public crate-rt_map-0.4 (crate (name "rt_map") (vers "0.4.0") (hash "0magiq5vy0rrjnbs12c7nz290cr70mhw0d84f11i2x8s12xll8pm")))

(define-public crate-rt_map-0.5 (crate (name "rt_map") (vers "0.5.0") (hash "11ap3yiknnwj048nl3piwym3bhjld7qyj5vsr6016mfzaginc4ym")))

(define-public crate-rt_map-0.5 (crate (name "rt_map") (vers "0.5.1") (hash "1dx7dqf16k85z57kwmqiywpfirq584hcsar5px8gfmic5yapyfjk") (features (quote (("rt_vec") ("default"))))))

(define-public crate-rt_map-0.5 (crate (name "rt_map") (vers "0.5.2") (deps (list (crate-dep (name "rt_ref") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "06xvp4dirf3r2hm1bp2x7k10z42dnph8al00dz2irrgsjyg5k3al") (features (quote (("unsafe_debug" "rt_ref/unsafe_debug"))))))

