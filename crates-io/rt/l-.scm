(define-module (crates-io rt l-) #:use-module (crates-io))

(define-public crate-rtl-sdr-0.1 (crate (name "rtl-sdr") (vers "0.1.0") (hash "0wkxrvlj8xyl535bz4cpkng4f0n98118j0jrlkb7z3c7nf0i42x7")))

(define-public crate-rtl-sdr-0.1 (crate (name "rtl-sdr") (vers "0.1.1") (hash "01b0p8ia0y6qx6p0pz8qxni1abg4c56dk1vifx2627gipzj7mxbs")))

(define-public crate-rtl-sdr-0.1 (crate (name "rtl-sdr") (vers "0.1.2") (hash "1bkrrhxaljawcpv1pkjmyz1qsz7npvmx2q81xc71fqknslv2agh4")))

(define-public crate-rtl-sdr-0.1 (crate (name "rtl-sdr") (vers "0.1.3") (hash "16my94073bwbj2answvn75rgwcdvmq00n2v0v5ipij2wkkdfss63")))

(define-public crate-rtl-sdr-0.1 (crate (name "rtl-sdr") (vers "0.1.4") (hash "01122va31dsw5ykfd6lnyb2i3jgwaw2417z4pyr1g0wxyk2blsm7")))

(define-public crate-rtl-sdr-0.1 (crate (name "rtl-sdr") (vers "0.1.5") (hash "16srqjs26wnggp4c9xknjspxnn023nxf1n10779vj1vq5l4vgz86")))

