(define-module (crates-io rt ee) #:use-module (crates-io))

(define-public crate-rtee-0.1 (crate (name "rtee") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.31") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0f8qlwih2yihg3g0p9559njx8x6vd013c5zikj8k8l16h41za7lg")))

(define-public crate-rtee-0.2 (crate (name "rtee") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~2.31") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0s3a0lyrllr4xq8jcaddmsfjxddlfkzz3y17m8q01kqfafxk6cl8")))

(define-public crate-rtee-0.2 (crate (name "rtee") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "01sm14ihddb1wdbviw53f20kybg0n42nvlz12yl99r1p6fhdz2af")))

(define-public crate-rtee-0.2 (crate (name "rtee") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.20.2") (default-features #t) (kind 0)))) (hash "0f22v9iyni8kdlknk8120a7kp7l2lkj0r9mal0hvc4sqwkmxcr41")))

