(define-module (crates-io rt a-) #:use-module (crates-io))

(define-public crate-rta-for-fps-latex-gen-0.1 (crate (name "rta-for-fps-latex-gen") (vers "0.1.0") (deps (list (crate-dep (name "rta-for-fps-latex-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rta-for-fps-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vvv18wzb3xb4f5l4b7vbcviwaqsq1h434s8mj8qsqxl8y2gzkjp")))

(define-public crate-rta-for-fps-latex-gen-0.1 (crate (name "rta-for-fps-latex-gen") (vers "0.1.1") (deps (list (crate-dep (name "rta-for-fps-latex-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rta-for-fps-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02wzwa3k6k1k89a7qlsycfx4j6x0krcy1zf0snarhdg21310mdnk")))

(define-public crate-rta-for-fps-latex-gen-0.2 (crate (name "rta-for-fps-latex-gen") (vers "0.2.0") (deps (list (crate-dep (name "rta-for-fps-latex-lib") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rta-for-fps-lib") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "11z5s17qk90x790vy731adrf739fxb5mszfnikc5ryl71k8d6vyc")))

(define-public crate-rta-for-fps-latex-lib-0.1 (crate (name "rta-for-fps-latex-lib") (vers "0.1.0") (deps (list (crate-dep (name "rta-for-fps-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1s9ak6w8n9xfni2wi9m7h52n0l26lbhg5r4i404f9chnxlzvsabc")))

(define-public crate-rta-for-fps-latex-lib-0.1 (crate (name "rta-for-fps-latex-lib") (vers "0.1.1") (deps (list (crate-dep (name "rta-for-fps-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0im8255ikxky0cw37ljcmmgz5r3gv2a63q4506j8xmja5ykvmra2")))

(define-public crate-rta-for-fps-latex-lib-0.2 (crate (name "rta-for-fps-latex-lib") (vers "0.2.0") (deps (list (crate-dep (name "rta-for-fps-lib") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1aphjl79sghgjzxhszqmk9f63qf2lg9by398kqzc3b5yhm2qxwbx")))

(define-public crate-rta-for-fps-lib-0.1 (crate (name "rta-for-fps-lib") (vers "0.1.0") (hash "1hpj6majzyf4q5iiqjrxff5arg88vm60cqi62z4fvqnbcswysigv")))

(define-public crate-rta-for-fps-lib-0.1 (crate (name "rta-for-fps-lib") (vers "0.1.1") (hash "0kvmkhc9lj60xznp59f2yyl4nfqy0d2a556k5ssiig39n5w4yysa")))

(define-public crate-rta-for-fps-lib-0.2 (crate (name "rta-for-fps-lib") (vers "0.2.0") (hash "02cr3p3bpvk2r9y4gv52h1r10d828lw5x1p81d00scdyzns3r4w3")))

