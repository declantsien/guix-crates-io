(define-module (crates-io rt om) #:use-module (crates-io))

(define-public crate-rtoml-0.1 (crate (name "rtoml") (vers "0.1.0") (hash "0yiy87yn7qgqhwnn7786z8rfg1nqvzk6lphv6g0lkfp86a1mfj8k")))

(define-public crate-rtoml-0.1 (crate (name "rtoml") (vers "0.1.1") (hash "0blk5ynsb567a36p6w8icdgwx3rs4n9l31zhi17xc4a9hfpwwpws")))

(define-public crate-rtoml-cli-0.1 (crate (name "rtoml-cli") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sensible-env-logger") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "07mxpi41ki3qc8hzr200wzwvnfczskf59agl3yinn22f22wr2i1q")))

