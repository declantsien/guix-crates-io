(define-module (crates-io rt on) #:use-module (crates-io))

(define-public crate-rton-tl-0.1 (crate (name "rton-tl") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "pom") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "11v3vg8fz8sg1fxgzl13lxv15prpi7r1bc41mfpb7pyyh3jiryxy")))

