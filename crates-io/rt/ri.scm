(define-module (crates-io rt ri) #:use-module (crates-io))

(define-public crate-rtriangulate-0.1 (crate (name "rtriangulate") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1h9warc0c0qykvij30qzdbig8yxb3abrnh4f9yxflv3fbm5s8vzd")))

(define-public crate-rtriangulate-0.1 (crate (name "rtriangulate") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1bmr66kxw956vq1n16v2g0xmcz7fzmx360mx1hwhkcy7v5w7vhq3")))

(define-public crate-rtriangulate-0.1 (crate (name "rtriangulate") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "15sbl3j3m7k09rnxjvx8fiyr59m7hmbhz4dxi3ya32x6p9alvfz1")))

(define-public crate-rtriangulate-0.2 (crate (name "rtriangulate") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0rqnf8nhpg3s0akj3wp78y753sdns0xx0jk4f4hf8f92i44r1m2l")))

(define-public crate-rtriangulate-0.2 (crate (name "rtriangulate") (vers "0.2.1") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1jpxr7m78wcb3smqw8cfbcl1axdsy07gyrdsrl2jlrn1x7331d8b")))

(define-public crate-rtriangulate-0.3 (crate (name "rtriangulate") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1jskrhj7pb80an009x3f5751q2920phdcrkjd4abvhvs5h16jz4a")))

(define-public crate-rtriangulate-0.3 (crate (name "rtriangulate") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "15qnbkzzalf5fd4xc4v27jq8z7g6nx3azd9dxzqmw42wdf5bsdy7")))

(define-public crate-rtrie-0.1 (crate (name "rtrie") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "100s583ss1fqsq9a42qbzx7cnwzq97khsl0mn9gay1gmgkkjv0kq")))

(define-public crate-rtrie-0.1 (crate (name "rtrie") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1f0aavdi5lrskgr3wjiy01m2532lzyx2xnax56564fhzcaa25fm3")))

