(define-module (crates-io rt nl) #:use-module (crates-io))

(define-public crate-rtnl-1 (crate (name "rtnl") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "rtnetlink") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("rt-multi-thread"))) (optional #t) (default-features #t) (kind 0)))) (hash "1i84hlcdzknj0yr5l21s7j4kh11kh59fyyddfkf5nd6bac0dadd2")))

