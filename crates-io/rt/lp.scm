(define-module (crates-io rt lp) #:use-module (crates-io))

(define-public crate-rtlp-lib-0.1 (crate (name "rtlp-lib") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1kjk78gb2pmxwwxxv11xgc7j7zr2s0945jzq6xf0m481g689pzlv")))

(define-public crate-rtlp-lib-0.2 (crate (name "rtlp-lib") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1rpdm0ahsbr42hkyrs4a6l42s4ir93s4z3n6y5dv93lfgbk7cjgb")))

(define-public crate-rtlp-lib-0.2 (crate (name "rtlp-lib") (vers "0.2.2") (deps (list (crate-dep (name "bitfield") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "11q7y5mbg1znhbsyvpfdhxa1rj61259dlg9b6f8kd6fsi85l7jn1")))

