(define-module (crates-io rt cc) #:use-module (crates-io))

(define-public crate-rtcc-0.1 (crate (name "rtcc") (vers "0.1.0") (hash "1g992hdmg3j27hndg9ri6p8w3l8cpzi68fkn5lmam770g7jxpc3v")))

(define-public crate-rtcc-0.2 (crate (name "rtcc") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "1a7l908s4bnr8lx0p2qzd2ar3kv97aby920jq1vxjjydgchzfj0h")))

(define-public crate-rtcc-0.2 (crate (name "rtcc") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (kind 0)))) (hash "151r4yjn7p435s9qx59j133ljpclq6hvnglvr7f38jj3pzfgjdgg")))

(define-public crate-rtcc-0.3 (crate (name "rtcc") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (kind 0)))) (hash "0c4r9nz28n5aqkv6mxh5i3p7v73vi1f1rippgkcaf2bwwyf628rn")))

(define-public crate-rtcc-0.3 (crate (name "rtcc") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.33") (kind 0)))) (hash "1zmkfd3db2bsq59csxy2jjpw2wj0ndl2b1zglwknxdyjpvax1yzl")))

(define-public crate-rtcc-0.3 (crate (name "rtcc") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (kind 0)))) (hash "0f88zjifkjiy579kfrwsaplnv3b1hmdjn3dpqprw9bbl08x3r5wm")))

