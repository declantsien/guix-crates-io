(define-module (crates-io rt ab) #:use-module (crates-io))

(define-public crate-rtab-0.1 (crate (name "rtab") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "0rbh0hcm7c31qqvrpqw2g2b2pdm6py60ckl5ql7jyhk7f01fxb8v")))

(define-public crate-rtab-0.1 (crate (name "rtab") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "1jcrxhs4zyksnsa5bb282frj2mvki4gqqnagjh2xdjqmr88jrvnz")))

(define-public crate-rtab-0.1 (crate (name "rtab") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "1wvh8j9hg1cadbcvglndh6vk79kkzjqqf9f64vpimswa1qkq9gba")))

(define-public crate-rtab-0.1 (crate (name "rtab") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "1nrmy38x4k0vsmlmx4v0ij6ql1k0mrq3mx4vj55qp3x1c94255pw")))

(define-public crate-rtab-0.1 (crate (name "rtab") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "18qsxaljcq9cgpn5bagnj0r4pqhhcszq0b8lzqc2pzmx7z5p7jf4")))

(define-public crate-rtab-0.1 (crate (name "rtab") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "1khb4zjf8164a7wdd9xwhz7zd50qgd8blmfdzj043a0w3qv9g3sw")))

(define-public crate-rtable-0.1 (crate (name "rtable") (vers "0.1.0") (hash "15xn6s8li7naqxxs0a4jr28npz2bl416vnmmgwwjf9wylvafw2mn")))

(define-public crate-rtable-0.1 (crate (name "rtable") (vers "0.1.1") (hash "0rp046ipy4z4wpj0z5kd527kirs69b44j8haw7gk8rngjs0jwzzy")))

(define-public crate-rtable-0.1 (crate (name "rtable") (vers "0.1.2") (hash "113lg74hd1mg45jpx06z95rzm12wd732iqfj7ybj4w2p0jgjzk9z")))

(define-public crate-rtable-0.1 (crate (name "rtable") (vers "0.1.3") (hash "08pfw5dzpk8n209yh8ykwc5c3cpz45x9lxdvmldd30grw9zyy35w")))

(define-public crate-rtable-0.1 (crate (name "rtable") (vers "0.1.4") (hash "1qccahgijcm19gr46xwhg76sf74323k6yvsi82i1w9lhbprgnqxb")))

(define-public crate-rtable-0.2 (crate (name "rtable") (vers "0.2.0") (hash "0c6ch85dlhwi741pskdb60ri4j4hsifp9vhmh7nx41s1q5c4gj82")))

(define-public crate-rtable-0.2 (crate (name "rtable") (vers "0.2.1") (hash "0m6jlm067p2glhhv08xdnxjx5xag2f747n8qsqfihk6d98y1w7pz")))

(define-public crate-rtabmap-0.1 (crate (name "rtabmap") (vers "0.1.0") (hash "0826lyrvays2msp3da466f23ldqg7sknl7y4ap9kfy4wpfqik8ra")))

(define-public crate-rtabmap-sys-0.1 (crate (name "rtabmap-sys") (vers "0.1.0") (hash "0dgkw7l5ka0sr5lbrzdgz72igjnbv9aam2chh72mbnz42xjacj44")))

