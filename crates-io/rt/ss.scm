(define-module (crates-io rt ss) #:use-module (crates-io))

(define-public crate-rtss-0.3 (crate (name "rtss") (vers "0.3.0") (hash "01qg52bivq5q9w9cr05w1781c66h6nwzv23ag6hda9q4ismm21p9")))

(define-public crate-rtss-0.5 (crate (name "rtss") (vers "0.5.0") (deps (list (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "1s28xf11jrzk02qgmj51wj2v4cb3k2p8l56m8733qxn2zqka7r2j")))

(define-public crate-rtss-0.6 (crate (name "rtss") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "14rl2nlbpyim82diwif833k5m1hcsffkm5fwrwxj9rywfnnn4jpy")))

(define-public crate-rtss-0.6 (crate (name "rtss") (vers "0.6.1") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "0qls7ba9wd15wpyap9hxfwz728lkshgqiw2g7rhg0ym6f0dkssgz")))

(define-public crate-rtss-0.6 (crate (name "rtss") (vers "0.6.2") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "1r1b6fynkjnpj5p3k209sa13mjvh4k0ghzwnribm48dh9v7lfnnv")))

(define-public crate-rtss-sys-0.1 (crate (name "rtss-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0w6xvmscdrmjqpjkpdxhm0j5zzvcay5l3plwppn577k9j2yi00hs")))

(define-public crate-rtss-sys-0.1 (crate (name "rtss-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)))) (hash "0kv1h8l09jaqh1xdnkyqkrw2m712gyq61ayvrhrcr2vmhc7vnf76")))

(define-public crate-rtss-sys-0.1 (crate (name "rtss-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "148yv5lhwrfzxrm9v4hxx8zca5ggxmapp29fg9ayhg333c60rb73")))

