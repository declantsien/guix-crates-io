(define-module (crates-io rt no) #:use-module (crates-io))

(define-public crate-rtnorm-rust-0.1 (crate (name "rtnorm-rust") (vers "0.1.0") (deps (list (crate-dep (name "GSL") (req "4.0.*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "0.4.*") (default-features #t) (kind 0)))) (hash "0g2jxxn9prpbs9fqn5bgq7xvflxk9gx27cv93sc6l0vf8x9lf5m9")))

