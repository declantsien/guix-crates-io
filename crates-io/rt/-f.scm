(define-module (crates-io rt -f) #:use-module (crates-io))

(define-public crate-rt-format-0.1 (crate (name "rt-format") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0662axvixhhb9cl20x91bfpymx6vi4j09dx4jmznz32w7sgi9s21")))

(define-public crate-rt-format-0.1 (crate (name "rt-format") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0qdnsh0r043irfpn8k2083w68qwq7qb5jllv5kkdf4im9xcf9850")))

(define-public crate-rt-format-0.1 (crate (name "rt-format") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1f0jndh895xsl6kmx5c7sb5mfgz6l8rxxjd1a3aiigxahn1swsyd")))

(define-public crate-rt-format-0.2 (crate (name "rt-format") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1c5hi3c9if7wsgvnwgy2sk02xmrs1yp8d7ix3lnxzrijzccknclk")))

(define-public crate-rt-format-0.2 (crate (name "rt-format") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1i476gh47cjipn4dg8y1ls4my7p1g9kbicg19aj7ivw1597kwd27")))

(define-public crate-rt-format-0.3 (crate (name "rt-format") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1bmxn40aaflxgrz5pdjdkk244aj293g65vyrg0dbnb65gwizyglm")))

(define-public crate-rt-format-0.3 (crate (name "rt-format") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1qjjwh9ny95xck1kp99gi6hfm9glrx54jx8npnj6yccxc7p7q225")))

