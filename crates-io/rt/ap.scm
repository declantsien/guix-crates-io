(define-module (crates-io rt ap) #:use-module (crates-io))

(define-public crate-rtapi-logger-0.1 (crate (name "rtapi-logger") (vers "0.1.0") (deps (list (crate-dep (name "linuxcnc-hal-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "19bx57r3bcmpcddnnbsav1iqg84yk2rl70ja12z9fb0i37y3y743")))

(define-public crate-rtapi-logger-0.2 (crate (name "rtapi-logger") (vers "0.2.0") (deps (list (crate-dep (name "linuxcnc-hal-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0irdp2jjsgz1agbqh6j11bg7b9xv16f495gxqkn1fxnlm2zgbrhv")))

