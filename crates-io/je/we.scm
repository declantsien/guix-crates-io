(define-module (crates-io je we) #:use-module (crates-io))

(define-public crate-jewel-0.1 (crate (name "jewel") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "embassy-time") (req "^0.3.0") (features (quote ("defmt"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (kind 0)))) (hash "05k0p3rgxpv7w0vclc895fv3ppd8ha072bvblm5nl6jd9k0k6fqd") (yanked #t)))

(define-public crate-jewel-0.1 (crate (name "jewel") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "embassy-time") (req "^0.3.0") (features (quote ("defmt"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (kind 0)))) (hash "0h4zjiv6jn82i6b7xg1bahxf47isalia1xfq3hc2xc6wnkcd46m4")))

(define-public crate-jewel-nrf52840-0.1 (crate (name "jewel-nrf52840") (vers "0.1.0") (deps (list (crate-dep (name "embassy-nrf") (req "^0.1.0") (features (quote ("defmt" "nrf52840" "time-driver-rtc1" "time"))) (default-features #t) (kind 0)))) (hash "1rvcik8p2y27fw0bifkwbvhm9bnkh4x1qjfg65zq0nx6bnzns0kx")))

