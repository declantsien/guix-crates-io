(define-module (crates-io je rb) #:use-module (crates-io))

(define-public crate-jerboa-0.1 (crate (name "jerboa") (vers "0.1.0") (hash "0k3p5jsh2hkz6gkss59alnhzjl0wafqpzsl6m8p5nq48s9s2sbmm")))

(define-public crate-jerbs-0.1 (crate (name "jerbs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (default-features #t) (kind 0)))) (hash "1abpjj1qkvb6q0n6azskvx8hcshq86j5pzl0nhxsgd9hjkjz7mca")))

(define-public crate-jerbs-0.2 (crate (name "jerbs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3") (features (quote ("formatting" "local-offset"))) (default-features #t) (kind 0)))) (hash "14vpxqwfcyg9y35mkp6x0fnyfk4vc8vpzf4w7q16r9qbd8zz6l1a") (features (quote (("default" "rusqlite/bundled"))))))

