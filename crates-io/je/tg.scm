(define-module (crates-io je tg) #:use-module (crates-io))

(define-public crate-jetgpio-sys-0.1 (crate (name "jetgpio-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0ajncfb76chiw6hz8gf3x5ji4kvsag83j1ksi4g73zgbvkal7v39") (links "jetgpio")))

(define-public crate-jetgpio-sys-0.1 (crate (name "jetgpio-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1hi726q1rj0bxj23r0yb7d9z3bnj29iv2xrd8zrxzdazjcxdcs00") (links "jetgpio")))

(define-public crate-jetgpio-sys-0.2 (crate (name "jetgpio-sys") (vers "0.2.0-beta.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0m93xl0lxd05gv0z1123c1nxs940ny3rwgqzylhb95m9y6mh5if9") (features (quote (("orin")))) (links "jetgpio")))

(define-public crate-jetgpio-sys-0.2 (crate (name "jetgpio-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0sxl4k5bz3w71q7v1v00r79fvxfbyyd2bidsjwnhnazf2z9i1b7c") (features (quote (("orin")))) (links "jetgpio")))

(define-public crate-jetgpio-sys-0.2 (crate (name "jetgpio-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "110gzk6rx71jij1zm2d760spxlp4sysw55861h13dn88hz372cyy") (features (quote (("orin")))) (links "jetgpio")))

