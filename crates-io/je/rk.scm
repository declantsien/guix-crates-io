(define-module (crates-io je rk) #:use-module (crates-io))

(define-public crate-jerk-0.0.0 (crate (name "jerk") (vers "0.0.0") (hash "1p0pqswpaa9fg1mrhbqzm0x779mf51wyq3d5vyjypd6l79v356ip") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-0.0.1 (crate (name "jerk") (vers "0.0.1") (hash "0vlcpmj2na0nr9l3vsf0mrn63viv2m9rf2dnnsjqkm31f966caib") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-0.1 (crate (name "jerk") (vers "0.1.3") (deps (list (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wjgv514ljwfsx5jsdq9bkgda3hbmvmw0l25w1g2xdq4hpkfapcy") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-0.1 (crate (name "jerk") (vers "0.1.4") (deps (list (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "11lfr3a0alga9gm8w74mbxbb3375rsxhvl3xmdr31bi0i9cib88c") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-0.1 (crate (name "jerk") (vers "0.1.5") (deps (list (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "19vq8s48b8f47pjpzh0g1x3h13bvas0jdn5ywz510gk603kfvbgd") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.1 (crate (name "jerk") (vers "0.1.6") (deps (list (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winerror"))) (default-features #t) (kind 0)))) (hash "002v32bb2dd9dkmrxc0083gphi3f6v2klcljazr3qfq4d528l725") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2 (crate (name "jerk") (vers "0.2.0") (deps (list (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winerror"))) (default-features #t) (kind 0)))) (hash "0zzjy55cakwlcvy6rjvgn1c2m2fb1xvsc5hyaz6qjxikk27glsaf") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2 (crate (name "jerk") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1pqy6mdfh8nhfjpyx3bcs37lv54s43qzm484sbxzdfbhmkdv5shc") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2 (crate (name "jerk") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "=1.3") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "=1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "042spfwrdcidq5h7xmb9966rrr4dcmnl70zmxsg1gan4khzyk4dg") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-0.2 (crate (name "jerk") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "=1.3") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "=1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1ibmzpiv7cjzipk0sjhycx12cv30c3i3wl96wwj9z78rghplm2l8") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-build-0.1 (crate (name "jerk-build") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)))) (hash "1pa6a8ri1hiign59n4rh7djxramfm7mw9860av0ms010dg8x0h3d") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-build-0.1 (crate (name "jerk-build") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)))) (hash "10wsiizkxd8pcmxahk2g5iqyg5qz8nv6dr5sbg558y48k15mwh2n") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-build-0.1 (crate (name "jerk-build") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)))) (hash "1l9fld2zgb0gybfwbai5xc9n1f62q0d9khy4kmpn7pxcr9pmxhkc") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-build-0.1 (crate (name "jerk-build") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "jerk") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0vfb7p57r3d2b8fwavh2rhb7db2qhlbwq12hh1hn4wm6ll1ckh6d") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-build-0.1 (crate (name "jerk-build") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "jerk") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "041zsb94dnahdda2lzvqsdm5vf89wbvaxcjf2afkqm9q8ckw53cm") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-build-0.1 (crate (name "jerk-build") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "jerk") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1cx994yqgnkwggbvw8sbr5a7kq9d7b2v0v50n1679xs4pf1sdlrs") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-build-0.1 (crate (name "jerk-build") (vers "0.1.6") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "jerk") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0qqybc8g8z7yj3x4l3cfkf8y35k43f0cg1wryqcw1bmcvb9yymfk") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-build-0.2 (crate (name "jerk-build") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "jerk") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0jq2mayqnbl1n97zyycl46pq6nrgbkn5vj6slfvw7nzasa0phks5") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-build-0.2 (crate (name "jerk-build") (vers "0.2.1") (deps (list (crate-dep (name "jerk") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "090rjndgacranck6h5r1anfhmbx95b490k01iplqhczg9z6jhd85") (features (quote (("nightly" "jerk/nightly") ("default"))))))

(define-public crate-jerk-test-0.1 (crate (name "jerk-test") (vers "0.1.0") (deps (list (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0j33dsm40zqnq1v8c7s86ywqdc7hs6x1dqg66mm099rdkr80ijjf") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-test-0.1 (crate (name "jerk-test") (vers "0.1.1") (deps (list (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "09g8hqdb588n20zarh6bbsc2dr8a5d067dd26yk2ab44w217srig") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-test-0.1 (crate (name "jerk-test") (vers "0.1.2") (deps (list (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "06jnjdxb2x7i8wjxgwr7nvliyi3i75fj5r9jaxib7kjzlqkhq1g1") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-test-0.1 (crate (name "jerk-test") (vers "0.1.3") (deps (list (crate-dep (name "jerk") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "05vf2r4ma8dxmc0whrlawbl656g7m0w8lf4di3i14y0h244nicr3") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-test-0.1 (crate (name "jerk-test") (vers "0.1.4") (deps (list (crate-dep (name "jerk") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0n9w9z7synqzdklwk0kzylmfh3sqy4k2gm32ihm8hrah7dhshpsp") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-jerk-test-0.1 (crate (name "jerk-test") (vers "0.1.5") (deps (list (crate-dep (name "jerk") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "06p9bc5j905zp9vwf3gkrbf0i9bsygr8iprviwnz74frr7rp3i4i") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-test-0.1 (crate (name "jerk-test") (vers "0.1.6") (deps (list (crate-dep (name "jerk") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0zxa12sq2raj15spgxkb1rrjgy51nhzz1fr31zksffb9pswy5jsd") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-test-0.2 (crate (name "jerk-test") (vers "0.2.0") (deps (list (crate-dep (name "jerk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "jni-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0j605i31hhsbx2a016mkacr7cdqabcg7zibx0724x8b23pahy8bd") (features (quote (("nightly") ("default"))))))

(define-public crate-jerk-test-0.2 (crate (name "jerk-test") (vers "0.2.1") (deps (list (crate-dep (name "jerk") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1v2g3gyyadrjn2rfph16kpv2wv0hfy8p2wr2hhkpn9pfvn80q711") (features (quote (("nightly" "jerk/nightly") ("default"))))))

