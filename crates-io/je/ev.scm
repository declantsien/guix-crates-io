(define-module (crates-io je ev) #:use-module (crates-io))

(define-public crate-jeeves-0.1 (crate (name "jeeves") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "06v2b2yrrdwkx8mnk777h2nk8i8908ncwwz3rlagwmwxyh2b18y9")))

