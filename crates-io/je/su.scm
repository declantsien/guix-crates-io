(define-module (crates-io je su) #:use-module (crates-io))

(define-public crate-jesus-0.1 (crate (name "jesus") (vers "0.1.0") (hash "0878f40zxxq2c7alym4y8jisws3j6437aa4rv6748scq4yrvqrh5")))

(define-public crate-jesus-chris-0.1 (crate (name "jesus-chris") (vers "0.1.0") (hash "0vrm2v1kd8rkcmkgrqblkxpniv9h617lfa211pmxx7g470qpahi5")))

