(define-module (crates-io je ky) #:use-module (crates-io))

(define-public crate-jekyll-to-gatsby-0.1 (crate (name "jekyll-to-gatsby") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "07p5m92mz1yklk2w0wdc9f06dl1yh64saip8m1zy5biqyvkhwiir")))

(define-public crate-jekyll-to-gatsby-0.2 (crate (name "jekyll-to-gatsby") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "164sym28dk3c2ihmi8ishns4zqm09m3xkxqsgyd6h9ghmckp79c9")))

(define-public crate-jekyll-to-gatsby-0.3 (crate (name "jekyll-to-gatsby") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zs50v8b2wf52m6n48yjcbz47l3j1jf5snf38iz5xfgfbz9kfyrm")))

(define-public crate-jekyll-to-gatsby-0.4 (crate (name "jekyll-to-gatsby") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mipc1n67pzzbxlwvl0nl2q32lyh9q0kb69ch9q6qhxm0d3ps0j3")))

