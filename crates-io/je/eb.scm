(define-module (crates-io je eb) #:use-module (crates-io))

(define-public crate-Jeebs-0.1 (crate (name "Jeebs") (vers "0.1.0") (hash "0a4vqv3ksiglg7q2a3mabrnd2qlnj23rpgy1vl1hhgbv12gxy2vm") (yanked #t)))

(define-public crate-Jeebs-0.1 (crate (name "Jeebs") (vers "0.1.0-beta.23120801") (hash "1gvkpj7f4knvwx48sac6zlandgiii4hx22c627zhb67k3y8x9lzm") (yanked #t)))

