(define-module (crates-io je de) #:use-module (crates-io))

(define-public crate-jedec-0.0.0 (crate (name "jedec") (vers "0.0.0") (hash "07hvhgjswa4lsn0m6l4pb1a298714j7k2mj050ysywdlshsd5kcq")))

(define-public crate-jedec-0.0.1 (crate (name "jedec") (vers "0.0.1") (hash "0rkg5cnwn6k97mwx09y3qs3kf9nr5zvdghhi06xvfm6p05hm7ahd")))

(define-public crate-jedec-0.0.2 (crate (name "jedec") (vers "0.0.2") (hash "14dwzbgxz39cpwldg9aks2hhdpgpdyh9ixk10lp35kbyhqlncil6")))

(define-public crate-jedec-0.0.3 (crate (name "jedec") (vers "0.0.3") (hash "04hkgq70f6sxgxml2aknw8cqjvry1xd1b8znw8l2fr90nngch1h3")))

(define-public crate-jedec-0.1 (crate (name "jedec") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)))) (hash "0rydlc75md5yygs5nw26b8ymrb5nmyi2kzljsq2nwsg50gikklbl") (features (quote (("std") ("default" "std"))))))

