(define-module (crates-io je rr) #:use-module (crates-io))

(define-public crate-jerry-0.1 (crate (name "jerry") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "090zrk4c6m4zj17fbkb6zsj9qq5fl4q5jps3hlfig83hlql4wlmb")))

