(define-module (crates-io je re) #:use-module (crates-io))

(define-public crate-jeremy-0.0.0 (crate (name "jeremy") (vers "0.0.0-jeremy") (hash "1m901a00h58glar2c0n3cf2i1ns0gp0ivzkzidz4v5rx76ia4kzx") (yanked #t)))

(define-public crate-jeremy-0.0.0 (crate (name "jeremy") (vers "0.0.0--") (hash "07caz4nicxi7nzvjwqalcff7v5dkhl62qp6y4zf0vqrls6an2rs7") (yanked #t)))

(define-public crate-jeremyBanks-0.0.0 (crate (name "jeremyBanks") (vers "0.0.0-0.0.0-0.0.0") (hash "0z519jy45151v8lrj8mdfmqr23hgnvx4d3nwv8ldiv59vd8ahfpm") (yanked #t)))

(define-public crate-jeremyBanks-0.0.0 (crate (name "jeremyBanks") (vers "0.0.0--") (hash "1mgcbsf4159nzh87shqgl26zqxklky0jcdkphj1j83lz7ncaj380") (yanked #t)))

