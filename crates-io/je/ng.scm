(define-module (crates-io je ng) #:use-module (crates-io))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.0") (hash "07z91ympw4r0gkwvx709smxcrz96vvz84x6sf4xjhs704va4jg91")))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.1") (hash "016qncwwzxyl6v7jv98dfwva3cy31v97x718vm4bx2jz59nvvp21")))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.2") (hash "1jfs16mn5bxbcp810gjj1d31f8kv1hr2wm2kfsfpwrax8d69n22s") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.3") (hash "10ggk0v6pykpzywf38inpkdwz63199slblmkzsw8yizc6hhj2dnh") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.4") (hash "14c30w5aakalry4ilqkbf2xsyda7v82dmswh5l2ncw40sqsjaf6b") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.5") (hash "1kf2mj7f3s3nvsaq3ycbj6mw32cfnh0g57zw51zai4zqm6wxwzfa") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.6") (hash "1409phvqjjzxabszd0hgcvix8gyclpd4qcv3y423xlcws5vxkybb") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.7") (hash "1d2srl75dgrnzplmqvjmfk3xpyfcqb01nqss9vhlpm0xlgnxx0fb") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.8") (hash "0n56vykw816fmyivfyp3va592wh7riplmdzqs2w18xq91wgw9xqv") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.9") (hash "06lfy3494a2i71nfygbphliw5xxknalx7mifycg92av0kq1ani50") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-jenga-0.1 (crate (name "jenga") (vers "0.1.10") (hash "149pnb83788br3mzl05ljzn051ddywxnvrhpcrmkhcap0afxazm8") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

