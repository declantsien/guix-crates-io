(define-module (crates-io je cs) #:use-module (crates-io))

(define-public crate-jecs-0.1 (crate (name "jecs") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "00r0qryy1pwiizz0n6i37zbwvdpc34v3rjwdhhgmm686smbqzmbv")))

(define-public crate-jecs-0.1 (crate (name "jecs") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1mamvhdgalr6lvv6zvldgfk5kagr98y1nb0dbld1652v1swymdc0")))

