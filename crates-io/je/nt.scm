(define-module (crates-io je nt) #:use-module (crates-io))

(define-public crate-jentry-0.1 (crate (name "jentry") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0li7ij2ra3zy3hnbh8vdq3ws3jlpizlx7rhj2005y6cc5lghc8aq")))

