(define-module (crates-io je ns) #:use-module (crates-io))

(define-public crate-jens-0.1 (crate (name "jens") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0rhxrpxidvfciphlgaw6xxddzlllfg52wk3a8bv8qradpvbg0knk")))

(define-public crate-jens-0.1 (crate (name "jens") (vers "0.1.1") (hash "0vrbci3jww2vza71g1xpw7p6c5zq5ap587nlihk4kinwz4rgazmj")))

(define-public crate-jens-0.2 (crate (name "jens") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0wwdls5g267lgd3sr45xdg65qajiya045v1m1gpzxgqj7pagbb2d")))

(define-public crate-jens-0.3 (crate (name "jens") (vers "0.3.0") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0zqa4awrypnj5pr2vpapqgfg8wnwdihv0ggzyj1w4m9c8ilgi8a5")))

(define-public crate-jens-0.3 (crate (name "jens") (vers "0.3.1") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1i4ak9ab19yrqlifgixcmqmxpib9xl0787lhzlxab3blz07x6c1j")))

(define-public crate-jens-0.3 (crate (name "jens") (vers "0.3.2") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "10hsyswaqa58z2fnhfq94i8ralf0ymgmnj085mgxvfrrnn0394cq")))

(define-public crate-jens-0.3 (crate (name "jens") (vers "0.3.3") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1vkrig33i3mdivr47dngf7idlkij90sw1xdxbdyf3jlz3gqzyi9c")))

(define-public crate-jens-0.4 (crate (name "jens") (vers "0.4.0") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0inpncr58arlksjms6w4nd22ac34mps77pnk5viw8qapphz8j85k") (yanked #t)))

(define-public crate-jens-0.4 (crate (name "jens") (vers "0.4.1") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0ya5c99mnq3d9kbjxxpcmdb0xx707myfs0qv3x5znrqf3pps722l")))

(define-public crate-jens-0.4 (crate (name "jens") (vers "0.4.2") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0qkmvw7k8s7mzvmai9wrblbj98i8l4ywljmjr04lhlizqgjkkq0q")))

(define-public crate-jens-0.5 (crate (name "jens") (vers "0.5.0") (deps (list (crate-dep (name "insta") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "175chgw4s34scw0qn4bbd52s1l5jck3n7yksjk3g4lkrj0kz0hss")))

(define-public crate-jens-0.6 (crate (name "jens") (vers "0.6.0") (deps (list (crate-dep (name "insta") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0hjsxra8sz2cbbzw1ygmm2bgprngyx6ykbnzch7mngjc79zpp9bx")))

(define-public crate-jens_derive-0.6 (crate (name "jens_derive") (vers "0.6.0") (deps (list (crate-dep (name "jens") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (default-features #t) (kind 0)))) (hash "1s71hh4qff9y2infl1mn7gqxkn5bansp1gm33qq6494bzvcfb15k")))

