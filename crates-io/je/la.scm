(define-module (crates-io je la) #:use-module (crates-io))

(define-public crate-jelastic-rs-0.1 (crate (name "jelastic-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01yl74p90lm3vb1lbh98a5h8zcp0cmhynp1xv8yirlqh7djvyab3")))

