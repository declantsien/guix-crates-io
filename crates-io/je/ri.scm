(define-module (crates-io je ri) #:use-module (crates-io))

(define-public crate-jericho-0.0.1 (crate (name "jericho") (vers "0.0.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)))) (hash "08z80cimlsc0sn8vfsbcpbqdgnz20j4nv7d2bc7pg9cmw6m0aazl")))

