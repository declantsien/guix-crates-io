(define-module (crates-io je ro) #:use-module (crates-io))

(define-public crate-jerome-0.1 (crate (name "jerome") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cli-clipboard") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1cp7b3akpq2ph9mas0i17fbvprbv6lvfapzv74s42pn67g5hkw3h")))

