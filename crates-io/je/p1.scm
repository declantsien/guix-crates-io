(define-module (crates-io je p1) #:use-module (crates-io))

(define-public crate-jep106-0.1 (crate (name "jep106") (vers "0.1.0") (deps (list (crate-dep (name "pdf-extract") (req "^0.4.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 1)))) (hash "11vhrimv5sff1bh2hz0vl22y6m89a2fg446q10z24dq2arias60v")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.0") (deps (list (crate-dep (name "pdf-extract") (req "^0.4.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 1)))) (hash "0p2bkzj9bm9hqdi007q1rldyak1m00j258hfd446ks2hdbnlx791")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.1") (deps (list (crate-dep (name "pdf-extract") (req "^0.4.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 1)))) (hash "1l36mz7wsq340jns5nvb5ii9lb67hip7vk3f6klki78p36k0swnk")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.2") (deps (list (crate-dep (name "pdf-extract") (req "^0.4.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 1)))) (hash "0nr9ky1ivdjx2va7fmc0z2qhg6mrgrsip9dbwj40wca7r5zd9al2")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.3") (deps (list (crate-dep (name "pdf-extract") (req "^0.4.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rj6pbzhy8klg06n0ppshn39x5wmx5p8cjj5vpfdmcniahc38787")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ly6023f92bnc2wmv1rg2zmszwm59vimj2hm94wh9j7vwj7d0z7m")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.5") (deps (list (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1x0llg59d4pnfhyf80qgkn8h5vjqk2gf4mrpi2q5schr0p97d64k")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.6") (deps (list (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lsr88y0cj0ns3vyi570yc83rqynd6l2366vki6pm6k559d9c3z8")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.7") (deps (list (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jasiqrf55x8lfah6qxlkgvrksrjsp0idvffl9ja3b03rialyxm0")))

(define-public crate-jep106-0.2 (crate (name "jep106") (vers "0.2.8") (deps (list (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1c50q10qv6hps8k2s75qd79jpqnarjjgd3vhi3jpzr3nw4xb74zz")))

