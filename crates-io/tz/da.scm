(define-module (crates-io tz da) #:use-module (crates-io))

(define-public crate-tzdata-0.1 (crate (name "tzdata") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "1aas3lcg9179nw4l7690z79nl4rzkgiq0651fgzwwman5b349wg0")))

(define-public crate-tzdata-0.2 (crate (name "tzdata") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "10m0pq65izkr7c2vsvnhzvmyvxvmbqxhnh9ipnmjzvwhakxq6nhm")))

(define-public crate-tzdata-0.3 (crate (name "tzdata") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "105d9ykr5j6i2cdjdk3xjscymfz9568rbzqjy8808hx6rb1b1f0w")))

(define-public crate-tzdata-0.3 (crate (name "tzdata") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "02mjhlr42c8lx0vci11ncq6xgs133k4j8sqz5d2qbd3b2mjr59q1")))

(define-public crate-tzdata-0.4 (crate (name "tzdata") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "1bk7phf0rzz56kmwmfm22am5rm8l3rkwvhmh9a1npdccxsa0mpa0")))

(define-public crate-tzdata-0.4 (crate (name "tzdata") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "1ncf19j6w1dfx4f5apyqbb56llm0rgsg9b4v0yyyw86vc65vk5zh")))

