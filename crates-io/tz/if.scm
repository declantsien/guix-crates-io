(define-module (crates-io tz if) #:use-module (crates-io))

(define-public crate-tzif-0.1 (crate (name "tzif") (vers "0.1.0") (deps (list (crate-dep (name "combine") (req "^4.6.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "1l560bq1kcyaav5brq626vr67jwdi8pj6c7yr32sg3z2nlfrkmj1")))

(define-public crate-tzif-0.2 (crate (name "tzif") (vers "0.2.0") (deps (list (crate-dep (name "combine") (req "^4.6.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "17lyr1yyq5351g3dj1kia6f3qaiq2panqyp5bdcl3ghcfrgzm1kp")))

(define-public crate-tzif-0.2 (crate (name "tzif") (vers "0.2.1") (deps (list (crate-dep (name "combine") (req "^4.6.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "1wlz4kw6hci4lh7sm0hy3596808dzam5jdix03gvn60inin5q3b6")))

(define-public crate-tzif-0.2 (crate (name "tzif") (vers "0.2.2") (deps (list (crate-dep (name "combine") (req "^4.6.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "1v2gzbx8qdbiyba5zxm49vfgj66hi9mvihcpvi3c8lzhiigw79fa") (rust-version "1.66")))

(define-public crate-tzif-0.2 (crate (name "tzif") (vers "0.2.3") (deps (list (crate-dep (name "combine") (req "^4.3.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "1zb62s2rnh2a7hs0m8p2ppdjcp6xj5m0kvx7xvrwc1lnj8cfpf31") (rust-version "1.67")))

