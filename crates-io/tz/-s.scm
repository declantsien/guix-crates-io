(define-module (crates-io tz -s) #:use-module (crates-io))

(define-public crate-tz-search-0.1 (crate (name "tz-search") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dfpg0l5bli7r54f43sjsyz5nynkmbw7kgvhvd56ysj3xj06mmw0")))

(define-public crate-tz-search-0.1 (crate (name "tz-search") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0h4rwrr3brbkpc7q4khc9dyi447i7mp0rv0sbkdwp950nwc9brlb")))

