(define-module (crates-io hc #{25}#) #:use-module (crates-io))

(define-public crate-hc256-0.1 (crate (name "hc256") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0w90s85nc8ij92ihjh2zqr87xv6pybqj3i9z8sq57am5fzckaa0d")))

(define-public crate-hc256-0.1 (crate (name "hc256") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0izfv3mxrgkvniaynaqh4pprv6f094zvhbkp6x7mm19r1xhi7klv")))

(define-public crate-hc256-0.1 (crate (name "hc256") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "01cz6jss9fhvzdq0k814wcn45r12canzqcb4n4zi0w72mnh3y149")))

(define-public crate-hc256-0.1 (crate (name "hc256") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^0.5") (kind 0)))) (hash "05vi8by3hnpbsr1frw684sfzp4la8p78fbclxsa9rbd4iavk3pc5")))

(define-public crate-hc256-0.1 (crate (name "hc256") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^0.5") (kind 0)))) (hash "0j2nc7kv4x9llp1hkbw9g8pg0rn2rhsp5qcdpd79srcnx0gj9n6w")))

(define-public crate-hc256-0.1 (crate (name "hc256") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)))) (hash "0d2fzr8fcnvxhnyz8salz2481rz9592r01nsjh00zjp0axp0dasq")))

(define-public crate-hc256-0.1 (crate (name "hc256") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)))) (hash "0xs86gll5qrbjg5m578p7fzasj1qrqsvrb2igvd3xh6fcpxmxnk7")))

