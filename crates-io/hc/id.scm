(define-module (crates-io hc id) #:use-module (crates-io))

(define-public crate-hcid-0.0.3 (crate (name "hcid") (vers "0.0.3-alpha") (deps (list (crate-dep (name "data-encoding") (req "= 2.1.2") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "= 0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0hflayafcz57gdbg3p0gb40ck44xk8q1qvl9i5gpdwmkffikwr6h")))

(define-public crate-hcid-0.0.6 (crate (name "hcid") (vers "0.0.6") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "reed-solomon") (req "= 0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0bpxikisfn0nvzramns4n409swfvs3n95i6gbpnjvpqpddzs4ply")))

