(define-module (crates-io hc od) #:use-module (crates-io))

(define-public crate-hcode_functions-0.1 (crate (name "hcode_functions") (vers "0.1.0") (hash "019zxb3i14qvgv31kmmk9grclh5jbjs0l04xhxc6jh4ll6k22z70")))

(define-public crate-hcode_functions-0.1 (crate (name "hcode_functions") (vers "0.1.1") (hash "168w3h75y3ix4li056a3bsvz7y2djk6i6ms9a7px71i3x7y4a1z4")))

(define-public crate-hcode_functions-0.1 (crate (name "hcode_functions") (vers "0.1.2") (hash "1ycv1hhns14qgkcz9iywvpy27dvnh2ydffk58svvsc5crwckj2rx")))

(define-public crate-hcode_functions2-0.1 (crate (name "hcode_functions2") (vers "0.1.0") (hash "0n9kzmci5yvy12y0gk89dc472p15pvpl443bp69cpm04sg81j6qc")))

(define-public crate-hcode_functions2-0.1 (crate (name "hcode_functions2") (vers "0.1.1") (hash "0zg7dwzkh2sm8p819668cwfzzvn2q1v5nmy55ilzz03kzsqxr3n3")))

(define-public crate-hcode_functions_eduardo-0.1 (crate (name "hcode_functions_eduardo") (vers "0.1.0") (hash "1mqwcdwhwsdadjhv93rvaws30i2ghfqjcdmgcsg14acljq618s7z")))

(define-public crate-hcode_rust-0.1 (crate (name "hcode_rust") (vers "0.1.0") (hash "1dnlzgpv99n2f0p1q6ing9a752xxq2say1bggb506bkyj6lq3g65")))

(define-public crate-hcode_rust-0.1 (crate (name "hcode_rust") (vers "0.1.1") (hash "05jgrcg5rdgw2j574485x5l8hks3dw10aj05znb11yxnj63xh1zj")))

