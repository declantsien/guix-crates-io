(define-module (crates-io hc #{12}#) #:use-module (crates-io))

(define-public crate-hc12-at-0.1 (crate (name "hc12-at") (vers "0.1.0") (deps (list (crate-dep (name "at-commands") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "debugless-unwrap") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "06kb3jal8ips2khq8y4m1dsnnbf4c3v54kqm0jik5z55xiydsqah")))

(define-public crate-hc12-at-0.2 (crate (name "hc12-at") (vers "0.2.0") (deps (list (crate-dep (name "at-commands") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "debugless-unwrap") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (kind 0)))) (hash "1lnpxc0yr05fll3xzabsq4p2yzl610kc6rhrcixr0pw45iybw46n")))

(define-public crate-hc128-0.1 (crate (name "hc128") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gdlrb70xsbvr4x2yslrk625pkmydfd8hnkb5szjbwyrgsl4i7mx")))

(define-public crate-hc128-0.1 (crate (name "hc128") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 2)))) (hash "1bj1sdb76gv5sr7z6jvmyjw4fh5l326lcxccmr916x044xzibrld")))

(define-public crate-hc128-0.1 (crate (name "hc128") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 2)))) (hash "0i2s6gaa8v7lhcq1p9cw1swxlrbl7s0lsawqv36kbi6hfr2m22cf")))

(define-public crate-hc128-0.1 (crate (name "hc128") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^0.5") (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 2)))) (hash "0fxgyrr17w6lbivgid313yrrkmqh2v8591v9fdkmf3rz62p9ikdl")))

(define-public crate-hc128-0.1 (crate (name "hc128") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^0.5") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 2)))) (hash "08ndpc0bvrw0d1smv0bv8f0yyaw9i8q4s92sa577qqfxfk4m5xxl")))

(define-public crate-hc128-0.1 (crate (name "hc128") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 2)))) (hash "11f6fd8il3rr3mnmzfw8i2sgxgrz9z08rwll12cqilsd6jwfk3hb")))

(define-public crate-hc128-0.1 (crate (name "hc128") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 2)))) (hash "08vhwkha1s63718cf5j0hfdcyypa4r8i455sylzqb5vccl6zxls0")))

