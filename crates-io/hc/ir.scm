(define-module (crates-io hc ir) #:use-module (crates-io))

(define-public crate-hciraw-1 (crate (name "hciraw") (vers "1.0.1") (deps (list (crate-dep (name "fd") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mjj1jhb0pdw349y7d32jwhbsi7rh6lp27chhqyz3s12rmc5y3si")))

