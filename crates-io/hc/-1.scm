(define-module (crates-io hc #{-1}#) #:use-module (crates-io))

(define-public crate-hc-128-0.0.0 (crate (name "hc-128") (vers "0.0.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "block-cipher-trait") (req "^0.6") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "stream-cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "stream-cipher") (req "^0.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "zeroize") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "03byra6l73lx3s3mwanpbjscl01j5c5a2ycjy7nbihhz3m4m8cv8")))

