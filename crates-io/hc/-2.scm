(define-module (crates-io hc #{-2}#) #:use-module (crates-io))

(define-public crate-hc-256-0.0.1 (crate (name "hc-256") (vers "0.0.1") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "block-cipher-trait") (req "^0.6") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "stream-cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "stream-cipher") (req "^0.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "zeroize") (req "^1.0.0-pre") (optional #t) (default-features #t) (kind 0)))) (hash "0xnsnpqyx5fv3xscf6bndxsw42vc1m6bl98cwh9nrjl860hp2236")))

(define-public crate-hc-256-0.1 (crate (name "hc-256") (vers "0.1.0") (deps (list (crate-dep (name "block-cipher") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.7") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "stream-cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stream-cipher") (req "^0.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "zeroize") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0n9dc7jm46wvxdi5f1b8g6pfykjzn5xij2hrk70p5hxj5zs951rq")))

(define-public crate-hc-256-0.2 (crate (name "hc-256") (vers "0.2.0") (deps (list (crate-dep (name "stream-cipher") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1p3wbpjwqq7jl728c0vhijl37vwm7khzmv555a28q7bsyicfy49a")))

(define-public crate-hc-256-0.3 (crate (name "hc-256") (vers "0.3.0") (deps (list (crate-dep (name "cipher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "01h4v6dlnwf8653k4hf90rbzjh2bf37yskj9114lsvkchjya4k7k")))

(define-public crate-hc-256-0.4 (crate (name "hc-256") (vers "0.4.0") (deps (list (crate-dep (name "cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (optional #t) (kind 0)))) (hash "1l9mg4agqra29z61dcklk2bdbkvf42lzx7045jb1vl8ac5x87xvm")))

(define-public crate-hc-256-0.4 (crate (name "hc-256") (vers "0.4.1") (deps (list (crate-dep (name "cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "=1.3") (optional #t) (kind 0)))) (hash "174iv6kzvi7gp09wiyq26dr53qm662xdcy8l1hz7cwcg1plv8r9r")))

(define-public crate-hc-256-0.5 (crate (name "hc-256") (vers "0.5.0") (deps (list (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1vrqgfqc3l2c79vmhvcmyrm7c8k4ps6mnj2mihxwqxm4ppfj3l9z") (features (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std")))) (rust-version "1.56")))

