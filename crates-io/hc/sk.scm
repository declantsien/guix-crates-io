(define-module (crates-io hc sk) #:use-module (crates-io))

(define-public crate-hcskr-0.1 (crate (name "hcskr") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.35") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "01qlz39cwbvngla5f51frhpwiq10v77lnryhy5r4yy2p33mbpnnr")))

