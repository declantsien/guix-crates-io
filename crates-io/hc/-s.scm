(define-module (crates-io hc -s) #:use-module (crates-io))

(define-public crate-hc-sr04-0.1 (crate (name "hc-sr04") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "17mj1yj86h4qwfc1amcrbd5asnch33fzg5rhrg48by9i8ywbmync") (rust-version "1.63.0")))

(define-public crate-hc-sr04-0.1 (crate (name "hc-sr04") (vers "0.1.1") (deps (list (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1j10f96b381frwzxqjspjghzzd2axs0z3125c0x32y1xrmsffnw7") (rust-version "1.63.0")))

(define-public crate-hc-sr04-0.1 (crate (name "hc-sr04") (vers "0.1.2") (deps (list (crate-dep (name "rppal") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "0c9k5rjbjvslc4hhlh0ng42m3qy3nxd81w9qjqz1f4hgayxmjj9v") (rust-version "1.63.0")))

