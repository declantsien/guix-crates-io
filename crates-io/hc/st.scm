(define-module (crates-io hc st) #:use-module (crates-io))

(define-public crate-hcstatic-str-0.1 (crate (name "hcstatic-str") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0s1wl7am4gj5risk730k8m6dbx7h3bbdiyar456r0xmlclr2xj8v")))

(define-public crate-hcstatic-str-0.1 (crate (name "hcstatic-str") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "09ywj4fkzip27dnml3m8jnh37wzd40pxd5bg84nbqsizncpnrdvy")))

