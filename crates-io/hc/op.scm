(define-module (crates-io hc op) #:use-module (crates-io))

(define-public crate-hcop-0.1 (crate (name "hcop") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0svnl13h5n5haa4ws83vgfz3n3l7b5m6viw04s64p1s146ljcl84")))

