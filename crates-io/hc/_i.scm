(define-module (crates-io hc _i) #:use-module (crates-io))

(define-public crate-hc_iz_membrane_manager-0.1 (crate (name "hc_iz_membrane_manager") (vers "0.1.0-beta-rc.3") (deps (list (crate-dep (name "hdi") (req "=0.2.0-beta-rc.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rcvch031ldkagrvyny9m8g18zga9h7vsvknhs7nsa683fgwfw6h")))

(define-public crate-hc_iz_membrane_manager-0.1 (crate (name "hc_iz_membrane_manager") (vers "0.1.0") (deps (list (crate-dep (name "hdi") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s9bly79qm871kapfs6qv8v16bd7nw8wyizy99z89r2fdg93vhhh")))

(define-public crate-hc_iz_membrane_manager-0.1 (crate (name "hc_iz_membrane_manager") (vers "0.1.1") (deps (list (crate-dep (name "hdi") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1454dx3jjmb807iwz20qbhmfz3sznqx14qjq4y11js2bcyf64zbl")))

(define-public crate-hc_iz_membrane_manager-0.1 (crate (name "hc_iz_membrane_manager") (vers "0.1.2") (deps (list (crate-dep (name "hdi") (req "=0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h4lmskcljwg58hdd1s8gf7ah61br5pz942n638y7nbiqkjnydv3")))

(define-public crate-hc_iz_membrane_manager-0.1 (crate (name "hc_iz_membrane_manager") (vers "0.1.2-alpha.0") (deps (list (crate-dep (name "hdi") (req "=0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qnq3w7vzp9bb5qm2igd2vmj88kvmyvpcpr9pq2db8sa1hj82g4f")))

(define-public crate-hc_iz_membrane_manager-0.1 (crate (name "hc_iz_membrane_manager") (vers "0.1.2-alpha.1") (deps (list (crate-dep (name "hdi") (req "=0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ij6nq52r6ca2f7djlj8vqrgc1vwq0wpgflhfmphc8b7rh0h0kdx")))

(define-public crate-hc_iz_membrane_manager-0.2 (crate (name "hc_iz_membrane_manager") (vers "0.2.0") (deps (list (crate-dep (name "hdi") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i2s33hyw803n4fn1srli3642pn3s3d6c1l7kivfvikl8zh18h8z")))

(define-public crate-hc_iz_membrane_manager-0.2 (crate (name "hc_iz_membrane_manager") (vers "0.2.1-beta-dev.0") (deps (list (crate-dep (name "hdi") (req "=0.3.1-beta-dev.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "10j1yvrfww1d0ccq65c6mr5y7k286l13a6cv055gq2r7ai877brs")))

(define-public crate-hc_iz_membrane_manager-0.3 (crate (name "hc_iz_membrane_manager") (vers "0.3.0-beta-dev.5") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "02n6ggsabk7fxmq65n8kw2lj9s0q88a8v63ip61nk890di4z5zbv")))

(define-public crate-hc_iz_membrane_manager-0.2 (crate (name "hc_iz_membrane_manager") (vers "0.2.1") (deps (list (crate-dep (name "hdi") (req "=0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dxbqhnf878d35cqckmxmlfmc3jhi1iq52dd5n1pwd0gcmdz5rk1")))

(define-public crate-hc_iz_membrane_manager-0.2 (crate (name "hc_iz_membrane_manager") (vers "0.2.2") (deps (list (crate-dep (name "hdi") (req "=0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "19yjfv1hyn0c4is4r4hxvbqimvjibb9hp6r0y423mad9wq17mv9f")))

(define-public crate-hc_iz_membrane_manager-0.2 (crate (name "hc_iz_membrane_manager") (vers "0.2.3") (deps (list (crate-dep (name "hdi") (req "=0.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y1n1w3gfqssg5z3dh8awlai6vb199xkgr5kn2fgq2sfd9ndq7wj")))

(define-public crate-hc_iz_membrane_manager-0.2 (crate (name "hc_iz_membrane_manager") (vers "0.2.4-rc.0") (deps (list (crate-dep (name "hdi") (req "=0.3.4-rc.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b5w6dgh5c7z036bgch51adk2j3ad0r22y7j9x9ci5s8m0lkby38")))

(define-public crate-hc_iz_membrane_manager-0.2 (crate (name "hc_iz_membrane_manager") (vers "0.2.5-rc.0") (deps (list (crate-dep (name "hdi") (req "=0.3.5-rc.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "18jigzy9gv1h8w06ninn4cv4qj6aq5ndkix2igg8c02sypzjvxf1")))

(define-public crate-hc_iz_membrane_manager-0.3 (crate (name "hc_iz_membrane_manager") (vers "0.3.0-beta-dev.35") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.31") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "10cs9mxmxd78pi4fip74fy9bm83fd04pcacx524r1x9wlk161wgl")))

(define-public crate-hc_iz_membrane_manager-0.3 (crate (name "hc_iz_membrane_manager") (vers "0.3.0-beta-dev.35-hotfix.1") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.31") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "07bix82qi7ch58jkidy0l8dcfbm3kcx9nq4xfhqr928szib1j5iy")))

(define-public crate-hc_iz_membrane_manager-0.3 (crate (name "hc_iz_membrane_manager") (vers "0.3.0-beta-dev.40") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.36") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bcbq0fmm8nig809filvql8n3yk615jjib70yvls58mml9shlrlb")))

(define-public crate-hc_iz_membrane_manager-0.4 (crate (name "hc_iz_membrane_manager") (vers "0.4.0-dev.2") (deps (list (crate-dep (name "hdi") (req "=0.5.0-dev.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wa5iwbmfz35f26gyfnjhdixw49vyix23j6ab0lrq0a0ghnxsng0")))

(define-public crate-hc_iz_membrane_manager-0.4 (crate (name "hc_iz_membrane_manager") (vers "0.4.0-dev.3") (deps (list (crate-dep (name "hdi") (req "=0.5.0-dev.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "02nfjqric543jqfn8jzcv5ngy59pkkxzpyflvl1qzpxhbr2176g8")))

(define-public crate-hc_iz_profile-0.1 (crate (name "hc_iz_profile") (vers "0.1.0-beta-rc.3") (deps (list (crate-dep (name "hdi") (req "=0.2.0-beta-rc.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ix7rknxsff9346l6xz991agdv7dnb4wwmjffiik7d6rflvz0cc9")))

(define-public crate-hc_iz_profile-0.1 (crate (name "hc_iz_profile") (vers "0.1.0") (deps (list (crate-dep (name "hdi") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fl85r1wxb5q0nnjgdv94va1khpmvmgppazdlk0fw1sm8rh2xqc8")))

(define-public crate-hc_iz_profile-0.1 (crate (name "hc_iz_profile") (vers "0.1.1") (deps (list (crate-dep (name "hdi") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "02dgmlcvlgfiwbgyb667izrafh4k1r7aq9j90bg6aa2s75ypbc20")))

(define-public crate-hc_iz_profile-0.1 (crate (name "hc_iz_profile") (vers "0.1.2") (deps (list (crate-dep (name "hdi") (req "=0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "16czfb2rvf2cf5kwhfpmc2afjf01x7w7qd9hy1i9h7b707j75dn5")))

(define-public crate-hc_iz_profile-0.1 (crate (name "hc_iz_profile") (vers "0.1.2-alpha.0") (deps (list (crate-dep (name "hdi") (req "=0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hd9vxnhaj8sk4xbhk8p11pmbyvdcknhkv3im8wmc6ikxakvczi6")))

(define-public crate-hc_iz_profile-0.1 (crate (name "hc_iz_profile") (vers "0.1.2-alpha.1") (deps (list (crate-dep (name "hdi") (req "=0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ja7acvgzi5ahxdrlqcjfjn71adxgcj2c2rlvwvd25x4y7sjsd1d")))

(define-public crate-hc_iz_profile-0.2 (crate (name "hc_iz_profile") (vers "0.2.0") (deps (list (crate-dep (name "hdi") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "051ywwnm0cklqljbrv1kr7iylblllmiwlr7cjy74xpb39sppf2jj")))

(define-public crate-hc_iz_profile-0.2 (crate (name "hc_iz_profile") (vers "0.2.1-beta-dev.0") (deps (list (crate-dep (name "hdi") (req "=0.3.1-beta-dev.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "15p8rz7dqd0lyjfi86bk7pfx9mbmimmhyivjjh5g5pp1pqg97m92")))

(define-public crate-hc_iz_profile-0.3 (crate (name "hc_iz_profile") (vers "0.3.0-beta-dev.5") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i86nfng1c18hmnz1zy2sypm1ww8gkjcba9rg87k1q2vnpw7x983")))

(define-public crate-hc_iz_profile-0.2 (crate (name "hc_iz_profile") (vers "0.2.1") (deps (list (crate-dep (name "hdi") (req "=0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "15s9z3p4mm2iv3zwi5pn3nki2rwrx6g4f7inkxf2ljl23k2y2baw")))

(define-public crate-hc_iz_profile-0.2 (crate (name "hc_iz_profile") (vers "0.2.2") (deps (list (crate-dep (name "hdi") (req "=0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mm0vzbhbsp53a9q47wrh81kniqsfkgf6lyqdby5qhbqdyvj2acv")))

(define-public crate-hc_iz_profile-0.2 (crate (name "hc_iz_profile") (vers "0.2.3") (deps (list (crate-dep (name "hdi") (req "=0.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dgy6asln4xhjp00kh32nmjsbjrn42c76fx2v61k2gpwk4g08y8l")))

(define-public crate-hc_iz_profile-0.2 (crate (name "hc_iz_profile") (vers "0.2.4-rc.0") (deps (list (crate-dep (name "hdi") (req "=0.3.4-rc.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v6m7n4mdlj8j7zjynka39b2xqrgyh9lk790wv8nxng5sg7qn4dv")))

(define-public crate-hc_iz_profile-0.2 (crate (name "hc_iz_profile") (vers "0.2.5-rc.0") (deps (list (crate-dep (name "hdi") (req "=0.3.5-rc.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r0cmdbvrxs5lby0hccmv433mv735sbj55n6fz24mn35iw68vzd7")))

(define-public crate-hc_iz_profile-0.3 (crate (name "hc_iz_profile") (vers "0.3.0-beta-dev.35") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.31") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1a56fgv006w7l8d5xsbvrfqcl6p82ph7qa6s3m5dqgpb5p4cvkkf")))

(define-public crate-hc_iz_profile-0.3 (crate (name "hc_iz_profile") (vers "0.3.0-beta-dev.35-hotfix.1") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.31") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q83rmvywwp5ns98szm9fwk937r5y0sl3iiawh72fcmh1hj342cm")))

(define-public crate-hc_iz_profile-0.3 (crate (name "hc_iz_profile") (vers "0.3.0-beta-dev.40") (deps (list (crate-dep (name "hdi") (req "=0.4.0-beta-dev.36") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hp19ygcxjbn4n9jbq1z542bjq040wg8cpc1glfwiri3gdq0bgp3")))

(define-public crate-hc_iz_profile-0.4 (crate (name "hc_iz_profile") (vers "0.4.0-dev.2") (deps (list (crate-dep (name "hdi") (req "=0.5.0-dev.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "114m95ahnfyzvrrvm8qs7avx7xhg760a6zrxz1fjbswyy65yqczk")))

(define-public crate-hc_iz_profile-0.4 (crate (name "hc_iz_profile") (vers "0.4.0-dev.3") (deps (list (crate-dep (name "hdi") (req "=0.5.0-dev.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r99ypm1276plngvgci82fdpd31jsrsrrkn9qqrzk5byhbl87cik")))

