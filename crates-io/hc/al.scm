(define-module (crates-io hc al) #:use-module (crates-io))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "15d6c2y27sqp9bvz3iysjb8hn0idsm8hxsk2xq39wwxngxls29gn")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1ii8dqjns3jxw5w2mpwnxcna2cy8izp3k6bh3dfa0ci5qxgbi2z9")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0blsz2vxh6knnk7p9hk7yw4wnlf6x0f179kkl8a7an7l74ggpk35")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1lm786xmn3b7cv57mnfvzvpj3xshcgq0plxd9p0x9rw0nkv9za9g")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1wpqxy0108kslh98j69xv4jk2m08wf0s9aj2a1qdavwdynnr6pxs")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "10ssmqrkrl6sylhkq0zkgmfcdsfhkr1lgh64m3nyaw03d4dj42rh")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1j10y0yfgkb9n8ps9k777mx0a14l1yx2lj7casbdp36509zclhn0")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0j741s65f7iwlgmc7l14hx40klpwmjdgsdnd0x0jps3435zmcdij")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.9") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0mlyqaali5gzicss7jxxz1s0pr0m4hzsj3hlrpchc7xm87h3r3dr")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.10") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "15nd5wbfdbd48qqzid52xs3r87wy70xwqgihnjvyyb1c7rw6m7ik")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.11") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0wsz050ijr79amx95xf8hx8zpd4akjxr7hryyrdgkz1ypaqdji2i")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.12") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "110bfgpv5z9pyh8ss2p92f62607cpgngd1ryl46zz3za60248srz")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.13-caesar") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0sqw7dajy4mw11vhv1xvvnnwv68zwb5cj71cg2qblyip14a3hsxx")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.14") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1qd7xsaz9am4j5f97jlysqygbmyq7xpvhlf2jzpjbmx1yqmgz2nb")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.15") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0yssi36z5fqgic79zzzniawgfp6yd1x6nff8yj6b2djz256rlfqd")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.16") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0b0krixbwslskcf61q5vad05ygkblb6k47d2r0jnwgj6dgcxdz56")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.17") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0iq2ng0qrc09afi9zpdda3f0jzg3529h1pncqnd8xgzzbxvmns89")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.18") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0x3fxhaimd00kly70zgyray1y56llz5bkijrl1xkhw22fb2dmcpd")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.19") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0wl7b1157val47gha5kgly84nlcwdax00nch0ybh2nx4avfa676z")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.20") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1mvzhm5r3azf9rhw0jbrnd5pmps9rjlkqbiy1xbh8dv96fcyk3m0") (yanked #t)))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.21") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xws7rfv86q6p22q1s9hbdpym3j2rqx2hxgn30h1fw898wrqdkjb")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.23") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0kp5hhhv9w7p0xznnjlhisdahn50b92zzq2bimzij3k2c1kf745y")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.24") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0iyy0czgq4sdx4ig61fl7svrwmqqvqmwrlfkmpf3xf0ljb7nyr0d")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.25") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1diyyx2i6bj43d3cpjigp4w1zra8h5b1zsnaj9r5fjj4zm290r4g")))

(define-public crate-hcal-0.1 (crate (name "hcal") (vers "0.1.26") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1sqd8dbvl8k4pz6dpmndwnnfnyz2fz199pc9aif5s70hlss79vj3")))

(define-public crate-hcal-0.2 (crate (name "hcal") (vers "0.2.0") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0fnpkkpslygd0djk3bb7vpzci7m380d7j8qwq03n18yx5gdai23n")))

(define-public crate-hcal-0.2 (crate (name "hcal") (vers "0.2.1") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0sgw23056xcx6c7kp0ymliskabr86wvdzldlll0w6w5aqpsynyzr")))

(define-public crate-hcal-0.3 (crate (name "hcal") (vers "0.3.0") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0sq2w0m0qc06a4axzr11hi98lxpvxi8w6j5ji682qsbx4hbm63qc") (yanked #t)))

(define-public crate-hcal-0.3 (crate (name "hcal") (vers "0.3.1") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0w592xfagws0arhjfg4fx756dx7xzqc3qaym94szwzc7gq7xczpn")))

(define-public crate-hcal-0.3 (crate (name "hcal") (vers "0.3.2") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02n75y98bnw32z2rfsgvhfbh20521nkb8ac1l9gpf6zwiirscjs2") (yanked #t)))

(define-public crate-hcal-0.3 (crate (name "hcal") (vers "0.3.3") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1qm30r11b18mw1bfyq6rfhr96nhdzvc9zrga72rgdnanw6q5kfrx")))

(define-public crate-hcal-0.3 (crate (name "hcal") (vers "0.3.4") (deps (list (crate-dep (name "bdays") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0drqsrka52h0fs60yn8z7dhm8bn02ixrw8dkk28xm1m4ax2aq46f")))

(define-public crate-hcal-0.4 (crate (name "hcal") (vers "0.4.0") (deps (list (crate-dep (name "bdays") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.13") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1wifphjf9pbnzvmbkikza9w9zfxs79md6qgix2kik71zn94d63x1")))

(define-public crate-hcal-0.4 (crate (name "hcal") (vers "0.4.1") (deps (list (crate-dep (name "bdays") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.16") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1a7bjf85v6v055q6i5xsnr8nzj53v2gi49ablnkd9dlissf2xwjq")))

(define-public crate-hcal-0.4 (crate (name "hcal") (vers "0.4.3") (deps (list (crate-dep (name "bdays") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "cbb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.16") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0gbm99saayq2pcz2rayhwl63nqmsqd2y8x6vnn87zpvm2bzjjxwj")))

