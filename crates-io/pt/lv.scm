(define-module (crates-io pt lv) #:use-module (crates-io))

(define-public crate-ptlv-0.1 (crate (name "ptlv") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "156vkvl214im21saq93ax0by3rv1cgs62dy091miaq4g7mwqsnhf")))

(define-public crate-ptlv-0.1 (crate (name "ptlv") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "1i10yqr5by553zdya3sc280b9da6nnak8za94aq1d0y154xplj75")))

