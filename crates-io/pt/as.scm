(define-module (crates-io pt as) #:use-module (crates-io))

(define-public crate-ptask-0.1 (crate (name "ptask") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "1zyi887ic256ba014aij619nda10w51198pjdq4899cq6rlxqk0i") (features (quote (("inlining") ("default" "inlining"))))))

