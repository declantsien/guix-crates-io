(define-module (crates-io pt sd) #:use-module (crates-io))

(define-public crate-ptsd-0.1 (crate (name "ptsd") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.0") (features (quote ("tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i2i32k6pgr06xljjnk5mxiway9k8h9h7mzw04h8wyckjg40znn9")))

