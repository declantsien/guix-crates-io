(define-module (crates-io pt -r) #:use-module (crates-io))

(define-public crate-pt-rtd-0.1 (crate (name "pt-rtd") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1qhb5g0b020w4b4bwr2g4xaqsjihf2mch0b20x570547a4hxyn35")))

(define-public crate-pt-rtd-0.1 (crate (name "pt-rtd") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0m58krg6553cyyl6h6hzvfpmk8ywqgmr07sz9acaj4avzqvys4qm")))

