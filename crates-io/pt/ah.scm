(define-module (crates-io pt ah) #:use-module (crates-io))

(define-public crate-ptah-0.0.0 (crate (name "ptah") (vers "0.0.0") (deps (list (crate-dep (name "serde") (req "^1") (kind 0)))) (hash "0zdwgxlz4l7zl13vcryhzsjq11qr9b4q3z8fvi4zsa5jvw7jnw9i")))

(define-public crate-ptah-0.1 (crate (name "ptah") (vers "0.1.0") (deps (list (crate-dep (name "ptah_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1w5996d0g7j7kvbx358mf09iafnrm8ys4kccla304klb4mciadm2") (features (quote (("derive" "ptah_derive") ("default" "alloc" "derive") ("alloc"))))))

(define-public crate-ptah-0.2 (crate (name "ptah") (vers "0.2.0") (deps (list (crate-dep (name "ptah_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1gdfzri4c75vqd4pki151cw0ncgfk0vdfmyl6pfq5rsmqisrqclv") (features (quote (("derive" "ptah_derive") ("default" "alloc" "derive") ("alloc"))))))

(define-public crate-ptah_derive-0.1 (crate (name "ptah_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "02ahnmd39ls1fai3pxflscdm4xb4bpy1d14cr5kl7jv39475y30j")))

(define-public crate-ptah_derive-0.2 (crate (name "ptah_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0xnm3wfwivd9wbqbicnmxvdv0yk8s085sn0ds06ypdlkqz5f7rx6")))

