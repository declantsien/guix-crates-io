(define-module (crates-io pt y2) #:use-module (crates-io))

(define-public crate-pty2-0.1 (crate (name "pty2") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">=0.2") (default-features #t) (kind 0)))) (hash "1m8m5avvlihb7v6b6ib5sz0c9iwv108ya8762a8lnrwrcgwyfqa4") (features (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

