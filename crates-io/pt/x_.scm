(define-module (crates-io pt x_) #:use-module (crates-io))

(define-public crate-ptx_compiler-0.1 (crate (name "ptx_compiler") (vers "0.1.1") (deps (list (crate-dep (name "find_cuda_helper") (req "^0.2") (default-features #t) (kind 1)))) (hash "1f36sb02s9jd5jjzcmnyr12qzas15lf6hwa0mx3qzxij9i5vz7cr")))

