(define-module (crates-io pt r-) #:use-module (crates-io))

(define-public crate-ptr-array-0.1 (crate (name "ptr-array") (vers "0.1.0") (hash "01snfn26szsdwc1fza358c4sqwhb99v07pbwvxqmbhnw1sx1ji9n")))

(define-public crate-ptr-array-0.1 (crate (name "ptr-array") (vers "0.1.1") (hash "18dwjs210q9186dlniz3rvqd8igcvkk4449sy28mzgpddj8rmmvc")))

(define-public crate-ptr-bool-0.1 (crate (name "ptr-bool") (vers "0.1.0") (hash "0fvl8mslrsnjaiwz2gvr2v7z98f93gcnzydkx63y80acx1f1z1s2")))

(define-public crate-ptr-origin-tracker-0.1 (crate (name "ptr-origin-tracker") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mashup") (req "^0.1") (default-features #t) (kind 0)))) (hash "016rbkpybsafgxiwqrgfjca5b8xgrmwzsz2iirrbhf807qjlz046")))

(define-public crate-ptr-origin-tracker-0.1 (crate (name "ptr-origin-tracker") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mashup") (req "^0.1") (default-features #t) (kind 0)))) (hash "0f7k48xwg78bqqlp0kd1np08qbg6h94311ywa0hyh3kyg3hyvp64")))

(define-public crate-ptr-origin-tracker-0.2 (crate (name "ptr-origin-tracker") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "02wg90lvrqi9194d0nnxvf3xiy52gvlc6rgmb1r06glhcwrim4w5")))

(define-public crate-ptr-union-1 (crate (name "ptr-union") (vers "1.0.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "02diz0kqvz2c5s2lj2ailkjipq9ydzfrb19r52pds7i1lvlw74g1") (features (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (yanked #t)))

(define-public crate-ptr-union-1 (crate (name "ptr-union") (vers "1.0.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0v2gh7pkh1xgw58aw5w4d8qaffdalxvhavw03lvlly085kmaxbfh") (features (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (yanked #t)))

(define-public crate-ptr-union-1 (crate (name "ptr-union") (vers "1.2.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0gddd02m2bh2zxpksq194b8knmragj60a0ksg77na0zpbm00zpgx") (features (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (yanked #t)))

(define-public crate-ptr-union-2 (crate (name "ptr-union") (vers "2.0.0-pre") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "09xdsrpm01xhxlzpla4q8m8rjw18j96vwlws59gs01pa1a459x8i") (features (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2 (crate (name "ptr-union") (vers "2.0.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "17swjfmkccsc73zfif4k0l6bnrsg6rnbjqx84wgl2xsyvqk9m1vj") (features (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2 (crate (name "ptr-union") (vers "2.0.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1b6dsspp7nvisjisk7bcjzpx20wjy547im614rj3pb6kb80dyzkv") (features (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2 (crate (name "ptr-union") (vers "2.1.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1qnfm1m2qry9nfw423v16c435l7055m2cnzs2qp8x5q1i6v8y3kl") (features (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2 (crate (name "ptr-union") (vers "2.2.0") (deps (list (crate-dep (name "autocfg") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0cja6xm05lzykxhkgz7304vax10yf4g4kb0dsrm89piv5b8zw2x9") (features (quote (("default" "alloc") ("alloc" "erasable/alloc")))) (yanked #t)))

(define-public crate-ptr-union-2 (crate (name "ptr-union") (vers "2.2.1") (deps (list (crate-dep (name "autocfg") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1khfcyfka97nzzrd03633587p6781x70xld3vrhrbjvd2s1nmfgn") (features (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

(define-public crate-ptr-union-2 (crate (name "ptr-union") (vers "2.2.2") (deps (list (crate-dep (name "autocfg") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0fipvkkjc9pdwx4ghjv77v20yx5k98gd4xdw09qs5pk1y6jbav13") (features (quote (("default" "alloc") ("alloc" "erasable/alloc"))))))

