(define-module (crates-io pt ai) #:use-module (crates-io))

(define-public crate-ptail-0.1 (crate (name "ptail") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1sj9z3ccxnssqn2ka94snrjl5pfadlqblfj9k8zp12swbc38ssby")))

(define-public crate-ptail-0.2 (crate (name "ptail") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "16xrq2mac2p4b550kc621wcqay97hf0nq2f2lvjqbzws3nyb5b0s")))

(define-public crate-ptail-0.2 (crate (name "ptail") (vers "0.2.1") (deps (list (crate-dep (name "console") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "17fgh4i6xsrzcmlni9l8nc25l22fxr1fhvd57c45b7dvcn1sz0l9")))

(define-public crate-ptail-0.2 (crate (name "ptail") (vers "0.2.2") (deps (list (crate-dep (name "console") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0n84lndzsikkv27cjsp5m2rv7q6rj6f7r689rsviq72my77j7xin")))

