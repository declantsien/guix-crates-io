(define-module (crates-io pt ri) #:use-module (crates-io))

(define-public crate-ptrie-0.5 (crate (name "ptrie") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1g76g3r0jqm073dsarn6dngm6j3hy8hgf5g778n7z4jq2j4p6c4w") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.5 (crate (name "ptrie") (vers "0.5.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1shji41qawmzb0jr1v2b9i89s3dpnpxafmikp8qxj8zcpcr20ml4") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.5 (crate (name "ptrie") (vers "0.5.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08pfj7gg5hyp4r797cj0bx78344n1wb4khfzf501r9wln2nr60bd") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.6 (crate (name "ptrie") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1xhwpagcd2cxd1svp9zbg6mwxl8i20cfc3l2jrc5iz3nx5zyxnjw") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ptrie-0.7 (crate (name "ptrie") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "11wmpjd98invwjrhnz8azjrz7pxa9sw0gwxfrzpchi5ycvslqq2s") (v 2) (features2 (quote (("serde" "dep:serde"))))))

