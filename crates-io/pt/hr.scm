(define-module (crates-io pt hr) #:use-module (crates-io))

(define-public crate-pthread-0.1 (crate (name "pthread") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "0knh38jzwqa4vdxls2wmg9fn28n8qkql90vv2pcsvj98nx34g6z2")))

