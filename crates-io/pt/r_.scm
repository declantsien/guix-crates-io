(define-module (crates-io pt r_) #:use-module (crates-io))

(define-public crate-ptr_cell-1 (crate (name "ptr_cell") (vers "1.0.0") (hash "0csczpcbmgpwb5yskr0a7bwv78arshff9jrlvkf54557b1kygwsf")))

(define-public crate-ptr_cell-1 (crate (name "ptr_cell") (vers "1.0.1") (hash "0x5bpjl0dj2ig56lpp7wiq7yd42k19hswrrmdhwjj0zj4h204bbg")))

(define-public crate-ptr_cell-1 (crate (name "ptr_cell") (vers "1.1.0") (hash "0ya5gz044yfqpvm26c4lnj15fgwn74x1wld6gbzjwj27rfdisana")))

(define-public crate-ptr_cell-1 (crate (name "ptr_cell") (vers "1.2.0") (hash "0vsgr27vww0jaa25bwkir92wvjqizfy5as7y876pg0v9dvb1mwvp")))

(define-public crate-ptr_cell-1 (crate (name "ptr_cell") (vers "1.2.1") (hash "1d2b1vbjcrq40j9k4xvifm6l71mig30qcfycy5mv1iqv0sk8r4kn")))

(define-public crate-ptr_cell-2 (crate (name "ptr_cell") (vers "2.0.0") (hash "1imppvpkj13ciyi0x266zjc1nqzqza83px6rwf0w0qc4x21ygh92")))

(define-public crate-ptr_cell-2 (crate (name "ptr_cell") (vers "2.1.0") (hash "1155anw19p7acjkd4q83bisb8709rnp588xzfbysndm666ghpwxk")))

(define-public crate-ptr_cell-2 (crate (name "ptr_cell") (vers "2.1.1") (hash "09lajzl08cy8lghlh08kgkj338v2xych3k986x4450h5yd23smy1")))

(define-public crate-ptr_cell-2 (crate (name "ptr_cell") (vers "2.2.0") (hash "0ymrcrcf8sjzqwf44r4pymkyralj9j76kknhw3f6blhyddi4v7kg")))

(define-public crate-ptr_eq-0.0.1 (crate (name "ptr_eq") (vers "0.0.1") (deps (list (crate-dep (name "ptr_eq-macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "14h49v00lhzd0knbl5qbw1794jkv6jz9xcvf59hg0b0dllaa3gyc")))

(define-public crate-ptr_eq-macros-0.0.1 (crate (name "ptr_eq-macros") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "171nxgvi9gfv5a9041wqqz0ap0a2pk94i7k6x3qd62dl4nli58hs")))

(define-public crate-ptr_hash-0.1 (crate (name "ptr_hash") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cityhash-102-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "common_traits") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "epserde") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "epserde-derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fastmurmur3") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "highway") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "metrohash") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "murmur2") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "murmur3") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "radsort") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "rdst") (req "^0.20.11") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sucds") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 0)) (crate-dep (name "wyhash") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.7") (features (quote ("xxh64" "xxh3"))) (default-features #t) (kind 0)))) (hash "0xnmj3xy0id32in0gr9wcdi4vmpii2vxbn09q0df9gjrjzg26rif") (features (quote (("default")))) (v 2) (features2 (quote (("epserde" "dep:epserde" "dep:epserde-derive"))))))

(define-public crate-ptr_hash-0.1 (crate (name "ptr_hash") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cityhash-102-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "common_traits") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "epserde") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "epserde-derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fastmurmur3") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "highway") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "metrohash") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "murmur2") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "murmur3") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "radsort") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "rdst") (req "^0.20.11") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sucds") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 0)) (crate-dep (name "wyhash") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.7") (features (quote ("xxh64" "xxh3"))) (default-features #t) (kind 0)))) (hash "167r9lgh2fq41s3ml485xjck3jfbwz6c2fr5mppcc37vp955lq6z") (features (quote (("default")))) (v 2) (features2 (quote (("epserde" "dep:epserde" "dep:epserde-derive"))))))

(define-public crate-ptr_info_lib-0.2 (crate (name "ptr_info_lib") (vers "0.2.1") (hash "1zvj24rvysfwbc2l5qbx4wchb8digms0dknslfizw321j7xgxj6i")))

(define-public crate-ptr_info_lib-0.2 (crate (name "ptr_info_lib") (vers "0.2.2") (hash "1lgzgviyffr0pi43xgxbj901kcnr4zv7a2abnvjdgm39a0xlhcnd")))

(define-public crate-ptr_iter-0.1 (crate (name "ptr_iter") (vers "0.1.0") (hash "1irm9xddjjcfpxck1zabvzyylqpwcm7m14gcizs918yy31ykaj3i")))

(define-public crate-ptr_iter-0.1 (crate (name "ptr_iter") (vers "0.1.1") (hash "0a7z7b51vna7hx03yhfajgdzmr8vp4l514wv64pzblzr1z3d471k")))

(define-public crate-ptr_meta-0.1 (crate (name "ptr_meta") (vers "0.1.0") (deps (list (crate-dep (name "ptr_meta_derive") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0abpyc795gfjpkqzwdzzzbzw57y7phc2xqw62kpa2g9xw2v1qgcp")))

(define-public crate-ptr_meta-0.1 (crate (name "ptr_meta") (vers "0.1.1") (deps (list (crate-dep (name "ptr_meta_derive") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "1651cw06zszrd965hvjz6kggywcc981805ivfgaanls0pgs81nda")))

(define-public crate-ptr_meta-0.1 (crate (name "ptr_meta") (vers "0.1.2") (deps (list (crate-dep (name "ptr_meta_derive") (req "=0.1.2") (default-features #t) (kind 0)))) (hash "04m6jg7isay9wrgphnp3gvjq9ci9y60qfp7hrgiiy9pzfqvp1r3d")))

(define-public crate-ptr_meta-0.1 (crate (name "ptr_meta") (vers "0.1.3") (deps (list (crate-dep (name "ptr_meta_derive") (req "=0.1.2") (default-features #t) (kind 0)))) (hash "0601nvrjw9kyl9admybqqnkmj0jyz7bgw6ik0g24cahq419ja93l") (features (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.1 (crate (name "ptr_meta") (vers "0.1.4") (deps (list (crate-dep (name "ptr_meta_derive") (req "=0.1.4") (default-features #t) (kind 0)))) (hash "1wd4wy0wxrcays4f1gy8gwcmxg7mskmivcv40p0hidh6xbvwqf07") (features (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.2 (crate (name "ptr_meta") (vers "0.2.0") (deps (list (crate-dep (name "ptr_meta_derive") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "01w6w8k9bf53zl4w16mz9k25vspdclw096lcykajxi06m86sibdw") (features (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.3 (crate (name "ptr_meta") (vers "0.3.0-pre1") (deps (list (crate-dep (name "ptr_meta_derive") (req "^0.3.0-pre1") (default-features #t) (kind 0)))) (hash "13hi8xczm084zlgv6nwmp3lxrikv1yqc6p7hszn8x91km1r64pih") (features (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta-0.3 (crate (name "ptr_meta") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "ptr_meta_derive") (req "^0.3.0-alpha.2") (default-features #t) (kind 0)))) (hash "0mkg2x6warj82mk6i7dr4dayx11k5brmcfdrgm9jrjdqpryarbcx") (features (quote (("std") ("default" "std"))))))

(define-public crate-ptr_meta_derive-0.1 (crate (name "ptr_meta_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yi8m0hn5lh1vgdqkcc4b743qhv62wvah9nfxz2r7w918qzdr6mw")))

(define-public crate-ptr_meta_derive-0.1 (crate (name "ptr_meta_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18w9h2vns1f5g9nwyvnz9zp07lhhkxyc199xf0awszvxz8r65kbv")))

(define-public crate-ptr_meta_derive-0.1 (crate (name "ptr_meta_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "042fwpas3fnfglwyhyjznq0nm6307ph3gbi37y6m13vjcfc5n02k")))

(define-public crate-ptr_meta_derive-0.1 (crate (name "ptr_meta_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b69cav9wn67cixshizii0q5mlbl0lihx706vcrzm259zkdlbf0n")))

(define-public crate-ptr_meta_derive-0.2 (crate (name "ptr_meta_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1llf2rb91da8f6di71c5bzq7xdzkl08g3ddf91apq3p2y96j5adw")))

(define-public crate-ptr_meta_derive-0.3 (crate (name "ptr_meta_derive") (vers "0.3.0-pre1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b5axmqb7rb284qy39ib0ikiib9qxnxwgn6x9n5nhmks13nb89mv")))

(define-public crate-ptr_meta_derive-0.3 (crate (name "ptr_meta_derive") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i8qwywzvjbh2c0cqy9argdjg972kgf80mjg4pggid1kvllp8rd1")))

