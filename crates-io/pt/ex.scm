(define-module (crates-io pt ex) #:use-module (crates-io))

(define-public crate-ptex-0.0.0 (crate (name "ptex") (vers "0.0.0") (hash "1lnl6k6qlcwmcg8vd0znqncn751wvwfv8132xznvwsl652bsd8ag")))

(define-public crate-ptex-0.0.1 (crate (name "ptex") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ptex-sys") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "16j6a3fgb9bg5xm0aarjncvdrk4l87w69rfybqzy6yvcgabxw64q")))

(define-public crate-ptex-0.0.6 (crate (name "ptex") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_float_eq") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ptex-sys") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g27bm0jq8y186jfz6pjv3ynnr0rllaib2mzwz9wfsxsh5zy2nrw")))

(define-public crate-ptex-0.2 (crate (name "ptex") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "assert_float_eq") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "cxx") (req "^1.0") (features (quote ("c++17"))) (default-features #t) (kind 0)) (crate-dep (name "ptex-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cbn4ylsdi5y9n02pgi8v95p0aa8s9daynzxb4psal2rnpz9icm8")))

(define-public crate-ptex-0.3 (crate (name "ptex") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "assert_float_eq") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "cxx") (req "^1.0") (features (quote ("c++17"))) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "ptex-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rplc6m2v4xv2jpkp9ixvbky92ixj7fl5j1j3kv81hf3arfj1p9n")))

(define-public crate-ptex-rs-0.0.1 (crate (name "ptex-rs") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zcbhw0h48x3m3a0lamy5603c6ba2d12gwhgn8ggzxlijwmfvvid") (yanked #t)))

(define-public crate-ptex-sys-0.0.0 (crate (name "ptex-sys") (vers "0.0.0") (hash "1nydh9571xnm9hq88k55i29m9xp929pgvvrccvvb5xz3x7n03d44")))

(define-public crate-ptex-sys-2 (crate (name "ptex-sys") (vers "2.4.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)))) (hash "1ka5w7w7b3w4ihqk3jzqzvhgyd3qrpzr37jhmpr5wyn5zl1nprn0") (yanked #t)))

(define-public crate-ptex-sys-0.0.1 (crate (name "ptex-sys") (vers "0.0.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)))) (hash "040fl21fcqbb9fwgs303y993zzsnaaazdli2l7qbg98y1rvf7pcy") (yanked #t)))

(define-public crate-ptex-sys-0.0.2 (crate (name "ptex-sys") (vers "0.0.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)))) (hash "1n0l04zhn7vrwdxq8r1sbh8cfq42zz0gkwsa6vqz3hpdsgsqfv58") (yanked #t)))

(define-public crate-ptex-sys-0.0.3 (crate (name "ptex-sys") (vers "0.0.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)))) (hash "1s4vkgkmfvxxq4607rnv3l347q3yfi18f75s871pil15mmbzb12i") (yanked #t)))

(define-public crate-ptex-sys-0.0.4 (crate (name "ptex-sys") (vers "0.0.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)))) (hash "03803xirhlgr8pib8zixrv8dyp5b24ga87d4985wdb0y85y7kp17") (yanked #t)))

(define-public crate-ptex-sys-0.0.5 (crate (name "ptex-sys") (vers "0.0.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)))) (hash "11w92vw8kf3k2cys1g0a4xh83rpakhgdc12jc7dadxjid0yczpz3")))

(define-public crate-ptex-sys-0.0.6 (crate (name "ptex-sys") (vers "0.0.6") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)))) (hash "0r08l36qmmrz82ban1bl33gs1xi56y3c5574ay8pxysgvarjh9yh")))

(define-public crate-ptex-sys-0.2 (crate (name "ptex-sys") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0") (features (quote ("c++17"))) (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)))) (hash "0wrqxw91rrjqad8z4wax32gyw4gfq71s7r7jfzvqsqm0yz3gr1c4") (links "Ptex")))

(define-public crate-ptex-sys-0.3 (crate (name "ptex-sys") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0") (features (quote ("c++17"))) (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "1qm5l1szsshzyrbx7lal2x85wbv1fzkrb963jmv1lp7sajpwf7sj") (links "Ptex")))

