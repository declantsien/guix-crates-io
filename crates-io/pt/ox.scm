(define-module (crates-io pt ox) #:use-module (crates-io))

(define-public crate-ptoxide-0.1 (crate (name "ptoxide") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.14.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0adir17zl2sknxyn3hmajxcd1nfpcbn1r3y73lj4qvg6yl8bwvrn")))

