(define-module (crates-io pt y3) #:use-module (crates-io))

(define-public crate-pty3-0.1 (crate (name "pty3") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.144") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y7603qss04yxpvkw2gjfb88l5di2264y5hddfbj8jk9viw59jzk")))

(define-public crate-pty3-0.1 (crate (name "pty3") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1x71lqv4p4qc2hg0baxw638qld5dpqj8y5r8innqkzspg8jsp0vj")))

