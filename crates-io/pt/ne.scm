(define-module (crates-io pt ne) #:use-module (crates-io))

(define-public crate-ptnet-core-0.1 (crate (name "ptnet-core") (vers "0.1.0") (hash "096vxsrwk9a679hgfyrwf4xmc5g81m6rcp9r58497l1s208kik7k")))

(define-public crate-ptnet-core-0.1 (crate (name "ptnet-core") (vers "0.1.1") (hash "1vj6fr044gwzry5vs678m62anda389gw8n9z3hq7k1g53sfq1r7c")))

(define-public crate-ptnet-core-0.1 (crate (name "ptnet-core") (vers "0.1.2") (hash "12lh0jzgx27gghp42ax8zrb30q9pb4vd80jlyarhvi0a514ly1sm")))

(define-public crate-ptnet-elementary-0.1 (crate (name "ptnet-elementary") (vers "0.1.0") (deps (list (crate-dep (name "ptnet-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1dh1z389nx7gp23fghnr14aj66vlamvr7i8sddvradgw7i2x0raa")))

(define-public crate-ptnet-elementary-0.1 (crate (name "ptnet-elementary") (vers "0.1.1") (deps (list (crate-dep (name "ptnet-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zc6ralxgbjwiizpf4cm4yh2hs0zzdi8gky53gxq7w22iq591bwc")))

(define-public crate-ptnet-elementary-0.1 (crate (name "ptnet-elementary") (vers "0.1.2") (deps (list (crate-dep (name "ptnet-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "08w1rpa1vvn7wyy2719ky7mqf49wrg9cs01cjc4rgzs1nylf2mad")))

