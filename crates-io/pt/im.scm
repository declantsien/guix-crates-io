(define-module (crates-io pt im) #:use-module (crates-io))

(define-public crate-ptime-0.1 (crate (name "ptime") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "02mq50b5ahdd4mpg4kmz015xva6zb550ajb2kbj214k64iwv9ksf")))

(define-public crate-ptime-0.1 (crate (name "ptime") (vers "0.1.1") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1506svb3vxw1mnqm7j75di810yk3xicxk9wyx59isvxf8z7q6b14")))

