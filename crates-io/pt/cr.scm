(define-module (crates-io pt cr) #:use-module (crates-io))

(define-public crate-ptcrab-0.0.0 (crate (name "ptcrab") (vers "0.0.0") (hash "0glgcl1fq0pwr76dsr559rfz4hsx1whj77hvx6bf26h1bdj9ijj2")))

(define-public crate-ptcrab-0.0.1 (crate (name "ptcrab") (vers "0.0.1") (deps (list (crate-dep (name "duplicate") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0d4z7qbzraz7im37apwh2jqdw1zkzmm5d9rn1v3vfqbmhvnpmhia")))

(define-public crate-ptcrab-0.1 (crate (name "ptcrab") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "duplicate") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "01c1mmv9i7wb10yihqj1wnd402ydy4k2rcb2qk7x1wyf6y0ns0nm")))

