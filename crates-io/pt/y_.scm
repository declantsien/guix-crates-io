(define-module (crates-io pt y_) #:use-module (crates-io))

(define-public crate-pty_closure-0.1 (crate (name "pty_closure") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gkx4cn47dsqnn7xvsar0h22wg94dmqqgvxxq0a4fc3fyipazj62")))

(define-public crate-pty_closure-0.1 (crate (name "pty_closure") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zfxhic3xkx1ay02sa8z41y7hcm9lfn3avrwvwlb1s79ry2md50q")))

(define-public crate-pty_closure-0.1 (crate (name "pty_closure") (vers "0.1.2") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lgnqv14fl4yvvkh8bfrhk602xpwv7n427kr0j1ykhn29jr57zkz")))

