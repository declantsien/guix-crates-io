(define-module (crates-io pt b-) #:use-module (crates-io))

(define-public crate-ptb-reader-0.1 (crate (name "ptb-reader") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "02x85a8xv12g64kgxsnpk34x084kxcabz3lclbqm200sqb60asv9")))

(define-public crate-ptb-reader-0.2 (crate (name "ptb-reader") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kk573id5lpycy02y0kk8xkrvj08fsd3mbwd7zmfdhrs1xwb6gb4")))

(define-public crate-ptb-reader-0.3 (crate (name "ptb-reader") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "0a8irhs613v6sfffs1r5dcgilbdxicyv4w1j0k3wgk86gsx037jm")))

(define-public crate-ptb-reader-0.4 (crate (name "ptb-reader") (vers "0.4.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "01fq61rb685k0xpziia1f499n4yl9cb2bbmnibry57shh364l40k")))

(define-public crate-ptb-reader-0.5 (crate (name "ptb-reader") (vers "0.5.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "0g24j9hnq6l3ll8bsjgsijd4wck8y01sb43kby4314kgxyjb0s1d")))

(define-public crate-ptb-reader-0.6 (crate (name "ptb-reader") (vers "0.6.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "02vxla91q96nfcpx15xwrffw8x50zgak3dmp826qzs7mrqbbrn3h")))

(define-public crate-ptb-reader-0.7 (crate (name "ptb-reader") (vers "0.7.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gnlfd9g69pkjy6krvzaaq4dj3yqrrxam9f4dy7344s7bqpbz4c1")))

(define-public crate-ptb-reader-0.8 (crate (name "ptb-reader") (vers "0.8.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)))) (hash "1cpmr8zzmf7pbj20alclgad8y4ca1n99zzzhw59dljqvl5ljzja3")))

(define-public crate-ptb-reader-0.9 (crate (name "ptb-reader") (vers "0.9.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ir1xns6hmd9qpfz9x3gcwrlbvqvdmfzy5mcnccvbpaikc3s1hbr")))

(define-public crate-ptb-reader-0.9 (crate (name "ptb-reader") (vers "0.9.1") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.1") (default-features #t) (kind 0)))) (hash "015sx5bcpbjs7294b6kxn2qnmy89np9jdvkfjlk845xj98y8jmzr")))

