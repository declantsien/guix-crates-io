(define-module (crates-io vo se) #:use-module (crates-io))

(define-public crate-vose-alias-1 (crate (name "vose-alias") (vers "1.0.0") (deps (list (crate-dep (name "float-cmp") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0i3yf8ah5q1ln46k3nnyvc8zllcr48jnz617wyjc60qpjl0qa179")))

(define-public crate-vosealias-0.2 (crate (name "vosealias") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0z8q7n43xwjf4j3szkydck19p9mkrikip9wyqhr75s2x5m4nkn5h")))

