(define-module (crates-io vo ig) #:use-module (crates-io))

(define-public crate-voight_kampff-0.1 (crate (name "voight_kampff") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1756milq0y1cna6sasr5a5l9jw8qs9886xn0fif5cp0np3pn2hnc")))

(define-public crate-voight_kampff-0.1 (crate (name "voight_kampff") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "05i2vmnmh3gf9xbrhl6qd9wyi9p31j6jha87kf7isnqqqny1jrps")))

(define-public crate-voight_kampff-0.1 (crate (name "voight_kampff") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1pjp82bkhz1gn32syxphqfaydhj2q69lnw8lbl1s98xis36z57hn")))

