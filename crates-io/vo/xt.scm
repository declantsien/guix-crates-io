(define-module (crates-io vo xt) #:use-module (crates-io))

(define-public crate-voxtree-0.1 (crate (name "voxtree") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1q09g4hf1511d2m0b2ppwzcqs176ig1shrjn0b5pva1czj0g3rsy") (features (quote (("no_std"))))))

(define-public crate-voxtree-0.1 (crate (name "voxtree") (vers "0.1.1") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0y92d3gkrrl7s82pg6vki5003wqnwmzvp83cmhk435hpagql0h8z") (features (quote (("no_std"))))))

(define-public crate-voxtree-0.1 (crate (name "voxtree") (vers "0.1.2") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (optional #t) (default-features #t) (kind 0)))) (hash "1yzcdwavn3yf9m2fd25hzfxp43fkp3l8jaqqy54l5idmrp7dcljj") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:cgmath"))))))

(define-public crate-voxtree-0.1 (crate (name "voxtree") (vers "0.1.3") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (optional #t) (default-features #t) (kind 0)))) (hash "09ixg84djkbl5apwm9kzr5jzvb4ddg18h5bqcs1r3qrsc9h42whq") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:cgmath"))))))

(define-public crate-voxtree-0.1 (crate (name "voxtree") (vers "0.1.4") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ans44af5mrvs9nmkmk0w4lsagx32g9scz1i0da3wczwwl338pq0") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:cgmath"))))))

(define-public crate-voxtree-0.1 (crate (name "voxtree") (vers "0.1.5") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (optional #t) (default-features #t) (kind 0)))) (hash "1cfs7wr2jqab5dkqpb2q69gskjf0h6253k8ybfp7jk51mpkmnp02") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:cgmath"))))))

