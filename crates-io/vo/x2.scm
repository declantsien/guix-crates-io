(define-module (crates-io vo x2) #:use-module (crates-io))

(define-public crate-vox2pmf-0.1 (crate (name "vox2pmf") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dot_vox") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "libpolymesh") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04vqdlgvkgw5wa4dhqb97qq1mxa6pn7851130bhnf7bdm8wzrjg6")))

