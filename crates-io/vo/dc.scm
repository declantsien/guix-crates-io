(define-module (crates-io vo dc) #:use-module (crates-io))

(define-public crate-vodca-0.1 (crate (name "vodca") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits" "derive" "proc-macro"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1a34bi5yjw5qpxr1n65jadfiz5lj6d5vh3kjay2zwrb5bs5v4wxj")))

