(define-module (crates-io vo lc) #:use-module (crates-io))

(define-public crate-volcano-0.0.1 (crate (name "volcano") (vers "0.0.1") (hash "1cikvmlmbp0rw2millad07rb3qypp4x70raxsn1lh053yllkk7wy")))

(define-public crate-volcasample-sys-0.1 (crate (name "volcasample-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.37") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "04dbl1cr40nvyh0ihb6rj5psvqag3y2malgjqi7axmbifgi54fly")))

(define-public crate-volcell-1 (crate (name "volcell") (vers "1.0.0") (hash "1g8p12dprbmv461fip7v98lkwqy6m31qqjdh8djacc4j1prphis0") (features (quote (("unstable") ("default"))))))

