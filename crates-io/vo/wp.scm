(define-module (crates-io vo wp) #:use-module (crates-io))

(define-public crate-vowpalwabbit-sys-8 (crate (name "vowpalwabbit-sys") (vers "8.6.1-beta") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "02q31xzcxrlxc0js4nbml0sid6ci6rwcgmxmx190h6jk2w7yfwl5") (links "libvw_c_wrapper")))

(define-public crate-vowpalwabbit-sys-8 (crate (name "vowpalwabbit-sys") (vers "8.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "0bs1p2nqxdrflyzzkdkb1yghlq5vkgib0mz152bklcn84n530pr5") (links "libvw_c_wrapper")))

(define-public crate-vowpalwabbit-sys-8 (crate (name "vowpalwabbit-sys") (vers "8.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0bwr4qgl3hpv6p212rvskcyav1936n2ivyclpydbv17jxr3p2dcq") (yanked #t) (links "libvw_c_wrapper")))

(define-public crate-vowpalwabbit-sys-8 (crate (name "vowpalwabbit-sys") (vers "8.8.0+post1") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0qp2iq48pjwv4gpc0zw7yc0rc39w5ldn77xprr9iphgf5sa868ip") (yanked #t) (links "libvw_c_wrapper")))

(define-public crate-vowpalwabbit-sys-8 (crate (name "vowpalwabbit-sys") (vers "8.8.0+post2") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "15dj639rj4rdpvvwmas7ny20bjflh9gnvrnd2hz0l4fvqxx0lvkq") (yanked #t) (links "libvw_c_wrapper")))

(define-public crate-vowpalwabbit-sys-8 (crate (name "vowpalwabbit-sys") (vers "8.8.0+post3") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "07yyp460ygpfm4bb1kdgfrsaz5fsr0iznqwck8lxfxh2jfhkdd47") (yanked #t) (links "libvw_c_wrapper")))

(define-public crate-vowpalwabbit-sys-8 (crate (name "vowpalwabbit-sys") (vers "8.8.1+vw-v8.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "16q118bkzgqnarxvd3pcza0cagjy7hh15ba0as3b3715cfp6ijry") (links "libvw_c_wrapper")))

