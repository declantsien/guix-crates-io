(define-module (crates-io vo li) #:use-module (crates-io))

(define-public crate-volition-0.0.1 (crate (name "volition") (vers "0.0.1") (deps (list (crate-dep (name "glium") (req "^0.17") (default-features #t) (kind 0)))) (hash "1w6cxhskib6q28kaam3l0nhikpvyqy7im9b3nb5r6cbxi61gzwa6")))

(define-public crate-volition-0.0.2 (crate (name "volition") (vers "0.0.2") (deps (list (crate-dep (name "winit") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1ld0mnbqhxcx1lfj4gmpk29j0sn1yiifgi3vnf6fyrb81m3rlk1q")))

(define-public crate-volition-0.0.3 (crate (name "volition") (vers "0.0.3") (deps (list (crate-dep (name "winit") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1kvfq6znwqvzfxr6p83zj5jiqqbnawsl9n638jni3aajr7mi23p8")))

(define-public crate-volition-0.0.4 (crate (name "volition") (vers "0.0.4") (deps (list (crate-dep (name "winit") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1w6xi5sl0ikibxjmybjnbqm8x3rd2rr6dc1r7qql0yi5kdjkhiwy")))

(define-public crate-volition-0.0.5 (crate (name "volition") (vers "0.0.5") (deps (list (crate-dep (name "winit") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1y1xxbjvci22179vq8lhsr7gzfh6yajnyp94g6zd4xadj75x53ih")))

(define-public crate-volition-0.0.6 (crate (name "volition") (vers "0.0.6") (deps (list (crate-dep (name "winit") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "1adm3shbypy154nir0j37gf75pr5358mmsf743mq02hqvv7kdar3") (yanked #t)))

(define-public crate-volition-0.0.7 (crate (name "volition") (vers "0.0.7") (deps (list (crate-dep (name "winit") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "1n7xyan1r464xwfids47w3059zipahq09amgkcgzlrhsl5ql8bnv")))

(define-public crate-volition-0.0.8 (crate (name "volition") (vers "0.0.8") (deps (list (crate-dep (name "winit") (req "^0.8") (default-features #t) (kind 0)))) (hash "0mdxdp2v7ksp8q4w8lsf16q0ppj3f3j7z6bjghwd14vxz7g7xa4b")))

(define-public crate-volition-0.0.9 (crate (name "volition") (vers "0.0.9") (deps (list (crate-dep (name "winit") (req "^0.8") (default-features #t) (kind 0)))) (hash "0nn2q213fkd1lnra1bq6806vrxmf3pny5h1rl0r85pzpn4xspa5j")))

(define-public crate-volition-0.0.10 (crate (name "volition") (vers "0.0.10") (deps (list (crate-dep (name "winit") (req "^0.8") (default-features #t) (kind 0)))) (hash "1saxsw3ly87mvaqzghyjdznzazmdprfpr8ca9k7742w193hfp9i6")))

(define-public crate-volition-0.0.11 (crate (name "volition") (vers "0.0.11") (deps (list (crate-dep (name "winit") (req "^0.8") (default-features #t) (kind 0)))) (hash "0dqljwg7jplam4cc1x6si0wmnlr5gcvrvlj5830jvy5b9kd1ik9p")))

(define-public crate-volition-0.0.12 (crate (name "volition") (vers "0.0.12") (deps (list (crate-dep (name "winit") (req "^0.9") (default-features #t) (kind 0)))) (hash "095q89gbcbf3s4d758m00v103asviyqq2xik2m0n9qwsmw6fsr58")))

(define-public crate-volition-0.0.13 (crate (name "volition") (vers "0.0.13") (deps (list (crate-dep (name "winit") (req "^0.9") (default-features #t) (kind 0)))) (hash "1a9dikf8diq2qr4063nj7jfjjfcb4x2cjaxrayy7iqlb7msmisg5")))

(define-public crate-volition-0.0.14 (crate (name "volition") (vers "0.0.14") (deps (list (crate-dep (name "winit") (req "^0.9") (default-features #t) (kind 0)))) (hash "1gmiq5cr5l0q7gx2mzkzkgk6x0w9dsisinkilrhy4a62ipcwzw4f")))

(define-public crate-volition-0.0.15 (crate (name "volition") (vers "0.0.15") (deps (list (crate-dep (name "winit") (req "^0.10") (default-features #t) (kind 0)))) (hash "1vg88lml0q49ym28z56i34m00pmcnr0szwrj8av3qaqlbwy788x9")))

(define-public crate-volition-0.0.16 (crate (name "volition") (vers "0.0.16") (deps (list (crate-dep (name "winit") (req "^0.12") (default-features #t) (kind 0)))) (hash "155w70s7lkcxlzx3b55d97wq59gh3qfp5czwr7lz6p27klpahwcj")))

(define-public crate-volition-0.0.17 (crate (name "volition") (vers "0.0.17") (deps (list (crate-dep (name "winit") (req "^0.16") (default-features #t) (kind 0)))) (hash "0wk7nws8ik4h2vsmdzgy2zd05x9h7jmbd6sc9jss5cnjkdsv8xva")))

(define-public crate-volition-0.0.18 (crate (name "volition") (vers "0.0.18") (deps (list (crate-dep (name "winit") (req "^0.18") (default-features #t) (kind 0)))) (hash "0r52hnd511fb3rp3azr658y4qkg3kbh67szhggz5f1wmqc8hyvgz")))

(define-public crate-volition-0.0.19 (crate (name "volition") (vers "0.0.19") (deps (list (crate-dep (name "winit") (req "^0.18") (default-features #t) (kind 0)))) (hash "017ri2701kc296w6x2g451m0g609lzkz9mivgxbahd8fk9gwyamj")))

(define-public crate-volition-0.1 (crate (name "volition") (vers "0.1.0") (deps (list (crate-dep (name "winit") (req "^0.21") (default-features #t) (kind 0)))) (hash "0v11dmkxblmzh3mphdh8c81lp8h7zwphxmv7xn2nbii85bawlc6p")))

(define-public crate-volition-0.1 (crate (name "volition") (vers "0.1.1") (deps (list (crate-dep (name "winit") (req "^0.21") (default-features #t) (kind 0)))) (hash "0p4ijgg3kcc7ffgp7g0ah851wkia7jivhshb7pbzjq7v8vf5gh1a")))

(define-public crate-volition-0.1 (crate (name "volition") (vers "0.1.2") (deps (list (crate-dep (name "winit") (req "^0.21") (default-features #t) (kind 0)))) (hash "1lh9z4nms6zjnxv03vjdpg3811c7v21394dh5f6bk61v6qlsb5rk")))

(define-public crate-volition-0.1 (crate (name "volition") (vers "0.1.3") (deps (list (crate-dep (name "winit") (req "^0.21") (default-features #t) (kind 0)))) (hash "1iy2bjgmxfp0yz8pz7r00if5qbhh1ms2850cy2cmk19lr8nf3adj")))

(define-public crate-volition-0.1 (crate (name "volition") (vers "0.1.4") (deps (list (crate-dep (name "winit") (req "^0.21") (default-features #t) (kind 0)))) (hash "1fi48awr4zb65n8kz1m1hpd2b8sr74jrv9ijkl6p3xi0417dnxc1")))

