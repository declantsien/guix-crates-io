(define-module (crates-io vo ja) #:use-module (crates-io))

(define-public crate-vojaq_parser-0.1 (crate (name "vojaq_parser") (vers "0.1.0") (hash "0i11zad1vydb1nwm6zrmi91v4g8vzfznapclszd36z01ffb4rpzs")))

(define-public crate-vojaq_parser-0.1 (crate (name "vojaq_parser") (vers "0.1.1") (hash "0h06lm0c7c9sxyd3d8yjsj89y0lsnysqpmwh84ihsxz45bg7wwyn")))

(define-public crate-vojaq_parser-0.1 (crate (name "vojaq_parser") (vers "0.1.2") (hash "1qr16k4ps6a7484wk7n8sm9mpbjndwfvihjm6jsjxjjy5cldw46r")))

(define-public crate-vojaq_parser-0.2 (crate (name "vojaq_parser") (vers "0.2.0") (hash "1z24l1h9vhv85wkg92yqly7whhmw3v16xqzwz2pxp0ana4fkbq20")))

(define-public crate-vojaq_parser-0.2 (crate (name "vojaq_parser") (vers "0.2.1") (hash "04vp9h2r2wskjac2lgp0cj55ahbaalgpazisa94g098g4b4brbv6")))

(define-public crate-vojaq_parser-0.3 (crate (name "vojaq_parser") (vers "0.3.0") (hash "1iqvl836lgsc4cjr9fxmhpjcr4551crf1s59bw9xln3iz5a59ldi")))

(define-public crate-vojaq_parser-0.4 (crate (name "vojaq_parser") (vers "0.4.0") (hash "18wlfmi649b9c9jq75fa2ykp7h8wsrknxwf362x9v7zfgvzif6q1")))

(define-public crate-vojaq_parser-0.5 (crate (name "vojaq_parser") (vers "0.5.0") (hash "0c1640y3y0i324sdf0g50xnnr7v4scrpm0wvzmfk6k4zc8k2a850")))

(define-public crate-vojaq_parser-0.5 (crate (name "vojaq_parser") (vers "0.5.1") (hash "0xyj37ngikkh1hn01kqix4iyyb1wb4d3j9czgqdgkdpkklp82ybw")))

