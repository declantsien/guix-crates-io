(define-module (crates-io vo lk) #:use-module (crates-io))

(define-public crate-volkswagen-0.1 (crate (name "volkswagen") (vers "0.1.0") (deps (list (crate-dep (name "built") (req "^0.3") (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1n0ha74ss9d78fsk500p8jdavxs35izlf91k2aasx13w2pgf1asq")))

