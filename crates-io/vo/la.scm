(define-module (crates-io vo la) #:use-module (crates-io))

(define-public crate-voladdress-0.2 (crate (name "voladdress") (vers "0.2.0") (deps (list (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "0j13s749ilbkca050m4xwbl7kq3yqc14pmf2pxnfq9nrj7n8zf94")))

(define-public crate-voladdress-0.2 (crate (name "voladdress") (vers "0.2.1") (deps (list (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "0mk1l2a6jg9ac0rm7msidgd1y1qlm6nv3p3j1dlzk50njk4nxirk")))

(define-public crate-voladdress-0.2 (crate (name "voladdress") (vers "0.2.2") (deps (list (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "1mw39warm086p80j4q0ibl6s3lvrkhcvkkxkjw8897rxb9m02n6j")))

(define-public crate-voladdress-0.2 (crate (name "voladdress") (vers "0.2.3") (deps (list (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "1wn4mnkdvmvhhv78k0m1fp4y7z85lm1l5glz7kpwcaklgczknvil")))

(define-public crate-voladdress-0.2 (crate (name "voladdress") (vers "0.2.4") (deps (list (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "1psbgashvr4hbmdai1224vra3j78fnic97b3l8jb5j8vvmiwb0m8")))

(define-public crate-voladdress-0.2 (crate (name "voladdress") (vers "0.2.5") (deps (list (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "0381z323ziivdzymzkihsbayyrqiyh6kxcr8crwp67552jmqa0i4")))

(define-public crate-voladdress-0.3 (crate (name "voladdress") (vers "0.3.0") (hash "0hri3dc0j7wmg6f23xwxs28l4h10c9kack3p4ls7nn89765hlyci")))

(define-public crate-voladdress-0.4 (crate (name "voladdress") (vers "0.4.0") (hash "14fhrlszn5gyj42dakx793k5vkgif0cw4x6vwiah2f9yj5cj1vd9") (yanked #t)))

(define-public crate-voladdress-0.4 (crate (name "voladdress") (vers "0.4.1") (hash "13218fy52lp2klzslsg0y67n8wi9b9932c2yn9cb25jl7x2q0cnj") (yanked #t)))

(define-public crate-voladdress-0.4 (crate (name "voladdress") (vers "0.4.2") (hash "0fdas9j3qmamd08dz5fyckw5ymjjpm0iijd4b55539gs34l9my19") (yanked #t)))

(define-public crate-voladdress-0.4 (crate (name "voladdress") (vers "0.4.3") (hash "1bjvg0p9fp16krx27ib9wy16giy24107j3fwwfb9gkgnighk4h5i") (yanked #t)))

(define-public crate-voladdress-0.4 (crate (name "voladdress") (vers "0.4.4") (hash "15czmrji6lhiss8rzvhcxp5qb29pmnjq6nicbcyzwd5smbif5g9h") (yanked #t)))

(define-public crate-voladdress-0.4 (crate (name "voladdress") (vers "0.4.5") (hash "1bi70p9ydbp2fzfcii752d402an90b47ljxac756fnzs2nrh072y") (yanked #t)))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.0.0") (hash "02zsnci428lfrzwgq59dqxr5jlhckjn4is8zprg4mmnd5wwl0g8z") (yanked #t)))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.0.1") (hash "0rn9pzx4kfbwgv1v5b579c5f1q3f4a4r5p9xj4b9mi1d9nmiq1db") (features (quote (("experimental_volregion") ("default" "experimental_volregion")))) (yanked #t)))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.0.2") (hash "02470s6lwjyrms384pcnfpl690vwplv7qf7dz8412b7gkx0x0mr1") (features (quote (("experimental_volregion")))) (yanked #t)))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.1.0") (hash "12md01az49lffg52dc6wrdblgzawf8v8wa7q8l379yhxdjq82q0i") (features (quote (("experimental_volregion") ("experimental_volmatrix")))) (yanked #t)))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.2.0") (hash "0a49sk39lzhj4fxazm6c549jpc69l4k9qc1bdsr579nd02c5xlc0") (features (quote (("experimental_volregion") ("experimental_volmatrix")))) (yanked #t)))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.2.1") (hash "1m7sbycz7z6inv167l61y0cpbm7dsr37jwginqf608ng5j164900") (features (quote (("experimental_volregion") ("experimental_volmatrix")))) (yanked #t)))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.2.2") (hash "113phshklrfwsvrf83wn6gjwdw94v5j7xhnib4nwcigm0dxvqvsp") (features (quote (("experimental_volregion") ("experimental_volmatrix"))))))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.2.3") (hash "0aryfabv1wnm45nlhj46fgwzzw7lzc9h6hjyn71xgfwbm9bdpnfw") (features (quote (("experimental_volregion") ("experimental_volmatrix"))))))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.3.0") (hash "1k2y1ffhk7f250zjxmc3c7ws3iv6g78bsdh96pwd75f9ggpkzgvz")))

(define-public crate-voladdress-1 (crate (name "voladdress") (vers "1.4.0") (hash "113hn9h40y2bm369zv0p87dikc1gs525sh72j8cm85z7qykskx3m")))

(define-public crate-volaris-0.0.0 (crate (name "volaris") (vers "0.0.0") (hash "1rbb5s25jbr7ka2yxzvsxlkyfknb45gcnax3h8mj3v9f63yqwys5")))

(define-public crate-volaris-core-0.0.0 (crate (name "volaris-core") (vers "0.0.0") (hash "1i68a424ysvlcaqcf9023cf32v2wk03r2sw8h9xj4msab2dp58g2")))

(define-public crate-volatile-0.1 (crate (name "volatile") (vers "0.1.0") (hash "158abvy2chj6x4vflzk0qmc3xklq9ryp3i0fq654zfj01xndgi9p")))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.0") (hash "1msds03wya7kdp95097kbg3zr6axn4v3i36dkaiz23xvawsh4v87")))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.1") (hash "14ngacf4adwjci14840f5nsvmpbaigfgyfkmrfj5sn7j21hjikm7")))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.2") (hash "170g06kghxnqdg2ai64hm6siva2qm9zyn35iv8f48kw1sxc15m0s")))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.3") (hash "1h5h6xjvxia0nq6bmw02yx3r85zqx5f2cwl2nj98yn8n1wjwacv6") (features (quote (("const_fn"))))))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.4") (hash "1s1zia2z9l67x2m2fq2gb7lbgrsffm85kyblm1253mpj5lx39m2l") (features (quote (("const_fn"))))))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.5") (hash "002xq7gwz0jfa7m0ka4jf8jhkprnq50fp2rgbjflg3knalf3kjnr") (features (quote (("const_fn"))))))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.6") (hash "0abzpkm4mh2kppvwxbr1y20capnng16j8n8iqlgw7b7snksyvw3a") (features (quote (("const_fn"))))))

(define-public crate-volatile-0.2 (crate (name "volatile") (vers "0.2.7") (hash "0i8rlhllyd70n07q4hb3szzflfdlpp6lgmb96mqz3zh6xp9nmc7n") (features (quote (("const_fn"))))))

(define-public crate-volatile-0.3 (crate (name "volatile") (vers "0.3.0") (hash "1rk0mviczy8n4hjl2f1qw088qqwh44ixmpyjcvhrcgzh12p6zrzq") (features (quote (("const_fn"))))))

(define-public crate-volatile-0.4 (crate (name "volatile") (vers "0.4.0") (hash "0akpn3vjzzaqzk4pq5myn78pamvnga44hczz7s9hhs8lv5nxyygs") (features (quote (("unstable"))))))

(define-public crate-volatile-0.4 (crate (name "volatile") (vers "0.4.1") (hash "1j1lncxa03wyjw4zc62331qgr76kivqvsdx62xjc94n7li4sj070") (features (quote (("unstable"))))))

(define-public crate-volatile-0.4 (crate (name "volatile") (vers "0.4.2") (hash "13x0ry290q7x4yf5va72fcxvcgiidx62zn6iv6lw17r71xxd8lwb") (features (quote (("unstable"))))))

(define-public crate-volatile-0.4 (crate (name "volatile") (vers "0.4.3") (hash "042fizq8zykq084xxvfc6dgz3ra7g1pz4v1m6j7y4lw3p8ww97ca") (features (quote (("unstable"))))))

(define-public crate-volatile-0.4 (crate (name "volatile") (vers "0.4.4") (hash "1w34yiy99a7cl6n51y0dk5ci030g6xz20vky6mrkkddq9vadphp4") (features (quote (("unstable"))))))

(define-public crate-volatile-0.4 (crate (name "volatile") (vers "0.4.5") (hash "033cdyvs12j7jph3nv8yw3xs9mmlgw6djkz0fjp612nskls9ijp3") (features (quote (("unstable"))))))

(define-public crate-volatile-0.4 (crate (name "volatile") (vers "0.4.6") (hash "14x7bvfjgk1xnl07n5f9i433vrw7gix08b8rdhs9p0rc7z38fa24") (features (quote (("unstable"))))))

(define-public crate-volatile-0.5 (crate (name "volatile") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "07n3gw4rlrlqnml997a6s7yacx4h8bkk2j3hprb0hdaqbzh7dv4m") (features (quote (("very_unstable" "unstable") ("unstable"))))))

(define-public crate-volatile-0.5 (crate (name "volatile") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0pnjd4cscrb7agbk34fjwq1xkb6cb1gb289ysk6h1bmpv7wwkaly") (features (quote (("very_unstable" "unstable") ("unstable"))))))

(define-public crate-volatile-0.5 (crate (name "volatile") (vers "0.5.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0dnrf5y4qq4h54wmdb07alg8sakka65jcmpjs94n18ds1i4fc55j") (features (quote (("very_unstable" "unstable") ("unstable"))))))

(define-public crate-volatile-0.5 (crate (name "volatile") (vers "0.5.3-rc.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "volatile-macro") (req "=0.5.3-rc.1") (optional #t) (default-features #t) (kind 0)))) (hash "0chj6296mma87zs1il3qsppry9whsy74w1kv13dygv1n8m1g5jj5") (features (quote (("very_unstable" "unstable") ("unstable")))) (v 2) (features2 (quote (("derive" "dep:volatile-macro"))))))

(define-public crate-volatile-0.5 (crate (name "volatile") (vers "0.5.3-rc.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "volatile-macro") (req "=0.5.3-rc.2") (optional #t) (default-features #t) (kind 0)))) (hash "0vbbqx0fixf8a0xvdhiz10ycllp0wn02lm3ici6pmilnq0mvjwal") (features (quote (("very_unstable" "unstable") ("unstable")))) (v 2) (features2 (quote (("derive" "dep:volatile-macro"))))))

(define-public crate-volatile-0.5 (crate (name "volatile") (vers "0.5.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "volatile-macro") (req "=0.5.3") (optional #t) (default-features #t) (kind 0)))) (hash "13n884k00l771vdchpnh3v6b90jf7n9lzzwcx591vhm7pjagc5gg") (features (quote (("very_unstable" "unstable") ("unstable")))) (v 2) (features2 (quote (("derive" "dep:volatile-macro"))))))

(define-public crate-volatile-0.5 (crate (name "volatile") (vers "0.5.4") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "volatile-macro") (req "=0.5.4") (optional #t) (default-features #t) (kind 0)))) (hash "0xryc4klhzfaca5nyng6awfray4jf9dcrzr0v50fzi2qs8vm6nph") (features (quote (("very_unstable" "unstable") ("unstable")))) (v 2) (features2 (quote (("derive" "dep:volatile-macro"))))))

(define-public crate-volatile-macro-0.5 (crate (name "volatile-macro") (vers "0.5.3-rc.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pl44ijfw3akzfihn0h0vqqkgb1siv361w53ydbhkm6mp05rm8j1")))

(define-public crate-volatile-macro-0.5 (crate (name "volatile-macro") (vers "0.5.3-rc.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00vps1r7yg3mn87faixzks0zxl3vazgc67jdqgdj598r35cwqvbk")))

(define-public crate-volatile-macro-0.5 (crate (name "volatile-macro") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06lqv0iq2gwk50f8asbyd0ps8zl2hrpznhqh2za1qr48yz16wbkx")))

(define-public crate-volatile-macro-0.5 (crate (name "volatile-macro") (vers "0.5.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i8qr3qwamz28bkq4fivd2drbfqhjmrsw7xp5ap80snpx0w3ql60")))

(define-public crate-volatile-mem-0.1 (crate (name "volatile-mem") (vers "0.1.0") (hash "0rm6w3hi04hq4jkig5v4brnymv2b999qmh6msnpcvmvvfgzbc818")))

(define-public crate-volatile-ptr-0.1 (crate (name "volatile-ptr") (vers "0.1.0") (hash "1sh7iyvra0cmmpad3ixjnffg794sffbpmrjkg8wi3b9ffzcd3g09")))

(define-public crate-volatile-ptr-0.1 (crate (name "volatile-ptr") (vers "0.1.1") (hash "08diq90aszdzsm1acknr61w5r4677gzlmyw7pr6xgmqnpid8209i")))

(define-public crate-volatile-register-0.1 (crate (name "volatile-register") (vers "0.1.0") (hash "1sdjkgz8s7wn4052ach5x0v13363906if0m5qb5pa84bfh8xp1a3")))

(define-public crate-volatile-register-0.1 (crate (name "volatile-register") (vers "0.1.1") (hash "0fq91xxai6v34mqmv3w4kd9pjjndip8miaf0fhl81cqsghmhfkza")))

(define-public crate-volatile-register-0.1 (crate (name "volatile-register") (vers "0.1.2") (hash "0jfm9wc2v4qsv32n9zp51snfhqsfh86djg6l7f4svlzjm2d8hw54")))

(define-public crate-volatile-register-0.2 (crate (name "volatile-register") (vers "0.2.0") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11ij0qrab4b1bc78bjm6k2s0hwgr9y22igfnn46r96yr2r3cnrqd")))

(define-public crate-volatile-register-0.2 (crate (name "volatile-register") (vers "0.2.1") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1dh8x7z1ywjzyziz3jyjj39imp37s05c46whf2pkyablkngz3s4y")))

(define-public crate-volatile-register-0.2 (crate (name "volatile-register") (vers "0.2.2") (deps (list (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1k0rkm81qyhn4r8f03z0sch2kyikkgjjfalpaami9c08c8m7whyy")))

(define-public crate-volatile_cell-0.0.1 (crate (name "volatile_cell") (vers "0.0.1") (deps (list (crate-dep (name "expectest") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-libcore") (req "^0.0.1") (default-features #t) (target "thumbv6m-none-eabi") (kind 0)) (crate-dep (name "rust-libcore") (req "^0.0.1") (default-features #t) (target "thumbv7em-none-eabi") (kind 0)) (crate-dep (name "rust-libcore") (req "^0.0.1") (default-features #t) (target "thumbv7m-none-eabi") (kind 0)))) (hash "1jlfd8glgz5m2jka4fgp3n1rq5phr55qmays9lx2cs7vj4bmcajv") (features (quote (("replayer" "expectest"))))))

(define-public crate-volatile_cell-1 (crate (name "volatile_cell") (vers "1.0.0") (deps (list (crate-dep (name "expectest") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-libcore") (req "> 0.0.1") (default-features #t) (target "thumbv6m-none-eabi") (kind 0)) (crate-dep (name "rust-libcore") (req "> 0.0.1") (default-features #t) (target "thumbv7em-none-eabi") (kind 0)) (crate-dep (name "rust-libcore") (req "> 0.0.1") (default-features #t) (target "thumbv7m-none-eabi") (kind 0)))) (hash "0wms3c3s08bkwvzz9zclja90chwcqjjs6lnbzk2gczn4dk2l9778") (features (quote (("replayer" "expectest"))))))

(define-public crate-volatile_unique_id-0.1 (crate (name "volatile_unique_id") (vers "0.1.0") (hash "136dk2v1nkq9yic6618x0h3mfqpsqjq1kr2awi4rdv0fm839zgkf")))

(define-public crate-volatile_unique_id-0.1 (crate (name "volatile_unique_id") (vers "0.1.1") (hash "1wqbbdh1rv0lgcj8s1yrnmb3rrnbpdm7hq22kiwk36vdmkl66wny")))

