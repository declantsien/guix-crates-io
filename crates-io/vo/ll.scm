(define-module (crates-io vo ll) #:use-module (crates-io))

(define-public crate-vollerei-0.1 (crate (name "vollerei") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "0wb4mav5mdg6nhcf487dirncqlxysalay35c05zq7z0jsldbgjbf")))

(define-public crate-vollerei-0.1 (crate (name "vollerei") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "0dk890nj20bjhgp56n0vixf5i1bzj6hmf73a8wdyh542p10xswbx") (yanked #t)))

(define-public crate-vollerei-0.1 (crate (name "vollerei") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "0xcifnmpsv3dg2jcz5agxdy7z0r0rgv43xp6wrhsak4vlxspppjc") (yanked #t)))

(define-public crate-vollerei-0.1 (crate (name "vollerei") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "1ar4nfpdj5sfnw3kif2phsh7whvswp5pbcqxky8bwsgf8jgysi59") (yanked #t)))

(define-public crate-vollerei-0.1 (crate (name "vollerei") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "1y94ly2fk101yb5bkib83dabcvs5qk49mhg383n4wfcwabp85lgc")))

(define-public crate-vollerei-0.1 (crate (name "vollerei") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "0r2f6wk831x8z3wymqd990zfi77lzblsr9zqksgpafp8nj2w7j4k")))

(define-public crate-vollerei-0.1 (crate (name "vollerei") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "01mj8l6w3avjqzikr4dyjippwi45sq36iagm78mcwvdffck05zfm")))

