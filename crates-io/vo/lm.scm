(define-module (crates-io vo lm) #:use-module (crates-io))

(define-public crate-volmark-0.1 (crate (name "volmark") (vers "0.1.0") (deps (list (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_trace" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0zk1qgrw913h1whr64mnqh5dp3rz54kismsghjcdy5nb10bwcay2")))

(define-public crate-volmem-0.1 (crate (name "volmem") (vers "0.1.1") (hash "028wsm8y615i8nz05h1snb51w1bwbk1yn2j55pimh81vrc0c4jn9") (features (quote (("unstable"))))))

