(define-module (crates-io vo dk) #:use-module (crates-io))

(define-public crate-vodk_data-0.0.1 (crate (name "vodk_data") (vers "0.0.1") (hash "16plkawj5x412z17wlrfljw33qrbaa5w5pcqwv7g1c9zvcdvak07")))

(define-public crate-vodk_data-0.0.2 (crate (name "vodk_data") (vers "0.0.2") (hash "14gyfspjlb8zaxml5z4ik2clv48lxk31mmq06rs2kxzrb7c0azbx")))

(define-public crate-vodk_math-0.0.1 (crate (name "vodk_math") (vers "0.0.1") (hash "05wqfvbay8q40i52gmdmzhb3kzmg1nvj24vakdhh0l704vwhznv4")))

(define-public crate-vodk_math-0.0.2 (crate (name "vodk_math") (vers "0.0.2") (hash "02nwfdzj77fc4alw8b0l5ypr7rl60qkbi3y2s52r3hz69jxp7p3j")))

(define-public crate-vodk_math-0.0.3 (crate (name "vodk_math") (vers "0.0.3") (hash "1c655r1mp3iwygw8yfzyig6f302a735rzsbpaqyl1wdn8rc0m17j")))

(define-public crate-vodka-0.1 (crate (name "vodka") (vers "0.1.0") (hash "0cp2z0spq1p902pcdi4hxvh17vr8bc45gkxx5q8n534qzsxbawvy")))

