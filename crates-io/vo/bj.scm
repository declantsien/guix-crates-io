(define-module (crates-io vo bj) #:use-module (crates-io))

(define-public crate-vobject-0.1 (crate (name "vobject") (vers "0.1.0") (hash "1l96rspnkkbf2bhy07iy34mwx30cqndbx0dqc0wvkk1w8f6y1zis")))

(define-public crate-vobject-0.1 (crate (name "vobject") (vers "0.1.1") (hash "0j950673ffc1y9pi0bdbzb6v8absanp4i5j4czhl158vhlqgz7kr")))

(define-public crate-vobject-0.2 (crate (name "vobject") (vers "0.2.0") (hash "1cvlnp552mpplp5d9vablwq2mdpvv78sg76i57y9jnj90wkxfwrb")))

(define-public crate-vobject-0.2 (crate (name "vobject") (vers "0.2.1") (hash "1n7f0731cnpy5a5wlwayhb6mq4ymb30lmjqk6cgmjvv6xn9ybj6q")))

(define-public crate-vobject-0.3 (crate (name "vobject") (vers "0.3.0") (hash "0zdadrm2c4zch8bqx35bd9jljd82lq6m8x7a0ysad2iw67w0gw1b")))

(define-public crate-vobject-0.4 (crate (name "vobject") (vers "0.4.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "0h91z5nj3lx3da0fvwhgcp0zvrk12cm0li98xcajkjq1fbp67rww")))

(define-public crate-vobject-0.4 (crate (name "vobject") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "1b5ywqbi5cnkhjc150yy42zjdkrh6n26lhwd9ffaga7rs806jfk1") (features (quote (("timeconversions" "chrono") ("default"))))))

(define-public crate-vobject-0.4 (crate (name "vobject") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "1r4s5srvigvnyaj1ksifp09m2fwghp57a6dlpvx70qh3j5b9jhb0") (features (quote (("timeconversions" "chrono") ("default"))))))

(define-public crate-vobject-0.5 (crate (name "vobject") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "0bvmlhqj2fac7m8680zd6qpbmxasabvhzkzh9bbzccyfdh9bc301") (features (quote (("timeconversions" "chrono") ("default"))))))

(define-public crate-vobject-0.5 (crate (name "vobject") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "1lv70cpxwvsilk2w74bmis5f22488ddnbks2jrplbzhwwgxpkm29") (features (quote (("timeconversions" "chrono") ("default"))))))

(define-public crate-vobject-0.6 (crate (name "vobject") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)))) (hash "1m3qxnrqbz4kx9lx93njz7c84izl55iz14bmk7xgrjh4fi1x2yij") (features (quote (("timeconversions" "chrono") ("default"))))))

(define-public crate-vobject-0.7 (crate (name "vobject") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yb7zv093w7z121ww2jy9g8i1yv1x8svr997gh7yba493ipf2bqv") (features (quote (("timeconversions" "chrono") ("default"))))))

