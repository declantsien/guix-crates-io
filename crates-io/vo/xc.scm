(define-module (crates-io vo xc) #:use-module (crates-io))

(define-public crate-voxcov-0.1 (crate (name "voxcov") (vers "0.1.0") (hash "0bmq9l8h3ajr5xndp64h5ky1cy3kj87ylwc977vyqrk8ls4iymp2")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1fc7rr9pk5kdb35snhkr6rxs8qsd218djs89q7s1s9w3w34j3pfw")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "16na9pj99kskmj8ckqs6jwxda0x51z645lhbfdik3ln1xhc01n04")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.2") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0hm58h6ahm87jr624jxf90kslc5zm4236qn2zqjm6pihvlz069r5")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.3") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1ajvz544qqvpzi0harnhlbgnri13lk0ccymngqh51majd5cvj3ks")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.4") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0abcis9lf7brywj2mfa3rjkb7bh568k5jfc12x94f0nmxjakfzqn")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.5") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "0d3vsmj608psivvdk0khmgxlcgj214sbzq3932z38y5brsj6s6a7")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.6") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "05lipswbzh5c824pdpi4ijrq0y8gqaqmfq5jfdl4wmx62wqwsm3w")))

(define-public crate-voxcov-0.2 (crate (name "voxcov") (vers "0.2.8") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "0ycwn1z871pj12lfbrwzyaydvl9p2wvk9kx91rlnvsnj7jkifrhn")))

