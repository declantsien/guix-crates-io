(define-module (crates-io vo x_) #:use-module (crates-io))

(define-public crate-vox_box-0.1 (crate (name "vox_box") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sample") (req "^0.2") (default-features #t) (kind 0)))) (hash "019b3wb6x5687rvl7i8fs35rxyb95za2pzh46hw632hn8fz2q0a1")))

(define-public crate-vox_box-0.2 (crate (name "vox_box") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sample") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "08zk0906n61qli3lgj9vk96zxp5gr4vm6r7q2h2xy4ggpx435mhz")))

(define-public crate-vox_box-0.2 (crate (name "vox_box") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sample") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "15gh9zglll194bwnawv907m8zmb5b6dimf0n5wai2y6wlp9h6qs1") (features (quote (("nightly"))))))

(define-public crate-vox_box-0.2 (crate (name "vox_box") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sample") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0w12i64khwnjbsrbxyrh5y9cg09bh4b26rg6kx8k11hmia5647wg") (features (quote (("nightly"))))))

(define-public crate-vox_box-0.2 (crate (name "vox_box") (vers "0.2.3") (deps (list (crate-dep (name "hound") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sample") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0lk3w1px6s2m8xgriilay9wajca1n4gp9xciigxckwkqrgiwkpl7") (features (quote (("nightly"))))))

(define-public crate-vox_box-0.2 (crate (name "vox_box") (vers "0.2.4") (deps (list (crate-dep (name "hound") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sample") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0plpsw3g89ggyycqm1qkvkdv3pqqkhd6q20mc6ymk4x6yzb8155h") (features (quote (("nightly"))))))

(define-public crate-vox_box-0.3 (crate (name "vox_box") (vers "0.3.0") (deps (list (crate-dep (name "hound") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sample") (req "^0.9") (default-features #t) (kind 0)))) (hash "1y1s212xjywwb1xhsnlcp9acx8z32p912qczvgcb8nqjn258wg0h") (features (quote (("nightly"))))))

(define-public crate-vox_geometry_rust-0.1 (crate (name "vox_geometry_rust") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1c81ap7zj4m4i40k7rx2r9qzwsprvv4v14d1lcx3dkcfprlx61nx")))

(define-public crate-vox_geometry_rust-0.1 (crate (name "vox_geometry_rust") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "tobj") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "15s9zli1kzyjmln9md4ymlgf7fmlv2jgjk6kcg3vdwbhprxs6zzy")))

(define-public crate-vox_geometry_rust-0.1 (crate (name "vox_geometry_rust") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "tobj") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "00nbibxwbdyygyp2vs6pgv0sl88iqjgpp459xdr4p6mcj6jpv5dg")))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.0") (hash "0ngv6lqqmiskk6v272ca0m3pxpmps09nfda8q8831k15x5hqpkh7") (yanked #t)))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.1") (hash "1gxkfl1d3c2vckp52jmlvn242lxsfcan4rvpy5b7r6i80k111l4r") (yanked #t)))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.2") (hash "1h2gklyrf8sw53zv3xjrvh75bkcq83hnmk5c9d0l40n6jfhcymfv") (yanked #t)))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.3") (hash "00s7q3nhl6a0yvm3lfkqg43jm162cnj3dgqqhi80n89iibba4mgr") (yanked #t)))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.4") (hash "19dkh5hnamnql1rs4rfbhnmcs25wrk827p31j2yzx4janad8npm5") (yanked #t)))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.5") (hash "0fppaf1adr1l2aahg1k3915bgkzpys4skg9qxg00zv7nhws0pwb1") (yanked #t)))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.6") (hash "1jm5ka8wkaab2zl89wzvg6xp7sr3b9wr6c3whzz6sjg3x37dfczk")))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.7") (hash "11hpzv55klzz91sw1wvhw9lnb6qlmdis9gg5f0sgrvmynscjghml") (yanked #t)))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.8") (hash "0pyqrs1w8rpp5vdnh1pxqlx68zp1v07qkm4g5qsyj4dhks3gd2zv")))

(define-public crate-vox_writer-0.1 (crate (name "vox_writer") (vers "0.1.9") (hash "10ccvxmw0d0rdy9g93yv1fqrmhvs3ji4gw8r0bfy1hv8q6ypfc4p")))

