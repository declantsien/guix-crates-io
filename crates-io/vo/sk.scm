(define-module (crates-io vo sk) #:use-module (crates-io))

(define-public crate-vosk-0.1 (crate (name "vosk") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "dasp") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vosk-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "00idnymlflzi5dq5dnhyb8wd4mw8nd0njgajqhmx0cnhp9q4p2sc")))

(define-public crate-vosk-0.2 (crate (name "vosk") (vers "0.2.0") (deps (list (crate-dep (name "cpal") (req "^0.14") (default-features #t) (kind 2)) (crate-dep (name "dasp") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vosk-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v37rm7xskq8fgbzrzzfch6ifsw98034d6wj8vm6mxwdhvfvh91y")))

(define-public crate-vosk-sys-0.1 (crate (name "vosk-sys") (vers "0.1.0") (hash "10vybx0pxlgk4q6xvwh2a5sjfkzw95sllbrxanrrx9yp2krwrqna")))

(define-public crate-vosk-sys-0.1 (crate (name "vosk-sys") (vers "0.1.1") (hash "0acx17sy9r6qy7sv8jngzpy32cf9i0d520bc3kbq550hra9sy6a1")))

