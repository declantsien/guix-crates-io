(define-module (crates-io vo m_) #:use-module (crates-io))

(define-public crate-vom_rs-0.1 (crate (name "vom_rs") (vers "0.1.0") (deps (list (crate-dep (name "decorum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0lc0hmrjjskd8bzdacc4c9q0a9v45ypzakl11sfg7yy5wzcha2sc")))

(define-public crate-vom_rs-0.1 (crate (name "vom_rs") (vers "0.1.1") (deps (list (crate-dep (name "decorum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1g3v9xafplmv25l4bcm7bwkr8v7j2v7q2fh36173p1hi52adlk76")))

(define-public crate-vom_rs-0.2 (crate (name "vom_rs") (vers "0.2.0") (deps (list (crate-dep (name "decorum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "05lx6qpif42v426xij9m3i1ljxkn2vf4b3zp356pxk94mdkn8433")))

(define-public crate-vom_rs-0.3 (crate (name "vom_rs") (vers "0.3.0") (deps (list (crate-dep (name "decorum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "06h1l05g5zn7xvd9jcab374gdwgddl81ps16mrdq4azsxvpvp8hn")))

(define-public crate-vom_rs-0.4 (crate (name "vom_rs") (vers "0.4.0") (deps (list (crate-dep (name "decorum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "094cc5n7ijkracm9s68niax5yg4rp5x2axjnlw90357b27zzyn55")))

(define-public crate-vom_rs-0.5 (crate (name "vom_rs") (vers "0.5.0") (deps (list (crate-dep (name "decorum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "158pkq50il9hcmv97dd5syms004b7zp041zl0i2hbd6kiw5my8as")))

(define-public crate-vom_rs-0.5 (crate (name "vom_rs") (vers "0.5.1") (deps (list (crate-dep (name "decorum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1bvi52laqi0cyz7bz1s4h8601gb4bwcdmfxrajqb4f6v3m63kkla")))

