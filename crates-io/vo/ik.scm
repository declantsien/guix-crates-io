(define-module (crates-io vo ik) #:use-module (crates-io))

(define-public crate-voikko-rs-1 (crate (name "voikko-rs") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2") (default-features #t) (kind 0)))) (hash "05w7h6wb6jij11c6qq7ybwbflwnlkmh63nap3m7r1c88bf4jx9qq")))

(define-public crate-voikko-rs-1 (crate (name "voikko-rs") (vers "1.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2") (default-features #t) (kind 0)))) (hash "0jbnclmg06454l8xsp18mhh1izfjl3wspvm97ifbd7xa1livnmbi")))

(define-public crate-voikko-rs-1 (crate (name "voikko-rs") (vers "1.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2") (default-features #t) (kind 0)))) (hash "0rslkxcf7rc9rq2npa1ayyzlw50ncsh4jj2dyilgzysjzx99mvpc")))

(define-public crate-voikko-rs-1 (crate (name "voikko-rs") (vers "1.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.6") (default-features #t) (kind 0)))) (hash "15bbi0483q6k3xp24rqhqd672v17vb2d23rcbp97nvs06kp2kjx3")))

(define-public crate-voikko-rs-1 (crate (name "voikko-rs") (vers "1.0.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7") (default-features #t) (kind 0)))) (hash "060rsws4ppy7g1kir09w26j40856r885fhlfwzn5jzx0d0yi4bc1")))

(define-public crate-voikko-rs-1 (crate (name "voikko-rs") (vers "1.0.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.8") (default-features #t) (kind 0)))) (hash "0xz26fsir87s5b8r1wbbmpfy0zp8r0kyk770fka3f4r4n1pkxbn6")))

(define-public crate-voikko-rs-1 (crate (name "voikko-rs") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.8") (default-features #t) (kind 0)))) (hash "0hd4ri8qp1acbx6cpqg0lw5idpgiapffiivfql4a2z3p89vv6rcg")))

