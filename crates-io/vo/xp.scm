(define-module (crates-io vo xp) #:use-module (crates-io))

(define-public crate-voxpop-0.1 (crate (name "voxpop") (vers "0.1.0") (hash "1b5g27sznn6sjpz30dhira0k1mc9k4sfmlkgv27xa6n750bvi2ff")))

(define-public crate-voxport-0.1 (crate (name "voxport") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "collada_io") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "dot_vox") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "stl_io") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0725f6iw0d0gf007npv9ra17symzzg40cdvgwhhclmdjxwxm26vh")))

