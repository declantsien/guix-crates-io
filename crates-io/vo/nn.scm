(define-module (crates-io vo nn) #:use-module (crates-io))

(define-public crate-vonneumann-1 (crate (name "vonneumann") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi" "winnt" "errhandlingapi" "sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1qmhpjg3iga989pad4ssnv8zxy3flvwp3synh7wqxgvzxwhwnnjf") (features (quote (("nightly"))))))

(define-public crate-vonneumann-1 (crate (name "vonneumann") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi" "winnt" "sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "02yi2j5z85171za94x6nxbd52fpdr3fnwcbp50xhvqdlwaz9zcic") (features (quote (("nightly"))))))

(define-public crate-vonneumann-1 (crate (name "vonneumann") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi" "winnt" "sysinfoapi"))) (target "cfg(windows)") (kind 0)))) (hash "1nylmayfssfpiczxpj091i668nzi7spsb7kfm1w39amzzd78d2vm") (features (quote (("nightly"))))))

