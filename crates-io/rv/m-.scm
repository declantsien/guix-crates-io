(define-module (crates-io rv m-) #:use-module (crates-io))

(define-public crate-rvm-lib-0.0.0 (crate (name "rvm-lib") (vers "0.0.0") (hash "1iqcc7zg06n4vagzpk23jmknfwn465v3ikk8jkjyrqzc9rh9iabm")))

(define-public crate-rvm-lib-0.0.1 (crate (name "rvm-lib") (vers "0.0.1") (hash "0sja2g649ckmcf1fky5z022w8fm8ipz49vxg8hva4qfhysanc8s9")))

