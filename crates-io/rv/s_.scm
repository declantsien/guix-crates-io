(define-module (crates-io rv s_) #:use-module (crates-io))

(define-public crate-rvs_derive-0.1 (crate (name "rvs_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03s6r78izbrk7kl47glzzcvgbwvr0p7ajv5fkn2slx9c00afg0dk")))

(define-public crate-rvs_derive-0.1 (crate (name "rvs_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1q3625jkbrc4jvyp6c7zm187hap5x1bdvgbjd8k9fbarg49i60j7")))

(define-public crate-rvs_derive-0.1 (crate (name "rvs_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17sbwa7ar37071c077qj0qag4kssj9m598lg1aj3j2lm7lwpq4xb")))

(define-public crate-rvs_derive-0.1 (crate (name "rvs_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kclxnmw94jdkv8ycrdilmfaz393r572h6i3v1q00pa8rp8amxcl")))

(define-public crate-rvs_derive-0.1 (crate (name "rvs_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07wr5svi63f7gfnzi9d3rlyj7m8y3jp05hplpm6dsj8dcflwhfp2")))

(define-public crate-rvs_derive-0.2 (crate (name "rvs_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gajhz1n1mbi3jg46bww777szfjnvqinx42fbg45c4xd02784jxg")))

(define-public crate-rvs_derive-0.2 (crate (name "rvs_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ha0pg90xnz8cs7d7bsc8smwlxb82jvcsf6yphbv64pqkqzmx28b")))

(define-public crate-rvs_derive-0.2 (crate (name "rvs_derive") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16v4v5m8q06z1sqy4d72msazppwx0rb86v5kryi7f8jgkgmvnz22")))

(define-public crate-rvs_derive-0.2 (crate (name "rvs_derive") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h4hna4fqh3qp1vam5y60brckzlrlv9i0jsakxaphxqamr4m1fm3")))

(define-public crate-rvs_derive-0.3 (crate (name "rvs_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rnb0wzi5jrbm1vp5729bmk2swd59fp5krgn78wmq75v3q7wvyky")))

(define-public crate-rvs_derive-0.3 (crate (name "rvs_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06929j17kfsjpp7g7b2fk4sw5229gnynxa66v73hwc84v9d91rz9") (yanked #t)))

(define-public crate-rvs_derive-0.3 (crate (name "rvs_derive") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19gjrqp2hn2q0p0n2680gmz0nqdg6fzcv3fvybag6m7bg0is27vf")))

