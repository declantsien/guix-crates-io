(define-module (crates-io rv c_) #:use-module (crates-io))

(define-public crate-rvc_core-0.1 (crate (name "rvc_core") (vers "0.1.0-alpha") (deps (list (crate-dep (name "rvc") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "0bpw9l600sc17vb1kbhf56z68l8ykn353j60cbp5k77zp1sg6sk3") (yanked #t)))

