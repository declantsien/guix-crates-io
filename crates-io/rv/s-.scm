(define-module (crates-io rv s-) #:use-module (crates-io))

(define-public crate-rvs-c-api-0.2 (crate (name "rvs-c-api") (vers "0.2.0") (deps (list (crate-dep (name "difference") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rvs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0yd0m5bpd3hnk75bdsjjp8sxfz93swr19bblhv3x6sp9glf5r2sq")))

(define-public crate-rvs-c-api-0.3 (crate (name "rvs-c-api") (vers "0.3.0") (deps (list (crate-dep (name "difference") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rvs") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0cd8zm821m6zk72agrgmw30g12nwhlz9wvhfq2izgzbkaz4qsyhb")))

(define-public crate-rvs-c-api-0.4 (crate (name "rvs-c-api") (vers "0.4.0") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rvs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0zvv1x16ia2bqlsxk10ccg4dabmykn1aqx9rywshwpzzkyzlpzjd")))

(define-public crate-rvs-c-api-0.4 (crate (name "rvs-c-api") (vers "0.4.1") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rvs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wh8xpk44cd5h7bys5s9bxrdg11iyk6jdxp390v6i2g9v92vzm9q")))

(define-public crate-rvs-c-api-0.5 (crate (name "rvs-c-api") (vers "0.5.0") (deps (list (crate-dep (name "difference") (req ">=2.0.0, <3.0.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rvs") (req ">=0.5.0, <0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.0.0, <4.0.0") (default-features #t) (kind 2)))) (hash "0j5al7zq3kgr9xbd0k3wnwmwmcxjgslgiygwcc0cn7iapqsmb7s1")))

(define-public crate-rvs-parser-0.1 (crate (name "rvs-parser") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "1pysihl46h5xfkqhlj75sqf604r948mv2kb3lsadd53dfs5zd6dp")))

(define-public crate-rvs-parser-0.2 (crate (name "rvs-parser") (vers "0.2.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "0dvzr8ydg8xw9fcgf9l684aapzwn36d1y3q5x7zhb5az237n074g")))

(define-public crate-rvs-parser-0.3 (crate (name "rvs-parser") (vers "0.3.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "1p9q1xxn2csacdq0cr5xh8rmlcpb6yxdfbgm5ixghl90rlrmnqh0")))

(define-public crate-rvs-parser-0.4 (crate (name "rvs-parser") (vers "0.4.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "10xp4fgyqpdd92w03iym9h63ddj2l8lvg3xfnjjnr8zf2vh4byif")))

(define-public crate-rvs-parser-0.5 (crate (name "rvs-parser") (vers "0.5.0") (deps (list (crate-dep (name "peg") (req ">=0.6.0, <0.7.0") (default-features #t) (kind 0)))) (hash "10zdr1m0g84g3mcz3b6ygsyd0nbrk6iig36shq1z2n1cr2y34nx2")))

(define-public crate-rvs-repl-0.2 (crate (name "rvs-repl") (vers "0.2.0") (deps (list (crate-dep (name "rvs") (req "^0.2") (default-features #t) (kind 0)))) (hash "18a98fkpp291fxg2qi881jwdlrkkv59gf7yaym7v0l8mc3hw94mk")))

(define-public crate-rvs-repl-0.3 (crate (name "rvs-repl") (vers "0.3.0") (deps (list (crate-dep (name "rvs") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jywvhfz98i6wrdi6jy9g4gb2plwl6k4p86vdf1ybsrqvyp3nlic")))

(define-public crate-rvs-repl-0.4 (crate (name "rvs-repl") (vers "0.4.0") (deps (list (crate-dep (name "rvs") (req "^0.4") (default-features #t) (kind 0)))) (hash "10hzbnwca15rhd3ij5vh948asp7xgiaawq672c22sl3r2xxgr8d2")))

(define-public crate-rvs-repl-0.4 (crate (name "rvs-repl") (vers "0.4.1") (deps (list (crate-dep (name "rvs") (req "^0.4") (default-features #t) (kind 0)))) (hash "06njy99k2cv6rxczy6f2mlyaisl7f8clj6xxzqgmpbmny3mfvc5m")))

(define-public crate-rvs-repl-0.5 (crate (name "rvs-repl") (vers "0.5.0") (deps (list (crate-dep (name "rvs") (req ">=0.5.0, <0.6.0") (default-features #t) (kind 0)))) (hash "1zyw55d9lw60q549r9pwl0v41yygwz9b433shdwizvzc9ffpk40q")))

