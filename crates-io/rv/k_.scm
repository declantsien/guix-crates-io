(define-module (crates-io rv k_) #:use-module (crates-io))

(define-public crate-rvk_methods-0.1 (crate (name "rvk_methods") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rvk") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "rvk_objects") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1fw7ii3kzfqrdng37jza008104zyhyknnlqd9gkfybwlwdxjhdn9")))

(define-public crate-rvk_objects-0.1 (crate (name "rvk_objects") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0a1w54gbw2w1p6dlvxmgw1wpv2i6q3w6vrzqd8dhzpykk8zz4iyw")))

(define-public crate-rvk_objects-0.2 (crate (name "rvk_objects") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1whd1k17ip9ibnic57129whi1gclsnw4bdn2kw38xv7530rqzz9x")))

