(define-module (crates-io rv tf) #:use-module (crates-io))

(define-public crate-rvtf-1 (crate (name "rvtf") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "vtf") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "vtflib") (req "^0.2.1") (features (quote ("static"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "18znjd5p0x011h07jr3jz684wfisf2ixfqr09mljmgvgaz4fhgq7")))

