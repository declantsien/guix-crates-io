(define-module (crates-io rv st) #:use-module (crates-io))

(define-public crate-rvstruct-0.2 (crate (name "rvstruct") (vers "0.2.0") (deps (list (crate-dep (name "rvs_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wyxlrifsxd554ljvczimh11j4mgh8c25g7n8b9ivxxzd2685k20")))

(define-public crate-rvstruct-0.2 (crate (name "rvstruct") (vers "0.2.1") (deps (list (crate-dep (name "rvs_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "109akv4lyqzy5hr9smsycwc0y4m4ds51a488jg9zz7r3a09y0az6")))

(define-public crate-rvstruct-0.2 (crate (name "rvstruct") (vers "0.2.2") (deps (list (crate-dep (name "rvs_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qnb279mhld93zmmg9fpyfp3rjnvf6my0h0czwqlpj11n8kz0gyx")))

(define-public crate-rvstruct-0.2 (crate (name "rvstruct") (vers "0.2.3") (deps (list (crate-dep (name "rvs_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gvwa0h6sjp6hy0m0f360vixg55rk5k8k52w64ww3cc7xiliqj27")))

(define-public crate-rvstruct-0.3 (crate (name "rvstruct") (vers "0.3.0") (deps (list (crate-dep (name "rvs_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "1a3va54x23ckj2h1j08firpy2zrxbfmw928cvhrwc00djjlbp506")))

(define-public crate-rvstruct-0.3 (crate (name "rvstruct") (vers "0.3.1") (deps (list (crate-dep (name "rvs_derive") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "07cbhrn8xf23y0cz1bwavf8sjyr1q4gy56vp4nl6raz2cychc835") (yanked #t)))

(define-public crate-rvstruct-0.3 (crate (name "rvstruct") (vers "0.3.2") (deps (list (crate-dep (name "rvs_derive") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0pcdzla1gkdv304i9k3w9xb2qp5c9q3mh138yd6bc1j5qc78c1si")))

