(define-module (crates-io rv mt) #:use-module (crates-io))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.0") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "jvmti-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0a3w8xvnbv1mr0fdc7y0biqpzg0csjc2qn4ca3n5kz0k13nc84w1") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.1") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0r25nzccpzj1sw6rlg4b54z3rvb3s8snsh72y8a8f5lmz0zlgg06") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.2") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "090jgz5id26456nr3jl6jxmih0hvi7vb86hsg3cl76ksxzfd9493") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.3") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0n5whs90n45pwyzq0a04a2r29bdnrnqkv45sp2814fcjq1rkgrwc") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.4") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0jglj0ingm2qysznhcxgfhhajsl5219kr1g6j7s7wvc15w5zsc7j") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.5") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1dy1bbk3i19zkhq6i66rb2ibyfxajl32kk0n822wvpd9il29idav") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.6") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0pkzhiwld2fa47lzzb3fms2raiblfiabr6rwkjdca40bw1avpf83") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.7") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0njvq7jwzg2wkfni1ys8qlm7xp9dcq5hmkn50cbhy5zbnwxgizxs") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.8") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1wmc8sdm0kcb62va3clr9a4nycl916vs91ax69zckm8jm5ma2b4l") (yanked #t)))

(define-public crate-rvmti-0.1 (crate (name "rvmti") (vers "0.1.9") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1r6i2hwvfq3nky3kh2liwnmp9ig9jwpxsb5w9rvfvd7hvfm9985v") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.0") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "09v9amlmxq9d9wb8hixpywdcj9bf21w824lsjcyqlqwshq8y5f9m") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.1") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0bqhz6zqizwx7fmf9pcih83zgiplqx951rdvqs5w6dkrc57b2lkk") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.2") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0gzsaq06mzfihd1g72j8gx0jl8l3ax2wnlhgljhdcrl049vjal9x") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.3") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0z88jxihx11fhdrcya2cn1qdzj8avxwnzwfv9l38cqimxzm378xj") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.4") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0xlds4fa17a1ndslm74dc5bg9ks9k8hy7677yffp8bdvijizv959") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.5") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "18kfwav4gpi80wlksgy609xdl75bs6mpr6dqyway95kajcjfng86") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.6") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0h42744ms23h3z9ky7l8p45mkqz4kicnrqxp9vqv0f57rlhv0vml") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.7") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "16xr352jfp79lzj69f79lvnv79hpf0bg2x3bnsnvacicqc887swl") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.8") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0lg9p6rf5kw51b5zbdg3ip3mjjczwy8q5d4lx1961a9b93j8i7g0") (yanked #t)))

(define-public crate-rvmti-0.2 (crate (name "rvmti") (vers "0.2.9") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "04jf4x0yp98qkz0rizq1v8i8ginwnz06q1lcys4hmxs3mqzxy231") (yanked #t)))

(define-public crate-rvmti-0.3 (crate (name "rvmti") (vers "0.3.0") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1pws3a9ipcv0vj7vwb5sw1d9xyicnwbmwp55p9qah9ac1bha931g")))

