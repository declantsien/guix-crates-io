(define-module (crates-io rv cs) #:use-module (crates-io))

(define-public crate-rvcs-0.1 (crate (name "rvcs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "07xcdarxm0y410xb0g5khfrf1rqhvla3xi0j3p2ms47pbprh9l9s")))

