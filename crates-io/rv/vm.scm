(define-module (crates-io rv vm) #:use-module (crates-io))

(define-public crate-rvvm-0.2 (crate (name "rvvm") (vers "0.2.1") (deps (list (crate-dep (name "integral-enum") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "rvvm-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rvvm-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0b2w3zfdzp9kx4z67z5ni5cj20hhmbk5mk71lsf433qakr2ybab1")))

(define-public crate-rvvm-0.2 (crate (name "rvvm") (vers "0.2.2") (deps (list (crate-dep (name "integral-enum") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "rvvm-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rvvm-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1i8k91vpzmvgrmhaljas50frsx3mhx29xcdyckkdvnn242nn9vyw")))

(define-public crate-rvvm-0.2 (crate (name "rvvm") (vers "0.2.4") (deps (list (crate-dep (name "integral-enum") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "rvvm-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rvvm-sys") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0kyaj1b62g6s8mj40l33z6l3yxqfx3m5kaqn18zqv0bq5bggwkh1")))

(define-public crate-rvvm-0.2 (crate (name "rvvm") (vers "0.2.5") (deps (list (crate-dep (name "integral-enum") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "rvvm-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rvvm-sys") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "13j32j7r3vxc89rxyrizmlf331j5q0q8hqmrscrbfqvvdp81x73i") (features (quote (("dynamic" "rvvm-sys/dynamic") ("default"))))))

(define-public crate-rvvm-macro-0.1 (crate (name "rvvm-macro") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "093x9g60n2j640glgmljb6bxlyd8jk70xhyaazwlkj8a2anzvxw6")))

(define-public crate-rvvm-sys-0.1 (crate (name "rvvm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0pilws11amy2jq9gzvzz7jmgxbwpkksmwrdz0ak16pfj237rslr5")))

(define-public crate-rvvm-sys-0.2 (crate (name "rvvm-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.25") (default-features #t) (kind 1)))) (hash "02y6261r55k9lfdb6rg2q4iyzaccyfsd1aq1j2djc1d5ixcdlgvs") (features (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-0.2 (crate (name "rvvm-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.25") (default-features #t) (kind 1)))) (hash "1ax7g4yy2f1kwyp5481mid5gmdkw086crf1c7rg0imx1dyga1z60") (features (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-0.2 (crate (name "rvvm-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1ybn4c9kqhb7p15hy0msqb92vkcq3hsnjzq22azj32g7hyjngdwc") (features (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-0.2 (crate (name "rvvm-sys") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1wv1aiblwk2yrh6scd5hxf1qrkwjfzy8pjpi7khvg9w8daxwbqg5") (features (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-1 (crate (name "rvvm-sys") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0j8hdq4l0102m51jgl1qjq85iilvi1bhknp3jpn7x59xpzqjdmkn")))

(define-public crate-rvvm-sys-1 (crate (name "rvvm-sys") (vers "1.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0z5a0vqbsr0z9xba2m9kakm42br2jlwqgyva9jikfs9z55hwzr4f") (features (quote (("dynamic") ("default"))))))

(define-public crate-rvvm-sys-1 (crate (name "rvvm-sys") (vers "1.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0chx2if51kqhfx38gy1dbzk4r9zcbbkxb81n5hhdwsry95h2xrns") (features (quote (("dynamic") ("default"))))))

(define-public crate-rvvm-sys-1 (crate (name "rvvm-sys") (vers "1.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0r69mrbhms7i2lqf2nhqa31arzbff97ii1n4xb24fnficl9wphmh") (features (quote (("dynamic") ("default"))))))

