(define-module (crates-io rv m_) #:use-module (crates-io))

(define-public crate-rvm_opcode-0.1 (crate (name "rvm_opcode") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "10ks05pfygzzwvkmd3aibx2f5gmrfwpy3ad3l1x6f0spi9a1n07j")))

