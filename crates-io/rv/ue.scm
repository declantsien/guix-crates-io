(define-module (crates-io rv ue) #:use-module (crates-io))

(define-public crate-rvue-0.1 (crate (name "rvue") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0mis1mhn6q6x3vzkl58rasvilvxxynlqsmkqzh9s1cmwrbd1fk04") (features (quote (("serde-serialize" "serde" "serde_derive"))))))

