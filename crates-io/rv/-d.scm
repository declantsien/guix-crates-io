(define-module (crates-io rv -d) #:use-module (crates-io))

(define-public crate-rv-decoder-0.1 (crate (name "rv-decoder") (vers "0.1.0") (hash "1iqw0im3w4xv93a2xzgnfa64l7qx69aqg35sm3kqnf2azkx94yzj")))

(define-public crate-rv-decoder-0.1 (crate (name "rv-decoder") (vers "0.1.1") (hash "1ycd8m5pak91282r8f8h8v3ybkrn69dcdrhdh63vxgq6x2lskk9a")))

(define-public crate-rv-decoder-0.1 (crate (name "rv-decoder") (vers "0.1.2") (hash "0w9lpsx9kggda2s6lar7fs9bj1976g74nx6zfyg81585csm98pz1")))

(define-public crate-rv-decoder-0.1 (crate (name "rv-decoder") (vers "0.1.3") (hash "0wfdi2pkcvx7whaqv8dfl2kd87nfz02vxbj08jbi6138fp72qc31")))

