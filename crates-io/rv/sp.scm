(define-module (crates-io rv sp) #:use-module (crates-io))

(define-public crate-rvsp-0.1 (crate (name "rvsp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "realfft") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "1iqcc7dldasi55h3ydarfb2pfw4za9v56srz1sc763i670yw37pi")))

(define-public crate-rvsp-0.1 (crate (name "rvsp") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "realfft") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "0ka1vw2v5hg297a84813gj1q1rdznnv2ny3i0492zmrhibf81fdv")))

(define-public crate-rvsp-0.1 (crate (name "rvsp") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "realfft") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "0pfp63al04ik9rrlqphwncdb98ljg7by8m0bh3pmbarpkyprxx52")))

