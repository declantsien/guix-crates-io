(define-module (crates-io tl c5) #:use-module (crates-io))

(define-public crate-tlc5940-0.0.0 (crate (name "tlc5940") (vers "0.0.0") (deps (list (crate-dep (name "gpio") (req "^0.4") (default-features #t) (kind 0)))) (hash "149ypkbb45lrv0vy6krz809hn8f4l0710aghb2zgrmkfm5664x6i")))

(define-public crate-tlc5940nt-0.1 (crate (name "tlc5940nt") (vers "0.1.0") (deps (list (crate-dep (name "gpio") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1im1ch34g4n13a60zdnlrszglx2109jh93d6d0m7xidkf2agm565") (yanked #t)))

(define-public crate-tlc5940nt-0.1 (crate (name "tlc5940nt") (vers "0.1.1") (hash "0palp67wdl3nlwz1plgv9bd5pb9vnwl5s7g95c3dwwjimfnl3995") (yanked #t)))

(define-public crate-tlc59xxx-0.1 (crate (name "tlc59xxx") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 0)))) (hash "1ya5hdbbv1w3yf5v0xqnf8qx9v4srfgmfgcb5m6f2ij766digzk2")))

(define-public crate-tlc59xxx-0.1 (crate (name "tlc59xxx") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 0)))) (hash "18bswsry1nci0v3jpbc8aqkpxkif6s565hnh4d9z5myj9zl3r0l3")))

