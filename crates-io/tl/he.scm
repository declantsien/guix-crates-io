(define-module (crates-io tl he) #:use-module (crates-io))

(define-public crate-tlhelp32-1 (crate (name "tlhelp32") (vers "1.0.0") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("handleapi" "tlhelp32"))) (default-features #t) (kind 0)))) (hash "1b1r70i15db1gycqy7alin07p5f0cq8rlr5r8cppw01mwmg5vfpr")))

(define-public crate-tlhelp32-1 (crate (name "tlhelp32") (vers "1.0.1") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("handleapi" "tlhelp32"))) (default-features #t) (kind 0)))) (hash "005hmdiq8150jbzazfwrivxs0ss74dqakv1him5gxspslz83sxap")))

(define-public crate-tlhelp32-1 (crate (name "tlhelp32") (vers "1.0.2") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("handleapi" "tlhelp32"))) (default-features #t) (kind 0)))) (hash "1947d23hprzxsbzs67s72rgr4m0bizlskjx7vlym2rsvi0ay9yix")))

(define-public crate-tlhelp32-1 (crate (name "tlhelp32") (vers "1.0.3") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("handleapi" "tlhelp32"))) (default-features #t) (kind 0)))) (hash "09dadfm10srgrs3hhr4q0niz1xxafb1y7m98f4bmzgb14ycnx4ym")))

