(define-module (crates-io tl ay) #:use-module (crates-io))

(define-public crate-tlayuda-0.1 (crate (name "tlayuda") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.61") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "004kzfx02ssks7mc8jfrb8p52krm8ly9dlm3kv15a48a4slwankf")))

(define-public crate-tlayuda-0.1 (crate (name "tlayuda") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.61") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1zh43fr8yf077h2z8gvpgkx5pqfw8k0787m4ra2949amx0b6x6v4") (features (quote (("allow_outside_tests"))))))

(define-public crate-tlayuda-0.1 (crate (name "tlayuda") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.61") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0w81xvv4ilq28bdhjg7xjmj8dwr3n918wksanlg6kbkqpsn3fvai") (features (quote (("allow_outside_tests"))))))

