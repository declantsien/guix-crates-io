(define-module (crates-io tl et) #:use-module (crates-io))

(define-public crate-tletools-0.1 (crate (name "tletools") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 2)) (crate-dep (name "sgp4") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "tle-parser") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0zcz6i641f7hh016wrw1zx27bchkh27f1avkrd8x5ni13vxy3yb7")))

