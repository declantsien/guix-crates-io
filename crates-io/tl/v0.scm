(define-module (crates-io tl v0) #:use-module (crates-io))

(define-public crate-tlv0838-0.1 (crate (name "tlv0838") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0jx39ls3j46yhq0652glam9wsjmmpdznw9mk3n5fgq20aq4zd2w2")))

