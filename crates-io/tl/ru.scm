(define-module (crates-io tl ru) #:use-module (crates-io))

(define-public crate-tlru-0.1 (crate (name "tlru") (vers "0.1.0") (deps (list (crate-dep (name "sn_fake_clock") (req "~0.4.0") (default-features #t) (kind 2)))) (hash "0rv9ivi971nd8hwx739yagbf9kg9xwp4k667kvg60ab94m1k496c") (yanked #t)))

(define-public crate-tlru-0.1 (crate (name "tlru") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.8") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (default-features #t) (kind 2)))) (hash "1569c38yq694gwvvkvwmy44mypijcn0n6zbjmw8i8r0b0si7k7p6") (yanked #t)))

