(define-module (crates-io tl og) #:use-module (crates-io))

(define-public crate-tlog-0.1 (crate (name "tlog") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.0") (features (quote ("local-offset"))) (default-features #t) (kind 0)))) (hash "1334klcq1cvja921apwrmhhppnl7kdl18729r50kwcvy4nf4wydz")))

(define-public crate-tlog-0.1 (crate (name "tlog") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.0") (features (quote ("local-offset"))) (default-features #t) (kind 0)))) (hash "0z4hfq7jh9s59dv643fys5s1iz4j60ssf7g17l01zgvl60apwxjz")))

(define-public crate-tlog-0.1 (crate (name "tlog") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.0") (features (quote ("local-offset"))) (default-features #t) (kind 0)))) (hash "0y8bbqm6bvx38fp3dhlc75mxhhrplw4pir4wv940kjavsryc8djz")))

(define-public crate-tlog-0.1 (crate (name "tlog") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.0") (features (quote ("local-offset"))) (default-features #t) (kind 0)))) (hash "1l1w9b4isb9xan4hiyg6y0l5kdhxvrk9s1kw4d1bnh48dg1ddrwb")))

