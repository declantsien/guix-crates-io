(define-module (crates-io tl -b) #:use-module (crates-io))

(define-public crate-tl-build-0.1 (crate (name "tl-build") (vers "0.1.0") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0k2jddppfaqxa5ir5rhk88wqi3674rn9adn58hrifi91vs8k6nf7")))

