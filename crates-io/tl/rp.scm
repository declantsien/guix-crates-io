(define-module (crates-io tl rp) #:use-module (crates-io))

(define-public crate-tlrpc-0.1 (crate (name "tlrpc") (vers "0.1.0") (hash "05acb7iz0p04yqz7cbyd39k29cqbw2v94wrzxggqgsj23b1nmr5s")))

(define-public crate-tlrpc-build-0.1 (crate (name "tlrpc-build") (vers "0.1.0") (hash "04bx8xxhpx24abaqbi762iz8yii6rj29pk7kabjwg4vwzyc4n36f")))

(define-public crate-tlrpc-derive-0.1 (crate (name "tlrpc-derive") (vers "0.1.0") (hash "0y0l2j033iz31b6v8rzb89jscgamqsiy8q2gaxn5dv625i8qg0s5")))

(define-public crate-tlrpc-futures-0.1 (crate (name "tlrpc-futures") (vers "0.1.0") (hash "1k5kpi7ajq7f8p0mdcx7cg2p7fb40axbnzjhkk91mzbb88l5i4n3")))

(define-public crate-tlrpc-net-0.1 (crate (name "tlrpc-net") (vers "0.1.0") (hash "0k8ah76cc7jyasg7rsbng1r88m5hkhzb3k12m1c29mh2fn7kpypr")))

(define-public crate-tlrpc-types-0.1 (crate (name "tlrpc-types") (vers "0.1.0") (hash "154wf2dqhqlakvsw2k3iv9akk5s12lsgdxcq3hbd0mq0fi09xq33")))

