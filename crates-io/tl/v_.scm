(define-module (crates-io tl v_) #:use-module (crates-io))

(define-public crate-tlv_parser-0.0.1 (crate (name "tlv_parser") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0") (default-features #t) (kind 0)))) (hash "02vdc3khapjk80z794q09lr77m2h4pnz49hzn8r1m092cpliw0n3")))

(define-public crate-tlv_parser-0.0.2 (crate (name "tlv_parser") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0") (default-features #t) (kind 0)))) (hash "1fw4wwfc9vwmxaywj619pzl4h0mxrz8k9yxz3g0yq347ys7h8w1r")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0") (default-features #t) (kind 0)))) (hash "06z8kjzv20kajgkypk5xwr1lmqacm051mbh8zz2g5rwv9hnw7k4k")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0") (default-features #t) (kind 0)))) (hash "09gzpmnrq0a0rhzp8i17ngz4jq0x17zybl6mydbkn4116f1gl35i")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0") (default-features #t) (kind 0)))) (hash "1xcpivkhfrw7vg71wdh9f6iarlm4kbbxrhfgzrli8z24z8zs0189")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0") (default-features #t) (kind 0)))) (hash "183n4n5yyivczj0nqjcanq3ffj0yrdl8484rcn5wn331jdizbwx9")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "10d5d2wnb2x5v2wwxk54vzcllzxzngfhnh4cmnrldj6n434nsikv")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "18r9kznh8d268ccmx3rmiw2p1lncf1ddgm7yi6qwq4imyrag4w2g")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "1adc5wgfy0k47dmqziigx06804cnqjmqibqv0mvq04hmm5fp1jyh")))

(define-public crate-tlv_parser-0.1 (crate (name "tlv_parser") (vers "0.1.8") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "03bjvvgbv6zkxafld7gbzzsaqrzwqigcynfi2k63lwfxa3bc9q7c")))

(define-public crate-tlv_parser-0.2 (crate (name "tlv_parser") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "1flga4mvdlizmsk1zfv2w50s3mm9sv8xmhq92c8h8ws7z64rmi48")))

(define-public crate-tlv_parser-0.3 (crate (name "tlv_parser") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "0a7wi1l7aay47181c577p2p9mqlci9ry2j1lw3r8nkjh2s0g9g21")))

(define-public crate-tlv_parser-0.4 (crate (name "tlv_parser") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "1mjpwic1vg2m85wsvlax7whn112xsvk3y7d56va8fyqss8qh1dxi") (features (quote (("std") ("default" "std"))))))

(define-public crate-tlv_parser-0.5 (crate (name "tlv_parser") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "0x69xxh52x7wl3c4ymgv57icjnrp3n4c3v0pi4r4icrfndckc15x") (features (quote (("std") ("default" "std"))))))

(define-public crate-tlv_parser-0.5 (crate (name "tlv_parser") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "0hz1akjvws2zicl1cmpdrg9ddrix7k1x3aslrs8rf72a4wly9v70") (features (quote (("std") ("default" "std"))))))

(define-public crate-tlv_parser-0.6 (crate (name "tlv_parser") (vers "0.6.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "1ccxm0nsv9pcq5nx6gkm6nypx8m1212maqrkmbppxgmmx6nxfvbr")))

(define-public crate-tlv_parser-0.7 (crate (name "tlv_parser") (vers "0.7.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "1733brg24lbd7za4plnxik7sq5as7wxwz1b4rwlhp5lc20f7iydn")))

(define-public crate-tlv_parser-0.7 (crate (name "tlv_parser") (vers "0.7.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "1h7hkrkd0cylnjkv880xid2mhanrgs7ll4k78wirgsf1lvywbf5q")))

(define-public crate-tlv_parser-0.8 (crate (name "tlv_parser") (vers "0.8.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "1544w6vkgjid45iv0vq5lz8rrx9mz54566h5szb589wz721mbzx3")))

(define-public crate-tlv_parser-0.9 (crate (name "tlv_parser") (vers "0.9.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 2)))) (hash "16gq26dnprcn8nwhrinr363gdkb0x65ryh3whki9vl7dgbi3vs2a")))

