(define-module (crates-io tl sh) #:use-module (crates-io))

(define-public crate-tlsh-0.1 (crate (name "tlsh") (vers "0.1.0") (hash "0vi3i46745yaz7zczvwy8ch051haj6nsb8qrk3c9jrrnck70xidb")))

(define-public crate-tlsh-fixed-0.1 (crate (name "tlsh-fixed") (vers "0.1.1") (hash "1is50zyqp32qgwaxwcx1kdli8lhy0agsk26w2a2y78gd121wlqpp")))

(define-public crate-tlsh2-0.1 (crate (name "tlsh2") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "18digg8ydq3m2gayc26fl847ldpwcvnzgjhfi1k9nm9rcq4pdfcn")))

(define-public crate-tlsh2-0.2 (crate (name "tlsh2") (vers "0.2.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "017mnfskndb91dj19bxdphmkgs9sh8mvy0z71kvjapg3wabxlzkx") (features (quote (("diff"))))))

(define-public crate-tlsh2-0.2 (crate (name "tlsh2") (vers "0.2.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1dni4yqvcc5s6cv33hkmv0dgnxz68yvpm0n2nigwcp9b2lqh00s0") (features (quote (("diff"))))))

(define-public crate-tlsh2-0.3 (crate (name "tlsh2") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1aq1sfqq4kjfir8m54zkcq2a6f7vsd63c8dc9i630k2nvykdcaa1") (features (quote (("diff"))))))

