(define-module (crates-io tl v3) #:use-module (crates-io))

(define-public crate-tlv320aic23-0.1 (crate (name "tlv320aic23") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "=0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lbzfbazij5kbnm7f7cf635w8y28wqbiygjpc0q2jc9hjp4rjp5a") (features (quote (("defmt-log" "defmt"))))))

