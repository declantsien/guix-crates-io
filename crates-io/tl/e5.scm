(define-module (crates-io tl e5) #:use-module (crates-io))

(define-public crate-tle5012-0.0.1 (crate (name "tle5012") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0i169qpjhzimd80nriradz1dzn8wjj3ry2ad9skr9hl18d8c31lz")))

(define-public crate-tle5012-0.1 (crate (name "tle5012") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6.7") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "serialio") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "stm32f7") (req "^0.7.0") (features (quote ("stm32f7x7" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "stm32f7x7-hal") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "0cc1jdzqxfsh5ygvk2hz87hdq5y13yrb7qjv74fs4f37z8ah66jw")))

