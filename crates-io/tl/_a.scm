(define-module (crates-io tl _a) #:use-module (crates-io))

(define-public crate-tl_auth_service-0.1 (crate (name "tl_auth_service") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "018wlg3nbhrz577xjrisgz86qmwnsizavq66g6nzq7jswd8rs577") (yanked #t)))

