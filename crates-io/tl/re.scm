(define-module (crates-io tl re) #:use-module (crates-io))

(define-public crate-tlrepo-0.1 (crate (name "tlrepo") (vers "0.1.0") (deps (list (crate-dep (name "git2") (req "^0.13.5") (kind 0)) (crate-dep (name "thread_local") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "14l8izkr3as6m6q1g0yi0rzxgr5azqikq0gskq5dlbx2k8rmrbaa")))

(define-public crate-tlrepo-0.1 (crate (name "tlrepo") (vers "0.1.1") (deps (list (crate-dep (name "git2") (req "^0.13.5") (kind 0)) (crate-dep (name "thread_local") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0mh4ryaw8dgwy4c2xvgjgzmpimik8070rf752p2a2bwcqk77xk10")))

(define-public crate-tlrepo-0.2 (crate (name "tlrepo") (vers "0.2.0") (deps (list (crate-dep (name "git2") (req "^0.14") (kind 0)) (crate-dep (name "thread_local") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1kfbwlia5qhrci4xgbsq1q5jp7nf5kpv2bpr0nh9kymzazcmnsir")))

(define-public crate-tlrepo-0.3 (crate (name "tlrepo") (vers "0.3.0") (deps (list (crate-dep (name "git2") (req "^0.15") (kind 0)) (crate-dep (name "thread_local") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "0dxh4wyks0x89szkz0nlb1fjm837dc10xc8ahp1ajbr40xmaail2")))

(define-public crate-tlrepo-0.4 (crate (name "tlrepo") (vers "0.4.0") (deps (list (crate-dep (name "git2") (req "^0.16.1") (kind 0)) (crate-dep (name "thread_local") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "0jw0pi6wpxzx89xqhmfml95kv8yxjvgdym9vv5vbz5dl8sfd03jh")))

(define-public crate-tlrepo-0.5 (crate (name "tlrepo") (vers "0.5.0") (deps (list (crate-dep (name "git2") (req "^0.17.1") (kind 0)) (crate-dep (name "thread_local") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "1cwyh8p2hl91vr3xifaxv56k6ls7smmfz46kfvxswk4pcq7kj13x")))

(define-public crate-tlrepo-0.6 (crate (name "tlrepo") (vers "0.6.0") (deps (list (crate-dep (name "git2") (req "^0.18.1") (kind 0)) (crate-dep (name "thread_local") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "1shn5557j8wliz5i85zf16cgr27drplzzni7g2niqx6k8nc1ka9z")))

