(define-module (crates-io tl ec) #:use-module (crates-io))

(define-public crate-tlecs-0.1 (crate (name "tlecs") (vers "0.1.0") (hash "0nrlhfqzsrb0kw0pyx794ldmaahdii3dvb7bjpx76l28icrbgqrn") (yanked #t) (rust-version "1.65")))

(define-public crate-tlecs-0.1 (crate (name "tlecs") (vers "0.1.1") (hash "17bikg6961clljzazvhyvhmxwdb27r76hxhww98p63fjkj4wngi2") (yanked #t) (rust-version "1.65")))

(define-public crate-tlecs-0.1 (crate (name "tlecs") (vers "0.1.2") (hash "1012hzfj2ncfxrbrb7gws7m3haaqwgpmfgk0jgw43i5643vcfvv2") (yanked #t) (rust-version "1.65")))

(define-public crate-tlecs-0.1 (crate (name "tlecs") (vers "0.1.3") (hash "1qlcbcv67icsgqrjaqr9x8pjn42gbrnasfshvgx7m651vw6jmc4b") (rust-version "1.65")))

(define-public crate-tlecs-0.1 (crate (name "tlecs") (vers "0.1.4") (hash "1vbf7b9bh2ccrky9gnx9ws5r67gx3l5qp6m19nlbrm72dqqvfpsy") (rust-version "1.65")))

