(define-module (crates-io tl is) #:use-module (crates-io))

(define-public crate-tlisp-0.0.1 (crate (name "tlisp") (vers "0.0.1") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "154zbmzz80a91y0pd722811zi37fw83zjd641cfcjc4jqrmbl5xx") (features (quote (("build-binary" "linefeed"))))))

(define-public crate-tlisp-0.0.2 (crate (name "tlisp") (vers "0.0.2") (deps (list (crate-dep (name "dyn-fmt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "02x9j2pqigsizwbl8fi9x3zmwl74aili79va5dx4fvm89g0d5bc4") (features (quote (("bin" "linefeed"))))))

(define-public crate-tlist-0.5 (crate (name "tlist") (vers "0.5.0") (deps (list (crate-dep (name "typenum") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1yqd6543mlw5hn9bk36b495j37izs64k11ci0p2zdng3hwvbva15")))

(define-public crate-tlist-0.5 (crate (name "tlist") (vers "0.5.1") (deps (list (crate-dep (name "typenum") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "19rnw4w0pkxwgb63vd6cvycdswq8gwpfarcgfrzvi6x9rxj8gw3b")))

(define-public crate-tlist-0.6 (crate (name "tlist") (vers "0.6.0") (deps (list (crate-dep (name "typenum") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1pqqmdf7kgp8ikzs9hs92k179ambhq3gmrls04lfxikz7v7wn0dn")))

(define-public crate-tlist-0.6 (crate (name "tlist") (vers "0.6.1") (deps (list (crate-dep (name "typenum") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0281lnzablmjzdr7m457wb7j2aqv1x0mys0mw86q18as4bv85l6h")))

(define-public crate-tlist-0.6 (crate (name "tlist") (vers "0.6.2") (deps (list (crate-dep (name "typenum") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1y99s20y1xnsv5yykkl1a9k7airw7vnxdvmx4fw80g9f1jzx01wd")))

(define-public crate-tlist-0.7 (crate (name "tlist") (vers "0.7.0") (deps (list (crate-dep (name "typenum") (req "^1.16.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.16.0") (default-features #t) (kind 2)))) (hash "0kpbpma94ansfypvszs495kk7hwgslv9fgwm7l2hpxb39j5g7wi9") (features (quote (("doc" "typenum")))) (v 2) (features2 (quote (("typenum" "dep:typenum"))))))

