(define-module (crates-io tl -c) #:use-module (crates-io))

(define-public crate-tl-codegen-0.1 (crate (name "tl-codegen") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1f0yhi9mswa1ag4pd05xx873rf5bkx0wm7g5d0j9chr3426xdc56")))

