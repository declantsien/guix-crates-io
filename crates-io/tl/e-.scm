(define-module (crates-io tl e-) #:use-module (crates-io))

(define-public crate-tle-parser-0.1 (crate (name "tle-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "0qa8ml0q0a0jrn9jxxr6f8sd49jhwwac11sxfi432glygk7gih6k")))

(define-public crate-tle-parser-0.1 (crate (name "tle-parser") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "1sjpqk78c16zznbrif7ghc84gdp0xva4idrj9s5liw7ydm367l6a")))

(define-public crate-tle-parser-0.1 (crate (name "tle-parser") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "0agb7d32f9rcpyd1gh5jf8fijqfqkz7g906h1663i9v9mgqz8rz8")))

(define-public crate-tle-parser-0.1 (crate (name "tle-parser") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pp8788080rzfncc9p0npvrfwphx8ikczvmywiqxhmdc86cjiqw4")))

