(define-module (crates-io tl bf) #:use-module (crates-io))

(define-public crate-tlbf-0.1 (crate (name "tlbf") (vers "0.1.0") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "12bgdj2cnfkfyz1y1qm3sd8mg0syd2x69wa9ph46j18g03vrz7nc") (yanked #t)))

(define-public crate-tlbf-0.1 (crate (name "tlbf") (vers "0.1.1") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "0hi7wq68qff2vyfwdissalzdsv3kvns4lsdbl4vzanzdc4qrzbw7") (yanked #t)))

(define-public crate-tlbf-0.1 (crate (name "tlbf") (vers "0.1.2") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1lxh2xdrj8p94bfih4qq9iavwb0fm42vydi7rmhmzbry48xf9jzf") (yanked #t)))

(define-public crate-tlbf-0.2 (crate (name "tlbf") (vers "0.2.0") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1rvf8bsippjjmybz6jxmf8idz1v0iy7byhsfxz9qb8bk5qhsrxzy")))

(define-public crate-tlbf-0.3 (crate (name "tlbf") (vers "0.3.0") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "0mgnsrdjmj0klwjigdshzl1qldml1876dqjpfmlzailisghrilbk")))

(define-public crate-tlbf-0.3 (crate (name "tlbf") (vers "0.3.1") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "0cgngq7hv0hmrnr0ym59xs4p5xk5lpp0dzgg0bw66pkcdqbw3lw9")))

(define-public crate-tlbf-0.3 (crate (name "tlbf") (vers "0.3.2") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "13xz13qw6xbdpw8xvmb8zjzb7s0kayh80r42kwjm503iv54khp2f") (yanked #t)))

(define-public crate-tlbf-0.3 (crate (name "tlbf") (vers "0.3.3") (deps (list (crate-dep (name "ghost") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "0wvmdx9vq7974v0887ggi7y6jnaddvlfh81yy6mbx3rvqh5ircz8")))

