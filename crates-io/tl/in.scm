(define-module (crates-io tl in) #:use-module (crates-io))

(define-public crate-tline-0.1 (crate (name "tline") (vers "0.1.0") (deps (list (crate-dep (name "hdf5") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "hdf5-sys") (req "^0.8") (features (quote ("static"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "physical_constants") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "00gvnp6q2l68p49rrimsx1hwshckisfya6vd3aq0h7k322ffx1pw")))

