(define-module (crates-io tl id) #:use-module (crates-io))

(define-public crate-tlid-0.1 (crate (name "tlid") (vers "0.1.0") (hash "1jg11d6a3vjih0zb5x1dwig1gjkd9pmdiiqcbgavsyni2iyd1hfd") (features (quote (("std") ("log") ("default" "std"))))))

(define-public crate-tlid-0.2 (crate (name "tlid") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "160ljdlk2p56lx8g56jv24qws62qa68b6v5i81vb3k422fz47jha") (features (quote (("std") ("log") ("default" "std" "num-traits"))))))

(define-public crate-tlid-0.2 (crate (name "tlid") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "07ja0sddcs66dcymgjb844mwgkg9rdmirmm9wm6q5sm2gxsa6n21") (features (quote (("std") ("log") ("default" "std" "num-traits"))))))

(define-public crate-tlid-0.2 (crate (name "tlid") (vers "0.2.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "07pn2wbj8hbsxzpichs3fvhpbirff6xfz1j0h20jkc0mwaf3y0yz") (features (quote (("std") ("log") ("default" "std" "num-traits"))))))

