(define-module (crates-io df mu) #:use-module (crates-io))

(define-public crate-dfmutex-0.1 (crate (name "dfmutex") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19ff97anzk1ykd3v2w75mw5yf18dh0iilwrgyy58c6ca1kqdkq3a")))

