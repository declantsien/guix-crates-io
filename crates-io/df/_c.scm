(define-module (crates-io df _c) #:use-module (crates-io))

(define-public crate-df_cp437-1 (crate (name "df_cp437") (vers "1.0.0") (hash "1zjibz4lpj2z768842lhi0kc6nbr8p89wvldzz5cssbhjdgc46hy")))

(define-public crate-df_cp437-1 (crate (name "df_cp437") (vers "1.0.1") (hash "0pl6c856g1lx2j1z51jl3jjy7msiagpj8c7mrih1mi905dnniyyn")))

(define-public crate-df_cp437-1 (crate (name "df_cp437") (vers "1.1.0") (hash "1wza9vx8rbm9amxmbyw25zaz0v8pjaf3bi797wcwsfif1lvp20v8")))

