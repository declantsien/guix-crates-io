(define-module (crates-io df kz) #:use-module (crates-io))

(define-public crate-dfkzr-0.1 (crate (name "dfkzr") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest-derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rfhkx9s7290f87s8xcc21nsvywa4ps6ckqaz5v07lkl8xrmfryq") (features (quote (("default")))) (v 2) (features2 (quote (("proptest" "dep:proptest" "dep:proptest-derive"))))))

