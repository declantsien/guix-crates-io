(define-module (crates-io df #{1t}#) #:use-module (crates-io))

(define-public crate-df1t-cyph-0.1 (crate (name "df1t-cyph") (vers "0.1.0") (hash "0zwwvb056md88mwf2qmip1ivlm9iil88251s9zjhm910d35karv9")))

(define-public crate-df1t-cyph-0.1 (crate (name "df1t-cyph") (vers "0.1.1") (hash "0ri1k7lal55lq0ab8g5dndycwjngynrmvi2a75l5z2jwqclj91lx")))

(define-public crate-df1t-cyph-0.2 (crate (name "df1t-cyph") (vers "0.2.0") (hash "0nxn3la066cw36my9f692y225h6whlp7ngiz5wsjlanc91xdq47p")))

(define-public crate-df1t-cyph-0.2 (crate (name "df1t-cyph") (vers "0.2.1") (hash "1d3cf549aa9in6izxny63aw6bw45k9ysycps8jilx29vs8kibj75")))

