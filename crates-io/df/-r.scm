(define-module (crates-io df -r) #:use-module (crates-io))

(define-public crate-df-rs-0.1 (crate (name "df-rs") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1hwg1l30zfxlhr41nqrk6lw58bmcpz1zc4z67m6fqf1zxy03cjrn")))

(define-public crate-df-rs-0.2 (crate (name "df-rs") (vers "0.2.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "14wlsx7srcg7cyhisdj48rvd39g2hwhqjc6rlvf8rfawsp6rvhis")))

(define-public crate-df-rs-0.2 (crate (name "df-rs") (vers "0.2.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1yzpdsy00dc232vxl19wpsyfylxcq7ahqiwc4ba30fdz2a8bjdww")))

(define-public crate-df-rs-0.2 (crate (name "df-rs") (vers "0.2.2") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "async-recursion") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bimg6ljwmx0bfp17gmr7bl4iw7wlp6wgzwz1rbk7d4ypcghyf0s")))

