(define-module (crates-io df -m) #:use-module (crates-io))

(define-public crate-df-maths-0.1 (crate (name "df-maths") (vers "0.1.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0jab9v9s548pn7kdlghq1pi9vkxxkc4ndxgr5jlcfs2lxh5x88hj")))

