(define-module (crates-io df -f) #:use-module (crates-io))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0rlafs26sanchk4gf1chsjx6zqg24r5dwry4jfgh518p3vqpn2f7") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1bjy9zv2zlzzchq5567d3ag7biwyy7vnh6q2i9vc908m53gv68pg") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "06yzzdlhi9624gsh0m36jvc91283g4wkdanmd3bxzq5l8yiyj3pw") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1jjmg0dmxmslh2xs80j07w6p12h8fbswgb9xlfp7ch1sihs7z3ci") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0im0xz20qss5zcrwx5bbbpf6ljkwhp9wip3n4qcs3lc5azp9qkfh") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "135n39fdjxihif01vrlmyqj3327yabipx6hv13fxyc78f6vf29q1") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1yz9p8syqdrfr1xpiw6vqvpry8nvnd9z33aq8k84whp4s42c52br") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1qj6aipa9yjbcv9mgy7a04kq9739rwvlaxpwgdraqs2a0gzz0bd6") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "16g5dd5a3qmdd8vx2mg1i0wgz1xg3nxhhd33iabp4lpnsa6dnx65") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.9") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1lbvmimnv8yz032savy46wy4pcbyskw38qdqkq1q9vzra5vvf7v6") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.10") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1kixsv6ikaffrjl8npmzy9krn08hzhcmdfxpa87yw34kphsda1sb") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.11") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1ja6y48n929h099dy1z1vjangm8a31q7qgh2l9l1517aig2q6zzi") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.12") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "13cv1psly7m3h3dprqjgjbidgcydf0ds04ik41fh3f8ncn5fa3kp") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.13") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1vvbgw05s3109wg1ngpzlnkb8c6ykx36ipc9kw3341vvpfskf4v7") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.14") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "16vdkh08rpxpmwikjh93mjl6rric9d5xmsn2jlx9pqxns9r9jk0s") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.15") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "172ya436q3n70an5m3wvdqrz0rllibck8avkpq8z8qdxcvqkh2dq") (yanked #t)))

(define-public crate-df-fields-0.1 (crate (name "df-fields") (vers "0.1.16") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "16axqf10lndwnrcjlczj8qnnvfmvb87f08xj83mb1v7nc70g8k5n")))

(define-public crate-df-file-0.1 (crate (name "df-file") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "01s13adwrx031k6vxn8jyshcjmmz49hz9zksy5kqb4svi35m6nrr") (yanked #t)))

(define-public crate-df-file-0.1 (crate (name "df-file") (vers "0.1.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "11rh320x3x7xm0avc08fp1pm4wmwly3ghzlhygsinl78iw4fx5ga") (yanked #t)))

(define-public crate-df-file-0.1 (crate (name "df-file") (vers "0.1.2") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "oxipng") (req "^8.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1zc3gqx2n8p2y2f581hz5w0631hlm6rb1q94sfj06j1h6w7q5mjh") (yanked #t)))

(define-public crate-df-file-0.1 (crate (name "df-file") (vers "0.1.3") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "oxipng") (req "^8.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1wpl29963y60b9yndwg4nqk5dnwsjvlnvyxgvchvy2xvjh58512v")))

