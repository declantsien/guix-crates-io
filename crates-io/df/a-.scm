(define-module (crates-io df a-) #:use-module (crates-io))

(define-public crate-dfa-regex-0.0.1 (crate (name "dfa-regex") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (default-features #t) (kind 0)))) (hash "03izj7k45935x279jw0dnd9ifg0cvv52mb7xsww8zxfa174wqv3c")))

(define-public crate-dfa-regex-0.0.2 (crate (name "dfa-regex") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "03bmczagyiya65i5a71j3ybdv9fv3wk83l9z61s2gixydnv5fkv7")))

(define-public crate-dfa-regex-0.0.3 (crate (name "dfa-regex") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "00q2x6gihy6hx8cgampz236015scmgl0s3w1gy3mxbxj1x8kx8hj")))

