(define-module (crates-io df co) #:use-module (crates-io))

(define-public crate-dfconfig-0.1 (crate (name "dfconfig") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jjw3zm9klk7kcd6bnz88rr1g18j54dnyqlbhkv5pmxlbvnh5yln")))

(define-public crate-dfconfig-0.2 (crate (name "dfconfig") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0r03hbx06923122bgjalzznc66myx2qfy1qrbggapsg4rin0hzsm")))

