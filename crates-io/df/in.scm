(define-module (crates-io df in) #:use-module (crates-io))

(define-public crate-dfinity-logo-0.1 (crate (name "dfinity-logo") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0mhnzijglacfnav6xqn7nmjl4bp8km8frv220qvzwzmksl4qw6w6")))

