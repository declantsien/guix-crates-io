(define-module (crates-io df #{23}#) #:use-module (crates-io))

(define-public crate-df2301q-0.1 (crate (name "df2301q") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yaqkx971923pvws92ah9vnq947417cgs6dc8nclaw6ncxnjhwnm")))

