(define-module (crates-io df a_) #:use-module (crates-io))

(define-public crate-dfa_learning_toolkit-1 (crate (name "dfa_learning_toolkit") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1di999qjr9hcm1p2dbhw8z3yfkib2llvk5kax67baazrp8p1baz0") (yanked #t)))

