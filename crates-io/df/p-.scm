(define-module (crates-io df p-) #:use-module (crates-io))

(define-public crate-dfp-number-0.0.1 (crate (name "dfp-number") (vers "0.0.1") (deps (list (crate-dep (name "dfp-number-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0hk8d3n9y5r8sgn307x2v8yshk7dn20mg0kf9ypic0aanpgzjqqz")))

(define-public crate-dfp-number-0.0.2 (crate (name "dfp-number") (vers "0.0.2") (deps (list (crate-dep (name "dfp-number-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "107hmv05jvlbgawy5px22mazp9d1pc4v3wa4n1z0ql9877wap0i9")))

(define-public crate-dfp-number-0.0.3 (crate (name "dfp-number") (vers "0.0.3") (deps (list (crate-dep (name "dfp-number-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1pv95fa2dfap9a2g66pcjmxllzanz1gckpspzgj22241big0imi5")))

(define-public crate-dfp-number-0.0.4 (crate (name "dfp-number") (vers "0.0.4") (deps (list (crate-dep (name "dfp-number-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1affkz72jyhrl8jzj1s9sbavwkmca399ljagdn0nb3bzfgdpr173")))

(define-public crate-dfp-number-0.0.5 (crate (name "dfp-number") (vers "0.0.5") (deps (list (crate-dep (name "dfp-number-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0cxjckz5b8dby6881ivn7f8bihp8z6db28m97m2rsq04qw28ycz6")))

(define-public crate-dfp-number-0.0.6 (crate (name "dfp-number") (vers "0.0.6") (deps (list (crate-dep (name "dfp-number-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "12j1kbsxpz73aqx6h75zxxalprg8ykxz0my541b10vprc0yz4h3r")))

(define-public crate-dfp-number-0.0.7 (crate (name "dfp-number") (vers "0.0.7") (deps (list (crate-dep (name "dfp-number-sys") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "1l9p3b7vrsv3qi9mkymvk3lxh2bkljcpv9j76njkl3gzzmj9z3ms")))

(define-public crate-dfp-number-sys-0.0.1 (crate (name "dfp-number-sys") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "0rvmgkrnlan5syqc69x9c0fzf0nvlnlvq2zl2q7z6ikdimh3m1s2")))

(define-public crate-dfp-number-sys-0.0.2 (crate (name "dfp-number-sys") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "1ja1lmqql9zqk1s78hid2f33nni1n0ib5cms6j6w4n0ing2dm7sh") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.3 (crate (name "dfp-number-sys") (vers "0.0.3") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "1p970z454wzqzmpv1ljsxipng9x2y3km6zmlikhcxwi2c6qlij23") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.4 (crate (name "dfp-number-sys") (vers "0.0.4") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0gyk00452lq6prxmaa7xd27k9yypl24x3y7rnj4y872vh02sr088") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.5 (crate (name "dfp-number-sys") (vers "0.0.5") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0fv4kg336r666cb8k6yabsryqjz9hanj2dw6ag0ys59v0n70ql2h") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.6 (crate (name "dfp-number-sys") (vers "0.0.6") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0v7dgh90g690xs03i35r4drnvv6k9h1fbc0bzzr7r7r983wqj7jm") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.7 (crate (name "dfp-number-sys") (vers "0.0.7") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "16x01mm6gk8acsa03iz3838f20ldlps9alw0mn6zr6bkh06l8qxs") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.8 (crate (name "dfp-number-sys") (vers "0.0.8") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0xxdf6wnr8xbh4si12x56imwfw6miqhnbnl7dx6skw72sd5qlsqx") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.9 (crate (name "dfp-number-sys") (vers "0.0.9") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "1rb5s1i5cxr76w0b02np5p57wsk7cgk6kll289n3ijfy6hwnnbj4") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.10 (crate (name "dfp-number-sys") (vers "0.0.10") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "028v7i123y2bsv7id8ldzlmdz32b3lxsd5pgdsz6f58wy27spldp") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.11 (crate (name "dfp-number-sys") (vers "0.0.11") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "0mi5mfgm8s3c1kspv7mjzjvcqjws2mg62983iscpq4y2clcxavwa") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.12 (crate (name "dfp-number-sys") (vers "0.0.12") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "15li48zf2bmk2lbd47s0gdmzn5b108n8ma9ajhjb3ypmjzq5fp49") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.13 (crate (name "dfp-number-sys") (vers "0.0.13") (deps (list (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 0)))) (hash "0dyznrsq4mkkgxwp96hlqrz5x7l062i00bhwsf9zvxmd9dqkirja") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.14 (crate (name "dfp-number-sys") (vers "0.0.14") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "19akjk5bsnq4l31h2pmqr64bnkdil97lpv0pgh3qci7fgvjk3sdk") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.15 (crate (name "dfp-number-sys") (vers "0.0.15") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1vdvn4xnalaghv3p3adapzc4hfd7ayklxfs79z82vk59vpw4lz21") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.16 (crate (name "dfp-number-sys") (vers "0.0.16") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)))) (hash "0mv9wag4v2653xcf6y4a57qyvd5svhgxk2xaskjc5gmb6cnkh225") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.17 (crate (name "dfp-number-sys") (vers "0.0.17") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)))) (hash "1x3xzisn8rpkbfxz9yarjj2lpsa2ya5mjlr168wdphiz4pvi1878") (features (quote (("global-rounding") ("global-exception-flags") ("call-by-reference"))))))

(define-public crate-dfp-number-sys-0.0.18 (crate (name "dfp-number-sys") (vers "0.0.18") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)))) (hash "0wl092b44nkwx8yikfhwci1rhpghjckpdpgfjgv1kw9nk12i5k1x")))

(define-public crate-dfp-number-sys-0.0.19 (crate (name "dfp-number-sys") (vers "0.0.19") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)))) (hash "1axasfk4010zlq2b6bmcg27sgaxd0h3xy7ijp2yicrzbgiaws7z3")))

(define-public crate-dfp-number-sys-0.1 (crate (name "dfp-number-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.90") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "16h875r7iqpvrfjja64qlmv9sj5p69clbfq5xpjrrys0qnypb3yk")))

