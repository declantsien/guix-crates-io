(define-module (crates-io df s-) #:use-module (crates-io))

(define-public crate-dfs-rs-0.0.1 (crate (name "dfs-rs") (vers "0.0.1") (hash "0chld4i22sas8pyb08wdgag18f8h46mdrmr4i0hvcbxdcl7kpd1f")))

(define-public crate-dfs-rs-0.0.2 (crate (name "dfs-rs") (vers "0.0.2") (hash "14nrhi7pbvgcaw3pkxh7pawzvv9naq5qsdnn4cbs545lyfk7d9sq") (features (quote (("unsafe") ("default" "unsafe"))))))

