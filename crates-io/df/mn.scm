(define-module (crates-io df mn) #:use-module (crates-io))

(define-public crate-dfmn-0.1 (crate (name "dfmn") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "online") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "14g816pwyy04jmybbgv6h2vrz2ddy2mc4crh75lmd9r4k3wc0hjd")))

