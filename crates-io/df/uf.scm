(define-module (crates-io df uf) #:use-module (crates-io))

(define-public crate-dfufile-0.1 (crate (name "dfufile") (vers "0.1.0") (hash "0abpkm94id6yx26764xa3bhac168pk4hi1w1q3nldfxr97kiy7jm")))

(define-public crate-dfufile-0.2 (crate (name "dfufile") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "169hmy2z7kd5ns724zpn7pqpkx4k8jqq4zk4ff91s5xjb0amz72k") (rust-version "1.58")))

