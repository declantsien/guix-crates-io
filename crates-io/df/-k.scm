(define-module (crates-io df -k) #:use-module (crates-io))

(define-public crate-df-kafka-0.1 (crate (name "df-kafka") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "kafka") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1imz4jksy8sp8874ifgnji9lpq6wr4f2r4mp2jg95gvwfkf4piy3") (yanked #t)))

(define-public crate-df-kafka-0.1 (crate (name "df-kafka") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "kafka") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "15g65hrh2z8x8wshjjn9a26sl0v9nyckncrh748bbn5hjqifmfrb") (yanked #t)))

(define-public crate-df-kafka-0.1 (crate (name "df-kafka") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "kafka") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0l6in7g45z3cb4msh8v4hs4d967bx6gqbnbv9n80zsbhx5jbdzrf")))

(define-public crate-df-kafka-0.1 (crate (name "df-kafka") (vers "0.1.3") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "kafka") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0viidc1zmkww90dglylk6h4nha7fl41bcbanm8n3hzalb23gy6fl")))

