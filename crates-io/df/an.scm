(define-module (crates-io df an) #:use-module (crates-io))

(define-public crate-dfang-0.1 (crate (name "dfang") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0fhbmjnp0j5rpvgnv67fiacljwqhsb9aabhgs0752hajjjxnvkgs")))

(define-public crate-dfang-0.1 (crate (name "dfang") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0rswxcgkry5zl9mc6blxk9r4pzy8kh74mpd27b7ldfp55cvkmdxp")))

(define-public crate-dfang-0.1 (crate (name "dfang") (vers "0.1.2") (deps (list (crate-dep (name "atty") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hzznx8maasjyy59g741y7k4i4rvc5zlgrp6j3fdibq939hs6bw5")))

(define-public crate-dfang-0.1 (crate (name "dfang") (vers "0.1.3") (deps (list (crate-dep (name "atty") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "18kvi585ksvcjj98hah756sxwz6hy213x0pp6vrvjayp2bfmc3vz")))

(define-public crate-dfang-0.1 (crate (name "dfang") (vers "0.1.4") (deps (list (crate-dep (name "atty") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "011rbq50maczsyzgv85j5cajw7116p8yp8v0cfy1nry4wq6graaq")))

(define-public crate-dfang-0.1 (crate (name "dfang") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12andrkj3pczyjxvmr51xs518hi4akpf22gs8zxa3izb7y3qh2fa")))

