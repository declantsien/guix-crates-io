(define-module (crates-io df r0) #:use-module (crates-io))

(define-public crate-dfr0299-0.1 (crate (name "dfr0299") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1r36w0783c5i7yniias3v4gmxyg4w9dvfilv5mlp56khlizgbcn7") (features (quote (("std" "num_enum/std")))) (v 2) (features2 (quote (("use_defmt" "dep:defmt"))))))

(define-public crate-dfr0299-0.1 (crate (name "dfr0299") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1qk9pcphlvqmy9qaizsjnmx9hng7kqaszy1rq3xrbpr5fa4jy8pi") (features (quote (("std" "num_enum/std")))) (v 2) (features2 (quote (("use_defmt" "dep:defmt"))))))

