(define-module (crates-io df la) #:use-module (crates-io))

(define-public crate-dflake-0.1 (crate (name "dflake") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1531v88w8n8hsjjyv9wzc24qqcl0hsn7n370fwmry64lfg2m698d") (features (quote (("default") ("chrono_support" "chrono"))))))

