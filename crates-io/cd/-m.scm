(define-module (crates-io cd -m) #:use-module (crates-io))

(define-public crate-cd-manager-0.1 (crate (name "cd-manager") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fps3kanxz1g3z0mxm0vzly1yv177v65vmr61zsyqrc8z4iy0x31")))

(define-public crate-cd-manager-0.1 (crate (name "cd-manager") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1jiflfjkpq1zbhxvqzsncf8mi3hyw1psimml3rplp0f45jcaw4wv")))

