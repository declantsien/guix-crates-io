(define-module (crates-io cd ds) #:use-module (crates-io))

(define-public crate-cdds-util-0.1 (crate (name "cdds-util") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0w7rsppg1w19nfprpw3mqfkwdgq6zs695kmf1d7m6qh58ix0xh0k")))

(define-public crate-cdds-util-0.1 (crate (name "cdds-util") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "1la4cl9vvi6ahyw2r0bm7fcyhiknvhmpxpmr7n31zjf24pbxrsj1")))

(define-public crate-cdds_derive-0.1 (crate (name "cdds_derive") (vers "0.1.0") (deps (list (crate-dep (name "derive-syn-parse") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0kp2xlw3c4n7jfx4qrbm7jra64gp0vf35vyxbmhsqi3g815f14bi")))

(define-public crate-cdds_derive-0.1 (crate (name "cdds_derive") (vers "0.1.1") (deps (list (crate-dep (name "derive-syn-parse") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1rqqzzd3fcynv535a9vywci2p8g6phig5rnsqfv74xa41fhqnlvf")))

