(define-module (crates-io cd up) #:use-module (crates-io))

(define-public crate-cdup-0.1 (crate (name "cdup") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09qnlz6nr3dn9gy2jvcncmqz4qk75f5vy74bfpsl55q1qjy79hfq")))

(define-public crate-cdup-0.2 (crate (name "cdup") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pw8jnfpzjd9mncakjm650vlasgrmrzljzjsd3qjwsk3916crrlr")))

(define-public crate-cdup-0.3 (crate (name "cdup") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy-matcher") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1vi1bgq3pbizac4snbdmdnjpx942fj9r1mkp8ys9fhmf516wzhh7")))

(define-public crate-cdup-0.4 (crate (name "cdup") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy-matcher") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0zv65nri6z5wzmc5ic9w1206gk3ihbgxpmk28m5n13vqwh3b4gvd")))

(define-public crate-cdup-0.5 (crate (name "cdup") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy-matcher") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "09hfdzfsziv0gy6wab8prhifd9rbxh90qg973074rfdchfrlfdym")))

