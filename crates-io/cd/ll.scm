(define-module (crates-io cd ll) #:use-module (crates-io))

(define-public crate-cdll-0.1 (crate (name "cdll") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 0)))) (hash "0mq3bsxwmyddy9zg66c5h5hc13nf83fgpls96i8q39ipgswv1shn") (yanked #t)))

(define-public crate-cdll-0.1 (crate (name "cdll") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 0)))) (hash "1jr6l8ir737qwsci9z9v5mbrnxnv8ikvd3gn9lr2irwicj8yfqrv")))

(define-public crate-cdll-0.2 (crate (name "cdll") (vers "0.2.0") (deps (list (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 0)))) (hash "13fz83fv3crhxs7ci9f7plvm9lm1kga1cabn98v7wh2jy36gyjxb")))

(define-public crate-cdll-0.3 (crate (name "cdll") (vers "0.3.0") (deps (list (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 0)))) (hash "16pmiyy8bx1lw2azdggdwj3x1x1dxf9qm5yj2rj3zbiim7bh1gcy")))

(define-public crate-cdll-0.4 (crate (name "cdll") (vers "0.4.0") (deps (list (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 0)))) (hash "119188dyyx4nfr8axzw2xpvniwvfjk0m4p58bwhymg7hifa8j6si")))

(define-public crate-cdll-0.4 (crate (name "cdll") (vers "0.4.1") (hash "16ia7klyg04dbvr8bg6f0srn73mz1ym6lx7r4zc62985sykvc5pd")))

(define-public crate-cdll-0.4 (crate (name "cdll") (vers "0.4.2") (hash "1m7vsgha9kaj2h0i880hxs8i1j4mcvqnf3aipj4ljb3p8ar5468b")))

