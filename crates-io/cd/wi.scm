(define-module (crates-io cd wi) #:use-module (crates-io))

(define-public crate-cdwifi-0.1 (crate (name "cdwifi") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19") (default-features #t) (kind 0)))) (hash "0h1bmbdzs5gfqlbwa1px6c45liwqlb0qzabd8d8angwrn8563irg")))

(define-public crate-cdwifi-0.1 (crate (name "cdwifi") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19") (default-features #t) (kind 0)))) (hash "1av8f7ap3drh241fwrx97dsyawhczwz22rmx4v33cdznkk8rwr10")))

(define-public crate-cdwifi-0.1 (crate (name "cdwifi") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19") (default-features #t) (kind 0)))) (hash "0q3acqkx7rydlsw9l76k7snd0v7an8aidwkmy2w2zlr6hp4pi30p")))

