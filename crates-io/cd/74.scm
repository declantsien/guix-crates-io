(define-module (crates-io cd #{74}#) #:use-module (crates-io))

(define-public crate-cd74hc4067-0.1 (crate (name "cd74hc4067") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "18qir02v7cmcqg388qz8zclrblyfqcwgn1c0cfwaanc21ba2j3r0")))

(define-public crate-cd74hc4067-0.2 (crate (name "cd74hc4067") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0vc62dj0j0203as2b1j9300l3576b97yf4k3a0yxl3k0bkaddcdz")))

