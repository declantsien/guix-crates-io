(define-module (crates-io cd i3) #:use-module (crates-io))

(define-public crate-cdi3-0.1 (crate (name "cdi3") (vers "0.1.0") (deps (list (crate-dep (name "remain") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "08x5wipijbaw1b2v7fz60ibxg13ay63kl48xgn15hfqm86hwvysl")))

(define-public crate-cdi3-0.2 (crate (name "cdi3") (vers "0.2.0") (deps (list (crate-dep (name "remain") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "0df13rlsd6f389pn5c3182nhaxgn7g8jxil6cy3w5z6w35rzlkc6")))

