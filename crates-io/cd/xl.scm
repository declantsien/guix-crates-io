(define-module (crates-io cd xl) #:use-module (crates-io))

(define-public crate-cdxlib-0.1 (crate (name "cdxlib") (vers "0.1.1") (deps (list (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0m3a52hkclgfjlc67gysr5p4k54l09q7vcllir7x6jlyfwz5pnnk")))

(define-public crate-cdxlib-0.1 (crate (name "cdxlib") (vers "0.1.2") (deps (list (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "044pm3ysg3wkzwa05bw21mqz8qb0r5f9ism448k2mm0k3kp2ngkv")))

(define-public crate-cdxlib-0.1 (crate (name "cdxlib") (vers "0.1.3") (deps (list (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0js0pxqaz4x9fxwmlqxi4r9w5zhgbic4if5i6zai7wgq4pmy073f")))

(define-public crate-cdxlib-0.1 (crate (name "cdxlib") (vers "0.1.5") (hash "0khwd97svybq9mx72x6yf7mh28pkq1amln9b7xbjfrms02jqak4n")))

