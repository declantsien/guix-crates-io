(define-module (crates-io cd l-) #:use-module (crates-io))

(define-public crate-cdl-list-rs-0.1 (crate (name "cdl-list-rs") (vers "0.1.0") (hash "0yhng19wmggrwh93rvskzznhsflymfxwsch95zgfpcmj1c5n366d")))

(define-public crate-cdl-list-rs-0.2 (crate (name "cdl-list-rs") (vers "0.2.0") (hash "0zzbvpjpmqvivdasjg3s3bv5f69n61waw0g05f7f92x8h618skgj")))

