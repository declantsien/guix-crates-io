(define-module (crates-io cd g_) #:use-module (crates-io))

(define-public crate-cdg_renderer-0.1 (crate (name "cdg_renderer") (vers "0.1.0") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (default-features #t) (kind 0)))) (hash "1z7jvpvd8pdyykjcb3ysz73hhs6m9h86pb27ybjyd3vn8zihhndl")))

(define-public crate-cdg_renderer-0.1 (crate (name "cdg_renderer") (vers "0.1.1") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (default-features #t) (kind 0)))) (hash "185ghmip33kwjgzvwm3j9n83fzv269h8lk0b375mhsi0sf7272k5")))

(define-public crate-cdg_renderer-0.2 (crate (name "cdg_renderer") (vers "0.2.0") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21") (default-features #t) (kind 0)))) (hash "1b0sl7jzqh17fdpqivx473wwhn7g4bn0rb3jjg089cryz30sgiv9")))

(define-public crate-cdg_renderer-0.3 (crate (name "cdg_renderer") (vers "0.3.0") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22") (default-features #t) (kind 0)))) (hash "03rmmgcdwgsbzjb069czpiyw86dncaqbrddigd5xjfiq46lr1989")))

(define-public crate-cdg_renderer-0.4 (crate (name "cdg_renderer") (vers "0.4.0") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22") (default-features #t) (kind 0)))) (hash "0jvyn4d42rc4sm462xgl4987588n91bxwhy5b1wvwaib6m967f5i")))

(define-public crate-cdg_renderer-0.5 (crate (name "cdg_renderer") (vers "0.5.0") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)))) (hash "1k2rrp007jzb6v148l3zzxaw8nf6jf6nvd6z55n7gzlwbmcwrha3")))

(define-public crate-cdg_renderer-0.6 (crate (name "cdg_renderer") (vers "0.6.0") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (kind 0)))) (hash "0c2pmfhzlbzwsfv5glsrhdiyfx2hxaa6ss8fd8phcn5i0wabxdcf")))

(define-public crate-cdg_renderer-0.7 (crate (name "cdg_renderer") (vers "0.7.0") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (kind 0)))) (hash "0qgy435xfkd0zwk3d72h91wdynh66jkcgx3p6a0hjpkh05g03knf")))

(define-public crate-cdg_renderer-0.7 (crate (name "cdg_renderer") (vers "0.7.1") (deps (list (crate-dep (name "cdg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (kind 0)))) (hash "15mgg4fp9gckgvcvaw7rf9aamms4b6mmjnimychj0rzq5r610lrp")))

