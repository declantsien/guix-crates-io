(define-module (crates-io cd iv) #:use-module (crates-io))

(define-public crate-cdivsufsort-1 (crate (name "cdivsufsort") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.47") (default-features #t) (kind 1)) (crate-dep (name "sacabase") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1yvrkpfsgx6imjyv10qcry668lrycc82i5jh4z5y2r2z31nvfr6z") (features (quote (("crosscheck"))))))

(define-public crate-cdivsufsort-2 (crate (name "cdivsufsort") (vers "2.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.47") (default-features #t) (kind 1)) (crate-dep (name "sacabase") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "134vyyznqaf24bksg29d9cid5fq0n1dsfbbn2sj9sq4pj40wxvzd") (features (quote (("crosscheck"))))))

