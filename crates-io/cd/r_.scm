(define-module (crates-io cd r_) #:use-module (crates-io))

(define-public crate-cdr_toolkit-0.1 (crate (name "cdr_toolkit") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "1k7i7vlhg0fhil9a9jnpi9wqjnaq6yv4f3al9l9v1gyf6ycjdilp")))

(define-public crate-cdr_toolkit-0.2 (crate (name "cdr_toolkit") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0n8fyrqzw46gm2cfk4i1kq77vpsfrhfphc6fzm3m4xb5jkyhjdad")))

