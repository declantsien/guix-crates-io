(define-module (crates-io cd ow) #:use-module (crates-io))

(define-public crate-cdown-0.1 (crate (name "cdown") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.15") (features (quote ("crossterm"))) (kind 0)))) (hash "1j575krh7b6f4z1g3m9ndj349dgm816v3ygv7swnkh5q9dyrrkhc")))

