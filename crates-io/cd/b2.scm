(define-module (crates-io cd b2) #:use-module (crates-io))

(define-public crate-cdb2-0.7 (crate (name "cdb2") (vers "0.7.0-alpha.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "memmap2") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0kx4by891342i5bxk4xl4jfdz0arla36vkk50vgcpn0khz7czb19")))

(define-public crate-cdb2-0.7 (crate (name "cdb2") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "memmap2") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)))) (hash "10jl5x2pmvmg0xkaspfjafybrml7n771zr95ygwf0q8x4cmlq5sx")))

(define-public crate-cdb2-0.7 (crate (name "cdb2") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "memmap2") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)))) (hash "0x578rqgf0b4fzpb8kyz2v94gy40qpzyw4331qkac46gmpfyjnxr") (rust-version "1.70")))

