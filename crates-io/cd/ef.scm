(define-module (crates-io cd ef) #:use-module (crates-io))

(define-public crate-cdefines-0.1 (crate (name "cdefines") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "0lyw366mxhmkv4nbq4bd4xsr2q3f84msgl3iv24d4kxg3jf6c03i")))

(define-public crate-cdefines-0.1 (crate (name "cdefines") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "0l05c7shyq3lrv2n87sr4sa8s6kyymf9w3fzm4glaqzc6k4jvhgn")))

(define-public crate-cdefines-0.1 (crate (name "cdefines") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "132rfj0zpj6kwxlrv464d7zzm34yk2j30z0921vns9wnd2sbxgfh")))

(define-public crate-cdefines-0.1 (crate (name "cdefines") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1yhzl74iq6jlpk1zpzv6g3d79lwrnpjijil8mqqfgxhfzfjyab6p")))

(define-public crate-cdefines-0.1 (crate (name "cdefines") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "0pd2a9qbl0ln9728yrnajnjvk1f5m0ck45d04b27lvv45w09479l")))

(define-public crate-cdefmt-parser-0.1 (crate (name "cdefmt-parser") (vers "0.1.0") (deps (list (crate-dep (name "gimli") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r0aqcv3h9q9xkxq5zcry5w4j6b627ynv1pws7kc6bp2087v0khk")))

