(define-module (crates-io cd yl) #:use-module (crates-io))

(define-public crate-cdylib-link-lines-0.1 (crate (name "cdylib-link-lines") (vers "0.1.0") (hash "141y4n9wqkqgmarwkfjg78jlxc93f28y4l67ivp79vkrcq7sbr2a")))

(define-public crate-cdylib-link-lines-0.1 (crate (name "cdylib-link-lines") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0d4zjzzlz22wfcr5z29fra8gryacy309f8r4vmg4r1qxccn30xk3")))

(define-public crate-cdylib-link-lines-0.1 (crate (name "cdylib-link-lines") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ay4di3prrvci37i9cbgy5chzznywx5p9bkf2w6b4x1yaqdwm0p4")))

(define-public crate-cdylib-link-lines-0.1 (crate (name "cdylib-link-lines") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "16agh8nlphy7xcyf43ra13v525xqcaxjp56s6ywlkqvvqffxnf8x")))

(define-public crate-cdylib-link-lines-0.1 (crate (name "cdylib-link-lines") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1wkhs6326pp545iamp4q205mryhn4xv35xnpa4g76mdllmzdn5x3")))

(define-public crate-cdylib-link-lines-0.1 (crate (name "cdylib-link-lines") (vers "0.1.5") (hash "1g10ididkzsxv3p9bw3y8iq7qprkmjqsn34w1gymmpxv13psp3nr")))

(define-public crate-cdylib-plugin-0.1 (crate (name "cdylib-plugin") (vers "0.1.0") (hash "1dl6fycxjcglpb2r82l4ih6s550l1j4fxn7sv6jksx1y35y27il4")))

(define-public crate-cdylib-plugin-0.1 (crate (name "cdylib-plugin") (vers "0.1.1") (hash "1j05kp1bh2cdhinqlhdl4kfzllbmzhy44s7f0q6i0zhgqlkmcrx9")))

(define-public crate-cdylib-plugin-0.1 (crate (name "cdylib-plugin") (vers "0.1.2") (hash "1jz3064hkvqjj38kggd97nyygkblqzvdyi749v3v4qm0sz4ca0gm")))

(define-public crate-cdylib-plugin-0.1 (crate (name "cdylib-plugin") (vers "0.1.3") (hash "1wfy0ygg5parj6bszz5n35crwb2m2mqn2dxvqflagjqi0hf849pa")))

(define-public crate-cdylib-plugin-0.1 (crate (name "cdylib-plugin") (vers "0.1.4") (hash "0pzg7k3p32byh5lxafrwkrgdg000nl82kdh0g51b66nals27gghp")))

(define-public crate-cdylib-plugin-0.1 (crate (name "cdylib-plugin") (vers "0.1.5") (hash "04lpjhx447piqhjqz8asz8z5bhz5gg0mgwsq0cis9qf9sbjqg29d")))

(define-public crate-cdylib-plugin-0.1 (crate (name "cdylib-plugin") (vers "0.1.6") (hash "1ywfy638cnylncqsb61sl73r8kvplwh6yz67w9zw5g8wziq70h2p")))

