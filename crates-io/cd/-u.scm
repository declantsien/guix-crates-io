(define-module (crates-io cd -u) #:use-module (crates-io))

(define-public crate-cd-utils-1 (crate (name "cd-utils") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "1f6002zylavawq6bs6ry3npwlb91jb4bb2451dcr9ckv46v01mh3")))

