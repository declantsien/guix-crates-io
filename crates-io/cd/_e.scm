(define-module (crates-io cd _e) #:use-module (crates-io))

(define-public crate-cd_env-0.1 (crate (name "cd_env") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1igsrfz773lbjplmdvpc1lky2936kw999za8l7d7plpvjazw7wxf")))

(define-public crate-cd_env-0.1 (crate (name "cd_env") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1kkrg25a9hlxy0x40mijki3a08waf2qmdhqps7aivi0mh30ira88")))

(define-public crate-cd_env-0.1 (crate (name "cd_env") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sfypcsz110v98svdvhkz16dzarqcid7cs9b4vha1jj7gy6ia8vc")))

(define-public crate-cd_env-0.1 (crate (name "cd_env") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18cmavyzx9p9brambbd8g7ps469fmnm29i4z2ym7yn9v833njgs6")))

(define-public crate-cd_env-0.2 (crate (name "cd_env") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bhjdy02hgg5km1ijifzqnsd26c9g57ipp6w1k4wijjivby2r73w")))

