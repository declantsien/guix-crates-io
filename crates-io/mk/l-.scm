(define-module (crates-io mk l-) #:use-module (crates-io))

(define-public crate-mkl-rs-build-0.1 (crate (name "mkl-rs-build") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 0)))) (hash "0n7vq1rlhvmg6750rlxzxq1d7hizp25vmd8bwgk542gymfgi0636")))

(define-public crate-mkl-rs-build-0.3 (crate (name "mkl-rs-build") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 0)))) (hash "1gg1z16ar41pxlgy3ziql92c1hy381g8fsznnjrf2hgcxiq2lbxm")))

(define-public crate-mkl-rs-build-0.3 (crate (name "mkl-rs-build") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 0)))) (hash "19z4ip3n87imxnqzqnkxrwdya26mfwvn68mw6xycjxf2w0hiyr0h")))

(define-public crate-mkl-rs-sys-0.1 (crate (name "mkl-rs-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.9.0-alpha.0") (default-features #t) (kind 2)))) (hash "0kp47l4fw85crd81h6nbmi1a52gsy7dvhnca8izp42frwgnh9gg5") (features (quote (("static-lp64-seq") ("static-lp64-iomp") ("static-lp64-gomp") ("static-ilp64-seq") ("static-ilp64-iomp") ("static-ilp64-gomp") ("sdl") ("dynamic-lp64-seq") ("dynamic-lp64-iomp") ("dynamic-lp64-gomp") ("dynamic-ilp64-seq") ("dynamic-ilp64-iomp") ("dynamic-ilp64-gomp") ("default" "sdl"))))))

(define-public crate-mkl-rs-sys-0.3 (crate (name "mkl-rs-sys") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "mkl-rs-build") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.9.0-alpha.0") (default-features #t) (kind 2)))) (hash "0sx6l8dyxhm4l3ghahc7a71gr9gxrrpvd8j3c26nlyzrb4xmr41d") (features (quote (("static-lp64-seq") ("static-lp64-iomp") ("static-lp64-gomp") ("static-ilp64-seq") ("static-ilp64-iomp") ("static-ilp64-gomp") ("sdl") ("no-link") ("dynamic-lp64-seq") ("dynamic-lp64-iomp") ("dynamic-lp64-gomp") ("dynamic-ilp64-seq") ("dynamic-ilp64-iomp") ("dynamic-ilp64-gomp") ("default" "sdl"))))))

(define-public crate-mkl-rs-sys-0.3 (crate (name "mkl-rs-sys") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "mkl-rs-build") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.9.0-alpha.0") (default-features #t) (kind 2)))) (hash "11hg75vhrkkv5qfmf8mb7f1m2xmwnbcfk5ib7in8z4avxcj0ha3h") (features (quote (("static-lp64-seq") ("static-lp64-iomp") ("static-lp64-gomp") ("static-ilp64-seq") ("static-ilp64-iomp") ("static-ilp64-gomp") ("sdl") ("no-link") ("dynamic-lp64-seq") ("dynamic-lp64-iomp") ("dynamic-lp64-gomp") ("dynamic-ilp64-seq") ("dynamic-ilp64-iomp") ("dynamic-ilp64-gomp") ("default" "sdl"))))))

(define-public crate-mkl-src-0.1 (crate (name "mkl-src") (vers "0.1.0") (hash "0b4nj88wk137ik7q7ahmfyyx1lf7f4fm6kk28irz3m228zhvlxy7")))

(define-public crate-mkl-sys-0.1 (crate (name "mkl-sys") (vers "0.1.0") (hash "06a9j30qnd5hgzpvdq81xga02g3hdag9lfcf2nrjb3plwsb00fq7")))

(define-public crate-mkl-tool-0.1 (crate (name "mkl-tool") (vers "0.1.0") (hash "0i50bwyq5720pwpi3xrm91pvs4iczldn4k650iq8cyycxpia6ibb")))

