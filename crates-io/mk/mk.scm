(define-module (crates-io mk mk) #:use-module (crates-io))

(define-public crate-mkmk-0.1 (crate (name "mkmk") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0gzlb96q5flp8frr0f90d3clqcixb3r1jla40g8v2vjm5yq4cy3z")))

(define-public crate-mkmk-0.1 (crate (name "mkmk") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "03pwmps4rqpzdfrlcn1gzl90jcl2gjd7ijhiz70iqqi1bcql8fiy")))

