(define-module (crates-io mk he) #:use-module (crates-io))

(define-public crate-mkheaders-0.1 (crate (name "mkheaders") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mktemp") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1yj7hi005hi0mqq7901kkpbbg9s57l67b71qwwrmf5nf3129ccvi")))

