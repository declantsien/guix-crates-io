(define-module (crates-io mk li) #:use-module (crates-io))

(define-public crate-mklink-0.1 (crate (name "mklink") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "junction") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (features (quote ("paw"))) (default-features #t) (kind 0)))) (hash "1c8zvy9i8bgdwnb55k5p78zcxcpks6xj28g0iyclpbw925abi1xs")))

(define-public crate-mklinker-0.1 (crate (name "mklinker") (vers "0.1.0") (hash "0hqrcfc5ccqaq1cscw4lvr7qbj2032xx8xg06069v8dz2ds39afd") (rust-version "1.78")))

(define-public crate-mklinker-0.1 (crate (name "mklinker") (vers "0.1.1") (hash "11kiy07n4licjg6qyx4rvb3z8h3hdcz4yqfm9pw6g8za4vvbrzkw") (rust-version "1.78")))

(define-public crate-mklinker-0.1 (crate (name "mklinker") (vers "0.1.2") (deps (list (crate-dep (name "aarch64_pgtable") (req "^0.1") (kind 0)))) (hash "0dy637pk78pcbyp43dqyw9fx5nksxflbdg27m5gl8rla4gkg7fa2") (rust-version "1.78")))

(define-public crate-mklinker-0.1 (crate (name "mklinker") (vers "0.1.3") (deps (list (crate-dep (name "aarch64_define") (req "^0.1") (kind 0)))) (hash "10k3ivljjj9xcwd0bly91qnpkw39s7w1k0wlvp1anr1jf9askd0y") (rust-version "1.78")))

(define-public crate-mklinker-0.1 (crate (name "mklinker") (vers "0.1.4") (deps (list (crate-dep (name "aarch64_define") (req "^0.1") (kind 0)))) (hash "1s9x5rsy2r3gzbi9f4iss1sqxddj30cs9jdkqn1k2l6wccfxdkkh") (rust-version "1.78")))

(define-public crate-mklinker-0.1 (crate (name "mklinker") (vers "0.1.5") (deps (list (crate-dep (name "aarch64_define") (req "^0.1") (kind 0)))) (hash "0hfh0ci9vpapvvqlqxqvnna6vq7aczw2w71iicamwv3gvazrx0cj") (rust-version "1.78")))

