(define-module (crates-io mk l_) #:use-module (crates-io))

(define-public crate-mkl_link-0.1 (crate (name "mkl_link") (vers "0.1.0") (hash "03qnjn9alsv4ds55y985397dmsvjarshdnn7acv56v7d82d9c70v") (features (quote (("tbb") ("openmp") ("default"))))))

(define-public crate-mkl_link-0.1 (crate (name "mkl_link") (vers "0.1.1") (hash "00gc6rfh8m8af7pjlknnalngy101f3ga7nz118yxlnpxn40i32sl") (features (quote (("tbb") ("openmp") ("default"))))))

(define-public crate-mkl_link-0.1 (crate (name "mkl_link") (vers "0.1.2") (deps (list (crate-dep (name "cblas_ffi") (req "^0.1") (default-features #t) (kind 2)))) (hash "10lppyg2h56dkr1di45smg2l3n22zhlivrb5xrqjy3561k9nag3c") (features (quote (("tbb") ("openmp") ("default"))))))

