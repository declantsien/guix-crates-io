(define-module (crates-io mk pi) #:use-module (crates-io))

(define-public crate-mkpir-0.1 (crate (name "mkpir") (vers "0.1.0") (hash "0iv23hywh7nrrlgfh2gjcydf22gz6yd34cz57jqixprw5ddmhy7x")))

(define-public crate-mkpir-0.0.1 (crate (name "mkpir") (vers "0.0.1") (hash "1ikzrrf7r549zdvidmxdl1qhz5d9jy7msybwwlsdnz31vprykyx5")))

(define-public crate-mkpir-0.0.2 (crate (name "mkpir") (vers "0.0.2") (hash "19sy84akhlxww40lgkrcwb659s0dp0lmnky3fkah76qdp0gn0pkb")))

