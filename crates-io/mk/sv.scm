(define-module (crates-io mk sv) #:use-module (crates-io))

(define-public crate-mksvg-0.1 (crate (name "mksvg") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "02z83j5xvsc6cg9imk9i57m0mrncxk82046k50jf4j2ii2982hjw")))

(define-public crate-mksvg-0.1 (crate (name "mksvg") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ngzki52hilpdwndb7m0l9z9n7k52gpcrjv34pac4gzs1ls9fqpz")))

(define-public crate-mksvg-0.1 (crate (name "mksvg") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b88qw2w8lsilqyxkgf88mzis75ip53wg88y2jx6bc67s3n142ip")))

(define-public crate-mksvg-0.1 (crate (name "mksvg") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zmv91pxi7k2nggqi1slmrjsxg3znvswxvmpl9hyvfp8c3b3sddr")))

(define-public crate-mksvg-0.2 (crate (name "mksvg") (vers "0.2.1") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wljkpmlnypcggmvn3hxh9j344n69ca71fs6mn4yhbh2akp9zar9")))

(define-public crate-mksvg-0.3 (crate (name "mksvg") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "1v1k7n958f6zxwybiw43y5iw96fj9p72frhc35q4h7jlranz19py")))

