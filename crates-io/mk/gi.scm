(define-module (crates-io mk gi) #:use-module (crates-io))

(define-public crate-mkgitignore-0.2 (crate (name "mkgitignore") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1slndkx4yx127aznxcaj21mbddqyalvsvkqy43rb7p1wc4wms67y")))

