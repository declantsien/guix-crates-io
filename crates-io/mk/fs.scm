(define-module (crates-io mk fs) #:use-module (crates-io))

(define-public crate-mkfs-btrfs-rs-0.1 (crate (name "mkfs-btrfs-rs") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0jkvdjba02p4648ciqa71kww3jqw37sb95bc1p15glr9ajj2qw20")))

(define-public crate-mkfs-btrfs-rs-0.1 (crate (name "mkfs-btrfs-rs") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "00y06j271wpdlznrdl7wx9dzpdk7ry8s0sp2cbrh66ay7q1c3p60")))

(define-public crate-mkfs-ext2-0.1 (crate (name "mkfs-ext2") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "mkfs-filesystem") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "010mvbsfk5b5f81a974bg6q9x1ibhh69wbcz70axcqq4xk8q1m4b")))

(define-public crate-mkfs-filesystem-0.1 (crate (name "mkfs-filesystem") (vers "0.1.0") (hash "10076fs7v1ljp1h2923p30z283bbz31r4qfhbnrf1rffjm2xzcv3")))

