(define-module (crates-io mk -f) #:use-module (crates-io))

(define-public crate-mk-findandreplace-0.1 (crate (name "mk-findandreplace") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.14") (default-features #t) (kind 0)))) (hash "1rbzjqaasp8ivdrsy1npxiwzlziiq7g22aljdn7r323d4jw70jvq")))

