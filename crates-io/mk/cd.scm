(define-module (crates-io mk cd) #:use-module (crates-io))

(define-public crate-mkcd-0.1 (crate (name "mkcd") (vers "0.1.0") (hash "11g37l2cbh3vbjqp5iwgd71qdgvbyy5grzvvggrzkk9d0ib7z3yc")))

(define-public crate-mkcd-0.5 (crate (name "mkcd") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "17ssr2kbfxacfsql68vplpxfyr88d9bspigvll2756klw3lvgq8k")))

(define-public crate-mkcd-0.6 (crate (name "mkcd") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bn721nyim8182dk39rzgq50w6g2753aj06ljsdgyafrqac4m8kg")))

(define-public crate-mkcd-0.6 (crate (name "mkcd") (vers "0.6.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0isc9fw9v9zpr5pqb756s91flhgx3ng3bs1sxlylbwcgbxwhg99m")))

(define-public crate-mkcd-0.7 (crate (name "mkcd") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "08h2295gmjlqxkh2cj8rnb060n46bvj7ipqf6j9la2bmxja8xvnl")))

(define-public crate-mkcd-0.8 (crate (name "mkcd") (vers "0.8.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zvz95w3fs97fkx0xi84k72ik7k0965s0wvnwk4w8mskxak3ipwy")))

(define-public crate-mkcd-0.8 (crate (name "mkcd") (vers "0.8.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m26wlr9a64l7j9sqikq5z7192avhknpd7s59byww72wrkgi51k3")))

(define-public crate-mkcd-0.8 (crate (name "mkcd") (vers "0.8.6") (hash "1g3x1wbq2i5fwhzgla2lcsv9y8a6nxriip4vhlxpw5xckxcs6jj3")))

