(define-module (crates-io mk is) #:use-module (crates-io))

(define-public crate-mkisofs-rs-0.1 (crate (name "mkisofs-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ni78jl00jsyf553x6wcv07ynn85x7mcqplbkx78hgb5ar84frhf")))

(define-public crate-mkisofs-rs-0.1 (crate (name "mkisofs-rs") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fg55xsa3zqnvman1ybna3frjlp8ci3a02xddq9gz8jvfaq6z3px")))

(define-public crate-mkisofs-rs-0.1 (crate (name "mkisofs-rs") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w06ip3sxipxzgy2v6mnmh84bvh6pa18h9i31c0bgi2qp1kgdsb1")))

(define-public crate-mkisofs-rs-0.2 (crate (name "mkisofs-rs") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bqqkk106dj93p04j3c5ybfy53nrcly79kyqxp8vhnm355r9r4ql")))

