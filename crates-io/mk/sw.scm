(define-module (crates-io mk sw) #:use-module (crates-io))

(define-public crate-mkswap-0.1 (crate (name "mkswap") (vers "0.1.0") (deps (list (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "page_size") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "14yrqpqh0vdp5wx9yx11r6gvw1rc5ah3na16m8vvipgmiw00h7c8")))

(define-public crate-mkswap-0.1 (crate (name "mkswap") (vers "0.1.1") (deps (list (crate-dep (name "hex-slice") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "page_size") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "11kig9d12y4bnwyaipy5y0b1m9gr6r16c6zd7arhlma665p1c6qa")))

