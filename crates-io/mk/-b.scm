(define-module (crates-io mk -b) #:use-module (crates-io))

(define-public crate-mk-bin-0.0.1 (crate (name "mk-bin") (vers "0.0.1") (hash "1bg79rqa2sll21az7a1cyr57mgkj4q9w3p9hrnm1i7m0wmn4l3p8")))

(define-public crate-mk-bin-0.0.3 (crate (name "mk-bin") (vers "0.0.3") (hash "05b0aycc4qxbsmhdrjnl4s6qcc0ryxz24w5c90zmkgvd8n2wlgkm")))

(define-public crate-mk-buddy-0.0.0 (crate (name "mk-buddy") (vers "0.0.0") (hash "0iir2vp3l02mnkjdb382r9yxz7r6sscsa5l202pbh501lv8y9drh")))

(define-public crate-mk-bump-0.0.0 (crate (name "mk-bump") (vers "0.0.0") (hash "0wj485jir4yaaswff8fg2gw45qsv6q54yxygd8hqrjvpqndzww1f")))

