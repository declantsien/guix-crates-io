(define-module (crates-io mk up) #:use-module (crates-io))

(define-public crate-mkups-0.1 (crate (name "mkups") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.1") (features (quote ("mman"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)))) (hash "1frlj15im9227xm9s8w7ks0xz446mxijpk8rsn684362qwl8p4mv")))

