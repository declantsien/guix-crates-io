(define-module (crates-io mk #{66}#) #:use-module (crates-io))

(define-public crate-mk66f18-0.1 (crate (name "mk66f18") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1df36yai5nvraxxms9zdc8n3n0h8xaw6ic1n0ig7p02ik6yidjq8") (features (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mk66f18-0.2 (crate (name "mk66f18") (vers "0.2.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13v7dyg0l0q391v8zrnc8k2jby372hxff0zdrk6y4gz6y6hnm2zk") (features (quote (("rt" "cortex-m-rt/device"))))))

