(define-module (crates-io mk dt) #:use-module (crates-io))

(define-public crate-mkdtemp-0.1 (crate (name "mkdtemp") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0d1di672cpg9566af3zl6h2cbyh43b80d920c6laq1ww84377clk")))

(define-public crate-mkdtemp-0.2 (crate (name "mkdtemp") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0acykhlhc0s7pw151n6l4q3jgddbr57sc2iwk0a80yilq8aazyf1")))

