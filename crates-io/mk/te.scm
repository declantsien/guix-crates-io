(define-module (crates-io mk te) #:use-module (crates-io))

(define-public crate-mktemp-0.1 (crate (name "mktemp") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1qygqcim3h7is0x5n790qvf6kiwi51pxcmpkazk2j99lfvk36p39")))

(define-public crate-mktemp-0.1 (crate (name "mktemp") (vers "0.1.1") (deps (list (crate-dep (name "uuid") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0sghzykck6y49gppvsq0lbnnv8pvf39hamcwavv1568lj6mmgfs5")))

(define-public crate-mktemp-0.1 (crate (name "mktemp") (vers "0.1.2") (deps (list (crate-dep (name "uuid") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1sca3v38mlvsvhiyyizjyfpx4ddd7cz1mh99s58n7wj4blxx6cvk")))

(define-public crate-mktemp-0.2 (crate (name "mktemp") (vers "0.2.1") (deps (list (crate-dep (name "uuid") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1r020904kv6sg4mk7yicwpw6hccgpwzy5v8rk9kmi2kdlv1y7qdf")))

(define-public crate-mktemp-0.3 (crate (name "mktemp") (vers "0.3.1") (deps (list (crate-dep (name "uuid") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0sfddmp9dl1qfx525gw64s8pshd1kczm98n27ngl6rgdkvmiq03p")))

(define-public crate-mktemp-0.4 (crate (name "mktemp") (vers "0.4.0") (deps (list (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "09vvgk86h3c3d0zn31y4v0krm9swm74nzn02p0ipk5swdisgrx7d")))

(define-public crate-mktemp-0.4 (crate (name "mktemp") (vers "0.4.1") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "18hd84l7k7i1mbjylagwg1miczh78cjrs55r0kggqcc28ivfcpcp")))

(define-public crate-mktemp-0.5 (crate (name "mktemp") (vers "0.5.0") (deps (list (crate-dep (name "uuid") (req "~1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "02l2yz0jkgyv0s0010z3lwx5p4qd95788kvqkz9igdvvvms1zp2b")))

(define-public crate-mktemp-0.5 (crate (name "mktemp") (vers "0.5.1") (deps (list (crate-dep (name "uuid") (req "~1.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1n3b0jppm5x71jy36gnik1nh19knci67h9n29b2gxbq1rpxxizk9")))

(define-public crate-mktemp-rs-0.1 (crate (name "mktemp-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)))) (hash "0wqsld601ng363pn58r87fccvp50lfy5hjy7nxl98l0hbvxp9smh")))

(define-public crate-mktemp-rs-0.2 (crate (name "mktemp-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)))) (hash "0x3vanxlbb9y7f9rxkknja86dnax2x64ia6clnzmf3v080amy54i")))

