(define-module (crates-io mk _c) #:use-module (crates-io))

(define-public crate-mk_console-0.1 (crate (name "mk_console") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "1nl2yqxjcnfh0ih1gghwvbmd4srdar8v18pbhqddkqrhhyj7snl1")))

(define-public crate-mk_console-0.1 (crate (name "mk_console") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "1frk4cjxpc7djjnpxc5irmx37sa93fk78qj4w0b8jpsrvn03asry")))

(define-public crate-mk_console-0.1 (crate (name "mk_console") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "0myfflmd2jrv3g9r19867a4lc1gy4701im5d1p4m60whswpag33f")))

