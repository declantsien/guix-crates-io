(define-module (crates-io mk en) #:use-module (crates-io))

(define-public crate-mkentity-0.1 (crate (name "mkentity") (vers "0.1.0") (hash "1hkzrjxrcgnhblz2imxv2xjy2pngzp6hvr67lkp7d466x04hsgx6") (features (quote (("zou") ("postgres") ("mongo"))))))

(define-public crate-mkenv-0.1 (crate (name "mkenv") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 2)))) (hash "1nzjwxswbh0f0ln44mnv8nzlzxd75pjsw9hyzh4j8vwlwc632rgx")))

(define-public crate-mkenv-0.1 (crate (name "mkenv") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 2)))) (hash "0grky7ilxymdnivhygxjv2jcf0v2i3prljsnsvypnkwlydszya37")))

(define-public crate-mkenv-0.1 (crate (name "mkenv") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 2)))) (hash "0q0lzn3shw2r7nanln324c16phcvs29r8grygl2ffcm5zbmrcwyl")))

(define-public crate-mkenv-0.1 (crate (name "mkenv") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 2)))) (hash "1laba6nh9ld50vjs15c7hiakl5src0cmqkzvhrzdyhwv1pjszfpz")))

(define-public crate-mkenv-0.1 (crate (name "mkenv") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 2)))) (hash "180vr6spnad5p5yx30xm8zzk447sl2zwaj3kqy6h2q1cn7701bkl")))

(define-public crate-mkenv-0.1 (crate (name "mkenv") (vers "0.1.5") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 2)))) (hash "1ff5zrkdx133n34y334sx8flw2pxq8n8xbxs7xg367qwdy9ixzcc")))

(define-public crate-mkenv-0.1 (crate (name "mkenv") (vers "0.1.6") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 2)))) (hash "0a8kfxzkjaggb7b5slbgyakd45j4nrhazyw2286anydif61dn9gp")))

