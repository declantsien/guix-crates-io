(define-module (crates-io mk tr) #:use-module (crates-io))

(define-public crate-mktree-0.1 (crate (name "mktree") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "06m2x2759jasdwk9pkvw954y3a1ycqhdhl1jj9l8wlfv1hdzcwq5") (yanked #t)))

(define-public crate-mktree-0.1 (crate (name "mktree") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "00cvxlxs78wx854gkwwcgmmj8r6drr4ky6yxmawxmmjaa0mpiisi") (yanked #t)))

(define-public crate-mktree-0.1 (crate (name "mktree") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "0rnnf1xvn5n9z42gin5rsasbhafbxr5zn9lg3352r9gh9d0kdl20") (yanked #t)))

(define-public crate-mktree-0.1 (crate (name "mktree") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "0c8z41fc09bbqk68zlvvrkv5c4x4hkgc2v826zjfxn10xyy1xks6") (yanked #t)))

(define-public crate-mktree-0.1 (crate (name "mktree") (vers "0.1.4") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "01nxjiz7yrad65pha2nzwcl6cf9vqn8mjcf181cvy4jjlyc7q9s3") (yanked #t)))

(define-public crate-mktree-0.2 (crate (name "mktree") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "04cwg8a0dgi0g6vyz0ls9400l9118anmvxf10i0ddc3crpc64s25") (yanked #t)))

(define-public crate-mktree-0.3 (crate (name "mktree") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "12p6j0w9awcq6ay5ypwqr8ym1mzah793i1fns1w1dfs02vj837fx") (yanked #t)))

(define-public crate-mktree-0.4 (crate (name "mktree") (vers "0.4.0") (deps (list (crate-dep (name "aquamarine") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "170vsszmxwiplihc4r31f5gwxb4ym01c90wxj2y0zcxhmb1j9bbm") (features (quote (("zou") ("postgres") ("mongo"))))))

(define-public crate-mktree-0.5 (crate (name "mktree") (vers "0.5.0") (deps (list (crate-dep (name "aquamarine") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1cmkfdl5ja95qmhk1qfy45b0810vibn29k8ycphdb3y9jr8wl7js") (features (quote (("zou") ("postgres") ("mongo"))))))

(define-public crate-mktree-0.6 (crate (name "mktree") (vers "0.6.0") (deps (list (crate-dep (name "aquamarine") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "mkentity") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y99ydj73x9spcjxhlyzdkyjkd2003y5n2vm2ybrdw85wym21ibn") (features (quote (("zou" "mkentity/zou") ("postgres" "mkentity/postgres") ("mongo" "mkentity/mongo"))))))

(define-public crate-mktree-0.6 (crate (name "mktree") (vers "0.6.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "mkentity") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1f1gyk7lari81am9h7wc67g12992s7ry2wgw648jagbw6fsp9q1y") (features (quote (("zou" "mkentity/zou") ("postgres" "mkentity/postgres") ("mongo" "mkentity/mongo"))))))

