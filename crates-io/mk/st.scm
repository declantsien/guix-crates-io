(define-module (crates-io mk st) #:use-module (crates-io))

(define-public crate-mkstemp-0.1 (crate (name "mkstemp") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "0.*") (default-features #t) (kind 0)))) (hash "0avfgfbhhach6mfrch3zfcyjln7kh8kshy9rjxzn15x3is4f7j03") (yanked #t)))

(define-public crate-mkstemp-0.2 (crate (name "mkstemp") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "0.*") (default-features #t) (kind 0)))) (hash "17ql7cp156hnw5b96bq67gazzav261ykyrvd4i5crjw4vfbqf7vg") (yanked #t)))

(define-public crate-mkstemp-rs-0.0.1 (crate (name "mkstemp-rs") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "188zvydfvmc5pj8ifjp2ylfwv1zsg30n9aynyc972is3i0dpbgi4")))

(define-public crate-mkstemp-rs-0.0.2 (crate (name "mkstemp-rs") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fkq4b0jxbd7a9nxcvdm9j3rm24k2xzifjzv0485zkf4dhkan1hg")))

(define-public crate-mkstemp-rs-1 (crate (name "mkstemp-rs") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kk0g83lxv7d0aahyg5csi6kp7mnhrzpakmmgs30kirh19yxl1mb")))

