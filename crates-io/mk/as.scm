(define-module (crates-io mk as) #:use-module (crates-io))

(define-public crate-mkascii-0.1 (crate (name "mkascii") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "termsize") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1iwhc369qkaci4rr6l69bs9hx0bf48x5ghm6v9p35sd4wrx4ncvj")))

