(define-module (crates-io mk pa) #:use-module (crates-io))

(define-public crate-mkpages-0.1 (crate (name "mkpages") (vers "0.1.0") (deps (list (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mkqi8bh39b7ivvzngw214c0kjmlv665xzim62rxgrya58lzbvyw") (yanked #t)))

(define-public crate-mkpass-0.1 (crate (name "mkpass") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0kra92inghv7sc13zrqlir8m0gyr7szjxcx8klzvmpixqqf91sz4")))

(define-public crate-mkpass-0.1 (crate (name "mkpass") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16s3h2x1s46vgx3y1dl5xb08r2xmf5v19blgxi62d4dq868fcb0g")))

(define-public crate-mkpasswd-0.1 (crate (name "mkpasswd") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand_os") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1ci718fvz8wkxmmv5bg9304vzbv0hhl2g0csn1fizmpyijwnr3b7")))

(define-public crate-mkpasswd-0.2 (crate (name "mkpasswd") (vers "0.2.0") (deps (list (crate-dep (name "rand_core") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand_os") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "15zk0sda6zrvgbvb0p4ir4ldw1zin2fb198kn987wci2w1dvr2lj")))

(define-public crate-mkpasswd-0.3 (crate (name "mkpasswd") (vers "0.3.0") (deps (list (crate-dep (name "rand_core") (req "^0.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "18jcfcxj267dj37zz5b5cq3fjcn386d528kjc0cjll91lkmwq8a8")))

