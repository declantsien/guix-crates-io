(define-module (crates-io mk ht) #:use-module (crates-io))

(define-public crate-mkhtml-2 (crate (name "mkhtml") (vers "2.0.0") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "12hky6krki03kcd3laki8k3c5bc3xz4k951przf64y0xwjiwjiq8")))

(define-public crate-mkhtml-3 (crate (name "mkhtml") (vers "3.0.0") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0wy36kb2xlzhv6yq3n3nhkib7242im5jcgfcxzqfb6qhv6ah2lk2")))

(define-public crate-mkhtml-3 (crate (name "mkhtml") (vers "3.1.0") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1dnr4vh1h9r4x9knlhkc793snzjzciwbgm3w2irzcg6c2pvr2v13")))

(define-public crate-mkhtml-3 (crate (name "mkhtml") (vers "3.2.0") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "123k24h5p2cz34s9pvjafvwdby7w973jijjxw6h8i08dxlwxgizc")))

(define-public crate-mkhtml-3 (crate (name "mkhtml") (vers "3.3.0") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0bmh5z75s6kj780yg4951gyyy3qcdp3s6hzsrzghggj18iprzq0z")))

(define-public crate-mkhtml-3 (crate (name "mkhtml") (vers "3.3.1") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1p8chf36dvfhphlw5wi2hcr2q3x84cm3ghqpqcimvdyk0yzhab8b")))

(define-public crate-mkhtml-3 (crate (name "mkhtml") (vers "3.3.2") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "17793jv3cnqbnxahkjml8d5n19a15p4ylhmx9rfcv2fg55zygcn8")))

(define-public crate-mkhtml-3 (crate (name "mkhtml") (vers "3.4.0") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1lqxy8jsyfl7rfzlz82dkpn4kpcd1z6rvvyiv9j1fjcgam8v2srh")))

