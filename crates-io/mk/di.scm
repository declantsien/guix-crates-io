(define-module (crates-io mk di) #:use-module (crates-io))

(define-public crate-mkdirp-0.1 (crate (name "mkdirp") (vers "0.1.0") (hash "01yajs9mclysq4nzwi19pw635xfzsqz061ga6xw7s677ya8bm5lp")))

(define-public crate-mkdirp-1 (crate (name "mkdirp") (vers "1.0.0") (hash "1w1p232cj8fwg2479b4k8dfx9r57ashsw8kqvk98dcr99kk1skl6")))

