(define-module (crates-io mk #{8d}#) #:use-module (crates-io))

(define-public crate-mk8dx_lounge_tag_extractor-0.0.1 (crate (name "mk8dx_lounge_tag_extractor") (vers "0.0.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.11.0") (default-features #t) (kind 0)))) (hash "0576kgj75ik0xr508shr5lfcyd4jp1zd0zwbyqhn23fb0wpkd5x3")))

(define-public crate-mk8dx_lounge_tag_extractor-0.0.2 (crate (name "mk8dx_lounge_tag_extractor") (vers "0.0.2") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.11.0") (default-features #t) (kind 0)))) (hash "1z9n164pi3pgj4gbv4ipa4sigbs28pb3x7xwmmjfmqb1vsdnbl13")))

