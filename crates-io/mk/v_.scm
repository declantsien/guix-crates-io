(define-module (crates-io mk v_) #:use-module (crates-io))

(define-public crate-mkv_chain-0.2 (crate (name "mkv_chain") (vers "0.2.1") (hash "0ylv6gi1ldvl76061x8pknw847a5sw94z4ag8fah8lnqzahr3886")))

(define-public crate-mkv_chain-0.2 (crate (name "mkv_chain") (vers "0.2.2") (hash "0y2k7khk7pqwvf1jlylvn1zn04mjgar7dvy7bbi12x49121nqzfn")))

(define-public crate-mkv_chain-0.3 (crate (name "mkv_chain") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0q4yxdizqlxs27yr7s7ijkz2mrk0wn88hh6343i9i7nxl2vqsr73") (features (quote (("default"))))))

(define-public crate-mkv_chain-0.3 (crate (name "mkv_chain") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "1sz5lpbhdqjh21rnrz60izqlbvipa1hp8lqgzi39gd8cfar8s42v") (features (quote (("default"))))))

