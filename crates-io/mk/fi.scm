(define-module (crates-io mk fi) #:use-module (crates-io))

(define-public crate-mkfile-0.2 (crate (name "mkfile") (vers "0.2.1") (hash "1fr7aaf1scxnr1lwf8xm3a3q27b7frbvb51w4zdp0hs1ahh9wp3g")))

(define-public crate-mkfile-0.3 (crate (name "mkfile") (vers "0.3.0") (hash "0k8vd30aj2c5gg7brzxjjkprrdfvwnv9ncdyw3390x0gcf75xd8s")))

