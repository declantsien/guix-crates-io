(define-module (crates-io mk mo) #:use-module (crates-io))

(define-public crate-mkmod-0.0.1 (crate (name "mkmod") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 0)))) (hash "17byp5xqfv2hbv1viz2ccyd05sndvpr63v11ymfcdbsfdaa2k56m")))

(define-public crate-mkmod-0.0.2 (crate (name "mkmod") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 0)))) (hash "12xbrnikdqkc80ghkrmwdvpmy9mac2r25gwx1v6rzvfg2f9r6zdf")))

