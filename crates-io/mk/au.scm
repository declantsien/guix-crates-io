(define-module (crates-io mk au) #:use-module (crates-io))

(define-public crate-mkaudio-0.1 (crate (name "mkaudio") (vers "0.1.0") (hash "1k1b3322acd8w7h07150iv1lywz9w73z507s2ixmff2zs1frnc1i") (yanked #t)))

(define-public crate-mkaudio-0.1 (crate (name "mkaudio") (vers "0.1.1") (hash "0p5mkc3x9nyhsgda6kyi4qg7bajixc25f8ib1fyi0z1h28bxwy6f")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "10ljjjsb57xz6xx8imjdv85lw81hc5dw9dv32r6djvy5q7cjk26h")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1js3m84fdcry1brfh3lq10xml98szjn7zasp1379dzqqdmjqvp3n")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.2") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1wha3y3y0qi5arjc6xgyiv46waafc5rm3ilvwsk8pd8xnzjw19ki")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.3") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "053ihvjlzv0mg22v38q1am5j5fdx6340xfqn4h2j1xcq8w533k8z")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.4") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1021147lx8wdzl3fc4wj0x4d668va5ps4l25jrl6qddqsxc6nl44")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.5") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1qqn55qcmigbvi0gbbz2nsz42dg4437mixc7wvpn6hannnd99md9")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.6") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "199agviw2zijn3igwgyifmrs22k3hiv8h9jgsr0cr73xa5fg0v7c")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.7") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "15rp2h72b428r5r11hx64mrdr687l5ghga6dlcqfx9j4p9rh5lnk")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.8") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1xa4a04ar9kdwla9j1qpzp1blcpzw8il1lyk16dfabzis394cbpv")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.9") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1m7shsmm79052nsb11pqb32jl401nxckllmf7pl56d5xsp5rmzsy")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.10") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "15c4rk6k70w6a060xs0pyx0mhhj9wwq3a0v8p8f3rrhfbm1mgc2r")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.11") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1r31sczch0ym6z6nw7pcr3m28qs4bqmzfbyhy949yx63wv3jbrd7")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.12") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1rjan5s20hz0spngxjix7ccp5hal313kj4ambwpy7ff5r55hxcsj")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.13") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1pv43cy19xppd0j3bpqn6fkbvqb46msjs5m8dxblpys6j2jgckns")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.14") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "19yhsbdg8wsrzaj8a83cl2vk1kjrigk0z83af44x662jma8x7890")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.15") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1qwc0w4wj8z3rla65gdr8p8kaw4ydih5k51hvfzxhas057nvl6n7")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.16") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "08hr33i9afdnjij9r38z8cfnpnlyw8nyzxvrxah6sg08r3irgakn")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.17") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1x89qsjlvwmsxlnvihyifnykpsa0vqnadh3xs19v5vvqzmhdbmfl")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.18") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "09mlq35bhdw3vy16dnyhz4fsdhqi9gbvny767z0674q469hgxxnj")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.19") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "06gw3msdvcg4z0wk1sgybxny2glr44517q1a9770gby16pka1wq7")))

(define-public crate-mkaudiolibrary-0.1 (crate (name "mkaudiolibrary") (vers "0.1.20") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1k1iphrpc7bw07zqdwai816ffkjg0b8i2ajwn45ay1lykinswfv9")))

(define-public crate-mkaudiolibrary-0.2 (crate (name "mkaudiolibrary") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "no_denormals") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1khvdmiqxjybbanwam7gr07z5msi5mw5a3iygq074xbpfxwafp3p")))

