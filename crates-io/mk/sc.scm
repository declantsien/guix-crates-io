(define-module (crates-io mk sc) #:use-module (crates-io))

(define-public crate-mkscratch-1 (crate (name "mkscratch") (vers "1.0.0") (hash "1f4lrbzid8pyk93vailnjh7833qv8cmxg8ny5b7j3skk5k9k429v")))

(define-public crate-mkscratch-1 (crate (name "mkscratch") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1n9j91fbrigdlyrc2kznvz4zx7v6zpiwmvn95pwzabjw9y33xph8")))

