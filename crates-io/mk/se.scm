(define-module (crates-io mk se) #:use-module (crates-io))

(define-public crate-mkserial-0.0.0 (crate (name "mkserial") (vers "0.0.0") (deps (list (crate-dep (name "num-bigint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "02ar77cbglg3xm2l0xas65f4f36qar5i8banj7sx3cr04xnqq0if")))

(define-public crate-mkserial-0.0.1 (crate (name "mkserial") (vers "0.0.1") (deps (list (crate-dep (name "num-bigint") (req "^0.3") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "121c9ck4rip45yz72k28ldljhqkwn5kzc88fq5lis7nm4fhqwvaw")))

(define-public crate-mkserial-0.0.2 (crate (name "mkserial") (vers "0.0.2") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1xjcdah2s1wvvhczab3fxk7nisbnk21nqb2bx2w73i4zki95nm5i")))

(define-public crate-mkserial-1 (crate (name "mkserial") (vers "1.0.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1fhbcghd6kyrdsgirhv7r85k5iwrp06qybqwmr1dfl2k2ldsjz2j")))

(define-public crate-mkserial-1 (crate (name "mkserial") (vers "1.0.1") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (features (quote ("rand"))) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (kind 0)))) (hash "05lbp9xkxrapsbnhz5hvy0f1z8wbdfxg4k1zmwgya8bfvpwygn8q")))

(define-public crate-mkserial-1 (crate (name "mkserial") (vers "1.0.2") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (features (quote ("rand"))) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (kind 0)))) (hash "1nmnm7nismxw1fnl8j3m77laicchq153byg7dhzysqmqswykb9ah")))

(define-public crate-mkserial-1 (crate (name "mkserial") (vers "1.0.3") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (features (quote ("rand"))) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (kind 0)))) (hash "0749jj56bds4m9hj6vprghfkv3yb9rkxjxnfnxv7n5iq6xv0vr9d")))

