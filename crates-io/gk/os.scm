(define-module (crates-io gk os) #:use-module (crates-io))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.1") (deps (list (crate-dep (name "content_inspector") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "1vw3a5qh2p20dj9syxk3p2vqqqk4yqswlkjd4gl1iid6siqrm360")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.2") (deps (list (crate-dep (name "content_inspector") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "0lmyvhcmhpvqjfxz9wxsvhilb342ssc1k3rm19yfz8ah2p4s17l8")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.3") (deps (list (crate-dep (name "content_inspector") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "1zf94q8nx5vijs0aw220vky06f6rcq7ybvjmijs2hvbskv9p2jvk")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.4") (deps (list (crate-dep (name "content_inspector") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "187zbgckv1gb8hf6fcivzyk2crj3y0ajmcgjmrwkbygybd8b821i")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.5") (deps (list (crate-dep (name "content_inspector") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "05qiih9ddr7anf00c0jwc672dwm8gw835afvp474v2ispccxmfch")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.6") (deps (list (crate-dep (name "content_inspector") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "0q0y1f1h0cjrfhs25v8vj19id3f3191z1w7lvm48razpgbc0y1kc")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.7") (deps (list (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1blaj8ikf6ws3zyg5vg7gnmsf9bgbsjznzpwrskcwqyi7shmgb0h")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.8") (deps (list (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "021qhwqpsm34bk15q2syg5bdfxda2546761g5czbivvp8p7vzmzh")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.9") (deps (list (crate-dep (name "env_logger") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0zy57r4czcscyhcmhvwn8q6jfwf4646lavghakb50nwpl7dhdq4j")))

(define-public crate-gkosgrep-0.1 (crate (name "gkosgrep") (vers "0.1.10") (deps (list (crate-dep (name "env_logger") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "1.*") (default-features #t) (kind 0)))) (hash "12yakkax2jv91rixyybbzxw1qi4bisxvl5b478r8dx5afn5j0zlz")))

