(define-module (crates-io gk ls) #:use-module (crates-io))

(define-public crate-gkls-rs-0.1 (crate (name "gkls-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "itertools") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0gqcgj8mnki8fsappg9p1avmyd7zsjkbmf4fyd0jr918m0mcycwm") (features (quote (("test_cbinding") ("examples" "plotters") ("default"))))))

