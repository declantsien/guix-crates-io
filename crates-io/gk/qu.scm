(define-module (crates-io gk qu) #:use-module (crates-io))

(define-public crate-gkquad-0.0.1 (crate (name "gkquad") (vers "0.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "smallbox") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1k5d33qqqis00pjqmg9ki7a6jlsf1qp7yinzb4xgbm2610k6rzp7") (features (quote (("std" "smallbox/std") ("single") ("default" "std" "single"))))))

(define-public crate-gkquad-0.0.2 (crate (name "gkquad") (vers "0.0.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1sxg2vambhvd919dy0as1f8h5v8axjyyk2k5z4xpxgdxcys2ly5g") (features (quote (("std") ("single") ("simd") ("default" "std" "simd" "single"))))))

(define-public crate-gkquad-0.0.3 (crate (name "gkquad") (vers "0.0.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0lw9kc5amwacxsxwkz0aad2wgg8fxrizjqjfvnv4m8wbdh96mn1b") (features (quote (("std") ("simd") ("double") ("default" "std" "simd")))) (yanked #t)))

(define-public crate-gkquad-0.0.4 (crate (name "gkquad") (vers "0.0.4") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "13m4q4liacdliqvr5rwq6b2pkvzkl4qgawsk9fz5ald20lhr23xr") (features (quote (("std") ("simd") ("double") ("default" "std" "simd"))))))

