(define-module (crates-io ad ja) #:use-module (crates-io))

(define-public crate-adjacency-list-0.0.0 (crate (name "adjacency-list") (vers "0.0.0") (deps (list (crate-dep (name "graph-types") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0w909ari86sbwi1nr1qidqc4h7p1wzcqihjzs06c984gx3h81rz9") (features (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

(define-public crate-adjacency-list-0.0.1 (crate (name "adjacency-list") (vers "0.0.1") (deps (list (crate-dep (name "graph-types") (req "0.0.*") (default-features #t) (kind 0)))) (hash "0icjs9dx2h0jyfs20wv80qrcfwa3ajkk99qsp50y8m0p1ybr9bf6") (features (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

(define-public crate-adjacency-list-0.0.2 (crate (name "adjacency-list") (vers "0.0.2") (deps (list (crate-dep (name "graph-types") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "09jb0dvz55izvdpc1aqwfmml3bbkxxrga15vja9l45kcyj9vkajm") (features (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

(define-public crate-adjacency-matrix-0.0.0 (crate (name "adjacency-matrix") (vers "0.0.0") (deps (list (crate-dep (name "graph-types") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0973vff90a1mfz5xvz4qpp9wsjv59xmfiqcyicmasf3a19qzgxg5") (features (quote (("default"))))))

(define-public crate-adjacency-matrix-0.0.1 (crate (name "adjacency-matrix") (vers "0.0.1") (deps (list (crate-dep (name "graph-types") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "09w4kljaa8arcyqkamp75vkn09wbyzpgc5jdxwbwkpsc7nfw1wls") (features (quote (("wolfram" "graph-types/wolfram_wxf") ("default"))))))

(define-public crate-adjacent-pair-iterator-0.1 (crate (name "adjacent-pair-iterator") (vers "0.1.0") (hash "0xi48krajymcn0p71ymrswqk8cvyikc5aasn67ciwvcfszsdk5ak")))

(define-public crate-adjacent-pair-iterator-0.1 (crate (name "adjacent-pair-iterator") (vers "0.1.1") (hash "1pi24zlrphzn321ahhvyf3zv2f9ik4yclfyzrrcs6qhv779jgymz")))

(define-public crate-adjacent-pair-iterator-0.1 (crate (name "adjacent-pair-iterator") (vers "0.1.2") (hash "10pd54phnddlkp72903djlvs716mhwmrg60zvs8jdrhm1l7ar6f1")))

(define-public crate-adjacent-pair-iterator-1 (crate (name "adjacent-pair-iterator") (vers "1.0.0") (hash "10r8fm2kr4pr116kfcsaym37r3z7mlvzs1x6a8lv68s8dwccv4r0") (rust-version "1.31")))

(define-public crate-adjacent_lines-0.1 (crate (name "adjacent_lines") (vers "0.1.0") (deps (list (crate-dep (name "iterslide") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "17kbq42q58c30fkjhcadsdp8w9ry5p84gd230p9s1ij95k0kb17w")))

