(define-module (crates-io ad va) #:use-module (crates-io))

(define-public crate-advance-0.1 (crate (name "advance") (vers "0.1.0") (hash "0h136vglzmx676laqwvhmvml8d627lfxnsnwcwvmn334lmjyp90p") (rust-version "1.36.0")))

(define-public crate-advanced-pid-0.1 (crate (name "advanced-pid") (vers "0.1.0") (hash "0wnj6x9hggzqp34kk1dlqbnxjcbs2gj0mzvqa9n1bi20kxzhnpg4") (features (quote (("std") ("f64") ("f32") ("default" "std" "f32"))))))

(define-public crate-advanced-pid-0.1 (crate (name "advanced-pid") (vers "0.1.1") (hash "0j0yraj05w67bmrz7x3zr9aak4mfbkasl0r2xn08n8kdi9hp6x0z") (features (quote (("std") ("f64") ("f32") ("default" "std" "f32"))))))

(define-public crate-advanced-pid-0.1 (crate (name "advanced-pid") (vers "0.1.2") (hash "032f8dhvsg8831n0p3wk955h2bxzx1nxm3zl4ji81849r34mmj87") (features (quote (("std") ("f64") ("f32") ("default" "std" "f32"))))))

(define-public crate-advanced-pid-0.2 (crate (name "advanced-pid") (vers "0.2.0") (hash "1d2rwhivy547vlf8szg5jjk8d641s3ygfddqq99f4x1kc96qixv9") (features (quote (("std") ("f64") ("default" "std"))))))

(define-public crate-advanced-pid-0.2 (crate (name "advanced-pid") (vers "0.2.1") (hash "1783q8c5mj5srvv8v426143v42k38n5f8xp0pvnrdqq35hkwhxgg") (features (quote (("std") ("f64") ("default" "std"))))))

(define-public crate-advanced-pid-0.2 (crate (name "advanced-pid") (vers "0.2.2") (hash "0nvcsmas36lkjhzhbdpbc8qk2p33l15a5gjdndxd42fk31x2ldhz") (features (quote (("std") ("f64") ("default" "std"))))))

(define-public crate-advanced_collections-0.1 (crate (name "advanced_collections") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1m1c8j8f26dx9rs258lmq22szhhb1l7npy7hqachmbbyl6vgca28")))

(define-public crate-advancedresearch-agent_safety_layers-0.1 (crate (name "advancedresearch-agent_safety_layers") (vers "0.1.0") (hash "0cm0l6dz3y94pblj06pv8prcalyx16aijdwh9rkahx2lhra77a48")))

(define-public crate-advancedresearch-asi_core0-0.1 (crate (name "advancedresearch-asi_core0") (vers "0.1.0") (hash "17vrh07r825084v15z3dq2mmfbcdrz1bcrl58p63wqcrzzpfs43w")))

(define-public crate-advancedresearch-asi_core0-0.2 (crate (name "advancedresearch-asi_core0") (vers "0.2.0") (hash "12aj3w2r3w9xidddqcj79n8nyns6z77jdf1fya4p6wgsfy8vfqw9")))

(define-public crate-advancedresearch-error_predictive_learning-0.1 (crate (name "advancedresearch-error_predictive_learning") (vers "0.1.0") (hash "0nm72smi0jm8j06wjsmqvhcwadh32s45avz5brxha7hfnj94l2ky")))

(define-public crate-advancedresearch-graph_builder-0.1 (crate (name "advancedresearch-graph_builder") (vers "0.1.0") (hash "1wcw0avw3jrz7rfk2dzwlnsc1ix7jv307fdczjmbkfqlipb7ng5z")))

(define-public crate-advancedresearch-higher_order_core-0.1 (crate (name "advancedresearch-higher_order_core") (vers "0.1.0") (hash "0n7xap5acq7317ksladyc7hrjmbbbq8dpg970l1nyvqq7sr40yd8")))

(define-public crate-advancedresearch-higher_order_core-0.1 (crate (name "advancedresearch-higher_order_core") (vers "0.1.1") (hash "18h0z7g3c44qlclcm9cx8j1nv5pd5w7g5r6142qdvpp59dpkq2cq")))

(define-public crate-advancedresearch-higher_order_core-0.2 (crate (name "advancedresearch-higher_order_core") (vers "0.2.0") (hash "180a3w4k36psbhhch05aayx6xpgidsm2hij0imiydrh7mq8p2rl3")))

(define-public crate-advancedresearch-higher_order_core-0.3 (crate (name "advancedresearch-higher_order_core") (vers "0.3.0") (hash "1rjhxlqfy40bfcfn2dic219pfrsmh7hc0rn70gs0djdq7sa012mv")))

(define-public crate-advancedresearch-higher_order_point-0.1 (crate (name "advancedresearch-higher_order_point") (vers "0.1.0") (deps (list (crate-dep (name "advancedresearch-higher_order_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "camera_controllers") (req "^0.31.0") (default-features #t) (kind 2)) (crate-dep (name "piston") (req "^0.49.0") (default-features #t) (kind 2)) (crate-dep (name "piston2d-graphics") (req "^0.34.0") (default-features #t) (kind 2)) (crate-dep (name "piston2d-opengl_graphics") (req "^0.68.0") (default-features #t) (kind 2)) (crate-dep (name "pistoncore-sdl2_window") (req "^0.63.0") (default-features #t) (kind 2)) (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "082crbyjnwfppd1212v3ixs0mdi6mlkrk5j1s6h0na5hjwp7xspc")))

(define-public crate-advancedresearch-higher_order_point-0.2 (crate (name "advancedresearch-higher_order_point") (vers "0.2.0") (deps (list (crate-dep (name "advancedresearch-higher_order_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "camera_controllers") (req "^0.31.0") (default-features #t) (kind 2)) (crate-dep (name "piston") (req "^0.49.0") (default-features #t) (kind 2)) (crate-dep (name "piston2d-graphics") (req "^0.34.0") (default-features #t) (kind 2)) (crate-dep (name "piston2d-opengl_graphics") (req "^0.68.0") (default-features #t) (kind 2)) (crate-dep (name "pistoncore-sdl2_window") (req "^0.63.0") (default-features #t) (kind 2)) (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0gil7663zm85x7662cwsm02pnz7j1cxgycc4yq6r108xlh9d1ylp")))

(define-public crate-advancedresearch-higher_order_point-0.3 (crate (name "advancedresearch-higher_order_point") (vers "0.3.0") (deps (list (crate-dep (name "advancedresearch-higher_order_core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "camera_controllers") (req "^0.31.0") (default-features #t) (kind 2)) (crate-dep (name "piston") (req "^0.49.0") (default-features #t) (kind 2)) (crate-dep (name "piston2d-graphics") (req "^0.34.0") (default-features #t) (kind 2)) (crate-dep (name "piston2d-opengl_graphics") (req "^0.68.0") (default-features #t) (kind 2)) (crate-dep (name "pistoncore-sdl2_window") (req "^0.63.0") (default-features #t) (kind 2)) (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1cgzsiys3xpyq9dq17hr4185axgrhad9xxvxrn3gmyrp9icjvi79")))

(define-public crate-advancedresearch-hypo-0.1 (crate (name "advancedresearch-hypo") (vers "0.1.0") (hash "0klpkjpjlrx4anxbqj43cqrph3vb8kzmjjsxnnqc8hspd786gyd2")))

(define-public crate-advancedresearch-hypo-0.1 (crate (name "advancedresearch-hypo") (vers "0.1.1") (hash "10v9llp27dcvjadvzhxnkk4jvc5n7aky2g01crzkdj3dj42g3p2l")))

(define-public crate-advancedresearch-hypo-0.2 (crate (name "advancedresearch-hypo") (vers "0.2.0") (hash "0nbxb0xvsd4dkh0x77slq1f1dliyy2rvci03cryv2fwpbvb8bz8c")))

(define-public crate-advancedresearch-max_tree-0.1 (crate (name "advancedresearch-max_tree") (vers "0.1.0") (deps (list (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "19cf33vkn0pl4zqvn154gzgjala6adciqw68q5r9pzyfgz2xlzzb")))

(define-public crate-advancedresearch-nano_ecs-0.1 (crate (name "advancedresearch-nano_ecs") (vers "0.1.0") (hash "1jmjz9xqlfzsgw8sa4crk2cc7i0chlv31mbszr7vvyiillsc70b5")))

(define-public crate-advancedresearch-nano_ecs-0.2 (crate (name "advancedresearch-nano_ecs") (vers "0.2.0") (hash "1ngkwwjzza5d8pfphwzh45rc5z4x8ig7w4hfklgb9v898zh7indf")))

(define-public crate-advancedresearch-nano_ecs-0.3 (crate (name "advancedresearch-nano_ecs") (vers "0.3.0") (hash "0cizg2fcaj6jv56agz46a3s31fb04g6p40fxwapvpg3pihd4fclk")))

(define-public crate-advancedresearch-nano_ecs-0.4 (crate (name "advancedresearch-nano_ecs") (vers "0.4.0") (hash "0c7gdz4p54kzfixwm1n5mzw49bmq5vrpb2jas3cxfmxqlvvqbxd7")))

(define-public crate-advancedresearch-nano_ecs-0.4 (crate (name "advancedresearch-nano_ecs") (vers "0.4.1") (hash "0pfsrq81d5qsyhsmm5551mzkwsvfkmaz02qc7w2dbdwz28pl6ibb")))

(define-public crate-advancedresearch-nano_ecs-0.5 (crate (name "advancedresearch-nano_ecs") (vers "0.5.0") (hash "0slrngm82ay8kazziq310k5hcpbr8d88qfp9qarcr2jqpngc1fc6")))

(define-public crate-advancedresearch-nano_ecs-0.5 (crate (name "advancedresearch-nano_ecs") (vers "0.5.1") (hash "0by864ncy1sdxjscp2hcb89xns8gbyrbnlchr70m64v6arkl3gsk")))

(define-public crate-advancedresearch-nano_ecs-0.5 (crate (name "advancedresearch-nano_ecs") (vers "0.5.2") (hash "15xx65h93lv5jxy5qqq51a19cxl6ifil4xblx0na6vngnq0sba56")))

(define-public crate-advancedresearch-nano_ecs-0.6 (crate (name "advancedresearch-nano_ecs") (vers "0.6.0") (hash "08q4cfphn2ad6sd10w94gqclqbgs90xgi9i9z1w59m1wg8ncaa28")))

(define-public crate-advancedresearch-nano_ecs-0.7 (crate (name "advancedresearch-nano_ecs") (vers "0.7.0") (hash "1rz6rvk31h0d56f1q28n09pj5v2nywll4gnf4vyg2clcl5l949d8")))

(define-public crate-advancedresearch-nano_ecs-0.8 (crate (name "advancedresearch-nano_ecs") (vers "0.8.0") (hash "0klmq2ggg3bv73cgpkc22hhc9xg4wg5kww18ddnf1jxclm61za4p")))

(define-public crate-advancedresearch-nano_ecs-0.9 (crate (name "advancedresearch-nano_ecs") (vers "0.9.0") (hash "1sainhva8jfb546731s25fx855394vvb8f2x9nc82ifznaz2m5yr")))

(define-public crate-advancedresearch-path_iter-0.1 (crate (name "advancedresearch-path_iter") (vers "0.1.0") (hash "07a06m8yp3ca6hp0sw2nms3h5xsag1cmn53k1nrmr8bxfwsi9rpy")))

(define-public crate-advancedresearch-path_iter-0.1 (crate (name "advancedresearch-path_iter") (vers "0.1.1") (hash "0vihajrfwkwf8briz890a0lazm03pwwrf5w5l09rp93kly71v988")))

(define-public crate-advancedresearch-path_iter-0.2 (crate (name "advancedresearch-path_iter") (vers "0.2.0") (hash "1pqn7ya859r8jhppi6a8r4i34nn41d7ixj13h1f090w2f7q5qxk7")))

(define-public crate-advancedresearch-path_iter-0.3 (crate (name "advancedresearch-path_iter") (vers "0.3.0") (hash "1xbmrg7qfjpcikg6jkphw0jb3kn1lvdvr7xc32qciyvy8m6njyn1")))

(define-public crate-advancedresearch-path_iter-0.4 (crate (name "advancedresearch-path_iter") (vers "0.4.0") (hash "0d5bi6p50l0pm6d08nsvi0hgxq0q5dnv08kmmzwgm4r5c5ddxhv0")))

(define-public crate-advancedresearch-path_iter-0.5 (crate (name "advancedresearch-path_iter") (vers "0.5.0") (hash "1aign4r0al71saw46cmfaajz2kn86gz1d69gfj2hcnd7cd2dy001")))

(define-public crate-advancedresearch-rigid_body-0.1 (crate (name "advancedresearch-rigid_body") (vers "0.1.0") (deps (list (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16hq498qy4j39q04riav5s6jbcy0cwda4j05pvrlhl3562y60q7p")))

(define-public crate-advancedresearch-tree_mem_sort-0.1 (crate (name "advancedresearch-tree_mem_sort") (vers "0.1.0") (hash "1m0jhqzxsxjkjdj4smsq0kbqi1w7kblvp3xcp67hg0c4dzmsnaw5")))

(define-public crate-advancedresearch-tree_mem_sort-0.1 (crate (name "advancedresearch-tree_mem_sort") (vers "0.1.1") (hash "0k1n6apm3c1c27n8yhz46f5rxmhwxfxy0qaklxdsdndpgg7pz1cf")))

(define-public crate-advancedresearch-tree_mem_sort-0.2 (crate (name "advancedresearch-tree_mem_sort") (vers "0.2.0") (hash "153033j4q8pw16byvlmggqx1az8alqigv7lgpp8lp3si330k5c5a")))

(define-public crate-advancedresearch-utility_programming-0.1 (crate (name "advancedresearch-utility_programming") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0ppyl2bdjscg1r64wp7l77vdnsbj5040ii5y2dijrdfz8qprqdnl")))

(define-public crate-advancedresearch-utility_programming-0.2 (crate (name "advancedresearch-utility_programming") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1znz50rpf57glra87bl130jvshr2pa9yq4k99x9f1lff6vp547rs")))

(define-public crate-advancedresearch-utility_programming-0.3 (crate (name "advancedresearch-utility_programming") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "15dlwh390n18mllhhp00wdbgbyv151v4wg3l6z4mmghphb3svh6n")))

(define-public crate-advanedresearch-trinoise-0.1 (crate (name "advanedresearch-trinoise") (vers "0.1.0") (hash "127bx6f2zlnbibcaxvbw8akbz12minnl7xcbagh578sy1whwxckn")))

(define-public crate-advanedresearch-trinoise-0.1 (crate (name "advanedresearch-trinoise") (vers "0.1.1") (hash "15sn2m5yvpddx1h4sbkzxhvcsnf10r2zbcd2r4pqlrydi5klprld")))

(define-public crate-advantage-0.0.0 (crate (name "advantage") (vers "0.0.0") (hash "18alw96l8qnpql3cwdsk2sikc5vjcjrx2alrnq61svfyl41rb0zf")))

(define-public crate-advantage-0.1 (crate (name "advantage") (vers "0.1.0") (deps (list (crate-dep (name "cbindgen") (req "^0.9") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)))) (hash "0zdrq37kaw8rd8gwkic2ydmcpszbr8060sg7g2xvfwvx714zkyn3") (features (quote (("ffi" "cbindgen") ("default"))))))

(define-public crate-advantage_derive-0.0.0 (crate (name "advantage_derive") (vers "0.0.0") (hash "06cjynm1ryp9xxnyhikn0fr4s62ly904j2lfbd7bz0rb3znwk15k")))

(define-public crate-advantage_macros-0.0.0 (crate (name "advantage_macros") (vers "0.0.0") (hash "1dsbal30wzp8ibilq26dxhiajrs246h780v5cdf8jmll32iqfdnx")))

(define-public crate-advapi32-sys-0.0.1 (crate (name "advapi32-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1226ahypf1i1f4g6c8mhkqz2n7zdl98081ng27hc8mvigcim0gr1")))

(define-public crate-advapi32-sys-0.0.2 (crate (name "advapi32-sys") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "154pprycib7b0llypk2s9l1fy4c5z290vgil7llh3f1pcav8s89a")))

(define-public crate-advapi32-sys-0.0.3 (crate (name "advapi32-sys") (vers "0.0.3") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "023gplmy0r90jf62s2lf5mg2kg8wsiicp3l1dy8yqhgs6qsv0cnm")))

(define-public crate-advapi32-sys-0.0.4 (crate (name "advapi32-sys") (vers "0.0.4") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "18zhq6yysv83469yabs4d7wjrnvxk8h70wyf5gsn3d7mhl1c8qr9")))

(define-public crate-advapi32-sys-0.1 (crate (name "advapi32-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "16j0x6hqahl3vkh42062hapy89kar01nd4f8w6iflfblpzcpcrbf")))

(define-public crate-advapi32-sys-0.1 (crate (name "advapi32-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "05i966nsh5fawysmlzdfk125di3lqh1q779l99m0ibfba19jn9xl")))

(define-public crate-advapi32-sys-0.1 (crate (name "advapi32-sys") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "1mvbqq7v9ibbn2hxizhyf4qq9ghbi6fyw8h21ir8drb750rr4z1h")))

(define-public crate-advapi32-sys-0.2 (crate (name "advapi32-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "16largvlrd1800vvdchml0ngnszjlnpqm01rcz5hm7di1h48hrg0")))

