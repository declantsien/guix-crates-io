(define-module (crates-io ad #{56}#) #:use-module (crates-io))

(define-public crate-ad5627-0.1 (crate (name "ad5627") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1izlc62zf468qbn4gwbbszmhx1030k0fc7zhy8naxr4da0f2wnd0")))

(define-public crate-ad5668-0.1 (crate (name "ad5668") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "1m9nxm7lh2047rnfiv1kqix72jm18ig60dhr533cgs5y7fl8a440")))

(define-public crate-ad5668-0.1 (crate (name "ad5668") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "12ypmpm449fbl6bxp42fwhy2jab3ai0rzm16agk6vig9p9h33zri")))

(define-public crate-ad5668-0.1 (crate (name "ad5668") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "0f8867fdi0jnq62kslmi6v3qqkfzzq5gjnrws9yxpq1d3hg9yw65")))

(define-public crate-ad5668-0.1 (crate (name "ad5668") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "03p46330np0p4k146b81ic78mzj6q8ryxnf1b21lcdkvhby7k3xm")))

