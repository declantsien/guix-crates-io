(define-module (crates-io ad s7) #:use-module (crates-io))

(define-public crate-ads7828-0.1 (crate (name "ads7828") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "058ca0igpaidpkzqwhlgakzknw0m2gdphm7j980fv910l9axhxrs")))

(define-public crate-ads7828-0.1 (crate (name "ads7828") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0fzm2yyfzv29hc58jmxr29yrgvkb23dp15ff95kdr4y3b9fmkadv")))

(define-public crate-ads7924-0.1 (crate (name "ads7924") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "13hmdvhfgy2znyp7w1xkfxg3cac0h0zk0hcfbxghajpgrmbljpqq")))

(define-public crate-ads7953-rs-0.1 (crate (name "ads7953-rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "077jns0n7ak9yl998gdvsqqn4wg2bgxnh4b7izazcxbsq4lwar81")))

