(define-module (crates-io ad dy) #:use-module (crates-io))

(define-public crate-addy-0.0.1 (crate (name "addy") (vers "0.0.1") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "0l8af57w4k445np5y46dw3w4lnxbji9fkx6zmh2g9kgvvhbvdckh")))

(define-public crate-addy-0.1 (crate (name "addy") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "0v6lnwhjjl676dfxxbnlkp3xfb75z5avk3q176jq4czf8by1kgcl")))

(define-public crate-addy-0.1 (crate (name "addy") (vers "0.1.1") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "0303d8bj6wy0wp8l1ax9mzjz6qn785igvpmmvg7zwpnp0y30arxr")))

