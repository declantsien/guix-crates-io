(define-module (crates-io ad ju) #:use-module (crates-io))

(define-public crate-adjusting_clock-0.1 (crate (name "adjusting_clock") (vers "0.1.0") (hash "0xsfcny6a59b1q75sl58703rf7j6w4z4slw8qm5y8jsmy2p2k2bb")))

(define-public crate-adjustp-0.1 (crate (name "adjustp") (vers "0.1.0") (hash "1jpdrcxmszsdmyvnm9wfcbf7msy1gwr2jr6k6s985kkhs58j3mr2")))

(define-public crate-adjustp-0.1 (crate (name "adjustp") (vers "0.1.1") (hash "1sbgh2a9kw7vmlghgagm1bw46y8x86cwbr4rzzhgyf2d3zpa725k")))

(define-public crate-adjustp-0.1 (crate (name "adjustp") (vers "0.1.2") (hash "1snpgl9ydngf9sh9b8r51hsha28la7xnk6xiwwrgm81v5cgr3nsv")))

(define-public crate-adjustp-0.1 (crate (name "adjustp") (vers "0.1.3") (hash "0s2ij54i4l8wrw78qpivb1zaifya50gs636yvvs7i36ffv3sgzss")))

(define-public crate-adjustp-0.1 (crate (name "adjustp") (vers "0.1.4") (hash "0l58dlkmdzy2rg6lj48hhds46sjh4lxzp3mzg4cj85cfqnzp258m")))

(define-public crate-adjustp-0.1 (crate (name "adjustp") (vers "0.1.5") (hash "1x1irayq83rc3d8yahagxr05piw5k9di4lbrb2pq45izjkvpfls2")))

