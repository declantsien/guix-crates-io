(define-module (crates-io ad c-) #:use-module (crates-io))

(define-public crate-adc-interpolator-0.1 (crate (name "adc-interpolator") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1imr2qplxwf8zssksp33165rf71gdqgl223yan3b9vqj85xv05x0")))

(define-public crate-adc-interpolator-0.2 (crate (name "adc-interpolator") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "03wnpqkgw57cr5p70na2s6k1wz7p9j37zh5cshyd0a4j1q496nc5")))

(define-public crate-adc-mcp3008-0.1 (crate (name "adc-mcp3008") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1pvkxrkvla18m94ajn275fw93csiinddyj7g00yw7mgqrf1gny1i")))

(define-public crate-adc-mcp3008-0.1 (crate (name "adc-mcp3008") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qylavlcy9qxiqjinqhwcc40zdf19mbglllcjbddfbc8sad4vd8g")))

