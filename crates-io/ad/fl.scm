(define-module (crates-io ad fl) #:use-module (crates-io))

(define-public crate-adflib-0.0.1 (crate (name "adflib") (vers "0.0.1") (hash "0q1pjiyb4xgc1dw1iqv8ds2rh9r3k0l0g4hacvnf81m5sb9b5n0j")))

(define-public crate-adflib-0.0.2 (crate (name "adflib") (vers "0.0.2") (hash "0g8l3bldw34fz0di7iw12ahsbx4iwix0sx32hl6iycv2dyl33nhp")))

(define-public crate-adflib-0.0.3 (crate (name "adflib") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0v85ra8cc40i1z3qwnfwvh0vw4sd0wmsrf29yfh70crn4k6gr659")))

