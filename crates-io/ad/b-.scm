(define-module (crates-io ad b-) #:use-module (crates-io))

(define-public crate-adb-rs-0.1 (crate (name "adb-rs") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty-hex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0w7524s0qyn2kihzi4y2w3i90sks10hvpwxnlwxzlr7g4rflkccn")))

(define-public crate-adb-rs-0.1 (crate (name "adb-rs") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty-hex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zhfvkipwj3vpjkwlzbfhfkk7c3rww15y7pmw6mg5sx5qi436241")))

(define-public crate-adb-rs-0.1 (crate (name "adb-rs") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty-hex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "079b155cm25gl2a4d2qdaxmycirsdyzrgcykqvpm0g890fzm3gnh")))

(define-public crate-adb-rust-0.1 (crate (name "adb-rust") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cn9wq8k9f0y1nr3k1zyk8fmdyvyyak7a3v4cx900by028dq45kg")))

(define-public crate-adb-rust-0.1 (crate (name "adb-rust") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15yhwckmxdm150cj0k1f4rpp0wnh0y623d77gvj14jjwbip1nxdl")))

(define-public crate-adb-rust-0.1 (crate (name "adb-rust") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "016k9gj65g652h085sy81mj92rdqlpqij5q8zpxc9bf7rx5q4r16")))

(define-public crate-adb-rust-0.1 (crate (name "adb-rust") (vers "0.1.3") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1apx2gzbfwn3vbv4qxcl4dwrk28skk7hywk2ssph7kmp7jcn8jj4")))

(define-public crate-adb-rust-0.1 (crate (name "adb-rust") (vers "0.1.4") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k0xjqkxg8c6j529c39cr40hv3ka0cfxlyjhqzzdc5shxlcy12r0")))

(define-public crate-adb-rust-0.1 (crate (name "adb-rust") (vers "0.1.5") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k9ra701dmpr9ns1i3xvylymjrvqaa35krkk0di5cgbnj0rn5arq")))

(define-public crate-adb-rust-0.1 (crate (name "adb-rust") (vers "0.1.6") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k5mysd51snn05lxa45q4riwlvcd2gjlaphjr9mdf519llyn6bq0")))

(define-public crate-adb-rust-0.2 (crate (name "adb-rust") (vers "0.2.0") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16jb5lagxya9g94sblvc6mx8514ry3jp6cx9qncbm79p77s4qd39")))

(define-public crate-adb-rust-0.2 (crate (name "adb-rust") (vers "0.2.1") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0slfhrqjlk185m3j4rcx8j223fdzj6h06i13n1nhp6p6laxrir79")))

(define-public crate-adb-rust-0.2 (crate (name "adb-rust") (vers "0.2.2") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yabn3sc8bj2ilbcisxcllwmgvkxjg735d968ljg3grn545v58nc")))

(define-public crate-adb-rust-0.2 (crate (name "adb-rust") (vers "0.2.3") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17smsiii5frncir8dgkn9zbhb6z3m0wil03rjfb7ppyhfznzrrfi")))

(define-public crate-adb-rust-0.2 (crate (name "adb-rust") (vers "0.2.4") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pfgv73lm3wwcwjk1xz33pd2rbadjy8g888gvqkbl8qb8p2g98y8") (yanked #t)))

(define-public crate-adb-utils-0.1 (crate (name "adb-utils") (vers "0.1.0") (hash "1pk75c3nw7i5p8b0s5cxp9gw9wsccf452zhbydx9gjlr8l7nwm6z")))

(define-public crate-adb-utils-0.2 (crate (name "adb-utils") (vers "0.2.0") (hash "0dspgjdxg1yd6slswd4q572rjkizskxj0ssi38cinxs699kvkw0v")))

(define-public crate-adb-utils-0.3 (crate (name "adb-utils") (vers "0.3.0") (hash "0r3jy66lii3pvmmachk10yikbvgd7wn74kphwxr4w12yz5ngwizh")))

(define-public crate-adb-utils-0.4 (crate (name "adb-utils") (vers "0.4.0") (hash "17kvnxv8zbdjcv9bigvcf0kdhw98z8qlj2m3ylvnw4qrj8v0wp4j")))

(define-public crate-adb-utils-0.5 (crate (name "adb-utils") (vers "0.5.0") (hash "0si5xpwyw2dlj0cl5wjzbi81gv8x2yfcpfr093yi79mzrdqszizj")))

(define-public crate-adb-utils-0.6 (crate (name "adb-utils") (vers "0.6.0") (hash "1w6nm2sm5j0kckl4c3d2w34bri20klhbzhdy8q0i8ssm3blg6c6s")))

