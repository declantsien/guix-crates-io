(define-module (crates-io ad em) #:use-module (crates-io))

(define-public crate-ademas-0.1 (crate (name "ademas") (vers "0.1.0") (hash "1n8y58g16wmr5q2vi6qfl8a17cg1rrg9cdpy2q9fcimsn4khdv3s")))

(define-public crate-ademas-tech-0.1 (crate (name "ademas-tech") (vers "0.1.0") (hash "1fl97gc2a1vy96vkj4pcwg1p968b634067zzn109qh0jaxah3byj")))

(define-public crate-ademastech-0.1 (crate (name "ademastech") (vers "0.1.0") (hash "1fq9n5abjf1advwzp3ak7sj0iwajvknc21vshxx3lmvwa61545h8")))

