(define-module (crates-io ad ob) #:use-module (crates-io))

(define-public crate-adobe-cmap-parser-0.1 (crate (name "adobe-cmap-parser") (vers "0.1.0") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "122fagywilyf7vj0jaclglyvipajzrcrpcllc72a0ca81cn9kdc3")))

(define-public crate-adobe-cmap-parser-0.1 (crate (name "adobe-cmap-parser") (vers "0.1.1") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "17pm6vzigshqa8sys33n4vawfjrjk8536m4yl4kq1pjg2z3br6ja")))

(define-public crate-adobe-cmap-parser-0.2 (crate (name "adobe-cmap-parser") (vers "0.2.0") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "03cn8xhjxahi1p45vsqfz11axmb0dygl1xz0h74x4cg2ifh4b6x9")))

(define-public crate-adobe-cmap-parser-0.3 (crate (name "adobe-cmap-parser") (vers "0.3.0") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0lxzlzs8j6kzl2xzss7l8di0hmqagcp5g7w0aggkc5wwzysk0dwf")))

(define-public crate-adobe-cmap-parser-0.3 (crate (name "adobe-cmap-parser") (vers "0.3.1") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "06n9g9p2jzarrxn0629bf4275hjqh2wpbq8c8s62qcfd75ivnq30")))

(define-public crate-adobe-cmap-parser-0.3 (crate (name "adobe-cmap-parser") (vers "0.3.2") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "11z9swpr15m1zhgcpw4zfc19z5sx7bvzk5jby2bz7vqqd4izlwph")))

(define-public crate-adobe-cmap-parser-0.3 (crate (name "adobe-cmap-parser") (vers "0.3.3") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1nlxh6j3cbklimmnqg8q7y4d90yyr5msklygasbfrj38dl3gban3")))

(define-public crate-adobe-cmap-parser-0.3 (crate (name "adobe-cmap-parser") (vers "0.3.4") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "097bin6kslbnvjmh9kab4inqkf668p65l3in7w7c3d5xsx9igwxs")))

(define-public crate-adobe-cmap-parser-0.3 (crate (name "adobe-cmap-parser") (vers "0.3.5") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1g54570yb12jlhw1whyy9z5mw8pwfbbmpw125h8bi2jh2zbajgbx")))

(define-public crate-adobe-cmap-parser-0.4 (crate (name "adobe-cmap-parser") (vers "0.4.0") (deps (list (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1mfq1f1cryayhnhn289303dnl9fphxcr5v2xc2hp1p3x61x966i6")))

(define-public crate-adobe-swatch-exchange-1 (crate (name "adobe-swatch-exchange") (vers "1.0.0") (hash "062idb7z0w90x42gap28ik9kqjmixyl75w2rf8l1h5k26gpgjfli") (rust-version "1.65")))

(define-public crate-adobe-swatch-exchange-2 (crate (name "adobe-swatch-exchange") (vers "2.0.0") (hash "18rjpic0rxrjlfqmwx00aq3byg220plhlk48c7k683m579b8dld1") (rust-version "1.65")))

(define-public crate-adobe-swatch-exchange-2 (crate (name "adobe-swatch-exchange") (vers "2.0.1") (hash "0s3y09qafg3szr6d9ss81ysj6jygk6liy4iaq03gy8y96pffik7p") (rust-version "1.65")))

