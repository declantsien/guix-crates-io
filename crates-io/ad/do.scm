(define-module (crates-io ad do) #:use-module (crates-io))

(define-public crate-addon_distributor-0.1 (crate (name "addon_distributor") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1pmaf30wspcr6cgzd3w0mk2f5j1hnmcnz75v4z2wcsy026h8d5kj")))

