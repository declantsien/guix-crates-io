(define-module (crates-io ad eq) #:use-module (crates-io))

(define-public crate-adequate-0.1 (crate (name "adequate") (vers "0.1.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "strfmt") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0mwdf4z2n8sqn400316dg65ihdknrdbl0iy19aadnw5n7g5n8r10")))

(define-public crate-adequate-0.1 (crate (name "adequate") (vers "0.1.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1dzxycy4ha3cgd91zcc73svzbzjmd53fdidkd08dfhqqf1zlsbdp")))

(define-public crate-adequate-0.1 (crate (name "adequate") (vers "0.1.2") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1njbwhp2z2d42g0jivrx0c93wxrhrlq13qmvqhkvv0iprsmf6bnq")))

