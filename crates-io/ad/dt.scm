(define-module (crates-io ad dt) #:use-module (crates-io))

(define-public crate-addtwo-0.1 (crate (name "addtwo") (vers "0.1.0") (hash "044kc3pxbznfp0538p0n90jriq9lz0src7zazgw9hgs86q0skizp")))

(define-public crate-addtwo_macro-0.1 (crate (name "addtwo_macro") (vers "0.1.0") (hash "1n8mayr7yr63j968v1har5isgyyajv72naixyv7c0cinwa6zqjpi")))

(define-public crate-addtwo_macro-0.1 (crate (name "addtwo_macro") (vers "0.1.1") (hash "01gcpijq6pqipmg1kn9j4zcwp57yi0si84gvkgyinzck6afmkvjh")))

