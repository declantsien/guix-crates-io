(define-module (crates-io ad li) #:use-module (crates-io))

(define-public crate-adlib-0.1 (crate (name "adlib") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "1likh7sn496560ihr81k1amgwm1pr1pcm72f67bkisyyx3ddcp5r") (yanked #t)))

(define-public crate-adlib-0.1 (crate (name "adlib") (vers "0.1.1") (deps (list (crate-dep (name "async-stream") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)))) (hash "05m6iffa3zx1q78gm79vcnjlyl49ass56af11ghh8bc4mkfgrvn7") (yanked #t)))

