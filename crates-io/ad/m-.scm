(define-module (crates-io ad m-) #:use-module (crates-io))

(define-public crate-adm-cli-0.1 (crate (name "adm-cli") (vers "0.1.0") (deps (list (crate-dep (name "actix") (req "^0.7.9") (default-features #t) (kind 0)) (crate-dep (name "adm") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.10") (default-features #t) (kind 0)))) (hash "0ilx6c8mnbij3892hjgqjyghbv9w6fclpv4ix6mzmsm52pb24k1f")))

