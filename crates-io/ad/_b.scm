(define-module (crates-io ad _b) #:use-module (crates-io))

(define-public crate-ad_blob_test-0.1 (crate (name "ad_blob_test") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "chrono-wasi") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hmac-sha256") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.4") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "08sk937ns464si648bb0s5s8myiym2ir2b6qzk0dqs50jbqxybfd")))

