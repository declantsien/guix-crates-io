(define-module (crates-io ad if) #:use-module (crates-io))

(define-public crate-adif-0.1 (crate (name "adif") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.2") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "^5.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1cp18ikcvzyymnh7mvdwvkajpky3g7yzvhq2raygp2rpkyggsk5z")))

(define-public crate-adif-0.1 (crate (name "adif") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.2") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "^5.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0xkpg71hbkx6mwnmhgy6lmvcl4nnq6hl9k4ab0i8m8qf2crgbi0w")))

(define-public crate-adif-0.1 (crate (name "adif") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.2") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "^5.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0skdz2zwq6sjm8c3fjrz9s88ng4wx4k08x7hzwjf08qfinss7qkd")))

(define-public crate-adif-0.1 (crate (name "adif") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "^5.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "08598ql74zj616i0an2li9hj35r6hhd58ph059ifi7kal2fw33sa")))

