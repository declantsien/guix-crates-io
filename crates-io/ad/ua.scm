(define-module (crates-io ad ua) #:use-module (crates-io))

(define-public crate-aduana-0.1 (crate (name "aduana") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "084a2qkvv2n3sl33mqa5fi621baqb36j1dx074mlkfmr7824dvdy")))

