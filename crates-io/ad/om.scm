(define-module (crates-io ad om) #:use-module (crates-io))

(define-public crate-adom-0.0.1 (crate (name "adom") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "15fsdx328whjhggmsq599xja60ig36l19lbin4jsikaxlbx2di7y")))

(define-public crate-adom-0.0.2 (crate (name "adom") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1hbggiwznyvpwml12pq92hgzwx38l3brkag3ay3z74xsswf6564v")))

(define-public crate-adom-0.0.3 (crate (name "adom") (vers "0.0.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "046cn41fc2p9b2jml40bgykiv46234lvk94v5sfcaxg61qs4gnjr")))

(define-public crate-adom-0.0.4 (crate (name "adom") (vers "0.0.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0xwhnlnx9b9sj0jasx123j2ssf4kjhq5wyxvxrpx9al5bsh3dpmh")))

(define-public crate-adom-0.0.5 (crate (name "adom") (vers "0.0.5") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0") (default-features #t) (kind 0)))) (hash "0lx9w9rm10s1gahz5q33acaq5r8ray978qj2l5nnkichi6fc606a")))

(define-public crate-adom-0.0.6 (crate (name "adom") (vers "0.0.6") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0") (default-features #t) (kind 0)))) (hash "054q4nwwla5pfcmpi76marms6zf0gwzp4gwld8hzzywdp57vyvab")))

