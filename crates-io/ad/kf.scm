(define-module (crates-io ad kf) #:use-module (crates-io))

(define-public crate-adkfjakdjk-0.1 (crate (name "adkfjakdjk") (vers "0.1.0") (hash "0vm302290fyphyx0iz4gb72dxiaqbiz5ss2k8fqf2izwhr0xq3kw")))

(define-public crate-adkfjakdjk-0.2 (crate (name "adkfjakdjk") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12sbnqznlz9yxfghaykfhjjwipcmnqqz7rsqj1r42any9h35kcv9")))

(define-public crate-adkfjakdjk-0.2 (crate (name "adkfjakdjk") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xrql28l67z2w304x4m5lk8a4njwz5jpn2wiqz720nvjjwd5gp1k")))

(define-public crate-adkfjakdjk-0.3 (crate (name "adkfjakdjk") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12q7pv9x2888gda4cwbgf7jzqn8864sa8d3q89mzhh1rlll64k7w")))

