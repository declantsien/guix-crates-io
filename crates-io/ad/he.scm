(define-module (crates-io ad he) #:use-module (crates-io))

(define-public crate-adhesion-0.1 (crate (name "adhesion") (vers "0.1.0") (hash "1dm1wqyqpzrzigqp3pb59wm4q65675208dab7iy516cl0fh8s4vd")))

(define-public crate-adhesion-0.2 (crate (name "adhesion") (vers "0.2.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "scan-rules") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "0lnjqvm52c1x7l8p9s0m43iz8gb5msb30aqga6fvlmvscz0ymlhv")))

(define-public crate-adhesion-0.3 (crate (name "adhesion") (vers "0.3.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "scan-rules") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "1w1i8rydrzw4c98w5wjjrck6pw3lq5wcvsns4whhh6abw4sx2qz8")))

(define-public crate-adhesion-0.4 (crate (name "adhesion") (vers "0.4.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "scan-rules") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "02wq9j7b1c43waky2idmv0j0smffh3mxyn6rsi24dv0hg93dnvxi")))

(define-public crate-adhesion-0.5 (crate (name "adhesion") (vers "0.5.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "scan-rules") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "0y8581imir8yv1v69qlyvwcazgsxm10904zail94aqjxpxv8wqr4")))

