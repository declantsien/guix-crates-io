(define-module (crates-io ad ra) #:use-module (crates-io))

(define-public crate-adrastea-0.1 (crate (name "adrastea") (vers "0.1.0") (hash "0i687y745q4c87a46i0yvhd73yawkmk1rhg4lb7qyasm2gfrjsy6")))

(define-public crate-adrastea_kernels-0.1 (crate (name "adrastea_kernels") (vers "0.1.0") (hash "07mlpf3a64mjmql50i13jyykbpfszwx9h3qbx1dbv7rv7lajav98")))

