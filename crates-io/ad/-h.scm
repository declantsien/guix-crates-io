(define-module (crates-io ad -h) #:use-module (crates-io))

(define-public crate-ad-hoc-iter-0.1 (crate (name "ad-hoc-iter") (vers "0.1.0") (hash "0zfrx3c3vy7sl69wx9zdjxg50bn7cnqd5085akcw5nvzdpxmkvij") (features (quote (("maybe-many") ("default" "maybe-many"))))))

(define-public crate-ad-hoc-iter-0.1 (crate (name "ad-hoc-iter") (vers "0.1.1") (hash "1qx6ic2f69jj6kf1qjvyghcz85q6xhdyp02b7y7jidw1id4i76pi") (features (quote (("maybe-many") ("default" "maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2 (crate (name "ad-hoc-iter") (vers "0.2.0") (hash "163rg7070lxm28lrsg6s7mfb1bahm2jwavc3050c7ci3niciqg81") (features (quote (("maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2 (crate (name "ad-hoc-iter") (vers "0.2.1") (hash "0qnr855c83di3y7h4lfhsiqrcyvzx0wfa9483jvwxfh0d5m9kswr") (features (quote (("maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2 (crate (name "ad-hoc-iter") (vers "0.2.2") (hash "1s001883zh88zvcda4plh8xgrqpsg8qivqi11yad2cv08wls8s2h") (features (quote (("maybe-many"))))))

(define-public crate-ad-hoc-iter-0.2 (crate (name "ad-hoc-iter") (vers "0.2.3") (hash "1j09hgdrlblky063ahbmqcjlx3dgpby308i6hwv33dffprvdva4h") (features (quote (("maybe-many"))))))

(define-public crate-ad-hoc-iterator-0.2 (crate (name "ad-hoc-iterator") (vers "0.2.1") (hash "1k5fr0dap87sr95wk2pdqpwp9l726zkmgb2xyl490zj188ddsd12")))

(define-public crate-ad-hoc-iterator-0.2 (crate (name "ad-hoc-iterator") (vers "0.2.2") (hash "03smn0p060rnrl8nvrzmb4ycmjz0xscm31y9lb47gqglmnb966j2")))

(define-public crate-ad-hoc-iterator-0.2 (crate (name "ad-hoc-iterator") (vers "0.2.3") (hash "0v70s0i26pma2k978wj46k301ac9264xfl6py111s75sfbvwpsi4")))

