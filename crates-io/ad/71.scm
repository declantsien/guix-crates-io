(define-module (crates-io ad #{71}#) #:use-module (crates-io))

(define-public crate-ad7147-0.1 (crate (name "ad7147") (vers "0.1.0") (deps (list (crate-dep (name "byte-slice-cast") (req "^1.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-1") (req "=1.0.0-alpha.10") (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "15cqs9bh9yjkmvl4wy62nkdkdb82c0mjjmnws2hiq8qwikcq50pn")))

