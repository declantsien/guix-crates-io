(define-module (crates-io ad or) #:use-module (crates-io))

(define-public crate-adore-0.0.0 (crate (name "adore") (vers "0.0.0") (hash "0hw4csb1lizcgvm7r5rw4y15yb0i00qzplipk5l5vjl5mxkha77v")))

(define-public crate-adorn-0.1 (crate (name "adorn") (vers "0.1.0") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1gffw5pgvw7jqgsg0rdz36dl9qzvbxd7vri28j8z88jkgjm64cwc")))

(define-public crate-adorn-0.1 (crate (name "adorn") (vers "0.1.1") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "04i3q95kckrc57pk90zn350a1w8is2ayc24rcdypbnmccskahnbn")))

(define-public crate-adorn-0.1 (crate (name "adorn") (vers "0.1.2") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "0zy6rifn0yapk9ydaclq8sxk04b92sx5jp4pd303f37rn3l12dld")))

(define-public crate-adorn-0.2 (crate (name "adorn") (vers "0.2.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0mkqfm4wmk13qckqf484piw6dq8ip3r8mzskbzcik8jsy87yqy4l")))

(define-public crate-adorn-0.2 (crate (name "adorn") (vers "0.2.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1vq20djh8hgq9yfnwp0hxb1ixdhn9fi5dg1y9kisfn2mmlr7fvgz")))

(define-public crate-adorn-0.3 (crate (name "adorn") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "01kamlx8dff4pqgnbi6slbmrhb40bz6x1l26d89d60kgckc6fqi5")))

(define-public crate-adorn-0.4 (crate (name "adorn") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0m90iwm4726zyb8bqdj43gxdnxbadfyxzcr1k8xsivv85sq7gzbp")))

