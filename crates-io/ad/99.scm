(define-module (crates-io ad #{99}#) #:use-module (crates-io))

(define-public crate-ad9959-0.1 (crate (name "ad9959") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1isykdbjg13hvvb997hwn3irxi4ac37c03rlss6qrazrivjri782")))

(define-public crate-ad9959-0.2 (crate (name "ad9959") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0gh5nsjl26dfavx1wlgl6m15f70h09sfaicqgx23sbmgrajx55cv")))

(define-public crate-ad9959-0.2 (crate (name "ad9959") (vers "0.2.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0h2i0kqhxymasha5abdna2glxigks317pps9vr65fd1d69rl3d08")))

