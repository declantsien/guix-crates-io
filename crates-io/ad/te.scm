(define-module (crates-io ad te) #:use-module (crates-io))

(define-public crate-adtensor-0.0.1 (crate (name "adtensor") (vers "0.0.1") (deps (list (crate-dep (name "generic-array") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "typename") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0s2sy2i3lgz5z3psxzyvl9gwldhl28dq775knlmhhfiiqj64968h")))

(define-public crate-adtensor-0.0.2 (crate (name "adtensor") (vers "0.0.2") (deps (list (crate-dep (name "generic-array") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "typename") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "1p1kzy7cc3gbw0l1hbc0dcp8413hh11a7qf4wwx54czlclsz9aj3")))

(define-public crate-adtensor-0.0.3 (crate (name "adtensor") (vers "0.0.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "158chin7msvbs1a0ly5inxdbrgarcf3xdz7wrjjwr71bx6ia8r3w")))

(define-public crate-adtest-0.1 (crate (name "adtest") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "0ixs5l5sy1y3gfjmnmwmmx48f89w3gswjx9mhid7k4fbypqiwxs0")))

