(define-module (crates-io ad d3) #:use-module (crates-io))

(define-public crate-add3-0.1 (crate (name "add3") (vers "0.1.0") (hash "0zk3v2mfr3y8pwkpl9zv6b3fqnvwzd8qcxqqv1x69hxfckwjs2qk")))

(define-public crate-add3-1 (crate (name "add3") (vers "1.0.0") (hash "1amavl52cihgv5h8s0ws08ji6821q4xrhb99ci9cgvyxkn3r57pw")))

(define-public crate-add3-2 (crate (name "add3") (vers "2.0.0") (hash "1kl7mh7mpm37gfhz3bpz612drj88r0p7jz9p19iz577m218v31n2")))

(define-public crate-add3-2 (crate (name "add3") (vers "2.0.1") (hash "1hmxmbvyv0hp5wdydsinr7kqcp404sf9sk3gj89518nxjm55827g")))

(define-public crate-add3-2 (crate (name "add3") (vers "2.1.0") (hash "177dcywn47dj8iqaahjdn3yyfrf4jlw71hkz08595gqrs2m4i8qx")))

(define-public crate-add3-3 (crate (name "add3") (vers "3.0.0") (hash "0vx716y18kqa356dzsayy2551z30wbscfd4gf6wql1g7hrvdhi2p")))

(define-public crate-add3-3 (crate (name "add3") (vers "3.0.1") (hash "0v17yr11f6ipkpfacjm6yl2mc16hyyavfndqqnxaxiwnky9f1yry")))

(define-public crate-add3-3 (crate (name "add3") (vers "3.1.0") (hash "1xnjnb4l43d1yqinhlkv0x11fxwd832s9hkh1ygay0bhv8hxkrxi")))

(define-public crate-add3-4 (crate (name "add3") (vers "4.0.0") (hash "05k9511dighx7dh4z2rxi3pyx19l0hbxkdarkz84p7dshfyzb6xs")))

