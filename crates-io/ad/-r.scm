(define-module (crates-io ad -r) #:use-module (crates-io))

(define-public crate-ad-rs-0.1 (crate (name "ad-rs") (vers "0.1.0") (deps (list (crate-dep (name "ema-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01rfq711fhj5xkvd93rhcamnn9kbv7kh2nsqr9a54fmbxq77i85h")))

