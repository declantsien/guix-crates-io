(define-module (crates-io ad #{53}#) #:use-module (crates-io))

(define-public crate-ad5328-0.1 (crate (name "ad5328") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1wal5gpw3g8my9ig7p1abjxspx54pxxal298fgpvi5gsb5nkb36r")))

(define-public crate-ad5328-0.1 (crate (name "ad5328") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1c1ajmv4p6w7gv46xjfiaj9rkfs54p52i3339dw8l5qj2qahpkxv")))

