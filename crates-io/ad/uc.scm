(define-module (crates-io ad uc) #:use-module (crates-io))

(define-public crate-ADuCM302x-0.1 (crate (name "ADuCM302x") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0l4fsyqhiz1cwizs6da7s7xl19gfqhgyqfl1wic37k4s1x7m1r4i") (features (quote (("rt" "cortex-m-rt/device"))))))

