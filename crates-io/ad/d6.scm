(define-module (crates-io ad d6) #:use-module (crates-io))

(define-public crate-add6-0.1 (crate (name "add6") (vers "0.1.0") (deps (list (crate-dep (name "add3") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1l3q5qkglmdg4qp64zd3q46sqdzmcyagk3iffq66r6vpg0xk35cl")))

(define-public crate-add6_64-0.1 (crate (name "add6_64") (vers "0.1.0") (deps (list (crate-dep (name "add3") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0s8j9lj8ck4vjsb4f3f01ym3bbhzkmnj2ry0qyqin3kw9cdg3gjf")))

(define-public crate-add6_64-0.2 (crate (name "add6_64") (vers "0.2.0") (deps (list (crate-dep (name "add3") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "03nrajmbp7pkbmcdyga6fxlnmn2zwwi7q8p1qhv1v9sxzm0053wk")))

(define-public crate-add6_64b-0.1 (crate (name "add6_64b") (vers "0.1.0") (deps (list (crate-dep (name "add3") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0mfsrxf3hpybmv33xyady1ckqqgdj1dhy1pgk2izh74h5y3vmbsd")))

