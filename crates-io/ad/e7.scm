(define-module (crates-io ad e7) #:use-module (crates-io))

(define-public crate-ade791x-0.3 (crate (name "ade791x") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)))) (hash "04cdrxw653cmwg3bsd2w394bgapngjfr1s5pmlwqkpganc0l3kl9")))

(define-public crate-ade791x-0.3 (crate (name "ade791x") (vers "0.3.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)))) (hash "157rhm686pmlxxmkpv46p60xhrwhw3815kc5i23fd9cbndkr312p")))

(define-public crate-ade791x-0.3 (crate (name "ade791x") (vers "0.3.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)))) (hash "0jmcm3f6pjssg2zlqmx0x5rd37qc7rqhgl3vq5vz7bg2h4qka9vk")))

(define-public crate-ade791x-0.3 (crate (name "ade791x") (vers "0.3.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)))) (hash "0469lsms93rya38r9yk8fnr1g0aw9l9bc1awc8xx2ip0s67pzx53")))

