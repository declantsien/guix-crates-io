(define-module (crates-io ad mo) #:use-module (crates-io))

(define-public crate-admob-android-0.1 (crate (name "admob-android") (vers "0.1.9") (deps (list (crate-dep (name "crossbow-android") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0k1z3qigkspy6p938zg819ah3pyrjr7ars1jkiwmw8m8s3gqzgbw")))

(define-public crate-admob-android-0.2 (crate (name "admob-android") (vers "0.2.0") (deps (list (crate-dep (name "crossbow-android") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dkqiz4pnyvmjkfjavk3pndyj4sx0qwqdd9qg5g6gkbp4zx0qsqn")))

(define-public crate-admob-android-0.2 (crate (name "admob-android") (vers "0.2.1") (deps (list (crate-dep (name "crossbow-android") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1aaqjl9klrknljvdc6x9hlzfysch936zawxmwxdv9aflin5cxcv5")))

(define-public crate-admob-android-0.2 (crate (name "admob-android") (vers "0.2.2") (deps (list (crate-dep (name "crossbow-android") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "04m9rjhqbxb1xs6474bp4615cv5xfh4mhfk9r63sp0ldr6biknhg")))

(define-public crate-admob-android-0.2 (crate (name "admob-android") (vers "0.2.3") (deps (list (crate-dep (name "crossbow-android") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0qwjcz6bbf2f4lfrpw8c7h3qipxx22izgjqnrid7snimldsilsjy")))

