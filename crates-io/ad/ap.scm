(define-module (crates-io ad ap) #:use-module (crates-io))

(define-public crate-adaptavist-0.0.1 (crate (name "adaptavist") (vers "0.0.1") (hash "1sihy7p5q9mxxxagjadj47m7q8wkglnh937sd9kd8lyys3zn90x1")))

(define-public crate-adaptemoji-0.1 (crate (name "adaptemoji") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25") (features (quote ("default-formats"))) (kind 0)))) (hash "1h44d4addld38zpsgkc8dfpr5615610ijbhz8l0bn48r2as4ya9k") (features (quote (("rayon" "image/rayon") ("full" "rayon" "cli") ("default" "full")))) (v 2) (features2 (quote (("cli" "dep:clap")))) (rust-version "1.74")))

(define-public crate-adaptemoji-0.1 (crate (name "adaptemoji") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25") (features (quote ("default-formats"))) (kind 0)))) (hash "0ff8mgh6n2qw20zbghk7gkasxy2k8h72vmv141v5al5aqi053s92") (features (quote (("rayon" "image/rayon") ("full" "rayon" "cli") ("default" "full")))) (v 2) (features2 (quote (("cli" "dep:clap")))) (rust-version "1.74")))

(define-public crate-adaptive-0.1 (crate (name "adaptive") (vers "0.1.0") (hash "1q0jynjvxwvqlq1dyq1701q1szkswwqy11c3p8j79vsx1akfaylz")))

(define-public crate-adaptive-barrier-0.1 (crate (name "adaptive-barrier") (vers "0.1.0") (hash "003ygsiqsd85v0p846q1ym23dbp4iagn89p7k6yrvbg9di1mbjqc")))

(define-public crate-adaptive-barrier-1 (crate (name "adaptive-barrier") (vers "1.0.0") (hash "1004swrxg9g755h0sk0y1kclk4y9hzk6dzl8772df4l4j44gqz8w")))

(define-public crate-adaptive-bezier-0.1 (crate (name "adaptive-bezier") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "16sjsz81aavjvjll3srhc1bnya5wzfk3x9is96ya6by947z7ghw0")))

(define-public crate-adaptive_backoff-0.1 (crate (name "adaptive_backoff") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "11cjn8352pc7q37gi2nkmcdjm6sbrcn1yy1n75bdnijbk7jhy7ip")))

(define-public crate-adaptive_backoff-0.2 (crate (name "adaptive_backoff") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qbpff5393cz04ki1060xs6xyahnv5xndj8lic9f7qpzcld2xh01")))

(define-public crate-adaptive_backoff-0.2 (crate (name "adaptive_backoff") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "016zzv0229a3gf52a4wclaih9y9z3432in96bj1hpi5n1m5gw4vc")))

(define-public crate-adaptive_card-0.0.0 (crate (name "adaptive_card") (vers "0.0.0") (hash "18fnbcw4clvxqcvqvbq9yfqiahv97h90f14zwx3fgzq14x8pxcpb")))

(define-public crate-adaptive_sleep-0.1 (crate (name "adaptive_sleep") (vers "0.1.0") (hash "1syx44aq94wsp89mfz7ai7yf8cc653087srw6gb7zz5qq2qgymlv")))

(define-public crate-adapton-0.0.1 (crate (name "adapton") (vers "0.0.1") (hash "0ppdnmcclzs72hn3zpx7vnrn717d82rbvhbdkfvxdycbx2ffb9g8")))

(define-public crate-adapton-0.1 (crate (name "adapton") (vers "0.1.1") (hash "1lln0414wlbda5ga93ch7c7sg4zf55yh9bgszq2wvy023i4z29wz")))

(define-public crate-adapton-0.2 (crate (name "adapton") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hj2jxp3fspxzmby2h7jy4djx3vfi7bjv0l1vkqskjqaxcdr9dlp")))

(define-public crate-adapton-0.2 (crate (name "adapton") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mlcp75z56yjn9xbsga1vjkxp33fpicmdzh72k7p6s1jzvy75yq1")))

(define-public crate-adapton-0.2 (crate (name "adapton") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0h0ypc2m0giwqqp8i1r6aqf49xrphkr976aicnmav9k8y1sklqg4")))

(define-public crate-adapton-0.2 (crate (name "adapton") (vers "0.2.4") (deps (list (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "166kxgpggq2wn20vwavbk3j1i1m6zm61514s2gyplgvba7cpgqkj")))

(define-public crate-adapton-0.2 (crate (name "adapton") (vers "0.2.5") (hash "17ma88f1p0193nldmn5zvs59ij6l8nh33c5glp4q56p1cy7sdyvl")))

(define-public crate-adapton-0.2 (crate (name "adapton") (vers "0.2.6") (hash "0s4r7f0c15dghwzgqad823msb8v3rvfr3smrzvxzy4ca37h9008l")))

(define-public crate-adapton-0.2 (crate (name "adapton") (vers "0.2.7") (hash "1lhja89mn5sb82gvys0h94y4441q9qsffkwyk74vm10jd69lrm4y")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.0") (hash "1bvr3rm2n5hgf8hpvac4y2s8ywjr7dl7afvv1z4a16ck386hxd05")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.1") (hash "1whm5x3xkly1dhy95abcgajljkbjldrykn249qc6sgh3fla6159p")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.2") (hash "0sv0xbdzqb3fhwiinnsr94qzzx2p0p0pxp60hv9fbfbmfxx4z7sm")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.3") (hash "1lpkp7hs86w0x9nz44dqhjm7m76pwmmwq7v02zalnsf49hpfg84a")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.4") (hash "1kdddsm2b2c26dvq2la61bjzq124pyclhkzz0qw23zchs7znxqq7")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.5") (hash "084vr18lj7rlfw41ksgxmivgb2garb02w24328bfyhlj3xsps5qs")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.6") (hash "08vliqr13a56i235ilrf9nrkn86c009kcs38vwwqy3x8516i6gl3")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.7") (hash "16y1khi1ks92laqbazblq8i2va5cf59zvdz2hy8cawqv0q5fyjq3")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.8") (hash "16zxfy5l33qdgawi24frrbjgg3fj5rh2qyxppyayzcqj5w1sbmyq")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.9") (hash "1f0mli0zlnaxk88v2pry802vpnad5i75lpjrnbk9gffl29ppmssa")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.10") (hash "122pn2sqjadknrvh2ak3zls8h0wrdnmn762z7jk4wq7109ks36ih")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.11") (hash "16i5grmzinl6lkmyd8qh0fm28cqq0z1c695h35qjzrvzjkc0v5s6")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.12") (hash "1x7lm4x4vivgb9xv2k526w5mz0dnmqghiddd9ms23zypg62l6xqz")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.13") (hash "1nch8apw7h16rdwdc0hwp3zvdp820ibxk628rxzlhsrzvz1gsawr")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.14") (hash "1s3sk1l8rkhlpz315b8pb8ap0hsczriji9jgbz1gwi19axh9pm5w")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.15") (hash "0gcyg671vnrjshmyqp901x5f9v4cimxyf2l0qarz8spc8jg6lkdk")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.16") (hash "0cmhv9cfvhjj6gshfialdkkjny5ncd9x1p6fk8y3bknqz9dihcnc")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.17") (hash "0bs4sw5kbq1hhs2zkw0g6j57373gy4vv30gjalpsif36f3jqmkqd")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.18") (hash "0dw21kmagarchnjn239x3x669nld2jrq4f35qrhmwqr7d23fsnrc")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.19") (hash "05rpxy31dmm6pa81qvx6zdrg65d26zv35vg3dbzgvsn7wrc1gjrh")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.20") (hash "1sa7720rfny7pr58mzxgj1vs98sk3hh0fbf72bqjg7aa6x0amyic")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.21") (hash "13a2xniwrnbck6b2ds4c3jl9yi1sjbmn79y3s62wn8sx662xvl1p")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.22") (hash "0c3asn5b3wbrzc0wsscs6v3a2lyfkz4qw4rdfrh71jc0bm6krhhg")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.23") (hash "1q01qhqpprrqwnxr3fnkj7b0m19c3l0q2y1x6g1sl38cy5jzar9b")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.24") (hash "182lx74j1mlfc3yryp2cv20ap3vki9k9iwar2s5kwwzvjpihm0y7")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.25") (hash "1s3gkrvf74rmmvdb84mkyqfggdib92dsx5h05x08cdxm3zirqj7f")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.26") (hash "05qkw7j5acbja97dxsky5pjcq3ay7qq63zr7bsdzv8q515c0r2b8")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.27") (hash "1hxi1ld097lbmy6f912fj8i3jglzqb4m3h7pk0z28y8l79p8kchl")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.28") (hash "108nqqqmmrnl5rsdvz3774r3q498vfpjsfknswvhzayh6hgi10h7")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.29") (hash "0nqd6q3g7cvgbyr8adj109hzgcr6z72ic3xffxdqm11ijfaibhgm")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.30") (hash "089c66ah26mxng10s27akn1qpi8ahyh88l7jc8rnw397bnfgsq7n")))

(define-public crate-adapton-0.3 (crate (name "adapton") (vers "0.3.31") (hash "0j4s1i7v7nd0gm0491nfi1yh708pk0mh5y8v80p8m5gl17y8h74q")))

(define-public crate-adapton-lab-0.1 (crate (name "adapton-lab") (vers "0.1.0") (deps (list (crate-dep (name "adapton") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ap2r919p3md2gxdl64ymggynpd7n68xflx8iw0bpk7iwgfnlv6m")))

(define-public crate-adaptor-0.1 (crate (name "adaptor") (vers "0.1.0") (hash "0bk6h4qd7z4g4lqchc7vnnm90byzpkirq90rlxv1djwq955j17sa")))

