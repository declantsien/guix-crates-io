(define-module (crates-io ad iv) #:use-module (crates-io))

(define-public crate-adivon-0.1 (crate (name "adivon") (vers "0.1.0") (hash "0hjgas2rgxdhx4pab3761qmravyc8mq353bzwpnk7q2xw8kglba3")))

(define-public crate-adivon-0.2 (crate (name "adivon") (vers "0.2.0") (hash "1bln43wk3hcmhxdbg3akfj54kin21a0jkjc1b51lzs4wd9rpdal1")))

(define-public crate-adivon-0.2 (crate (name "adivon") (vers "0.2.1") (hash "17xlyxsb38iyibi4xc62aar245vb33n6mm7b804m3inv7pljwahn")))

(define-public crate-adivon-0.2 (crate (name "adivon") (vers "0.2.2") (hash "1rj7fd4sr6v7yn3qqz9fa4fdjr51i4llnrjc0lgf2s0hlidcq6qp")))

(define-public crate-adivon-0.2 (crate (name "adivon") (vers "0.2.3") (hash "0hax6j3h1z5zncssys8jgr3cvvla60yb0hnaf3fgbwmjqxxk3cxn")))

(define-public crate-adivon-0.2 (crate (name "adivon") (vers "0.2.4") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "*") (default-features #t) (kind 0)))) (hash "1iwpr02r8fs3ki8pl1largx75a9k48qzrmg6v5gand2mvg5yvfnp")))

(define-public crate-adivon-0.2 (crate (name "adivon") (vers "0.2.5") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jfi6cvrpb5dycvgm7q4jw5dddlfv5dkhxdkz03qam3a7lc9vnbc")))

(define-public crate-adivon-0.2 (crate (name "adivon") (vers "0.2.6") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6") (default-features #t) (kind 0)))) (hash "1ac2l2n0xql5x0bqalc22cw2lx6rb06if2b4rgrkpc4xsm24m09m") (features (quote (("dev" "clippy") ("default"))))))

