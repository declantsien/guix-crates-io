(define-module (crates-io ad ss) #:use-module (crates-io))

(define-public crate-adss-0.2 (crate (name "adss") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "star-sharks") (req "^0.6.0") (features (quote ("zeroize_memory"))) (kind 0)) (crate-dep (name "strobe-rs") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0w6vv09m73yajgh3gprgm394n5m350dg3kiknf124qgz4yn6rg6q")))

(define-public crate-adss-0.2 (crate (name "adss") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "star-sharks") (req "^0.6.0") (features (quote ("zeroize_memory"))) (kind 0)) (crate-dep (name "strobe-rs") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1pxr4y4vgwp8i0bp9acm505q07nnf16vxvhr03j0nnbyhfw6hskn")))

