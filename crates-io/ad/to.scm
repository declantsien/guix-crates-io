(define-module (crates-io ad to) #:use-module (crates-io))

(define-public crate-adtobs-0.1 (crate (name "adtobs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "120s675m54wk6m7dnv6caif4y0zcq49ksv5jpn8z32g87anzysq2")))

(define-public crate-adtobs-0.1 (crate (name "adtobs") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1z7zxi8il1flji14vvzg6dv223jybim8a2qw3gzyaqfzi7sdk1jr")))

(define-public crate-adtobs-0.1 (crate (name "adtobs") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "12c2vdr652mrx8cq91jcmma9inybalq5xxq4brs2b4n2whd90qq4")))

