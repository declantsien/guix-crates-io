(define-module (crates-io ad di) #:use-module (crates-io))

(define-public crate-addin1c-0.1 (crate (name "addin1c") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1.11") (default-features #t) (kind 0)) (crate-dep (name "utf16_lit") (req "^2.0") (default-features #t) (kind 0)))) (hash "0nxmxcrjgy7ll20jkql0hl80mir2chfamm1l0fyz1mc3gpsq9nk1")))

(define-public crate-addin1c-0.1 (crate (name "addin1c") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^1.11") (default-features #t) (kind 0)) (crate-dep (name "utf16_lit") (req "^2.0") (default-features #t) (kind 0)))) (hash "1hrif1qv9f76cfsj8hsxdf6rddw0xw5ji6kakzy8133bykxa5r1w")))

(define-public crate-addin1c-0.2 (crate (name "addin1c") (vers "0.2.0") (deps (list (crate-dep (name "smallvec") (req "^1.11") (default-features #t) (kind 0)) (crate-dep (name "utf16_lit") (req "^2.0") (default-features #t) (kind 0)))) (hash "1hsd47rzp1llr6nyjbbrknx2zd03cbr5p7jjn7x1r5863918byaq")))

(define-public crate-addin1c-0.3 (crate (name "addin1c") (vers "0.3.0") (deps (list (crate-dep (name "smallvec") (req "^1.11") (default-features #t) (kind 0)) (crate-dep (name "utf16_lit") (req "^2.0") (default-features #t) (kind 0)))) (hash "1flpj9xamwnqly41sksdb01f4w4ivy5vcpwqgn4bv5z91p3fm9zd")))

(define-public crate-addin1c-0.4 (crate (name "addin1c") (vers "0.4.0") (deps (list (crate-dep (name "smallvec") (req "^1.11") (default-features #t) (kind 0)) (crate-dep (name "utf16_lit") (req "^2.0") (default-features #t) (kind 0)))) (hash "13d1z5ig3mwlcddz6crnv9ss2nw6sn29gcpr7lfyf2yzy43q23ci")))

(define-public crate-addition-lib-0.1 (crate (name "addition-lib") (vers "0.1.0") (hash "1kvh0aw0qzjbsxichkvnlrw25ha88w8hfljqnb270hac1q2d7klw") (yanked #t)))

(define-public crate-additional-accounts-request-0.1 (crate (name "additional-accounts-request") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.29.0") (features (quote ("event-cpi"))) (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0z81chd1dqawl0m7kz8vn8qzy50xy5s97lwhpci799kswrkjmgil")))

(define-public crate-additional-accounts-request-0.1 (crate (name "additional-accounts-request") (vers "0.1.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.30.0") (features (quote ("event-cpi"))) (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "14r5fdmysf5bfgraxbdlhqpf62abhj349zkjvmdxdjhgk4ap5zdb")))

