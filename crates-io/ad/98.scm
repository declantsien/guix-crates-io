(define-module (crates-io ad #{98}#) #:use-module (crates-io))

(define-public crate-ad983x-0.1 (crate (name "ad983x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "1yn28mrzz5qrm06x8h65s1nfyw9shksvc9iad9jbczj1603bas3q")))

(define-public crate-ad983x-0.1 (crate (name "ad983x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "14x6240lbf607p1acnpxxr7dgk32dxw4i1k6a9hfrfjhqr9ww189")))

(define-public crate-ad983x-0.2 (crate (name "ad983x") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ymydb4sn134bva4b6mfsrg3fn9nsxs29y1pm53y37s1z9d9q302")))

(define-public crate-ad983x-0.3 (crate (name "ad983x") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "081ynawngkgifhmci98qp4kvyd0m42y7nh37pnbjibjmgqaahr7k")))

(define-public crate-ad983x-1 (crate (name "ad983x") (vers "1.0.0") (deps (list (crate-dep (name "dummy-pin") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-bus") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0") (features (quote ("eh1"))) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1xl8dg7vh9rscsxx1c9kd01lgbnbhzxrpnp2xydfrilqb4p4bzgj")))

(define-public crate-ad9850-0.1 (crate (name "ad9850") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "01f8vavf7iiw0kidfcfabvwgv7wfy88b3wx1q98jsy7921kdckwm")))

(define-public crate-ad9850-0.1 (crate (name "ad9850") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1kqv0hf0cfji3ijax6k0q4jmn64mxri3z44ggajdmgjh9lsl8jp0")))

(define-public crate-ad9850-0.1 (crate (name "ad9850") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0yz911fmp3x7b4csp88v9pgvqi3g6jh4haq570261250rbrlngy7")))

