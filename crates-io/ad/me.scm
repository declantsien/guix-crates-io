(define-module (crates-io ad me) #:use-module (crates-io))

(define-public crate-admerge-0.1 (crate (name "admerge") (vers "0.1.0") (deps (list (crate-dep (name "byteseeker") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "12jipq9y5v33hgpa97x55qqi4bhl07bi7841gqiilfnixbcx4qbc") (yanked #t)))

(define-public crate-admerge-0.1 (crate (name "admerge") (vers "0.1.1") (deps (list (crate-dep (name "byteseeker") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dxpjznv85p8zfz1782w6iwqvswczq2b4bybhqrlf9ivnn98cch9")))

(define-public crate-admerge-0.1 (crate (name "admerge") (vers "0.1.2") (deps (list (crate-dep (name "byteseeker") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "06ai051nq26rprs17g7vjxlgmw9psy99l9s8pmkzq56plwhapabz")))

(define-public crate-admerge-0.1 (crate (name "admerge") (vers "0.1.3") (deps (list (crate-dep (name "byteseeker") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "13pr45cl9bwq2852gvzllyz0x32gpj394a8vygk7v6ybllsvpg9x")))

