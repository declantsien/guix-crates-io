(define-module (crates-io ad o-) #:use-module (crates-io))

(define-public crate-ado-test-0.1 (crate (name "ado-test") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.149") (default-features #t) (kind 0)))) (hash "0wjhgd60033rn9mxyshylia9j58v308jkfd713wmq0im7d523w8q")))

(define-public crate-ado-test-0.2 (crate (name "ado-test") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.149") (default-features #t) (kind 0)))) (hash "1wkcgkn3irvw31ild53l5hnn7yh7ns3j8d55m8cf32y8g4d810yd")))

