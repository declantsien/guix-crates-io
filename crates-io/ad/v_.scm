(define-module (crates-io ad v_) #:use-module (crates-io))

(define-public crate-adv_derive-0.0.0 (crate (name "adv_derive") (vers "0.0.0") (hash "0m58snnhz8rcw639635sx9z2zymzq6my7n4fq45y1lv9sspjr6aq")))

(define-public crate-adv_linalg-0.1 (crate (name "adv_linalg") (vers "0.1.0") (deps (list (crate-dep (name "adv_linalg_lib") (req "^0.1") (default-features #t) (kind 0)))) (hash "025w4q9yypam0andq6yhybib6vv77wlh1nbmf8a06s5xnsd13lg1")))

(define-public crate-adv_linalg_lib-0.1 (crate (name "adv_linalg_lib") (vers "0.1.0") (deps (list (crate-dep (name "adv_linalg_proc_macro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dspc15p3lmpigc8kqhf8klbladnywfk90wizn7w71428csf9arr") (features (quote (("no_std") ("full" "no_std") ("default" "full"))))))

(define-public crate-adv_linalg_lib-0.1 (crate (name "adv_linalg_lib") (vers "0.1.1") (deps (list (crate-dep (name "adv_linalg_proc_macro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "03157knkbcn1w6cw5z2japnn1i0fgf2n5cljww0fci4v8n9wg6aw") (features (quote (("no_std") ("full" "no_std") ("default" "full"))))))

(define-public crate-adv_linalg_proc_macro-0.1 (crate (name "adv_linalg_proc_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1zbpm3hgvpc6sbd5ki4jickyd7bv80xpy8f0vimqn3bk7ajx3ad9")))

(define-public crate-adv_linalg_proc_macro-0.2 (crate (name "adv_linalg_proc_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "venial") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0fps6wlcp9nk5r0d5v6d095463ngff02qn9qgwj3rjfxrys5k0pz")))

(define-public crate-adv_macros-0.0.0 (crate (name "adv_macros") (vers "0.0.0") (hash "1rriv3h7gpf1rmkhavk97rxf3g6lxfhwmg5njac536bmi5rfk0hj")))

(define-public crate-adv_random-1 (crate (name "adv_random") (vers "1.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)))) (hash "0z99mcnskkimyx9qsk2aflvwag8j11ls47iiya12w2rcvf4r2v2q")))

(define-public crate-adv_random-1 (crate (name "adv_random") (vers "1.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)))) (hash "0ndcl6fvzq8wxllrsp0lwkkj0rx8w2azmyrqg0b519bk55z6x39j")))

(define-public crate-adv_random-1 (crate (name "adv_random") (vers "1.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)))) (hash "14ahhzdwdscqxypkr4w8yfc655j7jcfd218lbyvksb48v67z90hf")))

(define-public crate-adv_random-2 (crate (name "adv_random") (vers "2.0.0") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1n4cn5a9s4iaaqzmk9wz7q3snd60yfy0l4nmcdz8v2yajf8zvii3") (features (quote (("default" "rand")))) (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-adv_random-2 (crate (name "adv_random") (vers "2.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "11cm3m7nll07k30x30223jxrj8w78falghclrik8lbizdxwz7ns4") (features (quote (("default" "rand")))) (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-adv_random-2 (crate (name "adv_random") (vers "2.0.2") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0yl9psb2gjvm15wlfs0v4w94wg9lmy7l9ahd40vhs5iawk1z4177") (features (quote (("default" "rand")))) (v 2) (features2 (quote (("rand" "dep:rand"))))))

