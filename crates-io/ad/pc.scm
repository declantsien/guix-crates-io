(define-module (crates-io ad pc) #:use-module (crates-io))

(define-public crate-adpcm-xq-sys-0.1 (crate (name "adpcm-xq-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1pgkgzn05xfs7iw8ic104xdayj7b4lnxq5c2vsx7bss1sz5q74b8") (features (quote (("nds") ("default"))))))

(define-public crate-adpcm-xq-sys-0.1 (crate (name "adpcm-xq-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "12ic20vh0cqg5y33z30yiqs78vwvz8gwb0r8x1a2kdxfcrabw3l6") (features (quote (("nds") ("default")))) (yanked #t)))

(define-public crate-adpcm-xq-sys-0.1 (crate (name "adpcm-xq-sys") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1g55a2i8kncr4mqgd017iahsabywzwqppn1kysn8jwrp7hgdi51b") (features (quote (("nds") ("default"))))))

