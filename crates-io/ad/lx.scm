(define-module (crates-io ad lx) #:use-module (crates-io))

(define-public crate-adlx-0.0.0 (crate (name "adlx") (vers "0.0.0-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "1xmfqd8lf2nqsyxjw7bfdnrii0nk4749s1mkigybgkwx3hnmmr7s")))

