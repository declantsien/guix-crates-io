(define-module (crates-io ad rg) #:use-module (crates-io))

(define-public crate-adrg-0.1 (crate (name "adrg") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.3.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1d47pd0jawlrrnmjfh0wmjssx8n2pkwlsly2xp98i83rjryk3n7v") (yanked #t)))

