(define-module (crates-io ad ts) #:use-module (crates-io))

(define-public crate-adts-0.2 (crate (name "adts") (vers "0.2.0") (deps (list (crate-dep (name "genindex") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1942hnf6bznl1yk1ylambnmwfxxbk16xb15bahpag2za184m2zw6") (features (quote (("std") ("default" "std")))) (v 2) (features2 (quote (("serde" "dep:serde" "genindex/serde"))))))

(define-public crate-adts-reader-0.1 (crate (name "adts-reader") (vers "0.1.0") (deps (list (crate-dep (name "bitstream-io") (req "^0.6.3") (default-features #t) (kind 2)) (crate-dep (name "hexdump") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "07dfvrgk9na8as52jrs4r5fjvpdsqc45x514abvdh29168h8w222")))

(define-public crate-adts-reader-0.2 (crate (name "adts-reader") (vers "0.2.0") (deps (list (crate-dep (name "bitstream-io") (req "^0.6.3") (default-features #t) (kind 2)) (crate-dep (name "hexdump") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1ycfhs9kj4d0963w6yrn5lbplrgdxnfdpl1zhvn78v0lbq4hkd7r")))

(define-public crate-adts-reader-0.3 (crate (name "adts-reader") (vers "0.3.0") (deps (list (crate-dep (name "bitstream-io") (req "^0.6.3") (default-features #t) (kind 2)) (crate-dep (name "hexdump") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1drfbizdbnsg9x02gz67afhxjx5qlk49z50gn81ckhfya2cknzq4")))

