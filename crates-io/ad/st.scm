(define-module (crates-io ad st) #:use-module (crates-io))

(define-public crate-adstxt-0.1 (crate (name "adstxt") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "macros"))) (optional #t) (default-features #t) (kind 0)))) (hash "1l1f7052lbqspifzfkpkaykka1lmniv3367l6yh632368jnx8scx") (features (quote (("parser") ("default" "parser") ("crawler" "log" "hyper" "hyper-tls" "tokio"))))))

(define-public crate-adstxt-0.1 (crate (name "adstxt") (vers "0.1.1") (hash "19c8kj6hrd1x9f2mz02wqj942jk67983p03f252lyx7r8kkl58xc") (features (quote (("default"))))))

