(define-module (crates-io ad of) #:use-module (crates-io))

(define-public crate-adofai-rs-0.1 (crate (name "adofai-rs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_jsonrc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pmkzbf1d5255rh2vyd2gabvigdi11j792484nsyh8cq93gacj1y")))

