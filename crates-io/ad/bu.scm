(define-module (crates-io ad bu) #:use-module (crates-io))

(define-public crate-adbutils-0.1 (crate (name "adbutils") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4.44") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.0.14") (default-features #t) (kind 0)))) (hash "08dix5nw3hylvi189kxz5zn91niak331z20bwasqjkjk261sbcv6")))

(define-public crate-adbutils-0.1 (crate (name "adbutils") (vers "0.1.1") (deps (list (crate-dep (name "curl") (req "^0.4.44") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.0.14") (default-features #t) (kind 0)))) (hash "1wninj43r09qkbq6cqcbh6d2d12j70gpp299i43gqsblps570vwg")))

