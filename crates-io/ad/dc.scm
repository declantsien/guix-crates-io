(define-module (crates-io ad dc) #:use-module (crates-io))

(define-public crate-addchain-0.0.0 (crate (name "addchain") (vers "0.0.0") (hash "0xy3s5rril5mwy6k5y3pn1y0p2sh7q3qih4y1pjygfs39irlqyrz")))

(define-public crate-addchain-0.1 (crate (name "addchain") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1728ndjvxwaz86g1ms6nhph6kn3zgjacv8z908h4kfx7jcn24xqi")))

(define-public crate-addchain-0.2 (crate (name "addchain") (vers "0.2.0") (deps (list (crate-dep (name "num-bigint") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w45hpybsx9gzhlxf6x9451kycg8xwj3x8qzjnk8wqm55926jbiv")))

