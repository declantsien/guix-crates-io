(define-module (crates-io ad ro) #:use-module (crates-io))

(define-public crate-adroit-0.0.0 (crate (name "adroit") (vers "0.0.0") (hash "0l3hx8wv5y13lybcbmfs5ai40azyvmjvdsy09hzmpl1ppwkj045c")))

(define-public crate-adroit-0.1 (crate (name "adroit") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14") (default-features #t) (kind 0)))) (hash "09bwi9y7wkl9a27jj28ydszhgw4f7by5k8211as4a8kkn0dyq5h4")))

(define-public crate-adrop-0.1 (crate (name "adrop") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1ysmha6awkmmf9fb3l38gjn2z4drqz3kyr6lrswk720q2hc2zr0n")))

(define-public crate-adrop-0.2 (crate (name "adrop") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1mi1h7dlyh44s6slrckv7hlqisbmzzgxgaabgy58pwcyaz5lvffd")))

(define-public crate-adrop-0.2 (crate (name "adrop") (vers "0.2.1") (hash "0a36hs4xlkg2djf5zsg3hlggwwd8mk37cp70kdniw3q0m3ryl01s")))

