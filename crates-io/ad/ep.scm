(define-module (crates-io ad ep) #:use-module (crates-io))

(define-public crate-adept2-0.1 (crate (name "adept2") (vers "0.1.0") (deps (list (crate-dep (name "adept2-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mwx1pjryrpaw1mgl627v36crsf9z0xpl10v03rq97aghnv7m74l")))

(define-public crate-adept2-sys-0.1 (crate (name "adept2-sys") (vers "0.1.0") (hash "10cmdrgx6yvdrlaxblw8q375lkqpan14j011xsp685irgx66i4k3")))

