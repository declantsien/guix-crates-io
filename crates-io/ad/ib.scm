(define-module (crates-io ad ib) #:use-module (crates-io))

(define-public crate-adibat-0.1 (crate (name "adibat") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0hlxyqv19iph51907yczz9dypibs7g8f6vn4giq2i117f2s2k509")))

(define-public crate-adibat-0.2 (crate (name "adibat") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0mvgkbbsvxlgpvfrxslqkykjxw1l5iy5573y1d7m96vpyx97psfq")))

