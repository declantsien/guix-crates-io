(define-module (crates-io ad s_) #:use-module (crates-io))

(define-public crate-ads_client-1 (crate (name "ads_client") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)))) (hash "0ifdcxl9b247slvik6a3bpil2b3q7k7azcvvnf0fa2whamggqdns")))

(define-public crate-ads_client-1 (crate (name "ads_client") (vers "1.1.0") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)))) (hash "0hywcnm4f1vi1ngljv8mbnhc0q5327qh6vy30l77lvb2qsp4kiqn")))

(define-public crate-ads_client-1 (crate (name "ads_client") (vers "1.2.0") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)))) (hash "0xqc7pdfs7arz8kp0v20fwm6ym591311mnwy5ai5pkick5p2awm5")))

(define-public crate-ads_client-1 (crate (name "ads_client") (vers "1.2.1") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)))) (hash "1kr1racxkrg7vk5wm2h90724qmd4lp65a81wad0dbbpzc7d5l6gg")))

(define-public crate-ads_client-1 (crate (name "ads_client") (vers "1.3.0") (deps (list (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)))) (hash "0c2aszk6wmkvkv3lryplirn7cdsva3lk58j0rhviwvgyy5bjy23q")))

(define-public crate-ads_client-1 (crate (name "ads_client") (vers "1.4.0") (deps (list (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)))) (hash "1fc0i5fi10hfng8bbh1556b1f17nfmwz10islpfn2jnpaiyvjpm8")))

(define-public crate-ads_client-1 (crate (name "ads_client") (vers "1.4.1") (deps (list (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)))) (hash "1c277wpayw6bgmw32h74xs76hv9caw4rvggfvyigmq541qbjxymc")))

