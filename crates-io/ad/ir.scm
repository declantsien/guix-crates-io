(define-module (crates-io ad ir) #:use-module (crates-io))

(define-public crate-adirector-0.1 (crate (name "adirector") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "time" "rt" "sync"))) (kind 0)))) (hash "07m3432ajln5b079gd6zlikavsa7xndjh3riw6s97ww8y7zx034y")))

(define-public crate-adirector-0.1 (crate (name "adirector") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "time" "rt" "sync"))) (kind 0)))) (hash "1cba4b0jy7hdyqx318rsxnxp2wj3innr1szpcqrfhk7v1wlgsc36")))

