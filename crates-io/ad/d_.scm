(define-module (crates-io ad d_) #:use-module (crates-io))

(define-public crate-add_and_sub-0.1 (crate (name "add_and_sub") (vers "0.1.0") (hash "1zjzkb90czllv5wg87vckggx8qgnvaw8bxndcfmlfbl0mpbnwdfv")))

(define-public crate-add_getters_setters-0.1 (crate (name "add_getters_setters") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1sgs5sjii6r8sax910ndp6hgpzqm0qsfajswk8aabjgdgimvhmkq")))

(define-public crate-add_getters_setters-0.1 (crate (name "add_getters_setters") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0iqjd9s6960j3s2q8cx306cgiq6qykynqh42526piknl7s44qf5m")))

(define-public crate-add_getters_setters-0.1 (crate (name "add_getters_setters") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0d7jh2bq7ns945yl8m2v90py23c7k3hcgl2kwf0hzjqfgx9ks62q")))

(define-public crate-add_getters_setters-0.1 (crate (name "add_getters_setters") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "00lmnh2b5cm9iygjr8f6vgj4x22ippjpi0j6lix0mq3wcpcdm1q8")))

(define-public crate-add_getters_setters-0.1 (crate (name "add_getters_setters") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0kw1ksg4rx4w4c7qwayxn0fzwf6fmc3lhhz0z5qn7d0n5fsyaf8i")))

(define-public crate-add_getters_setters-0.1 (crate (name "add_getters_setters") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "015qqx8rc3fli81y3w72dj10nmql282hbwzfk071gbpl5909l5mm")))

(define-public crate-add_getters_setters-0.1 (crate (name "add_getters_setters") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0mvvvxvzbmvsdfsfs97y6pyzf34nvpphn2sfrzfn6v6ld9blc5kx")))

(define-public crate-add_getters_setters-1 (crate (name "add_getters_setters") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1a8hr3sd99n42rsvwcxy4h3jxs3jb4cqbix6g05dg8j25i6k598b")))

(define-public crate-add_getters_setters-1 (crate (name "add_getters_setters") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0mx4pj6z0vri8vhjm866yi5y71xax8z6nxyqp27a0i8bsgwq3zvl")))

(define-public crate-add_getters_setters-1 (crate (name "add_getters_setters") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "13zjv2f2gqhjhj7r82mwn4d00ssg555lkljrg7c5551qfxy12i8f")))

(define-public crate-add_getters_setters-1 (crate (name "add_getters_setters") (vers "1.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0fs1frz2gpb4jmvnsbz3fzp45y69f0324hadn6f8234kqhkggjn7")))

(define-public crate-add_getters_setters-1 (crate (name "add_getters_setters") (vers "1.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "02ga0hlnhk5b1ihfyharb2z9f11gafy4ilf44rh2njvln5x6gpzg")))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.0") (deps (list (crate-dep (name "display_derive") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0v92i1awsgzbzjyr6pqqckig0ybj7k9rq08kx012vig2jd1kqpfn") (yanked #t)))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.1") (deps (list (crate-dep (name "display_derive") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "13ps7r5ziyngyp8s8ql24ajrxj75rzn63z9s87yvlxzmyc7i0ckv") (yanked #t)))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.3") (deps (list (crate-dep (name "display_derive") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0fy5ly84fiw9q4fg6xg6sk9zgifn3c7s2srcpclvcwwix7ckdw2v") (yanked #t)))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.4") (deps (list (crate-dep (name "display_derive") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0d56ih061n4ncnacgw8fv4ihwdvywzg01lial8zypzmjwdz9z6b5") (yanked #t)))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.5") (deps (list (crate-dep (name "display_derive") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "056rvdk285whnkay42fhm2sh3x7ifwpb8rc1bw91iqbj23yp2f9x") (yanked #t)))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.6") (deps (list (crate-dep (name "display_derive") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "15y12pjs09mwig59xksncy5j5qdwwpqhyrf2r6lhrl7r6iqv0bz7") (yanked #t)))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.7") (deps (list (crate-dep (name "add_macro-display_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0nlrhgn2bvdy77y6dak94nhwfpld7vkwc9d9613nms6pcw3m66pc")))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.8") (deps (list (crate-dep (name "add_macro-display_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "add_macro-parse_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1mzz2z6bx9swvy55jiaxpqjx4pn5sp60pbr9kzbwgy2zrbwr22b0")))

(define-public crate-add_macro-0.1 (crate (name "add_macro") (vers "0.1.9") (deps (list (crate-dep (name "add_macro-display_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "add_macro-parse_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1pz8amfl081bj0ic8i1w7qfgadq3bf5zvnw2nipbzz2nbcb1cg4s")))

(define-public crate-add_macro-0.2 (crate (name "add_macro") (vers "0.2.0") (deps (list (crate-dep (name "add_macro-display_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "add_macro-parse_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "15bwdbzk3c48pv984pkdl97z81sl6q3c3m2qa07nwf1b63bzr2y3")))

(define-public crate-add_macro-0.2 (crate (name "add_macro") (vers "0.2.1") (deps (list (crate-dep (name "add_macro-display_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "add_macro-parse_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0gyjds8b9crvynwzx508750y5dlma198234issmwkcwm860a3434")))

(define-public crate-add_macro-0.2 (crate (name "add_macro") (vers "0.2.2") (deps (list (crate-dep (name "add_macro-display_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "add_macro-parse_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "03y3m1ph135hlq6nkdn9bx7hw3gf03chj3gb1cn81a3pffrp3jq5")))

(define-public crate-add_macro-display_impl-0.1 (crate (name "add_macro-display_impl") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gjc4bm83sjprkn7d7n8b59swfrccjg6my2b58pj6x4c49ibi00x")))

(define-public crate-add_macro-display_impl-0.1 (crate (name "add_macro-display_impl") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q35r025gf23fp37ixfp7d4isjcabxbdn73flg6f7n2xf3ri1wlg")))

(define-public crate-add_macro-parse_impl-0.1 (crate (name "add_macro-parse_impl") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vzn1xpfz9fbdpn7wv2xjp40qjp9m3kzld88bmaan4m9sjj4qqhf")))

(define-public crate-add_macro-parse_impl-0.1 (crate (name "add_macro-parse_impl") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rz9difz14bixyvjj8y0ihwm8xpqpl81p9a7n9lzm1zfd3lylz7d")))

(define-public crate-add_one-zfk-rs-0.1 (crate (name "add_one-zfk-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1gffhahkvvmcxvz8ihja9zr2p7pbl5k5d674as2c00pp6rdgc4wp")))

(define-public crate-add_one_06122023-0.1 (crate (name "add_one_06122023") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "049kjwazv39nfry014v7gql9f0rm2c9jhnzifdhg2qa4cwk6n6iy")))

(define-public crate-add_one_70kg-0.1 (crate (name "add_one_70kg") (vers "0.1.0") (hash "05vgn2drpiwwygajfvii2pbcvn29pbrr4a30rgyw34j4p3gf95hi")))

(define-public crate-add_one_8732-0.1 (crate (name "add_one_8732") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "18smfi03p0gw2zwjfmrrkh7k5ysgv3rj8yi9dcqshkafln639lqk")))

(define-public crate-add_one_crate-0.1 (crate (name "add_one_crate") (vers "0.1.0") (hash "1n39hhdnhqryglym661xfzswrd6avd1pc8lax4lbip5yz9if0050")))

(define-public crate-add_one_hammahutz-0.1 (crate (name "add_one_hammahutz") (vers "0.1.0") (hash "0la1ir74szc4yrazgz2afd3sgzg0j4s653s24n0xnh7wn498lclp")))

(define-public crate-add_one_hammahutz-0.1 (crate (name "add_one_hammahutz") (vers "0.1.1") (hash "01s36c81rdrf3frzmfg9r8wfgaj5y8ha2yby5bnp2yijnrkavd13")))

(define-public crate-add_one_hiyuki-0.1 (crate (name "add_one_hiyuki") (vers "0.1.0") (hash "17gp701pz5wbs99cd6wqvwgi9aclazrskj8bsajfmjfir7z140n1")))

(define-public crate-add_one_john-0.1 (crate (name "add_one_john") (vers "0.1.0") (hash "01xg53afrzzk8s9l1d5kqkz008zbjam81c9b0m2ig764n2f5sv4s")))

(define-public crate-add_one_john-1 (crate (name "add_one_john") (vers "1.0.0") (hash "1jihq2cyzjinfhlqypyz4gvqq1pfw3ia3kxca6bqrvjplfiky627") (yanked #t)))

(define-public crate-add_one_test-0.1 (crate (name "add_one_test") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "18i0l9k9p41idalyfds5l8852zdh0k8z0m1wmzgfi44xl1h9pdwg")))

(define-public crate-add_one_test_1129-0.1 (crate (name "add_one_test_1129") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "09c390ka9qcn2lqxiw7byb66bzwm9bfb7bbhn1plz6l621jl9pmc")))

(define-public crate-add_rust-0.1 (crate (name "add_rust") (vers "0.1.0") (hash "1impkjvzx99cpd4s76cbb9ahdfgxpgkm5z48ravznhnrrcxcypiw")))

(define-public crate-add_rust-0.1 (crate (name "add_rust") (vers "0.1.1") (hash "1d7r2d1azh23p0mr1mmfgg1ary004ysmw1vqd52bcq8hvn0a7yn9")))

(define-public crate-add_test-0.1 (crate (name "add_test") (vers "0.1.0") (hash "0zxn6hfz0addg13ami858whgb5na5dpcc3bvwfwd9q44m1bn0mgr")))

