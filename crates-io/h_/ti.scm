(define-module (crates-io h_ ti) #:use-module (crates-io))

(define-public crate-h_time-0.1 (crate (name "h_time") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0iyldgg9r8yks874znhzpjpsl3syals6xrc1c48dy29j2n98q2rq")))

