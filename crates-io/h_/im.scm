(define-module (crates-io h_ im) #:use-module (crates-io))

(define-public crate-h_image-0.1 (crate (name "h_image") (vers "0.1.0") (deps (list (crate-dep (name "font-kit") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "pathfinder_geometry") (req "^0.5") (default-features #t) (kind 0)))) (hash "1lvzi4rjapslbixcssidvy1wk0pnid9sn52gss7k375f64qba2ij")))

