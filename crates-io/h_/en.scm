(define-module (crates-io h_ en) #:use-module (crates-io))

(define-public crate-h_encrypt-0.1 (crate (name "h_encrypt") (vers "0.1.0") (hash "19f64q8bpv7qvk1i0c1qzhffng7ihy6c8as188bjp2lm28k6c4wx")))

(define-public crate-h_encrypt-0.1 (crate (name "h_encrypt") (vers "0.1.1") (hash "06nj0sj6m4lhh9hnlf3jmjf4ngqnazwgxlkim8cy4iiaqx0clcbl")))

(define-public crate-h_encrypt-0.2 (crate (name "h_encrypt") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0kkldq9jdj0bz7j79yqnw7ibl978g3ajv7hykmkghcvrrmaacd2v")))

(define-public crate-h_encrypt-0.2 (crate (name "h_encrypt") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0w2fk893b3vwqxd56vpq6a6wp24cwahfr7glpk13gqxx071i5i6v")))

(define-public crate-h_encrypt-0.2 (crate (name "h_encrypt") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1x0p96dlgl19r8racrhza8brivngc1hn1mpbrm8gzswy58qy3jpd")))

(define-public crate-h_encrypt-0.2 (crate (name "h_encrypt") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1xyfdclxs5cids56myxy64wxvnh0iwzbnp33n52746pfzxk9xjna")))

