(define-module (crates-io h_ mo) #:use-module (crates-io))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.0") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "01if7g9c9yr06x7vr1fwzplg6i99vg39lhnxgnna1m0xihnsms4x") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.1") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "15xc0wrp6i1fjlmbkg3nfsqnj9m3a5wwm33n5fp89l95bgwhwczc") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.2") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0hyqvi998b4zd9rslhs1f254vpznygr7g044p8r7jxqnfwl87vvm") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.3") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "029jiynym203azn8z5mhpandjpz444cw5ylvvwyg1qy6cbjf28a3") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.4") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1y630z42wrxh0z7ncjafpmx6ykiqxmi8488nc9r5afbgn92h602q") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.5") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0jz0n57l1i9p4gkdlgmr48ajn0jnwb2p92vv9jc9l70k5skwz96y") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.6") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "175k8gasww7pi9qijn1x43c56h5pdhg86jfylg4wj5p1cjwcvvbh") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.7") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0f5g46iv11fpncdx39r4a0p10qrgx5mi5hja9s2g9275h77fxmnl") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.8") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "00r5fy8dsi263swplcp2yggcgw6ldgpw2hhpk39c6r06abflcppn") (yanked #t)))

(define-public crate-h_modals-0.1 (crate (name "h_modals") (vers "0.1.9") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "030m4fdfjp7ashs9981vh8pcxjvsb0n3wxa46kjk5b3c115gsxiy")))

(define-public crate-h_modals-0.2 (crate (name "h_modals") (vers "0.2.0") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1alw1g70m9lpfmkqq2dlkaq23zmfj0kglv0brssk3a64dk98zp5b") (yanked #t)))

(define-public crate-h_modals-0.2 (crate (name "h_modals") (vers "0.2.1") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "063v6mm9izdy2q6rx5d0vs0sk5kq7d5rbn7gxgspk7iipfxqzw9z")))

(define-public crate-h_modals-0.2 (crate (name "h_modals") (vers "0.2.2") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0rkhpbrkk5y1bdddz91kirl9l1x3s6qswnhlxrwz498w42liijfw")))

(define-public crate-h_modals-0.2 (crate (name "h_modals") (vers "0.2.3") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "022ni43rh4kknndmlm39g0ys816zy1n2idxmvp6izbz3pkx8vv1j")))

(define-public crate-h_modals-0.2 (crate (name "h_modals") (vers "0.2.4") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0xadw3pwaqw2g71w47hf8x80hzwi4lzxksbk4wgr18p10g9wwpkz")))

(define-public crate-h_modals-0.2 (crate (name "h_modals") (vers "0.2.5") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "05z48vvpwi810m1jdy8pxcarsp0w63hv3hkj0hv65jlv35mh2irq")))

(define-public crate-h_modals-0.2 (crate (name "h_modals") (vers "0.2.6") (deps (list (crate-dep (name "leptos") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0j0v23whjfsjqdqvyhgk9xsajdpxq7h1rmrrpx6hjlsaia2lyzrb")))

