(define-module (crates-io h_ ma) #:use-module (crates-io))

(define-public crate-h_mat-0.1 (crate (name "h_mat") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14f346283y16hl6raadillwyxi6iw5fkfk0vyc1bivj83dvnkavb")))

(define-public crate-h_mat-0.1 (crate (name "h_mat") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ind889kfwl0dw0xab379fg08fcvn59y2ch48nbj1ysndhm1dnzj")))

(define-public crate-h_mat-0.1 (crate (name "h_mat") (vers "0.1.11") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08lg3zc01f373ip40y8hgd6h1z288xk9sz2537y6902ygb3q32q0")))

(define-public crate-h_mat-0.1 (crate (name "h_mat") (vers "0.1.12") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "113lyx8pkfm677sw0sjh6q5nii1c6mbn9d1sv22270cd2aqjrghf") (yanked #t)))

(define-public crate-h_mat-0.1 (crate (name "h_mat") (vers "0.1.13") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hr8y1i4x2h8pw21ya5xahj5yki08ln59kqysnx5rrm6dxi2ddir")))

(define-public crate-h_mat-0.1 (crate (name "h_mat") (vers "0.1.14") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.166") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1baynpgr08xfzclmlw1pjfw722hqa1kgnpvag2syf7r24l6p1lhk")))

