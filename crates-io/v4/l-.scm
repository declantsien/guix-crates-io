(define-module (crates-io v4 l-) #:use-module (crates-io))

(define-public crate-v4l-sys-0.1 (crate (name "v4l-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "12d5ypvaa1kmcbiyxpw5lc4d691b93r4kpj4iffiwk7lkwk42g81") (links "v4l1 v4l2 4lconvert")))

(define-public crate-v4l-sys-0.2 (crate (name "v4l-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)))) (hash "1zj78arn0pani67qhqgbz4p8r9j3spr998bvmyha3838jrzw87gr") (links "v4l1 v4l2 4lconvert")))

(define-public crate-v4l-sys-0.3 (crate (name "v4l-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "0c7sjm9wzqlaf5dsf5y98ck618j6ib0wjibr25nygw3njjdlw88n") (links "v4l1 v4l2 v4lconvert")))

