(define-module (crates-io v4 v-) #:use-module (crates-io))

(define-public crate-v4v-types-0.1 (crate (name "v4v-types") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0ikzifbpgrw5lb325nhq40p2yrck5ambfhgds957sirl5lx9mqkm") (yanked #t)))

