(define-module (crates-io v4 l2) #:use-module (crates-io))

(define-public crate-v4l2-sys-1 (crate (name "v4l2-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.31") (default-features #t) (kind 1)))) (hash "0wf77jqn9bghrlx7nfbfyma54grgk3pzn6kzncc9k2ryld5kq7bp")))

(define-public crate-v4l2-sys-1 (crate (name "v4l2-sys") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)))) (hash "1fcvqn15sm312i47psbml76r4n9d6436yy0h78rny03ssg0aba4h")))

(define-public crate-v4l2-sys-1 (crate (name "v4l2-sys") (vers "1.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "rstest") (req "^0.6.3") (default-features #t) (kind 2)))) (hash "0mlzh4qay8ml4slpv65ydqj0zx4aqwpy7cb7kwam2i3r5gjighni")))

(define-public crate-v4l2-sys-mit-0.1 (crate (name "v4l2-sys-mit") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "0j75mc4jrqimq5k246r8l88w96gh5gfzsgghx1649nq3gagyi031")))

(define-public crate-v4l2-sys-mit-0.2 (crate (name "v4l2-sys-mit") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)))) (hash "0ki535aipfnwvbzk7zwn8mw96yd8y8a44q4z4bxivbzldp035jg0")))

(define-public crate-v4l2-sys-mit-0.3 (crate (name "v4l2-sys-mit") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "02wr26prs8x5qirnax7g6y42wqb9prmcgslkg3fcmfmrca1qfyb7")))

(define-public crate-v4l2loopback-0.1 (crate (name "v4l2loopback") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "nix") (req "^0.26.2") (features (quote ("ioctl"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "01jbrlgcmn8k3a91hsyy2aanx71skjvnxyhrc58cmqn0j1rf465v")))

(define-public crate-v4l2r-0.0.1 (crate (name "v4l2r") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "ctrlc") (req "^3.1.4") (default-features #t) (kind 2)) (crate-dep (name "enumn") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("ioctl" "mman" "poll" "fs" "event"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "001pwidarwf5zl2l7qx323jmg8bkmmqikbhi1k32q3msv6yxh0pc")))

(define-public crate-v4l2r-0.0.2 (crate (name "v4l2r") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "ctrlc") (req "^3.1.4") (default-features #t) (kind 2)) (crate-dep (name "enumn") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("ioctl" "mman" "poll" "fs" "event"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "004axcdim5nk41md25z6p588hx15pwk1caflv1i889dgc534l7wh")))

