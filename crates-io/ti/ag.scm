(define-module (crates-io ti ag) #:use-module (crates-io))

(define-public crate-tiago_functions-0.1 (crate (name "tiago_functions") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0vmmfvfbdlpa6wfmgvp5khc8pf8jgliykycrmac70fbg4ihp32rz")))

