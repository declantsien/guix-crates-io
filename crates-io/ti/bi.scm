(define-module (crates-io ti bi) #:use-module (crates-io))

(define-public crate-tibitset-0.1 (crate (name "tibitset") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0xqca3h56izhyb3daa9fg3ck807dklnkq9f94yvz7zspr51byv5c") (features (quote (("std") ("default" "std"))))))

