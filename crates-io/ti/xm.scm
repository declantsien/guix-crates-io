(define-module (crates-io ti xm) #:use-module (crates-io))

(define-public crate-tixml2svd-0.1 (crate (name "tixml2svd") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "19bsx7rbd5wv7dll8dqg8mifxj88iiwcj2zjga20vkjaanm28zl8")))

(define-public crate-tixml2svd-0.1 (crate (name "tixml2svd") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.31.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1c5im6fz5jn22zz1pqnfcphwqiqyy240qk4k7gqs5k9l2hlfv498")))

(define-public crate-tixml2svd-0.1 (crate (name "tixml2svd") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.31.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0z90pf994l9ba383gc3bipb5rkdrg4qrzwjbc92h13766pdr13z5")))

(define-public crate-tixml2svd-0.1 (crate (name "tixml2svd") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.31.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1333vv51ywm56z4yx2z6w187dmrcrlfa8khiz0479kbwbz7g1k89")))

(define-public crate-tixml2svd-0.1 (crate (name "tixml2svd") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-bom") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "19gvsxa2aljif233i8a882ggslsviisag2w9gw4y9za1z6clqmcs")))

