(define-module (crates-io ti am) #:use-module (crates-io))

(define-public crate-tiamat-0.1 (crate (name "tiamat") (vers "0.1.0") (hash "02bmwbkk36k6c2lirxlzq43ls1p9ay2nqv9f46f241s64jglbfvh")))

(define-public crate-tiamat-0.2 (crate (name "tiamat") (vers "0.2.0") (hash "1d081fns3m2d7sm67m22jva3w61x6g0xqmhhjsy8ff7bb50qw9dn")))

