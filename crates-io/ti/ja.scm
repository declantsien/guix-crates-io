(define-module (crates-io ti ja) #:use-module (crates-io))

(define-public crate-tija_tools-0.1 (crate (name "tija_tools") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v9jc5sngd54xgbx7xmml4sw2had3i8csf34fp1c8s8mlis1b03d") (yanked #t)))

(define-public crate-tija_tools-0.1 (crate (name "tija_tools") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pl427w0y7lm8lx2wfnkml89i8hjnsd1rwbdqjb353bal90rvzri") (yanked #t)))

(define-public crate-tija_tools-0.1 (crate (name "tija_tools") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lvsx55srl5j4ny7m0xi3fk8b4dyh2qcdvrhrzk1kpd5dkvp4g53") (yanked #t)))

(define-public crate-tija_tools-0.1 (crate (name "tija_tools") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07f8k6kkqvp4i8y4g16ds4ga5ixbyis8ifirgs1al3dhhp57jnv4") (yanked #t)))

(define-public crate-tija_tools-0.1 (crate (name "tija_tools") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11ax1q869qcckr4pmwxzcz317qsk3hgda6xmjsfh7n4f36vf6rfw")))

