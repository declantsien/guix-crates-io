(define-module (crates-io ti ct) #:use-module (crates-io))

(define-public crate-tictac-0.0.0 (crate (name "tictac") (vers "0.0.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "090nja2gm5idn9va2x9bb595535xdr5acmby54s2s1g7zyi82gm4")))

(define-public crate-tictacrustle-1 (crate (name "tictacrustle") (vers "1.0.0-next.1") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "0q8dscl7hs0mhrhn4i0431ds3qyh3ss067zgkzmg8d9pf8mz4ss2")))

(define-public crate-tictacrustle-1 (crate (name "tictacrustle") (vers "1.0.0-next.2") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "07vmxalazscfnk22izjrdbrff4hpl2q39dyykci3v6lsm3y3kkdy")))

(define-public crate-tictacterminal-1 (crate (name "tictacterminal") (vers "1.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0qq9n3sril9850myiplmvbnzqdns2v4qb33bl6splh38xpw6iami")))

(define-public crate-tictacterminal-1 (crate (name "tictacterminal") (vers "1.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0v64xhjlw26dbxvzalihhqapy53lavs40rkcyrwxgllf5jy77fli")))

(define-public crate-tictacterminal-1 (crate (name "tictacterminal") (vers "1.0.2") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1f03pkwh1fkcdywx9a7b017l4n5jwa4j3l2mwdaqxfq1b6hin0fq")))

(define-public crate-tictacterminal-1 (crate (name "tictacterminal") (vers "1.2.2") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0lmamn6cc6g2c0sg4cpj2ncl6r0zl7x62mr3x2xrvwf2qhy2vxsl")))

(define-public crate-tictacterminal-2 (crate (name "tictacterminal") (vers "2.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1ghsknfkrkxqwlf3imz204kqhy8caciqrdgpgpp9p0v8rb70sdvm")))

(define-public crate-tictacterminal-2 (crate (name "tictacterminal") (vers "2.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "07m5hh60xznimnqq3pnz5hbydf21qh9f6ivs2fwbxynp0l4f7saa")))

(define-public crate-tictacterminal-2 (crate (name "tictacterminal") (vers "2.0.2") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "04i5bw29lrjyvzd5mgdjyn49832brn46qzkw0m1ddkxcdqqrvbaj")))

(define-public crate-tictacterminal-2 (crate (name "tictacterminal") (vers "2.0.3") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1ndm3iydwz94ghnsgg2myv6sd1f19akqh491xcljx28dmi3dg43w")))

(define-public crate-tictacterminal-2 (crate (name "tictacterminal") (vers "2.0.4") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1h1z2fw4yicnibnawmcv9ma8y9fppfml25qz3r52d70qy347kfzh")))

(define-public crate-tictacterminal-2 (crate (name "tictacterminal") (vers "2.0.5") (deps (list (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "01dkjvsvcdmzw45frjwf575bbfwa4j1p5s9kb95fxw48lbyxnx3a")))

(define-public crate-tictactoe-0.1 (crate (name "tictactoe") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.15") (features (quote ("crossterm"))) (kind 0)))) (hash "0ljh61r7k1aznznyjir5d3vn3cqr773nhcjyb5snws50w3ky7vzw")))

(define-public crate-tictactoe-0.1 (crate (name "tictactoe") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.15") (features (quote ("crossterm"))) (kind 0)))) (hash "06c2g6zzzk3nab437659ddqh91famllsh7v8qg8bwxag7v2m122w")))

(define-public crate-tictactoe-0.1 (crate (name "tictactoe") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.15") (features (quote ("crossterm"))) (kind 0)))) (hash "14scdl6grsh7cllr7wqfprzj5zx61mhxsyddyf2f6f8ddzpw0k8c")))

(define-public crate-tictactoe-gui-0.1 (crate (name "tictactoe-gui") (vers "0.1.0") (deps (list (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1g9r09cb6g04sa3vb55iq9wlcafksvgkz389qsv5azzn4q7rfi2c")))

(define-public crate-tictactoe-gui-0.1 (crate (name "tictactoe-gui") (vers "0.1.1") (deps (list (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0x73b5n16l1k23aq7vfy9jvfaysvyi02mwfx7qmkjhwaah764mhi")))

(define-public crate-tictactoe-gui-0.1 (crate (name "tictactoe-gui") (vers "0.1.2") (deps (list (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1m2z2467hpcyrmsxki4cff3cvfdgbnkz8j9bc9rwgpdvj1r25gfz")))

(define-public crate-tictactoe-llem-0.2 (crate (name "tictactoe-llem") (vers "0.2.0") (hash "0rwjp9sm657rgnay9krxidgy62bb292dvp53cf5ri6qfh6lnz4ds")))

(define-public crate-tictactoe-llem-0.2 (crate (name "tictactoe-llem") (vers "0.2.5") (hash "16496f7bcaq8hknmh7pl0p6ry5rd8gm4aby0nwl2m10aid8h97s2")))

(define-public crate-tictactoe-rs-0.1 (crate (name "tictactoe-rs") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_graphics") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0rr289m0hc9k4wln1xglf5xgvhjya76vj7mp381vq6sn7amg4b0i") (yanked #t)))

(define-public crate-tictactoe-rust-0.1 (crate (name "tictactoe-rust") (vers "0.1.3") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "02kf5wqc3zdpv37wzpr0bjb0kkx238x66p36hp6ygrl4fizddvpa")))

(define-public crate-tictactoe-rust-0.1 (crate (name "tictactoe-rust") (vers "0.1.4") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0akfn11m0yzbdsiwy59fwapac32asyrlwwn4jb3pxl3rym1myj7j")))

(define-public crate-tictactoe_game-1 (crate (name "tictactoe_game") (vers "1.0.0") (deps (list (crate-dep (name "array2d") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rprompt") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1yw9v10zrix4whlfbvx4gjpz5xy7n8q1jfsa99g4sw7pyd3cpxzk")))

(define-public crate-tictactoe_game-1 (crate (name "tictactoe_game") (vers "1.0.1") (deps (list (crate-dep (name "array2d") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rprompt") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0iqf401v5vh2skvnxgds7r2kxxxsr9zkn3blh8x7pypr8ddq9vc0")))

(define-public crate-tictactoe_game-1 (crate (name "tictactoe_game") (vers "1.0.2") (deps (list (crate-dep (name "array2d") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "std_rng"))) (kind 0)) (crate-dep (name "rprompt") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1yw2mijs6dw0l3j63bin7whmnny8n7shcvnwg8kxgc6jvk4ahbvj")))

(define-public crate-tictactoe_menace_c-1 (crate (name "tictactoe_menace_c") (vers "1.0.0-alpha.7") (hash "08yclagy8gx1rwi4i82r9s1cxa3qqn5q4xs8dsvkg6pvkk4l93vq")))

(define-public crate-tictactoe_menace_player-1 (crate (name "tictactoe_menace_player") (vers "1.0.0-alpha.7") (deps (list (crate-dep (name "lib_tictactoe_menace") (req "^1.0.0-alpha.7") (default-features #t) (kind 0)))) (hash "0qnb2l9wwczpx50baqg67a8hi5a3gk08y717f1aifbj0a43qhdz0")))

(define-public crate-tictactoe_menace_s-1 (crate (name "tictactoe_menace_s") (vers "1.0.0-alpha.7") (hash "1lihh4al87k81q69vg23zfpkqw5zf5aky4s8r69wj5mnzdz0h83j")))

(define-public crate-tictoc-0.1 (crate (name "tictoc") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0qlqvx69z784m7hc957jvf575yzsc55cnv71lzfv75q9spg2bhwm") (features (quote (("localtime" "chrono") ("full" "chrono" "chrono/unstable-locales") ("default" "chrono"))))))

(define-public crate-tictoc-0.1 (crate (name "tictoc") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "084jrix6ps10gb3ipxy429girbphyivpzdkbwzy7ry7ypqcs97b5") (features (quote (("localtime" "chrono") ("full" "chrono" "chrono/unstable-locales") ("default" "chrono"))))))

(define-public crate-tictoc-0.1 (crate (name "tictoc") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "083d1rl98fqyd14b8jgsjqla5yj1q1625vmgwbs6bdf21niz4ak1") (features (quote (("localtime" "chrono") ("full" "chrono" "chrono/unstable-locales") ("default" "chrono"))))))

(define-public crate-tictoc-0.1 (crate (name "tictoc") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1x9lrj4kyakqmw3nqm6cgslfhm9j8kc103h2ky03b361p2km10ms") (features (quote (("localtime" "chrono") ("default" "chrono" "chrono/unstable-locales"))))))

(define-public crate-tictoc-0.1 (crate (name "tictoc") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1p59p2rmp228aya29792silbb5aycka0cjn2ar2d5zvj99nrjfdw") (features (quote (("localtime" "chrono" "chrono/unstable-locales") ("default" "chrono" "chrono/unstable-locales"))))))

