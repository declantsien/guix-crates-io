(define-module (crates-io ti li) #:use-module (crates-io))

(define-public crate-tiling-0.1 (crate (name "tiling") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.14.0") (features (quote ("png"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fg6bgm3z8ivvyjk47n3llll7qswg8c1msrz7zjl6bvvp5ld70rg")))

