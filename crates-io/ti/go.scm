(define-module (crates-io ti go) #:use-module (crates-io))

(define-public crate-tigon-0.1 (crate (name "tigon") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tungstenite") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sn4dhd19k28rp45k3r4i9xkh3xmllpkrl9cdjk4rnjch1s7q608")))

