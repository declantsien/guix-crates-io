(define-module (crates-io ti mp) #:use-module (crates-io))

(define-public crate-timpack-cli-0.1 (crate (name "timpack-cli") (vers "0.1.0") (hash "0swv8bkp798kqqg4fglm8hsf780znhi2f81bpnx39h4z5q47psx2")))

(define-public crate-timpack-cli-0.2 (crate (name "timpack-cli") (vers "0.2.0") (hash "08xc0pafv55m0qy3rxmrw0750ji4v74nmpjrmh1nshvp1jmdfn9z")))

(define-public crate-timpack-cli-0.2 (crate (name "timpack-cli") (vers "0.2.1") (hash "1aa3qxrbfrai2v0212r115nn9xfaapzkw2mrdchwm1lbhbkmf1k2")))

(define-public crate-timpack-cli-0.3 (crate (name "timpack-cli") (vers "0.3.0") (deps (list (crate-dep (name "which") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1y3pvkad8r3r6cymjsdmrvg4hjhbhby72gxrxpb25k189i52ii51")))

(define-public crate-timpack-cli-0.3 (crate (name "timpack-cli") (vers "0.3.1") (deps (list (crate-dep (name "which") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1wpinm2h8m330nnp50045is6jjr8q54gy2klrxxjpqbdgkjzd705")))

(define-public crate-timpl-0.1 (crate (name "timpl") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "timpl-decl") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "timpl-internal") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "timpl-proc") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "09kgfvny4bqwi1r6qy3jqp0hlhyw47n9q7dsaxb4alhz5azzn9y0")))

(define-public crate-timpl-0.1 (crate (name "timpl") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "timpl-decl") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "timpl-internal") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "timpl-proc") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "09sxbr0j5s1q8aslmwlhhdz11z3168jj2kcca1wsfgwq6ibfrni3")))

(define-public crate-timpl-decl-0.1 (crate (name "timpl-decl") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "timpl-proc") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "0fwyi2w9gn47m6cynbvac68nagqwv0jq82rp57sl9vhkx5fjbak6")))

(define-public crate-timpl-decl-0.1 (crate (name "timpl-decl") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "timpl-proc") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "0bhpgf8jkr60mzn4z28hh4diz322f8lan1a5rfgqg8g8dxpscpvx")))

(define-public crate-timpl-internal-0.1 (crate (name "timpl-internal") (vers "0.1.0-alpha.1") (hash "0y902qnq9sf3qwnfcn7zd1m89azai2rzikzg9x67f7yamc2rmdr8")))

(define-public crate-timpl-internal-0.1 (crate (name "timpl-internal") (vers "0.1.0-alpha.2") (hash "0kg6swnp3iglzfdy9hazr1s8n017shma7zzaza3jw4smiy561vb3")))

(define-public crate-timpl-proc-0.1 (crate (name "timpl-proc") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "timpl-internal") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "00w23h9nixf1687sw83wxx2gkz51a4p9wc4nspjbs6ndac85f3yn")))

(define-public crate-timpl-proc-0.1 (crate (name "timpl-proc") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "timpl-internal") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "188n7kimjx2pn2zib5y984a7qqrixa9r50sispq7k8dscf8vnb8v")))

