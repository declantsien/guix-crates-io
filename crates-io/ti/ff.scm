(define-module (crates-io ti ff) #:use-module (crates-io))

(define-public crate-tiff-0.1 (crate (name "tiff") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "0.10.*") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1rl4jc5cpk14lc7267s3xw5gckp9186irnyarnxi39zyh2p2im3c")))

(define-public crate-tiff-0.2 (crate (name "tiff") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "0.10.*") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1s0f4qzfd5snry7v5s24mpvxknpc7x2mmn12k2pyfabhb4iz90pp")))

(define-public crate-tiff-0.2 (crate (name "tiff") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "1.2.*") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "0.10.*") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1hmkllhgjiaraxzpwnw5f69v5k6fjkkrdcdx1b9czc9ws57nrk52")))

(define-public crate-tiff-0.2 (crate (name "tiff") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kn7psgpacns337vvqh272rkqwnakmjd51rc7ygwnc03ibr38j0y")))

(define-public crate-tiff-0.3 (crate (name "tiff") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "01vnb9d7qsjjvgdzzshavqrk2xx5rf5v0mkysj5kivgrff64qfbb")))

(define-public crate-tiff-0.3 (crate (name "tiff") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "0zgmbny2f8rssqmjdfvnysy0vqwcvlwl6q9f5yixhavlqk7w5dyp")))

(define-public crate-tiff-0.4 (crate (name "tiff") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1bbp7c4k5k808r6q070y371rllf38wcsck3dcpcb27nv53j528q0")))

(define-public crate-tiff-0.5 (crate (name "tiff") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bzzvxcx21pzryxgd7x7a1himiqs2y4k55754wzlr56sqj3qlfrz")))

(define-public crate-tiff-0.6 (crate (name "tiff") (vers "0.6.0-alpha") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cqaycgpw69k5ijj7l3hpd0bbphs44g9lgjzqbp0028893jxpsfm")))

(define-public crate-tiff-0.6 (crate (name "tiff") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "jpeg") (req "^0.1.17") (kind 0) (package "jpeg-decoder")) (crate-dep (name "miniz_oxide") (req "^0.4.1") (features (quote ("no_extern_crate_alloc"))) (default-features #t) (kind 0)) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "07d4jp2j62zfhyrfq6zi3cmyfn03i7k8jh95q0i3g5x868zlxsxb")))

(define-public crate-tiff-0.6 (crate (name "tiff") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "jpeg") (req "^0.1.17") (kind 0) (package "jpeg-decoder")) (crate-dep (name "miniz_oxide") (req "^0.4.1") (features (quote ("no_extern_crate_alloc"))) (default-features #t) (kind 0)) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ds48vs919ccxa3fv1www7788pzkvpg434ilqkq7sjb5dmqg8lws")))

(define-public crate-tiff-0.7 (crate (name "tiff") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.1.17") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yn3ffhrb9jlgdkhqgp1as0wpv98dn4fv03c1857q64pp2sll3j0")))

(define-public crate-tiff-0.7 (crate (name "tiff") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.1.17") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1cp0ivmahzi2l57d5rjjbcgff71na1na3x68vwwwxdlck6760iq2")))

(define-public crate-tiff-0.7 (crate (name "tiff") (vers "0.7.2") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.2.4") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11i2smxqa35a921pqs3x5xl7kf3cav3fhqd4xiqafiplhq4xmykw")))

(define-public crate-tiff-0.7 (crate (name "tiff") (vers "0.7.3") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.2.4") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rfbiz9ybj075iv6xdw1kmv7kdwqv3wxa2dk3qr1kqni68p6cnbj")))

(define-public crate-tiff-0.7 (crate (name "tiff") (vers "0.7.4") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.2.4") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1841879wnn7srxp6wm7lc7y69y7b0lfpim03iamy70sya4if8wcz")))

(define-public crate-tiff-0.8 (crate (name "tiff") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.3.0") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zc6z16bxkqcl79nz754i01dag66v484g09hmqcnq58a60lyyzgi") (rust-version "1.61.0")))

(define-public crate-tiff-0.8 (crate (name "tiff") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.3.0") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wg4a6w8sakyy0mggblg340mx8bgglx9hwsxsn8g5fpjkx7k6jbl") (rust-version "1.61.0")))

(define-public crate-tiff-0.9 (crate (name "tiff") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.3.0") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04b2fd3clxm0pmdlfip8xj594zyrsfwmh641i6x1gfiz9l7jn5vd") (rust-version "1.61.0")))

(define-public crate-tiff-0.9 (crate (name "tiff") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "jpeg") (req "^0.3.0") (kind 0) (package "jpeg-decoder")) (crate-dep (name "weezl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ghyxlz566dzc3scvgmzys11dhq2ri77kb8sznjakijlxby104xs") (rust-version "1.61.0")))

(define-public crate-tiff-encoder-0.1 (crate (name "tiff-encoder") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "1br06yj3x4jw04vcck8hlr5n3h3ldzj3355340wc66sfmbh8zdpc")))

(define-public crate-tiff-encoder-0.1 (crate (name "tiff-encoder") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "0vpc1zq1nv794nj2s8p02c170g9px5i9d6vvqfyz01xj4r2i70yi")))

(define-public crate-tiff-encoder-0.1 (crate (name "tiff-encoder") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "1r0awwaawd2dlivbp4f2r7dk51jx029iad6g153sjmnl56isa6sn")))

(define-public crate-tiff-encoder-0.2 (crate (name "tiff-encoder") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "0xaj03raz6fwivnsakiwzipjdm584sg66yzvngcjmvjbhgv6m4ly")))

(define-public crate-tiff-encoder-0.2 (crate (name "tiff-encoder") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "08343jw61q102r4arl1l66zbbv8j4vbpnqvi1nw76m980pjdw5iw")))

(define-public crate-tiff-encoder-0.3 (crate (name "tiff-encoder") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0vlhf17dn1zbiprwiq97qh1amfjyzsbwa3nbkw8igsqfly5brn5l")))

(define-public crate-tiff-encoder-0.3 (crate (name "tiff-encoder") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1dqhx8w623q1287ca3887f5za52qjvzj41asx7zvs587qsrh3vs4")))

(define-public crate-tiff-encoder-0.3 (crate (name "tiff-encoder") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0r44jpvr7s4bm3k85h58c3md9gnw64nqj2i5ss4drzjrip7b9pmy")))

(define-public crate-tiff_tags-1 (crate (name "tiff_tags") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1z9i2z2xc4djc94gzbh0nycpcn8c5n5rprkp6r28hzwgkspi2zll") (yanked #t)))

(define-public crate-tiff_tags-1 (crate (name "tiff_tags") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1xqrrnrijwfddxnqcy6j8s72w5yw38rdqh4gznnf1y846grhkd9a")))

(define-public crate-tiff_tags-1 (crate (name "tiff_tags") (vers "1.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1hinfcl23favwlfmv2mp25fz6dkv3sgnangi83pnl1rd13i592d1")))

(define-public crate-tiff_tags-1 (crate (name "tiff_tags") (vers "1.3.0") (hash "0z6zq0z9wr24bgf3gjij2n39ravib24w45hjw7nrmjzmq2g48rcf")))

(define-public crate-tiff_tags_nightly-1 (crate (name "tiff_tags_nightly") (vers "1.0.0") (hash "0gwaa594gdj9g2fqzblvmcaphfcpqdchh9qppp23zl186ar7mxx7") (yanked #t)))

(define-public crate-tiff_tags_nightly-1 (crate (name "tiff_tags_nightly") (vers "1.1.0") (hash "06hfcj5mx2pv178mq6684y1gi2bzgcjcsl0majcm6a39c94805lf") (yanked #t)))

(define-public crate-tiff_tags_nightly-1 (crate (name "tiff_tags_nightly") (vers "1.1.1") (hash "1hi0wcbs0hwzljg7aspcg97lgxdbx39sbffva4mi6v112qz4368s") (yanked #t)))

(define-public crate-tiff_tags_nightly-1 (crate (name "tiff_tags_nightly") (vers "1.2.0") (hash "00457i9srwb3c2y2jdv9hbihnwr3gq111423sqmq9g5chhf85jpr") (yanked #t)))

(define-public crate-tiffin-0.0.1 (crate (name "tiffin") (vers "0.0.1") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("fs" "env" "dir" "user" "mount"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0mldqsi1n3d022jjl6m65jg8fkfdfcffg0zy426bcgisfffqd92s")))

