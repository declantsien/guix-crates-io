(define-module (crates-io ti mf) #:use-module (crates-io))

(define-public crate-timfmt-0.2 (crate (name "timfmt") (vers "0.2.0") (deps (list (crate-dep (name "cli-table") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "grep-cli") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1q6sz04s0x9wp6plsccrn3ikxyzf1a8lql1njfcna957mvwp3i3k")))

