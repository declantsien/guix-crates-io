(define-module (crates-io ti nf) #:use-module (crates-io))

(define-public crate-tinf-0.10 (crate (name "tinf") (vers "0.10.0") (hash "1ik3d3mdyq9y4q19nyzjw2kdrjg0b1az6cvz1ck2nxdm8xf76wpb")))

(define-public crate-tinf-0.11 (crate (name "tinf") (vers "0.11.0") (hash "059308jyf4w27w7vrcvjz7kvjinqvnm1kjsxk34fnv85iyfy5nip")))

(define-public crate-tinf-0.11 (crate (name "tinf") (vers "0.11.1") (hash "0rzhcq11rnj2hzrq291zcwvv6wmkpsprz93qzwcs46f79p4p48cm")))

(define-public crate-tinf-0.11 (crate (name "tinf") (vers "0.11.2") (hash "0rycs8nm4zy6ac5h5h2qxddy7crrfqyw08d4zck4a6dc7z6mbkmw")))

(define-public crate-tinf-0.12 (crate (name "tinf") (vers "0.12.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1nc9q3c5k3r8phrv7hpzw6ivna95h4j90d3b49rqilqav9i8mpqp")))

(define-public crate-tinf-0.12 (crate (name "tinf") (vers "0.12.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1x0qpnhz8hd75byf61r7158c0l6wi9ksyv21vzmgmr3z4475ax90")))

(define-public crate-tinf-0.12 (crate (name "tinf") (vers "0.12.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1qpa2qjgcy938r47sgy32rn7l5j6q7czh52w01jdr4x14j37h3ih")))

(define-public crate-tinf-0.12 (crate (name "tinf") (vers "0.12.4") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0nhz8idfsypc59lggslrpghmn2f2kixss7gld6zjmp4mcsxhqb10")))

(define-public crate-tinf-0.12 (crate (name "tinf") (vers "0.12.5") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "06d6hab93ghkpj3bq8gdz07pkvvsh8m07zssmn5cc7saq15swx9w")))

(define-public crate-tinf-0.13 (crate (name "tinf") (vers "0.13.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0959v6z79yd0g4a2vcnd7vwvm19wpcfg6ay6lgdl8i05ma1rmkv1")))

(define-public crate-tinf-0.14 (crate (name "tinf") (vers "0.14.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0gd1cdyiaybvpv6wvjxxg87sxvlxh9zlg4bn2ibyys4fqlpsz37l")))

(define-public crate-tinfo-0.0.1 (crate (name "tinfo") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "0ahnrxbkdnsf2vak85hzlch9qwl7imlmksxs04i1a7k6xq8bizhs")))

(define-public crate-tinfo-0.1 (crate (name "tinfo") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)))) (hash "0jc6k9pmdwk2andd2lxqznf14hilyfjxv4lvwqmvc6f3g48f8yk0")))

(define-public crate-tinfo-0.2 (crate (name "tinfo") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)))) (hash "1wdyxpl4sqi07c51mr8j1xmahm5q7sls0qyfrdmzmhaa2c7lnsv0")))

(define-public crate-tinfo-0.3 (crate (name "tinfo") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)))) (hash "12lijk3bf2pp9mqz0vpr94a5gk38cn8lgyibqggz7db5jbjrq1y6")))

(define-public crate-tinfo-0.4 (crate (name "tinfo") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)))) (hash "1jmv9vdf3jvx8qk6mq2i4hph5yylhnrahma3qjbjcq68f5xx66ir")))

(define-public crate-tinfo-0.5 (crate (name "tinfo") (vers "0.5.0") (deps (list (crate-dep (name "getopts") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "0.*") (default-features #t) (kind 0)))) (hash "0v5w39yn0wz4hhz0d79cf7lki8dmxr13afnaffn6nfl35aaj3zra")))

(define-public crate-tinfo-0.6 (crate (name "tinfo") (vers "0.6.0") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0syn7nicdrc6mdaq6mkpybfg5hkshy1y9aglmghy58hx5j1fcyk4")))

