(define-module (crates-io ti s6) #:use-module (crates-io))

(define-public crate-tis620-0.0.1 (crate (name "tis620") (vers "0.0.1") (hash "1kllnn7ap1rc65gansasn1sinlgzqvwwrazdy6xwkir3ginq5m9y")))

(define-public crate-tis620-0.0.2 (crate (name "tis620") (vers "0.0.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "17calnvqcb1yj3b3vkr3n3fwwcm8mj4znvzd8j23fbfliw6qmb2f")))

(define-public crate-tis620-0.0.3 (crate (name "tis620") (vers "0.0.3") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0iqsdq0av1kwabfa8qbqmk1gg7fh4x356w82nvvgkx7358563dm4")))

(define-public crate-tis620-0.0.4 (crate (name "tis620") (vers "0.0.4") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1xlhz078aqbblvx9mn20rkcr44ppqbkaqmn2hpzy98qnjgz7r70k")))

(define-public crate-tis620-0.0.5 (crate (name "tis620") (vers "0.0.5") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0fqjn7pafv6vw6pfp4plqrcla965rrxyfrmlbzb1id758dlrmhmi")))

(define-public crate-tis620-0.0.6 (crate (name "tis620") (vers "0.0.6") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1gf5nz7vn0dgfa9i7hykdmqhdma9xp7arm09nw8rhj3kh9074155")))

(define-public crate-tis620-0.0.7 (crate (name "tis620") (vers "0.0.7") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1akq3d35pn2z64xyr70lp7mw43w0inglgg2lmql4wz9qnjgh1c1i")))

(define-public crate-tis620-0.1 (crate (name "tis620") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0kiz4v7higbm4qb4fbjwpw7yn9yyfwcv92527y62ky5iggh321ly")))

(define-public crate-tis620-0.1 (crate (name "tis620") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "encoding_rs") (req "^0.8.30") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "15r9xmdwq47mqz15nr4s6v7rk4g6kq5m6vkzqlzbr0p7ijvhmbwd")))

