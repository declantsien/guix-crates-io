(define-module (crates-io ti ni) #:use-module (crates-io))

(define-public crate-tini-0.1 (crate (name "tini") (vers "0.1.1") (hash "07wskcs9l5x42xphbajnsnsfsmq508yfvfyba286yrajn3qq1fkn")))

(define-public crate-tini-0.2 (crate (name "tini") (vers "0.2.0") (hash "0bhhmzxha48sx1hrzmbwi6yfni1516qdkbmsmhmdyxkk4rlamvhi")))

(define-public crate-tini-0.3 (crate (name "tini") (vers "0.3.0") (hash "1brbi3vp7xzyh1bi6fg225sqaf02dnnxagm6ldf8q0f7l08y35c5")))

(define-public crate-tini-0.4 (crate (name "tini") (vers "0.4.0") (hash "1a1qmxj0z12ndh20fl57zv26ak8j05d62f7kvbzamyixw3sqppji")))

(define-public crate-tini-0.9 (crate (name "tini") (vers "0.9.0") (hash "04irg0miank0cxvdkhxqz0xpcqd6sdbwpiqcsx0phmsc6rpcx23f")))

(define-public crate-tini-1 (crate (name "tini") (vers "1.0.0") (hash "0gg8sg4g1lgswnr43vjnzkq4155fygjvabxh3cayzgvwk4pji92n")))

(define-public crate-tini-1 (crate (name "tini") (vers "1.1.0") (hash "0w16bb38js4axqz6kq809sd9nyk1smdysdmv9xa10n5qwi2k4q59")))

(define-public crate-tini-1 (crate (name "tini") (vers "1.2.0") (hash "057pvrvi0sl6v37nz99s34s4qm7lag9w3c878nlpy3yzf8fprmg9")))

(define-public crate-tini-1 (crate (name "tini") (vers "1.3.0") (hash "0njnsm9klxbix0lvwha7j9nkm96g2jjh8cl8amgyn188bx6dy170")))

(define-public crate-tiniestsegmenter-0.1 (crate (name "tiniestsegmenter") (vers "0.1.0") (hash "070irb363crnxys5zqidwm6v79zqsz9d1gyp9bv37wf7pv515p9g") (yanked #t)))

(define-public crate-tiniestsegmenter-0.1 (crate (name "tiniestsegmenter") (vers "0.1.1") (hash "0zrhl6wg8qq7sbqddhi4nswlnhgd41n3r6h4d6z588llzyq8zjca")))

(define-public crate-tinify-rs-0.1 (crate (name "tinify-rs") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "17z8xiiph2vys3rgxsp49pjn9i6k3rcq685nj9ff17kfsnx9j4zl")))

(define-public crate-tinify-rs-0.1 (crate (name "tinify-rs") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1pssnmg8r9xrdl4c6pcijnnad56l0cprakf49b00cykk6nf74nhw")))

(define-public crate-tinify-rs-0.2 (crate (name "tinify-rs") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "01qk6r2axk40xyykdiqav11j4fbvsc1l0nll7sbmp0b4bj87436c")))

(define-public crate-tinify-rs-0.2 (crate (name "tinify-rs") (vers "0.2.1") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0adssir54b6rbx58qjvdlcv7avn8c5ds2hnjfliprh45p4ziq0ic")))

(define-public crate-tinify-rs-1 (crate (name "tinify-rs") (vers "1.0.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1k7i5mznqij01zvxfqx9kk97r5n52ymrn61bx0p0qpzk6x16m9al")))

(define-public crate-tinify-rs-1 (crate (name "tinify-rs") (vers "1.0.1") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "05raxx0mfx2i4phfz573asf8lx530zj9dnhyf4cynh5fmymzd5w7")))

(define-public crate-tinify-rs-1 (crate (name "tinify-rs") (vers "1.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "imagesize") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.149") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.149") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (features (quote ("alloc"))) (kind 0)))) (hash "0ik0xvqw6fpx41sfjjr90bxp66an01sjnj49aqpkkg8zpj0c6lbs")))

(define-public crate-tinify-rs-1 (crate (name "tinify-rs") (vers "1.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "imagesize") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.149") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.149") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (features (quote ("alloc"))) (kind 0)))) (hash "1vabav5w30nrclwycmz78hsnzrfn4f5qn7293pyfi56nmrznk2qa")))

(define-public crate-tinify-rs-1 (crate (name "tinify-rs") (vers "1.3.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "imagesize") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.149") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.149") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "1a8rv0ygky9vf7cgyfrraqnhznjxq5j451l8fl78k31r3wy2rp5w") (v 2) (features2 (quote (("async" "dep:tokio"))))))

(define-public crate-tinify-rs-1 (crate (name "tinify-rs") (vers "1.4.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "imagesize") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.149") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.149") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0ihxlkqy1sincr2f35gklxm2mirx0aa17hhnnya4gnsw97683sp8") (v 2) (features2 (quote (("async" "dep:tokio"))))))

(define-public crate-tinify-rs-1 (crate (name "tinify-rs") (vers "1.4.1") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "imagesize") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.149") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.149") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "06i047knhkmpimvym09wqji8lpdx2rznc13wya1c9rlvhinhx1vx") (v 2) (features2 (quote (("async" "dep:tokio"))))))

