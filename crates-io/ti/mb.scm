(define-module (crates-io ti mb) #:use-module (crates-io))

(define-public crate-timba-0.0.0 (crate (name "timba") (vers "0.0.0") (hash "1y7m1m8gyl6ik5hnxryqnby46wgr3qhx8hda27aljk6g91bhah6z")))

(define-public crate-timber-0.0.1 (crate (name "timber") (vers "0.0.1") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0n51rg1rmqp054xmgbv64kr0fgys9nybhiy3l9aqysi2x82lqdxb")))

(define-public crate-timber-0.1 (crate (name "timber") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1p06993gkicd15zjrqy1wmglhdlri00z23ymm4d9wg2clma9pwqj")))

(define-public crate-timberwolf-0.2 (crate (name "timberwolf") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "04vw5i5iq0h6ia02kd9nqnifh6bpr4gnayh7qk40d2b0fz9lz0ji")))

(define-public crate-timberwolf-0.3 (crate (name "timberwolf") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0p6z0lig9vkgiaayxc9m8q2zw0h9ay3n1zxarwkp5f26pydk9i5q")))

(define-public crate-timberwolf-0.3 (crate (name "timberwolf") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "101frj4d3xd1xq710c9zrnz4qj2fvfmapx84x71x9rknmzxbbx9d")))

(define-public crate-timberwolf-0.3 (crate (name "timberwolf") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0qnzcgjcdnm5x95vpp8j5w25ng264sg75a6w8j4k7z7r16h82ckf")))

(define-public crate-timberwolf-0.4 (crate (name "timberwolf") (vers "0.4.0") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "12k3zm502cbfrimmrdm5ndfqxapkqp2yiqv9flyxpfrlrf809c8p")))

(define-public crate-timberwolf-0.5 (crate (name "timberwolf") (vers "0.5.0") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rendy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.19.5") (default-features #t) (kind 0)))) (hash "03pf1qaizyafphdgn12n7yldhvp12pqmjfd4mzbx6yvjfx0dga0f")))

(define-public crate-timbre-0.1 (crate (name "timbre") (vers "0.1.0") (deps (list (crate-dep (name "sdl2") (req "^0.34.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "186q5kx48lk708qj5p4wwiyqd6v59nh7azcwwz83nf43p49vavbd")))

(define-public crate-timbre-0.1 (crate (name "timbre") (vers "0.1.1") (deps (list (crate-dep (name "sdl2") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1hygq3q9ghx0ch2i0wdmffg4nvw1k5fqqragzjdxm30vl3fcv2qf") (features (quote (("default" "sdl2/bundled"))))))

(define-public crate-timbre-0.1 (crate (name "timbre") (vers "0.1.2") (deps (list (crate-dep (name "sdl2") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1j3ccqm306wgb93kyvv985vninwck9px0ahbzfa8fji5xv5n16l0") (features (quote (("default" "sdl2/bundled"))))))

(define-public crate-timbre-0.2 (crate (name "timbre") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "sdl2") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "tracing-chrome") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.2.11") (default-features #t) (kind 2)))) (hash "1qb2i7qsaicpm05dwjfpanfrzgza6mnjc84gd5k9b8qrf4gi4fdk") (features (quote (("default" "sdl2/bundled"))))))

(define-public crate-timbre-0.3 (crate (name "timbre") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "sdl2") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2-sys") (req "=0.34.2") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "tracing-chrome") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.2.11") (default-features #t) (kind 2)))) (hash "1iksdb0rxinzi96z3b7ca500pmnl327lk0ycg3lah0kxiirs136i") (features (quote (("default" "sdl2/bundled"))))))

