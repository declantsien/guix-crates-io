(define-module (crates-io ti nd) #:use-module (crates-io))

(define-public crate-tinder-0.0.1 (crate (name "tinder") (vers "0.0.1") (hash "1qqdcppqd8k0d7qsw4v5c9qv440bq3aashs0mykclj703jfwabv4")))

(define-public crate-tindercrypt-0.1 (crate (name "tindercrypt") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "0.11.*") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "0.11.*") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "2.33.*") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "1.3.*") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "1.0.*") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "2.7.*") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "2.7.*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "0.14.*") (default-features #t) (kind 0)))) (hash "1bnzyvvgpcdjrlxkl7283cmr9b9pb7nkng8302j0khzd0kc88kb4") (features (quote (("proto-gen" "protoc-rust"))))))

(define-public crate-tindercrypt-0.1 (crate (name "tindercrypt") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "0.11.*") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "0.11.*") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "2.33.*") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "1.3.*") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "1.0.*") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "2.7.*") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "2.7.*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "0.14.*") (default-features #t) (kind 0)))) (hash "0va8hb9d55r1v6i62x3hhn5xghnprv0ra96g0hqczmpmi3dlhn12") (features (quote (("proto-gen" "protoc-rust"))))))

(define-public crate-tindercrypt-0.2 (crate (name "tindercrypt") (vers "0.2.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)))) (hash "1wd6986a0iidmkcfgvsssnwagr9rcvjzwwdj4pli7wg80jdc5wkj") (features (quote (("proto-gen" "protoc-rust"))))))

(define-public crate-tindercrypt-0.2 (crate (name "tindercrypt") (vers "0.2.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)))) (hash "0859zxma4ad0rknf1sw64l1j8w0kspp3xlx36n1hkfnmhf3pgkw7") (features (quote (("proto-gen" "protoc-rust") ("default" "cli") ("cli" "clap" "dialoguer" "lazy_static"))))))

(define-public crate-tindercrypt-0.2 (crate (name "tindercrypt") (vers "0.2.2") (deps (list (crate-dep (name "assert_cmd") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0vav6zflmp3pibq3kbpryglklr6cqp7a7s19pxxg4rdjip6dakzk") (features (quote (("proto-gen" "protoc-rust") ("default" "cli") ("cli" "clap" "dialoguer" "lazy_static"))))))

(define-public crate-tindercrypt-0.3 (crate (name "tindercrypt") (vers "0.3.0") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (default-features #t) (kind 0)))) (hash "1asm7xwjmw75nkav7z2a9mq2dhvhi8s2c7pwgwhkyz17hc3d8g70") (features (quote (("proto-gen" "protoc-rust") ("default" "cli") ("cli" "clap" "dialoguer" "lazy_static"))))))

(define-public crate-tindercrypt-0.3 (crate (name "tindercrypt") (vers "0.3.1") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (default-features #t) (kind 0)))) (hash "1b1ahlb56vqyhfa3kzxlzy6ps2lnlrhzc67glxqhqzsmk00gklpb") (features (quote (("proto-gen" "protoc-rust") ("default" "cli") ("cli" "clap" "dialoguer" "lazy_static"))))))

(define-public crate-tindercrypt-0.3 (crate (name "tindercrypt") (vers "0.3.2") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (default-features #t) (kind 0)))) (hash "04vmpbmjbvppm16bcii5cz0lvbrzifvf34i3jjla9lz58a24cl1h") (features (quote (("proto-gen" "protoc-rust") ("default" "cli") ("cli" "clap" "dialoguer" "lazy_static"))))))

(define-public crate-tindex-0.3 (crate (name "tindex") (vers "0.3.0") (hash "0kk8wiypa50ycmrmj20vkgw2z94m0cx61qh1v3jyhn9zbjihkba7")))

(define-public crate-tindex-0.3 (crate (name "tindex") (vers "0.3.1") (hash "1yryvj1yj59yw25dz5srawyr8ibkfimlg8sq253iswxgwzkf5f39")))

(define-public crate-tindex-0.4 (crate (name "tindex") (vers "0.4.0") (hash "0fpqwb18vg0jl6075dfivq9vnyml8s11zm68y0lgxzl7kahc6f8c")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.0") (hash "0k5db159qkmrvfr05zf69c9jw36qvbq0r94qc0imx1xzsflz5v7k")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.1") (hash "04yf418v3aqz76qhb44mvnv1cl53qsrpn046j1diypw214lfyxhr")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.2") (hash "027r6cgmxk0zcbz71k08rz7h4v2ji9aa2gmjzds122im99pk0zkw")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.3") (hash "1vdqajm3596fai72b7irk864iqqb7z5x85b7w5amrl2v0fa8lkqm")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.4") (hash "11n554hxvlvik0mhybdvnwcgyhb424zl7ibv8s6s6scnmgbfzab6")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.5") (hash "119iks3bc8mqcgb4flxqib9xzh4svs10nsxjz80vzkb34z9wrgwh")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.6") (hash "1r5kl0rpx29ngyq3fnvhy084zjwwm7mmvzvkpbf5f54y0cfh3ky2")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.7") (hash "1ralrn96smny0rc8pgzv3lg9fwgcxmxx50bd8c06m0hvfvg7ypfs")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.8") (hash "1dgd0r84cp9qmsvgnc60frc9rwm163wgngw0ki1mcfziszw6an7p")))

(define-public crate-tindex-0.5 (crate (name "tindex") (vers "0.5.9") (hash "1w7706gqzqz3k7mqid5rvchw1jc691m56ds3ay6mr9m56gdl1xyj")))

(define-public crate-tindi-0.1 (crate (name "tindi") (vers "0.1.0") (hash "1h1w7wl9dg9fjf1cbggcb0rw17lax5gqr3r0p7fl25dqngqcfx7h")))

(define-public crate-tindi-0.2 (crate (name "tindi") (vers "0.2.0") (hash "0gqk8z7ds9g8gssw5c1dhnykj3hsf4rx8prrsidn8iwlm1616x9s")))

(define-public crate-tindi-0.3 (crate (name "tindi") (vers "0.3.0") (hash "13bgzq5kv4622cw74h2p82fww453fx3j6k06xsj2hm8x1b1lz570")))

(define-public crate-tindi-0.4 (crate (name "tindi") (vers "0.4.0") (hash "13nnnfn73dclxyv35pkwyj3fvjin5dy01zkz8zkh984jb51al93x")))

(define-public crate-tindi-0.4 (crate (name "tindi") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15hwzz3hy7hlfp0dp203byafh6kn3cakvg124pk5s79hxx8ifghm")))

(define-public crate-tindi-0.5 (crate (name "tindi") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1x48c4dkzmr8lcw3xpk17ah5vwny63ni5ckak84pdih7scz06dgi")))

(define-public crate-tindi-0.5 (crate (name "tindi") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l2csyzilr6i14invqv1wqrk3bkfnwn3yync7bwyq7irsqvc3pw2")))

(define-public crate-tindi-0.5 (crate (name "tindi") (vers "0.5.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gbp651l2svkvm527s6lf3kdd2n4gi33ys4g3krvbs36rmr9pfqw")))

(define-public crate-tindi-0.6 (crate (name "tindi") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0m93kv3g4j29cnm25yhh4gy8sdb9c8lmlhjfkkalddgn40fxy25f")))

(define-public crate-tindi-0.6 (crate (name "tindi") (vers "0.6.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1837gpjcmb8316v2idpkz2fq9qxy0kg1iq5frcp0a5qsfidn25xh")))

(define-public crate-tindi-0.6 (crate (name "tindi") (vers "0.6.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00hdq2xv41330k7j9gwdf6qlmalqbs2k9b6zrkrmdcwn1dnjv1sn")))

