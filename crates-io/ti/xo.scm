(define-module (crates-io ti xo) #:use-module (crates-io))

(define-public crate-tixonome-0.1 (crate (name "tixonome") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1a65bpgkx5zqi3rlqjn9y086vyszzjgsc2pksl9f6qj2j0g4fqrm")))

(define-public crate-tixonome-0.2 (crate (name "tixonome") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1l652kh9blmjkqj34bv08srysvy4jclzr48h5z6yx9xmjhai2knv")))

(define-public crate-tixonome-0.2 (crate (name "tixonome") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "079kwr3z2b8pwp5nsacvm5vpyrzq80ij5p7sv1783nq3f719y8xp")))

(define-public crate-tixonome-0.3 (crate (name "tixonome") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1zvqlanj38ykdqvlfk21ad86qpb8jjvbxf96g7m4qpcs9ryr8asi")))

(define-public crate-tixonome-0.3 (crate (name "tixonome") (vers "0.3.1") (deps (list (crate-dep (name "die") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1m0x0l8x01xy1nj1am458h9dafnvm10y1fscch2fwalw5c1bg3cg")))

(define-public crate-tixonome-0.3 (crate (name "tixonome") (vers "0.3.2") (deps (list (crate-dep (name "die") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1dh0n8yrrjb3cfxi5a9k0rz451j8jsfpr33lpawnap3k59jkrdl4")))

