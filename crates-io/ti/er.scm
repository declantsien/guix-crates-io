(define-module (crates-io ti er) #:use-module (crates-io))

(define-public crate-tier-0.0.1 (crate (name "tier") (vers "0.0.1") (hash "0wxzn8fw83hcgl6c2f7bafbkw5422s5x2s83rg9d4j3kibvgwbbs") (yanked #t)))

(define-public crate-tiered-result-0.1 (crate (name "tiered-result") (vers "0.1.0") (deps (list (crate-dep (name "nullable-result") (req "^0.7.0") (features (quote ("try_trait"))) (kind 0)))) (hash "15g40mm6kcnx7b5m7azcww5fdm41wb0sgrfsrhpa546jmz27312d") (features (quote (("std" "nullable-result/std"))))))

