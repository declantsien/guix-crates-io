(define-module (crates-io ti c_) #:use-module (crates-io))

(define-public crate-tic_rng_toe-0.1 (crate (name "tic_rng_toe") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xxsgzv9ayj5dlpvmrpmixihhcq03hvm96wlpja9ijj8g5vnyrp7")))

(define-public crate-tic_tac_toe-0.1 (crate (name "tic_tac_toe") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.22.1") (default-features #t) (kind 0)) (crate-dep (name "conrod") (req "^0.49") (features (quote ("piston" "piston2d-graphics" "glium"))) (default-features #t) (kind 0)) (crate-dep (name "find_folder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.64.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "074i404h3im0vdyws29blrizvzy4zjf6mcw7qqw9pyvb3xrsyzh8")))

(define-public crate-tic_tac_toe-0.1 (crate (name "tic_tac_toe") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.22.1") (default-features #t) (kind 0)) (crate-dep (name "conrod") (req "^0.49") (features (quote ("piston" "piston2d-graphics" "glium"))) (default-features #t) (kind 0)) (crate-dep (name "find_folder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.64.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "14wvq6y3ybs62z4dj7cl32qs9ynqihj68xaj4k2c1xc6yfb9anx5")))

(define-public crate-tic_tac_toe_bit_patterns-1 (crate (name "tic_tac_toe_bit_patterns") (vers "1.0.0") (hash "0yzj18dr0xdzl2rvy1anbs0k3d023k4ax30qr8a9grn6f1gi4jai")))

(define-public crate-tic_tac_toe_rust-0.1 (crate (name "tic_tac_toe_rust") (vers "0.1.0") (hash "1lz1lxw2bn87j29xmj2mgqhmgngbgn8d86far0ap7kg763jsgm0f")))

(define-public crate-tic_tac_toe_rust-0.2 (crate (name "tic_tac_toe_rust") (vers "0.2.1") (hash "0mvifglyjq8na3vs3cpkxkjd5a6lfqycqi7f755yyalkrz0wkfbg")))

(define-public crate-tic_tac_toe_rust-0.3 (crate (name "tic_tac_toe_rust") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cc7p7vhancbkn2xg2p00wki1rqfz14sbq0m83545l5gwnszw8va")))

(define-public crate-tic_tac_toe_rust-0.3 (crate (name "tic_tac_toe_rust") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "134819967fg59cvbzppg6fl9fxgvr2k4rdd0qknh0qs5fp8fy2ay")))

(define-public crate-tic_tac_toe_rust-0.4 (crate (name "tic_tac_toe_rust") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "191clwwsdf9njymyy7pa7zdaggy86agcfbj54f7sk3cjy8fbfcng")))

