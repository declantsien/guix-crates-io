(define-module (crates-io ti #{64}#) #:use-module (crates-io))

(define-public crate-ti64-0.1 (crate (name "ti64") (vers "0.1.0") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1hjdxvv2kncnymczsrp852zm175w6a5pqlbl1yg7zk7hl12qap1v")))

(define-public crate-ti64-0.1 (crate (name "ti64") (vers "0.1.1") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1bhjn7m3rrb39sdzb4c3x3f4j684z05ncpllg0f7yfnwg8dmqw1r")))

(define-public crate-ti64-0.1 (crate (name "ti64") (vers "0.1.2") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1n41cg7bzhpxmk90a1sd5g4l86kpskafky9l0xhqhblbp903bnk0")))

(define-public crate-ti64-0.1 (crate (name "ti64") (vers "0.1.3") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.14") (features (quote ("formatting" "parsing"))) (default-features #t) (kind 0)))) (hash "0cgqaf2zzbzgydbg2k9yvfcj3wxlyqyxc3c7nc038x54g1biav2s")))

(define-public crate-ti64-0.1 (crate (name "ti64") (vers "0.1.4") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.14") (features (quote ("formatting" "parsing"))) (default-features #t) (kind 0)))) (hash "19qm21ydb27a58nrvyr26barkswwhmla58qd5j05xah7kvjiq9q8")))

(define-public crate-ti64-0.1 (crate (name "ti64") (vers "0.1.5") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.14") (features (quote ("formatting" "parsing"))) (default-features #t) (kind 0)))) (hash "1s2cxvrwg97kh0yr857bp2k1s41981pv6h1av9x0imyqwx36fz6v")))

