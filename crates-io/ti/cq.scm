(define-module (crates-io ti cq) #:use-module (crates-io))

(define-public crate-ticque-0.1 (crate (name "ticque") (vers "0.1.0") (deps (list (crate-dep (name "concurrent-queue") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "onetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ya19l2w7851lnd1ydwcw55fql53lkzblznzkkxjnvh3hdpcm8kp")))

(define-public crate-ticque-0.1 (crate (name "ticque") (vers "0.1.1") (deps (list (crate-dep (name "concurrent-queue") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "onetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1gxn3gcia2n32wxdkh6jqic4l2h3am7q6j9sjx00rv9ffi1zg4n9")))

(define-public crate-ticque-0.1 (crate (name "ticque") (vers "0.1.2") (deps (list (crate-dep (name "concurrent-queue") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "onetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "12xjyh1fz2p1cka2f6xsvnc4zra2c60rjaq8jih1zznskyx6djsl")))

(define-public crate-ticque-0.1 (crate (name "ticque") (vers "0.1.3") (deps (list (crate-dep (name "concurrent-queue") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "onetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "10gzn5936rw6d9w1jhlzhxhrcvfngmghj7cxm0k6vqhhlmh58m10")))

