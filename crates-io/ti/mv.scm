(define-module (crates-io ti mv) #:use-module (crates-io))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.0") (hash "0s329r921mpnkg784hiaxlbcr8w4yjaxn3mj3603qc7mi8bxr0ky")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.28") (hash "02jzsisaj9ii59jkzdzv4ks7ybx88nvp6pyqn865idmmvxbvl02s")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.29") (hash "0sf72xadwgg6ki592jkg875jm6avdkyvprrzwhs7nr0ann779h5h")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.31") (hash "188jbqxfaba39bw0v28pdppjrf9vjaxl2n5mwy51n2pbji2410pk")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.32") (hash "1hq7rr9s7fz8vykmq27fsr0pzprzl8k0p0bicbd4i28vwyi1n5z2")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.33") (hash "1748wxh1rqd19hai6l9dwhkv2s8prizkdpdja6r0p7z054ck0dc1")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.35") (hash "1hs92ybq831khi4fq5q53cy4nd023lizj1j3flf63q78n03k5dk7")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.36") (hash "1pf0zz9m6pga3m11kz7riyqw89h114fkj8fyi88jhxikxbf1jhca")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.37") (hash "0qis1lhcq1c109n1kjf89j600fliqz57xhkgpl177y48v3d4rckw")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.39") (hash "03jv4d0hrkssbyjn08nwzyx6zyj6b73c500v482i9jg9sfg52qd3")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.40") (hash "0q865p9dnx0qb82alrgrw931qdy72i8fxrysyx0bfhx41yh1k99s")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.41") (hash "0c1xzprp90mprp864aqwry5p479hc2s7fywdanr8m01g8kcigm8r")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.42") (hash "0xnxil8syfp741584zqic857rgmzpb4jcxd7h1jp46rx9j7gldgn")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.43") (hash "1ai5dbq5a9qj3vv33k4v5h3vzwbg4v7m8izkg5ijcnpd99qgvvcw")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.45") (hash "172zl6zmzq3slb3pdy2amykyr80z7f8x4wfx0ah49c39h8ayxh6b")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.87") (hash "0xj64biglrfd551zzj5dypl8n26y2wyqfw6fapfmy63aglg2dkgc")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.88") (hash "09cbwms4yf8sgpfc56xfcscqzgkdniiha8902yd90l4rpc7b7yn5")))

(define-public crate-timvw-hello-rs-0.1 (crate (name "timvw-hello-rs") (vers "0.1.91") (hash "1knbdcqpak4skl1ijbk9yhp4pxnn6wh7x7fssi18yskpkzawv2fc")))

