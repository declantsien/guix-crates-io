(define-module (crates-io ti l-) #:use-module (crates-io))

(define-public crate-til-cli-0.1 (crate (name "til-cli") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "14ksllpb8i0cr4wnh2d2s2fkn3yr7knrcqphp1r3m8wl5z8sajkn")))

(define-public crate-til-cli-0.1 (crate (name "til-cli") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1vga2v0l4w7hnr4j6sk8qirrxsx7x9j5n622wcvp2hd4d31z5258")))

