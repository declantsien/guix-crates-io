(define-module (crates-io ti jm) #:use-module (crates-io))

(define-public crate-tijme-calendar-0.1 (crate (name "tijme-calendar") (vers "0.1.0") (deps (list (crate-dep (name "bpaf") (req "^0.7") (features (quote ("derive" "bright-color"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "0iwyqhb82nk8ik6s64pja1v6d127spdwrsfr8lzwls4gw3rh2jpq")))

(define-public crate-tijme-calendar-0.1 (crate (name "tijme-calendar") (vers "0.1.1") (deps (list (crate-dep (name "bpaf") (req "^0.7") (features (quote ("derive" "bright-color"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "1k9ir600h5mym50m2za9ypbv5pw3nv145vnq694s2iq7zl6rchhb")))

