(define-module (crates-io ti d2) #:use-module (crates-io))

(define-public crate-tid2013stats-1 (crate (name "tid2013stats") (vers "1.0.0") (deps (list (crate-dep (name "rstats") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1vlwy4jbd5pxc8k01yrhl0c4g2b38q87ch6i7804bvgnw68by45p")))

(define-public crate-tid2013stats-1 (crate (name "tid2013stats") (vers "1.0.1") (deps (list (crate-dep (name "rstats") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "1jnqjcg67nr4l0c1brfbx70n1xq0vv00p2a1vm8hdqj0ahlzpqd9")))

(define-public crate-tid2013stats-1 (crate (name "tid2013stats") (vers "1.0.2") (deps (list (crate-dep (name "rstats") (req "^2.0.9") (default-features #t) (kind 0)))) (hash "0fxy2b99m98yhw0l22nmxnl09abrq8asna3bxrq19h8pnwqhjd5a")))

