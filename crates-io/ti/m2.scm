(define-module (crates-io ti m2) #:use-module (crates-io))

(define-public crate-tim2-0.1 (crate (name "tim2") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0bqadgdk4hm98wwpz2q8f7ldrbvsmq2741zc5xgjzskwzsha5i1z")))

(define-public crate-tim2-0.2 (crate (name "tim2") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0swan1dnvx328gm46r6yi5h2g7dsjjmxpl4jdmngnmwwfslab5q9")))

(define-public crate-tim2-0.3 (crate (name "tim2") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1q97vmsbscsmp1bqv8d6mwps5gjcndd4h423zrzfxpc3apyfdr7w")))

(define-public crate-tim2-0.3 (crate (name "tim2") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1n9s8n33065agqvdjj0d5pks5krnxdl39wfdqqgb280hs4yc81x6")))

