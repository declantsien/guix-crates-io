(define-module (crates-io ti of) #:use-module (crates-io))

(define-public crate-tioft_rust_get_http-0.1 (crate (name "tioft_rust_get_http") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.12.9") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0x5fdl95f66v0ars18xjw2xk8xwsgvvp148pwppipw1sksbdrz0n") (yanked #t)))

