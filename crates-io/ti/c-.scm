(define-module (crates-io ti c-) #:use-module (crates-io))

(define-public crate-tic-tac-rust-0.1 (crate (name "tic-tac-rust") (vers "0.1.3") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "15nq79vywxiykfxs1gxq9ynsmn7hr9rjyb121qpk9cl4vjlrv5zd") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-tic-tac-rust-cli-0.1 (crate (name "tic-tac-rust-cli") (vers "0.1.0") (deps (list (crate-dep (name "tic-tac-rust") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "update-notifier") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1w3xbk8nhi65ravy733gjc8p7hj94g6faxmx9z7wajvp1511s1sq")))

(define-public crate-tic-tac-rust-cli-0.1 (crate (name "tic-tac-rust-cli") (vers "0.1.1") (deps (list (crate-dep (name "tic-tac-rust") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "update-notifier") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0i6d421kmx1ycrz1f0b3rpkqjhm97jbdffzsg4spgzs6bkiy52y8")))

(define-public crate-tic-tac-rust-cli-0.1 (crate (name "tic-tac-rust-cli") (vers "0.1.2") (deps (list (crate-dep (name "tic-tac-rust") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "update-notifier") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bl4cxx8690xzy9dpv9sakf5f2lhci1bplpl276zgmp29795ni65")))

(define-public crate-tic-tac-rust-cli-0.1 (crate (name "tic-tac-rust-cli") (vers "0.1.3") (deps (list (crate-dep (name "tic-tac-rust") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "update-notifier") (req "^0.1") (default-features #t) (kind 0)))) (hash "155yn732hl81kmz3s7zc33j9vlbikw8aadzvn36v60b0nfczd7wv")))

(define-public crate-tic-tac-terminal-1 (crate (name "tic-tac-terminal") (vers "1.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "116psgrwzfjzxzpxxfphkw342b8mi6ih8n1s5jr0k4813dx6anyf")))

(define-public crate-tic-tac-terminal-1 (crate (name "tic-tac-terminal") (vers "1.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "166bzz0iqn8ixiyvwvdqnk7m0nx53m758xb90pgjh6nmm32689sz")))

(define-public crate-tic-tac-terminal-1 (crate (name "tic-tac-terminal") (vers "1.0.2") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1kci6415l7crc1z8ls13x40xhvz67qbvpjc27zmg8kkwpq0d6ymp")))

(define-public crate-tic-tac-toe-game-0.1 (crate (name "tic-tac-toe-game") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "10pxz54zpyzl3kdy4vlhpb8kmlr03aal3ffxr64zwcgfn7b0jjx8")))

(define-public crate-tic-tac-toe-game-rs-1 (crate (name "tic-tac-toe-game-rs") (vers "1.0.0") (hash "00h1p2v8kfkbwzx5p3fd00kgldq9rgrrmnyy6r9nmxi74apyd2qy")))

