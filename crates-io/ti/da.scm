(define-module (crates-io ti da) #:use-module (crates-io))

(define-public crate-tidal-0.1 (crate (name "tidal") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.11.27") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "tidalcache") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0lqmsfrfyxncb85v4cmxshrjndkqz5gv4qfi2ndiy44xnyb87fjk")))

(define-public crate-tidalcache-0.1 (crate (name "tidalcache") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "1prvmrnh3xnmz49qjvqbpxizwcwavlxic6ni5vs4s812m4m5wqaw")))

