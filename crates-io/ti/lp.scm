(define-module (crates-io ti lp) #:use-module (crates-io))

(define-public crate-tilper-0.0.1 (crate (name "tilper") (vers "0.0.1") (hash "1qgb8v5jin18mjpm6362qilzsh7pa9z2136z7hxf690gzwc838ns")))

(define-public crate-tilper-0.0.2 (crate (name "tilper") (vers "0.0.2") (hash "1x225cp3skf38n4jjygsfsg8fv7ny7v0aznc5z2nyns0r2w3qg7j")))

(define-public crate-tilper-0.0.3 (crate (name "tilper") (vers "0.0.3") (hash "1mdfhkfwmy7r43ny76v6fsmn9ag81n26iqjlv2mw2d74d9dpja7j")))

