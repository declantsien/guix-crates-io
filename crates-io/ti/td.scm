(define-module (crates-io ti td) #:use-module (crates-io))

(define-public crate-titdb-0.1 (crate (name "titdb") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "18wh77pdr5sm19fbdq0xwygvfqypb005qsj4dpbs3bkvd0k6a4q2") (features (quote (("default")))) (v 2) (features2 (quote (("rand" "dep:rand"))))))

