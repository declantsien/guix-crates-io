(define-module (crates-io ti ra) #:use-module (crates-io))

(define-public crate-tira-0.0.1 (crate (name "tira") (vers "0.0.1") (hash "0ag2sb6dffzkb2cpx23gpnha5gkv1sc79jj3z2d36dk7y2dhafar") (yanked #t)))

(define-public crate-tirana-0.0.0 (crate (name "tirana") (vers "0.0.0") (hash "126mv95xfbbjc3rfwvg17jd5b1l37pb9db7m8gil9d0y3afwgjs9")))

