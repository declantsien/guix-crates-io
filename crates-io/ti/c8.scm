(define-module (crates-io ti c8) #:use-module (crates-io))

(define-public crate-tic80-0.1 (crate (name "tic80") (vers "0.1.0") (deps (list (crate-dep (name "bilge") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "buddy-alloc") (req "^0.4.1") (default-features #t) (target "cfg(target_family=\"wasm\")") (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "tic80-sys") (req "^0.1.0") (default-features #t) (kind 0) (package "tic80-sys")))) (hash "1mn0firgv0rgyjszv9ib0alj7749avz30fhn845lcf3l5pjlvsrv") (features (quote (("default"))))))

(define-public crate-tic80-sys-0.1 (crate (name "tic80-sys") (vers "0.1.0") (hash "199qvdrs5yyr5717phr87z2mhdysj418hgh52n0zpil3pnn7xd2l")))

