(define-module (crates-io ti lr) #:use-module (crates-io))

(define-public crate-tilr-0.4 (crate (name "tilr") (vers "0.4.3") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cqb7y8p9gy3r0qb7wwz4zlvzgrcgb4qkdwr8540hgaga0940mdc")))

(define-public crate-tilr-0.4 (crate (name "tilr") (vers "0.4.4") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "044kp5bcrq1b9166fk6qx6axcgyvjjfjq4c88dqypikdbj4g3cy8")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "1g8yqjfim4b6jn8c2gwcfchr3im6csr4g067ack0ykl1zw1cy3sp")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "1ac74cbvvm2k1jpj5ar3d0myw6sfp6wa4rxqpspivcksx6k1hpb6")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.2") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "07pf1x9n0aiql3sdm4g5knzdb16vgbkzw564k10723pyra8xs8w6")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.3") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "0368035y3ah6b9d0r3aljccxqk6xhysi6v89s74samda8yvxkqk6")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.4") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "1hq21qr4qkmi8xk6l7qxkgygmkgm2navdqvsd6k3f4yflv1dd85k")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.5") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "1hm1s0dvchbkxchfk56cdczz2ivc5bc0m2hmzwwxzwffd6axxxac")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.6") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "0m5hcwwzrbb7xa9dr22j4li568wqy7qvcvc17dck69yp0hbiqz2a")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.9") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "0fpkdf1nznjivzvcrqsd9dyb10qmc4m5yqxx713vvs25sz516mfz")))

(define-public crate-tilr-0.5 (crate (name "tilr") (vers "0.5.10") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25") (default-features #t) (kind 0)))) (hash "1c99kxzdwhrkdn9sv6s0am19ai7h6551sfm1nna97fpjx2c1v75j")))

