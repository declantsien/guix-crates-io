(define-module (crates-io ti ll) #:use-module (crates-io))

(define-public crate-till-0.0.1 (crate (name "till") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (kind 0)) (crate-dep (name "pin-project") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "16vgz22vsad8y7rvra3yzanfcixpl0a1mxhysgsp2wjqfcn7hgad")))

