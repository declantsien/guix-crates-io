(define-module (crates-io ti mo) #:use-module (crates-io))

(define-public crate-timo-0.1 (crate (name "timo") (vers "0.1.0") (hash "18ia7iggajcfpydgrm0sjkvf3dg4k8nj5xfq001ilw6xlcz3fhz9")))

(define-public crate-timon-0.0.1 (crate (name "timon") (vers "0.0.1") (hash "04f5rxc693vv8npcv5jsy9ff96xjgr1hcmq80lcgdpnwyqfc09s0")))

(define-public crate-timone-0.1 (crate (name "timone") (vers "0.1.0") (hash "0wm684m0kmrp208jal9b82jvnhljyy67ii96hfr8d7l5myaa6cr4")))

