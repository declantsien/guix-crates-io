(define-module (crates-io ti ss) #:use-module (crates-io))

(define-public crate-tissue-0.0.0 (crate (name "tissue") (vers "0.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "tao") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.37") (default-features #t) (kind 0)))) (hash "1vpzxb6vlpgh4ihnic6c8gl61fij2vgb1y6r5163zad2ivxcdphm")))

