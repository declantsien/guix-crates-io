(define-module (crates-io ti fl) #:use-module (crates-io))

(define-public crate-tifloats-0.1 (crate (name "tifloats") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0v1wly307360mdraixs2g9jf7y7ig3dgcyw9zw8qs1jpc4ics8in")))

(define-public crate-tifloats-0.2 (crate (name "tifloats") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0d8il6ix0djqzzw4h1fvz9rviikb98ym0mc7bkknc35s1k9fjayc")))

(define-public crate-tifloats-0.2 (crate (name "tifloats") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0mj4c66nywm8id2gw2l4j49fvc3149z5kslnkzzzp3ld4qwy38zz")))

(define-public crate-tifloats-1 (crate (name "tifloats") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "07d4iky7i36x68fw98xhk78mnifdbvk5asrvl8zxgp2myngq8djp")))

(define-public crate-tifloats-2 (crate (name "tifloats") (vers "2.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "18rjbw6gk4x1j8x803qvc01pyi262qh1ayda0cf6zp9hq83xm96d")))

(define-public crate-tifloats-2 (crate (name "tifloats") (vers "2.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "11ah2zlaxb6bwvy2s0lzm7rwclmr0il3z064y64pkibgmrg0laz1")))

(define-public crate-tifloats-2 (crate (name "tifloats") (vers "2.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "18dcb7m9d7y0c0bsy6slhbnr0ivbjpaw1n7phys7ivxc8cjspnm6")))

(define-public crate-tifloats-2 (crate (name "tifloats") (vers "2.1.2") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "02sghigj82h0gfyy0pmcab18252g1j303vsll8vbnxrgzlxs6537")))

(define-public crate-tifloats-2 (crate (name "tifloats") (vers "2.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1lrq8p8xh9g18bw663dfh4xwk0a38q1zhxyb04pisk7c7m0zv05v")))

(define-public crate-tiflow-0.1 (crate (name "tiflow") (vers "0.1.0") (hash "0n08l8nzfmddawzk6wlaiw8dlxxnspzz57zq914rr2jcmkfkv93q")))

