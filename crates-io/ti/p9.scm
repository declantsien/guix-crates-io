(define-module (crates-io ti p9) #:use-module (crates-io))

(define-public crate-tip911-stakeset-0.0.2 (crate (name "tip911-stakeset") (vers "0.0.2") (deps (list (crate-dep (name "imbl") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "melstructs") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "novasmt") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "stdcode") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "tmelcrypt") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "10ckiqqsa4izn4dzxwqbk8g5zi5fmngil7pdcrzx5d79229xyawx")))

