(define-module (crates-io ti tl) #:use-module (crates-io))

(define-public crate-title_case-0.1 (crate (name "title_case") (vers "0.1.1") (deps (list (crate-dep (name "aok") (req "^0.1.11") (default-features #t) (kind 2)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)) (crate-dep (name "ucfirst") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0zljzfqxd48xkr0vhhx321idigqpzhi21j87m1rbkyhigh92cjx0")))

(define-public crate-title_case-0.1 (crate (name "title_case") (vers "0.1.2") (deps (list (crate-dep (name "aok") (req "^0.1.11") (default-features #t) (kind 2)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)) (crate-dep (name "ucfirst") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1mg8sh7ladndv4d7qdxpxhc029lc79ixd5xl8dy7ihh37h9mzv6l")))

(define-public crate-title_parser-0.1 (crate (name "title_parser") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1vxi6831iwxp5x3zmlfp05ykavfn4kxh16jhnbvgyh22sfbxgd0r")))

(define-public crate-title_parser-0.1 (crate (name "title_parser") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1kcsnqqz0fmvzps99lv6wpy9602dh1hl7k1gcs1mv2ba0v2rsxw5")))

(define-public crate-title_parser-0.1 (crate (name "title_parser") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "04vzzhs1h4fr1wdfw2pa1q1gds5ak2kmc4zzxwlzzq63y52s5ydr")))

(define-public crate-titlecase-0.9 (crate (name "titlecase") (vers "0.9.0") (deps (list (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1c7i1i4wynz3pllxghwsrz67yj1szj6p9k6r1a7s9rihmrjhmz2z")))

(define-public crate-titlecase-0.9 (crate (name "titlecase") (vers "0.9.1") (deps (list (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1zsckvvf1kvpxbwrj8h12mc08gqrjhi7vamaxwqs487v3visg65z")))

(define-public crate-titlecase-0.10 (crate (name "titlecase") (vers "0.10.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1j0naf5784bfiqc4vy6q31y1ya884v78p1yc3ilyjq8wfsd3jl06")))

(define-public crate-titlecase-1 (crate (name "titlecase") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "05qizspxihjhmzsd9y6kfxzrss4jl4y042wni4m2yk62rw8f8rgm")))

(define-public crate-titlecase-2 (crate (name "titlecase") (vers "2.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "00l8h1qkiggb6970wk73lq2769802j2nwpi7gk0fd4p8fqd2yypa")))

(define-public crate-titlecase-2 (crate (name "titlecase") (vers "2.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0iz09f9hv3kdpjbyg36b3p0dixpqc1s6ry8c31r4sckg2l5wycc4")))

(define-public crate-titlecase-2 (crate (name "titlecase") (vers "2.2.0") (deps (list (crate-dep (name "joinery") (req "<3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0jba1p2dqfy8rbj71l3h7qnyl9hfdnjdkblxrwmrqwyviz685316")))

(define-public crate-titlecase-2 (crate (name "titlecase") (vers "2.2.1") (deps (list (crate-dep (name "joinery") (req "<3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0hi0hkh2x78rvq7rhdgdzsgwcnlpvvb59hgnifsgwz01vf67lf9q")))

(define-public crate-titlecase-3 (crate (name "titlecase") (vers "3.0.0") (deps (list (crate-dep (name "regex") (req "^1.10") (features (quote ("std" "perf" "unicode-perl"))) (kind 0)))) (hash "05650b7k1pj0yddd150bdnk91jqnsq50cyxg462yqm7fnv3j6cmv") (rust-version "1.70.0")))

(define-public crate-titlecase-3 (crate (name "titlecase") (vers "3.1.1") (deps (list (crate-dep (name "regex") (req "^1.10") (features (quote ("std" "perf" "unicode-perl"))) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (default-features #t) (target "cfg(target_family = \"wasm\")") (kind 0)))) (hash "06cya9x174k6w1197gv0za0i4d48k7a1fdyfb7c4gzs87w87jinc") (rust-version "1.70.0")))

(define-public crate-titlecase-3 (crate (name "titlecase") (vers "3.2.0") (deps (list (crate-dep (name "regex") (req "^1.10") (features (quote ("std" "unicode-perl"))) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (default-features #t) (target "cfg(target_family = \"wasm\")") (kind 0)))) (hash "1yinddgqqhf6m7gln5x1m04yrslcr90p0j1i7n6mqf75q89f7idc") (features (quote (("perf" "regex/perf") ("default" "perf")))) (rust-version "1.70.0")))

(define-public crate-titlefmt-0.3 (crate (name "titlefmt") (vers "0.3.0") (deps (list (crate-dep (name "id3") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "metaflac") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "1rb1qr8kxr5k1svzwj9qg7ddzbwsrlkv9ss98njjicdnq8an3p7c") (features (quote (("titlefmtr" "metadata_libs") ("metadata_libs" "id3" "metaflac"))))))

(define-public crate-titlefmt-0.3 (crate (name "titlefmt") (vers "0.3.1") (deps (list (crate-dep (name "id3") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "metaflac") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "1dnwgm8fq6pnyza37rp8494lfllywfvcmj6k392cdi26mngk7f13") (features (quote (("titlefmtr" "metadata_libs") ("metadata_libs" "id3" "metaflac"))))))

(define-public crate-titlefmt-0.4 (crate (name "titlefmt") (vers "0.4.0") (deps (list (crate-dep (name "id3") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "metaflac") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)))) (hash "17bqvr6jzhjhnjr96f65j1ipb35zxqyga0ma24iapddgpqdalqag") (features (quote (("titlefmtr" "metadata_libs") ("metadata_libs" "id3" "metaflac") ("default" "unicode-normalization"))))))

(define-public crate-titleformat-rs-0.1 (crate (name "titleformat-rs") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)))) (hash "0kp2vw15v76m78h51rmj94f0hxrc07df92i04wq7qnrpd8m4d0n4")))

