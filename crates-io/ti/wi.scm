(define-module (crates-io ti wi) #:use-module (crates-io))

(define-public crate-tiwi-0.1 (crate (name "tiwi") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1cgqjli4yh227f5jls78shkcmszqj6k0fl1nxy222ykzl0zm2gap")))

