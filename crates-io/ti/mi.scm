(define-module (crates-io ti mi) #:use-module (crates-io))

(define-public crate-timi-0.1 (crate (name "timi") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bzkmjmpb64zsnjmg141ii4hy3w6cr5k84f6bs5l9035l9dcfhlp")))

(define-public crate-timi-0.2 (crate (name "timi") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12djjgzbwg9m9j04l2dfd835110l1w7ld2k3zddvidd0mjg3d1sa")))

(define-public crate-timi-0.2 (crate (name "timi") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "09qv2axsl7hq4j0x63pwbw1y9j5540hs1lmk90q76xx0bhzz0wa4")))

(define-public crate-timing-0.1 (crate (name "timing") (vers "0.1.0") (hash "050pcwd1a7iprhhkbhhr0ic7x1zv1bhk388jqiba38djw0yykxy4")))

(define-public crate-timing-0.1 (crate (name "timing") (vers "0.1.1") (hash "1aqs9wpv292hgsfva77km6wbj5rh0w11ni944lipl1lfvflw5kmp")))

(define-public crate-timing-0.2 (crate (name "timing") (vers "0.2.1") (hash "0fbgavahzrxkfwmnpblbzqp6ndwai2jj77mdckw3hvkijpdki7jx")))

(define-public crate-timing-0.2 (crate (name "timing") (vers "0.2.3") (hash "1w2b0wwm9g0f01riz0zf8ywighih34lm5ma99qmhx5mj18w77y8f")))

(define-public crate-timing-shield-0.1 (crate (name "timing-shield") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "1l3y24gh2can4k1s539b036i4vj7w33wwj1vrfljyr270a8abx2d")))

(define-public crate-timing-shield-0.1 (crate (name "timing-shield") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "08gi60439ydmbyw39bhllxdliyxbam9xa56cddd90b5l1j65g4bf")))

(define-public crate-timing-shield-0.1 (crate (name "timing-shield") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "0cgn0442yy50p2dfajwqyd3n4qzd1g0afnfr3kypazrdyd9kf0pn")))

(define-public crate-timing-shield-0.2 (crate (name "timing-shield") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0hqrw56111r56gdjb4m2pwb8lj3zsg9qmrmk8ygl5nk3ks31p30j")))

(define-public crate-timing-shield-0.3 (crate (name "timing-shield") (vers "0.3.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "06jz74h89q27kav6r48r2cg1sp85zw454k2rsn7jxjc0h5jsw7ny")))

(define-public crate-timings-0.0.0 (crate (name "timings") (vers "0.0.0-reserve.0") (hash "1337m6n63wqzvbsvhmz2qfv2sgx61hkw50ww2y87l6hhk3drp909")))

(define-public crate-timinigrep-0.1 (crate (name "timinigrep") (vers "0.1.0") (hash "0w4vp9hnpb8g2ald177csrszx3z0j05ppaffl0sgrjr39xgi33r4")))

