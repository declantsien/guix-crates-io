(define-module (crates-io ti ld) #:use-module (crates-io))

(define-public crate-tilde-0.0.1 (crate (name "tilde") (vers "0.0.1") (hash "1wrbayv4bdh4qdgqb8g0ixaq1wr2sppnav0y3cfh1w3ifmr7j6jd")))

(define-public crate-tilde-0.0.2 (crate (name "tilde") (vers "0.0.2") (hash "1hjwkbq18n6r12yvm832wgr61q4anawh35x1sks3k6h4y73rdjs9")))

(define-public crate-tilde-0.0.3 (crate (name "tilde") (vers "0.0.3") (hash "1sm8aq0ybpnjx2ir4wdgh4z72ml6c1zryqrzz1y09aid1vl8gk1f")))

(define-public crate-tilde-0.0.4 (crate (name "tilde") (vers "0.0.4") (hash "10jssv42wq9wc0w00gv4z40sciln1yrrr7fmyjncasdajc4vac14")))

(define-public crate-tilde-expand-0.1 (crate (name "tilde-expand") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1pvqqgzqn2x4abjmjs5dyh2mrqilshzllam1swan868k6c7swxsk") (yanked #t)))

(define-public crate-tilde-expand-0.1 (crate (name "tilde-expand") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1fhh1pxd6azg39s0r2gfh9yb44a0pis37hxm4sczzmqdn61g7cca")))

(define-public crate-tilde_derive-0.0.1 (crate (name "tilde_derive") (vers "0.0.1") (hash "0llnlyrwx53xcgdhknksyn025g29yglhkq4dgfynibwvspg2m2dd")))

(define-public crate-tilde_derive-0.0.2 (crate (name "tilde_derive") (vers "0.0.2") (deps (list (crate-dep (name "tilde") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "0kqc3d2aj4vv4v7slz1065jy091s4gd8sryx5qaclz7ifmx7rac1")))

(define-public crate-tilde_derive-0.0.3 (crate (name "tilde_derive") (vers "0.0.3") (deps (list (crate-dep (name "tilde") (req "^0.0.3") (default-features #t) (kind 2)))) (hash "1g3sz4nl1jdxwj5fx8lhbynyk239sj6fzfxy49g5g6pspz8ybnyj")))

(define-public crate-tilde_derive-0.0.4 (crate (name "tilde_derive") (vers "0.0.4") (deps (list (crate-dep (name "tilde") (req "^0.0.4") (default-features #t) (kind 2)))) (hash "0gxgf3sqbl3rcznajhpbkicsibbdczb9viqksdmyw6gys6bp39ak")))

(define-public crate-tilde_parser-0.1 (crate (name "tilde_parser") (vers "0.1.0") (deps (list (crate-dep (name "tilde-expand") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0qb7cyh9pilq0fks5bgm776vb7wz6xbakn7bh8bfn886fkga3sll")))

(define-public crate-tilde_parser-0.1 (crate (name "tilde_parser") (vers "0.1.1") (deps (list (crate-dep (name "tilde-expand") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1lcjicgdjbrw6zw67607javy9lhyh43n07ml5iqd1nnbhj4lnvbc")))

