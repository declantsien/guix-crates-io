(define-module (crates-io ti pc) #:use-module (crates-io))

(define-public crate-tipc-0.1 (crate (name "tipc") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "errno") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0la7ir91xx343rwvmkjlxc742zss4pkprrrmlgp4my46arqs39ck")))

(define-public crate-tipc-0.1 (crate (name "tipc") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "errno") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1yb3iqaccring7vl4qwk84zxbdzdjxfmd3hkd9nkk65mslm45g6f")))

(define-public crate-tipc-0.1 (crate (name "tipc") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "errno") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1l5maykcxxz166gzxqgk9z8gh37q2p4kvf5jvzmdnw5zs3ln84id")))

(define-public crate-tipccparser-0.1 (crate (name "tipccparser") (vers "0.1.0") (hash "1ijkjr9gpi3v4swliq9mmnvq66d4jxdx0wmc19h159zcvnwkqw1n") (yanked #t) (rust-version "1.56")))

