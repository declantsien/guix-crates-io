(define-module (crates-io ti s-) #:use-module (crates-io))

(define-public crate-tis-100-0.1 (crate (name "tis-100") (vers "0.1.3") (hash "0av0wg00wsyg2raa19mpa4q6s0f2ld183x4sngc8z397fgvzp52r")))

(define-public crate-tis-100-0.2 (crate (name "tis-100") (vers "0.2.0") (deps (list (crate-dep (name "hlua") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yv80iqlb9m4h0g27g19089gs8af6jmsg0zaal1l3kr9aahak13k")))

(define-public crate-tis-100-0.2 (crate (name "tis-100") (vers "0.2.2") (deps (list (crate-dep (name "hlua") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1r34azkqigm072gr0n5yfyh3jki10vzqa048k81cf7h6aiv5advx")))

(define-public crate-tis-cli-0.1 (crate (name "tis-cli") (vers "0.1.0") (deps (list (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0nfn41mwndw6zd74xxkb8qmysvancmxxma201m8lxmrh8m69dq1d")))

(define-public crate-tis-cli-0.1 (crate (name "tis-cli") (vers "0.1.1") (deps (list (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "16c2d84dn15gj2g2gl9dggayw3h7lvqwy0n1zzzbrz2di3rl7l0s")))

(define-public crate-tis-cli-0.1 (crate (name "tis-cli") (vers "0.1.2") (deps (list (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "012xi553dgybvq2p217zkwk49y7bm07swvn06a6cgijrx4bhm5b3")))

(define-public crate-tis-cli-0.1 (crate (name "tis-cli") (vers "0.1.3") (deps (list (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1yymzjs4sdjhjyqkq2sckyqw7fdh639sdq6in9y9g8pfvj65m0wb")))

(define-public crate-tis-cli-0.1 (crate (name "tis-cli") (vers "0.1.4") (deps (list (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "04b9kk9x52rf5r7mndaffnmm2vcvjik7dkzh3x2nji9ybc57nqpv")))

(define-public crate-tis-cli-0.1 (crate (name "tis-cli") (vers "0.1.5") (deps (list (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1fgvyn5kyisk212hri6zq25nx29dks9b04181akv0nfczwsakcdj")))

(define-public crate-tis-cli-0.1 (crate (name "tis-cli") (vers "0.1.6") (deps (list (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "08l0mvzmbsh6wkpqy7hnvhrnkgvlp8cz7lw850jgh7qi803l95dq")))

