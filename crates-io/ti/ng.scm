(define-module (crates-io ti ng) #:use-module (crates-io))

(define-public crate-tinguely-0.0.1 (crate (name "tinguely") (vers "0.0.1") (deps (list (crate-dep (name "mathru") (req "^0.2.1") (features (quote ("native"))) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "067kib73wdka2mpcrl7j4nw6vzd5q81654nqr51svv02d2qadb7x")))

(define-public crate-tinguely-0.1 (crate (name "tinguely") (vers "0.1.0") (deps (list (crate-dep (name "mathru") (req "^0.2.1") (features (quote ("native"))) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1fc3mcljf8bv7psvwssn7jkl6f8c446iwz16gdk8jmgswysngixk")))

(define-public crate-tinguely-0.1 (crate (name "tinguely") (vers "0.1.1") (deps (list (crate-dep (name "mathru") (req "^0.7.3") (kind 0)) (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0j6vd9gk3apdy44xjg3xmx08jy2izfmpzjd30n50wb79ddmncigj") (features (quote (("openblas" "mathru/openblas") ("netlib" "mathru/netlib") ("native" "mathru/native") ("intel-mkl" "mathru/intel-mkl") ("default" "native") ("blaslapack") ("accelerate" "mathru/accelerate"))))))

