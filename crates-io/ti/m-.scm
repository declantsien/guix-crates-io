(define-module (crates-io ti m-) #:use-module (crates-io))

(define-public crate-tim-rust-minigrep-0.1 (crate (name "tim-rust-minigrep") (vers "0.1.0") (hash "05xjfk2vdwmjfavalvckpx3sxwwm9arvzxj2ra6r2ks3acd3r3bq")))

(define-public crate-tim-rust-minigrep-0.1 (crate (name "tim-rust-minigrep") (vers "0.1.1") (hash "0jzvrkvybfby353aws1xd3hhmna6zjwissn62x9zkqcfp1imli3p")))

(define-public crate-tim-systick-monotonic-0.1 (crate (name "tim-systick-monotonic") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "stm32f0") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stm32l0") (req "^0.10") (optional #t) (default-features #t) (kind 0)))) (hash "11xgsvs99xxba5w0smfcaiyrvylpazny8c90h7s2c6dq4gi1m0a7") (features (quote (("l0x1-tim21-tim22" "stm32l0/stm32l0x1") ("f0x1-tim15-tim17" "stm32f0/stm32f0x1") ("f0x1-tim15-tim16" "stm32f0/stm32f0x1"))))))

(define-public crate-tim-systick-monotonic-0.2 (crate (name "tim-systick-monotonic") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "stm32f0") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stm32h7") (req "^0.13.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stm32l0") (req "^0.10") (optional #t) (default-features #t) (kind 0)))) (hash "1hadszpjvgvhbw5k5k34vhi3i0qmvb425iripsddh6f0yjzasdcm") (features (quote (("l0x1-tim21-tim22" "stm32l0/stm32l0x1") ("h743v-tim15-tim17" "stm32h7/stm32h743v") ("f0x2-tim15-tim17" "stm32f0/stm32f0x2") ("f0x1-tim15-tim17" "stm32f0/stm32f0x1") ("f0x1-tim15-tim16" "stm32f0/stm32f0x1"))))))

