(define-module (crates-io ti fi) #:use-module (crates-io))

(define-public crate-tifiles-0.1 (crate (name "tifiles") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1bqbbd8wqsgj2frjvwgnqb1ld2vnsc0gxiz664f43mgf50ix03qr")))

(define-public crate-tifiles-0.2 (crate (name "tifiles") (vers "0.2.0") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (optional #t) (kind 0)))) (hash "1z0gwrgisjp2hgd8l7f8q99pi36alwkb6jpcdaib02xh6d8sgnp6") (features (quote (("default" "bundles") ("bundles" "zip" "crc32fast"))))))

