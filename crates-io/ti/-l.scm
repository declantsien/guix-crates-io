(define-module (crates-io ti -l) #:use-module (crates-io))

(define-public crate-ti-lp55231-1 (crate (name "ti-lp55231") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "047crn91rvf69w2grzy739pyfppq2gzx4acy0l1n49w5nlqchvk7")))

