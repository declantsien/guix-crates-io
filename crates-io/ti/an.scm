(define-module (crates-io ti an) #:use-module (crates-io))

(define-public crate-tianlin_test_lib-0.1 (crate (name "tianlin_test_lib") (vers "0.1.0") (hash "1k41w88d13r4zz8a548qncqmk69qk4p1krf4j9ymps4mhpgsvvm1")))

(define-public crate-tianmao-0.1 (crate (name "tianmao") (vers "0.1.0") (hash "04s5lm7p2z3s2al6z017qg6x6vv52gcd6ypgm3dhmv5d7pff9y9f")))

(define-public crate-tianmu-fs-0.1 (crate (name "tianmu-fs") (vers "0.1.0") (hash "1y80ijwdaha3pjbc0s4v6mfqi3kl3d84q1rlhj56z8p2kjksr54m")))

(define-public crate-tianyi_api-0.1 (crate (name "tianyi_api") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "158nv71r1cyfzsisdswk14i7j4kvsa18vz6syip7bl02x5x1z377")))

(define-public crate-tianyi_api-0.1 (crate (name "tianyi_api") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0byxqr04r1cmxnbfh6ypy3frcxvh0fgkdwavimq8157bq2nbnwis")))

