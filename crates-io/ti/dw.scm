(define-module (crates-io ti dw) #:use-module (crates-io))

(define-public crate-tidwall_geohash-0.9 (crate (name "tidwall_geohash") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1d11papjn7l1f6gn0981n2fif09976vxygmwk06va0pifd55bdg5") (yanked #t)))

(define-public crate-tidwall_geohash-0.9 (crate (name "tidwall_geohash") (vers "0.9.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1p8wdadhdq4xg0xbzsi4a4zk0nqs7z3ydcx0cgca1xl3k2l0i8nv")))

(define-public crate-tidwell_guessing_game-0.1 (crate (name "tidwell_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0qnszbmvl4anw615vykzjbdmsk5b3gg9vflw0xwcpzzs28ijpyw9")))

