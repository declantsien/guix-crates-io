(define-module (crates-io ti co) #:use-module (crates-io))

(define-public crate-tico-1 (crate (name "tico") (vers "1.0.0") (hash "0r4lfgkav2l16rdnycmz16cw78mvsl9sqs17zcarqp90w2zkzmxr")))

(define-public crate-tico-2 (crate (name "tico") (vers "2.0.0") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "12dfa8g0r7gcn12i4k2cihk7cwn3lifvzp3pfhh69bwi1s3lyl10")))

