(define-module (crates-io ti qu) #:use-module (crates-io))

(define-public crate-tique-0.1 (crate (name "tique") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.11") (default-features #t) (kind 0)))) (hash "0k6y8viqw7qfbikgrjvkcvnx570i0m2vv888a4y2cmi1r0rqavv8") (features (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.1 (crate (name "tique") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.11") (default-features #t) (kind 0)))) (hash "0sf6gw9cz0y2zmiyhnv9hx0kipjij5jjd2clg0vkhpnvbmirqbwq") (features (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.2 (crate (name "tique") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.11") (default-features #t) (kind 0)))) (hash "1nvhpy9hhr9rlmvpi9z7ik1n6gnizvc4wlbb1cbkpz3ysn0llf5l") (features (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.3 (crate (name "tique") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.12") (default-features #t) (kind 0)))) (hash "07d0hxb1rq1b9xn8322sd7mrsid6jhkdj1m6machrv0if68k6ri1") (features (quote (("unstable" "nom") ("default"))))))

(define-public crate-tique-0.4 (crate (name "tique") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "tantivy") (req "^0.12") (default-features #t) (kind 0)))) (hash "11gyav6pznwwpwwd7bsjvs4npnmfwp5zj0zrxvx83xnrc14k0vw7") (features (quote (("queryparser" "nom") ("default"))))))

(define-public crate-tique-0.5 (crate (name "tique") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tantivy") (req "^0.14") (default-features #t) (kind 0)))) (hash "1yijph1ra7iidw5ynxxwiqw3dl3bw3k33mr3fybpzrc2h3dgc5i0") (features (quote (("queryparser" "nom") ("default"))))))

(define-public crate-tique-0.6 (crate (name "tique") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tantivy") (req "^0.15") (default-features #t) (kind 0)))) (hash "126ah4rd67a8830g3cs0gw153ys7hl5asjff8kwkw4cllsvy0vrd") (features (quote (("queryparser" "nom") ("default"))))))

(define-public crate-tique-0.7 (crate (name "tique") (vers "0.7.0") (deps (list (crate-dep (name "nom") (req "^7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tantivy") (req "^0.16") (default-features #t) (kind 0)))) (hash "0lm0267ikdi827hr506ycm9mbqhl1gxli5256f78s9dqlhwnydj3") (features (quote (("queryparser" "nom") ("default"))))))

