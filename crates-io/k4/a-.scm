(define-module (crates-io k4 a-) #:use-module (crates-io))

(define-public crate-k4a-sys-0.2 (crate (name "k4a-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "1jck0s185bknlc9adyj87k9h6zll05ifp4ggbb153bhrvvbl2pkx")))

(define-public crate-k4a-sys-temp-0.2 (crate (name "k4a-sys-temp") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "151xcqf5q70b0lx35h6if95qsmhfd9hax05831pjxxmh5aywx1k4")))

(define-public crate-k4a-sys-temp-0.2 (crate (name "k4a-sys-temp") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "0b00ip7cyw09dylmmj9zgkfv30f67a6ykdp2ckmrj8i0pkh3nj9d")))

(define-public crate-k4a-sys-temp-0.2 (crate (name "k4a-sys-temp") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "1zwix3klrkmpxga5f2k496vdaa9g2012340vp3633akqx3p93v1f")))

