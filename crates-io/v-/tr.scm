(define-module (crates-io v- tr) #:use-module (crates-io))

(define-public crate-v-trie-0.1 (crate (name "v-trie") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0389hqbrb8l2ka1hniihlqipgf4hy6lhx41nnw7168y7b8yrsdmq")))

(define-public crate-v-trie-0.1 (crate (name "v-trie") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1pmqaa8zxfm39wbvdzp29cyl2sr38mhln607jb939c8ibjs7d3b6")))

(define-public crate-v-trie-0.1 (crate (name "v-trie") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0chlqgd653kihi9flgb5ywh90bh22xzsni04pmimlv60l2bxxgbk")))

