(define-module (crates-io xh sl) #:use-module (crates-io))

(define-public crate-xhSleepFor-0.1 (crate (name "xhSleepFor") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1x8x5rpz2sa6nnckyg1ligiz2a3b3fzzhd14mdhcr3sacab05b2i")))

(define-public crate-xhSleepFor-0.1 (crate (name "xhSleepFor") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0v6fnscbvyjxbgabw4f10p39khw6v7kkc83s5k7wbwi61cmlhyf5")))

(define-public crate-xhSleepFor-0.1 (crate (name "xhSleepFor") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0fx7kzir0vz229ihnmwav61h891r3k5kh5bnfc3pfmdfpm9zhp0w")))

