(define-module (crates-io xh yp) #:use-module (crates-io))

(define-public crate-xhypervisor-0.0.9 (crate (name "xhypervisor") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "0pyh3jx0a7634c8y7lm31zwbmqqfyk0hzg30dc3w6q0xyb6b5384")))

(define-public crate-xhypervisor-0.0.10 (crate (name "xhypervisor") (vers "0.0.10") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "07hzvkm6hpb03yqc2shx2c63zd988sw07jssbgzrc17jr3gfg2v8")))

(define-public crate-xhypervisor-0.0.11 (crate (name "xhypervisor") (vers "0.0.11") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "148py4yykn7qq2b79j0vk31yjjvf0af6drbhx95sy2ifx2kxickz")))

(define-public crate-xhypervisor-0.0.12 (crate (name "xhypervisor") (vers "0.0.12") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "12jz6n0f5ba1c5myyagxf56jrbsl1c16nkszfv2dm584cdr74fk4")))

(define-public crate-xhypervisor-0.2 (crate (name "xhypervisor") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l9my604dv7s449viwih48rvrcx6yq9i574y83m04n4z7dl0gpsa")))

