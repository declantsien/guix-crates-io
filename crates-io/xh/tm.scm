(define-module (crates-io xh tm) #:use-module (crates-io))

(define-public crate-xhtml_minimizer-0.1 (crate (name "xhtml_minimizer") (vers "0.1.1") (deps (list (crate-dep (name "xml_tokens") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1m70yc1dgxw9ky6w0r6b58f9q98i6dwfcsvrkm81i1rj8lj75zym")))

(define-public crate-xhtml_minimizer-0.1 (crate (name "xhtml_minimizer") (vers "0.1.2") (deps (list (crate-dep (name "xml_tokens") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1sgmq0smfrim6frlpipnf8kzwsds253jc1dvajmxglaa5k6kxkch")))

(define-public crate-xhtml_minimizer-0.1 (crate (name "xhtml_minimizer") (vers "0.1.3") (deps (list (crate-dep (name "xml_tokens") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0mk1blxc3n4m6hgjbw5nwa5pl6064xgkzqg1yvcy6zbimdg2fmiy")))

(define-public crate-xhtml_minimizer-0.1 (crate (name "xhtml_minimizer") (vers "0.1.4") (deps (list (crate-dep (name "xml_tokens") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1pnxyivpvs3fyzwd7r0yl2qv7i56d2kvs4sw1pwg2z7ipaifx3h4")))

(define-public crate-xhtmlchardet-0.1 (crate (name "xhtmlchardet") (vers "0.1.0") (deps (list (crate-dep (name "toml") (req "*") (default-features #t) (kind 2)))) (hash "0q9y9a1n5vyx3pvsb3s0g3vmna16xmrb8qpi59wa45yfx1zv90fd")))

(define-public crate-xhtmlchardet-1 (crate (name "xhtmlchardet") (vers "1.0.0") (deps (list (crate-dep (name "toml") (req "^0.1.23") (default-features #t) (kind 2)))) (hash "0wd7izwwhf1c637hc0zc95zmcs4xn07dlnzng7cb639gmlm3dpcb")))

(define-public crate-xhtmlchardet-1 (crate (name "xhtmlchardet") (vers "1.0.1") (deps (list (crate-dep (name "toml") (req "^0.1.23") (default-features #t) (kind 2)))) (hash "1zzmhbr7142c5plnx06day0wvi1w1lc1grdw0zqfyqv1c8isc0j7")))

(define-public crate-xhtmlchardet-2 (crate (name "xhtmlchardet") (vers "2.0.0") (deps (list (crate-dep (name "toml") (req "^0.2.1") (features (quote ("serde"))) (kind 2)))) (hash "1mqys50avnr8h5r9qi32iy25jk2ng3qid2wb1h57adnianpxbnzv")))

(define-public crate-xhtmlchardet-2 (crate (name "xhtmlchardet") (vers "2.1.0") (deps (list (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.2.1") (features (quote ("serde"))) (kind 2)))) (hash "13dx08mxxk97s385a5ppvjf46m53h5hsmcpay1manzfrz3b0i4mk")))

(define-public crate-xhtmlchardet-2 (crate (name "xhtmlchardet") (vers "2.2.0") (deps (list (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.2.1") (features (quote ("serde"))) (kind 2)))) (hash "0cz187qsrp55da7aws10fqmr79jd5blh0wqa6lkg8m499rq73i5c")))

