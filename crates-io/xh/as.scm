(define-module (crates-io xh as) #:use-module (crates-io))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.0") (deps (list (crate-dep (name "gxhash") (req "^2.2.3") (default-features #t) (kind 0)))) (hash "0mbbaslaaiwyymbd2hfkg3c1rs3mkss1i96mp1sragwg4i8m0nyr")))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.1") (deps (list (crate-dep (name "gxhash") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "0rdqzxsp2mwz7jpi1rk39lhdcmd39w4z301xb2mi254i93hay1qh")))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.2") (deps (list (crate-dep (name "gxhash") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1l4qbij7jq6bq59s5qmz4im6dr24ldn4pwf5iflhkf9bixk570dq")))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.3") (deps (list (crate-dep (name "gxhash") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "01djcm451np73f75k6l58qjisgh1yl4irbxlcrjwlx0r27imygl2")))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.4") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "082f82g9l3cacb0511xm3xzbyr9dsb9dv8sr6zkx96qpdkf2lv03")))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.5") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1a239hcifcq08insj0ayisl2wxg567815710lyd685nbrscpz5c2")))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.6") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1h7za031n2vmbb91kvyh5sbg6sn2v4s1lhpbxq4iry4plssgq51b") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.7") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1clfnplxq07mfjh19wq2yd9j848my4nz7hpf583s6z3qib4c9cnz") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.8") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17i2q36ki3xdkqgfnvys4wwx5xj2703b04v6wqsi36zlzw2c6hsm") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.9") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "19mkyk3vcrzipdk9qq7q4lqsxk15sb6mcsv2kkn5sr82r7arlvs8") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.10") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "13dnz5sq055c72x9r9jx6wjhd7148a79k0ynb2wvvgp5ypbj0gqp") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.11") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0xh3im8sh7dsf3bsj85drh5zcx7jildgimr0h13mnar1dfchfbp1") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.12") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0dm1s4hxkk9yfs2s8c14w7chh0smpkm6drpxijk86hg3m8if2hzj") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.13") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1dlxxm939i9ghvba1hslfyds3zs6fw8yy773bp96snb90ipllidx") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.15") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0d2axpg1g8b54i45qr8v86qf8k3vgq2gdqgahd5423k8sndn00h2") (features (quote (("xhash") ("hasher") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.16") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "1g57i9vndzgfba54if5ds266gbwp8xya79y6qb12r25zn8h883xv") (features (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.17") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "00rnm5b7ssdizrpprqgrx9qi6bz11hnvnpqlxb1m0200qa4lp5gb") (features (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.18") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "1yqrl8xdwc0sgc4i76xv2z6vgwb53sdjzgf441mxyh02vi5zg90x") (features (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash") ("default" "xhash"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.19") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.7") (optional #t) (default-features #t) (kind 0)))) (hash "0xhnmpprxws3ikyq7f543vb73b1r422vq6zrab0x8r5wnz3vcwaw") (features (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash" "bin_li") ("default" "xhash") ("bin_li")))) (v 2) (features2 (quote (("speedy" "dep:speedy" "bin_li"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.20") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.7") (optional #t) (default-features #t) (kind 0)))) (hash "0b6pkrc6gw5lyadzss9f3bhraz2n7cbvsp5s2bx7pnvxh8yhffmy") (features (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash" "bin_li") ("default" "xhash") ("bin_li")))) (v 2) (features2 (quote (("speedy" "dep:speedy" "bin_li"))))))

(define-public crate-xhash-0.1 (crate (name "xhash") (vers "0.1.21") (deps (list (crate-dep (name "gxhash") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "intbin") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.7") (optional #t) (default-features #t) (kind 0)))) (hash "070i68hnbas85qha22ycs7a5xm63rh5qycha6yhs7y7gyfwcj0sy") (features (quote (("xhash") ("hasher") ("hash_li" "hasher" "xhash" "bin_li") ("default" "xhash") ("bin_li")))) (v 2) (features2 (quote (("speedy" "dep:speedy" "bin_li"))))))

