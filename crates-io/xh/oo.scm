(define-module (crates-io xh oo) #:use-module (crates-io))

(define-public crate-xhook-rs-0.1 (crate (name "xhook-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xhook-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06v9yfm2rqx924q1jh6364f16a4pphvsv72xz36ma3vya9miznv0")))

(define-public crate-xhook-rs-0.2 (crate (name "xhook-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xhook-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0w9n4qc5dg7qnbqhm6q213vwzcpnd7z19q8nzyg022mn7wwyr1nj")))

(define-public crate-xhook-sys-0.1 (crate (name "xhook-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yg6magm8s6l12sfrnfpwayqja47v46w8raz86fa9jqiwagwg4yp")))

(define-public crate-xhook-sys-0.2 (crate (name "xhook-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fpkgbdli3wpq6w936gkzf2ngm3c8sflcixivs95yldb1mflpypp")))

