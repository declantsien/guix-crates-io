(define-module (crates-io xh uu) #:use-module (crates-io))

(define-public crate-xhuuid-0.1 (crate (name "xhuuid") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1a54mvar48ipw5srdyvlclfd5wy6rbv16g51zgfm1pws4b15766a")))

(define-public crate-xhuuid-0.1 (crate (name "xhuuid") (vers "0.1.1") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "00zg4k7a1dgzv22dk5fhqbpj9hz7c6wf468nb1ar85vywal25b6g")))

