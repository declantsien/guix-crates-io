(define-module (crates-io rp ac) #:use-module (crates-io))

(define-public crate-rpack-0.1 (crate (name "rpack") (vers "0.1.0") (hash "0dywaqc1phnzqs70fjya2k6s10sl099f7ivfi8vjbkfdjlxyjyzw")))

(define-public crate-rpack-0.2 (crate (name "rpack") (vers "0.2.0") (hash "07wy5ciqmqv4fzssl294jcm68bywvagm0n7bxq436hplm7iggx7k")))

(define-public crate-rpack-0.2 (crate (name "rpack") (vers "0.2.1") (hash "1qsdm0idbh81l3lpmqvl0fmyfsi58ljmgfpjlvkdd7fwjf43wv8f")))

(define-public crate-rpack-0.2 (crate (name "rpack") (vers "0.2.2") (hash "175gwh0a1c216c2p533kzd853x457zcz2c4k2srv9jhffppz8zfz")))

(define-public crate-rpacket-0.1 (crate (name "rpacket") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pgp") (req "^0.12.0-alpha.2") (default-features #t) (kind 0)))) (hash "1928nij193zjihazhvfmchww4kpk155yqpfzxhlmdiqb5kjii8wg")))

(define-public crate-rpacket-0.1 (crate (name "rpacket") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pgp") (req "^0.12.0-alpha.2") (default-features #t) (kind 0)))) (hash "1jsz3rdnwyh0sr53lapqwzbcnh4izjh4przl7g68db65hmnbihp4")))

