(define-module (crates-io rp lu) #:use-module (crates-io))

(define-public crate-rplugin-0.1 (crate (name "rplugin") (vers "0.1.0") (hash "181lw80apdll0knw0m76jm1hq8jgph9wcg38qb8cj8vvklril4nr")))

(define-public crate-rplugin-0.2 (crate (name "rplugin") (vers "0.2.0") (deps (list (crate-dep (name "abi_stable") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rplugin_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "string_cache") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "swc_common") (req "^0.14.6") (features (quote ("plugin-base"))) (default-features #t) (kind 0)))) (hash "1pyi63pzxbn81ffixnrnk8hsadp5zgmpz4l2yz5v3wwlwkvz5k0k")))

(define-public crate-rplugin-0.2 (crate (name "rplugin") (vers "0.2.1") (deps (list (crate-dep (name "abi_stable") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rplugin_macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "string_cache") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "swc_common") (req "^0.14.6") (features (quote ("plugin-base"))) (default-features #t) (kind 0)))) (hash "1fjxp87imc1wcjkpl9nwjsxy5rif9gcvlh3ir5m1fhghq0xql1fh")))

(define-public crate-rplugin-0.3 (crate (name "rplugin") (vers "0.3.0") (deps (list (crate-dep (name "abi_stable") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rplugin_macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "string_cache") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "swc_common") (req "^0.15.0") (features (quote ("plugin-base"))) (default-features #t) (kind 0)))) (hash "0arvyy3r1ad5m16y3h4613f8gpsb89m2dh2w245yzv3mjzqdp1dn")))

(define-public crate-rplugin_macros-0.1 (crate (name "rplugin_macros") (vers "0.1.0") (hash "0g62qbg20ywjirq1q7vv5fbxqcxzbi74n4lpx8irdc747s3xhrrg")))

(define-public crate-rplugin_macros-0.1 (crate (name "rplugin_macros") (vers "0.1.1") (deps (list (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "swc_macros_common") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zyxg8qlglv39nq0wv4sr47b751ix6dbxagrjyvqfv7hwd25dwpg")))

