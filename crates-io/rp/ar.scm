(define-module (crates-io rp ar) #:use-module (crates-io))

(define-public crate-rpar-0.1 (crate (name "rpar") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "16s1h3jhj4y0813h60ldl6rfc6fh7zc0y4pm4pn2ai364zy62vw5")))

(define-public crate-rparallel-0.1 (crate (name "rparallel") (vers "0.1.0") (deps (list (crate-dep (name "rustyline") (req "^11.0.0") (default-features #t) (kind 0)))) (hash "0hqa1a08fnh6n9r2mgqc3jr2w5ly66bdr0gzxcjjfz0i56gqvm40")))

(define-public crate-rparif-0.1 (crate (name "rparif") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "httpmock") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1kg57crmvw2pwj46yzp5s5aha7iy6ag8y5y0nfqfr2z25hp89sh4")))

(define-public crate-rparif-0.1 (crate (name "rparif") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "httpmock") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1y23k1vhyxqwycryksfs9pv2w8087lg14ppd9jnql2b0gwp8ixai")))

(define-public crate-rparse-0.1 (crate (name "rparse") (vers "0.1.0") (hash "1srll04i2zdx1x1blz54r2xj3ml3dxgpv1a1y5yqr1w5lahw2hdv")))

(define-public crate-rparse-0.1 (crate (name "rparse") (vers "0.1.1") (hash "1ckr4qnkj4x06qina6n6hppnw4srgzsb8p7m001ayjrlmymb2v26")))

