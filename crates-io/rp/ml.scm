(define-module (crates-io rp ml) #:use-module (crates-io))

(define-public crate-rpmlib-0.0.1 (crate (name "rpmlib") (vers "0.0.1") (deps (list (crate-dep (name "rpmlib-sys") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1j42crsahhgk62cp7qvw4hbdam7a7dhydxjwf3v390741b8xnrfm") (features (quote (("default" "rpmlib-sys")))) (yanked #t)))

(define-public crate-rpmlib-0.1 (crate (name "rpmlib") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "rpmlib-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j83nydnpx92xs2an7acjzffhfadmkjw4qs05w7vb37x1n88jc2r") (yanked #t)))

(define-public crate-rpmlib-0.1 (crate (name "rpmlib") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "rpmlib-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nc1w41smqmz50xiqkyxb6ggg3bgk2wfxv28gd5k797qs5j99f56") (yanked #t)))

(define-public crate-rpmlib-0.1 (crate (name "rpmlib") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "rpmlib-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1") (default-features #t) (kind 0)))) (hash "076441bqv8hy1c5y1j5syabnpbydyqn1mnwp5by4yikvwwxymck2") (yanked #t)))

(define-public crate-rpmlib-0.99 (crate (name "rpmlib") (vers "0.99.0") (hash "1nn0naw3nzfm12ffgq9n8ndqczrr1m6ijza7vak2bh61q9n9gc9y") (yanked #t)))

(define-public crate-rpmlib-sys-0.1 (crate (name "rpmlib-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.35") (default-features #t) (kind 1)))) (hash "14mwphz6h8qkj0rxcmfabyql7i5pywf139h7p3g0d9qjv3kj2hkl") (yanked #t)))

(define-public crate-rpmlib-sys-0.2 (crate (name "rpmlib-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.35") (default-features #t) (kind 1)))) (hash "0mhf934wrrmcsxni73z2wfrz799gd2y6gr3lcgni8lh19nk7mrdp") (yanked #t)))

(define-public crate-rpmlib-sys-0.3 (crate (name "rpmlib-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.35") (default-features #t) (kind 1)))) (hash "01nhk3ss0z4rhbq4y6yc7m56kkvb9namjbcp2zc7xh1mh7zqdhyd") (features (quote (("rpmsign") ("rpmbuild") ("default")))) (yanked #t)))

(define-public crate-rpmlib-sys-0.4 (crate (name "rpmlib-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.35") (default-features #t) (kind 1)))) (hash "1crhj2610vd3rz5gcjm6qqg99rjrs3mlkadb24dvg2f907wy0vsk") (features (quote (("rpmsign") ("rpmbuild") ("default")))) (yanked #t)))

(define-public crate-rpmlib-sys-0.4 (crate (name "rpmlib-sys") (vers "0.4.1") (deps (list (crate-dep (name "bindgen") (req "^0.35") (default-features #t) (kind 1)))) (hash "1zna7qsc1in3a306jiwn0s4d9xqxnqa9axm0m7bgg41l87b4m8jb") (features (quote (("rpmsign") ("rpmbuild") ("default")))) (yanked #t)))

(define-public crate-rpmlib-sys-0.99 (crate (name "rpmlib-sys") (vers "0.99.0") (hash "0q10ism2mydfgwqrc2sqncwal6c5j5fkij4lf4s1xa6zz55581wj") (yanked #t)))

(define-public crate-rpmlx90640-0.1 (crate (name "rpmlx90640") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0q15hj3mwd2d8pazsf3kv1yr51ifhmjw5s5x90bxyv6ir1ip5j0y")))

