(define-module (crates-io rp tr) #:use-module (crates-io))

(define-public crate-rptree-0.1 (crate (name "rptree") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jhwcia6qfyg1gcrdxyya9bm25ck2fpx690j0qrrsxn915rsp4p4")))

(define-public crate-rptree-0.2 (crate (name "rptree") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rh7vh416b3s0ayjk8lpkqgyihd2dxh5y0mga33pfc2kw0c925rp")))

(define-public crate-rptree-0.3 (crate (name "rptree") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0s15fpnr3x687a9p5kxi629n1j8ab66xj75dp1mqjsd7faa9k12f")))

(define-public crate-rptree-0.4 (crate (name "rptree") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c4yxxxy7041x1sxhpni6r8ic16axxk0xb4bz0ni8f838af96nmd")))

