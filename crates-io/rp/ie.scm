(define-module (crates-io rp ie) #:use-module (crates-io))

(define-public crate-rpiet-0.1 (crate (name "rpiet") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "~0.15.0") (default-features #t) (kind 0)))) (hash "13yr7hgvgzh9j9jl2lwgibi5h61q0qhfbl8w6v9bzldg929fy76p")))

(define-public crate-rpiet-0.2 (crate (name "rpiet") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~2.33.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "~0.15.0") (default-features #t) (kind 0)))) (hash "00cgh6c6v6sdkk3z7qfvszd270lhlfrh0zb9685vnwygh8793r3g")))

(define-public crate-rpiet-0.3 (crate (name "rpiet") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "~2.33.0") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "~0.10.3") (default-features #t) (kind 0)) (crate-dep (name "png") (req "~0.15.0") (default-features #t) (kind 0)))) (hash "0bpsniw9sd0ymzm6qk9cmdxpi844qdhr3xdrcfrhbb2g908d00yv")))

