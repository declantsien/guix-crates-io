(define-module (crates-io rp at) #:use-module (crates-io))

(define-public crate-rpatch-0.1 (crate (name "rpatch") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "patch") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "02fl84nazmdcy5zfcnh6226vfbwjg929v6837hx5xxplz4ahkxbl")))

(define-public crate-rpatch-0.1 (crate (name "rpatch") (vers "0.1.1") (deps (list (crate-dep (name "patch") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "10qlyjpyxi46i3k7ca0abr0rg7fil3h77cy3gb93phs3balcwd2m")))

