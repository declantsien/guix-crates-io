(define-module (crates-io rp mb) #:use-module (crates-io))

(define-public crate-rpmbuild-0.1 (crate (name "rpmbuild") (vers "0.1.0") (hash "1h2n8ckg21950jahwmmyifzm6vnkqi33bdnkgjrigdzapfim9q22") (yanked #t)))

(define-public crate-rpmbuild-0.0.0 (crate (name "rpmbuild") (vers "0.0.0") (hash "1mzqck06l1l5pyb12ybk10fqsrlz5mp831dqc1zk6kcgnn2wprnq") (yanked #t)))

