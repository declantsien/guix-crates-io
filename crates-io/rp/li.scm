(define-module (crates-io rp li) #:use-module (crates-io))

(define-public crate-rplidar_drv-0.5 (crate (name "rplidar_drv") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "rpos_drv") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vhgamd6add2gzl4hpjfvppqfhyn50q0sjys72zb3ma12b1d4ia3")))

(define-public crate-rplidar_drv-0.6 (crate (name "rplidar_drv") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "rpos_drv") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1alj5f781hxqr0qy8lcdnjhvkxirfw42pkyzlpfgir04y9p0cp91")))

