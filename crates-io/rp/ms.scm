(define-module (crates-io rp ms) #:use-module (crates-io))

(define-public crate-rpmsign-0.0.0 (crate (name "rpmsign") (vers "0.0.0") (hash "1fr23slshhpmnf35v81zg2p9gww1bsbs8bqyf4hjnnlfap44z9bq") (yanked #t)))

(define-public crate-rpmsign-0.0.1 (crate (name "rpmsign") (vers "0.0.1") (hash "1vml7cjhnhcffq6kdx07v6cvdhj94fqk9mm7l0x1g9iyr0132qhn") (yanked #t)))

(define-public crate-rpmspec-0.1 (crate (name "rpmspec") (vers "0.1.0") (hash "0ws0zdlz75giwid3npxb51c08jkv06mhwlkbhjzz0fky5kx0q9lf") (yanked #t)))

(define-public crate-rpmspec-0.2 (crate (name "rpmspec") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sailfish") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0f58l5xaavmcjlhhnpbw1nmpbv9dpcfvckk9x35aq7bhqjp06czn") (yanked #t)))

(define-public crate-rpmspec-0.3 (crate (name "rpmspec") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pkgspec") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yblqc3ldzyva7xiv20varxgc087830sjpjiyfsgl144833ab3ai") (yanked #t)))

