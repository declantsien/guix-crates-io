(define-module (crates-io rp lt) #:use-module (crates-io))

(define-public crate-rpltree-0.1 (crate (name "rpltree") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rtshark") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "termtree") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1z2qzjspnw2652lyxcjnf87jx1xwh7b6x33qqq08p2qabqz1q7gl")))

