(define-module (crates-io rp wg) #:use-module (crates-io))

(define-public crate-rpwg-0.1 (crate (name "rpwg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jnk33cg9845k5a4i4vniczyi64qaah03i6x946bm7b908vplhql")))

(define-public crate-rpwg-0.1 (crate (name "rpwg") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "044xgf5pd3v9rdy7jr2l79n1j4ga1163crr66i7mball4d66f9ra")))

(define-public crate-rpwg-0.1 (crate (name "rpwg") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kzaq65k2zc7qgw4xhpr25irisdqxxbml4d968k8r3ys5h15rav5")))

(define-public crate-rpwg-0.2 (crate (name "rpwg") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hx0g5rmdhmh3w608wdxg7vlspfhcinmijv0qx1r5617pcr9yy0l")))

(define-public crate-rpwg-0.3 (crate (name "rpwg") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pp9pnhzlbcqalw7xi03iqj26zjnp55nqy9da3b6n521zyhm5bha")))

(define-public crate-rpwg-0.4 (crate (name "rpwg") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ab0gnm6w1ka2ysng6zfr3lr01sq2q69wh581liy3bl54qwglm29")))

