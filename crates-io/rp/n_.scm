(define-module (crates-io rp n_) #:use-module (crates-io))

(define-public crate-rpn_calc-0.1 (crate (name "rpn_calc") (vers "0.1.0") (hash "1gz5wlxvsqg3m61hvsjhzifz5g45h9zblkckk8x61f555k2kv20j")))

(define-public crate-rpn_calc-0.1 (crate (name "rpn_calc") (vers "0.1.1") (hash "1c6qfafjal2nxvv8l32dm0f03rj1cgzldijwvdbdppxamgfl0k1s")))

(define-public crate-rpn_calc0000123-0.1 (crate (name "rpn_calc0000123") (vers "0.1.0") (hash "1nisj67jag8xjzi4wnpjxgmb3hygl5p57g0szy8qhvfvrvlz9ldi")))

(define-public crate-rpn_calc20221028-0.1 (crate (name "rpn_calc20221028") (vers "0.1.0") (hash "1vvgp1zgrxhrvnd392ipax6am1rf0jqrw9yi4ylxnp3nb6rjdf98")))

(define-public crate-rpn_calc_2022_06_26-0.1 (crate (name "rpn_calc_2022_06_26") (vers "0.1.0") (hash "1afdgp2ljxf4nw4kchw157sqk14pj3dbg38jlpf06yl2fy2yknl1")))

(define-public crate-rpn_calc_53Tanuki-0.1 (crate (name "rpn_calc_53Tanuki") (vers "0.1.1") (hash "0zqnn0k14hdzb0vpfr07cp9h4vgak889zlxwzslmjh85vkwqijs4")))

(define-public crate-rpn_calc_555-0.1 (crate (name "rpn_calc_555") (vers "0.1.0") (hash "1d382a36id7xramdfw30kda7nm91vnpnaiw5ind03dg3c063dfwp")))

(define-public crate-rpn_calc_a-0.1 (crate (name "rpn_calc_a") (vers "0.1.0") (hash "0ahs82d9v07cb88x2ms29p8g7an0wyhlpnxvhf93z9sqn4m4h2ps") (yanked #t)))

(define-public crate-rpn_calc_a-0.1 (crate (name "rpn_calc_a") (vers "0.1.1") (hash "038cwnpmy547drh6gha2fdlf00z7dx9x8c635a929wa2hkb2w7w1") (yanked #t)))

(define-public crate-rpn_calc_a-0.1 (crate (name "rpn_calc_a") (vers "0.1.2") (hash "0nm7drxkf6xkvca5g9pna57w14a1gnjqf4rzwaqpycs4vmxn78yy") (yanked #t)))

(define-public crate-rpn_calc_antoliny0919-0.1 (crate (name "rpn_calc_antoliny0919") (vers "0.1.1") (hash "1j83gf6cdnhxjgz4ls2mj07z0zg0rff46vih49igzs6m1yc2myfc")))

(define-public crate-rpn_calc_by_banbiossa-0.1 (crate (name "rpn_calc_by_banbiossa") (vers "0.1.0") (hash "0p1sj1kww6rm6h1572li8g80zgvlxmiakb1vzdax6m1wl0jq6y7l")))

(define-public crate-rpn_calc_copy_package-0.1 (crate (name "rpn_calc_copy_package") (vers "0.1.0") (hash "100jh9pik6qr4bipp1cbn1mmz84ydhdsncf2bdwscb01b4jbfbn0")))

(define-public crate-rpn_calc_empty-0.1 (crate (name "rpn_calc_empty") (vers "0.1.0") (hash "1q4c0icx8fsv10i0hznal8c9fjbwz2422jjrin91fq427xrnk1q4")))

(define-public crate-rpn_calc_hellojaewon-0.1 (crate (name "rpn_calc_hellojaewon") (vers "0.1.0") (hash "1bcmwi4d2djf8kffpjpsg7rqniy6zrg6l3bhv2n0hlspcfyr07vn")))

(define-public crate-rpn_calc_JM-0.1 (crate (name "rpn_calc_JM") (vers "0.1.0") (hash "0jxb3g4xys0qw1l8mb29plgivrz744c0kc6q4nisgq3rpgfnawyr")))

(define-public crate-rpn_calc_JM-0.1 (crate (name "rpn_calc_JM") (vers "0.1.1") (hash "00prywr7d6lvgkb1vb14f2vmsl1gc1x2j06fvz783mk704d6n7yc")))

(define-public crate-rpn_calc_joonhoson-0.0.1 (crate (name "rpn_calc_joonhoson") (vers "0.0.1") (hash "0lazh88iqhli7a32mn1jzyshfchkfhqfwpf99q47i4fiy0acgb0z")))

(define-public crate-rpn_calc_jrk-0.1 (crate (name "rpn_calc_jrk") (vers "0.1.0") (hash "1g70dbi3wj10arl8klzq5d6p8racyvc2nk9702d2ac2lva5p5ik2")))

(define-public crate-rpn_calc_karin-0.1 (crate (name "rpn_calc_karin") (vers "0.1.1") (hash "04qniw7kf86nq2ssjy4735390nmf1ahl2v03z6i1vpjb0flvlrix")))

(define-public crate-rpn_calc_karishuteron-0.1 (crate (name "rpn_calc_karishuteron") (vers "0.1.0") (hash "18hc77xnkjvg66h0ndydw0zy2ks2hwv1jgk5mg3iyv1b6d9vyq8b")))

(define-public crate-rpn_calc_kh113-0.1 (crate (name "rpn_calc_kh113") (vers "0.1.0") (hash "1vihk0jg7h4i2lvpxxwfbj07f7y1hdpcbw1bk7yvlx3mj5wsxf9d")))

(define-public crate-rpn_calc_lib-0.1 (crate (name "rpn_calc_lib") (vers "0.1.0") (hash "1ydbvikdiwasq9rnpz5zhxpw2arcik2sdql56dxizc6ws3v50qfs")))

(define-public crate-rpn_calc_mo9-0.1 (crate (name "rpn_calc_mo9") (vers "0.1.0") (hash "0dvad3hzpj0lnd1ia7fhnkplj2jbg53kmfz074fsgdg4kgihg8ml")))

(define-public crate-rpn_calc_motiroll-0.1 (crate (name "rpn_calc_motiroll") (vers "0.1.0") (hash "1imdxahghri5nhnawhsna2pnldp15892lcdgshnkz95xhsydqbhi")))

(define-public crate-rpn_calc_okamoto_r32iefjoa-0.1 (crate (name "rpn_calc_okamoto_r32iefjoa") (vers "0.1.0") (hash "080yv98bn7v8nmwhps76ll2pb2s9fg8y7sznlq0b3k0fq6xy3dkb")))

(define-public crate-rpn_calc_perl-0.1 (crate (name "rpn_calc_perl") (vers "0.1.0") (hash "1plil5mfcwv9flqgkxnm35mznnr3rk2hgzsnkgav300vn7nd8n7f")))

(define-public crate-rpn_calc_perl-0.1 (crate (name "rpn_calc_perl") (vers "0.1.1") (hash "1pcbc87hsfphk31hxayfb1cx3mzf8hgnaljb1d9rxp8v98skb67h")))

(define-public crate-rpn_calc_practice-0.1 (crate (name "rpn_calc_practice") (vers "0.1.0") (hash "0s6sblqqysyf5pxzx6z4qk3ybwbv07s0mg48pmbf07smqqlgz47b")))

(define-public crate-rpn_calc_reader-0.1 (crate (name "rpn_calc_reader") (vers "0.1.0") (hash "1yk0xr8l8gl14pj5svicbmwjd65lximg8d2yif6q3w0zcw9ihamn") (yanked #t)))

(define-public crate-rpn_calc_rust_programming-0.1 (crate (name "rpn_calc_rust_programming") (vers "0.1.0") (hash "1aiqp9v02rk1m4sn91b2103ijxhvvp2j70qlgfapy2l2330mvqzl")))

(define-public crate-rpn_calc_sample1-0.1 (crate (name "rpn_calc_sample1") (vers "0.1.0") (hash "0l3rcnb3ihq4w37i9vkvn15dnj70gfmb12j6yny5c8v4x7nkvpp0")))

(define-public crate-rpn_calc_sog-0.1 (crate (name "rpn_calc_sog") (vers "0.1.0") (hash "0adxd9y95qygw204bgc96q7n60ibnnbhwzfz2d8s3wm358rzlylr")))

(define-public crate-rpn_calc_tamecat-0.1 (crate (name "rpn_calc_tamecat") (vers "0.1.0") (hash "143l049cdpy7r3a7v61gf1840r7rnxalsh4wnm6cxxvmizrb47cs")))

(define-public crate-rpn_calc_taqqu15_test-0.1 (crate (name "rpn_calc_taqqu15_test") (vers "0.1.0") (hash "1aw9cnbk4b3a31pk36sx5x4n1zhqbdxrd74xwnadpi4h5d7ij4kh")))

(define-public crate-rpn_calc_test_neuvecom-0.1 (crate (name "rpn_calc_test_neuvecom") (vers "0.1.0") (hash "0bjvvj282nbkm5j7h9861a6hcjsd6p5x1b057jx3p85b2yadj1kl")))

(define-public crate-rpn_calc_test_pptt_prv-0.1 (crate (name "rpn_calc_test_pptt_prv") (vers "0.1.0") (hash "113par65a57kys91l2wbzpjdip7kkkypxs1b8glcn9ms8ld68dy2")))

(define-public crate-rpn_calc_test_ver-0.1 (crate (name "rpn_calc_test_ver") (vers "0.1.0") (hash "1c6ps9dy2gwgyasg3a3kbhk2z6s3ic9l140yj5hv6pvqf486xy65")))

(define-public crate-rpn_calc_uc_sample-0.1 (crate (name "rpn_calc_uc_sample") (vers "0.1.0") (hash "1wmlxzp5a7p7qyhzvcw8azx6v579fc6ndgpf9x16zqk980vr0hy0") (yanked #t)))

(define-public crate-rpn_calc_uc_sample-0.1 (crate (name "rpn_calc_uc_sample") (vers "0.1.1") (hash "1cms5aiy99zrrjrj92wqvcjipcb2gxp9pabx6wwpmv6wa2azavm9")))

(define-public crate-rpn_calc_uc_sample-0.1 (crate (name "rpn_calc_uc_sample") (vers "0.1.2") (hash "1pcln2cazxbq886agf6h9c7ic8s7z4hxr434pirla7v2160ilizd")))

(define-public crate-rpn_calc_uskn-0.1 (crate (name "rpn_calc_uskn") (vers "0.1.0") (hash "1w0hhdd7gc8313laj6liyi5xinhf6j14s2vdcfzplarxnb5gkrrl")))

(define-public crate-rpn_calc_wip-0.1 (crate (name "rpn_calc_wip") (vers "0.1.0") (hash "0ik7cgwwgadyqi2dccfbhi5bha00gikm4m4w5c92fnpapw0dkraz")))

(define-public crate-rpn_calc_yhirano55-0.1 (crate (name "rpn_calc_yhirano55") (vers "0.1.0") (hash "0snfyby6icaakkzaj6f2930aa5yci7s9bj3fawl4hpdfijmvz8f8") (yanked #t)))

(define-public crate-rpn_calculator-0.1 (crate (name "rpn_calculator") (vers "0.1.0") (hash "1ri1mq6q0jr0y0kvz3l7kzfw11yp1ifivf6q12mh8mk6b7gpmh6h")))

(define-public crate-rpn_calcurator-0.1 (crate (name "rpn_calcurator") (vers "0.1.0") (hash "1mym04ld95kwvhfz4pk6aar238b3f08kn46lyr884aplgwgawpib")))

(define-public crate-rpn_icedpenguin-0.1 (crate (name "rpn_icedpenguin") (vers "0.1.0") (hash "1gzdmvghcl451j44h1p2h51v52i7k81lbhmg27mmgywzsi378xwj")))

(define-public crate-rpn_lib-0.1 (crate (name "rpn_lib") (vers "0.1.0") (hash "1ybdjg7v5ilqkcbhximj6jq0davdxwr01sjxazdcfas9am8vga8c")))

(define-public crate-rpn_lib-0.1 (crate (name "rpn_lib") (vers "0.1.1") (hash "15sidbyjiqsnyfxdc4zkxfp7dk8gy1s022dw7z6f5bppgx4pj48b")))

(define-public crate-rpn_lib-0.1 (crate (name "rpn_lib") (vers "0.1.2") (hash "02164jhlql3ravmwrin3n07wryn9pbik6ys0cxdw482hxpjp19ql")))

(define-public crate-rpn_lib-0.1 (crate (name "rpn_lib") (vers "0.1.3") (hash "1lcm6i3p1l2v1aacwlx662bpdzj8d9cankggnl613ad1vi6w3ags")))

(define-public crate-rpn_lib-0.1 (crate (name "rpn_lib") (vers "0.1.4") (hash "12kkskcbh9w9fl82l6l2lcjlj392sm7y7y0vggajlhnpi28p9wmb")))

(define-public crate-rpn_lib-0.1 (crate (name "rpn_lib") (vers "0.1.5") (hash "06yh5nc8wz2nfn8b03q5k89bgxx10byhhl07iy0d4gyckgfsdg8j")))

(define-public crate-rpn_lib-0.1 (crate (name "rpn_lib") (vers "0.1.6") (hash "1kf49yy5p1m2sw44kqp010knznpi2r78prxnx6a5n8z0528rzvl2")))

