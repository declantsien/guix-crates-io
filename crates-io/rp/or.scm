(define-module (crates-io rp or) #:use-module (crates-io))

(define-public crate-rportaudio-0.1 (crate (name "rportaudio") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.39") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.3") (default-features #t) (kind 1)))) (hash "1cm3zv2ybbikf98wmw6nw4v399dikkbcr1y3xa2awradi8wv28p2") (yanked #t)))

(define-public crate-rportaudio-0.1 (crate (name "rportaudio") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.39") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.3") (default-features #t) (kind 1)))) (hash "01yrc00hsdx4247gp742yp95zrr3n25crb7aq67g0pd7r2hxwn4k")))

