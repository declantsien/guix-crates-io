(define-module (crates-io rp ni) #:use-module (crates-io))

(define-public crate-rpni-0.1 (crate (name "rpni") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf8-chars") (req "^1") (default-features #t) (kind 0)))) (hash "1wcwbb9yz14f1jjpaqis5vxpibjqm4yx9mcjdw42267r3kbz184h")))

(define-public crate-rpni-0.1 (crate (name "rpni") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf8-chars") (req "^1") (default-features #t) (kind 0)))) (hash "1ivp1d6rypialrx7dapvr6dhf5v13fl86idkpxijxzx0zv9xq7g7")))

