(define-module (crates-io rp gf) #:use-module (crates-io))

(define-public crate-rpgffi-0.0.1 (crate (name "rpgffi") (vers "0.0.1") (hash "11clgdxgx2p2x1ds215z5v5lf6xd39zk6m1zhdgrbn53j1lgimz7")))

(define-public crate-rpgffi-0.1 (crate (name "rpgffi") (vers "0.1.0") (hash "0njjsdj13lcvyg88vq3sjlcbb7kdy06fcgk08mvlawc7zz7szhar")))

(define-public crate-rpgffi-0.1 (crate (name "rpgffi") (vers "0.1.1") (hash "10wm230jb5gpw9fm6lbk4m5r6w2gjilqskg5ixl0wfjgrwljz6gk")))

(define-public crate-rpgffi-0.2 (crate (name "rpgffi") (vers "0.2.0") (hash "132yvbwj32k293lasjkxsxs6595pixm5z6v1d17bx9l0gwspdn0k")))

(define-public crate-rpgffi-0.3 (crate (name "rpgffi") (vers "0.3.0") (hash "00hmzrjfimd5as6w74kmlws2hfplwh5s937f0pg59jbjfqwjnb5w")))

(define-public crate-rpgffi-0.3 (crate (name "rpgffi") (vers "0.3.1") (hash "1ky1i25mb6qsii5s6sr287mn6n2r08lyfdl4d8q7837k8rgv3znr")))

(define-public crate-rpgffi-0.3 (crate (name "rpgffi") (vers "0.3.2") (hash "0g1vjxmpqcfm1f5pwq8zg143386pfnykm3saz58dfnzybc8irra5")))

(define-public crate-rpgffi-0.3 (crate (name "rpgffi") (vers "0.3.3") (hash "164pfm1kdq2pbznj98hakqwy47cmydjd41casiwq0afvf8fmdydk")))

