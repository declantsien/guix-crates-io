(define-module (crates-io rp ca) #:use-module (crates-io))

(define-public crate-rpcap-0.2 (crate (name "rpcap") (vers "0.2.0") (deps (list (crate-dep (name "bytepack") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "bytepack_derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "14q0xjmvbky51nn9mk59wvpi2f506fnaf30hijfr16hhk4ds4h93")))

(define-public crate-rpcap-0.2 (crate (name "rpcap") (vers "0.2.1") (deps (list (crate-dep (name "bytepack") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "bytepack_derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "1ps8qwh7c7v5s2l8i4fckw12rgkx6f5vg9xp678lpgdnwhwijmna")))

(define-public crate-rpcap-0.2 (crate (name "rpcap") (vers "0.2.2") (deps (list (crate-dep (name "bytepack") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "bytepack_derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "1n8gx36m6606855dsski0wszfpcl68xn09rxa046cqgl0sipma4d")))

(define-public crate-rpcap-0.3 (crate (name "rpcap") (vers "0.3.0") (deps (list (crate-dep (name "bytepack") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "bytepack_derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "0jkfyfs4cmwizlhc8kn271c3gq5zib3j7rmc74b6c3rbmbs5pw04")))

(define-public crate-rpcap-1 (crate (name "rpcap") (vers "1.0.0") (deps (list (crate-dep (name "bytepack") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1flrr069fkagcr8s94njlpsndd9r4bws6549myxhynzq1awvqj3l") (features (quote (("default"))))))

