(define-module (crates-io rp uc) #:use-module (crates-io))

(define-public crate-rpuc-0.2 (crate (name "rpuc") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)) (crate-dep (name "rpu") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1m2fwgsvqgxp1d082qq512fg9zlzvrvmvmqyv6ib11wyvdg4i4la")))

(define-public crate-rpuc-0.2 (crate (name "rpuc") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)) (crate-dep (name "rpu") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16y9969av66mjk5z7g5bzj819gsivmrk5zwp2swl6cqd6yybflss")))

(define-public crate-rpuc-0.2 (crate (name "rpuc") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)) (crate-dep (name "rpu") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0knadm6f83liar05ab3f9gklcjq0vhlm1jbpmdsvpqabrccp0ms0")))

(define-public crate-rpuc-0.2 (crate (name "rpuc") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)) (crate-dep (name "rpu") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "10p7hvprqd4fk0q3i72z3k9h99ny4i1771pw7gydyqjkwhwp7w53")))

(define-public crate-rpuc-0.2 (crate (name "rpuc") (vers "0.2.5") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)) (crate-dep (name "rpu") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0yqwg9vpyxywn43p8znryncxfsr0c3ggwrw9axlgkf50c7jfh6bj")))

(define-public crate-rpuc-0.3 (crate (name "rpuc") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (default-features #t) (kind 0)) (crate-dep (name "rpu") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1l1h32ypp1sj7h9n7c1gkm4y3sqdrpzs0d8n3275wvcmk4c71mby")))

