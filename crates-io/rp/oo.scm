(define-module (crates-io rp oo) #:use-module (crates-io))

(define-public crate-rpool-1 (crate (name "rpool") (vers "1.0.0") (hash "1wj0lqyws2yn863lrq6m3ljwmb185w5dz63j2c31031ynyd74q5v")))

(define-public crate-rpool-1 (crate (name "rpool") (vers "1.0.1") (hash "1i8i2k5q5k5apjgj35yf4kqxyjm6fv1q6zyb2347m7pkcai65alk")))

(define-public crate-rpools-0.2 (crate (name "rpools") (vers "0.2.2") (hash "1f0iqipndkcnr9p5kdfdvh22n3zyb54j3d6v9h27vzm0pivdn7w8") (yanked #t)))

(define-public crate-rpools-0.2 (crate (name "rpools") (vers "0.2.3") (hash "111qdlwiigmglfbivb8kr3916apkv3z0jj74nphlqpn0wqyay1j3") (yanked #t)))

(define-public crate-rpools-0.2 (crate (name "rpools") (vers "0.2.4") (hash "0qv9gj5sr1p63bm1f2fzrrv2yab6wnww11ild5pm2m7m8qvkghf4")))

(define-public crate-rpools-0.3 (crate (name "rpools") (vers "0.3.0") (hash "0ab72mrzp7dqc2bic4lrxk0b7rb7013k9nm1lcn2ib786yb5bw80")))

(define-public crate-rpools-0.3 (crate (name "rpools") (vers "0.3.1") (hash "000bi562kiaw8l6amf3qnffdkq5by88kmh4iy8ws0xdpcinkrbv1")))

