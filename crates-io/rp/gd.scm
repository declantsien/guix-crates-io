(define-module (crates-io rp gd) #:use-module (crates-io))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "14z3hvk2ffsyy54hqkdrmzh00s18jq4wk33lad9yvc3yvazxyynq")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1wdfh1mdf0g3y9ycjwg2v9710jjwq3fawgc9gwd5vrpv8nfz3kmy")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0xg8rmv20mmiaxwh35afbs52n768hm26as9vwclsq6wznyxjs3ip")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1cd815vgvnmfxk3f49bxly5x20vqfnicsr076wvfcjnkl1zkb5cj")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "08i2lpim0izc3zir2i17kvrdfs91aj2jxbm0c2a9wyrmd1j3x6zx")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "18d4y25cp8sy5r9s4a95sxszilc8qpbszgg6pmmv9avcbzs4fgbn")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1xxsl6sif5ps7h1yac8cxbxgxjgpi38pdsqzm1ag67khkqvhicrk")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.2.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0jrvnkji6m06l8xbvkshma51csffjfxsf51m8qrrdcjrb98368fd")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.2.2") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "048sfad72d77jmycsz3m6ydgpbb3qpw4mmzmz4zh3vzbd19rfzmn")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.2.3") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0wpjpbq3lwp5hd7z8f0h9h2dxf9q8k3d9r47d91j1bgfb7r1mhpd")))

(define-public crate-rpgdk-1 (crate (name "rpgdk") (vers "1.2.4") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1zam21kpb21jdpdr898wmb0pjiilcni9v1nz9bmsjcwaqvrfhdlv")))

