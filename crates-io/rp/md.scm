(define-module (crates-io rp md) #:use-module (crates-io))

(define-public crate-rpmdb-0.1 (crate (name "rpmdb") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (features (quote ("fs"))) (target "cfg(unix)") (kind 0)) (crate-dep (name "rusqlite") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1qx4mzfcry94q1dk5pc6dhphb0dmki70b0pvfph0shgx6a3vdss3")))

