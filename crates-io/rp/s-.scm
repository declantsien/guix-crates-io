(define-module (crates-io rp s-) #:use-module (crates-io))

(define-public crate-rps-sys-0.1 (crate (name "rps-sys") (vers "0.1.0") (hash "0a4gawcvg87f7ica99vk220kdn8arzhvqilfqcdx4q6l51qz3lfn")))

(define-public crate-rps-sys-0.2 (crate (name "rps-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0rnfwd88pw35r5fd6ij6r5qhrbhhcjcp7s6p0v13i6125pwryhyy")))

(define-public crate-rps-sys-0.3 (crate (name "rps-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0ysvmiy4v9i7xwk3jw6gjz46800c2jl6bnfc5mwk49cxzvycwd7z") (features (quote (("vk") ("d3d12") ("d3d11")))) (links "rps")))

(define-public crate-rps-sys-0.4 (crate (name "rps-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "18p7i2g52mmr1ks387lyikrxwmcb0hzlvibsnwzx86jj3149hrw2") (features (quote (("vk") ("d3d12") ("d3d11")))) (links "rps")))

