(define-module (crates-io rp lo) #:use-module (crates-io))

(define-public crate-rplot-0.0.1 (crate (name "rplot") (vers "0.0.1") (hash "0j985xpj4f91mw47ygsrmdmj1k18knj1hqjd1gpzl4ywjyqygdba")))

(define-public crate-rplotlib-0.1 (crate (name "rplotlib") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "prost") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.8") (default-features #t) (kind 1)))) (hash "1wmgyz0a2v5l750yiyh3g0z6qs0yl6d4wsrj763738sn54w0pmc4")))

