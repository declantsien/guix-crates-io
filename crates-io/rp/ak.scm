(define-module (crates-io rp ak) #:use-module (crates-io))

(define-public crate-rpak-0.1 (crate (name "rpak") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "19rl4m1vsvm70gk2fx2pbvgl88n1vwz6xv0qcn43a78gqjx59mjc") (features (quote (("std" "byteorder/std") ("default" "std"))))))

(define-public crate-rpak-0.2 (crate (name "rpak") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)))) (hash "1jyf7chw6wfmhvyyn40bmka93v5difild118rmwy4v1ixn7xr6yl") (features (quote (("std" "byteorder/std") ("default" "std"))))))

