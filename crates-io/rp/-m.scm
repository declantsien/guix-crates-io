(define-module (crates-io rp -m) #:use-module (crates-io))

(define-public crate-rp-mock-0.1 (crate (name "rp-mock") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1nyf3ilir1dpfcfjc3zf2kdq230hki6xdlri444sbyhhkcjdvb3a") (yanked #t)))

