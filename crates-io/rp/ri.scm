(define-module (crates-io rp ri) #:use-module (crates-io))

(define-public crate-rprime-0.1 (crate (name "rprime") (vers "0.1.0") (hash "1gs9cpjp5gbsln9c11my0cibg59zyj2ng6gjgbgvbdbbchdfsack")))

(define-public crate-rprime-1 (crate (name "rprime") (vers "1.0.0") (hash "13pzgq2pz5in2qzfns25hrzza5dn59nrd9lcby62grqld33wcv4i")))

