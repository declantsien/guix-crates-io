(define-module (crates-io rp ls) #:use-module (crates-io))

(define-public crate-rpls-0.1 (crate (name "rpls") (vers "0.1.0") (hash "1fd7xm24vl3x7jh2fczr73ipa38ykjnvk6xzwlj53iiq3p4dwh5w") (yanked #t)))

(define-public crate-rpls-0.1 (crate (name "rpls") (vers "0.1.1") (hash "1q9yxb3gxkzbyxy1x7ph9062vh2635g6b3j6jjhbfg8g9jp4lxn7") (yanked #t)))

(define-public crate-rpls-0.1 (crate (name "rpls") (vers "0.1.2") (hash "0bza85qwqqghs08srcdgd6mi16cb0p5p17y11divgs0slnlr38bh")))

