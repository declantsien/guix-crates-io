(define-module (crates-io rp su) #:use-module (crates-io))

(define-public crate-rpsutil-0.0.1 (crate (name "rpsutil") (vers "0.0.1") (hash "0wxygbic0ar9nxpx2wmjgg3kjf4klsp117kbyqwy8j7j8f4yj9jy")))

(define-public crate-rpsutil-0.0.2 (crate (name "rpsutil") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12i50rqqga12r5i5zfmak3x78qjs3rng8j7f0x5j655gn0jyk1xa")))

(define-public crate-rpsutil-0.0.3 (crate (name "rpsutil") (vers "0.0.3") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zdqsp4cjbbg252kchhmy2nn8a29f7933kygkmai73rc84xsqamv")))

