(define-module (crates-io rp cl) #:use-module (crates-io))

(define-public crate-rpcl2-derive-0.1 (crate (name "rpcl2-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1csxv60ca38ag1xf3cr3cl83safk5vdxjs40i01mw7jpw5sdmnv5")))

(define-public crate-rpcl2-derive-0.2 (crate (name "rpcl2-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1mzmd7l86bdkkd6vx2ds0cqrbljxcds9br850a70mids4qjp06wk")))

