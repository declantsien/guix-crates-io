(define-module (crates-io rp mv) #:use-module (crates-io))

(define-public crate-rpmvercmp-0.1 (crate (name "rpmvercmp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1vxb4mq7dca2m9w9iww78hyxzhv1ihwn2xhjic81bkysnlm9awrv") (features (quote (("cmdline" "clap")))) (rust-version "1.60")))

