(define-module (crates-io rp os) #:use-module (crates-io))

(define-public crate-rpos-0.1 (crate (name "rpos") (vers "0.1.0-alpha.0") (hash "1gyp3nw7ac2rpw1hpd9rv82226flla9clv6an7jivwga6cwdl99b") (yanked #t)))

(define-public crate-rpos-0.1 (crate (name "rpos") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)))) (hash "1wir1hxpajxiwa0zwz06jl4j3f3dmhqai952cs8yhivpsf9c0fdz")))

(define-public crate-rpos-0.2 (crate (name "rpos") (vers "0.2.0-alpha.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)))) (hash "1kl7i62hm913jwfkfdp58zvrdjv2j5xwm55rz2f3p89bkh1klr7d") (yanked #t)))

(define-public crate-rpos-0.2 (crate (name "rpos") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)))) (hash "0a8z2kpjclinvl9697rvzvxs1zs7mwyfbf8sgdx9bbcb51x4yrfh")))

(define-public crate-rpos-0.2 (crate (name "rpos") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)))) (hash "0fbp32z6ilybi16g23n96kap0mcjvxyl44wzqnnqpzdqpspz1biy")))

(define-public crate-rpos-0.2 (crate (name "rpos") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)))) (hash "11i5749lj7m7v75ggg6pmnvm34yq6yc9kzy5pjr1cgpq28swypd2")))

(define-public crate-rpos-0.3 (crate (name "rpos") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)))) (hash "1k5y7mvflz34gbv4rzwhpwrdn96lkssk0zh8552n1lwdy793z3lx")))

(define-public crate-rpos_drv-0.1 (crate (name "rpos_drv") (vers "0.1.0") (hash "1jx335fzhacdafp1sxssy9wk46rxlln8cfsa2wgw9l52wqlvc035")))

(define-public crate-rpos_drv-0.2 (crate (name "rpos_drv") (vers "0.2.0") (deps (list (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)))) (hash "0yrjyqnjj2aqh9qiy2xnkhbynr8154bvi9hzvc40023g1756gdg9")))

(define-public crate-rpost-0.1 (crate (name "rpost") (vers "0.1.0") (hash "01gzfwmfkpi8r5274hnhllyr31jp72mxpk2yhmqyxmpiiagcx49w")))

