(define-module (crates-io kp at) #:use-module (crates-io))

(define-public crate-kpathsea-0.1 (crate (name "kpathsea") (vers "0.1.0") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bjnm2kvnb9dk100iwvbs7kkhh5s38h9w3k8gzpnv176bpbja324")))

(define-public crate-kpathsea-0.1 (crate (name "kpathsea") (vers "0.1.1") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "163m31m7jmk3gbzg95jzzzkbs7xmf6hybfh5cgy0m5h5h48asl8k")))

(define-public crate-kpathsea-0.1 (crate (name "kpathsea") (vers "0.1.2") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1aaafm8bh5s8n188cxdq4bv90kdm6nccspdgw4i4fjpz9m1v8zji")))

(define-public crate-kpathsea-0.1 (crate (name "kpathsea") (vers "0.1.3") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01kbqvj2j0rgqibnq9vm43hciz0i39jn3jcg43i8al1zqvwxcqpb")))

(define-public crate-kpathsea-0.2 (crate (name "kpathsea") (vers "0.2.0") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1l13ppks1603r0x8pdn53ablrqa6f8znjsra6a2y0i9dpkc1aq80")))

(define-public crate-kpathsea-0.2 (crate (name "kpathsea") (vers "0.2.1") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "07kfh8mvblzw7cbcxk23i07kw4vji599rcwvxqv02jvyi09slxyn")))

(define-public crate-kpathsea-0.2 (crate (name "kpathsea") (vers "0.2.2") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "083yp6mnin9hr6hkypafmi9z6mv09dcvifiv620sy28gfinwxg44")))

(define-public crate-kpathsea-0.2 (crate (name "kpathsea") (vers "0.2.3") (deps (list (crate-dep (name "kpathsea_sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "1f5s15siq4cqwbc69fvfvradvbgcwjgnyl3hwyziqq7n28n2vdwy")))

(define-public crate-kpathsea_sys-0.1 (crate (name "kpathsea_sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1npcrdrarxais58z23xfvpr8camsw841ar79w25955vghz63nkzm") (links "kpathsea")))

(define-public crate-kpathsea_sys-0.1 (crate (name "kpathsea_sys") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1b7sqjfgkw5vrszcgf7l2hdvlbi1n0w212r9f0wb8p1abl1jm52a") (links "kpathsea")))

(define-public crate-kpathsea_sys-0.1 (crate (name "kpathsea_sys") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0a7zn9l3cqy1dp91kj8f0waniwl2wl38666hcbvss8yy8z09hd13") (links "kpathsea")))

