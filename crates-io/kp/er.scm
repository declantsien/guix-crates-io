(define-module (crates-io kp er) #:use-module (crates-io))

(define-public crate-kperf-0.0.0 (crate (name "kperf") (vers "0.0.0") (hash "1i03w3g5943wrh4nrm47iwgs98mn3pm2sck2lb1ibx1dp3kiki5b")))

(define-public crate-kperf-0.1 (crate (name "kperf") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (features (quote ("backtrace"))) (default-features #t) (kind 2)) (crate-dep (name "kperf-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0n1nhq0rmnv6di1q5x0k5bi061s33zvm1hi7h8d5mnvzdykc3l9n")))

(define-public crate-kperf-rs-0.1 (crate (name "kperf-rs") (vers "0.1.0") (deps (list (crate-dep (name "kperf-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "07r25s0g00w1xdk59f498rsr996nk1cvly0bd2jj9nx26pzsri1h")))

(define-public crate-kperf-rs-0.1 (crate (name "kperf-rs") (vers "0.1.1") (deps (list (crate-dep (name "kperf-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "183w5krl74v25jxw538y4zgdy77vrsjwzsgkqncrxxwp829haf1x")))

(define-public crate-kperf-sys-0.0.1 (crate (name "kperf-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0ym4n5r97n1hl6i6b20g5xh8gbq271g53xwjk471yk265s7wqkmx")))

(define-public crate-kperf-sys-0.0.2 (crate (name "kperf-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0gn8bn6vyzmby7k3iskag7z59nwjgc72dls7m3vm3ilzl9nb4cr8")))

(define-public crate-kperf-sys-0.0.3 (crate (name "kperf-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0bz4agyzv17ad55w6pzri9jhpk8392xy1mq66212l6kyjzrl4m9g")))

