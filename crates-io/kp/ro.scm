(define-module (crates-io kp ro) #:use-module (crates-io))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-alpla.1") (hash "194gyd02nm7c52yj5b9w949g16hkz2mq0z24ik32prv93043lh9y")))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-alpla.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "108sb76r4xw7cv7x4c8yv5gy2p8zapaq2gjxkjk9cmma0fpiv6pn")))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-alpla.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "0lpf5afclk8gfxqzcbb0ygh98975xzd81zvpx56m4ryiwyyhiibz") (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-alpla.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "1xyjbkbdppvw2jmv7jk963sxpvylp5w4f0yf5rqcwwlpl2jikxdb") (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-alpla.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "10g3nczbd73gwq7dng8hkdy8a3m6vi1b9kf54j6dhmm35d7f8adx") (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-alpla.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "0m4fj0jskicxmca6j6md252l9vwvxn5diwinh1dnrjx37x6yivdf") (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-alpla.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "0bk4n4bzyxkf8f3fwfnr5cx76krsw2gzz8l4rnvvi8gpxb1x7g12") (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-beta.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "046xj5xjd2saz1gs12lwwxv4ifg074fzihngg0cr74zgi56dr635") (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-beta.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "08f2ak9hkd8lvgvsk4x3cyncn7m97g0y00z3kdg26gf41g8a87rg") (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-beta.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "1gxf7vkg9zqfzazliqkzdqagy1xwr09vc1cj3x6cb6xx12y5bps4") (features (quote (("builtin_diagnostic")))) (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-beta.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (optional #t) (default-features #t) (kind 0)))) (hash "1imirq4crsh6b1z9f88bfjilqkxwl9v03mmy95avwz5zml14zjmg") (features (quote (("builtin_diagnostic")))) (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-beta.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0iiij07vz26c8p4s1vg8pq8q01vnbc8ynwg7wz0kv31sy1sqzrkn") (features (quote (("builtin_diagnostic")))) (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-beta.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0g1cvpx4l58bvf6gbz6vc61sk53a1mm8ji7jxv3kik2pkdd7649m") (features (quote (("builtin_diagnostic")))) (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc-parser-0.0.1 (crate (name "kproc-parser") (vers "0.0.1-beta.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1xzk61a064c74sf0m0nz9f45rx7pi4w972ipd6r5pwrs3vccb8x4") (features (quote (("builtin_diagnostic")))) (v 2) (features2 (quote (("proc_macro_wrapper" "dep:proc-macro2"))))))

(define-public crate-kproc_macros-1 (crate (name "kproc_macros") (vers "1.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "036kqkxadkxmda67k1qykp1ja5s00br1pk2bw4hj4vbc5cha0h90")))

(define-public crate-kproc_macros-3 (crate (name "kproc_macros") (vers "3.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0xxbrrp14mi32jbqzsw59sk6ks7hxlv9f3m33fb4jvlv49nn8nh5")))

(define-public crate-kproc_macros-3 (crate (name "kproc_macros") (vers "3.0.1") (deps (list (crate-dep (name "kmacros_shim") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0igpp0cs12s5h7b1qrj703zalx1n3a3290aqyjnll13599191lyg")))

(define-public crate-kproc_macros-3 (crate (name "kproc_macros") (vers "3.0.2") (deps (list (crate-dep (name "kmacros_shim") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1b92gp5yhj97d4lm40gf44vwj14l2lr2gla86s42k5lvn782p0al")))

(define-public crate-kproc_macros-4 (crate (name "kproc_macros") (vers "4.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0li51plc7qssnpq104shv3zmf0lpj0b466jvqxx7agil1xsm2ixb")))

(define-public crate-kproc_macros-5 (crate (name "kproc_macros") (vers "5.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0z5s9ldqfl66g8a3z55m5kc6y95slra0i9f890ph8xmyghhpqi3g")))

(define-public crate-kproc_macros-5 (crate (name "kproc_macros") (vers "5.1.0") (deps (list (crate-dep (name "kmacros_shim") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1xqgsym88flvisqf878cp7vs0kjiy3gvwz185119nv47km50bmyv")))

(define-public crate-kproc_macros-6 (crate (name "kproc_macros") (vers "6.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "09pqh2d21da4pjvbi67v5r6f2kwv0kf9dck6xijz10lgd0m5ygh8")))

(define-public crate-kprofiling-0.1 (crate (name "kprofiling") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)))) (hash "1j01xvnj621n0sb7azhysslwng5qw3ikjq62mw88xz8nazbx0g24")))

(define-public crate-kprofiling-0.1 (crate (name "kprofiling") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)))) (hash "17vr778081pbc6hh5dkr8lkv9zp09gk20zw3i2y8jq0fqh5dd33y")))

