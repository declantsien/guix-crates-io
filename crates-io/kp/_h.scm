(define-module (crates-io kp _h) #:use-module (crates-io))

(define-public crate-kp_heuristics-0.1 (crate (name "kp_heuristics") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1x87923c94pqpc8w1xwzhkydcqgbbayhmk07kpk56x2nr8cb58ad")))

(define-public crate-kp_heuristics-0.1 (crate (name "kp_heuristics") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "09zn123pp3797kihgnwi3bpxb7s9ysmf9jz35fi53a2nyk4wr1aw")))

(define-public crate-kp_heuristics-0.1 (crate (name "kp_heuristics") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0qrlnzv73pn5gzy2x6mj6zm3182d76jx1h1ydh2ba1rpkzxixijs")))

