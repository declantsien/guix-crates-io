(define-module (crates-io kp la) #:use-module (crates-io))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.0.0") (hash "0qyqivyja332ynv4sag9l98pwdkfhnkg1psyd55cfpxvpxz6qhkn")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.1.1") (hash "1912mbsp10662cillhzadyykrl5w5051108lb0niwp22g6wzjaz8")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.2.1") (hash "1i8zdjwxilxd3msa9ixah6l4iv9gmlai5z130prls4qw3jhcsv31")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.2.2") (deps (list (crate-dep (name "protobuf") (req "^2.25.2") (default-features #t) (kind 0)))) (hash "1571c6s53y26vxzpl6i1mga4i4b468p570xc7m5lsv5a4fv80kyj")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.4.0") (deps (list (crate-dep (name "protobuf") (req "^2.25.2") (default-features #t) (kind 0)))) (hash "0zanl59kzh3dxls8gdg1b081841lap7imnyvsxvdzrxpj7dsh1yp")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.4.1") (deps (list (crate-dep (name "protobuf") (req "^2.25.2") (default-features #t) (kind 0)))) (hash "124q401213rgz3habqfdpihgc29y0050hcl4b4mhq5kl9s6rjm8f")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.1") (deps (list (crate-dep (name "protobuf") (req "^2.25.2") (default-features #t) (kind 0)))) (hash "0bmw85kfkr29b91q7x7si2fk0dk5sqix5hbamw21la1hh6rdwl6c") (yanked #t)))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.0") (deps (list (crate-dep (name "protobuf") (req "^2.27.1") (default-features #t) (kind 0)))) (hash "0da1mqcb2pqn917hl1l72faqza3665yvzvydy0g2cvdhlljdrxdh") (yanked #t)))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.0-rc1") (deps (list (crate-dep (name "protobuf") (req "^2.27.1") (default-features #t) (kind 0)))) (hash "0868yxsp1qiy913x55mvf1pnc82cvdw5zbr2pmgk3ng0bi1hp6r0")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.6") (deps (list (crate-dep (name "protobuf") (req "^2.27.1") (default-features #t) (kind 0)))) (hash "1lc5abqmcgzjqrfr0kkxsx0k4s4zlln0ggxhpx95vfrpgrircxrd")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.6-rc1") (deps (list (crate-dep (name "protobuf") (req "^2.27.1") (default-features #t) (kind 0)))) (hash "0zr5qnwk96zhpp5sxsv40aifwdawimq4lay6139aqi6cx7v52x43")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.6-rc2-10501") (deps (list (crate-dep (name "protobuf") (req "^2.27.1") (default-features #t) (kind 0)))) (hash "1pb0plq206b25lqf057xksby01jsiazpij4mmn4axrgdc2sb5krr")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.7") (deps (list (crate-dep (name "protobuf") (req "^2.27.1") (default-features #t) (kind 0)))) (hash "05vd0ngv067hrf5mwajyn970nm59y12gm7al5zpy4dk967j5wz3z")))

(define-public crate-kplayer-rust-wrap-1 (crate (name "kplayer-rust-wrap") (vers "1.5.8") (deps (list (crate-dep (name "protobuf") (req "^2.27.1") (default-features #t) (kind 0)))) (hash "001jmxrahv6afd7krb68xhb0zhx9dvhp9xrkzkhik4h12nzmpvzb")))

