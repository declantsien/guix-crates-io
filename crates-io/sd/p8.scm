(define-module (crates-io sd p8) #:use-module (crates-io))

(define-public crate-sdp8xx-0.1 (crate (name "sdp8xx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "sensirion-i2c") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1r5ssrls3xns0bvxf470k6i1r7b8ila9plvrw7ysp6y6fbgjizxb")))

(define-public crate-sdp8xx-0.2 (crate (name "sdp8xx") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "sensirion-i2c") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gxgix8rm4iwmga42wfiizyr9g7c25k0g3r38fc09sqalyxk3s8p")))

