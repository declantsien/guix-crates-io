(define-module (crates-io sd wd) #:use-module (crates-io))

(define-public crate-sdwd-0.1 (crate (name "sdwd") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "systemd") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hdqlxa9l38cby8y7cwgcmmdmcan3ykl8q5ngdhl395myps0m9v3")))

(define-public crate-sdwd-1 (crate (name "sdwd") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "systemd") (req "^0.4") (default-features #t) (kind 0)))) (hash "19l55qyfiwjn3nhds1ddgp6raandlpi6x59vhl0kfqs69lyxpyck")))

