(define-module (crates-io sd la) #:use-module (crates-io))

(define-public crate-sdlang-0.0.1 (crate (name "sdlang") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "~0.10.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4.6") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "~0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "~2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "~2.1.0") (default-features #t) (kind 0)))) (hash "1hf3ira6skxrqiffqnc8yrxvc0p2051f2lv3x3n0hsmjkgj3z2wd")))

