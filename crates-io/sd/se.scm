(define-module (crates-io sd se) #:use-module (crates-io))

(define-public crate-sdset-0.1 (crate (name "sdset") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0k58fzdfdfalgnz6sfa9bkgjbcyx18v264ds68s3b38b1xli0s8l")))

(define-public crate-sdset-0.1 (crate (name "sdset") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1diq7yk71pyam6gmqylrp7icjjdai9x2a4dj8v49jbwwcravgd30")))

(define-public crate-sdset-0.1 (crate (name "sdset") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "10dlngz336y3vdbmj4papax0p2sg5ykxahq2vsm0ybgris8sgxhj") (features (quote (("unstable"))))))

(define-public crate-sdset-0.2 (crate (name "sdset") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1ha8nqhpv6v20fc4m52zgaqm32bwprwm4300mjasw21y00xljall") (features (quote (("unstable"))))))

(define-public crate-sdset-0.2 (crate (name "sdset") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0d70yraayx9wm5mn57ici0i1dg3f87x3z5c8m45x37z9z4798zwm") (features (quote (("unstable"))))))

(define-public crate-sdset-0.2 (crate (name "sdset") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0pbvv7nv28rw8fhzfghhmcfj67x4cmpz05126fl3ys5q6lq55dlx") (features (quote (("unstable"))))))

(define-public crate-sdset-0.2 (crate (name "sdset") (vers "0.2.3") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0hrq4qpijc8dd5yirs3q16y6r7azdm6kmgimgdvlv71sm8kdf6nm") (features (quote (("unstable"))))))

(define-public crate-sdset-0.2 (crate (name "sdset") (vers "0.2.4") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1s7d923qs1p7695ralq9jirpmias1z5nc2w5ksvjprz3slvg435b") (features (quote (("unstable"))))))

(define-public crate-sdset-0.2 (crate (name "sdset") (vers "0.2.5") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0kcvladf5kll7qg8g27jc0s10ihr8wpyi9akdi4srpapb7z5v66v") (features (quote (("unstable"))))))

(define-public crate-sdset-0.2 (crate (name "sdset") (vers "0.2.6") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "05gk4cz18fvpnnd67m9yibkaw04mih1fcrnph6p192hvf4xq5259") (features (quote (("unstable"))))))

(define-public crate-sdset-0.3 (crate (name "sdset") (vers "0.3.0") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1if2gkq3l73lqc26s7k6aimjs780q2iw66d6h3rbyz5185rrm5f6") (features (quote (("unstable"))))))

(define-public crate-sdset-0.3 (crate (name "sdset") (vers "0.3.1") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0fxc7ifvl8gg0974bzc9zc9aj4hqqgq3vxvwlwmgiyrck3j90s47") (features (quote (("unstable"))))))

(define-public crate-sdset-0.3 (crate (name "sdset") (vers "0.3.2") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zzbygzwhz100c7icqjmz857hl7qhmfib9rnfdvgfqr46xfbvlxy") (features (quote (("unstable"))))))

(define-public crate-sdset-0.3 (crate (name "sdset") (vers "0.3.3") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1581m43bj1fkyfrdjicp5ng9i4pwy70dk25gcxnpphyn8dvl9lmn") (features (quote (("unstable"))))))

(define-public crate-sdset-0.3 (crate (name "sdset") (vers "0.3.4") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pyhsvqcb36g9m4sdm4hnrmwgyq6l5dj4kjvn1jwhwf6nn46mk38") (features (quote (("unstable"))))))

(define-public crate-sdset-0.3 (crate (name "sdset") (vers "0.3.5") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0hnjxc5wmy62wf329cg7rc68glxmkcmblxkqs2aw7cpndpnv8rin") (features (quote (("unstable"))))))

(define-public crate-sdset-0.3 (crate (name "sdset") (vers "0.3.6") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jb39hyiwxlwkbb5xmb059prn36nhypvn3xlcg2r7rna5fmpmzav") (features (quote (("unstable"))))))

(define-public crate-sdset-0.4 (crate (name "sdset") (vers "0.4.0") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0fnrfw18dhlrl7k6ng6h8vri8yq28fwwg6y8fqhpjmw5b3h1zcnb") (features (quote (("unstable"))))))

