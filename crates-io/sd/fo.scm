(define-module (crates-io sd fo) #:use-module (crates-io))

(define-public crate-sdformat-rs-0.1 (crate (name "sdformat-rs") (vers "0.1.0") (deps (list (crate-dep (name "RustyXML") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0dwxppx4rh6hb5qg3z7naay9a56nm8i2lzkd7idcc5pw46k33gn8")))

