(define-module (crates-io sd -s) #:use-module (crates-io))

(define-public crate-sd-sys-0.1 (crate (name "sd-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0ipmhldbg59yy61ik6nwgjzicm221b706p8vrzfirivra477haxh") (links "systemd")))

(define-public crate-sd-sys-0.1 (crate (name "sd-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0vaklaf56fzc66m5ya7fkq7pz1dlsy1marg1hi001xfbc81md3d2") (links "systemd")))

(define-public crate-sd-sys-1 (crate (name "sd-sys") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1k293nd3pczh5l5y8anclrdi07m9axs2hla485bajx0gvvr71y5w") (links "systemd")))

