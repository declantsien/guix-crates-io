(define-module (crates-io sd ns) #:use-module (crates-io))

(define-public crate-sdns-0.1 (crate (name "sdns") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (features (quote ("rustls-tls" "trust-dns"))) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sh96f8qabkf22sz14g7hqm4773svw6iwp1mivbcsq8fik0d6fr3") (yanked #t)))

