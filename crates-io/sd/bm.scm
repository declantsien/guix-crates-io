(define-module (crates-io sd bm) #:use-module (crates-io))

(define-public crate-sdbm-0.1 (crate (name "sdbm") (vers "0.1.0") (hash "1awiv3ckd2ymcqnspafffslq2mccrkksv2igri9zizdaskjmahvs")))

(define-public crate-sdbm-0.1 (crate (name "sdbm") (vers "0.1.1") (hash "1l9vm08rxskwyppv801x0c5bn9lm4qmrsq9nd5irh0yhsg54ydp6")))

(define-public crate-sdbm-0.1 (crate (name "sdbm") (vers "0.1.2") (hash "1w3749vnpxv8vl4qzgksp98s7v0jln9kchrm2g5ha5vhjjdhp2zz")))

(define-public crate-sdbm-0.1 (crate (name "sdbm") (vers "0.1.3") (hash "18a00jb3smhmbzcmmymhijr8fqjsvsr684f0lhyzmdbxm334shgq")))

