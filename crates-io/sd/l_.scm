(define-module (crates-io sd l_) #:use-module (crates-io))

(define-public crate-sdl_64-0.1 (crate (name "sdl_64") (vers "0.1.0") (deps (list (crate-dep (name "gfx_64") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log_64") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.60") (kind 1)))) (hash "0cvk8l9zfzc41nk0fylypxa557l4sqsyhli3gpq4c4r8wkzcxj56")))

(define-public crate-sdl_image-0.3 (crate (name "sdl_image") (vers "0.3.6") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sdl") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1lh1qiv7k76qha4f6l1yl5waqy2m2rmm109jkgdkniv1jhvdv246")))

(define-public crate-sdl_image-0.3 (crate (name "sdl_image") (vers "0.3.7") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sdl1_2-rs") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0s3add4lj8l3d2kl55zbf5javzff9cdqqgshg7z7xslspicg9c23")))

(define-public crate-sdl_image-0.3 (crate (name "sdl_image") (vers "0.3.8") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sdl1_2-rs") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1p3da0hsqyhapfb4j2blvay9a14fxpqgg7khcqgf2m82z06kz8h9")))

(define-public crate-sdl_rust_demo-0.1 (crate (name "sdl_rust_demo") (vers "0.1.0") (deps (list (crate-dep (name "sdl2") (req "^0.34.3") (default-features #t) (kind 0)))) (hash "0zjvjcr669ija3j0wcpzfgjc474q91ws6p8svnjbfqal02bk5x68")))

