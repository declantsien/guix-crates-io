(define-module (crates-io sd sb) #:use-module (crates-io))

(define-public crate-sdsb-0.0.1 (crate (name "sdsb") (vers "0.0.1") (deps (list (crate-dep (name "chain-trans") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">1.0.184") (features (quote ("derive" "std" "alloc"))) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1zwfjavb10pk6yavfka445vhcyia0rg7b6xdabhzw3gdn5jz4iw8")))

