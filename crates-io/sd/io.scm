(define-module (crates-io sd io) #:use-module (crates-io))

(define-public crate-sdio-host-0.1 (crate (name "sdio-host") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "0pgwdfiyzl2sdkkcscfvvrq98s6wbb1gcqg38nlwgmsppv484i40")))

(define-public crate-sdio-host-0.2 (crate (name "sdio-host") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "08295ygmkl62vxh0ggwhnw8fivm6vgq6g3nj2m4677k1ikicj824")))

(define-public crate-sdio-host-0.3 (crate (name "sdio-host") (vers "0.3.0") (hash "038nkynayp1cqyqhxgw4a1z6lb4y984h7kabhl4ijvqmp6m0arr0")))

(define-public crate-sdio-host-0.4 (crate (name "sdio-host") (vers "0.4.0") (hash "1y6s0npmib4ap2h6wvl7f0wg0sb7syyjyank0739w13461y5igik")))

(define-public crate-sdio-host-0.5 (crate (name "sdio-host") (vers "0.5.0") (hash "0npq6xqx8nnr485kgl2s7gd27a2l3b8lgklf6a63hk7ykigh4g7r")))

(define-public crate-sdio-host-0.6 (crate (name "sdio-host") (vers "0.6.0") (hash "02cwxpp41ha6z3ggyc1jkcs1m4syfp83fjw256m381md00dl2p1m")))

(define-public crate-sdio-host-0.7 (crate (name "sdio-host") (vers "0.7.0") (hash "05223kzavvk3rigbd07mbmpd13dqd83cbi4y9f068ay288gq42sv")))

(define-public crate-sdio-host-0.8 (crate (name "sdio-host") (vers "0.8.0") (hash "1j44z53bllq7a570klph44lgmkaajl6rx5v99b8r8j4xh9620rpv")))

(define-public crate-sdio-host-0.9 (crate (name "sdio-host") (vers "0.9.0") (hash "1qkf5ln8vrn5y1a57ysc0k6msici6sb3lp7mnxawvv0fjp5y4a5k")))

(define-public crate-sdio_sdhc-0.1 (crate (name "sdio_sdhc") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "stm32f4xx-hal") (req "^0.8") (features (quote ("stm32f407"))) (default-features #t) (kind 0)))) (hash "1w188dz421vgvjhlgvdjc7qmz1zpz1wiv97rdca3n8p8iirqnmjr") (yanked #t)))

(define-public crate-sdio_sdhc-0.1 (crate (name "sdio_sdhc") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "stm32f4xx-hal") (req "^0.8") (features (quote ("stm32f407"))) (default-features #t) (kind 0)))) (hash "1fiv27wn1x8s12nvmx4cnnzgp69j2vha59da819ifhmn0hbcyk16") (yanked #t)))

(define-public crate-sdio_sdhc-0.1 (crate (name "sdio_sdhc") (vers "0.1.2") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "stm32f4xx-hal") (req "^0.8") (features (quote ("stm32f407"))) (default-features #t) (kind 0)))) (hash "170c05n93h6l1jpwqpvqhj7rq9xs8iby3xb2w9dypn7rmfwag83y")))

(define-public crate-sdio_sdhc-0.2 (crate (name "sdio_sdhc") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "fat32") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stm32f4xx-hal") (req "^0.8") (features (quote ("stm32f407"))) (default-features #t) (kind 0)))) (hash "0cvkr5nlhz88x5j60hhj47g87bakca959md87a4w1dlc2np8hkpd") (features (quote (("filesystem" "fat32") ("default"))))))

(define-public crate-sdio_sdhc-0.2 (crate (name "sdio_sdhc") (vers "0.2.1") (deps (list (crate-dep (name "block_device") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "stm32f4xx-hal") (req "^0.8") (features (quote ("stm32f407"))) (default-features #t) (kind 0)))) (hash "06ih3zljxfwnpyjwpnk04j67aj7kzb8lp22b6mpdr86b1z12p2cp") (features (quote (("filesystem" "block_device") ("default"))))))

(define-public crate-sdio_sdhc-0.2 (crate (name "sdio_sdhc") (vers "0.2.2") (deps (list (crate-dep (name "block_device") (req "=0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "stm32f4xx-hal") (req "^0.8") (features (quote ("stm32f407"))) (default-features #t) (kind 0)))) (hash "152md40wz0rg3zfjj9im6payh4hsjiwajy4nc8ryx0q82gjdk2hz") (features (quote (("filesystem" "block_device") ("default"))))))

(define-public crate-sdio_sdhc-0.2 (crate (name "sdio_sdhc") (vers "0.2.3") (deps (list (crate-dep (name "block_device") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "stm32f4xx-hal") (req "^0.8") (features (quote ("stm32f407"))) (default-features #t) (kind 0)))) (hash "0j4gxsy2b8m57ixhnrqz8bi9jdcaj3i616kiv12j8z587y47mi18") (features (quote (("filesystem" "block_device") ("default"))))))

