(define-module (crates-io sd fg) #:use-module (crates-io))

(define-public crate-sdfgen-0.5 (crate (name "sdfgen") (vers "0.5.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)))) (hash "037lf5yg9ldn3q6snaq6phd2pdn5x0dl3bbqjhr3lmblz8vh56mg")))

(define-public crate-sdfgen-0.6 (crate (name "sdfgen") (vers "0.6.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)))) (hash "09z452i8x4fngc800chihpzi853i96rl5kcfp617lmhgb02825ls")))

(define-public crate-sdfgen-0.6 (crate (name "sdfgen") (vers "0.6.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)))) (hash "0f8dz7wv34sj9smi71mj3x6b571wwhiv7gvv8kjm7y70z5f1h71m")))

(define-public crate-sdfgen-0.6 (crate (name "sdfgen") (vers "0.6.2") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)))) (hash "0dcik68av7429hlvg89s8msclp577p33n51wxh3d660hbk6rp8nc")))

(define-public crate-sdfgen-0.6 (crate (name "sdfgen") (vers "0.6.3") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)))) (hash "1fk47drk2910m53a0c37lgwfipk1gm43qbviq17qqf6i8ywzw8a9")))

