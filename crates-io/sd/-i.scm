(define-module (crates-io sd #{-i}#) #:use-module (crates-io))

(define-public crate-sd-id128-0.1 (crate (name "sd-id128") (vers "0.1.0") (deps (list (crate-dep (name "sd-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j2ddkcqma7bpzk2k7yzkbhmn1zymfql5dpqf75fy5dpcfnqncs4")))

(define-public crate-sd-id128-0.1 (crate (name "sd-id128") (vers "0.1.1") (deps (list (crate-dep (name "sd-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hrdm0520l9rcdj9d1raw3wh3rddhvk8j8yjnhl0w9ig00ajx1z0")))

(define-public crate-sd-id128-0.1 (crate (name "sd-id128") (vers "0.1.2") (deps (list (crate-dep (name "sd-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a7lx9g8xfgnlwf9lc1cvy4is96ymslmqlmdfcq40hy870a62clq")))

(define-public crate-sd-id128-1 (crate (name "sd-id128") (vers "1.0.0") (deps (list (crate-dep (name "sd-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "08d19g81r9gwwca65bs6v8mrb49plb73kksdfjjpskhw5ysh1nk7") (features (quote (("default" "240") ("240") ("233"))))))

(define-public crate-sd-id128-1 (crate (name "sd-id128") (vers "1.0.1") (deps (list (crate-dep (name "sd-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gk5i8jgm46rqzrs2rbhyg3jaz3k25gi4hb13c095jnhmia63h06") (features (quote (("default" "240") ("240") ("233"))))))

