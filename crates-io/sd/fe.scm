(define-module (crates-io sd fe) #:use-module (crates-io))

(define-public crate-sdfer-0.1 (crate (name "sdfer") (vers "0.1.0") (hash "0jzbildx1p2knwj6jh40c9v3ssrgddq54kgk2nhabyxnmpmx17c5")))

(define-public crate-sdfer-0.2 (crate (name "sdfer") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (optional #t) (default-features #t) (kind 0)))) (hash "1nfk44za4qmqnqnbp8kyzvna98p73764rsmzbg55qshy3bkkrkws")))

(define-public crate-sdfer-0.2 (crate (name "sdfer") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24.7") (optional #t) (default-features #t) (kind 0)))) (hash "1va0q962zpplmw38x9mfz9fvx2qhxcmxsz1c406sf8f7qzmpbz97")))

