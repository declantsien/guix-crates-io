(define-module (crates-io sd c-) #:use-module (crates-io))

(define-public crate-sdc-parser-0.1 (crate (name "sdc-parser") (vers "0.1.0") (deps (list (crate-dep (name "combine") (req "^3.8.1") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.2.7") (default-features #t) (kind 2)))) (hash "0nwvh9110c34yypw9xa9rb4a3rnfr7px2q87hzznnwdhb6522c9b")))

(define-public crate-sdc-parser-0.1 (crate (name "sdc-parser") (vers "0.1.1") (deps (list (crate-dep (name "combine") (req "^3.8.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.2.7") (default-features #t) (kind 2)))) (hash "1210gaancfwkcmkmgd4kizs33yqcafyzaclmdasxrflvyggvjbnv")))

