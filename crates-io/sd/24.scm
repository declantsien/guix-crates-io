(define-module (crates-io sd #{24}#) #:use-module (crates-io))

(define-public crate-sd2405al-0.1 (crate (name "sd2405al") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "rtcc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dd56cm7zhbs6xsf8lf6sixmkgashl4dlvqf1pdckb4nlwhxsvya")))

