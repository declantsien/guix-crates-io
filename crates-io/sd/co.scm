(define-module (crates-io sd co) #:use-module (crates-io))

(define-public crate-sdcons-0.1 (crate (name "sdcons") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (features (quote ("std" "std_rng"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wq10qg4d5918caarw1055q5kch2sp54xfkhi0zd8y1drwph64w4")))

