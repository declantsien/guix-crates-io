(define-module (crates-io sd fu) #:use-module (crates-io))

(define-public crate-sdfu-0.0.1 (crate (name "sdfu") (vers "0.0.1") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "17cissj3rbzv02a53sygs9c3wwmxba7rc4nr7sm12ff2kp2ci42l")))

(define-public crate-sdfu-0.1 (crate (name "sdfu") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "1m0bhd6i06799054df38zq7zg8zsq4n1ci4kcvfcryf12vddizy7")))

(define-public crate-sdfu-0.1 (crate (name "sdfu") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "0zwd1d65klwx2x23ckyxapvp4w13n6cxsdr7yrisvnvkj7jw2fri")))

(define-public crate-sdfu-0.1 (crate (name "sdfu") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "14a3rwr2iw6gx14a8gax2vf5chkkcimh696xk7mxqkxa8195jl9z")))

(define-public crate-sdfu-0.2 (crate (name "sdfu") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ultraviolet") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "1cvzbln6krzh033wp9ispgqklrnz9rvyhi74ghlwk9jvxhpnns7c")))

(define-public crate-sdfu-0.3 (crate (name "sdfu") (vers "0.3.0") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ultraviolet") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "0p12xdxjdhlfxs988218aiqj2322viw40akf7xa01xx1w2k3pmm9")))

(define-public crate-sdfu-0.3 (crate (name "sdfu") (vers "0.3.1-alpha.1") (deps (list (crate-dep (name "glam") (req ">=0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.27") (default-features #t) (kind 2)) (crate-dep (name "ultraviolet") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ultraviolet") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "vek") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.15") (default-features #t) (kind 2)))) (hash "1265d20wxmyaq0ph6267x0va54v9p29g78qhh11p2bkg6az5y9g2")))

(define-public crate-sdfui-0.1 (crate (name "sdfui") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0b70ix1x8i9sl6vk9dic34cfzw8ziy0jf74hvflpbdb6f02k8x4g")))

(define-public crate-sdfui-0.1 (crate (name "sdfui") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "147w2yf3m7aipggvszyr74m7kbddis75dvp7a41lbg9afk65h990")))

