(define-module (crates-io sd l1) #:use-module (crates-io))

(define-public crate-sdl1_2-rs-0.3 (crate (name "sdl1_2-rs") (vers "0.3.7") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "064zmwsy9qwp6fs8br6d80k1qqyxcgzw1m0as9i1am6ksvfjc9zv")))

