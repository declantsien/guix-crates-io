(define-module (crates-io sd -r) #:use-module (crates-io))

(define-public crate-sd-rs-0.1 (crate (name "sd-rs") (vers "0.1.0") (deps (list (crate-dep (name "sma-rs") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "0gxpfbv2cc2lhj12k7alwnci7xm7f4qjy7pfqwcmdxrgyps4p856")))

