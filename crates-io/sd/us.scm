(define-module (crates-io sd us) #:use-module (crates-io))

(define-public crate-sdust-0.1 (crate (name "sdust") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "needletail") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "needletail") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "12pbaml8w9wi9vk2il5xwxn3jd52pqw9lrsy6f5lqkwa1cvnad52") (features (quote (("cli" "clap" "needletail"))))))

