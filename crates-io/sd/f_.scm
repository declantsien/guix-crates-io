(define-module (crates-io sd f_) #:use-module (crates-io))

(define-public crate-sdf_glyph_renderer-0.1 (crate (name "sdf_glyph_renderer") (vers "0.1.0") (deps (list (crate-dep (name "derive-error") (req "~0.0") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "~0.26") (optional #t) (default-features #t) (kind 0)))) (hash "1rfn21hg8qsqgcjf72p0sln4fd0k5ivsksafp445788c6kz1mr0w") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.1 (crate (name "sdf_glyph_renderer") (vers "0.1.1") (deps (list (crate-dep (name "derive-error") (req "~0.0") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "~0.26") (optional #t) (default-features #t) (kind 0)))) (hash "0yf9qgfgh89wav0im6nsjkqmnhhzj6f0yv86d78rx47wxg6xab3a") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.1 (crate (name "sdf_glyph_renderer") (vers "0.1.2") (deps (list (crate-dep (name "derive-error") (req "~0.0") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "~0.26") (optional #t) (default-features #t) (kind 0)))) (hash "1ii81fw6mqsqa4m2gl9ybwib0sxlifv0ysx4ivy95l3grrpaam7g") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.1 (crate (name "sdf_glyph_renderer") (vers "0.1.3") (deps (list (crate-dep (name "derive-error") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)))) (hash "1md34j1kvd4yxhij6qahsc3hsmz4pysf64dxns4pbaxbaaxp52xb") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.2 (crate (name "sdf_glyph_renderer") (vers "0.2.0") (deps (list (crate-dep (name "derive-error") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "^0.28.0") (optional #t) (default-features #t) (kind 0)))) (hash "1rxhk1dcbx4f058j3gqlgq1229wsj8v2lf1f7664s512aj064fc4") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.3 (crate (name "sdf_glyph_renderer") (vers "0.3.0") (deps (list (crate-dep (name "derive-error") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "^0.30.1") (optional #t) (default-features #t) (kind 0)))) (hash "1v0b3pgn00rpic2rnzrlx1k829q9vs40br9mf7za8y9rxbij1pa9") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.4 (crate (name "sdf_glyph_renderer") (vers "0.4.0") (deps (list (crate-dep (name "derive-error") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "^0.31.0") (optional #t) (default-features #t) (kind 0)))) (hash "050zjvhjbabci37gk5gxn768w18jwnj10jr5kzcgxa610bqswidm") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.5 (crate (name "sdf_glyph_renderer") (vers "0.5.0") (deps (list (crate-dep (name "derive-error") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "^0.32.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qq9znqn4y9vgqagq1cbmd30rc22a3qh5wagi1v622gqgn3g6j0q") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-0.5 (crate (name "sdf_glyph_renderer") (vers "0.5.1") (deps (list (crate-dep (name "derive-error") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "freetype-rs") (req "^0.32.0") (optional #t) (default-features #t) (kind 0)))) (hash "1l7v7hz8z3w30zd2iw2lv9hwh7skp1h2hagl1iv0b6g0nkzvrlca") (features (quote (("freetype" "freetype-rs"))))))

(define-public crate-sdf_glyph_renderer-1 (crate (name "sdf_glyph_renderer") (vers "1.0.0") (deps (list (crate-dep (name "freetype-rs") (req "^0.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1fdkwg2ypi9pzrvc50fmi5hvq8sqrib5ic03kr80pql1s4ac21cb") (v 2) (features2 (quote (("freetype" "dep:freetype-rs")))) (rust-version "1.65")))

(define-public crate-sdf_glyph_renderer-1 (crate (name "sdf_glyph_renderer") (vers "1.0.1") (deps (list (crate-dep (name "freetype-rs") (req "^0.35.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "0kyr0gv3nghpsw6v7hvfpfsn0sf8nlnb2fpvx62ckw7zlkv21wjy") (v 2) (features2 (quote (("freetype" "dep:freetype-rs")))) (rust-version "1.74.0")))

