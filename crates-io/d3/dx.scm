(define-module (crates-io d3 dx) #:use-module (crates-io))

(define-public crate-d3dx12-0.0.0 (crate (name "d3dx12") (vers "0.0.0") (hash "1h071hh6dglls1a5y6nvdvb313ihd3viyqrdaq1bh8n1p3baqyvz")))

(define-public crate-d3dx9-sys-0.1 (crate (name "d3dx9-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("d3d9" "d3d9types" "d3d9caps" "minwindef" "unknwnbase" "winnt" "winbase"))) (default-features #t) (kind 0)))) (hash "1rgbaiqpa2n71fy7pp64fvnnp6w6kyqaca5vkrma07xz2lwvhfy5") (yanked #t)))

