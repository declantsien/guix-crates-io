(define-module (crates-io d3 dc) #:use-module (crates-io))

(define-public crate-d3dcompiler-sys-0.0.1 (crate (name "d3dcompiler-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1778sc48yamqp5zckadlr9w1vkm869gz4bgkqsryn2vzx8y6w2s0")))

(define-public crate-d3dcompiler-sys-0.2 (crate (name "d3dcompiler-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "191crqady50f2yqa3af1pg2r796yrmm485m2y01mvd6i6224f280")))

