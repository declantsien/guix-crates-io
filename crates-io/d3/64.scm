(define-module (crates-io d3 #{64}#) #:use-module (crates-io))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-0.1 (crate (name "d364cb02-af78-4e8d-9444-89788203d49b") (vers "0.1.0") (deps (list (crate-dep (name "d364cb02-af78-4e8d-9444-89788203d49b-internal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qp6kj56kgb5kcr02vipkcnh09rhmkzfnqgx5bqrglb4s9b83yn7") (yanked #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-0.2 (crate (name "d364cb02-af78-4e8d-9444-89788203d49b") (vers "0.2.0") (deps (list (crate-dep (name "d364cb02-af78-4e8d-9444-89788203d49b-internal") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "08zs22xvnzzwazzvmvbs9xpikacpacarknbpi3zq1z9haq11b7qv") (yanked #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-0.2 (crate (name "d364cb02-af78-4e8d-9444-89788203d49b") (vers "0.2.1") (deps (list (crate-dep (name "d364cb02-af78-4e8d-9444-89788203d49b-internal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "18psggqmwdw4gmvibvrl6w81cfsvpd4abqff9wx36rxk8vqlf8z3") (yanked #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-internal-0.1 (crate (name "d364cb02-af78-4e8d-9444-89788203d49b-internal") (vers "0.1.0") (hash "098c9vzjwpqkgi45mahx46w7yx796adc85skfy0gzqgsyhzn391d") (yanked #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-internal-0.2 (crate (name "d364cb02-af78-4e8d-9444-89788203d49b-internal") (vers "0.2.0") (hash "04zxbwswkqnl2qwc9x0s01c7mbg5m2c3iyxpr425ai62628wigmn") (yanked #t)))

(define-public crate-d364cb02-af78-4e8d-9444-89788203d49b-internal-0.2 (crate (name "d364cb02-af78-4e8d-9444-89788203d49b-internal") (vers "0.2.1") (hash "1k3jiqcy6iiqfxfgr2k111id6sss2cfydjbdpngz9yx0rrcjvnpj") (yanked #t)))

