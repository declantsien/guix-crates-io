(define-module (crates-io d3 ne) #:use-module (crates-io))

(define-public crate-d3ne-0.1 (crate (name "d3ne") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "02q6rxjdxrrkmjykgv7z8pw3gg36s7xxdww56fgkn7n2mg91m46k") (yanked #t)))

(define-public crate-d3ne-0.1 (crate (name "d3ne") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "083x49fw3yzi35mp1aqk9wbsfwrayvgqqra973ds2i0jawnwsrvw")))

(define-public crate-d3ne-0.1 (crate (name "d3ne") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0r2s8pj01p9ma04163ikyf1b6vd2ajp1xr6bmgnb5gazl4mjf6xi")))

(define-public crate-d3ne-0.1 (crate (name "d3ne") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "16j4810m38yw8im2yy6g5kw5sya9ym77affspih3gn62ndgp2zc1")))

(define-public crate-d3ne-0.1 (crate (name "d3ne") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "1wyw2fcbqxwwvfwdw14pa973242aakiq224yvg7hnyrpk6147rdv")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "1b4i8id9f46b49vwf52zc6mbxdl4znxjacy3dv917mninmkb897q")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0vwsgn965cq8lxg6082nrgdb7xhyrwj12lf5nc37m0lv8s5sah3g")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "14m95ipw7kxq8358gxhg16wjw50d6qs8nlzp0j3l8q6wl6rr1442")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "1ya6lcla46bfflc7fpxxxwa8mri1z6l43bj6ic5rxj76srn6af55")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "1h20w27bhfk77fw3c1g43jkh16g0ywndiqd21w42ih2xl54babvi")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.5") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0pv5fnc3h3h58zb4i20qd8gbq33r45m0m002glyqzr8bb52q2q4p")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.6") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0339mcpqp97km21z6w202a4y14mzpj05kqpv57hvbk3mh9vqz2yf")))

(define-public crate-d3ne-0.2 (crate (name "d3ne") (vers "0.2.7") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0brka865fp9y5ma79dqi65xm5h2i2qk6qx9r6p8ad01lydzmxlx0")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0qvjhvai2lcyvinjly2gl78i039l33bjbk408k30bh0lmd3hi5iy")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0z9ym6yha3lmgc8kycvz83s44329mjd7yvw2h0286yd5rwj7bq72")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "16vx8ib7463c80r75gnz5x69dzcnilsv0hvxir3y9yg7qm0959fn")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.3") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "12jgi5ggyqz4d22l0pj6sm57jmkgavjbdxvky0jkb5wxxhnz2581")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.4") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "11qi6av4fk3f47zl0l2v82733w24c0dz4z7f9621420zrk31jy6s")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.5") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1yj9a5dq8m04s94bisf9py7qsw9fldfhmi37nvnhbpjp2dskx06p")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.6") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0vsig5xhagdzawr69qp08g0l18jps42jgxphymgfjkdpxh8p7ish")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.7") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0d1gdhp3pjfy0maw4l8hkpam29qp83pxlhqzr4qkj3jprjibxdb6")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.8") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0za5j2rpd04cw22hfj9dg9832qpz2sa64w4ryyhxi0kli3c49izl")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.9") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1plc282c7k5p9lz238q5432k1vdiirpgrhzvh8aa66533yj5fs4s")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.10") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "13y3fjzc64x4y9zh1lhnhvppg4pycdi9pvgm0zvc28cfnmx3ay8i")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.11") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1807afzkhg2qkzkr7a7w42k9r90d5yfyba5gbh3gmz7p7rv7pn7x")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.12") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "18vbn8c7rjq5f9q4nwigl2kh68nvvk99gbfpzyzl7bk5l19gif12")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.13") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1g51yy4q0cms7xpqy74bbprsvsmvnq14wz7x5hxvy0ijqqz0fmld")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.14") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1blal7zddxsy2ds3q40g3ywn35514qqljap9yn7iidc7m7wj97cq")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.15") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1s150pjnhr0f06cg0088b8pz9n86kfji6040wf0b1bv073zks8mg")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.16") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "079mfacrmf78qrkr9zs8z7p8kirl6k9clhq71pwpvd8kbck58r5n")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.17") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1yzryrvlxka5syiilp66jv27vhnp4l8dyizxi6650a5mqfqrmmay")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.18") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0ylz3glglqcas83ypi4slbzj5lkhp39vyxc7b3657b7r5893acij")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.19") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "06mzka5m72fnlwl2558a9vzr406jviny1f04rd4wnfxi6pyxzbmf")))

(define-public crate-d3ne-0.3 (crate (name "d3ne") (vers "0.3.20") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0c2szajkgnkr4pk6izdb9mc2cb0vps2wcs5qmmiaz8xb4msphx21")))

(define-public crate-d3ne-0.4 (crate (name "d3ne") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1m9mg9piqv1k8nq0ak5sh0fh2sa5kf335g25461f7g1vgvs27ssj")))

(define-public crate-d3ne-0.4 (crate (name "d3ne") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "07dhdgj8qz4hr1kg5qcb1kydywldcxrakr0qnfpap4q4rqny4q6x")))

(define-public crate-d3ne-0.4 (crate (name "d3ne") (vers "0.4.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1xx6k7ka328cqwrdv8m69rdm3f2gwvd1l4ffdk3vc47j9r1grykf")))

(define-public crate-d3ne-0.5 (crate (name "d3ne") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0njbfqbikx4kfi3sb5n0kfncmklwa82h8rxy9pyig57n0kwd1va4")))

(define-public crate-d3ne-0.5 (crate (name "d3ne") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1ik7g3pjwxqwx4vhxm05402c65c286r348dhkm6psnzrsjjc5439")))

(define-public crate-d3ne-0.5 (crate (name "d3ne") (vers "0.5.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1khxvnqq7g1pslx3g5wdk2261yaw2aaa7ixpx824pfr1m9ah6b00")))

(define-public crate-d3ne-0.5 (crate (name "d3ne") (vers "0.5.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1arbfw4vfgw13wvdkxrksqqm3v188a0g7p1ab8yqyrc6mdjvy59b")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "03h44wis5k3skrglygqq7ynz7rd1ad4ps4f6xykhvc31yxjhjz70")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "07gf160v93a49ggxkc9hd3aqz836k2dxsqlfmd27c3dy8i2xcn3b")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "07875am3x8v58hz88gljciwhqi2z30xz1azp4hf27zyaiq45s711")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0ldqf6a6aiqmcvglb9d52l6826bckrzb8xipi0dj8n5pc189lw4p")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0vrrywmkq2lxkx2xcxwsjg2fp9jcr7bhvihzq0r2ankdpy93rgz4")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "13llwba7a45kg2g2lwqz7a9hagrdxkzanwfkz7abdpvcy1x1dv17")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "10d9s2369ymv68sa4s1a3770alnm8s8fsrl1bbjr6iscswx1am51")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1m5bhq6b2r8sad06jaj6jsbblknq4ns94rxsx4095cl7w5avzr3h")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1cx09bkcr3y8s43qiypwbr56ny5xzcncwfx08xx5c0yyfigfyqkp")))

(define-public crate-d3ne-0.6 (crate (name "d3ne") (vers "0.6.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0vqzbghbxyi7maqhvm0kidjh8k57mpb3wrjxid4f7a7hnjz2m1ws")))

(define-public crate-d3ne-0.7 (crate (name "d3ne") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "063ia6704ll73l59wwgmkkd0kjs26j972nffbxwrcw0b5mfmav6m")))

