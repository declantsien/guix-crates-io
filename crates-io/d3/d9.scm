(define-module (crates-io d3 d9) #:use-module (crates-io))

(define-public crate-d3d9-sys-0.0.1 (crate (name "d3d9-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0mrlwa0kpd154f2vp0r13hb3mba53g09a30iwb6nlphblr46ic9c")))

(define-public crate-d3d9-sys-0.0.2 (crate (name "d3d9-sys") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "16mghc8bk2paj8b00fzaa34va1rq4ly5268b4sc51n27bhdpadna")))

(define-public crate-d3d9-sys-0.1 (crate (name "d3d9-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0jjangmfns9sa5qpls5dzj1phvwnhv3db80gkpxy51d6cdv2as05")))

(define-public crate-d3d9-sys-0.1 (crate (name "d3d9-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1pny9yfzclq4kq6hks3b63yn0lazbx6v3a3y9ddg7dml15l6xrkn")))

