(define-module (crates-io d3 d1) #:use-module (crates-io))

(define-public crate-d3d11-0.1 (crate (name "d3d11") (vers "0.1.0") (deps (list (crate-dep (name "dxgi-win") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "06xndvv4850whv5j9k204y9c2778fq5zkpkh3r3lb0s3in6156i6") (yanked #t)))

(define-public crate-d3d11-rs-0.0.1 (crate (name "d3d11-rs") (vers "0.0.1") (hash "14d219wgdnc80aj85kvff61cyzjngxb8m27kp6v5l70qrk58px01")))

(define-public crate-d3d11-sys-0.0.1 (crate (name "d3d11-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0k6w3cx0hq9xs3rgsb8ncipjxqd1n8zq30lip58yrnl8px897p46")))

(define-public crate-d3d11-sys-0.1 (crate (name "d3d11-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "0sq0gkfbhn8y90jk0jwvnjz7b94c9m6s97px1va3wk2cc33rzhk9")))

(define-public crate-d3d11-sys-0.2 (crate (name "d3d11-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "00la5b5d2nqbr4qzx5raah3wv3z6p3f6xg3ijdwy6wyqv4lzfvzn")))

(define-public crate-d3d11-win-0.1 (crate (name "d3d11-win") (vers "0.1.0") (deps (list (crate-dep (name "dxgi-win") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1wwvn3m65gpvnpyaz88h455rgv674921bx0cw4b0h5cybljz0n66") (yanked #t)))

(define-public crate-d3d11-win-0.1 (crate (name "d3d11-win") (vers "0.1.1") (deps (list (crate-dep (name "dxgi-win") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1wwpsa214wxjiw2zp0n8h04bsycbknsv92vhngyx0s0lvz74j591") (yanked #t)))

(define-public crate-d3d11-win-0.2 (crate (name "d3d11-win") (vers "0.2.0") (deps (list (crate-dep (name "dxgi-win") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "168z5skxqxnahc4d2h3nc3xcljr5hq4vc5dpipbdp5idqzhcmkbl") (yanked #t)))

(define-public crate-d3d11-win-0.2 (crate (name "d3d11-win") (vers "0.2.1") (deps (list (crate-dep (name "dxgi-win") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0mpq2y4c2aglv7zbbsnqzaxg9l6ryvb1ynbda9w8wr87x4s7rdxi") (yanked #t)))

(define-public crate-d3d12-0.1 (crate (name "d3d12") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "0854fblk0r7wxvy7ikmjcfaxmnfppx5lc24ismqb14swqm3mbnjg")))

(define-public crate-d3d12-0.2 (crate (name "d3d12") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "0wzfsaq2qxp45lmaijp9i2k6r284mdwnx26asgyd5z37armf5454")))

(define-public crate-d3d12-0.3 (crate (name "d3d12") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "0qmn4jys3kbq2305vpwsx95f9fdc7z6526fcdca5wplhi67d8zmw") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.3 (crate (name "d3d12") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "18knwgddl8rfdnrbas5fznn4daxiw9ddm1nmrsq06kz3xb24ncn1") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.3 (crate (name "d3d12") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "1i3fxkxhmlspcx71ag0sd14wy5vzvi7m5049bw1m6z1cnb70r9nh") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.4 (crate (name "d3d12") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "0a2455wih1685hhwf2x0dbri8q8bclihqi7w57qpyz74byrd27h9") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.4 (crate (name "d3d12") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "0339f9ij9hq4bxk31km97sy0jdc8if14pvkdssly15hyimwgvbid") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.5 (crate (name "d3d12") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgi1_5" "dxgi1_6" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "0n97ibcb9xwdxwhhxy908nsvrdrng1x9cgfdbq10w7ivyphi8yc2") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.6 (crate (name "d3d12") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgi1_5" "dxgi1_6" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "1nirigg48lvilgbwgbk89xrf2k1ak60wgqy0xslx8ywfb8pxxw6q") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.7 (crate (name "d3d12") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req ">=0.7, <0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgi1_5" "dxgi1_6" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (kind 0)))) (hash "084z4nz0ddmsjn6qbrgxygr55pvpi3yjrrkvmzyxs79b56ml8vp1") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.19 (crate (name "d3d12") (vers "0.19.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libloading") (req ">=0.7, <0.9") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgi1_5" "dxgi1_6" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "01x322av5z761lrgcfzyxwfpwqznc5pihlmp4k5a340221zp8g9y") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-0.20 (crate (name "d3d12") (vers "0.20.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libloading") (req ">=0.7, <0.9") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("dxgi1_2" "dxgi1_3" "dxgi1_4" "dxgi1_5" "dxgi1_6" "dxgidebug" "d3d12" "d3d12sdklayers" "d3dcommon" "d3dcompiler" "dxgiformat" "synchapi" "winerror"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "04v8b1vvvpc2df2a9chf1k7baaxqjnqhb4v5gz3idgbr7mjzx2xj") (features (quote (("implicit-link"))))))

(define-public crate-d3d12-api-0.1 (crate (name "d3d12-api") (vers "0.1.0") (hash "0x9zggh3ndhrg9cghying1scpzykb31hsfpnda0vhba9llz4dyzj")))

(define-public crate-d3d12-rs-0.0.1 (crate (name "d3d12-rs") (vers "0.0.1") (hash "0lbiy8armvah0jfsi2s8rf2jp2jn2il7zd6ifaqz2j0dyywiirqr")))

(define-public crate-d3d12-sys-0.0.1 (crate (name "d3d12-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1ihdcr86ragmj33sbcjvx0xm83qjgclmffjdmvxa0161sb6sfi0l")))

(define-public crate-d3d12-sys-0.2 (crate (name "d3d12-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1cwn21fjaazsp662wbq2lypy259vw6drlfhxhs6kw5xsk3xr7bq6")))

