(define-module (crates-io d3 -d) #:use-module (crates-io))

(define-public crate-d3-derive-0.1 (crate (name "d3-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xzixcdc4isgq2r2fscn4c43c1dy7r6vqkrf6wpk2h2q4klhcwqd")))

(define-public crate-d3-derive-0.1 (crate (name "d3-derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qyh3y74h3j14pqdv87fbrvknlh27p1bdxpx3vh4v0kfxzlvg1fn")))

(define-public crate-d3-derive-0.1 (crate (name "d3-derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "08axdzqp2lazz976y0024rmd91dmyqx4b7bqacmnyj3ww1kyvmrr")))

(define-public crate-d3-derive-0.1 (crate (name "d3-derive") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dcz2mahw1sndwmkadwcflawlq0zc2d41zz0n3vwbfb3fblz1yg0")))

