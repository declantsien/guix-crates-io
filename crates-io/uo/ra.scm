(define-module (crates-io uo ra) #:use-module (crates-io))

(define-public crate-uora-0.0.0 (crate (name "uora") (vers "0.0.0") (hash "1mj02fy75x63dxcy19d5anyvbc7flrsydlhv7aj6f2n613zh97d8")))

(define-public crate-uora-core-0.0.0 (crate (name "uora-core") (vers "0.0.0") (hash "1pq4s4a82yhjwabjgffjvc161mrpwk9s7qdg1713vipbsssiixx6")))

