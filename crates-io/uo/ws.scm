(define-module (crates-io uo ws) #:use-module (crates-io))

(define-public crate-uows-crypto-0.1 (crate (name "uows-crypto") (vers "0.1.0") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "09ih6d3ivyl6hy4v9dw663250aliml4jkir1nqssjhvcp0bx0iik") (yanked #t)))

(define-public crate-uows-crypto-0.1 (crate (name "uows-crypto") (vers "0.1.1") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "1575d0pa8d5av9cx3pi35z09lvx198ymx9y61d8vc01ql1jylxb4") (yanked #t)))

(define-public crate-uows-crypto-0.1 (crate (name "uows-crypto") (vers "0.1.2") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "17x6b7cdbvyisza4i3h47rls6gyfxwjpijgdk83vna0mc7agdnbj") (yanked #t)))

(define-public crate-uows-crypto-0.1 (crate (name "uows-crypto") (vers "0.1.3") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "1vlhx41xvpshkhdh7symfjzr9av95jwgl5641s324g85zy8gnnbm") (yanked #t)))

(define-public crate-uows-crypto-0.1 (crate (name "uows-crypto") (vers "0.1.4") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "0q6yc78myx13f6i5d82khxk3yy56jy1zv4dl8dig76lm8cy58iy4") (yanked #t)))

(define-public crate-uows-crypto-0.1 (crate (name "uows-crypto") (vers "0.1.5") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qjvqj1p5kapfkwvc6v5hdj7sxar3ivnk6szw7lnzx517qqzc9pa") (yanked #t)))

(define-public crate-uows-crypto-0.2 (crate (name "uows-crypto") (vers "0.2.0") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "19k9nrd7q0jq9iwzgmirss061mmzl64rvgihzb80vd49bpzh550y") (yanked #t)))

(define-public crate-uows-crypto-0.2 (crate (name "uows-crypto") (vers "0.2.1") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "0przc29258g3cwn5hv9w5x7y4jy53g8b5a5l0cajky54wxmx4ika") (yanked #t)))

(define-public crate-uows-crypto-0.2 (crate (name "uows-crypto") (vers "0.2.2") (deps (list (crate-dep (name "aes-gcm") (req "^0.8") (default-features #t) (kind 0)))) (hash "1b91yhmc1g7kg32d569rpi5kpvhyanxwr0z11nyviyl3msa5i4c5") (yanked #t)))

