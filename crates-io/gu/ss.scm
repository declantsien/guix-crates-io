(define-module (crates-io gu ss) #:use-module (crates-io))

(define-public crate-gussing_game-0.1 (crate (name "gussing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0dn7w01k1xsa2dkjkx2yibhh3pq98c9k9m6qw6wzg4y28629sr0h")))

(define-public crate-gussing_game-0.1 (crate (name "gussing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0xlgp65zzbg9mgkgg210zwyavkjcaryhmdbb0jh7qff9gw3i82y2")))

(define-public crate-gussing_game_RustBook-0.1 (crate (name "gussing_game_RustBook") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "01ijz6nqkgwbs39w540qxxh0klj8pq4n0fkwfccrrklbq2kbq8a8") (yanked #t)))

