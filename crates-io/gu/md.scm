(define-module (crates-io gu md) #:use-module (crates-io))

(define-public crate-gumdrop-0.1 (crate (name "gumdrop") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "1pws0bz343rp49w1iv0q9m52q140q7m41zxypm0w58mmr75hc017")))

(define-public crate-gumdrop-0.2 (crate (name "gumdrop") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.2") (default-features #t) (kind 2)))) (hash "0vh8z997x0pp3fhcb43hz6yg25cn01hfjdxwyardw57ydddy8yvw")))

(define-public crate-gumdrop-0.3 (crate (name "gumdrop") (vers "0.3.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.3") (default-features #t) (kind 2)))) (hash "10b9qysh6ac58wsz2796mgi3myql9j196rlbw6f9xffj30b7yjr1")))

(define-public crate-gumdrop-0.4 (crate (name "gumdrop") (vers "0.4.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jcl1f73lxhsrqv9hx34p7bawg1b52lzv30qzfrbdhz3708c8xmp")))

(define-public crate-gumdrop-0.5 (crate (name "gumdrop") (vers "0.5.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.5") (default-features #t) (kind 0)))) (hash "0a1vijqsf0qnqi6jp4mgxggjhjmidk661vj9fd7d0rwib5wpqp1r")))

(define-public crate-gumdrop-0.6 (crate (name "gumdrop") (vers "0.6.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.6") (default-features #t) (kind 0)))) (hash "1k67q8vkzvrdz80nsaaqw8r3w44k9aj0bdfh3hx0kbxr7c8v8mvs")))

(define-public crate-gumdrop-0.7 (crate (name "gumdrop") (vers "0.7.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.7") (default-features #t) (kind 0)))) (hash "1pmw21ggwqqizh66zp7bylbffi6qs064w2rfj3rc3smyq65r0l7f") (features (quote (("default_expr" "gumdrop_derive/default_expr") ("default"))))))

(define-public crate-gumdrop-0.8 (crate (name "gumdrop") (vers "0.8.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.8") (default-features #t) (kind 0)))) (hash "02waas37nr8y669lnm1ifp6bkx79v3bd0bd4s9qcyy04aifiyms6") (features (quote (("default_expr" "gumdrop_derive/default_expr") ("default"))))))

(define-public crate-gumdrop-0.8 (crate (name "gumdrop") (vers "0.8.1") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "gumdrop_derive") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1qr94qa0h758hn11yhqs2wmb1xaq8adjs8j6hljg1xnji7wh1isv") (features (quote (("default_expr" "gumdrop_derive/default_expr") ("default"))))))

(define-public crate-gumdrop_derive-0.1 (crate (name "gumdrop_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0fs225bapmf793h8ghppyxvdww4zrdwsnxbnri9ld8mry1d8rwnz")))

(define-public crate-gumdrop_derive-0.2 (crate (name "gumdrop_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1kfi32hz90ddfcjqzc9x65r5flz6gr33wixg9pipcj2wnxligx43")))

(define-public crate-gumdrop_derive-0.3 (crate (name "gumdrop_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "086hm84il45ijph8kbfhcbyvvm7jxnsxh28w11h56sn1cjxkff2z")))

(define-public crate-gumdrop_derive-0.4 (crate (name "gumdrop_derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (default-features #t) (kind 0)))) (hash "0ysl2icw177qbd72a0vny5pmihinjba8wf1862apbrwa2ajf6kcj")))

(define-public crate-gumdrop_derive-0.4 (crate (name "gumdrop_derive") (vers "0.4.1") (deps (list (crate-dep (name "quote") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (default-features #t) (kind 0)))) (hash "0c2y31qsp9n3s51s6p1cywkgigadhmy5adaqaidsv4nl7l3na66m")))

(define-public crate-gumdrop_derive-0.5 (crate (name "gumdrop_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "1gfxl7innnk60459plhsjkyr251hl3knmljwbw3dj136zmikdi04")))

(define-public crate-gumdrop_derive-0.6 (crate (name "gumdrop_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xzi26w0yfm3jcwn4kgn5qzdijgqzjk4wzh2wa37a3q95j74irfs")))

(define-public crate-gumdrop_derive-0.7 (crate (name "gumdrop_derive") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1mrm1h4n13b3n79d1215jq9q9d6sgcvfzdb8i5mcmds0vvj4qich") (features (quote (("default_expr" "syn/full") ("default"))))))

(define-public crate-gumdrop_derive-0.8 (crate (name "gumdrop_derive") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "01cdc7w5wf1g9i04ykcssczjmmnl6jky47a648sp710df5yg0pli") (features (quote (("default_expr" "syn/full") ("default"))))))

(define-public crate-gumdrop_derive-0.8 (crate (name "gumdrop_derive") (vers "0.8.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "17d91ai4p9f9cwhqqnyivw9yi7prl9xzpaqq3a1yfxwx8k9rp7vj") (features (quote (("default_expr" "syn/full") ("default"))))))

