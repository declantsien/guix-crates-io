(define-module (crates-io gu sw) #:use-module (crates-io))

(define-public crate-guswynn-test-crate-0.1 (crate (name "guswynn-test-crate") (vers "0.1.0") (hash "06kh3cgkfh66gayw4mz8j7kaz1d5m9i3ff9hbdpvpfdn3c4p8d8h")))

(define-public crate-guswynn-test-crate-0.2 (crate (name "guswynn-test-crate") (vers "0.2.0") (deps (list (crate-dep (name "guswynn-test-dep") (req "~0.1") (default-features #t) (kind 0)))) (hash "1lp3563m23ygq3i6akzsfv9ks2m0q5mnfqlysf2l729xnhdh723s")))

(define-public crate-guswynn-test-crate-0.3 (crate (name "guswynn-test-crate") (vers "0.3.0") (deps (list (crate-dep (name "guswynn-test-dep") (req "~0.2") (default-features #t) (kind 0)))) (hash "19ykkvc96s0qd5icjshdzykndq9fq0qpa15g2zwbaz56lrbrg2sp")))

(define-public crate-guswynn-test-dep-0.1 (crate (name "guswynn-test-dep") (vers "0.1.0") (hash "0z1y3kyi2rc6whq712ss8vc1ic814qzdlqak0di2mwr8mmv33z5b")))

(define-public crate-guswynn-test-dep-0.2 (crate (name "guswynn-test-dep") (vers "0.2.0") (hash "13qzpmmzl21lwvzy3r04g3vcxkjhlkg56ryyrk47p22myd9506v3")))

(define-public crate-guswynn-test-dep-0.2 (crate (name "guswynn-test-dep") (vers "0.2.1") (hash "0apqc1da41j1105zmqqvmz9406vgjscxj7rxfr25pjkdk131d2cv")))

(define-public crate-guswynn-test-dep-0.3 (crate (name "guswynn-test-dep") (vers "0.3.0") (hash "0ns9a6wwcsi2fk87wsdxbps9pbs4pb2g62nync3fv9fp3znjq683")))

(define-public crate-guswynn-test-dep-0.3 (crate (name "guswynn-test-dep") (vers "0.3.1") (hash "18lz85cil8q82ir5s7jc4ryh1krvw69mkw7dzapzcysa7qfsc3zk")))

