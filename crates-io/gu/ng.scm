(define-module (crates-io gu ng) #:use-module (crates-io))

(define-public crate-gungeon-save-0.0.1 (crate (name "gungeon-save") (vers "0.0.1") (deps (list (crate-dep (name "app_dirs") (req "^1.2.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "056zkp6rqddwrbfp0i210a35kkbq9f84ibl1ls0lyqwn1vybvm4f")))

(define-public crate-gungnir-0.0.0 (crate (name "gungnir") (vers "0.0.0") (hash "1alqkxpffvnbnqxbrahdb7xabnss9lmdw5iid2sgfpwpjb5hb3lj")))

