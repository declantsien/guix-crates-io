(define-module (crates-io gu ar) #:use-module (crates-io))

(define-public crate-guard-0.1 (crate (name "guard") (vers "0.1.0") (hash "18dsgmiqhbdwgfz32j1bkvd16bh0vc2wsds9bphm67cr3bbklkp1") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.1 (crate (name "guard") (vers "0.1.1") (hash "11zk8rjwg17k4ayw40x0p2iczrdfclyxck5yyh3m2pv66zy19ldv") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2 (crate (name "guard") (vers "0.2.0") (hash "15zrhpiwxwrhakp1c4imwpcyjyyyl7m1qp0s9aafyg7h0874m7jl") (features (quote (("nightly") ("debug")))) (yanked #t)))

(define-public crate-guard-0.2 (crate (name "guard") (vers "0.2.1") (hash "0x78mkk1c0mmch5crcjm7vaw0q7srpz0q4jhj6cgmh00ka949z1q") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2 (crate (name "guard") (vers "0.2.2") (hash "153fhv8nlv9005lk0yyfisrsvmaxjsm7ybypzd1a5ryfmbykwm1v") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2 (crate (name "guard") (vers "0.2.3") (hash "0rdcwvaim9sjzvpra97zh8nrjj3i6dam1aqn8gzq9ziy0idgyfna") (features (quote (("nightly") ("debug")))) (yanked #t)))

(define-public crate-guard-0.2 (crate (name "guard") (vers "0.2.4") (hash "15yvqwnvblhsjqih3dgkdaprvpw7jwa5vcr3icp3ys581800lqzh") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2 (crate (name "guard") (vers "0.2.5") (hash "0r9d3splmi4cfw4ngdf2sgm35y4ry0ndkzgkhnw71l08zcdh83mc") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2 (crate (name "guard") (vers "0.2.6") (hash "0am6rjh95h91q40fs3hcj3g765lljz9wg9vv2irf6wbdix65nd73") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3 (crate (name "guard") (vers "0.3.0") (hash "0cb2idw4nb5gzk05x9illhmskm50a4d8p74kgpivxjlqan7hpymn") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3 (crate (name "guard") (vers "0.3.1") (hash "0n4lksmhjcqihla0sq2ga09bgm1fnprlj4qknc7kq2q963smajkp") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3 (crate (name "guard") (vers "0.3.2") (hash "0n2gbfjq03sfmbara36d3w80c4vqd2bdglxqydz9nlqhf1j8z346") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3 (crate (name "guard") (vers "0.3.3") (hash "18paa29s5jfvjxpqr0fyh35agkjfrmjbfwczr2fd0czxsh82bq0y") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3 (crate (name "guard") (vers "0.3.4") (hash "0k6i5jbcjdw1rw6h8ywkzp6kw0q9mc2afl540i5dqycq3izll6w2") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.4 (crate (name "guard") (vers "0.4.0") (hash "0zm9gbavrcw3250nb81il3g0z8yg4544b6jl8990fa4l8yg2hr3d") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.5 (crate (name "guard") (vers "0.5.0") (hash "1cdvdylsp0ywq3fkglsxnljnjq9kfxnnq9zfvwnklaavqm7ddwz1") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.5 (crate (name "guard") (vers "0.5.1") (hash "1slkgi0xj1gqjq44cvg8rc65mg3fq1v47bzvfcxqlkx03v2kr2gz") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.5 (crate (name "guard") (vers "0.5.2") (hash "112d0sidfjm28fcpzx79k891pl1smwpvnrr4kb6g7fai8jy04x3x") (features (quote (("nightly") ("debug"))))))

(define-public crate-guard-trait-0.1 (crate (name "guard-trait") (vers "0.1.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2") (default-features #t) (kind 0)))) (hash "0v02ppic9nm0aaij9li3l0d91v0kb7yjw0vx60g4jwbw056p3qbr") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.1 (crate (name "guard-trait") (vers "0.1.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2") (default-features #t) (kind 0)))) (hash "0n0i4pzmv5ira938iiz85nfanxsnpylrwmvx5xvsjk2906lmb1wc") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.2 (crate (name "guard-trait") (vers "0.2.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2") (default-features #t) (kind 0)))) (hash "01m9ppw5rxxdp7vmkafc6kni60bckc7srkivnj9bi1x3sjywfkfy") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.2 (crate (name "guard-trait") (vers "0.2.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2") (default-features #t) (kind 0)))) (hash "1c9x04vrsl7qgd8zqk85wijjjdsww13sppcq8rnbx15j0kwza9fb") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.3 (crate (name "guard-trait") (vers "0.3.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2") (default-features #t) (kind 0)))) (hash "05v87ypwwm2jmr6bnay11a5wz40pfpfar139g5s1fa165r8jj4rf") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.4 (crate (name "guard-trait") (vers "0.4.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2") (default-features #t) (kind 0)))) (hash "0ap2hc4r1dcb1b4xyr4knily6c8hmx68gixm3zn2fh2hrhdhc5w0") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.4 (crate (name "guard-trait") (vers "0.4.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2") (default-features #t) (kind 0)))) (hash "18gi23djz4wiyrpjidwpv3z94irh90dv8r7igzk6zhdr8i4wf51z") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard_let-0.1 (crate (name "guard_let") (vers "0.1.0") (deps (list (crate-dep (name "pmutil") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("fold" "full" "derive"))) (default-features #t) (kind 0)))) (hash "1jyi5flqc1zmbqgbc417h1jayan56rcjbqrwgwr9gza5h7j7l35a")))

(define-public crate-guard_let-0.1 (crate (name "guard_let") (vers "0.1.1") (deps (list (crate-dep (name "pmutil") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("fold" "full" "derive"))) (default-features #t) (kind 0)))) (hash "0k1cgbx7xirdlgxxbq95pk6lh2wgyxy79sgzhcl1hm6yxzpp6dy5")))

(define-public crate-guard_let-0.1 (crate (name "guard_let") (vers "0.1.2") (deps (list (crate-dep (name "pmutil") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("fold" "full" "derive"))) (default-features #t) (kind 0)))) (hash "1hxki3bqiii7b2rbdg14in3j83dd49achlr0ngzi41jypzk88nzm")))

(define-public crate-guard_macros-1 (crate (name "guard_macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("printing" "parsing" "proc-macro" "full"))) (kind 0)))) (hash "0zgmz51lypsdiwds57c0qnyaps4wl50wvplf3pjyzfcnqadqldly") (features (quote (("debug-print" "syn/extra-traits"))))))

(define-public crate-guard_macros-1 (crate (name "guard_macros") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("printing" "parsing" "proc-macro" "full"))) (kind 0)))) (hash "16ap4wa3szl929khc2dvhhlwhdc0qc2a1jydqp67bz2qa8pvaw9x") (features (quote (("debug-print" "syn/extra-traits"))))))

(define-public crate-guard_macros-1 (crate (name "guard_macros") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("printing" "parsing" "proc-macro" "full"))) (kind 0)))) (hash "17wd2flhns2bqpm4qcplnwd5xphisqz2v8dixr4ivznri5122mdi") (features (quote (("debug-print" "syn/extra-traits"))))))

(define-public crate-guardian-0.3 (crate (name "guardian") (vers "0.3.0") (hash "13rnwh4yx5hvfggqsvx4r5bajfpqp1j1jralplaby9svb6qgr71z")))

(define-public crate-guardian-0.3 (crate (name "guardian") (vers "0.3.1") (hash "0vn5xhiwvgc7ziv7npksczkmpsd4fwfiw2p71sdipp810lr5lv3r")))

(define-public crate-guardian-1 (crate (name "guardian") (vers "1.0.0") (hash "1clcbn14rpb9dzrrkh0gf10yhqfdh1svcz9nnm3hnmnqlanjarcd")))

(define-public crate-guardian-1 (crate (name "guardian") (vers "1.0.1") (hash "0m6avspl7gx4wsmzz9fi95znfi4f9cnini0dgjjhw3cfprk1fpvn")))

(define-public crate-guardian-1 (crate (name "guardian") (vers "1.0.2") (hash "0p487li66bn012hpivyns1rkf2gsk6c8mdwib3kblgdd7ic6jshy")))

(define-public crate-guardian-1 (crate (name "guardian") (vers "1.1.0") (hash "1da82ha3p03dy127091rcl79smxgfkkh0ra8y0mbpsfji53ia5v8") (rust-version "1.56.0")))

(define-public crate-guarding-0.1 (crate (name "guarding") (vers "0.1.1") (hash "0xn74dn249px1rbc0z97gqy23rlvgghawcpv5yr7m1dj1icz1qyd")))

(define-public crate-guarding-0.2 (crate (name "guarding") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-java") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-javascript") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "10x97isy84yrkpj30wxw9y78g3gdgg0kyz0g0riclrmzddpkrb3m")))

(define-public crate-guarding-0.2 (crate (name "guarding") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-java") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-javascript") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "10lr940d86nwfvbapjag7f2kna2azxnnmsfv7nvp1s6aj2vlgmci")))

(define-public crate-guarding-0.2 (crate (name "guarding") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "guarding_core") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "guarding_parser") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-java") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-javascript") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "02cnrnny4yv1i602pfj9pyrhjh8r93kfzvdbj45xpg7mbbxscxk4")))

(define-public crate-guarding-0.2 (crate (name "guarding") (vers "0.2.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "guarding_core") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "guarding_ident") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "guarding_parser") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "178v39agcf7jvgv27y7ci831xrpf5d7v099y999a1czfak83434z")))

(define-public crate-guarding_adapter-0.1 (crate (name "guarding_adapter") (vers "0.1.0") (deps (list (crate-dep (name "guarding_core") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "guarding_parser") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1rvwhsd3cqmx32jljw8b64cgmx44xfqzbhy54aflm30iby0s3dbd")))

(define-public crate-guarding_core-0.2 (crate (name "guarding_core") (vers "0.2.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "156w25srcf4a5li3z3pfjh5sndp1g2hj34q851qghhs3d6p2hf81")))

(define-public crate-guarding_core-0.2 (crate (name "guarding_core") (vers "0.2.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1160k5cpphydprqm00n4rlz7cjcpf2x3mi04dk3c3wv24sf5mvwq")))

(define-public crate-guarding_core-0.2 (crate (name "guarding_core") (vers "0.2.3") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "15y4kh9pf1rfms0kfxdrxbql5dbz163bx54lncmc77mfsgxqpqds")))

(define-public crate-guarding_core-0.2 (crate (name "guarding_core") (vers "0.2.4") (deps (list (crate-dep (name "guarding_parser") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "030p1wycszm0jg0fmb23nqp0pjkvrz4cn40ynj319a4j8xa5ll3h")))

(define-public crate-guarding_core-0.2 (crate (name "guarding_core") (vers "0.2.5") (deps (list (crate-dep (name "guarding_parser") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0ks3c93jzjnh1m804bzks862f5cj22gxq79vqid8bl6baagscky3")))

(define-public crate-guarding_core-0.2 (crate (name "guarding_core") (vers "0.2.6") (deps (list (crate-dep (name "guarding_parser") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "09ib3yypybs1d8nxnf2wbdk85r4s2i3pc03y0vwqjk3r4m86mjbx")))

(define-public crate-guarding_core-0.2 (crate (name "guarding_core") (vers "0.2.7") (deps (list (crate-dep (name "guarding_parser") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "13wgdih9vqzdl8hghq9rvg6ps90mhbrvdry124rcw7r2km4snq5c")))

(define-public crate-guarding_ident-0.0.1 (crate (name "guarding_ident") (vers "0.0.1") (deps (list (crate-dep (name "guarding_core") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-java") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-javascript") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "18xdwh8ask0xnkxsm0a5m2840qlnrfk05i4dyv5arkain93ga018")))

(define-public crate-guarding_ident-0.2 (crate (name "guarding_ident") (vers "0.2.5") (deps (list (crate-dep (name "guarding_core") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-java") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-javascript") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0f09jbdwlxx3s8ls6j8z9sg50qssrsyixkyg816fhhwmbkvssa90")))

(define-public crate-guarding_ident-0.2 (crate (name "guarding_ident") (vers "0.2.6") (deps (list (crate-dep (name "guarding_core") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-java") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-javascript") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0lzbhiih35yyczsspfwy4ca40s6w4kdswsh71787mrflawl3f604")))

(define-public crate-guarding_ident-0.2 (crate (name "guarding_ident") (vers "0.2.7") (deps (list (crate-dep (name "guarding_core") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "=0.19.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-java") (req "=0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-javascript") (req "=0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req "=0.19.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0iqqqgw93rw39g4iihf2xa08jwm8b5yf3yfbymfdr08233iaa1ql")))

(define-public crate-guarding_parser-0.2 (crate (name "guarding_parser") (vers "0.2.3") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0w10vwvwj34qwp6rakmlgrh35vlszcsi3p0kqr1khfq539agil4a")))

(define-public crate-guarding_parser-0.2 (crate (name "guarding_parser") (vers "0.2.4") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "01y7jv9dcchn86d5b9kjdwyvjlg11nc5d1864nn2ifljx9r6ggx7")))

(define-public crate-guarding_parser-0.2 (crate (name "guarding_parser") (vers "0.2.5") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0kbj9inbmkh1kv13myf6xc3sqlwgpavmnmgip3xvvndgvlb3h3ym")))

(define-public crate-guarding_parser-0.2 (crate (name "guarding_parser") (vers "0.2.6") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1177v9cip9mq7jxx5fr8l89rwfq7dqlgg9xdjgdmhxgm8nczg658")))

