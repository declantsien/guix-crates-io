(define-module (crates-io gu ro) #:use-module (crates-io))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.0") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "160w25hrfpb0y4ps1pfq659550gdbkc4j3wzi5r46rdrg10l2zqh") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.1") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wc5414s577x4kv35778warpb7bd94yrrsz5wj6gc46g99raaz3p") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.2") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0v35651v1h06ms1g4lykxrdyhj8jbrlcbcn0c38yblx6nhc4h51c") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.3") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0l04mbzim93xgxqkxap5jgd36asrvyxvzzsyhdxhxwqppdiw6i93") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.4") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1adzmimw6ydj3r0s7xjg0cdjcfkhjbclm594xgixl0r2i5z7n7q6") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.5") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1jq4gz2y4whngqyj1lh015zg1883mri9y3rc43wh46hwvixlfvcx") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.6") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1fdwk8s210mhb8b03vppb1pi868ijijvz61lqqkjpq3shwl1kg7g") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.7") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "1mv2nb5s8bhglwqlih90nxgvi72pckr3z61gmfd80ch3k2r21ng1") (yanked #t)))

(define-public crate-gurobi-0.1 (crate (name "gurobi") (vers "0.1.8") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "03akk9izikshkj4y2fwhadpc7gz37wcz8zlp356dh9hkjw6s5lqp") (yanked #t)))

(define-public crate-gurobi-0.2 (crate (name "gurobi") (vers "0.2.0") (deps (list (crate-dep (name "gurobi-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "0x1zmmjiwj57s52kf160alimvickrch0x23007nwziiav4sb0mh2")))

(define-public crate-gurobi-0.3 (crate (name "gurobi") (vers "0.3.0") (deps (list (crate-dep (name "gurobi-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "14w9wgqh09bwnahpbd0gncrwhng7avlzc0gdh3dnw1vp2297cq1p")))

(define-public crate-gurobi-0.3 (crate (name "gurobi") (vers "0.3.1") (deps (list (crate-dep (name "gurobi-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "1vpwla0iybr7c1g43kgr0xg151k7wg0b6m2jf8qmmaaxpbf0pv4v")))

(define-public crate-gurobi-0.3 (crate (name "gurobi") (vers "0.3.2") (deps (list (crate-dep (name "gurobi-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "0sdz0kb0jcfb4161808gdalnj6iv31sfkg87lvkzhn5sw47mj2ra")))

(define-public crate-gurobi-0.3 (crate (name "gurobi") (vers "0.3.3") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gurobi-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "1mqmgv5h50kqd93hpi4f76m8ljpr5zx65knpyp7acfa65xss9ivc")))

(define-public crate-gurobi-0.3 (crate (name "gurobi") (vers "0.3.4") (deps (list (crate-dep (name "gurobi-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "1fsda8rb5q91rgx6maj0gdxygy0xjyyqq5qsrnv1js4k4i0jyqbg")))

(define-public crate-gurobi-sys-0.1 (crate (name "gurobi-sys") (vers "0.1.0") (hash "0rkwd1r1hfr5lifsl8gyfj02172ikrwqg3y89jmcs35zmkwprff0")))

(define-public crate-gurobi-sys-0.1 (crate (name "gurobi-sys") (vers "0.1.2") (hash "1gfww4v0r1zwihmrsc4zg5islq0c85gxd800p4a8rjlv7njjhc10")))

(define-public crate-gurobi-sys-0.1 (crate (name "gurobi-sys") (vers "0.1.3") (hash "0kj4hd4b1vkqvq4x8zrsf58n81ck44f2br6wzryx2csnhjbpkxq6")))

(define-public crate-gurobi-sys-0.1 (crate (name "gurobi-sys") (vers "0.1.4") (hash "1rl9mm9zpxn4jqbyg4pv8x9q453802csq4vpmh0rg733jr7096jx")))

(define-public crate-gurobi-sys-0.3 (crate (name "gurobi-sys") (vers "0.3.0") (hash "1jdvxbpbc849prs2v24asx9dl6jh2zrbayksyqfr0s21a3mxzar7")))

