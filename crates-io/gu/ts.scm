(define-module (crates-io gu ts) #:use-module (crates-io))

(define-public crate-guts-0.1 (crate (name "guts") (vers "0.1.0") (hash "1z05y5z29l0j8f6pxsriz4rvjy9a1m7s3y69f3lw9b3g9h6407hk") (features (quote (("never_type"))))))

(define-public crate-guts-0.1 (crate (name "guts") (vers "0.1.1") (hash "1mgw0fzcjq7p6dicam69igd7cgay1fvxp8gwys6nwv4wq54xpv0w") (features (quote (("never_type"))))))

