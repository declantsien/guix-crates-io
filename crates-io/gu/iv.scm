(define-module (crates-io gu iv) #:use-module (crates-io))

(define-public crate-guiver-0.0.1 (crate (name "guiver") (vers "0.0.1") (deps (list (crate-dep (name "druid-shell") (req "^0") (default-features #t) (kind 0)))) (hash "1k5hcvr3g58jzm69gzfcnb2qf0grr9ah1zqmy9fd0qvdwx3aw3vq")))

(define-public crate-guiver-0.1 (crate (name "guiver") (vers "0.1.0") (deps (list (crate-dep (name "druid-shell") (req "^0") (default-features #t) (kind 0)))) (hash "10idd2fd6w4z3wyffjx14mdic6zs6czx4b0vx0dhd1q5alvfs95s")))

