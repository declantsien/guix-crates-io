(define-module (crates-io gu io) #:use-module (crates-io))

(define-public crate-guion-0.1 (crate (name "guion") (vers "0.1.0") (deps (list (crate-dep (name "qwutils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x09d9v9v9qjjxh40919x36zhaqi4ad4gwizywshmpvl92lss0rm")))

(define-public crate-guion-0.1 (crate (name "guion") (vers "0.1.1") (deps (list (crate-dep (name "qwutils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01mvpcq2yq6a2dkcbqb83xv9jvpad2r1mnl9vx1nf373jw0cbi2f")))

(define-public crate-guion-0.2 (crate (name "guion") (vers "0.2.0-dev1") (deps (list (crate-dep (name "qwutils") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ri2i1cmpqjcrwl8yqjx7zpnwzjd1dplv9mf9rvhph2x2qblb0mm")))

(define-public crate-guion-0.2 (crate (name "guion") (vers "0.2.0-dev2") (deps (list (crate-dep (name "qwutils") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1xh6r0xsyi439sj3m9c259m9qcphhfsj86xjk5mxaddhx0g0yi74")))

(define-public crate-guion-0.3 (crate (name "guion") (vers "0.3.0") (deps (list (crate-dep (name "qwutils") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0as0bknfv0z3qbgrzr4mpi2r3izhpbaz8yvp1nlh8lyliczmlly9")))

(define-public crate-guion-0.4 (crate (name "guion") (vers "0.4.0") (deps (list (crate-dep (name "qwutils") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xkfmsmr8wbcbr3v42vrfipfk965a30rd9ggy8p0sbkipgqip93m")))

(define-public crate-guion-0.5 (crate (name "guion") (vers "0.5.0-dev1") (deps (list (crate-dep (name "qwutils") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jchvfkdlx4670rqk3jhz4jg5rs2b59fzwv3gdh8gkdszm5wipwm")))

(define-public crate-guion_sdl2-0.1 (crate (name "guion_sdl2") (vers "0.1.0") (deps (list (crate-dep (name "guion") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "qwutils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.33") (features (quote ("ttf"))) (kind 0)))) (hash "196knjpc69i7zifbr1yhcwrvj88k6y79xbq2gwbqyq051g3gwsqd")))

(define-public crate-guion_sdl2-0.1 (crate (name "guion_sdl2") (vers "0.1.1") (deps (list (crate-dep (name "guion") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "qwutils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.33") (features (quote ("ttf"))) (kind 0)))) (hash "08j6cygshg2l1dxqn61zn2i2h378jv4736ipimhryc1b4jivrxxn")))

(define-public crate-guion_sdl2-0.2 (crate (name "guion_sdl2") (vers "0.2.0-dev1") (deps (list (crate-dep (name "guion") (req "^0.2.0-dev1") (default-features #t) (kind 0)) (crate-dep (name "qwutils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.33") (features (quote ("ttf"))) (kind 0)))) (hash "1myzypy1631473i3pxzgnmh3mhghj73f7ahbl1mk20wc76kbsc8v")))

(define-public crate-guion_sdl2-0.2 (crate (name "guion_sdl2") (vers "0.2.0-dev2") (deps (list (crate-dep (name "guion") (req "^0.2.0-dev2") (default-features #t) (kind 0)) (crate-dep (name "qwutils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9") (features (quote ("gpu_cache"))) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34") (kind 0)))) (hash "06qbnzfwz69n0kagbykhajhwfmdvvadm8qng44md810pa04vj2cc")))

