(define-module (crates-io gu sk) #:use-module (crates-io))

(define-public crate-gusket-0.1 (crate (name "gusket") (vers "0.1.0") (deps (list (crate-dep (name "gusket-codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1h6h88ypgggkjnx7ihpmrrx7i5ikq31q1imprxgcbq6q3mx3wv6z")))

(define-public crate-gusket-codegen-0.1 (crate (name "gusket-codegen") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("parsing" "full"))) (default-features #t) (kind 0)))) (hash "0zi96fayngp3662ddrf8hmkmcyi77lb5ngq9h87f6jlx7i535gww")))

