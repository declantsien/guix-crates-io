(define-module (crates-io gu mb) #:use-module (crates-io))

(define-public crate-gumbel-top-bucket-0.1 (crate (name "gumbel-top-bucket") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0a52sas69jzxgkdf0lh8hcr7my3iijfla26k0plijr4b43qr16xf")))

(define-public crate-gumbo-0.1 (crate (name "gumbo") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "codegen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cruet") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "pluralizer") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "120d56yg48gwqr3iy9jr54mpzh9lx5c2vp5i848c2wjzzczzhxr2")))

