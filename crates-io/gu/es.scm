(define-module (crates-io gu es) #:use-module (crates-io))

(define-public crate-guess-0.1 (crate (name "guess") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1h7y2flq0bj5qbpmfbgwgz48pmyxnrw68ysxxb5rl9q5w9yvr3bd") (yanked #t)))

(define-public crate-guess-num-0.1 (crate (name "guess-num") (vers "0.1.0") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1wfxyxad41fz58z116dnw20gwghylqb2cx3qd07vcs2vc1z4f2sz")))

(define-public crate-guess-num-0.1 (crate (name "guess-num") (vers "0.1.1") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1s7csc97qca6irmbd04v6qymhfnnhyr5sp8vh896mxwap31xx6jq")))

(define-public crate-guess-num-0.1 (crate (name "guess-num") (vers "0.1.2") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "14cq93wlc33xb7fxyz9sad1365k3zi02grrdq051q8yvyzl7xi9k")))

(define-public crate-guess-num-0.1 (crate (name "guess-num") (vers "0.1.3") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1c7as1r876ybmwxjb8q6h2x2r9vzdnwnada655csq6d96p91l7k8")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.0") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "dark-light") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "octocrab") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.142") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "044xgryab4sailhnzkywc6qm936vn0lj3d1r96ism8lbwv5fkyr1")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.1") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "dark-light") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.142") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "16pcm4sj362318v7cwn57lm63jwf5g7x4kshdkfywn3v3qvmspvc")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.2") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "dark-light") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.142") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1qlqsb81mbbxakci27vb8d5clw9scx1chys4rs7z566w956q0amn")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.3") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "dark-light") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1vn437dqyn9yy3148fpb5d715dv399kjnn6kpbp3pj2akb4c1ysx")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.4") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0pm0ychadpkjc81j7dpq10hmbnhd8w31vxs1ybrz4plpp2q1ac79")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.5") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1k3lq9cw0sfjv9mdgryniig70srxz0imci1rramrpfmwcmf7dy9q")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.6") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1fpvy1zl3rxmkfkh7qi546bknvd2rlm68banabp88jhzw0lwvinc")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.7") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1b976d3n629y9d7praf2s0qgdriw1djca5rhaw8d176hq40pd4cb")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.8") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0bcw2aak5l13h8hgfzng3qyvd5lrb0ykzachijpkq3bvnbfcf25v")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.9") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "12fhrfbdcfhpx4jy9s7rip3s72jv4mkz3vqa61mkl637qjja0vcz") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.10") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0s9k5s4s2adjisqd15nhlfwan5n1pp7ys4alp07b39fvm3nk4azj") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.11") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0sn9vy4sbzypjz3ak8n98raalb0jk4dlnrjpf97fy334i63zxs99") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.12") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0lq916fqmf7r8a0ikv4x9ixjb72msg23jgakcz3jhynflgz8cghm") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.14") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "170gxs5kidhcnq66hw3x2mlwv9lijlwk9cl7c1rf9kpsb6kkhpkc") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.15") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "08km7gfydbawfkbagp1aymscaqaznmizsbwhz8fgvrxjpar6f80k") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.16") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "13kmvni4jghg3bzxfncswpjdah8cyjp4cchgz18x8gc5nzhcqqld") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.17") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0wp9g69n52p37q3mhrid0zf669xpbiaj3830xhhyybl8irpzkyiz") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.18") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0fbqbsi1fi41lks9w1j7641a2cp5zyqqzfsbiq3b8938jggqz3qw") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.19") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "18ib7nikmf67k6wcjifhn5aqm6p6w75x5ahj8kznq2nmfyqn6wnw") (rust-version "1.63")))

(define-public crate-guess-that-lang-1 (crate (name "guess-that-lang") (vers "1.0.20") (deps (list (crate-dep (name "ansi_colours") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (features (quote ("parsing" "regex-onig"))) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0ihfi44jk8ppvzhimw2pjx8c5ph0nj7ip5yi7yzxbwmq7lv9ynnv") (rust-version "1.63")))

(define-public crate-guess_game-0.1 (crate (name "guess_game") (vers "0.1.0") (hash "0i1d1snz7y41p6dil5ndrlfd0k0jrw85k8vym510wds49zfvzxg4")))

(define-public crate-guess_host_triple-0.1 (crate (name "guess_host_triple") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.5.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0n5rsgxzcpqk5vxadpc4g4kv6h5ph3wdima0cmwlaq12ng7pr8k2")))

(define-public crate-guess_host_triple-0.1 (crate (name "guess_host_triple") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.5.6") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0frbl2aq5yb8avhlln6p3xmqz86h2q8d2rap272mbqjgw5dgqrqn")))

(define-public crate-guess_host_triple-0.1 (crate (name "guess_host_triple") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.5.6") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "166xcrp626jhxk7mj4mfyy84iwb96l0k146h6f4qxx4bq2mh711d")))

(define-public crate-guess_host_triple-0.1 (crate (name "guess_host_triple") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "^0.5.6") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1z2shlrdczwv9wk7r94jqqg3ywz1flpzl4jfv0lhcjf74glqqnmk")))

(define-public crate-guess_number-0.1 (crate (name "guess_number") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "typename") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0a83am27mkffwy4wjlkrqmzc1ff5a4qv4ml3r1aviwjd8zc4az0i")))

(define-public crate-guess_number_game-0.1 (crate (name "guess_number_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0yplgskdkbpbbyf3s9kqv6zjcjqc3scrrr6s18axxrsiygp5bn6z")))

(define-public crate-guess_number_game-0.1 (crate (name "guess_number_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1cag6f364a57zc5bdbl29xgdmrs5wlb3kayi8p7k1gli0apq4hd5")))

(define-public crate-guess_paraboloid-0.1 (crate (name "guess_paraboloid") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1zdci72jcgnbs55q0bakc8bpy71d9ah6l3w2qj25wj9r6b9mk6hb")))

(define-public crate-guessg-0.1 (crate (name "guessg") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0b0jja2lkimpsh9s100i60h6bjdy2yx23fgz1min3ajmmn7hp4zp")))

(define-public crate-guessg-0.1 (crate (name "guessg") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1ddgj10vghqgp4x6sk7d3s9z6i0p7pzc700pd5y7ni5y2654dx6n")))

(define-public crate-guessgame-0.1 (crate (name "guessgame") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ac1d3l9aix8ffm8qxklkhymdh7603rkchs2cxinm8aw5nmg4c9f")))

(define-public crate-guessing-game-101-0.1 (crate (name "guessing-game-101") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1pqn73p60jxg0pbcqcgi3frjwjgrh5x7wpia35wdylmz5hmzmnj0") (yanked #t)))

(define-public crate-guessing-game-101-0.1 (crate (name "guessing-game-101") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1sdljd69qbhrrycgqng8slpbm4r60sg4cs6fspgndvavza1741sl")))

(define-public crate-guessing-game-by-sathya-0.1 (crate (name "guessing-game-by-sathya") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "106rz2pzp5s13942cygh8rfm7zma43c9xy3hd9h13gxln6h5qds0")))

(define-public crate-guessing_fame-0.1 (crate (name "guessing_fame") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0rq5878pna3nj86c3v4394xl5x5p4bji120bprkl5b4agy7fi7c0")))

(define-public crate-guessing_game-0.1 (crate (name "guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0c21kxd0xhq6bdh1nl1py8rrpshnaa2xnmb7hwxzli1011llywvz")))

(define-public crate-guessing_game-0.1 (crate (name "guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1knagshs87afixlz8h0gzcif7s2pr8vyv2vz5pnfrxicppvzj1zd")))

(define-public crate-guessing_game-0.1 (crate (name "guessing_game") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xrrkzz7adrsy9l81j38xrh11k51zrv673dd5p11pcx1p9c375xv")))

(define-public crate-guessing_game-79888c11-a5a6-4548-a26a-c85ea39d0499-0.1 (crate (name "guessing_game-79888c11-a5a6-4548-a26a-c85ea39d0499") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "05xgigxdb1mn5x1vbv1snc5mmnbknhnb7rgc4fzv7p08j315xs6q") (yanked #t)))

(define-public crate-guessing_game-panzerstadt-0.1 (crate (name "guessing_game-panzerstadt") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0gd9dfj6xyyar89l9f0kinyihhzml0jij81wlzhii91k5xr37aid")))

(define-public crate-guessing_game-xfossdotcom-0.1 (crate (name "guessing_game-xfossdotcom") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1n44y8wwm2m4iyp0rzs5fljsfcpajlcrgvivz9gd46dv7xhxfi4f") (yanked #t)))

(define-public crate-guessing_game-xfossdotcom-0.1 (crate (name "guessing_game-xfossdotcom") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1izkcwkmr8ci3kmbl3wgdiqwrs60bz404gq42p9wz193d5ph8i4b")))

(define-public crate-guessing_game1234-0.1 (crate (name "guessing_game1234") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0pislwa62wz9ys6mzzg7fdnq97kar3pfsxfiinrk4xhdnp647m0g") (yanked #t)))

(define-public crate-guessing_game1234567-0.1 (crate (name "guessing_game1234567") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0kmpq840csd6ccg75za3hir88m3szn47mmhha36jvi74v6y3n3qx")))

(define-public crate-guessing_game1697-0.1 (crate (name "guessing_game1697") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0g2dmxhf9gam1vna51w6xla36sd0qj72grnxpmzbr75k89xi0nz0")))

(define-public crate-guessing_game_-0.1 (crate (name "guessing_game_") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "10fmvfvbz0iljblkh6x4fbfw112j6zf12gifzkaqyk6qm620famy")))

(define-public crate-guessing_game_0xc789-0.1 (crate (name "guessing_game_0xc789") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1b9ciqsxgihwybkxgaka826q765h4zfj41wajr3hp5ryg2yzsn1h")))

(define-public crate-guessing_game_1029384756-0.1 (crate (name "guessing_game_1029384756") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "08yr89z3bz9h6xrxxpxshvidlh7hl2prpscj1qa9fw5axb3j5il3") (yanked #t)))

(define-public crate-guessing_game_1732891-0.1 (crate (name "guessing_game_1732891") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "030ynmnng6c3yfm42a28mq25qsdsmgsv6bpjdps4xjyyz6z38xga")))

(define-public crate-guessing_game_1llksp581jqha-0.1 (crate (name "guessing_game_1llksp581jqha") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0jxrfydk8s6gsc6vhdga155y3zg7y307cps2fjl7him9cp9yh13g") (yanked #t)))

(define-public crate-guessing_game_1llksp581jqha-0.1 (crate (name "guessing_game_1llksp581jqha") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0lgk0gl1l6c697rbjg49jsicd4pmafz8b1k87ghz7mmgxz496k4a") (yanked #t)))

(define-public crate-guessing_game_2022_02_27-0.1 (crate (name "guessing_game_2022_02_27") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1vlnixrc7lfbpgvxz7cqqh19dv7nl1lbhj6vd8xsid9axav5zv9m") (yanked #t)))

(define-public crate-guessing_game_2022_02_27-0.1 (crate (name "guessing_game_2022_02_27") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "17r6bfgv9ybwc738m9ia3y9z8kvdk2bvjk0aap9arbql04kprrh1") (yanked #t)))

(define-public crate-guessing_game_2022_02_28-0.1 (crate (name "guessing_game_2022_02_28") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0lvsif3sqnn5wnxj9jd45hxypqwd0bx22106ml24h0nd6ifgpfc5")))

(define-public crate-guessing_game_2022_02_28-0.1 (crate (name "guessing_game_2022_02_28") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qzcvp0m98k2fa1wc837qxg1bk8lwxikdiigxcva43fc0v4wjmxc")))

(define-public crate-guessing_game_2857-0.1 (crate (name "guessing_game_2857") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0yz97ln44lhkcl2mbpwl0rwkns8ycls4fizgh25z0ijdmvmax9iw")))

(define-public crate-guessing_game_752-0.1 (crate (name "guessing_game_752") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1ki3xw1d0gs4kjzhvwpqv0m18wazihy0fgmqva6xphwrjp141b18") (yanked #t)))

(define-public crate-guessing_game_987uhv567-0.1 (crate (name "guessing_game_987uhv567") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0h3rqczrzbfxddqw50x6hwabj08zmwm6g3x8vb8ifwk37h48sd2z") (yanked #t)))

(define-public crate-guessing_game_aogaki-0.1 (crate (name "guessing_game_aogaki") (vers "0.1.0") (hash "1r8vx0k1b224ag3aqd1i65wnak57mbwxlyg8ric82fx3nx5si68b")))

(define-public crate-guessing_game_aogaki-0.1 (crate (name "guessing_game_aogaki") (vers "0.1.1") (hash "1pfzxqmbshd8qypakmai6vsqp6421441a38gs8pnzgh9qbjnk4b8") (yanked #t)))

(define-public crate-guessing_game_baadii-0.1 (crate (name "guessing_game_baadii") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "10vwvk1vpwhy0ibj69546p0gjm2dfi03850p53kls3nv0lwjxr4v")))

(define-public crate-guessing_game_basic-0.1 (crate (name "guessing_game_basic") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0rsh8jbhvh6dq7rlan5nng4hnf54igpka6khcjhrp5nydk0zv4wb")))

(define-public crate-guessing_game_BEEF-0.1 (crate (name "guessing_game_BEEF") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "01238m8xc0cnnif2pm9w76jrbn8kmw4i9d0pf4p9k4pknr24ammx") (yanked #t)))

(define-public crate-guessing_game_by_basit-0.1 (crate (name "guessing_game_by_basit") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11l9sjwajg3iq14kfyyd3lii47vvlzw22q8sg5blf50flp3a1pgw")))

(define-public crate-guessing_game_by_bo-0.1 (crate (name "guessing_game_by_bo") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1pml0mixfbfi1sg0zpr3drqk4ah27gqbxbjky5zpin01iw8k02fm") (yanked #t)))

(define-public crate-guessing_game_by_bo-0.1 (crate (name "guessing_game_by_bo") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1w58q2akb8kabdgg8j2nd1w0yks1fghyq3q0796yn9nrq2v5a737")))

(define-public crate-guessing_game_by_ghostwriter-0.1 (crate (name "guessing_game_by_ghostwriter") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dwgwar8dsgq5abwxjkyknzkxpswaidkpbrx6hq3imjjjl68x44z") (yanked #t)))

(define-public crate-guessing_game_by_polariusz-0.1 (crate (name "guessing_game_by_polariusz") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0s99hnxjzhlgm1mz31lq4fapv9hklxijsms26jbzr92kfiz6l38s")))

(define-public crate-guessing_game_by_rust_book-0.1 (crate (name "guessing_game_by_rust_book") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0vfbnawsh5nhzvnp25545pfbalhfnxszsidckkkxc704qxvw72q9")))

(define-public crate-guessing_game_c4rls-0.1 (crate (name "guessing_game_c4rls") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1f4kpcj0vmr51cmk1w7c5w377dsix2q0ikji342pfipx11gsdphl")))

(define-public crate-guessing_game_christoph_blessing-0.1 (crate (name "guessing_game_christoph_blessing") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0pyfnbb3laafikp8987fvxl0ifki4ayc2vis814203lq3r6z21iz")))

(define-public crate-guessing_game_cloud698-0.1 (crate (name "guessing_game_cloud698") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "178nw9ybwrvx5bc715j7j6bhnjyp1w9psrgd7qajand4z2dc6lgw") (yanked #t)))

(define-public crate-guessing_game_cloud698-0.1 (crate (name "guessing_game_cloud698") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1f3197nlpzy8qdl382py2m2ca0zbjd6wk6x349ynqdxbkl1ay4wc") (yanked #t)))

(define-public crate-guessing_game_crate-0.1 (crate (name "guessing_game_crate") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1kh4ljlpv65r0k74dbj3zp4qsdz7knvsqfksc7xy6hyvp0g1kw2q") (yanked #t)))

(define-public crate-guessing_game_crate-0.1 (crate (name "guessing_game_crate") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0qs686r20l6wzhmwjvjx7d974y01ym347qclx5ca4azx28fzf89h")))

(define-public crate-guessing_game_cz-0.1 (crate (name "guessing_game_cz") (vers "0.1.0") (hash "02j9qjyml6rr0k0zkqnmhbk1r5rwvaya4jbzkn112w9dka3zyg44")))

(define-public crate-guessing_game_cz-0.1 (crate (name "guessing_game_cz") (vers "0.1.1") (hash "1vaapzs5xckl845y26yzv6j490c6nx4g5kahrsa5xb7jxmij8rs1")))

(define-public crate-guessing_game_demo_v010-0.1 (crate (name "guessing_game_demo_v010") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "04qpljy2rc0gkmmzfpqkn1qd85pgms0ay1rr8kghr5zankwn3kzw")))

(define-public crate-guessing_game_e8a6167-0.1 (crate (name "guessing_game_e8a6167") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0rkh21r5q78spb6wck3af925m653nqfhk637r1vhbmdlc7y9ll7x") (yanked #t)))

(define-public crate-guessing_game_edg-0.1 (crate (name "guessing_game_edg") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ax0l20hqxgvg02vlfn5ml22radnbvbckbdcx41y92b5lwlbzfc1")))

(define-public crate-guessing_game_edzzn-0.1 (crate (name "guessing_game_edzzn") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m21b89vgpbgym0irddgrshsjby411gm74n2v47nh4fi765sli4n") (yanked #t)))

(define-public crate-guessing_game_edzzn-0.1 (crate (name "guessing_game_edzzn") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xpfh4033k4279cqzm6f87aq16bjakkyxijrd1ffq3pfbpg035xh")))

(define-public crate-guessing_game_elon-0.1 (crate (name "guessing_game_elon") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1k1z3j9dzgsb4dd4b1mn50l7kqh0l7h504lhsjz2c68b2l8lp9ss")))

(define-public crate-guessing_game_example_from_book_crg-0.1 (crate (name "guessing_game_example_from_book_crg") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0ckvkddczrs01x05d357l8f0cn4w8403ckgway99bka4k9zn7wl4")))

(define-public crate-guessing_game_fdr-0.1 (crate (name "guessing_game_fdr") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "08jng3287jcwx6f2pnymy6h8rkdr87lmh2hlj4n6x2kbbsq4l6xd")))

(define-public crate-guessing_game_fedor_nemira-0.1 (crate (name "guessing_game_fedor_nemira") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0qyx11mr1qr5jpjgbci5p1l0rhd6zqwmw7hk5lj3xpdzqjn4sf05")))

(define-public crate-guessing_game_fedor_nemira-0.1 (crate (name "guessing_game_fedor_nemira") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0v956zps7g610b0s5kjkljfngr7yp8pdfypgljywji09nc84q7br")))

(define-public crate-guessing_game_first_test_0987654321-0.1 (crate (name "guessing_game_first_test_0987654321") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0p853x21dqlv0jqsqpbg6py7sw269njhvhdzfzb4nyg681xc5h6r")))

(define-public crate-guessing_game_for_testing_123-0.1 (crate (name "guessing_game_for_testing_123") (vers "0.1.0") (hash "1i3rj62jwhz3zp12zlkjm9kc79ag9d047sn7hlrmq8fb53qz9r3g")))

(define-public crate-guessing_game_fork-0.1 (crate (name "guessing_game_fork") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "077zql7s89pmmzbwhmyh5hv1nz8cgv9jij0xjprnsx63i9wk9g3r") (yanked #t)))

(define-public crate-guessing_game_fork-0.1 (crate (name "guessing_game_fork") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0lk9172sklihxdrnrgd0id8fwasv46fal2gh4x2sv14zb1bv9ap8")))

(define-public crate-guessing_game_from_rust_book-0.1 (crate (name "guessing_game_from_rust_book") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0zzkw32lrgak2alqfmc7hizsa9p3vck3yknmz49sckjjikhmam3x")))

(define-public crate-guessing_game_fsy-0.1 (crate (name "guessing_game_fsy") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0wa3bwr6w3nc3jzadw8218q3qq36rvnppqpyli0bbs7m5pp1fpq5")))

(define-public crate-guessing_game_fsy-0.1 (crate (name "guessing_game_fsy") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1ij4vz1djjkz2zw2w0gyg7ryc8cqmdwih4v8ckfn747abwabdrpj")))

(define-public crate-guessing_game_game-0.1 (crate (name "guessing_game_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "00mqvx864spgg1ybssk3zwyxa24f29izrqvglzmxl7a8128yl2jx")))

(define-public crate-guessing_game_game-0.1 (crate (name "guessing_game_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "15jvy695j9v0xlcdawasbqz4rsr383pcbmajmn9kmjdlxik6bldn")))

(define-public crate-guessing_game_harper-0.1 (crate (name "guessing_game_harper") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1hpwnrydnmxz8shj89x9nnx7ppjgm7ywwhd4grc3qabngnjhnzdy")))

(define-public crate-guessing_game_how_original-0.1 (crate (name "guessing_game_how_original") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "007pr1181xidyr38nm90nk80qnbc0crgiwdd5jvb2f336mvfq6sc")))

(define-public crate-guessing_game_hxx-0.1 (crate (name "guessing_game_hxx") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1k8p5nlwfxad1pwjzs4v1n4ccs8m7099cj66fqj6fzfm7n0k41ij")))

(define-public crate-guessing_game_iing-0.1 (crate (name "guessing_game_iing") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xxqwwa6vfy0sfqmr5rm8jxikvhvj4g0d4307lkfdm1vfclmy027")))

(define-public crate-guessing_game_jacob-0.1 (crate (name "guessing_game_jacob") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "08z1jg8891cxr7mw01jlbs9zjf179wyny7x9qkda164isgd7y7sg") (yanked #t)))

(define-public crate-guessing_game_kongweian1-0.1 (crate (name "guessing_game_kongweian1") (vers "0.1.0") (hash "0p91ha088f16gbgmgd6m6zxml5qrc8zm76b37215wa7h77l7nj1s")))

(define-public crate-guessing_game_kongweian1-0.2 (crate (name "guessing_game_kongweian1") (vers "0.2.0") (hash "1yb3l4bng3lxp7bfpijags2vl9n3mdg53ab0za7zb10g782wicnb")))

(define-public crate-guessing_game_leo_rust-0.1 (crate (name "guessing_game_leo_rust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "04zhh89jhsg25zikxisy2vyfacq40wjdx481c8rlxa01j6nhhzga") (yanked #t)))

(define-public crate-guessing_game_man-0.1 (crate (name "guessing_game_man") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0sq7hlanvc620x0f5ydsvvlrfzvihrmjlryy1hj7vax51r6vvcx0")))

(define-public crate-guessing_game_mlw-0.1 (crate (name "guessing_game_mlw") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "02dxcqvdwcm9nqnq0h0vp72rkb0iy2gwlsrw48mfsfic417srgs2") (yanked #t)))

(define-public crate-guessing_game_mlw-0.1 (crate (name "guessing_game_mlw") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "15wlhdg789y0ihxjpav4yv2kdy2vniax8z8rkcaf644952w5pn9l")))

(define-public crate-guessing_game_morbo84_test-0.1 (crate (name "guessing_game_morbo84_test") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "07jnifmrxhc8kqig242qzaqn774gdiivf06c58bzkkxhfihjig6a") (yanked #t)))

(define-public crate-guessing_game_ms-0.1 (crate (name "guessing_game_ms") (vers "0.1.0") (hash "0wr596zjv6gfr4slq2sj9k2j6kp7i8bbsdhw9z6ha85ywq5xxfj0")))

(define-public crate-guessing_game_muganwas-0.1 (crate (name "guessing_game_muganwas") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0z4d1qhcgjkxg4g9jskh04yryrqcv9ilywclmam8ydp383hh4kvw")))

(define-public crate-guessing_game_new_one-0.1 (crate (name "guessing_game_new_one") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0sxzfkvyhcmyirr9j16zjm0wvv0yvyyz9d9vkn3d2f10336483vv")))

(define-public crate-guessing_game_not_so_good-0.1 (crate (name "guessing_game_not_so_good") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1zakjklzl3cdm913mfax7vybpb8rr5rvksyrzn2ir1d8xf641chn")))

(define-public crate-guessing_game_npolytor-1 (crate (name "guessing_game_npolytor") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "124zb1jzh9ws5421glkmw23k4zhy18jl0d1wyrk35nzvl2knfyp3")))

(define-public crate-guessing_game_npolytor-2 (crate (name "guessing_game_npolytor") (vers "2.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1cifxav8mj4k8wqi0vwmr7drd2pl5nf4gmx85yym4sdygg93h54n") (yanked #t)))

(define-public crate-guessing_game_poen-0.1 (crate (name "guessing_game_poen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "107sprkc0xqfgsm09mga4c0xwh4d2hl99npiyr3c643m8f2f6ci0")))

(define-public crate-guessing_game_poen-0.1 (crate (name "guessing_game_poen") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0p1i319h6acdyxg97p5k33cgx854z3hlpak0bmkg403j22vkm1s2")))

(define-public crate-guessing_game_r41l1m4-0.1 (crate (name "guessing_game_r41l1m4") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1yxvzz2n2ir07bsxdnhr6qqb32aqnwdh69q8rvp1ghyflqnc4sfb")))

(define-public crate-guessing_game_ramiel-0.1 (crate (name "guessing_game_ramiel") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "18qbdkabw7c6z3whfg2nk44jy4nxz0hi5iw3h4dvg3ma8gwfgaza")))

(define-public crate-guessing_game_randy-0.1 (crate (name "guessing_game_randy") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "159zqd6lpy1mrzcwp48xh9s0s124igdhs46w7n9niis7a9k9ar64")))

(define-public crate-guessing_game_rust_book-0.1 (crate (name "guessing_game_rust_book") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1ybyzg0rwnxwaah89qzryzawjsbsayhyn3a1v7b3pmxyzc3qv1c8")))

(define-public crate-guessing_game_rustbook2023-0.1 (crate (name "guessing_game_rustbook2023") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1srwk5y9a208aigmjg0g4q93l9y18q9hcfzngsv2vq599c2y8mn8")))

(define-public crate-guessing_game_sam-0.1 (crate (name "guessing_game_sam") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0zn81pf4dif54cr74vqjhvfprc5wrr110ggmc66gi0nq8cy229sw") (yanked #t)))

(define-public crate-guessing_game_sbrytiuk-0.1 (crate (name "guessing_game_sbrytiuk") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vlszk50faaja25rbcgx3lwbrqyhr04yixdk48s9icwlh7yadn4b")))

(define-public crate-guessing_game_sbrytiuk-0.1 (crate (name "guessing_game_sbrytiuk") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xwlw53bx2z92s91fkhnhvigp1mhzrhzsg19frgwvlwsz3glyqmx")))

(define-public crate-guessing_game_shalzz-0.1 (crate (name "guessing_game_shalzz") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1f76maxrlcyrd5hk75f4k00y9axk18kwskc3s6wa88058crcbjxd")))

(define-public crate-guessing_game_shi-0.2 (crate (name "guessing_game_shi") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1wzc9na6b0px68z3n6wpn1c31bbck8jc851ry67rrm2n6c3fiabw")))

(define-public crate-guessing_game_silloi-0.1 (crate (name "guessing_game_silloi") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0dl5r3caayfr5bqhb5yqv0b1a0bnwaa0cs4r0ilpnz4iggpjzm1s")))

(define-public crate-guessing_game_SoubhikDeb-0.1 (crate (name "guessing_game_SoubhikDeb") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "17hvkj4rrffwpbap53dhrw85ll7rdbgv1a28araqgm3y68ai04b5")))

(define-public crate-guessing_game_tarik_learning-0.1 (crate (name "guessing_game_tarik_learning") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1raqha3y87w62ngqmsvd60ynxj4c3fwq32b74awd06l25wwzhqlm") (yanked #t)))

(define-public crate-guessing_game_test-0.1 (crate (name "guessing_game_test") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0l9mj1hi09bchh1qh31rrcjir5nqc3x3qsnxis03x8ydk8p1r52d")))

(define-public crate-guessing_game_test011-0.1 (crate (name "guessing_game_test011") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1v0a8yrj9kyk36dagqnn85sx6gmaaqsmh3fklnsg73zqvdh27kfw") (yanked #t)))

(define-public crate-guessing_game_test12424-0.1 (crate (name "guessing_game_test12424") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1c8wmijm7mg0z49640gr6911l1fjb55gn3mygr34z0lmnj2z4ij9")))

(define-public crate-guessing_game_test_123-0.1 (crate (name "guessing_game_test_123") (vers "0.1.0") (hash "0mb4s171gygapr6m6ldb50w32zjgpw6157yzr9jhybjiippqhp6w")))

(define-public crate-guessing_game_testtesttest-0.1 (crate (name "guessing_game_testtesttest") (vers "0.1.0") (hash "0jf5yb7v3mx5b68vrbdxdh2xfhi5x9x8dsdlcx399708a9i4xsai") (yanked #t)))

(define-public crate-guessing_game_thahir-0.1 (crate (name "guessing_game_thahir") (vers "0.1.0") (hash "1ya4w1si46nd01w11rszxpns1bv6c0p4w9411q1f6s6s7zjiwfya")))

(define-public crate-guessing_game_thahir-0.1 (crate (name "guessing_game_thahir") (vers "0.1.1") (hash "1yf7cqlywf906rifqp57g19xqxg7i6km36iqgvf6475r16y9i90n")))

(define-public crate-guessing_game_unique-0.1 (crate (name "guessing_game_unique") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "04zx4a2fbgb3i8yipgmvm8smky9zjlgm51wjijq15wc2pvkkjsz1") (yanked #t)))

(define-public crate-guessing_game_unique_t-0.1 (crate (name "guessing_game_unique_t") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "04qf3hhxl3va5yvwraz310ys7n0c6jiz84mkgqj6zaf9qyf3iq4y")))

(define-public crate-guessing_game_v2-0.1 (crate (name "guessing_game_v2") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0p3qflqk6cl0j3kx3yd3xf4bcyvnci6bzvpdk7yplpf9l7s803jz")))

(define-public crate-guessing_game_v2-1 (crate (name "guessing_game_v2") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vrb9mibc76fcz8v2xjg7w7gbbyv8479y41makdjwnrz7gaj6v6l")))

(define-public crate-guessing_game_v94876-0.1 (crate (name "guessing_game_v94876") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1ddj4jzpjq0c3n31aaf3y0s38n13h88jmgxp2wll1jw66lnxr6xr")))

(define-public crate-guessing_game_viggin-0.1 (crate (name "guessing_game_viggin") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16dam8sb9a9564aj57dxvp00hdgsdhkh72fb3b76ml2mh26f40l3")))

(define-public crate-guessing_game_washimimizuku-0.1 (crate (name "guessing_game_washimimizuku") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1833brc0q8bg6ijgpyq8v04mxssf4gx7jdiqgiq4mg5j9p26hrrq")))

(define-public crate-guessing_game_whatever_test-0.1 (crate (name "guessing_game_whatever_test") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "11xlkw1z50pj495qx4qiqxgc6x31wvzl2bv4s4xxz8730vv7dhdb")))

(define-public crate-guessing_game_xiaochai-0.1 (crate (name "guessing_game_xiaochai") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0zk27cahh94zfscdqr073bam5pq4wf8azl3zj4gm20zaah2j1g7i")))

(define-public crate-guessing_game_xiaochai-0.1 (crate (name "guessing_game_xiaochai") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0n653607j1iwlrvasigp1g7isfgsdlwizdvxxrl9chps91lz47n7")))

(define-public crate-guessing_game_xxx-0.1 (crate (name "guessing_game_xxx") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "004waj5r4gjy5h53j791n49whkz8fb2bqq2978p98y0r82gc4hz8")))

(define-public crate-guessing_game_xxxxxx-0.1 (crate (name "guessing_game_xxxxxx") (vers "0.1.0") (hash "1bihq53kqp4iwkfb9pky4hf7q7h775zjnd407qyd7khgi19x66x8")))

(define-public crate-guessing_game_ys-0.1 (crate (name "guessing_game_ys") (vers "0.1.0") (hash "19pwsg7gvs55hajlx7s41n1mv0s95c564vxkfcwy1achypbdavk1")))

(define-public crate-guessing_game_zix-0.1 (crate (name "guessing_game_zix") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "18sj7c9mxcjaww2m7a2j22dxph100c9d7jvd10p6i4gf08y54223")))

(define-public crate-guessing_game_zix-0.1 (crate (name "guessing_game_zix") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1bkjjmpcycf3ywcdfp9in95gwymda0b1lwpmin31gw9r3b9niqmz")))

(define-public crate-guessing_game_zp-0.1 (crate (name "guessing_game_zp") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1l2p42mgrdz4nm69g0ld60blyk2m41f0idlvk37r570dp08xdbbb")))

(define-public crate-guessing_gameJOOO-0.1 (crate (name "guessing_gameJOOO") (vers "0.1.0") (hash "1zdn77ny8vm29fqdrda86xkwr6zrw3qmi9iarfgyn07z9lk3lmrv")))

(define-public crate-guessing_gamelukagaric-0.1 (crate (name "guessing_gamelukagaric") (vers "0.1.0") (deps (list (crate-dep (name "guessing_gamelukagaric2") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bx3a28s9ryi8bn4cafqh76by5nbkh2r1gclra25d7w3svnj5wqw")))

(define-public crate-guessing_gamelukagaric2-0.1 (crate (name "guessing_gamelukagaric2") (vers "0.1.0") (hash "1nn6da6x957hs7688zizkqdmhzbpvlai9db0d4wv6zsv93impj67")))

(define-public crate-guessing_games-0.1 (crate (name "guessing_games") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "048craci2bq1x6b8mld9shxv0037lss28xd50nz9q6pn0cjhgl6p") (yanked #t)))

(define-public crate-guessing_gametestxyf-0.1 (crate (name "guessing_gametestxyf") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0q2g9zdgjppqbab438kl69m9p806hrjc8vaw2zbray5sfl1z34z0")))

(define-public crate-guessing_name-0.1 (crate (name "guessing_name") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "=0.8.5") (default-features #t) (kind 0)))) (hash "1vx3r5fi7b26k58p8hnzxr6k3z1qq76pwk81q86rl58psd2i6ihy")))

(define-public crate-guessing_names-0.1 (crate (name "guessing_names") (vers "0.1.0") (hash "1czcyjgv5sgy99s0xf313gxq0h7p7lxs3prkiszg5b6p9vwvbmcl")))

(define-public crate-guessing_number-0.1 (crate (name "guessing_number") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1c7hcf0rs33mdk9xdhdqfbp9d2i4zj9yp27vs7ybw033h6brn368")))

(define-public crate-guessing_number-0.1 (crate (name "guessing_number") (vers "0.1.1") (deps (list (crate-dep (name "actix-web") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1l2q647ndw1ayqrqy4nqpv3dccf658qi8kpa90dxsd53g7jwa80s")))

(define-public crate-guessing_puzzle-0.1 (crate (name "guessing_puzzle") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1wyb7v8cshcj1dc1y94xxfk17sid8gkx10r3wwrx5hrzry6xngq3")))

(define-public crate-guessing_utils-1 (crate (name "guessing_utils") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0v8k0pyscgm6c2b4cafy83ww8grn312fwgcpgifn8jd8h93gghdr")))

(define-public crate-guessing_utils-1 (crate (name "guessing_utils") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1fgcc4rc4ilz76x97s8hxcrcj5jgz3a0zkavhxk3w8gk06a20m8c") (yanked #t)))

(define-public crate-guessing_utils-1 (crate (name "guessing_utils") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0la0b8kk9wxhlhxd0wdq84d23y9rxyc67s196p88aj5bybn044r5")))

(define-public crate-guessing_utils-1 (crate (name "guessing_utils") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1q48ib2wkpfa6xgl4c6kjazl40incbsk6k3k1wfanr510fxds1nz") (yanked #t)))

(define-public crate-guessing_utils-1 (crate (name "guessing_utils") (vers "1.0.4") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1zq8nb5gpz4cm8nm0g5pdhic9h568yhdp2f7rx5k69vbzag193sy")))

(define-public crate-guessmetest-0.1 (crate (name "guessmetest") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1hirzkscqbfjgl84hsxscfcqfiw5iyrawnbhzac68yb8ryphvcsp")))

(define-public crate-guessnumgame-0.1 (crate (name "guessnumgame") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1n2fly4hf5sr517cmbc4kak81ljljpwfn4z4cwxs1zv92isdryv6")))

(define-public crate-guessture-0.1 (crate (name "guessture") (vers "0.1.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)))) (hash "1mxqald57p2iwnkrshd9kn8gsc6x0ljq3qr3l57dr4jmn6vfx9ck")))

(define-public crate-guesswhat-0.1 (crate (name "guesswhat") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "149lan03n1jz7cbxp9ljxsirv8glk4mq4z6j1czfp0dasf0ycpqd")))

(define-public crate-guest_cell-0.1 (crate (name "guest_cell") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0cvg2w7r0g04m5h9jdsmmcpcqfp2w0rdcpvb8ki07d12ncbvsam5")))

(define-public crate-guest_cell-0.1 (crate (name "guest_cell") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "15z7dab5kg9f2zl2l2ghv3bc64yvy1v1sfaz6c6kjr92kqgq5ss0")))

(define-public crate-guest_cell-0.1 (crate (name "guest_cell") (vers "0.1.2") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "092jwlsyrkga6lcpbffdny7nb2lh80c79qgadakz99asfy2qic6m")))

(define-public crate-guest_cell-0.1 (crate (name "guest_cell") (vers "0.1.3") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0y98y7zs180qpn6az1rh31v3i4k770lh3bpg650fic19qjsksl5a")))

(define-public crate-guest_cell-0.1 (crate (name "guest_cell") (vers "0.1.4") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0rz5qa2fyavpkfkv3mxjnnc5rh0jmk8n4nihp5c5w2h42cczyznq")))

(define-public crate-guest_cell-0.1 (crate (name "guest_cell") (vers "0.1.5") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "11cv3d7dmrd67j77fiphmg5l39jmyr5lr2ldsvm5h63j36rmpxgw")))

(define-public crate-guestfs-0.1 (crate (name "guestfs") (vers "0.1.0") (hash "1zx4vkfrxzmq18n84j3cip1ydlb7ya1blfi4qwslyvpqfn0ljwg2")))

(define-public crate-guestfs-0.1 (crate (name "guestfs") (vers "0.1.0-pre1") (hash "12yln76ar824gbglkk2n2ypwj35cd5q0jyd82pjlrl5jz2mflk3q")))

(define-public crate-guestfs-0.1 (crate (name "guestfs") (vers "0.1.0-compat1.40.0") (hash "01irlv73vws94s6qij789jg4yb8cvb4glprl9sqgs3yz43w3cabw")))

