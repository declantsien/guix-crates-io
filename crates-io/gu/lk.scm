(define-module (crates-io gu lk) #:use-module (crates-io))

(define-public crate-gulkana-0.1 (crate (name "gulkana") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1xbvgmlzqkl42ingppwys6whrldj6lmfigvp7hq127vw0d8nd663")))

(define-public crate-gulkana-0.1 (crate (name "gulkana") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "14yjpnjy1fmf8mhpr7cwh53bc129cl5wh5v8k2szlvjapn348sb1")))

(define-public crate-gulkana-0.1 (crate (name "gulkana") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "04gyx5vgx0anj58vqan15wjqawgsjwjh2z73i57dywnlj2vnnzsv")))

(define-public crate-gulkana-0.2 (crate (name "gulkana") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "023jl23kphk6ppnzpyf21ihb56jc4a15lvmj8wl4339j4p9chrq8")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0nrydj18780xad948gdz13abfpy036s9vr37zd25lgv1fhhxqfz2")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "06z70by2zdawapm02mlk0q87rqcf52ms1zs5w9a022s38apjsm1n")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "03dqm1vj9ys54linpxbpyy5na5nyq5lvjz8kn70ih2gqg50p85nf")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1fibi2qh80nffavz3zgcz0ppyal8alqpl5hi6fm55yv1b0lh1lzs")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.0.4") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1rxdvd1q6yss91nvqnd33q2iill8p03qklajgbvnny6p8lzik1qx")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.0.5") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0wkr4fzcxcwh7hipw5bswzph053fbj2ilq9dnkq8zha0c7x0f564")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.0.6") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1ga28rnnx89irp4vlm6fy1hz549zf7lqncax0n38qskwixq17933")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0d74zg9bdmwj1g0gpqkz5dy8m9lrizplh6v7nh4i6q6b2zrgi7cw")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "19d9g848kg0g2wwl8plq7wlfxzk7s73ni6xi1rnvq6vil5qh4l97")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.1.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "18a014vkxa89s97zxsf7v3zjfc1pvbj61q0dy8axlgsgyd2nyk0x")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.1.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0ga52h0xnicsfya9p98rqfpz0w6bgxyhg94dzrv164p5lkz9mrps")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.1.4") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "11qa2das5ykcz6694mv6lnzbn4nc9pp8845h9d5hp0nxqdzxxn5j")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.2.4") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0gsn8zpsna8pd7dsnm7dgbj6yx6ckrgrm2srkdwmjmv2bhiyylqs")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.2.5") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "064hib71xnk42sqkbi829x1bbqkrp18rhc90dv113nm5hp6rdsjn")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.2.6") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0a8wqb0d3y411wv98mx8gc2r9352r5fnncib6pcxjfffvzdpdmnp")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.2.7") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1kjnzg866c82wn45v6gkr6m0qws2ynb3lf0afxcsxma55qfz7jyl")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.2.8") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "085wsdqz1w90ii0kw915j5z1kpwyn9y6wxfj2jvrw79x65zar57j")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.3.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0g3wgd9bq46m958blhdbng80cdsabvj65gcx2knm7a482312ml3n")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.3.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0nzbl6b0gqz0dv29rc0i7s84hvfnbvxqwks5n9cp2ffqmbwsk8kg")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.3.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "02070iwp5kybhfjdkj65a7is13b2jzhxfr8n14y4aa72ic2jrrvh")))

(define-public crate-gulkana-1 (crate (name "gulkana") (vers "1.3.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "12wc5sj4d9qwwj8fm256nxvsyrzyhksmrdk4kvx5qqbz2p6km20p")))

