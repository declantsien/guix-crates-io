(define-module (crates-io gu ui) #:use-module (crates-io))

(define-public crate-guuid-1 (crate (name "guuid") (vers "1.0.0") (deps (list (crate-dep (name "base32") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("std" "getrandom" "std_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "1x98j6y9y119rklz0701igp661qafzqx3j3x7sz2a55jwi2111qq") (features (quote (("std" "rand" "base32") ("no_std") ("default" "std") ("alloc" "base32"))))))

