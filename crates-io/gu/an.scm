(define-module (crates-io gu an) #:use-module (crates-io))

(define-public crate-guanabana-0.1 (crate (name "guanabana") (vers "0.1.0") (hash "0hlfy8bi17cdkyiipll320jdxj7i1zy72gb5ibsrxfrw1hmfwbq6")))

(define-public crate-guance-0.1 (crate (name "guance") (vers "0.1.0") (hash "1dl9b32xd4jh22b7924bx23rm70df22psmv4kjs9mky62bxjwrsh")))

