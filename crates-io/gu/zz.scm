(define-module (crates-io gu zz) #:use-module (crates-io))

(define-public crate-guzzle-0.1 (crate (name "guzzle") (vers "0.1.0") (deps (list (crate-dep (name "guzzle-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "126mnk7wgwyyrmp20qh2c9im0h5dq3ak9gvj963yrxxlfhfws1qp")))

(define-public crate-guzzle-1 (crate (name "guzzle") (vers "1.0.0") (deps (list (crate-dep (name "guzzle-derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 1)) (crate-dep (name "trybuild") (req "^1.0.17") (default-features #t) (kind 2)))) (hash "09yp6mbjwmrml0q6w6cgrqzndhikiqprnwg1nv2jrxxz6668lxiy")))

(define-public crate-guzzle-derive-0.1 (crate (name "guzzle-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.36") (default-features #t) (kind 0)))) (hash "198irjnrviynlqiiyfmnwdkscjs1dly89y11lfih8jw97knfhx7p")))

(define-public crate-guzzle-derive-1 (crate (name "guzzle-derive") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0dqywbmrh02gzjvf5hhnhr8ir9n8jvh4556bgs9fkxfrjpn764s5")))

