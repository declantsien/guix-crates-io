(define-module (crates-io gu in) #:use-module (crates-io))

(define-public crate-guin-0.1 (crate (name "guin") (vers "0.1.0") (hash "0rw1b5981fqrknx9frc587p34cyv3wgx69v01k7v76c76xmm3p0b")))

(define-public crate-GuiNistRs-0.1 (crate (name "GuiNistRs") (vers "0.1.0") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "egui_extras") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "nistrs") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "winres") (req "^0.1.12") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0lp3934vx4xkx8wscv99vafjjfy2s8i2rvvqsirhfd4nb9fmd34s") (features (quote (("wgpu" "eframe/wgpu"))))))

