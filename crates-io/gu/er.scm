(define-module (crates-io gu er) #:use-module (crates-io))

(define-public crate-guerrilla-0.0.0 (crate (name "guerrilla") (vers "0.0.0") (hash "0gk5qiagfddh6c9ylbwm01h5c74rdhda32x1ivdiqjrs9g414ak3")))

(define-public crate-guerrilla-0.1 (crate (name "guerrilla") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (target "cfg(any(unix, macos))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1bqik94ys8xz3c2d77nrhbq3qr3fq7izqyvklq849v3w7agv272a")))

(define-public crate-guerrilla-0.1 (crate (name "guerrilla") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (target "cfg(any(unix, macos))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1x4sqycfwlcpslyw6rgbhay8ydm0i815x5vr1j4iy03p49hycm9a")))

(define-public crate-guerrilla-0.1 (crate (name "guerrilla") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (target "cfg(any(unix, macos))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0dwck4yhgbz1b48pngcz2wdy2jab614j82lp972v22cb90iwii7x")))

(define-public crate-guerrilla-0.1 (crate (name "guerrilla") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (target "cfg(any(unix, macos))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "104a8pbfh115f3hnjl49xnjz8ygv7bh7qk92qzl7xl0alpdm04vf")))

(define-public crate-guerrilla-0.1 (crate (name "guerrilla") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (target "cfg(any(unix, macos))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "00m8ri2mq4z6qhg9238kqjy9ygsjwqr74hhixppfz475772w0yis")))

