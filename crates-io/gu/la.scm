(define-module (crates-io gu la) #:use-module (crates-io))

(define-public crate-gulagcleaner_rs-0.10 (crate (name "gulagcleaner_rs") (vers "0.10.1") (deps (list (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "0j2gk9jsyclr8v5g55hwikx4hm40v5yplxfi4hxrh4256qp4bfwk")))

(define-public crate-gulagcleaner_rs-0.10 (crate (name "gulagcleaner_rs") (vers "0.10.2") (deps (list (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "0nii355kas8ihaf5lvmipjkj1mwbbjbh01jd2jp1jbbcfz4xvflg")))

(define-public crate-gulagcleaner_rs-0.10 (crate (name "gulagcleaner_rs") (vers "0.10.3") (deps (list (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "0wi2w6rd0lq23cav4kmdyy78g4xzsglg97lkdghi0jlhhmgcblxc")))

(define-public crate-gulagcleaner_rs-0.11 (crate (name "gulagcleaner_rs") (vers "0.11.0") (deps (list (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "0j4rhbgs6s46xcdb178k4hwziiq6jw9c398w1l7w1fdqfk09zbh8")))

(define-public crate-gulagcleaner_rs-0.11 (crate (name "gulagcleaner_rs") (vers "0.11.1") (deps (list (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "0ss4ckjm1mayps9fn0j13fm31kl2vhzvbnllbmicakshvq3y9lnm")))

(define-public crate-gulagcleaner_rs-0.12 (crate (name "gulagcleaner_rs") (vers "0.12.1") (deps (list (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.32.0") (default-features #t) (kind 0)))) (hash "1a2mkm5dhzblh95dcdki6d5lx8pqqhwk1pqd936zmhi43nvpsyk4")))

(define-public crate-gulagcleaner_rs-0.12 (crate (name "gulagcleaner_rs") (vers "0.12.2") (deps (list (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.32.0") (default-features #t) (kind 0)))) (hash "0i7lbkbksp56i9g6r8c3rd17aa5ki0wz0k3z8dsh5lg2p7c5nrx6")))

(define-public crate-gulali-2019 (crate (name "gulali") (vers "2019.3.4") (hash "0ba29xsxiknbl5h4xhyq23dfi926wj5g7bwq9600ab19dz4b8wp5")))

(define-public crate-gulali-2019 (crate (name "gulali") (vers "2019.3.5") (hash "1r389yxbysp0dc8kmrwqq04fh5fgk84cbwyzkl0h84jakbf6dk57")))

(define-public crate-gulali-2019 (crate (name "gulali") (vers "2019.3.6") (hash "1hw7pyax2cqkr69iqlwsjgzf7dcdarsm9qx576mrvdhn4rqlibh9")))

(define-public crate-gulali-2019 (crate (name "gulali") (vers "2019.3.7") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "0d203saf1xiwvl3266c4jq2lv12f0y0rvwqn6dklc7avav6w5072")))

(define-public crate-gulali-2019 (crate (name "gulali") (vers "2019.3.8") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zl518mm80sg2glw8w4yj8plkvxjc2wd65pvdvdxsd5am2kggbzq")))

(define-public crate-gulali-2019 (crate (name "gulali") (vers "2019.3.9") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "09rqrml9jzfc9xamahwgskzqg945hk5x5fbmwxbbbsjyr1g2lwmx")))

