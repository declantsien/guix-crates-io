(define-module (crates-io gu it) #:use-module (crates-io))

(define-public crate-guit-0.1 (crate (name "guit") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "03vkvmg2xia4w7d2xpcq67hkn148yyklbb2bvp4z263qip0h6ybr") (yanked #t)))

(define-public crate-guit-0.1 (crate (name "guit") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0irqlyyshvi856ikyf9aj6cxklykyln6n2382pymhk3bpgnmaa48")))

(define-public crate-guitar-0.1 (crate (name "guitar") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "1p3kindvb5lqs66mhdynm9zx52lm8fjzzkq4d6s2r0cl27q0canp") (yanked #t)))

(define-public crate-guitar-0.1 (crate (name "guitar") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "1jaam55n752xpw1xspy3aqyysxd2f9a9aifmc1g0xisb2v84wm0y")))

(define-public crate-guitar-riff-0.1 (crate (name "guitar-riff") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)))) (hash "03g3dvq0bgn3qkcsq4fk09nvcppizh62w24m2mcma445z2yq803c")))

(define-public crate-guitar-riff-0.1 (crate (name "guitar-riff") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)))) (hash "19dd6ja57yhskrpps9nsyahgwrcdzr56h80g5p28xjxns6c9dnp9")))

(define-public crate-guitarpro-0.1 (crate (name "guitarpro") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "fraction") (req "^0.10") (default-features #t) (kind 0)))) (hash "020b1cl50ka2b41gwfzwsbm9lxmpk3kjx04im6yri896pgwk6pwc") (features (quote (("build-binary" "clap"))))))

