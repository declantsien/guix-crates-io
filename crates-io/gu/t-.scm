(define-module (crates-io gu t-) #:use-module (crates-io))

(define-public crate-gut-derive-0.1 (crate (name "gut-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0532nmb5vc5bzfpfyq177dia5s5fip3pa2fnnlvr624s7fb5zl84")))

(define-public crate-gut-derive-0.5 (crate (name "gut-derive") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00fy5rzdanpmrib47mzr3qjiscd82kl0fhlzmfxr89vy0dymb92g")))

(define-public crate-gut-lib-0.1 (crate (name "gut-lib") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7") (default-features #t) (kind 0)))) (hash "069k4xpp6zx6s2dvsnj9hb4im9dx8w42wa6j52qfm1ypwj541scb")))

(define-public crate-gut-lib-0.1 (crate (name "gut-lib") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.20") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7") (default-features #t) (kind 0)))) (hash "1a3xci8gmn1i5d6a9py8nlji97pw3a815rfi502sxg0b7i1dnc6g")))

(define-public crate-gut-plugin-0.1 (crate (name "gut-plugin") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1m3c1zwvmx68dkpndr9m3z0vsll586vwxq4z5q2znflx063skmdr")))

