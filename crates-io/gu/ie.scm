(define-module (crates-io gu ie) #:use-module (crates-io))

(define-public crate-guiedit-0.1 (crate (name "guiedit") (vers "0.1.0") (deps (list (crate-dep (name "egui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "egui-sfml") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "guiedit_derive") (req "=0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "sfml") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "10ykv1y5zcaymqv5mvx3annnfgb0a5gpf9cx3c7fsf5cs25mj307") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("sfml" "dep:sfml" "egui-sfml") ("derive" "dep:guiedit_derive"))))))

(define-public crate-guiedit_derive-0.1 (crate (name "guiedit_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11cxkh9w3sra80wc7warr75lq0f7ah142ydzrwhbryrd5q66xbqc")))

