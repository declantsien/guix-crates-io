(define-module (crates-io gu nz) #:use-module (crates-io))

(define-public crate-gunzip-split-0.1 (crate (name "gunzip-split") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("std" "derive"))) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "19yd92h9z83iwzlnc6py0gws3x5n93jsmr77347yradqp0yy09hg")))

(define-public crate-gunzip-split-0.1 (crate (name "gunzip-split") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("std" "derive"))) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "0i72dipbf757vghi12fcy280z9lvfi3dd49v72b6h3gf3v08vyi6")))

