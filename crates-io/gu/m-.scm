(define-module (crates-io gu m-) #:use-module (crates-io))

(define-public crate-gum-rs-0.2 (crate (name "gum-rs") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "term-table") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0259s8j8mdi1kyh4vkq6y0085rhncvhsdn89zrm43s9x6p790pkh")))

