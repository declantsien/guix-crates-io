(define-module (crates-io gu i_) #:use-module (crates-io))

(define-public crate-gui_64-0.1 (crate (name "gui_64") (vers "0.1.0") (deps (list (crate-dep (name "gfx_64") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log_64") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "math_64") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ttf-parser") (req "^0.15") (kind 0)))) (hash "0cmk3vgwhfwnbb6qgaf6df674hzn2q9504jbanm3lk2bn0hm35ki")))

