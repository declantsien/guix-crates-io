(define-module (crates-io ae st) #:use-module (crates-io))

(define-public crate-aesthetic-1 (crate (name "aesthetic") (vers "1.0.0") (deps (list (crate-dep (name "unicode_hfwidth") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0p31j91cghvyf9mcarf170kh9wi775hsp2yinwjsxw0hkm83pj3p")))

(define-public crate-aesti-0.1 (crate (name "aesti") (vers "0.1.0") (deps (list (crate-dep (name "fmt-extra") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "index-fixed") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0jf3r4ihw4fdcdbvk6w088rrqf7v47605y8jgrbi5g7hd6b5ysmv")))

(define-public crate-aesti-0.2 (crate (name "aesti") (vers "0.2.0") (deps (list (crate-dep (name "fmt-extra") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "index-fixed") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0y1f3vdzgdj7whbmac3yipiy66s8mcrp9rv3115jdm6zd95ip7jr")))

(define-public crate-aesti-0.2 (crate (name "aesti") (vers "0.2.1") (deps (list (crate-dep (name "fmt-extra") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "index-fixed") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0n44k0xjyy5xjfy4h7w3y4z7cajfd49cdrrslj4jbl8n8sr4rbzn")))

(define-public crate-aesti-0.3 (crate (name "aesti") (vers "0.3.0") (deps (list (crate-dep (name "fmt-extra") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "index-fixed") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0vbiaj210i5fb3nf6vvdkkhiyyhvzq5m9z06f8aiaxnx3i4202d0")))

