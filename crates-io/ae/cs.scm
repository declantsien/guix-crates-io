(define-module (crates-io ae cs) #:use-module (crates-io))

(define-public crate-aecs-1 (crate (name "aecs") (vers "1.0.0") (hash "0c7nvbjkrkgabar3k38kgc06mb4w13rcs856qgp7nb56q9nxzpzl") (yanked #t)))

(define-public crate-aecs-1 (crate (name "aecs") (vers "1.1.0") (hash "1n8bc6vxgqvhnyga7aplfribxkvwym4lqy646zwcymlh159apmig") (yanked #t)))

(define-public crate-aecs-1 (crate (name "aecs") (vers "1.1.1") (hash "1lxrlmdd4jlf4lmm7ld1yv35ghzzkm74p3by0fr0f4q9rzhrlsr9") (yanked #t)))

(define-public crate-aecs-1 (crate (name "aecs") (vers "1.1.2") (hash "1jd4hw8si2ssblzmh6mczhm0d3pnazsypj68hb2klapmidb0jz15") (yanked #t)))

(define-public crate-aecs-1 (crate (name "aecs") (vers "1.2.1") (hash "142fb9qb1hf4agrvbz595w0j1ixkq7jjr62a1v93j4cpxprl3mc9") (yanked #t)))

(define-public crate-aecs-1 (crate (name "aecs") (vers "1.2.2") (hash "0kndf1v3sv4zvanwna0g5xakrb26qi5c7s00qflm2c28fcqqpy9f") (yanked #t)))

(define-public crate-aecs-1 (crate (name "aecs") (vers "1.2.3") (hash "017jklm7bz9m3jhgq52xnlxd97zcf6nrchi5ahhx1qb6hndxvf4a") (yanked #t)))

