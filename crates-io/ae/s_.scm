(define-module (crates-io ae s_) #:use-module (crates-io))

(define-public crate-aes_crypto-0.1 (crate (name "aes_crypto") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "0hrz9m6dwbv1wi932hwvy6mdv8fqkaxvas25g3hl0b0kp8w74alk") (features (quote (("vaes")))) (yanked #t)))

(define-public crate-aes_crypto-0.2 (crate (name "aes_crypto") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "0lf0slzyhifjkjn5wbxbig8cxd99mds4j812avjjrnxjxdhc6195") (features (quote (("vaes"))))))

(define-public crate-aes_crypto-0.2 (crate (name "aes_crypto") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "0fpqw1b45nr18yiznzdrb4fxapmhhkkj81k8lmp5zda1kl4hg7al") (features (quote (("vaes"))))))

(define-public crate-aes_crypto-0.2 (crate (name "aes_crypto") (vers "0.2.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0ja8pxd6ykjks543s8snvdvkz92v5bgm8q30fbfm91xcj2vl3m0c") (features (quote (("vaes"))))))

(define-public crate-aes_crypto-0.2 (crate (name "aes_crypto") (vers "0.2.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0j5yap9ics0xrgfm63yfbp4fa11ixc2sj2afq7f3d0bnh5wsjwrg") (features (quote (("vaes"))))))

(define-public crate-aes_crypto-1 (crate (name "aes_crypto") (vers "1.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "118qh301vkigcx0q9zjsn96n539ycxghyj18smf4nrhm25nnfijq") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-aes_crypto-1 (crate (name "aes_crypto") (vers "1.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0bdf2vgfz4vpnnbr68vgisml44s6vaixvnqqfgdx5w2k0vzcq3zm") (features (quote (("nightly"))))))

(define-public crate-aes_crypto-1 (crate (name "aes_crypto") (vers "1.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "09n1s0vvdf7yba9ds0whmfh24j6ik6f65bgq9kf45anaxbp6m3zq") (features (quote (("nightly"))))))

(define-public crate-aes_ctr_drbg-0.0.1 (crate (name "aes_ctr_drbg") (vers "0.0.1") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0930nlj68vh167py2w71hm1w5qfk20ggdzbyl2812lsfbwh1zmcw")))

(define-public crate-aes_ctr_drbg-0.0.2 (crate (name "aes_ctr_drbg") (vers "0.0.2") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l4hlx6jhjzapc18b95l270varsxms7l4qpf1g2055azpch2x96h")))

(define-public crate-aes_frast-0.1 (crate (name "aes_frast") (vers "0.1.0") (hash "0ph5b7s1hkg5wvad03i63az4ldmy47d1mf78lrfgzcd9z13gq9gz")))

(define-public crate-aes_frast-0.1 (crate (name "aes_frast") (vers "0.1.1") (hash "18rpnvfkllm2d2zx4yad9qakx0kqirs27ksnaxj3jmwj1bfk0hrm") (yanked #t)))

(define-public crate-aes_frast-0.1 (crate (name "aes_frast") (vers "0.1.2") (hash "049bhrar7qnq3mcdj0qcdma9h86xrr64k042w9j72z5qrvx4hf9p")))

(define-public crate-aes_frast-0.1 (crate (name "aes_frast") (vers "0.1.4") (hash "06vzjw9l8dm73cyp2dzphqh6yvvfbcm2lch9c0fl44r3w6iglahb")))

(define-public crate-aes_frast-0.1 (crate (name "aes_frast") (vers "0.1.5") (hash "01af9vzdncw443x4513pfdaj22i198hrwl6ffq2jy9x95k0mzb8c")))

(define-public crate-aes_frast-0.2 (crate (name "aes_frast") (vers "0.2.0") (hash "0ndgw4130wfkrqns4yxih4qnsbqnh3ywljf6j7ha5bn7w5vb5rh7")))

(define-public crate-aes_frast-0.2 (crate (name "aes_frast") (vers "0.2.1") (hash "0182d6w36j8kdg18r57i7b4khwq3fp633x5waa8gq97mhf05ksan")))

(define-public crate-aes_frast-0.2 (crate (name "aes_frast") (vers "0.2.2") (hash "0rcj55whfh9ykf8nknkhqfyyrp420ifx88vc8vgrq61pcwchwrwz")))

