(define-module (crates-io ae ri) #:use-module (crates-io))

(define-public crate-aerial-0.1 (crate (name "aerial") (vers "0.1.0") (deps (list (crate-dep (name "html5ever") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.47") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tendril") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "13mv28i062y2n265bqc0rwnd7y903rz7c53jx729b43xfpszy38a")))

(define-public crate-aerith-0.1 (crate (name "aerith") (vers "0.1.0") (hash "088z55p88d6ibyjqbyq8gn0q4gpskf6ks50wv39g86sk2mv0fmsa")))

