(define-module (crates-io ae -r) #:use-module (crates-io))

(define-public crate-ae-renderable-0.1 (crate (name "ae-renderable") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "10cnyxlw247cmlr79xj7j8g4a2g6ijv105mms494ps4vxrn0al3h") (rust-version "1.65")))

