(define-module (crates-io ae -d) #:use-module (crates-io))

(define-public crate-ae-direction-0.1 (crate (name "ae-direction") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "1gjh8gpiadjpk4900bm0mcaj961av4fzqf5wxrzd1phjbqgygjnf") (rust-version "1.65")))

(define-public crate-ae-direction-0.1 (crate (name "ae-direction") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "1g7cknm31h3mfl64c408152a4xav4fja172bd0484cr82307mpd1") (rust-version "1.65")))

(define-public crate-ae-direction-0.1 (crate (name "ae-direction") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.90") (kind 0)) (crate-dep (name "typeshare") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "11i8dmxxzz6a1pdh6yk4fy2nzbyjky5821v3003f7smhhf7ww6ms") (rust-version "1.65")))

(define-public crate-ae-direction-0.1 (crate (name "ae-direction") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.90") (kind 0)) (crate-dep (name "typeshare") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "05lnl540f4yvrlnyvmmvx0jqcwpm3yfs92riqzk94dhp83ip95fv") (rust-version "1.65")))

(define-public crate-ae-direction-0.1 (crate (name "ae-direction") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0pfbynx8nghgysf8xwvfhrcff5x92wgshgp9gjx3aan0pxpqw8rw") (rust-version "1.65")))

