(define-module (crates-io ae -p) #:use-module (crates-io))

(define-public crate-ae-position-0.1 (crate (name "ae-position") (vers "0.1.1") (deps (list (crate-dep (name "ae-direction") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "161yqxc3cp69cplc7h30k1swnq82hkyh2ypi1pca87qd0jwa89bf") (rust-version "1.65")))

(define-public crate-ae-position-0.1 (crate (name "ae-position") (vers "0.1.2") (deps (list (crate-dep (name "ae-direction") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.90") (kind 0)) (crate-dep (name "typeshare") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0my9kx4nlw64jix96pb5arg3ig9xi37qww9p59sap0c11rxm89pz") (rust-version "1.65")))

(define-public crate-ae-position-0.1 (crate (name "ae-position") (vers "0.1.4") (deps (list (crate-dep (name "ae-direction") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "08almq1zc2fqna67vxssp7wqfy0njcbla9yfs4qz6nfcdlaf9s39") (rust-version "1.65")))

