(define-module (crates-io ae si) #:use-module (crates-io))

(define-public crate-aesid-0.0.0 (crate (name "aesid") (vers "0.0.0") (hash "0q6p2y3s0kpgfps97i36b58gggh7yzzg8xdszy7yl7hadj6gn32p")))

(define-public crate-aesir-0.1 (crate (name "aesir") (vers "0.1.0") (hash "1nfnvilqxzavlxlm82zfqhljsbww5qgvdrfh7ygvydsy883vcqwg")))

