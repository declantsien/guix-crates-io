(define-module (crates-io ae vu) #:use-module (crates-io))

(define-public crate-aevum-0.1 (crate (name "aevum") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "01m145azr22zib3rg4b577qp30nj5748l9lx14rvq5w71gzql410")))

(define-public crate-aevum-0.1 (crate (name "aevum") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "091h2xis2ii1k38fdlv472ll1liwznyxlm999sz6264l8wi0afzl")))

