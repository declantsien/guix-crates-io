(define-module (crates-io ae io) #:use-module (crates-io))

(define-public crate-aeiou-0.1 (crate (name "aeiou") (vers "0.1.0") (deps (list (crate-dep (name "aeiou-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "101wyawfz21ml2gp7hl74xqm9cqs4rxqp065frpxyfyj8fbzldv9") (features (quote (("derive" "aeiou-macros"))))))

(define-public crate-aeiou-macros-0.1 (crate (name "aeiou-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "1hn7nh0q3bzxws41qy5jjv4qkz5kqmpw9zah97wvnhxbwp6a8cy4")))

