(define-module (crates-io ae -l) #:use-module (crates-io))

(define-public crate-ae-library-template-0.1 (crate (name "ae-library-template") (vers "0.1.0") (hash "1daqbpxbfm4wm5xh6i3hlgk9dyz866vyvm1bwzm5wgv90gcl96fz")))

(define-public crate-ae-library-template-0.1 (crate (name "ae-library-template") (vers "0.1.1") (hash "1vmb1k2pdjmbcl4nx5xl57mdwi4fp6pln2msa8mpdmm4gjjic953")))

