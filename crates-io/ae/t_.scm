(define-module (crates-io ae t_) #:use-module (crates-io))

(define-public crate-aet_authentication-0.1 (crate (name "aet_authentication") (vers "0.1.0") (hash "1ffps683hq4b22vz1s5qgygrap5hjavi5ss8b2jkfahnf16y8kc1")))

(define-public crate-aet_authentication-0.1 (crate (name "aet_authentication") (vers "0.1.1") (hash "0x4pa4skvmj2b38caxia5il4qi7k0y1ps03p2si0cs4cdzpbqh7i")))

(define-public crate-aet_file_settings-0.1 (crate (name "aet_file_settings") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 0)))) (hash "0yx6q7a48ni03gynsnvwfqi5jbrh890ydqk3iphzhssivpanfk9k")))

(define-public crate-aet_file_settings-0.1 (crate (name "aet_file_settings") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 0)))) (hash "00rq5kp5yz27skpr44ln40b2mbv49lq8691gh8y67r0vnnfjxl4y")))

(define-public crate-aet_file_settings-0.1 (crate (name "aet_file_settings") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 0)))) (hash "1blm9iyb0xcdppby5bf6izj35bjf3k3aak61cd89h8byd9qqb140")))

(define-public crate-aet_file_settings-0.1 (crate (name "aet_file_settings") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*") (default-features #t) (kind 0)))) (hash "12bdavspj5a01ijf4h9r5gzgqmyh6z061ni2ziwc7q3m9x1pby94")))

