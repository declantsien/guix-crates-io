(define-module (crates-io uc hr) #:use-module (crates-io))

(define-public crate-uchr-0.0.1 (crate (name "uchr") (vers "0.0.1") (hash "1dfnb8g4apcvcca4zjr0j3sbasy2j563ssjqhvq37jx763b8z99f")))

(define-public crate-uchr-0.0.2 (crate (name "uchr") (vers "0.0.2") (hash "14ff8jzc4xmzf5b108sijkv98mydqg453r0xwphsq6yp39as4pi0")))

(define-public crate-uchrono-0.1 (crate (name "uchrono") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "00zmpdqnwk1d6l1jnajlxx1gplpmxvrj73cm1gq27k0nhdiakkn4") (features (quote (("std" "lazy_static") ("default" "std"))))))

(define-public crate-uchrono-0.1 (crate (name "uchrono") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jwghk5hp3hqgcprz9lnzz7asbbhf636211phhyvlakb8b6817hl") (features (quote (("std" "lazy_static") ("default" "std"))))))

