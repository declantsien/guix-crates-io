(define-module (crates-io uc l-) #:use-module (crates-io))

(define-public crate-ucl-sys-0.1 (crate (name "ucl-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "0dc3904achvksiyap0kv9k7fc2icn0f8dwd301pa52m6k7ykbrxl") (yanked #t) (links "ucl")))

(define-public crate-ucl-sys-0.1 (crate (name "ucl-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "1ijrzv73v83404axrzxvywq1h7dnbpjpw859s6mxx61ddxy4ag88") (links "ucl")))

