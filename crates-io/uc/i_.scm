(define-module (crates-io uc i_) #:use-module (crates-io))

(define-public crate-uci_rs-0.1 (crate (name "uci_rs") (vers "0.1.0") (hash "14fx830z23na7wvskvafp7792xal71hljy7ad5qkvav56rmqi4x0")))

(define-public crate-uci_rs-0.1 (crate (name "uci_rs") (vers "0.1.1") (hash "1xw3p1czx1ih27klijr4gnafgrp5y54vf571yd8za5i63wvwi3r5")))

(define-public crate-uci_rs-0.1 (crate (name "uci_rs") (vers "0.1.2") (deps (list (crate-dep (name "fastrand") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1wihhk2kic3m36n720n3a679wid3amcgm3nn5kk66x1ld12qiglg")))

