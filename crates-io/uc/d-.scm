(define-module (crates-io uc d-) #:use-module (crates-io))

(define-public crate-ucd-data-0.1 (crate (name "ucd-data") (vers "0.1.0") (deps (list (crate-dep (name "bzip3") (req "^0.2.3") (features (quote ("bundled"))) (default-features #t) (kind 1)))) (hash "1z7yjmaqb2pxf9g86b8a6j45mrp51y3iiv4zhghn9a36ww6b16lm")))

(define-public crate-ucd-generate-0.1 (crate (name "ucd-generate") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ucd-parse") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "07fl9jx1azprpl94rcsfkpiar90sdxlcq8dmp05wfx0hlxjmiyg8")))

(define-public crate-ucd-generate-0.1 (crate (name "ucd-generate") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ucd-parse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cfxj4hyws7lqaafaf0jwvxg6p1qpw95mvlva3pv9ajzaf7zg8f5")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ml913gkgp1npwmhm27jc8s6isyv3yljr0qglc3q1b1hcra9q4ds")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "123z87hwf1dh9479s9mmhs9wqir2amdyllwggiwgzaakbs716ar7")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0rq6cqx4514xdq0l9k41ij0vvdkrjlm9yprxhhsh6a652zw423lp")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0xkgpxmrxpxnf35gla6k5qfff3nf4hxw85za9p6bnrswiyw1fg22")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "14nb1s6lvwa3g0y9jpd8jnm4hwf34jmpxxfpqkli60bwimmrpv0q")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0b3d4nvfryyfv9pl3ipx01v22j21f4jj9vwjvgibj5822p033f4k") (yanked #t)))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.6") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.3") (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "19wilkwld2z8c3xjay4bcw3xszp0blckjw99kkjpd600v9jkij83")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.7") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0cgsiqajb2jb54rfh9g3hfkms15avahj6l0an3pvig1fcc3kk5dx")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.8") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0bpspyh9ybgr0rxrgsmsa4whxhn0911i0ixqcnmrdwvzq3dfd3xp")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.9") (deps (list (crate-dep (name "byteorder") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req ">=2.33.0, <3.0.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req ">=0.1.9, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req ">=0.1.8, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req ">=0.1.3, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req ">=0.1.8, <0.2.0") (default-features #t) (kind 0)))) (hash "16ks1pa1ndg8r81rvvp1l04azf5872cpc9wnjr1sfjrm2icg6z6j")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.10") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "065jgzxfd5cpfrcjp32gr770hs4zyq7jbdclnnwpr1s0v1n3ih1n")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.11") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0zsg7sfjyqqr7yymbwq1jzbk33gf9qm5axfmzim66hgy3p3ymrxr")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.12") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "19dz1i6h79ngsw5kj205agydy6k003rs8ms0mj0nnyiiqfmbf3z3")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.13") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0n468z8c38g14ay9mvgw2mdwn5q3qxx6fwkrpylgzblm7lbwb3pf")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.14") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0v8xdqsxfv8s6274hlwf3lfq4mhnfxcik4mwd83zhbvx8azqynkg")))

(define-public crate-ucd-generate-0.2 (crate (name "ucd-generate") (vers "0.2.15") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ucd-parse") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0mwy8ixnah1j1iiaqr5hvv7s47inv5mlna7ym8vc9fdaabiz8xmw")))

(define-public crate-ucd-generate-0.3 (crate (name "ucd-generate") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.34.0") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fst") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ucd-parse") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ucd-util") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "19g9lm7bzrirvxhawh7f2j278pjfqjmc9iy5v9p4grwmbmsx1xxj") (rust-version "1.70")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "033vkk4ypchn4a3hprgrzhgs5pa7iya9ynb3h3zh4bl8iypvhm05")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1z0pxkiycvgvzsn39a10yfbjjyswwrvmyd210khg1miwm3iyyj7z")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1l12ri4y7bly29xmm5k7jj3wrr1lb6xbjcnm1ldyc6hxlmh906i9")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "13mq6c85r6ak10gjlq74mzdhsi0g0vps2y73by420513gfnipm97")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1d06wcnzs1kqdc0wszlycclhvr92j9v08nkq0w7jyld69nzm4sya")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "unicode"))) (kind 0)))) (hash "10bncqsc9r3rskyp04i4z9zb3a2ycfjznpsp6ikgs75lbggks29r") (yanked #t)))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "unicode"))) (kind 0)))) (hash "08kyyj2hlchvis4bamd3mvw9vb769ga9vzkm22yl2vdx56rm7h04")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "unicode"))) (kind 0)))) (hash "0ms8rhkckwnmjlxz7k01sqxsywbc73wx2l2gc2slclbkpyi5jzjq")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "unicode"))) (kind 0)))) (hash "0z0vw78ynfjhylmf7bizl6yf82fg5rs6sfilb03vdf7nbp9zhsaj")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "unicode"))) (kind 0)))) (hash "18hb5jykk1lg7aaycjfivq7i3lwjlgxfbn9wz9kblsk07f1gi06y")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.10") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "unicode"))) (kind 0)))) (hash "0fr5c5fm488l17fcns00hazayax33f8305yf01awbx4qm5b0abgw")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.11") (deps (list (crate-dep (name "regex-lite") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0nvwjv9jc3842dmdda753rvzhwzin2k97576ln0zqjbml261y1ci") (rust-version "1.70")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.12") (deps (list (crate-dep (name "regex-lite") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0a7lampg020dzdgnh4bakk22zi86csg7klpfawpqrcapc5imjb11") (rust-version "1.70")))

(define-public crate-ucd-parse-0.1 (crate (name "ucd-parse") (vers "0.1.13") (deps (list (crate-dep (name "regex-lite") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fbryah32sswz34kfi61mvkmhq1kwgvia2v6q7s4vgzw488zhvy0") (rust-version "1.70")))

(define-public crate-ucd-raw-0.1 (crate (name "ucd-raw") (vers "0.1.0") (hash "1458a74pzaymlav200jfdlai40bn26ahf10214qazjjhbm8nmx63")))

(define-public crate-ucd-raw-0.2 (crate (name "ucd-raw") (vers "0.2.0") (hash "1amk0k0v1h6ahqgnjs6123ncyl5szjcx8qg7fa48c0mvh60ncywv")))

(define-public crate-ucd-raw-0.3 (crate (name "ucd-raw") (vers "0.3.0") (hash "0i736x54zhb0dhgcdqz6hffysxznpj8m346n7wky9bx0vcgd2xii")))

(define-public crate-ucd-raw-0.4 (crate (name "ucd-raw") (vers "0.4.0") (hash "10rxai44gzilm8s7k1rq0nn13i3ggnvvyclqhsizmmxy6f1xwq9y")))

(define-public crate-ucd-raw-0.5 (crate (name "ucd-raw") (vers "0.5.0") (hash "07z0v28vff0irqgqj1ikagv5nvfrdl8n21xvl7pyj6ix6xh4yax4")))

(define-public crate-ucd-trie-0.1 (crate (name "ucd-trie") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0v2ihmj4zds8bl4404qqginc6bkjh7z0mj9vaa10vldjmqbd14pk") (features (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1 (crate (name "ucd-trie") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0xwxkg0fyclbz8fl99iidq4gaw2jjngf8c6c8kqnqhkpzsqwbabi") (features (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1 (crate (name "ucd-trie") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1hh6kyzh5xygwy96wfmsf8v8czlzhps2lgbcyhj1xzy1w1xys04g") (features (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1 (crate (name "ucd-trie") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "072cblf8v3wzyaz3lhbpzgil4s03dpzg1ppy3gqx2l4v622y3pjn") (features (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1 (crate (name "ucd-trie") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "070lb1h3abm00zvrj4wa4gls8zwzx53sp2iq5gg8amgyqjchamw9") (features (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1 (crate (name "ucd-trie") (vers "0.1.5") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)))) (hash "10ggllapxq99cxxy179wbklmabj5fikm02233v4idf7djvcw8ycy") (features (quote (("std") ("default" "std"))))))

(define-public crate-ucd-trie-0.1 (crate (name "ucd-trie") (vers "0.1.6") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)))) (hash "1ff4yfksirqs37ybin9aw71aa5gva00hw7jdxbw8w668zy964r7d") (features (quote (("std") ("default" "std"))))))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.0") (hash "07x3vgdvgnlwq3gh4r9fbp30lyslcbyqgy92midw97ya4xz5dj9s")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.1") (hash "0p98v8mqksf3sa4jfvj6imajjdp2ava1lafsrpk8y3wxcgbf4azx")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.2") (hash "0i14sn3k88d693cyblykrvli6p0lbwn9vb9904hwvb8czylvzy6h")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.3") (hash "11lgx380zgqsm265cg78w2mcjpmldbwbi01lb5w48hyqwi720p2k")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.4") (hash "1r1mldava1zzy4yy765qs6pv5cmv813lvaa71665fsyzy76zafwg")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.5") (hash "0x088q5z0m09a2jqcfgsnq955y8syn1mgn35cl78qinkxm4kp6zs")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.6") (hash "0y1m6n94c3apdh95bnfl6jny30khlqb1mld1fa25zlnmgqkvm4k2") (yanked #t)))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.7") (hash "13ng291mkc9b132jjf4laj76f5nqm5qd2447rm8bry3wxbdc5kaw")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.8") (hash "0dnjzvcb3q5b69w214wvsmdz2b08jmkxfp0ykckqqd2x15752py8")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.9") (hash "1z4v3hhbbqjwszj300wpz5fy5hn4zdr63fxi1v0z48mi27vcpgv5")))

(define-public crate-ucd-util-0.1 (crate (name "ucd-util") (vers "0.1.10") (hash "00z0if2q1zlwkbaymfyhxmas1v1jgy1hv8mhz156345m69fzrlmb")))

(define-public crate-ucd-util-0.2 (crate (name "ucd-util") (vers "0.2.0") (hash "1hvzw355chhyzklk8dv2h2xp4zcxzl52dpks7mjarclz6zv7wll6")))

(define-public crate-ucd-util-0.2 (crate (name "ucd-util") (vers "0.2.1") (hash "10qr4vgah6pbvv9hskiysrvcm9pvzfh01dwnig5r1a9r7x9s61sy")))

