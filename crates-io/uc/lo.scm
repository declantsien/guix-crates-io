(define-module (crates-io uc lo) #:use-module (crates-io))

(define-public crate-ucloud-cdn-log-parser-0.1 (crate (name "ucloud-cdn-log-parser") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1vhahqhl52a0g6891yxigwrc4wfyxh3gch4drlvm1rihj0rgpsw7")))

(define-public crate-ucloud-cdn-log-parser-0.1 (crate (name "ucloud-cdn-log-parser") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "15m33ibxr68abvg44dj0qr5v0q0k1svbifx7fj4wkj9s1bihwcn6")))

(define-public crate-ucloud-cdn-log-parser-0.1 (crate (name "ucloud-cdn-log-parser") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0dalbki6xicm7md0b0552psidvphkv78mld53rymfwzzsq8lalq3")))

(define-public crate-ucloud-cdn-log-parser-0.1 (crate (name "ucloud-cdn-log-parser") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17j7wbigksi05619y0nhq8n9aq6hv8dq0xvz7754w3nz075w1zv9")))

