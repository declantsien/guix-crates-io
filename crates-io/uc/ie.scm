(define-module (crates-io uc ie) #:use-module (crates-io))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yqb2974wac0mvcwsnlhr518hdhq07b7ig8rd42gvh5m3vkysqkw")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v86bd3wani4wm1af203fs8xqycgclix04xgw6y28m7h3zwdzzhz")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0swscdvd6df2lr7f7khgdm907bnzk7cnba38985ramwv4mlqjq3g")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.3") (deps (list (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ff3c98bgg6dgrmcxqimfwhcn84awji88al3wc4vgv2gz2qk5x14")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.4") (deps (list (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0q75j1gj92jnq0lzn6bb1r1qqxzj8q9dni9b9ky2bgin5w892wnb")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.5") (deps (list (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12ndircvrh0siin44jkb4rbw57bbyb6ifb7vg9an3lhc459wdmhr")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.6") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0r5b1w2dw8pp00mgsfdnp1zz9dymf3a2r2vsj3d6z22s89p0dnzp")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.7") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wlsv10w0wqj8i60fpyd33l8q4czrbmxiajrfjfyrkqhi304lmff")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.8") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0m72c7z7bn4cm49jf641j3xbm5fsz1pjx44izbrizi4mf5aaaz8g")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.9") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1sz934jdk0fk6wdi5lcfxqfm906y1i50k14r8fqc88riqfm8f6k0")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.10") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07bkan1qvzdqnhk75clqzpw7a239zff88iy955xm67jzhdrvfxpk")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.11") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "042zppss9wgvrd9y2df81ky1gjbqdyvk0nz9bp5zapif7zfva1gf")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.12") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bcpi6jsz4gnbbgywh49p2wz2kpgb9irj1pjkay24bkvdb0mdarf")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.13") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rk77w44pslmlvjc9gi07185n3ri8wxgcd4j2mm4iq9hmp28n663")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.14") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wcc22cmygzpmwbkbmyl1afq1gdrwxbs6yv4gw7g1kld2wc9rh52")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.15") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cj5kan7kimi6bbd82h7mxajcwass70bn3qd28slvdgng342g3sj")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.16") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15ikm047cgp8pwkhwzbdisi23k66b1ajsiviaagp45fzwj2gj2v8")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.17") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1x7mnh9qdrvjyr3ii2y51l1n6mzp2hbz0qhbkawxq3zcz16lfzxg")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.18") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05qq6lc122qqkw0khxh2ckm3mrcazb8zj3k0jax892cv44yhij14")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.19") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xzwx8wbs85g4k6pyzinidya9arrc2g18ycdk4b2dhr40f8szh2m")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.20") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05zaxmnw8hhww59lxpm2wgwkv5phw6kb0fpadjgjqr5j37n74vyj")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.21") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pqak2n11y34iy86kai9f8pby1py8fk85x8h6vzdy4wmwnzcy9f5")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.22") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xnhdykvsvz6hafys6dvxdqa5a409xy1ams8x9y4f1qpp4v8wkrc")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.23") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yzwjsrsgk48rfzzb06sp0vxpq4cnmxhw3nb4k38lxxmf4w53f8s")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.24") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18yfwwrhwllxfcjhvsrgvbgd815vfpwi27638zdq69287mlxqjwf")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.25") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yzmyws5638x55bfwkhryg59s5al6zzfxvimx80fxgbdn86i0qb8")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.26") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mr185i3rgkbxwk82x0i029lhr86zbg47829n341920q7zd64bar")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.27") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yd8xbsm56gpdy62nvk58d22dz5lcsy0zaa6xx7dmki24a50jxgi")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.28") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "020nfbn3zvk1ws76zs8xnazx34lssnfb4qvcbbfwh77rx9pf6bm1")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.30") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1109gwri4c3qlmczj12129kfhqfwvspiwwhx1m4hvsl1rvm40q34")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.31") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vhw3jz2y2zmzy7rr10vwvgf72cxcr5p6b9h7wjgclj658zzzvkr")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.32") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jzgkqlcz6sn5jl16wdd9hplnvcfhh6r5ih1m56zy8vmdspx3h89")))

(define-public crate-uciengine-0.1 (crate (name "uciengine") (vers "0.1.33") (deps (list (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "envor") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13swv0dlb7inahf1b9pyb2wwcj3rp81nq95ygqy1fj034jrw9ys0")))

