(define-module (crates-io uc hi) #:use-module (crates-io))

(define-public crate-uchimizu-0.0.1 (crate (name "uchimizu") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "06gpd2x6sbw5bnmdymxj4ax3fyqvmjpl188wqdziiqk24w2vvb94") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde" "dep:chrono"))))))

