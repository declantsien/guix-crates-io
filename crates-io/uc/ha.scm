(define-module (crates-io uc ha) #:use-module (crates-io))

(define-public crate-uchan-0.0.1 (crate (name "uchan") (vers "0.0.1") (hash "1hfzf9x2fy3pmi82f081nlwirsridc2bl7crjlrn62dxc6y5fm8f")))

(define-public crate-uchan-0.1 (crate (name "uchan") (vers "0.1.0") (deps (list (crate-dep (name "cache-padded") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "05lmvznvmw3q4m759234kxam608fc7yy67ak74jxc17lzf47vlns") (features (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1 (crate (name "uchan") (vers "0.1.1") (deps (list (crate-dep (name "cache-padded") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "07gh7cjgp596ci5kvvkpzskwjy4w2cnbshvqa1f4i05nmzwcp7bd") (features (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1 (crate (name "uchan") (vers "0.1.2") (deps (list (crate-dep (name "cache-padded") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0x5x9hr4967gkvf64csyf81czl2sj2y8q0q2dxqfn5ly2a50n2d7") (features (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1 (crate (name "uchan") (vers "0.1.3") (deps (list (crate-dep (name "cache-padded") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0ml2n56zdcigp9zshg5lb5fr2z44m1rj32qxp3aiprip2wq21zn6") (features (quote (("std") ("default" "std"))))))

(define-public crate-uchan-0.1 (crate (name "uchan") (vers "0.1.4") (deps (list (crate-dep (name "cache-padded") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "sptr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1p0rbskc18famzy6n7g0i8mpw0c4ibbd1a3a18g6nzcpjmxs0iah") (features (quote (("std") ("default" "std"))))))

(define-public crate-uchardet-0.0.1 (crate (name "uchardet") (vers "0.0.1") (deps (list (crate-dep (name "uchardet-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1n5f2sxvm8xjd4pxfacfw4nx06x7l9fkk3w1z91gax158fw126qb") (yanked #t)))

(define-public crate-uchardet-0.0.2 (crate (name "uchardet") (vers "0.0.2") (deps (list (crate-dep (name "uchardet-sys") (req "= 0.0.2") (default-features #t) (kind 0)))) (hash "1rm7dcqk228v4dygck95vd7g5la82jai28cgx7yskypl2wm9ahxy") (yanked #t)))

(define-public crate-uchardet-0.0.3 (crate (name "uchardet") (vers "0.0.3") (deps (list (crate-dep (name "uchardet-sys") (req "~0.0.2") (default-features #t) (kind 0)))) (hash "09r7x3d03qv5gq1bslljil55vi9zs4ndm9fbngnzg5hill4ilavj") (yanked #t)))

(define-public crate-uchardet-0.0.4 (crate (name "uchardet") (vers "0.0.4") (deps (list (crate-dep (name "uchardet-sys") (req "~0.0.2") (default-features #t) (kind 0)))) (hash "1q81lqn0cn5n9vkdlvwl88ncclrxpga9nmgwh3l57wpwidfvkkki")))

(define-public crate-uchardet-0.0.5 (crate (name "uchardet") (vers "0.0.5") (deps (list (crate-dep (name "uchardet-sys") (req "~0.0.2") (default-features #t) (kind 0)))) (hash "14ghb1y1yin251mmirsvybi3l6d6apgmi4vxkk6hyjnhpmdk3fdp") (yanked #t)))

(define-public crate-uchardet-0.0.6 (crate (name "uchardet") (vers "0.0.6") (deps (list (crate-dep (name "uchardet-sys") (req "~0.0.2") (default-features #t) (kind 0)))) (hash "0gnlk02bnhmxr29wcfrlqyfnbfnw6qyrvgxbvzcdb79wn1cla6vr")))

(define-public crate-uchardet-0.0.7 (crate (name "uchardet") (vers "0.0.7") (deps (list (crate-dep (name "uchardet-sys") (req "~0.0.2") (default-features #t) (kind 0)))) (hash "0mwak3y3ka47kpk5frd5f6mg5gi30s04998dj2l8ijhw4gifhxf6")))

(define-public crate-uchardet-0.0.8 (crate (name "uchardet") (vers "0.0.8") (deps (list (crate-dep (name "uchardet-sys") (req "*") (default-features #t) (kind 0)))) (hash "0ici1aq8a560bwbrfdnd7rc1bl69aya4bvqbbzm4c7dk0jgfr09i")))

(define-public crate-uchardet-0.0.9 (crate (name "uchardet") (vers "0.0.9") (deps (list (crate-dep (name "uchardet-sys") (req "*") (default-features #t) (kind 0)))) (hash "13z11vg26f1sn1vk84x6nnwi2qnls452asqhj9w5q9vjc1svw4ba")))

(define-public crate-uchardet-0.0.11 (crate (name "uchardet") (vers "0.0.11") (deps (list (crate-dep (name "uchardet-sys") (req "*") (default-features #t) (kind 0)))) (hash "012i8i91xcxl9aqiwvr03cfn83xl6cxy8q0395vs54j56shm3k0p")))

(define-public crate-uchardet-0.0.12 (crate (name "uchardet") (vers "0.0.12") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "*") (default-features #t) (kind 0)))) (hash "12w2sdanlgp1pws2q8iy75l32x6chslzkgswidvcnzx31qc0mf76")))

(define-public crate-uchardet-1 (crate (name "uchardet") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0lfc318v73xpdrpfxa9wwb4ldkab77vv16mkrg5d6n38xlzrmdc1") (features (quote (("unstable"))))))

(define-public crate-uchardet-2 (crate (name "uchardet") (vers "2.0.0-pre.1") (deps (list (crate-dep (name "error-chain") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "^2.0.0-pre.1") (default-features #t) (kind 0)))) (hash "1avf75m38fs5qyn96275xfz4dnfnny9m2x9ljrh4ag3kp6xyzv8y") (features (quote (("unstable"))))))

(define-public crate-uchardet-2 (crate (name "uchardet") (vers "2.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1dvw89lqf30ard0n070am35zas9plyv6gmyz8m87jxghqw7zvzdp") (features (quote (("unstable"))))))

(define-public crate-uchardet-2 (crate (name "uchardet") (vers "2.0.1") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "074arzqy0dj87rpf968gyfawca64mvp3nw4j4yq6i3557gpckcnl") (features (quote (("unstable"))))))

(define-public crate-uchardet-2 (crate (name "uchardet") (vers "2.0.2") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "174l2s3b0ll8iq5d3cbc39pppj9yj7ah46i9ymdynar99kdx15m4") (features (quote (("unstable"))))))

(define-public crate-uchardet-2 (crate (name "uchardet") (vers "2.0.3") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "1rmrh63kjr5aykr5i8c31ny2cmalap1045w1v39gk93mbs8xd8vx") (features (quote (("unstable"))))))

(define-public crate-uchardet-2 (crate (name "uchardet") (vers "2.0.4") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "uchardet-sys") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "1m6li17239s8p7s6rsvsjim3gbzrklrdsc8hglqr5s1hn517gwrv") (features (quote (("unstable"))))))

(define-public crate-uchardet-sys-0.0.1 (crate (name "uchardet-sys") (vers "0.0.1") (deps (list (crate-dep (name "pkg-config") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "001znvi4qba1hkwmrz3blgsqrp7k7r92f9r5p5rsz9vmlbp30rh5")))

(define-public crate-uchardet-sys-0.0.2 (crate (name "uchardet-sys") (vers "0.0.2") (deps (list (crate-dep (name "pkg-config") (req "~0.0.1") (default-features #t) (kind 0)))) (hash "1m1vhyfzyqdik0lmnilx5q23ngrfyz1lfynjpxsq031bvk75flbq")))

(define-public crate-uchardet-sys-0.0.7 (crate (name "uchardet-sys") (vers "0.0.7") (deps (list (crate-dep (name "pkg-config") (req "~0.0.1") (default-features #t) (kind 1)))) (hash "1pd0w4wp889m25hxc95lrqixpw9r0i6y4bsn9b4bqry3h22hvpfw")))

(define-public crate-uchardet-sys-0.0.9 (crate (name "uchardet-sys") (vers "0.0.9") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1jzqrsik0640cgvddx908acy3w4mqdplj23lz3wliw1g61qsrwmw")))

(define-public crate-uchardet-sys-0.0.10 (crate (name "uchardet-sys") (vers "0.0.10") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "0ma475jbzja29xqaqiaym21fgp57nzg5d61269gispcr7yr9fcqz")))

(define-public crate-uchardet-sys-0.0.12 (crate (name "uchardet-sys") (vers "0.0.12") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "00j2rwqf0ibd30m5yjj0ryrvylb1sd8pr8xykf3b71xiccfcprfw")))

(define-public crate-uchardet-sys-1 (crate (name "uchardet-sys") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1d1q9zcy1c2fa2nb4mq4a9acy0jpivywhdhdli5bckr151a5nvch")))

(define-public crate-uchardet-sys-2 (crate (name "uchardet-sys") (vers "2.0.0-pre.1") (deps (list (crate-dep (name "cmake") (req "^0.1.18") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "15dimn6x0yzm9nhqvv7djkx71yhn45yy5bgn3fhihjagbwy6xpkq")))

(define-public crate-uchardet-sys-2 (crate (name "uchardet-sys") (vers "2.0.0") (deps (list (crate-dep (name "cmake") (req "^0.1.18") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "0spiw53cp67v6akvyb6330bzpjvgxhknqbk35hhyih4cgcm6q6gc")))

(define-public crate-uchardet-sys-2 (crate (name "uchardet-sys") (vers "2.0.2") (deps (list (crate-dep (name "cmake") (req "^0.1.18") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "18b869gbwjb2lnv7nz3ch9280p1p58yrz1895wyz3byf1lbf816r")))

(define-public crate-uchardet-sys-2 (crate (name "uchardet-sys") (vers "2.0.3") (deps (list (crate-dep (name "cmake") (req "^0.1.18") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "1w05sm09xdk7bx21x98wc6d3w7052l5khbf0qyd2ksrh27mz3kw6")))

