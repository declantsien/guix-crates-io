(define-module (crates-io uc s2) #:use-module (crates-io))

(define-public crate-ucs2-0.1 (crate (name "ucs2") (vers "0.1.0") (hash "1q42vx91wxgnrcn6mxjmd3z1fza4xpijmxz7157721sb361k8n4f")))

(define-public crate-ucs2-0.1 (crate (name "ucs2") (vers "0.1.1") (hash "1c861iav1gv03g70yw86alwj1mmdcb5wpzcv301k6a96f465qy65") (yanked #t)))

(define-public crate-ucs2-0.2 (crate (name "ucs2") (vers "0.2.0") (hash "0r6a39j485fkk88k3k1iz4byfzg2k40gg47gfmz3874a3kb6cxai")))

(define-public crate-ucs2-0.3 (crate (name "ucs2") (vers "0.3.1") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "12nfqiqa4wagkiqp696g2f3dmy13pwjpgf561ly62njl8d71y1l5")))

(define-public crate-ucs2-0.3 (crate (name "ucs2") (vers "0.3.2") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "1f4slwl2s410ml3hkrrcxhb56l32jjwbmdj1ar3ps4wl828l7mms")))

(define-public crate-ucs2-0.3 (crate (name "ucs2") (vers "0.3.3") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "1fkyrqsklkrxrmapjs3p8x6krb195hy8y9pcaw6405pk2672jyfz") (rust-version "1.56")))

