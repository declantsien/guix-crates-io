(define-module (crates-io uc on) #:use-module (crates-io))

(define-public crate-ucontext-0.0.1 (crate (name "ucontext") (vers "0.0.1") (hash "0la5241dnr5l4cvba655wswr8jxdb0azy7bi3ddb4ksvwpnbq2sn") (yanked #t)))

(define-public crate-ucontext-0.0.2 (crate (name "ucontext") (vers "0.0.2") (hash "0f9jmmsh7dvdxmbbgm050a7cxsqcn9z7j9fsjn5ssp3gwdi2imf6") (yanked #t)))

(define-public crate-ucontext-0.0.3 (crate (name "ucontext") (vers "0.0.3") (hash "1aa8sq0p5vkakmv5ri46ahcym378h3wc78zl1dbjadhxalcwqswh") (yanked #t)))

(define-public crate-ucontext-0.0.4 (crate (name "ucontext") (vers "0.0.4") (hash "0v70gydx3gdxnmv4bf5v00787wxnpyql5g9ynrjh0a6k7zi88nx4") (yanked #t)))

(define-public crate-ucontext-0.0.5 (crate (name "ucontext") (vers "0.0.5") (hash "1mq0dk6rhpf61fbfi5wkgi7vkbih7aklk4kgj0n6nj7wcdiply6b")))

