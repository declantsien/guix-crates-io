(define-module (crates-io uc re) #:use-module (crates-io))

(define-public crate-ucred-0.0.1 (crate (name "ucred") (vers "0.0.1") (hash "0i771ynwqwrsl2dkd3hvydn8rqglpp8s9qzrbikc6jhqlr359jkg") (yanked #t)))

(define-public crate-ucred-0.2 (crate (name "ucred") (vers "0.2.0") (hash "1imwqhi4l3r7zl2gx6dxkqr0wyb07xn91g73i1wm2rkc7yijk7rh") (yanked #t)))

(define-public crate-ucred-1 (crate (name "ucred") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1993mg3mbk1n1fikc9ir0xl4266gq2nqvkk6ijc9gjmqd2f44f62")))

(define-public crate-ucred-1 (crate (name "ucred") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14lzr6livq0pc128giqm6jr2gm4mfaqb5x8fnqih841d0ybblijs")))

(define-public crate-ucred-1 (crate (name "ucred") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yimjnrzsj674ca723j24ab3mbkf4p0dysgznv9srb7bzly78525")))

