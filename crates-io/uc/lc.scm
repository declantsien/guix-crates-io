(define-module (crates-io uc lc) #:use-module (crates-io))

(define-public crate-uclcli-0.1 (crate (name "uclcli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1n49qx8kvcfjh97ha3qvi2311zrnl6m5apzb5gfpvyq7s6dy02gb") (links "ucl")))

