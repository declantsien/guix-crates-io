(define-module (crates-io uc la) #:use-module (crates-io))

(define-public crate-uclanr-1 (crate (name "uclanr") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1hdg9i1rl4lxn7wm1vf18s1pmzb2ng4vgshpdpf474zbhzf46fz3")))

(define-public crate-uclanr-2 (crate (name "uclanr") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1v604qrd2gc493arzg79dynbzf15vd9v7ia83w0pfh9xw7w939bs")))

(define-public crate-uclanr-2 (crate (name "uclanr") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^4.3.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vhzdr9ylxwrmrbzmjv6n8kgp1z1fs2hysi8bglmdsd01b4nbz3b")))

(define-public crate-uclanr-2 (crate (name "uclanr") (vers "2.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1n3kbj4afl4p5agg378gxr4y1kmdfk2yhhccr1sq6gvxzd7xsxcm")))

