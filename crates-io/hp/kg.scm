(define-module (crates-io hp kg) #:use-module (crates-io))

(define-public crate-hpkg-0.0.1 (crate (name "hpkg") (vers "0.0.1") (hash "195bk3dq6z5jxiz95ygharq85wdfjikk2j07r3x529m8ga58bh1v")))

(define-public crate-hpkg-0.0.2 (crate (name "hpkg") (vers "0.0.2") (hash "0bw43p46k640iir3s1i9ld06scy0gppwz8470hmf2zfbsppbqxra")))

