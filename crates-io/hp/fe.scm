(define-module (crates-io hp fe) #:use-module (crates-io))

(define-public crate-hpfeeds-0.1 (crate (name "hpfeeds") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "1dgrbhsj1l2izmcybm8ccb30df4xj33db8czgd156lxcnja6jk65")))

