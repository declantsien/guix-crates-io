(define-module (crates-io hp s_) #:use-module (crates-io))

(define-public crate-hps_decode-0.1 (crate (name "hps_decode") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "193ranx0pf3x58mcnbfgrn66nskmk7rbnw5fmsh30sj3xni707yr")))

(define-public crate-hps_decode-0.1 (crate (name "hps_decode") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0ivjbj7axir2bcwssbmfdybxvcqs5syrw9vmk3kax8qav15my9is")))

(define-public crate-hps_decode-0.2 (crate (name "hps_decode") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1v16svy97ld22f7mhqx8ivxysijdlkrx1m0c558sqcczjz50s1b4") (v 2) (features2 (quote (("rodio-source" "dep:rodio"))))))

(define-public crate-hps_decode-0.2 (crate (name "hps_decode") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1wa3k4skihk2kq0n6z2402j7p2rlgkqsyhbrj9c46ccmj3i4vr9v") (v 2) (features2 (quote (("rodio-source" "dep:rodio"))))))

