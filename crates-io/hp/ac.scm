(define-module (crates-io hp ac) #:use-module (crates-io))

(define-public crate-hpack-0.0.1 (crate (name "hpack") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1p0qf62llk8lb6j1vpn27pxgyiyv8s3wsnbmarl1c4jdapnigyrb") (features (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.0.2 (crate (name "hpack") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0n84hzw2klksrzi1r7qxcw695ipsppq9s9y754kxdqzmv7l041kn") (features (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.0.3 (crate (name "hpack") (vers "0.0.3") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1bn7xskgi2zgn37ypfqdqd0gvd62lz30j65485ika6c5najp1s93") (features (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.1 (crate (name "hpack") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0afxg4fg0mdz4x6lklhc97v8qa806kwh6rsw93bzxqhlrg66i6cn") (features (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.2 (crate (name "hpack") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0n1v6icq4x2jh0fyf6p43ljrmzpsxaw120bhkmnl1xjclg9sfb9x") (features (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.3 (crate (name "hpack") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1njdhpcax2k20vwrd7pnmrgar4cd4nskbc07s9yq28xda09zcs0w") (features (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-patched-0.3 (crate (name "hpack-patched") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1s582f2bws8b7bzzjag87qbnvkqdqjr6l3wn5bviqnbgmklnipc3") (features (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack_codec-0.1 (crate (name "hpack_codec") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gblnx7sh6h50ac94vkz50yyx5gx8nmr4fxp43x22l6sjkji59m7")))

(define-public crate-hpack_codec-0.1 (crate (name "hpack_codec") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "00il50dswihl8sfgnj8k9x2ixb7mhb1jdfysw7mi80fciq0zqq81")))

