(define-module (crates-io hp n-) #:use-module (crates-io))

(define-public crate-hpn-client-0.1 (crate (name "hpn-client") (vers "0.1.0") (deps (list (crate-dep (name "hpn") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1zbh0j3jrvcmyq82a0hqfhanc5lqnp62w013xc3mw19jndxg9y3l")))

(define-public crate-hpn-client-0.1 (crate (name "hpn-client") (vers "0.1.1") (deps (list (crate-dep (name "hpn") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1cxjfqmfhr9z5iskgc09v3kjpqbj7ixzkfir7ir82rb2kcdrhb1f")))

(define-public crate-hpn-client-0.1 (crate (name "hpn-client") (vers "0.1.2") (hash "1vdmavznlgfhh9jrbk9xmr59jlw678ypixgz4zfv32fw2mhz6vfq")))

(define-public crate-hpn-client-0.1 (crate (name "hpn-client") (vers "0.1.3") (hash "1kn2kl66s8jfffmz9mpl60k13ngcp0g01fq5mzh16h3zhvxaxnb0")))

