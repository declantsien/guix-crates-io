(define-module (crates-io hp m-) #:use-module (crates-io))

(define-public crate-hpm-hal-0.0.0 (crate (name "hpm-hal") (vers "0.0.0") (hash "0zvrbqy9i34k764rd07ahlw5in5scwqg6w5kwxilvdxs3x4cwslc")))

(define-public crate-hpm-metapac-0.0.0 (crate (name "hpm-metapac") (vers "0.0.0") (hash "021s8g9a84wlxa4cqfr2l5y7bn1pcrfbl4psb37c9r8rw4as53lc")))

(define-public crate-hpm-rt-0.1 (crate (name "hpm-rt") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "r0") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "riscv-rt-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14i0qmka2ic65pqbyk9hg4ak8xj7gx2dvfsqfyxck7d5fsdxlphr") (rust-version "1.59")))

