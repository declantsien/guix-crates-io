(define-module (crates-io hp nc) #:use-module (crates-io))

(define-public crate-hpnc-0.1 (crate (name "hpnc") (vers "0.1.2") (deps (list (crate-dep (name "hpn") (req "^0.6") (default-features #t) (kind 0)))) (hash "12bf76x4sifv7j605yqwwlpinl0b5k8hhhg91ss7q927ih12v71m")))

