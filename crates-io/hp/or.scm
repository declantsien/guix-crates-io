(define-module (crates-io hp or) #:use-module (crates-io))

(define-public crate-hporecord-0.0.1 (crate (name "hporecord") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1c87jxpzib1wrrq15fs2z706y30a9p5m7ks2nilz8vb1cw96a5zr")))

(define-public crate-hporecord-0.0.2 (crate (name "hporecord") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "10krg5sn0cpzvscd445l8mzyw96zv1p2p3bsmn5p3ymf5saryd74")))

(define-public crate-hporecord-0.0.3 (crate (name "hporecord") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0grwvljaybz3rxj4w40kgis7gap2rid7ir9zvbic70l029pr80z2")))

