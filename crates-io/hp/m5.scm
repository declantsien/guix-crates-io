(define-module (crates-io hp m5) #:use-module (crates-io))

(define-public crate-hpm5361-pac-0.0.1 (crate (name "hpm5361-pac") (vers "0.0.1") (deps (list (crate-dep (name "critical-section") (req "^1.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "03j1fqliw3j4frxyick25y0kgfnbxcb4rfnbdqrg1hjjjsm23zhp") (features (quote (("rt" "critical-section"))))))

(define-public crate-hpm5361-pac-0.0.2 (crate (name "hpm5361-pac") (vers "0.0.2") (deps (list (crate-dep (name "critical-section") (req "^1.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1ns8b6zlflyp2483x93xb0ml5ia0qrr6f4riqqkmkw0nf6ilbqv1") (features (quote (("rt" "critical-section"))))))

(define-public crate-hpm5361-pac-0.0.3 (crate (name "hpm5361-pac") (vers "0.0.3") (deps (list (crate-dep (name "critical-section") (req "^1.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0838fr7rczvc8lra28zyg05k07khyql4s8iszg8sqpa8z1mxrlyb") (features (quote (("rt" "critical-section"))))))

