(define-module (crates-io hp gl) #:use-module (crates-io))

(define-public crate-hpgl-0.1 (crate (name "hpgl") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "13wkbg8a9qy84jyxyhmpcy0d3fczj6y0ildz3m2bsbjfga1qalya")))

