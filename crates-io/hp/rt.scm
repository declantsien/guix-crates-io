(define-module (crates-io hp rt) #:use-module (crates-io))

(define-public crate-hprtree-0.1 (crate (name "hprtree") (vers "0.1.0") (hash "12xaggz1qwd8lazs989qgsr0srgmrqz0iasbcjpvyl69l1r7cbpy")))

(define-public crate-hprtree-0.2 (crate (name "hprtree") (vers "0.2.0") (hash "0ib9km4mgrldyfrk0fdm8gnmcraph62j8vg6h5hcmm2rzgp1ll34")))

(define-public crate-hprtree-0.2 (crate (name "hprtree") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "114cqqrsvdpq1f21kl84hcvybkfjp7z2179ql3lizprz30f6f39m")))

