(define-module (crates-io ce se) #:use-module (crates-io))

(define-public crate-CESE4015_Concurrency_RJN_JVL-0.1 (crate (name "CESE4015_Concurrency_RJN_JVL") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "095zxr5jzzbrmxfrpadwakmjzjv8m4vx362yfkspynz42amcsl2f")))

