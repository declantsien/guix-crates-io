(define-module (crates-io ce pa) #:use-module (crates-io))

(define-public crate-cepack-0.1 (crate (name "cepack") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pelite") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.2") (default-features #t) (kind 0)))) (hash "0vjcfqlh1crx151f4h33g33wqvdjg6rddprvagk5a36lm6rb48mg")))

