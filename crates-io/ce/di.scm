(define-module (crates-io ce di) #:use-module (crates-io))

(define-public crate-cedict-0.1 (crate (name "cedict") (vers "0.1.0") (hash "1n66bamysb46dy5qy0k2ybnw16cbsdnxv2nfxcxzb4509x71dmnw")))

(define-public crate-cedict-0.1 (crate (name "cedict") (vers "0.1.1") (hash "04714prwgpq7akc2acqjfdbfvyb4cwrx87a6jag4ig2i7k03v983")))

(define-public crate-cedict-0.1 (crate (name "cedict") (vers "0.1.2") (hash "0jmg2659cpir40vjpqs4zvns3w79h7kx2mgxrwc3m6aqsk6z7n2x")))

(define-public crate-cedict-0.1 (crate (name "cedict") (vers "0.1.3") (hash "14cx6rrg2b3rszxyzzr16kl2iwc22alc8vnvfyn671gk6zbd7qa0")))

(define-public crate-cedict-0.2 (crate (name "cedict") (vers "0.2.0") (hash "183r1sfqzwqyrygxzb0pd0a5cfsf879ngv5hd5v1z9gq76b96mpz")))

(define-public crate-cedict-0.2 (crate (name "cedict") (vers "0.2.1") (hash "076sd3m8h17g9bavhca1jx38msv8dlr37c6i1nay0xwdrfz5890p")))

(define-public crate-cedict-0.2 (crate (name "cedict") (vers "0.2.2") (hash "0a9f3sd0kgx7rb3hz2a4nxz70slwiwcwssshmav1n4dbh4ahn0pr")))

(define-public crate-cedict-0.2 (crate (name "cedict") (vers "0.2.3") (hash "12slcq7r7rmbn6s7wyjk4nwfa65xxrb133h7qhlp1py6wqa7djsd")))

(define-public crate-cedict-0.3 (crate (name "cedict") (vers "0.3.0") (hash "07yba9axj95283p17212y3fy5q8im0k04fa8cda8j2rfi48rqpjh")))

(define-public crate-cedict-0.3 (crate (name "cedict") (vers "0.3.1") (hash "009k3666kazy3859nl4adh0x34scxfpqqgc97nhh1acj9ax2l1m5")))

