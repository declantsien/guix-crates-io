(define-module (crates-io ce a6) #:use-module (crates-io))

(define-public crate-cea608-types-0.0.1 (crate (name "cea608-types") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1i2s9mwqi1yw3g76g030q0plsrgb45fhykcsszx7smzfmrl6add2") (rust-version "1.65.0")))

(define-public crate-cea608-types-0.1 (crate (name "cea608-types") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ydgj89p62761ipzaha2zs8dhjqkifq2z5bqysbzfvgc30kxr0mr") (rust-version "1.65.0")))

(define-public crate-cea608-types-0.1 (crate (name "cea608-types") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "11lzqplcdng4n2rxffli3n4b5ldbh68ap8vqs80rfblbnhv5zxrw") (rust-version "1.65.0")))

