(define-module (crates-io ce rm) #:use-module (crates-io))

(define-public crate-cerm-1 (crate (name "cerm") (vers "1.0.0") (hash "05k7n92jc2dpbz0d0jlhkaqjssa9ky0cd4j73fravix0ajavjw5f")))

(define-public crate-cerm-1 (crate (name "cerm") (vers "1.1.0") (hash "1aqqvcal5mzwqvdd0sh7sb5y1697wipvw8yjsw08lhp002qgaggm")))

(define-public crate-cerm-1 (crate (name "cerm") (vers "1.1.1") (hash "0p1kzcxn7rc85hc4i0diz3mki3x3hf8vwiar6fqnd6as9r069ank")))

(define-public crate-cermic-1 (crate (name "cermic") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1i048x6sbwkdv2zgyybclv6clymbyph4gap5iq8vbk57z3i28hxa")))

