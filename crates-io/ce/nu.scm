(define-module (crates-io ce nu) #:use-module (crates-io))

(define-public crate-cenum-1 (crate (name "cenum") (vers "1.0.0") (deps (list (crate-dep (name "cenum-derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1fyyab8510rjjhgsggvybhvr6c53wmbw8xr8z8nspy6ckz88nqgs")))

(define-public crate-cenum-1 (crate (name "cenum") (vers "1.0.1") (deps (list (crate-dep (name "cenum-derive") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1a3fwf8dvy635jybnq9d75ndrlsr14p4zqhb6mwgzgvsxxiz4hj5")))

(define-public crate-cenum-1 (crate (name "cenum") (vers "1.0.2") (deps (list (crate-dep (name "cenum-derive") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1vhxp0rpnxh885i54shdxzg0bx91zwszib3plhhchyc9wvwvp9lf")))

(define-public crate-cenum-1 (crate (name "cenum") (vers "1.0.3") (deps (list (crate-dep (name "cenum-derive") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0044k1h20d4sn60srwz0ahrqrbjfw7l311y34sal3p6wlgx72bjl")))

(define-public crate-cenum-1 (crate (name "cenum") (vers "1.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0641jq4fd412a2d33q1qk9yj1f9csw15dmbfrg6mlna3dca35a30")))

(define-public crate-cenum-1 (crate (name "cenum") (vers "1.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0v1npbhamd0id0ym84ycds24dq9cdv3l8jxiq4xcxz1i6h33630g")))

(define-public crate-cenum-derive-1 (crate (name "cenum-derive") (vers "1.0.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1090i841rv7d9dqw194my41bba4a0dixzpgb98ys4dl6j2vs908s")))

(define-public crate-cenum-derive-1 (crate (name "cenum-derive") (vers "1.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "09gaq8lc6sw12dvshkgc75mrh0g5fw82ac3139wmji7h3q4d5abs")))

(define-public crate-cenum-derive-1 (crate (name "cenum-derive") (vers "1.0.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0jna5v5cjx0hh6izywg0dh8jvhls37zks4xdp3rw98kq8qp5rkdm")))

(define-public crate-cenum-derive-1 (crate (name "cenum-derive") (vers "1.0.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1n3q3k6rfliikwi3rdyqxh0p9xfg4m33yjjigzmigff6izzk7rck")))

