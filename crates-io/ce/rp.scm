(define-module (crates-io ce rp) #:use-module (crates-io))

(define-public crate-cerpton-0.1 (crate (name "cerpton") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)))) (hash "1hhl31w9br6yvxkjjj3sbdh8jyib0k2p4pmrk80zsav026fcchbx")))

(define-public crate-cerpton-0.1 (crate (name "cerpton") (vers "0.1.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)))) (hash "1knr17n83iid2yvmkx0az349bx6385y61k2n0hiy6s9bj9ay1b1h")))

