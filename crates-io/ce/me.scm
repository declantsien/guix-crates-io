(define-module (crates-io ce me) #:use-module (crates-io))

(define-public crate-cement-0.1 (crate (name "cement") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1bbp954p8ag6h8c0dwym72hrfsv0r9v8adn5kmiaajp0kpya3v9x") (yanked #t)))

(define-public crate-cement-extractor-0.1 (crate (name "cement-extractor") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)))) (hash "0461akqmmrv18zawy5ci2g43rqa6nqsj8rlr95f65vvx386afqg9")))

