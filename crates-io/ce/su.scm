(define-module (crates-io ce su) #:use-module (crates-io))

(define-public crate-cesu8-0.0.1 (crate (name "cesu8") (vers "0.0.1") (hash "1faiafsvfjznj5xq7d50xvbz2pb25nhlchx7813ch2s9fpq0y4gd")))

(define-public crate-cesu8-0.0.2 (crate (name "cesu8") (vers "0.0.2") (hash "0pb49j6xvynwinz362vjgvpbp4msynli8i5bh28rdaq5kmnf397g")))

(define-public crate-cesu8-0.0.3 (crate (name "cesu8") (vers "0.0.3") (hash "17djfhxif81ghc96kg7s1z1bkn4xwf4n88hgyqwabrmm3bfyrxac")))

(define-public crate-cesu8-0.0.4 (crate (name "cesu8") (vers "0.0.4") (hash "1vmicn90jyjki1qwnwf30qr27vgj2c0gai66g3qayldfvplswwas")))

(define-public crate-cesu8-0.0.5 (crate (name "cesu8") (vers "0.0.5") (hash "07nwmhxhhjbjaxlx61fxzfj35fqx2kzxakifvqqwgww1653bizyw")))

(define-public crate-cesu8-0.0.6 (crate (name "cesu8") (vers "0.0.6") (hash "1af42jbk3xfhwgb5wny2fzgp1m6g0bvyzh10y5rkyg0hfm7qxdib")))

(define-public crate-cesu8-0.0.7 (crate (name "cesu8") (vers "0.0.7") (hash "17rdcwahircz8lcs1sv3i40ijs70djrnsyaymqf42j3sp4wamspm")))

(define-public crate-cesu8-0.0.8 (crate (name "cesu8") (vers "0.0.8") (hash "01hnqdzvz7lz1ma738hm18ck79myqcfwacw092wl43ig04s0zlsi")))

(define-public crate-cesu8-0.0.9 (crate (name "cesu8") (vers "0.0.9") (hash "1q0yhq2msjn73xj42f665c1yp7qqbwimgfkbbhp4jz663xrnqvh2")))

(define-public crate-cesu8-1 (crate (name "cesu8") (vers "1.0.0") (hash "09mykdyym4a4v9f6qg6blhlar89fq47d6zrkdwzw364mr4mki7cj") (features (quote (("unstable"))))))

(define-public crate-cesu8-1 (crate (name "cesu8") (vers "1.1.0") (hash "0g6q58wa7khxrxcxgnqyi9s1z2cjywwwd3hzr5c55wskhx6s0hvd") (features (quote (("unstable"))))))

(define-public crate-cesu8-str-1 (crate (name "cesu8-str") (vers "1.0.0") (hash "06m107zkv3y3p64y2pz7bqqqc3kkdjygbkk9jdiz5qf18xda5vnh") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-cesu8-str-1 (crate (name "cesu8-str") (vers "1.0.1") (hash "1g3z8m0and1yj97x0akmvvjqiwgwqa1k8ghsvc671ynha30qbv8v") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-cesu8-str-1 (crate (name "cesu8-str") (vers "1.0.2") (hash "0vl8jpczfzywsif0frqy7bfgvc9y8jnl0b808kx13nvj1qcd5y7c") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-cesu8-str-1 (crate (name "cesu8-str") (vers "1.1.0") (hash "01wlyr3pl2ml1k9r9ll8kf9bnh9788vs6h70pwaqz3ai074jkdxj") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-cesu8str-0.1 (crate (name "cesu8str") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.24") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0s590l7jvs2560i8pl6m15dpg6c9nznfsl4rq0cnx9srfs6zn678") (features (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

(define-public crate-cesu8str-0.2 (crate (name "cesu8str") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.0.24") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 2)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0lx0hp92rszam6z2vd76n9bwwrbk701274jjy2iwk6lhwv0wnj7l") (features (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

(define-public crate-cesu8str-0.2 (crate (name "cesu8str") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.0.24") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 2)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1r9rvys55682rfw8mq0jf05rl9ms9dbr80cmm7fyf18jhiqax1nb") (features (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

(define-public crate-cesu8str-0.2 (crate (name "cesu8str") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^4.0.24") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 2)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1hg3jvp6s6sgyh9255xj5qivghllsb3dl4di4hx7gx629czp3jj1") (features (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

