(define-module (crates-io ce ta) #:use-module (crates-io))

(define-public crate-cetar-0.1 (crate (name "cetar") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.46") (default-features #t) (kind 0)))) (hash "1zkwwh7vbv18ayb4almpqknip8cc144541llk5njj03qbs0am86y") (yanked #t)))

(define-public crate-cetar-0.1 (crate (name "cetar") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.46") (default-features #t) (kind 0)))) (hash "0wb10jad2a5sl11xxppwshn14hhdg397nb7pljzkpq1wngbdnwhs") (yanked #t)))

(define-public crate-cetar-0.1 (crate (name "cetar") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.46") (default-features #t) (kind 0)))) (hash "0df042kdksq5f9ybcdd6fl2hj098s34cj2hgc0sb5pnpbayw8wpz") (yanked #t)))

(define-public crate-cetar-0.1 (crate (name "cetar") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.46") (default-features #t) (kind 0)))) (hash "0yccnmmfbfcm059j0g12fgbnic05ddx15galiwf6c2517s5jg3x6") (yanked #t)))

(define-public crate-cetar-0.1 (crate (name "cetar") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.46") (default-features #t) (kind 0)) (crate-dep (name "httpmock") (req "^0.7.0-rc.1") (default-features #t) (kind 2)))) (hash "0binshygs0lcam6969xa4izmfh73xijdlbn4cvscaymh5rx6zhr2") (yanked #t)))

(define-public crate-cetar-0.1 (crate (name "cetar") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.46") (default-features #t) (kind 0)) (crate-dep (name "httpmock") (req "^0.7.0-rc.1") (default-features #t) (kind 2)))) (hash "1c98b7pgw2kqcmiz4pl3gsjcz5b362wdhkwhp54f3alqni0zk7m0")))

