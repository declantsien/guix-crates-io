(define-module (crates-io ce f-) #:use-module (crates-io))

(define-public crate-cef-sys-0.0.1 (crate (name "cef-sys") (vers "0.0.1") (hash "1n3ll7907b798a9590xv287snnnv6rr91dvijc6clkkbvwaf6vaq")))

(define-public crate-cef-sys-0.0.2 (crate (name "cef-sys") (vers "0.0.2") (hash "188wn1jqiq7sggp9wkw0rzpniqfryl5p8mfpq1w5y5fbmz2k2v15")))

(define-public crate-cef-sys-0.0.3 (crate (name "cef-sys") (vers "0.0.3") (hash "001h3pc6aps1xwdangdnxxk0ahs6kbia990lhdi4wi3s4xw4285f")))

