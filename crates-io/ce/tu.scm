(define-module (crates-io ce tu) #:use-module (crates-io))

(define-public crate-cetus-0.0.0 (crate (name "cetus") (vers "0.0.0") (hash "1ny3gx5iqmzc51am5w0sgqrpvhb7aszn1hsnrrd98wifdvql56gz")))

(define-public crate-cetus-utils-0.1 (crate (name "cetus-utils") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (features (quote ("maths"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.8.0") (features (quote ("color"))) (default-features #t) (kind 0)))) (hash "1p0d1i7v20a0mjh86dyicwx58m5gjz4ql9kcfjhsdqcany07jzsb")))

