(define-module (crates-io ce nv) #:use-module (crates-io))

(define-public crate-cenv-cli-0.1 (crate (name "cenv-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03m8dykdf1z217i46xj2hb9zlynx3hyhd08n1vvgj04sbs5lnps4")))

(define-public crate-cenv-cli-0.1 (crate (name "cenv-cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1paidn2r2jp7ky1qrbc43g88a08g6v85kycnmk6y31h0jh3gasxb")))

(define-public crate-cenv-cli-0.1 (crate (name "cenv-cli") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jid0k3kzjr2j7rs6j19lpq2mhv7cb5q44wfqw6f3sdv9yn766a2")))

(define-public crate-cenv_core-0.1 (crate (name "cenv_core") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "1yb220qx66544a7rcrgnrl9h4snivjnpr0brcadrlxadhj5hawii")))

(define-public crate-cenv_core-0.2 (crate (name "cenv_core") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0kh4zzgln0bfa749d5v2rhzazw9hvwsdxaiv6m9yqnzyac1x4ynm")))

(define-public crate-cenv_core-0.3 (crate (name "cenv_core") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "044pq3if4diic8hm2an7gn77gj8pqlf0rqrqiaxdbk409jm9ga36")))

(define-public crate-cenv_core-0.3 (crate (name "cenv_core") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)))) (hash "0y79igwwdgl2y0an2n0sgssvrm5xns74v9fa0qix1hnyyz20v20l")))

(define-public crate-cenv_core-1 (crate (name "cenv_core") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)))) (hash "0drllh3vriyc247b8rkqvw87x8b01pyz0vqv83qfhmg5lf7krvdn")))

