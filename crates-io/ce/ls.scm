(define-module (crates-io ce ls) #:use-module (crates-io))

(define-public crate-celsium-0.1 (crate (name "celsium") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0zch277lj69kxny8d4qpi9crfahfb5bgb9qmbgiinhj04s6mr207")))

(define-public crate-celsium-0.1 (crate (name "celsium") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1pf3d0v69hdx4f70qpvjbisapxgnw8avlb7czdl9787fpdngmsyc")))

(define-public crate-celsium-0.1 (crate (name "celsium") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0ykvvg8s1s83v9a49g78kvn1kk7bgqj3gds8yr0q2ryal69nnmc2")))

(define-public crate-celsium-0.1 (crate (name "celsium") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (default-features #t) (kind 0)))) (hash "18pmycak922khalxwx1r070ji90ramm5k2sffh2szdmg40cgkcpn")))

(define-public crate-celsium-0.1 (crate (name "celsium") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (default-features #t) (kind 0)))) (hash "00bqv7kjk5048fzwa507shfibsak66cz7syzpmv82zn64cz77ijj")))

(define-public crate-celsium-0.1 (crate (name "celsium") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (default-features #t) (kind 0)))) (hash "00bkpmnbhk2zldkdvh10yb94hrrjmnysh83nbd1bcknzdg8cvs7z")))

