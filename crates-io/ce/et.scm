(define-module (crates-io ce et) #:use-module (crates-io))

(define-public crate-ceethane-0.1 (crate (name "ceethane") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pdjlagp5790gjk0qbk3wpjkkg1jykfbmpc798i81iwr2wysvziy")))

(define-public crate-ceetle-0.1 (crate (name "ceetle") (vers "0.1.0") (deps (list (crate-dep (name "ceetle_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n1fi4saycah9xxkc8bxkdri5b94a8kcrbnbqqyp9r2dws17hj8f")))

(define-public crate-ceetle_macros-0.1 (crate (name "ceetle_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "18f8zr4bh396ix01vcvphb23zkwakaz6ff8zm1g5n61qw8fdg664")))

