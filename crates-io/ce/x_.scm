(define-module (crates-io ce x_) #:use-module (crates-io))

(define-public crate-cex_derive-0.0.1 (crate (name "cex_derive") (vers "0.0.1") (hash "071pq97fg20wxzv4b40x93fnxvkz79y237p0rj5bzjhviwgv0x4d")))

(define-public crate-cex_derive-0.1 (crate (name "cex_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "19b158qi0s19jljknyvjdwdqlfdhp3069g3f9xwzd9q2hp3m67ni")))

(define-public crate-cex_derive-0.1 (crate (name "cex_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "13z1kdxbs8wgqq293ly46x8b7l2ax69r1nxx4fwa6qs398kf8rwz")))

(define-public crate-cex_derive-0.2 (crate (name "cex_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1d0gx4jp24pzaaanj526gaz3ryvppx31xkpgb6c4i9fw81cyg9nd")))

(define-public crate-cex_derive-0.3 (crate (name "cex_derive") (vers "0.3.0-alpha") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "109cpr4kkfirg9lhd8a5m3fm91hy69wlb8nqxql263ahfi8cl6vk")))

(define-public crate-cex_derive-0.3 (crate (name "cex_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1w7p7i60gpjbvdxpr07qvgs0l8b25m5c6lwbb501059xv2f9rqnr")))

(define-public crate-cex_derive-0.4 (crate (name "cex_derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0y8swaz2r96srr71pnd4vsgkfrjawrngaq9iv39wpfj1ha9nahvz")))

(define-public crate-cex_derive-0.5 (crate (name "cex_derive") (vers "0.5.0") (deps (list (crate-dep (name "indexmap") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1iiinyb89iakhxa62b89l9xn307xfgggjhn9rsixlyl29i0cjzyz")))

(define-public crate-cex_derive-0.5 (crate (name "cex_derive") (vers "0.5.1") (deps (list (crate-dep (name "indexmap") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1dar6r60y2xzqgwpf2rmg01khg550xzckni86hsppd2n46wafkcq")))

(define-public crate-cex_derive-0.5 (crate (name "cex_derive") (vers "0.5.2") (deps (list (crate-dep (name "indexmap") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "13ckdkbvgw5qz05av2nwj9bh14snz7ck7yk0k5rjwzbdcp6lhl2v")))

(define-public crate-cex_trading-0.1 (crate (name "cex_trading") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.14.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.93") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0adnnap5xzny6s9vxcni5wp9rm3chg8d7b8vkmd8ca0ghq3vmiqf") (yanked #t)))

(define-public crate-cex_trading-0.1 (crate (name "cex_trading") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.14.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1prd83fhnwjgd3zkcv687z0fhz3m8j7499bcrk8zlslam64q2lx7") (yanked #t)))

