(define-module (crates-io ce pr) #:use-module (crates-io))

(define-public crate-ceprustico-0.1 (crate (name "ceprustico") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "03wi2f83c3mh72349pakbhddg1imzf8cziybici80lv15sj17x99")))

(define-public crate-ceprustico-0.1 (crate (name "ceprustico") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0xq3rc6dh9sh8sacnwj17bv8ak8sm799haq1rq89jwj3a803my26")))

