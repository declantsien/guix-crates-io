(define-module (crates-io ce mc) #:use-module (crates-io))

(define-public crate-cemconv-0.1 (crate (name "cemconv") (vers "0.1.0") (deps (list (crate-dep (name "cem") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "wavefront_obj") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "1f7q406ljwf9b7mkmz86kv2ci4jqykkbsk4fhbyysfv6xzahkm9g")))

