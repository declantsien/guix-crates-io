(define-module (crates-io ym od) #:use-module (crates-io))

(define-public crate-ymodem-0.1 (crate (name "ymodem") (vers "0.1.0") (deps (list (crate-dep (name "crc16") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.0") (default-features #t) (kind 2)))) (hash "1cmsy4173n5ghq65jid2yibg8cjm887g1kyydwkadvnzwp0yhbg5")))

(define-public crate-ymodem-0.1 (crate (name "ymodem") (vers "0.1.1") (deps (list (crate-dep (name "crc16") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.0") (default-features #t) (kind 2)))) (hash "1caxl0qb0va4cq7h082c42fqcf08d449v09i2jw1vyz764yw1ilj")))

