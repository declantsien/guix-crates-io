(define-module (crates-io ym mp) #:use-module (crates-io))

(define-public crate-ymmp-0.1 (crate (name "ymmp") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1skfyfmr5y59jdnqgqbvc1hqj67a2as2gxgh4y0xjqpi5mp1nnj7")))

(define-public crate-ymmp-0.1 (crate (name "ymmp") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0098lh7y7jd6hhc5b1cbq77753pkc8r7758gxjh74ycbzrixbk2z")))

(define-public crate-ymmp-0.1 (crate (name "ymmp") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0l4pr6slancvg4a1h6bzh68igaxq3dgp0bdwyvx8hlvnhqafarlf")))

(define-public crate-ymmp-0.1 (crate (name "ymmp") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gs0nwqb4hpyygdlg94slxz4sj7awj4siif43sasnybhjjvm5acq")))

(define-public crate-ymmp-0.1 (crate (name "ymmp") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0qzw72xwyjjkgrlgm6pml22nj127mdyfr9g2p1hm54wq8dnmg98k")))

