(define-module (crates-io ym lf) #:use-module (crates-io))

(define-public crate-ymlfxr-0.3 (crate (name "ymlfxr") (vers "0.3.2") (deps (list (crate-dep (name "bytebuffer") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ulid") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mzs4rhiz30183r0aal9qscw91hc6kx0gy85kwi7z809gg7sfzsp")))

