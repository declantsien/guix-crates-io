(define-module (crates-io ym l_) #:use-module (crates-io))

(define-public crate-yml_dialog-0.2 (crate (name "yml_dialog") (vers "0.2.3") (deps (list (crate-dep (name "bevy") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 2)))) (hash "06wwjx5rvj5h36mf4nh198zrsn9jlhzs4f961q47mfxrjhzv9w1i")))

(define-public crate-yml_to_ron-0.1 (crate (name "yml_to_ron") (vers "0.1.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.3") (features (quote ("serde_impl"))) (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0p79sqp7ivjqqa5xkiaygv37j1c54sxbd45fcyvx80p1rk2q6ijw")))

