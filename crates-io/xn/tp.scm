(define-module (crates-io xn tp) #:use-module (crates-io))

(define-public crate-xntp-0.1 (crate (name "xntp") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "1rfj8aphbarqqlddhddx4bdj7jsc0r2p863az71pvbl4gl5y7hnx")))

(define-public crate-xntp-0.1 (crate (name "xntp") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "1b8018q46g97imdwzg59s47vxs6m1lffdg4np5h1cnpiqhqxcbk4")))

(define-public crate-xntp-0.1 (crate (name "xntp") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "0v8y6z9d7y4wpna0dhbllnlk641ysrb0yv42c1dqjikpdblaf52i")))

(define-public crate-xntp-0.1 (crate (name "xntp") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "11qa7rxxnrak1b6c1d5pnx3r71dsnscgghl6i3dvi5qa98kjxxj9")))

