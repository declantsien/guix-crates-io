(define-module (crates-io xn vm) #:use-module (crates-io))

(define-public crate-xnvme-0.0.0 (crate (name "xnvme") (vers "0.0.0") (hash "0jccwcbrnzjir3s7xs0miwz7nkfpfphmr8jbbk1b2ng58iccxx8c")))

(define-public crate-xnvme-sys-0.0.0 (crate (name "xnvme-sys") (vers "0.0.0") (hash "1k413jr18pi9926wz0yn0j4d1j5cplrfg53f7ywg5zz0vidiabya")))

(define-public crate-xnvme-sys-0.7 (crate (name "xnvme-sys") (vers "0.7.3") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6") (default-features #t) (kind 1)))) (hash "1w5vnnfhv4llhirlfkj65swiiqmh8j0dshcsp4ng6skbs77lgls5")))

(define-public crate-xnvme-sys-0.7 (crate (name "xnvme-sys") (vers "0.7.4") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6") (default-features #t) (kind 1)))) (hash "0l0lp91r3dhxvxs0xfqny0schkvlbfharqry2nqzz995ccp9n1xi")))

