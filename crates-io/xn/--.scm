(define-module (crates-io xn --) #:use-module (crates-io))

(define-public crate-xn--ls8h-0.0.1 (crate (name "xn--ls8h") (vers "0.0.1") (hash "0caw4hf7r21f46zg49qxzzh63h2sncaxdls4gd4lj1p0psvd78wb")))

(define-public crate-xn--ls8h-0.0.2 (crate (name "xn--ls8h") (vers "0.0.2") (hash "0f45qzv882x095l4inc6mspasv5b31ysyfirjb4my8lag0fnwhdv")))

(define-public crate-xn--rnd-6cd-0.1 (crate (name "xn--rnd-6cd") (vers "0.1.0") (hash "1qd0g1czi67rh1mhqdyw8y098zirjlx9p2wcq8v6v74rnawpdv43")))

