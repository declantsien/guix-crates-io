(define-module (crates-io xn ft) #:use-module (crates-io))

(define-public crate-xnft-tool-0.1 (crate (name "xnft-tool") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0lzzwq6isi6cv1l0zqssymg0hc4jn98ybkhpf7sh3w65c0gkspyd")))

(define-public crate-xnft-tool-0.1 (crate (name "xnft-tool") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0phvg9xz3dqdq49n2c94yr37dq551dxr4a35mc661qdhayplx353")))

(define-public crate-xnft-tool-0.1 (crate (name "xnft-tool") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1g4hg9hm6r7rnrkw65q7hlxxg3a164q1n0x4r12p801jq3dqdl5v")))

(define-public crate-xnft-tool-0.1 (crate (name "xnft-tool") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0qdfyf6al1cvhys0v72pr5z587ai25nrq7sa3i44knrywvs95bg6")))

(define-public crate-xnft-tool-0.1 (crate (name "xnft-tool") (vers "0.1.4-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "09v5kcnn2jvpzy3p6n6sp2skkb1ljd9ilzvyfizi5s1rl5ickz40")))

