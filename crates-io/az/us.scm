(define-module (crates-io az us) #:use-module (crates-io))

(define-public crate-azusa-1 (crate (name "azusa") (vers "1.0.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.1.2") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "06bf03wn37x11n9y6pg7qvlg9d9hxybvwfcq3w72s0gs9jpb7k4p") (yanked #t) (rust-version "1.63")))

(define-public crate-azusa-1 (crate (name "azusa") (vers "1.0.1") (deps (list (crate-dep (name "aho-corasick") (req "^1.1.2") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "1s74p3scj74gf2y1f76wqp4nb1ybjszfmx4w9cqdrzs639xl0i2l") (rust-version "1.63")))

