(define-module (crates-io az am) #:use-module (crates-io))

(define-public crate-azamcodec-0.1 (crate (name "azamcodec") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "16fr4216l0q3wfjrns33h9qq1f903h45a95wyxcplgqq2f8hcr57")))

(define-public crate-azamcodec-0.1 (crate (name "azamcodec") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0jlmw9qgfdvh0rnyrnxygjsq2v91l8nrq6blsnq41sly8f3pzvqj")))

(define-public crate-azamcodec-0.1 (crate (name "azamcodec") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0jx4sjxg6866wvrhvcidajyh34sqnxbr5haiwaawd6xchdnx8vny")))

(define-public crate-azamcodec-0.1 (crate (name "azamcodec") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1rcnhyz68v153graj9c6n8hwii7qdjjl9467qsjbn41b947cjs58")))

(define-public crate-azamcodec-0.1 (crate (name "azamcodec") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0flpsqvxsyw447q0a4q4ck15dszb4ax2w079p39wm6zfscvkfwsq")))

