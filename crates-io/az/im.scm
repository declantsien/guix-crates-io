(define-module (crates-io az im) #:use-module (crates-io))

(define-public crate-azimuth-0.0.0 (crate (name "azimuth") (vers "0.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "16agagfjjwr8k3pisn67kq5ng75liz52i5h6rzba45cpmzg38l7w") (yanked #t)))

(define-public crate-azimuth-0.0.1 (crate (name "azimuth") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wxdy8bws89aj02zpqsg6sisw32k35sm2xnf8zp0hcgi6is7i8ip") (yanked #t)))

(define-public crate-azimuth-0.0.2 (crate (name "azimuth") (vers "0.0.2-azimuth") (hash "10nlj0l8an8yvl1wxjf5zz5i4vh2s6f9xl8ascyhgqb01a6175rp") (yanked #t)))

(define-public crate-azimuth-0.0.0 (crate (name "azimuth") (vers "0.0.0--") (hash "0ddf5x2bgzjsr75h9wcfn8k34si6hr0kmpkjj6ypn5x7qkq7h77y") (yanked #t)))

