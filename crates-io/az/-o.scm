(define-module (crates-io az -o) #:use-module (crates-io))

(define-public crate-az-openai-rs-0.1 (crate (name "az-openai-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "1ymlbdy617wl03dc25ld68rn5zqp7p333plryaf9vgg1xxvjnb7p")))

