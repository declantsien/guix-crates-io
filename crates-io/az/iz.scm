(define-module (crates-io az iz) #:use-module (crates-io))

(define-public crate-azizka_rust_i18n-0.1 (crate (name "azizka_rust_i18n") (vers "0.1.0") (hash "1mlxhlc8grd2lnp6qssp9fr80y5l8107lpj80fznbwfayxxlffip")))

(define-public crate-azizka_rust_i18n-0.1 (crate (name "azizka_rust_i18n") (vers "0.1.1") (hash "06xx0wmx8mph9sa5v958i90lmv78djac603ipijjm50avk8zg4z6")))

(define-public crate-azizka_rust_i18n-0.1 (crate (name "azizka_rust_i18n") (vers "0.1.2") (hash "0fagpd642cbd6a90yss8syaf3v35hf2npmqwmj92j9srm1wmgw0p")))

(define-public crate-azizka_rust_i18n-0.1 (crate (name "azizka_rust_i18n") (vers "0.1.3") (hash "1bg1cnks9vyfyl5qwk4448gpdjn5f53vwmd9cm4q2gpknjl3r575")))

(define-public crate-azizka_rust_i18n-0.2 (crate (name "azizka_rust_i18n") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03vddxlfz30d1p6n2rrh6s6na3rwdsi9bhxa99nlzaqa984fn48p")))

(define-public crate-azizka_rust_i18n-0.3 (crate (name "azizka_rust_i18n") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10dwapj0flyq0nskr86rsfslp6sjcn4ysnwd1n89mbginpvrbrxp")))

