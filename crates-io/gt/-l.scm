(define-module (crates-io gt -l) #:use-module (crates-io))

(define-public crate-gt-ltq-0.1 (crate (name "gt-ltq") (vers "0.1.0") (deps (list (crate-dep (name "gt-directed-bijective-connection-graph") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fmq1nqdwpk6i5idcs27ww31jzmy2521ck5qh90ajhj8ldz8scsz")))

(define-public crate-gt-ltq-0.1 (crate (name "gt-ltq") (vers "0.1.1") (deps (list (crate-dep (name "gt-directed-bijective-connection-graph") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "1q1cm1dm1bdjhhkqsn8jly26fqza4m3712zfmmhpqr82a5a40pjd")))

