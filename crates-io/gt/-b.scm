(define-module (crates-io gt -b) #:use-module (crates-io))

(define-public crate-gt-bijective_connection_graph-0.1 (crate (name "gt-bijective_connection_graph") (vers "0.1.0") (deps (list (crate-dep (name "gt-directed-bijective-connection-graph") (req "^0.2") (default-features #t) (kind 0)))) (hash "015r4m2ahriadf5qh0bydkq9ql6jy8lrd5608fjy3fqwwh1xniwp")))

