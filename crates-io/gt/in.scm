(define-module (crates-io gt in) #:use-module (crates-io))

(define-public crate-gtin-0.1 (crate (name "gtin") (vers "0.1.0") (hash "14rv6ig9cdskxrbnqgxs0vmgmis9rgiv86g5k7waj2s5xay3yh6w")))

(define-public crate-gtin-validate-0.5 (crate (name "gtin-validate") (vers "0.5.0") (hash "0v9v8dhlzff0c7vj3hibbbsagwsc6b96rw66h7dp0ashhv9yvfwn")))

(define-public crate-gtin-validate-1 (crate (name "gtin-validate") (vers "1.0.0") (hash "1ry735ql5q6vclb0116waavghnryp77mgabh11pi2fva3wpyfkkm")))

(define-public crate-gtin-validate-1 (crate (name "gtin-validate") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.7") (default-features #t) (kind 2)))) (hash "01d9vz8flcr3dg1fm4lkkazbgnnid3a6sgi54b5clp408rz6wbl7")))

(define-public crate-gtin-validate-1 (crate (name "gtin-validate") (vers "1.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.7") (default-features #t) (kind 2)))) (hash "110zczq031msl9k5jm6vyvq2cgykrhjs64plv6ayjyhim77d0pvh")))

(define-public crate-gtin-validate-1 (crate (name "gtin-validate") (vers "1.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)))) (hash "0k76s8wsgjq4j7n94py9wf1icnhm8gj77bbh56lmks2a619d1p7q")))

(define-public crate-gtinny-0.1 (crate (name "gtinny") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1ay604lc8qmgyacg0qaw9d9zpvf31676vk8nfpzzx39biqvsrimc")))

(define-public crate-gtinny-0.1 (crate (name "gtinny") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1pkzkvz4zd161qvxz8bpy29fhgbnvsy3vzhnrz5346qha9nnr47m")))

(define-public crate-gtinny-0.1 (crate (name "gtinny") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1ji6hb2qy1pcx8k0m8g6dgdqix539f5jxnpwbcwr9r71gn4j08ml")))

(define-public crate-gtinny-0.1 (crate (name "gtinny") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "12nyp1bi0ihzld22vxlla0hjzib3fp4db9w85f24r3him9qy1wgm")))

(define-public crate-gtinny-0.1 (crate (name "gtinny") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1z80l2bkncgd28snxr71wx086nbrysx2h12ijcybmfxzlfymmyjy")))

