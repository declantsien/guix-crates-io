(define-module (crates-io gt yp) #:use-module (crates-io))

(define-public crate-gtypes-0.1 (crate (name "gtypes") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "171vz4f4jwbjfm1i496difh4a77v6iziav9fqljimhaykfi1jism")))

(define-public crate-gtypes-0.1 (crate (name "gtypes") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "01f6lag07mlvj6nhxm9mip1jwx1x0wxkyz2ncbbg9zmlv2f9z9rp")))

(define-public crate-gtypes-0.1 (crate (name "gtypes") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1f28dc3jahzq4pkwrrqrcnrnwjlln16fi57nn5fvn9prrgnalgzh")))

(define-public crate-gtypes-0.2 (crate (name "gtypes") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h9wxjq2prp1af20vvvqxx67ay7wa14i9wqmw5q1dpsyaxvjv1f8")))

