(define-module (crates-io gt m-) #:use-module (crates-io))

(define-public crate-gtm-js-0.1 (crate (name "gtm-js") (vers "0.1.0") (deps (list (crate-dep (name "gtm-js-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.58") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "0arm3sqc33zr5bbikskgkf94b4sma5m6zk13jwj1xykp12ccz4ma")))

(define-public crate-gtm-js-sys-0.1 (crate (name "gtm-js-sys") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.58") (default-features #t) (kind 0)))) (hash "17dqxdih7wdkazlgnx4880qk5v7jnjp5aj1wa9jni7dpgqrwr52x")))

