(define-module (crates-io gt ri) #:use-module (crates-io))

(define-public crate-gtrie-0.1 (crate (name "gtrie") (vers "0.1.0") (hash "0j5ff8d8pqwf41947yhpfsaz234lyjf99vmyjjii2vldrsf590gz")))

(define-public crate-gtrie-0.1 (crate (name "gtrie") (vers "0.1.1") (hash "1brgy8i4g7gwdn8aflrni38ha1wpqgh897c3gfz39kk8858a6kbd")))

(define-public crate-gtrie-0.1 (crate (name "gtrie") (vers "0.1.2") (hash "0yz4z3gpqh69b4z92lsmxcdadyzxl7nlxnkcvd2ml0rlnzylfr1s")))

(define-public crate-gtrie-0.2 (crate (name "gtrie") (vers "0.2.0") (hash "0xfaaan58awi71jnvnq3fi44sgk52025s63b4k6ra7zw4b5vb5g5")))

(define-public crate-gtrie-0.2 (crate (name "gtrie") (vers "0.2.1") (hash "06lj0w5z2yv99pn8xjn3cm817qk1azd0hhfiydx4ff0961b2p105")))

(define-public crate-gtrie-0.3 (crate (name "gtrie") (vers "0.3.0") (hash "0y9qajc54zdffc62ym382kpbs6vw69bi4gmh57vlirbz60p5srlv")))

(define-public crate-gtrie-0.4 (crate (name "gtrie") (vers "0.4.0") (hash "0b7y5idspsyrwnaacb69qk0wbwj51hws15sjljwf5h1xpfb3ljql")))

