(define-module (crates-io gt p-) #:use-module (crates-io))

(define-public crate-gtp-parser-generator-0.1 (crate (name "gtp-parser-generator") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "pom") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "18r67lk60kj573s5nkrws8sp4pxk748lgfvk007hl64kll28p3pl")))

