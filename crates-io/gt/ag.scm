(define-module (crates-io gt ag) #:use-module (crates-io))

(define-public crate-gtag-0.1 (crate (name "gtag") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~1.5") (default-features #t) (kind 0)))) (hash "1r9r7nf5p207wkjwy1y04nn0hy6yzb0qb4z0j93x4v54njccbia1")))

(define-public crate-gtag-0.2 (crate (name "gtag") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~1.5") (default-features #t) (kind 0)))) (hash "1yybzh63bnjb0hlfj8yy2mi46cqingkaybyhb25fcvd472km0ca9")))

(define-public crate-gtag-0.2 (crate (name "gtag") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~1.5") (default-features #t) (kind 0)))) (hash "15d19nf0qb5qzy17jmjzlhffq77c1vyy5lsn4amwj5a7xk7apmsh")))

(define-public crate-gtag-0.3 (crate (name "gtag") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "~1.5") (default-features #t) (kind 0)))) (hash "01icil5hhwajq0d9lip28bbdm58q2k1rsvflanijcrph0vaw2qkg")))

(define-public crate-gtag-js-0.2 (crate (name "gtag-js") (vers "0.2.0") (deps (list (crate-dep (name "gtag-js-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.58") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "0rxqikfpamgrqm330rbiffss4qgjzavv6np3sw55hln1pb7cmvwx")))

(define-public crate-gtag-js-sys-0.1 (crate (name "gtag-js-sys") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.58") (default-features #t) (kind 0)))) (hash "0zpixndngiv2mhai3nxr9lhjxmzi373kfmbcqxfrsyiz6kmmzw2h")))

(define-public crate-gtag-js-sys-0.2 (crate (name "gtag-js-sys") (vers "0.2.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.58") (default-features #t) (kind 0)))) (hash "0zkv0ffazlqs7li3msw44cr6swdgxjwl902cvlbkarkg1rh8nccj")))

(define-public crate-gtag-rs-0.1 (crate (name "gtag-rs") (vers "0.1.0") (deps (list (crate-dep (name "gtag-js-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.58") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "1pjjdxb0i6brsxjk67n2wfma4iqjl3ljhmf6br35h33127jp8haw") (yanked #t)))

