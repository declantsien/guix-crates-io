(define-module (crates-io gt ra) #:use-module (crates-io))

(define-public crate-gtrans-0.1 (crate (name "gtrans") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ahinxk35h1cc78cb60bvyxjd0p05d37lv1ihjkf03idskx6mn6l")))

(define-public crate-gtrans-0.1 (crate (name "gtrans") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k0i0jqw7zbr0l5c20n3yai3k2rs1b9yn1z8jrfrwgvyqfbgdpp6")))

(define-public crate-gtrans-0.1 (crate (name "gtrans") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kahhwvgblc0rwajcvpgfclksm3mmjjdlpdycshy5ghc6s9zb2j8")))

(define-public crate-gtrans-0.1 (crate (name "gtrans") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zzqqf76vsngd3z6qvgm6aad4a00k0zrj9kqy95s435nm9f9is59")))

(define-public crate-gtrans-0.2 (crate (name "gtrans") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hgx69hq6y8hlv787r45g200pxdyqlqd0cccnngcbz92knni5b7k")))

(define-public crate-gtrans-0.2 (crate (name "gtrans") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "08wisk7jd7bmqynk90pk58d8ryh7ch1mcwr18g2k3vfy0gjj7fw3")))

