(define-module (crates-io gt -d) #:use-module (crates-io))

(define-public crate-gt-directed-bijective-connection-graph-0.2 (crate (name "gt-directed-bijective-connection-graph") (vers "0.2.1") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gt-graph-path") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ckxn9vkif5blva7p0abjf3bxhiwvv8r9i2vphqis8zlv4ikcygj")))

(define-public crate-gt-directed-bijective-connection-graph-0.3 (crate (name "gt-directed-bijective-connection-graph") (vers "0.3.0") (deps (list (crate-dep (name "gt-graph") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gt-graph-path") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hmhkiglvn31lwdwq012yzvy8zcmhgr7dipcc6c31fsf5fwkpzr4") (yanked #t)))

(define-public crate-gt-directed-bijective-connection-graph-0.2 (crate (name "gt-directed-bijective-connection-graph") (vers "0.2.2") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gt-graph-path") (req "^0.1") (default-features #t) (kind 0)))) (hash "18pg78vdgqhccav3aak92q4i9in3gq2659qi5gcilnfxbw8l5x99")))

(define-public crate-gt-directed-bijective-connection-graph-0.2 (crate (name "gt-directed-bijective-connection-graph") (vers "0.2.3") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gt-graph-path") (req "^0.1") (default-features #t) (kind 0)))) (hash "17j4zzx43lhq8a1f22g7pmpm8d972axjm6w1xfp15m6lgh9j256w")))

