(define-module (crates-io gt ts) #:use-module (crates-io))

(define-public crate-gtts-0.1 (crate (name "gtts") (vers "0.1.0") (deps (list (crate-dep (name "minreq") (req "^2.0.3") (features (quote ("https"))) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0zpdkq996jd9vngr7bqb2f4zzr24l5a5g3rqwnngkwhvawqsp1gp")))

