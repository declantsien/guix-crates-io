(define-module (crates-io gt a-) #:use-module (crates-io))

(define-public crate-gta-ide-parser-0.0.1 (crate (name "gta-ide-parser") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1r2k2xypiqq32m7dpy58qv2mcrhh8hcqaxwysvg00km7xzqx5k3d")))

(define-public crate-gta-ide-parser-0.0.2 (crate (name "gta-ide-parser") (vers "0.0.2") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1qwavw1nx9qjfn6sy4h0l487bq2nl7r4f9qwlq6f1l0q6jjnzfbd")))

(define-public crate-gta-ide-parser-0.0.3 (crate (name "gta-ide-parser") (vers "0.0.3") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0vpzymrl7mi98jx51bynzx3d3db7sqbkk5nz96mvylf757x2zawb")))

(define-public crate-gta-ide-parser-0.0.4 (crate (name "gta-ide-parser") (vers "0.0.4") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0n6wk6fwilhwzj7ggzqi18jb9amcm2jrs26ips95aqifv3zqm1f9")))

(define-public crate-gta-img-0.1 (crate (name "gta-img") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08fwlwflwk73nkapqk96f5vrps633nnfkgw3nmhszwnhvn0qbsi6")))

(define-public crate-gta-vc-settings-0.1 (crate (name "gta-vc-settings") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "02ahfiiv9hri7yhq8zk94vl7if7aw5ikybizyx13j708q7c734ad")))

