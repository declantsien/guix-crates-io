(define-module (crates-io gt kt) #:use-module (crates-io))

(define-public crate-gtktranslate-0.1 (crate (name "gtktranslate") (vers "0.1.5") (deps (list (crate-dep (name "gtk") (req "^0.8.0") (features (quote ("v3_16"))) (default-features #t) (kind 0)))) (hash "18l18jfrlvi9lac67p32dj8cl17jz45m15kaaz04cjjv51i7g8ri")))

(define-public crate-gtktranslate-0.2 (crate (name "gtktranslate") (vers "0.2.0") (deps (list (crate-dep (name "gio") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0") (default-features #t) (kind 0)))) (hash "187ac9s1fyj0lvpqxrbfpas01ii014hwnbig58q31pyal1fnwf1w")))

(define-public crate-gtktranslate-0.2 (crate (name "gtktranslate") (vers "0.2.5") (deps (list (crate-dep (name "gio") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0") (default-features #t) (kind 0)))) (hash "1fc92rwcal215m4wk7yinxdkq0phjw25y044wzlbx7ipl3xd7sg5")))

