(define-module (crates-io gt ok) #:use-module (crates-io))

(define-public crate-gtokenizers-0.0.11 (crate (name "gtokenizers") (vers "0.0.11") (deps (list (crate-dep (name "rust-lapper") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0bpdid5vdahzh16fzswpnv962xjm0gwm1w4nk3am5hfanjpx7632")))

(define-public crate-gtokenizers-0.0.12 (crate (name "gtokenizers") (vers "0.0.12") (deps (list (crate-dep (name "rust-lapper") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "05njznfcllaqpkky7y2kpgafzkgsa8ylwbkgbypyzklvaqs894cp")))

(define-public crate-gtokenizers-0.0.13 (crate (name "gtokenizers") (vers "0.0.13") (deps (list (crate-dep (name "rust-lapper") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0hi8pnzfb5svg077gzl4yxjzm1gpq1ks47nljjyhs08m99qra0ys")))

(define-public crate-gtokenizers-0.0.14 (crate (name "gtokenizers") (vers "0.0.14") (deps (list (crate-dep (name "rust-lapper") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0kxv0343wbg16drznvg89090b6b5rhxv5lbggfzf382ihh5jgq5r")))

(define-public crate-gtokenizers-0.0.15 (crate (name "gtokenizers") (vers "0.0.15") (deps (list (crate-dep (name "rust-lapper") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "07zk8a7zbq7a2r64i9vg31aqw912q76pq7y5lab7fqkkmz3kkfqs")))

(define-public crate-gtokenizers-0.0.17 (crate (name "gtokenizers") (vers "0.0.17") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "rust-lapper") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0bc991aql65da3anl5wl3jrdxd2h2y8ag429yrwnbm4jj312swvv")))

(define-public crate-gtokenizers-0.0.18 (crate (name "gtokenizers") (vers "0.0.18") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "rust-lapper") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "15mqsh6pmkzbfzy9fx1f4ayfz5vdiri53k2qnrc8ckshkfyv8p69")))

