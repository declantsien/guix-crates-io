(define-module (crates-io gt -g) #:use-module (crates-io))

(define-public crate-gt-graph-0.1 (crate (name "gt-graph") (vers "0.1.0") (hash "05sq58gnfzb0cm4y7j7gixsnz8kf7431kaj20q56186v61096qgr")))

(define-public crate-gt-graph-0.2 (crate (name "gt-graph") (vers "0.2.0") (hash "176z91mhxp5sqih2rpfnnqqxf540f229ih6ika9lixvi9sagsl63") (yanked #t)))

(define-public crate-gt-graph-0.1 (crate (name "gt-graph") (vers "0.1.1") (hash "1vq4dh271g79kw2l9dm922nyyfhzmi11mbghkcs8rd2ji8h61l1s")))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.0") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hn40fpxwl1nhw51slkrv2zfbyxvn6p44s220yarkb3ymsycjzpw")))

(define-public crate-gt-graph-path-0.2 (crate (name "gt-graph-path") (vers "0.2.0") (deps (list (crate-dep (name "gt-graph") (req "^0.2") (default-features #t) (kind 0)))) (hash "049c45r2g3a1d6sn376z0cilp3vhc4fygnk5msk46wxr9ibdf3ni") (yanked #t)))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.1") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "11nmi9r4h24g7rwk0846148m9ivxnqdy4fnx8l0iw96iicasmgxy")))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.2") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yn1h6fhk2iwaqxv6jv80mbghr1zxi5j3qckkzb6fwdn0hzpz27r")))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.3") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b5ggpi74k4dkzwc17b1vvijilrghpl9s67h8qlg41v1kj1ls1nq") (yanked #t)))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.4") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yjbs3kv6jfwrszf393cax2ymw7hj2a19bzpcncq62c5v3s9azkr") (yanked #t)))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.5") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "00k3rp745c8ciqc8n6pxv5j6vx3nfv92c8narwd6y026x6pw5dwg")))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.6") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zy60issyi2p7vj6q9z9lhl1d51l69s1z5fv5x9yy5f2z1xhdr3q")))

(define-public crate-gt-graph-path-0.1 (crate (name "gt-graph-path") (vers "0.1.7") (deps (list (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xdwpvv6pm1zsabbszbnxn7vf4r6fj4835y1s13hlzik740hj9p6")))

