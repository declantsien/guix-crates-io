(define-module (crates-io gt k5) #:use-module (crates-io))

(define-public crate-gtk5-0.0.0 (crate (name "gtk5") (vers "0.0.0") (hash "0022mvbxdsx3z5z37b3dwpd96g43dnh2z96w4jacdvswx9cpxmlp")))

(define-public crate-gtk5-macros-0.0.0 (crate (name "gtk5-macros") (vers "0.0.0") (hash "03j70wdsd8l79dfkr2q089gy16jvb2690y8k3nwnvvxb0kbgwjfj")))

(define-public crate-gtk5-sys-0.0.0 (crate (name "gtk5-sys") (vers "0.0.0") (hash "0p32kpcv4xiwlga4q3j0g9d5ckrvkw0lj5mz30kcydc4n31laspp")))

