(define-module (crates-io gt -h) #:use-module (crates-io))

(define-public crate-gt-hypercube-0.1 (crate (name "gt-hypercube") (vers "0.1.0") (deps (list (crate-dep (name "gt-directed-bijective-connection-graph") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "14vnabv9bic6lg741x8z3pbal7ibsadq1fhsaw6rjc4hvda3zf69")))

(define-public crate-gt-hypercube-0.1 (crate (name "gt-hypercube") (vers "0.1.1") (deps (list (crate-dep (name "gt-directed-bijective-connection-graph") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "gt-graph") (req "^0.1") (default-features #t) (kind 0)))) (hash "109n91iycnw05s2pn198hn02y8gk0xyxpsbh18p27l6xiqbjdf25")))

