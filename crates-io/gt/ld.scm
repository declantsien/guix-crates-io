(define-module (crates-io gt ld) #:use-module (crates-io))

(define-public crate-gtld-data-0.2 (crate (name "gtld-data") (vers "0.2.1") (hash "010cvcicajw31l7fpkaq5nd00kyrkajawzbvmp3zlb6q0rlhaxk1")))

(define-public crate-gtld-data-0.2 (crate (name "gtld-data") (vers "0.2.2") (hash "1bs15h2lrnqlffpir4vi3mybi2xp7nszd5555j1p409ppsz194nf")))

(define-public crate-gtld-data-0.2 (crate (name "gtld-data") (vers "0.2.3") (hash "1lsn80vabj5z2hmwl2bypf23dvd3zqigxzvlxdbrwp833awad9g5")))

(define-public crate-gtld-data-0.2 (crate (name "gtld-data") (vers "0.2.4") (hash "152y8m12bqr54hnn2ykw0zy1daqzx4xsm2ccrjfk4zi3d8kqp1v3")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.0") (hash "17hcwaqmq79jlsh80jzjalq8kj2myq9ac7hnzyqlb23g4h2i03hy")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.1") (hash "1i86yxizcjj8krhlxmibz1bpngsqz1nwkjc0yfgsagc8ar83ndc8")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.3") (hash "0jaigrn906g4ki2dmllaimm81nl1c8wjbj9g848xbpfajcijq1zz")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.4") (hash "1i59smq0nblr3vqiajy64xjbhx40q9vz1fw1p0f4irczfqbcwprm")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.5") (hash "1rlv3g5qxykrq0v11ir0kjmcb833wi8y781w6rzynp3pi64r2ln1")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.6") (hash "1579j8z14v50c1g1jmgdhcqjyjxcrdyjhmz0fw1lygjdik8haf1a")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.7") (hash "1pdama0gxv6w6rvcxh4j1kcl275dknjav093wy3h1w5ircdk74xm")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.9") (hash "0ba61g6li7dbl0pnjsj8anz6jk08scsbhjnh50xlb6m6nsizmfpi")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.10") (hash "0wf86nwl1jkxnsccmzrqsidv23h6ayiczjq4s5brskhsagzrxzlc")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.11") (hash "17960k5bwv1wc2krrq79kifnhlv2bbgzkvs7h6xj4x5bf1wch4bm")))

(define-public crate-gtld-data-0.3 (crate (name "gtld-data") (vers "0.3.12") (hash "0fki8fhl39z2rn1jd398q613v1xv8gjswjisiy8kbyzg0vbxs4mc")))

(define-public crate-gtld-data-0.4 (crate (name "gtld-data") (vers "0.4.0") (deps (list (crate-dep (name "curl") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "select") (req "^0.3") (default-features #t) (kind 1)))) (hash "135fbn6fxqihrn5f7fld044jb0aixrkljd0kp8h97pkr73xj5w96")))

(define-public crate-gtld-data-0.4 (crate (name "gtld-data") (vers "0.4.1") (deps (list (crate-dep (name "curl") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "select") (req "^0.3") (default-features #t) (kind 1)))) (hash "0h1q132wm9cp85wy5z8qmd7jb124snidsfjldpi8wr93iw73wkh8")))

