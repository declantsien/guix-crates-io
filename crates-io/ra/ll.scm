(define-module (crates-io ra ll) #:use-module (crates-io))

(define-public crate-rall-0.1 (crate (name "rall") (vers "0.1.0") (deps (list (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "07xb97nn6iz1l5591qy3y36pf3gmdcxgfisvypkmc8z91xk50ixl") (features (quote (("doc-images"))))))

(define-public crate-rall-0.2 (crate (name "rall") (vers "0.2.0") (deps (list (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "0rhp6qw3kmljfbjkw557jhf3kvbkcpc1q3xgwjsb12vy0s7j9l04") (features (quote (("doc-images"))))))

(define-public crate-rall-0.3 (crate (name "rall") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "1flkx20f9dbwwavq4vz8n0hpb6z11570sy867cm330j7s2ymgc4k") (features (quote (("doc-images"))))))

(define-public crate-ralloc-1 (crate (name "ralloc") (vers "1.0.0") (deps (list (crate-dep (name "clippy") (req "^0.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ralloc_shim") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "unborrow") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1p9fbfrhxs5zdd2jkws0kx1w0z81lk1c02b9p1f397prb1x7zj0h") (features (quote (("write") ("unsafe_no_mutex_lock") ("tls") ("testing" "log" "debugger") ("security") ("no_log_lock" "log") ("log" "write" "alloc_id") ("default" "allocator" "clippy" "tls") ("debugger") ("allocator") ("alloc_id"))))))

(define-public crate-ralloc_shim-0.1 (crate (name "ralloc_shim") (vers "0.1.1") (deps (list (crate-dep (name "syscall") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "04118nvwwh2dddnl8p0laxz8kvsr34xwqflc1962sw72n2hask06")))

(define-public crate-rally-0.1 (crate (name "rally") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1l8zalc421zfcrql4085i7j6g845pfycs18c7hbgfb7pwrbpian7")))

(define-public crate-rally-0.1 (crate (name "rally") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0iv9xld62jdmmchbhqnsz87flqw9j0vx9l59d9hpfx53ap25gj90")))

