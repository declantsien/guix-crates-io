(define-module (crates-io ra yf) #:use-module (crates-io))

(define-public crate-rayfork-sys-0.0.1 (crate (name "rayfork-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fsii0k4wgpglxq2bmfzz3y6saz7f4avqlz2hfadscf3si5bj4di")))

