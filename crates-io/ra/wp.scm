(define-module (crates-io ra wp) #:use-module (crates-io))

(define-public crate-rawpnt-0.1 (crate (name "rawpnt") (vers "0.1.0") (hash "0zr86sbbfxgqh43y0bf2gq8hw1xgz2q18bn6dg7l5h2rlrrsz39j")))

(define-public crate-rawpointer-0.1 (crate (name "rawpointer") (vers "0.1.0") (hash "06ghpm9y7gacks78s3maakha07kbnwrxif5q37r2l7z1sali3b7b")))

(define-public crate-rawpointer-0.2 (crate (name "rawpointer") (vers "0.2.0") (hash "0ijkjxy15g87smf1fvyhafv6slvr13cf3sl827rwvfkj5b95gs35")))

(define-public crate-rawpointer-0.2 (crate (name "rawpointer") (vers "0.2.1") (hash "1qy1qvj17yh957vhffnq6agq0brvylw27xgks171qrah75wmg8v0")))

(define-public crate-rawprinter-0.1 (crate (name "rawprinter") (vers "0.1.0") (deps (list (crate-dep (name "axum") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap-num") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "escpos-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1x69yxmvliym7a26r3j5wqa49cn1mi3akp7sbyd7pki0qv62v14m")))

(define-public crate-rawprinter-0.1 (crate (name "rawprinter") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap-num") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "escpos-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0") (default-features #t) (kind 0)))) (hash "04h1vmr5bvcqh1s4awb6yyllkc7hgryg9x4qhszbfrrfbwgqagn6")))

(define-public crate-rawprinter-0.1 (crate (name "rawprinter") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap-num") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "escpos-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0") (default-features #t) (kind 0)))) (hash "1si5drpkd55d03ch31ff222qvh47p6vvak7a2jwn1xqhaqaqid0q")))

(define-public crate-rawproxy-0.1 (crate (name "rawproxy") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.5.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "async-uninet") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0yj85qlyy0i78y049332h3634lyfnqvll6h428l2af0rj9mw935z")))

(define-public crate-rawproxy-0.2 (crate (name "rawproxy") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.5.0") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.5.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "async-uninet") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0zhp0zh0n4w4cl7glfq0ha9ccpw8pv6n07cdhpqnlidcq6m65zrc")))

(define-public crate-rawproxy-0.3 (crate (name "rawproxy") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.5.0") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.5.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "async-uninet") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1kccsbx2y37jv044q3zw4rrwp1l4ixhnl6vvkk5w0858g8aa03fc")))

