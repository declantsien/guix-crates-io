(define-module (crates-io ra lg) #:use-module (crates-io))

(define-public crate-ralgeb-0.1 (crate (name "ralgeb") (vers "0.1.0") (hash "08g3d1smbl4ims9ivjpllrx9gxvass1kj9wp08zx1y15hb5afny8")))

(define-public crate-ralgebra-0.1 (crate (name "ralgebra") (vers "0.1.0") (hash "0faqk4a2l8v8m4h51r2ac3ar2yh43lr0k12mbylf4qv149bhg380")))

(define-public crate-ralgebra-0.1 (crate (name "ralgebra") (vers "0.1.1") (hash "14ixr0l6raw86rbgp2scj9lxhrnk3wl1s91ysdqqzaimfnjawgp2")))

(define-public crate-ralgebra-0.1 (crate (name "ralgebra") (vers "0.1.2") (hash "0lnl0lypnhl01hp0va6wbaqkiik48fp2rvwhjyaniknhs5c3lzr6")))

(define-public crate-ralgebra-0.1 (crate (name "ralgebra") (vers "0.1.3") (hash "01xhvsfvfqbddrahipdgagqgbr9rfwv9s070jb9dmf68xqd51sm4") (yanked #t)))

(define-public crate-ralgebra-0.1 (crate (name "ralgebra") (vers "0.1.4") (hash "1g5c2clr7m78iwnfv9flr046jqzs2a7rbx5ksx34n70ijj8hp9qh")))

(define-public crate-ralgebra-0.2 (crate (name "ralgebra") (vers "0.2.0") (hash "04dh8fqc2zdmggiwwa1f4dngzxma9ymmkdb3vjgfivw7hmhfngv7")))

(define-public crate-ralgo-0.1 (crate (name "ralgo") (vers "0.1.0") (hash "1w1mzh26dwll9vavl6ipxn17kq7gg8fgafqzn1xzgqnad8fjm9rv")))

