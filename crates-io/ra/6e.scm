(define-module (crates-io ra #{6e}#) #:use-module (crates-io))

(define-public crate-ra6e1-0.2 (crate (name "ra6e1") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1h37givng01kkzfy3indblbdhva0r4fhjgilagjbz0aa8ysv0jsl") (features (quote (("rt" "cortex-m-rt/device"))))))

