(define-module (crates-io ra ke) #:use-module (crates-io))

(define-public crate-rake-0.1 (crate (name "rake") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ywli0k1mbn436wnrgn5dvc5f1j96awrpxakbrx65pm1v7z8mi1i")))

(define-public crate-rake-0.1 (crate (name "rake") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "01gq8d4lk2300z3xsrp0z3wm19cpq5m28x0jxgaf85m3d0wy2mam")))

(define-public crate-rake-0.1 (crate (name "rake") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "~1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.0") (default-features #t) (kind 0)))) (hash "1nif54j4c9v8fin0c8dgn9jpa0dizvhj5wh795w27pjx5jvkch12")))

(define-public crate-rake-0.1 (crate (name "rake") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "~1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.1") (default-features #t) (kind 0)))) (hash "0yhxkd2pfawk83fic61g6aagclqx144jj0m448h83zlvi3cykmha")))

(define-public crate-rake-0.1 (crate (name "rake") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "~1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.1") (default-features #t) (kind 0)))) (hash "1jj4zfz6x92v5ibbxj7v0mvvjgv3pilimmfhjivz3xfqcwf67705")))

(define-public crate-rake-0.1 (crate (name "rake") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "~1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.1") (default-features #t) (kind 0)))) (hash "091r4gxkf6qj1j0w3lk5lb74g4xnpjq6ngdml5bllm4580q14gfg")))

(define-public crate-rake-0.2 (crate (name "rake") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "17zkzz9jfmak5g2dmz4r0r605yyq8fyph8lmf2faxpyw1nxzq0h1")))

(define-public crate-rake-0.3 (crate (name "rake") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "1jd3f8x3jf414wf89mldvw0nncl3iqfmj5b5gxf9b3ypf95bnca4")))

(define-public crate-rake-0.3 (crate (name "rake") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "08b1kv87g8ifcair5kimr7li3mpa7czsniy0p27rmd7qqxi8k0v2")))

(define-public crate-rake-0.3 (crate (name "rake") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1s11afsph6hdr15f7jrjzb7q7kl5w338ljq5zfwz7bsv6avpm4gr")))

(define-public crate-rake-0.3 (crate (name "rake") (vers "0.3.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1df5jskws2dgv6yi997y4d0x934qsaxjn7mcvj6lnq40n0yhjdcl")))

(define-public crate-rake-0.3 (crate (name "rake") (vers "0.3.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1350llcx0rpwbga81y9rg3glbpnligcs31yqvgr09i6w5gliagwr")))

