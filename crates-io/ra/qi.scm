(define-module (crates-io ra qi) #:use-module (crates-io))

(define-public crate-raqiya-bible-reference-0.1 (crate (name "raqiya-bible-reference") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "06xdljxfb0kz8m4n4mdw6s64rrb1c9kr1d8rsk8kg01xcn2bhbi6")))

(define-public crate-raqiya-bible-reference-0.1 (crate (name "raqiya-bible-reference") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "17lh9pl2c6ni9zz4vj93s9y8k86l1x89h05xwa3dxf39g55n863b")))

(define-public crate-raqiya-bible-reference-0.1 (crate (name "raqiya-bible-reference") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0bjvzgy7422f25hxd4n1zmc88v8aqbrnz46sq7n0ik0sbq761ir5")))

