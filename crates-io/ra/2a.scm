(define-module (crates-io ra #{2a}#) #:use-module (crates-io))

(define-public crate-ra2a1-0.2 (crate (name "ra2a1") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0r8g489b8f8kjrdvld1hj1m5nljnswxgan43326f87afn9ccv6sf") (features (quote (("rt" "cortex-m-rt/device"))))))

