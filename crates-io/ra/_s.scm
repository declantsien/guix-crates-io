(define-module (crates-io ra _s) #:use-module (crates-io))

(define-public crate-ra_syntax-0.1 (crate (name "ra_syntax") (vers "0.1.0") (deps (list (crate-dep (name "drop_bomb") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rowan") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "text_unit") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.0") (default-features #t) (kind 2)))) (hash "1sbc4xjfcr35la7n60gj77248352nr12fjsahyy1agv20wcqwc5y")))

