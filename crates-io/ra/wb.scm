(define-module (crates-io ra wb) #:use-module (crates-io))

(define-public crate-rawbson-0.1 (crate (name "rawbson") (vers "0.1.0") (deps (list (crate-dep (name "bson") (req "^1.1") (features (quote ("decimal128"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "decimal") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xxf6arbvmxq9xhpxb3y5r19mag9a73f80mq7iqphj81k8l34r6c")))

(define-public crate-rawbson-0.1 (crate (name "rawbson") (vers "0.1.1") (deps (list (crate-dep (name "bson") (req "^1.1") (features (quote ("decimal128"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "decimal") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gf3fi2dxn3yvq51prx2xm6v99p9nrkvpnycgbdp7wqdgrwkwnak")))

(define-public crate-rawbson-0.2 (crate (name "rawbson") (vers "0.2.0") (deps (list (crate-dep (name "bson") (req "^1.1") (features (quote ("decimal128"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "decimal") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03lc5fg6zcmp3wayz3h7i6ia7nmyxqz66v66d0xjs6wlwdq29f7z")))

(define-public crate-rawbson-0.2 (crate (name "rawbson") (vers "0.2.1") (deps (list (crate-dep (name "bson") (req "^1.1") (features (quote ("decimal128"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "decimal") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0y7nlrrbxpah19px7qbslmw1y1sz0h4p8lnbq4pkvcs8hsa15m0w")))

(define-public crate-rawbytes-0.1 (crate (name "rawbytes") (vers "0.1.0") (hash "1r1r2v9m1g0m2zfs22gylxgnsxw9hv4a2b0paas5jgvc1n071c88")))

(define-public crate-rawbytes-0.1 (crate (name "rawbytes") (vers "0.1.1") (hash "1fyqfl205ckyzwis8857y7n0m0fii7dcnmmzcfni3z92qbs82vd2")))

(define-public crate-rawbytes-0.1 (crate (name "rawbytes") (vers "0.1.2") (hash "0q1yf6aa4n5bw4kv4gcsr7n6iyip8vgdhfgjqw96vf5g2s7hil04")))

(define-public crate-rawbytes-1 (crate (name "rawbytes") (vers "1.0.0") (hash "106sccmc4j6i61amf2df0648q9iid87c1rh33lvwlcc7v2xg2dz0")))

(define-public crate-rawbytes-1 (crate (name "rawbytes") (vers "1.0.1") (hash "0lrjcrwvhvi8iy8970yavz7iiqiwa7bygwrabid5zybn3vhgkk0m")))

