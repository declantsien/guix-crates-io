(define-module (crates-io ra tr) #:use-module (crates-io))

(define-public crate-RatRod-rs-0.1 (crate (name "RatRod-rs") (vers "0.1.0") (deps (list (crate-dep (name "dylib") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "more-asserts") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)) (crate-dep (name "serde_json_any_key") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sparse_matrix") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1a7jgr91lwcvaahgb4xq11zxn0hdi94vhcvkxnajvw4zf40k070c")))

