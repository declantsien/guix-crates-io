(define-module (crates-io ra ds) #:use-module (crates-io))

(define-public crate-rads-0.1 (crate (name "rads") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ch2wzlvmr0gxq4hr55rvvmjx93x03ijrhsls142cxasi229dnyn")))

(define-public crate-radsort-0.0.0 (crate (name "radsort") (vers "0.0.0") (hash "0kxm4szp98xfwqlnjdlzqc0radzi7lmcd57qw2pkb0n9b42g33gr")))

(define-public crate-radsort-0.1 (crate (name "radsort") (vers "0.1.0") (hash "02vsyxigycsa20zk66g396kvxq47smg69qnzw49dmznk1qwrdz8p")))

