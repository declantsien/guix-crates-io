(define-module (crates-io ra ii) #:use-module (crates-io))

(define-public crate-raii-change-tracker-0.1 (crate (name "raii-change-tracker") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "111l2aggzq2hs5h6p8zgddhj7fvlyhkm488pqg1kgybkaswskxxj")))

(define-public crate-raii-change-tracker-0.1 (crate (name "raii-change-tracker") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "1h1x3925piyynszf3r03dvhzzirz1mzv0pc2b0k5l38kjwli7bfx")))

(define-public crate-raii-counter-0.1 (crate (name "raii-counter") (vers "0.1.0") (hash "0hn6gn1nyp0xa73q8bkspp0ry115sd23ckhg47zpclwwsbcw3hd2")))

(define-public crate-raii-counter-0.1 (crate (name "raii-counter") (vers "0.1.1") (hash "0cqfhx6c6vgxv81l05k29rbpwcrsywypg9j9z5vpgk2dz9ig9nql")))

(define-public crate-raii-counter-0.2 (crate (name "raii-counter") (vers "0.2.0") (hash "1g92clvzdl45999agvqbpahx1wydygmaf0k35vn3w24hpdpz3108")))

(define-public crate-raii-counter-0.2 (crate (name "raii-counter") (vers "0.2.1") (hash "1r6kz717fdmz6jylg1iqzh4g1kpdi0q0rh2ybfjzmhfmizmwjb6a")))

(define-public crate-raii-counter-0.3 (crate (name "raii-counter") (vers "0.3.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gmbgy9nhhcv5l8266vd27zqsgvjapwclp89002gnw6nrbkzq4iw")))

(define-public crate-raii-counter-0.4 (crate (name "raii-counter") (vers "0.4.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xgs4yy79kpac3jjhgj1ymy2ky3b1xplviy4pb42p0il5l4l7ghq")))

(define-public crate-raii-counter-futures-0.1 (crate (name "raii-counter-futures") (vers "0.1.0") (deps (list (crate-dep (name "futures-intrusive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "10571d6pc8rgc4fa6smnfy2s52jflrmmnm1hmqw7d45q2x6gjbsh")))

(define-public crate-raii-map-0.1 (crate (name "raii-map") (vers "0.1.0") (deps (list (crate-dep (name "owner-monad") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1clgmwrj8lgw4fvzq6kdqva1w851v0nwjwxqckqb9k12qaxg6yjp")))

(define-public crate-raii_flock-0.1 (crate (name "raii_flock") (vers "0.1.0") (deps (list (crate-dep (name "fs2") (req "^0") (default-features #t) (kind 0)))) (hash "0lq2ppqswvi0afrn7nf59k11815bk3cbviiwavvi0vaz09i6mf4l")))

(define-public crate-raii_flock-0.2 (crate (name "raii_flock") (vers "0.2.0") (deps (list (crate-dep (name "fs2") (req "^0") (default-features #t) (kind 0)))) (hash "1zqhm7vz69nkzsg8a4fqajiirjjnmdjkcsbxh92wgk4z1qgj6vv4")))

