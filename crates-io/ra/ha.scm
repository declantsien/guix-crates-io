(define-module (crates-io ra ha) #:use-module (crates-io))

(define-public crate-rahashmap-0.1 (crate (name "rahashmap") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0kzpmfpvrxxjnpr9rf0jbgb4aj6d7w126607hghxssj7b2xpkbjr")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "19wdf1dnvv90zjrbcb80g4w0n9bwzkv6yx8i3k6p9sg7ds45bfr9")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "07q8qk26v9m0r1dqxvqnn6qxpr5pgcfxlgj09wbyhijnx62xxh7d")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0japypr31svbw7sx0vxqp7w9qpc85dhq2afv0r1rk0hxhkm0k3py")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "161w6kmsn8y84hl4cj65j6419qqqm8irc4k25dmr1shfyllmzwhk")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0nbm9jn3f1dlg65b7d2zzmvgzpyhqr3agk1bip756xcpgs1kihkn")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "10ifvcbkrhyxvf9ykic6n3f60h7cqdflr0vk3ln1r5bqzi805gwx")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1l88i2iciqz1jfgwwb0rjyx53m1xyk7ml3h23n53w7nbaw9z95k7")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.7") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1wc3hh1wrlr0cqxcxw6cd1ybxn970gvsi8zfzm5lfci5615ly98w")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.8") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "15r9s0cff9hxl4830vjypbl5k41v31is4g7b82b8ra51w8qi04n8")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.9") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0wvqhgj5y1qzf00qi82nzzjnws5dra5vl4l1j162rf9wvxmydjjn")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.10") (deps (list (crate-dep (name "rand") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1j39b10fsdq6sx32ia3iz9a6i4rdv8yp510caplvl2f6iqfjzcyd")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.11") (deps (list (crate-dep (name "rand") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "06l2ydm9mhw1pn8ma1kj3v7vzdvvrzzb7sa1a8ks9d8l51xbi6dc")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.12") (deps (list (crate-dep (name "rand") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0aiwyi11zhvsibz933glzc8kx4mh5y9rc212x0yxr5iv8ragk2pl")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.13") (deps (list (crate-dep (name "rand") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1h5iw9ari5d3p7062cy4hgq6l20jn7bb3c923n2js1myf1k6i1h9")))

(define-public crate-rahashmap-0.2 (crate (name "rahashmap") (vers "0.2.14") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1d3b3kvlg9chxcc36j3kf634si0z3y70vr9l84sshn6i7jkbz33c")))

(define-public crate-rahat3062_minigrep-0.1 (crate (name "rahat3062_minigrep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0mi1zxngz5z63sh43b1dzvlfnpz08y462hypqz4ycdy23v80gmy3") (yanked #t)))

(define-public crate-rahat3062_minigrep-0.1 (crate (name "rahat3062_minigrep") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1809551wzj3hqlrppy3p97qdz495yai1khpwvlpqj0gzz3w1flfg") (yanked #t)))

(define-public crate-rahat3062_minigrep-0.1 (crate (name "rahat3062_minigrep") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "012h8p6jba0a4kypswlb2cc585az07v1n9n460ygplbfg3w1fm0i") (yanked #t)))

(define-public crate-rahat3062_minigrep-0.1 (crate (name "rahat3062_minigrep") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1vh9dpxj3m6kc2wjpk5xjq1h3b2jz257rgyj8dj6w6g92zrr82y0")))

(define-public crate-rahat3062_pool-0.1 (crate (name "rahat3062_pool") (vers "0.1.0") (hash "0kzs0qnzkxbvd18h0lzj5mhhyksw9cwgxfz5c6cpf6lvsmbsj7kw")))

