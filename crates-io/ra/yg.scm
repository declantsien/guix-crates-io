(define-module (crates-io ra yg) #:use-module (crates-io))

(define-public crate-raygon-0.1 (crate (name "raygon") (vers "0.1.0") (hash "0yinasym51z88mk80cfg5x467l8h9fn75iwzf8hzgnb9lxd81rqp")))

(define-public crate-raygun-0.1 (crate (name "raygun") (vers "0.1.0") (hash "0yaq6nmq3ivsyk16dysn4bwrnwbg9bhcgwj85av961s3m38r9k63")))

