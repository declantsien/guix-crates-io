(define-module (crates-io ra is) #:use-module (crates-io))

(define-public crate-raise-1 (crate (name "raise") (vers "1.0.0") (hash "197w559zzs3x77g2jqr4lv2kwkskgfkqbjzkym3f6r0q8jfbn8v4")))

(define-public crate-raise-2 (crate (name "raise") (vers "2.0.0") (hash "12g55hjvif7jmp9w519na4b76381xhvngc0pj5ahgl0drh7dw9si")))

(define-public crate-raisensu-0.1 (crate (name "raisensu") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0msjjxblghmlq7g41nskj6k5wg2rcgp1icmii39hn2aymp6imwsb")))

(define-public crate-raisensu-0.1 (crate (name "raisensu") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0srhh1za84k4p8y2yb48m1zb6y28dykdr9hm6gwalqyf71dl54s7")))

(define-public crate-raisin-0.0.1 (crate (name "raisin") (vers "0.0.1") (hash "1iqjhl6m1grkpwigzjp1pvw789pp0l8lkdp7qy680kps0jblvl1y")))

