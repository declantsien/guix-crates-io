(define-module (crates-io ra l1) #:use-module (crates-io))

(define-public crate-ral1243-1 (crate (name "ral1243") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "z80emu") (req ">=0.9") (kind 0)))) (hash "0jfr8fy4s9y1if1d6wl2f0zi9xf90qink8z3jrr8c1yqflzpggpk") (features (quote (("std" "z80emu/std") ("default"))))))

(define-public crate-ral1243-2 (crate (name "ral1243") (vers "2.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "z80emu") (req ">=0.11") (kind 0)))) (hash "08hqhm0nff7wj6nz89skp39c44z7g99hm25al44ki3isx4bw87fy") (features (quote (("std" "z80emu/std") ("default"))))))

(define-public crate-ral1243-2 (crate (name "ral1243") (vers "2.0.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "z80emu") (req ">=0.11") (kind 0)))) (hash "1vjh2mcjyvb1fc5j5l37qq97w3jzwsvpllk541lkysz15mka028d") (features (quote (("std" "z80emu/std") ("default"))))))

