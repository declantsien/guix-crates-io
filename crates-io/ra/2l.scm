(define-module (crates-io ra #{2l}#) #:use-module (crates-io))

(define-public crate-ra2l1-0.2 (crate (name "ra2l1") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1h4jm1qw9mn0p9whlpha5zvy52vp2a2xhjnx06flkssy4w92azyq") (features (quote (("rt" "cortex-m-rt/device"))))))

