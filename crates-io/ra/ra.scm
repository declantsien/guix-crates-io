(define-module (crates-io ra ra) #:use-module (crates-io))

(define-public crate-rara-0.1 (crate (name "rara") (vers "0.1.1") (deps (list (crate-dep (name "combu") (req "^1.1.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wmyk5fdwzx71lkl4r097xppczndf43yzbmiw2dszabx348r5lfx")))

(define-public crate-rara-0.1 (crate (name "rara") (vers "0.1.2") (deps (list (crate-dep (name "combu") (req "^1.1.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12fi5chn865bdqh9dnpp9pplvh9flxl6cajya4ds6fld1ljva8w5")))

(define-public crate-rara-1 (crate (name "rara") (vers "1.0.0") (deps (list (crate-dep (name "combu") (req "^1.1.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02xj3y6mj775d9dvyqgfc72pcc0grw54bg5pwc15xily9ii7mxkh")))

(define-public crate-rara-1 (crate (name "rara") (vers "1.0.1") (deps (list (crate-dep (name "combu") (req "^1.1.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02qgnswn63z2l2q0bj3h56s8q1knqcp0kswdab6mqpqvnsricgb6")))

(define-public crate-rara-1 (crate (name "rara") (vers "1.0.2") (deps (list (crate-dep (name "combu") (req "^1.1.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ac8wi50dl8956j7n818qvcd7j4nyb1ikaky25340p7sb6r0rnn5")))

(define-public crate-rarathon-0.0.1 (crate (name "rarathon") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "*") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "0annyvwidds49zlz4pb83csd6pv5m1lzy1qadr0p8ldk843mf8dl")))

