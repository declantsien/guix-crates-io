(define-module (crates-io ra sc) #:use-module (crates-io))

(define-public crate-rasc-0.1 (crate (name "rasc") (vers "0.1.0") (hash "07z3bf4s735g46lxl3li1b5i29q7x7bs4l4hc7j5ydb4shryyjmf")))

(define-public crate-rascal-0.1 (crate (name "rascal") (vers "0.1.0") (hash "0ibp3nvh0gzczgcnnsr90j762v2qahyjvb48wp2pipbq4biq8df3")))

(define-public crate-rascal-0.1 (crate (name "rascal") (vers "0.1.1") (deps (list (crate-dep (name "rascal_bytecode") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_compiler") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_scanner") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_vm") (req "^0.1") (default-features #t) (kind 0)))) (hash "02vrw724mj1iqsvl8imh49lkz9imhdscg677dwjxm65fvfnwzl2k")))

(define-public crate-rascal-0.1 (crate (name "rascal") (vers "0.1.2") (deps (list (crate-dep (name "rascal_bytecode") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_compiler") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_scanner") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_vm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0g4r6dn4s8iv1qfc72sxk5m00mavpp2ndxb7ffpls4n10scc2nms")))

(define-public crate-rascal-scheme-0.1 (crate (name "rascal-scheme") (vers "0.1.0") (hash "0fl543l60sczs5zr2mmj13q16f9b3ibzdbs136yyakxa0pq5qib1")))

(define-public crate-rascal_bytecode-0.1 (crate (name "rascal_bytecode") (vers "0.1.0") (hash "1pfcjw6drv1jxrs67ac6i5ip0ldrfmcskkrxfgqymmhdml95ayxw")))

(define-public crate-rascal_bytecode-0.1 (crate (name "rascal_bytecode") (vers "0.1.2") (hash "03y9775ln05zbzv3pv588gxj5rijqfndx9hw5kb4jjgrnp1sy3xi")))

(define-public crate-rascal_cli-0.1 (crate (name "rascal_cli") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rascal") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1clljnzdfvyjyzns7g837fs5k6nqkpkzjyq9mksagn44k5jz1w2y")))

(define-public crate-rascal_compiler-0.1 (crate (name "rascal_compiler") (vers "0.1.0") (deps (list (crate-dep (name "rascal_bytecode") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_parser") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "137ws1dsgpdqx2rhnnzvq7v7lvgj30di4qjqkifvrq6k1pxp1355")))

(define-public crate-rascal_compiler-0.1 (crate (name "rascal_compiler") (vers "0.1.2") (deps (list (crate-dep (name "rascal_bytecode") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_parser") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ki088n1kys4r1ci1wwkxkldcr26nihqn2zqj9sxz3sjs7ps7298")))

(define-public crate-rascal_parser-0.1 (crate (name "rascal_parser") (vers "0.1.1") (deps (list (crate-dep (name "rascal_scanner") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1xmwiyrgyq96g6i6ma8gz8ikcmnd1ny929g8zcpc2ljkyr19wd4q")))

(define-public crate-rascal_parser-0.1 (crate (name "rascal_parser") (vers "0.1.2") (deps (list (crate-dep (name "rascal_scanner") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0kl862ix8xgvzzz77xcfxir3h0qb1iz826mqa2h1gzbf15ghqdc7")))

(define-public crate-rascal_scanner-0.1 (crate (name "rascal_scanner") (vers "0.1.1") (hash "192bslka9blv26q0hs99wh4fgf3vnw0a5g8dcrjk70hl78p2fg30")))

(define-public crate-rascal_scanner-0.1 (crate (name "rascal_scanner") (vers "0.1.2") (hash "1l6h7x0jy8rvxv9mlmmkwhdp9sp3682yw6li2r29wk7qhw0xkmpc")))

(define-public crate-rascal_vm-0.1 (crate (name "rascal_vm") (vers "0.1.0") (deps (list (crate-dep (name "rascal_bytecode") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_compiler") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rascal_scanner") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10z03zdzdk8mn14zjh1cqg8sb6wlffx1ar51xijqnidpfviybz8r")))

(define-public crate-rascal_vm-0.1 (crate (name "rascal_vm") (vers "0.1.2") (deps (list (crate-dep (name "rascal_bytecode") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_compiler") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rascal_scanner") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "035q338hzrmij6mw4ndi2hbdz0albiqni7rkhmf7rkjbkpbqnm6f")))

(define-public crate-rascam-0.0.1 (crate (name "rascam") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mmal-sys") (req "^0.1.0-1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.5") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0mkh4qfifv4k103wyxz91n4m7i5i6b2grfp3kmsdl2w5dg8l0z8l") (features (quote (("default") ("debug"))))))

(define-public crate-rascam-0.0.2 (crate (name "rascam") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mmal-sys") (req "^0.1.0-3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rzvb247yf0pws6pbfj5b20ch2mjwg6sg7idjzkmb04l0l24lkr8") (features (quote (("default") ("debug"))))))

(define-public crate-rasch-0.1 (crate (name "rasch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "execute") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "125rvdq1bzkwh4xwx4jakc947qdny67syfl7kdb0wnf08c46507i")))

(define-public crate-rasch-0.1 (crate (name "rasch") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "execute") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0rjzryxbzpm2b1rqqmc3pnhixpf3yrdqqpl1c3azxw9b6m1m8g1b")))

(define-public crate-rascii-0.1 (crate (name "rascii") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1mr67wmmq13c1qnial6fx22v131rm5b1ixys3cjml59gfgrmriih")))

(define-public crate-rascii-0.1 (crate (name "rascii") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1ybrw9np1lnh7i11186mmz67bp6jx3ylgypzi0db7d1ka4nskg9v")))

(define-public crate-rascii-0.1 (crate (name "rascii") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1ldxw8kpvggpmwg54bkil007sxxsx2b4y209pji65snrjysnixpf")))

(define-public crate-rascii_art-0.1 (crate (name "rascii_art") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rgb2ansi256") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0vzl5z6wfpgigbjxdlp2as2rgwkp4yxpz0nd7zdafnsq5wkwdsnx")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rgb2ansi256") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0328mq52pzas3sld8a232ig92yi8lv1g04j5bkfn4kv7nkb2i7sl")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rgb2ansi256") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ddak078994fvy9ibgqk32y67n90b7xxky80k7hhjwd5fmdd0kys")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rgb2ansi256") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11max5qx90ba82da6aa0nyjjwngs61paljb64gwrmmc94bvlvlv2")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rgb2ansi256") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0dskd77a9z8ynjdid3j8gwp3d92da375g7ixfmpjj5r9wbks1r4y")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.4") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rgb2ansi256") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1x29ksjxqnsk9ii91fy2rmfdjl5s131lkmx21cyv8j11b3yylq0a")))

(define-public crate-rascii_art-0.1 (crate (name "rascii_art") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "18nz0z5s545q5lxigp3cxl38akv5wv2yxk4s2pbw7g87sdinz348")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.5") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "13nq7vkd1n692b18m6rgmawdkm0k7nlm161lcj7h6lhv7z3j61mf")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.6") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0m5k538nl85960wnswqgs8mnlvxzg7vy2dnbdmnj2hh9j8m48x0c")))

(define-public crate-rascii_art-0.2 (crate (name "rascii_art") (vers "0.2.61") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0vr66zjmfq4by6zjvpgafi8974hy1kvg8pfwi9kajxpwcny7s0p1")))

(define-public crate-rascii_art-0.3 (crate (name "rascii_art") (vers "0.3.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1754h1lc1qar14rykxqkc0gib3kk40d3vnmyrxhmr10asn2gf42d")))

(define-public crate-rascii_art-0.3 (crate (name "rascii_art") (vers "0.3.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0w0519p0xxj61r77cfmdrcxnc3kqc9alabznnqxcm25mhzbs3442")))

(define-public crate-rascii_art-0.4 (crate (name "rascii_art") (vers "0.4.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0kbmni5iah236w2z8vilmlkqvlgs041axn1kcvb8yfjcn8qa91an")))

(define-public crate-rascii_art-0.4 (crate (name "rascii_art") (vers "0.4.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0fvnnnwf3l095l72gczcf66lhwly76mdwr7yqhvgf1bsqvcry6fb") (yanked #t)))

(define-public crate-rascii_art-0.4 (crate (name "rascii_art") (vers "0.4.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "expanduser") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0mrlha1ghxz2bqs0vvs0lkd0zpbvkpxxc3jys2w3vav9p1w1ywn1")))

(define-public crate-rascii_art-0.4 (crate (name "rascii_art") (vers "0.4.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "expanduser") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1gfp924zfmw81laq8armnk2mb1yrhcc21l1fc0s672iw84ncqx41")))

(define-public crate-rascii_art-0.4 (crate (name "rascii_art") (vers "0.4.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "069ayzgvy5md4bv9ynqb78ri2r2qzw8nx1dlypn83cvhnf6qw953")))

(define-public crate-rascii_art-0.4 (crate (name "rascii_art") (vers "0.4.5") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0r8wddadw4fb2v8h3kpg5hyrlh65x128zbjavprd9d5a6qx5fzz2")))

(define-public crate-rasciigraph-0.1 (crate (name "rasciigraph") (vers "0.1.0") (hash "18dp71md5wz5wbrsqsiwhrcij3fp3narpmi0za5jqh1nlazcqyv5")))

(define-public crate-rasciigraph-0.1 (crate (name "rasciigraph") (vers "0.1.1") (hash "10mc6552k4ci5nz62lmf7yl12b8nqwz9jm2m8z0vfpckgq6nky8h")))

(define-public crate-rasciigraph-0.2 (crate (name "rasciigraph") (vers "0.2.0") (hash "0x6bmmsq2blarlqdvin9yc8r45kdgl0gdlpaxh4vpkd0fqyjg8w9")))

