(define-module (crates-io ra wm) #:use-module (crates-io))

(define-public crate-rawmodel-0.0.0 (crate (name "rawmodel") (vers "0.0.0") (hash "1qy62r5g1bj18k53a9r10l57zb5vw1nfwfiia26xyvgrwbws1jbn")))

(define-public crate-rawmodel-macros-0.0.0 (crate (name "rawmodel-macros") (vers "0.0.0") (hash "1razrv9zj1xa6hmq3nbmpd5fiyf2d4fdq2hkxdaf9xavwnvzk0c6")))

(define-public crate-rawmodel-traits-0.0.0 (crate (name "rawmodel-traits") (vers "0.0.0") (hash "1bg019ki7f559kszy0q2rd0a6f1kcyk4sccrvh2gzhr8al47g2bj")))

(define-public crate-rawmv-0.1 (crate (name "rawmv") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.106") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (kind 0)))) (hash "1vk39s5h7b4pbgm2g4sqv3v47szi3lybvp2n3d8sf3yfg488hrkv")))

(define-public crate-rawmv-0.2 (crate (name "rawmv") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.106") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (features (quote ("combined-flags"))) (kind 0)))) (hash "1x6czh8348gydz2ap7n9srnkyvb083l879bzlvmsqdgp139c6zsc")))

(define-public crate-rawmv-1 (crate (name "rawmv") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (features (quote ("combined-flags"))) (kind 0)) (crate-dep (name "rustix") (req "^0.37.19") (features (quote ("fs" "std"))) (kind 0)))) (hash "1nr6v3r2jl6f2lbyvbc2dmmg0lkmzg4arrkrs04w8lvbq70s0nvm")))

(define-public crate-rawmv-1 (crate (name "rawmv") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (features (quote ("combined-flags"))) (kind 0)) (crate-dep (name "rustix") (req "^0.37.19") (features (quote ("fs" "std"))) (kind 0)))) (hash "1pa07npqx135vjrmj10bn3rc676dbhj1kq84fkylpbxv8g400hj8")))

(define-public crate-rawmv-1 (crate (name "rawmv") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (features (quote ("combined-flags"))) (kind 0)) (crate-dep (name "rustix") (req "^0.38") (features (quote ("fs" "std"))) (kind 0)))) (hash "1w0r4fnygdjayl1ss1vmyj6lq517b70kyjay00mv4hi09ajnd4fd")))

