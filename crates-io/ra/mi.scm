(define-module (crates-io ra mi) #:use-module (crates-io))

(define-public crate-raminspect-0.1 (crate (name "raminspect") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.26.2") (features (quote ("ioctl"))) (default-features #t) (kind 0)))) (hash "1h8911g1aasvh2v7invwpj1dxsl8d5wqjp6cxh59jd2q3dzaqi0p")))

(define-public crate-raminspect-0.2 (crate (name "raminspect") (vers "0.2.0") (deps (list (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "179jpzanr1yvy8wsdc59ndlbklbj78773z6wppl2cbwbfqlbv9rn")))

(define-public crate-raminspect-0.2 (crate (name "raminspect") (vers "0.2.1") (deps (list (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "02sx1aq9x2hssb52ywzfw4fagjr0hlp5gn2v5xvwf1afjr2l45wi")))

(define-public crate-raminspect-0.2 (crate (name "raminspect") (vers "0.2.2") (deps (list (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "0ay3fg87v5qbfh5s38drw4yjz9xy0ycq7wqqny4hrvdca2cc1l5w")))

(define-public crate-raminspect-0.2 (crate (name "raminspect") (vers "0.2.3") (deps (list (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "1b836mvggirsnbhn6hqvl0vrpbis3jacawaxhjipi9mw3ga9arhr")))

(define-public crate-raminspect-0.3 (crate (name "raminspect") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libc_alloc") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0p44nx6sfvs8ccdb4cgx6niafhfnyrwqdqahrnwd1234v4l08nz6")))

(define-public crate-raminspect-0.3 (crate (name "raminspect") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libc_alloc") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0knm24ach0aavwr6167cn05s4jy0qjxpywbk9c32kk54h3flikrp")))

(define-public crate-raminspect-0.4 (crate (name "raminspect") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libc_alloc") (req "^1.0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0hggpdgkir4ln3q29j9x0g61bsqgknpxzm5h3zjs5sl5hx3zfg4l") (features (quote (("libc_allocator" "libc_alloc") ("default" "libc_allocator"))))))

(define-public crate-raminspect-0.5 (crate (name "raminspect") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0zq55ghk3lxhrc8szxzlyrvg5ggxvgjxs9p81rxi6v5ngsv79lkl")))

(define-public crate-raminspect-0.5 (crate (name "raminspect") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1cmiqiqq0ihpgr49nz767ckmi2kkgxwcz22ql3zcaanijqgz6ydm")))

(define-public crate-raminspect-0.5 (crate (name "raminspect") (vers "0.5.2") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "193yr3cgs1yd85kwallcp9x34s8i2hlrcdcz0d21260yhdrkilvb")))

(define-public crate-raminspect-0.6 (crate (name "raminspect") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0664xjngd5d3jpq1l1jdf531mhz96nmxg7lfnf57v4k6q9w0cgd2")))

(define-public crate-raminspect-0.6 (crate (name "raminspect") (vers "0.6.1") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0wv42zx8cwdqlsvbr92mcnbkndnfqqpm9s1syjaqqlvm9c5ayxbd")))

(define-public crate-raminspect-0.6 (crate (name "raminspect") (vers "0.6.2") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "10igzr3pyw0afv87yjcv88lx8lbqxgvnfsh3r76rnf7qfqf5c665")))

(define-public crate-raminspect-0.6 (crate (name "raminspect") (vers "0.6.3") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1nbg16j0adf835anpph0qkbiync8p3ih17b1jhrlyz16q7sy78vp")))

(define-public crate-raminspect-0.7 (crate (name "raminspect") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0s6lzvzmf46rlc9ns9a2idj0rm7zmlsnxwb4f88ax1phg6wdx0c4")))

(define-public crate-raminspect-0.7 (crate (name "raminspect") (vers "0.7.1") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0jn17dj6r6149vjcckbdv3cajdy9iba42n1hrymix3dji41f2c0w")))

(define-public crate-raminspect-0.7 (crate (name "raminspect") (vers "0.7.2") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1sr4kbcd747b34m23098y4ibgrrzv71nwfda1zh0107bg9z2p390")))

(define-public crate-raminspect-0.7 (crate (name "raminspect") (vers "0.7.3") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "05q98pfsgakzi0ayabnv3vq8s0v65283s89imlqmgnwp8a6i4791")))

(define-public crate-raminspect-0.7 (crate (name "raminspect") (vers "0.7.4") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "09xibvc1jgy74zzcmk4kz2aa5v84vnb8sp42cnhrcf2z6jnin3gy")))

