(define-module (crates-io ra yt) #:use-module (crates-io))

(define-public crate-raytracer-0.1 (crate (name "raytracer") (vers "0.1.0") (hash "1xg1vhscdzbss8hzm4ncmmm7w0apdmq2y1g5ys817lqijyy6q6qs")))

(define-public crate-raytracer-0.1 (crate (name "raytracer") (vers "0.1.1") (hash "1fqify53adxml6q52y9c2r3frkmq1fg9vxdrbwfj8gr4isjmfnxl")))

(define-public crate-raytracer-0.2 (crate (name "raytracer") (vers "0.2.0") (deps (list (crate-dep (name "ice-threads") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1plys9gzql06f3a06pwn4yshm6nrs1gz4frjw3a3yns2wbl9gjnx")))

(define-public crate-raytracer-0.2 (crate (name "raytracer") (vers "0.2.1") (deps (list (crate-dep (name "ice-threads") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0b62qwd0ln2na3rmqjh5rmhcyqj9fmhz5v4l85xcqx3pcnyy5j6k")))

(define-public crate-raytracer-0.2 (crate (name "raytracer") (vers "0.2.2") (deps (list (crate-dep (name "ice-threads") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1d40xrrg10axycxf755ivn6gcg3m1wj7zzi21wgllkbm3czskvqg")))

(define-public crate-raytracer-rs-0.1 (crate (name "raytracer-rs") (vers "0.1.0") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (features (quote ("wgpu"))) (kind 0)) (crate-dep (name "egui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "type-uuid") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1difrr2bdmv73f5pj1mb0yz59lkxn6930i0icdsqbdxp2c6qhp7j")))

(define-public crate-raytracer-rs-0.1 (crate (name "raytracer-rs") (vers "0.1.1") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (features (quote ("wgpu"))) (kind 0)) (crate-dep (name "egui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "type-uuid") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0clbn82ndpjqrzd09wc8bmgyywwy643wng711vw9nc5wqklvh0x4")))

(define-public crate-raytracer-rs-0.1 (crate (name "raytracer-rs") (vers "0.1.2") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "05xs0vi6bm28xxkjj66skgznbqncwnffcczpy7rbn9k9h1pl5dsm") (features (quote (("wgpu" "eframe/wgpu"))))))

(define-public crate-raytracer-rs-0.1 (crate (name "raytracer-rs") (vers "0.1.3") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1y86n1msfk0w5lvfywj041l0w9js3wrgl6nklk44m75g5xa9lxwk") (features (quote (("wgpu" "eframe/wgpu"))))))

(define-public crate-raytracing-0.1 (crate (name "raytracing") (vers "0.1.0") (deps (list (crate-dep (name "array_math") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1gpy4dmngya1r7j5hfmhq8fd5za6286qsjjf784n8yfdm5mkh7aw")))

