(define-module (crates-io ra cr) #:use-module (crates-io))

(define-public crate-racros-0.1 (crate (name "racros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0a99ggxlpxr43vphdcrmhwp17zsqfa7lp6sxih515v4gxadv28s7")))

(define-public crate-racros-0.2 (crate (name "racros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0h35kk6wafagjzjj4h47kwvfpv7swd7f1zg92b5589vbprsa6qw2")))

