(define-module (crates-io ra w_) #:use-module (crates-io))

(define-public crate-raw_audio-0.0.1 (crate (name "raw_audio") (vers "0.0.1") (deps (list (crate-dep (name "fon") (req "^0.5") (default-features #t) (kind 0)))) (hash "1q9k3gynyf7xgakxqypz7m17440d7kmsrbc1bfhrjrdgq2ic2xxj")))

(define-public crate-raw_enum-0.0.0 (crate (name "raw_enum") (vers "0.0.0") (hash "0f7p57769gdcp13fvfx97nvwzpydx95yz62hqa7k96byjqn0dcif")))

(define-public crate-raw_enum_macro-0.0.0 (crate (name "raw_enum_macro") (vers "0.0.0") (hash "05zb2ay2fgdw497y4dr4i5j3cp233d7cgr7yylzkgrwkl6lhsi1b")))

(define-public crate-raw_pointer-0.1 (crate (name "raw_pointer") (vers "0.1.0") (hash "0107vcls5ppyfgcb0c0k7dqm133g3ppwm1hbcxc9yqvzpnp125ri") (yanked #t)))

(define-public crate-raw_pointer-0.1 (crate (name "raw_pointer") (vers "0.1.1") (hash "1gh3angckvv8lc8jf17k66jc6hz4qgljaf1n82ygc7b4m7f0w1wd")))

(define-public crate-raw_pointer-0.1 (crate (name "raw_pointer") (vers "0.1.2") (hash "0s5myqls07j4b0j9p9jw04rn0c0v7j41bq2krbn16c9cgm4pi4pm")))

(define-public crate-raw_pointer-0.1 (crate (name "raw_pointer") (vers "0.1.3") (hash "1qdvhph7hpz6bn6rx8zbamq7z2bs145hzqhzzr7brkbk6lz8f7k1")))

(define-public crate-raw_pointer-0.1 (crate (name "raw_pointer") (vers "0.1.4") (hash "1k0x84hr5ls93akdqg8j2n34b6qlzsdbg39f99z34ddp852qqf81")))

(define-public crate-raw_serde-0.1 (crate (name "raw_serde") (vers "0.1.0") (deps (list (crate-dep (name "raw_serde_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n2fxf3ylcg20sm4bg9wpj9agal1r8n5ygkfmxfd7wv51j6sm1vx")))

(define-public crate-raw_serde-0.1 (crate (name "raw_serde") (vers "0.1.1") (deps (list (crate-dep (name "raw_serde_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1a410cb1a57sxswq0lpdx0gl2hl3c4y8nffng3rzn3jsyn4052k2")))

(define-public crate-raw_serde-0.1 (crate (name "raw_serde") (vers "0.1.2") (deps (list (crate-dep (name "raw_serde_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1hl1wfy8mizwx5d87x2amzpn2l3czrbfah0aq4p91qgw3vz7r28l")))

(define-public crate-raw_serde-0.1 (crate (name "raw_serde") (vers "0.1.3") (deps (list (crate-dep (name "raw_serde_derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "147gghywxfypgbfc2nl5lwk8y7f478h74wrazhfwys51x3ki28xw")))

(define-public crate-raw_serde-0.1 (crate (name "raw_serde") (vers "0.1.4") (deps (list (crate-dep (name "raw_serde_derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1rcd2pa5q8nmg7a40k8bjfipjhk9r24hwk8gbf3rnnx3pkzhs91k")))

(define-public crate-raw_serde_derive-0.1 (crate (name "raw_serde_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0g7p1xgl4x7jjrhxpqlry7m1lk9n84kfciwsg3k7g6jk4gk39vfr")))

(define-public crate-raw_serde_derive-0.1 (crate (name "raw_serde_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1p0qvcmr1l70lznhk1r6l70a6qkm91qi4pldp09hv2an110jf8cc")))

(define-public crate-raw_serde_derive-0.1 (crate (name "raw_serde_derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1bl7rljqx01x4r4w1dy37hij6h0168xr4s5c6gh75b9c67g8g6ik")))

(define-public crate-raw_slice-0.0.0 (crate (name "raw_slice") (vers "0.0.0") (hash "1dgb31kzrwl0jx5lcbf2lf5f9mdga0k541ph2jhbh6sdawl4jy90")))

(define-public crate-raw_stdpipes-1 (crate (name "raw_stdpipes") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0inpfqiv7bqqf18bm7cn6fha0l4n3djr450jgl083c3k1vmjxbbn") (yanked #t)))

(define-public crate-raw_str-0.1 (crate (name "raw_str") (vers "0.1.0") (hash "0kv5zd8g3s1pw4jxywcjxp2da4ack4cnnp0grc0dpjny0k9804w4") (features (quote (("std") ("default" "std"))))))

(define-public crate-raw_sync-0.1 (crate (name "raw_sync") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "0.*") (features (quote ("winbase" "winerror" "ntdef" "synchapi" "handleapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "11jg1s2gn8y2qx2z9aj5xrm4khw974i9x0cqh8rdsh5aks1h7x8g")))

(define-public crate-raw_sync-0.1 (crate (name "raw_sync") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "0.*") (features (quote ("winbase" "winerror" "ntdef" "synchapi" "handleapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0f31gvbs5sb33m0srvf004v3rj6xr3qaciihz7781451mj6rrn2j")))

(define-public crate-raw_sync-0.1 (crate (name "raw_sync") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "0.*") (features (quote ("winbase" "winerror" "ntdef" "synchapi" "handleapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "13cch0jfwqknlf7cvw3wrk1y62n8bic28j3kg318f1w61ygzx6dg")))

(define-public crate-raw_sync-0.1 (crate (name "raw_sync") (vers "0.1.4") (deps (list (crate-dep (name "cfg-if") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "0.*") (features (quote ("winnt" "winbase" "winerror" "ntdef" "synchapi" "handleapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0hl47iyfvxrwyniclqgvbf82hwc6i29cghi2c7bcjh7dni1wd6r0")))

(define-public crate-raw_sync-0.1 (crate (name "raw_sync") (vers "0.1.5") (deps (list (crate-dep (name "cfg-if") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "0.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "0.*") (features (quote ("winnt" "winbase" "winerror" "ntdef" "synchapi" "handleapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1k48aq0xgdgh2zq5mzj44rk136jn0111d584qx8hm60zavivsd1a")))

(define-public crate-raw_sync_2-0.1 (crate (name "raw_sync_2") (vers "0.1.5") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt" "winbase" "winerror" "ntdef" "synchapi" "handleapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1jzxwkvpipwm0k7pd229dm4sabanplw2d73q6rbd2cbyl5gv8rzh")))

(define-public crate-raw_terminal-0.1 (crate (name "raw_terminal") (vers "0.1.0") (hash "187rsgqbb91vjl50vyafm8rzxzyb5hrbpg4d9zgc1ql09g8vgnsx")))

(define-public crate-raw_tty-0.1 (crate (name "raw_tty") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1cgqjh9i0q4v1i85rbygzjjaxgi9a6mdhj2x7w6yyja0a3bi5xai")))

(define-public crate-raw_vulkan_handle-0.1 (crate (name "raw_vulkan_handle") (vers "0.1.0") (hash "1nyywz5k4nc8b9b28z8spx8alr2f0mm5dvvvfyq1074447niipal")))

(define-public crate-raw_vulkan_handle-0.1 (crate (name "raw_vulkan_handle") (vers "0.1.1") (hash "1cp4bq1m5rnqkyxdj1rnzcvgin49yd2narmvvzxwqv9py12zdnr3")))

