(define-module (crates-io ra wn) #:use-module (crates-io))

(define-public crate-rawn-0.1 (crate (name "rawn") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "06110vk34i7biqfp2pyqn46cr16slk867nahsdir844dnjg0bm13")))

