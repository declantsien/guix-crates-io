(define-module (crates-io ra qm) #:use-module (crates-io))

(define-public crate-raqm-0.1 (crate (name "raqm") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "raqm-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13h8rhxaykx1xxq0wsl6c9fygcyxpr8rdzaxy0dfik90y7sd74z1")))

(define-public crate-raqm-sys-0.1 (crate (name "raqm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.17") (default-features #t) (kind 1)))) (hash "0ql5am0hn32xnb5qwch2cc2m3r2hsh7hln9wc8aj4h96sss517c3")))

(define-public crate-raqm-sys-0.2 (crate (name "raqm-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.17") (default-features #t) (kind 1)) (crate-dep (name "freetype") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "05c3pcvyah3dqqwzls0qb46vvwpay16ma22vdx87giiqg4i6h1jd")))

(define-public crate-raqm-sys-0.2 (crate (name "raqm-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.17") (default-features #t) (kind 1)) (crate-dep (name "freetype") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "03nsbhr6y6d4xxxfn0qqr02fxmh1qfdxvx56467wk8gfc7mximsc")))

