(define-module (crates-io ra kh) #:use-module (crates-io))

(define-public crate-rakh-0.1 (crate (name "rakh") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1rc7741k9g07ilpy88yqkpyyliyyy9fp131f9vq1fxwr8p73zd62")))

(define-public crate-rakh-1 (crate (name "rakh") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0f5397dlvqgb4yvc0hggag2lnisvah343bxl61cb6faqs1s3a0q0")))

(define-public crate-rakh-1 (crate (name "rakh") (vers "1.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1jy3nlyk1zi92wbnzn2hmyq0qpb9hz4wdvn02qy4q49dkfr9qnws")))

(define-public crate-rakh-1 (crate (name "rakh") (vers "1.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "05balssx3sbfsrk3a6syqhhw25p6cs151m0r9fvy4r0ynr8j0p9f")))

(define-public crate-rakh-1 (crate (name "rakh") (vers "1.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0mfac88j99qbhaw7akpvmgv39rfbzjsmd8dsdmj0v4dw6r28pymh")))

(define-public crate-rakh-1 (crate (name "rakh") (vers "1.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1nraf7h6jy3922f1bd6hwvdkps7j5xslyyabix8znb2n7b630zc2")))

