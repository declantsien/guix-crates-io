(define-module (crates-io ra b-) #:use-module (crates-io))

(define-public crate-rab-core-0.3 (crate (name "rab-core") (vers "0.3.3") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0y4446y49kfgic5l56sq7zhmmd9jpzl9qrybqrkcfbrbvmqzsydp")))

(define-public crate-rab-core-0.4 (crate (name "rab-core") (vers "0.4.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gj2c8is18fws6pm1iy1j3cmph58nmxyr25qmd9ach8jdamylyrl")))

(define-public crate-rab-core-0.4 (crate (name "rab-core") (vers "0.4.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0z4wz7ajaprymqx5mznilk5rqssvks5288dzscrxwxnc8h0g7zv6")))

