(define-module (crates-io ra tb) #:use-module (crates-io))

(define-public crate-ratbaseconfig-0.1 (crate (name "ratbaseconfig") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "03rgga4zwlqiiiyh4h7al9pjhkgn3577gidbz09ki7bw71sw8j54")))

(define-public crate-ratbaseconfig-0.1 (crate (name "ratbaseconfig") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0k6nih134xrw7jshmbpsb6fy5q0z5byxb59svi7gas2rrxqrn53j")))

