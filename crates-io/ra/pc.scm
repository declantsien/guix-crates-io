(define-module (crates-io ra pc) #:use-module (crates-io))

(define-public crate-rapc-0.1 (crate (name "rapc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "13gf0s5y3d628rp8frbw82wv449f3kyw42cprmf4mxayr4ph5hc5")))

