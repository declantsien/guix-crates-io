(define-module (crates-io ra wc) #:use-module (crates-io))

(define-public crate-rawcmd-0.0.0 (crate (name "rawcmd") (vers "0.0.0") (hash "07f420m2ab69sr58c1lskcgbwx5r8v0j1gzachmyfm4dh9sh8nbv")))

(define-public crate-rawcmd-0.1 (crate (name "rawcmd") (vers "0.1.0") (hash "0hlrmnp5z2jh4v01w2dj3gv5rp45ygw7jghfw90lrn01nfix4i9m")))

(define-public crate-rawcmd-0.2 (crate (name "rawcmd") (vers "0.2.0") (hash "0zqpzvz52dy6h51mynchljzfbmb88m3c6vimjpfvy904x5qjvy7i")))

(define-public crate-rawcmd-0.2 (crate (name "rawcmd") (vers "0.2.1") (hash "1bz7i3w5h6nlvz0pl626lcf81cxw8rfgv7kvbf9q9sp4gg61wsda")))

(define-public crate-rawcmd-0.2 (crate (name "rawcmd") (vers "0.2.2") (hash "0jh0236339z0kqibqxfqn8labl8zgmvwh3nhhpw5vlm8xp7w03mg")))

(define-public crate-rawcmd-0.2 (crate (name "rawcmd") (vers "0.2.3") (hash "1qisvjwv3xhn8pancn9ry0c4mq65miy4wvivf89j7xsxljm3xz63")))

(define-public crate-rawcmd-0.3 (crate (name "rawcmd") (vers "0.3.0") (hash "1c3ahmk0mb3zwjk4vrn6gh22yjmcy7hgij49y667l1qqv40yhyqd")))

(define-public crate-rawcmd-0.3 (crate (name "rawcmd") (vers "0.3.1") (hash "18b8cqgmpjmzmbldfhrd2x9kf0fp3lm30g2l6ajrqm2j4rwcy2jy")))

(define-public crate-rawcmd-0.3 (crate (name "rawcmd") (vers "0.3.2") (hash "1a3lns1gcnrn2ng2x2fhss2xp853pzgaxclvyp1j2z8lkqgskwpv")))

(define-public crate-rawcmd-0.4 (crate (name "rawcmd") (vers "0.4.0") (hash "1f0q9g58cba42pd2hfr8il38mfbm2kl5akzsj4r9lfkz8cx7danv")))

(define-public crate-rawcmd-0.4 (crate (name "rawcmd") (vers "0.4.1") (hash "112k6n7csfhjdviy6scay633pfk355r7dkyrl4svqd06qnxbbfzb")))

(define-public crate-rawcmd-0.4 (crate (name "rawcmd") (vers "0.4.2") (hash "11rr44pr24v3fmxlinwsjcad0r2r4h9bcncagwkhl1861mxw6jlv")))

(define-public crate-rawcmd-0.4 (crate (name "rawcmd") (vers "0.4.3") (hash "0dkviz9058c6w9dlv8mx17dvbai4wdjw08ic9rmbfgl0y46m93yn")))

(define-public crate-rawcmd-0.4 (crate (name "rawcmd") (vers "0.4.4") (hash "03j5dkbh90l1vvyvbil30506db3wj88pvz5sbx2ydmygdp90v3gf")))

(define-public crate-rawcmd-0.4 (crate (name "rawcmd") (vers "0.4.5") (hash "09jd9kvm14vskjiw9fjx88xsfwibri1yd1005szxx4zf11fgmpma")))

(define-public crate-rawcmd-0.4 (crate (name "rawcmd") (vers "0.4.6") (hash "1r2xg1izc6w1zjykq3syvwprx8lmm29gkpkwmdnwz59wmq31ffha")))

(define-public crate-rawcmd-0.5 (crate (name "rawcmd") (vers "0.5.0") (hash "14fdl4wjzxk38rl262pkn7a8700aizc59g5p50ms627vj4myxld6")))

(define-public crate-rawcmd-0.6 (crate (name "rawcmd") (vers "0.6.0") (hash "0n42208p2xiibq8r4bvxxa68v1n5ad5jc6wdjwpmq5jw38mkywkn")))

(define-public crate-rawcmd-0.6 (crate (name "rawcmd") (vers "0.6.1") (hash "17vw4vl7pr2rp7q6kddfx9h7rsdz9dl7garkr59q5wr4jhfyycjg")))

(define-public crate-rawcmd-0.6 (crate (name "rawcmd") (vers "0.6.2") (hash "1bw161rczw1i4h8jn0d1dl8fjk72553rc22ddmvvgjjk60ngp2qh")))

(define-public crate-rawcmd-0.6 (crate (name "rawcmd") (vers "0.6.3") (hash "1d2q2j3dv4bff622czfbb0ii8d0w0k46if7im8dxmqg98r3zs1sh")))

(define-public crate-rawcmd_style-0.1 (crate (name "rawcmd_style") (vers "0.1.0") (deps (list (crate-dep (name "rawcmd_utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xxq4mm72l9wzy65wb4fjbxj0q847klryb38qyh18lc8adxw0xs8")))

(define-public crate-rawcmd_style-0.1 (crate (name "rawcmd_style") (vers "0.1.1") (deps (list (crate-dep (name "rawcmd_utils") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "15xbx1bbl00bqc11rnk4llb90ngzsn06h772g7d7s0fml3hplm0b")))

(define-public crate-rawcmd_table-0.1 (crate (name "rawcmd_table") (vers "0.1.0") (deps (list (crate-dep (name "rawcmd_utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1m12ijnp5nmr2d712d1561yc6ksf9rlwbalcc4swqhbzp60dg6l3")))

(define-public crate-rawcmd_utils-0.1 (crate (name "rawcmd_utils") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.3.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0bs1w1vx8m9sqqyvzyg0hj1q9r09glvkwpdih7w796jiml55jn90")))

(define-public crate-rawcmd_utils-0.1 (crate (name "rawcmd_utils") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.3.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "15wybhn3l453dkddl23shdm3j6ajqx28r4dj6hv66zh9xwdmcbh2")))

(define-public crate-rawcode-0.1 (crate (name "rawcode") (vers "0.1.0") (deps (list (crate-dep (name "rawcode_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "071avr2lszn5r305gdhj9gcjd24r5lvm14vc4p483jjkf92i25d6") (features (quote (("no_std") ("derive" "rawcode_derive") ("default" "derive"))))))

(define-public crate-rawcode-0.2 (crate (name "rawcode") (vers "0.2.0") (deps (list (crate-dep (name "rawcode_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1xh8hhs7xyfq9a4y3j8w0kijwwp4rdfbxfzpnl5m10rn8va4japl") (features (quote (("no_std") ("derive" "rawcode_derive") ("default" "derive"))))))

(define-public crate-rawcode-0.2 (crate (name "rawcode") (vers "0.2.1") (deps (list (crate-dep (name "rawcode_derive") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "0sc50wy1l3h9rjhwc8y69n5a251kgdqxk6fxpifcykidnizlvng6") (features (quote (("no_std") ("derive" "rawcode_derive") ("default" "derive"))))))

(define-public crate-rawcode-0.3 (crate (name "rawcode") (vers "0.3.0") (deps (list (crate-dep (name "rawcode_derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0253w32s7fmdf0faqzq3xwpmw0bz8f5k6ajqrx0z25fvc4dz4wq1") (features (quote (("std") ("derive" "rawcode_derive") ("default" "std"))))))

(define-public crate-rawcode-0.3 (crate (name "rawcode") (vers "0.3.1") (deps (list (crate-dep (name "rawcode_derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pg3r17039rv9dy2wnr1l61vpc80bnw0g79jvw4pwjas2dvsflj7") (features (quote (("std") ("derive" "rawcode_derive") ("default" "std"))))))

(define-public crate-rawcode_derive-0.1 (crate (name "rawcode_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v115wkimk0cv460rxiv3z6y40nixcxvfba317fjzzbz7yqya6h4") (features (quote (("default"))))))

(define-public crate-rawcode_derive-0.2 (crate (name "rawcode_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1alw6k9lgvz3z88siij0amafhwnzvdw72wzbyz4p906ijc08r31x") (features (quote (("default"))))))

(define-public crate-rawcode_derive-0.2 (crate (name "rawcode_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pzkj0z04a8wzpwrs6x6yyvr9z27ykjbzlw8dccsdb8l7rcw6r48") (features (quote (("default"))))))

(define-public crate-rawcode_derive-0.3 (crate (name "rawcode_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1azh45kbk1qjjh01vm8v1iillcrxcbz1wzkxigg8kdzaishnzr2l") (features (quote (("default"))))))

(define-public crate-rawcopy-rs-0.1 (crate (name "rawcopy-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "normpath") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "ntfs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1aaywqm5g5i7js058yfhb80h05wds4nchg7aykjzihnbgsiyhal5") (yanked #t)))

(define-public crate-rawcopy-rs-0.1 (crate (name "rawcopy-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "normpath") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "ntfs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "046d306qjl4sfg7kgv1x4ck7f95hx1hgisqc550hx0pdp1mwgi4g") (yanked #t)))

(define-public crate-rawcopy-rs-0.1 (crate (name "rawcopy-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "normpath") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "ntfs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0g879nlcnp1pgnlj04rq3ksd90vd9s3syiig24lb3a6yzd3k158k")))

(define-public crate-rawcopy-rs-0.1 (crate (name "rawcopy-rs") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "normpath") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "ntfs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1vpbwva2vfm2cs920z3lrhq3b6bh1rs9z25mbf80jzzr3bzygx0g")))

(define-public crate-rawcopy-rs-next-0.1 (crate (name "rawcopy-rs-next") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "normpath") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "ntfs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0q7cpn0cnjmc7wy4c37g37sxl5w304cxpvj6swfm4cv61nlidr8s")))

