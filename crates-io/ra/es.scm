(define-module (crates-io ra es) #:use-module (crates-io))

(define-public crate-raestro-0.1 (crate (name "raestro") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "10bbfnxw1bnf9p07d6vwjccbvi8gwamz56lsw08xadfkwiin18f4")))

(define-public crate-raestro-0.2 (crate (name "raestro") (vers "0.2.0") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "0x37zh9n9d21kp1smfllgfh6h522hpyw1bgqkxgp4b39zzb87hcx")))

(define-public crate-raestro-0.3 (crate (name "raestro") (vers "0.3.0") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "09p7fh7x4kmh2kgvm2m8dhr1rzizpa4m87q3zxlwz6xfnb4z0clc")))

(define-public crate-raestro-0.4 (crate (name "raestro") (vers "0.4.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1chlfd362xkf4yjpq6w6cnphrl156k8868ph13sm8gcyq4qr2dsb") (yanked #t)))

(define-public crate-raestro-0.4 (crate (name "raestro") (vers "0.4.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "13qk3bzrv5apv51b687iwdd0g4lf3gwc056xk589wwylfd16sh7p") (yanked #t)))

(define-public crate-raestro-0.5 (crate (name "raestro") (vers "0.5.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1sx02n24mr8dzcyy9f5696b2jfnmv4ad5jbdbqw1dj54sww2gklq")))

