(define-module (crates-io ra yi) #:use-module (crates-io))

(define-public crate-rayimg-0.0.1 (crate (name "rayimg") (vers "0.0.1") (hash "0dpaqsd163yjcra59dqz92j9c5y1pxkcs656458gczygsyj6s2cm")))

(define-public crate-rayimg-0.0.11 (crate (name "rayimg") (vers "0.0.11") (hash "171nbvmawgq8rak66a87acjmf607ddmjwqnc66vn8d9kcikc0msx") (yanked #t)))

(define-public crate-rayimg-0.0.2 (crate (name "rayimg") (vers "0.0.2") (hash "0ycszvkl8s51c8grnnpskjw2xp4mah5mrrd4gkj6w9ngdz9zi1zv")))

(define-public crate-rayimg-0.0.3 (crate (name "rayimg") (vers "0.0.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "09r974y5nihsql2s7h38idgnlh66q4rc1jl8s7jlv0mjnqi8h2xv")))

(define-public crate-rayimg-0.0.4 (crate (name "rayimg") (vers "0.0.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "018xhw7gqw2a7yvdfss096w4451zdlm2j1620f52yfw74a2scb3g")))

(define-public crate-rayimg-0.0.5 (crate (name "rayimg") (vers "0.0.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1irvwxigxzqvalnwni4lvhkihizh27n3zsficdn2klsblvbw2h8y")))

(define-public crate-rayimg-0.0.6 (crate (name "rayimg") (vers "0.0.6") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "10n01apvzcygc4zpqzkn7p80r602qzypkpdkrqsdz9yad3ga1mwb")))

(define-public crate-rayimg-0.0.7 (crate (name "rayimg") (vers "0.0.7") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1gm21y8zz3hwgwig3k0a49gc8231j2hj1dxnq68cda2gqg0c6vl3")))

(define-public crate-rayimg-0.0.8 (crate (name "rayimg") (vers "0.0.8") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zf23cd8168141jy43jnq3nk7i7zn8smhwp5wnz9bwc43gh4p3sl")))

(define-public crate-rayimg-0.0.9 (crate (name "rayimg") (vers "0.0.9") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0di90ic495klcg7vaj815hv01vlgg2zydqkg6hgcwhhi31ig5krd")))

(define-public crate-rayimg-0.0.10 (crate (name "rayimg") (vers "0.0.10") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0lihy35vwr1g6xad0idscdbjwxg45d824sqpc965r1jfha1y7qii")))

(define-public crate-rayimg-0.1 (crate (name "rayimg") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "152vkvqfqjqmc9hqng3l1qzlg1x3f1s1yqr3qi0jpflkvq2jki7b")))

(define-public crate-rayimg-0.1 (crate (name "rayimg") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03ax8h4lz28my0ldpx33zlr486d558xffshjc2jfcvrnmh4xcbhg")))

(define-public crate-rayimg-0.1 (crate (name "rayimg") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0qmmakgc35x1a624pnpn5xjbpyn32raapvyizm95fp1b683jvpg4")))

