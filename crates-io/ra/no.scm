(define-module (crates-io ra no) #:use-module (crates-io))

(define-public crate-rano-0.2 (crate (name "rano") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "09j1snbp0p46vmv4qifs7x94sk613ssqyjwq7khfd2zb8xysvay3")))

