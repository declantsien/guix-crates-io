(define-module (crates-io ra da) #:use-module (crates-io))

(define-public crate-radamsa-0.1 (crate (name "radamsa") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.103") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.2") (default-features #t) (kind 1)))) (hash "1l23mbcbw4j7fa0z72qkc6wwbfyqyrrgwkc7158ld5wcn702h47s")))

(define-public crate-radamsa-0.1 (crate (name "radamsa") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.103") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.2") (default-features #t) (kind 1)))) (hash "1nvd7d834pfm413imp1xciycc76xbvvlqygr2ylmi9z4wf970hkd")))

(define-public crate-radamsa-sys-0.1 (crate (name "radamsa-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)))) (hash "1hfx86340gdbnbx51drpsrvn91yfdx03siccvbvka0af6d50h5z7")))

(define-public crate-radar-0.0.1 (crate (name "radar") (vers "0.0.1") (hash "0by4xa1am8pbhjynshfsz4af89dmx31m9m71hpanyf92wnmjvhqn")))

(define-public crate-radar-rs-0.0.1 (crate (name "radar-rs") (vers "0.0.1") (deps (list (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1183qy9xyzazhc5dbr20hqgy5f6jm2v34ck89ii5x2jbyz9cnmz9")))

(define-public crate-radar-rs-0.0.2 (crate (name "radar-rs") (vers "0.0.2") (deps (list (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zamy035ksgj37zbgmy425jgkm90n43p2sy20lypbb2lid7ksnzn")))

(define-public crate-radar-rs-0.1 (crate (name "radar-rs") (vers "0.1.0") (deps (list (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wwsk1llybi2w9icnl23brlijpl6xg2b2vbwl0wca5yxs2a121zm")))

(define-public crate-radare2-sys-0.1 (crate (name "radare2-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)))) (hash "01zv3mia8g7pylzjbbmi4y29axgw8v7sj8pwzgnh7ai0xkswvs72")))

(define-public crate-radarr-api-rs-3 (crate (name "radarr-api-rs") (vers "3.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "09bdrg8y3skcnc2r3x6n0274agy16p6pihcicqwyd9gyj69832wq") (yanked #t)))

(define-public crate-radarr-api-rs-3 (crate (name "radarr-api-rs") (vers "3.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0skfc20jl60wbn9i1g6bwbcrii4i81c32lx8mpm55gzqsy9h7isi")))

