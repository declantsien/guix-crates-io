(define-module (crates-io ra wz) #:use-module (crates-io))

(define-public crate-rawzeo-0.0.1 (crate (name "rawzeo") (vers "0.0.1") (deps (list (crate-dep (name "circular-buffer") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "103i11vxjb81amwwag0jpd8nqmjhf1s2z1cppgmac83k2j9qy9jh") (features (quote (("default" "bin") ("bin" "circular-buffer" "serialport")))) (rust-version "1.66.1")))

