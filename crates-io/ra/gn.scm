(define-module (crates-io ra gn) #:use-module (crates-io))

(define-public crate-ragnar-0.1 (crate (name "ragnar") (vers "0.1.0") (deps (list (crate-dep (name "bip32") (req "^0.5.1") (features (quote ("bip39"))) (default-features #t) (kind 0)) (crate-dep (name "bip39") (req "^2.0.0") (features (quote ("rand_core"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1k950lx67sbk2bx1ryf02rvdl6y2ad6n6scwwkrh3a06fvv7nv03")))

(define-public crate-ragnar-0.1 (crate (name "ragnar") (vers "0.1.1") (deps (list (crate-dep (name "bip32") (req "^0.5.1") (features (quote ("bip39"))) (default-features #t) (kind 0)) (crate-dep (name "bip39") (req "^2.0.0") (features (quote ("rand_core"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0331mqad04ymn8zwry6b5knhm6d73hdm2yxjg3m4zzsd64w01mz4")))

(define-public crate-ragno-0.1 (crate (name "ragno") (vers "0.1.0") (hash "1h4j6sh0bjfvq86qnsgm7wmmyn2yjj77mfwan3c44dzkqp3vid1s")))

