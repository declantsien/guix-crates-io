(define-module (crates-io ra #{6t}#) #:use-module (crates-io))

(define-public crate-ra6t1-0.2 (crate (name "ra6t1") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1f3hyhrskbj4gk13bx27b4vkdi5hw08zdav25w7qjj2nx8mm8x97") (features (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ra6t2-0.2 (crate (name "ra6t2") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1yx4mx4ng45lbaazdzyqwf6xz25qxccjx0mw84vngi10ik7haj75") (features (quote (("rt" "cortex-m-rt/device"))))))

