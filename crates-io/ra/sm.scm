(define-module (crates-io ra sm) #:use-module (crates-io))

(define-public crate-rasm-0.0.0 (crate (name "rasm") (vers "0.0.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.58") (default-features #t) (kind 0)))) (hash "1kiknd205ylbczfxqf01x2s3s156a9h7nlw25ihdsw3bzkylx94z")))

