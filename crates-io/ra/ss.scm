(define-module (crates-io ra ss) #:use-module (crates-io))

(define-public crate-rass-0.2 (crate (name "rass") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "2.*") (default-features #t) (kind 0)) (crate-dep (name "gpgme") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "2.*") (default-features #t) (kind 0)))) (hash "1bcmbd29lf2m2vw16q04s4xi808l0kjd42s789pwzg5sn4g6z94l")))

(define-public crate-rassert-1 (crate (name "rassert") (vers "1.0.0") (hash "0fl3kcbjy72gzsq42ajyizchi587932hbykzcgyg6sglgs3wq3cw")))

(define-public crate-rassert-1 (crate (name "rassert") (vers "1.1.0") (hash "0lbhn5h28wgv3mwy90z0m2v9zyihr3hx5p38pn1x65gx9pjpnhza")))

(define-public crate-rassert-1 (crate (name "rassert") (vers "1.2.0") (hash "0c8y43rwcp68f3m19md4jwn6gk4kkc6z11774k3jd49k5ikyniv9")))

(define-public crate-rassert-1 (crate (name "rassert") (vers "1.3.0") (hash "0ynbrcacxcm1vxgrspg12h92iclv00lswpbwgmkamrcpv0g4ddx3")))

(define-public crate-rassert-rs-1 (crate (name "rassert-rs") (vers "1.0.0") (hash "1bsnrvspvdss51zz82ziic1ikgg644z5517vi24aghhsvaayzc0z")))

(define-public crate-rassert-rs-2 (crate (name "rassert-rs") (vers "2.0.0") (hash "1m2gya8pnb6i372mk295iv5pk9hs5r4cx4pdvhh9jz768v5fzb3p")))

(define-public crate-rassert-rs-3 (crate (name "rassert-rs") (vers "3.0.0") (hash "167h6jql163m80hlh4bdnbqyw6zvhv79kv2akwj46hjfrq7qy9lv")))

(define-public crate-rasswd-0.1 (crate (name "rasswd") (vers "0.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "06fan49wqv7xykgj5f0hz07c10q6vym1nrck8y3lxyn8mfih8ach")))

