(define-module (crates-io ra me) #:use-module (crates-io))

(define-public crate-ramen-0.0.0 (crate (name "ramen") (vers "0.0.0") (hash "1486sqypp3996s52vjgpskrymm3mvk0prw4rnjr57f78jqb0a5hl")))

(define-public crate-ramen-0.0.1 (crate (name "ramen") (vers "0.0.1") (hash "0915306pw3qa7x6a97alpdr7vks1l490h0647r3ml3pqc3yqqhki")))

(define-public crate-ramen-0.0.2 (crate (name "ramen") (vers "0.0.2") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1bj0j3vgqk3inc828ds7vpjvdddji6dbb4lkhjk0f8y7ivmgk2x3") (features (quote (("parking-lot" "parking_lot") ("default"))))))

(define-public crate-ramenon-0.1 (crate (name "ramenon") (vers "0.1.0") (hash "0yb8gj97x5bxy0l2qs4fv7x0f5qfp0glfkjyay8pfyvcrijpbaf8") (yanked #t)))

(define-public crate-ramenon-0.1 (crate (name "ramenon") (vers "0.1.1") (hash "0w05fn1lrvnnyp97wz1nh37x0hnziciwr1b633j8bvikdv3bixhy") (yanked #t)))

(define-public crate-ramenon-0.1 (crate (name "ramenon") (vers "0.1.2") (hash "1gj9hzkz43wrcbbdn00745l5n3h5l7xs6nk3xz60gni0yxx9jk2l") (yanked #t)))

(define-public crate-ramenon-0.1 (crate (name "ramenon") (vers "0.1.3") (hash "1i7c90pimhdf3kf4zd3ms7a8vsx7r0mc7vfjxww92jxz671z7vg1")))

(define-public crate-ramer_douglas_peucker-0.1 (crate (name "ramer_douglas_peucker") (vers "0.1.0") (deps (list (crate-dep (name "legasea_line") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0byfim1n3sn66ad5qnj5rhh96dfiy7ihxd2af6rcnds4hnd9xci2")))

(define-public crate-ramer_douglas_peucker-0.2 (crate (name "ramer_douglas_peucker") (vers "0.2.0") (deps (list (crate-dep (name "legasea_line") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0d6xrkfxfca2dngl6lg224clipi61nqymdm0h7pc92gzf9xx929i")))

(define-public crate-ramer_douglas_peucker-0.2 (crate (name "ramer_douglas_peucker") (vers "0.2.1") (deps (list (crate-dep (name "legasea_line") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "19h0gzmcf9j28rslm9qc5wn9lha7hxfxikma62dpspi9xpf051ha")))

(define-public crate-ramer_douglas_peucker-0.2 (crate (name "ramer_douglas_peucker") (vers "0.2.2") (deps (list (crate-dep (name "legasea_line") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "10z7bjwsiyi94zxiii8wqnr5k6ywxccva4gfms3w4x8f6s6jmvrz")))

