(define-module (crates-io ra y_) #:use-module (crates-io))

(define-public crate-ray_tracing_core-0.1 (crate (name "ray_tracing_core") (vers "0.1.0") (deps (list (crate-dep (name "glm") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "18xzb6gyn1xizz46lz6wngrkdm3dzbzw3dlycgfr00mgp6d7xbkj")))

(define-public crate-ray_tracing_core-0.1 (crate (name "ray_tracing_core") (vers "0.1.1") (deps (list (crate-dep (name "glm") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1v4v58mqq82ncqp42i8v50apwv0f48akp72b927qhry0wb70smi8")))

(define-public crate-ray_tracing_in_one_weekend-0.1 (crate (name "ray_tracing_in_one_weekend") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1wmv3ddmicab2a613535z8r6xq2ig0p28w6bypzkaqiccbzils4k")))

(define-public crate-ray_tracing_show_image-0.1 (crate (name "ray_tracing_show_image") (vers "0.1.0") (deps (list (crate-dep (name "ray_tracing_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ray_tracing_utility") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "show-image") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0fd4n03pvz8fs57v9d79kijkcsnrs4l1kp8mlz7wwqh3jp6wlkiy")))

(define-public crate-ray_tracing_show_image-0.1 (crate (name "ray_tracing_show_image") (vers "0.1.1") (deps (list (crate-dep (name "ray_tracing_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ray_tracing_utility") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "show-image") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1axpshi9bf3j13xbz55dr2qf9z87lrckj7vj0z2s4a15i62l013j")))

(define-public crate-ray_tracing_utility-0.1 (crate (name "ray_tracing_utility") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "ray_tracing_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rm60p1sppvzr6qskgfrs534vzg6nvv2l297x8p49l5ndcxyfv64")))

(define-public crate-ray_tracing_utility-0.1 (crate (name "ray_tracing_utility") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "ray_tracing_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xjnyp4mw6ss1iw8nab9mvp2d5wvy6a6lgr33j7liib70q217gzl")))

