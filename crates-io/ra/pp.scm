(define-module (crates-io ra pp) #:use-module (crates-io))

(define-public crate-rapper-0.1 (crate (name "rapper") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1zxpy24jn6799drrzhli3bw4cys22sxyd2cri5rll4xa78zsbbl7")))

(define-public crate-rapper-0.1 (crate (name "rapper") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "162dh8zwnn2nbwm87j8khpj5xp41qsskh68i4hzyqzm4b7vw93l1")))

(define-public crate-rapper-0.1 (crate (name "rapper") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0ilsxbjpqj0w1ajxga2gd7j1hblk1pqbdvwkfc68hwg9vmxs1gyq")))

(define-public crate-rapper-0.1 (crate (name "rapper") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1acmjyf33jv9ab494ch62sxnkw50knp76wvj1kz23lvad9g0a6vp")))

