(define-module (crates-io ra _t) #:use-module (crates-io))

(define-public crate-ra_traits-0.0.1 (crate (name "ra_traits") (vers "0.0.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0xq7ix2svif1lvmcb5zdcamhnkl4i976x2hv9v9vg5qbj75j3xcc")))

