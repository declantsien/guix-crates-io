(define-module (crates-io ra io) #:use-module (crates-io))

(define-public crate-raio-0.1 (crate (name "raio") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1j4zwmcrviv1rxva87zgd47al6y7x566r1w4gkjyjfl0nl61akld")))

(define-public crate-raio-0.2 (crate (name "raio") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "async-trait") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "deadpool") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "packs") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "packs") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "packs-proc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "0a4iyxkfasxx8061dkkimcxxcks05j0vrnf9yxpkhagphdhkk0l1")))

