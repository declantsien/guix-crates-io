(define-module (crates-io ra mn) #:use-module (crates-io))

(define-public crate-ramn-currency-0.4 (crate (name "ramn-currency") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0gdzd16vaij2b3n8y3m0aha6shacc6ix6j083b75d5jpmh7bw853") (yanked #t)))

(define-public crate-ramn-currency-0.4 (crate (name "ramn-currency") (vers "0.4.1") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "1xj432bwnm9fd2bgng4d5pcvp4nnzr31kbqjic7d7frain7p2yqs") (yanked #t)))

