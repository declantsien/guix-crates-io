(define-module (crates-io ra xb) #:use-module (crates-io))

(define-public crate-raxb-0.2 (crate (name "raxb") (vers "0.2.0") (deps (list (crate-dep (name "raxb-derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0gx5qa3qzsjsc1xf9lj9xmx8pxhllj8svdicsdyyaik8v072mbcb") (rust-version "1.78.0")))

(define-public crate-raxb-derive-0.1 (crate (name "raxb-derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0nfh5k4viipx5x4lrdsqydzq1670wkmykpf3xwbakzkqw6vvsrjs") (rust-version "1.78.0")))

(define-public crate-raxb-derive-0.2 (crate (name "raxb-derive") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1yl0nsfa1wrv3d495mbzvrl0g8v1xx0wi64ggl4422cz7rgvrm4x") (rust-version "1.78.0")))

(define-public crate-raxb-validate-0.1 (crate (name "raxb-validate") (vers "0.1.0") (hash "1z8b83584jr9y9jvk8fnx8mhw5c8gjwdrl95dn51s3b90f5mw3yc") (rust-version "1.78.0")))

(define-public crate-raxb-validate-0.2 (crate (name "raxb-validate") (vers "0.2.0") (hash "1g16cs0ra2glfhp3gp1winyk6w5sjbbyxlgzs7jyc5hb67171kbr") (rust-version "1.78.0")))

(define-public crate-raxb-xmlschema-0.1 (crate (name "raxb-xmlschema") (vers "0.1.0") (hash "0nqgxqscsmyss22h7hblnm5zgpmxx8f4pq2vxkcbviwa0h5mlgxl") (rust-version "1.78.0")))

(define-public crate-raxb-xmlschema-0.2 (crate (name "raxb-xmlschema") (vers "0.2.0") (hash "007wwfhns6whdn48p445pyl16jh0wp3ajf65ksk4rc71666zcsfc") (rust-version "1.78.0")))

