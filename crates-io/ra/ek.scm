(define-module (crates-io ra ek) #:use-module (crates-io))

(define-public crate-raekna-0.1 (crate (name "raekna") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "raekna-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raekna-compute") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raekna-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raekna-storage") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raekna-ui") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gf3fr4ywmmyd8cxbz0ny14alizn1fwdb75f1hchj2lpa6r0vwvw")))

(define-public crate-raekna-0.1 (crate (name "raekna") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "raekna-common") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-compute") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-storage") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-ui") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17h9db6n8v7079q2qxq536jyv0byqjwni3rmr299lfm2pxczkvnh")))

(define-public crate-raekna-0.2 (crate (name "raekna") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-compute") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-parser") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-storage") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-ui") (req "^0.2") (default-features #t) (kind 0)))) (hash "00ijv4bkql4sigsb4b7794z1xsr9gggry1wk85954lsfw38kmapn")))

(define-public crate-raekna-0.2 (crate (name "raekna") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-compute") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-parser") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-storage") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "raekna-ui") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zpjnw0sndsc5kjsd87z114kdk8xaz89lh6n9mqq830dwb9afkf6")))

(define-public crate-raekna-common-0.1 (crate (name "raekna-common") (vers "0.1.0") (hash "1yinxzcr29yk5g6xz6vxpnvgi8b6x6px5i0y01npwli49ydq6sh6")))

(define-public crate-raekna-common-0.1 (crate (name "raekna-common") (vers "0.1.1") (hash "0xr2g7cb9scfq26qa0xk3achjv0fqjj8ljxwjc5ldxbilwgypkl9")))

(define-public crate-raekna-common-0.2 (crate (name "raekna-common") (vers "0.2.0") (hash "1bap57kzmsn6qpwm45z9p8a9w1yxkzzlx6989lp5x6qagdjm2kj3")))

(define-public crate-raekna-common-0.2 (crate (name "raekna-common") (vers "0.2.1") (hash "0n25id5qzmsivzy55wjfbvc2yw8xanl9l4sgr84kjg3v199g8w1v")))

(define-public crate-raekna-compute-0.1 (crate (name "raekna-compute") (vers "0.1.0") (deps (list (crate-dep (name "raekna-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wm24bsss96a68yna3psygybygfsx9gx1gha2bjripl2pr2m94qb")))

(define-public crate-raekna-compute-0.1 (crate (name "raekna-compute") (vers "0.1.1") (deps (list (crate-dep (name "raekna-common") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sb65vfn9dlix6kxp70xy5pb2k5fvrnq8d3pry46ihnm0h8725ph")))

(define-public crate-raekna-compute-0.2 (crate (name "raekna-compute") (vers "0.2.0") (deps (list (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)))) (hash "04gz31mahk4bjc4v8c357ykz482jlkifj2krskn21gyr43n1l9x2")))

(define-public crate-raekna-compute-0.2 (crate (name "raekna-compute") (vers "0.2.1") (deps (list (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)))) (hash "00a8nd9xwws5xkq134kc6wgmg121p5mkp3s8zplfr5nmnf6wnwgy")))

(define-public crate-raekna-parser-0.1 (crate (name "raekna-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kjdayn0lq49rnmw56lw0yl7nsymh66q2qgx3blwkagr8s1439yj")))

(define-public crate-raekna-parser-0.1 (crate (name "raekna-parser") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y4f0g4dxj0jrjy8572dxhgycsgsc00wajsi87nikv750bxrshpy")))

(define-public crate-raekna-parser-0.2 (crate (name "raekna-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)))) (hash "099nw33v2hx7daaa82vyd8zbnr2v3g37kl9mdaixx8j9awhd4jl0")))

(define-public crate-raekna-parser-0.2 (crate (name "raekna-parser") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ywfa98650nq91hpf4lxd8pfa9br6fs5h3nc1lwyxgdycairlar6")))

(define-public crate-raekna-storage-0.1 (crate (name "raekna-storage") (vers "0.1.0") (deps (list (crate-dep (name "raekna-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fih58zg891hq18xg3wkicmgvnvgi49c5fbarxk5k9yz027jqx0m")))

(define-public crate-raekna-storage-0.1 (crate (name "raekna-storage") (vers "0.1.1") (deps (list (crate-dep (name "raekna-common") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pj90akaqvjywnwzsbamj1jpcd3y7rwbbccrjz7c74fy2jvnnva6")))

(define-public crate-raekna-storage-0.2 (crate (name "raekna-storage") (vers "0.2.0") (deps (list (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g40sgf4yik3a2lw71lv7wr8kd1iz2n7j5da71c7x9pwi27gph69")))

(define-public crate-raekna-storage-0.2 (crate (name "raekna-storage") (vers "0.2.1") (deps (list (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wyppck32xv5y93v5ifd3hqvzr999iy3zbr3wp7ym9qy32s8549r")))

(define-public crate-raekna-ui-0.1 (crate (name "raekna-ui") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "wgpu_glyph") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 0)))) (hash "198mqx1ia21grg3slypxn44g9npggrhmw102ps6pmv76fvicwl34")))

(define-public crate-raekna-ui-0.1 (crate (name "raekna-ui") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "wgpu_glyph") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 0)))) (hash "134pjnwfz7wsi5bpgz7z7n892jf7fmqjq1rfg873r90gxj0km81k")))

(define-public crate-raekna-ui-0.2 (crate (name "raekna-ui") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "wgpu_glyph") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 0)))) (hash "0nd7jjd74im68rakigrp5n8dipmnf3kd9cmyg66rh3nszb77608j")))

(define-public crate-raekna-ui-0.2 (crate (name "raekna-ui") (vers "0.2.1") (deps (list (crate-dep (name "bytemuck") (req "^1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "raekna-common") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "wgpu_glyph") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.27") (default-features #t) (kind 0)))) (hash "1ln1r9ygnk2v4xlp2j7nx8fyjhax280l51xvz34096r78afmrj0x")))

