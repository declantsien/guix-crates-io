(define-module (crates-io ra un) #:use-module (crates-io))

(define-public crate-raunch-1 (crate (name "raunch") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "0zrin15n9ljg8a54bivhwiz96fawh2mng9031l5js76xfbfxiiw6")))

(define-public crate-raunch-1 (crate (name "raunch") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "1ia9mfxpsvpdr0dzcr1kb6q911nmyw26x0jwbz2ikm6ci5kj4rqs")))

