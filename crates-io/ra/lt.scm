(define-module (crates-io ra lt) #:use-module (crates-io))

(define-public crate-ralte32-0.1 (crate (name "ralte32") (vers "0.1.0") (hash "15a5ydpmqlnwkkga329yby95s0fmmq976l01vbjjsh3n8ch3hiqv")))

(define-public crate-ralte32-0.1 (crate (name "ralte32") (vers "0.1.1") (hash "0d5avwwwx883f27bqagakwsbbrq9r1phshyrqghwdhrhb008bxq9")))

(define-public crate-ralte32-0.1 (crate (name "ralte32") (vers "0.1.2") (hash "00vil0v8fs10lviy8cipg86967a9262spwkvc90labzi9jgr0r6a")))

(define-public crate-ralte32-0.1 (crate (name "ralte32") (vers "0.1.3") (hash "1ri90bnrrkkszxj6wd3sqmzkfhqvl38wjdz1kh6ibz4qd3czhq5m")))

