(define-module (crates-io ra ki) #:use-module (crates-io))

(define-public crate-raki-0.1 (crate (name "raki") (vers "0.1.0") (hash "0nh6m62bdqwayi2mizrwxhxvxk8lzlpvyvarqy9341nrzy8vjz1x")))

(define-public crate-raki-0.1 (crate (name "raki") (vers "0.1.1") (hash "0mqpjilvp71vd4abnrpj47wmj3hv5dqwnd0x7jyxxa2ysjabmpzy")))

(define-public crate-raki-0.1 (crate (name "raki") (vers "0.1.2") (hash "09advc9i6w70b6av4pd7qqyb0rsi6vqhxygzm7pd2n5lxf5xxdza")))

(define-public crate-raki-0.1 (crate (name "raki") (vers "0.1.3") (hash "0ln046y1rr18q4ka9wh6lfflf4x5z3pn1vwz9cy4sn64m1h4nn20")))

(define-public crate-rakino-0.1 (crate (name "rakino") (vers "0.1.0") (hash "0yjsds0vkqc6xdi6ybnmhpnck6q8lpxn5wnbdli858jahrqzha2p")))

