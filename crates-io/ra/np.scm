(define-module (crates-io ra np) #:use-module (crates-io))

(define-public crate-ranpha-0.1 (crate (name "ranpha") (vers "0.1.0") (deps (list (crate-dep (name "bpaf") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "qrcode-generator") (req "^4.1.6") (default-features #t) (kind 0)))) (hash "1ffnp56cc33ipqcy5nmn1fp9wlgf9k1zw70c9cihvsdnbyp9xpab")))

(define-public crate-ranpha-0.1 (crate (name "ranpha") (vers "0.1.1") (deps (list (crate-dep (name "bpaf") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "qrcode-generator") (req "^4.1.6") (default-features #t) (kind 0)))) (hash "1yw613vsp3w45gi3ihqvx61s6big6m6467ymvi1y9gbm05mrg7a0")))

