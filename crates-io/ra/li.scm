(define-module (crates-io ra li) #:use-module (crates-io))

(define-public crate-raliguard-0.1 (crate (name "raliguard") (vers "0.1.0") (hash "1sajgjm4ixqvniigmbh572msfjmcrdlwkqafr9wq4zx8drcydry8")))

(define-public crate-raliguard-0.1 (crate (name "raliguard") (vers "0.1.1") (hash "134y983ndkby718jyw4zgksbcijf6v8lyyzq9m1w6bl0malxwkic")))

(define-public crate-raliguard-0.1 (crate (name "raliguard") (vers "0.1.2") (hash "0dmk28ir5ipwghq31l74dr7sznim0692wibfkjxla9q6vagv6prx")))

(define-public crate-raliguard-0.1 (crate (name "raliguard") (vers "0.1.3") (hash "01jw83kggdip3zjqn445rv1z128ihfa4a228rfwjs0h48qkc2drm")))

(define-public crate-ralik-0.0.1 (crate (name "ralik") (vers "0.0.1-alpha.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "my_serde") (req "^1") (optional #t) (kind 0) (package "serde")) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0j7ki592722k1npzrh3cvqwq4mdgk0szgycsijvzzmbnczdvmcgy") (features (quote (("serde" "my_serde" "num/serde") ("default"))))))

(define-public crate-ralik-repl-0.0.1 (crate (name "ralik-repl") (vers "0.0.1-alpha.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "build-info") (req "^0.0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "build-info-build") (req "^0.0.19") (default-features #t) (kind 1)) (crate-dep (name "ctrlc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "ralik") (req "^0.0.1-alpha.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1adm7vzi542zxrkzchvnnkwqvpjgx8n42cc9sfd52szf9676swja")))

