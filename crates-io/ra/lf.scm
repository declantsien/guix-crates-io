(define-module (crates-io ra lf) #:use-module (crates-io))

(define-public crate-ralf-0.1 (crate (name "ralf") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "raft") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1abyw44068phdddbzxydq1y4nvwha47lzai3fpf3nv97wf8higrr")))

(define-public crate-ralf-0.1 (crate (name "ralf") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "raft") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vmkfvq4x2d8pairid20vjr6kg44kvw7v9hbqpggfvvi0vkp8qlj")))

