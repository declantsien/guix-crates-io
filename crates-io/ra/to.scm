(define-module (crates-io ra to) #:use-module (crates-io))

(define-public crate-rato-0.1 (crate (name "rato") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (kind 2)) (crate-dep (name "hashbrown") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "19cz1mvxhyabqfjn8acxdclb4hcyzy7q4y24zqw6rqflhkk7wvm0")))

(define-public crate-rato-0.1 (crate (name "rato") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1kp0j86xyvspxrhjz9y7xw94r1qlb7ilflh1nf61b94rk7gc53kb")))

(define-public crate-rato-0.1 (crate (name "rato") (vers "0.1.2") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "14pppmk3bckmyjpsb365al2jr3jqk06k4vzb3yfyiv38iff6hqd4")))

