(define-module (crates-io ra rg) #:use-module (crates-io))

(define-public crate-rargs-0.2 (crate (name "rargs") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lwx7x52nkszf2jkllak45lbphgjkxry4kr7r0sbsgwpbbq5dv76")))

(define-public crate-rargs-0.2 (crate (name "rargs") (vers "0.2.2") (deps (list (crate-dep (name "assert_cli") (req "^0.5.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "0a7llvym9286bfmiiqb5cyam95ps0j57ds2gjwj5x5hxpm21grz1")))

(define-public crate-rargs-0.3 (crate (name "rargs") (vers "0.3.0") (deps (list (crate-dep (name "assert_cli") (req "^0.6.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0inqvlys44las6nzpifw7hw1fci1rz4wwz5v81sl4d6rvz2v2abj")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.0") (hash "05anw8lw7ipg4fcljghgzskf4pw97yaqyj8nmr7nysn23g8lh1g2")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.1") (hash "0s2cyrbvq4xqwzq62jnw8cdfkhd44h3ap7c95gw1mdhk1xj3v49l")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.3") (hash "0gc53s79sm9knskhx7b488xrylcadicij2z3bkfq6bdi59afnjx9")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.4") (hash "13ncf4mc13v13qyrd647kw01n7vjc5vqaaiglr08zszv6x7z0rf0")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.5") (hash "1xg101956pdnvs2s0bk8zjp9h1dg6kv8p71xydp6n5cjrrlsp6h5")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.6") (hash "1xfyv000q0mgkjg8jaiahldb25kgb7zykbc7fm10nd6gb7ssclh1")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.7") (hash "0slpjam6mmnw19y8lrvqicr10cr7n27872h36a2jxdz4xsknm51f")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.8") (hash "1wrjfzi4hx51655gg44c7w0yqn7n8w03c09f5mdywgwvl9k1kyiz")))

(define-public crate-rargsxd-0.1 (crate (name "rargsxd") (vers "0.1.9") (hash "1n9l3rixv23405bw8q5ssl14xwyl5vpfw21szdw2l941589cwgv2")))

(define-public crate-rargsxd-0.2 (crate (name "rargsxd") (vers "0.2.0") (hash "0w3hwi3x8g3jp7y15v85rbxawkryfgdmjz75fvp1g4akj93kvb1k")))

(define-public crate-rargsxd-0.2 (crate (name "rargsxd") (vers "0.2.1") (hash "0z6idc1qjm7mi6w8hnrvq4v4bi7pqi171lm6kfrzv2nz6csjyjxj")))

(define-public crate-rargsxd-0.2 (crate (name "rargsxd") (vers "0.2.2") (hash "109kdnfndhlj6q0anwhlzaiw6zb9sdvxjqrzscyv0wy7fpba7v1l")))

(define-public crate-rargsxd-0.2 (crate (name "rargsxd") (vers "0.2.3") (hash "0rxrmfqzv1ggfxh5wcr3n05gi1h152c57zc3f8m7lwxqnip3ppbv")))

(define-public crate-rargsxd-0.2 (crate (name "rargsxd") (vers "0.2.4") (hash "18fwbgz7bdj1rgplyrcphzcqxr8bl5aa84mnyqf79d1rgwxvzz4p")))

(define-public crate-rargsxd-0.2 (crate (name "rargsxd") (vers "0.2.5") (hash "02mwbg8116br88qmpj3ffw24dv7wpsq21w2s7k7lw8g2jnphvx2y")))

(define-public crate-rargsxd-0.2 (crate (name "rargsxd") (vers "0.2.6") (hash "1vg7njihyskd8iiki2z8cc61xckmnl6b9lmi6hnndg4y0dsv0k75")))

