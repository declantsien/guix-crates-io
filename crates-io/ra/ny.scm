(define-module (crates-io ra ny) #:use-module (crates-io))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.1") (hash "1d2k31kyfgdy7qaprz39qwn42k21rcabadrb62ibhh0jbwsg9qd9")))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.2") (hash "0akavrz6hhqhlx2473jmbcwfiv6br98x355jcmzilnn1i9f44jsk")))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.3") (hash "0jbf2bpinz9jm1qqcai2saib7n11xfcl7x4zgd1hd80fxlswxlc9")))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.4") (hash "0rplicalp6vgb6x8blscnaf56l7amz8ymhfpscd6p00v4qb82980")))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.5") (hash "0dy5q558xpq428nny7vjbg9lz29257xl4cq3kk5d7cfrwhsicdk5")))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.6") (hash "1dwndqd2pfd4a2rmh82wg3nhy8s6s5g888k4i20mqw9byvxxvwpi") (features (quote (("url") ("default" "url") ("b255"))))))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.7") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0pwnb87vwkwcr3f265cxjdxxxdvsxlan6pjlzxslnzd608fkb1z1") (features (quote (("url" "strany") ("strany") ("default") ("b255"))))))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.8") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1swmkxz6zd41sxjcqii5cx3rp0b8nn4fhw163r044rbpjb75pxzc") (features (quote (("url" "strany") ("strany") ("default") ("b255"))))))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.9") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0bnpfj5ic5nsx8x76gzy5zafwj19mdd8b9k6dby23h2dycjmfk18") (features (quote (("url" "strany") ("strany") ("default") ("b255"))))))

(define-public crate-rany-0.1 (crate (name "rany") (vers "0.1.10") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0gr2ilrm2nbkifqn5470f3ysicdmx5578zxbr19arqzqvdsnpbjg") (features (quote (("url" "strany") ("strany") ("default") ("b255"))))))

