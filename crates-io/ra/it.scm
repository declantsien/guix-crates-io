(define-module (crates-io ra it) #:use-module (crates-io))

(define-public crate-raitech2020_guessing_game-0.1 (crate (name "raitech2020_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1306gq78lfx2cly5i4d56arljvxzm706q2vvnbxar6bl95di6ay0")))

(define-public crate-raitech2020_guessing_game-0.1 (crate (name "raitech2020_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1n79ii0avbk8vzja7wciihqi9s4y7z3p344mgli0044aghly1gjg")))

(define-public crate-raith-common-0.1 (crate (name "raith-common") (vers "0.1.0") (hash "1dy7573z5r27jdl9aqzc7il5pangp1cg763laffznh54l53kcq54")))

