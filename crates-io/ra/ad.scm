(define-module (crates-io ra ad) #:use-module (crates-io))

(define-public crate-raad-0.1 (crate (name "raad") (vers "0.1.0") (hash "0xglsgwpszmj4xg16wnz6q8qpg6iaqz8lap59hc6kfn4wlbgi3gl") (rust-version "1.79")))

(define-public crate-raad-0.1 (crate (name "raad") (vers "0.1.1") (hash "1ya85kydxamiqjijk1vkmwbg4l4qpas87xyh72344w4fqabj6mj0") (rust-version "1.79")))

(define-public crate-raad-0.1 (crate (name "raad") (vers "0.1.2") (hash "0qpdfc1yhsrnzmn617lyx23w5wxyxrh3950qagadvyyh3v66xa1k") (rust-version "1.78")))

