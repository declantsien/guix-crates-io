(define-module (crates-io ra ms) #:use-module (crates-io))

(define-public crate-rams-0.1 (crate (name "rams") (vers "0.1.0") (deps (list (crate-dep (name "dioxus") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "dioxus-web") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rams-proc-macros-internal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zsripcak4jka61var01di8c2iq6g69jig9rdhv8l2pfh8cqqp3i") (yanked #t)))

(define-public crate-rams-core-0.1 (crate (name "rams-core") (vers "0.1.0") (hash "1qcda37xslm29gd5v7zp0v50jxd940d23fsq0d9s6ncny253rd8i") (yanked #t)))

(define-public crate-rams-proc-macros-internal-0.1 (crate (name "rams-proc-macros-internal") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "proc-macro" "parsing" "printing" "clone-impls"))) (kind 0)))) (hash "0fbmd4y4qqcc6sznkal11qcrhiick5s9m316yk5ingak8h4c22jx") (yanked #t)))

(define-public crate-ramsgate-0.1 (crate (name "ramsgate") (vers "0.1.0") (deps (list (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_trace" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "volmark") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p6hn30c3ai8kz843qjrac1v24gfp8a9pr2k15is9wlz46hfkhi9")))

