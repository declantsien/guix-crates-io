(define-module (crates-io ra va) #:use-module (crates-io))

(define-public crate-rava-0.0.0 (crate (name "rava") (vers "0.0.0") (hash "1xsd0bmp4ly8dnl5j1b02mvksi9kb5hyrkb8x8282g7i3g88x3fa")))

(define-public crate-ravascript-0.0.0 (crate (name "ravascript") (vers "0.0.0") (hash "0divahx81vclh4k28dzz8a8vzq2wxkg8s4rpcyr1fqb7k1yq2w8c") (rust-version "1.60.0")))

