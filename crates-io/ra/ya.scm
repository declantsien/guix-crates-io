(define-module (crates-io ra ya) #:use-module (crates-io))

(define-public crate-rayauth-session-0.1 (crate (name "rayauth-session") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0hrpzn3x1iw0f6gx814zx1vg0dcaqry150lczm6sx6mpc9y20qm6") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

