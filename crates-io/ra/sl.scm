(define-module (crates-io ra sl) #:use-module (crates-io))

(define-public crate-rasl-0.0.0 (crate (name "rasl") (vers "0.0.0") (hash "0fc6i0x7m7d124a6xdsiswj740ky4w0a5j3517bn044kzsk3xwhi")))

(define-public crate-rasl_builder-0.0.0 (crate (name "rasl_builder") (vers "0.0.0") (hash "0h9xbjbsl8di598p4ifwz8b8yfmkmhgp7rccyk5cgrjm2mqjnfif")))

(define-public crate-rasl_macros-0.0.0 (crate (name "rasl_macros") (vers "0.0.0") (hash "1zlkdx95c4x9xbjx9f07xp5fivvmb34gzr6iap7w6p7wgkj85jvn")))

(define-public crate-rasl_std-0.0.0 (crate (name "rasl_std") (vers "0.0.0") (hash "0l144p17mh3v9mx8rl4s315mlr350i3s8dj9dm2i7iidh45p0ki0")))

(define-public crate-rasl_translator-0.0.0 (crate (name "rasl_translator") (vers "0.0.0") (hash "0pl06kawd1kxa3b6zsh3jfcazas7bdxrgyvf07va9swk95na77f6")))

(define-public crate-raslib-0.1 (crate (name "raslib") (vers "0.1.1") (hash "1zmavd6al1lqab47ggdg6zclcn0qy3p99mkacaj5pn1qmif6gz32")))

(define-public crate-raslib-0.1 (crate (name "raslib") (vers "0.1.2") (hash "1gnfmwg6mnx3iys72xrg2xbqw86r5g0kx5lyy34grs9x5iymcz8c")))

