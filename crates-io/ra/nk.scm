(define-module (crates-io ra nk) #:use-module (crates-io))

(define-public crate-rank1-0.1 (crate (name "rank1") (vers "0.1.0") (hash "0455nld5wg6lfycwc7kgxp8979g9411k515z164kwz6gjlgwxspp")))

(define-public crate-rank_biased_centroids-0.1 (crate (name "rank_biased_centroids") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "06wg8x02igd8b027v94xpgwsmfbinfh23460pah1hjg6jx15jnwp")))

(define-public crate-rank_biased_centroids-0.2 (crate (name "rank_biased_centroids") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1gn8l7j69gcf3m9gillanxlab4rvwk9pag4brkvisdyynk4d9snd")))

(define-public crate-rank_biased_centroids-0.2 (crate (name "rank_biased_centroids") (vers "0.2.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1m0vcg2ippsril0jrfpdgv458jsciav7dv7azqnckkwsjb5wsjim")))

(define-public crate-rank_biased_centroids-0.3 (crate (name "rank_biased_centroids") (vers "0.3.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1c4bnxvyi75rd19417zrgma7dh5s7vhllwz8i9mh7a0c3ah64rkg")))

(define-public crate-ranked_voting-0.3 (crate (name "ranked_voting") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ixgadjsazmfj7cimxh3167h4qrygiq7nx9mjjkih2gly61vl0ww")))

(define-public crate-ranking-0.0.1 (crate (name "ranking") (vers "0.0.1") (hash "1rgb2kp0bchxrcdsq9n4cnazzd3gz0x8wdch7vv9avharb30sg58")))

(define-public crate-rankmap-0.1 (crate (name "rankmap") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "0s9a7lcw0ypm3w2r89w557pjfh951bl2avkspr213inbldr08zyj")))

