(define-module (crates-io ra nl) #:use-module (crates-io))

(define-public crate-ranluxpp-rs-0.1 (crate (name "ranluxpp-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "166kqalj7d58gp3rxrfy6qwds6v2acjs5prm48ykhq435kmn6j6s") (yanked #t)))

(define-public crate-ranluxpp-rs-0.2 (crate (name "ranluxpp-rs") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1sykwx4xqhdn6g8pnk23yhbf3pmqlwjhm90ixqiahzi56233rp1a") (yanked #t)))

(define-public crate-ranluxpp-rs-0.3 (crate (name "ranluxpp-rs") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0c61plf8hk9mdpnaz6kzwaag2zx5gdz7f1wiw12im7fykihc2n0r")))

(define-public crate-ranluxpp-rs-0.3 (crate (name "ranluxpp-rs") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "03ydffmgj9jh0jbidfrc1jj64bn475kc6rvlli21n16ss392zx87")))

