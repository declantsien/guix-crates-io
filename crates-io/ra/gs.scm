(define-module (crates-io ra gs) #:use-module (crates-io))

(define-public crate-rags-0.1 (crate (name "rags") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "min-max-heap") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1jpwl8j490icrn5pcqfv4fvvaaiapf7aigvlc2sivw88idlkfsn4")))

(define-public crate-rags-rs-0.1 (crate (name "rags-rs") (vers "0.1.0") (deps (list (crate-dep (name "bit-set") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "128iyj2wv1zv1z7461maja84wyc5i6rx1q4hxa5s3li548jc7jjs")))

(define-public crate-rags-rs-0.1 (crate (name "rags-rs") (vers "0.1.1") (deps (list (crate-dep (name "bit-set") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0h7fpb5hs4f3i3cibx4mp3czkicp2ykzqh7p76n7qpr33x6d5cmy")))

(define-public crate-rags-rs-0.1 (crate (name "rags-rs") (vers "0.1.2") (deps (list (crate-dep (name "bit-set") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1ajj3ygd5is56xgr60yaxns5xvgnxhc3v0hv5qxkf8g6h0fdj7sn")))

(define-public crate-rags-rs-0.1 (crate (name "rags-rs") (vers "0.1.3") (deps (list (crate-dep (name "bit-set") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "006b64ds1v1zy25mfblbzxrig7w8xhmw16fdk3vk7crb7qzfwbq1")))

(define-public crate-rags-rs-0.1 (crate (name "rags-rs") (vers "0.1.4") (deps (list (crate-dep (name "bit-set") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0sa5in2cg9iifxdr48v9a2dxwx150jzzclcvplij9xhq95m1f1vb")))

