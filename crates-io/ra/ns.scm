(define-module (crates-io ra ns) #:use-module (crates-io))

(define-public crate-rans-0.1 (crate (name "rans") (vers "0.1.0") (hash "073vrw5hlmqg49nyil8qdan567kq8h6lhwr9am4gqd8g0a1cwk04")))

(define-public crate-rans-0.1 (crate (name "rans") (vers "0.1.1") (hash "0minjyjbjg06f58c4908d1j4cw4vljd83kkynwygy007hz8mzppi")))

(define-public crate-rans-0.1 (crate (name "rans") (vers "0.1.2") (hash "0w4p3nzx2lbr9vwg35agzbdyk05502brwb6qrgim6amr64nqfwr0")))

(define-public crate-rans-0.2 (crate (name "rans") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "ryg-rans-sys") (req "^1.0.7") (features (quote ("byte" "64"))) (default-features #t) (kind 0)))) (hash "1392jd674ffgpqij4ikpnry43w59mm3rp76di1zxj6c5jwc4lhqq")))

(define-public crate-rans-0.2 (crate (name "rans") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "ryg-rans-sys") (req "^1.0.7") (features (quote ("byte" "64"))) (default-features #t) (kind 0)))) (hash "0w863c0ldd6anbh7mfssw5w1ply4wgmqh3xgznxgyhm70i760c9a") (rust-version "1.57.0")))

(define-public crate-rans-0.3 (crate (name "rans") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "ryg-rans-sys") (req "^1.1.0") (features (quote ("byte" "64"))) (default-features #t) (kind 0)))) (hash "0jv0b3v1lywz7cn0kj1yihcdwpv2c5p1ay4vvipdnwavspxz5iyi") (rust-version "1.60.0")))

(define-public crate-rans-0.4 (crate (name "rans") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "ryg-rans-sys") (req "^1.2.0") (features (quote ("byte" "64"))) (default-features #t) (kind 0)))) (hash "14zs5lmgqzb5y39dkb0iypsr4clq66wdjk6ws77mjjfvg0bw7gvi") (rust-version "1.71.0")))

(define-public crate-ransbachm-testing-art-0.1 (crate (name "ransbachm-testing-art") (vers "0.1.0") (hash "1539ia7rh4rlspnf65hxkbb7j01zizl5wcx5q62mvhza0fha9hnj")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.0") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1y7xi2lm29zxqsixgw60b9sdx5wwy3clgm9m2pj1ic6iihxnaazv")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.1") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1p8czcb6w0ln313y3fnw7s8ymnj0hl1ww0s3dhr73qrvgia3bwxw")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.2") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "18hgn57jm1775vxdcrhf9d162cfhrq7m6npm382w6gd6xj12ysq9")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.3") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "006rxm4cfbkqhj0wfm561g8z2cyaljahz3jp12q5bqsc8ylzgxsj")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.4") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "129z0bk45admrr6cl3lxgij1h5in80dwll0vshnhpvz6f0hbm1hx")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.5") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1v1yhgmw1v6vx237al7cknl6mbkdn4x35mqwskfdnpknmw9g70ml")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.6") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0jz5hy66dvbfqfra6n461gnl08py3m5sbgi84q5dzxfsv1mci2hb")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.7") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1pl2iivmg3kzsbd9q6plvmgw4wihf6yqngrdj8g2gb6v11zs1zv0")))

(define-public crate-ransel-0.1 (crate (name "ransel") (vers "0.1.8") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "11hmmsgzhrlv27wv62dmw0mp33km01kakc3if6r3xgyk0g45lz7y")))

(define-public crate-ransel-0.2 (crate (name "ransel") (vers "0.2.0") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1kj0si5k2hy19pdwngv81h1yq5zcb83qy2s9bqavbyb018h8x6lj")))

(define-public crate-ransel-0.2 (crate (name "ransel") (vers "0.2.1") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "random") (req "^0.14.0") (default-features #t) (kind 2)))) (hash "0x2m1gzw1jhc157j5q4wgbcsqz6lj0i81rkq6chm8fsk8nzr61a0")))

(define-public crate-ransid-0.1 (crate (name "ransid") (vers "0.1.1") (hash "1x48bi5vzcvki9vvm59j5r2a9yafj239c7nkq3f0xqdfrz7yj35y")))

(define-public crate-ransid-0.1 (crate (name "ransid") (vers "0.1.2") (hash "1s10l0gmws2da2vq6xvw17kf17zv58b6kdyd2yyjznmnyd34v6k5")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.0") (hash "185xl8ry6wxwq463ncci0qjylcz4pk8p78qzqyy18k621m61n36f")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.1") (hash "1n4dziv23zbxx4v0fb3big6ms7vpwyz5pcmcj0yc5g6vfa90vsza")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.2") (hash "0h80yny611s4v3iskxaq5a1pgbip8wmx82flii1byqqm0pibd556")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.3") (hash "0knxqwj58pa2386i5wgsv80rif2apa06dnydar375b8s0kkc7vq1")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.4") (hash "0p2z4rkvrardydi8ab2xc16sl00z716j5q2sh58pd8qk93kldja2")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.5") (hash "109dfj5xjr11dc44dbsbapig7vdy6bxpp9qv1cxhprpnbj416rvc")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.6") (hash "1w2f9bn41mw2k2wpkk5szv8c3khf6qyadklqbwgakagcxrqlc1z2")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.7") (hash "09rpsc90ryssa6kyhjasajm4jhnq71xg130ynyikb3bm0g7afw78")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.8") (hash "0wkf605jq6ak43xvsm0np87iapxay6kdsqs232x6d0vl2ycwh65h")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.9") (hash "0hacglx2f4awm8gp58j97dhd53q18c7s4v5f18sxh6aab8r768z2")))

(define-public crate-ransid-0.2 (crate (name "ransid") (vers "0.2.10") (hash "197gqdj8fkbykz9m6kddm21mc1abznpj9azhac9zq235qlpxhkkm")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.0") (hash "1z5v0z8qryq8j3igdjkq7sqbni8xdwgn3a40kf9vb18iz28lhjq7")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.1") (hash "1k9f0lazkg4ifjziv3028z9n7vrgnw3560v89018wh1aybz54jax")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.2") (hash "02fkqdfamiqwvkk5p0wsfx6cippw9cdk53wq5s975ddbrlkcslzh")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.3") (hash "0m6891crv8xf8fjlpl3dvw3smyp03ybyhf2cr8idhn9wy63910w1")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.4") (hash "19lxk8nhqdg89x9facrw61rn5bj7dzhxzmybv56q4bv15sbnpism")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.5") (hash "0x0bfdm9kybwbp7dgr5hnm5lii1p5kd2rgsbh0nrvjvf821h8gmz")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.6") (hash "0m87hmp7y077pbmyjap78204hjlcrb7drgzyzrkd0yhhbxy5qqiw")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.7") (hash "1lia7cn537zh641sds5mxzy284cndw8zvih29blm57acx7h3kq07")))

(define-public crate-ransid-0.3 (crate (name "ransid") (vers "0.3.8") (hash "1ajv5sk5xi4rfpw43ldhhw6qfi4ng2k1fvz9d6z4izvsbxa8z6v5")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.0") (deps (list (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "18kz4g3d9iclh8h2j934mb55jvmn8zz18zm77zv4ms5706nafscd")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.1") (deps (list (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nwrivy4pymc579nk9j72m8f1g2wwkfqm2hid9r0kvxwk5w5gzja")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.2") (deps (list (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "1g2bz1xlqaww5m0fdf0h4388isj36dva24rrp6cg3sbvnl40fv6j")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.3") (deps (list (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "15xfzyv6p5vis6j6n9ldasfgvsfwj5659i2ag5zc7iijq1hq3zvc")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.4") (deps (list (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "0njb30ayvldaym92sjwynxpzsfih5bpk74bhyvcjfglk3f1f8b3f")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.5") (deps (list (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lxbc8g08r11v07hm3rkrqgzv08wnlk0992m0y41biz01v05pn20")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.6") (deps (list (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "0w9g3bj3l3cm3jpj82abr0w5a11pq7h38d83pwjbp82cgb6zvicg")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.7") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "008n7qmls0a0v7c87gn4y1h6iwh6amw6hwk3lykvd2kd7qk6waz0")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.8") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "0i9hph73j16742cc3x1djihpsdri10nckqb4z8yid4053caszf92")))

(define-public crate-ransid-0.4 (crate (name "ransid") (vers "0.4.9") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ls597icw6i08zv501f6y511cb0vc6npzm8y4kx5xv73pfq0pr46")))

(define-public crate-ransid-0.5 (crate (name "ransid") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "vte") (req "^0.8") (default-features #t) (kind 0)))) (hash "1dgsvymwp0l501qk81xhxr3i28j99n64r36m3rgllz8jgqvbp621")))

(define-public crate-ransid-log-0.4 (crate (name "ransid-log") (vers "0.4.7") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "vte") (req "^0.3") (default-features #t) (kind 0)))) (hash "0r1qz82bz2kj6ajb7nqivln6ds8h41vki8h5c3si6n000jp26gqd")))

(define-public crate-ransidsole-0.1 (crate (name "ransidsole") (vers "0.1.0") (hash "1vjjc6krxn3zazjwy45k7p3za7bxyjrrf36sjdq58wkgm66m2r1z")))

