(define-module (crates-io ra #{2e}#) #:use-module (crates-io))

(define-public crate-ra2e1-0.0.1 (crate (name "ra2e1") (vers "0.0.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1v2iv4vp01nfljr7khdqfcmh6wqk4b5vjis3vkchpwxpcasgvbc8") (features (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ra2e1-0.2 (crate (name "ra2e1") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17mp2iwn1s9lgjj6mrgxl8d6ck3gyq69n0sp7564gb6h9kqw0px2") (features (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-ra2e2-0.2 (crate (name "ra2e2") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ckwbh9zj0bkmqrxgq6xyhigxjizp4fzbr32j2r4gwwpb4114r9i") (features (quote (("rt" "cortex-m-rt/device"))))))

