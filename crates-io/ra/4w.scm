(define-module (crates-io ra #{4w}#) #:use-module (crates-io))

(define-public crate-ra4w1-0.2 (crate (name "ra4w1") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1p9n9rrz3mirfr5pjmjkqznhb3ac69fhif48icppi5j02wbbs1g4") (features (quote (("rt" "cortex-m-rt/device"))))))

