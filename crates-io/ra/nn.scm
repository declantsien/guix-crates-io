(define-module (crates-io ra nn) #:use-module (crates-io))

(define-public crate-rann-0.1 (crate (name "rann") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "12lxsh8vrq6j50f0jxhdq27r0p3s8a3gdn4hj7rgq3mh4djvx952")))

(define-public crate-rann-0.1 (crate (name "rann") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1cl0pjgyyaljn9jkfki66d74900h032xdw2w50pqbldnqxa4xxni")))

(define-public crate-rann-0.1 (crate (name "rann") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hgrqk1qk0n7vgqwajkbl3qdhf7cigj1199pn8kqm8x5p4a1fpq5")))

(define-public crate-ranno-0.1 (crate (name "ranno") (vers "0.1.0") (hash "1sn6sq3m073zfw4cay6yjx88ps96m64jranzj76hlmzvv220whwr") (features (quote (("default" "alloc") ("alloc"))))))

