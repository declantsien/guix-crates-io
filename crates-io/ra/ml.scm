(define-module (crates-io ra ml) #:use-module (crates-io))

(define-public crate-raml-0.1 (crate (name "raml") (vers "0.1.0") (hash "0lylalwclr7zqn3cm836v2hj4q8sgvnp11nyvzdnql8p2xliwmx0")))

(define-public crate-ramlink-0.1 (crate (name "ramlink") (vers "0.1.0") (deps (list (crate-dep (name "const-assert") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0vgpf4mxxviq86m9j9n0ziqvhqpwyr5j1a9zysg2a9c5772g6wrl") (features (quote (("producer") ("consumer"))))))

