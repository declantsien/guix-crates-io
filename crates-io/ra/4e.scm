(define-module (crates-io ra #{4e}#) #:use-module (crates-io))

(define-public crate-ra4e1-0.2 (crate (name "ra4e1") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "02ggn3bwy5hphz7s2ijl09yzp5s8ychkjx92m4s4l6qlw1f9xvvg") (features (quote (("rt" "cortex-m-rt/device"))))))

