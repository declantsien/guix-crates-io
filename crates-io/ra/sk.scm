(define-module (crates-io ra sk) #:use-module (crates-io))

(define-public crate-rask-0.0.0 (crate (name "rask") (vers "0.0.0") (hash "0gbgyhmkw5wkf9w3s197i1cniw0vjygamx4w3v1vx30ls8v5dxh4")))

(define-public crate-rask-0.1 (crate (name "rask") (vers "0.1.2") (deps (list (crate-dep (name "chainmap") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "019z2ynw70w9lr9p8dv63051m32jhlkjmyrdjgnyghsn2skf6d2q")))

(define-public crate-rask-0.1 (crate (name "rask") (vers "0.1.3") (deps (list (crate-dep (name "chainmap") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0g32q3ld7769m0cic97vg9yrfwg06hhqh8mbnpfn4h2nx2b8yczg")))

(define-public crate-rask-0.1 (crate (name "rask") (vers "0.1.4") (deps (list (crate-dep (name "chainmap") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0j1dbxnrdm49664rxfz9fzlh20aifx83pfql7g62j74gz83i5kbp")))

(define-public crate-rask-0.1 (crate (name "rask") (vers "0.1.5") (deps (list (crate-dep (name "chainmap") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "102hkz9dbn8rdarwyrbjfwp36hssi11csywgb9v1v9drip161ryf")))

(define-public crate-rask-0.2 (crate (name "rask") (vers "0.2.1") (deps (list (crate-dep (name "chainmap") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0zqrys2ahw0icqda7c51sghmx30451a40dpz3bdpfkp18g85d981")))

(define-public crate-rask-core-0.1 (crate (name "rask-core") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rask-liburing") (req "^2.4.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "slab") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0nzpgrfvvb26rnzninh14va15q6387x665lii46h4jby0sx45v8k")))

(define-public crate-rask-liburing-2 (crate (name "rask-liburing") (vers "2.4.0") (deps (list (crate-dep (name "bitflags") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rask-liburing-sys") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0zyvr2ghn9k62s2j9as5ld57ivq5d09hcv3p0rmgfac7145vjd4x")))

(define-public crate-rask-liburing-sys-2 (crate (name "rask-liburing-sys") (vers "2.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0mv10np91dyllch0k118n1lzczz4nv3iwmza6z5w2iwpb563bkip")))

(define-public crate-rask-proxy-0.1 (crate (name "rask-proxy") (vers "0.1.0") (deps (list (crate-dep (name "rask-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19rhpja00arldyihjzwwj9frdmgywhmsgzrjgk180fn97r0cqgyh")))

(define-public crate-rask-web-0.1 (crate (name "rask-web") (vers "0.1.0") (deps (list (crate-dep (name "rask-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vj2r833ydimp6dms4kcl9wh0dz82ar8bhmm6r7kz124qa4rfq03")))

(define-public crate-raskell-0.0.1 (crate (name "raskell") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1n5xfp6ngxl89421rn2dhqcfp7jzzh3xzs9hb1v0wsxwc171fpyn") (rust-version "1.61.0")))

(define-public crate-rasks-0.1 (crate (name "rasks") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "02f3a455yw9z6qk301gb5bilx4y36ygrn5rbvz6q7m2zlcv7gzzh")))

(define-public crate-rasks-0.2 (crate (name "rasks") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1511vhfv3ir9c6x7p862wk4jjvcjzm7xcg0cqg7xkrm1wdrk20wh")))

(define-public crate-rasks-0.3 (crate (name "rasks") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0d7wxrjgadk6001xq07gjfiz3z1ab6wzqwza5h1szm5k2slf2jlj")))

(define-public crate-rasks-0.3 (crate (name "rasks") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08hj6q9w6gaxihk1ljbc8xbag6vd60brxxmp3vc2i6hwxfnn2qhz")))

(define-public crate-rasks-0.3 (crate (name "rasks") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0v2vjridwgzkmarmsw9dd7mcpj3baf2vwjnhf61q06dpc5nn6bh1")))

(define-public crate-rasks-0.3 (crate (name "rasks") (vers "0.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l5rs9n9y2r5bj6fzla36px23mirm08hd2mfw9ys6dnnffwm1r35")))

(define-public crate-rasks-0.3 (crate (name "rasks") (vers "0.3.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c1r8yb8knpz1yl3j7ghaiddqy0s23d05iagdzlm4wv5givgdzla")))

