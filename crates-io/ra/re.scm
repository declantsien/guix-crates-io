(define-module (crates-io ra re) #:use-module (crates-io))

(define-public crate-rare-0.1 (crate (name "rare") (vers "0.1.0") (hash "1szi50gn1rmjwk2q286v608p4291l3fpq638dsfyn8l8dyajnngg")))

(define-public crate-rarena-0.0.0 (crate (name "rarena") (vers "0.0.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.8") (kind 0)) (crate-dep (name "fs4") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "loom") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0961d9sq8sg24vx83nnqkyhs6jwy6h7ybghk2hmqph2lp2aciszc") (features (quote (("std") ("memmap" "memmap2" "fs4" "std") ("default" "std") ("alloc")))) (rust-version "1.56")))

