(define-module (crates-io ra _r) #:use-module (crates-io))

(define-public crate-ra_rustc_lexer-0.1 (crate (name "ra_rustc_lexer") (vers "0.1.0-pre.1") (deps (list (crate-dep (name "unicode-xid") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "07lzfhxji6q4ahrqnql0a5ilx7j3mxhslms6kin8m5r2z1r2gng8")))

(define-public crate-ra_rustc_lexer-0.1 (crate (name "ra_rustc_lexer") (vers "0.1.0-pre.2") (deps (list (crate-dep (name "unicode-xid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fwpbl6lzgrxdm9v52a291mlp1qg26gqzg50z3bsvpvl2nlwvb3b")))

(define-public crate-ra_rustc_lexer-0.1 (crate (name "ra_rustc_lexer") (vers "0.1.0-pre.3") (deps (list (crate-dep (name "unicode-xid") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jisz9a8qg9ylgs9n9j6s8drxkq9xc1va4kxbk9zh3w2h7s1ldq4")))

