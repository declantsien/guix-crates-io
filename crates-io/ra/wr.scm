(define-module (crates-io ra wr) #:use-module (crates-io))

(define-public crate-rawr-0.1 (crate (name "rawr") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7.14") (default-features #t) (kind 0)) (crate-dep (name "serde_codegen") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0rzqrv8a5p63gc1iaawjymp2vm266v83f9srj8xcsgbq4xnhx3id")))

(define-public crate-rawr-0.1 (crate (name "rawr") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7.14") (default-features #t) (kind 0)) (crate-dep (name "serde_codegen") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1bfvh46kk71yxckqgwv44g7blb7wdnvx6lizp6y32i4plcyz5r1m")))

(define-public crate-rawring-1 (crate (name "rawring") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1i7mfaf9x77cisaqq75s79kvg1npj3j5jcnakm1y4sa707v6z4sr") (yanked #t)))

(define-public crate-rawring-2 (crate (name "rawring") (vers "2.0.0") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "0b2givs07v77jz19wd82rgxp8gwinkcxgan007davn402cdm1ib0") (yanked #t)))

(define-public crate-rawring-2 (crate (name "rawring") (vers "2.0.1") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "1f235ai3n0wagp2bzfm64hnp3zbfjlp9is8lnzsc8i3plqjjnlv3") (yanked #t)))

(define-public crate-rawrrr-0.1 (crate (name "rawrrr") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1g605wwbk6lf5gbcxyar5pdll7ap1hml97zc2i1xgnbyicx9iz5w")))

(define-public crate-rawrrr-0.2 (crate (name "rawrrr") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0rbi6a5bpwyj445g15fwhpz6dadjzyjbjgv5lvm17ygmbhwijs2i")))

(define-public crate-rawrrr-0.2 (crate (name "rawrrr") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("consoleapi" "handleapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1sk35k4ahkxh4yx7z1k1dr9g9x0lsc7b2l2x59fni0rh9n4m2y3p")))

