(define-module (crates-io uv ge) #:use-module (crates-io))

(define-public crate-uvgen-0.1 (crate (name "uvgen") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "rectutils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06vsn51xp5dxlfblprjdz7irfjjnsvc3mwc5hy7dp6bzfcfnxzlh") (rust-version "1.72")))

