(define-module (crates-io uv c-) #:use-module (crates-io))

(define-public crate-uvc-src-0.1 (crate (name "uvc-src") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "05vkva9hiq6i3b09v6dixihzrbjm1yf96m14jwblxsyjxf44cr0x")))

(define-public crate-uvc-src-0.1 (crate (name "uvc-src") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0ip6gzzdxynda0r9f1qyxxzbqdxfy09igjg0hhwvkc22101frd1n")))

(define-public crate-uvc-src-0.1 (crate (name "uvc-src") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "1kj02dk0b3dfgybd06nakaxzw1yfbapdx2hq0lnvm4l69ql2fgj1")))

(define-public crate-uvc-src-0.2 (crate (name "uvc-src") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "mozjpeg-sys") (req "^0.10.4") (kind 0)))) (hash "07mnyrvz1hl8cngb9z9kb4j1w5ws8ly8khdwh9p0rm7awn9irxrl")))

(define-public crate-uvc-src-0.2 (crate (name "uvc-src") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.61") (default-features #t) (kind 1)) (crate-dep (name "libusb-sys") (req "^0.4.2") (default-features #t) (kind 0) (package "libusb1-sys")) (crate-dep (name "mozjpeg-sys") (req "^0.10.4") (optional #t) (kind 0)))) (hash "0dfly6pixqwhfvwwczf17fp7rnkx8flh05rw29f8z15dnrhzpj7q") (features (quote (("jpeg" "mozjpeg-sys")))) (links "uvcsrc")))

(define-public crate-uvc-sys-0.1 (crate (name "uvc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)))) (hash "0jj0lw053m9l73swgidf4hc0402v5lhrpl7wagsh4yxkp6dap48i") (links "uvc")))

(define-public crate-uvc-sys-0.1 (crate (name "uvc-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)))) (hash "1f6pm6p1y5285lyd90ybj7rqfw3i65kjrmnbg9y4fza25vn6ngw3") (links "uvc")))

(define-public crate-uvc-sys-0.1 (crate (name "uvc-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)))) (hash "04plxvy21x7cvcvcj62x3yz6mvssh74sz03qwsiww3ixq9lwwp90") (links "uvc")))

(define-public crate-uvc-sys-0.1 (crate (name "uvc-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.51.0") (default-features #t) (kind 1)))) (hash "0a6qkn8lybhlj3mm7xa706kp9pa131z6cfzfsdrysj83dp2p0ss1") (links "uvc")))

(define-public crate-uvc-sys-0.1 (crate (name "uvc-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "0pz0h414w9v6ynf9j3d0ipisdxc3qnzap5970ifbi6szdbj4x1lr") (links "uvc")))

(define-public crate-uvc-sys-0.1 (crate (name "uvc-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "06kw0q6lxz1h6wslbwb7ai77hcqh0vw8zc8pk6fg1k7r6293iy6w") (links "uvc")))

(define-public crate-uvc-sys-0.2 (crate (name "uvc-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)) (crate-dep (name "uvc-src") (req "^0.2.1") (features (quote ("jpeg"))) (optional #t) (default-features #t) (kind 0)))) (hash "007b4plhm51kwmmnh4iixd9x2m0isi0samgpqjyqrjdxix25qrz3") (features (quote (("vendor" "uvc-src")))) (links "uvc")))

