(define-module (crates-io uv ec) #:use-module (crates-io))

(define-public crate-uvector-0.1 (crate (name "uvector") (vers "0.1.0") (hash "1pp5k46n1j9qjzsp1yr1iy9dhh3lrxxrdrmbzjki8nlghzwgw708")))

(define-public crate-uvector-0.1 (crate (name "uvector") (vers "0.1.1") (hash "1zz1wz8c7khjbkyy6hph6hyp5ad8ihmynb6pdcd4cniway1a8g4f")))

(define-public crate-uvector-0.2 (crate (name "uvector") (vers "0.2.0") (hash "00jk7picpiixy6jb5vg160v8874bgvd3glrrkgmpy315qpwqax90")))

(define-public crate-uvector-0.3 (crate (name "uvector") (vers "0.3.0") (hash "1g29y0rw5245mdk1w7knfj91rigr5lbkayvlfcirdsrwnq1i33jx")))

