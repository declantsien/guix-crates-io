(define-module (crates-io uv th) #:use-module (crates-io))

(define-public crate-uvth-0.1 (crate (name "uvth") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "10mrl22mx9kl8ivg1wajjjlaaf0n2ix3hdy1n6mzaxh91bbip5kf")))

(define-public crate-uvth-1 (crate (name "uvth") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0rab35rp063q4642i38hbrpxxvzgp5frasnnwmw7h5xzjgqhds3x")))

(define-public crate-uvth-1 (crate (name "uvth") (vers "1.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1rlrcxjxpcchh6crxwaj57hivwwaszv07v4ylaqrlhd4vxzc370q")))

(define-public crate-uvth-1 (crate (name "uvth") (vers "1.0.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "138wcyyhc58awqdc29vhyvk68l85xq50g552r8wyyf9824mdnayj")))

(define-public crate-uvth-1 (crate (name "uvth") (vers "1.0.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0bmdh7wwcylx8k5b7arizcb0ylqkakf49g94wmrvpaw97h8wbpgw")))

(define-public crate-uvth-1 (crate (name "uvth") (vers "1.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1scwfg0xi5j5xsm5yggd9gq29yrfqvn2jjy5jhvzigdjdrf38v4v")))

(define-public crate-uvth-2 (crate (name "uvth") (vers "2.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)))) (hash "17wyxzhjxj85ca55cqmixg86m9rhwavi3xf3w2562x98gz77l0h8")))

(define-public crate-uvth-3 (crate (name "uvth") (vers "3.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "049gzairkhcjfn226gvjzvijy5x91ay480n4ll9171wxshpjmv4z")))

(define-public crate-uvth-3 (crate (name "uvth") (vers "3.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1mf60zfbapdl1spaackgfhwl1g697f5dzw8rldsiqwpqsagz5zh7")))

(define-public crate-uvth-3 (crate (name "uvth") (vers "3.0.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0jpmq7l5c1ma1m27q7cciwikd945ax4shy0pncav7za03g6cy16b")))

(define-public crate-uvth-3 (crate (name "uvth") (vers "3.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "04f3s3f4jyjzg66h48lhfwggdr5rl5ra981klak165pcfssfipaq")))

(define-public crate-uvth-3 (crate (name "uvth") (vers "3.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1gxbhzrbw6cf1c9rjrfpx0d54p609nw9j6vsrl7ynwyij1w1d6p5")))

(define-public crate-uvth-4 (crate (name "uvth") (vers "4.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "16bcmd9chds3x0dzhk40bscyq07grnwg1lyp3qz3v3007bik74gr")))

(define-public crate-uvth-4 (crate (name "uvth") (vers "4.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0sxvw57ii7r5fdzm17yaqimadgb4lxvis7xfdi6375kb23wi0n8y")))

