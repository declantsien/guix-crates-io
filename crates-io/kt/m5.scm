(define-module (crates-io kt m5) #:use-module (crates-io))

(define-public crate-ktm5e-dice-0.1 (crate (name "ktm5e-dice") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "^0.3.1") (features (quote ("nightly"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "165gy968ydvjifcdpcl2rbn8fj4z9r295n142c78smai9y3gh08a") (features (quote (("benchmark"))))))

