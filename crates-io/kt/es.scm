(define-module (crates-io kt es) #:use-module (crates-io))

(define-public crate-ktest-parser-0.1 (crate (name "ktest-parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rbpxvxqfl3rf6fx5apcl06wgihqssrfzjr27lzqbfwa02w7a44v")))

(define-public crate-ktest-parser-0.1 (crate (name "ktest-parser") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cv41xy1jcvx84qgdzm10y0zznagw4rqsvy07abfck2c2j35mhjm")))

