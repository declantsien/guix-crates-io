(define-module (crates-io kt s-) #:use-module (crates-io))

(define-public crate-kts-analyze-0.1 (crate (name "kts-analyze") (vers "0.1.0") (deps (list (crate-dep (name "arrow") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "datafusion") (req "^0.14") (kind 0)))) (hash "0mcif1mfyi0q72v08z29s41zh6vg1aq8m3902jkg1pq7ry2bgz48")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)))) (hash "13j5xnm2ck135vgr333hbh8lxipf2y419a9fmyag9cghga555i38")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)))) (hash "1y1lcp94mbkxlibmfz42gsr9w4637nwh9dxqvlpl5p4mpn7n1a6b")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.2") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "12ypd20mg59wax73y1ai1mfzxffvgzgznbfs2lb0dm8h7fp3ghw9")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.3") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "10ih492vd7pcn646pww6hdjvgwlnsybczv5mbwzdalzwxsf2k7sq")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.4") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "06nd637whm7dlh4x1fg95cfdc7yf6ldgzim0jq6y76s9nzx91k6i")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.5") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "00is9952n1m1bbxywg3hll8dqlcmzhhw5qs3r99wdf59w7pv5l97")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.6") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "0vq0sg8rlydw3y8nvd0n6dkppvlq1vj28cbyi89swgh9g1akbwr2")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.8") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "18p5lqkb19c7cj4v3wzj4ny8lnyaghzppc4fha3src1pzs10z3mg")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.9") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "1377bjg811wr904l1f6ijdidh2wn14ppibp8wsxiy6zmirqvdgiy")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.10") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "1n6cnpi22f8xf835lrzn8f8sym6fagpvdxlqa50g12j4zhvwcpng")))

(define-public crate-kts-etl-0.1 (crate (name "kts-etl") (vers "0.1.12") (deps (list (crate-dep (name "arrow") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "datafusion") (req "^0.14") (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kts-analyze") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15.0") (features (quote ("bundled" "functions" "unlock_notify"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "148qgyljr9wylf8a3yc1d89k9kyjdqzlzss1h7y79z0ckh8dny90")))

