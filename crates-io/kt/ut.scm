(define-module (crates-io kt ut) #:use-module (crates-io))

(define-public crate-ktutils-0.1 (crate (name "ktutils") (vers "0.1.0") (hash "1636lwqy938b6b62ihmjx2y7m2hp6nhw2jifmm56z1b8hv8znppm") (yanked #t)))

(define-public crate-ktutils-0.1 (crate (name "ktutils") (vers "0.1.1") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "1xvsxavnvqm57j93sv02p6nwqsxjm92057w6svy8zqr7cn0rjm5s") (yanked #t)))

