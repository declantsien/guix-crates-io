(define-module (crates-io kt en) #:use-module (crates-io))

(define-public crate-ktensor-0.0.1 (crate (name "ktensor") (vers "0.0.1") (hash "1inpkmi2w9cnhpf4943qjjjxbmcjrb05m6p3h8rbv7dryvym5x2l")))

(define-public crate-ktensor-0.1 (crate (name "ktensor") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "05d47ivydr1h7dhv0y4nwcxk7kc5fm6f0hl989x3000s00hvaxvd")))

