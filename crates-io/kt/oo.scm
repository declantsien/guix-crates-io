(define-module (crates-io kt oo) #:use-module (crates-io))

(define-public crate-ktool-0.1 (crate (name "ktool") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "block-modes") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "des") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "shell_command") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10yzl4ng4bwxfnd2j9dhp7sk3cydgxlrq6xh3g0h0gkhw8a5yqvg")))

