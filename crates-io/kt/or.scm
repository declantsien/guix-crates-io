(define-module (crates-io kt or) #:use-module (crates-io))

(define-public crate-ktorrent-0.1 (crate (name "ktorrent") (vers "0.1.0") (deps (list (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1k6azy01iwy9ih9fx5b1vznfd5sp4gjlsr1rcwxzif018lxm7dxm") (yanked #t)))

(define-public crate-ktorrent-0.1 (crate (name "ktorrent") (vers "0.1.1") (deps (list (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0brf38n8lvb1bd7ymyn13jlwfs3gib6ifqgik7hfbr140jcx0cp3")))

(define-public crate-ktorrent-0.1 (crate (name "ktorrent") (vers "0.1.2") (deps (list (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1m52gvv7lwp80plg02psmgs64p52kllqg6yy76b2pzw5wjam106z")))

(define-public crate-ktorrent-0.2 (crate (name "ktorrent") (vers "0.2.0") (deps (list (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0270b2nrjp6y81ysdxd5paf36lg9l24rviqazf17p486dnixsmly")))

