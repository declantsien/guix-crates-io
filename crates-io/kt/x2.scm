(define-module (crates-io kt x2) #:use-module (crates-io))

(define-public crate-ktx2-0.2 (crate (name "ktx2") (vers "0.2.0") (hash "0gbm5ywzjiyd84jjs9mgiv6qyqrhhzfcjvj8acziw587h1m2dd1i") (features (quote (("std") ("default" "std"))))))

(define-public crate-ktx2-0.3 (crate (name "ktx2") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0f781irq66llyrqxwz7yh56nnxdrlvm3j0bss84y80pcm445xml7") (features (quote (("std") ("default" "std"))))))

(define-public crate-ktx2-reader-0.1 (crate (name "ktx2-reader") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-core" "fs"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-core" "fs" "macros"))) (default-features #t) (kind 2)))) (hash "18s18g5sbcbfvip6qcd4ivmksbsy0yqm3r9xwnrrdfyx1c8wxzrb")))

(define-public crate-ktx2-reader-0.1 (crate (name "ktx2-reader") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-core"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-core" "fs" "macros"))) (default-features #t) (kind 2)))) (hash "1vznwpiglchkbhv3nh08c1cjfgczyn4c4wb8jz5f3akvw907hkxg")))

(define-public crate-ktx2-reader-0.1 (crate (name "ktx2-reader") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-core" "fs"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-core" "fs" "macros"))) (default-features #t) (kind 2)))) (hash "0wiwcbz29d4afp54ljlcl74xc6fqcwiay0kmyf72r01wnq1rqfnr")))

