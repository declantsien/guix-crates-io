(define-module (crates-io kt xs) #:use-module (crates-io))

(define-public crate-ktxstats-0.1 (crate (name "ktxstats") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "0mfwr05imbws59gsf5ld1h7wb9jzg0isqazmb50mhz392s8pz4dc")))

(define-public crate-ktxstats-0.2 (crate (name "ktxstats") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "1lmq7rz6vmx2h4pd4lynkmsj35k7c0laj9hh9qn4acj4siv6hav2")))

(define-public crate-ktxstats-0.3 (crate (name "ktxstats") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "0v5j5s6spxm7zfy04vdaiwj5gc76iddj3nc2flyf6adjxmhnrban")))

