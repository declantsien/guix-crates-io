(define-module (crates-io kt i_) #:use-module (crates-io))

(define-public crate-kti_cqrs_provider_rs-0.0.1 (crate (name "kti_cqrs_provider_rs") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "kti_cqrs_rs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0b49g7x80ybdwm91ym4130ybqbq6izzh7h4ni7c64y89b2gqy7m4")))

(define-public crate-kti_cqrs_provider_rs-0.1 (crate (name "kti_cqrs_provider_rs") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "kti_cqrs_rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gsvrkjjfy7hl7gpnydqsvhrsgaw68fxggf16ry8jn06ykffvjc6")))

(define-public crate-kti_cqrs_provider_rs-0.2 (crate (name "kti_cqrs_provider_rs") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "ioc_container_rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kti_cqrs_rs") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y6ypxxpdb8jf4iw9mrc9p6jsh2p3zwnw122yvasw2r5cvs52dad")))

(define-public crate-kti_cqrs_rs-0.0.1 (crate (name "kti_cqrs_rs") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ghdirvplg94diays37fmm2i48ghbmj3avgz0yizi7q7zb6aib5m")))

(define-public crate-kti_cqrs_rs-0.1 (crate (name "kti_cqrs_rs") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dndrgpq6mqzpsf6wgrbw0wg2635xxqm9c0d3gfc21ad04vklm1z")))

(define-public crate-kti_cqrs_rs-0.2 (crate (name "kti_cqrs_rs") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15n9zpf30almqr1dfmidj2v32xzpxcjxdpm85nld14j9y8r0pv0g")))

