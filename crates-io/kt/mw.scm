(define-module (crates-io kt mw) #:use-module (crates-io))

(define-public crate-ktmw32-sys-0.0.1 (crate (name "ktmw32-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0pzzvgivmdgwcmydvj42r6ykcr1anisjf20x7shndys59wzs8rlj")))

(define-public crate-ktmw32-sys-0.1 (crate (name "ktmw32-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "1vm0776ljri8yw2i7x3iswbal1nwj9dbg46riph9lxzzd6l174vz")))

(define-public crate-ktmw32-sys-0.1 (crate (name "ktmw32-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "01dv7g8fml3q5qp1y6qsixia1jzvb6nyaxz1l8063dlwj14x6yry")))

