(define-module (crates-io gi dl) #:use-module (crates-io))

(define-public crate-gidle_future-0.1 (crate (name "gidle_future") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1f23cpj6ivbkimhbvsiwjz0r32gccmwqd9m8x240myrn3fnkyg8f")))

(define-public crate-gidle_future-0.2 (crate (name "gidle_future") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1mz7wpw4lbr3zfp66mwv048w18rr9p3ljzc24las76dfrqqyhrd1")))

