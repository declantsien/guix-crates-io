(define-module (crates-io gi me) #:use-module (crates-io))

(define-public crate-gimei-0.1 (crate (name "gimei") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "regex_macros") (req "^0.1.17") (default-features #t) (kind 2)) (crate-dep (name "yaml-rust") (req "*") (default-features #t) (kind 0)))) (hash "09ikcb2ncr7wchyvasrf32fcfhjn4a6pbdh2iwnxp9jafhgyrgkj")))

(define-public crate-gimei-0.1 (crate (name "gimei") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1r6wj6mz9asjd4blh8drhwhna6kajzixvf1128037zbqx22xcp18")))

(define-public crate-gimei-0.2 (crate (name "gimei") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "03xhssy7x1xis3zkwc18alc320mrb67k8h0myrj9kcxyamp85dsl")))

