(define-module (crates-io gi ff) #:use-module (crates-io))

(define-public crate-giffy-0.1 (crate (name "giffy") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)))) (hash "138a4b62d2d0arcsv8q3nijhapsn7g5ypgkqyahslad4pz34lm52")))

(define-public crate-giffy-0.1 (crate (name "giffy") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)))) (hash "05dyn7sasm276j773bb920qbqbn47apqb33g2z24rs4wlfpxyvak")))

(define-public crate-giffy-0.1 (crate (name "giffy") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)))) (hash "0g0qzcfd5hgm7brq9scp8aabs2vn8bp2gzpvqnsm14p6saddar88")))

(define-public crate-giffy-0.1 (crate (name "giffy") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "0lbqkqbfs5mm1vs510wdsws7ikkkfd3541hyaf125h5v1yscfyii")))

(define-public crate-giffy-0.1 (crate (name "giffy") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "0j3rrkzkfx2knj9kj9d5q9gdf1m1jfdrsdz9v0n70z5yzp6ss48b")))

(define-public crate-giffy-0.1 (crate (name "giffy") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "0i18wi959wf3spywisbrf14l1b8gyx03cd51gyz1m3jgnwxcbm0a")))

(define-public crate-giffy-0.1 (crate (name "giffy") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "0w2ik9vam90rcrn0hb63z32igj199mck74fffsk7c1aajfqalnlw")))

(define-public crate-giffy-0.2 (crate (name "giffy") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "0rrag63l5m9ph74g233vykwi6hij90xmjq962fhfy3xldg4sqr73")))

