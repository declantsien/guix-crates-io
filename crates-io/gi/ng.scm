(define-module (crates-io gi ng) #:use-module (crates-io))

(define-public crate-ginger-0.1 (crate (name "ginger") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "18ivzmhmsgw7pmc5l29sgvan6f40nv49rap03spvz92znd5jzp59")))

(define-public crate-ginger-rs-0.1 (crate (name "ginger-rs") (vers "0.1.0") (deps (list (crate-dep (name "approx_eq") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "17fhmsfm00j7pwswc7nlpf3kal8msq2980fwckxzbvml7b0cdbc8")))

(define-public crate-ginger-rs-0.1 (crate (name "ginger-rs") (vers "0.1.1") (deps (list (crate-dep (name "approx_eq") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "06lnxfxa5wikbkhvikyylsi2zv9s7nbj7xfr5rgglxzimczz329h")))

(define-public crate-gingerlib-0.1 (crate (name "gingerlib") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "0aw1x00gwaldkd9fd41x92agdb3nd37sy2ylcjks8agpy4iqkr0l")))

(define-public crate-gingerlib-1 (crate (name "gingerlib") (vers "1.0.0") (deps (list (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.14") (features (quote ("trace"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0ki2q14b41xnjxdnkkhbx2fcp02hgidzprn1vwzf21b3zzfkzq5d")))

