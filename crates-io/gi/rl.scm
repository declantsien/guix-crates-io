(define-module (crates-io gi rl) #:use-module (crates-io))

(define-public crate-girl-0.0.0 (crate (name "girl") (vers "0.0.0") (hash "0gn8wmwbnnz6iqlpsd5d26q0iap4zqqls3dq1dcjwm75ljy6lfj0")))

(define-public crate-girldick-0.1 (crate (name "girldick") (vers "0.1.0") (hash "18f77x9dbsdn3bl2v6z9z4c13jya8nvz39mggb1pcpqsdhkla63a")))

(define-public crate-girlfriend-0.1 (crate (name "girlfriend") (vers "0.1.0") (hash "0f3gl48hr2vr8fjxsg04kgwxnknmmh5i4mmvc3zd2hx79pmcg73d")))

(define-public crate-girlfriend-0.2 (crate (name "girlfriend") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (default-features #t) (kind 0)) (crate-dep (name "deno_core") (req "^0.145.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ysx4sa7xsf6vjwag1ssb4pwwcif1lmsp9wlhl3xx1ijk5b75i42")))

(define-public crate-girlfriend-0.3 (crate (name "girlfriend") (vers "0.3.1") (deps (list (crate-dep (name "deno_ast") (req "^0.22.0") (features (quote ("transpiling"))) (default-features #t) (kind 0)) (crate-dep (name "deno_core") (req "^0.174.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lck5dckq4gdcp0zls1aixs6c4b6i5l8s76hbqmymwiyxnndb0w2")))

(define-public crate-girlmath-0.1 (crate (name "girlmath") (vers "0.1.0") (hash "0hzk2yq3k1n1k7k0hais4zfrqcr85yplqqbqxp596yyrlclawg4i")))

