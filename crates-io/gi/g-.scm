(define-module (crates-io gi g-) #:use-module (crates-io))

(define-public crate-gig-cli-0.1 (crate (name "gig-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "14qbjcb3llgjixpdxrlyhg0vpcjgycdrdrcys8ssnrvab82sgd1r")))

(define-public crate-gig-cli-0.1 (crate (name "gig-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0kn3qlm03gz06cmx6jbad4g1nfchbcxm65lsqiwmscsg1p7cxjhp")))

(define-public crate-gig-cli-0.2 (crate (name "gig-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "09a3qs3k1nkcxgkfg4y1yyf6fiph01g2d3a0hjiwi4zl65g4v5f7")))

