(define-module (crates-io gi f_) #:use-module (crates-io))

(define-public crate-gif_parser-0.1 (crate (name "gif_parser") (vers "0.1.0") (deps (list (crate-dep (name "weezl") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "02qv38k6pn5jlwa0np72cddsqph09zpi4brgjsixhfniyghvbvsy")))

(define-public crate-gif_parser-0.2 (crate (name "gif_parser") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "weezl") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "07l5cng4md1qf40dw779wi1lbjwsrkp1kpacjmcgnxz29lcldj4p")))

(define-public crate-gif_parser-0.2 (crate (name "gif_parser") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "weezl") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1zhgpxszgr213n4dn6bs8axx1q1x692yggfw9cv6gj3vxc0p0rb7")))

