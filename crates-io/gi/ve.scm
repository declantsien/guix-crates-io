(define-module (crates-io gi ve) #:use-module (crates-io))

(define-public crate-give-0.1 (crate (name "give") (vers "0.1.0") (hash "05rad08m5va0ly3q05gh5yxk4srk6pqzyj7syj1xdz2rp5xmq0x8") (yanked #t)))

(define-public crate-give-me-some-fibonacci-0.1 (crate (name "give-me-some-fibonacci") (vers "0.1.0") (hash "06py3w26w5qhl4f1828v45qxb5cxz3ypi6644a78ryv3n7fc1y9c")))

(define-public crate-give-me-some-fibonacci-0.1 (crate (name "give-me-some-fibonacci") (vers "0.1.1") (hash "0rqcvb8kd835cjm89h5wiis4s947vda2k1896jrk8n4npaa8ax3k")))

(define-public crate-give_a_sheet-0.1 (crate (name "give_a_sheet") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "18jyyjgpfibm0mn34svzlp6r5gwqvqv2vwla1rnqx7a7vva8nsvl") (yanked #t)))

(define-public crate-give_a_sheet-0.1 (crate (name "give_a_sheet") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0rvq7aw3yjx4qjq1aw2l3f7aplvgg5wyfln3yipd5v2bzw1wjf4m")))

(define-public crate-give_a_sheet-0.1 (crate (name "give_a_sheet") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "012ca4kffba2khkgc5jisbhrc4hpr9iv6r0w7m5a210kw4pdd147")))

(define-public crate-giveup-0.1 (crate (name "giveup") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "107axinz7a64x7s6ijjayvvr4q6f94w1zvp59mz5m6zwn0pp4dqp")))

