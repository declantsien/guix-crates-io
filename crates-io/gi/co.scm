(define-module (crates-io gi co) #:use-module (crates-io))

(define-public crate-gicopy-0.1 (crate (name "gicopy") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "1gf51n24bdk0k8bjrj91sbpxlpha0m6ydmz1wcanwzzbxycvslgp")))

(define-public crate-gicopy-0.1 (crate (name "gicopy") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "15cj85h7h10gxlb3x9vyx27v22lnsx7sbsxmvh4930s0rzl5rrxg")))

