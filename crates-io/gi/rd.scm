(define-module (crates-io gi rd) #:use-module (crates-io))

(define-public crate-gird-0.1 (crate (name "gird") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.6") (default-features #t) (kind 0)))) (hash "070s6f1b7kiw26accp7rb4b8c3vxmx61yrnvqvcr72gbf5nyapf6")))

(define-public crate-gird-0.1 (crate (name "gird") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.6") (default-features #t) (kind 0)))) (hash "17z7yzxk8s6p7aq8zi9nvacfac8q5p3wlp9hs8gc3808abynh8zd")))

(define-public crate-gird-0.1 (crate (name "gird") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.6") (default-features #t) (kind 0)))) (hash "0kvvwpm6cqcbfiazggvdgbyc1hxk4gnjfk1nix798vrsscng6qn4")))

(define-public crate-gird-0.2 (crate (name "gird") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.6") (default-features #t) (kind 0)))) (hash "04v3gqkqfaxq7iy5ds7wfrpm2gsrvy7w831dzi7gsc472rldmrym")))

(define-public crate-girder-0.1 (crate (name "girder") (vers "0.1.0") (hash "0mv7k2vn5fwj5gr7gydwr3p9fr891q4cm7fp7hrciw95crazvb5x")))

