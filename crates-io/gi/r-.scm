(define-module (crates-io gi r-) #:use-module (crates-io))

(define-public crate-gir-format-check-0.1 (crate (name "gir-format-check") (vers "0.1.0") (hash "1fx7xcmm7s7xmqnj0fh119m2lpkw5w3zb4d0317x21z7ymw6rmqb")))

(define-public crate-gir-format-check-0.1 (crate (name "gir-format-check") (vers "0.1.1") (hash "0znl4qcgmg0656zk9vjkwdn9wj1zpkd0m0l5jnzmahd80ii7vf4b")))

(define-public crate-gir-format-check-0.1 (crate (name "gir-format-check") (vers "0.1.2") (hash "0cnl6gmcpyxjqvbq3v92c7vz6pg9lw19bmpamw5qxzdsgfk9qf5p")))

(define-public crate-gir-format-check-0.1 (crate (name "gir-format-check") (vers "0.1.3") (hash "05n4g8yqkyzrnxbqyrkjqjxsfxdy3q78jk0ny54ffv2qm09sjp9s")))

