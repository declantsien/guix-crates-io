(define-module (crates-io gi _t) #:use-module (crates-io))

(define-public crate-gi_t-0.0.1 (crate (name "gi_t") (vers "0.0.1") (hash "1y531pchcr25vv55iqyiarlp43xrxgbls9ddddcdgzad6531g3ay")))

(define-public crate-gi_t-0.0.2 (crate (name "gi_t") (vers "0.0.2") (deps (list (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0f4gn05b589b0ymhrb37bb589f8wwwd9jgqagwlmfka8licznk45")))

(define-public crate-gi_t-0.0.3 (crate (name "gi_t") (vers "0.0.3") (deps (list (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0lpa22qirzy8is861a7pxzjzd03isrn1j7lksz3vk8sp6pzwdy74")))

(define-public crate-gi_t-0.0.4 (crate (name "gi_t") (vers "0.0.4") (deps (list (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "07qw1jwd85j8nan4r1x5bpk6mnb6xbffivkag7yaip74mwfk8d0g")))

(define-public crate-gi_t-0.0.5 (crate (name "gi_t") (vers "0.0.5") (deps (list (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "1nvxbfvib4ap37a9jzqdapz57k7gk1mp14w6rdly2v5xijyn4g69")))

(define-public crate-gi_t-0.0.6 (crate (name "gi_t") (vers "0.0.6") (deps (list (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0ciy1vvmiyx9v8raa1fjbhymhkhx5xrcv1an19p45ss2iwqzk75f")))

