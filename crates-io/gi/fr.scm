(define-module (crates-io gi fr) #:use-module (crates-io))

(define-public crate-gifriend-0.1 (crate (name "gifriend") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0hwkjb42f7g8mim5mff1117zshf5afbac3flw0a6vjx3shqxcdcc")))

(define-public crate-gifriend-0.1 (crate (name "gifriend") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "lzw") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1r40mn1h934dwlr1dn7npvmyb51cakivc6wx1x0cyygaqsncyw6r")))

