(define-module (crates-io gi tn) #:use-module (crates-io))

(define-public crate-gitnr-0.1 (crate (name "gitnr") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22.0") (features (quote ("default"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("default" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "tui-input") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.7.1") (features (quote ("gzip" "native-tls"))) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "0m2x7d1idnm9izwc14mbzsy9m3ih4q2wswxj14z414q7zj9rkd5b")))

(define-public crate-gitnr-0.1 (crate (name "gitnr") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22.0") (features (quote ("default"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("default" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "tui-input") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.7.1") (features (quote ("gzip" "native-tls"))) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "0zs9fv0lpxswv3j2cm81vnqd9bc6zv9x676k3ikajx78yhaa2wi0")))

(define-public crate-gitnu-0.0.1 (crate (name "gitnu") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0ivzvl93q227prwrnlqq8lpghvxxd5djk4xysp8zwq0j363pp879") (yanked #t)))

(define-public crate-gitnu-0.0.2 (crate (name "gitnu") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "11i49iabrgyh5y95blm205vqpcjgnifrgh5n9lhcgidq1hjws97v") (yanked #t)))

(define-public crate-gitnu-0.0.3 (crate (name "gitnu") (vers "0.0.3") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1blvcfja9ygr157fyy5gl8vwqg35irlnlmhfqldr6a9qxli6s03m") (yanked #t)))

(define-public crate-gitnu-0.0.4 (crate (name "gitnu") (vers "0.0.4") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "07k9wgxbkzfsmja3wshvaz79cc4acw6l72rnv43vb4fc8bj2q388") (yanked #t)))

(define-public crate-gitnu-0.0.5 (crate (name "gitnu") (vers "0.0.5") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0j3z89i24akyijaa49ak0m2nhr3my97000hsib1sdpbf2ipnlgz9") (yanked #t)))

(define-public crate-gitnu-0.0.6 (crate (name "gitnu") (vers "0.0.6") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1ac7jvxf4gn49kcg289j4pwjbs1rvy6hcsvlqy91s62kmqj88fzi") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "033xahkdp6hyn75hhq8y900dgdlygjfa63qpfxkql5hl9wx3kjyj") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0qal7px1yqkscs9hpsr0qwc9n0qwbfgzn2x669l4vrdrr19gcxk4") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1aqqy83l1x4qxjjjbiq8vflgwaj3pkc5z0x7ihcsa1war3sd1lhj") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1k5739bf5gy5bf43dvd5xyvypb3idm1l06iidl3s2samffp46k18") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.4") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "122m93p1i4yz64w0xi0r2myibb4sv9g0abvqlf1rky5hj38rhsg8") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.5") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1czyl8j9asvyylj87i89nn3lmna536k8nlm8mmd9pgg0k3bsg64i") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.6") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0rlzan6yib5ga3dqsd2ccdkmbwp20219w1r1g5rcv0pgn9x00w84") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.7") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0sfmk8gc0653rnbvvvbvi59hbpyrv1ff04x16ay6rmhv5kn2sw5m") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.8") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1fnrr6mg7b961lsq9dy34hd504yvrkxqhswrmnl5mb169592lv6n") (yanked #t)))

(define-public crate-gitnu-0.1 (crate (name "gitnu") (vers "0.1.9") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1n4szbpnk5dx9jkfp0cgzsgxp6ksbrg559yf8lsazq3bdwp8mhy1") (yanked #t)))

(define-public crate-gitnu-0.2 (crate (name "gitnu") (vers "0.2.0") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1ns3wb7wnvzkq7rbkrdv6j62wyym9yw6nhmc9i0pxrjs4vwwj9g1") (yanked #t)))

(define-public crate-gitnu-0.2 (crate (name "gitnu") (vers "0.2.1") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "12hfq7ms3viyd7jgwp7ppfy2h8n35ql1qfpkw0s104wjdm7ws7iv") (yanked #t)))

(define-public crate-gitnu-0.2 (crate (name "gitnu") (vers "0.2.2") (deps (list (crate-dep (name "git2") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0v178xxl69mfllai623cqzs2skfy2i8rvfjf3s74x6zcb049kyjp") (yanked #t)))

(define-public crate-gitnu-0.2 (crate (name "gitnu") (vers "0.2.3") (hash "0q7dz7vphf5h0jm5jmk98w2vyfpf7pb11mf5afcg0ninhg7ydymp") (yanked #t)))

(define-public crate-gitnu-0.3 (crate (name "gitnu") (vers "0.3.1") (hash "0v7krpxv8wvvqiblipljijc8qm006hzyxmcyc3kazncv9a8v3asv") (yanked #t)))

(define-public crate-gitnu-0.3 (crate (name "gitnu") (vers "0.3.0") (hash "1h6x8bw73wpf6h9csd6z057k6w5x2xqw7g74skzdgpcyvbc0n7s5") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.0") (hash "0xa7gbamd8lxccpji6279srj2rbyqc11pc4fjk86m0j7wyxhx34c") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.1") (hash "1p7kg1d3gxg34lkxrb45m068xdmqx7imn8fsq5idhrj5rlivlqf4") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0h0mbvg2d0zyl7k4n6nk28gm5ipidsl94dan0j9x8sw4z89hcknr") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "18fbjf0r51460wx6mlnf9q45psa9904z9h7d74ii7f9ppkam2sm1") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1klyr7b0qdfcj3k59dwzn9chvcc5rm6v80cy4w39374p4nlbvq6p") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.5") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0i5yryl7af77f01d1njzv28vcyf9jvn8xa3k1s7x1yf9f9fx0ha8") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.6") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0xvdwdi2yhgm268fvxn46sm8f76hw54fq3xfznd71c955qf3zxyz") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.7") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "13rxlbbfxj5dil1j75wk5bxjrv1krb6wk5vvhijxzvyvqgahj389") (yanked #t)))

(define-public crate-gitnu-0.4 (crate (name "gitnu") (vers "0.4.8") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0mn4cnjzwx0avi6gyhqn8ql011zrknayqzk1qr50k96ap8hcsb8g") (yanked #t)))

(define-public crate-gitnu-0.5 (crate (name "gitnu") (vers "0.5.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0j2lc1ha07axm1ps26n6nvyply6l6hn0fsvsbxjlpz521a5ghx7d")))

(define-public crate-gitnu-0.5 (crate (name "gitnu") (vers "0.5.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "183wbrbxj16yc1fddk5f0v81xnbam1s7z99a7blwzmd172bc76lp")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "063sy7vlnwmdvq552vk0cl191zljdhyiplci67xvh0fahbdhkvbh")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1scwzrn6grgpqcnx1p18r2hngcab491js3j9kszg0hjba8pvbywl")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1q6dzphwsv6kmwaay1rd6d2mp4xwkrx4is1zwkqwkqxwsh12d6d5")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "02d126c2x2sfmhxg7n89n5bw1qxslzs08v5zwrgaslzrphr8dyv5")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0yq42laim465bq94mf791iiaa74rmc07vcksf37na0fff0kl4d9h")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.5") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0bg9fmdmy1dvs3wclqj35njxwadrwkbqxknjdg8k266ryalyq8f1")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0nyy9700nhd7c9xcy68i74d9rx4ykfx2bpplagbv4il5x3dpgzw1")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0vhdmpnndgx5l9397b48f4cqlzqlgm75qpb289k9b0yal69j8gni")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "043dh8gp4f3gjn6md2bl4kzg59yxb9gjwqimv4r5y6lysnpdbkmj")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0h6zqzpbwxms378434d8j9yvxw2yv2cx995zkhjq8hjg5g7x56ng")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha5") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "05nw7a9459cxxcvyawnxygv7ja3h4g8nybxqvi0w8p0rzlxvh6v9")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha6") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "113hbf1hxlrs5v9njk7k385dszfalg094krzzphlsm79hhvywlqj")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha7") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1bgyc2467mxgr4gprg6lyl7glcswfp1myfyw7ks7qbakwj0ifd05")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha8") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1qdjsmsw37rhcbq2s2q2i2nj0ca5d2qszhmxi52d511a7czwwykg")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.6-alpha9") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "19yzwfvp30q1f8jdmd9q2pqz1dpxz59j8d7qqkm4bf4sfm2i55qi")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.7") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1rk0viahyvyv37sjxwfs7k5g3gsikxbv4q7ngk7igijjh4n8q4j2")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.8") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1iv4yf65x316m9prqhimjj8bjwgsm8a6faygfk5pgg9918ckgg38")))

(define-public crate-gitnu-0.6 (crate (name "gitnu") (vers "0.6.9") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "147mqwfb0v26zzn2qhdhpxqlrkpcbhkd39qpg1g93a1qacp4zxd5")))

(define-public crate-gitnu-0.7 (crate (name "gitnu") (vers "0.7.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1w26s4v4sj8rbdk1bbldcy499mbybcl6cfxp7p4h45zd8i3m2b4n")))

(define-public crate-gitnu-0.7 (crate (name "gitnu") (vers "0.7.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1ya3s6c91zq6lijxjr3s20wjprkix7hsv721b953f4sajl88sbpv")))

(define-public crate-gitnu-0.7 (crate (name "gitnu") (vers "0.7.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0nnwrac0ai7s9dsdij0g8wvjm3wziiwg0nz8q8j2r9xdm1x8870f")))

(define-public crate-gitnu-0.7 (crate (name "gitnu") (vers "0.7.3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "04537mj07nh4l87q4rsab5wwvrd00w4ccbpqjvd1cjp098rnvn5j")))

(define-public crate-gitnu-0.7 (crate (name "gitnu") (vers "0.7.4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0zi7gmgsl3hp5nz8vdmdpkv4wfbqnjircpi2qpy95g6ch3dl3r6v")))

(define-public crate-gitnu-0.7 (crate (name "gitnu") (vers "0.7.5") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1fgbjqzalbi409j31sgmaf46fq9f781m0c82dsxpg5qxsv1n3yx3")))

(define-public crate-gitnu-0.7 (crate (name "gitnu") (vers "0.7.6") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1jjlidfp1fx97gqxw7mcmxayv46c4gkqsblrn59lws0jki49983k")))

