(define-module (crates-io gi ld) #:use-module (crates-io))

(define-public crate-gild-0.1 (crate (name "gild") (vers "0.1.0") (hash "1xrk0dy51h68ksl4i91zl2qaad3ffvjrm55wg66fvili91ik1kix")))

(define-public crate-gild-0.1 (crate (name "gild") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x13gi10rfq2xrcvxxk2k8qsinap6m514czala3avlwh4nq23047")))

(define-public crate-gilder-0.1 (crate (name "gilder") (vers "0.1.0") (hash "1sxvj0h25lr5av3519q9c2w0wvyyiwndzw6aiq3k2813py97gpbz")))

(define-public crate-gilder-0.1 (crate (name "gilder") (vers "0.1.1") (hash "07dfz1ias4x2102k3plzd78jn2c2baz5jas3x9r017nf4jv7yaqp")))

(define-public crate-gilder-0.1 (crate (name "gilder") (vers "0.1.2") (hash "1pfj70b59i9ldailmy668r5d6b375rhf0jni5ib7wd9203404lm4") (yanked #t)))

(define-public crate-gilder-0.1 (crate (name "gilder") (vers "0.1.3") (hash "0zwrmrfskp80bw9hbj69l4r1fg83z47xxp9c3fjrxswr1qylmpzj")))

(define-public crate-gilder-0.1 (crate (name "gilder") (vers "0.1.4") (hash "1v2xrlaraa3159qhjs3skasgbijrh4xgqz55l94ibmil7gzj9c0l")))

(define-public crate-gildlab-cli-0.1 (crate (name "gildlab-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.3") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "graphql_client") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "multihash") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1lwd82zg8zicjnkc84f33wpplw827y3r0r1k432dx4v1118rmhms")))

