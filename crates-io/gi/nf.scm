(define-module (crates-io gi nf) #:use-module (crates-io))

(define-public crate-ginfo-0.1 (crate (name "ginfo") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.7") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0icrv25myr5ighqmn10j9444kn84hz51mpal81bgs8q55q999cbk")))

(define-public crate-ginfo-0.1 (crate (name "ginfo") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.21.7") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18km72z7723ggycjv0w4ah8xm01rwdxi7xxgsn0my556y6zwy8nc")))

