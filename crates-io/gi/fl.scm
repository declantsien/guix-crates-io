(define-module (crates-io gi fl) #:use-module (crates-io))

(define-public crate-giflar-0.0.1 (crate (name "giflar") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "045a4mcrhfzgxkcis386gxvm88zgwqvj7sgm1l1lbi51q2ds11kj") (yanked #t)))

(define-public crate-giflar-0.0.2 (crate (name "giflar") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1izp2zz4hsnzszhl634fm8y08p4p5jzhgp7g7kl3h12lc2mz0328") (yanked #t)))

(define-public crate-giflar-0.0.3 (crate (name "giflar") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03n05j8m1sqaddnfcbx2gbjh2zg2vnq26m0pndpnaav40h2c1zax") (yanked #t)))

(define-public crate-giflar-0.0.4 (crate (name "giflar") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1b77r6m5rz2kgnsa2ihbidhbq8bk24djcldd320ka8jqn6c8h0kv") (yanked #t)))

(define-public crate-giflar-0.0.5 (crate (name "giflar") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "108v6lyxzn2h5l19i1h57klj0flphllxkvv51y40b7rykc7l4fmr") (yanked #t)))

(define-public crate-giflar-0.0.6 (crate (name "giflar") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "00ixgq0ifhw1f72fclby5rk265r51x5wryk7i5yg9cgm1yxmz1lw")))

