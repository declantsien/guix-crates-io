(define-module (crates-io gi mi) #:use-module (crates-io))

(define-public crate-gimic-0.1 (crate (name "gimic") (vers "0.1.0") (hash "1i8xig65qlxq24fxd9lkx3lk248zrz1hzmrnmi5prm6ci454nhf8")))

(define-public crate-gimic-0.1 (crate (name "gimic") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)))) (hash "1ss90kr68bykdil68hsag55p6fdz1w07lvicnwki3l5zhgrz84gm")))

(define-public crate-gimic-0.1 (crate (name "gimic") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "1g37r1ah43sdwkxgd23svpad8jjwvdm3ba24a6fzl83rs64hwyj5")))

(define-public crate-gimic-0.1 (crate (name "gimic") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "1ya089rnd8p4mgz5r7472r4f4z554p9hcxxmm5csaf4bn0xh5mhc")))

(define-public crate-gimic-0.1 (crate (name "gimic") (vers "0.1.31") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "0l3gzlfr2khalv67a9an8dr2vdkjn7sjxkjzp5596qyamd28wzwv")))

(define-public crate-gimic-0.1 (crate (name "gimic") (vers "0.1.32") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "093jcsrwmdgfyfl8bfp993zl4si1lq4069q1p7sgh5jarp6dw8px")))

(define-public crate-gimic-0.1 (crate (name "gimic") (vers "0.1.33") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)))) (hash "06422nwdkd6s92w1cpbqccl2sw3dy1r20xgbmnrk3hk4smc358hc")))

