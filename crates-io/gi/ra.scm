(define-module (crates-io gi ra) #:use-module (crates-io))

(define-public crate-gira-0.1 (crate (name "gira") (vers "0.1.0+reserved") (hash "1i8591qwma0mmj0n838qi9x4bjpxlz35vq4g46zby1brfrhkahnx")))

(define-public crate-girage-0.0.0 (crate (name "girage") (vers "0.0.0") (hash "1dw9gfv71478cs7f50sm750gysdln28y6k0wgfd1imgivnxp3bsx")))

