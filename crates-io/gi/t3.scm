(define-module (crates-io gi t3) #:use-module (crates-io))

(define-public crate-git3t-0.1 (crate (name "git3t") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "graphql_client") (req "^0.12.0") (features (quote ("reqwest"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "036mwp2z4crniyasljvm677pm60g1f5vn2szp5zs8hs814f1pygf")))

