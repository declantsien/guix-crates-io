(define-module (crates-io s2 pa) #:use-module (crates-io))

(define-public crate-s2pac_ch32v103-0.1 (crate (name "s2pac_ch32v103") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "riscv-rt") (req "^0.12.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "svd2rust") (req "^0.31.5") (kind 1)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1w7f06syiixh5xhiy6s907ihlhmwg11dfg8w9nrfqbi76x4c2gm1") (features (quote (("default" "rt")))) (v 2) (features2 (quote (("rt" "dep:riscv-rt"))))))

