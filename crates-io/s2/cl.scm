(define-module (crates-io s2 cl) #:use-module (crates-io))

(define-public crate-s2client-0.1 (crate (name "s2client") (vers "0.1.0") (deps (list (crate-dep (name "protobuf") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "182n04zalhj2djaclvw1dwfyb90d8wgbmn6ls6ygsxfj6niiscw4")))

