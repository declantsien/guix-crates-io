(define-module (crates-io gx i-) #:use-module (crates-io))

(define-public crate-gxi-derive-0.4 (crate (name "gxi-derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vv00lxm58qzggjziycd3brjbpnxrzyrq95gy7im5hs763x9aydm") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-derive-0.5 (crate (name "gxi-derive") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1las986axh9ks2wdhc643ac00af22z1ysn0jmfm9j011al0r87d3") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-derive-0.6 (crate (name "gxi-derive") (vers "0.6.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0v50vxsq7cxd4pxgzgxsvwgn2nb26g6irkq6jxph0zzlsmfr8023") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-derive-0.6 (crate (name "gxi-derive") (vers "0.6.1") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vd55ycac75s62n90v98zf6samrifdk8cwkw4vnp341aqq56n172") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-macros-0.4 (crate (name "gxi-macros") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yn8qw4g5hrfqgbk6b51rcbwkxnhi4x2hccvm8bqj67a7ddw0phv") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-macros-0.5 (crate (name "gxi-macros") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rji22rn2pjlwwsmriazmv1zqxwl5rmhwbyb6kcl9yww2r7f7p67") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-macros-0.6 (crate (name "gxi-macros") (vers "0.6.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08zxvvzy61cqv7sak4r22vhi0j0hinravlj344izhrw24vxfqsl6") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-macros-0.6 (crate (name "gxi-macros") (vers "0.6.1") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fgfjds6zn0bd2gba9v4222a190hp7dmgfdg5ad4f51kdjylif9m") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-transpiler-0.4 (crate (name "gxi-transpiler") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g9dplgv3y2ndsm7xq55rbix595xhf559wz55dqr6ccpghj15y28") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-transpiler-0.5 (crate (name "gxi-transpiler") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l8mylps2c0hv532zs029n5lagrwk6svs04mq1wx4c86ibpkd6z6") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-transpiler-0.6 (crate (name "gxi-transpiler") (vers "0.6.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08sib21pgb6nc6ikhcax1bda6pq04sbpijvq5jlp2r2bgvrif0fa") (features (quote (("web") ("desktop") ("default"))))))

(define-public crate-gxi-transpiler-0.6 (crate (name "gxi-transpiler") (vers "0.6.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g2f5yxbvb45lfc98s8a88m3hfmy5agizqfhh6vmcj76pwx5pyds") (features (quote (("web") ("desktop") ("default"))))))

