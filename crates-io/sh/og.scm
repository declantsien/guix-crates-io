(define-module (crates-io sh og) #:use-module (crates-io))

(define-public crate-shog-0.1 (crate (name "shog") (vers "0.1.0") (hash "0kgchrlmiz1l79rwwwxmjyf804dkgxs1rzyp9jv9p1jqzny4ag30")))

(define-public crate-shogai-1 (crate (name "shogai") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1crwsajqlljmakpy5jkyxg9i3hvh82dhnljwgx5sz8rmfra56vxn")))

(define-public crate-shogai-1 (crate (name "shogai") (vers "1.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1z533jfhfkzf5p1kg9xffp9b4a2awbyqq4jq1lc1qbnln6likcjb")))

(define-public crate-shogai-2 (crate (name "shogai") (vers "2.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1lyx8v3pwwmabhqv7b5amgx7w3m3g7zlacy32n4lzhy63p5hw7d2")))

(define-public crate-shoggoth-0.0.1 (crate (name "shoggoth") (vers "0.0.1") (hash "044mc9b78s0lnadhqz86dcwsz44g4wc5kh8a3yai4rnbrdbhsvfa")))

(define-public crate-shoggoth-0.0.2 (crate (name "shoggoth") (vers "0.0.2") (hash "10i0rsi6ag7in28ygcvmwnlgcrnr1hwmnlcq6zzbirlnbkm3is8a")))

(define-public crate-shoggoth-0.0.3 (crate (name "shoggoth") (vers "0.0.3") (hash "04f0aparg789hgph6pxnkm3vk62zdiknjdxynp58d6sjj54484n5")))

(define-public crate-shoggoth-0.0.4 (crate (name "shoggoth") (vers "0.0.4") (hash "05llivc37075dclr94s04npcdmfpzwci4h55kq070fpvm7v1bxf1")))

(define-public crate-shoggoth-0.0.5 (crate (name "shoggoth") (vers "0.0.5") (hash "07mry5dsamxfz4mqs2id9qpjlb9l0lp66jannwz4b2nzlrdikqrn")))

(define-public crate-shoggoth-0.0.6 (crate (name "shoggoth") (vers "0.0.6") (deps (list (crate-dep (name "unify") (req "*") (default-features #t) (kind 0)))) (hash "0machhdz91dwz9chj9k78f0gjhjq40p6gh2l553x6hlx006wyswl")))

(define-public crate-shoggoth-0.0.7 (crate (name "shoggoth") (vers "0.0.7") (deps (list (crate-dep (name "unify") (req "*") (default-features #t) (kind 0)))) (hash "15fqzzxzakh75n5g3kx93d4m0fj4r237jalbfw90ys5yqy0plm7k")))

(define-public crate-shoggoth-0.0.8 (crate (name "shoggoth") (vers "0.0.8") (deps (list (crate-dep (name "unify") (req "*") (default-features #t) (kind 0)))) (hash "1gxvak60r8zmdwladg4lxfchlv30wv4x6wjcy2k48br4imbh07bh")))

(define-public crate-shoggoth-0.0.9 (crate (name "shoggoth") (vers "0.0.9") (deps (list (crate-dep (name "unify") (req "*") (default-features #t) (kind 0)))) (hash "0lc9vkfzbmqq1yq8nivz5nklb7i3kgh2n365cwplg0qnc7x5ki96")))

(define-public crate-shoggoth-0.0.10 (crate (name "shoggoth") (vers "0.0.10") (hash "05hxi2a2aja64jmp4yjvl71wqpyqcc1i86kjgpk4xj4p6n0zz8s7")))

(define-public crate-shoggoth-0.0.11 (crate (name "shoggoth") (vers "0.0.11") (hash "06akvrb4bklwp9wm9s2qfcblynw2h2xdw8ziishdzp4fxy0cfs1b")))

(define-public crate-shoggoth-0.0.12 (crate (name "shoggoth") (vers "0.0.12") (hash "0iiy83p0ypx2vzhhngjbyrdq2m63wjp2k4qis8bfsvng545p4xx1")))

(define-public crate-shoggoth-0.0.13 (crate (name "shoggoth") (vers "0.0.13") (hash "00nbla5aih074qs4dgzf3zc4ggydcfkd4zfjvzsb89y2fnmmsh2g")))

(define-public crate-shoggoth-0.0.14 (crate (name "shoggoth") (vers "0.0.14") (hash "00kkhqbb4aqd6wficflknn2kfbyklzhcgy3vsfvrhn313yr70783")))

(define-public crate-shoggoth-0.0.15 (crate (name "shoggoth") (vers "0.0.15") (hash "101dwqxqwb6p7wpf163wcffrhsqrh8askavjlg4ihdpxqry6x74n")))

(define-public crate-shoggoth-0.0.16 (crate (name "shoggoth") (vers "0.0.16") (hash "1ky82xhnbv4knkwc8n74hn6d9hw97g9csskcb6n6n4pzwh5a7cf4")))

(define-public crate-shoggoth-0.0.17 (crate (name "shoggoth") (vers "0.0.17") (deps (list (crate-dep (name "shoggoth_macros") (req "*") (default-features #t) (kind 0)))) (hash "0fgip2n299kidspclryndia2nkqm5154bszzl41gqfn7b1qci71m")))

(define-public crate-shoggoth-0.0.18 (crate (name "shoggoth") (vers "0.0.18") (deps (list (crate-dep (name "shoggoth_macros") (req "*") (default-features #t) (kind 0)))) (hash "1wca4gk07wz8xay0vf4dzcy3bf1rf7cpd2pbi4xj97c54ki6nyh5")))

(define-public crate-shoggoth-0.0.19 (crate (name "shoggoth") (vers "0.0.19") (deps (list (crate-dep (name "shoggoth_macros") (req "*") (default-features #t) (kind 0)))) (hash "18sl73khana7l7dbffcjjkaglqdpdxsq5njyijq67pncrmk1z1i4")))

(define-public crate-shoggoth_macros-0.0.17 (crate (name "shoggoth_macros") (vers "0.0.17") (hash "1hcsrjf7bylsbdz5cri42b8wyzm9kpcwbzzqgqwy6rjyhwrx7i06")))

(define-public crate-shoggoth_macros-0.0.18 (crate (name "shoggoth_macros") (vers "0.0.18") (hash "1h649d9xy7crbsf1sclkvq688n6ncsxph3r0l82hm1v907ph3n28")))

(define-public crate-shoggoth_macros-0.0.19 (crate (name "shoggoth_macros") (vers "0.0.19") (hash "1psb33cb1i371f7iinh99q6348xiqylvr2hbahy0905v6m01lgz4")))

(define-public crate-shogi-0.1 (crate (name "shogi") (vers "0.1.0") (hash "1qbsh77mki9mqh10cqywmzrkzhp8jkxrig0z9a50mjq1l2dwz08s")))

(define-public crate-shogi-0.2 (crate (name "shogi") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0icsiw2399rcdsrdjhgymf280sgdh54djcfq3lqh17r0lafdhq1y")))

(define-public crate-shogi-0.3 (crate (name "shogi") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1s3fjzqjnw2pqfmkfvk107wf79fjhkjnbrxr72ry38daps8gscym")))

(define-public crate-shogi-0.4 (crate (name "shogi") (vers "0.4.0") (deps (list (crate-dep (name "itertools") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1a33ykajvnymy3p11cisijs4pshy9zxhg72m6l62knrxh5vmh2by")))

(define-public crate-shogi-0.5 (crate (name "shogi") (vers "0.5.0") (deps (list (crate-dep (name "itertools") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "10hfkhmsnsw3cx8x4v640g1kqzkjz2dzg4f6b1s85j54j47xhzrz")))

(define-public crate-shogi-0.6 (crate (name "shogi") (vers "0.6.0") (deps (list (crate-dep (name "bitintr") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "14j5388hygx60hi3p62gl40352k49rnw5jz73bqbsfyclrdrkdjh")))

(define-public crate-shogi-0.7 (crate (name "shogi") (vers "0.7.0") (deps (list (crate-dep (name "bitintr") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1dq7a0nkzxhnf4kycm520s3y638g0yqhzrmcyi9i73bfmpd1wl3z")))

(define-public crate-shogi-0.8 (crate (name "shogi") (vers "0.8.0") (deps (list (crate-dep (name "bitintr") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0jk34kfsw9vm1h90rnq6yr5v0c7pdlasfyks19hf3sk9sa7pwsyi")))

(define-public crate-shogi-0.9 (crate (name "shogi") (vers "0.9.0") (deps (list (crate-dep (name "bitintr") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "165fjchpw4yka9jipm7hadlal170prpymd6cx68cmbk455wam7yp")))

(define-public crate-shogi-0.10 (crate (name "shogi") (vers "0.10.0") (deps (list (crate-dep (name "bitintr") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1mxn590mlhp4gi4c22wq4j0f4sh3qh6zmas5qvbh25zdcqp8r1fg")))

(define-public crate-shogi-0.10 (crate (name "shogi") (vers "0.10.1") (deps (list (crate-dep (name "bitintr") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "16jm9p046jxgf6b3nj5mbmhac6zxy4ksc5589yacb78ssm1jjjlm")))

(define-public crate-shogi-0.10 (crate (name "shogi") (vers "0.10.2") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0xjgvhvvpn4f3jsnnwjpa24dasc689argi378h2k23fzh5iijfja")))

(define-public crate-shogi-0.10 (crate (name "shogi") (vers "0.10.3") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "18jklksy8wiynca2kgk9fmn1xg7d0a10asj9a1sh1ffgxgbxw53s")))

(define-public crate-shogi-0.11 (crate (name "shogi") (vers "0.11.0") (deps (list (crate-dep (name "bitintr") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i8cwh9k0nm96ps34zaydvc71acwiv349z775j1zn1cdfa01707w")))

(define-public crate-shogi-0.12 (crate (name "shogi") (vers "0.12.0") (deps (list (crate-dep (name "bitintr") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1635jnncbfxrn3iyx9gmzsxa3kqm6jfw8lx4p77pxv8m85gn8jks")))

(define-public crate-shogi-0.12 (crate (name "shogi") (vers "0.12.1") (deps (list (crate-dep (name "bitintr") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zjr2zs036ldwwbc6na3w3x6y9a1ck328a4i9yif6gfnb8634swk")))

(define-public crate-shogi-0.12 (crate (name "shogi") (vers "0.12.2") (deps (list (crate-dep (name "bitintr") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ixcb7dfkq51w6bsgyz1nvi3x1mj238vq78kidk1b2i49xw91qn9")))

(define-public crate-shogi-img-0.1 (crate (name "shogi-img") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (features (quote ("png"))) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0xbmq9if4cgcbr008d3d894m9p2pwgcjf00d3hn4ijz9x7s9lz1g")))

(define-public crate-shogi-img-0.2 (crate (name "shogi-img") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (features (quote ("png"))) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0llv5rmp5ddclvq5rqsqc716indihfg1i2n8s02ryfb5l3fj10kh")))

(define-public crate-shogi-kifu-converter-0.1 (crate (name "shogi-kifu-converter") (vers "0.1.0") (deps (list (crate-dep (name "csa") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.31") (default-features #t) (kind 0)) (crate-dep (name "jsonschema") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "shogi_official_kifu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "06iq4hy7h2211yslgdcivk29l26c15rxn0y9pgnfaq3xmhiawva7")))

(define-public crate-shogi-kifu-converter-0.1 (crate (name "shogi-kifu-converter") (vers "0.1.1") (deps (list (crate-dep (name "csa") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.31") (default-features #t) (kind 0)) (crate-dep (name "jsonschema") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "shogi_official_kifu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b80pgid3yrybz30pwy64b1mz3gdhx7zni38j4zncpd2lf0v3xcv")))

(define-public crate-shogi-kifu-converter-0.1 (crate (name "shogi-kifu-converter") (vers "0.1.2") (deps (list (crate-dep (name "csa") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.31") (default-features #t) (kind 0)) (crate-dep (name "jsonschema") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "shogi_official_kifu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vss14qrzhkydi496gc9ll30jq4kidib8m08lj8hnvpd0kvm84rm")))

(define-public crate-shogi-kifu-converter-0.1 (crate (name "shogi-kifu-converter") (vers "0.1.3") (deps (list (crate-dep (name "csa") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.31") (default-features #t) (kind 0)) (crate-dep (name "jsonschema") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "shogi_official_kifu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jcbz18gjpi9r5ilywymanksyc73wj45q5vrx13yzlan9cpdqjp9")))

(define-public crate-shogi-kifu-converter-0.2 (crate (name "shogi-kifu-converter") (vers "0.2.0") (deps (list (crate-dep (name "csa") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.31") (default-features #t) (kind 0)) (crate-dep (name "jsonschema") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "shogi_legality_lite") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "shogi_official_kifu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "15pvj1ksx69j9r0vbgika8p4jyw1wy1cjzpmrv5hcw575qa42s0m")))

(define-public crate-shogi-kifu-converter-0.2 (crate (name "shogi-kifu-converter") (vers "0.2.1") (deps (list (crate-dep (name "csa") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.31") (default-features #t) (kind 0)) (crate-dep (name "jsonschema") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "shogi_core") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "shogi_legality_lite") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "shogi_official_kifu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "08cj0jjg9w16070jj5a549mrfsi7bqk3f6awgsfqh0bvvc98z56w")))

(define-public crate-shogi_core-0.1 (crate (name "shogi_core") (vers "0.1.0") (hash "1n71q8bvxjyfrabk8gy2fslkm7fdh9584smr5wg2kk69bjwrnhxp") (features (quote (("std" "alloc") ("ord") ("hash") ("default" "std") ("alloc")))) (rust-version "1.60")))

(define-public crate-shogi_core-0.1 (crate (name "shogi_core") (vers "0.1.1") (hash "0d3syivqpm3fg98sm6vq2nm02cs4kf7giilg6x91q9vjyny9s77v") (features (quote (("std" "alloc") ("ord") ("hash") ("default" "std") ("alloc")))) (rust-version "1.60")))

(define-public crate-shogi_core-0.1 (crate (name "shogi_core") (vers "0.1.2") (hash "0fkh66gaqw3as141fibqjgzvp0wyvva3rcnfc1r32qbjmsfs2pgi") (features (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (rust-version "1.60")))

(define-public crate-shogi_core-0.1 (crate (name "shogi_core") (vers "0.1.3") (hash "01khjvda2k5q05r8szv16dln59wdq9svy3bldnnw6y2fiplw073z") (features (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (rust-version "1.60")))

(define-public crate-shogi_core-0.1 (crate (name "shogi_core") (vers "0.1.4") (hash "0bfgxys4j7y53cq858i3hb3z7ngas7imd4fdqjx5iyvxcq3rrjgh") (features (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (rust-version "1.60")))

(define-public crate-shogi_core-0.1 (crate (name "shogi_core") (vers "0.1.5") (hash "055aj60wb1iqj8w7jcd3i9pljjn963as3ga6cc5vyrkwzzj7s2ix") (features (quote (("std" "alloc") ("ord") ("hash") ("experimental") ("default" "std") ("alloc")))) (rust-version "1.60")))

(define-public crate-shogi_legality_lite-0.1 (crate (name "shogi_legality_lite") (vers "0.1.0") (deps (list (crate-dep (name "shogi_core") (req "^0.1.3") (kind 0)))) (hash "0i6dj1770843y91syxp1dn7n0ib2frd1ynjz2cywcg3dqrafimmm") (features (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (rust-version "1.60")))

(define-public crate-shogi_legality_lite-0.1 (crate (name "shogi_legality_lite") (vers "0.1.1") (deps (list (crate-dep (name "shogi_core") (req "^0.1.3") (kind 0)) (crate-dep (name "shogi_usi_parser") (req "=0.1.0") (kind 2)))) (hash "05jgz7vavi3ampnbljsgy0krs5r4i2bwdlypzbwrl9lja1zkn6sa") (features (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (rust-version "1.60")))

(define-public crate-shogi_legality_lite-0.1 (crate (name "shogi_legality_lite") (vers "0.1.2") (deps (list (crate-dep (name "shogi_core") (req "^0.1.3") (kind 0)) (crate-dep (name "shogi_usi_parser") (req "=0.1.0") (kind 2)))) (hash "0zh079iql9dyrghy1cycl77a54yb97gs51v35rrvfysp0qlf2vbh") (features (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (rust-version "1.60")))

(define-public crate-shogi_official_kifu-0.1 (crate (name "shogi_official_kifu") (vers "0.1.0") (deps (list (crate-dep (name "shogi_core") (req "^0.1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "shogi_legality_lite") (req "^0.1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "shogi_usi_parser") (req "=0.1.0") (default-features #t) (kind 2)))) (hash "0f7nqimsaa9lwpl5hz15djhczljc0k11mzrv3nn290wcfffp967l") (features (quote (("std" "shogi_core/std" "shogi_legality_lite/std") ("kansuji") ("default" "kansuji" "std")))) (rust-version "1.60")))

(define-public crate-shogi_official_kifu-0.1 (crate (name "shogi_official_kifu") (vers "0.1.1") (deps (list (crate-dep (name "shogi_core") (req "^0.1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "shogi_legality_lite") (req "^0.1.1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "shogi_usi_parser") (req "=0.1.0") (default-features #t) (kind 2)))) (hash "1h6hhwmj73qdscqrcp2chy54wxphbny1wg9gigpcj9yc510s06hr") (features (quote (("std" "shogi_core/std" "shogi_legality_lite/std") ("kansuji") ("default" "kansuji" "std")))) (rust-version "1.60")))

(define-public crate-shogi_official_kifu-0.1 (crate (name "shogi_official_kifu") (vers "0.1.2") (deps (list (crate-dep (name "shogi_core") (req "^0.1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "shogi_legality_lite") (req "^0.1.2") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "shogi_usi_parser") (req "=0.1.0") (default-features #t) (kind 2)))) (hash "1qhglcyvpjkfk6il1fvls1czj13nvpr2njh0z6rzkbqjyw9hjx8n") (features (quote (("std" "shogi_core/std" "shogi_legality_lite/std") ("kansuji") ("default" "kansuji" "std")))) (rust-version "1.60")))

(define-public crate-shogi_usi_parser-0.1 (crate (name "shogi_usi_parser") (vers "0.1.0") (deps (list (crate-dep (name "shogi_core") (req "^0.1.1") (kind 0)))) (hash "0qlcxy6a8xjq734r1gvypj2s7ayr9bhkgci3jmfbrw2w0nrdm6pk") (features (quote (("std" "alloc" "shogi_core/std") ("default" "std") ("alloc" "shogi_core/alloc")))) (rust-version "1.60")))

(define-public crate-shogiutil-0.1 (crate (name "shogiutil") (vers "0.1.0") (hash "0n4gaw4bd0a2340gzl03sn5g76c0nbd6lqwdb3rmf5lbs9xrjpjh")))

(define-public crate-shogiutil-0.2 (crate (name "shogiutil") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0bswkzc8f2azka781yhy3dl6yj72yymnmzqlwddbb60ncxz0lf6b")))

(define-public crate-shogiutil-0.3 (crate (name "shogiutil") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "01ns59nkn60y4bhzfw8l5a41rhcv4fq7mhjr1fwm62m0yxd41dfp")))

(define-public crate-shogiutil-0.3 (crate (name "shogiutil") (vers "0.3.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1cnv4ywaaz13wv1ld74zajazgxml6lk4v3ywzdr9wbi31rq7qv1p")))

(define-public crate-shogiutil-0.4 (crate (name "shogiutil") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "011x6rhc2988li4098v1ndzkvyavgn46q39s404dg2k5hafadm1y")))

(define-public crate-shogiutil-0.4 (crate (name "shogiutil") (vers "0.4.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0kxr91jvinckj3y99nrqbxrmivg1d74ixsgg3dlclcmf6kkw5g4k")))

(define-public crate-shogiutil-0.5 (crate (name "shogiutil") (vers "0.5.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "19lcifp2g825a2xd5db456i2v460i3yryfdw74hcpn07g1272gm0")))

(define-public crate-shogiutil-0.5 (crate (name "shogiutil") (vers "0.5.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "186d0gw3np5zkqn1apw5qs2ask4zpvkzfmaal9sgb769xm9r1nwm")))

(define-public crate-shogiutil-0.5 (crate (name "shogiutil") (vers "0.5.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "15ddgk29qxaxvky47q6s9dwpyw5a9fhkxbpiygz11cnffpqnsp0a")))

(define-public crate-shogiutil-0.5 (crate (name "shogiutil") (vers "0.5.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "04552fspqyi1gqypjy9kc7ghrvb3gyisffl9fkk44fpm1jrswrnh")))

(define-public crate-shogiutil-0.5 (crate (name "shogiutil") (vers "0.5.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "04wvg65awcphgxvbhxlzi8nckg53afbvdxbpsjzs854rl50xglbx")))

(define-public crate-shogiutil-0.6 (crate (name "shogiutil") (vers "0.6.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1rzhnsbc4qq0g1qmmvxxji5g1sb3wazld9rzjapfm8qf8bwzdxzl")))

(define-public crate-shogiutil-0.7 (crate (name "shogiutil") (vers "0.7.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0n10g0dv0vm5x08ak7y4ygqc3knmw0xbbr3x1fl0127zsdihb8dc")))

(define-public crate-shogiutil-rs-0.1 (crate (name "shogiutil-rs") (vers "0.1.0") (hash "1ghdhgr3xyc1ag05p648cj5wwa6cxx52dn2r2ha64k9qaasnr9jj") (yanked #t)))

(define-public crate-shogo-0.1 (crate (name "shogo") (vers "0.1.0") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.5") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "1g7ll8hzn2svvbpnpq55vpqllwwnvgj92zn1kd58wj2agmnz8acs")))

(define-public crate-shogo-0.1 (crate (name "shogo") (vers "0.1.1") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.5") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "14q4lc9rx70mvhzmm7jswhvfka2y95dlgrpim2291bvwff16rhap")))

(define-public crate-shogo-0.2 (crate (name "shogo") (vers "0.2.0") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.5") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "0ap30wyf9shd5vr8njpi5xhnb7hpzlaz82as8j43fxkhlimrsm64") (features (quote (("simple2d"))))))

(define-public crate-shogo-0.2 (crate (name "shogo") (vers "0.2.1") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.5") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "0ghk2k1rxiiwkhyn5nvsq7ai1akf8yvmp2i02vbi15rz8gc0s196") (features (quote (("simple2d"))))))

(define-public crate-shogo-0.3 (crate (name "shogo") (vers "0.3.0") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.8") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "1dcb996p7j87qdpk8p413cll7hp9lvb4qkyalwvmghx1370v1bwp") (features (quote (("simple2d"))))))

(define-public crate-shogo-0.4 (crate (name "shogo") (vers "0.4.0") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.8") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "15n793jf973pnznallxlh4vbd56d4728vpg38ffx6nmm4xyd8x3d") (features (quote (("simple2d"))))))

(define-public crate-shogo-0.5 (crate (name "shogo") (vers "0.5.0") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.8") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "0xvc1imczypyywi8hqbc68ymqmmckf101xk43jkg9v417p0ajvbk")))

(define-public crate-shogo-0.5 (crate (name "shogo") (vers "0.5.1") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gloo") (req "^0.8") (features (quote ("futures"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("OffscreenCanvas" "MessageEvent" "DedicatedWorkerGlobalScope" "Worker" "WorkerOptions" "WorkerType" "DomRect" "CanvasRenderingContext2d" "MouseEvent" "Touch" "TouchEvent" "TouchList" "ErrorEvent" "Performance" "Document" "Element" "HtmlCanvasElement" "KeyboardEvent" "WebGlBuffer" "WebGlVertexArrayObject" "WebGl2RenderingContext" "WebGlProgram" "WebGlShader" "WebGlUniformLocation"))) (default-features #t) (kind 0)))) (hash "0llxqkfpflij78xypvbfp4zcq286hl2n165dpn7yq7lh0lm2jfi9")))

(define-public crate-shogun-0.1 (crate (name "shogun") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "shogun-rust-procedural") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08xhzsw5gc10d912m2z04x2y85s7ymp1kv4634f1lrgwy77p257p")))

(define-public crate-shogun-0.1 (crate (name "shogun") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "shogun-rust-procedural") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "shogun-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1phsk2ys61iaxxslqv54mnl7rhn4252avp37qscrc9c51knwcl3i")))

(define-public crate-shogun-rust-procedural-0.1 (crate (name "shogun-rust-procedural") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dhv340xg0dfcsy57n068zs7yljzqwpa9l5n06v6lqp5mrslkgky")))

(define-public crate-shogun-rust-procedural-0.1 (crate (name "shogun-rust-procedural") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "shogun-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "15ndfydqpakahid1m4r1p3yjmxmk40p672qnw2i02w6qq8gqg54m")))

(define-public crate-shogun-sys-0.1 (crate (name "shogun-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0ckh9gd1y0h6mpipcw81z3qy27acx96qngscjhrs5wkds6afa8yq")))

