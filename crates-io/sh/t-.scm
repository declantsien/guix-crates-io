(define-module (crates-io sh t-) #:use-module (crates-io))

(define-public crate-sht-colour-0.1 (crate (name "sht-colour") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)))) (hash "1f5znz056921y6s7ihmxg6m8ww2ajy3c2dnr3n1d51lq3ia28zy7")))

