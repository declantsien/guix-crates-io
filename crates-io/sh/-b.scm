(define-module (crates-io sh -b) #:use-module (crates-io))

(define-public crate-sh-builtin-bash-0.1 (crate (name "sh-builtin-bash") (vers "0.1.0") (deps (list (crate-dep (name "sh-builtin-bash-bindings") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sh-builtin-bash-proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p3kh9vcxzqchmph864mrdf1nfh0impavfg46pwgxmhxsc9fy0kc")))

(define-public crate-sh-builtin-bash-bindings-0.1 (crate (name "sh-builtin-bash-bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "0w76iwrinm0pab9zi41nx2p6i2420kmhdjl5847cb94gngfhn3vk")))

(define-public crate-sh-builtin-bash-proc-0.1 (crate (name "sh-builtin-bash-proc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sh-builtin-common-util") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0j1gjwfb35b2z7kxd93bw5dgc4iygxrajvxccdlh34hjpshikwd3")))

(define-public crate-sh-builtin-common-util-0.1 (crate (name "sh-builtin-common-util") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0c6l561hiiyq1g3zqfc5k7j9ch75lczxnir848fxz7jn11wgv6gl")))

