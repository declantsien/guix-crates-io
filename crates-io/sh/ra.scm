(define-module (crates-io sh ra) #:use-module (crates-io))

(define-public crate-shrapnel-0.1 (crate (name "shrapnel") (vers "0.1.0-alpha.29") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1zsl9bi14zy4401zj1p4ny1bnf1kwwkf90gs3k9flzvhrrg2qfsl")))

