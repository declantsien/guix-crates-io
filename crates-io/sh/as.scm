(define-module (crates-io sh as) #:use-module (crates-io))

(define-public crate-shas-0.0.1 (crate (name "shas") (vers "0.0.1") (hash "08c37hw4ppwikmbgrpgip99baa4km1r4qcgpxlx05rb2rg0qj9z4")))

(define-public crate-shash-0.1 (crate (name "shash") (vers "0.1.0") (hash "03w9yq4pajxycd8kklvc69m44yvy0diszlr51zqjjfj6q7bllas7")))

(define-public crate-shash-0.1 (crate (name "shash") (vers "0.1.1") (hash "03mwjfkhiqfyy4radm8bd088zl9j35az3vl4yq0z20azyqy2apvd")))

(define-public crate-shasum-0.4 (crate (name "shasum") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cb7cnc236m400hdxfw588yv006xza3h016c2fy0krzi1y7ks2pf")))

(define-public crate-shasum-0.5 (crate (name "shasum") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bn7vqklqiipv0ryl990chsvzvp9r7rp161hqcb7b75sx6awg32r")))

(define-public crate-shasum-0.5 (crate (name "shasum") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1634141jrzzr1cd078yzqkhnz33bzknbpy70dqnxhmzsnpcygpx5")))

(define-public crate-shasum-0.6 (crate (name "shasum") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "02w129hgl92bg4b8vq20imr65ya84lqbgax56yz2sa52c1ln0gav") (yanked #t)))

(define-public crate-shasum-0.6 (crate (name "shasum") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "14cgq2c564pycnkjvm427lj7kll7awm8pfci289rh4h8wc4sr25y")))

(define-public crate-shasum-0.7 (crate (name "shasum") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)))) (hash "1yzdzixcfz9m87f7yacxkcpkzv90l7qkbdkar8rdrw4yf2dksfy9")))

