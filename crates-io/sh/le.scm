(define-module (crates-io sh le) #:use-module (crates-io))

(define-public crate-shlex-0.1 (crate (name "shlex") (vers "0.1.0") (hash "0bc2ys49j7jmb7n58q5hr4spj0c2gsx4fzwrwyscivnjkw3kyzmz")))

(define-public crate-shlex-0.1 (crate (name "shlex") (vers "0.1.1") (hash "1lmv6san7g8dv6jdfp14m7bdczq9ss7j7bgsfqyqjc3jnjfippvz")))

(define-public crate-shlex-1 (crate (name "shlex") (vers "1.0.0") (hash "0gf773p2snqpw69rzh8s1wdlq8dc8c1ypmiv516il1fdyb46i9a2")))

(define-public crate-shlex-1 (crate (name "shlex") (vers "1.1.0") (hash "18zqcay2dgxgrd1r645mb79m4q745jcrqj659k11bwh99lx8bcj3") (features (quote (("std") ("default" "std"))))))

(define-public crate-shlex-1 (crate (name "shlex") (vers "1.2.0") (hash "1033pj9dyb76nm5yv597nnvj3zpvr2aw9rm5wy0gah3dk99f1km7") (features (quote (("std") ("default" "std"))))))

(define-public crate-shlex-1 (crate (name "shlex") (vers "1.2.1") (hash "0zzn7iyq9r71hw2w7zyq10pz9hy23jrghx187zkqvq6287mdvgm2") (features (quote (("std") ("default" "std"))))))

(define-public crate-shlex-1 (crate (name "shlex") (vers "1.3.0") (hash "0r1y6bv26c1scpxvhg2cabimrmwgbp4p3wy6syj9n0c4s3q2znhg") (features (quote (("std") ("default" "std")))) (rust-version "1.46.0")))

