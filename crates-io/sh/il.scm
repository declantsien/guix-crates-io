(define-module (crates-io sh il) #:use-module (crates-io))

(define-public crate-shill-0.1 (crate (name "shill") (vers "0.1.0") (hash "06vhrsnzs00dznqln0mmb47v9m4kpckv80qryyawm264wi0qcnkr")))

(define-public crate-shill-0.1 (crate (name "shill") (vers "0.1.1") (hash "01pq8j0ga9mcff2rmg4dlpwsrf5dnrwv2n5mjjsgnxh2apyrpykh")))

(define-public crate-shill-0.1 (crate (name "shill") (vers "0.1.2") (hash "1nchy5c29dzh6vb0iyc2jswgab0zzy45vlfymyr6bqb83zpv00li")))

(define-public crate-shill-0.1 (crate (name "shill") (vers "0.1.3") (hash "0s5mjwnf0ab13d7k515cyfxnbyd5k5fwm6ndchk28fjgp7p4535d")))

