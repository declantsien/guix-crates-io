(define-module (crates-io sh ea) #:use-module (crates-io))

(define-public crate-shea_grrs-0.1 (crate (name "shea_grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)))) (hash "1vj1nrp7xqbligq5fc3x2fb57q5zp9x63i9za31h8clqnnl93q90")))

(define-public crate-sheaf-0.0.1 (crate (name "sheaf") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1nfr7iyxkx55gymnadvhj2xhm6mgqb73wrawwm1s3zl71afcq4ik")))

(define-public crate-sheatmap-0.1 (crate (name "sheatmap") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1pswrgl81pz2j4p3kbvs6705p1ky6q03ffhb8zhnij8fjv1sykhd")))

