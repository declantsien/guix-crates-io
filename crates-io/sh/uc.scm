(define-module (crates-io sh uc) #:use-module (crates-io))

(define-public crate-shuc-0.1 (crate (name "shuc") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0x3kiw0l6y2jdwl001qhr8crk0rbaj8w7qhr8ynm9xfmg59yjqc7")))

(define-public crate-shuc-0.1 (crate (name "shuc") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1fmaqfkw2l0513sx5dxrspkwsnd1d60270nsgclnigqvrlm4id2q") (yanked #t)))

(define-public crate-shuc-0.1 (crate (name "shuc") (vers "0.1.2") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "066xdcgdpga654r5wlss266wz0dp6lpw7vq77h6rwnx186gcniy4")))

(define-public crate-shuck-0.0.0 (crate (name "shuck") (vers "0.0.0") (hash "0wpri0w3az3mlpa0xv3q5mk36qqnw6z0pb49xrnyzdprdn49rm47")))

