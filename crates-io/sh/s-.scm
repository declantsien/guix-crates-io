(define-module (crates-io sh s-) #:use-module (crates-io))

(define-public crate-shs-hl-bot-0.0.0 (crate (name "shs-hl-bot") (vers "0.0.0") (hash "0xxik5l4w7frpnida10cvxcyhac2ylj51320mgq0h24iqfhwr65a")))

(define-public crate-shs-hl-bot-0.0.1 (crate (name "shs-hl-bot") (vers "0.0.1") (hash "1x35mj8f4jznhp3r6l0smddp3hgzhw98nfh17pbhfl9110rixq3k")))

