(define-module (crates-io sh at) #:use-module (crates-io))

(define-public crate-shatter-0.1 (crate (name "shatter") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1wn51vradx4fy9w6dyx1vz073b2mxsl038fj31jfcvnpzy9jyzdr")))

(define-public crate-shatter-0.1 (crate (name "shatter") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0d0qwchvq2zp01s3gil8jnankpjaljfgp61p0zk11yvyq22aiq8k")))

(define-public crate-shatter-0.1 (crate (name "shatter") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0xs119kz668kknn5fkzvqnyhzkjfhpfjglvg08x9ly05z0bd6wjc")))

(define-public crate-shattuck-0.1 (crate (name "shattuck") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "070hiw6sjngq3hdj6zrjh40cg6mp9bzf70559yb1wx1vfk7lwx8d")))

