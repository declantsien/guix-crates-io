(define-module (crates-io sh ie) #:use-module (crates-io))

(define-public crate-shield-0.1 (crate (name "shield") (vers "0.1.0") (hash "06rzayq5qkfsxph59mk0d9hrnldjhram1kwgny1m0r4wqhpvg6w3")))

(define-public crate-shield-maker-0.1 (crate (name "shield-maker") (vers "0.1.0") (deps (list (crate-dep (name "ab_glyph") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "034kvfsqdgd531inam2ypi4vdis4q75pps6xjg5kcnsqb2szadci")))

(define-public crate-shield-sim7000-1 (crate (name "shield-sim7000") (vers "1.0.0-alpha.1") (deps (list (crate-dep (name "at-commands") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "00221szhf8bm0ljg78s0kmzi1rvn40n8na66mzjl4nw2smr1l9wq")))

(define-public crate-shield-sim7000-1 (crate (name "shield-sim7000") (vers "1.0.0-alpha.2") (deps (list (crate-dep (name "at-commands") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1bzdvk7akm76bn9r680j3blh44yb9zypmsgrwb3rwbd8fkpcv024")))

(define-public crate-shielded-0.1 (crate (name "shielded") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.14") (default-features #t) (kind 0)))) (hash "0yc5qljilivs617qmzq580plzv9kmmnazl2ax71hcpj4z0avb6bp")))

(define-public crate-shielded-0.1 (crate (name "shielded") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)))) (hash "1qny8srmn9scjpjzad0f7ilgjdn66npwar0cnk4kxkb43hcr9lpv")))

(define-public crate-shielded-0.1 (crate (name "shielded") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^1") (kind 2)) (crate-dep (name "ring") (req "^0.17") (default-features #t) (kind 0)))) (hash "12ciiyfaxkl92vpp076da72h6537w9ws7m53j4j8by62wsw5lpbf")))

(define-public crate-shieldify-0.1 (crate (name "shieldify") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.11") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "03raihmzl35x3pka0a0dbbn36bw2p3iy52z321nrzw52cqymg6yy")))

(define-public crate-shieldify-0.1 (crate (name "shieldify") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.11") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "0y2ksgnmy1g1ahhgvp8pwcg0npfhhz20bvlsmn70xisj3l8b1gc8")))

(define-public crate-shieldify-0.1 (crate (name "shieldify") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.11") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "129k819r1ry5y5m9qqgrvj5ysrrmm1xsyzvgh1zidfar4p3dzb7w")))

(define-public crate-shieldify-0.1 (crate (name "shieldify") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.11") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1s09r05cyqlbc9by6mnd7kk3clymz3id2l8ifv83ipkm4p1wjgql")))

(define-public crate-shields-io-0.1 (crate (name "shields-io") (vers "0.1.0") (hash "1k7jg00wypq5qlvqf5zar8axfhzimaqwla8r44gfyvc8w7flig7h")))

(define-public crate-shields-test-dummy-crate-msrv-3452398210-0.69 (crate (name "shields-test-dummy-crate-msrv-3452398210") (vers "0.69.0") (hash "1vz5f6zzlxlp9p882ch99bwmjlwm4kcgwn5flmznpd0jkbqdkvcs") (rust-version "1.69")))

