(define-module (crates-io sh pr) #:use-module (crates-io))

(define-public crate-shproto-rs-0.1 (crate (name "shproto-rs") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "18dqy8fi94239bakqqcwbffv5ksx048wlsfqg45nx2sx0s7v8p5w")))

(define-public crate-shproto-rs-0.1 (crate (name "shproto-rs") (vers "0.1.1") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "0qsaldzj6klkj3jvxz4z3f1cpb5j5m3zpw8h741qgn8ncqlsxbrr")))

(define-public crate-shproto-rs-0.1 (crate (name "shproto-rs") (vers "0.1.2") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "14md87fn63lfwnfgxf9wy8gmn9rr56dmcdfsxpclw9rg5893y67r")))

(define-public crate-shproto-rs-0.1 (crate (name "shproto-rs") (vers "0.1.3") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "0qar48w0ln0wrxy5z5zlfwv78ij14pm1ra0h99347lnz904zkzw9")))

(define-public crate-shproto-rs-0.2 (crate (name "shproto-rs") (vers "0.2.0") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "17gvx1r61qcfyhv4hfm3nrv2zg17gqs0p3dwczxhk838miq9ciyb")))

(define-public crate-shproto-rs-0.2 (crate (name "shproto-rs") (vers "0.2.1") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "0gwjhvaqglv4a2g1zkp77zf48v2n723cfrpkdmsy6fa8zkp92mq7")))

(define-public crate-shproto-rs-0.2 (crate (name "shproto-rs") (vers "0.2.2") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)))) (hash "14ainqw7zdsgci56nrif3pzkfb4dbkrzq59n18d47i8wjyzyh4vy")))

