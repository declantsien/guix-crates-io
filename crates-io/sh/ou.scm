(define-module (crates-io sh ou) #:use-module (crates-io))

(define-public crate-should-0.1 (crate (name "should") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1q4jd8f0877p7p9am8292mah8mprggcmhc0yrxz56q44yyzymn7k")))

(define-public crate-should-0.2 (crate (name "should") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "0c34ccy4nf5vff16shw8dxdccar9ffa4b0cbq93pyczry8y76siq")))

(define-public crate-should-0.3 (crate (name "should") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zp61y11yd6zxdr81zlwn7w2bpbmnn09irdr7jhd10my4p8avjj0")))

(define-public crate-should-0.4 (crate (name "should") (vers "0.4.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "14l080agh1yp6pjb658zw5cjlyg0kjlbpgbdjnhfll1xjc24qy98")))

(define-public crate-should-0.4 (crate (name "should") (vers "0.4.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1753b9hdchxb4nc9gpnl6lcq26fxqajqsmmw7l0i7b9khbnf7dyl")))

(define-public crate-should-0.4 (crate (name "should") (vers "0.4.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1jqimp8hx8m5ma8p18dgpxr5l71xjflgfr8z0hjwpf837ybb0wwa")))

(define-public crate-should-be-0.1 (crate (name "should-be") (vers "0.1.0") (hash "00cy4hr0ywlzccnpq5fbs2cvvj0ycmihqxl1918fkj6d3w4ayckx")))

(define-public crate-should-color-0.1 (crate (name "should-color") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "0paxsv0alx1nzlmvqg4y1xxr2fp74qipvw9j9y6cr0kw4f4hmd29")))

(define-public crate-should-color-0.2 (crate (name "should-color") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "0fw079incblyc0hahsbprd8s6d6lxrsvg82qycjcbvvjfkji90m2") (features (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.2 (crate (name "should-color") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "0x2pcjkkcgc34bps0q2gnys3x3xkk1ask0r2kzmjwai597qqb391") (features (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.3 (crate (name "should-color") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "06y4gjdpyvag0cqvvvjhxxbiqidls9h7pj64c122g5zlkv6kqlbd") (features (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.4 (crate (name "should-color") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("color" "derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "0w11wl0crx4d6z9jnp3bayaa3vqmf20vz1cf2pipsydd9s7bp8qs") (features (quote (("no_color") ("default" "no_color" "clicolor" "clicolor_force") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.5 (crate (name "should-color") (vers "0.5.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("color" "derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("cargo" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "0rkqyikm7y4p81ayv3qs2c5y0n6y61qqc0zlabbj1jpwijfi8pp5") (features (quote (("stream" "atty") ("no_color") ("default" "no_color" "clicolor" "clicolor_force" "stream") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.5 (crate (name "should-color") (vers "0.5.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("color" "derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("cargo" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "termcolor") (req "^1.1.3") (default-features #t) (kind 2)))) (hash "0hggc5bnmd599hqsf0gn288zq99aij6pa8qakzzd8k8c5nk9nxnq") (features (quote (("stream" "atty") ("no_color") ("default" "no_color" "clicolor" "clicolor_force" "stream") ("clicolor_force") ("clicolor"))))))

(define-public crate-should-color-0.5 (crate (name "should-color") (vers "0.5.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("color" "derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("cargo" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "document-features") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "termcolor") (req "^1.1.3") (default-features #t) (kind 2)))) (hash "0f3q8pvanplahzza2ablc4vyrxkhg46ks4ckbfvyhnvlhxhw4l0h") (features (quote (("no_color") ("default" "clicolor" "clicolor_force" "no_color" "stream") ("clicolor_force") ("clicolor")))) (v 2) (features2 (quote (("stream" "dep:atty") ("clap" "dep:clap"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.0") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ykyvcgbf0jhr3xy7jxym242949v7b5q8y0gl3j63d2ygafbcdd2")))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.1") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rh0zwxxq8037ggk4hy02cx2fdclmr4j1q8mccqdprp5vmnk04zy")))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.2") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0xgj3k2rrbgdxybxhjfgz8sx221nacn8vf64d4pzbz51vxk9ls0f")))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.3") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "06n2z39vx4ag795xmc6n7x6y9pdysdypys60jgqpi3a3pa0w1jbx")))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.4") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "173cik0ykm27i6nrdhdf79x7yj8rcybrg7p2aisbfip53c8pg9hv")))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.5") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "051i8pprp225vc8hi9crj65n4q9r23riyi1bp1mnxgfch75mpbh1")))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.6") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1b5cg0niycwh0bd519qqidprgayi47930sdk966gzgwhdg2nnlqc") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.7") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0ad8if9329rlls5yc3zzisjsjqc18c57jghrxklq4b76diabkc8j") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.8") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1p1x5pmg6v7iq2dzbipl8d4br7z69d1y4y9gvaxi5r41vyw9dz6p") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.9") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0bq3pjichw5awzfnmckkk0ia7bi36d8ycb18ixp8idng5ra3nmd5") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.10") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1a4jcf6flha600793n78fkspyb1ir35bzgf16vq8lqcx11b08d83") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.11") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "19wvz34j5ryiyn4wkjifw0886dk7z0d99bii9adxjfy91ih15k3v") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.12") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1iaj1mqjn735w86dgaic1i7g4yqij7fi1xq5bs68wcn42kw3v44d") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.1 (crate (name "shoulda") (vers "0.1.13") (deps (list (crate-dep (name "shoulda_core") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0zj9dql2jc15bpc4nvr7klpb95i8hqglm06xzngrxxc43ar2ybwd") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.2 (crate (name "shoulda") (vers "0.2.0") (deps (list (crate-dep (name "shoulda_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0v73caiccvvwcnh2mfh39y9w8qjb7krrsd9bx4ivkvcx68sydh4x") (features (quote (("fmt" "shoulda_macro/fmt") ("default" "fmt"))))))

(define-public crate-shoulda-0.2 (crate (name "shoulda") (vers "0.2.1") (deps (list (crate-dep (name "shoulda_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "shoulda_macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "01lhd2dh6wdrydlyghc1wa1lr2bd0slh00sxrhpzk5nzik0fincs")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1njsn0507b0iyp5ylr7qsvr2gcznbkzi4habdfgzv2kjxpb0m2i2")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1hd1p8x1pijdv97ms7jr48fd6kvwn7ds5r3iq7f2riji2a2spp93")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0g411995wyzwv14l201skskm8hpn20ak22kqz4a1pfkbx43cl133")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0wp8qbr4w3fzik5fvjnc71dzbq7zyh14vn9gbph03c6h0aspfk9v")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1vylhfx98sa54y2adrxifibj43dljmngxfk73dkmaajbvvgb0hmj")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.5") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0ilzizk9pxb341kv0mf63jikli3pd9440n6d16ki87h35lbpl1jb")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.6") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1qpm4bs99ds54kqvvqiyn76mxx27b72i4y449g3k60x1r87kvvry")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.7") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1jqaqamk3m49arnq751g3a19kfjbw3d7z98l87riy7aa9nlh0nww")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.8") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0wz46g659kdnbrzws509yj77rp8dkfh9wsprrjhv9m2q60mv4fd9")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.9") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1dp6ylay8r2pc28kz6hclm7r2xfs0840bvjp1s1sqh8fvc1xgfly")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.10") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "impl-trait-for-tuples") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1mr57l91nlnkn41hx3hjlb4bkhrw2p6y7mmi2ai8xn49yql0iq00")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.11") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "impl-trait-for-tuples") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0bixfaai2ar89i6ijmad8bnvfahmlmzr3hf8lqvc0pgi9qql7jxi")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.12") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "impl-trait-for-tuples") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1cqjs55grljsq2sq5mdqbz9h91r9pp2fj03c0rgmngkpgszba0sh")))

(define-public crate-shoulda_core-0.1 (crate (name "shoulda_core") (vers "0.1.13") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "impl-trait-for-tuples") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1k4zam3vmq8xj1diy8sbmhxglrb65k5bxwmlnraa5l6mlhv060jh")))

(define-public crate-shoulda_core-0.2 (crate (name "shoulda_core") (vers "0.2.0") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "impl-trait-for-tuples") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "05y2fszly85v473hwvhff1w7qvagzfx4ffak1ycgfzxmfbh521fa")))

(define-public crate-shoulda_core-0.2 (crate (name "shoulda_core") (vers "0.2.1") (deps (list (crate-dep (name "const_env") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "impl-trait-for-tuples") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "09ayn3hmpp6b8vg1rmjna5h4gm2nfk8i4fyrpjjl7g9xvi4a8xmm")))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1p9xbsvk4dsy043x6k23bws7wy86igasli7qrijwgq9kjz3xvsq9")))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zwaqafqjjm5agg9wg0nzkarxflh66d22wgjn88ib9mg9nibqx3f")))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mihnylhzmpwlx96aixc47r0g6xvixbjkyzwpw3afgwiyzazl0ym")))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "03axmx0064d726jf1003ybz2dk802am8xcjxrgpcss7n3pxk0yyk")))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19gf4a7a7sw6jwc80mgsqc3laksmvjzh709kigmf5bhvsfg4bq70")))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.5") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0l6l3by3mza1gjgzsjm5642v77nllr496d9k85yc0k33pvxiv008")))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.6") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fa50lpj3vrz91lvsgps8zdr0893fbccim025xhxpx61vw8r8x6c") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.7") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vnmdf43ylrw5hk7h5rl71c1pqj4p7b36y6shwsr3v1176nnfvyf") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.8") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v8j1rs5ni6wr19avy1vj5n5fzcvrh84hhyv5yapcrwdm6nm3lqi") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.9") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d4rz3s8sxfhxcsyayyqxr81mxndp4r0ldy619sk1p9wp3fi03fg") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.10") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ns2ybpdspjyq5v6idrczdijxfwx9lqdmznimsn5lr05b0pz2fzv") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.11") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13ckang5glrhbaz1gqlpwqj65nirqz7n0il8kkjjq8xi9jm3wrxr") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.12") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l4zbaff6vicfd5bl2c718c9sjfqw6f7wi3054kqkky2amdaw4xj") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.1 (crate (name "shoulda_macro") (vers "0.1.13") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s07bvbg5vphrpkijpzsafsygml8ishhfvcjj0yafg5dpnnc9bj3") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.2 (crate (name "shoulda_macro") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "07ajvsqc8s406d52pmdkxs7iygbl8i61hgj5c5mqgf0vlyljnknx") (features (quote (("fmt" "rustfmt"))))))

(define-public crate-shoulda_macro-0.2 (crate (name "shoulda_macro") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rnvq0m0jyrd4viycwrvmr9a81dlnk4h910vpqbx0b7v8pslvqjy")))

(define-public crate-shoulds-0.1 (crate (name "shoulds") (vers "0.1.0") (hash "1n2ba6xa6xnvjy6vj5jnjzmc5sa5amjrwr5rdpx64mb3kad88yvj")))

(define-public crate-shoulds-0.1 (crate (name "shoulds") (vers "0.1.1") (hash "0hxxl507jq80bpav813asgw56igx9pbp85475c83j7z58xh5610n")))

(define-public crate-shoulds-0.1 (crate (name "shoulds") (vers "0.1.2") (hash "0dlvwk5mp3savn9phn9vwwh04gidwdkhcz7jyw8i7l5fhhcyp0k2")))

(define-public crate-shoulds-0.1 (crate (name "shoulds") (vers "0.1.3") (hash "1lyysywc14kjnpx91z6r154ff86jgvvkn3skrbrdiyxi5r0qicsr")))

(define-public crate-shoulds-0.1 (crate (name "shoulds") (vers "0.1.4") (hash "0z2z7cskysrfqv6z33h8nkz3kysb582nrc0l8b3wj9a0qja4mrrz")))

(define-public crate-shoulds-0.1 (crate (name "shoulds") (vers "0.1.5") (hash "1xpmzf4mlgkx8sa0z1v9g9isfws7j03n1sv1c69nryywm518f4id")))

(define-public crate-shoulds-0.1 (crate (name "shoulds") (vers "0.1.6") (hash "01kh8493z96358cs805d11r0f1cfm8pax01q9cc9azipikplpic3")))

(define-public crate-shout-0.1 (crate (name "shout") (vers "0.1.0") (deps (list (crate-dep (name "shout-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0i96xanfc1jkrjj3jl3fp981qpcffc1gj7vjihm7rm4viv3vynj3")))

(define-public crate-shout-0.1 (crate (name "shout") (vers "0.1.1") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11n9vnzcld2kaf2yxk4q4i53y4ppyv2za0spf3iwg8w4yxldyq8h")))

(define-public crate-shout-0.1 (crate (name "shout") (vers "0.1.2") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1njp2bf1zwljpaby5ihjmlnxp5pck5jlfmzl84dmncmkzazggwfa")))

(define-public crate-shout-0.1 (crate (name "shout") (vers "0.1.3") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0fr2sd7ikggqs5h7w910n9l1yi209ryx48ax08501z2a83iz4sww")))

(define-public crate-shout-0.1 (crate (name "shout") (vers "0.1.4") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0zyxpfvp9842rm86fn1jl5f4qhfzcknq6hfwlihfnipapn0622q1")))

(define-public crate-shout-0.1 (crate (name "shout") (vers "0.1.5") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0lydxjzn878r253dnk1bhrannrqj576b0zf431b1j40k550577zp")))

(define-public crate-shout-0.1 (crate (name "shout") (vers "0.1.6") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "029cqzsyyahy7bv504l9lmvg45n18g3nlgs6yhfrpy2vm52yygwv")))

(define-public crate-shout-0.2 (crate (name "shout") (vers "0.2.0") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1p3xfqmyp5vnz6na3q3s2wg3p2mwibdwnbl6llsmijbm2k2iy8iy")))

(define-public crate-shout-0.2 (crate (name "shout") (vers "0.2.1") (deps (list (crate-dep (name "shout-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ixf1yb6dkvdqv2108jsjrdqxz3rl71kadrrzmpq43a761g82lns")))

(define-public crate-shout-sys-0.1 (crate (name "shout-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05gcf51g460n8cs62zqd88qn8siy24afcnldd7jszb0z696d6pg6")))

(define-public crate-shout-sys-0.1 (crate (name "shout-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0db5l9372ckq2rmr33pl143nnq34ai724zmarnszj5zg55zzns4v")))

