(define-module (crates-io sh oj) #:use-module (crates-io))

(define-public crate-shoji-0.0.0 (crate (name "shoji") (vers "0.0.0") (hash "0c9srvz3z719s21ckjrxpa1igfmzkhmdvddd1bq3yyflj43w7gdx")))

(define-public crate-shoji-0.0.1 (crate (name "shoji") (vers "0.0.1") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b4np1zpsj99p6gaqfxjxrfmqwjc02jkk9cm1inhq3pz5a0vj74l")))

(define-public crate-shoji-0.0.2 (crate (name "shoji") (vers "0.0.2") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "15vv0nsyrbmg1wrgxzzjw76yzjsd1zl3b9m6m0kpy54krsfrd7vj")))

(define-public crate-shoji-0.0.3 (crate (name "shoji") (vers "0.0.3") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xbp597ji4akrymyqxhhx7wzhmxkvmzbyw36hjdy5yffdbxlppxf")))

(define-public crate-shoji-0.0.4 (crate (name "shoji") (vers "0.0.4") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r9pqf5sg0dcqp0p0invcvy9dlflrihy41qsf6ciryxg8wz6bnxc")))

(define-public crate-shoji-0.0.5 (crate (name "shoji") (vers "0.0.5") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "1drmv34hqv41d0vrrc7phk0ffn9p2dsxfbz333ccy9qqkq4b0bhi")))

(define-public crate-shoji-0.0.6 (crate (name "shoji") (vers "0.0.6") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cc0f3jy03bswwi0k649272sij03d4i9jz48c7nn1b1bp7i116wn")))

(define-public crate-shoji-0.1 (crate (name "shoji") (vers "0.1.0") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "051vhfxmbahbf7mq9qmy61vz4yld711nxaw9fmyqgj3x24xkfy4y")))

