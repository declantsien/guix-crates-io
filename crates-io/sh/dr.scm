(define-module (crates-io sh dr) #:use-module (crates-io))

(define-public crate-shdrr-0.1 (crate (name "shdrr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.5") (default-features #t) (kind 0)))) (hash "18r49zfpgvqci9i443gxcsg782la64218wymlkpfs5a973m7h7d2")))

(define-public crate-shdrr-0.1 (crate (name "shdrr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.5") (default-features #t) (kind 0)))) (hash "0d4yxpijnpzihjib1wdmq9ziz8x91bw86p5airjhbsm2y48qmlvy")))

(define-public crate-shdrr-0.1 (crate (name "shdrr") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.5") (default-features #t) (kind 0)))) (hash "0fy029laq598nm5sv6002sh7lzyy529zrkmi9m45q7d926sw4ij3")))

