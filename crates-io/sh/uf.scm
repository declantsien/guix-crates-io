(define-module (crates-io sh uf) #:use-module (crates-io))

(define-public crate-shuf-rs-0.1 (crate (name "shuf-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (kind 0)))) (hash "0b97cc81qa11mk5my747ghcw5raagicfm0qkia1hq47v5rlr5bjn")))

(define-public crate-shuffle-0.1 (crate (name "shuffle") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "13baz28dprpb3hn54jkpjmw70gmy1hvv924v6v9cssr51b6gq1jb")))

(define-public crate-shuffle-0.1 (crate (name "shuffle") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "01p70p9jwzi9lc611y26kcd9dn595nlngjvrdd547r6s2yg7ynkl")))

(define-public crate-shuffle-0.1 (crate (name "shuffle") (vers "0.1.2") (deps (list (crate-dep (name "bitvec") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1lb6bg4g8cgqiaczvvz7g92bgy7p5nl0a30rcy3frlc18q3h3lcq")))

(define-public crate-shuffle-0.1 (crate (name "shuffle") (vers "0.1.3") (deps (list (crate-dep (name "bitvec") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0xhm3bmxywbr27qvj4b4naykd9q0h7frc71z3zwhcc5gvmhi6np3")))

(define-public crate-shuffle-0.1 (crate (name "shuffle") (vers "0.1.6") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0g7nzy5s5s7qgcy1p983g0fvqp3vxgm1kxjbb4hj1jvp4xzg9zap")))

(define-public crate-shuffle-0.1 (crate (name "shuffle") (vers "0.1.7") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1cjfms6cd8y720nbq35f8s4pkq4h5znf0b0rlqi6wwp0df0bxv1g")))

(define-public crate-shufflebag-0.1 (crate (name "shufflebag") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03f8d0mp1kwkqvj09m967inlf8lklphqnr02m6saslfiagvsidsq")))

(define-public crate-shufflebuf-0.1 (crate (name "shufflebuf") (vers "0.1.0") (hash "1aq2ay8j83rdqnafyaqslqcqa78l67j3g58s83gmz2yh3awks1l4") (yanked #t)))

(define-public crate-shuffled-iter-0.1 (crate (name "shuffled-iter") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1zbyys9xqnid5cbh77x9yacfn4kr2jw34sbv017nn94k4qai1iak")))

(define-public crate-shuffled-iter-0.2 (crate (name "shuffled-iter") (vers "0.2.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "14izvzrmm9dq4jjll96j4ia5an19if92m2n74jg99j12hm8mczax")))

(define-public crate-shuffling-allocator-1 (crate (name "shuffling-allocator") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.79") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "02rk46njg8yswbvp1v5r35cvkq4ny0nbdsgzhimccrdkpyp78a5b")))

(define-public crate-shuffling-allocator-1 (crate (name "shuffling-allocator") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.79") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1pibf7db5y21ly81siby0s9pkxpgxplivdpr7h5crgs8ngbvnjkn")))

(define-public crate-shuffling-allocator-1 (crate (name "shuffling-allocator") (vers "1.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("synchapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1y47z5kzws0i6ip0qpm3aakmxqpbzba9j33lmaznxlbk8d2hm344")))

(define-public crate-shuffling-allocator-1 (crate (name "shuffling-allocator") (vers "1.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("synchapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "15n4dx86xvldq0p9vl5fbfc3k8g2rg2nzcjadw0dk2c4m5zrgsaf")))

(define-public crate-shufflr-0.1 (crate (name "shufflr") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0vpqjmsa9xsw16vnxx6fzsnrss90r7xx4b5k825d7jklzvdma62j")))

(define-public crate-shufflr-0.1 (crate (name "shufflr") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "1n25nh53p27k2iqr22w3x7b5sa7cdxcfwvlyvca9ppdik4bzfamq")))

(define-public crate-shufflr-0.1 (crate (name "shufflr") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0l3hb372pi1a1mcxqhar1rngcwqg49fbsnpx282fj4b8b9dg2rm4")))

(define-public crate-shuffly-0.1 (crate (name "shuffly") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.23.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1hsb65imhinwmvhb20zp1xqcgl1r21klmiyf0y6f4l7f299ka7wp")))

(define-public crate-shuffly-0.1 (crate (name "shuffly") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.23.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0w35p9wb10ahk7av886blxqk11ammfawzhb0ygbnggxlgwmr5mmn")))

(define-public crate-shuffly-0.1 (crate (name "shuffly") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.23.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "00rqbamrsrflykk48wpzlhl9m2s7w5s3vxh8bwspmrm7n3pl8bv1")))

(define-public crate-shuffly-0.1 (crate (name "shuffly") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.23.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0r3a1h8d0ypsh2cx86w8sh3hiv38109as7zalix8pqzp0s5l3pif")))

