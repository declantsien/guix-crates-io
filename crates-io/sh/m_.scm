(define-module (crates-io sh m_) #:use-module (crates-io))

(define-public crate-shm_open_anonymous-1 (crate (name "shm_open_anonymous") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0scyccpcka5mqjwlxrainmwzilgws6b60dm45vfv115acdx3j507")))

(define-public crate-shm_open_anonymous-1 (crate (name "shm_open_anonymous") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0h1069yxffsck6kn81qkcrc72lj7psvr3dznibkm9vjqhn3f5y80")))

(define-public crate-shm_ring-0.1 (crate (name "shm_ring") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "0gch1m50xabg4hwa5nw53q2rsxifi52danjm5zry175fh87nwvka")))

(define-public crate-shm_ring-0.2 (crate (name "shm_ring") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "0ybp6wq09746gx0yf5584z0ax8b5s3wm0digws9l8bmx65b5rlhb") (features (quote (("avx2"))))))

(define-public crate-shm_ring-0.2 (crate (name "shm_ring") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "1jn9ymi0hybsf002kls5b93xwymcb83611c25qiq3srj5swnywsw") (features (quote (("avx2"))))))

(define-public crate-shm_ring-0.2 (crate (name "shm_ring") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "020dibm5953322yr9cx9pfw111kplv5nz8m9in1b2mmd1vg1siip") (features (quote (("avx2"))))))

(define-public crate-shm_ring-0.2 (crate (name "shm_ring") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "0cjapaxqwhs3hfrzxhpn9mfrnskgbhlkpzpz7jrfapy4izlzr44p") (features (quote (("avx2"))))))

