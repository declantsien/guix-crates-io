(define-module (crates-io sh ir) #:use-module (crates-io))

(define-public crate-shirakami-0.1 (crate (name "shirakami") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1l3h56psh3l9sq9lx24kmpvjyfp7bcsd068v5xkkvc80vv5h2w19")))

(define-public crate-shirakami-0.1 (crate (name "shirakami") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1bkddnmwdfx7q7ycaw2wjkijb8fi5zq5sgfycsvxsqr3i6nmhy4w")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "05qn1frszcsbxz7jx7xp654i6ps0k5334q22h780yn2n0rj42fig")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1ycgsp8sdkpkvzak0i42rjajvckww7ysw0pv2wy9nxhhvrg7scgn")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.2") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "12mm7bad1l2n081svrz31178m3199ld47ri3k4065cqb9p5ivqhk")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.3") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0hrdfmmb2qwxfj3h0mq8ih174cjlhlwa24nm4p1r4q04ywpg2cqs")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.4") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "18p6b1v5jm4h1rk0s5l61z25vdqbm9kp9m9dzvs5azk904p3q4yx")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.5") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1aiym1m5kd5j3paqd39d5j9156vp5j2qnic9a0z2s9a63j5pi8x2")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.6") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "061zpc7x98j5bhcq8cq3049h0p85pd9s1n0y2zniafy1d7m98b8k")))

(define-public crate-shiratsu-naming-0.1 (crate (name "shiratsu-naming") (vers "0.1.7") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "185b4023ss77q1p3nwwwv6kzx13j30x235njz291m0v3cqfci67f")))

(define-public crate-shiritori-0.1 (crate (name "shiritori") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "0scixbyi8vwkzzb7l3q2fm6dq5ar7x06a08lmb8bii62md60zcik")))

(define-public crate-shiritori-0.1 (crate (name "shiritori") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "0p8mn9c2r2a4cfc13r6vcnan63hfs1fx6wm4inv2kngyy788c912")))

(define-public crate-shiro-0.0.1 (crate (name "shiro") (vers "0.0.1") (deps (list (crate-dep (name "base16ct") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "base64ct") (req "^1.6.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "md2") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)))) (hash "0l73dblpkc8g10wi1wl3z8b4fjh0503m45s52y1lihv2mxpq58n8")))

(define-public crate-shirodl-0.1 (crate (name "shirodl") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (features (quote ("suggestions" "color" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "console") (req "^0.14.1") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y45309wh3g8s3l5pcg0pvm9k93d3mr6p1yivnddzdqmz86qqmf0")))

(define-public crate-shirodl-0.1 (crate (name "shirodl") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (features (quote ("suggestions" "color" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "console") (req "^0.14.1") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("socks"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03j0rhywj6vaxrcr6vgpkid19yl3a7vx0jw0pgnlv1xpslmsbwiv")))

(define-public crate-shirodl-0.1 (crate (name "shirodl") (vers "0.1.3") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (features (quote ("suggestions" "color" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "console") (req "^0.14.1") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("socks"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ychq9bai3x5w60hkhrfxh4vbcy7h2rnfv3qxfn6h3c96npnf029")))

(define-public crate-shirt_giveaway-0.1 (crate (name "shirt_giveaway") (vers "0.1.0") (hash "1f28rwpvw02sa3ninx01s4swa9c7x9fap08k30g2cqcw06968i9f") (yanked #t)))

(define-public crate-shiru-0.1 (crate (name "shiru") (vers "0.1.0") (hash "1rid2q9379b56bhvrlfi64dngnh9fpki627bb7ldb7l1kkzajcv1")))

(define-public crate-shiru-0.1 (crate (name "shiru") (vers "0.1.1") (hash "1mqr2wsx9n2122iwcpaf51pmyini2q3jl4yvhv2xj5pqjgnvbznq")))

(define-public crate-shiru-0.1 (crate (name "shiru") (vers "0.1.3") (hash "0knjy02n6njfps4agayzc00q072sz273gn4av9r8vxqjdmx791ba")))

(define-public crate-shiru-0.1 (crate (name "shiru") (vers "0.1.4") (hash "0fzahxxhphysp7xyk53w1y860r0w8h6x2wx4g5rx1z6lm35k1n2h")))

(define-public crate-shiru-0.1 (crate (name "shiru") (vers "0.1.5") (hash "1rc3d1qq93gh8ggy1i3llqapnlyhx35cxn6r922kwwswx6clakry")))

(define-public crate-shiru-0.1 (crate (name "shiru") (vers "0.1.6") (hash "189sgx0rykyfilnwqz7lp0pxqhvyphzx26c3ng77bxw32w2hzvc1")))

(define-public crate-shiru-0.1 (crate (name "shiru") (vers "0.1.7") (hash "098akc2f95qvfnmg4h28c8jhcvava65ilg22vl53q8aplnqx6q58")))

