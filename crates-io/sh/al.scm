(define-module (crates-io sh al) #:use-module (crates-io))

(define-public crate-shalc-0.1 (crate (name "shalc") (vers "0.1.0") (deps (list (crate-dep (name "slog") (req "^2.5.2") (features (quote ("max_level_trace" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "slog-stdlog") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "19dx8v5mqdf7xw816rjg1z4i0adz185a1p6cb3131lsc1cckhjry")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1id61528fnk6rgzn4a527rmarlqk4k90qp0qd3282am3h4mrjnad")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "01kad73637zbs29j93cn99fvahalpd3fy4jgydgm7s4dlnj90cb4")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1lx4w6gkr08lr1bi9bpf5azp5p7pp708ys3jlf38k9f0mb1p7d1g")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.3") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "084kks4gi424w8zbk7z1g7h4gr0jjbhj5w28pxlr8d2ci2qvis8l")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.4") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0nq636n58a0blrr9mdvqwhl4fifqslcbxhg6n3xal2as4mlgvfvn")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.5") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "05bc5ynpwnqjc1cn8vlqvcrqblr7a2c9fwvm5gxw478s02drnh1p")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.6") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "17fh5p4smfz2wmpixcqw172igmgpw3fmn27hr6m89mid54h00xcv")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.7") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1nn1qs20a1j7fby4k795cdzyvj5p4p6vm0vqpq41wjzmx92xv8z3")))

(define-public crate-shale-0.1 (crate (name "shale") (vers "0.1.9") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1sairayvf2iyiy6migmiwmpzil92allm4rnbxdmbkdl95b5v5p8k")))

(define-public crate-shall-0.0.0 (crate (name "shall") (vers "0.0.0-0.0.0-0.0.0") (hash "0q75wcrk05z3ijg6w63gfsbfzwssh7i31hvlfyiiccy3xg0ivig0") (yanked #t)))

(define-public crate-shall-0.0.0 (crate (name "shall") (vers "0.0.0--") (hash "1zc6cd3gbjzb42vng3p5ljgzqwbrn6d5hpqj84mllhld3vya23b7") (yanked #t)))

(define-public crate-shallot-0.1 (crate (name "shallot") (vers "0.1.0") (hash "1gynxmizcsnz5cnhixdacadw4hxihkg4cc18dsmxdbl76vnv8d1n")))

(define-public crate-shallow-0.0.0 (crate (name "shallow") (vers "0.0.0") (hash "0qlj811gbz3zqg6v3vc5n5bhqcyjzlwd777xqnpz34amz85fkcaz") (features (quote (("default"))))))

(define-public crate-shallow-0.0.1 (crate (name "shallow") (vers "0.0.1") (hash "1d4zq4mig46p0mvzra6c5skxjz4d1xkq600zqslm5c4i8qzrkxwc") (features (quote (("default"))))))

(define-public crate-shallow-0.0.2 (crate (name "shallow") (vers "0.0.2") (hash "1vnjhqgr06c6dky7sdsba7fckflfybvsdzm3am7cxpdl7dp8sf7l") (features (quote (("default"))))))

(define-public crate-shallow-0.1 (crate (name "shallow") (vers "0.1.0") (hash "01rzdy9l1cfbi6axpvk9d04yl1hfhdw8ifliag3aal0w7qx23wn9") (features (quote (("default"))))))

(define-public crate-shallow-0.1 (crate (name "shallow") (vers "0.1.1") (hash "0iq8l1xqq1807axlkczbqv2fm4hccmw2cbiziwmbjy777xf2bbg2") (features (quote (("default"))))))

(define-public crate-shallow-0.2 (crate (name "shallow") (vers "0.2.0") (hash "00xcvm2sc6xjbijm0v75haw5ahfvgi6n3r2lfz84gjp4w9k9vk62") (features (quote (("default"))))))

(define-public crate-shallow-debug-0.1 (crate (name "shallow-debug") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.10") (default-features #t) (kind 0)))) (hash "1hqqhx2hvk6yz4ipa60dd8s9fc8gh83hxz18pzsc2mmb8k23cpmc") (rust-version "1.60.0")))

(define-public crate-shallow-tees-0.1 (crate (name "shallow-tees") (vers "0.1.0") (hash "084sw9c0v1ikwsqs564vvsrds45kf89armhwba6dkqfdm27imdmf")))

(define-public crate-shallow-tees-0.1 (crate (name "shallow-tees") (vers "0.1.1") (hash "01wfi2p1bpf1mgd00fkiv8aj6xfd06w8zsy56yyi7z92lyn4fizl")))

(define-public crate-shalrath-0.1 (crate (name "shalrath") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0h9yry99fj8cv9h7a6s54b2c0dnvf26hszmc4mpcfc8x20arqsq7") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1 (crate (name "shalrath") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "054krxk1vxpxiidk5z2dy1zpnhla9s93z0xg5x52xmy3d2b91161") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1 (crate (name "shalrath") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wsv3ag87ykqlqy4vzn2cahs2y8yx4619z4rgff7f54rpcvxkg92") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1 (crate (name "shalrath") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "05xaam8lwzyvlrz681awk6vaxinwkcm9i3iv97n0r9f0bg1mdsm2") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.1 (crate (name "shalrath") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0m1qivdva05nyp65qm7m11jqxbap34bv73c22iiprdmy9pw77z9h") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2 (crate (name "shalrath") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "12w2g98g2hsh47ymi8536r57vjabrjni5brhvm3b7k10wyap2bpr") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2 (crate (name "shalrath") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "00dyw4swlf0pbx4rgk9mnhkspdjrpqg5nplbr7l430klyircr1yq") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2 (crate (name "shalrath") (vers "0.2.3") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0h9sw01ag9jndhvwq3r5x35d0b3bv8rnwyj9yfc78ahsg8qf38bz") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2 (crate (name "shalrath") (vers "0.2.4") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xfafhvg7xx0qh0f29srq7zaxvfvmlidzwsy8jshsxh65jhww3qs") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2 (crate (name "shalrath") (vers "0.2.5") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "145r7b9gr2ps21h06biwqfn2fx7gb4pdwgkxra19rxzgcw3bgvgn") (features (quote (("non_foss_tests") ("default" "serde"))))))

(define-public crate-shalrath-0.2 (crate (name "shalrath") (vers "0.2.6") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1rwldl9yz8xgcbjr69p4lffksg4qzbq665npbpwz0lp3vff6syin") (features (quote (("non_foss_tests") ("default" "serde"))))))

