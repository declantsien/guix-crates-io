(define-module (crates-io sh eb) #:use-module (crates-io))

(define-public crate-shebang-0.0.0 (crate (name "shebang") (vers "0.0.0--") (hash "00yrhfbd2ic3frfp5sp0ma1750j03rkq63bk6mijczip383m5nnn") (yanked #t)))

(define-public crate-shebling-0.0.0 (crate (name "shebling") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "shebling-codegen") (req "=0.0.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1a87f57gm3cjls4m0cl51dkhfz114pab6h23392fjzk2cdq0m5ia")))

(define-public crate-shebling-ast-0.0.0 (crate (name "shebling-ast") (vers "0.0.0") (hash "0jf45pmxr4ly97d2piz3yfb4ll1632d9ag2c14i637k6k9nw4996")))

(define-public crate-shebling-codegen-0.0.0 (crate (name "shebling-codegen") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.54") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.11") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "11jlqph77k1sqswqb8rc4q81fwc0343sfgwl9rjq846c4bjm8rjx")))

(define-public crate-shebling-lexer-0.0.0 (crate (name "shebling-lexer") (vers "0.0.0") (deps (list (crate-dep (name "miette") (req "^5.9.0") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "shebling-ast") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "08p6zxlw9lyn2sjh8kp2a47l4jnmrymsmv1rmfzh0kac7xpgmag2")))

(define-public crate-shebling-linter-0.0.0 (crate (name "shebling-linter") (vers "0.0.0") (hash "0dqmblgb0fpl65a6n6q2bkgpj5gpdjlwxk7vf57wlpvgid1kpdcm")))

(define-public crate-shebling-parser-0.0.0 (crate (name "shebling-parser") (vers "0.0.0") (hash "1hcdx2zz9kc7cc75n8ipjdn0fapax87ldswbsplk7pqai6qjczi4")))

