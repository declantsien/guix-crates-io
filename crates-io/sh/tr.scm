(define-module (crates-io sh tr) #:use-module (crates-io))

(define-public crate-shtring-0.1 (crate (name "shtring") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "1x70jcpm08bdlawjk8g98v2fp19wvyq7f796a1r01ab57dsmqh9b")))

