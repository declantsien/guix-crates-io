(define-module (crates-io sh am) #:use-module (crates-io))

(define-public crate-shaman-0.1 (crate (name "shaman") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1xa4d1rh67q4pp260fl3ixzmanc49n8ycn8ifzsvh1vsx6vlh6iv")))

(define-public crate-shambler-0.2 (crate (name "shambler") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)) (crate-dep (name "shalrath") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "usage") (req "^1.4.0") (features (quote ("rayon"))) (default-features #t) (kind 0)))) (hash "1clg9jwnrcxvwyqqxs0lj2j1032x5aqvvbsx0nqnnmd6hw6ihyn5")))

(define-public crate-shame-0.0.0 (crate (name "shame") (vers "0.0.0") (deps (list (crate-dep (name "derive-where") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "impls") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "pub-fields") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ylvbxmwrcfp0218mfrxpfby7w7l7gvj0waqk1aqimwfl3jg8lax")))

(define-public crate-shame-0.0.1 (crate (name "shame") (vers "0.0.1") (deps (list (crate-dep (name "derive-where") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derive_everything") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "impls") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "pub-fields") (req "^0.1") (default-features #t) (kind 0)))) (hash "1466bkxff62av2p0d6wmcqc3ip2dpvki8x76kqprjmii1a56cnp3")))

(define-public crate-shamir-1 (crate (name "shamir") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "05dl25qqh4hvfp53pplzrwpw8azgdahh9h8q26ljk56wv8mjd18x")))

(define-public crate-shamir-1 (crate (name "shamir") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1clwr0kdcly1fmwy42i7hckdfvkhvrzhkzh8xniisq20vsr2x1la")))

(define-public crate-shamir-1 (crate (name "shamir") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mwc2p681rrbibsbrhg7ly9idv1cjp71sbcvs015p4rvdzzx7506")))

(define-public crate-shamir-2 (crate (name "shamir") (vers "2.0.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "05z3vx89xrv6qgsybqfx65jmyf9j3js6zm5mb2gzj3pcrh54wczk")))

(define-public crate-shamir_file_secret_sharing-1 (crate (name "shamir_file_secret_sharing") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0m0n5vpa5z482196x9gm57ayi87jzq6wpnzsw1hf2bxsl7xdv175") (rust-version "1.60")))

(define-public crate-shamir_secret_sharing-0.1 (crate (name "shamir_secret_sharing") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "15l85hryw8l1js9xnpbfwqa9cqs05fv5iy9lkf9r344c9crzjgh4")))

(define-public crate-shamir_secret_sharing-0.1 (crate (name "shamir_secret_sharing") (vers "0.1.1") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "14k5zdj7xrcm4dz6dffrf813rkwv2690af642c9sgas9xcdnnvh8")))

(define-public crate-shamirsecretsharing-0.1 (crate (name "shamirsecretsharing") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15mjs3iyx34nsh615l1am3hd6b5y8q2l1xi8gys3s0zc9qch4cyx")))

(define-public crate-shamirsecretsharing-0.1 (crate (name "shamirsecretsharing") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02zwk2g7z9f1q2n6ckdfg61qw9pg7cg650jl3k3nwz95qqv166kb")))

(define-public crate-shamirsecretsharing-0.1 (crate (name "shamirsecretsharing") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "chacha20-poly1305-aead") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "01fdli821f52vcizpgcn7cn0flmg5w25gpb5qpylp1w0dljizw1l")))

(define-public crate-shamirsecretsharing-0.1 (crate (name "shamirsecretsharing") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "chacha20-poly1305-aead") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "01qyfbc9m46d79dzmd1n60a22lj813kk5swdf02k7kcfg7fx5b8a")))

(define-public crate-shamirsecretsharing-0.1 (crate (name "shamirsecretsharing") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "chacha20-poly1305-aead") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "13w8rd8hi16qcbcps8iqrr0r00xsindm2wrj62d3jvk753wd608r") (features (quote (("have_libsodium"))))))

(define-public crate-shamirsecretsharing-0.1 (crate (name "shamirsecretsharing") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "chacha20-poly1305-aead") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xsalsa20poly1305") (req "^0.6") (default-features #t) (kind 0)))) (hash "0wkr6pglqpnis9jv2xg8anbxgid9aqx9yzn7h706si0lry2za3bc") (features (quote (("have_libsodium"))))))

