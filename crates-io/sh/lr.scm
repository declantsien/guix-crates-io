(define-module (crates-io sh lr) #:use-module (crates-io))

(define-public crate-shlrt-macros-0.0.1 (crate (name "shlrt-macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "1sdgm5sn70f1v5hfamwlb8zlshgsvzk0vxnmf39i999174hn8q8y")))

