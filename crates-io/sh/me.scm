(define-module (crates-io sh me) #:use-module (crates-io))

(define-public crate-shmem-0.1 (crate (name "shmem") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 0)))) (hash "0r686cfr2bavxjilj2lr3sll6chk5gci0ia9dkm99gxqbxkz3z2z")))

(define-public crate-shmem-0.2 (crate (name "shmem") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0d0piscxa37hicx78grxr0zqjvbd2kw455gaa3mqfia96k67y64j")))

(define-public crate-shmem-bind-0.1 (crate (name "shmem-bind") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1nbh4xldsrbbvgw6vqyayfk5aq8khlsqw77nk4al461gywx72l79")))

(define-public crate-shmem-bind-0.1 (crate (name "shmem-bind") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "198wklm0b1r9zm1vvw66vp95rc8ni3wsdcr6vajvqaxp47fk0cxh")))

(define-public crate-shmem-bind-0.1 (crate (name "shmem-bind") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1hnrrg7a8s99m69bh1jil12bzvg0fsqvihb6v4z79lc2s54j5nk3")))

(define-public crate-shmem-bind-0.1 (crate (name "shmem-bind") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "01l5ndvwzwv7slp2171q0nawjwbd536niaj6zn3xgkfd75xk21pw")))

(define-public crate-shmem-ipc-0.1 (crate (name "shmem-ipc") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "dbus-crossroads") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.85") (default-features #t) (kind 0)) (crate-dep (name "memfd") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v62nfsnl3n3vgvl93mg1i3j92i3r36k0ikms5l4337r27vgfmf7")))

(define-public crate-shmem-ipc-0.2 (crate (name "shmem-ipc") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "dbus-crossroads") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.85") (default-features #t) (kind 0)) (crate-dep (name "memfd") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jczjj6kd88krfhbnzxy4g1cqs9qinslza56rjr0s6jq3h4bqh6k")))

(define-public crate-shmem-ipc-0.3 (crate (name "shmem-ipc") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "dbus") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "dbus-crossroads") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.85") (default-features #t) (kind 0)) (crate-dep (name "memfd") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.3") (default-features #t) (kind 0)))) (hash "0spji1chkliwi0lph342kp7673mzk5bcsnymzsb7mdks3pf2ab7d")))

(define-public crate-shmemfdrs-0.1 (crate (name "shmemfdrs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.34") (default-features #t) (kind 0)) (crate-dep (name "syscall") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "1wpd5jqrkjxhy0l4v2bdln77vpc58ix9my2ykkv6a4zrnch0g5jh") (features (quote (("memfd" "syscall"))))))

(define-public crate-shmemfdrs-0.1 (crate (name "shmemfdrs") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1znn1pd9bdz0kwhjd5bk68lccggwri5pksffami106j7ldzfyphp") (features (quote (("memfd" "sc"))))))

(define-public crate-shmemfdrs-0.1 (crate (name "shmemfdrs") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.33") (default-features #t) (kind 0)))) (hash "1f5kdj45s7gwkdan7a47v2nmwdfc2rnpczxdw0ngzj0y5xwcv81d") (features (quote (("memfd"))))))

(define-public crate-shmemfdrs-0.1 (crate (name "shmemfdrs") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0f1c75gx3jg57wy6c3sgr0za278373r07677hyzx0nrqn4ycwfcz") (features (quote (("memfd") ("default" "memfd"))))))

(define-public crate-shmemfdrs-0.1 (crate (name "shmemfdrs") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "1nybicq9navypgrq95qn46icd699p8q3sy0r5laa33rvayjslz50") (features (quote (("memfd") ("default" "memfd"))))))

(define-public crate-shmemfdrs2-1 (crate (name "shmemfdrs2") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k7mf3k6j9ipc0lkycxjzw99jhi5839ycaf6k57y84gqazwmr83h")))

