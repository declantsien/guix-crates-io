(define-module (crates-io sh r3) #:use-module (crates-io))

(define-public crate-shr3-0.1 (crate (name "shr3") (vers "0.1.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)))) (hash "1fpa8ch1ga0w5p8jan6b4qzmj2r52w4pizvn90kv1rxcjp3iy44z")))

(define-public crate-shr3-1 (crate (name "shr3") (vers "1.0.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)))) (hash "1408miy70hg4yp893lgcp0a64cwsd1kadi53vilzggfnp9s0rz0s") (features (quote (("default") ("__devmode__"))))))

