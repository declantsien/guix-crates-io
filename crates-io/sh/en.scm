(define-module (crates-io sh en) #:use-module (crates-io))

(define-public crate-shen-0.1 (crate (name "shen") (vers "0.1.0") (hash "1d9m67qnbgajjv0sfsza2hg5pdbd9brcc5n5y0b6s1932xpla8lj")))

(define-public crate-shen-nbt5-0.4 (crate (name "shen-nbt5") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0031f1kf89zsrf6k5m4gnb6afl6bykrlbfx8m12qrd5xdqfyd2j3") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4 (crate (name "shen-nbt5") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "10nv93a4ih5qypb7m9rllv88qimi5nb0v49r200w9qq32yzyngf0") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4 (crate (name "shen-nbt5") (vers "0.4.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wg6p58qp5vnrc3wv2abfdycbi8iyv6sxwgw0wv1zgi58w6s6lfk") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4 (crate (name "shen-nbt5") (vers "0.4.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xndblqh5c0nkrhb54g3n6vwc9lhnyiq4bppy9rb2r8zsd8pw2gm") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-shen-nbt5-0.4 (crate (name "shen-nbt5") (vers "0.4.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "18dh071nf4qyzwgz3zfcqbck0k2sjgbki9y6klbx39qkvf60fyh2") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-shennong-0.1 (crate (name "shennong") (vers "0.1.0") (hash "0q6zs6lfbm3v09sfn67wsjzpxvvagj9n19bdf208hxjdc05191xn")))

(define-public crate-shentong-0.5 (crate (name "shentong") (vers "0.5.5") (hash "048hhss47y47cbzqiwiwmcbv25c3ydaaal0mmrvfqxxyvi2y968s") (features (quote (("aq_unstable")))) (yanked #t)))

(define-public crate-shentong-0.5 (crate (name "shentong") (vers "0.5.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "oracle_procmacro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 2)))) (hash "03nyvxxzmzz1hx139dmw96g1yjd29bld7as6jkwi15yqiynw87v4") (features (quote (("aq_unstable")))) (yanked #t)))

(define-public crate-shentong-0.5 (crate (name "shentong") (vers "0.5.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "oracle_procmacro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 2)))) (hash "185s1c3q46xgpk4x1jgnrf9rba566mivxrzak52g0074zw6g6dnh") (features (quote (("aq_unstable"))))))

(define-public crate-shentong-0.5 (crate (name "shentong") (vers "0.5.8") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "oracle_procmacro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 2)))) (hash "1vp7gw8jix17c6a5bnhnl98wqkxbjq9bdwcmp7hk3i5ga4ad079f") (features (quote (("aq_unstable"))))))

