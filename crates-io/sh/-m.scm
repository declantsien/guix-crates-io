(define-module (crates-io sh -m) #:use-module (crates-io))

(define-public crate-sh-macro-0.1 (crate (name "sh-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nr8mdijcadqs4sjdcprs2f3gb9xs03g8qibqymswpxmhk5apja7")))

(define-public crate-sh-macro-0.1 (crate (name "sh-macro") (vers "0.1.1") (deps (list (crate-dep (name "litrs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "172nf24plfpl88fzvmf9fm1wy7kmgqv4s4k5gcxvlak25mqyh4jj")))

(define-public crate-sh-macro-0.2 (crate (name "sh-macro") (vers "0.2.0") (deps (list (crate-dep (name "litrs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k3njf8qjhdsr2s5fn6x13r4ha9cb327vq7439jv3311arild59n")))

(define-public crate-sh-macro-0.2 (crate (name "sh-macro") (vers "0.2.1") (deps (list (crate-dep (name "litrs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1143vz5r0gkd4fhg0zdvdrr0c3mzd49jbiqfrakr04rbibiignhn")))

