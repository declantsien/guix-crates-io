(define-module (crates-io sh ed) #:use-module (crates-io))

(define-public crate-shed-0.0.0 (crate (name "shed") (vers "0.0.0") (hash "0xqjincs07zqdvrm7crnp4c94rhfqixmlh6pphp3ldkcfd5y8pqz") (yanked #t)))

(define-public crate-shed-0.1 (crate (name "shed") (vers "0.1.0") (hash "16wxa4yvm0axhxy51ar22ks9iinwicvh7k25xv1fkwf64br7gz19") (yanked #t)))

(define-public crate-shed-0.2 (crate (name "shed") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sx933pgv4c8clbw2drr27xbcfd2bbr6gqa4z6fs6lgppcvpw5nf")))

(define-public crate-shed-0.2 (crate (name "shed") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "080xayb7g5hp529zn4wshhzfs7mryqadf136qwnlfvl25gca73ph")))

(define-public crate-shed-1 (crate (name "shed") (vers "1.0.0") (deps (list (crate-dep (name "rmp-serde") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jcp4gscrams6r8a7mhys38qhpc45gd3c3rwjqakis5274x5j4zm")))

(define-public crate-shedron-0.1 (crate (name "shedron") (vers "0.1.1") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "13bwa1w4s974ss5gv5hxz7m9vp2xcn5gjxbvigr80xnr5gvc7bvf") (features (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

(define-public crate-shedron-0.1 (crate (name "shedron") (vers "0.1.2") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0h0al7fswbnzz3d1zzx5yb1irj6qcs0vhn4pvalnczqb28h6wxzy") (features (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

(define-public crate-shedron-0.1 (crate (name "shedron") (vers "0.1.3") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dmylvsycr6yg0m2adj8ckhvazcj9gvlcrgxlm4r9k24mr7v7dc1") (features (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

(define-public crate-shedron-0.1 (crate (name "shedron") (vers "0.1.4") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1mb9nglglbvs1gcmg4cbpqsdmppclsya8l2han6bjrdhscsch9qx") (features (quote (("map-operators-do-single") ("default" "map-operators-do-single"))))))

