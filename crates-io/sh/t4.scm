(define-module (crates-io sh t4) #:use-module (crates-io))

(define-public crate-sht4x-0.1 (crate (name "sht4x") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.20.0") (default-features #t) (kind 0)) (crate-dep (name "sensirion-i2c") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ckgcqm21dbbnnmk8pipfyqz7fp76r5s7k5msy7lbyay1cq27dbh") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

