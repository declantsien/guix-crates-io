(define-module (crates-io qc ms) #:use-module (crates-io))

(define-public crate-qcms-0.1 (crate (name "qcms") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jhs467m2hvf68zf5xbdki36jv5ka2xk1m41ccnvh08xjyf7b7rr")))

(define-public crate-qcms-0.2 (crate (name "qcms") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0ry3x5vix5qc1axg93q7sz346sp0ljy0spj5xn9q4sylx9wqrx34") (features (quote (("iccv4-enabled") ("default") ("c_bindings" "libc"))))))

(define-public crate-qcms-0.3 (crate (name "qcms") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1yihv9rsa0qc4mmhzp8f0xdfrnkw7q8l7kr4ivcyb9amszazrv7d") (features (quote (("neon") ("iccv4-enabled") ("default" "iccv4-enabled" "cmyk") ("cmyk") ("c_bindings" "libc"))))))

