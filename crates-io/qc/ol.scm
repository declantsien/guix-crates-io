(define-module (crates-io qc ol) #:use-module (crates-io))

(define-public crate-qcollect-0.1 (crate (name "qcollect") (vers "0.1.0") (deps (list (crate-dep (name "qindex_multi") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raw-vec") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1sfw3jz69yscxmv36xq9jkjs4jwiw2jy39ns72hf71pjnpxrv59q")))

(define-public crate-qcollect-0.2 (crate (name "qcollect") (vers "0.2.0") (deps (list (crate-dep (name "qcollect-traits") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raw-vec") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0s5bsvpc7nc030gy3c852fzx8svzbxcmm6gbzfpbm9fv9lysrp22")))

(define-public crate-qcollect-0.3 (crate (name "qcollect") (vers "0.3.0") (deps (list (crate-dep (name "qcollect-traits") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "raw-vec") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0faj78xi0rlm32xmij2dc52mlzkig0nr8w2icrrzndvjri8mqy0z")))

(define-public crate-qcollect-0.4 (crate (name "qcollect") (vers "0.4.0") (deps (list (crate-dep (name "qcollect-traits") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "raw-vec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0rhgqcf6pgm83h4244gpnb2svprygqydz8hyjak3gw68vyd10hqy")))

(define-public crate-qcollect-0.4 (crate (name "qcollect") (vers "0.4.1") (deps (list (crate-dep (name "qcollect-traits") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "10gbi3qqh30b7bcz15h4p52fmsyijxarj4dpkddk3r6sds1zg1wh")))

(define-public crate-qcollect-traits-0.1 (crate (name "qcollect-traits") (vers "0.1.0") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0fkw477gd1g34yxyndy1a8n01jjn4jyldzhb0cfhx8cax5rrhyag")))

(define-public crate-qcollect-traits-0.2 (crate (name "qcollect-traits") (vers "0.2.0") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0ri6y5hblvnj6lmfjdpww6rgxgh838dgbjravvca0a3p4kz6qpd8")))

(define-public crate-qcollect-traits-0.3 (crate (name "qcollect-traits") (vers "0.3.0") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "01sj94419v6zvwzz088zlbq0i2wcj5zm3iv7vcahl2gjd9ww1q77")))

(define-public crate-qcollect-traits-0.3 (crate (name "qcollect-traits") (vers "0.3.1") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "03ygz4bl6c72l3xmvlk6mcl2w82zhvr0phclfsrqrs61q5c9wiww")))

(define-public crate-qcollect-traits-0.4 (crate (name "qcollect-traits") (vers "0.4.0") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0waw5b5njxn7w5saxsl26mfgg21vlckr0s3mmmknb8sy6gpcy80x")))

(define-public crate-qcollect-traits-0.4 (crate (name "qcollect-traits") (vers "0.4.1") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "05v6hrcfp4v6dy8c2pibccgclab2cl4clh5jc9qfi0gjlvj2s8qk")))

(define-public crate-qcollect-traits-0.5 (crate (name "qcollect-traits") (vers "0.5.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0fzryxxpyfshkcaasffvy7nbiwy905qpmknzzh92ajqlnk7l250m")))

(define-public crate-qcollect-traits-0.5 (crate (name "qcollect-traits") (vers "0.5.1") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0b1w99nc7250b7qi4m36axmpcv2sqz09c7n77h2ibrynwmwpaggm")))

(define-public crate-qcollect-traits-0.5 (crate (name "qcollect-traits") (vers "0.5.2") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yclbz17svm5adzzmbrc9425rm83grljdj15jw8s576khvlbvikd")))

(define-public crate-qcollect-traits-0.5 (crate (name "qcollect-traits") (vers "0.5.3") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1r3bz06803yb4k4d4zlzmpdpclchfkfkdv081i313dcllhsdk0li")))

(define-public crate-qcollect-traits-0.6 (crate (name "qcollect-traits") (vers "0.6.0") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1413vdk3n131c0kalirq9hadkxs8k8jnki3g9smws29dqwy3acjh")))

(define-public crate-qcollect-traits-0.6 (crate (name "qcollect-traits") (vers "0.6.1") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1fnmbskgzj7c1prfnva4khdxaa7zdhxn8agw9ixliqb5mqqf731b")))

(define-public crate-qcollect-traits-0.7 (crate (name "qcollect-traits") (vers "0.7.0") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "199k11f80kz8fk05pg98y7pjg04m7k91z7lfjrxv39yylfcsr9rk")))

(define-public crate-qcollect-traits-0.7 (crate (name "qcollect-traits") (vers "0.7.3") (deps (list (crate-dep (name "bit-vec") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1nr7yk6wncb4pspphxzisnidz8s5psim13hbia7xm4rl9fy4grkm")))

(define-public crate-qcollect-traits-0.7 (crate (name "qcollect-traits") (vers "0.7.4") (deps (list (crate-dep (name "bit-vec") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0djgq38sca0zi1vzjqkr3xnm0af26g7hygk1931z7fbaz1hf05mr")))

