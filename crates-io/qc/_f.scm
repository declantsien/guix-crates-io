(define-module (crates-io qc _f) #:use-module (crates-io))

(define-public crate-qc_file_parsers-0.2 (crate (name "qc_file_parsers") (vers "0.2.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)))) (hash "09yy4vc3wdyp1gg70p7085r15r15jjgmgz9cp3rphssxsjw5rq3s")))

(define-public crate-qc_file_parsers-0.2 (crate (name "qc_file_parsers") (vers "0.2.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)))) (hash "16jm2kv3bhrayzhblb0h6zn3c6sjby3z3q6bb48r118bvmvacrgl")))

(define-public crate-qc_file_parsers-1 (crate (name "qc_file_parsers") (vers "1.0.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)))) (hash "1zdak4gsmj9xnyhsigjq6bgdqj7bi2fj8fsw42p8d6cjy1rjdykp")))

(define-public crate-qc_file_parsers-2 (crate (name "qc_file_parsers") (vers "2.0.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)))) (hash "1fjqky1hqks8r6lzn52jq1zd0plxgsz14sddd85yhyvhddjvjw5x")))

(define-public crate-qc_file_parsers-2 (crate (name "qc_file_parsers") (vers "2.0.1") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "093yfgc25ll4z6cski7vcvmjqpkcfvinbvr6526mx40q75h3fsfy")))

(define-public crate-qc_file_parsers-3 (crate (name "qc_file_parsers") (vers "3.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0m5rdas7w1fh06dnfiq2h399z4v0qyaskg99xd3ijpvm3d5rrck3")))

(define-public crate-qc_file_parsers-3 (crate (name "qc_file_parsers") (vers "3.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "09whd5i11q2924z6sq2v09awna55lsy026wrpqyr8y7zfbl2khla")))

