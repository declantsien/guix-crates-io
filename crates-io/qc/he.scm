(define-module (crates-io qc he) #:use-module (crates-io))

(define-public crate-qcheck-1 (crate (name "qcheck") (vers "1.0.0") (deps (list (crate-dep (name "qcheck-macros") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("getrandom" "small_rng"))) (kind 0)))) (hash "1w7knnk2zhhczmymh6hdddsld4vlvmm6lpn930nxclfs891bsfdl")))

(define-public crate-qcheck-macros-1 (crate (name "qcheck-macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ddiynmcs0i49gxx6vky3jnsg7nvyj1ikw8q9f81a1k9bapvjhi7")))

(define-public crate-qchess-0.0.1 (crate (name "qchess") (vers "0.0.1") (hash "1ks4k82ld2c52pvp1l0cm6whahvp65zpxmxh2z6qgispfs2164f8")))

