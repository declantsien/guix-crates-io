(define-module (crates-io qc gp) #:use-module (crates-io))

(define-public crate-qcgpu-0.1 (crate (name "qcgpu") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "libquantum-patched") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.1.43") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "12lzw3p94nwry1z10axcj06m0ncxa4lkap2y49khy0bcc54zhgvl") (features (quote (("default") ("decoherence"))))))

