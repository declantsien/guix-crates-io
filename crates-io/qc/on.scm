(define-module (crates-io qc on) #:use-module (crates-io))

(define-public crate-qcontext-0.1 (crate (name "qcontext") (vers "0.1.0") (deps (list (crate-dep (name "qcell") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "qcontext-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16hnivnv34v28z979cr0nrl5igzjj5gwym9hx8kvh1qfbhq41390")))

(define-public crate-qcontext-0.2 (crate (name "qcontext") (vers "0.2.0") (deps (list (crate-dep (name "qcell") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "qcontext-derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lfq8p0cpzfv806bpbaxnkfy8fr6w517vrwvilwn32rrq472nb2i")))

(define-public crate-qcontext-0.3 (crate (name "qcontext") (vers "0.3.0") (deps (list (crate-dep (name "qcell") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "qcontext-derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06jlnli7zz44lfz8jrh8ardirjvq58gj4qfa8g3v7vavg48ypnfk")))

(define-public crate-qcontext-0.4 (crate (name "qcontext") (vers "0.4.0") (deps (list (crate-dep (name "qcell") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "qcontext-derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1k4hpg2bss0ajwrq3vgnxkfwq7nwr1c563v1b6b0kg2vy8jprpxj")))

(define-public crate-qcontext-derive-0.1 (crate (name "qcontext-derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1xd74d3z457j5dfhg2xsvi4zd432qq46j5j3r7lslsq9c06hm2ax")))

(define-public crate-qcontext-derive-0.2 (crate (name "qcontext-derive") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1fwkff43cw5q92ssywvmnnj0h96dzwvprm70biqzadifs8jbsd59")))

(define-public crate-qcontext-derive-0.3 (crate (name "qcontext-derive") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "199z77cgdr3sajnmaxjgf7bsfhvzsbkhr43hy3nnadahvhjc41q4")))

(define-public crate-qcontext-derive-0.4 (crate (name "qcontext-derive") (vers "0.4.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0x3rfvvj5m0iyszf18g04g7lc3fgi9j0dixaw9v8xwb396c5h4d4")))

