(define-module (crates-io la zu) #:use-module (crates-io))

(define-public crate-lazuli-0.1 (crate (name "lazuli") (vers "0.1.1-rc.2") (deps (list (crate-dep (name "lazuli_core") (req "^0.1.1-rc.2") (default-features #t) (kind 0)) (crate-dep (name "lazuli_derive") (req "^0.1.1-rc.2") (default-features #t) (kind 0)))) (hash "07q8hh1466d6xfz46zk3m44zpff0b62k4h7vbg2g5mzy5ybxk4r5")))

(define-public crate-lazuli_core-0.1 (crate (name "lazuli_core") (vers "0.1.1-rc.2") (deps (list (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.2") (default-features #t) (kind 2)))) (hash "00g5953x120wbx7a7cn60mxqgapijbwb45k8hz6wl10cjhk2q5yr")))

(define-public crate-lazuli_core-0.1 (crate (name "lazuli_core") (vers "0.1.1-rc.3") (deps (list (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.2") (default-features #t) (kind 2)))) (hash "0gaqy4i2a4sp8xidfaw1ln7hahi55agscz3lm7w7g1rbyzlqs3ny")))

(define-public crate-lazuli_derive-0.1 (crate (name "lazuli_derive") (vers "0.1.1-rc.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "0g5v42mk97jnw5pxc37d6qgi04rzpjqhdzq748imxmfwg1snnkbi")))

