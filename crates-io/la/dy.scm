(define-module (crates-io la dy) #:use-module (crates-io))

(define-public crate-lady-deirdre-1 (crate (name "lady-deirdre") (vers "1.0.0") (deps (list (crate-dep (name "lady-deirdre-derive") (req "^1.0") (kind 0)))) (hash "0ryg2xcxzclkal1jgbdhv5bsg5s7z42jma8qqvnczaxks1c4ga33") (features (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-1 (crate (name "lady-deirdre") (vers "1.0.1") (deps (list (crate-dep (name "lady-deirdre-derive") (req "^1.0") (kind 0)))) (hash "1hypvc13ca5bhjxprxydv3szjs0piymg5s526x93vvl0n9wr5r4c") (features (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-1 (crate (name "lady-deirdre") (vers "1.0.2") (deps (list (crate-dep (name "lady-deirdre-derive") (req "^1.0") (kind 0)))) (hash "1axkdnw5v80hdpg89wxqycgamxdp35a428r3cy5xk6ppy89jns1n") (features (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-1 (crate (name "lady-deirdre") (vers "1.0.3") (deps (list (crate-dep (name "lady-deirdre-derive") (req "^1.0") (kind 0)))) (hash "1b9ffy2vxrf30zc3yjm09gb8wp0agyk9vb9bkhj4y5d8p7zzms3r") (features (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-1 (crate (name "lady-deirdre") (vers "1.0.4") (deps (list (crate-dep (name "lady-deirdre-derive") (req "^1.0") (kind 0)))) (hash "00ccxlxc0a4mk4yf5hp7mdl3cxw3pxaxzv06i8qssvs874wslq2b") (features (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-1 (crate (name "lady-deirdre") (vers "1.0.5") (deps (list (crate-dep (name "lady-deirdre-derive") (req "^1.0") (kind 0)))) (hash "0502rkiwg1dihxw5gj6550zysbq90j47azhjdimf6ig0pyldwk4k") (features (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-derive-1 (crate (name "lady-deirdre-derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("default" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1w28fqq4607lfp2gkm0w8lzv5jnx9p9g07hwg5gd2b8xkzzfmdpz") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-derive-1 (crate (name "lady-deirdre-derive") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("default" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1fh9shrjy28j02jkv17f5sm3i00jqihvagmr2yq59zzqvyqqsz41") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-derive-1 (crate (name "lady-deirdre-derive") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("default" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1vr7qawyzzb0pgmk02z3p2i3ljy08f2429r6lh7xzw6mvvjhiywr") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-derive-1 (crate (name "lady-deirdre-derive") (vers "1.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("default" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "18fkwlciwjbs9ybvlyvgyrcwhwxrsjgjj9c64j5vrllpsyb8v9f4") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-derive-1 (crate (name "lady-deirdre-derive") (vers "1.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("default" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fc5z1qfzc04pvpr1m46vw7lksrp9z6acgrsxblxr2k7i0j4nvsf") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-lady-deirdre-derive-1 (crate (name "lady-deirdre-derive") (vers "1.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("default" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1fpsnbvq1xwjf4dhy1ykajjxc6cz0inhp3cn5xr3766dls2mg2f1") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-ladybug-0.0.0 (crate (name "ladybug") (vers "0.0.0") (hash "0f9mz4584mqkxq4k9x6fpn6wsmrv9bgr9czpb1fx923bri9x5f0s") (features (quote (("enable") ("disable")))) (rust-version "1.68")))

