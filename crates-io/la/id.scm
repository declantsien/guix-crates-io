(define-module (crates-io la id) #:use-module (crates-io))

(define-public crate-laid-0.0.0 (crate (name "laid") (vers "0.0.0") (deps (list (crate-dep (name "glam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "miniquad") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "miniquad_text_rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0szl6spjcf2imc7r0mcjklfv6h18ql0grv6llkfl27qf3cks31lr")))

