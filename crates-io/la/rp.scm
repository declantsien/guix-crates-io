(define-module (crates-io la rp) #:use-module (crates-io))

(define-public crate-larp-0.0.1 (crate (name "larp") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0vpm3n7m4vlhsb8fczs2g4jbl07z8s3lmd8sj4813kparyab3yqw")))

(define-public crate-larpa-0.0.0 (crate (name "larpa") (vers "0.0.0") (hash "0j8jcdiiwq9b1i3k7l8kf6jddpqayx0b5xdlg1m63x5lpn1dzanh")))

