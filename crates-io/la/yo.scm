(define-module (crates-io la yo) #:use-module (crates-io))

(define-public crate-layout-cli-0.1 (crate (name "layout-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.18") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "layout-rs") (req "^0.1.0") (features (quote ("log"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1dlz898z84c4cbs2lj42bqaxb7yx6jr788lvrm5z3l9l9c4kdibs")))

(define-public crate-layout-cli-0.1 (crate (name "layout-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.18") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "layout-rs") (req "^0.1.1") (features (quote ("log"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "13yhcsymd8g2lrkg1q67kbkw7dq0b8w9drzqnrnj6dpg778zx7wq")))

(define-public crate-layout-cli-0.1 (crate (name "layout-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.18") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "layout-rs") (req "^0.1.2") (features (quote ("log"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "07cclxgixjpq8qnlr16vfg1gzajp8ax0345sbcpw210pvlma76qz")))

(define-public crate-layout-lib-0.1 (crate (name "layout-lib") (vers "0.1.0") (deps (list (crate-dep (name "layout-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fnzcsyh6m3grxl2cicb464fikp16chd77rf1x9mh9n94h3cvgay")))

(define-public crate-layout-lib-0.1 (crate (name "layout-lib") (vers "0.1.1") (deps (list (crate-dep (name "layout-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1m1q8wq61cg2shwwpipyy9i1qc9ypnj7xcnvpdzbdwg1lcnrk9g6")))

(define-public crate-layout-macro-0.1 (crate (name "layout-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rxpwclxz1vwbgcnxmly7s5203fi9q8m0jjqmhyjds99sndx12kz")))

(define-public crate-layout-rs-0.1 (crate (name "layout-rs") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "0ri5gk6di0alk6j6ipdqnk82lq0ibnp0y4y7mflfcfikdmmdknch")))

(define-public crate-layout-rs-0.1 (crate (name "layout-rs") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "1q44jn18vlsxh2x9ny71zxd8k4pwy1wwlvi1hzcc41wnrf3yyr0i")))

(define-public crate-layout-rs-0.1 (crate (name "layout-rs") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "1a6y7wjc0p7cbawdwa9gkvb1c518697nchbk5aj9r0vc7a5b5pl4") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-layout21-3 (crate (name "layout21") (vers "3.0.0-pre.2") (deps (list (crate-dep (name "gds21") (req "^3.0.0-pre.2") (features (quote ("selftest"))) (default-features #t) (kind 0)) (crate-dep (name "layout21converters") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "layout21protos") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "layout21raw") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "layout21utils") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "lef21") (req "^3.0.0-pre.2") (default-features #t) (kind 0)))) (hash "1lrryyirwhp9sgjks8iqq5mib946liiji3sn6gwz80yydv9gxmrl")))

(define-public crate-layout21converters-3 (crate (name "layout21converters") (vers "3.0.0-pre.2") (deps (list (crate-dep (name "chrono") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "gds21") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "layout21protos") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "layout21raw") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "layout21utils") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "lef21") (req "^3.0.0-pre.2") (default-features #t) (kind 0)))) (hash "1a7nfjk70jjcy0ikv30x8h0l0bhpb5jnsxd5gy6dnjpwlws59xqb")))

(define-public crate-layout21protos-3 (crate (name "layout21protos") (vers "3.0.0-pre.2") (deps (list (crate-dep (name "vlsir") (req "^3.0.0-pre.2") (default-features #t) (kind 0)))) (hash "0pjjbzxzd3shnvm9192rvw96i2dzix93ipysq53ny005z98vb0av")))

(define-public crate-layout21raw-3 (crate (name "layout21raw") (vers "3.0.0-pre.2") (deps (list (crate-dep (name "enum_dispatch") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "gds21") (req "^3.0.0-pre.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layout21protos") (req "^3.0.0-pre.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layout21utils") (req "^3.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "lef21") (req "^3.0.0-pre.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0dvzham7sq72kdk3xvmlh1lsbqllm50s2a22awabg4vx18v0mna6") (features (quote (("proto" "layout21protos") ("lef" "lef21") ("gds" "gds21") ("default" "gds" "lef" "proto"))))))

(define-public crate-layout21utils-0.2 (crate (name "layout21utils") (vers "0.2.0") (deps (list (crate-dep (name "by_address") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "0gnkp9cj3rby1m7fqa0h7kcd82bp5i56qq62l0slab0vs6jyfdqp")))

(define-public crate-layout21utils-3 (crate (name "layout21utils") (vers "3.0.0-pre.2") (deps (list (crate-dep (name "by_address") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8.10") (features (quote ("rust_decimal"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.10") (default-features #t) (kind 0)))) (hash "18bpqhyzn7bx0an4cfr9al2bg70ivwn14filz8n12jwbaaaa5ph7")))

(define-public crate-layout2d-0.1 (crate (name "layout2d") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r7zdkwm2nnjs62mm9z8maxznhqzjgb334ack9y980p2swixsnx9")))

(define-public crate-layout2d-0.1 (crate (name "layout2d") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0bp6sivpqzhd7qy57zndxyd3dxpl05n9kwia0px0q5x9hashy698")))

(define-public crate-layout2d-0.1 (crate (name "layout2d") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "0.3.*") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1sibh4sgn6df542brmdddhf96yfdczbncqsy7ykyajkfwri699km") (features (quote (("use_simd"))))))

(define-public crate-layout_engine-0.1 (crate (name "layout_engine") (vers "0.1.0") (hash "0gsn7vaa4388v2y493bk21r08lk6mslcbw4dcxrzk5a559hhjkg7")))

(define-public crate-layout_engine-0.1 (crate (name "layout_engine") (vers "0.1.1") (hash "00b50im59zi01yqsy8hd0mcibqx7mp78rn1pdnkk39445p3m43kg")))

(define-public crate-layout_engine-0.2 (crate (name "layout_engine") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1871amf1amz54lnap268zn5cchkh0aasaar36zgmp5mi6j2l06h5")))

(define-public crate-layout_engine-0.3 (crate (name "layout_engine") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1pk7fh96xhhwcr85hp9rnp5x7xi0ap2h107wvg2z8dyqhy45j93k")))

(define-public crate-layout_engine-0.4 (crate (name "layout_engine") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1s4wawiy62ya6yiyi1cwrf32vyrmijgvl19kkd07aygli419h160")))

(define-public crate-layout_engine-0.5 (crate (name "layout_engine") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0q7qppp9ykbndp3g7xazf0a2b3956yknwcv0n0b45n1gcz59n640")))

(define-public crate-layout_engine-0.5 (crate (name "layout_engine") (vers "0.5.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bqpvg2pvrs01vp6cyk05qjg7fhk5ik7878s415q4b6zq5n1yghh")))

(define-public crate-layout_engine-0.5 (crate (name "layout_engine") (vers "0.5.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0l1npqd2mahcc6hbw679s7y45dnxm8535g2cilcrqhpxwwbwx1lp")))

(define-public crate-layout_engine-0.5 (crate (name "layout_engine") (vers "0.5.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dg2kk2xx76ffcfly13q03557xwv27p3iwxmbahgskx9qw1n20yq")))

(define-public crate-layout_engine-0.6 (crate (name "layout_engine") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1iraa4ajajs24zjld8ywqzr2c8ky89bsw92lnlwgbjnbwxwl34cw")))

(define-public crate-layout_id-0.0.1 (crate (name "layout_id") (vers "0.0.1") (hash "01ffi62h8r7v5yfwsc4sr3mrw8s27plbd3zy06g3gdvkays5gdna")))

(define-public crate-layout_id-0.1 (crate (name "layout_id") (vers "0.1.0") (deps (list (crate-dep (name "twox-hash") (req "*") (default-features #t) (kind 0)))) (hash "1ra3gbq0h047vy0nq0yh9i70b79dr160lrc6nwmmlqd5b8viqq42")))

(define-public crate-layout_id-0.2 (crate (name "layout_id") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "*") (default-features #t) (kind 0)))) (hash "1xpr5gyqha6qapnfk3w633p8d1kxal1ygyj8rmpq4y0lcwzsyd74")))

(define-public crate-layout_id-0.2 (crate (name "layout_id") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "*") (default-features #t) (kind 0)))) (hash "1ziah5xpgdwcgfsk1laaa10w5m3j4026cdp20x89bal3m04zbvjh") (features (quote (("use_clippy" "clippy"))))))

(define-public crate-layout_id-0.2 (crate (name "layout_id") (vers "0.2.2") (deps (list (crate-dep (name "clippy") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "*") (default-features #t) (kind 0)))) (hash "0kq10lfbh8gjkzg1l02xq7cq2yw3yd52ykw1kwfbn3z0pvzy7ndp") (features (quote (("use_clippy" "clippy"))))))

(define-public crate-layout_id-0.2 (crate (name "layout_id") (vers "0.2.3") (deps (list (crate-dep (name "clippy") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^0.1") (default-features #t) (kind 0)))) (hash "072w2fnh3lpx6jjnjn8hbbmpdq9dpykcwkra3w5qr80ay0dibdmx") (features (quote (("use_clippy" "clippy"))))))

(define-public crate-layout_id-0.2 (crate (name "layout_id") (vers "0.2.4") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^0.1") (default-features #t) (kind 0)))) (hash "02lisynh6bzrp0d6m5qs6gr0wl0xxp7dym7r50d2x23k01ym44zh")))

(define-public crate-layout_id-0.3 (crate (name "layout_id") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xa9lv6gllil18vn6mxkq5kbz6jd9v4jgs92vfnn9kz693syw1id")))

(define-public crate-layouts-0.1 (crate (name "layouts") (vers "0.1.0") (deps (list (crate-dep (name "getset") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tycho") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1k7q4mascswml9xfvwj2rg7zp8j58gdn08b1zqxv7ja0kc02wa0y")))

(define-public crate-layouts-rs-0.1 (crate (name "layouts-rs") (vers "0.1.0") (deps (list (crate-dep (name "derive-object-merge") (req "^0.1.0-alpha1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "object-merge") (req "^0.1.0-alpha1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vkyhil8is267nn8sb80fc9srlj76z0cmjybhh7mx2imdkb9dv8h")))

