(define-module (crates-io la fo) #:use-module (crates-io))

(define-public crate-lafont-0.0.1 (crate (name "lafont") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "kiss3d") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1zw24y5qclvvw9i1cg76kp0zzmyjnzb71y8awcbgvyq4x2p7lmmc")))

