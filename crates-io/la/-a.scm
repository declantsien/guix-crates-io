(define-module (crates-io la -a) #:use-module (crates-io))

(define-public crate-la-arena-0.1 (crate (name "la-arena") (vers "0.1.0") (hash "0ffs7mm5shgixbd5r950mmjbj7y2k5ksrmvmhb3hbk16p6rmlf5h")))

(define-public crate-la-arena-0.1 (crate (name "la-arena") (vers "0.1.1") (hash "1hvg8wclgcskr7a7r194h833d3xrc1qklj7na4bhbl968jkx4giq")))

(define-public crate-la-arena-0.2 (crate (name "la-arena") (vers "0.2.0") (hash "1kwf6c3k0r7yih65482cw957lxb4zm6fqg3idqd516kd4j4lzihs")))

(define-public crate-la-arena-0.2 (crate (name "la-arena") (vers "0.2.1") (hash "1qxyl8lhc9s40gav8f9vbcw4yy40riygj7n8xayqf9xs8zsvjgfl")))

(define-public crate-la-arena-0.3 (crate (name "la-arena") (vers "0.3.0") (hash "09457xx5gwjaaghgzr3wlj0ycnaw8xidxf9kp87dwpz1046zxaqx") (rust-version "1.56")))

(define-public crate-la-arena-0.3 (crate (name "la-arena") (vers "0.3.1") (hash "01l3bvxzrpr7m80xill1v0rq10a6zxws8prq1zb83965vhlz4lip") (rust-version "1.56")))

