(define-module (crates-io la tc) #:use-module (crates-io))

(define-public crate-latch_iter-0.1 (crate (name "latch_iter") (vers "0.1.0") (hash "0ma8fqaxkh5bkw6wpdp9ka23rl28a886ignpmdwpnl7nl9i08z78")))

(define-public crate-latch_iter-0.1 (crate (name "latch_iter") (vers "0.1.1") (hash "12p2xabfhcq2r0hyqznlwgbqwy8fnpv6kw8x6b1nj5cmjw56bzdz")))

(define-public crate-latches-0.1 (crate (name "latches") (vers "0.1.0") (deps (list (crate-dep (name "atomic-wait") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "0nqnx6l1lfl4pbbhmfmkygigb2cnbz5pszpwxdjw4qiy94cfzaka") (features (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (yanked #t) (v 2) (features2 (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.1 (crate (name "latches") (vers "0.1.1") (deps (list (crate-dep (name "atomic-wait") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "0jh99m1wz6w8lxzgr4mghpa891dd05xfdzlybzmh8gg4jyhd893f") (features (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (v 2) (features2 (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.1 (crate (name "latches") (vers "0.1.2") (deps (list (crate-dep (name "atomic-wait") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "15b7wcnd60xpi0x4n5hglgjl6z2i643945zadk318isdvdpcj44c") (features (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (v 2) (features2 (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.1 (crate (name "latches") (vers "0.1.3") (deps (list (crate-dep (name "atomic-wait") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "137rvds91p7jn11mr25papafhwz1l396hih3fcbccpzivfpf488r") (features (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (v 2) (features2 (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.2 (crate (name "latches") (vers "0.2.0") (deps (list (crate-dep (name "atomic-wait") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "013j5dpp4pz6zk69lw4np1xjg5hxk18pl2kfd8k72m79wi647ci8") (features (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (v 2) (features2 (quote (("atomic-wait" "dep:atomic-wait")))) (rust-version "1.63")))

