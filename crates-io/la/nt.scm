(define-module (crates-io la nt) #:use-module (crates-io))

(define-public crate-lantern-0.1 (crate (name "lantern") (vers "0.1.0") (hash "1mcky8qal47nx6ws0cx4mkz5nxiyhwd9dy7glq9lj13zvxnlmvnd")))

(define-public crate-lanterns-0.1 (crate (name "lanterns") (vers "0.1.0") (hash "1yaam3a68j402339igbv8yirwhhzc9x9rz2d4n83ma30926jc146")))

(define-public crate-lanthanum-0.0.0 (crate (name "lanthanum") (vers "0.0.0") (hash "0nminx6safsfyw9qmwj7vz8w9p705gggmvf9zakfqcasbmj202sv")))

