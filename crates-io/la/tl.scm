(define-module (crates-io la tl) #:use-module (crates-io))

(define-public crate-latlon-0.1 (crate (name "latlon") (vers "0.1.0") (deps (list (crate-dep (name "geo-types") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)))) (hash "1lxfv4ihz6rl8pj5r8gp9cvr7k9h8hfxznsgi06q3h2g5ni6mssn")))

(define-public crate-latlon-0.1 (crate (name "latlon") (vers "0.1.1") (deps (list (crate-dep (name "geo-types") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)))) (hash "0aaclgvhawn2rj29w016j7dvwklb93a2c61dk9hq30lvbyzx813z")))

(define-public crate-latlon-0.1 (crate (name "latlon") (vers "0.1.2") (deps (list (crate-dep (name "geo-types") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)))) (hash "060jvmz9lzv083f86h1xc780dmjvkqmjcps2pqbjnp9sa3z0xs89")))

(define-public crate-latlon-0.1 (crate (name "latlon") (vers "0.1.3") (deps (list (crate-dep (name "geo-types") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1w7v2p2jivjf1j7y568hfkpbfkjhl5w5hczsb2zbys1z0h90wayl")))

(define-public crate-latlong-0.0.0 (crate (name "latlong") (vers "0.0.0") (hash "0b4cdz1vyrwddprxbxkq5cribs60bargq34sx4m3f2wq5h7bfv2g")))

