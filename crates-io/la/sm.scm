(define-module (crates-io la sm) #:use-module (crates-io))

(define-public crate-lasm-0.1 (crate (name "lasm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "comment") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)))) (hash "171yqb9xjf49qz4fvzya3ci00nfi71z0zsp225r8hxmd69pwjj2k")))

