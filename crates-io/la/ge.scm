(define-module (crates-io la ge) #:use-module (crates-io))

(define-public crate-lager-0.2 (crate (name "lager") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0wwyz7y74i5sm4y0xg172bay1q96s2jj82jy5msqjkcc413b5qd6")))

