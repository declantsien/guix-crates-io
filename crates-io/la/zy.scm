(define-module (crates-io la zy) #:use-module (crates-io))

(define-public crate-lazy-0.3 (crate (name "lazy") (vers "0.3.0") (deps (list (crate-dep (name "stainless") (req "*") (default-features #t) (kind 0)))) (hash "13fq3d52l61q1nckl4vs42r8njp618lgzswhmdk9qnmb8hbrv11d")))

(define-public crate-lazy-0.3 (crate (name "lazy") (vers "0.3.1") (deps (list (crate-dep (name "stainless") (req "*") (default-features #t) (kind 0)))) (hash "0bpm0bh1n0w983zyn5z74nfad4wi1yzgpmbaqqkzl7iwb5i3dv09")))

(define-public crate-lazy-0.3 (crate (name "lazy") (vers "0.3.2") (deps (list (crate-dep (name "stainless") (req "*") (default-features #t) (kind 0)))) (hash "1yljpq22pgbyz1achdkapk9h9a0qciymnw907p1jhi1i452n1vjg")))

(define-public crate-lazy-0.4 (crate (name "lazy") (vers "0.4.0") (deps (list (crate-dep (name "debug_unreachable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "oncemutex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req "*") (default-features #t) (kind 2)))) (hash "1rw1xb1rifwmzqsk0a3ncbvqn9rv2sw4hh6fciyhx12lxr06fgja")))

(define-public crate-lazy-0.4 (crate (name "lazy") (vers "0.4.1") (deps (list (crate-dep (name "debug_unreachable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "oncemutex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req "*") (default-features #t) (kind 2)))) (hash "0czwdsj2qs1y6b77q16zg0ql928288hbcz7iksm55gm4q1xx06xy")))

(define-public crate-lazy-0.5 (crate (name "lazy") (vers "0.5.0") (deps (list (crate-dep (name "debug_unreachable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "oncemutex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req "*") (default-features #t) (kind 2)))) (hash "1c1qar2p0kpdwbagd59zb5z6b74bcc65xjpxwy9vxjz2kwyhz93h")))

(define-public crate-lazy-0.5 (crate (name "lazy") (vers "0.5.1") (deps (list (crate-dep (name "debug_unreachable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "oncemutex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req "*") (default-features #t) (kind 2)))) (hash "0m2p760yckvrcin3dfdjzpwb2wac2nkbr2dp17w3kd8jqfm7mf8c")))

(define-public crate-lazy-0.5 (crate (name "lazy") (vers "0.5.2") (deps (list (crate-dep (name "debug_unreachable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "oncemutex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req "*") (default-features #t) (kind 2)))) (hash "08wv8bd8rr9dg8vckblha87k777qakigqz10ixvvwyb6d3zmsjzn")))

(define-public crate-lazy-0.5 (crate (name "lazy") (vers "0.5.3") (deps (list (crate-dep (name "debug_unreachable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "oncemutex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req "*") (default-features #t) (kind 2)))) (hash "13w82wl8khsxwkmizdj1h1dd0j30gl5w5zaqqvy4a7sg8nwy68pm")))

(define-public crate-lazy-arduino-0.1 (crate (name "lazy-arduino") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^2.0") (default-features #t) (kind 0)))) (hash "0jgqbx9wwhg8cbg476mx991r76a1nk0k6slvy6szq1mkdhc0d6k7")))

(define-public crate-lazy-arduino-0.1 (crate (name "lazy-arduino") (vers "0.1.2") (deps (list (crate-dep (name "termion") (req "^2.0") (default-features #t) (kind 0)))) (hash "1sdzc6a0xyw6p9kjhgcyihlbv83fhsh7sxaf3s3vxind48gsha5h")))

(define-public crate-lazy-arduino-0.1 (crate (name "lazy-arduino") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0") (default-features #t) (kind 0)))) (hash "0iq6qlw62gzpv5627gknrv2xrfn9kfvrdni4qsyk5yvnxfckbp1w") (yanked #t)))

(define-public crate-lazy-arduino-0.1 (crate (name "lazy-arduino") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0") (default-features #t) (kind 0)))) (hash "06iqapyhaz1xkiyd7rgk0q9b25xsyhkw8bd3hkc8xi3dv6np3b3k") (yanked #t)))

(define-public crate-lazy-arduino-0.1 (crate (name "lazy-arduino") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0") (default-features #t) (kind 0)))) (hash "130w05gchw6icddaw8jy4vp043mwlvvfwv6n4lsa93l1ch28bhky")))

(define-public crate-lazy-array-0.1 (crate (name "lazy-array") (vers "0.1.0") (hash "0w385j6973l9vwa1a2fzr8hkm6li3qmdciqij0n59np1q2b61cky")))

(define-public crate-lazy-array-0.1 (crate (name "lazy-array") (vers "0.1.1") (hash "0ks1ja95pgqydmck4qmjzbc342z5ql895p04g8g40ah85fhrnmj1")))

(define-public crate-lazy-array-0.1 (crate (name "lazy-array") (vers "0.1.2") (hash "11k7gqdnhvjwk063185bfw5jlir08xfajxgb8rd16a33w8aypgqa")))

(define-public crate-lazy-attribute-0.1 (crate (name "lazy-attribute") (vers "0.1.1") (deps (list (crate-dep (name "async-once-cell") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "lazy-attribute-core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "048yjj36ric5dyz176kfqq8v4ardck6ik3ibx4rl1qgdryrb4f22") (features (quote (("default") ("async" "lazy-attribute-core/async")))) (rust-version "1.67")))

(define-public crate-lazy-attribute-0.1 (crate (name "lazy-attribute") (vers "0.1.2") (deps (list (crate-dep (name "async-once-cell") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy-attribute-core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "07nnh6av4l9lks2qgk66a223af6cn7p85s57nzv0j5dn2vjvr44q") (features (quote (("default") ("async" "lazy-attribute-core/async" "async-once-cell"))))))

(define-public crate-lazy-attribute-0.1 (crate (name "lazy-attribute") (vers "0.1.3") (deps (list (crate-dep (name "async-once-cell") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy-attribute-core") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "09iyc4c1gwga7296ylhsyqyghwzrmmpbz44xaj6b3qhns0nqh5xh") (features (quote (("default") ("async" "lazy-attribute-core/async" "async-once-cell"))))))

(define-public crate-lazy-attribute-0.1 (crate (name "lazy-attribute") (vers "0.1.4") (deps (list (crate-dep (name "async-once-cell") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy-attribute-core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "185ldq40yclvdgibqf0ph2ydpc4xj53qjyx19j1h90lkc9k0f1jk") (features (quote (("default") ("async" "lazy-attribute-core/async" "async-once-cell"))))))

(define-public crate-lazy-attribute-core-0.1 (crate (name "lazy-attribute-core") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-once-cell") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-crate") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)))) (hash "0y7qp9hsc37yh4slmkalrc5zwgwpy5wrqh5xva6vlvznixr42w77") (features (quote (("default")))) (v 2) (features2 (quote (("async" "dep:async-once-cell")))) (rust-version "1.67")))

(define-public crate-lazy-attribute-core-0.1 (crate (name "lazy-attribute-core") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-crate") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17lv2brl9i7i4piww8b6bp4n548q0c9iy2yzkmxd8jj7nvg82yy0") (features (quote (("default") ("async")))) (rust-version "1.67")))

(define-public crate-lazy-attribute-core-0.1 (crate (name "lazy-attribute-core") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-crate") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1zvwpsw42h95hnh81y0j3j6lssmrhs4mbaisp8y363bhhbvv7wn6") (features (quote (("default") ("async"))))))

(define-public crate-lazy-attribute-core-0.1 (crate (name "lazy-attribute-core") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro-crate") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "00s7qr0b66dckjc4d5iyn0mbmpg8mbs55x5fi6xykl81h0zzsb4s") (features (quote (("default") ("async"))))))

(define-public crate-lazy-attribute-core-0.1 (crate (name "lazy-attribute-core") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro-crate") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0hvzvqd3zvazwx0cjfl5l6fgj2m1y4d93i0wdjxknnngjqwprgvr") (features (quote (("default") ("async"))))))

(define-public crate-lazy-bytes-cast-0.1 (crate (name "lazy-bytes-cast") (vers "0.1.0") (hash "10x1hfjgqgw1m8hkzs18mi3l6sfjhjyca6zvxypp49zifdzd1r6z")))

(define-public crate-lazy-bytes-cast-0.1 (crate (name "lazy-bytes-cast") (vers "0.1.1") (hash "1p8skizwal6lg7qpghly5rinbkacdb4d4xp3hj63j80n90ivisay")))

(define-public crate-lazy-bytes-cast-0.1 (crate (name "lazy-bytes-cast") (vers "0.1.2") (hash "1h85kifa22nf9n03njbn246rjckrqjw1lv73lnd3lzkz0vsc0msr")))

(define-public crate-lazy-bytes-cast-0.2 (crate (name "lazy-bytes-cast") (vers "0.2.0") (hash "0945rcagsv0fyjci6s4qpjfsdqwh1zjw4vpqdnwdmb5fid4f6kd6")))

(define-public crate-lazy-bytes-cast-0.3 (crate (name "lazy-bytes-cast") (vers "0.3.0") (hash "0pzlzann3h128907mch82hypga07wja11s05sgk0mjcykx4r2ycy")))

(define-public crate-lazy-bytes-cast-0.4 (crate (name "lazy-bytes-cast") (vers "0.4.0") (hash "0kc8bdh0j113rwbnz8nzrap7kcw0pccas7xz8lnfnjmmq4gzk2fv")))

(define-public crate-lazy-bytes-cast-0.5 (crate (name "lazy-bytes-cast") (vers "0.5.0") (hash "1yx0zhkfgaz165646mfsjig4hdg8gr87vz4prj48j22fn07krkm9")))

(define-public crate-lazy-bytes-cast-0.5 (crate (name "lazy-bytes-cast") (vers "0.5.1") (hash "09spr9a5349gsyjc9fqj8afqg4q9b3pa3ghnzmvjb8216xk9hcgq")))

(define-public crate-lazy-bytes-cast-1 (crate (name "lazy-bytes-cast") (vers "1.0.0") (hash "02ryf9hmhb3g2fmvf1xms0pj572n3ha8yrn61lajpwp382vhg9lh") (features (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2 (crate (name "lazy-bytes-cast") (vers "2.0.0") (hash "1w2r0vk3s7c9mnygahzc8j7rc3984ni78qhgcgxm0577rrl2xc81") (features (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2 (crate (name "lazy-bytes-cast") (vers "2.1.0") (hash "02ywqdyiy8h4v4vax11s3g9lcjxzdypidb0wcyzd2njvclqk7kd9") (features (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2 (crate (name "lazy-bytes-cast") (vers "2.2.0") (hash "1ilmnqyrm6wyha86w85d7890z76mmfy4rik167jpc9srf7y90ymz") (features (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2 (crate (name "lazy-bytes-cast") (vers "2.3.0") (hash "0vv4pj6igd34n984hp725jva8qfsb2waycg047ky6s1cs3bxyd5x") (features (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-3 (crate (name "lazy-bytes-cast") (vers "3.0.0") (hash "10lr9gzic8nn8mjg0f1bhzvj93xzz4qxj688mnyc0pdf8a7l2pf2") (yanked #t)))

(define-public crate-lazy-bytes-cast-4 (crate (name "lazy-bytes-cast") (vers "4.0.0") (hash "00v4qlxikkl7ib68w4disf8q1hjlc39d1clrfm9irxs9dh0pc0bw")))

(define-public crate-lazy-bytes-cast-5 (crate (name "lazy-bytes-cast") (vers "5.0.0") (hash "0gvbcrzkjnwsj1s8m742nmi9g0xg75dwb8x01bw9hqbqraa6ncwi")))

(define-public crate-lazy-bytes-cast-5 (crate (name "lazy-bytes-cast") (vers "5.0.1") (hash "0sr0dy1jfg7bjwm9js4hk0ngl0cmgparq2idv1m1bkc9y2cp898h")))

(define-public crate-lazy-bytes-cast-6 (crate (name "lazy-bytes-cast") (vers "6.0.0") (hash "1dxj63h6w6bqjxg7h6sjgg4mi68ja4gd4w5jr30c99ajz3z6wg51")))

(define-public crate-lazy-bytes-cast-6 (crate (name "lazy-bytes-cast") (vers "6.0.1") (hash "0s320205wb25l6i3z6zs7wzsd3cs7609r9lq3hznpjli4q2nxvk2")))

(define-public crate-lazy-bytes-cast-7 (crate (name "lazy-bytes-cast") (vers "7.0.0") (hash "1wa8jzqw6810cmzkmgs405djk0bi9b985127r28xk9bhkxyd0lvk")))

(define-public crate-lazy-char-iter-0.1 (crate (name "lazy-char-iter") (vers "0.1.0") (hash "10y825hj3lsisb2ry7i8v7v24vdpwaa05i1r110h0kysnimk29cf")))

(define-public crate-lazy-cogs-1 (crate (name "lazy-cogs") (vers "1.0.0") (hash "0da10ip2b0484l1a5wsbl3qszsv7rjixmqjzi6ab470l9ry1cb2b")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.0.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1cjfg28jw1f38989795q8rq6fxiklp1ij2pq08bjlg8xmrshg1wp") (yanked #t)))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.0.1") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0mywar067fpc6652wayahxklia2igmaw1jjl97yyydwqqm08xkb9") (yanked #t)))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.0.2") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1bx7r7zv9df29pa2l4rdj1vz6h0aw9bl3akmw1pyx4jv4hvwac8a")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.1.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "016hcfq5c9sanbxazlif2n70b3ccrafvchn6qyvl9h1lzmhzwdm4")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.2.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "17xkfm1a199vcds2xh7qbw7qr0497z7yvizr9nanrhnnzp06nwyz")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.2.1") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0raycvkw5ywknsxagvw2m0iblfmik4psfm95a2z8ndi7dr879crj")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.2.2") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "19x3yjqig1zik1frvyr4ylhqjcf6y7xxvcg3zjzad1cz562sbmp4") (yanked #t)))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.2.3") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "12wfydh4l2zsmgina0vz78llnrdnyqs53z0q1mzjia3045w68fxs")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.3.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "04wdbzy5lz191s7vcrhp3fr3v5fshzna08gm8bskxbmlf1l7zn1v")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.3.1") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0a5lyvphj5gici8y8a4789riz2jibdn0hh92a58nlnql3whng6vq")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.4.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1202y85vrabmbh7ksclahrra78rn0c1p5cc43r9crr81cfdpjbbj")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.4.1") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0wqgvs36k3f1zc0kq2465nk6b2qxmxmsn4fi4y93784ifmm8cwgr")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.5.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0hag907mfnp96pdd1gmlgrsjv52y07id25106vdz059lbr7lhc1v")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.5.1") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1cq1a9j3sf109wkll41a3zsjkb6zr3p7fwsxzpmr0mrhd66kacbf") (yanked #t)))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.5.2") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1z398m2d9wgf6vfr00gl60fabyvjn0b68vi40yylxy8a2av78lw3")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.5.3") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1hdyd892hv9iik7vmpz73ds0hgjxcgichmbflh7i32i23yqfhwdm")))

(define-public crate-lazy-db-1 (crate (name "lazy-db") (vers "1.5.4") (deps (list (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "0xhcryz2bb6kmqi87ypj1g428rdij7yqbdy702z5zg8y83y70j6f")))

(define-public crate-lazy-etherscan-0.1 (crate (name "lazy-etherscan") (vers "0.1.0") (deps (list (crate-dep (name "cfonts") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "ethers") (req "^2.0.11") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22.0") (features (quote ("all-widgets"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1fwz8kqx1dmjn9k7wh9rqqmqyxhx7mcq2y0vpahfj3dazmx2cfai")))

(define-public crate-lazy-etherscan-0.1 (crate (name "lazy-etherscan") (vers "0.1.1") (deps (list (crate-dep (name "cfonts") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "ethers") (req "^2.0.11") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22.0") (features (quote ("all-widgets"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1ayg6jvy120lrg41zwwq061xj6w9qxhjdg8jcf988vv4g9i90zfj")))

(define-public crate-lazy-git-checkout-0.1 (crate (name "lazy-git-checkout") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.14") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "00pqwcp4hlgc64zlgq4q21k0n5x8z8asv4x49c7zxwayxq8w3h31")))

(define-public crate-lazy-git-checkout-0.1 (crate (name "lazy-git-checkout") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.14") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0qkihjpifyzjmmh83xps1lysfjrqw83g8v5f80nwh50gy4i0jf81")))

(define-public crate-lazy-git-checkout-0.1 (crate (name "lazy-git-checkout") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.14") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "15a98z8vp6403a084cg4293lcs8b05zhpwcx272dzwqhbdr6faqw")))

(define-public crate-lazy-git-checkout-0.1 (crate (name "lazy-git-checkout") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.14") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1qlc84bxm3z16fgab98aym5r1wp9qdjg2d6h1k49s2akzsdc5w80")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "10lbyh02nhxvspd3iygn2lfhrzqbjqzk2xc1sc0y85kg1r6l6zqa")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "09bgy3rlkpl3wsvvkhn53a696x9yznvhjd5xj859m1d97208gva4")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.2") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "07afbnacafxhznpc74js9b57a31842vsrwwn17l11adw9jgbq4wc")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.3") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1fn5zvb2b91gdsaaxqmhx5xgkvk82yvb72dvv88qkk79j948wlj4")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.4") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0np31gf6b4y2rf64na97sz3dggjj6p426rkhsbpqv7k842phdx60")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.5") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1lrzilw1fxs06v9f23lccl2mr0im8gdwqa9b8jda5nsrva2rzmff")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.6") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1cxnlnrzvgc66hz3jx7wa84ig184zpfbf719wvinnn50axj257pr")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.7") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1xig4al1f8dyb66q8n8zcxsz1aidpmxdbbywx70dqp22maqnysad")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.8") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0f9a4gsdxa2n1m6sh6g2c1ln0brpm1b3srrnlsrmppsxsily8wxl")))

(define-public crate-lazy-heap-0.1 (crate (name "lazy-heap") (vers "0.1.1-alpha.9") (deps (list (crate-dep (name "slab_allocator_rs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1d5pqrbh48hd0prbcd93416c1n9qs2ayrykb6jz6j16syfc7smgm")))

(define-public crate-lazy-init-0.1 (crate (name "lazy-init") (vers "0.1.0") (deps (list (crate-dep (name "scoped-pool") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "13lffspxglbk8ib4ajjr881gqj5l9zr3slf633q81z0bi0cnf6hv")))

(define-public crate-lazy-init-0.1 (crate (name "lazy-init") (vers "0.1.1") (deps (list (crate-dep (name "scoped-pool") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0ljrr0lxdan9ln7n7cfl6ff637k4fmf6xzm13bi8fihb62fb2v2r")))

(define-public crate-lazy-init-0.2 (crate (name "lazy-init") (vers "0.2.0") (deps (list (crate-dep (name "scoped-pool") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0wrpqjdar2jmraa4lr287bl3wp68kyd9rwcnlm14j7zzmb4a2y89")))

(define-public crate-lazy-init-0.3 (crate (name "lazy-init") (vers "0.3.0") (deps (list (crate-dep (name "scoped-pool") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "04ykc9ccprzca4224y0ci2wq479kawdf53nsfra936i3mwrj47z7")))

(define-public crate-lazy-init-0.4 (crate (name "lazy-init") (vers "0.4.0") (deps (list (crate-dep (name "scoped-pool") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1arhx5bazfpggfgsbq1873fd2v3y17c96555xj452pcvrkrja437")))

(define-public crate-lazy-init-0.5 (crate (name "lazy-init") (vers "0.5.0") (deps (list (crate-dep (name "scoped-pool") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "058qswwdkrx5x4m8ds1y29ql28d8dclgcssf6831vac7pr07al93")))

(define-public crate-lazy-init-0.5 (crate (name "lazy-init") (vers "0.5.1") (deps (list (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "0vvhq8xdmajy2ai8p7zxja68a95n7m65xhdgjapxq4mc4qv9ch4z")))

(define-public crate-lazy-lru-0.1 (crate (name "lazy-lru") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "01ddvvvxqz0k3r9cx5lhxlg6r52lkr27nkg2picf8hx0qscj67pb")))

(define-public crate-lazy-lru-0.1 (crate (name "lazy-lru") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0arl2fzhw20z5dcmi2a62bh8alnq3hv45n0glc3cwm4mqyk3q6q8")))

(define-public crate-lazy-lru-0.1 (crate (name "lazy-lru") (vers "0.1.2") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1wgflfdpk243xjbrj6mab3vyxifmnfxigv9qx4wdzwvn2ay366zq")))

(define-public crate-lazy-observer-0.1 (crate (name "lazy-observer") (vers "0.1.0") (hash "05dhql0lq2l1fklxkh0pb4r5wrxpbbbp6mm224g65gzhyzqadgwj")))

(define-public crate-lazy-observer-0.3 (crate (name "lazy-observer") (vers "0.3.0") (hash "066dw4ah8lamkq0sp25bpa96sqv9xl1kayjjhbwpdc92ixbq1ma6")))

(define-public crate-lazy-panic-0.1 (crate (name "lazy-panic") (vers "0.1.0") (hash "06znzlfbj6xdsxhbyq3sdq1lhfsav282b3s310avfblrh1dpji9k")))

(define-public crate-lazy-panic-0.1 (crate (name "lazy-panic") (vers "0.1.1") (hash "0qmiyizgdxfb452yn5n5xsgbvpmffnw9833gqx0adbqw7v3l00ik")))

(define-public crate-lazy-panic-0.2 (crate (name "lazy-panic") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0fwy3ck8agmdywc87c3r1n89n0pnjbi7vzkwgcq44ql7akzqv6m3") (features (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-0.3 (crate (name "lazy-panic") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "04javjpzllrhv5ig0q7wmdjghxls37jh3an95b4bw0cvgvyi711l") (features (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-0.3 (crate (name "lazy-panic") (vers "0.3.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0m3ckiiv4nkgc0zb406acmxclyvbb703p4rna16r0c3yfxcfc72d") (features (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-0.3 (crate (name "lazy-panic") (vers "0.3.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1mc29qv04hnkncqhlwv72z7b599xch7avdk06i6q7w75g58yhm94") (features (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-1 (crate (name "lazy-panic") (vers "1.0.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0vgaq2afgxxv5c8x5vp3j91wg281jd66hcz72p3sgvkidfkv7d90") (features (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-pbar-0.1 (crate (name "lazy-pbar") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "0ds0i268hzqxyybdkx3jvs9b4dwcjaam1hss1kkgxka4410zqs3p")))

(define-public crate-lazy-pbar-0.1 (crate (name "lazy-pbar") (vers "0.1.1") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "0fcpqv60hv70kvc7c6l4hyyxy37v73ajhi1lh6xw90wmb3nr2icp")))

(define-public crate-lazy-pbar-0.2 (crate (name "lazy-pbar") (vers "0.2.0") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "023xn8a3znc7h1wcqlfcm5kk0ijs1zmmrwywz9azlidcchgi29ig")))

(define-public crate-lazy-pinned-0.1 (crate (name "lazy-pinned") (vers "0.1.0") (hash "0i1vp8s4xl3qiiw25kkwkvgck4igg4dw2rmz18dpwhggsza418c5")))

(define-public crate-lazy-pipe-0.1 (crate (name "lazy-pipe") (vers "0.1.0") (hash "17icpa0rzd0c542pyv2mmxqmyqvahz4jx8ndryqb3cpzharc9jik") (yanked #t)))

(define-public crate-lazy-pipe-0.1 (crate (name "lazy-pipe") (vers "0.1.1") (hash "09mahr93w57zqd3sbsdyp5ar5yrhjcxn8y82dlnxixymn57mgw3n") (yanked #t)))

(define-public crate-lazy-pipe-0.1 (crate (name "lazy-pipe") (vers "0.1.2") (hash "0502ajz7b549yxy439x02nkx8f59ipggw4kngb5n92j2rbsd2x8z") (yanked #t)))

(define-public crate-lazy-pipe-0.1 (crate (name "lazy-pipe") (vers "0.1.3") (hash "0m6ma48i0fmj4ijh6bv410ghacfzd61p3j6amrm4ngp0mznl5pgq") (yanked #t)))

(define-public crate-lazy-pool-0.1 (crate (name "lazy-pool") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.1") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1qld1wqb3i6rdyv6yrlv0qyd5r3ipjvxxxm1kvxwysk5iaglic23") (yanked #t)))

(define-public crate-lazy-pool-0.1 (crate (name "lazy-pool") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1bsm967jmr5am80flvq5r50qqjnv93p7k5fpnx65bcp49w7p1dqp") (yanked #t)))

(define-public crate-lazy-pool-0.1 (crate (name "lazy-pool") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "02wscqsm8ky5cbhjil3dydm5i12wwm6rwgn2xlcwl0fnxsqi0gch") (yanked #t)))

(define-public crate-lazy-pool-0.1 (crate (name "lazy-pool") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1zac169l1479kisg8wjsjvs240chn11jdch0rkrl4y9vdr13ghvn") (yanked #t)))

(define-public crate-lazy-pool-0.2 (crate (name "lazy-pool") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1y3hk918wmyvq0a120qmlcxvqcn9dl87jg199iap40wyb5v3frpw")))

(define-public crate-lazy-pool-0.2 (crate (name "lazy-pool") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-await") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0vg5y4ak43lkjy0y644slnmxg8w5rbaw9h1x45qzb98bmcrlxyk1")))

(define-public crate-lazy-pool-0.2 (crate (name "lazy-pool") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-await") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "127zmwfgq16sh0ccm87ap4daidl434s73ld6m8p5jxc2maddwlzj")))

(define-public crate-lazy-pool-0.2 (crate (name "lazy-pool") (vers "0.2.3") (deps (list (crate-dep (name "futures") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "04323vn11swssxij8qafjlgiqs4d483n5d4wl01i1nsy763igs35")))

(define-public crate-lazy-pool-1 (crate (name "lazy-pool") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0yhizp2nzd4p0crcr7vwsl8f1v55094pq61b1px6pm2rpkj46ssy")))

(define-public crate-lazy-pool-1 (crate (name "lazy-pool") (vers "1.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0ljcffw0d7kkimv80iaqmbxz74a88cxk81sys4s7fifxd2syvqim")))

(define-public crate-lazy-pool-1 (crate (name "lazy-pool") (vers "1.3.0-beta") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1d71sn3a977ji9lpl9jwm1zln8zl0pdbxf1aqkb9i2j93mv3nrcc") (yanked #t)))

(define-public crate-lazy-pool-1 (crate (name "lazy-pool") (vers "1.3.0-beta.2") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0zl1ns1lrf751zdk56fic06n2hhcbyj3kp80cn2ylpplipaar4r1") (yanked #t)))

(define-public crate-lazy-pool-2 (crate (name "lazy-pool") (vers "2.0.0-beta.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0dg8jvdhci4z88s4iwjp7d3wnp17zxqfs70gp44zx8j9hrr5l26f")))

(define-public crate-lazy-pool-2 (crate (name "lazy-pool") (vers "2.0.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "~0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "17p24qcn1yrgcrkc7z6qiym0zd3knmix139bvnmsgylj3a2pif5w")))

(define-public crate-lazy-pregen-0.1 (crate (name "lazy-pregen") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_debug" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "09vbf2dkhv40mf49hxzzm2gcrq562mm1nj0gmfdxz1pkh8cxgr4p")))

(define-public crate-lazy-pregen-0.1 (crate (name "lazy-pregen") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_debug" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "0kkdkl33yi34q1m5bd2skzsrw9nh552q63rwm2dslfa1iqi3wwdr")))

(define-public crate-lazy-pregen-0.1 (crate (name "lazy-pregen") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_debug" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "04rfs1pa9gmih7jgwgaka7dz9gcfbjhsjnjplw8fsj3yf4dfsaf4")))

(define-public crate-lazy-pregen-0.1 (crate (name "lazy-pregen") (vers "0.1.3") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_debug" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "10ach5zmn4ipvry0l5vzpciimlaggrwvwf81b9qkn35zhzy5q0fv")))

(define-public crate-lazy-pregen-0.1 (crate (name "lazy-pregen") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_debug" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ra3k656yniqwix8zqn6xjbd9amajp15qiql7si8yxjhmmiriy6s")))

(define-public crate-lazy-prime-sieve-0.1 (crate (name "lazy-prime-sieve") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "0dyir8gpisab9lygfs90m3pn8ngmwddfmxgrc40ajy2by3ws4jnw")))

(define-public crate-lazy-prime-sieve-0.1 (crate (name "lazy-prime-sieve") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "0a74h36fvq8l8vyarvl3rll117qbnzpfxribhwq45kavhrx0cjpm")))

(define-public crate-lazy-prime-sieve-0.1 (crate (name "lazy-prime-sieve") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "04n4l62ynk1x5h1nh7vvr9nfgzlgjp37x0vj7frfi4axwadr663s")))

(define-public crate-lazy-prime-sieve-0.1 (crate (name "lazy-prime-sieve") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "02r9hgpjl4bp0rgi3r0kd034xc771j5xvy077648niqr082v4wmb")))

(define-public crate-lazy-queue-0.1 (crate (name "lazy-queue") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.17") (default-features #t) (kind 2)) (crate-dep (name "tokio-sync") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1mr413bdf2f2lj14nmbmlmxdvkdr9fbd5wsrxrinr3m8gvccdci6")))

(define-public crate-lazy-queue-0.1 (crate (name "lazy-queue") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "tokio-sync") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "10mhc5hg2narnmy0r7vppfc9fz23z0x7j1a6y47d6vicvdqvj7yy")))

(define-public crate-lazy-queue-0.2 (crate (name "lazy-queue") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.6") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.6") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1mmljkn584z4cfmrv3bdf2304y37q2wr93n262i0dk9vxmqmmarg") (yanked #t)))

(define-public crate-lazy-queue-0.2 (crate (name "lazy-queue") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.6") (features (quote ("sync" "stream"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.6") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0k0phjj9a38ipciqrqqp912rahdqj9yya0zgak4lwaysir6c0sh8")))

(define-public crate-lazy-re-0.1 (crate (name "lazy-re") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "053f17mc4gbbycl7acmpx8xna5afk1yj9pbsxbc4z9fsjr0fxlri")))

(define-public crate-lazy-re-0.1 (crate (name "lazy-re") (vers "0.1.1") (deps (list (crate-dep (name "memoffset") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ax3gdclrwc70icvdr2li7vqwqd4k6nxlpbfafsyy22w0hjg3fwa")))

(define-public crate-lazy-regex-0.1 (crate (name "lazy-regex") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ns44f652g088ir6n2b4vbb0888083bh3pkj2kdg0q7lb9c2p5nx")))

(define-public crate-lazy-regex-0.1 (crate (name "lazy-regex") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "0l27zyavmlpzrxb42pyqq49f2855lw9hmh4vwzsmgbznknijf2nc")))

(define-public crate-lazy-regex-0.1 (crate (name "lazy-regex") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "1zr4s2cv9l2d6bf7j1kablfsbl27sr78l3dys6fzv611qrv15n83")))

(define-public crate-lazy-regex-0.1 (crate (name "lazy-regex") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "0f12g0i6l5ipcrcwms9283j2j9njbyqc1z176ncd5p61db19p020")))

(define-public crate-lazy-regex-0.1 (crate (name "lazy-regex") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "03s75cfjrkwqmlqdvnvz7y6qhk8jbmb4p6l9la8z2w27rx12p8w3")))

(define-public crate-lazy-regex-1 (crate (name "lazy-regex") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "10zjzil9y7gclc7x9ixiipnp42nyxzirhq5jxblswzz83vc5hhvx")))

(define-public crate-lazy-regex-1 (crate (name "lazy-regex") (vers "1.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "002fnbiib0f29f1wl796b54ml4jk3bh6rka8cks4mrzfx9r2cywj")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.0.0") (deps (list (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "proc_macros") (req "^2.0.0") (default-features #t) (kind 0) (package "lazy-regex-proc_macros")) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1i4xakca60n3z4fwgz4vkwnv38ji4ng2dglgryvwjnxd6q0narlx")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "proc_macros") (req "^2.0.1") (default-features #t) (kind 0) (package "lazy-regex-proc_macros")) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "18smnic6sj97dm6l6cvhkjdb18qmdc0mi2wcj9c0yf95by389vjm")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.0.2") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1lb17gm8iqda5dqkbpgq7yn6ik3r7aky0nkd32f0lax8pjl3kkaw")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.1.0") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "07n4zz4fwlf8821mrncngax5s7z1pxwp0z4ly3wfsyywv3jg603v")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.2.0") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1csn15x57k2xwgif6g8v8b9i2zw3pmn39k7wbzd947zmyiw89m09")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.2.1") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0m2anc6knp9dndkyqqf7bd7dly7p3is5vgf0ln4fgxkj2bwril8p")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.2.2") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (kind 0)))) (hash "0g9bksfbb9m53yjkmql00yxmablk200lcn7wjmzf4bdz7rvid6li") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("default" "regex/default"))))))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.3.0") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (kind 0)))) (hash "0zfm5zrwbrld57irnvbsv2d6l42c9c1mmcjybi03jzgdnqp2zcdn") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.3.1") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (kind 0)))) (hash "1k0p3svj4lmnlys0qi797mn59kh0phwi0iaajxxp44xlk2zmd5mf") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.3.2") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (features (quote ("std"))) (kind 0)))) (hash "108r4namij1k5jc7jf4nyvlqk48pjgx2padrsdyq2p23rawfaji7") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("default" "regex/default")))) (yanked #t) (rust-version "1.65")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.4.0") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (features (quote ("std"))) (kind 0)))) (hash "0a6fz205ih7wznbr0fm64k8i8k3zrdwjs2vfd1as0ds0q88s41pv") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("default" "regex/default")))) (rust-version "1.65")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.4.1") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (features (quote ("std"))) (kind 0)))) (hash "14v7ayd3vpr8jp924z9ynh0f31gjy2ajf9ax8amqgzdyi4pxl1d5") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-2 (crate (name "lazy-regex") (vers "2.5.0") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (features (quote ("std"))) (kind 0)))) (hash "1m1mhaf7d488jx3gcy4yvmywi4zphpjqrs59gm5q39lfqqiw8qzz") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-3 (crate (name "lazy-regex") (vers "3.0.0") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "regex-lite") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1xb84pyyn6yr72bcgzac5l5xk3y3jsyqfzalns0iks77p75gw351") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("lite" "regex-lite") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-3 (crate (name "lazy-regex") (vers "3.0.1") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "regex-lite") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "009wj6651mf4x48zmnkr5g29aab54hj2m76nqgv9ya2ymlcisiap") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("lite" "regex-lite") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-3 (crate (name "lazy-regex") (vers "3.0.2") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "regex-lite") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1yh5igsvsydn5jibalmssi2v417ajzr2as1bd87hmxidgd0vs8z7") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("lite" "regex-lite") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-3 (crate (name "lazy-regex") (vers "3.1.0") (deps (list (crate-dep (name "lazy-regex-proc_macros") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (optional #t) (kind 0)) (crate-dep (name "regex-lite") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0k3sjdcr13j119kgz1h7f1k2mnl787ig9ag436ymipxgjm2vw4jx") (features (quote (("unicode-segment" "regex/unicode-segment") ("unicode-script" "regex/unicode-script") ("unicode-perl" "regex/unicode-perl") ("unicode-gencat" "regex/unicode-gencat") ("unicode-case" "regex/unicode-case") ("unicode-bool" "regex/unicode-bool") ("unicode-age" "regex/unicode-age") ("unicode" "regex/unicode") ("std" "regex/std") ("perf-literal" "regex/perf-literal") ("perf-inline" "regex/perf-inline") ("perf-dfa" "regex/perf-dfa") ("perf-cache" "regex/perf-cache") ("perf" "regex/perf") ("lite" "regex-lite") ("default" "regex/default")))) (rust-version "1.56")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ldmrxv9827q7cykd4s9h3mpy3gj3hlq2mgn9nwclckfw19jx1r1")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "10nsnv3h6kkwblsnjjdnppd8j0k0lyg44x5bddjkx7xka97va3lz")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "04xb6jzz9rlimg3a1vxc5xkdrwwfiq1qs4z0vzzdxw7px49z4pa9")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13ac6vz5rq1pfg1jv2hbp1qviw7ax3rzshi3w47nyad1pqdk4vyd")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12qqvpz9bji8yf5q95c8qcnwn3bikvw0cwy0l1pg9fr4m8ciqy27")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kpqhhs2p9zn2q4cfjz30vkwgl4znwapwlj057i5pkwj3f5r64kc")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dkf94rpg55kfwfc5vsvjn5rpvbmgq0da9b6jw3irxaal3q6pgjz")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14rjlb49k7vjfqznr0qd26p0d56qkimvfzpkrhkcg6q6ci96wjgj")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dnxf93km1qp627b3sny0whirdgjk1big97xj7q40dpplm130g6s")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08gn2273jpd21rwiy4wjv5pc796bbd3aa1w4k7b31yjkxfpxyalv") (yanked #t)))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xs6yg39cvf4s96k9jyk65xqjh3hhkgfng0hy0gx7v9jnl796mp2")))

(define-public crate-lazy-regex-proc_macros-2 (crate (name "lazy-regex-proc_macros") (vers "2.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08hwc7w6y20v32p08swg9ar9p86gawas4bp60zi8bkjniwdw3pwf")))

(define-public crate-lazy-regex-proc_macros-3 (crate (name "lazy-regex-proc_macros") (vers "3.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n6mw94l82vhfkdhh9yra8zazx8ixdjjsm6nkrl7xbcxv3ynsx2g")))

(define-public crate-lazy-regex-proc_macros-3 (crate (name "lazy-regex-proc_macros") (vers "3.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pvjdh8q6bsnn6hil7nd46mg4w04pb8cba885s32xvph768is2hg")))

(define-public crate-lazy-regex-proc_macros-3 (crate (name "lazy-regex-proc_macros") (vers "3.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02zgaxdq95s0xm4svbdz6f4xkf4kncl5gjfdzyxgr9wpdj7dbg24")))

(define-public crate-lazy-scoped-0.1 (crate (name "lazy-scoped") (vers "0.1.0") (hash "09g9331p3a9fxqsnql267ivkyvcfa3sy2niysdmmf5mzyn4g3hhj")))

(define-public crate-lazy-seq-0.1 (crate (name "lazy-seq") (vers "0.1.0") (hash "1w5qqk8cshqixv2b2adbhr1wiai5a4hji81jwv66x1blr59c8h6i")))

(define-public crate-lazy-seq-0.1 (crate (name "lazy-seq") (vers "0.1.1") (hash "10nnjsa8447ka10bfw881zpjpwn5bvsbr0dpqxjm9gw1hraxic94")))

(define-public crate-lazy-seq-0.1 (crate (name "lazy-seq") (vers "0.1.2") (hash "1b0lh73wi9h6wc6vrap98dnnz9zi97lzg3b9vl0875lxsnp90qk4")))

(define-public crate-lazy-settings-0.1 (crate (name "lazy-settings") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "=0.13.2") (default-features #t) (kind 2)) (crate-dep (name "darling") (req "=0.14") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.13.1") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "=1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "syn") (req "=1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 0)))) (hash "11fnhriyqbgbh9ag5mn6yhj4hb98b12jg3qjkxjfbjqv0a4y2ma7") (yanked #t)))

(define-public crate-lazy-settings-0.1 (crate (name "lazy-settings") (vers "0.1.1") (deps (list (crate-dep (name "config") (req "=0.13.2") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "=0.14") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "getset-scoped") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "=1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 0)))) (hash "1gqfdca18nhp1yw9knpmh38vz6rpxyw3blp332bs4yqw4d1k0rvs")))

(define-public crate-lazy-settings-0.2 (crate (name "lazy-settings") (vers "0.2.0") (deps (list (crate-dep (name "config") (req "=0.13.2") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "getset-scoped") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "lazy-settings-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 2)))) (hash "0v16aapjx9drk337z24ss33iv0ixx4xanc1al9scgnp1nmsq61l0")))

(define-public crate-lazy-settings-0.3 (crate (name "lazy-settings") (vers "0.3.0") (deps (list (crate-dep (name "config") (req "=0.13.2") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "getset-scoped") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "lazy-settings-macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 2)))) (hash "0ir8dcipxqg7lhq92n39mlxg55wb1zr8dy3ci991fmd8pfxbyka9")))

(define-public crate-lazy-settings-0.4 (crate (name "lazy-settings") (vers "0.4.0") (deps (list (crate-dep (name "config") (req "=0.13.2") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "getset-scoped") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "lazy-settings-macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "=1.0") (default-features #t) (kind 2)))) (hash "1bsgscbx51f7g819xcfzk41s9c20i8m643k0rrna3dy1qlyyqwb9")))

(define-public crate-lazy-settings-macros-0.2 (crate (name "lazy-settings-macros") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "=0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "=1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=1.0") (default-features #t) (kind 0)))) (hash "0mg8n69dfzxm0pdgqmsz5azqmgw4z2wqpmlfhhv4kffasic6laml")))

(define-public crate-lazy-settings-macros-0.3 (crate (name "lazy-settings-macros") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "=0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "=1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=1.0") (default-features #t) (kind 0)))) (hash "11jwzl6hkm7hd3fymgjhpaqmw87hiazvfsgpls1dhzrp3n2k876s")))

(define-public crate-lazy-settings-macros-0.4 (crate (name "lazy-settings-macros") (vers "0.4.0") (deps (list (crate-dep (name "darling") (req "=0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "=1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=1.0") (default-features #t) (kind 0)))) (hash "091kqlbwr04f61fiikpnbcwiwkims67ncdh5q994kxb1cjmndjc8")))

(define-public crate-lazy-socket-0.0.0 (crate (name "lazy-socket") (vers "0.0.0") (hash "035hmsbdbdp5i49j90akx1lid031dksaalmqnl6isl6gn35pcclx")))

(define-public crate-lazy-socket-0.0.1 (crate (name "lazy-socket") (vers "0.0.1") (hash "151ivgmnimdxpzzjd0a2v0z3c49sh65jka4ssyq74a8izf55hmny")))

(define-public crate-lazy-socket-0.0.2 (crate (name "lazy-socket") (vers "0.0.2") (hash "1dhg1b2nbm9k661sx18fkcyz82c7qc91ki4bvx0hyfvcdr80n2cs")))

(define-public crate-lazy-socket-0.0.3 (crate (name "lazy-socket") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1h9gpxq5kgprs7bzhg6ckcw1biys0xsf1kkb2mb3l33qq11prxsm")))

(define-public crate-lazy-socket-0.0.4 (crate (name "lazy-socket") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1s3s14iwc0qvrnfqrj0jvfjrcsrxiksvblyy14xnzz56n98whhs5")))

(define-public crate-lazy-socket-0.1 (crate (name "lazy-socket") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1gqmdcb323q9s7pfss66v9y6qj3yll1w0af57p83d4rn3zsz8rg3")))

(define-public crate-lazy-socket-0.1 (crate (name "lazy-socket") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1h75bqmlri3jbaz2m4qk04nragxcj9flj6513lsz5i8318405q77") (yanked #t)))

(define-public crate-lazy-socket-0.1 (crate (name "lazy-socket") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0dwbcpa65llligihbk8113kj83bny4xsi5yw82i79s2hjfkydhqs")))

(define-public crate-lazy-socket-0.2 (crate (name "lazy-socket") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "ws2_32-sys") (req "^0.2.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0xlqqg75vrxh16rww8kkdyrg3il8k1mrdywy49jvx9jb58p924m4")))

(define-public crate-lazy-socket-0.2 (crate (name "lazy-socket") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.22") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "ws2_32-sys") (req "^0.2.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0i7x5f6ncwadv0kxxx9lrbkp11pjlz333r3wch399vih84g6lfna") (features (quote (("safe_buffer_len") ("default" "safe_buffer_len"))))))

(define-public crate-lazy-socket-0.2 (crate (name "lazy-socket") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.22") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "ws2_32-sys") (req "^0.2.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0prbgc19rvrc0lgqs43wbf9yhim0jjs04bs1dgm3900i93fx4q7b") (features (quote (("safe_buffer_len") ("default" "safe_buffer_len"))))))

(define-public crate-lazy-socket-0.3 (crate (name "lazy-socket") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.22") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "handleapi" "winsock2" "ws2def" "winerror" "ntdef" "inaddr" "in6addr" "ws2ipdef"))) (target "cfg(windows)") (kind 0)))) (hash "1w15rvimn5q9rmk8akhdnjdda285w0gsqs61shf025vvyqwh1xrf") (features (quote (("safe_buffer_len") ("default" "safe_buffer_len"))))))

(define-public crate-lazy-st-0.2 (crate (name "lazy-st") (vers "0.2.2") (hash "1xm4lcm018z14mai3dim4cd2iz3s5i2jxx816j2nxsp7jnjx2a5l")))

(define-public crate-lazy-st-1 (crate (name "lazy-st") (vers "1.0.0") (hash "16i7yrrnwcj4sv7fx2b0danl0kafs7isnajl8s69dd10wg61p9nj")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "03b101sdifwzvldsj38042cqypskc96aiv001pq99njaxvcmlbx9")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "01nyi5w0hqvm5ajz021f7ha7xh3yy7vv3kv36qv7gb92302c10rd")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "13mgh9j943siycn0him7mfn4sdgbf5zqy62x51yhhsich2lkyyc9")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "1g97j149wy818scmxxkn4rchdq093kw39k169q1sbms8pas580jj")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "0anxqdgs52m0cwdbxvkzfpn3pbhmks6fypqck52939g8cmnclknn") (features (quote (("benchmark"))))))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "06spnp7q3kik5vx79pr193m5dv5ldw97gc329nm2nzksnwn2smm1") (features (quote (("benchmark"))))))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 2)))) (hash "1vd308kxmmxkjkdjdvi5m18yblgbnish9906020jvisvdh6srrcv")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.2.2") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 2)))) (hash "1rwz4cv73n5kmh8cxn6yypr956117knwnd2rrd0lxmyadw8pl223")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 2)))) (hash "01xzng5g01h7f5g0fldrfg50pmcq3z49wycs6nar7k5wchv2mq37")))

(define-public crate-lazy-static-include-1 (crate (name "lazy-static-include") (vers "1.3.1") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 2)))) (hash "00nxggf77na1ms0qzsrbc52i94k1944d342d49my725r2m3ml9n7")))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (features (quote ("parsing" "full"))) (optional #t) (default-features #t) (kind 0)))) (hash "12ybn9w2rsialrngij5incxsl0ksz952b4v7lzl632rh647sa9y4") (features (quote (("no_std") ("default" "starts-ends-with-caseless" "syn")))) (yanked #t)))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (features (quote ("parsing" "full" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "02hcsikiy8vs5a2flaah59n168k46y5120h6a12n766y84pklyii") (features (quote (("no_std") ("default" "starts-ends-with-caseless" "syn"))))))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (features (quote ("parsing" "full" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "13dgifshnh2dv6v0zv9fsjp1rid51jnyqa6xcm1xpf2gd797p8cy") (features (quote (("no_std") ("default" "starts-ends-with-caseless" "syn"))))))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (features (quote ("parsing" "full" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "15y5kb9ay77jgw1hxjdhzgvz0p09sgmxd0xpklc842kprjsqi3ak") (features (quote (("no_std") ("default" "starts-ends-with-caseless" "syn"))))))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("parsing" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0hrqpx05kk0a0261385c3zwiabn02am3ir83gbs0g4jzm8xvwgsb")))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.2.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("parsing" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1b1ldwl2jzkcjb9isl6gd5bkajwqf26s0fx02xdr47y2q6hmf1bg")))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.2.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("parsing" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0092b0prmblbi3ns8nkmwcwqjgii4jzhqih2yrbx1lp18120d06x")))

(define-public crate-lazy-static-include-2 (crate (name "lazy-static-include") (vers "2.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "starts-ends-with-caseless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0a59i4mk05nilrd9q7ppgplxi80ws0yc5kdirxnhma672vjh00p1")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rzjgfa6ck085bzf1w3r1mm5blchw6lw24z652yv6m1094zazwzy") (yanked #t)))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mmcw9hprx5mg9wsf8fdrkhdgjaxnx4vca83ff7jijahrbx336ky") (yanked #t)))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18w0iz93gsgk6yr60znnc8dz5cjzy13sk0a7zldmcpxsh2rmc5dc")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "099swccqnbrbnn5hc38af7gr3zb9f62rb4318am2pk58pg99p3gi")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16gwd4z7cy98lbsf24hi3lfr3279wgvryxf1qy150czm064lrsa5")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lam1ilh6kafjhf2iajlgc822zy3smbal56b6a6ngn62c2ljx52c")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g7p2z8h0rhys25xga0y9xlv8s34riirfvvjnd9zrmf6hq1crj3j")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.0.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y55alsz6yaxsk6d7wpg7d2bz1cspm3l7p89r149abqvbamgmmv7")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j1q7xsx0j0ms06b5haylcvc1ni2i10rc4x01d04724pax2ns5li") (yanked #t)))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (features (quote ("tuple"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "024r0ascryccis0p3xl7gl17vx7wxg4sfa99v3lgknib402gw0k0")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.1.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (features (quote ("tuple"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3.1.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0361jcwbzjvxribiywdd1bci3i3wdbcr5a4kcmwl75xv3h3xang5")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.1.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (features (quote ("tuple"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3.1.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c5zj541sbcz3ibm977vx4pd16dzpzw6csvkl30h02br88zy3hmi")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.1.4") (deps (list (crate-dep (name "assert-eq-float") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (features (quote ("tuple"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3.1.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "196zlphcqblyr7rx1svghbl67yb4lgrjxifrjmklkhbczi3slysr") (rust-version "1.56")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.2.0") (deps (list (crate-dep (name "assert-eq-float") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (features (quote ("tuple"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3.1.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hxy4nl50n637pirwxzly203hqlxw7nn214fmym25kylf15j84di") (rust-version "1.56")))

(define-public crate-lazy-static-include-3 (crate (name "lazy-static-include") (vers "3.2.1") (deps (list (crate-dep (name "assert-eq-float") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "manifest-dir-macros") (req "^0.1.6") (features (quote ("tuple"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slash-formatter") (req "^3.1.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qv32xsmv89lyl9n4m98275lwms33v5bzwjw4hfl099y0zsp3xv7") (rust-version "1.60")))

(define-public crate-lazy-string-replace-0.1 (crate (name "lazy-string-replace") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.2") (default-features #t) (kind 0)))) (hash "00s0ghacpagwiqng33jmlxrchz0wk3aw8ji3whpcsaqkqmacbymn") (features (quote (("nightly") ("default"))))))

(define-public crate-lazy-string-replace-0.1 (crate (name "lazy-string-replace") (vers "0.1.1") (deps (list (crate-dep (name "memchr") (req "^2.2") (default-features #t) (kind 0)))) (hash "013iy5x01c0wsjrby4rjsjhlbkdcg4n3y5x9bkdld5p14ywzacb2") (features (quote (("nightly") ("default"))))))

(define-public crate-lazy-string-replace-0.1 (crate (name "lazy-string-replace") (vers "0.1.2") (deps (list (crate-dep (name "memchr") (req "^2.2") (default-features #t) (kind 0)))) (hash "0zp3qb3kc1p42sf0x9l0p11ya1dzsf5bm6i3vlndn3nrcvvpsnwa") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-lazy-string-replace-0.1 (crate (name "lazy-string-replace") (vers "0.1.3") (deps (list (crate-dep (name "memchr") (req "^2.2") (default-features #t) (kind 0)))) (hash "1gb0dk0vsfj50vflmmvjna5k6g84i1ckd506r5rzyk20jpcmb7ih") (features (quote (("nightly") ("default"))))))

(define-public crate-lazy-transform-0.1 (crate (name "lazy-transform") (vers "0.1.0") (hash "1ahzra9nwp5nabw6gq2w0ll5rpy0bc4ay17wi60kaasv9zjmayrg")))

(define-public crate-lazy-transform-0.1 (crate (name "lazy-transform") (vers "0.1.1") (hash "01dlzdk2dji5h70acl2b6aww78h1bp4fk221wg2671vi7izwm66p")))

(define-public crate-lazy-transform-str-0.0.1 (crate (name "lazy-transform-str") (vers "0.0.1") (deps (list (crate-dep (name "cervine") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "gnaw") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0b85nzf04v9j8ysz20r6gl3g5fppywcghwh6m2gppmlfp4fg39b2")))

(define-public crate-lazy-transform-str-0.0.2 (crate (name "lazy-transform-str") (vers "0.0.2") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "cervine") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "gnaw") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0qmjhsdcc2lmc7zij5yzbbmh17gn7f9ggaca4gv0iwkwk63pgfsg")))

(define-public crate-lazy-transform-str-0.0.3 (crate (name "lazy-transform-str") (vers "0.0.3") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "cervine") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "gnaw") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0qn1akn96df4j8y3p8nmf9kpxq9m3d8djvdrlk00dws2jynwnjxl")))

(define-public crate-lazy-transform-str-0.0.4 (crate (name "lazy-transform-str") (vers "0.0.4") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "cervine") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "gnaw") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0q16yb3kw6fcbi2n2bw8bm1n9c79zg5bw45wlnnfvx9f5gaiyyv8")))

(define-public crate-lazy-transform-str-0.0.5 (crate (name "lazy-transform-str") (vers "0.0.5") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "cervine") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "gnaw") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "00kb0n448br6ci0p9dp6r26ggfj58mwr51ihxfvnnc80gsa9xikk")))

(define-public crate-lazy-transform-str-0.0.6 (crate (name "lazy-transform-str") (vers "0.0.6") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "cervine") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "gnaw") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0zimvshm3k5x5lbrplbhdqc60zf08wkk1ik6krq560yswn6z1wnk")))

(define-public crate-lazy-wrap-0.1 (crate (name "lazy-wrap") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1iyqlyh9qbm5jindq87v2ba5n6yiaymda9fvk9jl38310an2y2vp")))

(define-public crate-lazy-wrap-0.2 (crate (name "lazy-wrap") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "13pkfd7m6mld7l0slcbms3s7hlbwp8sr8hvv0874zhnr062p9rjn")))

(define-public crate-lazy-wrap-0.3 (crate (name "lazy-wrap") (vers "0.3.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "12bd268ah15hhplaazr36n2fhmjgbxj77l3dj1y9zybvfs0s0r98")))

(define-public crate-lazy-wrap-0.3 (crate (name "lazy-wrap") (vers "0.3.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1wgpzyi090nf3fypj9jjch60nfvcbfvppc058sdjdv9b3yg5z4w3")))

(define-public crate-lazy-wrap-0.4 (crate (name "lazy-wrap") (vers "0.4.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "09hcg1ci5qlb634sj4vvgma011a541imgnm8d3348wf47h0nglwr")))

(define-public crate-lazy-wrap-0.4 (crate (name "lazy-wrap") (vers "0.4.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1qir2nf80rn6dm9i3g4sb9nw3mf4i9jda53fbbkaad3dqqzcypxh")))

(define-public crate-lazy_async_pool-0.1 (crate (name "lazy_async_pool") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qutex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qrkr5djvgi1f7s15ng72cgmgrp2hvg10dghq6r9fq72x7rng3ic")))

(define-public crate-lazy_async_pool-0.1 (crate (name "lazy_async_pool") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qutex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.2") (default-features #t) (kind 2)))) (hash "039xni9wwikmwpwiz4knkkbyw1j9w12lih4c9h5qr0lir136yl2q") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.1 (crate (name "lazy_async_pool") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qutex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.2") (default-features #t) (kind 2)))) (hash "0yjigf667zn3jzap2w0rvwj3s85f009rqj9r846majp8l5pnphmd") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.1 (crate (name "lazy_async_pool") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qutex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.2") (default-features #t) (kind 2)))) (hash "1klqqmmcmgigzshsfbplk14dmxq3b5hy4zsqqb1j42bsh90s6d56") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.1 (crate (name "lazy_async_pool") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qutex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.2") (default-features #t) (kind 2)))) (hash "1d8ijy3n3001936z828h5fmmxf3c354mx6zn0i6cp5g6wmwd75dv") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.2 (crate (name "lazy_async_pool") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.2") (default-features #t) (kind 2)))) (hash "07f6smiyb1a7dpkfjn56967c4yaj06jcgp126jv6715kqpq2cv71") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.2 (crate (name "lazy_async_pool") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.2") (default-features #t) (kind 2)))) (hash "1vycqjidbk618rzfxdqwfwnycvb7byzwm7l9y95jrfk0gaw66gjw") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.3 (crate (name "lazy_async_pool") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.5.2") (default-features #t) (kind 2)))) (hash "1007k0jxbswgxvrzwmlfc4m1znvyqs6a788j34a6wblab6bv6j9h") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.3 (crate (name "lazy_async_pool") (vers "0.3.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.5.5") (default-features #t) (kind 2)))) (hash "1dbgprzkgf8asg16aa79ak4m2wiaqjpkiyg6vykmzwpwbk6ihwlp") (features (quote (("timeout"))))))

(define-public crate-lazy_async_pool-0.3 (crate (name "lazy_async_pool") (vers "0.3.2") (deps (list (crate-dep (name "async-channel") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.5.5") (default-features #t) (kind 2)))) (hash "0b5zrgy9h6v9279mkhr75nps8w2a2kn1zs36yag6bpjn59r3iy9p")))

(define-public crate-lazy_async_pool-0.3 (crate (name "lazy_async_pool") (vers "0.3.3") (deps (list (crate-dep (name "async-channel") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.5.5") (default-features #t) (kind 2)))) (hash "115sckdpaibn6q8a1k7fzhl2v2fgddrjwrf16m773q3791flikq6")))

(define-public crate-lazy_async_promise-0.1 (crate (name "lazy_async_promise") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time"))) (default-features #t) (kind 0)))) (hash "080rbb1ggp3xihjl4hj82h2ds6nhvv34ck9ypascb4igfzjgwa2y") (yanked #t)))

(define-public crate-lazy_async_promise-0.1 (crate (name "lazy_async_promise") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time"))) (default-features #t) (kind 0)))) (hash "145l7jl1pfp81p17s8fp9j9hwdmbk74nwl11s6shsmqxcnjiglwv")))

(define-public crate-lazy_async_promise-0.1 (crate (name "lazy_async_promise") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time" "fs"))) (default-features #t) (kind 2)))) (hash "1x1hp3nnqq6d4mwl10aqhgr93gfxmlmxgay06kgvvci6c1sbcanh")))

(define-public crate-lazy_async_promise-0.2 (crate (name "lazy_async_promise") (vers "0.2.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time" "fs"))) (default-features #t) (kind 2)))) (hash "0y19c0s4lfd393aqsby0anihl44alwm53cq80mapliv5s34gy2ai")))

(define-public crate-lazy_async_promise-0.3 (crate (name "lazy_async_promise") (vers "0.3.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time" "fs"))) (default-features #t) (kind 2)))) (hash "0y4dwpmj08g384a0pz2r09l2v9bz1zxs1kyvrqpkm6n3qva3ssib")))

(define-public crate-lazy_async_promise-0.3 (crate (name "lazy_async_promise") (vers "0.3.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time" "fs"))) (default-features #t) (kind 2)))) (hash "0wvhai2vyib9bsjcq1fmah4mim3llasbwl7h6hkhpl8sbl7krrx1")))

(define-public crate-lazy_async_promise-0.4 (crate (name "lazy_async_promise") (vers "0.4.0-RC1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time" "fs"))) (default-features #t) (kind 2)))) (hash "1crfvgh0gwqdm8w50cp5ri90g8s4k6qmdy9331jr76fgz94pbcss")))

(define-public crate-lazy_async_promise-0.4 (crate (name "lazy_async_promise") (vers "0.4.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time" "fs"))) (default-features #t) (kind 2)))) (hash "09zg31l0fj2l0wki4rnbzyxlijbsp3g8y6z9qjnm4dzjhck5nigd")))

(define-public crate-lazy_async_promise-0.5 (crate (name "lazy_async_promise") (vers "0.5.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "sync" "time" "fs" "macros"))) (default-features #t) (kind 2)))) (hash "1s70kh2djjgps9q8375668xkyy7xssx51hiprjzci2jz1nzba8ql")))

(define-public crate-lazy_bencoding-0.1 (crate (name "lazy_bencoding") (vers "0.1.0") (hash "1wn27204b8d5z4w2za0jbjmrvm3vw1vihl9zm7wbhq8s3h54ag80")))

(define-public crate-lazy_bencoding-0.1 (crate (name "lazy_bencoding") (vers "0.1.1") (hash "18800s5masvzyck80a2jznkp977pfrf8q3s1scglg43vq127ah05")))

(define-public crate-lazy_cat-1 (crate (name "lazy_cat") (vers "1.0.0") (hash "01cral4qlpd8g78zmh3ph8xhrwfg7gaqarw1yy5cc5hr3hh0z5ik")))

(define-public crate-lazy_cat-1 (crate (name "lazy_cat") (vers "1.0.1") (hash "142c6rrybnmll05bhssr0xs98ci8ah0mqqwq7lx7w6yw69hfrvl1")))

(define-public crate-lazy_cat-1 (crate (name "lazy_cat") (vers "1.0.2") (hash "01w9sxjrdvk0i9abchsn59r6vr39x9miwbwd2lygdlg4mr45yywg")))

(define-public crate-lazy_concat-0.1 (crate (name "lazy_concat") (vers "0.1.2") (hash "1553bzfqrg0lbna1ipim32zz0m8rmx6qzq94vvvywriblxygkw12")))

(define-public crate-lazy_concat-0.1 (crate (name "lazy_concat") (vers "0.1.3") (hash "0pavi098jsqrln3369v2k0sy7jrrbnq9vrhdjwxv4bps9hznyf3p")))

(define-public crate-lazy_conf-0.1 (crate (name "lazy_conf") (vers "0.1.0") (hash "1rryjc5kx432n7j2c07yp26fc6paalznl03c7smw6y7mhhps7kdc")))

(define-public crate-lazy_conf-0.1 (crate (name "lazy_conf") (vers "0.1.1") (hash "13d5nd04g6yr2hfybijximnwp2wh21lgij2b9xdwnbmpqz56w7s4")))

(define-public crate-lazy_diamond_square-0.1 (crate (name "lazy_diamond_square") (vers "0.1.0") (deps (list (crate-dep (name "seahash") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1vksblg2qzazmlccsfnk40kr170qxs7ardrm2n0qsw94nc4rn8c9")))

(define-public crate-lazy_diamond_square-0.1 (crate (name "lazy_diamond_square") (vers "0.1.0-1") (deps (list (crate-dep (name "lds_simple_view") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5") (default-features #t) (kind 0)))) (hash "15hl573mycvky31x8rw7fzxrksx98bznklxf7k583a531r5q87h4") (yanked #t) (v 2) (features2 (quote (("simple_view" "dep:lds_simple_view"))))))

(define-public crate-lazy_diamond_square-0.1 (crate (name "lazy_diamond_square") (vers "0.1.1") (deps (list (crate-dep (name "lds_simple_view") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5") (default-features #t) (kind 0)))) (hash "05471n1c83mya5356fia23kvklymninbcxqsyvrvdw4znp8fpcij") (v 2) (features2 (quote (("simple_view" "dep:lds_simple_view"))))))

(define-public crate-lazy_diamond_square-0.1 (crate (name "lazy_diamond_square") (vers "0.1.1-2") (deps (list (crate-dep (name "lds_simple_view") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ihis7v500knrhikwi4xf0cp5q3pxf1s92b0qb9p1n5xn02jj9mz") (yanked #t) (v 2) (features2 (quote (("simple_view" "dep:lds_simple_view"))))))

(define-public crate-lazy_diamond_square-0.2 (crate (name "lazy_diamond_square") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5") (default-features #t) (kind 0)))) (hash "0809pchm3v4s9a48max5nf5kznmaxyxifxanlrkr8g7p3xdr3ach") (v 2) (features2 (quote (("simple_viewing" "dep:image"))))))

(define-public crate-lazy_diamond_square-1 (crate (name "lazy_diamond_square") (vers "1.0.0") (deps (list (crate-dep (name "image") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5") (default-features #t) (kind 0)))) (hash "14gbvkj26ps2rmr1ihf5bln6g5war07hqg2pb20swn1274yav03p") (v 2) (features2 (quote (("simple_viewing" "dep:image"))))))

(define-public crate-lazy_diamond_square-1 (crate (name "lazy_diamond_square") (vers "1.1.0") (deps (list (crate-dep (name "image") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5") (default-features #t) (kind 0)))) (hash "0pd0hqz1gq4j625fk2zvwy60nq21m32b8jmk4hbyqqxszksjf96f") (v 2) (features2 (quote (("simple_viewing" "dep:image"))))))

(define-public crate-lazy_errors-0.1 (crate (name "lazy_errors") (vers "0.1.0") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "1nycn3kzmpv9a76c948bbd59vkz1wvyxh86lhas0wk2kcw5d78cw") (features (quote (("std")))) (v 2) (features2 (quote (("eyre" "std" "dep:color-eyre"))))))

(define-public crate-lazy_errors-0.1 (crate (name "lazy_errors") (vers "0.1.1") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "1hkg6yr2ya7pwzsd87zc8csmdf62swbli600pb22a09wsrs0s9y1") (features (quote (("std")))) (v 2) (features2 (quote (("eyre" "std" "dep:color-eyre"))))))

(define-public crate-lazy_extern-0.1 (crate (name "lazy_extern") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ifqfh9m5wxw6drlpzxvkfjlcd9kmm9zrs954hqj39l75c6wa5is")))

(define-public crate-lazy_fn-1 (crate (name "lazy_fn") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y8c7sya5fsib30qwcymc0v4diks07m3wq0jp5mqhps6351fzi8n")))

(define-public crate-lazy_fn-1 (crate (name "lazy_fn") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09r19vhimdddsy9g7ssnscvgxbaa6d45md3dwj6jarcq1smc40dr")))

(define-public crate-lazy_fn-1 (crate (name "lazy_fn") (vers "1.0.2") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "00iswficj57234n1ky5aagxjs411lx4lxjglsk8gzhwiad5mkg6y")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.0.0") (hash "1czdsr5ripgd3v51592v9fanvpv2n3ndmr8kq4yng8b6rgql0ifw")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.1.0") (hash "1n5nfcnq45zgqjdxn65hbfgs3jkfsbrqpjyf196rqd353js28c2w")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.2.0") (hash "1splc2x720yp4vjf08qwlr3yd4bpf6hm0ajya7259v6yl2qm90w0")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.2.1") (hash "0lln8yj1gssbb358937fdjl0k88dpiv3riryjn8ljv0imqfjqayh")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.3.0") (hash "1fh1x5mry78cf1qri9qbwbhhn9kjk6b6z5yhpxjn691ysk4ky2dx")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.3.1") (hash "0bj3rw076b7xhldv8cmxf2w29ypkyf8p0pd3g2zvjvsqr8ghs8kz")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.4.0") (hash "1wbg89wq86paapv7zh5lnnk9lfy1qm0v1fm5ijxwyyc389dmvf60")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.5.0") (hash "1x8839l7y84dy1i4s478m6w5yax3xf2iql24yd61g9h10kq43b5v")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.6.0") (hash "19pdzphw2gk22a442pzc0xpmxjrslncrcbnzcb1mi16h937vlb27")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.7.0") (hash "110c9kmgsm9rgxlgfrg50qs56jqhai0si39h2g13kfv1jkj72z8n")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.7.1") (hash "0yji2fbf5d6sz4fkln2b89ksbhjm78b3d94j51qh5yzfq07ck2q3")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.7.2") (hash "0d8v5pib72c9q1lwgwq0shj57740p6bn7r4nswl8m4spq4qyz1b3")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.7.3") (hash "14386q3wfq7m528x78kk6ywh02rkj8ycqrb6q8ppvb30g441adfc")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.7.4") (hash "022hk3ay5ps06n4bpap8y9xiz7hbikik08l8a1px9fc80hvzmjqz")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.8.0") (deps (list (crate-dep (name "horrorshow") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "1g2imwcml6isr0nb9ibiawzby7giz7cpcyjzi1v1cia7dwp1fjyr") (yanked #t)))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.8.1") (deps (list (crate-dep (name "horrorshow") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "0js8b9i0b62rlcl4fw9piqzy1jjcqm74dx8xxyvh8kj3bzh8ay0w") (yanked #t)))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.8.2") (deps (list (crate-dep (name "horrorshow") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "0h25b0hm40sbl0z018453x4aalxzr73y4n9687mbihx19blfk8cj") (yanked #t)))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.8.3") (deps (list (crate-dep (name "horrorshow") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "13ysa9ricahj47fx9x0rp1gzw9v16slzgs40klfhyfqdlwqzv2gk")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.8.4") (deps (list (crate-dep (name "horrorshow") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "0ip79hmfc3xyjbb1j0l5avrp190csbg1x8s2alhcmayq4lvxz06v")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.9.0") (deps (list (crate-dep (name "horrorshow") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "0syz7aicq900jgd2l088m0adnjbaj56nfr8phlm071i03p1p08a8")))

(define-public crate-lazy_format-1 (crate (name "lazy_format") (vers "1.10.0") (deps (list (crate-dep (name "horrorshow") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "1hxxmyarfl700yd7ppkmvc74cdp9b8ckbyb48j9hcc6nkjz64mmh")))

(define-public crate-lazy_format-2 (crate (name "lazy_format") (vers "2.0.0") (hash "0kcmk470ia7mpi0as8j96vzn2lk9hj76nn7da6x69na7zwksy81c")))

(define-public crate-lazy_format-2 (crate (name "lazy_format") (vers "2.0.1") (hash "09ff9qaf6j938as6v332d8mibf75giw7c7w143d9r56fdib0nlg3")))

(define-public crate-lazy_format-2 (crate (name "lazy_format") (vers "2.0.2") (hash "10kkk96fknmknl0ha0lbhi0yh6pcbi3p4i8r726lpcqkxr4whg4q")))

(define-public crate-lazy_format-2 (crate (name "lazy_format") (vers "2.0.3") (hash "14m7zcfvfmf1ywa5p76vi841kcldyk6r4k6dyvaqwmvx52dyjyg4")))

(define-public crate-lazy_futuristic-1 (crate (name "lazy_futuristic") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0q7b57x0aq5cq4w7ww8b4b4b2r04il56cca5s0k9sk9r9ipc0pj4")))

(define-public crate-lazy_futuristic-1 (crate (name "lazy_futuristic") (vers "1.0.1") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "08q4frqx692krw00mv8jvkyvgfw4qa24q59x89xj3fvhkdhkk0ny")))

(define-public crate-lazy_futuristic-1 (crate (name "lazy_futuristic") (vers "1.0.2") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "171rn0n6dlsqznxb4q3xmd8id9dwiximwm90s51ihf1w7i04gjpx")))

(define-public crate-lazy_id-0.1 (crate (name "lazy_id") (vers "0.1.0") (hash "0qdr4x40vh9d0fb8mzr9g5pcvh3garhn0ja5mrg9ybmyqa8rm34h")))

(define-public crate-lazy_list-0.1 (crate (name "lazy_list") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "~1.19.0") (default-features #t) (kind 2)))) (hash "1lcpwrb77p0i1vqvzf4mj48j9qs5hqh137x5fnk9nakb1qfk4r2n")))

(define-public crate-lazy_list-0.1 (crate (name "lazy_list") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "~0.5.1") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "~1.19.0") (default-features #t) (kind 2)))) (hash "11ihhfwmkhd3rmam3gipaz5z2b84g9hxasikqpkpwahkgr6xp2hi")))

(define-public crate-lazy_mut-0.1 (crate (name "lazy_mut") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0nss35z5g1a5bysqbrcm9m597d7yavhhyv9arnlycm7da7m26ykn")))

(define-public crate-lazy_rc-0.1 (crate (name "lazy_rc") (vers "0.1.0") (hash "1dfqkzgyv846bix65qd49lz307ry0jfa2nar9nj93p6dhxwhgbla")))

(define-public crate-lazy_rc-0.1 (crate (name "lazy_rc") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0qn4r3lym8djvcnycc3a4a5bz2f1vz146bvdwydw7xgsia5776fj")))

(define-public crate-lazy_rc-0.1 (crate (name "lazy_rc") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1hfcsq96i1s9xj0gvl8sy254959ksd1091yzix5n1dwzdxyhifmb")))

(define-public crate-lazy_rc-0.1 (crate (name "lazy_rc") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "01dkwdlgyhi63kv3r83j7snx2d61ccn7aksyqlq3xx1rk0qb8y2l")))

(define-public crate-lazy_ref-0.1 (crate (name "lazy_ref") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "0fyby2238gng6wn0dw5mhgbbkjn7c96d736h5sqm1flxmgf8w0wx") (yanked #t)))

(define-public crate-lazy_ref-0.2 (crate (name "lazy_ref") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "06kx88zhdkaq5jx6zicvcp5ja3n3j0qlcp9q0i64px9wkd6sz0ga")))

(define-public crate-lazy_ref-0.3 (crate (name "lazy_ref") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "13jsmvhbpqlywyhpafnys52ss3dhrrlb9ss8954cdfhhrs2q2vvp")))

(define-public crate-lazy_ref-0.4 (crate (name "lazy_ref") (vers "0.4.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "0hz9jzmpl6h4r47cx2ajdkzij5vhc8ri6k9h4vsnvadyg6n6lcyx")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.0") (hash "0xr7qlk99kn7vf5p1gcmssfl60d26jy2zdji1v1i5v1pa534mpjh")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.1") (hash "1dfi7m5i3q8wbvyi6akp1g9wcjpy2yi0xf6zr8b714j67xq7kfjj")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.2") (hash "0l3b0xgx9j2kcbpwc483cliz912dlkps3y44bwj29nwg5kgk6v9b")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.3") (hash "1c63hlcplqivcjn1yr6dqvzla4acrn01bgrx1li4y30shh3v4kji")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.4") (hash "0l85qls2sxsz2lpazqnf208hbnyy90dgv48yy1b6h814kmnn37na")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.5") (hash "1fwsk0cd7d0kisvz7snfl9wvrgc8zdgc10gkq9dnhpyyxs59ak60")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.6") (hash "1h4nkp81znxhx2p1sxaj7my110vqfaqrvzagzcghm0ppankz4hww")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.7") (hash "1xj28ajdi4x033ka7gajna1ya3f7m6mk9k0lk3c34j6cdip4jq7c")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.8") (hash "1yhry89qdjjyp8l8vhd9jcw4qm1w866rajdqkrzi9rmd4kfa7qdq")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.9") (hash "0a16lmp3p0ns3xjalm53d853187lnqj76g0f9x2nsyx4mcj8zq3n")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.10") (hash "1xlv9wv1sbnr86xc4h5ljrdfxkgdq2bf6c7bafywdiw2s772c3h7")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.11") (hash "1l15npn688s93281qf16rsvavzs4yz9v5hdjadsx6ay3rd21p372")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.12") (hash "0yyjjax556g8d5dp4bfn6hnv7miqr7kqcl20qvfdyiibvnhf0i45")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.13") (hash "0ppv7hm3dyizw8mv6apbkdn4cya8l3yziqi6xdrw1br02wz1rv38")))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.14") (hash "0dj5cvfvs3svm7q2z9ry11l0mb8zb0s87mvjzbkikghc97f0fvj6") (features (quote (("nightly"))))))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.15") (hash "0jf4lsjrbmc40yg5s09kfabipc6g93357px6ywwspyq00hdx2zsl") (features (quote (("nightly"))))))

(define-public crate-lazy_static-0.1 (crate (name "lazy_static") (vers "0.1.16") (hash "05vl1h4b0iv800grsdyc3fg2bq29p70wjav6zpjvxxd5i8d6s66g") (features (quote (("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.0") (hash "0fwmvdx0fi2b8j1ib15f1alv9xhrqbari6di49j1pdim9d1ix7jg") (features (quote (("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.1") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0zzyvxi66zm9blfprrzw3cjyf9f0jd8w7nfb4g5kvfw5lb17w929") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.2") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "16z1h7w702sxnscak38jykxlhxq0b5ip4mndlb46pkaqwzi0xgka") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.3") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0yg6f2zf9ymplrksvl0scxsx2ch6mbr1ny7zdlrhywixq1blrrzp") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.4") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1p30qiivkkg1d43s10pj3g07zpriqbdxy0hbc99gfcfkjzfv34bj") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.5") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "15qsxv3rxp4hyd9lgbhi5442yx26gfm7sx64cmjl06m2p5iwacj7") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.6") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1cxb8g3ar37xapk1pbq5v9kpap6xdzaja5k1j71lhiks3i1bhq9g") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.7") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0kb1bdcxrvih26p50iix91q0zg7030a5wxsdf1klfdlwrai7wgm3") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.8") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1gsgx41jlpamhparl0mr2h434nqw4flan2j2qqz87p96nxd58drv") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.9") (deps (list (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1mgqfm1hgjy1id1089fagnwnmb0cxqi8m9v1llavkhx4l67ybrf9") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.10") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0mrdihpmvblbd921aa5bsl1cl1iyvvbn6xwvm10ll7arc9xb6vi3") (features (quote (("spin_no_std" "nightly" "spin") ("nightly") ("compiletest" "compiletest_rs"))))))

(define-public crate-lazy_static-0.2 (crate (name "lazy_static") (vers "0.2.11") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)))) (hash "0wxy8vak7jsx6r8gx475pjqpx11p2bfq4wvw6idmqi31mp3k7w3n") (features (quote (("spin_no_std" "nightly" "spin") ("nightly") ("compiletest" "compiletest_rs"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.0.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)))) (hash "0z9335v3jxxfwsmxl1r4hf0vbhzpyjfw4isbw6dz2rd3v93i1wy8") (features (quote (("spin_no_std" "nightly" "spin") ("nightly") ("compiletest" "compiletest_rs"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.0.1") (deps (list (crate-dep (name "spin") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)))) (hash "0f9pwzdlssfd1n4kz4qbm39ddk96409974wpiq5lnn6r59g2qhg6") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.0.2") (deps (list (crate-dep (name "spin") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)))) (hash "1lvdk5mkhw0im12fignd0i38miy2gyh5cjfrrwqs7dk2scspqjgv") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.1.0") (deps (list (crate-dep (name "spin") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.1.4") (default-features #t) (kind 1)))) (hash "19vincf2navflh6jlcysalbwyj78nc4mdfa5rlp0lyv5ln4qnj6a") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.2.0") (deps (list (crate-dep (name "spin") (req "^0.4.10") (features (quote ("once"))) (optional #t) (kind 0)))) (hash "18fy414dxmg92qjsh1dl045yrpnrc64f7hbl792ran5mkndwhx53") (features (quote (("spin_no_std" "spin") ("nightly"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.3.0") (deps (list (crate-dep (name "spin") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "052ac27w189hrf1j3hz7sga46rp84zl2hqnzyihxv78mgzr2jmxw") (features (quote (("spin_no_std" "spin"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.4.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0in6ikhw8mgl33wjv6q6xfrb5b9jr16q8ygjy803fay4zcisvaz2") (features (quote (("spin_no_std" "spin"))))))

(define-public crate-lazy_static-1 (crate (name "lazy_static") (vers "1.1.1") (deps (list (crate-dep (name "spin") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.1.4") (default-features #t) (kind 1)))) (hash "0az813wdll13lksglr7wj1lr5iiyvx4ya127rx4zpbhr69ldm7nq") (features (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_thread_local-0.1 (crate (name "lazy_thread_local") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_family=\"unix\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fibersapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "10f7cdnmlarv203wc11cfmhpyv4ydpma3a43y8zzrdik5khkk0n9") (yanked #t)))

(define-public crate-lazy_thread_local-0.1 (crate (name "lazy_thread_local") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_family=\"unix\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fibersapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0gjwjs0fv7spsvxhxz2096bc6cf4lfw6d082030s71xs3zgxl7iv")))

(define-public crate-lazy_thread_local-0.1 (crate (name "lazy_thread_local") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_family=\"unix\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fibersapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0vwhl20gapxd5r9y82b143dlh0v9nq6jgkphag6kmf63jgdbcqrr")))

(define-public crate-lazy_transducer-0.1 (crate (name "lazy_transducer") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.8") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)))) (hash "0d5l4bvzg6siwizwiw7dc8slb9k758ng115sfk0gkx5i597lg8lw")))

(define-public crate-lazy_transducer-0.1 (crate (name "lazy_transducer") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.8") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)))) (hash "1f9cmhzii0xs4w8301f80jwz2kf2f6c5xlh504ihhldck4jwal5m")))

(define-public crate-lazy_transducer-0.1 (crate (name "lazy_transducer") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.9") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)))) (hash "124cccmpvd3mxgla54l9irj4ld156bis8ryaa6cnysk6zl3w32i8") (features (quote (("use_scroll" "scroll") ("default_features"))))))

(define-public crate-lazy_transducer-0.2 (crate (name "lazy_transducer") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0g4ypxxrfhlg9ramk7sf87hjgsv9lqmfy4pqmh3v16wr85ihf5wk") (features (quote (("use_scroll" "scroll") ("default_features"))))))

(define-public crate-lazy_transducer-0.2 (crate (name "lazy_transducer") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0c75462mxnnhywccapmy3ah6a9xahpxgqh87yr9d7xnzjzlghbly") (features (quote (("use_scroll" "scroll") ("default_features"))))))

(define-public crate-lazy_type-0.1 (crate (name "lazy_type") (vers "0.1.0") (hash "1zi088yfr7wbjmxdjr5jdvxd00vyifnhy00sjfvicyj3j680l9m3")))

(define-public crate-lazy_type-0.1 (crate (name "lazy_type") (vers "0.1.1") (hash "0gncwgs6qdqjha9winfvnkwc2vsskc97ihqgzjjmim1qampimi97")))

(define-public crate-lazybox-0.1 (crate (name "lazybox") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "lazybox_rm") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mg00n2hav70v7ibkmnn0i6s9b255cb4b6vjfpna5izlggj0wyqy") (yanked #t)))

(define-public crate-lazybox_rm-0.1 (crate (name "lazybox_rm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml" "wrap_help"))) (default-features #t) (kind 0)))) (hash "09x70pfd57prwfdac25ss3dhhx9067bjm0xigwkqw0aq5jkimxgw") (yanked #t)))

(define-public crate-lazyc-0.5 (crate (name "lazyc") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1mik7scgpm1x56f76xrydb590b4vi233wy1c3729vs3ms6pyxaiq")))

(define-public crate-lazyc-0.5 (crate (name "lazyc") (vers "0.5.2") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "12s51a67f8y756gpmpc5i2sxhjzq47zl36sl2fymd8y9hqc068vp")))

(define-public crate-lazyc-0.5 (crate (name "lazyc") (vers "0.5.3") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1qk6sp3lm23hknslfq6ya823sb2pn9fnfsyqqi62asxdn2b2cwvp")))

(define-public crate-lazyc-0.5 (crate (name "lazyc") (vers "0.5.4") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0g4c30a35caa3blqyvh208l68r5n524i7my88x0k5n9w6kcsnsmi")))

(define-public crate-lazyc-0.6 (crate (name "lazyc") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0g5xc7hijclcbhw127jqd4490md3jqa91wj3rl79ij4ighi00b8i")))

(define-public crate-lazyc-0.6 (crate (name "lazyc") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0s762hnq83fhnv7h8zp705pbv4v303vqydg5cn5fq79s0zyxqdfm")))

(define-public crate-lazycc-0.1 (crate (name "lazycc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6") (default-features #t) (kind 0)))) (hash "0493ms4rpk1ndr8ggr727xdnlaar4s04f3jvv3z2ivhvdp39xrly")))

(define-public crate-lazycell-0.0.1 (crate (name "lazycell") (vers "0.0.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "096bba8bvdlx66ix3cb1j3mpkh4qpg22lrg2rzirr9qyj7vjji0m") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.1 (crate (name "lazycell") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0f3yb8jpcf5gw50v3b5fm6wkaqgf7qz1nnh11kbwn0whkbkkghwg") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.2 (crate (name "lazycell") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1bg3zhi17ffm40ysibbirmag8r8rscc9v3gjlbpiyn8qp2p0aj29") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.2 (crate (name "lazycell") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "02bcp63giw041hlprlg52bfr9rwpnvyphaxpfg29g6q4js4knnc3") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.3 (crate (name "lazycell") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1xnbbphgdmqhwhrssnvhq8fj82k6sydrj143081lvgwh91qkavz5") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.4 (crate (name "lazycell") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0szgqfl2aw18kn9cf6qqpxxkiw6x6hx9y4r3gklnxn1r8xn304nf") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.5 (crate (name "lazycell") (vers "0.5.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0cb3h771aqnq753krx6f6vpxw3nn41b8gaj24q6y7wqy5z1aaf7c") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.5 (crate (name "lazycell") (vers "0.5.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "16w5c75sb7xjica1ys6w8ndxvy001y52fjz722m07yqid1x5nn1v") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.6 (crate (name "lazycell") (vers "0.6.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vy0gxy28z8afkqhvgl6x895k72i1wsxarnmw4zlmvvhphwqiw56") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1 (crate (name "lazycell") (vers "1.0.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0l0yy7djpirza5w37slbilgcnxa9hgpkwrncb0chi5jw6v84hfnk") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1 (crate (name "lazycell") (vers "1.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zfg2c5550j72a3bsbcmkhlrnv0fx4mm1zm65g7szw1r3d0lqvg2") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1 (crate (name "lazycell") (vers "1.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1q41614v4nqg1xi2hzc2hpiklfz5f14wjby9rvzbwa43lwq4rfnx") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1 (crate (name "lazycell") (vers "1.2.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gvqycmpv7parc98i6y64ai7rvxrn1947z2a6maa02g4kvxdd55j") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1 (crate (name "lazycell") (vers "1.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0m8gw7dn30i0zjjpjdyf6pc16c34nl71lpv461mix50x3p70h3c3") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazychacha-0.1 (crate (name "lazychacha") (vers "0.1.1") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.1") (features (quote ("getrandom"))) (default-features #t) (kind 0)))) (hash "06jn28kjpzwndazgwxsqni4w3dh4pw4lf1wj2105b3vyl52mdm6x")))

(define-public crate-lazycli-0.1 (crate (name "lazycli") (vers "0.1.12") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.15") (default-features #t) (kind 0)) (crate-dep (name "ticker") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)))) (hash "07ypkirfn605xyvkwjr6wgf3wzlnn4wai3wjvl13ky0nykndlx3q")))

(define-public crate-lazycli-0.1 (crate (name "lazycli") (vers "0.1.13") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.15") (default-features #t) (kind 0)) (crate-dep (name "ticker") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)))) (hash "1ngayb6ql1jbl949qxiz3dkh7byl2qcbsd0pmb9dgcph8q9945ra")))

(define-public crate-lazycli-0.1 (crate (name "lazycli") (vers "0.1.14") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.15") (default-features #t) (kind 0)) (crate-dep (name "ticker") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)))) (hash "09v7p7hb21cmpkiy978i26fh20vn3va4hi5gmzi6qks521nxacik")))

(define-public crate-lazycli-0.1 (crate (name "lazycli") (vers "0.1.15") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.15") (default-features #t) (kind 0)) (crate-dep (name "ticker") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)))) (hash "0i5wfm73acaw5bir08d4xhk33qixkbrkw60667faf1z2aq50x9q0")))

(define-public crate-lazycrate-0.0.1 (crate (name "lazycrate") (vers "0.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1dq3wjrvd322xxva37ckp8gp907q12s9gwp4hhk6ximpdxyy0bhc")))

(define-public crate-lazyext-0.0.1 (crate (name "lazyext") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1iypdzaxq8l1fjmya33cri2din83a316jsw30a7qvr5iqcfrl8h0") (features (quote (("default"))))))

(define-public crate-lazyext-0.0.3 (crate (name "lazyext") (vers "0.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazyext-slice") (req "^0.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1zrr3n89k59b6qx0k53xa10mjjgkdggjwkwgnsbqzakgqqnzwx3w") (features (quote (("std" "lazyext-slice/std") ("full" "std") ("default" "full"))))))

(define-public crate-lazyext-slice-0.0.1 (crate (name "lazyext-slice") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "17mb141n7z5higmzjqcd53z463z5svhl1yri3cpgq2ldgdyz8lwi") (features (quote (("std" "alloc") ("full" "std" "bytes") ("default" "full") ("alloc"))))))

(define-public crate-lazyext-slice-0.0.2 (crate (name "lazyext-slice") (vers "0.0.2") (deps (list (crate-dep (name "bytes") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1zsgbsw9cbylnx0537kg1v1nk9zz0gmq5ds3k75jrmid84xikazc") (features (quote (("std" "alloc") ("full" "std" "bytes") ("default" "full") ("alloc"))))))

(define-public crate-lazyf-0.1 (crate (name "lazyf") (vers "0.1.0") (hash "0mnlyc0fgizwgnqdyxqpppxhlh0924ard9rw2gvn46gviyylmk30")))

(define-public crate-lazyf-0.1 (crate (name "lazyf") (vers "0.1.1") (hash "0ry4bnzvazm9dpaa6gjvn9m3j9zi5kr3bdxs32gygq4n0bv08dxv")))

(define-public crate-lazyf-0.1 (crate (name "lazyf") (vers "0.1.2") (hash "0afrx5i2bmrm6z3gch7sm90j1lfkfi2675h0w0qqagg2fsdy0aix")))

(define-public crate-lazyf-0.1 (crate (name "lazyf") (vers "0.1.3") (hash "1b2lgxzx3b2k5jgc47ridrji582bflgjn1cqgyv79xyy22pb3qlh")))

(define-public crate-lazyf-0.1 (crate (name "lazyf") (vers "0.1.4") (hash "0wnwszbkma0i12sv2wqalbl9mif1vbi42nwn1n7y0njw04cyf2v8")))

(define-public crate-lazyfunctions-0.1 (crate (name "lazyfunctions") (vers "0.1.0") (hash "0g5bl1f08qwpxfgr85nlbki2hp6v0s3kxza2kda7a7wwbrz23bh6")))

(define-public crate-lazyfunctions-0.1 (crate (name "lazyfunctions") (vers "0.1.1") (hash "0v17hiyw2yl34ssnf0rnsick5h17n2087hhb81hh2lcpgx69zwxg")))

(define-public crate-lazyfunctions-0.1 (crate (name "lazyfunctions") (vers "0.1.2") (hash "06jb9rfqilikjrbl5irvmxvysnyanmy9a9vcd62q80v8011i7fld")))

(define-public crate-lazygtd-0.0.0 (crate (name "lazygtd") (vers "0.0.0") (hash "0ag3ccgix805bnkh9wn36w8ayyb4yv4ngqwfqffxy38lafqwb2gm")))

(define-public crate-lazyivy-0.2 (crate (name "lazyivy") (vers "0.2.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0kq9kcb41m7n40ndlhk25awa3ssyi4v3qxy94l5ch6bmjhf5447w")))

(define-public crate-lazyivy-0.3 (crate (name "lazyivy") (vers "0.3.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0zrvg3lf6pvds3c9xx00xqab0q4phvifv2pqj4i16kj4i10ywdww")))

(define-public crate-lazyivy-0.3 (crate (name "lazyivy") (vers "0.3.1") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1m7p6kpkjsnk4va0ylm4x83c5h5a0q3l9l0qbrg2djs1xgnx6kw3")))

(define-public crate-lazyivy-0.4 (crate (name "lazyivy") (vers "0.4.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0dfv8hx75xyzqpvvb9swhzwlishvvh11ccypk9s1plpr99ln3la3")))

(define-public crate-lazyjj-0.1 (crate (name "lazyjj") (vers "0.1.0") (deps (list (crate-dep (name "ansi-to-tui") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.39.0") (features (quote ("filters"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.3") (features (quote ("serde" "unstable-rendered-line-info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.13") (default-features #t) (kind 0)) (crate-dep (name "tui-textarea") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tui_confirm_dialog") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1yp3ds6klwgxwr6xcw8cc7cd2zcf9220vq5xfxg0v76j0wfn7g87")))

(define-public crate-lazyjj-0.1 (crate (name "lazyjj") (vers "0.1.1") (deps (list (crate-dep (name "ansi-to-tui") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.39.0") (features (quote ("filters"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.3") (features (quote ("serde" "unstable-rendered-line-info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.13") (default-features #t) (kind 0)) (crate-dep (name "tui-textarea") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tui_confirm_dialog") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1nzk5bhlwxkv27ai2i4xkyais7y570y3xfafcv67lidcpc912wb3")))

(define-public crate-lazyjj-0.1 (crate (name "lazyjj") (vers "0.1.2") (deps (list (crate-dep (name "ansi-to-tui") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.39.0") (features (quote ("filters"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.3") (features (quote ("serde" "unstable-rendered-line-info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.13") (default-features #t) (kind 0)) (crate-dep (name "tui-textarea") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tui_confirm_dialog") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1qzf3lrji3ayn5fbhdryfb3f41q130j7dlna7hjky5ss7m4w6y0a")))

(define-public crate-lazyjj-0.1 (crate (name "lazyjj") (vers "0.1.3") (deps (list (crate-dep (name "ansi-to-tui") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.39.0") (features (quote ("filters"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.3") (features (quote ("serde" "unstable-rendered-line-info"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.13") (default-features #t) (kind 0)) (crate-dep (name "tui-textarea") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tui_confirm_dialog") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0cygv9k5h16d0s768viygxdhvra3lpwrxb6bf6yfygz2y7c6gh3b")))

(define-public crate-lazyk-rust-0.1 (crate (name "lazyk-rust") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)))) (hash "1ni0af17138irsnbxpisrhxp1jrcvdgkp02idnyi3j91zfz2wl5a")))

(define-public crate-lazylink-0.1 (crate (name "lazylink") (vers "0.1.0") (deps (list (crate-dep (name "lazylink-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0klgz4qzb92fiyhrr95r8l4ipwbkzvs381a37xp5jh8yfpj611qv")))

(define-public crate-lazylink-0.1 (crate (name "lazylink") (vers "0.1.1") (deps (list (crate-dep (name "lazylink-macro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "09bp7wc3486q5g95bynqcp7f0ilg9dqqfzv7bl860q1igxykk25z")))

(define-public crate-lazylink-0.1 (crate (name "lazylink") (vers "0.1.2") (deps (list (crate-dep (name "lazylink-macro") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0px3nxw79bwjrxc04wmzvmbqsanj71rsalyl52vcny9568a46y0g")))

(define-public crate-lazylink-macro-0.1 (crate (name "lazylink-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1x65lb9xglhg6865w13gi4bm264dh0hswy0agcz7hnc4w5hd4ysm")))

(define-public crate-lazylink-macro-0.1 (crate (name "lazylink-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0k5y47804fa32xh4zzsdgfrhcyjgj7iyqlbs8wf29rx010ac1r4s")))

(define-public crate-lazylink-macro-0.1 (crate (name "lazylink-macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0c9lgf9kr894h24x9w6rk7h057zsr2dfy964m071x1ar4dpc2rq3")))

(define-public crate-lazymail-0.0.0 (crate (name "lazymail") (vers "0.0.0") (hash "13b3h03ry2l0azf4lpi1i76w7hzjpl62xmf8zvm6pmmbjjcfink0")))

(define-public crate-lazyme-0.0.1 (crate (name "lazyme") (vers "0.0.1") (deps (list (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)))) (hash "03yh777d30x3ahqaa58p1dx3gm2g5yw7pq8b3cl3damsdmn84j42")))

(define-public crate-lazyme-0.0.2 (crate (name "lazyme") (vers "0.0.2") (deps (list (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)))) (hash "16kimyn1qqvm0lkdnzq801zk1qnxjm65f3m1whgyfsd8w30gpz2d")))

(define-public crate-lazyme-0.0.3 (crate (name "lazyme") (vers "0.0.3") (deps (list (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)))) (hash "1c3j9fmb81km72r8zq9nbgxxpc9nzlv7m7575dqa80vk9zjrw8nz")))

(define-public crate-lazyonce-0.2 (crate (name "lazyonce") (vers "0.2.0") (hash "18an8fbfd0sc84fccnlfq1vi47a3dls4zfwfc6lmy1089mdg8777") (yanked #t)))

(define-public crate-lazyonce-0.3 (crate (name "lazyonce") (vers "0.3.0") (hash "0zgz2zxahvnayhyz9g9jvbpigkv17zickcz0ay00jd10vg3f8gdm") (yanked #t)))

(define-public crate-lazyonce-1 (crate (name "lazyonce") (vers "1.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.9") (default-features #t) (kind 0)))) (hash "1w3d60v1cwj8ijbl1w7bnpjlzyar7xcbbyc2aib9rgqny785v1b9") (yanked #t)))

(define-public crate-lazyonce-2 (crate (name "lazyonce") (vers "2.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "03pqalk4gj6nmqcpm9dyivr09hqxzn9drdkl6jcqvklazhpmspfa") (yanked #t)))

(define-public crate-lazyonce-2 (crate (name "lazyonce") (vers "2.0.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1k0q9xjsmnhy9mf8s68qwnwam9sqwjijgf749gfwydh5fzihnib8") (yanked #t)))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "0600d0jcmdh43rda0p9x9rbv9yc4qj85n1xf22ik00mmfzy1j324")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "1fyl7lkmxbvj4ilrwnap1g01gsfxnp7r8dcympk9laknkb97g85g")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "194kv5dry4ksdp2zjd2qw0j05qr1ssb530bps86wmp4ix5n4pp03")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "0pfavz4mz9s1zybxw68skxvqw0ckxqh80k1lpqr1pinlb7kdk2pa")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "11ixyrgbl0b53db20r14wykiai1d55l0d8jwklzi22kgdwz25n6l")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.5") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "07mpcgwwg1pfigxnvkzw875mcf35h9z71n8zggl0g0jv8p09asx3")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.7") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "1jrkafvk8nfjgvr8z0zx4imvbiffiw5pqqh2l82kz1vd4wi6ssrl")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.8") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "13874bx3kj4gyq7hmgwncmyfkchglxarsgpd0hxj636pawphk5m2")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.9") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "1f3fvcj10y4v6l3x9vllvcb9y8q0b0sjb50rgf31rx4mjnbwcwnc")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.10") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0cwqkrbc8slz6pdkw5srpx2s0h2s7126c0ijkyc8w5j28lm4m5v0")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.11") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0rhzi9gx7w173v47fzjr0lxzarmans3fyhc2nbydkivr9z8sm8nq")))

(define-public crate-lazyrand-0.1 (crate (name "lazyrand") (vers "0.1.12") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1ynldn0xyv90zclv1vmxvfl4i22i9n7mc79y2fw7pm228jrc9fsx")))

(define-public crate-lazysort-0.0.1 (crate (name "lazysort") (vers "0.0.1") (hash "15l63vdqsmpwihqnd9jphmfxsd8s9cdwqn852p468k4ka5bk9nlf")))

(define-public crate-lazysort-0.0.2 (crate (name "lazysort") (vers "0.0.2") (hash "0h878gbhn8a8vc1zqrbmkl9d11c6zaxkasxxs84ipjfhi2jxnpzr")))

(define-public crate-lazysort-0.0.3 (crate (name "lazysort") (vers "0.0.3") (hash "0q3dff2fn76fgiq915baadi7rhhcg4glv26prz5qvrw8ck566qs8")))

(define-public crate-lazysort-0.0.4 (crate (name "lazysort") (vers "0.0.4") (hash "16f96cs1z085ywglg5yk54r21d2s513fwkbwx3xk0g52wfqivgfi")))

(define-public crate-lazysort-0.0.5 (crate (name "lazysort") (vers "0.0.5") (hash "12jm5nvs285irkhyxc6zjwjj2s2ym5bij62wimlgls8ww3xbyjr7")))

(define-public crate-lazysort-0.0.6 (crate (name "lazysort") (vers "0.0.6") (hash "0mdhfsinnw0dj8c7p57rnwhc3ia3jm7ry9fpzf3wrjlgaq9isb5q")))

(define-public crate-lazysort-0.0.7 (crate (name "lazysort") (vers "0.0.7") (hash "0zjr9sfkjrz92igy7prrr03nifhrhqj4amn65jk1mp2gbg19hy5w")))

(define-public crate-lazysort-0.0.8 (crate (name "lazysort") (vers "0.0.8") (hash "1ay4qq33zwy5a483pb7fb4fgmzy6wyp6zljfb8wmyv14zr0nrds2")))

(define-public crate-lazysort-0.0.9 (crate (name "lazysort") (vers "0.0.9") (hash "0lw6isfjs2iqwvp7lcz9bjbymi702hn0xmhckwgg1xp9dk89an6d")))

(define-public crate-lazysort-0.0.10 (crate (name "lazysort") (vers "0.0.10") (hash "013bdh0vbap39kzsvx3q9ahv4gr29ijgx1s98m00rhd1v5nnjx8r")))

(define-public crate-lazysort-0.0.11 (crate (name "lazysort") (vers "0.0.11") (hash "10pzkik9xc35r5i7jph7m59csz3mjb1c1b6r5vyndy6g01l4imqj")))

(define-public crate-lazysort-0.0.12 (crate (name "lazysort") (vers "0.0.12") (deps (list (crate-dep (name "rand") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1nwmpyrig5rhk8zm8dm8s4haq765mzic23wp0g150mni7xc9hq19")))

(define-public crate-lazysort-0.0.13 (crate (name "lazysort") (vers "0.0.13") (deps (list (crate-dep (name "rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0gsf60zpgd98paf9fp83gnm71vjbsgnjrnk4w7ds50zw1cjx4g47")))

(define-public crate-lazysort-0.0.14 (crate (name "lazysort") (vers "0.0.14") (deps (list (crate-dep (name "rand") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "16z1850p718gi06yshq8m6v7yd9fvxl6xhvqa6psw04hdba893y6")))

(define-public crate-lazysort-0.0.15 (crate (name "lazysort") (vers "0.0.15") (deps (list (crate-dep (name "rand") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1ilq6mj0ydvk8a83jym533rrlzp9g1i9nyhqbr6pndgc16s5i8cn")))

(define-public crate-lazysort-0.0.16 (crate (name "lazysort") (vers "0.0.16") (deps (list (crate-dep (name "rand") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1jc0wmhsxmlb00q39ssdsbcv36lr869z20pmgwrsjnh4z88f7r3p")))

(define-public crate-lazysort-0.1 (crate (name "lazysort") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0vz4kdwq37x6g58l6zinhw831a3948qgcbfkh6fyz12j0dvh1is8")))

(define-public crate-lazysort-0.1 (crate (name "lazysort") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fq04qmpy140ba3m9isyx667rqj8yfdfg2blibd03f3zpp259q4y")))

(define-public crate-lazysort-0.2 (crate (name "lazysort") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "02lhvv1w4wjwbv3kqmrrzxnsb4cblc88yjdm6b890h6izv6whghq") (features (quote (("nightly"))))))

(define-public crate-lazysort-0.2 (crate (name "lazysort") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req ">= 0.3, <= 0.5") (default-features #t) (kind 2)))) (hash "13qncky7xc2g450y15rkw45bjwxw7vjq8jfphwphw7i37gs2zqnh") (features (quote (("nightly"))))))

(define-public crate-lazytable-0.1 (crate (name "lazytable") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)))) (hash "1jp9zm1mqgbqfpvggql9qm7l6vkgfrchf46kxl11jb9k2ldqf1z7")))

(define-public crate-lazytest-0.1 (crate (name "lazytest") (vers "0.1.0") (hash "1a81kg9wwbsmx0sljpz0gxlj3c58fkc7j2165wh26fbxicwnaig2")))

(define-public crate-lazytest-0.1 (crate (name "lazytest") (vers "0.1.1") (hash "09s1f3nrargyypm3122vlgfgc6j3dhhqmh15fzl68pc5n4acsp4n")))

(define-public crate-lazytest-0.1 (crate (name "lazytest") (vers "0.1.2") (hash "17w7mgwrzbdk3dcmhy2l2mq4lih11kswq942a25ql1s1alyyplh0")))

(define-public crate-lazytry-0.1 (crate (name "lazytry") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "1yl07bjhjyqzsj35byqf9srhjmny85v0gr677cda9hwbjiwgnyvm")))

(define-public crate-lazyxchacha-0.1 (crate (name "lazyxchacha") (vers "0.1.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.1") (features (quote ("getrandom"))) (default-features #t) (kind 0)))) (hash "1fjb1kd9jyqpn4m7py1854jxmmy2ql941wq8vqs4cjs4vzsv5g45")))

(define-public crate-lazyxchacha-0.1 (crate (name "lazyxchacha") (vers "0.1.1") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.1") (features (quote ("getrandom"))) (default-features #t) (kind 0)))) (hash "158w0knjp2qq3xj65ga0nmnv59cva9xfyg256x991pymjzn5ac65")))

(define-public crate-lazyxml-0.0.1 (crate (name "lazyxml") (vers "0.0.1") (hash "0crv3x0gwa5dzjl0ra698w1h7b03hfq6689r122pl4k946aphlp1") (yanked #t)))

(define-public crate-lazyxml-0.0.2 (crate (name "lazyxml") (vers "0.0.2") (deps (list (crate-dep (name "memchr") (req ">=2.3.0, <3.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "14ki29r5za1r1yjmc9wdml26qshnkg9rff2l43gh4h867cyz4mm9") (features (quote (("use-memchr" "memchr") ("default" "use-memchr")))) (yanked #t)))

(define-public crate-lazyxml-0.0.3 (crate (name "lazyxml") (vers "0.0.3") (deps (list (crate-dep (name "memchr") (req ">=2.3.0, <3.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1dslvhqmhvvxk9gdf44bn9sdc3fjmamkrvc6jjvs64jpqd40zrm4") (features (quote (("use-memchr" "memchr") ("default" "use-memchr")))) (yanked #t)))

