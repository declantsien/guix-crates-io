(define-module (crates-io la ck) #:use-module (crates-io))

(define-public crate-lacking-0.0.1 (crate (name "lacking") (vers "0.0.1") (hash "1jlksbmzh9bax1gfd6b9cw4nf5zbp36wp2vznlyfr43q4ah5sgzz")))

(define-public crate-lacking-0.0.2 (crate (name "lacking") (vers "0.0.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51.0") (optional #t) (default-features #t) (kind 0)))) (hash "07xk2m0b11342p4rhp48wxqvfmqjlgxdplqw4gvaskc2xgx5x24d") (features (quote (("desktop" "glfw") ("default") ("browser"))))))

