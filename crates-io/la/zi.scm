(define-module (crates-io la zi) #:use-module (crates-io))

(define-public crate-lazing-0.1 (crate (name "lazing") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rc6k637xi3pzivj22d4rx288d691grjhs2rvzi8r7iyh7k9vvb3")))

(define-public crate-lazing-0.1 (crate (name "lazing") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17fxj24vsi1hyxzgghk4yjhlgcqcjl1sr9hdm6l8ypjb95sr567l")))

