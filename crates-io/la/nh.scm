(define-module (crates-io la nh) #:use-module (crates-io))

(define-public crate-lanhu-0.1 (crate (name "lanhu") (vers "0.1.0") (hash "1gqmm5bm3j50pcf862lcn3x7kyi6q6yl0hld4y6bnpz9c35jmbhs")))

(define-public crate-lanhuapp-0.1 (crate (name "lanhuapp") (vers "0.1.0") (hash "1pmzpncsl2b4sv3knpcf9a8sk3m9qginmjgd9lhy3xzj5w3ay0hi")))

