(define-module (crates-io la tk) #:use-module (crates-io))

(define-public crate-latke-0.0.0 (crate (name "latke") (vers "0.0.0") (deps (list (crate-dep (name "ahash") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-rc.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "topograph") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)))) (hash "0qdll380wf3p0cfnf1pf6h66vn39q2g5ybhndxv0a6i1nh73jvpx")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0xa76zab692gn8dpqnaq1bk5qp9hhn270sl80iywkm25sjsdkmqg")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0lly1l59j09qn6ng2y5kzjcx33148s0w0zlivkm3v8ifhpcz4f7s")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "00w21lwim8wp6wsqb4xcb1gq3qhcpc0drhlxkw1cpqhnkz11hs0y")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0yjzw7rsz78340w9nkibl787agx4v7jqlgysf634zn3rq2d6kran")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0ybxzyk252k9n7hp33bg7schs1gq7sp873kghk7qv876dy9n8fra")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1xyb6v180s8g11yv0dz9f0m0b61sbrcla4p1gfd1zrdngwx6bgah")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0h9d1qkgv7xsjdbf9w5x17y3rza5pvl7lqdkicp16kg2qvfikf3z")))

(define-public crate-latkerlo-jvotci-1 (crate (name "latkerlo-jvotci") (vers "1.0.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "19yi69czqigdcbzxqczw3308hrhky1py3dl48mh9bghgpz274cah")))

