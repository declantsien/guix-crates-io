(define-module (crates-io la iz) #:use-module (crates-io))

(define-public crate-laizy-0.1 (crate (name "laizy") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0i0r1070dn66yvd5nrnr28rq84qn2y0nj8mdk4gybr7m1907rszq") (features (quote (("nightly"))))))

