(define-module (crates-io la ny) #:use-module (crates-io))

(define-public crate-lanyard-0.1 (crate (name "lanyard") (vers "0.1.0") (hash "1b0902hsg6r71bl8bvjvis1r98m1xibd2ihyjvafq8mzvcrg9r7f") (features (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-lanyard-0.1 (crate (name "lanyard") (vers "0.1.1") (hash "1ff9m6pm2gfmwxxnpnzlg8db6knbabvf41nb6dzqf2bv6nhncgrh") (features (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-lanyard-0.1 (crate (name "lanyard") (vers "0.1.2") (hash "0rrp7cc2n8bcyad4r3wl7djfs7hdish8svwvsdr2dw1gqam5ma7a") (features (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

