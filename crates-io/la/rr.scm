(define-module (crates-io la rr) #:use-module (crates-io))

(define-public crate-larry-0.1 (crate (name "larry") (vers "0.1.0") (hash "00gjsjwlganiya651xcvalkf2fijz5z1rn5zwnmg9bxv6ikgk0a1")))

(define-public crate-larry-0.1 (crate (name "larry") (vers "0.1.1") (hash "19gah6phdnaa204ixj1c9p4x2clx6n8bcq50zvarffxq3ancg9gd")))

(define-public crate-larry-0.2 (crate (name "larry") (vers "0.2.0") (hash "1jdd903yq0k84z52z4336plshyd809v39qxgb2xkn0ml76rblrnd")))

(define-public crate-larry-0.3 (crate (name "larry") (vers "0.3.0") (hash "09wabg4pcjvpzmgbxcyz7bsax59g0vj9md30b1cjiq6yzqw7rvzn")))

(define-public crate-larry-0.3 (crate (name "larry") (vers "0.3.1") (hash "1cwx3aals7kmagz71gkm2bm0ay60y6vvk549id0gcijbafn0j2fn")))

