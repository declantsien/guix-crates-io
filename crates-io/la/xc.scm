(define-module (crates-io la xc) #:use-module (crates-io))

(define-public crate-laxcow-0.1 (crate (name "laxcow") (vers "0.1.0") (hash "1bq3dx990sh82zxfnksm0h3m2qdg063ggq6riszzr0dgdwmyaf53")))

(define-public crate-laxcow-0.1 (crate (name "laxcow") (vers "0.1.1") (hash "0x8b27hld1xa2ib1phiy1w36rvf6r0i1xv9rsjcp8y22gvr7hhqx")))

(define-public crate-laxcow-0.1 (crate (name "laxcow") (vers "0.1.2") (hash "0v50qc6989ka15yk16qyyp6bfipabgj9k8izw5hjv281k4f54w4v")))

(define-public crate-laxcow-0.2 (crate (name "laxcow") (vers "0.2.0") (hash "1rdw8ghw0pfwkaraq9as29k4jd8zy35nwk827npgbyk6xaf25ycn") (features (quote (("default" "alloc") ("alloc"))))))

