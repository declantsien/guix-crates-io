(define-module (crates-io la pp) #:use-module (crates-io))

(define-public crate-lapp-0.1 (crate (name "lapp") (vers "0.1.0") (deps (list (crate-dep (name "scanlex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b7br1q6kzavh76a418wmvs9mjs0zl3nkibhzgzwc4cbs8xm4b89")))

(define-public crate-lapp-0.2 (crate (name "lapp") (vers "0.2.1") (hash "0ri46ali39k65mr78dx02gjwaqgyxi8yfcva1zbnyjvfxjz2bjmp")))

(define-public crate-lapp-0.2 (crate (name "lapp") (vers "0.2.2") (hash "000q8kaqly5qqmimcij6gk3wv12r8qa12qs0r41lz1iayim6binh")))

(define-public crate-lapp-0.3 (crate (name "lapp") (vers "0.3.0") (hash "1ksw2chsn55ycyk51x2f488c0628kcrrchq0lximycsssc6mlapr")))

(define-public crate-lapp-0.3 (crate (name "lapp") (vers "0.3.1") (hash "1a5f1xk6ksv6l4r8jwxm100m6a2i9likz0rzmkfnv2zqyrlqcxa4")))

(define-public crate-lapp-0.4 (crate (name "lapp") (vers "0.4.0") (hash "0acqmpdw1s2vnpzwxzp7a229gwh37cwajaddfni3g55szrd4igv0")))

