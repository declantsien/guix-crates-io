(define-module (crates-io la it) #:use-module (crates-io))

(define-public crate-laithean-0.1 (crate (name "laithean") (vers "0.1.0") (hash "0gbwiza2p13qbnk75csy9r1p8xr48w84js0f04048d2c8hg2khch")))

(define-public crate-laithean-0.2 (crate (name "laithean") (vers "0.2.0") (hash "0ivkrcgay6z02cy1cicrphz58hmab2yki3hryihvpmcvjcb67fgp")))

