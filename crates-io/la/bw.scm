(define-module (crates-io la bw) #:use-module (crates-io))

(define-public crate-labware_tracker-0.1 (crate (name "labware_tracker") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kqldl14vy4iw58hnmdyqfg92ybafpfwrxamfkv5dp9127bbls75")))

