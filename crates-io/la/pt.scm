(define-module (crates-io la pt) #:use-module (crates-io))

(define-public crate-laptimer-0.1 (crate (name "laptimer") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1j166ngc47frmjn7laa2fiv2n519lz85pz6qm12d6vj6d32hnngy")))

