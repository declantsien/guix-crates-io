(define-module (crates-io la pl) #:use-module (crates-io))

(define-public crate-laplade-0.1 (crate (name "laplade") (vers "0.1.0") (hash "1q9a01x9cnh03j64awbqg8ib0cjqq8fn536a2zmy33m489da5qhp")))

(define-public crate-laplade-0.1 (crate (name "laplade") (vers "0.1.1") (hash "0l7pr9p95d8bq063ziaafi895nychk4f6lbk5dianad9gqc5mdn5")))

(define-public crate-laplade-0.1 (crate (name "laplade") (vers "0.1.2") (hash "05l46xypvlqmlb8m9il1m77f34w860hapj3zf6z22xpn6ja328cr")))

(define-public crate-laplas-0.0.0 (crate (name "laplas") (vers "0.0.0") (hash "0dyz769di0d231r1pwnc6n5yaicr3j6al07rqi06p21kmikf7zrd")))

