(define-module (crates-io la rg) #:use-module (crates-io))

(define-public crate-large-primes-0.1 (crate (name "large-primes") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("release_max_level_off"))) (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0bx3xmm30g0lng50g6haqw48j4yc194x77yyi3yk788aks1s0b8b")))

(define-public crate-large-primes-0.2 (crate (name "large-primes") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("release_max_level_off"))) (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0v72y2iw0vgghayjsvkwp285z1d08v9k4572fdp8czznkm3dg3by")))

(define-public crate-large-primes-0.3 (crate (name "large-primes") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("release_max_level_off"))) (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "01g2p7c02znb2f47drpyqx16iq94zmms7nzf6pdci63v93z3qpx6")))

(define-public crate-large-primes-0.4 (crate (name "large-primes") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("release_max_level_off"))) (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "074x89lbj77b8d5hfyk7qx1z41cna1k5cam438dr3krz20aqqjfb")))

(define-public crate-large-primes-0.5 (crate (name "large-primes") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("release_max_level_off"))) (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0vf0c369mx3060jk7d3gwlr7jwlpwr0dscc6dx6xm9hf4xaz1mmx")))

(define-public crate-large-primes-0.5 (crate (name "large-primes") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("release_max_level_off"))) (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1snj7fmh0raj99bbbc9s4iaxw6rih1y1yncs1f96q12cl9nd5wfh")))

(define-public crate-large_int-1 (crate (name "large_int") (vers "1.0.0") (hash "1zzj3byaf6ixcibp2na9br343avwps1bxnbjbvhhikmbbn6y7lha") (yanked #t)))

(define-public crate-large_int-0.1 (crate (name "large_int") (vers "0.1.0") (hash "0zzsxyq1i39rkkzfnzjrsynb86vbv6y54wmnicyfdkqhlp9jii74")))

(define-public crate-large_int-0.2 (crate (name "large_int") (vers "0.2.0") (hash "02il1sl0fqsixpyr5mz3vcq85ypcdgp285wm9xx9aa589mppf5dd")))

(define-public crate-large_int-0.2 (crate (name "large_int") (vers "0.2.1") (hash "0z5ypssg1ibgx4l8am713kcy9yz43nhd35npz3vmn71kv8y6dcnd")))

(define-public crate-large_int-0.2 (crate (name "large_int") (vers "0.2.2") (hash "1jq2ycb33j9fc23mkkgbiibb550x8463ar5za4xswwvgdk9m79b9")))

(define-public crate-largeint-0.1 (crate (name "largeint") (vers "0.1.0") (hash "07rk87v5aarm4xj353ydc8nifvknxbl3plw5l4v14zf7qb8pxxn0")))

(define-public crate-largeint-0.2 (crate (name "largeint") (vers "0.2.0") (hash "0z03l9piqv1vsg29n1x4k0c8mgjsn9vk3c0hjmiaqhighq6dirlj")))

(define-public crate-largest-remainder-method-0.1 (crate (name "largest-remainder-method") (vers "0.1.0") (hash "0mrcrwvj7bl66d337m4z4xc9k7llal2cvhnzv4zlmlf1j6l83jfq")))

(define-public crate-largest-remainder-method-0.1 (crate (name "largest-remainder-method") (vers "0.1.1") (hash "070xc5faq6387yawma2klnwqd2bwn3wvvm0yr8rqnyks6x7vaswj")))

(define-public crate-largo-0.0.1 (crate (name "largo") (vers "0.0.1") (hash "1g3l30caa00n67cnlzcrw83c507wdmynaq28fgpp2nrby3h1sj69")))

(define-public crate-largo-0.0.2 (crate (name "largo") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "0.8.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.3.*") (default-features #t) (kind 0)))) (hash "1f3wbfklz0malm0zbi2iha8kkv18x7m4ga6l4zqlpwy2kh8kzrcj")))

