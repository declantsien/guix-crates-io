(define-module (crates-io la li) #:use-module (crates-io))

(define-public crate-lalits_guessing_game-0.1 (crate (name "lalits_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0widkiqrck29pk6h52996sllsm62pvjxgs20d7y2wmz39wshc5jn")))

