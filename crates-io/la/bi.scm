(define-module (crates-io la bi) #:use-module (crates-io))

(define-public crate-labirust-0.1 (crate (name "labirust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0n2c9wjzp2ynfrk0xmm558dggh7336a3qsykqyyvwajlfhd46mv9")))

(define-public crate-labirust-0.1 (crate (name "labirust") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "098791jjh9189k6g495v2frsg586q3v5ba16vg474209ddknqnyx")))

(define-public crate-labirust-0.2 (crate (name "labirust") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1xvwr0xxb5j1n07z3bbz08zj6b6314s780j58x2l5g1w1pl3344c")))

(define-public crate-labirust-0.2 (crate (name "labirust") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0l6h14bfpz3wqld345za53mgpr8m1vx5kgdnc46f1qg6sr761w45")))

(define-public crate-labirust-0.3 (crate (name "labirust") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "134z6mcdngfz265v73vqq0sshaiwnjipai6bbbxd37hqikpyd2dy")))

(define-public crate-labirust-cli-0.1 (crate (name "labirust-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "labirust") (req "^0.3") (default-features #t) (kind 0)))) (hash "1v4f4qchk207dcm7a3qsc7xp11fzdl90jm3vzsqv4p64pabwpg3i")))

(define-public crate-labisu-0.1 (crate (name "labisu") (vers "0.1.0") (deps (list (crate-dep (name "normalize_url") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r7mqxszjrzv76s417padjwsq4892bbfp5ms1qkyrmq0bqm7gr8y")))

(define-public crate-labisu-0.1 (crate (name "labisu") (vers "0.1.1") (deps (list (crate-dep (name "normalize_url") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gbirf6ssikvjdb009rqipd9bcdkvriwxcd1djnacvm3mpayds35")))

