(define-module (crates-io la wr) #:use-module (crates-io))

(define-public crate-lawrencium-0.0.0 (crate (name "lawrencium") (vers "0.0.0") (hash "16ns1bshhs1lh51q8gzns2rx3mlaqmxcpn4km8mnbsamzjcspr39") (features (quote (("struct_hash") ("struct_eq") ("struct_default") ("struct_debug"))))))

(define-public crate-lawrencium-1 (crate (name "lawrencium") (vers "1.0.0") (hash "0313f59sp5knng9m1cj4smvzs6wzaw7fkqz8q0d3d55f85dc4x9i") (features (quote (("struct_hash") ("struct_eq") ("struct_default") ("struct_debug"))))))

