(define-module (crates-io la bs) #:use-module (crates-io))

(define-public crate-labs-oneshot-0.1 (crate (name "labs-oneshot") (vers "0.1.1") (hash "1yv0lizgqwi9anykd357q6i8hm69lrk03lhzsc7mdi4hqxg1q57i") (yanked #t)))

(define-public crate-labs-oneshot-0.1 (crate (name "labs-oneshot") (vers "0.1.2") (hash "0yzycrcq9yvabzklpysskks0n6qms1my28wj6kxv4r5i7x9dwy6w") (yanked #t)))

(define-public crate-labs-oneshot-0.1 (crate (name "labs-oneshot") (vers "0.1.3") (hash "13879cj0b88zhpj17kvbdrcda3scccz8i7v961cxawxp8mnsqhsc") (yanked #t)))

(define-public crate-labs-oneshot-0.1 (crate (name "labs-oneshot") (vers "0.1.4") (hash "0wcf45gnjva6sqklagxhbk6xljhzmf6vvx1shqg1iqbzcn4qc2fv") (yanked #t)))

(define-public crate-labs-oneshot-0.1 (crate (name "labs-oneshot") (vers "0.1.5") (hash "1i7yc35qdbzsc1kbjhm3dg0yg7yzvq1ahlbvm6yvir906qnh58pj") (yanked #t)))

