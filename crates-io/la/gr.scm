(define-module (crates-io la gr) #:use-module (crates-io))

(define-public crate-lagrange-interpolation-0.1 (crate (name "lagrange-interpolation") (vers "0.1.0") (hash "1rh64dc1ca5gv3swp4va3cl0zh4sfhmfcc9aq1ljr6xk60wkx3kp") (yanked #t)))

(define-public crate-lagrange-interpolation-1 (crate (name "lagrange-interpolation") (vers "1.0.0") (hash "0cgrkk649l7kqk4hc91p15zac4n68i3cb4gfr5mi2mf8c9ndrpzg")))

(define-public crate-lagrangian_interpolation-0.1 (crate (name "lagrangian_interpolation") (vers "0.1.0") (hash "1fk0liq4wq7x8kd88l86b1xr40y3fg8hkzd7ijkv9ahcs4nlw4ai")))

(define-public crate-lagrangian_interpolation-0.1 (crate (name "lagrangian_interpolation") (vers "0.1.1") (hash "1ch5rq1c8gs924rjnapsx3j6b4vkr2x3l095vybph8psh63gl4pq")))

(define-public crate-lagraph-0.1 (crate (name "lagraph") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1ybzk4vlrdnzpag9n6shrwhf24m7dabbambcj9ql6ravqbal4gsi")))

(define-public crate-lagraph-0.2 (crate (name "lagraph") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0fhqs53xlyqhakdh1ycc1h2vaz61x0bxnc5haignddbzwlmjkapc")))

(define-public crate-lagraph-0.2 (crate (name "lagraph") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1f6kwmx4lliydf10v6jd6xqs695lhnk2xmv0fcpk25497hj4lbkq")))

