(define-module (crates-io la sa) #:use-module (crates-io))

(define-public crate-lasagna-0.0.1 (crate (name "lasagna") (vers "0.0.1") (hash "0q6zw5lpppi3lxjznccsffwbcwnjmg1spkd525ajdh3xv7qw80ai")))

(define-public crate-lasagna-buffer-pool-0.0.0 (crate (name "lasagna-buffer-pool") (vers "0.0.0") (hash "0n6bilm4i2rarr5mxa26jjrbnbx7qhbhaygfl3kb2v6zilhhqm48")))

(define-public crate-lasagne-0.0.1 (crate (name "lasagne") (vers "0.0.1") (hash "12yvz286bgcddn41jw0nk78ch51pfssxbyq1wv3h4vasm26smf2n")))

(define-public crate-lasanga-0.0.1 (crate (name "lasanga") (vers "0.0.1") (hash "1qq9v5w7dimmydi2k6lgc5bi72rvxdi8syy9gsx2gqwp3gixwpaf")))

(define-public crate-lasange-0.0.1 (crate (name "lasange") (vers "0.0.1") (hash "0b096d1rh1y9d1f39ivr6dard6hj6agc1jazrgxjvacm47h3f4xx")))

