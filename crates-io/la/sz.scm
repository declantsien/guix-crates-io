(define-module (crates-io la sz) #:use-module (crates-io))

(define-public crate-laszip-sys-0.1 (crate (name "laszip-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.30") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^2.2") (default-features #t) (kind 2)))) (hash "12d4c7mn4lwy90jx8jhw885arh23zf7skkpmc427v5br2xsgg4y3")))

(define-public crate-laszip-sys-0.1 (crate (name "laszip-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0rkh619s0ihwb1c6arzcx6axmpfwqbcvaqhz915ymv0jyvii56ah") (links "laszip")))

(define-public crate-laszip-sys-0.1 (crate (name "laszip-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "17113y14pf21h0aipjkknh1mmxk9kmxdbpbyfpk7w0wfyj5p72si") (links "laszip")))

