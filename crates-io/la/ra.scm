(define-module (crates-io la ra) #:use-module (crates-io))

(define-public crate-lara-0.1 (crate (name "lara") (vers "0.1.0") (deps (list (crate-dep (name "blas-src") (req "^0.2.1") (features (quote ("intel-mkl"))) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.12") (features (quote ("blas"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.10") (features (quote ("intel-mkl"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-parallel") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "10bj64rw5bqvzj2yy50d4h4kwvlmlk90i28pskjq2kmidcbq7d63")))

(define-public crate-laravel-0.1 (crate (name "laravel") (vers "0.1.0") (hash "1q37fny9pcb4r25qkkmycab81n14k7mzpa45kn0p9k7zhbqngvmd")))

(define-public crate-laravel-0.1 (crate (name "laravel") (vers "0.1.1") (deps (list (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0s8wk6cl9v1ymd1mdsfn04lwzikzf4g6dm6jjh53aqzg45dgkdaa")))

(define-public crate-laravel-0.1 (crate (name "laravel") (vers "0.1.2") (deps (list (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0fwgib4i330n0dmpzjj6winblyx7997lnjrpv43qwhwni5x3gfj8")))

(define-public crate-laravel-0.1 (crate (name "laravel") (vers "0.1.3") (deps (list (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0jm10x45fasxj562699k5s68s0mb224xpszbfmkwkqg6h8vk478w")))

(define-public crate-laravel-0.1 (crate (name "laravel") (vers "0.1.4") (hash "1abgcmj1bkaqixvlkibhrx02b7ifplxgkia3ymf3jq4qdhs8lfps")))

