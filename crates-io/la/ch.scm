(define-module (crates-io la ch) #:use-module (crates-io))

(define-public crate-lachesis-1 (crate (name "lachesis") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fjfh252ihy9gakgxlj57rknvci3sghdcx4qcp2pbkky21bxzrfy")))

(define-public crate-lachesis-1 (crate (name "lachesis") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fs8w61k7hq46nqikc9q9pj6rxlf4np3sja21w1jrzdyzhgcgvvy")))

(define-public crate-lachesis-1 (crate (name "lachesis") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bnfyh0cz9igfbdm1xxdpg6r69pn868nvqpjaxm5nxsl3md4c4rs")))

