(define-module (crates-io la sy) #:use-module (crates-io))

(define-public crate-lasy-0.1 (crate (name "lasy") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ether-dream") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jfjciq0ix9s4z2qrjjiybdd5yh8zd324v7i43g4zc4rsd0xd69w")))

(define-public crate-lasy-0.2 (crate (name "lasy") (vers "0.2.0") (deps (list (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ether-dream") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "14x433znwfwg77c2wbn6rxrgnp9vglhgxqa1rf6jj8b5c6hqm9yp")))

(define-public crate-lasy-0.2 (crate (name "lasy") (vers "0.2.1") (deps (list (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ether-dream") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d5zg82xdb1xxd7i5r55485fx7mwh5y81310yf264xv91p8q8a4w")))

(define-public crate-lasy-0.2 (crate (name "lasy") (vers "0.2.2") (deps (list (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ether-dream") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mmqg450m4wx61f5dbsb9cy524j032h1nahm775x48ai546z45vn")))

(define-public crate-lasy-0.2 (crate (name "lasy") (vers "0.2.3") (deps (list (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ether-dream") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "0axngb2gwh0rw4ina3cd95yjsmcpfrycnr071nzkqbbp6yz77xr2")))

(define-public crate-lasy-0.2 (crate (name "lasy") (vers "0.2.4") (deps (list (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ether-dream") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mhgvc3gvavvb2sfdfsd6r221rhwgyxplrl73qjnhir9ddib7yda")))

(define-public crate-lasy-0.3 (crate (name "lasy") (vers "0.3.0") (deps (list (crate-dep (name "petgraph") (req "^0.5") (kind 0)))) (hash "04l7ap6244lpzz05wdfry3andm63iaahs4ajc7s3qhchy14z00qz")))

(define-public crate-lasy-0.4 (crate (name "lasy") (vers "0.4.0") (deps (list (crate-dep (name "petgraph") (req "^0.5") (kind 0)))) (hash "1mqgi63raz0c89r7xf8pvpv2xmkflbrajah2p2i3h9ibgi9jafw5")))

(define-public crate-lasy-0.4 (crate (name "lasy") (vers "0.4.1") (deps (list (crate-dep (name "petgraph") (req "^0.5") (kind 0)))) (hash "0lmgqvd1vpj356bpn0g8akyprzgrmf32zjwpp84mx98hacg1vmxx")))

