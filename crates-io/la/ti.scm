(define-module (crates-io la ti) #:use-module (crates-io))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.0") (hash "035b7c4bsnhxm4kssd5b38hz16iyzws10zzhwnnd7n5d8s16sk3w")))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.1") (hash "1w4j3h7b2myr5zz4wwma2z0y1qbn4ln63xdz635wbrgj54v11f09")))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.2") (hash "0b2mjq9nrzyzn3hx2sq0z75f2pbl3ga8qcfyxkj2zvf0rn21nsnm")))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.3") (hash "1mphginfs0h43lqg319c2bk6717aqpbf95kyngpg856bc5l6za6y")))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.4") (hash "0kanwhpw55if7q1bc20pxxx34c5h551p9py22q7bgbvibwgh4k7h")))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.5") (hash "0mp737041ndym73arw42pm235afb186q6j1n0j4xlx1gyfc1hsxx")))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.6") (hash "0fyja08jhmm5bycbb4di8h4xavl5kmyfgb42rzzd1pw6vryps1x5")))

(define-public crate-latin-0.1 (crate (name "latin") (vers "0.1.7") (hash "0psrs4rklhywj4rjsx9pbry4rpphpafx9i5p7sjm18acg6vrv7jx")))

(define-public crate-latin1str-0.1 (crate (name "latin1str") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "02x6cx0f0dzggvbmlfr4sqzqis4rrsrwpkxyhisvmq4hz1hhqxqq")))

(define-public crate-latin1str-0.1 (crate (name "latin1str") (vers "0.1.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "011qxp860xnrsmdyblnj4ckisc8857a4b75ihzq2lc3my9m9k9qb") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-latin1str-0.1 (crate (name "latin1str") (vers "0.1.2") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "02z1rgp2sa2ygxvjsplslnj2j54yfdfidrcr2vgxa853xi4np4g5") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-latin1str-0.1 (crate (name "latin1str") (vers "0.1.3") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mzhn5n95v82idxsw8qz8pvmbqmrjz8wnfkz72gsjdrpy76vvn3w") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-latin_square-0.1 (crate (name "latin_square") (vers "0.1.0") (deps (list (crate-dep (name "moon_math") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "moon_stats") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0sczwm31v5sqrldx7l05qfdygzbsxd79hwi6m3wlw06h6iz8kjhv") (yanked #t)))

(define-public crate-latin_squares-0.1 (crate (name "latin_squares") (vers "0.1.0") (deps (list (crate-dep (name "moon_math") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "moon_stats") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "007h59qv58f93xdrs3ikl5dwx6l5ygqxas31b74i5x5kg5x3lk1f")))

(define-public crate-latin_squares-0.1 (crate (name "latin_squares") (vers "0.1.1") (deps (list (crate-dep (name "moon_math") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "moon_stats") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08sk80gyvas8hk3854wd1nlfaqakfc9pypdrmzgq2ykm2yl017sy")))

(define-public crate-latin_squares-0.1 (crate (name "latin_squares") (vers "0.1.2") (deps (list (crate-dep (name "moon_math") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "moon_stats") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0yqsv77d1xjv8y4mvd6pjjz7618csv88qv5dbdkk6hmlq20c0f0i")))

(define-public crate-latinify-1 (crate (name "latinify") (vers "1.0.0") (deps (list (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "0d6fhwzddmf0r0mcryxgmvrk6wpz9kkj4dmx9mf0p6v1g4za6gys")))

(define-public crate-latitude-0.0.1 (crate (name "latitude") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "barrel") (req "^0.6") (features (quote ("sqlite3" "mysql"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.4.0-beta.1") (features (quote ("runtime-tokio" "macros" "any" "mysql" "chrono" "decimal" "sqlite"))) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (default-features #t) (kind 2)))) (hash "18va73j19i4mg1dgfikz0nbl58sis4fx6scqdij65256ghz8ldkf")))

(define-public crate-latitude-0.0.2 (crate (name "latitude") (vers "0.0.2") (deps (list (crate-dep (name "async-trait") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "barrel") (req ">=0.6.0, <0.7.0") (features (quote ("sqlite3" "mysql"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req ">=0.4.1, <0.5.0") (features (quote ("runtime-tokio-rustls" "macros" "any" "mysql" "chrono" "decimal" "sqlite" "migrate"))) (kind 0)) (crate-dep (name "thiserror") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 2)))) (hash "0ijczvwznpm17v2x8ld43l78d6rpqv5ivbbgvsj4giq25y6653s2")))

(define-public crate-latitude-0.0.3 (crate (name "latitude") (vers "0.0.3") (deps (list (crate-dep (name "async-trait") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "barrel") (req ">=0.6.0, <0.7.0") (features (quote ("sqlite3" "mysql"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req ">=0.4.1, <0.5.0") (features (quote ("runtime-tokio-rustls" "macros" "any" "mysql" "chrono" "decimal" "sqlite" "migrate"))) (kind 0)) (crate-dep (name "thiserror") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 2)))) (hash "0320msc7mlmvjqn66mkp75gh77zj3090qrayh46zhsrd7kk590yg")))

(define-public crate-latitude-0.0.4 (crate (name "latitude") (vers "0.0.4") (deps (list (crate-dep (name "async-trait") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "barrel") (req ">=0.6.0, <0.7.0") (features (quote ("sqlite3" "mysql"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req ">=0.4.1, <0.5.0") (features (quote ("runtime-tokio-rustls" "macros" "any" "mysql" "chrono" "decimal" "sqlite" "migrate"))) (kind 0)) (crate-dep (name "thiserror") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 2)))) (hash "1z2wy6fs4kyyicwjzrsqw1z3jxcvw5fms8zn4lmbx02vrxzd5l5b")))

