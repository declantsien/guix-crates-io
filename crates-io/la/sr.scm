(define-module (crates-io la sr) #:use-module (crates-io))

(define-public crate-lasrs-0.1 (crate (name "lasrs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)))) (hash "0lgfjb3spypdp5fmvx5hv8zylvxs6ycvcrplmc1brq48wnv57ka7")))

(define-public crate-lasrs-0.1 (crate (name "lasrs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)))) (hash "161bksb8pz1v92aky47zqgvglwb8bpi1im574411vj9piq0kvfw6")))

(define-public crate-lasrs-0.1 (crate (name "lasrs") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)))) (hash "1a1m54l7r8hgdm57k1przj3zz2ips83ym60px1pn4vhzd9cim4q5")))

(define-public crate-lasrs-0.1 (crate (name "lasrs") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)))) (hash "10dcyxck1h59m4qdd7a5pvww095835csqfy1m0lp6jk5mn5rgl4z")))

