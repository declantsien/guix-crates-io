(define-module (crates-io la rs) #:use-module (crates-io))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "05s8mkz1kgr9q2nrjhp1yzdz9iizmg3310yhnisx034gqdqzq8an")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1q3yxs351hjayhgb287x2210kmgirarg62wd468rfg52zhacckiv")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "122vz5zgxxiwib2gpw7xzjgdcwgc8jpq6685b0xl14dh3r6l7ac1")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1hq4f94wag2kb6w8hc25iym2b8ysdm98ra9m233bl7fnqbbw6mlf")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "052n45mwy9jipw4068h3zyqz5pm3mjf8ml9434qb9yhdn230gx7n")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "11bsyd1nmhzj0hnm0ald7pzyn399abzpmwzy36z8vqf9ricysklg")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.6") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1x1wybb8l6688gzw75kz3jlv68zngqzl1bwya63b73d10v2a200x")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.7") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0njrkv6pq184mhwr64sslqdjfli9s1cd23ab4vpkczz1kh320bzl")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.8") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0cmcp0ch8x2m5mrjrzv02g4wiqfalggyqwlz3vvxbwdw1lrqcv2a")))

(define-public crate-lars-0.1 (crate (name "lars") (vers "0.1.9") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "10khdlfjpfi0d4bh6mca3jjh137awhqia4dh9db1ncxkfv00yq24")))

(define-public crate-lars-0.2 (crate (name "lars") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1y36dxkl9rg0i7y33myq4nplf9p99id4k76cgc9h4g15knyig7h2")))

(define-public crate-lars-0.2 (crate (name "lars") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0ckklsmzw2hmxdd1dsw660pl5p4vlapcjyrq34h8ply1wyi4mvmv")))

(define-public crate-lars-0.2 (crate (name "lars") (vers "0.2.3") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0yddqigsy626phgk8g59y56iyvki5ffds0k8aaj3m9mv3pnpl9kf")))

(define-public crate-lars-0.2 (crate (name "lars") (vers "0.2.4") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0q535780w37nr29zhkmxvcvgkgf2s3464kg4wnkzdgf7vkramnsy")))

