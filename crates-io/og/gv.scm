(define-module (crates-io og gv) #:use-module (crates-io))

(define-public crate-oggvorbismeta-0.1 (crate (name "oggvorbismeta") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "lewton") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.7") (default-features #t) (kind 0)))) (hash "0rjdfkyshqghaxrd9g9s8chmpja5d3is9srdqxgh30mb8g8g8v59")))

