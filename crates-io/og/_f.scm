(define-module (crates-io og _f) #:use-module (crates-io))

(define-public crate-og_fmt-1 (crate (name "og_fmt") (vers "1.0.0") (hash "0ya2zx8xpns7hqmm48kz6flla5ylmnjccych7aqfyd8iy3qmn70b")))

(define-public crate-og_fmt-1 (crate (name "og_fmt") (vers "1.0.1") (hash "13g5i5ak1rd7fsv4vcgdjnhvx856zqfizjlx29s2wcd6jg2mwp0g")))

(define-public crate-og_fmt-1 (crate (name "og_fmt") (vers "1.0.2") (hash "0lkx16zhg7qnzi0yfgnx8a7qidfr0zkw5ry0wd3l3vkmyp5nb8zh")))

(define-public crate-og_fmt-1 (crate (name "og_fmt") (vers "1.0.3") (hash "1qks0v2d594vl9qyqpv9g9wibvaag4dl4i3cxdrnrykkb4ybckaz")))

(define-public crate-og_fmt-1 (crate (name "og_fmt") (vers "1.0.4") (hash "0p64wflksy3w4rrxmlns3bih11rf4a1aak4xbsr38wr788v2a78l")))

