(define-module (crates-io og l3) #:use-module (crates-io))

(define-public crate-ogl33-0.1 (crate (name "ogl33") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0d0zv0zjyh43m8l1b9fsmsfsaki8k83ai9gizihsq6m0380vns8w") (features (quote (("debug_trace_messages") ("debug_error_checks")))) (yanked #t)))

(define-public crate-ogl33-0.1 (crate (name "ogl33") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0h0phs138lkf593iyh6wmmfk65m3akvh0xxs9ddwwbfq1fdvv6gw") (features (quote (("debug_trace_messages") ("debug_error_checks")))) (yanked #t)))

(define-public crate-ogl33-0.1 (crate (name "ogl33") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1284y0z2v2vnx4issblknlds874zbax0f28mdv091jfggh61cgzj") (features (quote (("debug_trace_messages") ("debug_error_checks")))) (yanked #t)))

(define-public crate-ogl33-0.1 (crate (name "ogl33") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "09mx6xgb49x8wq8611xzygcvpfr952gaqasr6wixijl10070v7kr") (features (quote (("debug_trace_messages") ("debug_error_checks")))) (yanked #t)))

(define-public crate-ogl33-0.1 (crate (name "ogl33") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "12snc0y112mv5g9ak9assnlwqlg4amzqcswk3hs85bzn8bjk81c8") (features (quote (("debug_trace_messages") ("debug_error_checks")))) (yanked #t)))

(define-public crate-ogl33-0.1 (crate (name "ogl33") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1m74cbfng77i3gvfn2nvyjp635vlwwa5zjs5gz9qw0qjmi7rrkwy") (features (quote (("debug_trace_messages") ("debug_error_checks")))) (yanked #t)))

(define-public crate-ogl33-0.1 (crate (name "ogl33") (vers "0.1.6") (deps (list (crate-dep (name "beryllium") (req "^0.2.0-alpha.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "04130xvrzw2q38ryqajvv8nmmnn8724kdf2r5vqzr1zxcxkn82d0") (features (quote (("debug_trace_messages") ("debug_error_checks"))))))

(define-public crate-ogl33-0.2 (crate (name "ogl33") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "beryllium") (req "^0.2.0-alpha.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1b62r0q4mhda7hns0ij7db3gwfa46is36s5rlvdi3b297xkqmfbn") (features (quote (("debug_trace_messages") ("debug_error_checks"))))))

(define-public crate-ogl33-0.2 (crate (name "ogl33") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "beryllium") (req "^0.2.0-alpha.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1f4syjn3xifrwj5lsczdhikh6jkwfji9djldbxh1xawqvgpzqg94") (features (quote (("debug_trace_messages") ("debug_error_checks") ("compatibility_profile"))))))

(define-public crate-ogl33-0.2 (crate (name "ogl33") (vers "0.2.0") (deps (list (crate-dep (name "beryllium") (req "^0.2.0-alpha.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0ciyrl7rffiag1ma6z3avbwa0cy8a75lvp4w1vwpfs713alg2k42") (features (quote (("debug_trace_messages") ("debug_error_checks") ("compatibility_profile"))))))

(define-public crate-ogl33-0.3 (crate (name "ogl33") (vers "0.3.0") (hash "0rk08kayadj49jv1pchrbc3w73jl976v8fdm1q1mg4mb3idgnf1d")))

