(define-module (crates-io og aw) #:use-module (crates-io))

(define-public crate-ogawa-rs-0.1 (crate (name "ogawa-rs") (vers "0.1.0") (hash "1wqk9wz1rxf5f2ppzsdxgm4y2d9mbz14vm35gsbmbmlvfblbll7s")))

(define-public crate-ogawa-rs-0.2 (crate (name "ogawa-rs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vmap") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0gbs2m8jhh0ykq5qnnlrh96j8whljw40jp90qfc6ga5hnx09jbxx")))

(define-public crate-ogawa-rs-0.2 (crate (name "ogawa-rs") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vmap") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "10mlgjqwjvwwlbwni2kaadfh3drmc95i9rig0nsm29zpfwhsggx6")))

(define-public crate-ogawa-rs-0.3 (crate (name "ogawa-rs") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kjgqhpjqwzqcdghfccfb283lxgln4g5bwjl8y01p103hhj52vry")))

(define-public crate-ogawa-rs-0.4 (crate (name "ogawa-rs") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cqi6m9716vb38pzf8fw9v3v3wgx67wkkgdg85lqfnawsq4j9pwg")))

