(define-module (crates-io og g_) #:use-module (crates-io))

(define-public crate-ogg_metadata-0.1 (crate (name "ogg_metadata") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qf2v5gd1mkk5n64ama87922xfg3kishd185sy9lia0z4xwgp4y5")))

(define-public crate-ogg_metadata-0.2 (crate (name "ogg_metadata") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.3") (default-features #t) (kind 0)))) (hash "1macy9cym7p8lf6r93qj9ax67qds76h8liy0vh94ijg7d0xlf80y")))

(define-public crate-ogg_metadata-0.3 (crate (name "ogg_metadata") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.3") (default-features #t) (kind 0)))) (hash "00dd82dmxpfchr9j35pshnm6q790v4rq8113rc4sgym6bxrwqmg7")))

(define-public crate-ogg_metadata-0.4 (crate (name "ogg_metadata") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xy2s1c21v279dpyi2qn77gqc2cx5rnvz27p03cszcsn97k565xv")))

(define-public crate-ogg_metadata-0.4 (crate (name "ogg_metadata") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ogg") (req "^0.5") (default-features #t) (kind 0)))) (hash "1454s3g2rfwqgaizi4g2dqkfk5l92hkd4nn5ayjwp6a38lbmfrpw")))

(define-public crate-ogg_next_sys-0.1 (crate (name "ogg_next_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "git2") (req "^0.15.0") (kind 1)))) (hash "1zdr843ib7dx20b8x22vkhkp4ndq5d16kbd0zri2c9ymbdn3f3af") (features (quote (("build-time-bindgen" "bindgen")))) (yanked #t) (links "ogg") (rust-version "1.60.0")))

(define-public crate-ogg_next_sys-0.1 (crate (name "ogg_next_sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)))) (hash "0mginibr0lpp73r3w6m9p5lfjdgm42rx41fam0ffmh20kmk58qmv") (features (quote (("build-time-bindgen" "bindgen")))) (links "ogg") (rust-version "1.60.0")))

(define-public crate-ogg_next_sys-0.1 (crate (name "ogg_next_sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "172wy58namfqjmf0nmkln4vc911mz4s45p190rrhmf4is23igzg9") (features (quote (("build-time-bindgen" "bindgen")))) (links "ogg") (rust-version "1.60.0")))

(define-public crate-ogg_next_sys-0.1 (crate (name "ogg_next_sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "1bjnz7lrxl93ynb9ai0bhq7h2jya3rm4ka2n96r6lvvqd1cg4jmc") (features (quote (("build-time-bindgen" "bindgen")))) (links "ogg") (rust-version "1.64.0")))

(define-public crate-ogg_pager-0.1 (crate (name "ogg_pager") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0v3p57v9hwhigihc69ikqvk6nq67agx0grz2cm8l57kn46nmdgk7")))

(define-public crate-ogg_pager-0.1 (crate (name "ogg_pager") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1ijrv7ipcz5hcbn9iyqfwrnm48awmpg844n8sy15ik1wmacbg7qx")))

(define-public crate-ogg_pager-0.1 (crate (name "ogg_pager") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0qsnnba58jfc6cikr3m7x84gya495w4r8yw1fqzyxaxqs54bvxfc")))

(define-public crate-ogg_pager-0.1 (crate (name "ogg_pager") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1wph92mpv4p9k0hsirxjhkngxrc73w9yf6p8w6yhbmrm15biz6r1")))

(define-public crate-ogg_pager-0.1 (crate (name "ogg_pager") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0wg293x46wkanypyxi1h9g3gy640ahas87m31d28p8ihpxbqz1km")))

(define-public crate-ogg_pager-0.1 (crate (name "ogg_pager") (vers "0.1.8") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "09yc7m9pk4a7j3iwc4gzig5796cgniwjqf0mgxblhjsidkcifljg")))

(define-public crate-ogg_pager-0.1 (crate (name "ogg_pager") (vers "0.1.9") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "00xxzzhrxq4pbnwl0qwlvir7dnqxpshi3ys867lixk6jhqmw6i5v") (yanked #t)))

(define-public crate-ogg_pager-0.2 (crate (name "ogg_pager") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0v804yvz7s5vrdm5v3mpny54wsaczdvba9cj89b2sigx25n2ap0z")))

(define-public crate-ogg_pager-0.3 (crate (name "ogg_pager") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "06r49cf8y5k7bmg7rg4nrzyj78jm612k6zyswgab9qdqcqjif1s5")))

(define-public crate-ogg_pager-0.3 (crate (name "ogg_pager") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0nv3ifg187jbqlw065b80dlqnkd5d4xi6b1j7k9qng6a4jky8w1a")))

(define-public crate-ogg_pager-0.3 (crate (name "ogg_pager") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1x29l3gbc1id0qhb1wiaaylyfib0vl885x76d12mvfbq0m66r11a")))

(define-public crate-ogg_pager-0.4 (crate (name "ogg_pager") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1g51y78vws7mwdgvv1821nm12l5qd5a4rblcyap37fg0nrdhc1ca") (yanked #t)))

(define-public crate-ogg_pager-0.5 (crate (name "ogg_pager") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0h6bjzxg8jqaqifbllg8xwv78vwid7an409d94f8xs2xdr08l88d")))

(define-public crate-ogg_pager-0.6 (crate (name "ogg_pager") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1jjk8qlv2n6208gnimwx50450h2bvmi1g7g3yqrc69bv70xxcjf9")))

(define-public crate-ogg_pager-0.6 (crate (name "ogg_pager") (vers "0.6.1") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1yp6yx17mxzvzk6irvx4kayvpz9f44w9a9vpmf85hg2k13wbxc47")))

(define-public crate-ogg_vorbis_ref-0.0.1 (crate (name "ogg_vorbis_ref") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "ogg-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "vorbis-sys") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0434pz33qzfy6bgjabh7y02anbsqnvp8ji0kahqci0mqryjq97xg")))

(define-public crate-ogg_vorbis_ref-0.0.2 (crate (name "ogg_vorbis_ref") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "ogg-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "vorbis-sys") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0npkav0gmf1ncb6w0s38g2zskfrzj84zm62bi16gisihbxdhn0dm")))

(define-public crate-ogg_vorbis_ref-0.0.3 (crate (name "ogg_vorbis_ref") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "ogg-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "vorbis-sys") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0vjyw1mnlnn36pn6vwx1cgjdrnpwh8i9p8msky8h90866n78g607")))

