(define-module (crates-io og an) #:use-module (crates-io))

(define-public crate-oganesson-0.1 (crate (name "oganesson") (vers "0.1.0") (deps (list (crate-dep (name "oci") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dny4vkhgrx86h3xax87w0ffq1hli9kw90hv99yb690d9ynfdznq")))

