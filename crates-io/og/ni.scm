(define-module (crates-io og ni) #:use-module (crates-io))

(define-public crate-ognibuild-0.0.18 (crate (name "ognibuild") (vers "0.0.18") (deps (list (crate-dep (name "pyo3") (req "^0.18") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "1n3pjp3s40pnvfb7i08pkjwhcvbkadlsxb18q0j45664v82v6pbh")))

(define-public crate-ognibuild-0.0.19 (crate (name "ognibuild") (vers "0.0.19") (deps (list (crate-dep (name "pyo3") (req "^0.18") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "0sacqkan3fjn6sziihyhk6cfdlzxzk66s54vcjf5139vj9n1d7g9")))

