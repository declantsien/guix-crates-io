(define-module (crates-io og ri) #:use-module (crates-io))

(define-public crate-ogrim-0.1 (crate (name "ogrim") (vers "0.1.0") (deps (list (crate-dep (name "ogrim-macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "01mq96znydjz5112cizaqs5n8fwzsj5vj63sdvz9z9cccpssisbd") (rust-version "1.72")))

(define-public crate-ogrim-0.1 (crate (name "ogrim") (vers "0.1.1") (deps (list (crate-dep (name "ogrim-macros") (req "=0.0.3") (default-features #t) (kind 0)))) (hash "1d3mz7jsqdild30hdwgpd6h7ybwakk5pjn74mmbwfhgh40zz3ag9") (rust-version "1.72")))

(define-public crate-ogrim-macros-0.0.1 (crate (name "ogrim-macros") (vers "0.0.1") (deps (list (crate-dep (name "litrs") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0jk5qvm30fnilcd7zpi14r5kylaxf25gk44csc5x8mpwlgiwaw7i")))

(define-public crate-ogrim-macros-0.0.2 (crate (name "ogrim-macros") (vers "0.0.2") (deps (list (crate-dep (name "litrs") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "12wizqli2pvb28000rvg3jk42bksbqljx6aki2y4w4ip8rxddn8c")))

(define-public crate-ogrim-macros-0.0.3 (crate (name "ogrim-macros") (vers "0.0.3") (deps (list (crate-dep (name "litrs") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "00rrbw8z1jp6mza4is11bs2vvqlpqxcvwpliiyzchl7rjk57ihfr")))

