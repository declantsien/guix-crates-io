(define-module (crates-io og am) #:use-module (crates-io))

(define-public crate-ogam-1 (crate (name "ogam") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "maud") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)))) (hash "1qcpydf6llsax9ag3kqjvk8i9997pd1yjrd5zyrz7j2i1lx7l0dx") (features (quote (("html" "maud") ("default"))))))

(define-public crate-ogam-1 (crate (name "ogam") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "maud") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)))) (hash "1v0qnm8cxb5ns53ma3i558pxbhkky74nh6q11x5nslwyqycgs73l") (features (quote (("html" "maud") ("default"))))))

