(define-module (crates-io og -l) #:use-module (crates-io))

(define-public crate-og-libwebp-sys-0.1 (crate (name "og-libwebp-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1wjxpil86wq9jakxyrg8wrbnyvvfyilpv7g05gbkzigrd8m86ry4")))

(define-public crate-og-libwebp-sys-0.1 (crate (name "og-libwebp-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "11r22x50qbniz4vncnsagbkwp7xhaxcir2v1znps6fir7cwhjmvg")))

