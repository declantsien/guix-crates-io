(define-module (crates-io og ra) #:use-module (crates-io))

(define-public crate-ograc-0.1 (crate (name "ograc") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0mmmq71d377hijbv1xjn5fxv8ig9p6rj5i8pvd1jlggak18nqsag")))

(define-public crate-ograc-0.1 (crate (name "ograc") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "06gpv12lmrl46dczmfq5wfyxh1ij067lqarkmhc2xjyrdsyxidbn")))

(define-public crate-ograc-0.1 (crate (name "ograc") (vers "0.1.2") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1w7058w4dwq3jmm3m8rfwgj7ynfil4gj00svr2rgw3nsfhcqnlrf")))

(define-public crate-ograc-0.1 (crate (name "ograc") (vers "0.1.3") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0mkzv971pcnkbjvhn40gpf31ny090597m4gkzjsk1jf4yp5431sr")))

(define-public crate-ograc-0.1 (crate (name "ograc") (vers "0.1.4") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1fhlc647ivflb603kzfpzp6g9zwhkl84wkc4qj60s0fhfj7fmq14")))

(define-public crate-ograc-0.1 (crate (name "ograc") (vers "0.1.5") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1nkwgilrx7x4r733vy54ksbydbpwv723jq1nbx73dbqfm3gw4idj")))

(define-public crate-ograc-0.1 (crate (name "ograc") (vers "0.1.6") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1gr9kd2gcshv3s9pz83445ii7dr0w42582qwqrnbbxyh3jdvx7vr")))

