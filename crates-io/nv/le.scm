(define-module (crates-io nv le) #:use-module (crates-io))

(define-public crate-nvle-0.1 (crate (name "nvle") (vers "0.1.0") (deps (list (crate-dep (name "neovim-lib") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "06vcmaj557lqcji4y2pda9h2x8rfn5ylhyblpv98jvkrsh9n8axk")))

