(define-module (crates-io nv -f) #:use-module (crates-io))

(define-public crate-nv-flip-0.1 (crate (name "nv-flip") (vers "0.1.0") (deps (list (crate-dep (name "float_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 2)) (crate-dep (name "nv-flip-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0j7gq818rmpxdf13nm7bkx6c2rlf6ncn1v08ymw9izkmp4xb44i8")))

(define-public crate-nv-flip-0.1 (crate (name "nv-flip") (vers "0.1.1") (deps (list (crate-dep (name "float_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 2)) (crate-dep (name "nv-flip-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1glm1pfhvyfjs6bycgb06pidg7l7rg6qsadg2s045bw7bhnbazgq")))

(define-public crate-nv-flip-0.1 (crate (name "nv-flip") (vers "0.1.2") (deps (list (crate-dep (name "float_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 2)) (crate-dep (name "nv-flip-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x0r3bgmciqs5amrj8dn6hmdhciqhnshx78xvrq04y96qa5cdhsf")))

(define-public crate-nv-flip-sys-0.1 (crate (name "nv-flip-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 2)))) (hash "10z1dfpsz2dx3zhdifac5hhwbj8vjgbmf903pcl4gx813b70rqbp") (links "nv-flip")))

(define-public crate-nv-flip-sys-0.1 (crate (name "nv-flip-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "float_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 2)))) (hash "14y9q7hx9i49978qps1ygih02cxychxsnvdcrh7sfaq083niwblk") (links "nv-flip")))

