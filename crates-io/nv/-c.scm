(define-module (crates-io nv -c) #:use-module (crates-io))

(define-public crate-nv-card-0.1 (crate (name "nv-card") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qpfvn8cz7xfzn30mgzmm11gps5phi0kcgsmyzs5w7ypyfqq1znj")))

(define-public crate-nv-card-0.1 (crate (name "nv-card") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wb1mm4wl335s37azjjz21n31zcm6c2nwd1hp9ygp62d734b6bmc")))

(define-public crate-nv-card-0.1 (crate (name "nv-card") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jfa7rczn22g9h9262j9qp8p1mq9fl3m9az5p7hzawz5v8mnv97v")))

