(define-module (crates-io nv bl) #:use-module (crates-io))

(define-public crate-nvblas-sys-0.1 (crate (name "nvblas-sys") (vers "0.1.0") (hash "0ml94ndiphmrd87xalwqkbi72psprdad2y91n3qfs4w6z8cps36m") (links "nvblas")))

(define-public crate-nvblas-sys-0.1 (crate (name "nvblas-sys") (vers "0.1.1") (hash "0jaib6al3dhhmhckgcsk2baxxn6ff9xg6bamix8jvz41z34ylqxr") (links "nvblas")))

