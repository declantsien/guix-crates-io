(define-module (crates-io nv cl) #:use-module (crates-io))

(define-public crate-nvcli-1 (crate (name "nvcli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.2.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nvapi_sys_new") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0dn7rfzw7fgxmrs1kfcv4cxgfm7m19qnq651f8xgj3x4ccqg83xm")))

(define-public crate-nvcli-1 (crate (name "nvcli") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nvapi_sys_new") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1x9vy5pklv9rzqmrf5kp4zl93w6hwi883fvrs4s9f844zcnzy9q9")))

(define-public crate-nvcli-1 (crate (name "nvcli") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nvapi_sys_new") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1pxk0lmbbjp76nbrr6gpi1y24z0rwpw4ba2jp0zsqiqhd4xml84a")))

(define-public crate-nvcli-1 (crate (name "nvcli") (vers "1.1.2") (deps (list (crate-dep (name "bunt") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nvapi_sys_new") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0pzxk9wm3v09r85ldgcmww5w50w4pfvzx8d143blx0805vdy7nmf")))

(define-public crate-nvcli-1 (crate (name "nvcli") (vers "1.1.3") (deps (list (crate-dep (name "bunt") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nvapi_sys_new") (req "^1.530.1") (default-features #t) (kind 0)))) (hash "1k9jg22m960ly40k31x1jbqx1nw3s9xly577klzwqy4fsdxfa5p8") (yanked #t)))

(define-public crate-nvcli-1 (crate (name "nvcli") (vers "1.1.4") (deps (list (crate-dep (name "bunt") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nvapi_sys_new") (req "^1.530.1") (default-features #t) (kind 0)))) (hash "0qs8ikndf6bzpi8mdlp1c2cdbmnybsjv249w69p8b4hzd6xz0yby")))

