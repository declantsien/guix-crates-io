(define-module (crates-io nv vm) #:use-module (crates-io))

(define-public crate-nvvm-0.1 (crate (name "nvvm") (vers "0.1.0") (deps (list (crate-dep (name "find_cuda_helper") (req "^0.1") (default-features #t) (kind 1)))) (hash "0x126f9r895hmmvjqbdm02fs49s5kzk85p2zf44ycghdqrz3ip2h")))

(define-public crate-nvvm-0.1 (crate (name "nvvm") (vers "0.1.1") (deps (list (crate-dep (name "find_cuda_helper") (req "^0.2") (default-features #t) (kind 1)))) (hash "120h6w7kf9qwbi4r47acvh1pfiln6x0j537z8s9mg4m0nyjy8qr8")))

