(define-module (crates-io nv ps) #:use-module (crates-io))

(define-public crate-nvps-0.1 (crate (name "nvps") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1nh96q2dv7zyzmqc4ymzaxnncsdr3x720vkjx1rx4651r8r5cv8z")))

