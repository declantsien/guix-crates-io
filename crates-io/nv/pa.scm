(define-module (crates-io nv pa) #:use-module (crates-io))

(define-public crate-nvpair-0.1 (crate (name "nvpair") (vers "0.1.0") (deps (list (crate-dep (name "cstr-argument") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "nvpair-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0srv20j0c376xas0sxlzl29cr1j318wwmmrz7qaxpfq8r00kyjmv")))

(define-public crate-nvpair-0.2 (crate (name "nvpair") (vers "0.2.0") (deps (list (crate-dep (name "cstr-argument") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "nvpair-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qmvm3g7bks04nhq3y8jwb5p8pw7cvp5z8h12pcg1mm6ayh4skjs")))

(define-public crate-nvpair-0.3 (crate (name "nvpair") (vers "0.3.0") (deps (list (crate-dep (name "cstr-argument") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nvpair-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "15f1nq4qw4lf260f24093zzl4d8gc1h6gbi8fqcnywc43rhbsmvw")))

(define-public crate-nvpair-0.4 (crate (name "nvpair") (vers "0.4.0") (deps (list (crate-dep (name "cstr-argument") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nvpair-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1d21faf3zqh3mdbpphpwx4hzb4ymh5v28ianwj716fz1gdp7w94f")))

(define-public crate-nvpair-0.5 (crate (name "nvpair") (vers "0.5.0") (deps (list (crate-dep (name "cstr-argument") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nvpair-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "178c182pmv0q3sgk7a844565qzns1y8fmkgvgqsk8p2xhlnz1vkq")))

(define-public crate-nvpair-rs-0.2 (crate (name "nvpair-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stone-libnvpair") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0384cyz4l3yqxvriz8f139q9v5f8v7rw2bxddrxwph2w3vc3fkrr")))

(define-public crate-nvpair-rs-0.3 (crate (name "nvpair-rs") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stone-libnvpair") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0haj4s3pvginynl49pl11zygkii72dcnkhr1nk24a1kdbrazn0a7")))

(define-public crate-nvpair-sys-0.1 (crate (name "nvpair-sys") (vers "0.1.0") (hash "1j5ncby70yr2z8q6rxjb37lylylj0q3xscm7lya11nv035swri2z")))

(define-public crate-nvpair-sys-0.4 (crate (name "nvpair-sys") (vers "0.4.0") (hash "0nmpdr7m4ii7bdrrf7ml7h7k5v2w8wwzh4c4knf9582h5a22l8l1") (links "nvpair")))

