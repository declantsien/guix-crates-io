(define-module (crates-io nv tx) #:use-module (crates-io))

(define-public crate-nvtx-1 (crate (name "nvtx") (vers "1.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)))) (hash "1s7cr006n0jxg9k486x8caz3qz1zzz57bmfrjx818d8sw2fh58m7")))

(define-public crate-nvtx-1 (crate (name "nvtx") (vers "1.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)))) (hash "1iryx0xxr0qnyn2v7yh75s1sf5c27qqywrgh3daffj3kn2cc739m")))

(define-public crate-nvtx-1 (crate (name "nvtx") (vers "1.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0q8j8fy26n6m5fm2jcag72gc19lpys30z33sqqdy149mm42s3nfz")))

(define-public crate-nvtx-1 (crate (name "nvtx") (vers "1.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0b7586w86zrc1fd79ws4w2i71xg4nh76fcxcji5rxy8rh1g8abmd")))

(define-public crate-nvtx-rs-0.1 (crate (name "nvtx-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1hs6jwsyz7x55p3h79lxi4cbw00h65qsilykg5v5s41z4gbvaynr") (yanked #t)))

(define-public crate-nvtx-rs-0.11 (crate (name "nvtx-rs") (vers "0.11.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "01fzlkdzxp93msqwgihla0insrp743d61bdy3zvbfkxdv95az6m0") (yanked #t)))

(define-public crate-nvtx-rs-1 (crate (name "nvtx-rs") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)))) (hash "09j14k7zgy8d7237k6qkrv94bv5blv8b1a0y4hjdkiqb35r3y5j4") (yanked #t)))

(define-public crate-nvtx-rs-1 (crate (name "nvtx-rs") (vers "1.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)))) (hash "11xzhd1sz6cbgp1dhgbba71nsccllhb0h340sh37ja1mkb1svnqc") (yanked #t)))

(define-public crate-nvtxw-0.0.0 (crate (name "nvtxw") (vers "0.0.0") (hash "15m7jfzjhyfl8rxndbidmw0s6sjdgwxby6bdl0n1zgqqr00p116q")))

(define-public crate-nvtxw-sys-0.0.0 (crate (name "nvtxw-sys") (vers "0.0.0") (hash "04w866p5x61zbc7n81hcdcklqplm4jw7zpshd86bh963vn1k99kj")))

