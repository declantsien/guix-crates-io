(define-module (crates-io nv fb) #:use-module (crates-io))

(define-public crate-nvfbc-0.1 (crate (name "nvfbc") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)) (crate-dep (name "nvfbc-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustacuda") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_core") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_derive") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1bgfrj4z4v46hp6k96rmxj092ww7wgii96w2gr9l5kdp1ssjixx0")))

(define-public crate-nvfbc-0.1 (crate (name "nvfbc") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)) (crate-dep (name "nvfbc-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustacuda") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_core") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_derive") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0dw7bj7k5n07yrk6rk99ngnhgmjjaswpj1pgy3s6bkgcs7kv2c6q")))

(define-public crate-nvfbc-0.1 (crate (name "nvfbc") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)) (crate-dep (name "nvfbc-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustacuda") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_core") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_derive") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0z9ynr2m1iw39r860yxq3wmbw5mhnagh02xj0ajd3aw7fd0igqxb")))

(define-public crate-nvfbc-0.1 (crate (name "nvfbc") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)) (crate-dep (name "nvfbc-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rustacuda") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_core") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_derive") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0gcabajg0pffq6hvmnf3pybw5ac6s8fk30gk15myr00lbpbabb6h")))

(define-public crate-nvfbc-0.1 (crate (name "nvfbc") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)) (crate-dep (name "nvfbc-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rustacuda") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_core") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_derive") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1qqc749scy2ib9ssm0vlpzk4849snz3ap6zv5gd4dsqda4njqixw")))

(define-public crate-nvfbc-0.1 (crate (name "nvfbc") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)) (crate-dep (name "nvfbc-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rustacuda") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_core") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustacuda_derive") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1hpc0dw74r94pjmai29vmnz8y6q7j60mj0zc6ah08nf5ak2vafdk")))

(define-public crate-nvfbc-sys-0.1 (crate (name "nvfbc-sys") (vers "0.1.0") (hash "1aw9vd6laasyx6fxq4shbv68xiskdjk3vlva8r31485av4qlxgfh")))

(define-public crate-nvfbc-sys-0.1 (crate (name "nvfbc-sys") (vers "0.1.3") (hash "1r9dyjgd1i65fj3sb1d4g5d2pa5xcj4lqndpm79hj3hhzl8i4lp3")))

(define-public crate-nvfbc-sys-0.1 (crate (name "nvfbc-sys") (vers "0.1.4") (hash "17hlrs807x3l5a6gmhyc9lw2ay6qf3sl36g4i1s23wbbk2hvzyc6")))

(define-public crate-nvfbc-sys-0.1 (crate (name "nvfbc-sys") (vers "0.1.5") (hash "0nsdy78xliqyq0b8j09a48naxpgdcyxcknl8l83qzk4ahhlw64hk")))

