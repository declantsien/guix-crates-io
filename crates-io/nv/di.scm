(define-module (crates-io nv di) #:use-module (crates-io))

(define-public crate-nvdialog-0.1 (crate (name "nvdialog") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0fk0nfplffsz9jn9rkmy9q3zbf62fn5py85dk9y56xlh1x4qi76w") (yanked #t)))

(define-public crate-nvdialog-0.1 (crate (name "nvdialog") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1hzwzgijif8sxnqxy4bxyh3bm6935ajgjkzcasvdq2wk9gazlmrk") (yanked #t)))

(define-public crate-nvdialog-0.1 (crate (name "nvdialog") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1snpyz7bflg2m384s7afsf7f7ca1y1i6b31p4sh9rd5r2x9xqgj6") (yanked #t)))

(define-public crate-nvdialog-rs-0.1 (crate (name "nvdialog-rs") (vers "0.1.0-rc0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lj86rg6zk0kibwmhzsivv1qqlvk35vigrslccgrcl50dv52byha")))

(define-public crate-nvdialog-rs-0.1 (crate (name "nvdialog-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kw217zlbwbhjxw6xzzmrh1sg079a11lxs0sb7pc6r1qwzqidj1k")))

(define-public crate-nvdialog-sys-0.6 (crate (name "nvdialog-sys") (vers "0.6.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "1ha1nh96kvzwspmsafafc4zv4mzsamvwjmr0g46hfmvx76iz4q09")))

