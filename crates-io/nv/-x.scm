(define-module (crates-io nv -x) #:use-module (crates-io))

(define-public crate-nv-xml-0.1 (crate (name "nv-xml") (vers "0.1.0") (deps (list (crate-dep (name "xml-rs") (req "^0.4") (default-features #t) (kind 0)))) (hash "0f8cm8qlrid2sngw1clhxrmsz3zx1qiad1i5i693rz731lxkzhgc")))

(define-public crate-nv-xml-0.1 (crate (name "nv-xml") (vers "0.1.1") (deps (list (crate-dep (name "xml-rs") (req "^0.4") (default-features #t) (kind 0)))) (hash "04b9qpj8hxzs3vncdy3j8v2fjm2jv84kdy7j4505743m4wvmivkc")))

(define-public crate-nv-xml-0.1 (crate (name "nv-xml") (vers "0.1.2") (deps (list (crate-dep (name "xml-rs") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rs9x4vd1hb1awhbq77fn06hxiz9afr3pq6cz03c9mys694vpgmq")))

(define-public crate-nv-xml-0.1 (crate (name "nv-xml") (vers "0.1.3") (deps (list (crate-dep (name "xml-rs") (req "^0.4") (default-features #t) (kind 0)))) (hash "0zixvgyk33cn3ikb9k53mhwllaa1601220c06x6kp5dhpak3yp98")))

