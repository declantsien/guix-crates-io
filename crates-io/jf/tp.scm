(define-module (crates-io jf tp) #:use-module (crates-io))

(define-public crate-jftp-0.1 (crate (name "jftp") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z5alxy7cwa1y5421amizd6ykdkcyq1frsglvl8r7njywcbzb4xi")))

