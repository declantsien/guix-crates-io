(define-module (crates-io jf if) #:use-module (crates-io))

(define-public crate-jfifdump-0.1 (crate (name "jfifdump") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)))) (hash "104mbkxs9vg738a2n4ij13i6k2ssa40c8smcjms473y2m7ahbh56")))

(define-public crate-jfifdump-0.1 (crate (name "jfifdump") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)))) (hash "14vl5yxrhrsfcfmi8nszyyh01nsr0pi3l5h70vwsnndfbfqy4cvb")))

(define-public crate-jfifdump-0.2 (crate (name "jfifdump") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)))) (hash "1jlkiq01jhjb3h9bwm48i75zaql7w766dn1a8val2l70gvmwhg6d")))

(define-public crate-jfifdump-0.3 (crate (name "jfifdump") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)))) (hash "114s68sk8g5mfd92fdwjkpagjqnsh7lr9f9r115xqd2qdrr1wfkr")))

(define-public crate-jfifdump-0.3 (crate (name "jfifdump") (vers "0.3.1") (deps (list (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)))) (hash "0icsz1fjn0q8wczjb5gq0pr0ps7a623w5kaiv01bv4dxhxmcsfw6")))

(define-public crate-jfifdump-0.3 (crate (name "jfifdump") (vers "0.3.2") (deps (list (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)))) (hash "08xwi9jf5wkq5kmc44km4ihlkd3raw1khvbnmzz6ly32rinm9y64")))

(define-public crate-jfifdump-0.4 (crate (name "jfifdump") (vers "0.4.0") (deps (list (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)))) (hash "09h44bf7sxibxqy1c7aq7b434skibssc9js4bxnyckbjyalywmib")))

(define-public crate-jfifdump-0.5 (crate (name "jfifdump") (vers "0.5.0") (deps (list (crate-dep (name "jzon") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "187mg86xq6drfy8xjf9wz9nxxrpsl52573w54l33cxl19ngv7z2j") (features (quote (("json" "jzon") ("default"))))))

(define-public crate-jfifdump-0.5 (crate (name "jfifdump") (vers "0.5.1") (deps (list (crate-dep (name "jzon") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "019cn4q9vflzll2rj3vkv1vi4qb98lfz2wm2b0z23yx87v44frij") (features (quote (("json" "jzon") ("default"))))))

(define-public crate-jfifdump-cli-0.3 (crate (name "jfifdump-cli") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "jfifdump") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1qy2v81sfd914daggr4zxf6x2darsmn574h1yxjxspvpwxjkvkl8")))

(define-public crate-jfifdump-cli-0.3 (crate (name "jfifdump-cli") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "jfifdump") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "037l46x7h06j8q80nik17624i97rwp99l48x77z2as3s0i6wm7ci")))

(define-public crate-jfifdump-cli-0.4 (crate (name "jfifdump-cli") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "jfifdump") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "06psl4h86vgykysz19kydfi61fibzb79bn2mdarkbm7pmxfc992y")))

(define-public crate-jfifdump-cli-0.5 (crate (name "jfifdump-cli") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "jfifdump") (req "^0.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "11psfkvmaj458dxxqsjr65gc3lk2x5zyiqlj1s40s699i3ia4xqx")))

(define-public crate-jfifdump-cli-0.5 (crate (name "jfifdump-cli") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "jfifdump") (req "^0.5.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0myq6my4df5bb0mk2h57gi8xj9d4pksich788h30lz7cjg0cz394")))

