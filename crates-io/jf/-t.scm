(define-module (crates-io jf -t) #:use-module (crates-io))

(define-public crate-jf-tool-0.1 (crate (name "jf-tool") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "os_info") (req "^3.8.2") (default-features #t) (kind 0)))) (hash "1h9z1935s15m1bnpgxr0jki34wjb4pys3kbvspwq9zbk131s91pw")))

