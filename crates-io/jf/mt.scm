(define-module (crates-io jf mt) #:use-module (crates-io))

(define-public crate-jfmt-1 (crate (name "jfmt") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0i83q78rs189vc005bkchp9vvah4pkmd68s2fn6gc21pqp0jprgv")))

(define-public crate-jfmt-1 (crate (name "jfmt") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1zkkf5wjhlp5nw0x5s39zwh4gsfm0ghdlw3aswhmwra07wc9cqcp")))

(define-public crate-jfmt-1 (crate (name "jfmt") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "1wiwdynn7sz0sj195fckrvv1w88ac75m1nmz9l9m2ylsbrs1hhdj")))

(define-public crate-jfmt-1 (crate (name "jfmt") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde-transcode") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.78") (default-features #t) (kind 0)))) (hash "1xlnpl7kakbd0zklpnd1i0hl12mjqrifwlzfbf76f0sfbqlm2qfl")))

