(define-module (crates-io jf rs) #:use-module (crates-io))

(define-public crate-jfrs-0.1 (crate (name "jfrs") (vers "0.1.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wd877a35kp5v0yd2qrl5zbkirc87s30vspiljf7v58p6mvqj7yh")))

(define-public crate-jfrs-0.1 (crate (name "jfrs") (vers "0.1.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06frccamwsykhyjvykmc6csq9nhcpb9a29zdszz85xqc7pnvas04")))

(define-public crate-jfrs-0.1 (crate (name "jfrs") (vers "0.1.2") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l7735aks2j956rxis03jcr3l2gpmldp6hifsikcjcv1haxlz3ni")))

(define-public crate-jfrs-0.1 (crate (name "jfrs") (vers "0.1.3") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0h5ds1wvh8ji4ry8ljrniic5bcv73sjdgld1dfnbsl6slix2km2y")))

(define-public crate-jfrs-0.1 (crate (name "jfrs") (vers "0.1.4") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14q6ywdpahwljzwm8zfxra2frf5wa3lrdq1h77a7jnra55if1fai")))

(define-public crate-jfrs-0.2 (crate (name "jfrs") (vers "0.2.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xgji7zag1mkrbwv3xi193p3cm41s8if8mcayy3v1a3mf4yw24pz") (features (quote (("cstring"))))))

(define-public crate-jfrs-0.2 (crate (name "jfrs") (vers "0.2.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qrmbarz6hlm2zbcr2gkvvzsap9bhm06fz6y2cxmnxxzgnh116jr") (features (quote (("cstring"))))))

(define-public crate-jfrs-0.2 (crate (name "jfrs") (vers "0.2.2") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "105r2xd1fmcyp4xky6ccbk2f39s613q34jninn388bch7mlw8wb9") (features (quote (("cstring"))))))

(define-public crate-jfrs-0.2 (crate (name "jfrs") (vers "0.2.3") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "025bj8bw8pvj5dpgdpyxb7mw40lzb8n854zgdf166ihls0mpc6p8") (features (quote (("cstring"))))))

(define-public crate-jfrs-0.2 (crate (name "jfrs") (vers "0.2.4") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bhyccqqbv2vgj97f43dlw54j1mvhicsh9zkncyzcgvm3lv70gsm") (features (quote (("cstring"))))))

(define-public crate-jfrs-0.2 (crate (name "jfrs") (vers "0.2.5") (deps (list (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0mr434ajp0ygk92vixhqlsdj5zs01wpf8m9wd64c7flfkwry17fm") (features (quote (("cstring"))))))

