(define-module (crates-io bq #{40}#) #:use-module (crates-io))

(define-public crate-bq40z50-0.1 (crate (name "bq40z50") (vers "0.1.0") (hash "1p6cfdch68wmaxh0djl9s0r104fpc37lghgydgiqxbj0w16znm4r")))

(define-public crate-bq40z50-0.1 (crate (name "bq40z50") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "0v6ifxfqfklfg0nghzmh3i2v1rnijmyldgdqbyy19s7vs24mjd7j")))

(define-public crate-bq40z50-0.1 (crate (name "bq40z50") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "1gwwipravcpgfkf7hphfsl7d81jx99hdxkw72vl9y6g7scrapl64")))

(define-public crate-bq40z50-0.1 (crate (name "bq40z50") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "0nj72364m3kh44m1y36961par5dryw1wkfw0rk4f5px6vwnfvy4b")))

(define-public crate-bq40z50-0.1 (crate (name "bq40z50") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "0rfn21qj2dkk8h6f6pjmgpi8daa07l27s7inkg8nmcab649nbail")))

