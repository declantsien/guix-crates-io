(define-module (crates-io bq rs) #:use-module (crates-io))

(define-public crate-bqrs-0.1 (crate (name "bqrs") (vers "0.1.0") (hash "1k03dzxz26ccmzs6zzagpvppzibd0zk2rgxkn8pf8j8q0hk82839")))

(define-public crate-bqrs-0.1 (crate (name "bqrs") (vers "0.1.1") (hash "1hdfgbplfyc7h78v0h3dsdxan9mmxgg6mx7ppbprbfq9xgq1l1kj")))

(define-public crate-bqrs-0.1 (crate (name "bqrs") (vers "0.1.2") (hash "0sr7i2s094rlm6011bq549r81iwdh993v69jrmmgnbxinm9948g8")))

(define-public crate-bqrs-0.1 (crate (name "bqrs") (vers "0.1.3") (hash "0hcvs6w8ywqbzy47l4fch6jqhrvd2kgkc855xfz8xzishcp2nfgl")))

