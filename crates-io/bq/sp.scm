(define-module (crates-io bq sp) #:use-module (crates-io))

(define-public crate-bqsp-0.4 (crate (name "bqsp") (vers "0.4.3") (deps (list (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "146h584a22hfj8bm6gh48l8k6l995whgm1g197kz11z49dsv5p9j") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-bqsp-0.5 (crate (name "bqsp") (vers "0.5.1") (deps (list (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "1830p9n6niy237l4g5f3s7iva329iwn9xw4nmkzgnk9m7qvh4ihj") (features (quote (("default" "async") ("async" "tokio"))))))

