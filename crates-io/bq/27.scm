(define-module (crates-io bq #{27}#) #:use-module (crates-io))

(define-public crate-bq27xxx-0.0.1 (crate (name "bq27xxx") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5.0") (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)))) (hash "1iyzr5jipldhyl4c821adp5sc9ac8nix3iqrsidac03yv0hgxwbg")))

(define-public crate-bq27xxx-0.0.2 (crate (name "bq27xxx") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5.0") (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-rc.3") (default-features #t) (kind 0)))) (hash "0y9izdxlm4lmzrf6wmsq7czzvfj5rmr08lai3zvxsyybh7mxwfnv")))

