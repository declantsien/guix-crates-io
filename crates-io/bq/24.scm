(define-module (crates-io bq #{24}#) #:use-module (crates-io))

(define-public crate-bq24195-0.1 (crate (name "bq24195") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "~0.2") (default-features #t) (kind 0)))) (hash "0d63y4h26cwi21dw44z8dglsa2r3an4qiqapw3skk6w9qcclrxd9")))

(define-public crate-bq24195-0.1 (crate (name "bq24195") (vers "0.1.2") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "~0.2") (default-features #t) (kind 0)))) (hash "1xv1ql5d85zz9vyi9sqfmjmc86p79ivxdg4rzg98wwx995nc6fsq")))

(define-public crate-bq24195-i2c-0.1 (crate (name "bq24195-i2c") (vers "0.1.0") (deps (list (crate-dep (name "arduino_mkrvidor4000") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req ">= 0.1.10") (default-features #t) (kind 0)))) (hash "0fjmppkbmrmc6fapbslnls4gbafx2hssb59d6bmzqlcqzyyfkfl2") (yanked #t)))

(define-public crate-bq24195-i2c-0.1 (crate (name "bq24195-i2c") (vers "0.1.1") (deps (list (crate-dep (name "arduino_mkrvidor4000") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req ">= 0.1.10") (default-features #t) (kind 0)))) (hash "1p1vh976bdxpr16dasad4ih7kz6ckdy2gypqgqwn6k5ijqk1w707") (yanked #t)))

(define-public crate-bq24195-i2c-0.1 (crate (name "bq24195-i2c") (vers "0.1.2") (deps (list (crate-dep (name "arduino_mkrvidor4000") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req ">= 0.1.10") (default-features #t) (kind 0)))) (hash "17dvvd3mp21qzm3sc2bk2wvy0dgl0d5yh0pc842d15i7rkpnf8zl")))

