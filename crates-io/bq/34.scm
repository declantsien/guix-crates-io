(define-module (crates-io bq #{34}#) #:use-module (crates-io))

(define-public crate-bq34z100-0.1 (crate (name "bq34z100") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ayvq5yl11xd7vbagbliv5ibca83zk1wmrj06547b83k9zppdraz")))

(define-public crate-bq34z100-0.2 (crate (name "bq34z100") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0w9nnb0gsffwfzkdwly0v47hrv0y7jqcj44m7q76xdmggl9i39ny") (features (quote (("write" "std") ("std"))))))

(define-public crate-bq34z100-0.2 (crate (name "bq34z100") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0xxlcn1q5y3d8a0mjxznaw2vp7j8wqcqf35bri6wpqx6p65vwl2h") (features (quote (("write" "std") ("std"))))))

(define-public crate-bq34z100_rust-0.1 (crate (name "bq34z100_rust") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0zz9a75p2a0gwp3x2lw5i81sm9hhqgsdxr96977rq128ag2dh16s") (yanked #t)))

