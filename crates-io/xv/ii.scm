(define-module (crates-io xv ii) #:use-module (crates-io))

(define-public crate-xvii-0.1 (crate (name "xvii") (vers "0.1.0") (hash "1wv8ap643qnmp07cmlkvwiszpbzlfph69vpb31yq3xajbnhpm4ng")))

(define-public crate-xvii-0.1 (crate (name "xvii") (vers "0.1.1") (hash "1yglzp0gbfvmqggxrqrp4s9x9bgfi2ajiv63h45rx3xccz2xnz5v")))

(define-public crate-xvii-0.2 (crate (name "xvii") (vers "0.2.0") (hash "046zsqci9966sx0zs9lxa29raaqkhziscn2n132i7jbp0cszniky")))

(define-public crate-xvii-0.2 (crate (name "xvii") (vers "0.2.1") (hash "1ghqm7x6z47cbv7ls2mbyckaydphwp0ihi5x5rsl97q15hadsg0g")))

(define-public crate-xvii-0.2 (crate (name "xvii") (vers "0.2.2") (hash "0fhyqzhclb3dgk0bqf2n5fl6g2mq9m0cz3dms8dzh2n9acdz5w3k")))

(define-public crate-xvii-0.2 (crate (name "xvii") (vers "0.2.3") (hash "0h67sl57af4xs22x8dhx300y3b0asdpixdw0yxqy8j99pjl7gdpn")))

(define-public crate-xvii-0.3 (crate (name "xvii") (vers "0.3.0") (hash "1k4kpxrh6xk7rj9x07c106bg2g1vhbimvnhbbrr37d9ns868jy10")))

(define-public crate-xvii-0.3 (crate (name "xvii") (vers "0.3.1") (hash "1i7rvfi7a05fd6v3j95issxnppzshimxz50fb35lp3z21hi3480x")))

(define-public crate-xvii-0.3 (crate (name "xvii") (vers "0.3.2") (hash "1sjrjzdqh63amhna138l3fhpr5g1vya0kfbwwm8y5id2ypyvdad3") (yanked #t)))

(define-public crate-xvii-0.3 (crate (name "xvii") (vers "0.3.3") (hash "1b8173x26ssi1ikjhz60kf4h4fdzn9rkzhbm8b6i6zmpshyncqrq")))

(define-public crate-xvii-0.3 (crate (name "xvii") (vers "0.3.4") (hash "0xmmwl1fhhydnivpja0ly7h32jcglbnfd2n9znkd10xlav5fr553")))

(define-public crate-xvii-0.4 (crate (name "xvii") (vers "0.4.0") (hash "1dpkl88a459ln5718crd0zlz7fyxyl3vn08bdwysv5nh0vn14x1i")))

(define-public crate-xvii-0.4 (crate (name "xvii") (vers "0.4.1") (hash "1vg5ircv8wwhn4bfhajkblaxx4yy8a7q6rvnjhh220r3qgplwn0r") (features (quote (("std") ("default" "std"))))))

