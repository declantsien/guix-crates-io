(define-module (crates-io xv ma) #:use-module (crates-io))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "157x3hn4msq8a6m5ybsjgd5bav1h3q4gy4ivzmlqw5gk1wc6j7gn") (yanked #t)))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1fdgiz4wpip8lyyk06pi6s2yhyknxpj8hdq46mqyp60i6cvyij0l") (yanked #t)))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1am5k4pyx12lgqpn92cv21z1yi134y32q0p6mq4pv64fkhkkqla9") (yanked #t)))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "00k28s763lrz1bdbsjaxgx1v969jg6p47nf91hxx5wy09s05yhj8")))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1562b6f0a9mbbcnvk8xkgp6hs81kvann46dhg62cp8wkl3i0g9kk")))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0kl2lc5wqbzfgd8wl9kvqlxd7v1lvhvzm8kr9gzgl17p8blcmfhs")))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "15vady9gbnbn70p9ml38xvcfnjhlzc30s7y3lj6cli7qw9ysg9fq")))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.3-1") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1kz5i1c4yzkmkpwgifxdgyia5iszym2ldlwzrn77c8rnnls0sydm")))

(define-public crate-xvmath-0.1 (crate (name "xvmath") (vers "0.1.4") (deps (list (crate-dep (name "paste") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0yzfni9ksschmxh1wq5f6ybqk818rnr9vcqjj4gc3m0wadazis94")))

