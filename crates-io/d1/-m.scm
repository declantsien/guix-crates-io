(define-module (crates-io d1 -m) #:use-module (crates-io))

(define-public crate-d1-mini-0.1 (crate (name "d1-mini") (vers "0.1.0") (deps (list (crate-dep (name "esp8266-hal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0hayvsl9s3182yhmjw03m4ngr7l6nbigxaq9rdzsswn9j80gczf8")))

(define-public crate-d1-mini-0.2 (crate (name "d1-mini") (vers "0.2.0") (deps (list (crate-dep (name "bitbang-hal") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "esp8266-hal") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v4vcpjwwdx1xsmlrhs2fap5fh20wjjbz9afkja9s8m7s4gsvly2")))

