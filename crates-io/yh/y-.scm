(define-module (crates-io yh y-) #:use-module (crates-io))

(define-public crate-yhy-email-encoding-0.0.1 (crate (name "yhy-email-encoding") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.22") (kind 0)) (crate-dep (name "memchr") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1wzcxa43fhf3dsw23ysjjsyz111rmci9bw85wh2ij2sjj6vqbah2") (rust-version "1.57")))

(define-public crate-yhy-email-encoding-0.0.2 (crate (name "yhy-email-encoding") (vers "0.0.2") (deps (list (crate-dep (name "base64") (req "^0.22") (kind 0)) (crate-dep (name "memchr") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "0zj1mpdicarjkwmk98lnbbsbalnmb894cmxl0jbq465cmnbmf8y5") (rust-version "1.57")))

