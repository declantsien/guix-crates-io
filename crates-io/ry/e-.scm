(define-module (crates-io ry e-) #:use-module (crates-io))

(define-public crate-rye-grain-0.0.1 (crate (name "rye-grain") (vers "0.0.1") (deps (list (crate-dep (name "cargo-make") (req "^0.36.3") (default-features #t) (kind 2)) (crate-dep (name "grcov") (req "^0.8.13") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode_categories") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1wvprjs2mfjfvrjbglardb1ln1xd42m26vr2zadkwcq3zq1qj065")))

(define-public crate-rye-macros-0.0.1 (crate (name "rye-macros") (vers "0.0.1") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0gc30z606mypp2n2hrr47rv7n9qp0wzxkzay8bicpx1lb4bb8b32")))

