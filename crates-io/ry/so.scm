(define-module (crates-io ry so) #:use-module (crates-io))

(define-public crate-ryson-0.1 (crate (name "ryson") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "str-macro") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "010xaxbzcq7igmk7g677rbdxz1zmybzw0xmvf67bydn1h40synff")))

