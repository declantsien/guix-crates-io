(define-module (crates-io ry sk) #:use-module (crates-io))

(define-public crate-rysk-core-0.0.1 (crate (name "rysk-core") (vers "0.0.1") (hash "1h3mg95zb1c2nxnisrh7chwmlz0n5z718jqr8ghakljv5li5s9yp") (features (quote (("rv32e") ("ext-q") ("ext-m") ("ext-f") ("ext-d") ("ext-c") ("ext-a") ("default"))))))

(define-public crate-rysk-core-0.0.2 (crate (name "rysk-core") (vers "0.0.2") (hash "0qbnlnswdq2xvmpi05hafnapkmdh4x1wg2sry56y77dgyqigjaz9") (features (quote (("rv32e") ("ext-m") ("ext-csr") ("default" "ext-csr"))))))

(define-public crate-rysk-core-0.0.3 (crate (name "rysk-core") (vers "0.0.3") (hash "1a4h6m78941vh186i6px217lms8k77dy619zlr6ns3jrcngz6jd7") (features (quote (("ext-m") ("ext-csr") ("default" "ext-csr" "ext-m"))))))

(define-public crate-rysk-tools-0.0.1 (crate (name "rysk-tools") (vers "0.0.1") (deps (list (crate-dep (name "rysk-tools-macro") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "070j7gfjfyzvjld416v54iff9ld03qy265k3nf340lg3csvqrj7k")))

(define-public crate-rysk-tools-macro-0.0.1 (crate (name "rysk-tools-macro") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05k5am3b4ssxkijqkzyr5gvadii9bdzzhg9y5l4s6bs1q5jfdb9m")))

