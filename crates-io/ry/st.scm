(define-module (crates-io ry st) #:use-module (crates-io))

(define-public crate-ryst-0.1 (crate (name "ryst") (vers "0.1.0") (deps (list (crate-dep (name "ryst-openai") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "1r22s0s94libka8a7ypywhh0k5wgwwg47z68n4kjnd63zjnhlbrc") (features (quote (("stable" "default") ("experimental" "stable") ("default"))))))

(define-public crate-ryst-openai-0.1 (crate (name "ryst-openai") (vers "0.1.0") (hash "1i1iyi8g864jlpr9g08m5p5n7m1j26czrv7lcl1qfyqs3hw3sd73") (features (quote (("stable" "default") ("experimental" "stable") ("default"))))))

