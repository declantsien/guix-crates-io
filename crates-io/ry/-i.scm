(define-module (crates-io ry #{-i}#) #:use-module (crates-io))

(define-public crate-ry-interner-0.1 (crate (name "ry-interner") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "04pch1h7fyrgxx884imx60w1fl400p3f0wic79ccw4skn986kwhl") (yanked #t)))

(define-public crate-ry-interner-0.1 (crate (name "ry-interner") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "0lkx9sxdsn79ivi0mbfhl2vipw0ws377h1rxzshn9i5m2d6z01h1") (yanked #t)))

