(define-module (crates-io ry uj) #:use-module (crates-io))

(define-public crate-ryuji-rust-0.0.1 (crate (name "ryuji-rust") (vers "0.0.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "1v1nmrjaaa23s8isxv1m3f7pywpvjq2z38yjn0546l77ib1jd537") (v 2) (features2 (quote (("hashmap_json" "dep:serde_json"))))))

(define-public crate-ryujin-0.0.0 (crate (name "ryujin") (vers "0.0.0") (hash "0qfdj1liv1y6s86jg4v7hprwww446s712svy2c3pgh6w4rjzcy32")))

