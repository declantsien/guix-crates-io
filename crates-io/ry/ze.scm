(define-module (crates-io ry ze) #:use-module (crates-io))

(define-public crate-ryzen-reader-0.1 (crate (name "ryzen-reader") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0prr6mnzf9fdigr6c77gvw8ds52nxiykbr6nf2iz9fnigcgwy1dm")))

(define-public crate-ryzen-reader-0.1 (crate (name "ryzen-reader") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jyd2q5f6snrp5vcla6pdk6pqvypkg955sf9ybbi9yhcy4x1siw0")))

(define-public crate-ryzenadj-rs-0.1 (crate (name "ryzenadj-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)))) (hash "1gb6m0ws7zfdbcrqkhqzlj9ia4x77hy8sz4hljdpailw9n1g4xzw")))

