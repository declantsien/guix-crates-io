(define-module (crates-io ry pp) #:use-module (crates-io))

(define-public crate-rypper-0.1 (crate (name "rypper") (vers "0.1.0") (hash "1pm1wjbrgd6naji2zvz2qc9xld0df5pans21n5ivnq7kmi27r1kx") (yanked #t)))

(define-public crate-rypper-0.1 (crate (name "rypper") (vers "0.1.0-alpha.0") (hash "1w2df9f3a499iiqp50b7n36xi4lbywfdhvxf2p4qqmbal841kpq7") (yanked #t)))

(define-public crate-rypper-0.1 (crate (name "rypper") (vers "0.1.0-alpha3") (hash "0bmg52zgdnsb50m2ly8y9wni4zbvgqmzl6rvrbmygz8yxlsahsci") (yanked #t)))

(define-public crate-rypper-0.1 (crate (name "rypper") (vers "0.1.0-alpha.4") (hash "1bgimvzp7ff88yyrnn1cmscp4jr1mfxgnp3fzjml8gxl9r8wjrhj") (yanked #t)))

(define-public crate-rypper-cli-0.1 (crate (name "rypper-cli") (vers "0.1.0-alpha.4") (hash "0cz36xgg76clqi6qch370afxs0h4wh2z6js3ihg8wgrvl8jr5hcc") (yanked #t)))

(define-public crate-rypper-core-0.1 (crate (name "rypper-core") (vers "0.1.0-alpha.4") (hash "0p3k096bpvnlq4zh9kdkj2mjzdpc916bg0fzz24q2p5l06gjccaj") (yanked #t)))

(define-public crate-rypper-reader-0.1 (crate (name "rypper-reader") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "quick-xml") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "sequoia-openpgp") (req "^1.16.0") (default-features #t) (kind 0)))) (hash "0vj7dr69pi7f1k58616anis4r1kix29ac5z2pcqanmj3dvshipxf") (yanked #t)))

(define-public crate-rypper-utils-0.1 (crate (name "rypper-utils") (vers "0.1.0-alpha.4") (hash "12laz6q9rzdrbpfmqwnhmcfxywav26niydz5ai9fpmgw2iskbjpa") (yanked #t)))

