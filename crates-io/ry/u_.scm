(define-module (crates-io ry u_) #:use-module (crates-io))

(define-public crate-ryu_floating_decimal-0.1 (crate (name "ryu_floating_decimal") (vers "0.1.0") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0wzyma89xxg8l7sv0qh5907lyvdgj3krxp8gs11182fnbwfyj3bh") (features (quote (("small"))))))

