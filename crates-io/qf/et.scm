(define-module (crates-io qf et) #:use-module (crates-io))

(define-public crate-qfetch-0.1 (crate (name "qfetch") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1s8xmv6xk1d823j7iiydapibslkrl7ialz8qs67jhk4nayj5m2ll") (yanked #t) (rust-version "1.60")))

(define-public crate-qfetch-0.1 (crate (name "qfetch") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "13xkxlacymis0a8am4dbm2x9zhqpijxpc41ppqv5pcgbvp12w6z8") (rust-version "1.60")))

(define-public crate-qfetch-0.2 (crate (name "qfetch") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0m4bjc4fgxl2fipdzi5zqrbllj84i4c8wvzpra0szywh40vwslx5") (yanked #t) (rust-version "1.60")))

(define-public crate-qfetch-0.2 (crate (name "qfetch") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0a2r9najlzji8rmfzah6dlw93bci173ipw49fq1aqyr3q93yacx5") (rust-version "1.60")))

(define-public crate-qfetch-0.3 (crate (name "qfetch") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0r7lgjil6v96k90limj0xsjhfl1bmrriiri0v4n540kad04abq71") (rust-version "1.60")))

(define-public crate-qfetch-0.4 (crate (name "qfetch") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1kw519caq6jdb74qrdnil7243falz8n3vqwix8g5694p90w820zw") (rust-version "1.60")))

(define-public crate-qfetch-0.5 (crate (name "qfetch") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0qxm6kyfivfbkc2fg2sxymfygvvpzyp0b736gh5zrf3l4z2ymbj4") (rust-version "1.60")))

(define-public crate-qfetch-0.6 (crate (name "qfetch") (vers "0.6.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0padl4rsl5bpg0647ix65kzpm3w0kn217mcq7vwa9xq4vwv6l5p5") (rust-version "1.61")))

(define-public crate-qfetch-0.7 (crate (name "qfetch") (vers "0.7.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1bf32mngyb80d1ds7wpr43v5zcqd3bqhz75n9ba876kx5075bcfp") (rust-version "1.61")))

(define-public crate-qfetch-0.8 (crate (name "qfetch") (vers "0.8.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "os_type") (req "^2.4") (default-features #t) (kind 0)))) (hash "1irn3gxbi0mg5g245hb57s3d9l9xarvbkyczhs006ldzf2mnx6v9") (rust-version "1.61")))

(define-public crate-qfetch-0.9 (crate (name "qfetch") (vers "0.9.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "os_type") (req "^2.4") (default-features #t) (kind 0)))) (hash "0lawn2k2kcqwljgssd289d7vwxmqyka00syp7wyz587xyzkkgl6s") (yanked #t) (rust-version "1.61")))

(define-public crate-qfetch-0.9 (crate (name "qfetch") (vers "0.9.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "os_type") (req "^2.4") (default-features #t) (kind 0)))) (hash "016vssvvbwjn8xkkvbq4a3qc4v6bls5zx7dqkkp1fx0m2ri328ll") (rust-version "1.61")))

