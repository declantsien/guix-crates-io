(define-module (crates-io dm d_) #:use-module (crates-io))

(define-public crate-dmd_core-0.2 (crate (name "dmd_core") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "15550r3sb4anv5fbkb7585jd8d23vivr9ymgfl56ysb9vz41cc0d")))

(define-public crate-dmd_core-0.2 (crate (name "dmd_core") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0xj0fgbjw36iyy3gg87p75lqb0ds5x9bn3im7730r33bqi6n85f7")))

(define-public crate-dmd_core-0.3 (crate (name "dmd_core") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1jkw78bf8lnqmj2vj3792294h0kpk64l4vzzmm8fgf0h47jdjp5l")))

(define-public crate-dmd_core-0.3 (crate (name "dmd_core") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "15wb91vvglbk252yhr3kr4rcl7127yfj0k2j67j8gmzashkzjjbr")))

(define-public crate-dmd_core-0.4 (crate (name "dmd_core") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "02mnbz4ld01j292f5n54mhv6lkmsamss8arbl0jbwj04gc3kr2ns")))

(define-public crate-dmd_core-0.6 (crate (name "dmd_core") (vers "0.6.2") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)))) (hash "0bhr7d0ml9rdminp5jldvcdzrhjc8qpk0x7xc2r9irswfvbbq4dv")))

(define-public crate-dmd_core-0.6 (crate (name "dmd_core") (vers "0.6.3") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)))) (hash "0mx33vqnq41hcm2bn2drk439arniszsdsa01caikw2ybxyb29wck")))

