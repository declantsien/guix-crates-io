(define-module (crates-io dm ac) #:use-module (crates-io))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "01hpfvqk1y6s57pmq6jpzjsr996abi9rf35if2xr9nn2jm3jh2pm") (yanked #t)))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1l31is7w6pw1mbxdg6ymjis8wpbvy911q2g8mdazqplkryab204h") (yanked #t)))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0p42p5rzhybgm2j6ax3bj4i5kz53jwa38xcls5llpbfg3hymgpvl") (yanked #t)))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1l4zdrgppz3j61cx8d73474z0ig0xz4icc19ld40vjaabrbydjgm") (yanked #t)))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1ga2vc76cyfbmplbvy9hmqdksri6fxirbgzryjajv3danvhvhwy7") (yanked #t)))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1l2wszw2fmmjy1kxm16zpx4vk3qxzj5wrf65v7ff79mrx9xk9vya") (yanked #t)))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1sfm5krhn01jz2lhmw4w74p4zbg9sxvja0fwlzzl9wsnw360590g")))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0n1ddc1kypp1vxs9c77nhxyyj1flzvq2mcpd7q2jx2fpdkz38cai")))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "132ijwm2qwkqadd0vlhszca7d72j47k8qhklkd5rj6m0gn61d97c")))

(define-public crate-dmacro-0.1 (crate (name "dmacro") (vers "0.1.9") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0qmqm8sazicscdv670jm68a23dfhw7zxh85xzxfi68wgkn4s2l6x")))

