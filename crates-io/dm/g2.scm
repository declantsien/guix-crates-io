(define-module (crates-io dm g2) #:use-module (crates-io))

(define-public crate-dmg2nix-0.1 (crate (name "dmg2nix") (vers "0.1.0") (deps (list (crate-dep (name "dmg") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "download_rs") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0iiqyfnad621blir03cs3y6fz6fwn9i1wcbipw48xpybxys48c1n")))

