(define-module (crates-io dm od) #:use-module (crates-io))

(define-public crate-dmodules-0.1 (crate (name "dmodules") (vers "0.1.2") (deps (list (crate-dep (name "elrond-wasm") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0yqinmymin5vb2f9phyak9qb3vqvrib4a0rmgqvs1zmkr12zgqvb")))

