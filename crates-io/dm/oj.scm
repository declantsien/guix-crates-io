(define-module (crates-io dm oj) #:use-module (crates-io))

(define-public crate-dmoj-0.1 (crate (name "dmoj") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1df3bpb57gybnrpv2ar167kw2599b6rms51nrg1zzvs6lqf440ij")))

(define-public crate-dmoj-0.1 (crate (name "dmoj") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0m20g0hq18i5mmwz2irzlwv0sk782gjc3mvdj0sq61d2pd6vwqga")))

(define-public crate-dmoj-0.1 (crate (name "dmoj") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0f4m0232mzzl7hilyglw4vwiw8ir0hq1zfli73i5v7phcb5naxqh")))

(define-public crate-dmoj-0.1 (crate (name "dmoj") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1awiwqs3j2saq663sfw6srap4b15j3fgwfi9yfi19fn2n3i0mbaj")))

(define-public crate-dmoj-0.1 (crate (name "dmoj") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1csyvx6ng9a01b8x57gqjgsnfylj6x9c2sgaak4dr9fq814riaf5")))

(define-public crate-dmoj-0.1 (crate (name "dmoj") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1wc41q83c81hysqyx4y2xrwg2bj8vasdjpmkinwjlmhxfxas3y51")))

