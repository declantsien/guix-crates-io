(define-module (crates-io dm at) #:use-module (crates-io))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0k02p7wfssnz9i7a90xsf0783x1m6az9l8gsvivv17jdnjch2ny0")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1jrgsi0fkfx72l04a3g76wadbgnmmrh97pbnwipncaqyxqz54vjd")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0mxjxgxfaqn95fjsdjnw74nrwg310bsm2fwqiymgf2mc5lgm5vql")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "0fyicb1v56fl19gpp0cp6r20x5sqiph34q0lcb6mymap6kmmk26n")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "13iyd46fb7b5kfc6yyl539bj1sf3xacbckyfj91h9j5lw69ns1g0")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.19") (kind 0)))) (hash "027j8k9l17c0ws8j0862d50m3i5vz0h80jxs4nw5r9q52dl4d58k")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.19") (kind 0)))) (hash "10d41yrxdqa9hchl7bn7np5gbwxqn4cm7ay4h5fbnc38b51sbi5n")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.19") (kind 0)))) (hash "1j50x4rjkg09y8xpc4g9dv2lqnh9h3khpjm2l3mw92vqan1ksj2n")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.19") (kind 0)))) (hash "1zhipqrvgdwmipi9mhnk2z40b3lsalk41fndmbpj1jy96hw63wnb")))

(define-public crate-dmatcher-0.1 (crate (name "dmatcher") (vers "0.1.11") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.20.0-alpha.3") (kind 0)))) (hash "00rjrrx33i0kfrwr8awk6xrw6lkhzbsbjym91pjndb4m9fx28p1y")))

(define-public crate-dmath-0.1 (crate (name "dmath") (vers "0.1.0") (hash "0aw6iw8dsx230634kh74q13p9s77w9qyz24n1zspy0fnhmj2zm6g")))

