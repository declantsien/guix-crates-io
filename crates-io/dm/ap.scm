(define-module (crates-io dm ap) #:use-module (crates-io))

(define-public crate-dmap-0.1 (crate (name "dmap") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pqi1b44yxchns3la9bn3w52n0bfh824b9dhhnmw5pkxhv3rydw5")))

(define-public crate-dmap-0.2 (crate (name "dmap") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "16nwiq3xxcxymvl57mls88sr5qda0camq8cs90wy7zjpp0mcb9rh")))

(define-public crate-dmap-0.2 (crate (name "dmap") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cqzydswf0nqqc4bm2h30aici4mnfg34xhncl53fihg71s2mhawj")))

