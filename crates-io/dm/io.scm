(define-module (crates-io dm io) #:use-module (crates-io))

(define-public crate-dmio-0.1 (crate (name "dmio") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kp6lq2slyw7rrxhqmxj1c37ipjh9f924ij5n4962agdp8w4znvi")))

(define-public crate-dmio-0.1 (crate (name "dmio") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gryp9i7zhx208nj8z4mh9xhzl5jr5zigfw0v4kkpq62vq1bag4f")))

(define-public crate-dmio-0.1 (crate (name "dmio") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)))) (hash "0y8b5vvqsc0rm3g6sj1ly2f8y0ln4zwkzqid50c6p92p5f06d98h")))

(define-public crate-dmio-0.1 (crate (name "dmio") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "14n3vbrjrz6g67i26s1n3nprfwv0rlzpyrpjdgg94c2ikva86qly")))

