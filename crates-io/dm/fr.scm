(define-module (crates-io dm fr) #:use-module (crates-io))

(define-public crate-dmfr-0.1 (crate (name "dmfr") (vers "0.1.0") (deps (list (crate-dep (name "regress") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0a57c3rw2zaka44qjk0l8w5hxhayil4gsj1bqa50fy3qd93i4zxv")))

(define-public crate-dmfr-folder-reader-0.0.1 (crate (name "dmfr-folder-reader") (vers "0.0.1") (hash "1nydmqdgfdc76l2b0q2fccaykzqp6wmcvdc8lhh20aqn9dklzpsf")))

