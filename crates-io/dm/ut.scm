(define-module (crates-io dm ut) #:use-module (crates-io))

(define-public crate-dmutil-0.1 (crate (name "dmutil") (vers "0.1.0") (hash "1hajmm2d3g44s6s6hcbc0i5xc7prbhd0i1id7g48ss20mn9kclrn")))

(define-public crate-dmutils-0.1 (crate (name "dmutils") (vers "0.1.0-alpha.0") (hash "1xw8d5s3dlrql7x1xdjlcwascz65qy6vnn77nffasyvxy6dddx0n")))

(define-public crate-dmutils-0.1 (crate (name "dmutils") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zaf28v34b3rz9jyir08ccv7hkh259g96xsq76i2fj7b530ndwvn")))

(define-public crate-dmutils_derive-0.1 (crate (name "dmutils_derive") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "148vw5jw2yn5k07i3w2wldxa5ca6bnmi6v3l87jnr2i8nvy17wz0")))

