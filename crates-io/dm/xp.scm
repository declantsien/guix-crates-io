(define-module (crates-io dm xp) #:use-module (crates-io))

(define-public crate-dmxparser-0.1 (crate (name "dmxparser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)))) (hash "1idjp0rv5cqkbmlzsf9j1vd3ciy34ynkxrvp0wdag4gxvynvyijp")))

