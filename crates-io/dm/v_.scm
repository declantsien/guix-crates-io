(define-module (crates-io dm v_) #:use-module (crates-io))

(define-public crate-dmv_derive-0.0.0 (crate (name "dmv_derive") (vers "0.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sdz5s7hkw2nj5azy6z0v281q67k162f0gcky2x92984hxrgq7qh") (yanked #t)))

(define-public crate-dmv_derive-0.1 (crate (name "dmv_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fjwp1dviaypx98i9c35z0847zlsaw2jy029m1aysml1nvf5h309") (yanked #t)))

(define-public crate-dmv_derive-0.2 (crate (name "dmv_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cybglk8iwgrn8dlfyjr7s97zf85kjk2v4mk8ps2z1xs8baaps8v")))

(define-public crate-dmv_derive-0.2 (crate (name "dmv_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ksmrsq9bl7h4n2i2y3fnsqzds7s64ag2gqr9ixsqmbmvalfm6fb")))

(define-public crate-dmv_derive-0.2 (crate (name "dmv_derive") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "15l1lvv0y69852cmqadlr3d4660hzn1pyhx15ij02ixr4s1aj77y")))

