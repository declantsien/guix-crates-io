(define-module (crates-io dv i-) #:use-module (crates-io))

(define-public crate-dvi-to-text-0.1 (crate (name "dvi-to-text") (vers "0.1.0") (deps (list (crate-dep (name "dvi") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02qk6xgbhn3xlpqb8x7fjzpm0slnh1k9zcqiw44cirqr70x4gkd1")))

(define-public crate-dvi-to-text-0.2 (crate (name "dvi-to-text") (vers "0.2.0") (deps (list (crate-dep (name "dvi") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "04qx1677y21750sfs6l8zpax7abxxhzpaiday1mm7nh7g7qclkkc")))

(define-public crate-dvi-to-text-0.2 (crate (name "dvi-to-text") (vers "0.2.1") (deps (list (crate-dep (name "dvi") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1fsrv8hby8jdd1grfkc8gznj13k8jmix62ra9snwsb5avi90fppc")))

