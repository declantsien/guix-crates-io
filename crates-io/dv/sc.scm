(define-module (crates-io dv sc) #:use-module (crates-io))

(define-public crate-dvsc-0.1 (crate (name "dvsc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "094da0yx6d1vawfkm45pm58ikb1f91w9y2vly31vdfx15m823ins")))

