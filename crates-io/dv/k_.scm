(define-module (crates-io dv k_) #:use-module (crates-io))

(define-public crate-dvk_ext_debug_report-0.1 (crate (name "dvk_ext_debug_report") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "dvk") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "shared_library") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0ch5g5wr4d3xh4zv04jijrm2rcw660ay272zxjd3sydiw9p0phxg") (yanked #t)))

