(define-module (crates-io dv bv) #:use-module (crates-io))

(define-public crate-dvbv5-0.0.0 (crate (name "dvbv5") (vers "0.0.0") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)))) (hash "0p8crcq0yjz4m15fm8l78sr5hsrq78ybfyq255bsq21mqdx1i9vp")))

(define-public crate-dvbv5-0.0.1 (crate (name "dvbv5") (vers "0.0.1") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "057f38hqlaw33g769d2w8mmwhffljc75ga9pvk8qgby3z1jsqp0q")))

(define-public crate-dvbv5-0.1 (crate (name "dvbv5") (vers "0.1.0") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0axwfdxn6nqa4qazzbkkr6kc2fnpzi4mka575x9rng4pi292fxxg")))

(define-public crate-dvbv5-0.1 (crate (name "dvbv5") (vers "0.1.1") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0qm7l32j4m8jv39ji8pp6vkpb1s2i04508j4qzvlycgs69hl9ip3")))

(define-public crate-dvbv5-0.2 (crate (name "dvbv5") (vers "0.2.0") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1ahrcb0y80mbz1dxgansgfsx8y378yqxb6cqjmq4ww8hpjy4nv1v")))

(define-public crate-dvbv5-0.2 (crate (name "dvbv5") (vers "0.2.1") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "03sn8dq6w9qi7gzy0ysjkl583d6fxzmsjpxjifca75dzd7rwz8j0")))

(define-public crate-dvbv5-0.2 (crate (name "dvbv5") (vers "0.2.2") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "06p6w2qdcrh3fpyvidnxz7lscjk7yk5zz0v013lw3vr37hlghnw8")))

(define-public crate-dvbv5-0.2 (crate (name "dvbv5") (vers "0.2.3") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1fvliskgh1ckdhka5fnv2bkvacvj0zqnmyq6l0bwln1xqc4x2aj1") (features (quote (("docs-rs"))))))

(define-public crate-dvbv5-0.2 (crate (name "dvbv5") (vers "0.2.4") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "03lm4dq03caff5nvwpqj71m2v72x27wx84xaqjxz8y58sfsixifb") (features (quote (("docs-rs" "dvbv5-sys/docs-rs"))))))

(define-public crate-dvbv5-0.2 (crate (name "dvbv5") (vers "0.2.5") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0vaabi20fj8q3xh0vmxn58ish3s0gc80n9485gyplkq8la2q832l") (features (quote (("docs-rs" "dvbv5-sys/docs-rs"))))))

(define-public crate-dvbv5-0.2 (crate (name "dvbv5") (vers "0.2.6") (deps (list (crate-dep (name "dvbv5-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)))) (hash "17axd427yjy40d3g6wz0hkl7baxfp0i1p2vrl77ai8c83kc9rw86") (features (quote (("docs-rs" "dvbv5-sys/docs-rs"))))))

(define-public crate-dvbv5-sys-0.0.0 (crate (name "dvbv5-sys") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "1wwqjpl98x1l68r7ypnbfnjn7nxdlbkpdrwnp67msgji5d7lx81i")))

(define-public crate-dvbv5-sys-0.0.1 (crate (name "dvbv5-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "0mbzl4apq8ydlcvb1rmrq0dhp5sbmhcssvg0l0rg1n46acr7mm39")))

(define-public crate-dvbv5-sys-0.1 (crate (name "dvbv5-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "0hznyjd665mxywj0dil6jaaqv2jz41kcydki7iabdndrc040gnjv")))

(define-public crate-dvbv5-sys-0.1 (crate (name "dvbv5-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "191nwc0fhsxdrafwzxhh0lql54nwyfi4vdmq15fk4blcn72xg1w7")))

(define-public crate-dvbv5-sys-0.1 (crate (name "dvbv5-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "0w2zfzbfzcqshmc4w03h2c2ga77ndrblb0538ks65jhyy6brdvm4")))

(define-public crate-dvbv5-sys-0.1 (crate (name "dvbv5-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "1lw6k060fqxjjjhvklsinvlkxywzm5jvj3il017vq2pg9iiljyl8") (features (quote (("docs-rs"))))))

(define-public crate-dvbv5-sys-0.1 (crate (name "dvbv5-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "11f6l6qvfid7wfd4ar9fl0g77b4yv54kmzj6pglsnxp1rc24k2im") (features (quote (("docs-rs")))) (links "dvbv5")))

(define-public crate-dvbv5-sys-0.2 (crate (name "dvbv5-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "18vm7f5mjhi82flhfnmvqj1kkf88mkvcg9ziwyi2lg650wpnk2wy") (features (quote (("docs-rs")))) (links "dvbv5")))

(define-public crate-dvbv5-sys-0.2 (crate (name "dvbv5-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "08bb2nasmf2imwhiiwcpnq0kqc8lyp0973zvk311hwpa99jjain1") (features (quote (("docs-rs")))) (links "dvbv5")))

