(define-module (crates-io dv i2) #:use-module (crates-io))

(define-public crate-dvi2html-0.1 (crate (name "dvi2html") (vers "0.1.0") (deps (list (crate-dep (name "dvi") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)))) (hash "1zi66sqr092zfy1m88slldzsm40ixczyhlq0kgqqyp9pr0bgvgq1")))

(define-public crate-dvi2html-0.2 (crate (name "dvi2html") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "dvi") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)))) (hash "1z6yhqn3c17bfn3annx0jc9agvvanvayn5rb0av6ll8g35zmc9dd")))

