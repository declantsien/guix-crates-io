(define-module (crates-io dv ec) #:use-module (crates-io))

(define-public crate-dvec-0.1 (crate (name "dvec") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1zy94iapq9m5a8a8dw7rmnkn284gz457n40a65j29sswkcpf5iy0") (features (quote (("serde_" "serde" "serde_derive") ("default")))) (yanked #t)))

(define-public crate-dvec-0.2 (crate (name "dvec") (vers "0.2.1") (deps (list (crate-dep (name "dnum") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.8") (optional #t) (default-features #t) (kind 0)))) (hash "02ajh8gdvf3ma8bbrr33lpff2fhzp63wzqyl9qiszgz821mbhk6x") (features (quote (("serde_" "serde" "serde_derive") ("default")))) (yanked #t)))

(define-public crate-dvec-0.3 (crate (name "dvec") (vers "0.3.0") (deps (list (crate-dep (name "dnum") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1nagkia9bpzzsf5kkjfnprvndpgy759kllm0ijkr20c5rz2rn2bs") (features (quote (("serde_" "serde" "serde_derive") ("default")))) (yanked #t)))

