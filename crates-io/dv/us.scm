(define-module (crates-io dv us) #:use-module (crates-io))

(define-public crate-dvush-lib-with-protos-0.1 (crate (name "dvush-lib-with-protos") (vers "0.1.0") (hash "14sn7syg5a73p3n1ysr3a7bfjfsijpn2nkw0dxnk4g9d8dww6yz7") (links "libwithprotos-workaround")))

(define-public crate-dvush-lib-with-protos-0.2 (crate (name "dvush-lib-with-protos") (vers "0.2.0") (hash "0ysyqwa2kd6syj0kx81z5g6z1s7j2a5jd95j9i2m0i1gykw8gvd9") (links "libwithprotos-workaround")))

