(define-module (crates-io dv dc) #:use-module (crates-io))

(define-public crate-dvdcss-0.1 (crate (name "dvdcss") (vers "0.1.0") (hash "1kjicxxqqki1lh16q1ycb9rpywy8k9sfvw7n9g66pyzp9ncx27sp")))

(define-public crate-dvdcss-0.1 (crate (name "dvdcss") (vers "0.1.1") (hash "0ss7gz2q0239h7zb8a5f4g2pf2pbiadwg7fr7ywg0c9h07413bnp")))

(define-public crate-dvdcss-0.1 (crate (name "dvdcss") (vers "0.1.2") (hash "0bq1j8lfhrx8ksj8ma9pccj6wrzh22y98z6vy0a0a0mzp1jrp462")))

