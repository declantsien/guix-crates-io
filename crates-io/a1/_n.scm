(define-module (crates-io a1 _n) #:use-module (crates-io))

(define-public crate-a1_notation-0.1 (crate (name "a1_notation") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0nyaxb3wcz663lmdrpdhfyxfdd3mkjjgcjy723vclnmcg4jifiqj")))

(define-public crate-a1_notation-0.2 (crate (name "a1_notation") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jsn5jb571kypc5pp1bi0105yr6p32r25jyikw9pahcmzprs59i4")))

(define-public crate-a1_notation-0.2 (crate (name "a1_notation") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "070wzffcb86kincgdqbfqj5nx64ry6akbj45dji4v2z3ci1pyj5r")))

(define-public crate-a1_notation-0.2 (crate (name "a1_notation") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mq0gb9l5a9vaa1160w8d9sb8zqsnazai08csx1x77dmd52qs5f1")))

(define-public crate-a1_notation-0.3 (crate (name "a1_notation") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0nar7l84lyblbj3ly2y3ag9fgdwagc8my16jjxyhl7kshk3n23ik")))

(define-public crate-a1_notation-0.3 (crate (name "a1_notation") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gqpr7avcww3n3zxlhq7j93cxp285qiay2l6bhhwcsmdxyv5bcj4")))

(define-public crate-a1_notation-0.4 (crate (name "a1_notation") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cbz0n2gz5rmrjvdbrhxhq7llxrsw1hqr2wqwysl3x33bz6nvvdk")))

(define-public crate-a1_notation-0.4 (crate (name "a1_notation") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0i5qbrb1nm0gxsl06bqk0ykx6ys48h57m23w36rs041wlbwn5dfv")))

(define-public crate-a1_notation-0.4 (crate (name "a1_notation") (vers "0.4.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14m24v1dvar564sckj6vzdg01h0l62dra1x2kylv9jiswac5s6zp")))

(define-public crate-a1_notation-0.4 (crate (name "a1_notation") (vers "0.4.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1dfifn9nw2bm7v8xmhl2d25763whpjcqjy86b9yhxwqg3qx76h89")))

(define-public crate-a1_notation-0.5 (crate (name "a1_notation") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wp7dg4707g2h905qk9ssp2809n74jqss7gf3nlvfpyy0ay1gq45")))

(define-public crate-a1_notation-0.6 (crate (name "a1_notation") (vers "0.6.0") (deps (list (crate-dep (name "rkyv") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "03alym16zc40p5njfj7im3dyq8l49nk9269bhbd1xcbsch3b0bmf") (v 2) (features2 (quote (("serde" "dep:serde") ("rkyv" "dep:rkyv"))))))

(define-public crate-a1_notation-0.6 (crate (name "a1_notation") (vers "0.6.1") (deps (list (crate-dep (name "rkyv") (req "^0.7.43") (features (quote ("std" "bytecheck" "validation"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0k56v45r5zx2vxgkwk5j6ghaksld53cn4x6inpfn3n419n4nmz68") (v 2) (features2 (quote (("serde" "dep:serde") ("rkyv" "dep:rkyv"))))))

(define-public crate-a1_notation-0.6 (crate (name "a1_notation") (vers "0.6.2") (deps (list (crate-dep (name "rkyv") (req "^0.7.44") (features (quote ("std" "bytecheck" "validation"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0smzvw50rzzfk92wv3h96y4gwizv1n88s2qhsk6lva0xh24jpkpc") (v 2) (features2 (quote (("serde" "dep:serde") ("rkyv" "dep:rkyv"))))))

