(define-module (crates-io sq sl) #:use-module (crates-io))

(define-public crate-sqslisten-0.1 (crate (name "sqslisten") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clokwerk") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.38.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sqs") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0zfqlmagzkf9vgngal2q61fc5hns5frjbhf36dznrjm3n678vg9v")))

(define-public crate-sqslisten-0.1 (crate (name "sqslisten") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clokwerk") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.38.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sqs") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "065f4lxycq4nbbr91w7ka793dzxw8045nfki78pkmf511nvzyr68")))

