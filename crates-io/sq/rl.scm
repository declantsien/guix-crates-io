(define-module (crates-io sq rl) #:use-module (crates-io))

(define-public crate-sqrl-protocol-0.1 (crate (name "sqrl-protocol") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0z4b9q99lyrs3qk5clmh7hsngc013m5y56gi1pi4f3b8mk1c1bix")))

(define-public crate-sqrl-protocol-0.1 (crate (name "sqrl-protocol") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0kia9k0jf1azhqb8f8mnw2rddrf50msc7p3f16q71c15hlgqpx2f")))

(define-public crate-sqrl-protocol-0.1 (crate (name "sqrl-protocol") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1a40nbpf6hjhv1wh95q44icy9913d7ragllli755jb24aaid9nsc")))

