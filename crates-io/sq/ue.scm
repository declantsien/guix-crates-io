(define-module (crates-io sq ue) #:use-module (crates-io))

(define-public crate-squeak-0.1 (crate (name "squeak") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 2)))) (hash "0ghpk9xnx10mqhpafibpx5n7r3s3aic92rd3ggxdxm31452wsxw8") (rust-version "1.60.0")))

(define-public crate-squeak-0.2 (crate (name "squeak") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 2)))) (hash "1gqzqm78gp80hb2p9bf4chf5q674sp0plmp94bw2bqf1hqhgwjjl") (rust-version "1.60.0")))

(define-public crate-squeal-0.0.1 (crate (name "squeal") (vers "0.0.1") (hash "0g9yq61rvwk3v2a14arbws4vcpfjl8va1zg8ywd8r81hxih0lim7")))

(define-public crate-squeal-0.0.2 (crate (name "squeal") (vers "0.0.2") (hash "1db2dfysda5wymrwa2hzklz2d6h4aikcm3mywkvgw77k2i6i128j")))

(define-public crate-squeal-0.0.3 (crate (name "squeal") (vers "0.0.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0apsiqblfg3zwxc45vpfm78aj3xi1ls2ns8qpa8l2mhjwznxc659")))

(define-public crate-squeal-0.0.4 (crate (name "squeal") (vers "0.0.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1zw1lcv70vdw5parjmlq3hp2d17gp2xgdn40aqkdhpk3hggskmf5")))

(define-public crate-squeal-0.0.5 (crate (name "squeal") (vers "0.0.5") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "postgres") (req "^0.19.7") (default-features #t) (kind 0)) (crate-dep (name "testcontainers") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "testcontainers-modules") (req "^0.2.1") (features (quote ("postgres"))) (default-features #t) (kind 2)))) (hash "1x03fxlbp5wq0vpr3l32v0xd2gnzqy5s03hdn6z7b1dgfq8idd3b") (features (quote (("postgres-docker")))) (rust-version "1.75.0")))

(define-public crate-squeal-0.0.6 (crate (name "squeal") (vers "0.0.6") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "postgres") (req "^0.19.7") (default-features #t) (kind 0)) (crate-dep (name "testcontainers") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "testcontainers-modules") (req "^0.2.1") (features (quote ("postgres"))) (default-features #t) (kind 2)))) (hash "0ar04hjbmk2ybfdx50qcbcb1chzrrismz5hx7rzcwga52blgp389") (features (quote (("postgres-docker")))) (rust-version "1.75.0")))

(define-public crate-squeegee-0.1 (crate (name "squeegee") (vers "0.1.0") (hash "0hms7q3igifcr2z6mqhwh2pqgif21mjy5hws4wfk8l5qqgplq277")))

(define-public crate-squeeze-0.0.1 (crate (name "squeeze") (vers "0.0.1") (hash "0fhi62mbnlkzpnw8swr9rz7rr00zkg0z051lbh2fsvp6ximvcak5")))

(define-public crate-squeezer-0.0.0 (crate (name "squeezer") (vers "0.0.0") (hash "03q3jnqd4bswc50li1bid92a65ibnpszzbckhmy3gjdyh2gvsfbb")))

(define-public crate-squeue-0.10 (crate (name "squeue") (vers "0.10.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1bfpla9nz5sk4ibcbqh3gdjvsc0zb0snaj1rdl476pcas0vwrih4") (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.60")))

(define-public crate-squeue-0.10 (crate (name "squeue") (vers "0.10.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "10d9myanjgpn21xrbcxcsfhfxrjcjmmaw51kislyj5bhmvhi2kk7") (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.60")))

(define-public crate-squeue-0.10 (crate (name "squeue") (vers "0.10.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "10ycmngn2v70pzd91pvwipkrl2566kcdnnz2mxz3mq0kb133ws0z") (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.60")))

(define-public crate-squeue-0.10 (crate (name "squeue") (vers "0.10.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1pglgrl3wlii3b529dsrxc0kh9p42nkw2nnniw1xjwi6qp6ka639") (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.60")))

