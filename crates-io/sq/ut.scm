(define-module (crates-io sq ut) #:use-module (crates-io))

(define-public crate-squtils-0.1 (crate (name "squtils") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sea-query") (req "^0.23") (features (quote ("derive"))) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0qvvsm53vdr18lpdx8g3sdy1p9cdj3mwh1jsxqkfhp3b9ghz523h")))

