(define-module (crates-io sq sh) #:use-module (crates-io))

(define-public crate-sqsh-0.1 (crate (name "sqsh") (vers "0.1.0") (hash "0j7fvfx69h89s340g2a3xx0hd2i77qmysx2m2hssdr9fxx6pnczf")))

(define-public crate-sqsh-0.1 (crate (name "sqsh") (vers "0.1.1") (hash "1cc84ilk4q83kgxamdslicqv5n8ha82xanpmz6lh6hpfd80fysfk")))

(define-public crate-sqsh-testdata-0.1 (crate (name "sqsh-testdata") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "0.4.*") (default-features #t) (kind 0)))) (hash "0fxaz1yy9gs0yxmi5vmybwmdjgkv5fvivg4jj8izh1nh1sfffsiq") (yanked #t)))

(define-public crate-sqsh-testdata-0.1 (crate (name "sqsh-testdata") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "0.4.*") (default-features #t) (kind 0)))) (hash "11ya0ynxba6q7pi65jxv1vygkb763d1mi9k1vic4kgkkw8h5pvgf") (yanked #t)))

(define-public crate-sqsh-testdata-0.1 (crate (name "sqsh-testdata") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "0.4.*") (default-features #t) (kind 0)))) (hash "17b0wlwsrp4gk2yyij9dcd7s4gda04f4d64i30cib4dsrylkqr62") (yanked #t)))

(define-public crate-sqsh-testdata-0.1 (crate (name "sqsh-testdata") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "1.4.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "0.4.*") (default-features #t) (kind 0)))) (hash "0vqc0hkw4z1yll3a2fs24q4ypm1hxb4xxc6v8s3qg6bgygkh9319") (yanked #t)))

