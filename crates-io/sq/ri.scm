(define-module (crates-io sq ri) #:use-module (crates-io))

(define-public crate-sqrid-0.0.1 (crate (name "sqrid") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0x6si20wqcrblb95hbphk8hb8mb5ngfplw43psqr3asqv39h0jpz")))

(define-public crate-sqrid-0.0.2 (crate (name "sqrid") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0aif9aq75ygxxb95sqk4n8vq2g8f7mg7pnbvhj5fxnc8k0q65b1p")))

(define-public crate-sqrid-0.0.3 (crate (name "sqrid") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0zp49j6vgg9nrhm9j1awwsv9nd5pqifxrpgap33qcq1pn94pz55q")))

(define-public crate-sqrid-0.0.4 (crate (name "sqrid") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1w35sv1fhyvz7k73zivlzgq308qcwbz7zzgf23ns0wj4ykd27y6n")))

(define-public crate-sqrid-0.0.5 (crate (name "sqrid") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1cssk59253hw8fap16k8ig2g1qhyd6sfgpsfym3bmk48bgm3d1gw")))

(define-public crate-sqrid-0.0.6 (crate (name "sqrid") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "16qd2zfp183b4jp11x9iklrdb392rg9p174qgk56jnjy7zfiqb05")))

(define-public crate-sqrid-0.0.7 (crate (name "sqrid") (vers "0.0.7") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "05xr70mf9s1w6ilr4gq6ysmx3bsnqry3mlmp9ircv635899qrkzv")))

(define-public crate-sqrid-0.0.8 (crate (name "sqrid") (vers "0.0.8") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0pli58200pvi098g8rlglm5scp96lnjx20kfa7pp2z6fn7jdb1km")))

(define-public crate-sqrid-0.0.9 (crate (name "sqrid") (vers "0.0.9") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1w5452v5lsm4crwl58188bswprafwyq01sycgviinycv8fmbxxir")))

(define-public crate-sqrid-0.0.10 (crate (name "sqrid") (vers "0.0.10") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "104jxz1klb70fkkfj17fgjc9bkr3al4x57b9dx999zl662fq7gmz")))

(define-public crate-sqrid-0.0.11 (crate (name "sqrid") (vers "0.0.11") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0679cxcrzx5z839znqfj3zarrx9narydqj7gpi6j56by4zsgipbb")))

(define-public crate-sqrid-0.0.12 (crate (name "sqrid") (vers "0.0.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1w3iclyg1rl1aq5hzyfd57wz7rg38nh2iv3pf65v5i5b60l8iz33")))

(define-public crate-sqrid-0.0.13 (crate (name "sqrid") (vers "0.0.13") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0f1dpg6hsinqvkkbidgqxf5m6y5gcd8fyvsn1gdcqzbgnvwd11z6")))

(define-public crate-sqrid-0.0.14 (crate (name "sqrid") (vers "0.0.14") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1pibgbd1lyp30kdk898jpnxhlkchv6k8189gjzapq6kj13998jbz")))

(define-public crate-sqrid-0.0.15 (crate (name "sqrid") (vers "0.0.15") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1ii623xaqgi367q5i8k5g9rz1ha9ln211cpsm2hjvcj553vn9prb")))

(define-public crate-sqrid-0.0.16 (crate (name "sqrid") (vers "0.0.16") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1wwws48hmg7x32hn7rx90p5qlwy6djrc8lr3wijsdr7ga4lj4kqd")))

(define-public crate-sqrid-0.0.17 (crate (name "sqrid") (vers "0.0.17") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "059l0qslccdj22prdaix0d3ff9g53pr2bihf7508ql0i9k0cfbx7")))

(define-public crate-sqrid-0.0.18 (crate (name "sqrid") (vers "0.0.18") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "02i2k09iqnv8q43vn57qcbxz5m9z8qxhxxkyaiidyqhggi1fzg22")))

(define-public crate-sqrid-0.0.19 (crate (name "sqrid") (vers "0.0.19") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "17rrbjldf25jgy72bqals0kqfbn6r648h0fr7snma8m15rfgryrp")))

(define-public crate-sqrid-0.0.20 (crate (name "sqrid") (vers "0.0.20") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0r68y8qpafrqjmrm9pcda7qaszbd313pvkkx91dfhaskricw1ys8")))

(define-public crate-sqrid-0.0.21 (crate (name "sqrid") (vers "0.0.21") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "12zhqpda37wcks3079w2h6j310maa2k6dl4cicyiv3zrlr1vsy7r")))

(define-public crate-sqrid-0.0.22 (crate (name "sqrid") (vers "0.0.22") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "09n1wi522bv0m0ck1556c5rskljbv0d1xdxyg1qw8bwfi12apgwj")))

(define-public crate-sqrid-0.0.23 (crate (name "sqrid") (vers "0.0.23") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0fjzk1glccm1683x008vwjfaz286mi5sg14pxxqq3k1y1pvp98fg")))

(define-public crate-sqrid-0.0.24 (crate (name "sqrid") (vers "0.0.24") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1mab593wnr9wdxq3mnybkvrzf5m9lvv141w465jw7r2vrl4qsbnq")))

(define-public crate-sqript-0.1 (crate (name "sqript") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.5") (features (quote ("os-poll" "os-ext"))) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.1") (features (quote ("term" "process" "fs"))) (default-features #t) (kind 0)))) (hash "001a5vybrwhgrq44zqwh185x08zdmyd3v6b54id1yv5i4z1h0izi")))

(define-public crate-sqrite-0.1 (crate (name "sqrite") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0fvdn4kirc7i3rlj570qxkvyz2qxjxbd41bbdll912ypvi869i6h")))

(define-public crate-sqrite-0.2 (crate (name "sqrite") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0rzfscdidhh0kn50xspgzbf6jlxnzyk8phc5knp6zy3jhkkifns6")))

(define-public crate-sqrite-0.3 (crate (name "sqrite") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0rs2qdz77mjhhjgj8gc6bc4zr0xcq97fz11sydz44gzgc7fhqz9d")))

(define-public crate-sqrite-0.4 (crate (name "sqrite") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1c276n43hfpmhlnxvpjfwizqnmvpwa2ja1n3j7fgyafzcs83100f")))

(define-public crate-sqrite-0.5 (crate (name "sqrite") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0vmvv9rfhwk9xkysmfq3d8zlvxgl271cj40kmn0ahza2c2bnhvl7")))

(define-public crate-sqrite-0.6 (crate (name "sqrite") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0ynhvqk0r4nlsin28250rigans41kadwm63sxdb6f1lgkk7dhp3k")))

(define-public crate-sqrite-0.6 (crate (name "sqrite") (vers "0.6.1") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0xvn9z5ka3yl8pf1mplw677lh1jgryph9l2j4d88r106r84vna51")))

(define-public crate-sqrite-0.6 (crate (name "sqrite") (vers "0.6.2") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "08kin8mjwxhhf14rv5ihjx1wkiq9dpllb8lc8dxr24rzin7mi4np")))

(define-public crate-sqrite-0.7 (crate (name "sqrite") (vers "0.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1chpr1lhkswlip3d88s0hamaji5sbj6mrawwkk080h6nb2i6rz2z")))

(define-public crate-sqrite-0.7 (crate (name "sqrite") (vers "0.7.1") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0qdyfmakngp9zy9439mgam8aw2hg0q5d0za6icc1l9b9vd1ykgqf")))

(define-public crate-sqrite-0.8 (crate (name "sqrite") (vers "0.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("fmt" "std" "ansi"))) (kind 2)))) (hash "1ppfqs80d6n7kmcky7kk7mrp2r2754hgcw5zx79218sxv4amzvn2")))

(define-public crate-sqrite-0.8 (crate (name "sqrite") (vers "0.8.1") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("fmt" "std" "ansi"))) (kind 2)))) (hash "1b44jpd47qd55mq7jifqsc4fxgy9g5bnyj3yhrdcxwkiw17rh16j")))

(define-public crate-sqrite-0.8 (crate (name "sqrite") (vers "0.8.2") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("fmt" "std" "ansi"))) (kind 2)))) (hash "1i7jv6vx6jdqfh63xmqf2prfx4xnkgvzqgpln3w2v3nrp9knzzx9")))

(define-public crate-sqrite-0.8 (crate (name "sqrite") (vers "0.8.3") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("fmt" "std" "ansi"))) (kind 2)))) (hash "023829b31fh97hajpp6j395n5djzvni8hwcafk1jm0y2wrvffr6z")))

(define-public crate-sqrite-0.8 (crate (name "sqrite") (vers "0.8.4") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("fmt" "std" "ansi"))) (kind 2)))) (hash "0z0vfp3k7gm0wvisr97sackycb3p1q5s5g20rkrlq954v76d42jq")))

