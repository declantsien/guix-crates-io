(define-module (crates-io sq lv) #:use-module (crates-io))

(define-public crate-sqlvec-0.0.1 (crate (name "sqlvec") (vers "0.0.1") (deps (list (crate-dep (name "rusqlite") (req ">=0.27, <=0.30") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xai10lq0l62mld26q5d28hpw9ja8ivla7hn0a0zpfnfi6qrvd3c") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sqlvec-0.0.2 (crate (name "sqlvec") (vers "0.0.2") (deps (list (crate-dep (name "rusqlite") (req ">=0.27, <=0.30") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16himn5dppbf47ckfjf680h5dy9slkmrcaxhfpbp7lv5jk3dvp92") (v 2) (features2 (quote (("serde" "dep:serde"))))))

