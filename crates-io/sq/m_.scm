(define-module (crates-io sq m_) #:use-module (crates-io))

(define-public crate-sqm_parser-1 (crate (name "sqm_parser") (vers "1.0.1") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "03x75qg4g9gjcby1h06x91y4j9kcs5k3fjd3rkmpfqp1x7g1kvar")))

