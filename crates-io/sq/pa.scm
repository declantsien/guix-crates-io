(define-module (crates-io sq pa) #:use-module (crates-io))

(define-public crate-sqparse-0.1 (crate (name "sqparse") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "^6.1") (features (quote ("power-of-two"))) (default-features #t) (kind 0)))) (hash "0xapv9clsbskiasfh1bm8zg5qbq3g9z3hgw8pvxhv8lvfwblb42c")))

(define-public crate-sqparse-0.2 (crate (name "sqparse") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "^6.1") (features (quote ("power-of-two"))) (default-features #t) (kind 0)))) (hash "0xm3l3ihl75mcchicg9g4gm2gv9wnic7a0rymdmxqs8pv1szrfsy")))

(define-public crate-sqparse-0.3 (crate (name "sqparse") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "lexical") (req "^6.1") (features (quote ("power-of-two"))) (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5") (default-features #t) (kind 0)))) (hash "0wyr4rnv7jlb8rjz749gzw6k858w1mj2ch90fmn2n80s1nfhbbf3")))

