(define-module (crates-io sq uo) #:use-module (crates-io))

(define-public crate-squote-0.1 (crate (name "squote") (vers "0.1.0") (hash "0dmrzhp39wm7ml2g7rbr51a257h44n27m738i2v1idcd6nb36gay")))

(define-public crate-squote-0.1 (crate (name "squote") (vers "0.1.1") (hash "12jc1kqivfr0dcvbig7zcvd9y6mc9i44w2rba9yjmv8dmxkdd9bl")))

(define-public crate-squote-0.1 (crate (name "squote") (vers "0.1.2") (hash "0zcz8irsmljmnn9jl6vhv35wb8jgnrkx4akdg7m5a94ys1zz3k0z")))

