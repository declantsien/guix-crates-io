(define-module (crates-io sq sm) #:use-module (crates-io))

(define-public crate-sqsmv-0.1 (crate (name "sqsmv") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sqs") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0a8mizxgffjzp0zgrbs3ivbbw1qjw67gx4s0nqbqfk989m64v4rh")))

