(define-module (crates-io sq l-) #:use-module (crates-io))

(define-public crate-sql-ast-0.5 (crate (name "sql-ast") (vers "0.5.1-alpha-1") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "0p4wq6ch8y0ax6agmdarw7gs5qc6yk11xrl08nb17xz8izwy7gs1")))

(define-public crate-sql-ast-0.6 (crate (name "sql-ast") (vers "0.6.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "1qkryclrshpgvc0alvjv5bq1pw9gcng629i6zpbxz49r9wrv8s0x")))

(define-public crate-sql-ast-0.7 (crate (name "sql-ast") (vers "0.7.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "1mim06r3476h4mkrswwlxc1pk4101m0lxslmzwr0la1kbwxwykg1")))

(define-public crate-sql-ast-0.7 (crate (name "sql-ast") (vers "0.7.1") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "1sfi7fyfmfmznjgzp5s87vqd2q9zaadmp3msz6lnsgypby7npvcs")))

(define-public crate-sql-ast-0.7 (crate (name "sql-ast") (vers "0.7.2") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "0zs2i5jd5n8zxrcjfwsrbnz45q65nq02pj1pqn1ancllvzczvzmb")))

(define-public crate-sql-ast-0.7 (crate (name "sql-ast") (vers "0.7.3") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "1b5pvaxqs4vrv7hh0w106qc3nar367fv4xr2237ngabsr3jf61p0")))

(define-public crate-sql-ast-0.7 (crate (name "sql-ast") (vers "0.7.4") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "18f2k4lai3q4fvaigwmzk3arv7iqpfz6wlxphl1r3ci9vpnycgsd")))

(define-public crate-sql-ast-0.7 (crate (name "sql-ast") (vers "0.7.5") (deps (list (crate-dep (name "bigdecimal") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "1ga75xndv2agw1la9y3pn1kw0fl2ixzkl8nk5387qwlgmpfigv6l")))

(define-public crate-sql-ast-0.8 (crate (name "sql-ast") (vers "0.8.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "14zr81b4iydx23q6mdl4zk68jb1sxk40haj22z637x166zkr4k22")))

(define-public crate-sql-audit-0.1 (crate (name "sql-audit") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.8.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5.10") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.4.1") (features (quote ("postgres" "runtime-async-std-rustls" "macros" "json" "chrono"))) (default-features #t) (kind 0)))) (hash "1zfl12lmvp5iiln4qm0iaa7dki88clp812kvbfknl1gcxb35yf2p")))

(define-public crate-sql-audit-cli-0.1 (crate (name "sql-audit-cli") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.8.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "sql-audit") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.4.1") (features (quote ("postgres" "runtime-async-std-rustls" "macros" "json" "chrono"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "0ncpibmwprad5g912cvgkmxvmcrzgw36bgz1qwqxi4aqdyj40mcz")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.0") (hash "0w0in192wllnbdz9c8jvwnw3qrd7jk80gkwby7cfzklvwqlb5g4s")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.1") (hash "0h4k6h4wyr9i8lfvlak6ralrnb3gf117qfcwzdzw4c19rjpmq8ww")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.2") (hash "1rrag0idxjsdqizxwl5ymgng1mfbb6sgd5s1ywwdz0k4vmhhn1a3")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.3") (hash "1h2kpcy8c9qx1n3v74kp472r9jy512fa8hlzzxgnlhzsmkf5gh93")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.4") (hash "1rj51a81xw3m3qh2fwb089zsb7n3iyiqzcrhy533vgnizpwz5cz8")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.5") (hash "0wnkvikgx7yhybn78blrqrbvn6jzyqcvjaayqly6l09h3ka6qi5g")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.6") (hash "19cmbwwx9ykr7q9xqzjrsyhm5q6y6hs38h64m901f7x4x6hshfy7")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.7") (hash "07s4xwrbyd6wi82ch6cgavm1196sxn22si0zvx5x2sinyb5i0vfg")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.8") (hash "16y1g2iyxgsiaq93kidb3c5g9n5ysssh5x5243wm391mv0m2xms0")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.9") (hash "1wmds15nvb1980lrb0qfp6swbqf87fwf6x94gpbaaygyrpk0rrwf")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.10") (hash "1nl6k0syn3krzsy228wjxs0za843fk3hawd97yznbpiq8gcmsf6a")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.11") (hash "08lwh98hyslm7zyagw8qg8bbrfvfvyyvvqs3h40xdnrhklaidhj2")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.12") (hash "16qvbfi7h8psgfb03pqy83wx9ld0l1nk21sx71nbfgqgfiqvqbvz")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.13") (hash "12i67g3b00k1z7riwpdip4m1y85y50p3lib28lwkc80z6r6krvjk")))

(define-public crate-sql-builder-0.1 (crate (name "sql-builder") (vers "0.1.14") (hash "15569v3n3p7s568v8fgi1l6p5sb78g40mdg7q7wy8xdcjylz2qjj")))

(define-public crate-sql-builder-0.2 (crate (name "sql-builder") (vers "0.2.0") (hash "0bvwlbb7sns6dlq62q25fs5j5crqkdmijj5d72l5f985j69bldml")))

(define-public crate-sql-builder-0.3 (crate (name "sql-builder") (vers "0.3.0") (hash "0b5y82pnqzqnrzjagr3dpz5af0dd1fnynfjvzbwgpjf29szkcz8r")))

(define-public crate-sql-builder-0.4 (crate (name "sql-builder") (vers "0.4.0") (hash "1f5hnkfk0ka1gcgzs4ahs42aqg2d4a1aapqsqyvjzqrzkj7f04yz")))

(define-public crate-sql-builder-0.5 (crate (name "sql-builder") (vers "0.5.0") (hash "06xs57m1nr7as2qfh6q0gmvj7j5jdr6k8znvcq55znpgr5f2318g")))

(define-public crate-sql-builder-0.5 (crate (name "sql-builder") (vers "0.5.1") (hash "0x7imbrkmyyx8hca1527ywadfby176y0rcmw9rybccqgny1xbvpa")))

(define-public crate-sql-builder-0.6 (crate (name "sql-builder") (vers "0.6.0") (hash "0r8rv5higcpvmac3zbr8lxh88b5nvwk5a8c396q3nzpmfm2v1hbv")))

(define-public crate-sql-builder-0.7 (crate (name "sql-builder") (vers "0.7.0") (hash "12smxyzlyd0fvhz5dfch33j3m8yafl4rsw37qvp3h8kfyn0wl0h3")))

(define-public crate-sql-builder-0.8 (crate (name "sql-builder") (vers "0.8.0") (hash "0pdxbbvcvb353i4yswl9z7wa44gbf545npjxcsprm7rxp2hnrxa8")))

(define-public crate-sql-builder-0.8 (crate (name "sql-builder") (vers "0.8.1") (hash "0hfrc8gbb32ji3mkvkgh3h4l8rza22zhzlck52i30ffpqsnih1j9")))

(define-public crate-sql-builder-0.9 (crate (name "sql-builder") (vers "0.9.1") (hash "18adqdrwsvp28anipwmhiii97i8x4sfsxpl7fhanw04qnmqlgzwq")))

(define-public crate-sql-builder-0.10 (crate (name "sql-builder") (vers "0.10.0") (hash "16li6zrl25cdnywl2rg10q6b3nrabw4g7k38ic0m3fmbhcglvwz8")))

(define-public crate-sql-builder-0.10 (crate (name "sql-builder") (vers "0.10.1") (hash "18ywhpbzw3ja67aikhfdih1dal35rl2ml5h5sjdjbw6l4ij1izqw")))

(define-public crate-sql-builder-0.10 (crate (name "sql-builder") (vers "0.10.2") (hash "0dl1xr6ji65qr3kf21s303y3y9z0zi38nna9vsc3ix5l83gkdrjp")))

(define-public crate-sql-builder-0.10 (crate (name "sql-builder") (vers "0.10.3") (hash "0yk96q2xvll9zm02sam23rffvxjygf7ilyhsqgyzcmdldy7jw1h9")))

(define-public crate-sql-builder-0.10 (crate (name "sql-builder") (vers "0.10.4") (hash "00gzvrh956x4zwbmn9b23sxff4rsd9fvixc0plrg6nwr0xvzxvjj")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.0") (hash "0bsqs6qd47gd2q5majkwgwrcmhzicyxhy226kn02cwg2k2h51ywc")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.1") (hash "1865r492lklddnfbmxxlixf4wpf2213b4bznmapp8vlpasv4x5vb")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.2") (hash "0i430ghys3s2bv5b1bryyp37r5h1bwr8zkhh4j56phx184mi0bll")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.3") (hash "1lcljsndf2l1sy0hrqi10gaggpcilgrpm5r4ccb600rlsdc43rnw")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.4") (hash "04z78dvlw8iky17py85ja281lq0vqp40pl3aikndmshqhdh1jsmk")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.5") (hash "0ndvvl26yrnr203l3fihc94458vypay21w4vwjx4f66xanlwvnn7")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.6") (hash "0sxfhk1vdqzg1wnjx3miqb05k5jda428dyfc70krskaa6dw9ax1i")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.7") (hash "1drbag97nbflym0b7dc6llvcmca4flzdf2kzv2pwy892j8p45jin")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.8") (hash "0zwk2zh1g81kgqslswh6w762d9f22yrsn0j9865ihjqayr1qwpbd")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.9") (hash "1j7bnbi9w1988zc7563092rls41z4xn7s3l2xa724kykd752z8qi")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.10") (hash "01w6p3q80yz30ixxayi9m1l2rijmmamg04yf9vrn76bjwkspb01k")))

(define-public crate-sql-builder-0.11 (crate (name "sql-builder") (vers "0.11.11") (hash "1ghp339f2imj4znj3lcz10dkmy6i5lp8l8qh9k12wpawcwycf1hq")))

(define-public crate-sql-builder-0.12 (crate (name "sql-builder") (vers "0.12.0") (hash "01k660gmvdkpdqrc9ygqcyy8gnhh0xirdh1s42vryf2xdcknhy36")))

(define-public crate-sql-builder-0.12 (crate (name "sql-builder") (vers "0.12.1") (hash "00v1bg5dlc0pl5yhwv6f8q3hvmy3slm5fv9f1k912p26r3db33nj")))

(define-public crate-sql-builder-0.12 (crate (name "sql-builder") (vers "0.12.2") (hash "0qsmy1f61cvz08ya42kiww506g61rinjpvgz7bzvq6ri0r9cq6mp")))

(define-public crate-sql-builder-0.13 (crate (name "sql-builder") (vers "0.13.0") (hash "0i6sqp4s2lrrfj3ax2vq79k9pwy5nbg2ncwf9a0c6nz8maqrd7v5")))

(define-public crate-sql-builder-1 (crate (name "sql-builder") (vers "1.0.0") (hash "0645fbkyic115ksf0fn03zdghvn9jq85qy5x462agqs251xk73d6")))

(define-public crate-sql-builder-1 (crate (name "sql-builder") (vers "1.1.0") (hash "09h7k7widd36b435sb5aqpla0211mmakfp858kr19chzdrm2y99n")))

(define-public crate-sql-builder-1 (crate (name "sql-builder") (vers "1.1.1") (hash "14hcmyzi497xlp34md2ys2a961m5h7m5wla75gbh12mgq81rn893")))

(define-public crate-sql-builder-2 (crate (name "sql-builder") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i8vafkm74sg8vcjiir366p156s740rbvr0p7bj1byzrg6gk5nj7")))

(define-public crate-sql-builder-2 (crate (name "sql-builder") (vers "2.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w8ngvq5y36f5yhq9f8xb9kl09lnajv714ijli40s2xvbz29aa22")))

(define-public crate-sql-builder-2 (crate (name "sql-builder") (vers "2.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "099c25s8fh85rf10l7zwy9i2bdnhv1q5s0yk2vi2lq3bqjcmsr2m")))

(define-public crate-sql-builder-2 (crate (name "sql-builder") (vers "2.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m197mixqdnxvcm79j0726pqp6qxq8c5s28rxkxlj06c1kijp83y")))

(define-public crate-sql-builder-3 (crate (name "sql-builder") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h8d71y9hlsds5g4w987lxn3hip2rncfihgi9k1jpikisi27wr4c")))

(define-public crate-sql-builder-3 (crate (name "sql-builder") (vers "3.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ydj42ss8c2rccdbippribhzgm35adg295skpxw54syhbbxi581x")))

(define-public crate-sql-builder-3 (crate (name "sql-builder") (vers "3.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "15kc10dbifh6ldv4lc4vs1r3rs5jg98f38xrdx7w7bfglc0na8dc")))

(define-public crate-sql-builder-3 (crate (name "sql-builder") (vers "3.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xqr7032c8glihg5x5i7lx22i8h9xd19wmd4x867aqanh8n8br9j")))

(define-public crate-sql-builder-3 (crate (name "sql-builder") (vers "3.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "00cr3424hlba86sx60p21a56x2srrp7ykb0jrhhwmwykqchwapmh")))

(define-public crate-sql-builder-3 (crate (name "sql-builder") (vers "3.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h5xp47zz9chv545lpmal51fq3z162z2f99mb4lhcbgcsaaqs05i")))

(define-public crate-sql-comment-parser-0.1 (crate (name "sql-comment-parser") (vers "0.1.0") (hash "03jbnchaiy4av4rssv7jbw9p98zzh20394prr04zb5503figwaqz")))

(define-public crate-sql-encrypt-0.1 (crate (name "sql-encrypt") (vers "0.1.0") (hash "1yn3h03w3byv9gx12vya7lvyw4wdd90vspxcc277xdyzr4d3z7bd") (yanked #t)))

(define-public crate-sql-gen-0.1 (crate (name "sql-gen") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7") (features (quote ("postgres" "runtime-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qdxz6jrq2jkqp10fbm8pv5m72nlbgbsm31x2irir9zp2wk48kxm")))

(define-public crate-sql-gen-0.1 (crate (name "sql-gen") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7") (features (quote ("postgres" "runtime-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p1chyhm4g72gnqwq4j0k7pdpy7nk6a66skypfjqlzxvkd4d1c0y")))

(define-public crate-sql-gen-0.1 (crate (name "sql-gen") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7") (features (quote ("postgres" "runtime-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cgy5vlpk1kb6gwyp6dp1mv445zfnj72h58jc4108snq5nd99hq2")))

(define-public crate-sql-gen-0.1 (crate (name "sql-gen") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7") (features (quote ("postgres" "runtime-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "167pfg6lagr7y1gnrf9q2asfs03l0yw951d51ps40x8brpa37ij0")))

(define-public crate-sql-gen-0.1 (crate (name "sql-gen") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7") (features (quote ("postgres" "runtime-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zx8xd2gs2qi8dg313q681xbrk17iqw48pa39b9xcl5km3h0m938")))

(define-public crate-sql-gen-0.1 (crate (name "sql-gen") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7") (features (quote ("postgres" "runtime-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx-cli") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "testcontainers") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "testcontainers-modules") (req "^0.3.5") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01n5yyq4sana8il5asalyikjc1qfzbybham5ka26j6gzpss42gyy")))

(define-public crate-sql-insight-0.1 (crate (name "sql-insight") (vers "0.1.0") (deps (list (crate-dep (name "sqlparser") (req "^0.43.1") (features (quote ("visitor"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "05kbczc9l9aiaw394a9bdipsgia05yg82siyjwdm2vqzsgwgkvmd")))

(define-public crate-sql-insight-0.1 (crate (name "sql-insight") (vers "0.1.1") (deps (list (crate-dep (name "sqlparser") (req "^0.43.1") (features (quote ("visitor"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0qzvsd52sh1gxa11m31dyhaj5iknhr7y20jmfk9g1gz0n29zgw08")))

(define-public crate-sql-insight-cli-0.1 (crate (name "sql-insight-cli") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "sql-insight") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1fhc5pzqly6ym8n8cxq8s277ygwr64bsqj7nj2qhil7bclqzxdqf")))

(define-public crate-sql-insight-cli-0.1 (crate (name "sql-insight-cli") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "sql-insight") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "14sx7dgzl06g9lhg401h3fzfxjg9hj37hgrbas4j1bfyw5vn1v99")))

(define-public crate-sql-js-httpvfs-rs-0.0.1 (crate (name "sql-js-httpvfs-rs") (vers "0.0.1") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qz2xs41fllkjkpasxzpgj8mwi1q03737lmkrf4ga46fbrsipgv3") (features (quote (("bundled"))))))

(define-public crate-sql-json-path-0.1 (crate (name "sql-json-path") (vers "0.1.0") (deps (list (crate-dep (name "jsonbb") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libtest-mimic") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "simd-json") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0dagn18fndl11qlg8zrmlxi4a44zfzqicnaksidll0nvq7jh1n86")))

(define-public crate-sql-mel-0.8 (crate (name "sql-mel") (vers "0.8.0-rc1") (deps (list (crate-dep (name "async-std") (req "~1.12") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "melodium-core") (req "=0.8.0-rc1") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "=0.8.0-rc1") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-native-tls"))) (default-features #t) (target "cfg(any(target_env = \"msvc\", target_vendor = \"apple\"))") (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-rustls"))) (default-features #t) (target "cfg(not(any(target_env = \"msvc\", target_vendor = \"apple\")))") (kind 0)) (crate-dep (name "std-mel") (req "=0.8.0-rc1") (default-features #t) (kind 0)))) (hash "03gkd6wqhaa5khvjrkay3hgg0icasrxxmfq5qmsv7aj910r4fpwr") (features (quote (("real") ("plugin") ("mock")))) (rust-version "1.60")))

(define-public crate-sql-mel-0.8 (crate (name "sql-mel") (vers "0.8.0-rc2") (deps (list (crate-dep (name "async-std") (req "~1.12") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "melodium-core") (req "=0.8.0-rc2") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "=0.8.0-rc2") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-native-tls"))) (default-features #t) (target "cfg(any(target_env = \"msvc\", target_vendor = \"apple\"))") (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-rustls"))) (default-features #t) (target "cfg(not(any(target_env = \"msvc\", target_vendor = \"apple\")))") (kind 0)) (crate-dep (name "std-mel") (req "=0.8.0-rc2") (default-features #t) (kind 0)))) (hash "1m8g5bhxi4ima3q3s9z7rrx55qan133b73x37pcwpljdisl7rz18") (features (quote (("real") ("plugin") ("mock")))) (rust-version "1.60")))

(define-public crate-sql-mel-0.8 (crate (name "sql-mel") (vers "0.8.0-rc3") (deps (list (crate-dep (name "async-std") (req "~1.12") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "melodium-core") (req "=0.8.0-rc3") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "=0.8.0-rc3") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-native-tls"))) (default-features #t) (target "cfg(any(target_env = \"msvc\", target_vendor = \"apple\"))") (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-rustls"))) (default-features #t) (target "cfg(not(any(target_env = \"msvc\", target_vendor = \"apple\")))") (kind 0)) (crate-dep (name "std-mel") (req "=0.8.0-rc3") (default-features #t) (kind 0)))) (hash "1a3qm1knvak39bicdq3byappzd880sv8njkh960ap737ychm7ksf") (features (quote (("real") ("plugin") ("mock")))) (rust-version "1.60")))

(define-public crate-sql-mel-0.8 (crate (name "sql-mel") (vers "0.8.0") (deps (list (crate-dep (name "async-std") (req "~1.12") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "melodium-core") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-native-tls"))) (default-features #t) (target "cfg(any(target_env = \"msvc\", target_vendor = \"apple\"))") (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("runtime-async-std" "postgres" "mysql" "tls-rustls"))) (default-features #t) (target "cfg(not(any(target_env = \"msvc\", target_vendor = \"apple\")))") (kind 0)) (crate-dep (name "std-mel") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0jh05b5m64x7km0qlrc1vxm073fkcmvmd7ra7ajzh9bk5q07y6yq") (features (quote (("real") ("plugin") ("mock")))) (rust-version "1.60")))

(define-public crate-sql-migration-sim-0.1 (crate (name "sql-migration-sim") (vers "0.1.0") (deps (list (crate-dep (name "sqlparser") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0sa5rand7a8xx99w1rsrbf3j12qvii3a2qvfgqjx9hvi1ngmda4x")))

(define-public crate-sql-migration-sim-0.1 (crate (name "sql-migration-sim") (vers "0.1.1") (deps (list (crate-dep (name "sqlparser") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "03qz5sm2icdhd2xxzd8gik9l0z8gy4shzq0q199p4k19ivfqgjk7")))

(define-public crate-sql-migration-sim-0.1 (crate (name "sql-migration-sim") (vers "0.1.2") (deps (list (crate-dep (name "sqlparser") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1cf1gnnv2dp5iq0nj6nq0c251554df084yin6ghn1lgjk5rg95nz")))

(define-public crate-sql-migration-sim-0.1 (crate (name "sql-migration-sim") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.195") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.43.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0clsax1531944s137g1r1vi5327b0674dlwfbiqfq3f7aqzjr22l") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde" "sqlparser/serde"))))))

(define-public crate-sql-migration-sim-0.1 (crate (name "sql-migration-sim") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.197") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0v9912b2kylj7gdmx1y1h4kmqy0k6ic1kirhxjf9gwkihmyfsdlj") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde" "sqlparser/serde"))))))

(define-public crate-sql-migration-sim-0.1 (crate (name "sql-migration-sim") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.197") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1aahf44wviymgl6jps91rbaym461nx5m0pbjvix3088zq03c17p4") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde" "sqlparser/serde"))))))

(define-public crate-sql-parse-0.1 (crate (name "sql-parse") (vers "0.1.0") (hash "1vmcg3m8zns5lxk40f3a0dp1m0sslx71alj83iv06rkd16mlhl0m")))

(define-public crate-sql-parse-0.1 (crate (name "sql-parse") (vers "0.1.1") (hash "1mbg6rc80j9dw25lk146dagd6dl9ksnql4cw99fqfq0ffjsy90n0")))

(define-public crate-sql-parse-0.2 (crate (name "sql-parse") (vers "0.2.0") (hash "1vcry97k6h5swgwpkxgkgrl64yhd224mghxgdbc32iws35mkhsh4")))

(define-public crate-sql-parse-0.2 (crate (name "sql-parse") (vers "0.2.1") (hash "1cpsdcwfd4vj2sg6zg8dx84aif99dr4p0i3x2l0i841f7m0n9l4z")))

(define-public crate-sql-parse-0.3 (crate (name "sql-parse") (vers "0.3.0") (hash "0rx4swvi4sprp4dfwkhvmfi8fmjgli9pvv52jxv19kmrrik1kfjq")))

(define-public crate-sql-parse-0.4 (crate (name "sql-parse") (vers "0.4.0") (hash "0fk1h69glaz4wf2ypa4msikvg6v0p3yjvaznvd41r85bvqkx489s")))

(define-public crate-sql-parse-0.5 (crate (name "sql-parse") (vers "0.5.0") (hash "1cnjbxnc83ks0c63gm8hwv39q2bg3c8kiv6z51qx3rbfvl8nqdym")))

(define-public crate-sql-parse-0.6 (crate (name "sql-parse") (vers "0.6.0") (hash "069k662qvxhkh1xz1yynxa9znwn69vgfwz7yalvn59hwga41xj4l")))

(define-public crate-sql-parse-0.7 (crate (name "sql-parse") (vers "0.7.0") (hash "0awzch9ylh8wzfbbnzsxf1r3idsajrcrxjplbx4698r1xdw9l81b")))

(define-public crate-sql-parse-0.8 (crate (name "sql-parse") (vers "0.8.0") (hash "11khl97xcmbivaczib63d2kxxqlimaj5c334sngxcgkj3y0q45kl")))

(define-public crate-sql-parse-0.9 (crate (name "sql-parse") (vers "0.9.0") (hash "0z7pj3hhh7k0yppmvm82208aday64kx59yrjsc1d415fjxg6ww8y")))

(define-public crate-sql-parse-0.10 (crate (name "sql-parse") (vers "0.10.0") (hash "14qll3hfywa4w9053n3i7nvs0006c9j544b6pwz96gg8x9wlka7w")))

(define-public crate-sql-parse-0.10 (crate (name "sql-parse") (vers "0.10.1") (hash "1gv84fdlgffmczjx9ipjk7s8f3bvrdry0fljadw0b7ccr3q25s9v")))

(define-public crate-sql-parse-0.10 (crate (name "sql-parse") (vers "0.10.2") (hash "012s8zvgmncqbihi7f0zhx0pc2fbca8qllfxdlim1vy3i9spjm4i")))

(define-public crate-sql-parse-0.11 (crate (name "sql-parse") (vers "0.11.0") (hash "1fzc530a5170w7q74xy4qsb0ysmkxkx91v38pqr8h7vyfqql4kd0")))

(define-public crate-sql-parse-0.12 (crate (name "sql-parse") (vers "0.12.0") (hash "00hpzjqgaw1gnn0ymyrhx4n76h2bly00njc9ziv30i787vka4cpw")))

(define-public crate-sql-parse-0.13 (crate (name "sql-parse") (vers "0.13.0") (hash "05v0sj3knivxj9is7i4v5q1c7rwjafiaaczj5sdgf845bww01zr2")))

(define-public crate-sql-parse-0.14 (crate (name "sql-parse") (vers "0.14.0") (hash "00499r39z2cqbfxlg5xaivms3ggxgyga3n9iz9s452almjdwx6zd")))

(define-public crate-sql-parse-0.14 (crate (name "sql-parse") (vers "0.14.1") (hash "091yvg3kni3dcsl7cfssz316ldsn21h74lgvmm1qry3x14gpnmw9")))

(define-public crate-sql-parse-0.15 (crate (name "sql-parse") (vers "0.15.0") (hash "0wrx266g2sk5z34q75nvv6h7cnpav52kv4y354gys9qmxzgfaa9k")))

(define-public crate-sql-parse-0.15 (crate (name "sql-parse") (vers "0.15.1") (hash "0a42r8pfmx7al2hppjx6v814aznlzyxndjg0mbmy9n4jxzdszlp9")))

(define-public crate-sql-parse-0.15 (crate (name "sql-parse") (vers "0.15.2") (hash "0mnri7pknsfjpvx00i9nny0k4cis8ris6rqj4a8kkwpp3k51y5av")))

(define-public crate-sql-parse-0.16 (crate (name "sql-parse") (vers "0.16.0") (hash "0c7xms0lwsm9cjs936q4xdb2qz1n95367whbj2ispk8111p478nx")))

(define-public crate-sql-parse-0.17 (crate (name "sql-parse") (vers "0.17.0") (hash "1g38zapq926vihm9ibnv0mklyfbjnhrf2ij8rvfhsvcs4x85422g")))

(define-public crate-sql-parse-0.18 (crate (name "sql-parse") (vers "0.18.0") (hash "1xqv2c4ddigy15rfn2clps50cvilmafhrpw2ix77jmi8zb0vfml1")))

(define-public crate-sql-parser-0.1 (crate (name "sql-parser") (vers "0.1.0") (hash "1kxx05pk23rg27fz35a9nd491iyygr4sixb0y9imy8vqbzqp1277")))

(define-public crate-sql-redactor-0.1 (crate (name "sql-redactor") (vers "0.1.0") (deps (list (crate-dep (name "sqlparser") (req "^0.32.0") (features (quote ("visitor"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "02vwr0ksi1n278964rgiiz7df1s2230k13qy6ivswj6kk4k16gy0")))

(define-public crate-sql-script-parser-0.1 (crate (name "sql-script-parser") (vers "0.1.0") (hash "03kxf6x2kchg5c1c8kx8zzlakbwyljbl56ps5da5b7bvacq6l8zr")))

(define-public crate-sql-script-parser-0.1 (crate (name "sql-script-parser") (vers "0.1.1") (hash "1b5hjxyxrh9g9gz2skn3gpyxfl4ycv0gc4gvwnhjfy69nacm8y7v")))

(define-public crate-sql-script-parser-0.1 (crate (name "sql-script-parser") (vers "0.1.2") (hash "1m2gzs7bv0bf6gy5wri5g6cbaacyd6xqkvcrz2jz0j5mbjzzh3r0")))

(define-public crate-sql-table-0.1 (crate (name "sql-table") (vers "0.1.0") (deps (list (crate-dep (name "inject") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f9i2xi3qnkz58gmzjvms9i78d8ycrppvx3k40rx2qr6rpmpcgki") (yanked #t)))

(define-public crate-sql-table-0.1 (crate (name "sql-table") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sql-table-inject") (req "^0.1") (default-features #t) (kind 0)))) (hash "18qpnc8p6rrf8a9crrrgv4m2sacx9kwaxfw59p8yqzcpmnv8acjs")))

(define-public crate-sql-table-0.1 (crate (name "sql-table") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sql-table-inject") (req "^0.1") (default-features #t) (kind 0)))) (hash "0z5g0dk5x6bz0pj08jxv0wginngl08yg80sf4ji8vrcgzxrm9ngb")))

(define-public crate-sql-table-0.1 (crate (name "sql-table") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sql-table-inject") (req "^0.1") (default-features #t) (kind 0)))) (hash "13vbyvgvd5ya1plgygn5g77hhnmhla69y4gzjlmfdyi063xln4lg")))

(define-public crate-sql-table-0.1 (crate (name "sql-table") (vers "0.1.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sql-table-inject") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kk8rrlid5wx8p5xm3q34mw6mwnyd35d4kvly6fvdp35w1ycgf3i")))

(define-public crate-sql-table-inject-0.1 (crate (name "sql-table-inject") (vers "0.1.0") (hash "1gpld9z30jbdiyd1qjhrp3ndx4n6n1ih7gvhh6bp5wvcxlvqyqh2")))

(define-public crate-sql-type-0.1 (crate (name "sql-type") (vers "0.1.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0kd9aqjj6fb3my7dp61s9rag5f9pf46jx8g0mbwc7a3ygfv723qs")))

(define-public crate-sql-type-0.1 (crate (name "sql-type") (vers "0.1.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13366gpmz8kzw6w72dsvhz8a3kb1gqwkd4y1iyvi9pb7fbq48zhc")))

(define-public crate-sql-type-0.2 (crate (name "sql-type") (vers "0.2.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0s338njv663vg6wgf3zxgla8axzc7qsvr6ash1zl1s3nblvl78a6")))

(define-public crate-sql-type-0.3 (crate (name "sql-type") (vers "0.3.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "10155rz6cpxvb56vmwa06k3zzyg0vk080jbz8jrmkgsqf8z1pgnb")))

(define-public crate-sql-type-0.3 (crate (name "sql-type") (vers "0.3.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.3") (default-features #t) (kind 0)))) (hash "0a8ny52n1awkmypbj8bw2rb6ds12464ki4j0sp3773hvnfysgccz")))

(define-public crate-sql-type-0.4 (crate (name "sql-type") (vers "0.4.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pkl3fbmipjlbp4jyrlxag5k0f42m1x0c7gkx9f5sx8rj488gwkl")))

(define-public crate-sql-type-0.4 (crate (name "sql-type") (vers "0.4.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dbwsjhf50ji3kxf28wq5m7b0cpsmfajb7jfr4wfnkagc0771jp7")))

(define-public crate-sql-type-0.4 (crate (name "sql-type") (vers "0.4.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.4") (default-features #t) (kind 0)))) (hash "0x349xffkwrz06ln01ixrfyq1hd8kvdzh30lv0252ds0k82gfrrr")))

(define-public crate-sql-type-0.4 (crate (name "sql-type") (vers "0.4.3") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jh117wy7m4h7rj1xl3rrawpxs4j8v29cwaknz56f3272hhvz3na")))

(define-public crate-sql-type-0.5 (crate (name "sql-type") (vers "0.5.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.5") (default-features #t) (kind 0)))) (hash "0nkg2gn87ikxzm64r9csqc9w9acm1mp1vrhrcz8ksbaq7bqlbvix")))

(define-public crate-sql-type-0.6 (crate (name "sql-type") (vers "0.6.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.6") (default-features #t) (kind 0)))) (hash "0dzpi7b2n6l1nlxgfw9yqp109l7imphgnc8adgxij8c9hipdzhbw")))

(define-public crate-sql-type-0.7 (crate (name "sql-type") (vers "0.7.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.7") (default-features #t) (kind 0)))) (hash "04m8c2307kvjlmbhn5br1w7f9fcnnih329c038fn3i4a3qvy5p29")))

(define-public crate-sql-type-0.8 (crate (name "sql-type") (vers "0.8.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.8") (default-features #t) (kind 0)))) (hash "1z8rgn884zngdqfimprgwd1c6f22p14i8a7g201rwp2dimjd4cpi")))

(define-public crate-sql-type-0.8 (crate (name "sql-type") (vers "0.8.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jrwsqsq6d1s5x3qafz0qgkfrajwx0giiccn6vh8197g71xd0wqp")))

(define-public crate-sql-type-0.9 (crate (name "sql-type") (vers "0.9.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.9") (default-features #t) (kind 0)))) (hash "06agb1330pshi4qnvb63l8gjv7bk4l5lyhx47rqdhaw1hkln80q9")))

(define-public crate-sql-type-0.10 (crate (name "sql-type") (vers "0.10.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0x6vrw6bnqy40hhc2kxxfiqaghd2325cd15hyc986xn9fhyrz7mq")))

(define-public crate-sql-type-0.10 (crate (name "sql-type") (vers "0.10.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1dglx5zbwkvydjymj57rj0ln1h2gp8nkd4vxp47avd8vp9ci9vqz")))

(define-public crate-sql-type-0.11 (crate (name "sql-type") (vers "0.11.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "16il0v1brwvv298w19mlrv71l5czxm0vca1c3qz4lkqmxdzzrs6f")))

(define-public crate-sql-type-0.12 (crate (name "sql-type") (vers "0.12.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "03f74542nfg059xbf7vwa4ri6jk5g40rg4la6idpc2bn0l3c52cg")))

(define-public crate-sql-type-0.13 (crate (name "sql-type") (vers "0.13.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1f00w9kwfzjklfyhs71f6nbir9gj8z8mhiliacb55srn3zg988a8")))

(define-public crate-sql-type-0.13 (crate (name "sql-type") (vers "0.13.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1l6fr2p3sddlszn9b2bgf2p9hql5gahzlmp2267p0h9qj7rkhkmq")))

(define-public crate-sql-type-0.14 (crate (name "sql-type") (vers "0.14.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1x92xskg4x6alz7c5qfir9ciqqx2n8yrgqmnfvn70kh8fw6nsyav")))

(define-public crate-sql-type-0.14 (crate (name "sql-type") (vers "0.14.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0mhabx39ss3vhqar0m6sn432y1169lsgf3pgf273mqj2qzvl40r3")))

(define-public crate-sql-type-0.14 (crate (name "sql-type") (vers "0.14.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0yh0q3y50kcgbqvklisaa2s6r8xc5cwbdz8ln8kpp25ks3ixnfqb")))

(define-public crate-sql-type-0.15 (crate (name "sql-type") (vers "0.15.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.15") (default-features #t) (kind 0)))) (hash "010cik39vw9z9xpnlrbzvskzvwlwj0s464cl6zw7qimpjx71cbba")))

(define-public crate-sql-type-0.15 (crate (name "sql-type") (vers "0.15.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.15") (default-features #t) (kind 0)))) (hash "1yy5mz836z1r8j2bccm8lryp5v1kqf22m5zzvhhr7ja6j8d6g3z5")))

(define-public crate-sql-type-0.15 (crate (name "sql-type") (vers "0.15.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.15") (default-features #t) (kind 0)))) (hash "1x97j6mp2p39h6ib3gmpd3nfw54w2krxh0mhcfyn8nvg4l00gd6j")))

(define-public crate-sql-type-0.15 (crate (name "sql-type") (vers "0.15.3") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "09i1nac8rx20550rxqrj0csdp1a5r2xdinb1hapsdf2rj651gmcb")))

(define-public crate-sql-type-0.15 (crate (name "sql-type") (vers "0.15.4") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "18d4l3y46s1adbb8w22490jraf1zs0ckvlj4ssjy932483ksbxbh")))

(define-public crate-sql-type-0.16 (crate (name "sql-type") (vers "0.16.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "111v8zxl7dlidrnr8r0rp3r399nz18bp3h9aycg41xghnsq7pvzl")))

(define-public crate-sql-type-0.17 (crate (name "sql-type") (vers "0.17.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1sqs79y0hryfsnlbzm3zfbjwndb3d8cyyng61c2fnm7nv0140gr4")))

(define-public crate-sql-type-0.18 (crate (name "sql-type") (vers "0.18.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "sql-parse") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0mpcgrl96q9fpc63iwcdv6cvdfixld2lsmwc7px92i14yrg2ndi2")))

(define-public crate-sql-xpool-0.1 (crate (name "sql-xpool") (vers "0.1.0") (hash "0f64plxg9622zacgwhng1nqny1hbj5j893i8zis0aycl4ag7075d")))

