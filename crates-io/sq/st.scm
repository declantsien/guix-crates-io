(define-module (crates-io sq st) #:use-module (crates-io))

(define-public crate-sqstransfer-0.2 (crate (name "sqstransfer") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sqs") (req "^0.42.0") (default-features #t) (kind 0)))) (hash "1rp512q3irdz4lzq7cv97d4sck8gl1m6ncp1w1qkcnnpl1nrndhx")))

