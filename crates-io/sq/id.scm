(define-module (crates-io sq id) #:use-module (crates-io))

(define-public crate-sqids-0.1 (crate (name "sqids") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.173") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)))) (hash "03zi976rvbpsm8f1zng38c354hcpx0axbghkz7hynfnb6n65j0z9")))

(define-public crate-sqids-0.1 (crate (name "sqids") (vers "0.1.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.173") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)))) (hash "0vrd89pw1i465xmj2ppw62cmq5f08kvpsdx44cfbj1diq25j7xkh")))

(define-public crate-sqids-0.2 (crate (name "sqids") (vers "0.2.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1z7y9s0i26b3pvw5ncr9wdyxb2dciz5f9g6jz54fzpaqya60r2iv")))

(define-public crate-sqids-0.2 (crate (name "sqids") (vers "0.2.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "0d5q7brvwjjnjh8hc6rs80lzlb747r3d9g8hf2j9hfppk5gddbgv")))

(define-public crate-sqids-0.3 (crate (name "sqids") (vers "0.3.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.106") (default-features #t) (kind 0)))) (hash "19827j4xayhv94r51jr7akr9i0c6y44c2yii87bs8s1699g6f58c")))

(define-public crate-sqids-0.3 (crate (name "sqids") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.192") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "02bcpkqrbz0x4hw55k7cl12aqzw9ppkydlhrzwlrjpxg79fdwcfz")))

(define-public crate-sqids-0.4 (crate (name "sqids") (vers "0.4.0") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "1pwfsfnh628w7v45fjl4p96vf6fxihx7l8xd59vqx0x9ign7jizs")))

(define-public crate-sqids-0.4 (crate (name "sqids") (vers "0.4.1") (deps (list (crate-dep (name "derive_builder") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "17b3hh66hnh1blns5g314qqrf9i3i7084bsv9sh0sksrmq88yclz")))

