(define-module (crates-io sq nc) #:use-module (crates-io))

(define-public crate-sqnc-1 (crate (name "sqnc") (vers "1.0.0") (deps (list (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "1hcjn59qwc2i2zdry46yn7r6ks1dspr3nrgi9w34r0q3k71ck93y") (features (quote (("alloc")))) (rust-version "1.66")))

