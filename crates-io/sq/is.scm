(define-module (crates-io sq is) #:use-module (crates-io))

(define-public crate-sqisign-0.1 (crate (name "sqisign") (vers "0.1.0") (hash "0kmrc32ckfzsln93jjsggv38c4s3g7j13srq4xs7a8vwlr7zc0xx")))

(define-public crate-sqisign-0.1 (crate (name "sqisign") (vers "0.1.1") (hash "0yg89y3ggvyw0adps9cfmpcc52p150rd5agqwc7b9lrkq1711nnl")))

(define-public crate-sqisignhd-0.1 (crate (name "sqisignhd") (vers "0.1.0") (hash "0r2nngamrxp1abwpyj4yv5pkymk0sw093ifz73w3sfl03rw2sx7d")))

