(define-module (crates-io ui d_) #:use-module (crates-io))

(define-public crate-uid_store-0.0.1 (crate (name "uid_store") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0y6bgik132gyn887mdi95m2w7i6r1c7hrvdqp19y7rdkwv8xh4mc")))

(define-public crate-uid_store-0.0.2 (crate (name "uid_store") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "04mhlqx406kjli34k5261g3ayqx6ii2215qczfvnfdkps9dlmvvk")))

(define-public crate-uid_store-0.0.4 (crate (name "uid_store") (vers "0.0.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0w979cdyijfah2ss1cfakdf94n4y532jn0fi2qwk0h0fxi4asni1")))

(define-public crate-uid_store-0.0.5 (crate (name "uid_store") (vers "0.0.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wp84nbiv7f34cqp3sfjgdwv1a2vgdz3f18v1s9aiy9w45jmdgn9")))

(define-public crate-uid_store-0.1 (crate (name "uid_store") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0mzp9lxx2f99ra3czpggkz0n9rfzhxp863kl71j6rcyn05wfbpiz")))

(define-public crate-uid_store-0.1 (crate (name "uid_store") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "00cz62kwjp809zbspmlm1gpsr5hll5s8pd570dwvdiqsmfd36xkq")))

(define-public crate-uid_store-0.1 (crate (name "uid_store") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0nvpa93bmx4gf3542w9sdrzgw9bfim80ykp8ihlswwil1h1fkqr4")))

(define-public crate-uid_store-0.1 (crate (name "uid_store") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1r88m7cqpl31pcs542wh9lwl690bl2v4j4gk92kz939clx7i5zhd")))

(define-public crate-uid_store-0.1 (crate (name "uid_store") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03pm4imwh2b82rz3i9wfdr63zwz6sc10y7p832y4i55f7s4blffh")))

(define-public crate-uid_store-0.1 (crate (name "uid_store") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0q52d3v9h2728il88hdw1w1wsv18g1vq1wb7k5cidzhmsh6fg84s")))

(define-public crate-uid_store-0.1 (crate (name "uid_store") (vers "0.1.6") (hash "04l3lklwyxdlrvqc2jfwf7c6rssf4wii5x1vcfyq234rn3hx6gn7")))

