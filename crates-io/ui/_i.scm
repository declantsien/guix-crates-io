(define-module (crates-io ui _i) #:use-module (crates-io))

(define-public crate-ui_image1d-0.0.1 (crate (name "ui_image1d") (vers "0.0.1") (deps (list (crate-dep (name "aflak_plot") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0c59skwyqr7ivklf921h1x74dcjfr33fhxqprjr1n7j2gpvpi3v9")))

(define-public crate-ui_image2d-0.0.1 (crate (name "ui_image2d") (vers "0.0.1") (deps (list (crate-dep (name "aflak_plot") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0lncwlsnyz9i24iddmm6r3nhfhczs6isx53jhvrbf4xvnrr0dw84")))

