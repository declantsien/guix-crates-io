(define-module (crates-io ui -s) #:use-module (crates-io))

(define-public crate-ui-sys-0.1 (crate (name "ui-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "make-cmd") (req "^0.1") (default-features #t) (kind 1)))) (hash "095kmc2nybafkdzpqyqn21lj28dgh1pbm9ccdfx9gb898m1xij06")))

(define-public crate-ui-sys-0.1 (crate (name "ui-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "make-cmd") (req "^0.1") (default-features #t) (kind 1)))) (hash "0p649yylkaxcpc9nik2hyi1kgylrll906b8csp23lzpn05rj7z5k")))

(define-public crate-ui-sys-0.1 (crate (name "ui-sys") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xf76ydsscbqidqclcnn87w9hdc98wcb4sfgi3wnz47f8qiz9wby") (features (quote (("fetch") ("default" "fetch" "build") ("build")))) (links "ui")))

