(define-module (crates-io ui ev) #:use-module (crates-io))

(define-public crate-uievents-code-0.1 (crate (name "uievents-code") (vers "0.1.0") (hash "0sgr9c9n68r858vs77jyjx4lsq56sblbzmpqfn60mx1s7c3y5dnq") (features (quote (("non_standard_intl") ("legacy")))) (rust-version "1.56.1")))

(define-public crate-uievents-code-0.1 (crate (name "uievents-code") (vers "0.1.1") (hash "0arahqfaf4bzx3b2v0mhzw90qjbkqc82jsf2ibkwf3nmbz29cd2l") (features (quote (("non_standard_intl") ("legacy") ("enum")))) (rust-version "1.56.1")))

(define-public crate-uievents-code-0.1 (crate (name "uievents-code") (vers "0.1.2") (deps (list (crate-dep (name "strum") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (optional #t) (default-features #t) (kind 0)))) (hash "1iwf9pd6lzp5c3d2x9z3n8fvnxarjdmrq8p86n4bbckhgnkp1rd8") (features (quote (("non_standard_intl") ("legacy") ("enum" "strum_macros" "strum")))) (rust-version "1.56.1")))

