(define-module (crates-io ui #{4-}#) #:use-module (crates-io))

(define-public crate-ui4-macros-0.1 (crate (name "ui4-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "15s78zjrcvl7swhvn6s0v7xzckls6djq9mk4qs2636xanv5w4kk9")))

