(define-module (crates-io ui l_) #:use-module (crates-io))

(define-public crate-uil_parsers-0.0.2 (crate (name "uil_parsers") (vers "0.0.2") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "*") (default-features #t) (kind 0)) (crate-dep (name "phf_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uil_shared") (req "= 0.0.2") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "0jphvan8vrnvx6f07fkx9zszydyfc9nmm2fal8kvax5qvdh0zsb5")))

(define-public crate-uil_parsers-0.0.3 (crate (name "uil_parsers") (vers "0.0.3") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "*") (default-features #t) (kind 0)) (crate-dep (name "phf_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uil_shared") (req "= 0.0.3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "0rcv8cqld6h6n7cpyyyhpy7i67wf6an9dwls0nddafmgswm9lkya")))

(define-public crate-uil_shared-0.0.2 (crate (name "uil_shared") (vers "0.0.2") (hash "0a8sj8dhsp0pqy0yr6khrzgg54zjr52jxlcz082j3sh1kw353zl5")))

(define-public crate-uil_shared-0.0.3 (crate (name "uil_shared") (vers "0.0.3") (hash "12jhhljcmv4icnmwwp7im4j24ggkjidnv4ysmp216h2jhfvcqsq0")))

