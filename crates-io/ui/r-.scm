(define-module (crates-io ui r-) #:use-module (crates-io))

(define-public crate-uir-core-0.0.0 (crate (name "uir-core") (vers "0.0.0") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "intertrait") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0w3dy5rigqawpl8kllb5mlbw1i8dzpsnwjw3gg11gz7zrld74crs") (features (quote (("default"))))))

