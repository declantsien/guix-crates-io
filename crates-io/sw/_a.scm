(define-module (crates-io sw _a) #:use-module (crates-io))

(define-public crate-sw_auth-1 (crate (name "sw_auth") (vers "1.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0z2h99q5xxnyy0bnfkkk2mzmc9i94rb4b0fch3yi1wgv4w9zwcfz")))

(define-public crate-sw_auth-1 (crate (name "sw_auth") (vers "1.1.1") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1dr872h75kh4rmk0xkqnfnwdv19sf9k9akp7j5v8b2337djs9wsm")))

(define-public crate-sw_auth-1 (crate (name "sw_auth") (vers "1.1.2") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0f302vizqh35q43cy57pb86j4yfmsrkm06574b83pg19ik9apwqn")))

