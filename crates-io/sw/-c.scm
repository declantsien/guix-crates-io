(define-module (crates-io sw -c) #:use-module (crates-io))

(define-public crate-sw-composite-0.1 (crate (name "sw-composite") (vers "0.1.0") (hash "134fgh7vz88xmxns40k5ymcwicfsiib13s2c0r3ihxdb4pb59lxg")))

(define-public crate-sw-composite-0.1 (crate (name "sw-composite") (vers "0.1.1") (hash "1yzz2fngbz0arkhrl0w3sqq9sxydpp1irraw89sjn9fyqzhwn7pb")))

(define-public crate-sw-composite-0.1 (crate (name "sw-composite") (vers "0.1.2") (hash "0rc6g72mzvxlm2zadi0ivp6aaa4vih34r5scxlh9l9bibvd2lmfi")))

(define-public crate-sw-composite-0.1 (crate (name "sw-composite") (vers "0.1.3") (hash "0v2amrvai330ch9az7agm2ihmmwvhih2y9mhhv79j0kcgnssxaw4")))

(define-public crate-sw-composite-0.1 (crate (name "sw-composite") (vers "0.1.4") (hash "1kx9yi1bs4a81ajyhr5fq5lh2glimkzrjw8phq9snkfs7pjm2qrw")))

(define-public crate-sw-composite-0.2 (crate (name "sw-composite") (vers "0.2.0") (hash "0rr3r4kx0z8sw9wj4g4gbjii21jl1ff1nav6jkymyg2gszp0ljy7")))

(define-public crate-sw-composite-0.2 (crate (name "sw-composite") (vers "0.2.1") (hash "1i8prb66k8c5gbacyxf93zz61bg38vhhh3x747250bmjl56mjki4")))

(define-public crate-sw-composite-0.3 (crate (name "sw-composite") (vers "0.3.0") (hash "04bimg4gsc6nwh1288jsf5msmbsnh81466yrigqribf17n8r3vys")))

(define-public crate-sw-composite-0.4 (crate (name "sw-composite") (vers "0.4.0") (hash "1v0j9kq5idg4mqdwjmpxykx8b25zjsk8r6gl9rh3dyh4p96g0rj4")))

(define-public crate-sw-composite-0.4 (crate (name "sw-composite") (vers "0.4.1") (hash "02bj2xkv3b33qqmljblq5202fg44dnns1b6j99qdwla5cpadp5rd")))

(define-public crate-sw-composite-0.4 (crate (name "sw-composite") (vers "0.4.2") (hash "0xa60a974qwa4m1f2jqdakysyj22nh5mj2r096107svpl0v28wz8")))

(define-public crate-sw-composite-0.4 (crate (name "sw-composite") (vers "0.4.3") (hash "0l4l2xk7p3hzwxk8ggbadv23kn7sd4cypwli5xzrgh0z395mnbi0")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.0") (hash "122j4ky9pibjfqbxp8xijfhmnrm53nwfzd4kyrnwd6p3gjx5gs65")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.1") (hash "0fqbayxr6lpqna1q1v904b2fdpwzqsg8y747yd24v7c8877ps2pj")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.2") (hash "04cghhwlki88fxk7i8knhqw11dbpr95sca3ljy1zr5afrxrnn11r")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.3") (hash "14wh4fpaxp052zdph26l71d8yb9q8vqbln4y5v9cm54m1lh8yqx1")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.4") (hash "1g3ihcpgq8bhizgynbf5xsyhc59m84qzjnrlzmnahsy0jflrg3gz")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.5") (hash "0nqbkc0434pggq7nxzbnqjh236z4yq6l8kgaac6d7957mfi48zir")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.6") (hash "07x98fgqrxz1n7lhfrd6r1dvyswjk1iirxdmvfh42xfkq51bkxfn")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.7") (hash "1isjy19591r9jzw66dfq94f7b38ssq6j9sv5vdix426z1a9fsfah")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.8") (hash "1wdk332k748j8z65pg0vg8w1ylvjfk9jpdkrhxqg16w30rvl4zmn")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.9") (hash "0q6ywfsr3cc21q222fqrddna4w9ilzdryz51i6gkr3m1v3gzb2bz")))

(define-public crate-sw-composite-0.5 (crate (name "sw-composite") (vers "0.5.10") (hash "1cmrizpmxi91g3ys53jmdqz5qd2k0j5s5xviy0b65a2d15aigfjy")))

(define-public crate-sw-composite-0.6 (crate (name "sw-composite") (vers "0.6.0") (hash "1r243x28qjr6d0zg2864wla2qc49z0mjdcjaqlscdpd4z2j5rvxv")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.0") (hash "16sfys00k37pcdm1h4icwwivs1i5bdy7czz3zgjvgnrbfzbp3gbi")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.1") (hash "1mgpk2cf5r081p5yalbihzkzx7dkibwlbjvmi9bv05fys098a9r6")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.2") (hash "1lpif6ybdvxrvvdd7r4xmcb0h8dj353wky3da3n7zrf770bnz8sh")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.3") (hash "1x8pm6f66dw13r2garhzmad0p9qrqh1iz6ifynwd23dh48d1q79v")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.4") (hash "04y5bjiisf13znx2ykp2rrpsv4w42igrak0w1clzmw49i7h4pby1")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.5") (hash "1564a8mgxiiamhykvvyfqr4bfpbpys2crpfjr1r8dihd3brkwfqf")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.6") (hash "154vnfd1d252rah7d2vz58ipw1c4dr823yddhiv01i6diaw015a8")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.7") (hash "0wv9z5ib2dxc1va7023lr57kvkx8j4kaasz2x4bh8z6caiifl57b")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.8") (hash "1141w9w6g0ci2hi54zipvlvh9wmwbwg8jg36d76995iwig24hvsd")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.9") (hash "1fkxwhbvwrr9yr5m6xm605v2csnr71lnzk3glwk61lnba5szcz8s")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.10") (hash "1zrdvr6zfw4sm3ybr136bjdgnlgw1ykl9w24mbk59slcch64wh8z")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.11") (hash "00dgxgi03vl96z5hq9n7fr2zvjkwsbqsf57qns1hjvasmlxzniz6") (yanked #t)))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.12") (hash "0kdc4bx2vfl7i4n8pxggnbqwskmq8293d82bcawvizhnqfisnhm0")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.13") (hash "0iys92m4p5qpzk5w53kxsipgs0k0vkib5xw68j5fmk45cyvdbw4z")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.14") (hash "12w06zv25lcd4k1f69sbicnx6yv3lrr2f53592zyhj0pgc2795p3")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.15") (hash "07wb17xy665hhf0kc99h80f4k52chy7fffir5kvf6h45scflz38b")))

(define-public crate-sw-composite-0.7 (crate (name "sw-composite") (vers "0.7.16") (hash "1m3i4vdmixzcsnbffiws8jimsxdq1n3346kkmmha1bxljmwgpj4s")))

(define-public crate-sw-croaring-0.3 (crate (name "sw-croaring") (vers "0.3.11") (deps (list (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "roaring") (req "^0.5.2") (default-features #t) (kind 2)) (crate-dep (name "stack-croaring-sys") (req "^0.3.10") (default-features #t) (kind 0)))) (hash "03s62c8ksah0nkmqz6294nr4m6rlqrb5fzvvrn50lbmgc6pr2477")))

