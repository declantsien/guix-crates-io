(define-module (crates-io sw im) #:use-module (crates-io))

(define-public crate-swim-0.1 (crate (name "swim") (vers "0.1.0") (hash "0yh0wb1rgik0ybl056zgan3x7czvra5l8wkc6adi2pgx89ilaknb") (yanked #t)))

(define-public crate-swim-0.2 (crate (name "swim") (vers "0.2.0") (deps (list (crate-dep (name "swim-core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "swim-util") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lkclzfsm7jz0r8vh05j3pdild34cjl44as7yisnnrzqba7imksr") (yanked #t)))

(define-public crate-swim-0.2 (crate (name "swim") (vers "0.2.1") (deps (list (crate-dep (name "swim-core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "swim-util") (req "^0.2") (default-features #t) (kind 0)))) (hash "147z7shpbb3r4flbgffyl7gpwip3wrg7sv5cpjkpb4b7x35wxykd") (yanked #t)))

(define-public crate-swim-0.2 (crate (name "swim") (vers "0.2.2") (deps (list (crate-dep (name "swim-core") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "swim-util") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0shlz9png4lbh0y96330kz56cgf33xmx9xgdw93q41mx970vayys")))

(define-public crate-swim-0.3 (crate (name "swim") (vers "0.3.0") (deps (list (crate-dep (name "swim-core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "swim-util") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1lq8i6iia8cs8fnan9ixcfb00vz1892cgkq99757q82akfdy7dkc")))

(define-public crate-swim-app-0.1 (crate (name "swim-app") (vers "0.1.0") (deps (list (crate-dep (name "mockall") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1p8cy70bgsxcr59apcmb0ic6p5p70dz5gzsl8mwh5xb9kc1y198p") (yanked #t)))

(define-public crate-swim-contrib-0.2 (crate (name "swim-contrib") (vers "0.2.0") (hash "0yfs22dnsy8rcmyy2snb6rcrrydii23ifnn165z7dv3q9miz39q5")))

(define-public crate-swim-contrib-0.2 (crate (name "swim-contrib") (vers "0.2.1") (hash "1sq6amva6i0471cx02flf79kl9jgzizdr3107640xfhqkag782hk")))

(define-public crate-swim-contrib-0.3 (crate (name "swim-contrib") (vers "0.3.0") (hash "0anw05kyc2g506niylm7j0dqvgsajw1kmmiaj87j9ff8jj5fh42p")))

(define-public crate-swim-core-0.2 (crate (name "swim-core") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("http1" "http2" "server" "tcp"))) (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "routerify") (req "^3.0") (features (quote ("hyper-http1" "hyper-http2"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kgf624sb5r3psz7w4y56a6p304zbni862b3yisyabiw8pw8aicq")))

(define-public crate-swim-core-0.2 (crate (name "swim-core") (vers "0.2.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("http1" "http2" "server" "tcp"))) (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "routerify") (req "^3.0") (features (quote ("hyper-http1" "hyper-http2"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "04g3cmghcw3j7in6mbw4xg63phrgi19lxb2372272xrvggl5gbxp")))

(define-public crate-swim-core-0.3 (crate (name "swim-core") (vers "0.3.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("http1" "http2" "server" "tcp"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "routerify") (req "^3.0") (features (quote ("hyper-http1" "hyper-http2"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "yansi") (req "^0.5") (default-features #t) (kind 0)))) (hash "1v035dnkm6rmx02qvi4msc5dqbsxpwyl05p680ma0bbzhdzvqdmw")))

(define-public crate-swim-db-0.2 (crate (name "swim-db") (vers "0.2.0") (hash "0qd81z4ls82fbr0mxf0k8glw385qxkpi8xx7mjlhhg0bzf35pigi")))

(define-public crate-swim-db-0.2 (crate (name "swim-db") (vers "0.2.1") (hash "1p5q5dz47h0404kwmqb8vzpvmxj4rh1scgbcdnpfjw4mlajri2ns")))

(define-public crate-swim-db-0.3 (crate (name "swim-db") (vers "0.3.0") (hash "1dp9aifnr19r02lqaj78b8flqyg67a514k4m0sadpgzdx61raxc3")))

(define-public crate-swim-middleware-0.2 (crate (name "swim-middleware") (vers "0.2.0") (hash "0xhxr5pb7f2q5mvysllkfszsn86li8lwfc5rzb2afq4swrl1zp80")))

(define-public crate-swim-middleware-0.2 (crate (name "swim-middleware") (vers "0.2.1") (hash "08miix3x0nzpwqy7r76scw1i76p9735fm334s7hrmnsakxvckbni")))

(define-public crate-swim-middleware-0.3 (crate (name "swim-middleware") (vers "0.3.0") (hash "1g54x7rv4hw4z30dhajhmjp5xhky1d799dc8bg8420rzrcr2fdgw")))

(define-public crate-swim-template-0.2 (crate (name "swim-template") (vers "0.2.0") (hash "02wd8gfxni001dpf4w76220ja17h2bjx8565kyh1cvwp28wwy9d4")))

(define-public crate-swim-template-0.2 (crate (name "swim-template") (vers "0.2.1") (hash "0zs4h2kfdwyqk4wl3gcxikl9bkb2nsv9077ybzsca74v7rjiqj7i")))

(define-public crate-swim-template-0.3 (crate (name "swim-template") (vers "0.3.0") (hash "0s927m9yh20sqz3ll4342qrlj1linpcaq0ybdf7mk82m5gby71yn")))

(define-public crate-swim-util-0.2 (crate (name "swim-util") (vers "0.2.0") (hash "00xffqz0fhanxm6dzcaqafk781i9b322pkl62v2sbcz44adqmrky")))

(define-public crate-swim-util-0.2 (crate (name "swim-util") (vers "0.2.1") (hash "15zsmcr623m296q89mkdj2cxkajyiv2zi0c4v9nf8in8k3i87kag")))

(define-public crate-swim-util-0.3 (crate (name "swim-util") (vers "0.3.0") (hash "1wxkjk0y90qjclv2mk13svl62zhjqgx1vdh2lkyy1hdkmz6w4490")))

(define-public crate-swimlane-0.1 (crate (name "swimlane") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "httpmock") (req "^0.6.8") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0vjzgyi3jvgbkqg0r752h54aj6228b394cygm0ybspmrrc9v54z3")))

(define-public crate-swimmer-0.1 (crate (name "swimmer") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.6.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "1hbisd8dkjpxkxa8kjhq030cgi9qf6mqgfg76vzb46y0kj8wk67j") (features (quote (("smallvec-impls" "smallvec") ("hashbrown-impls" "hashbrown"))))))

(define-public crate-swimmer-0.2 (crate (name "swimmer") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "lifeguard") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.6.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "thread_local") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "17ryrmhv7v549lgl94kj7yqga6g51rf8gadlzdggd2i5p16ysa2g") (features (quote (("smallvec-impls" "smallvec") ("hashbrown-impls" "hashbrown"))))))

(define-public crate-swimmer-0.3 (crate (name "swimmer") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "lifeguard") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.6.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "thread_local") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0mz1w4f0nn0gavmhx3mawm1nslg75ah55n0p72qw94lqj9hfygcn") (features (quote (("smallvec-impls" "smallvec") ("hashbrown-impls" "hashbrown"))))))

(define-public crate-swimming-0.1 (crate (name "swimming") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 2)) (crate-dep (name "lock_pool") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "057m4gzpjvdg7qx2rwy2mqs0ck0d9hslh1b0z9da6rc74nihyr57") (features (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

(define-public crate-swimming-0.1 (crate (name "swimming") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 2)) (crate-dep (name "lock_pool") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qj8xd2kvw29svahkmx9qniz2gmy1ch0z7bd13glfw45laymp70n") (features (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

(define-public crate-swimming-0.1 (crate (name "swimming") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 2)) (crate-dep (name "lock_pool") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "13j4bj989bbrg5brafn1a37jia7xvxcv24kzdm4k5r91biamywm6") (features (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

(define-public crate-swimming-0.1 (crate (name "swimming") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 2)) (crate-dep (name "lock_pool") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0y7zpzxhccbl1r2i4axm2kmxhr6b7d0w76a5k7frz1spf66vi49n") (features (quote (("std" "lock_pool/std") ("alloc" "lock_pool/alloc"))))))

(define-public crate-swimming-pool-0.1 (crate (name "swimming-pool") (vers "0.1.0") (hash "0n0d7hw3p8ksyvm137vpq2w62kw6v01aflmjn0nhkwz3d201zxfc")))

(define-public crate-swimming-pool-0.1 (crate (name "swimming-pool") (vers "0.1.1") (hash "1la9jms4c8288gyphsmpd55fp4s7gi0sqp198ybapwgm84f3js3h")))

(define-public crate-swimming-pool-0.1 (crate (name "swimming-pool") (vers "0.1.2") (hash "0fvbg9my872nlw852ncm1vcda93w3pdcqnkrd5n2d26al5msdjpw")))

(define-public crate-swimming-pool-0.1 (crate (name "swimming-pool") (vers "0.1.3") (hash "1b074lcxv8bh9lng6bppkh20g0ch36vy2367mqz9llcqqv837nlp")))

(define-public crate-swimming-pool-0.1 (crate (name "swimming-pool") (vers "0.1.4") (hash "1n60ifjmqia8c6mj0i4g4pvydkyla2b1lnd7d4slgir21aw9qjlj")))

