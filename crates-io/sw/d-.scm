(define-module (crates-io sw d-) #:use-module (crates-io))

(define-public crate-swd-rs-0.1 (crate (name "swd-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1b8779j8lz74mbz5plfjmidhqjjxgnhqngja8inm457hk2x39mbc")))

(define-public crate-swd-rs-0.1 (crate (name "swd-rs") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "14m01gp3sfmnjsh7l6q2kbbh2i4wswiy1gnmvg22v9pdhcnjm5z1")))

