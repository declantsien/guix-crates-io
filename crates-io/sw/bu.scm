(define-module (crates-io sw bu) #:use-module (crates-io))

(define-public crate-swbuf-0.1 (crate (name "swbuf") (vers "0.1.0") (hash "07n67vifn9gnw61g8pzfj5m70a7bbv0yr5bnxnynp9dchyw1dndq") (yanked #t) (rust-version "1.61.0")))

