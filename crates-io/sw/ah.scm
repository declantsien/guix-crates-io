(define-module (crates-io sw ah) #:use-module (crates-io))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.0") (hash "1ygbbw0qa10wzakih6glipn2v8af4zm0z68lpkvlsv26kb16zln2")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.1") (hash "0dsvig9cmfxyq27qlvbwy3bshcapqwjr0j4050bday0lkb5mn2p5")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.2") (hash "0fsqfl2dil9sbrdh4fwg4gmvj0x5wqzghqiba1jl17gcqi9vl3zy")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.3") (hash "1c0jxkjb3vadg5m93xkl8yy13zlrgcc6i47hinkfmnvh370yn9cx")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.4") (hash "1bvpr4h9i18j1gyxbv0kfsbq0l40f9kqccfvq3shbjmwz0fam7vn")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.5") (hash "1ibx455d743yzv56zd3h6q8swi5c0b9hgngrd2y7x79cfqykm6pm")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.6") (hash "0127vrahj3m562jxazqs0cqln4vhkbj3896l8khjkbrwhm0l6r5x")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.7") (hash "01rzbhyn2gwajgdsmzmqlr2v0w3cavd0vqsdripj3l2zazjmqsww")))

(define-public crate-swahili-dsl-0.1 (crate (name "swahili-dsl") (vers "0.1.8") (hash "0wahrxra5w8g1y8bp1xrnxcbc3bnyn9sani1934f88afcwjldvdl")))

