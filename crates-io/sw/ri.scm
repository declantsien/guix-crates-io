(define-module (crates-io sw ri) #:use-module (crates-io))

(define-public crate-swrite-0.0.1 (crate (name "swrite") (vers "0.0.1") (hash "05zi6jyfnk840bkbgwdwr1y4qhvhsvrykkikfzfi4609r4cxpccq") (features (quote (("osstring"))))))

(define-public crate-swrite-0.0.2 (crate (name "swrite") (vers "0.0.2") (hash "14faf57hi4prjhfbhl7r5bllqy7316qqlf0fb58nv226sp7ppalc") (features (quote (("osstring"))))))

(define-public crate-swrite-0.1 (crate (name "swrite") (vers "0.1.0") (hash "00vsicglylq4qq6dc497jdgzfnxi5mh7padwxijnvh1d1giyqgvz") (features (quote (("osstring"))))))

