(define-module (crates-io sw ot) #:use-module (crates-io))

(define-public crate-swot-0.1 (crate (name "swot") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.10") (default-features #t) (kind 1)) (crate-dep (name "phf_shared") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^6.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "validator") (req "^0.14") (default-features #t) (kind 0)))) (hash "1i5xvpzirfhrkfn2w5gp0vxhr27i75xxvb1dn3h9m0dp95rjsg99")))

