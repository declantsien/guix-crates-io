(define-module (crates-io sw ym) #:use-module (crates-io))

(define-public crate-swym-0.1 (crate (name "swym") (vers "0.1.0-preview") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "jemallocator") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lock_api") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "00lbag878127i29d9a02p0cgkaqvii2v2hbx7vjbjn2q6h572nxx") (features (quote (("stats" "lazy_static") ("default") ("debug-alloc"))))))

(define-public crate-swym-htm-0.1 (crate (name "swym-htm") (vers "0.1.0") (hash "0769kyy4js51jd7r2nn4pmc7v4apvk2bixahkqrxcfx5dwdar4ma") (features (quote (("rtm") ("htm") ("default"))))))

