(define-module (crates-io sw iz) #:use-module (crates-io))

(define-public crate-swizzle_3ds-0.2 (crate (name "swizzle_3ds") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "04y3q1qr5ywwb00r215h6l44m8adwrzldrj1vm4z876yaz3s47x7")))

