(define-module (crates-io sw m3) #:use-module (crates-io))

(define-public crate-swm341-pac-0.1 (crate (name "swm341-pac") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req ">=0.6.15, <0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "03vqfhvixyzj48hfzf867phnfyv4zfpd223rzqbiljxn1aliji36") (features (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-swm341-pac-0.2 (crate (name "swm341-pac") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req ">=0.6.15, <0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "11ca27cxfjamknd22g1a9wkp4r1bzbb03whzpfcy11rimxl836jf") (features (quote (("rt" "cortex-m-rt/device"))))))

