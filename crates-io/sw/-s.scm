(define-module (crates-io sw -s) #:use-module (crates-io))

(define-public crate-sw-sw-logger-rs-1 (crate (name "sw-sw-logger-rs") (vers "1.0.0") (deps (list (crate-dep (name "all_asserts") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1bv79mbxic0s8dxgz00wqb9il59zv2dzynm9wkschqzkv6afa2xl") (yanked #t)))

