(define-module (crates-io sw he) #:use-module (crates-io))

(define-public crate-swhen-0.1 (crate (name "swhen") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "073k9qhnb5g1ssqbd9v0lzj9iakv3m8dg77ijwvkvfy2p7ig67xc")))

(define-public crate-swhen-0.1 (crate (name "swhen") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "010pyfibrywbs2xi4rai295plhn38hb84d4aizqv8nkxmsmxsgcg")))

(define-public crate-swhen-0.1 (crate (name "swhen") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1sbvgy0k1nca42yf1vx1msbapvxrwa4shx2sw3r8jpdc6zy7flb1")))

