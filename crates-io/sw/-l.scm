(define-module (crates-io sw -l) #:use-module (crates-io))

(define-public crate-sw-logger-rs-1 (crate (name "sw-logger-rs") (vers "1.0.0") (deps (list (crate-dep (name "all_asserts") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0q96p8kw2csl494h2r1m99r539pqb30s1xzn1bnil3yrqfgrqw66")))

(define-public crate-sw-logger-rs-1 (crate (name "sw-logger-rs") (vers "1.0.1") (deps (list (crate-dep (name "all_asserts") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "03dyk00hnqk6zg66axlxgjh5symjl6p4dl84amsbp8j81gqy5c7g")))

(define-public crate-sw-logger-rs-1 (crate (name "sw-logger-rs") (vers "1.1.1") (deps (list (crate-dep (name "all_asserts") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1f2lqksh7wlda3dhc929zg2by0sk3vimg4laxpm04d1j12ba937b")))

(define-public crate-sw-logger-rs-1 (crate (name "sw-logger-rs") (vers "1.1.2") (deps (list (crate-dep (name "all_asserts") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "03jcvhnmrs096i55c1csmjlx1xy1q23x1dd9hld2qilyjxnlhdld")))

