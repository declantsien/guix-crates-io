(define-module (crates-io sw ct) #:use-module (crates-io))

(define-public crate-swctx-0.1 (crate (name "swctx") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0g69lm1d1cmj4lxpyiqqr8cj6s69b7wjh9dlmg189k46sdm5ls8h") (features (quote (("dev-docs")))) (rust-version "1.56")))

(define-public crate-swctx-0.1 (crate (name "swctx") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1cgaa5wvi9pb9kabnj1b0mlrxpqc6lfc9h35qxmyblxc4sj77xp8") (features (quote (("dev-docs")))) (rust-version "1.56")))

(define-public crate-swctx-0.2 (crate (name "swctx") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1jrgm5icj1xm9zbqwkpsdvqr3ciqjjayr9y87665252lh7wsmv1w") (features (quote (("dev-docs")))) (rust-version "1.56")))

(define-public crate-swctx-0.2 (crate (name "swctx") (vers "0.2.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.31.0") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "14qh07s3xkraas943f3bzzwrd8ga0j5x1j5hlc39n9s1iyja7vvs") (features (quote (("dev-docs")))) (rust-version "1.56")))

(define-public crate-swctx-0.2 (crate (name "swctx") (vers "0.2.2") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1mdwbn3rkz4qsdvdjn566dp0n73kay6x6xmr3mrs98xrzvmayzb4") (rust-version "1.56")))

