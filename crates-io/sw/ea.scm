(define-module (crates-io sw ea) #:use-module (crates-io))

(define-public crate-swear-0.1 (crate (name "swear") (vers "0.1.0") (deps (list (crate-dep (name "boxfnonce") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "080g86dfzlipjw0y13plbjbgkv74mf9xvvkphra6mdk5xmxxnlv4")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.0") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "10vabm95db1ymjpz1cbhq2vs4b42a59iwwwcpdqj4wg158mfxvfg")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.1") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "1r57cr4dxgq490mvv1alsphlnc6smqpiwk6vbygspajmlml3irr8")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.2") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "169mhnfmq1gk9lxis57j2b85sg8x424ayx5gfpa4jn7rb9sk26ap")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.3") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "1va57cd9x3jbllqgbifksr17biwc051j1gnmn31w5547s7pjni9h")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.4") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "1mlwy91717k2l6fkxcss2397svvxhnp41w36vkga8f9fznzwpqrj")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.5") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "1gi7jyw9hksrs56cd1zmi1fq8iifrhi53v71cwhwa0ivfrclzqk8")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.6") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "0k94hc4b00aya162izbvj3nq5ajp3zm4csbhn0v9566csaba7l5n")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.7") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 0)))) (hash "0qwi6ql4h07r9cjz6ymp56375c590qji8lhpcz8227yjvigk93k0")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.8") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "08mjf18a1d4s4dn4yi8fz95q9jv95x9kpcs3fdak7r0bd2zhlgp8")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.9") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "06p11i522lkc072wflxz1dnhcmn7fm5bagsbp15xc1s4rvznpss7")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.10") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "06pv2n7kk9c86p7fzlw5dcpqbs6wn0cfl811l70jxlwh4546i445")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.11") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "0zq07sxclqjm4w8hw1kz2grmwvr0dg6i6qpfhm2zkqfw8gqi2z07")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.12") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "081a1qw84b3s0k9f9ksa1zk9lcbp0fyx5khbrh3v70wcsj6gsbfy")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.13") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "0ss478syvbvw9mp3pziavpp6bz02klji7kghwpr84q6iq30ljdmj")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.14") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "1m27lj78ykbifq0hb23v8dqad7yc84n41mjszzad7mb1clc8qlfc")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.15") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "1rrizawp95hip81mc6m5ihrqli4bq0hmx0iv2y1zsbf2afljfgy7")))

(define-public crate-swears-0.1 (crate (name "swears") (vers "0.1.16") (deps (list (crate-dep (name "pancurses") (req "^0.16") (features (quote ("win32"))) (default-features #t) (kind 0)))) (hash "0jhqbadyschq3477x56gb4xc86xhhccm8lb132vvxvhlxmyi223x")))

