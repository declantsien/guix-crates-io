(define-module (crates-io sw nb) #:use-module (crates-io))

(define-public crate-swnb-complex-0.1 (crate (name "swnb-complex") (vers "0.1.0") (hash "1bypr5q5hvasxf6jn4nk0d04qh2slyl26saf6kx49ig3s7w0vjjz")))

(define-public crate-swnb-complex-0.2 (crate (name "swnb-complex") (vers "0.2.0") (hash "1mnilym4n9nig88m56944xrkj4526np4yn894593rk8hz41mpkgc")))

(define-public crate-swnb-complex-0.2 (crate (name "swnb-complex") (vers "0.2.2") (hash "1zy1j7y8jmrlb54dkhl7wzh5kccrkl04gijr18zqhigq7pfx6f2a")))

(define-public crate-swnb-timer-0.1 (crate (name "swnb-timer") (vers "0.1.0") (hash "1gdwcw71qkhsa9i0cls2zjpqxr5bjyad36hp8iw9hk6flqz948d3")))

(define-public crate-swnb-timer-0.1 (crate (name "swnb-timer") (vers "0.1.1") (hash "17l5nsrx0jl0v2xlh680dsg18cdrhfcy7ambl5m18a9g9ga44nxi")))

(define-public crate-swnb-timer-0.1 (crate (name "swnb-timer") (vers "0.1.2") (hash "12yypfs72ffca2bhg6dmp0fpdn1550rb7k3yp6f9z7zmdz26nqbq")))

(define-public crate-swnb-timer-0.2 (crate (name "swnb-timer") (vers "0.2.0") (hash "1sjv8kcwgl4b0gafhzqdbri5k2vi1irv81zjs4i2dbaz81r506b4")))

