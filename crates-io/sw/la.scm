(define-module (crates-io sw la) #:use-module (crates-io))

(define-public crate-swlang-0.0.2 (crate (name "swlang") (vers "0.0.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0pkmvl8w7p2znm5sbdzplrvfvarnapnf1mwnffdyc5bx9s9cmsln")))

(define-public crate-swlang-0.0.3 (crate (name "swlang") (vers "0.0.3") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "11bpw5hw91cfg8r2b216i59v5fawmv5m1lxkyvk72fbmkm0dzw5m")))

(define-public crate-swlang-0.0.4 (crate (name "swlang") (vers "0.0.4") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "1sm454ym1diw20p397wf7263mwsvcr79v4bglfvqiq7y6xsvfm6n")))

(define-public crate-swlang-0.0.5 (crate (name "swlang") (vers "0.0.5") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0yl5i3d2pyfw943abrvg7bh8fplxbcc32vxk1mpcdhn87rsl59yn")))

(define-public crate-swlang-0.0.6 (crate (name "swlang") (vers "0.0.6") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "09nw2i5yisxzgx225993rbbyyzn0cjca5gy8ajq045mbn5qcdc2j")))

(define-public crate-swlang-0.0.7 (crate (name "swlang") (vers "0.0.7") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0g2ill2qj9n7ls06hh68r8c357af05sf9hb2kfrn79jd773b3n60")))

(define-public crate-swlang-0.0.8 (crate (name "swlang") (vers "0.0.8") (deps (list (crate-dep (name "actix-rt") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h19j71dvi6lf57kwaml0dmmici3fnagl1c1khpaysgzzs9j8h4w")))

(define-public crate-swlang-0.0.9 (crate (name "swlang") (vers "0.0.9") (deps (list (crate-dep (name "actix-rt") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "cargo-tarpaulin") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03cv55hwfjjdyqxh8qji0fmdphy2vrwyxmr4hy00v0f51n5zci1x")))

(define-public crate-swlang-0.0.10 (crate (name "swlang") (vers "0.0.10") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03f1n1p76g4rr7n6h454a2q5jyh7sjfx40nvn68vm7g5ajslisg2")))

(define-public crate-swlang-0.0.11 (crate (name "swlang") (vers "0.0.11") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dj874lpxz763if61nksqf3daplimbhldsspmb23gkxga25vzgp2")))

(define-public crate-swlang-0.0.12 (crate (name "swlang") (vers "0.0.12-pre-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^7.2.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0f7v7kmz2xwq0mj44zcbvp2dwkbcjsxqnjawq13k4dnybz3zi4wc")))

