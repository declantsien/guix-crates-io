(define-module (crates-io sw il) #:use-module (crates-io))

(define-public crate-swil-0.1 (crate (name "swil") (vers "0.1.0") (deps (list (crate-dep (name "raw-window-handle") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "x11rb") (req "^0.12.0") (features (quote ("allow-unsafe-code" "dl-libxcb" "resource_manager" "xkb"))) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "xkbcommon") (req "^0.7.0") (features (quote ("x11"))) (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0il1ywxszniysw24f3bm2wnnlqmgj7v3n1yl5cvyrkfqlcvcjg33")))

(define-public crate-swil-0.2 (crate (name "swil") (vers "0.2.0") (deps (list (crate-dep (name "raw-window-handle") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "x11rb") (req "^0.12.0") (features (quote ("allow-unsafe-code" "dl-libxcb" "resource_manager" "xkb"))) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "xkbcommon") (req "^0.7.0") (features (quote ("x11"))) (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0dgq47706w4ymchd4crk8wlhx1gfb021m37fzzkjycgvggi6myi2")))

