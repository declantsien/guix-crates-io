(define-module (crates-io sw ed) #:use-module (crates-io))

(define-public crate-swedish_iban_tool-0.0.1 (crate (name "swedish_iban_tool") (vers "0.0.1") (hash "10ba91zghjni360w94giv9jva1wacy1khyw2sgfnrpnnk6ajvc1b")))

(define-public crate-swedish_postal_codes-0.2 (crate (name "swedish_postal_codes") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.7") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dwkyz24pc85a9qp6sg6xl84lw07455yvc6y89zjf1nfsj4gp1hj")))

