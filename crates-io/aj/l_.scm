(define-module (crates-io aj l_) #:use-module (crates-io))

(define-public crate-ajl_logger-0.1 (crate (name "ajl_logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "1a0lm39c0xgxnd91kqdi65gr7j4qr8qspnjgcj84zlmy09mpcbp8")))

(define-public crate-ajl_logger-0.1 (crate (name "ajl_logger") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "07361kap15i7kn8kzwjbj59684nsc6c5n22j58ak47hngi6v0jax")))

