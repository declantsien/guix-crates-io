(define-module (crates-io aj im) #:use-module (crates-io))

(define-public crate-ajimu-0.1 (crate (name "ajimu") (vers "0.1.0") (deps (list (crate-dep (name "egg-mode") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.25") (default-features #t) (kind 0)))) (hash "1imlvn770s842lv4y0v39slklkxcqhvc0q9359lqza8f6axw71x0")))

