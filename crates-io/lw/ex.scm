(define-module (crates-io lw ex) #:use-module (crates-io))

(define-public crate-lwext4-sys-0.1 (crate (name "lwext4-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0m3wm2yym6gjjg7y1nsyqg30wqhcjdv5vmw90zw7ij8gszvn8n7y") (links "lwext4")))

