(define-module (crates-io lw ge) #:use-module (crates-io))

(define-public crate-lwgeom-0.0.0 (crate (name "lwgeom") (vers "0.0.0") (deps (list (crate-dep (name "lwgeom-sys") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0xckp2kf81d098yv91a02ymfcin4paw0m1889jp7sai5cbfhabxm") (yanked #t)))

(define-public crate-lwgeom-0.0.1 (crate (name "lwgeom") (vers "0.0.1") (deps (list (crate-dep (name "lwgeom-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0am4352kpa9kxcrhj3iwh2fplkaq0l0zhzk701i6h9drs8s8y81w") (yanked #t)))

(define-public crate-lwgeom-0.0.2 (crate (name "lwgeom") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lwgeom-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "004sb83v0v9k9wfw96f1kndqikvxk1wxf4ims9ddizla0yy4fv3g") (yanked #t)))

(define-public crate-lwgeom-0.0.5 (crate (name "lwgeom") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "lwgeom-sys") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k1k1fbzkw5aygfwfqfic7dwxjkkcl9xa25d14p30r9sv1yl1ikh") (yanked #t)))

(define-public crate-lwgeom-0.0.6 (crate (name "lwgeom") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "lwgeom-sys") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fx7rz4nf3wm0y4cjdc3h5gyb7d64sbpnsr5054i7w9vax0ppmg8") (yanked #t)))

(define-public crate-lwgeom-0.0.7 (crate (name "lwgeom") (vers "0.0.7") (deps (list (crate-dep (name "foreign-types") (req "^0.5") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "lwgeom-sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nqgpvwrzw7nl954ffjsypwgwvxjgp8hyq7ad8p44j878gd6qnlv")))

(define-public crate-lwgeom-sys-0.0.0 (crate (name "lwgeom-sys") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "0wgx7aafq8v1n81v2y3zqprgrfkgi4x8ivh5rriq0f7zqms5nxc2") (yanked #t)))

(define-public crate-lwgeom-sys-0.0.1 (crate (name "lwgeom-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 1)))) (hash "1wgb5jsljdjc663c98rjfnir6bzj1sg931n40v0njjdzgxsrgsfq") (yanked #t)))

(define-public crate-lwgeom-sys-0.0.2 (crate (name "lwgeom-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.3") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1qh4da5a1nmlrns499q3jk7lw6fl0vb7q0wmpgj3a704khlbmby3") (yanked #t)))

(define-public crate-lwgeom-sys-0.0.4 (crate (name "lwgeom-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "080xsawl4crrrc8nycl269r9v4sgnrkb65y27z8pvc1qh1b1j0b7") (yanked #t) (links "lwgeom")))

(define-public crate-lwgeom-sys-0.0.5 (crate (name "lwgeom-sys") (vers "0.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0qvgyaz00jmy351imz8r44ad31cpamdkdcrlamglxv2xhv9vkbnl") (yanked #t) (links "lwgeom")))

(define-public crate-lwgeom-sys-0.0.7 (crate (name "lwgeom-sys") (vers "0.0.7") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0r2a8n2knnvid5zzli7lpxdfzqr92mzk4dzycf472nxayfdicp92") (links "lwgeom")))

