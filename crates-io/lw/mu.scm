(define-module (crates-io lw mu) #:use-module (crates-io))

(define-public crate-lwmud-0.1 (crate (name "lwmud") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00l1pny5za0r0lw6w3h1avr6m9k4zvabw2fr8p8zbj5nc1pi06g2")))

