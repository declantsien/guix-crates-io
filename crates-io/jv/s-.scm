(define-module (crates-io jv s-) #:use-module (crates-io))

(define-public crate-jvs-packets-1 (crate (name "jvs-packets") (vers "1.0.0") (hash "1r5h8s1na75q4bkdfgq2ymsbd4z56lnka3wzip8wg8i1a6fgc0sa") (features (quote (("jvs_modified") ("jvs") ("default" "jvs" "jvs_modified"))))))

(define-public crate-jvs-packets-1 (crate (name "jvs-packets") (vers "1.0.1") (hash "1dszzyw90syz5lkqxcg9mjk9nwy6c6ifnihrgpqbbvc42mbiqkwl") (features (quote (("jvs_modified") ("jvs") ("default" "jvs" "jvs_modified"))))))

(define-public crate-jvs-packets-1 (crate (name "jvs-packets") (vers "1.0.2") (hash "07iplb8lf1v2iridg1zcknhcw4ra574n6zrsk3q0y7by9mm35vfn") (features (quote (("jvs_modified") ("jvs") ("default" "jvs" "jvs_modified"))))))

