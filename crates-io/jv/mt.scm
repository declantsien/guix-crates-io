(define-module (crates-io jv mt) #:use-module (crates-io))

(define-public crate-jvmti-0.0.1 (crate (name "jvmti") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "jni") (req "^0.18") (default-features #t) (kind 0)))) (hash "02ms8kfmi9sr6si9056ia8bw8bgxpb23qx0gz2l1nx81fb4hinli")))

(define-public crate-jvmti-0.5 (crate (name "jvmti") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "0.4.*") (default-features #t) (kind 0)))) (hash "1cigq6bb42lhhsz0vni6r2j5pvvv73rb1556jm26nd0rfpn7kb67")))

(define-public crate-jvmti-sys-0.1 (crate (name "jvmti-sys") (vers "0.1.0") (deps (list (crate-dep (name "jni-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1m7ljc00hinzaknk55skdk0qp5y2c5nqncr3k5dpjk3yr4vg254w")))

(define-public crate-jvmti_wrapper-0.1 (crate (name "jvmti_wrapper") (vers "0.1.0") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "jvmti-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lp1qxqzwgip43yzfn41xvs1dfgyz15p2msdmr5vii2bc2bmc8d8") (yanked #t)))

(define-public crate-jvmti_wrapper-0.1 (crate (name "jvmti_wrapper") (vers "0.1.1") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "jvmti-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0rnfwk2mnkkb63slfcyy3nsqagghcd1hnm19dxzxnwsja5ifsfbx") (yanked #t)))

(define-public crate-jvmti_wrapper-0.1 (crate (name "jvmti_wrapper") (vers "0.1.2") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "jvmti-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0s35pfqhvwcd9amhflliaskx60za935ymaii7wc3zbfjxwy3fnnf") (yanked #t)))

(define-public crate-jvmti_wrapper-0.1 (crate (name "jvmti_wrapper") (vers "0.1.3") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "jvmti-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0cy70bbllb6j91xx1mi3nx4lg7l08dy3hggg7kpc1j96z4x77r8s")))

