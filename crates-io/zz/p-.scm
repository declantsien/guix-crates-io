(define-module (crates-io zz p-) #:use-module (crates-io))

(define-public crate-zzp-tools-0.1 (crate (name "zzp-tools") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "zzp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "031jzggm32ddjrhvi7krz4ybyz9n385pwhwksypsv8djnpcvz0wb")))

