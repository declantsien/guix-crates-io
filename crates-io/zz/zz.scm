(define-module (crates-io zz zz) #:use-module (crates-io))

(define-public crate-zzzz-0.0.0 (crate (name "zzzz") (vers "0.0.0") (deps (list (crate-dep (name "zzzz_rt_logic") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1lcdj163xgs56i20qssvpwwmckyiygygxykgzhd9zqv7y9wyxvh5")))

(define-public crate-zzzz_rt_logic-0.0.0 (crate (name "zzzz_rt_logic") (vers "0.0.0") (hash "0250f17ak15c8rslyc1n83cj69b1z8qsigh3ffn8y3m237iw8ri0")))

(define-public crate-zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz-0.0.0 (crate (name "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz") (vers "0.0.0--") (hash "19h7fljh4pg56sapl7rm0a8mkzrvvh22zf3dmv07phjs6rrsjgpz") (yanked #t)))

