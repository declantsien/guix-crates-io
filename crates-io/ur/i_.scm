(define-module (crates-io ur i_) #:use-module (crates-io))

(define-public crate-uri_encode-1 (crate (name "uri_encode") (vers "1.0.0") (hash "098rgs5l26ys82kxkxqfqpghpmrfxgiaddxj92k20frmknabfirp")))

(define-public crate-uri_encode-1 (crate (name "uri_encode") (vers "1.0.1") (hash "1svwz3w4lpldcsvnxz99ng0vy4vjqly2qsd2pjyq91vgfcfbbb1l")))

(define-public crate-uri_encode-1 (crate (name "uri_encode") (vers "1.0.2") (hash "0ly4sdkalxkvkng80vc2si85iydxmk6by3gg6cnqqdrfr082w16i")))

(define-public crate-uri_parser-0.1 (crate (name "uri_parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0zl93kvq1c2x5ki3fv409059bix03qbi73qwq6lp7zkhxnb2h8ni")))

(define-public crate-uri_parser-0.2 (crate (name "uri_parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1y9kkbr4vsp636da78i42q77m1rinfs258nd8vjnjjy6szn9005s")))

(define-public crate-uri_path_router-0.1 (crate (name "uri_path_router") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0sgxsyvzs42540m9rmzfl7898m5kdlk2fikinqm7pvrxj8q3bzyi")))

