(define-module (crates-io ur lm) #:use-module (crates-io))

(define-public crate-urlmatic-0.1 (crate (name "urlmatic") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1nc8mngh5p2zsqq2nlnkfnja9jcq3z8akm1kzlx3f4adi3yxcxb7")))

(define-public crate-urlmatic-0.2 (crate (name "urlmatic") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0i0s0jlq75w4a2djcdm6xwa2yfbjjqfr1sj5lrv173p4yy0nsy3r")))

(define-public crate-urlmatic-0.2 (crate (name "urlmatic") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "182p910zacsl16i6vfjfk04kqzw8k3i1y26jbxvmnnn60j80ds3g")))

(define-public crate-urlmatic-0.3 (crate (name "urlmatic") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0khw451xb4w01m22g6mlmf86hrcwbs6mvwrm088v26s31izh6a8q")))

(define-public crate-urlmatic-0.4 (crate (name "urlmatic") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1znhsgjhvk087f4gwh0yszsp7s4y3v6bqgx65rjlm2jzqymxmkqi")))

(define-public crate-urlmatic-0.5 (crate (name "urlmatic") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1rlh6qx4a5ppqvz3az2s4l4jmzy1b5sijhg35cinaqa5jqi89ix8")))

