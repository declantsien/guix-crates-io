(define-module (crates-io ur ld) #:use-module (crates-io))

(define-public crate-urldecode-0.1 (crate (name "urldecode") (vers "0.1.0") (hash "0ryyxv44c498ffpk5xpqp0jazis1xchkrxnyqhnqf7gqg7wc8f16")))

(define-public crate-urldecode-0.1 (crate (name "urldecode") (vers "0.1.1") (hash "1p4f5kdqk2azgmr10nsx3i2fdnc67fl98pw8ff588s52kfvjw307")))

(define-public crate-urldecoder-1 (crate (name "urldecoder") (vers "1.3.2") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "die-exit") (req "^0.5.0") (features (quote ("red"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("fs" "macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.3") (default-features #t) (kind 0)))) (hash "0p5z9jriq6x085zawy05q99g34zpmx2frv9927jabprgk9ddq8bd")))

