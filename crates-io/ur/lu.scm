(define-module (crates-io ur lu) #:use-module (crates-io))

(define-public crate-urlutils-3 (crate (name "urlutils") (vers "3.3.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "osutils") (req ">=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "130svj6gwf0dmilb9c8b2rzkl8sn66djb5g6n1xhap1s9miwr2s7")))

