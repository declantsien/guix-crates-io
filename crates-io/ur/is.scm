(define-module (crates-io ur is) #:use-module (crates-io))

(define-public crate-uris-0.1 (crate (name "uris") (vers "0.1.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "00xjiix7wns0iahrbvkydskhai74a187rw7mgfzvylfiwd242ilp") (yanked #t)))

(define-public crate-uris-0.1 (crate (name "uris") (vers "0.1.1") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "188bjbsf5wz7ypcqa7dhlyf65lizl8s9walsaizl6hzzr7hniy32")))

