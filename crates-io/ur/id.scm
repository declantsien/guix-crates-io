(define-module (crates-io ur id) #:use-module (crates-io))

(define-public crate-urid-0.1 (crate (name "urid") (vers "0.1.0") (deps (list (crate-dep (name "urid-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "195672gs136vczn1r4hkjg5vfa7vdzr26bzv6lwhk0z7cvbvaa38")))

(define-public crate-urid-derive-0.1 (crate (name "urid-derive") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vyzywa7br0j0wr8137wign7b10zc9h2pc3m88vb9qwmm28lfcpm")))

(define-public crate-urid-derive-0.1 (crate (name "urid-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i1nf0sgq4ai051h17s9msaavl3jfzdmdlsy8455pr88y0pfx7l1")))

