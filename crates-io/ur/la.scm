(define-module (crates-io ur la) #:use-module (crates-io))

(define-public crate-urlable-0.1 (crate (name "urlable") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1qab5kk91jlyw4v2z48qfcmba5669pgk93kpi77i07nkdfzacw3f")))

(define-public crate-urlable-0.1 (crate (name "urlable") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1yd0yxz8knhv4mvqqa5klzr4c16sc8k8dh7bm8xifmjbmss9y4ah")))

