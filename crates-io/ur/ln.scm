(define-module (crates-io ur ln) #:use-module (crates-io))

(define-public crate-urlnorm-0.1 (crate (name "urlnorm") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "0v64c4v92nw0kdq0nl7hqh815siw7mqmx7h7xdnaw4nka4z19lja") (features (quote (("nightly"))))))

(define-public crate-urlnorm-0.1 (crate (name "urlnorm") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "0dlgc53x3si54y4yiy0ssj2dx0z2zwsvya0swrfq1fkgzjaz5gc2") (features (quote (("nightly"))))))

(define-public crate-urlnorm-0.1 (crate (name "urlnorm") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "1rf4dsvgckfiyrs8qcapd3ap4bz0jx49sj27frszzv5khwirs2m1")))

(define-public crate-urlnorm-0.1 (crate (name "urlnorm") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "1lyf6nljl1zkjzw5afyp8843625qhzddazsrkdjhbm4jjllpwzqw")))

(define-public crate-urlnorm-0.1 (crate (name "urlnorm") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "1q3pv19jav5ia2apkpj5k011d0gk9cyka4r64h831zx3dnx9640d")))

(define-public crate-urlnorm-0.1 (crate (name "urlnorm") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "16a9b9vlai5gwn384fq96ywj06l3bc3z3kkfhxqfyp0zjnkzzybw")))

(define-public crate-urlnorm-0.1 (crate (name "urlnorm") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "07blgy4vjxiwga7qm46mz36aqjhh1jiwx63s5nc8z3yriy2dhkkg")))

