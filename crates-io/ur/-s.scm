(define-module (crates-io ur -s) #:use-module (crates-io))

(define-public crate-ur-script-0.1 (crate (name "ur-script") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (kind 0)) (crate-dep (name "num") (req "^0.4.0") (kind 0)))) (hash "10af36gkymz39wlx0jiqw379b7fxplwzwxrdkzhh5l5cpd5my5r7") (features (quote (("std" "num/std" "nalgebra/std") ("libm" "num/libm" "nalgebra/libm") ("default" "libm")))) (yanked #t)))

(define-public crate-ur-script-0.1 (crate (name "ur-script") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (kind 0)) (crate-dep (name "num") (req "^0.4.0") (kind 0)))) (hash "14m8qgcj0w6idpq7vxjlp7lagp8wxy0rcgzbhmbwhbfkf5378q73") (features (quote (("std" "num/std" "nalgebra/std") ("libm" "num/libm" "nalgebra/libm") ("default" "libm"))))))

(define-public crate-ur-script-0.2 (crate (name "ur-script") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (kind 0)) (crate-dep (name "num") (req "^0.4.0") (kind 0)))) (hash "00xzb7wbwn87dh9lpzyyxnpd5lk41dirivzpzwcx7ia6ay014nln") (features (quote (("std" "num/std" "nalgebra/std") ("libm" "num/libm" "nalgebra/libm") ("default" "libm"))))))

