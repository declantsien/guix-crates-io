(define-module (crates-io ur sh) #:use-module (crates-io))

(define-public crate-urshort-0.1 (crate (name "urshort") (vers "0.1.1") (deps (list (crate-dep (name "axum") (req "^0.5.16") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s3mpn4k0h3cq3xzsakm4zfsp7p5sg0yjwsqzy2vzkrcm8q2hzkl")))

