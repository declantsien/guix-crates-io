(define-module (crates-io ur l_) #:use-module (crates-io))

(define-public crate-url_encoded_data-0.5 (crate (name "url_encoded_data") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "19qdnsqcris01v8wg54x87fx82pj4y187269nwfnvvz5fap45v32")))

(define-public crate-url_encoded_data-0.6 (crate (name "url_encoded_data") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1sqp266xv9hkjbb01s1vvz6hc5y3wc1i83f0rif4akl5wgyvzzgs")))

(define-public crate-url_encoded_data-0.6 (crate (name "url_encoded_data") (vers "0.6.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0wjrl0p0x0sa64m91sfwn3arvanmcsmvbkw5nk1y48kvclsxy7sy")))

(define-public crate-url_open-0.0.1 (crate (name "url_open") (vers "0.0.1") (deps (list (crate-dep (name "url") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("shellapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0y6wwcbgixiqfyq81lrnfvp8viachp6sa5ba9v395rinbrvql2xf")))

(define-public crate-url_open-0.0.2 (crate (name "url_open") (vers "0.0.2") (deps (list (crate-dep (name "url") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("shellapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0v5dyaplzhawr6snsrxw4j32sv7whi09hqxai64is0hw5x3v1idq")))

(define-public crate-url_params_serializer-0.1 (crate (name "url_params_serializer") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "0g97j0z5rzcb675gf8w03xqqj601jpn69s90zgvzh1fck0k7narr")))

(define-public crate-url_params_serializer-0.1 (crate (name "url_params_serializer") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "0xdc5iyap04nm0015c8hrglf65jswzs7swx6i8kkg4znhmnpd8v4")))

(define-public crate-url_parser_cli-4 (crate (name "url_parser_cli") (vers "4.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 2)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0rx51qy0sxc85ikjzc8xprcn4kicyfdk4z2inygk7g60bddpsh9g")))

(define-public crate-url_parser_on_rust-1 (crate (name "url_parser_on_rust") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "00r6r9ig614x6l57kvg0n55kc6h0a5rjy5jq7idd6qs3ijd03yw4")))

(define-public crate-url_path-0.1 (crate (name "url_path") (vers "0.1.0") (hash "14xwbn04f5zh1vm8m783kp55j964zrm4s1y5g38469iskmlz5vmp")))

(define-public crate-url_path-0.1 (crate (name "url_path") (vers "0.1.1") (hash "1kf8aqz65hw1y5j0wszrxac1jfxp6cf6k4w434dwxrfk0ymzp14z")))

(define-public crate-url_path-0.1 (crate (name "url_path") (vers "0.1.2") (hash "10ni2a6nsf8nnfnxxn41rli393f0d0fg38zs6i2ha6hsghqwzjar")))

(define-public crate-url_path-0.1 (crate (name "url_path") (vers "0.1.3") (hash "10n8s6gj7h4wxhzl3m8k3dbdx4b1zpf6myjlzfs3pz5pg647liqh")))

(define-public crate-url_serde-0.1 (crate (name "url_serde") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0y20qdkw9zs7l1ymk7i7c1kgrzx4z6pvyixfv4bx924h3y1hlw6m")))

(define-public crate-url_serde-0.1 (crate (name "url_serde") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0c72jl9g848aa050islq1lz2cpv0hzpkyfsypydsnvxhxqgh92cc")))

(define-public crate-url_serde-0.1 (crate (name "url_serde") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1fch1wnhp7bvwixlggc43i1pb2zdsaq5radsv6ab74hnqndgr8k8")))

(define-public crate-url_serde-0.1 (crate (name "url_serde") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "01pimxrwwfwya45qw785nz2r5sqakq938s8ng60pfc5fcw5brpb4")))

(define-public crate-url_serde-0.2 (crate (name "url_serde") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1snxgdzlcj5mpnbkpnzm533l6830qf9hrmmxshizhlpfy6cx1rvl")))

(define-public crate-url_validator-0.1 (crate (name "url_validator") (vers "0.1.0") (hash "1xrsngc8234f7m68jrglwqyiqfdsbdjs84l4fys7pgcdqsj2qi5y") (yanked #t)))

(define-public crate-url_validator-0.1 (crate (name "url_validator") (vers "0.1.1") (hash "11qj5xirqdalmf5w9rk0mr65zrx82pj88zaccixhykx03jm95q94") (yanked #t)))

(define-public crate-url_validator-0.1 (crate (name "url_validator") (vers "0.1.2") (hash "0isiryp804xyg0sakznkzwiqafrniff9rb1fr7d447g6wd7k5yw0") (yanked #t)))

(define-public crate-url_validator-0.1 (crate (name "url_validator") (vers "0.1.3") (hash "0r3vqrxwifvbrs5a6zfymb0d2yh96ihy4zfihbzmjnm781ln9ixp") (yanked #t)))

(define-public crate-url_validator-0.1 (crate (name "url_validator") (vers "0.1.3-2") (hash "1bs0mjz6yr3z02rsqxpcvlkjdlycsmgsppsl483s3d56aiy550n2") (yanked #t)))

(define-public crate-url_validator-0.1 (crate (name "url_validator") (vers "0.1.4") (hash "0vrzs1k64z7z0lnmq62a98i2kmfdq94b5bamibdwcdhrk9sylphh") (yanked #t)))

