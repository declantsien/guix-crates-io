(define-module (crates-io ur it) #:use-module (crates-io))

(define-public crate-uritemplate-0.1 (crate (name "uritemplate") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "01qs5r2pmzx1rc88a24kzvx8nlqnlwnljlfxyjxbaazdxc7h52lw")))

(define-public crate-uritemplate-0.1 (crate (name "uritemplate") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "09h3z5a37xmp8yvg5iwcm6qrc4acmqpkh58ca0hxv6s7y76h1ail")))

(define-public crate-uritemplate-0.1 (crate (name "uritemplate") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)))) (hash "0j97yaxhygv8q55wnjya683c9dllwfahmyq0shphrm40fcna7sh1")))

(define-public crate-uritemplate-next-0.2 (crate (name "uritemplate-next") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "136i9f5vj46z4xqwba3jnvg263gfi2v2zcpcn5aq4lizzk8ripmw")))

