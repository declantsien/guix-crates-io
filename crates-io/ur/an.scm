(define-module (crates-io ur an) #:use-module (crates-io))

(define-public crate-urandom-0.1 (crate (name "urandom") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dataview") (req "^0.1") (kind 0)) (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "146xk93jp6lw7gbxzcz41f0biz06jfgqjbapmh4y4nrcaq8zbslg") (features (quote (("default" "getrandom"))))))

(define-public crate-urandom-0.1 (crate (name "urandom") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dataview") (req "~1.0") (kind 0)) (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "116k3h6hkl3rnrv89w6kk7ba16d6cgln4lnblfk3alfwxgh1mn0w") (features (quote (("std") ("default" "getrandom" "std"))))))

(define-public crate-uranio-0.1 (crate (name "uranio") (vers "0.1.0") (hash "0wh44wfww0lgs2bkv38hxxwscdhzv78d4sjmzcd17ivbzymm5k3d")))

