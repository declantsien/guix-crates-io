(define-module (crates-io ur le) #:use-module (crates-io))

(define-public crate-urlencode-0.1 (crate (name "urlencode") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1") (default-features #t) (kind 0)))) (hash "1ai519ghf5mhg22yap3rvc3w49rivz89zkxhfmy1yridq16r9b7q")))

(define-public crate-urlencode-0.1 (crate (name "urlencode") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1") (default-features #t) (kind 0)))) (hash "1k15h7bycfip0xk8zwgcva59lwm4qn6vgla9j529p53qf59ar647")))

(define-public crate-urlencode-0.1 (crate (name "urlencode") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1") (default-features #t) (kind 0)))) (hash "01c6aqswrsl6f5gbyl9whl0dnkbqw53caw43gikimcgrvvkipfzq")))

(define-public crate-urlencode-1 (crate (name "urlencode") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0h08mpzrr9y5cgwz3nlg207ifv6936pbf7dsk183jmzdbi7r4n62")))

(define-public crate-urlencode-1 (crate (name "urlencode") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1bsmqxc839ygrdk10r7ykkphsx2fazi2mqzs0pkdas5jk8hsi4n9")))

(define-public crate-urlencoded-0.1 (crate (name "urlencoded") (vers "0.1.1") (deps (list (crate-dep (name "bodyparser") (req "*") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "*") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "1gvfsbwymip4087sakrwlspnkm4b7gai4cqfkdzbfxyklyiicyzl")))

(define-public crate-urlencoded-0.1 (crate (name "urlencoded") (vers "0.1.2") (deps (list (crate-dep (name "bodyparser") (req "*") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "*") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "0n1a72ng6i8rp5rydgnh589nm028my9zn3d1xk8w4agxpxbvs4pk")))

(define-public crate-urlencoded-0.2 (crate (name "urlencoded") (vers "0.2.0") (deps (list (crate-dep (name "bodyparser") (req "*") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "*") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "12zq34g64f86inc6nw5yw5z2fr99vgjvzq70pddjyr6s6c8diq0q")))

(define-public crate-urlencoded-0.2 (crate (name "urlencoded") (vers "0.2.1") (deps (list (crate-dep (name "bodyparser") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2") (kind 0)) (crate-dep (name "plugin") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cgksl9nv0fjwdgfhklvdgaivc15547srki9wdgh44pg8fm9hd4d")))

(define-public crate-urlencoded-0.3 (crate (name "urlencoded") (vers "0.3.0") (deps (list (crate-dep (name "bodyparser") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mqsdlx9fggwy3w9g94rf6hi8lnni0qi3v441h5fv7yyizlqgf6z")))

(define-public crate-urlencoded-0.4 (crate (name "urlencoded") (vers "0.4.0") (deps (list (crate-dep (name "bodyparser") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x4qdv6x8x2ckv9yszxylrbffivphklr3lqf7098qrlpnw646vbm")))

(define-public crate-urlencoded-0.4 (crate (name "urlencoded") (vers "0.4.1") (deps (list (crate-dep (name "bodyparser") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.2") (default-features #t) (kind 0)))) (hash "0bhfqx11s9bck4fxk8bpg2899abnmchwbaqcyp6vbvdyl39z5p2x")))

(define-public crate-urlencoded-0.5 (crate (name "urlencoded") (vers "0.5.0") (deps (list (crate-dep (name "bodyparser") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.2") (default-features #t) (kind 0)))) (hash "06fkf73fpcs19p7q7jx714r55azi82pvdkdiaf52kxyn6s370a4c")))

(define-public crate-urlencoded-0.6 (crate (name "urlencoded") (vers "0.6.0") (deps (list (crate-dep (name "bodyparser") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "iron") (req ">= 0.5, < 0.7") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "1qrkblcj3gpz256d5fci9g9ig20mxlavy25gj6p612qi740zalha")))

(define-public crate-urlencoding-0.1 (crate (name "urlencoding") (vers "0.1.0") (hash "0in43cfgqm42r56y29y90slw7n2h8zqajbafa4lhgr7vm1whc30b") (yanked #t)))

(define-public crate-urlencoding-0.1 (crate (name "urlencoding") (vers "0.1.1") (hash "1lk6lvbjwpkv2kvvj9zypz8mgaaadn3ashp1s3fr5kri0dvv90wn") (yanked #t)))

(define-public crate-urlencoding-0.2 (crate (name "urlencoding") (vers "0.2.0") (hash "0xnjw1mjjixnp7830fyx7n4xvkdmhnwdc3agij5xkakrfz9xvzif") (yanked #t)))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.0.0") (hash "1v9piz5dwrkiyk0w9cfr55y9rqnr90rw9r52wmblrfx854b5dwrx") (yanked #t)))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.1.0") (hash "10pia3ibfhjwdsiipvvwrfn82lg17k70n1jj75jlf7xmmfi1mj0v") (yanked #t)))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.1.1") (hash "14sm5c8idb5jzib8dwf85p5yhd65vxjh946p80p49d2j6fsjw8y9")))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.2.0") (hash "1c016jzdvkswmnpza0iilpms66hgp07qwvamvl0f9hbndcz1jn7q")))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.3.0") (hash "15gpq5968jmjra5srlr6c7djx5sg4x9yxdrbav6drsfa4yv33cfa") (yanked #t)))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.3.1") (hash "1830ba256w8hwzl4azjpfcm2md5411b9lr00ds12chdp12lxf2ly") (yanked #t)))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.3.2") (hash "1z6igpqmb3kwjppbbwymy4l8x3nz2lrzwzzlhkw1jim2bb3r3qr1") (yanked #t)))

(define-public crate-urlencoding-2 (crate (name "urlencoding") (vers "2.0.0-alpha.1") (hash "06pp8sa00xdrqvjizc6a37dyhs68fa1ccy9kl71qdvqygr3w0379") (yanked #t)))

(define-public crate-urlencoding-1 (crate (name "urlencoding") (vers "1.3.3") (hash "1yqgq2qigm1s8zyv23j0422j4vn20ppnnizx9b7p629sw1sh27ss")))

(define-public crate-urlencoding-2 (crate (name "urlencoding") (vers "2.0.0") (hash "0a6m0vahk16s382rcfgc3x4amw5zbcrangg4qqfddfzwxncvqnyr")))

(define-public crate-urlencoding-2 (crate (name "urlencoding") (vers "2.1.0") (hash "08cq5w84imxrpyifhmx719026dzjih29gdq0ncsb1fcs08qhkfb8")))

(define-public crate-urlencoding-2 (crate (name "urlencoding") (vers "2.1.2") (hash "1agfwfzw66krnpqjiv4mhjqq1fcqgwdzikd7x9v835inz4kp9nz8")))

(define-public crate-urlencoding-2 (crate (name "urlencoding") (vers "2.1.3") (hash "1nj99jp37k47n0hvaz5fvz7z6jd0sb4ppvfy3nphr1zbnyixpy6s")))

(define-public crate-urlendec-0.1 (crate (name "urlendec") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.8") (default-features #t) (kind 0)) (crate-dep (name "assert_fs") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1.5") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.0") (default-features #t) (kind 0)))) (hash "0n7zbfy3iqnk2ws8rz9cpmkd9xqwvrv3k1w3lyf6qfjlhg9k0vip")))

(define-public crate-urlex-0.1 (crate (name "urlex") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0cf3y30g8xj82ba1jyjavq7qi01n4yjhqg27k5skq7ld9nll9pbh")))

(define-public crate-urlex-0.1 (crate (name "urlex") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "19r73x8l9hw433mcbl5fy6gvqw0afirxq51ll06qxlmn9mzws2xv")))

(define-public crate-urlexpand-0.0.1 (crate (name "urlexpand") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1fm898wdz1v0s3kynhdlvyhacp3pjl8v08g6nin792qpb0m3jwl2")))

(define-public crate-urlexpand-0.0.2 (crate (name "urlexpand") (vers "0.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0pzzziym9x07av4147rdq0cfw0qaxwjbmfwvzcz59vaycp41yqsv")))

(define-public crate-urlexpand-0.0.3 (crate (name "urlexpand") (vers "0.0.3") (deps (list (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0y08wwfbbxdc2mjlc6vhj5y7wxa1iyrpgh64335w7z4g9625wawr")))

(define-public crate-urlexpand-0.0.4 (crate (name "urlexpand") (vers "0.0.4") (deps (list (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0fgp6bp1xqrrl4lzcwhscqyx6kdshhsgjzv7zp63chxgpsi56zqw")))

(define-public crate-urlexpand-0.0.5 (crate (name "urlexpand") (vers "0.0.5") (deps (list (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1f7163j4nkwpc444lsmay08ib2gb7vyxr3sax04a23f094xhdhw8")))

(define-public crate-urlexpand-0.0.6 (crate (name "urlexpand") (vers "0.0.6") (deps (list (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1pp3b5fwcy0vhh241vhbg29kkfmw7yr1y67xfmav1l031m43lh64")))

(define-public crate-urlexpand-0.0.7 (crate (name "urlexpand") (vers "0.0.7") (deps (list (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1k6aq157v636j4xxkf1gyv36c726vc9580jidgfnkx0xa0af7ni5")))

(define-public crate-urlexpand-0.0.8 (crate (name "urlexpand") (vers "0.0.8") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1dc3zqnj7nkscgrmij6h9yzvzn6nknn57d6nqvqdzq6bgsg76107")))

(define-public crate-urlexpand-0.0.9 (crate (name "urlexpand") (vers "0.0.9") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1jmf3in89z2bp4lf5bisp5bf0dknahva96h99ws3d1hfz4gzc3ll")))

(define-public crate-urlexpand-0.0.10 (crate (name "urlexpand") (vers "0.0.10") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1ivigffpi7mvl6wyf86azia8np680xn9cwgz1k6hnc931g3m2bfp")))

(define-public crate-urlexpand-0.1 (crate (name "urlexpand") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0skgvc60d037xx8qkqkwp9j7pfi3yn681ggq4pmgj12ai6nkj31v")))

(define-public crate-urlexpand-0.1 (crate (name "urlexpand") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "01nk2maxcsp6gjf39ir92qjq2wqwlrqik9g4930iz9ns6sk220pq")))

(define-public crate-urlexpand-0.1 (crate (name "urlexpand") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "12lk6c7ankmm7rb55nhpr5hwnqhm640g96w3jq9gz9aryhmb9c6h")))

(define-public crate-urlexpand-0.1 (crate (name "urlexpand") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1igrfb2njn1xrgrxg1y95crqq6qa41aypngi2j0jilr6j7zfnmis")))

(define-public crate-urlexpand-0.2 (crate (name "urlexpand") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1v5ggjh4rlsss7fkw09riz18m6pfhq17fqlclmk9fv211022b0kx") (features (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2 (crate (name "urlexpand") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1m03rlij667x3jyp4vhivyf8fqg1q570g7yfcaldcn71g4b5a7nl") (features (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2 (crate (name "urlexpand") (vers "0.2.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0xnrzcyrdhkys5qf4h84nzb9hlyl50975llvkhryakhg98xvlw9g") (features (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2 (crate (name "urlexpand") (vers "0.2.3") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1i396ph9wwqqx1n0gkp3116ad0j9cknxkr8icrrcaywbvr0r40nl") (features (quote (("blocking" "tokio"))))))

(define-public crate-urlexpand-0.2 (crate (name "urlexpand") (vers "0.2.4") (deps (list (crate-dep (name "base64") (req "^0.21.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("native-tls-vendored"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1mw0fsz6afj42pviiqpw88c9w7rz8krif81b30f38b2ajq63a074") (features (quote (("blocking" "tokio"))))))

