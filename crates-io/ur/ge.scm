(define-module (crates-io ur ge) #:use-module (crates-io))

(define-public crate-urgent-0.1 (crate (name "urgent") (vers "0.1.0") (hash "12533b9v4g4y2knrv2lfcv36i6jb03gv7x1i5p47ab1zayp51ihd")))

(define-public crate-urgeopts-0.1 (crate (name "urgeopts") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0cdrxc25c4v4diqqfcprhdad1fmwbkd5mwh9mg0bmikcfq2rxjil")))

(define-public crate-urgeopts-0.1 (crate (name "urgeopts") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1ahalhknd9fjrwf5nsnisw61w17frb1q3ms7x241vsf18b6vk94r")))

(define-public crate-urgeopts-0.1 (crate (name "urgeopts") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0dyn3vq1ccgwrc3pnb6w7lfwffknbwwck2y86l1sx12kl3b67jdv")))

