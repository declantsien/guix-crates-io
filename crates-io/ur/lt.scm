(define-module (crates-io ur lt) #:use-module (crates-io))

(define-public crate-urltemplate-0.1 (crate (name "urltemplate") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1zyhjr3sl0wz09ilc2dk7y5fycgry2yi3shy2kxb4caq9vrha48y")))

(define-public crate-urltemplate-0.1 (crate (name "urltemplate") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "150m18gy3jzvwrrr03k8zi3c9890rd0zkq58yn7fy0jybhfa66hz")))

(define-public crate-urltemplate-0.1 (crate (name "urltemplate") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0ki5bimmc8dp8lbyyc3z64yfrh0jggdhzbxq2a4mwz5qlp687qaz")))

(define-public crate-urltemplate-0.1 (crate (name "urltemplate") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0vlxfqjdjf508nyv6pn25xibwcyia85zj9df4wnbabqyx9b483hc")))

(define-public crate-urltemplate-0.1 (crate (name "urltemplate") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.86") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1wdp4j3rd8vaf8irv4cz1x893cdzk1k3r130p637jz7r7bd7ncq9")))

(define-public crate-urltemplate-0.1 (crate (name "urltemplate") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.86") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1aavrw56jbf47yqdsyff3d214yiqc1c8dm59j48hngxx9l60aqbc")))

