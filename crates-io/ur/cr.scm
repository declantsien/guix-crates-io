(define-module (crates-io ur cr) #:use-module (crates-io))

(define-public crate-urcrypt-rust-0.1 (crate (name "urcrypt-rust") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)))) (hash "11snb9zm37fwdxjfmzdd5j80xhldsgny2gs00mqif1c5dhmjd7yz") (yanked #t)))

(define-public crate-urcrypt-sys-0.1 (crate (name "urcrypt-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)))) (hash "099x4wqhpp2ifqd5wn2np8mn5a7gx1z9x6w7ia4lkh7rqlc64q3d")))

(define-public crate-urcrypt-sys-0.1 (crate (name "urcrypt-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)))) (hash "069h8w11a439axgmd9ysbrc42g89wx55xiv7xdc38yjjbbwm3myf")))

