(define-module (crates-io ur #{20}#) #:use-module (crates-io))

(define-public crate-ur20-0.1 (crate (name "ur20") (vers "0.1.0") (hash "07z72dzz9qp267lpchn7x8gyclbgrb5w1dwjqh525qn6hp4j1xz8")))

(define-public crate-ur20-0.1 (crate (name "ur20") (vers "0.1.1") (hash "05v6xskd7blnhvmnf8vp3vbnj91lan6wcphkzwhywi42137cqwna")))

(define-public crate-ur20-0.1 (crate (name "ur20") (vers "0.1.2") (hash "1z5qvzkgknih7ym8wgxy8hyd2wsfjmcql7r4yx3i9a86cpmydgr4")))

(define-public crate-ur20-0.2 (crate (name "ur20") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1lajyvj0ws1kxkx1vj3qhxza95hrqqfy0c0jc305zkf14ym8mpva")))

(define-public crate-ur20-0.3 (crate (name "ur20") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qcnbvcggl4z7j96wsa74zpfvvaqsi5y4l5cqmxzp946xbn9awvq")))

(define-public crate-ur20-0.4 (crate (name "ur20") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jlcf1dl3v0hlpxl22j9q3mxdj6iy6gip0in0bfjnazamm60bsjp")))

(define-public crate-ur20-0.4 (crate (name "ur20") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "09qf5gydc6rmi5cpjhnh0v4fqqv21bd4296mwvlp6d1wy7yanms0")))

(define-public crate-ur20-0.4 (crate (name "ur20") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j52k769061ysay9dsrq3snq4ckk2r4mix1ri0w76s5qkfdg5549")))

(define-public crate-ur20-0.5 (crate (name "ur20") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ngbi2r1hz7hhx1apip24j10qpzfxlc9qk8hhbbyp7hpr3s87avk")))

(define-public crate-ur20-0.5 (crate (name "ur20") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dg5l43m639kyx3wp0d6qb6k9xsx9372ra2b48p2cq5rbki3glr0")))

(define-public crate-ur20-ethercat-0.0.0 (crate (name "ur20-ethercat") (vers "0.0.0") (hash "1lawlw6byxhhgsq6m7id778psyx1chwrkyrh7qc8507s2a668a19")))

(define-public crate-ur20-modbus-0.1 (crate (name "ur20-modbus") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "^0.2") (features (quote ("tcp"))) (kind 0)) (crate-dep (name "ur20") (req "^0.3") (default-features #t) (kind 0)))) (hash "0k43xj73pcvinyf17x8813804a98wnwikyqz8m1bnm9q5ipknrca")))

(define-public crate-ur20-modbus-0.1 (crate (name "ur20-modbus") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "^0.2") (features (quote ("tcp"))) (kind 0)) (crate-dep (name "ur20") (req "^0.4") (default-features #t) (kind 0)))) (hash "0689rgpclr9x9mvaqmfzaa6qxfqpdjqwpp93vra1ivqgd8pkc31d")))

(define-public crate-ur20-modbus-0.1 (crate (name "ur20-modbus") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "^0.2") (features (quote ("tcp"))) (kind 0)) (crate-dep (name "ur20") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bdcb0jhgc4k63qrq75wcg244c23wqshvg7xwkh2rkhpsbfsjsg9")))

(define-public crate-ur20-modbus-0.2 (crate (name "ur20-modbus") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "^0.2") (features (quote ("tcp"))) (kind 0)) (crate-dep (name "ur20") (req "^0.5") (default-features #t) (kind 0)))) (hash "0r9jlw3c17s3vwd3m2w58fs1rpd8s3rq9bxdy1iihn5ixnsy81k3")))

(define-public crate-ur20-modbus-0.3 (crate (name "ur20-modbus") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "^0.3") (features (quote ("tcp"))) (kind 0)) (crate-dep (name "ur20") (req "^0.5") (default-features #t) (kind 0)))) (hash "0pb60xk7zdb3rvvmmam2ih6s9qdc71a7c856vfsnfh4h5jfp1d02")))

(define-public crate-ur20-modbus-0.4 (crate (name "ur20-modbus") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-modbus") (req "^0.4") (features (quote ("tcp"))) (kind 0)) (crate-dep (name "ur20") (req "^0.5") (default-features #t) (kind 0)))) (hash "1xqrapzkai7dn7h1gf97bqsyhv6vpiq94f7w0n0wiqm93gad336k")))

