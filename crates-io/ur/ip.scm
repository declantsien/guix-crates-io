(define-module (crates-io ur ip) #:use-module (crates-io))

(define-public crate-uriparse-0.1 (crate (name "uriparse") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0cz5sl4jhimw7aw2qps5k2kvbg11n6afg38cwf4yjvrzl8m9q6dd")))

(define-public crate-uriparse-0.2 (crate (name "uriparse") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1xy4snz28ks8a7yandw1fhw79wda2xga9z35w25kfy67dprmam8p")))

(define-public crate-uriparse-0.2 (crate (name "uriparse") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "171bbg4gwf3wxwd4f20ssjvp61cg21z3rsm01fx7ak06fr6csrwp")))

(define-public crate-uriparse-0.3 (crate (name "uriparse") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1kw2vfybc7d0y5jjkp4as7bczf9sjgpimxsf66cn59iz4yj2nwmn")))

(define-public crate-uriparse-0.3 (crate (name "uriparse") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1ziprkhzikwrfqrdl1bbii940yndcp7mf1phrrf9z75kv42znzbl")))

(define-public crate-uriparse-0.3 (crate (name "uriparse") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "01frxrqqrbhc7lnvrs7pncypwv35qz9qwfnfypyagkbayd39w302")))

(define-public crate-uriparse-0.3 (crate (name "uriparse") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1c4rnnqqkwcwqz8m6kaxnqa12vsjz4v4mhcq0byzcc0im3zlba1b")))

(define-public crate-uriparse-0.4 (crate (name "uriparse") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "01a84c7zaj5ywbvbi92j4965nkss5a1jqbr9np7jk6h41iw5vbsi")))

(define-public crate-uriparse-0.5 (crate (name "uriparse") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0y3yg9xy42f3kpay8hh4gnr9m65bsh0dw1qsxzdb675y9sy765z1")))

(define-public crate-uriparse-0.6 (crate (name "uriparse") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1v2gzdgis1ihpn14xsq13i7w8s17wv0zb3r109kbw7gxlshw168n")))

(define-public crate-uriparse-0.6 (crate (name "uriparse") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0536603drya7gba5zwnvqi9hk30kld6hns356k1i81z4d7g4f0ai")))

(define-public crate-uriparse-0.6 (crate (name "uriparse") (vers "0.6.2") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0sidvmx6cmch8cvzbjc34yf7k6s9ymf226r1ygshbvyc56vqnp9g")))

(define-public crate-uriparse-0.6 (crate (name "uriparse") (vers "0.6.3") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 2)))) (hash "08bj3dyb3az288nmba014abwy11gqjiznnn5b8a8w5h4ljnv25g5") (features (quote (("default"))))))

(define-public crate-uriparse-0.6 (crate (name "uriparse") (vers "0.6.4") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 2)))) (hash "1zsjwc715x4720y4b3dsdras50imvaakqgyl59n3j2fq0kyd0002") (features (quote (("default"))))))

(define-public crate-uriparser-0.1 (crate (name "uriparser") (vers "0.1.0") (deps (list (crate-dep (name "uriparser-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "13z4033dxyxcbpnjzxhg4ziqmvxdrcgw1nkbyd2nk37n7c7ywazc")))

(define-public crate-uriparser-0.2 (crate (name "uriparser") (vers "0.2.0") (deps (list (crate-dep (name "uriparser-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "04r1si5jrcv6bbz4fn6dnnnpsj8daq5jm9f8k79qx98bhf2jh9zf")))

(define-public crate-uriparser-sys-0.1 (crate (name "uriparser-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0q8ay2q4cjv6nh3x33b94fmrhkanw54swj7wy7by7kbx79jip3ch")))

