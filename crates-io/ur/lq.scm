(define-module (crates-io ur lq) #:use-module (crates-io))

(define-public crate-urlqstring-0.1 (crate (name "urlqstring") (vers "0.1.0") (hash "00hi0zmciv7knzjkryhg6p74ry3qrzc7nfd8w960lx93r6z3w0la")))

(define-public crate-urlqstring-0.2 (crate (name "urlqstring") (vers "0.2.0") (hash "112k47abxsrziyf6bx70nkf0c83h21bvrs97c2s7m46w68yg6yjq")))

(define-public crate-urlqstring-0.2 (crate (name "urlqstring") (vers "0.2.1") (hash "1vdg5h96silhd20qimi4vfpmmicw7phrliys8ag3a5c4z26ngjph")))

(define-public crate-urlqstring-0.2 (crate (name "urlqstring") (vers "0.2.2") (hash "0pgsdx5xg8wcfa96n3yydq7rcrwsj6xr2akh21qlw8mkzki3d1xx")))

(define-public crate-urlqstring-0.3 (crate (name "urlqstring") (vers "0.3.0") (hash "0y4c12ski49ba7zx1ka6gfs4wfd4knmm0q43q1mrh2dsqz1zcc5n")))

(define-public crate-urlqstring-0.3 (crate (name "urlqstring") (vers "0.3.1") (hash "0h8k28pr9p571bqwqdgb4737lg98nyfiyxpxl12l7xdpin985nl6")))

(define-public crate-urlqstring-0.3 (crate (name "urlqstring") (vers "0.3.2") (hash "0xdyrsxv18bqk8lzrc5cssk4x0mqsysnahdqbdf1d5ir3x5s9f3q")))

(define-public crate-urlqstring-0.3 (crate (name "urlqstring") (vers "0.3.3") (hash "0k23350ddxymiwvaphx07qmakrj6n8f7f2dnaynbl24br45sb6rl")))

(define-public crate-urlqstring-0.3 (crate (name "urlqstring") (vers "0.3.4") (hash "06mw1h6m6g6rjfkasrzjnn2ldikx9ss7rf9fv6vi10a542jl3ra3")))

(define-public crate-urlqstring-0.3 (crate (name "urlqstring") (vers "0.3.5") (hash "1k70q8yflr1vmmhpdvkdhc7w4px3qsndfz7cv0c5f1kal1rk9vr5")))

