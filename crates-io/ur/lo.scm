(define-module (crates-io ur lo) #:use-module (crates-io))

(define-public crate-urlocator-0.1 (crate (name "urlocator") (vers "0.1.0") (hash "1dwl9zvlv1m62vxb2xyldw73y3p02drglc69n57nlxbdlhb9wwlh") (features (quote (("bench"))))))

(define-public crate-urlocator-0.1 (crate (name "urlocator") (vers "0.1.1") (hash "000ppssk6gb73d4cdn17i5bvwp8rqd35czgpykx96krbcmba3al0") (features (quote (("bench"))))))

(define-public crate-urlocator-0.1 (crate (name "urlocator") (vers "0.1.2") (hash "1xzhwmqrqyk8p3s5npqpidrn0gjapqx5fshrx633fk56j7cm8qm1") (features (quote (("nightly"))))))

(define-public crate-urlocator-0.1 (crate (name "urlocator") (vers "0.1.3") (hash "0r5ig00np3svjpvb1gha3ni798cwj2w7rnlwrc8jrrw7bvlb2yri") (features (quote (("nightly"))))))

(define-public crate-urlocator-0.1 (crate (name "urlocator") (vers "0.1.4") (hash "181mqlydc1i7q57jp56pbysn4km4cx34jldr92kv1bvd217rmqql") (features (quote (("nightly"))))))

