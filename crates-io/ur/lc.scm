(define-module (crates-io ur lc) #:use-module (crates-io))

(define-public crate-urlcat-0.1 (crate (name "urlcat") (vers "0.1.0") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "0g5nnwn7f78yiflvi4hz042hymh5zgd3s4xqj8kjaq1p8jp2fid4") (yanked #t)))

(define-public crate-urlcat-0.1 (crate (name "urlcat") (vers "0.1.1") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "0h3jxcn8kwrx0yw4rjkpbsa3r351q6xwc9xw8y3zj31ks1xz6jb0") (yanked #t)))

(define-public crate-urlcat-0.1 (crate (name "urlcat") (vers "0.1.2") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1gpiyjzipkj5h7azccc7kq7gjklmpd39awcz5fb1jvx36yl699ll") (yanked #t)))

(define-public crate-urlcat-0.1 (crate (name "urlcat") (vers "0.1.3") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "0m0fk182hf4i11vihfka5wg64ydch2c5zzpl7d30vrz6f2d0zh4z") (yanked #t)))

(define-public crate-urlcat-0.1 (crate (name "urlcat") (vers "0.1.4") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "02k7wfd8347kmwqf43zx3gllsx40y4f2zs2krw1bsicqf5mwqrd8") (yanked #t)))

(define-public crate-urlcat-0.1 (crate (name "urlcat") (vers "0.1.5") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1ci92q55gif1zm6fncfk27vb1d5syqkafm8kiswalfq7pscvypdr")))

(define-public crate-urlcat-0.2 (crate (name "urlcat") (vers "0.2.0") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "082mfklmk6g49s1d5lmysgj4bmdgkryvl9h5dq88cj0lmrbwjhcz")))

(define-public crate-urlcat-0.2 (crate (name "urlcat") (vers "0.2.1") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "038zn0j0cwlls9aad9nfxn6qng40dwdim9hlxsxgnjq55c8mdvq6")))

(define-public crate-urlcat-0.2 (crate (name "urlcat") (vers "0.2.2") (deps (list (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1gbkg5lkhi858dlq07qvii0vnvq888cmqxndq3bpqb7ak8pis06m")))

(define-public crate-urlchecker-0.1 (crate (name "urlchecker") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "04997y52452cchhr9r815v05h1hkdbbrkcm2gnkwa85ksw96ffjy")))

(define-public crate-urlchecker-0.2 (crate (name "urlchecker") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1cafmq28lccxk0a5b8wa49ihli882k48qdv1gy612rj53i3kgnsh")))

(define-public crate-urlchecker-0.3 (crate (name "urlchecker") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1haddbg721gdf5dqgwsc960fy208fb5ngczdz9yhj4ib44q2slys")))

(define-public crate-urlcode-0.0.0 (crate (name "urlcode") (vers "0.0.0") (hash "1sigiwf84iwhf82w4f2gg4wqzxcwj536rcbiss2cq0qkwg45hwll") (yanked #t)))

(define-public crate-urlcode-0.1 (crate (name "urlcode") (vers "0.1.0") (deps (list (crate-dep (name "percent-encoding") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qyca3l86z2vkila34kyhp14dcpdr0zjgbxyj4dimzrd1ivlsimx")))

