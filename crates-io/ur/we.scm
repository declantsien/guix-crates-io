(define-module (crates-io ur we) #:use-module (crates-io))

(define-public crate-urweb-0.1 (crate (name "urweb") (vers "0.1.0") (hash "1fnx8rl8k8x20wdrya2xaz1w0jiblfjn36yxryc4063lacp87iw6") (yanked #t)))

(define-public crate-urweb-0.1 (crate (name "urweb") (vers "0.1.1") (hash "1fxnnfay2sr2w9xi5av09yapbh7vl4nkb1dv9966pc4qqm1dm4ny")))

