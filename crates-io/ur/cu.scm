(define-module (crates-io ur cu) #:use-module (crates-io))

(define-public crate-urcu-0.0.1 (crate (name "urcu") (vers "0.0.1") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 2)))) (hash "1rjz26572c4zqxvm3nsy3zganxfac3hlg1xcn43ryc4727m1v814") (links "urcu-memb")))

(define-public crate-urcu-0.0.2 (crate (name "urcu") (vers "0.0.2") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 2)))) (hash "0grfcyrf9f4n2vngdysnwlb7l5qgc6q4zhqf6a8aw13x7d1i6gif") (links "urcu-memb")))

(define-public crate-urcu-0.0.3 (crate (name "urcu") (vers "0.0.3") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 2)))) (hash "0rwrvqvm69qym1i3xm4y1h603zngmajzwhhcpafjl83bwp8pmalx") (links "urcu-memb")))

(define-public crate-urcu-sys-0.0.1 (crate (name "urcu-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "0vm4kpmdggx1cmwfmfawpspkfgl60485s9pc98jgns68fccs0jqc") (links "urcu")))

(define-public crate-urcu-sys-0.0.2 (crate (name "urcu-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "04g0153qni73797gs3bwi9hjr2iyrhazyw9bry5r2d3ls4ihssjc") (links "urcu")))

(define-public crate-urcu-sys-0.0.3 (crate (name "urcu-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "144441i05zpia1vm4vimab4k74az20m2y8yxmz4jfk1f9964gnbx") (links "urcu")))

(define-public crate-urcu-sys-0.0.4 (crate (name "urcu-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "0pqswc9k8niq54ggrd2q6n5y8xqcqspggb5998gs1y7a2nvaix8k") (links "urcu")))

(define-public crate-urcu-sys-0.0.5 (crate (name "urcu-sys") (vers "0.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "1dbig1a81935ari3gf8v2gyzjxsr7zg0qba04g9vmy9998g73wgz") (links "urcu")))

