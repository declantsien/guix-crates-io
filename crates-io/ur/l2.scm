(define-module (crates-io ur l2) #:use-module (crates-io))

(define-public crate-url2-0.0.1 (crate (name "url2") (vers "0.0.1") (deps (list (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "0c37x9i3i4vg0pxpmljmsb2hfpwf4jck9f45y7w36ybzk9kmlh4b")))

(define-public crate-url2-0.0.2 (crate (name "url2") (vers "0.0.2") (deps (list (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "1610mmwfrg9y2fwsgipfq3jgggnb35hlwq9pmvhzr036gljmfglh")))

(define-public crate-url2-0.0.3 (crate (name "url2") (vers "0.0.3") (deps (list (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "0nh26fqxnwsgcraq41ahqf7qxbmigxn3sflidsyixwi5yzcznkbr")))

(define-public crate-url2-0.0.4 (crate (name "url2") (vers "0.0.4") (deps (list (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "17dh824k09q9yxmlmjk0lxwjac5x6z7z11cszw9znqqbc0z2pqdi")))

(define-public crate-url2-0.0.5 (crate (name "url2") (vers "0.0.5") (deps (list (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "1my664a6byyxy3sjg0kz1wv7g0c7f6sdfw91nhx785k1bgmkfm2l")))

(define-public crate-url2-0.0.6 (crate (name "url2") (vers "0.0.6") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "07hv0603wz8bm2bb4bw198m4pdnjvkdgzx886cv2v1p93lzx3768")))

