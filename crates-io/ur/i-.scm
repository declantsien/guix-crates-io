(define-module (crates-io ur i-) #:use-module (crates-io))

(define-public crate-uri-builder-0.1 (crate (name "uri-builder") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.6") (default-features #t) (kind 0)))) (hash "17qp5brgcjii3l481r89qhg6kcg64ddav14ym1da4nqjf21rghdn")))

(define-public crate-uri-formatter-0.1 (crate (name "uri-formatter") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "071qf3qk9hx74xk12pra1h0xp29firf1pfqhm7c0gncg1h7hx301")))

(define-public crate-uri-formatter-0.1 (crate (name "uri-formatter") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0ig4pv4am75mdq7crfl1gy5rra7dw6d60kld31p5jwp93vpl6g7a")))

(define-public crate-uri-formatter-0.1 (crate (name "uri-formatter") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0rsd9999skyak3yx4qbzm82dyjxkpaw2aq9fircmhycz5l6mw5a1")))

(define-public crate-uri-parsing-rs-0.0.1 (crate (name "uri-parsing-rs") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prop-check-rs") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0pjl894dmpgrfqnwb4ravrw4nd2gk940kqdhpsg7bji5x1hianvg") (features (quote (("default" "serde"))))))

(define-public crate-uri-pattern-matcher-0.1 (crate (name "uri-pattern-matcher") (vers "0.1.0") (hash "1qri25qqhfsfq6shzrkrriq12w7n4zj8s4svav3pxp5254n17pb4")))

(define-public crate-uri-pattern-matcher-0.1 (crate (name "uri-pattern-matcher") (vers "0.1.1") (hash "02f9xmyfwvl7zap27c5yz0rkm36cwdfr4qvw9iq35fb2phrr0a4x")))

(define-public crate-uri-pattern-matcher-0.1 (crate (name "uri-pattern-matcher") (vers "0.1.2") (hash "1l9c6s9j14vlfaqgbln2vs38bmkm6ciwsb62s68aq73bvq3d1iz2")))

(define-public crate-uri-pattern-matcher-0.1 (crate (name "uri-pattern-matcher") (vers "0.1.3") (hash "11igm4p9n1fgrhy12sz0vzc3i0jd4aj94snzhi7g248bg5jl04dn")))

(define-public crate-uri-pattern-matcher-0.1 (crate (name "uri-pattern-matcher") (vers "0.1.4") (hash "02aq1l02izsj022rjpx3a086aydg0a2w5yddyhrlarjxdnnwnc5h")))

(define-public crate-uri-pct-0.1 (crate (name "uri-pct") (vers "0.1.0") (hash "1ifm8dgavyfqnyj5yxa4pn59k0k2bmr26v6imdkzjvqgaaa1zkgj")))

(define-public crate-uri-pct-0.1 (crate (name "uri-pct") (vers "0.1.1") (hash "0957bcp83ms21fgqxx2dcp25daa164hi3q6lnrb4w3rsjnqlccx2")))

(define-public crate-uri-resources-0.1 (crate (name "uri-resources") (vers "0.1.0") (hash "0nhwpkryifxcvq760qhyag1s744bdl4bx1w8362h5xcrnrw5mwgp")))

(define-public crate-uri-resources-0.1 (crate (name "uri-resources") (vers "0.1.1") (hash "035289qdminvyy8i9yg37l4akihlxb6x2farfi5596wwazrb4qy4")))

(define-public crate-uri-resources-0.1 (crate (name "uri-resources") (vers "0.1.2") (hash "1j6knn8wji69z0933g08hkwliv82i78nbx8d806prv7kqrc0s7af")))

(define-public crate-uri-resources-0.1 (crate (name "uri-resources") (vers "0.1.3") (hash "071dmnhp7v2b3fkcy14hifgly4p0pwvnjx05slv77qgpj8lkqn82")))

(define-public crate-uri-resources-0.1 (crate (name "uri-resources") (vers "0.1.4") (hash "0lrlcx04lrdva11sxr0r57irjc1piqg4z31vbbblbrk2q7m4n5d6")))

(define-public crate-uri-resources-0.2 (crate (name "uri-resources") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "03clslf8w52m5n6n5lbx4s17xd1avvl0n4z0jv352ih7855z8iar")))

(define-public crate-uri-resources-0.2 (crate (name "uri-resources") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "07idgvs7f5ka9mb20jid6vd8iv73h8wwqr7n2xv5l1kxn06vdz6p")))

(define-public crate-uri-resources-0.2 (crate (name "uri-resources") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0l9gw07nid6slld6c4srw01vn4qbalpqhsk6aqngdd2mzf4dx4vv")))

(define-public crate-uri-resources-0.2 (crate (name "uri-resources") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "15g4fsqpzgmbpmiwmj69a7jps4syrwlzscrfw10yyb1zivr5z3j4")))

(define-public crate-uri-routes-0.1 (crate (name "uri-routes") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "1slqpz7yn7wvj2k70pdbhg2vgx4bfv41c5cab2yl1gjmd6c4xrxr")))

(define-public crate-uri-routes-0.1 (crate (name "uri-routes") (vers "0.1.1") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "05xfx72k4hsj335dkzv038srclz5z8fdpk6lzfiq574zywv3j2z4")))

(define-public crate-uri-routes-0.1 (crate (name "uri-routes") (vers "0.1.2") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "0f87cyrkbg2zmxzvzcy5jg8cmzxnmp3x99skmpl15xbqlmcm3zb9")))

(define-public crate-uri-routes-0.1 (crate (name "uri-routes") (vers "0.1.3") (deps (list (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "0i8sch53yf76a0588diff2xc1wg6css32dqlv4qfhaqm7qb7anqj")))

(define-public crate-uri-rs-0.0.1 (crate (name "uri-rs") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prop-check-rs") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0h2nv0x5xc9hrz7v6kb3jsmz19ww0lpzvz7m3yvv7nm3xqhns5qn") (features (quote (("default" "serde"))))))

(define-public crate-uri-rs-0.0.2 (crate (name "uri-rs") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prop-check-rs") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "13zdd4can7j62j3algpg6g4q0w0znm6034qzmc2ydp7y9b0gjlci") (features (quote (("default" "serde"))))))

(define-public crate-uri-rs-0.0.3 (crate (name "uri-rs") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prop-check-rs") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0f8drhf7j9mkprvg0pizvcmw07acak90klccqsp2d7pgmqdv6k74") (features (quote (("default" "serde"))))))

(define-public crate-uri-rs-0.0.4 (crate (name "uri-rs") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prop-check-rs") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1cv1k5bh8m29d49ggm7b35lp07v48w6y3wn81ibjhhy391md726h") (features (quote (("default" "serde"))))))

(define-public crate-uri-template-system-0.1 (crate (name "uri-template-system") (vers "0.1.0") (deps (list (crate-dep (name "uri-template-system-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a8w4q4mcw0na18gzmvjf1pghm1qndw0bqs02pfh1cy9g6slgkjh")))

(define-public crate-uri-template-system-0.1 (crate (name "uri-template-system") (vers "0.1.1") (deps (list (crate-dep (name "uri-template-system-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a35j8bnjwax5pj3ah45svmbrgxpc6v8yyrg1qhywyga2jlzc4gz")))

(define-public crate-uri-template-system-0.1 (crate (name "uri-template-system") (vers "0.1.2") (deps (list (crate-dep (name "uri-template-system-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "1srrl618dvpbckax0l1c6ky6ravx2jy5l81baskwyhhxn5vq7v5i")))

(define-public crate-uri-template-system-0.1 (crate (name "uri-template-system") (vers "0.1.3") (deps (list (crate-dep (name "uri-template-system-core") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "04km5vw1gychmq3gmf55rb3y60va818mgdgg96w1489x2plly2cv")))

(define-public crate-uri-template-system-0.1 (crate (name "uri-template-system") (vers "0.1.4") (deps (list (crate-dep (name "uri-template-system-core") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1r9qdviw00bnqr33w8ghks51hmalddqmrn0lm3g35h2ds5nblpb2") (rust-version "1.64.0")))

(define-public crate-uri-template-system-0.1 (crate (name "uri-template-system") (vers "0.1.5") (deps (list (crate-dep (name "uri-template-system-core") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0xy1rq4d6xi3jjdbzmdd9a7jjwxf485hx7mxfc515zx4lk496ipd") (rust-version "1.64.0")))

(define-public crate-uri-template-system-core-0.1 (crate (name "uri-template-system-core") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "02gi6c170mw1pf5wvbx3lydfd6zbgblr5h6az4xmkzf2khrndql7")))

(define-public crate-uri-template-system-core-0.1 (crate (name "uri-template-system-core") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1iczacrfp6jzw9qv78bnszv4rdvahc40v8dadhgrqmz03qs7q3x0")))

(define-public crate-uri-template-system-core-0.1 (crate (name "uri-template-system-core") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1rl3sq7vw183cih9s0m0257xrrz0d8i9i56gyi9q5avi99c2p1vq")))

(define-public crate-uri-template-system-core-0.1 (crate (name "uri-template-system-core") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1niz2c13hckac2cgsikfn9gizfb54ka4dsqiksfq4psx708bihyj")))

(define-public crate-uri-template-system-core-0.1 (crate (name "uri-template-system-core") (vers "0.1.4") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1w1g7silihnd74fw9lf0r8yh3cpw0zsjs51ba5kwl22cslwcyk4g") (rust-version "1.64.0")))

(define-public crate-uri-template-system-core-0.1 (crate (name "uri-template-system-core") (vers "0.1.5") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1yriz3bk9lfccsx723galk46as679mhhmfx0v63gqbprb8wm9wi6") (rust-version "1.64.0")))

(define-public crate-uri-url-0.1 (crate (name "uri-url") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "151mk2k69q9x6wp2xaim83xmwzrxzfk9lkvbk4q3c4180snfywzm")))

(define-public crate-uri-url-0.2 (crate (name "uri-url") (vers "0.2.0") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "08a774g8p32wvfgb4w6hg7mz0ym4vfiyv7ilnv44hrjjdvw292r0")))

(define-public crate-uri-url-0.3 (crate (name "uri-url") (vers "0.3.0") (deps (list (crate-dep (name "http") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0wrgaqfs16xnv11fqa5kh10w2cgfiv1zl23854i9balsg5x7ma66")))

