(define-module (crates-io cl -f) #:use-module (crates-io))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.0") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "11bivpri2zfr3yqxyvdgp9gm8vvf08xy8kkbl0v5pmnn25sfkpjr")))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.1") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1yrnsqmcm47grrsa7spa5yi0cfaqs2bcc50q6h1bz0sv2rrl96wx")))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.2") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0z2y5q7qyy8q9205ifq8pnwxg85g0qmnvhhs2nnhpfkpb7kxz4yx")))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.3") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "06fgm5agrxkwhcz26696h74xmg4ys6l82jfm3pq84zyqsl7ybpy6")))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.4") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vx9d582ikhcnxi0a06s300a0a8v1d15l0x73x71g6q6kkv4adxp")))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.5") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "07mcysx1rgb6dvfbkq7gymmg2fn9nnyzj6p2s30n8lrq06xcq7h5")))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.6") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1qnbdpah2v0bja98dakglpan298misn4fy4m7pkzjqsyq1bpwcs0")))

(define-public crate-cl-format-0.1 (crate (name "cl-format") (vers "0.1.7") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nvfqrzg59xjiyqp0vqyg70g5cvm0g8ib5sln72xds4gxrk006mi")))

(define-public crate-cl-format-0.2 (crate (name "cl-format") (vers "0.2.0") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12sla1hvhvqrh0z6f3gpw84vrjf2k5q2lgb6m27xiqfnrcdpli7q")))

(define-public crate-cl-format-0.2 (crate (name "cl-format") (vers "0.2.1") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0w8skrlrhwp8sldx8i76ggwpx2mlbax4mywflwwlhx9q26frpp7k")))

(define-public crate-cl-format-0.2 (crate (name "cl-format") (vers "0.2.2") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "17ycbhpgdg9xwd0bgpk1q0ga3bzsk5ra6ymd2p38plhq11p8gvr6")))

(define-public crate-cl-format-0.2 (crate (name "cl-format") (vers "0.2.3") (deps (list (crate-dep (name "cl-format-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "radix_fmt") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0a8mnc4yhabjg6dqj3vcwxj6yxwrk9ym88n4yjlvn1b7i54kkghm")))

(define-public crate-cl-format-macros-0.1 (crate (name "cl-format-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "11ck6bn4v3f7h699bccyg03ldrw46rqs1518zyicc2ws5cxmmimb")))

(define-public crate-cl-format-macros-0.1 (crate (name "cl-format-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1jf3mmc5i9b2ly7lfwxrdf9avir60yxxygbw2wcf3ymm4izaf5zf")))

(define-public crate-cl-format-macros-0.1 (crate (name "cl-format-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1cwwl0gfnlp5rca07y5a84kd1x55bkhwx8blpw48yzj42skic86b")))

(define-public crate-cl-format-macros-0.1 (crate (name "cl-format-macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "167zks27zg0znnxhp78wrxjm02ch080sw008jdir6j1846w2j43y")))

(define-public crate-cl-format-macros-0.1 (crate (name "cl-format-macros") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1w7bj74ig2kil3dzasmfdy20ms9fwqp7bki2fsvwxz5l14nhgrmg")))

(define-public crate-cl-format-macros-0.1 (crate (name "cl-format-macros") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "163l7h1gkkqaqvaxcaf90bfhikicki11pmwq7mszq7bqv14m2jn3")))

