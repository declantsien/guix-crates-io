(define-module (crates-io cl ix) #:use-module (crates-io))

(define-public crate-clix-0.1 (crate (name "clix") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33.3") (default-features #t) (kind 0)))) (hash "0gxafkjaj17qp9j7w6x2z9dzvvjixl2nccyvsd53nrbpjskr588m")))

