(define-module (crates-io cl id) #:use-module (crates-io))

(define-public crate-clide-0.0.1 (crate (name "clide") (vers "0.0.1") (hash "1pv3cwznc524m047dynj6wlp40mm4pykppx4h4jfk0h73sy538bs") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.0.1") (hash "0mr5075n98qwak2918alb3vw9kadqz8ai0sfiy7z2p6cjch1lp09") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.0.2") (hash "0zmj4wcv3iqqrylf7yka495am1fkjsmybw282n722rrhzjw4fi3a") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.0.3") (hash "13zjnrgla3ngmk386rwfmpmy1p7h97d2hnx8qkayjd0vipcj30pp") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.1.0") (hash "16izjpdf2dvncdyrpxb0lv1h49xgm42wcf9mx9avpdflwila9982") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.1.1") (hash "1yjn1s5c4ks9ybrvmpv7mq18n1ff9dnfsmrzch9m9nz4vbld7hpv") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.2.1") (hash "1zrxjc5w5p0s4wm8qmvpmfndh6zg1aryb2nsr23xvjs4phjv5c9s") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.3.0") (hash "0sl9fqpy69ya350kky07i3p9hc87vag71v3h8i7v2l1qbd3k5xcj") (yanked #t)))

(define-public crate-clidi-1 (crate (name "clidi") (vers "1.3.1") (hash "03xw04mlxjsr8qwhq43xs39jb0j912k4x884bif8k6g4li32f8vs")))

(define-public crate-clido-0.1 (crate (name "clido") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "14mxdal8ymvmdr65hvbm10wsjdwdywwc697y30w9mzfv13kicq89")))

(define-public crate-clido-0.1 (crate (name "clido") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1dgka9wgbhzhr700ki01sjah0gv8rzv88h1rk07svx278ywb0xy0")))

(define-public crate-clido-0.2 (crate (name "clido") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "175dbfjrh52b9ky0sqwyd4jqdjr1xhmld39h0s99nv631gr2lkpj")))

(define-public crate-clido-0.3 (crate (name "clido") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("color" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1g9ysndkjv4klss8m1rbh9rp40l1j4zgyri01cfgp12h6la0l2ln")))

(define-public crate-clido-0.3 (crate (name "clido") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("color" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "0man9ynzhqwp3ki92qplj8zv1zzzcc5x74y6y1c7gw5f3m5jfy6d")))

(define-public crate-clidogs-0.1 (crate (name "clidogs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.14") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "0i3pf0zlhal45ndp27i9g4wh292bg5ggyl8rzvd1x2fs15lnp5ny")))

