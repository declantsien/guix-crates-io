(define-module (crates-io cl dr) #:use-module (crates-io))

(define-public crate-cldr-29 (crate (name "cldr") (vers "29.0.0-alpha") (deps (list (crate-dep (name "bzip2") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bzip2") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "git2") (req "^0.4.3") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^0.7.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.7.14") (default-features #t) (kind 0)))) (hash "1xw5kqd7lqgmi7iasbdkpsrmhvadqcbd456agaijin40qjs7wsdq") (features (quote (("units") ("segments") ("roc") ("rbnf") ("per") ("numbers") ("modern") ("misc") ("localenames") ("jap") ("isl") ("ind") ("heb") ("full") ("eth") ("default" "all-modern" "compress") ("dates" "numbers") ("dan") ("core") ("cop") ("compress" "bzip2") ("chi") ("bud") ("all-modern" "modern" "core" "dates" "localenames" "misc" "rbnf" "segments" "units") ("all-full" "full" "core" "dates" "localenames" "misc" "rbnf" "segments" "units") ("all-calendars" "dates" "bud" "chi" "cop" "dan" "eth" "heb" "ind" "isl" "jap" "per" "roc")))) (yanked #t)))

(define-public crate-cldr_pluralrules_parser-0.1 (crate (name "cldr_pluralrules_parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)))) (hash "1y4jm57zivx1sf1dlrjg7pg4rfj78mlbgchmwvjpdrycb0p04y2m")))

(define-public crate-cldr_pluralrules_parser-1 (crate (name "cldr_pluralrules_parser") (vers "1.0.0") (deps (list (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)))) (hash "1gb2w9xb9lczr5yq6xmrsk63hxqabmpl1irwbixwk7fzwp0gf1sf")))

(define-public crate-cldr_pluralrules_parser-1 (crate (name "cldr_pluralrules_parser") (vers "1.0.1") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)))) (hash "13k0fyyixznrxd7caxlrmv79sys3n44k2l1yac9l21bqnnyy9jbk")))

(define-public crate-cldr_pluralrules_parser-2 (crate (name "cldr_pluralrules_parser") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "13vm7lq1lzpwpi4rggfbrf9ci059168w68kx486srh3dfwaxlqxc")))

