(define-module (crates-io cl ru) #:use-module (crates-io))

(define-public crate-clru-0.1 (crate (name "clru") (vers "0.1.0") (hash "0jqffnmmx3y2ylpvr2r1hssgijdwd91x58q18hg9w6d42yz0g0xz")))

(define-public crate-clru-0.2 (crate (name "clru") (vers "0.2.0") (hash "0abnpnnjagx4ngdr4sxp21xfpag8fngfk4mbbfmia83ad6rm5pba")))

(define-public crate-clru-0.2 (crate (name "clru") (vers "0.2.1") (hash "094hw757ic039bnlvqqf7zi9ic5vwi3pnd0b76q510vp6bhhxqa4")))

(define-public crate-clru-0.3 (crate (name "clru") (vers "0.3.0") (hash "06nixxqa9gdfg3ldcm41lpcrln8xjzdzz0rvvq3d17d91mpv3aml")))

(define-public crate-clru-0.4 (crate (name "clru") (vers "0.4.0") (hash "14yx8q4ccsd5qmjndn8ah7n22fzrbiz9hnqb3cfdj6v9l1ngf7sr")))

(define-public crate-clru-0.5 (crate (name "clru") (vers "0.5.0") (hash "02hy88i8vagcb2gy7kxwjrsyvzg5q0rx477sfnll5r78vp9np391")))

(define-public crate-clru-0.6 (crate (name "clru") (vers "0.6.0") (hash "16hm7gjircg8k4zmgyz9hayfiz2ca9zi9x1d74r7wg2yd91z8azz")))

(define-public crate-clru-0.6 (crate (name "clru") (vers "0.6.1") (hash "01xq2vm3pfkja6crsh5r7idzyhy0dhjd8dz2y1zn00rf62kiy6dq")))

(define-public crate-clru-0.6 (crate (name "clru") (vers "0.6.2") (hash "0ngyycxpxif84wpjjn0ixywylk95h5iv8fqycg2zsr3f0rpggl6b")))

