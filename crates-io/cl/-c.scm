(define-module (crates-io cl -c) #:use-module (crates-io))

(define-public crate-cl-calc-1 (crate (name "cl-calc") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)))) (hash "0p9l3mfnz4y4zzw02ksswa8y6nrshy1m2lpsb06dax4w5fh0d4jm")))

(define-public crate-cl-calc-1 (crate (name "cl-calc") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)))) (hash "1adzmf1nancby96cpcnfy38gs3g51l8jysdwxyp2sv49m6p6r88b")))

(define-public crate-cl-calc-1 (crate (name "cl-calc") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)))) (hash "0f259lp1r2rswpxhifc7995y10g91yzm3dh8kz7186cqgb7k1dqn")))

