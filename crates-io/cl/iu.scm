(define-module (crates-io cl iu) #:use-module (crates-io))

(define-public crate-clium-0.1 (crate (name "clium") (vers "0.1.0") (hash "171rfa00dymphyfwa5faxy1ypkhzb9dhgnvwrzkgpqjk38wjmyxf")))

(define-public crate-cliutil-0.0.1 (crate (name "cliutil") (vers "0.0.1-alpha") (hash "1l8zfxl1l5vpj1wnzcblmswfbz2y19xgrpm7qr88z7xfx8afz2gk") (yanked #t)))

(define-public crate-cliutil-0.0.2 (crate (name "cliutil") (vers "0.0.2") (hash "0pz0kgcnzmn7nk9mwz1ipfsr9vm5aziix4mdszr1njzwiqi8gyl8") (yanked #t)))

(define-public crate-cliutil-0.1 (crate (name "cliutil") (vers "0.1.0-alpha.0") (hash "01ja34827x801hlgrhxfra0n59xj0m5y1wy36nq7rzf0h0i377wg")))

