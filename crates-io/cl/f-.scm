(define-module (crates-io cl f-) #:use-module (crates-io))

(define-public crate-clf-parser-0.1 (crate (name "clf-parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "bytecheck") (req "^0.7") (kind 0)) (crate-dep (name "rkyv") (req "^0.7") (features (quote ("size_64" "validation"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6") (kind 0)))) (hash "18sh94w5vzjb6hm0a8zbvg4p8v7qjmsckkv2igl4b9qq4nz6mf17")))

(define-public crate-clf-parser-0.2 (crate (name "clf-parser") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.1") (features (quote ("std"))) (kind 0)) (crate-dep (name "bytecheck") (req "^0.6") (kind 0)) (crate-dep (name "rkyv") (req "^0.7.40") (features (quote ("size_64" "validation"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6") (kind 0)))) (hash "1v8l0bhdg93nb560hckafx4n6sjpdzjln6py0dxdxf5z8lxa24pj")))

