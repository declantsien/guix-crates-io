(define-module (crates-io cl af) #:use-module (crates-io))

(define-public crate-clafrica-0.1 (crate (name "clafrica") (vers "0.1.0") (deps (list (crate-dep (name "clafrica-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "15z4mqbxq4wp2psjai7ynqp923vprxhpgk4lh0wfp6hn3sar5v7b") (yanked #t)))

(define-public crate-clafrica-0.1 (crate (name "clafrica") (vers "0.1.1") (deps (list (crate-dep (name "clafrica-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1bv56yfzqyfr4l8nxq5cvma33bmj8fqm96gajizaxqgzjv9m3al8")))

(define-public crate-clafrica-0.2 (crate (name "clafrica") (vers "0.2.0") (deps (list (crate-dep (name "clafrica-lib") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rdev") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0anb6zdzyb27v0iqga8vj72mxkh2w1niwz1hqv161cp07jib8wxi")))

(define-public crate-clafrica-0.2 (crate (name "clafrica") (vers "0.2.1") (deps (list (crate-dep (name "clafrica-lib") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rdev") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0jf67gcc4s8ny70kbhjprx4xsh6kwzqjigwglhhpjqs3qcih7zqg")))

(define-public crate-clafrica-0.2 (crate (name "clafrica") (vers "0.2.2") (deps (list (crate-dep (name "clafrica-lib") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rdev") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0j4vny3iilcyskaf0i4s4h71l45y8h7ww7lmgk4f6mydq9xsqpd2")))

(define-public crate-clafrica-0.3 (crate (name "clafrica") (vers "0.3.0") (deps (list (crate-dep (name "clafrica-lib") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rdev") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0057a8s1jvf28lb9qd8hpgilxc76prwh3c736rcanqjrihhgsnpk")))

(define-public crate-clafrica-0.3 (crate (name "clafrica") (vers "0.3.1") (deps (list (crate-dep (name "clafrica-lib") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rdev") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0ylx88jgadba6c9mrqxy1faw7w47hljz9j1214293znd32d356pn")))

(define-public crate-clafrica-0.4 (crate (name "clafrica") (vers "0.4.0") (deps (list (crate-dep (name "clafrica-lib") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rdev") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rhai") (req "^1.15.1") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "046k2gsjblbbflkcnwvxj09gjyih2ziwxh7zs8ajmzsjh2427a29")))

(define-public crate-clafrica-0.4 (crate (name "clafrica") (vers "0.4.1") (deps (list (crate-dep (name "clafrica-lib") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rdev") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rhai") (req "^1.15.1") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0rnrdb7qns95bbc45wgh5xf87vnk6hwbz1k8zrgzigyx8zxyrwyz")))

(define-public crate-clafrica-lib-0.1 (crate (name "clafrica-lib") (vers "0.1.0") (hash "0qg3hpwd88n483nwxic1m499bq3xbj65x2ch7sxwi1lkj70768kn")))

(define-public crate-clafrica-lib-0.1 (crate (name "clafrica-lib") (vers "0.1.1") (hash "1jgbz8cc59lwca574kayg05jn5wyxyv4jwnrhyc0cg0y5kmlmn0w")))

(define-public crate-clafrica-lib-0.2 (crate (name "clafrica-lib") (vers "0.2.0") (hash "1hzhlsxnxh9qjjmr1pnbznjw05zljfkd2a6af62a3d8xz2fmxjf6")))

(define-public crate-clafrica-lib-0.3 (crate (name "clafrica-lib") (vers "0.3.0") (hash "1w0j19vkkq7afac374n6kmzx45cd1kdldhlw72jgxk331vxrx54w")))

(define-public crate-clafrica-lib-0.3 (crate (name "clafrica-lib") (vers "0.3.1") (hash "076j9xjk7zsm7aq34lkk6m7z3c7n83cipmya2jr55zkyp9bmgbhr")))

(define-public crate-clafrica-lib-0.3 (crate (name "clafrica-lib") (vers "0.3.2") (hash "01azxb3wxqh2ycz4sj8jwbb4r8xicknqrhw0fv9i14ahjxr9sgg2")))

(define-public crate-clafrica-wish-0.1 (crate (name "clafrica-wish") (vers "0.1.0") (deps (list (crate-dep (name "clafrica") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "196l7cx75xza1wy04acxfyjwlqv130frf6gl8lmhwpq4g99fm5aa") (yanked #t)))

(define-public crate-clafrica-wish-0.1 (crate (name "clafrica-wish") (vers "0.1.1") (deps (list (crate-dep (name "clafrica") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00ca5z556p3f9i8n6kxwv2ian5288cls9hcf7mbliifhzhvrmr6x")))

(define-public crate-clafrica-wish-0.2 (crate (name "clafrica-wish") (vers "0.2.0") (deps (list (crate-dep (name "clafrica") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "10grm44ix7zd71430s4l6n6x1w4qxi9yh666818qy02i5g6lm8ip")))

(define-public crate-clafrica-wish-0.2 (crate (name "clafrica-wish") (vers "0.2.1") (deps (list (crate-dep (name "clafrica") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0rrghlghlr8snpljr6wzyyv53yqpzf6m7j61accx25jy44h651gz")))

(define-public crate-clafrica-wish-0.3 (crate (name "clafrica-wish") (vers "0.3.0") (deps (list (crate-dep (name "clafrica") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "09mcrl4ix7q182gq892q475h79w2awh1qb0sqi5a42ddak4y8r02")))

(define-public crate-clafrica-wish-0.3 (crate (name "clafrica-wish") (vers "0.3.1") (deps (list (crate-dep (name "clafrica") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rstk") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0wv70310p2yrichz2b25361jvpar0j4jdfglj8zhzbaw24zx9z2n")))

