(define-module (crates-io cl un) #:use-module (crates-io))

(define-public crate-cluna-1 (crate (name "cluna") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.29.0") (default-features #t) (kind 2)))) (hash "065lj6d2x34j6l5zdcj640z0r69bn453rc657d71j17vxqccbdx1")))

