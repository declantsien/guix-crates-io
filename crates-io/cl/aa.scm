(define-module (crates-io cl aa) #:use-module (crates-io))

(define-public crate-claap-test-dont-use-0.1 (crate (name "claap-test-dont-use") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1gzjydypx4j3f67gxy2zvlcjf4r1z334igvpk9z852d6h8ggr1c1")))

