(define-module (crates-io cl ha) #:use-module (crates-io))

(define-public crate-clhash-sys-0.1 (crate (name "clhash-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "095644aqz3nlrj93gypg6mqib2f9ss38lbicv09cr8kxzzsbvgv1")))

(define-public crate-clhash_rs-0.1 (crate (name "clhash_rs") (vers "0.1.0") (deps (list (crate-dep (name "clhash-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0rn3sncik45bsqcm82mc1rx4f27gcf2pbsm84rdzpr0lr6wbqcha")))

