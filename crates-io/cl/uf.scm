(define-module (crates-io cl uf) #:use-module (crates-io))

(define-public crate-cluFlock-0.1 (crate (name "cluFlock") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (kind 0)))) (hash "0wciwb3lm5j3nddcnfdigy7jcmcz8hvhpck6c4rahnz5i125ff15")))

(define-public crate-cluFlock-0.1 (crate (name "cluFlock") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (kind 0)))) (hash "1fb3ahhzhxdxbpx6401h5kyhpw8c8hbrw1vfinnygpzqi4dj8sbx")))

(define-public crate-cluFlock-0.1 (crate (name "cluFlock") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)))) (hash "0d7f3hz2krp2n13gkjg1yi1fqpzaa90laxrcccxgigkwk865f5i1")))

(define-public crate-cluFlock-0.2 (crate (name "cluFlock") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "04dllv45d1yiji7d0hc7w70v7vak19blxnjx6spaz88pb9kgdz5l")))

(define-public crate-cluFlock-1 (crate (name "cluFlock") (vers "1.2.4") (deps (list (crate-dep (name "cluFullTransmute") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (kind 0)))) (hash "13f15nxy9jm2syzslwfcqvkgw56d7162k5mcp497273mv6ydz330") (features (quote (("nightly" "cluFullTransmute") ("default"))))))

(define-public crate-cluFlock-1 (crate (name "cluFlock") (vers "1.2.5") (deps (list (crate-dep (name "cluFullTransmute") (req "^1.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("fileapi" "minwinbase" "winnt" "ntdef"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gil2076kch93x2r5xjcp0mk8lhavqip0wjlc47syzd3drpmi6nw") (features (quote (("nightly" "cluFullTransmute") ("default"))))))

(define-public crate-cluFlock-1 (crate (name "cluFlock") (vers "1.2.7") (deps (list (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("fileapi" "minwinbase" "winnt" "ntdef"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "121g8q945lk5hqvxf571dqsjylg5lhaaacxn2x608f34v2xzmh55") (features (quote (("win_fix_woudblock_in_errresult") ("default" "win_fix_woudblock_in_errresult"))))))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.0.0") (hash "1kivvgh5bc0m269jjzyd9fm1sdqjz3m386glf797ac5ibh3jk435") (yanked #t)))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.0.1") (hash "0fzy44kv85v09rjapnvqq5vlcxk0dn4g2djbns1gdwib19s20r2r")))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.0.2") (hash "0b7lml4ffgqymxx6hkiv482sm8pdp4cqcs58lsnvcbvx99iy8jrq")))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.0.3") (hash "1rapl2vcmyb85dakg3rczlnvpsadk3shapfb6ilzp31z95f8pqgb")))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.0.5") (hash "06946zbnnxvydw7vwna63rkc4wwmsy2831jl4kn1p3p095vs7swk")))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.0.6") (hash "0hln3rnvpxf5xc07cvc5i9bybmzjiw69mdj4dh0ifknyzz0annky")))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.1.0") (hash "0qgjzsb4mn81x4cr266cpf93pfcssprxxjsxpp119z8sj8lqss8f")))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.2.0") (hash "0vn4an74xrski3wc5pg2nf2z4rf2fmxy4aghrx0vgshwzr1gwwgs") (features (quote (("support_size_check_transmute") ("default" "contract" "compatible_stdapi" "support_size_check_transmute") ("contract") ("compatible_stdapi"))))))

(define-public crate-cluFullTransmute-1 (crate (name "cluFullTransmute") (vers "1.3.0") (hash "0nmbngmr4j84wpnjy8pdd1cyhkwmsvgisjl6sz5q3lk0lwd8z2dz") (features (quote (("to") ("support_stderr") ("support_size_check_transmute") ("inline") ("default" "contract" "inline" "compatible_stdapi" "support_size_check_transmute") ("contract") ("compatible_stdapi"))))))

