(define-module (crates-io cl d3) #:use-module (crates-io))

(define-public crate-cld3-0.1 (crate (name "cld3") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "1rizj5mvjailb05g2w5d4yq1bsj31bhpnx6vwahxwdm87n7n3g8s") (links "cxx-cld3")))

(define-public crate-cld3-0.1 (crate (name "cld3") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "184rj4da2f1r57gk4hdn75cph444087p8pqbhvj6q3yss3nyf1cx") (links "cxx-cld3")))

