(define-module (crates-io cl ex) #:use-module (crates-io))

(define-public crate-clex-0.1 (crate (name "clex") (vers "0.1.0") (deps (list (crate-dep (name "ethnum") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "00lvxlhqynw9ing8vvmzc31hj4aaswahdpg6jxmapaq3cgafdfa5") (features (quote (("llvm-intrinsics" "ethnum/llvm-intrinsics") ("default" "ethnum"))))))

