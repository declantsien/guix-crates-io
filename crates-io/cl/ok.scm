(define-module (crates-io cl ok) #:use-module (crates-io))

(define-public crate-clokwerk-0.1 (crate (name "clokwerk") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pii7cpkppdqzj7zv72abgfc82sc0r51yvwsb9k5hwcjhd2fabzx")))

(define-public crate-clokwerk-0.2 (crate (name "clokwerk") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mm95fvdm72maa8h8px5s37y9kyyd64iffpan6fcppk4mi5zyp41")))

(define-public crate-clokwerk-0.2 (crate (name "clokwerk") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1b2gdb2qaw28i9qwpbzx331v0fglghrzk0hgajaa489q6qyckpix")))

(define-public crate-clokwerk-0.2 (crate (name "clokwerk") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fc8zizv5r84gn1nf87ww0dcv2hfcfnxrvvivc5k1x0a5i2v2vz9")))

(define-public crate-clokwerk-0.3 (crate (name "clokwerk") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)))) (hash "002m8sanss4m6bv5v7ih4v6zq04b1r992r6y51h3vn12js230mbh")))

(define-public crate-clokwerk-0.3 (crate (name "clokwerk") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)))) (hash "1mki1msfs7dxidc7n34nl0z7a8yb0yjh76ncqcwv3wgycx62ppqr") (yanked #t)))

(define-public crate-clokwerk-0.3 (crate (name "clokwerk") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)))) (hash "0m1xxbm8yhbmfhp1vvsg0lwyw3kzcbvlz1grsvdb316d6chf3vl3") (yanked #t)))

(define-public crate-clokwerk-0.3 (crate (name "clokwerk") (vers "0.3.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)))) (hash "01pc5jwn24izkri6rl8pfbpkhn8dnl4yr9dcsnrd4dpzjlj5b64z")))

(define-public crate-clokwerk-0.3 (crate (name "clokwerk") (vers "0.3.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)))) (hash "1ha8vjhzninb07804dx4a6pbhmywdn5m9gv2rka2iynf79nplygr")))

(define-public crate-clokwerk-0.3 (crate (name "clokwerk") (vers "0.3.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)))) (hash "1a9pka2470n6mvsjq78zlfn5xbp4c0pvslhdvs7yqbkqvwz3g6bs")))

(define-public crate-clokwerk-0.4 (crate (name "clokwerk") (vers "0.4.0-rc1") (deps (list (crate-dep (name "async-std") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.5") (features (quote ("rt" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0wxz44g9d1m5kzd3fg1x1h03sfq6l5njksijhig9mqs87ymj5jlc") (features (quote (("default" "async") ("async"))))))

(define-public crate-clokwerk-0.4 (crate (name "clokwerk") (vers "0.4.0") (deps (list (crate-dep (name "async-std") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.5") (features (quote ("rt" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0xhdy480fj3zcrmwry3q86x1inrkdbmihrvsy7fpwvfbbwv8s45x") (features (quote (("default" "async") ("async"))))))

