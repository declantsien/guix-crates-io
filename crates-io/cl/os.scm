(define-module (crates-io cl os) #:use-module (crates-io))

(define-public crate-clos-0.1 (crate (name "clos") (vers "0.1.0") (hash "146jsa54za40kmjbbl2i6znvzanlxb4h0a61f0qpjsfsjfpqxzr3")))

(define-public crate-closco-0.1 (crate (name "closco") (vers "0.1.1") (hash "1hglmi98mx9bjk4kka51wca2bxcylljkdss98ha3bavn9fbjwxb7")))

(define-public crate-close-0.1 (crate (name "close") (vers "0.1.0") (hash "1d5d4a10a77zfymhfik98dip3d83df2ypngdpml010603hr6g2ha")))

(define-public crate-close-0.1 (crate (name "close") (vers "0.1.1") (hash "03n2s0kxf76qzjjv4pyam1pxc9acwlkp8asm67bkdzaas7z0h1x6")))

(define-public crate-close-0.1 (crate (name "close") (vers "0.1.2") (hash "114fn308dyh1isv62rf2z4jfm0i1wsj9qjkkwd4q74mmk9kyryb1")))

(define-public crate-close-0.2 (crate (name "close") (vers "0.2.0") (hash "12zkyib4d9acb1ksn0fg6g0c4g03da57xc3gjvv24c5w6agg4mfk")))

(define-public crate-close-err-1 (crate (name "close-err") (vers "1.0.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ma3c1n929ca51p1nb4qlgkbd65618c2sl9f689xplmcr0yxh3ag")))

(define-public crate-close-err-1 (crate (name "close-err") (vers "1.0.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "18pfhb3gqrlc8p1q4hb3c79g6ywgjj2kpyq4pycikl2jr8h1pf3k")))

(define-public crate-close-err-1 (crate (name "close-err") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1zmvkksg47vszgk5lzzdslyjdf1rjnka40f7mxfczcr44v8dvp5h")))

(define-public crate-close-file-0.1 (crate (name "close-file") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1p5indsgviqr9hvlsybl15yj8l8433bllf6v8abid53i76lca5xn")))

(define-public crate-close-to-0.1 (crate (name "close-to") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "07909xlw9nzjxb5283chzjmm7jdfvvvg4nkav7l5cdlrjyvxizk5")))

(define-public crate-close-to-0.2 (crate (name "close-to") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1ibm20xx6cn9kx57ya9hccpd58s8qvpwpmh4h5ly5nbc8wx3kmjf")))

(define-public crate-close_already-0.1 (crate (name "close_already") (vers "0.1.0") (deps (list (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "09qa837vfrz99kisbq4kjx06i18yipzgwp4s693n2k8g7inbr9q5") (rust-version "1.72")))

(define-public crate-close_already-0.2 (crate (name "close_already") (vers "0.2.0") (deps (list (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "0llp40y4ampwnvvrd1048xdsi6yj7xid1dcr2nkk5a57hzfpg673") (rust-version "1.72")))

(define-public crate-close_already-0.2 (crate (name "close_already") (vers "0.2.1") (deps (list (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "0ivhmgdnxm8j75bnzxxbyb3xiranf55l632297fgkq1gwvjlkvvj") (rust-version "1.72")))

(define-public crate-close_already-0.3 (crate (name "close_already") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blocking") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mutually_exclusive_features") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "threadpool") (req "^1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (default-features #t) (kind 2)))) (hash "1ww36n8cw1679pf8ckgchh2d9ikab1pyd8apd272mzp5x3ws3brj") (features (quote (("default" "backend-threadpool")))) (v 2) (features2 (quote (("backend-tokio" "dep:tokio") ("backend-threadpool" "dep:threadpool") ("backend-smol" "dep:smol") ("backend-rayon" "dep:rayon") ("backend-blocking" "dep:blocking") ("backend-async-std" "dep:async-std")))) (rust-version "1.72")))

(define-public crate-close_already-0.3 (crate (name "close_already") (vers "0.3.1") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blocking") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "mutually_exclusive_features") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "threadpool") (req "^1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.1") (features (quote ("rt" "fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.1") (features (quote ("io-util"))) (default-features #t) (kind 2)))) (hash "09m9rys36yvrg526rss72d0nx8v0g9w509z1bsvlrmdpmwv0w29n") (features (quote (("default" "backend-threadpool")))) (v 2) (features2 (quote (("backend-tokio" "dep:tokio") ("backend-threadpool" "dep:threadpool") ("backend-smol" "dep:smol") ("backend-rayon" "dep:rayon") ("backend-blocking" "dep:blocking") ("backend-async-std" "dep:async-std"))))))

(define-public crate-close_already-0.3 (crate (name "close_already") (vers "0.3.2") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blocking") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "mutually_exclusive_features") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "threadpool") (req "^1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.1") (features (quote ("rt" "fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.1") (features (quote ("io-util" "macros"))) (default-features #t) (kind 2)))) (hash "0s2d58mws7vr18wi2i07z880dk7nqms1551d542j6zq7a86mf3yf") (features (quote (("default" "backend-threadpool")))) (v 2) (features2 (quote (("backend-tokio" "dep:tokio") ("backend-threadpool" "dep:threadpool") ("backend-smol" "dep:smol") ("backend-rayon" "dep:rayon") ("backend-blocking" "dep:blocking") ("backend-async-std" "dep:async-std"))))))

(define-public crate-close_already-0.3 (crate (name "close_already") (vers "0.3.3") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blocking") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "mutually_exclusive_features") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "threadpool") (req "^1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.1") (features (quote ("rt" "fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.1") (features (quote ("io-util" "macros"))) (default-features #t) (kind 2)))) (hash "16mi8marmpix3ssfxw451fcpx716anvzbwx8mmxhlfj6qwvha331") (features (quote (("default" "backend-threadpool")))) (v 2) (features2 (quote (("backend-tokio" "dep:tokio") ("backend-threadpool" "dep:threadpool") ("backend-smol" "dep:smol") ("backend-rayon" "dep:rayon") ("backend-blocking" "dep:blocking") ("backend-async-std" "dep:async-std"))))))

(define-public crate-close_enough-0.2 (crate (name "close_enough") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.26") (features (quote ("color" "wrap_help"))) (optional #t) (kind 0)))) (hash "1h6vz1s20g98hda3yniiw9wvrz15n8s4l29c9wvd7g6nxj6j2pyn") (features (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-close_enough-0.2 (crate (name "close_enough") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~2.26.2") (features (quote ("color" "wrap_help"))) (optional #t) (kind 0)))) (hash "12qi2lz2kpkq82hfyk61p9f3nd3l1krpjf18148y30hjh4z1wb2d") (features (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-close_enough-0.3 (crate (name "close_enough") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "~2.33.0") (features (quote ("color" "wrap_help"))) (optional #t) (kind 0)))) (hash "16b9d2q0x6x9xnjrkjg1wz8xwrrvnv96kbxbqw6jzmvg0d157x5f") (features (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-close_enough-0.4 (crate (name "close_enough") (vers "0.4.0") (deps (list (crate-dep (name "home") (req "^0.5.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "~0.3.21") (optional #t) (default-features #t) (kind 0)))) (hash "1rj339qhbqyj320fgi2l1fy6m88ixdhhhyh2kr5rgbh0jqc2x5p4") (features (quote (("default" "cli") ("cli" "home" "structopt"))))))

(define-public crate-close_enough-0.5 (crate (name "close_enough") (vers "0.5.0") (deps (list (crate-dep (name "directories") (req "~4.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "~0.4.18") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "~0.3.26") (optional #t) (default-features #t) (kind 0)))) (hash "0pg64q4zy369g1klilxna573ia9h3vpiz843f5i9jck07aafpqm9") (features (quote (("default" "cli") ("cli" "directories" "structopt"))))))

(define-public crate-close_fds-0.1 (crate (name "close_fds") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19v8w40mkr5k1nwh6y1ws7v14j2209qpy10f7qf71xpn3xf4qxhk")))

(define-public crate-close_fds-0.2 (crate (name "close_fds") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "192n44ll70d3s875l6zpz3nvmx9v0z0b4h981f9b3zlrnbamgw8b")))

(define-public crate-close_fds-0.2 (crate (name "close_fds") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r53kqp5dh75mr1lcmkb0fm1p0f0s6dqif1bp38pyw7kjnykphi2")))

(define-public crate-close_fds-0.2 (crate (name "close_fds") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x1dbhczrv2hxnyhpyy3i74c30sk7ad1par3wrlvf0bx0m3qq09c")))

(define-public crate-close_fds-0.3 (crate (name "close_fds") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cm4v5v55xd1xr9hqh21izg3m2sdymdqx4xd828v6k122ss9k058")))

(define-public crate-close_fds-0.3 (crate (name "close_fds") (vers "0.3.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iidn5smcgyfln2kd3sk3njcnzzpls3sz7qgccyb1xr9sf6khh9i")))

(define-public crate-close_fds-0.3 (crate (name "close_fds") (vers "0.3.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vd2i1gkvfcgdlzgrkgivhx3ky0zs98g8q3mwmwrxmg97pridi1v")))

(define-public crate-closed01-0.0.1 (crate (name "closed01") (vers "0.0.1") (hash "165csggkmlrc0djqdvxkkv8qjlzbx1s1sm6d1cds59956n0vzz3q")))

(define-public crate-closed01-0.1 (crate (name "closed01") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0fknya0vl9ah7s5y2bgrwmsca78m0rarnk2lisr7xm17658ya9nx")))

(define-public crate-closed01-0.1 (crate (name "closed01") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vkiinsvmgqyghjf8ql1cpz3vp1wvb102qzr8gy3ll2yypp2igkd")))

(define-public crate-closed01-0.2 (crate (name "closed01") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dvign0nyrw6491y2c2bj54x5zp1m7dipi6ncnih62g6a8k7kmmj")))

(define-public crate-closed01-0.3 (crate (name "closed01") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rbfsp7sask780400s8sbrrfjzj9zvzgyi82vddzl27nl8qx7yv1")))

(define-public crate-closed01-0.4 (crate (name "closed01") (vers "0.4.0") (deps (list (crate-dep (name "nalgebra") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "09v8vfx25vl2gjp4z0md1xrcnr5bm3n9hc4h2qzk5gdpnzkwwimz")))

(define-public crate-closed01-0.5 (crate (name "closed01") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0db2i1zf7gvadm96gvcs6xrblbgx55rw1ffqbv3ffbz7r45wzxpb")))

(define-public crate-closefds-0.1 (crate (name "closefds") (vers "0.1.0") (deps (list (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0l4lmyc4pzcbjjq4i5cj924vp9qw2zj6iwfrd8djc0l8a9miwk1n")))

(define-public crate-closer-0.1 (crate (name "closer") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 0)))) (hash "0hv0yg9c1z2lrqalzs5mg7wc8hvr06vjchxw8n433xr1xm6hp93i")))

(define-public crate-closer-0.1 (crate (name "closer") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 0)))) (hash "0s6igib4s99sd8vws4wp0g8gmnyv65pdy3qylk5ijn3czg6viisc")))

(define-public crate-closer-0.1 (crate (name "closer") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 0)))) (hash "00mdqawkpjbi5p9w7wvgzy1ylz8sa5zzpbhh491b3xd38yrx3icc")))

(define-public crate-closest-0.1 (crate (name "closest") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ap0lpahcfr9zvarhg7573bk79qiamijs4bs40w534n3gq26a140")))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.0") (hash "0rc3rsjzaczf2f9dyjkln3fpcn0k74rk7j1zfq2rvndjm1670f48") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.1") (hash "0bvs7s4xgcfi13156y5w0ml844a5ljbd41chqjvkasgc1mrks6mb") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.2") (hash "10rqafasz9d6sq43jy7i77w4w3myl8z1cd6ahr7599vzmyvbprz4") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.3") (hash "106pyn3j3wqcwv5c7mv0lygc1q920amg38qjnprfa0lqd4xyjxk6") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.4") (hash "031pbg6j8b4cpgjwpw5sim05dlri2j7djdhk0b0ihzwv5h75f3gf") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.5") (hash "1m220fk4ia2fnbfb5af54smlw2zyssr5d5v8rs567hqhfv2j0zmj") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.6") (hash "19b0nzwh81949411qh4zcmi0qyx7r6103fdriz7pw0k3llncg5aa") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.7") (hash "1pbkyhc3dpcrc5pja0ssxd5j2ld7fdgj26q41fhzazrkwihr8qc6") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.8") (hash "07m2h9dg3wd88s8ic3fwlbi0r9b86b7jc3q9dzfn2wjbjq8a0mr4") (yanked #t)))

(define-public crate-closest-sum-pair-0.1 (crate (name "closest-sum-pair") (vers "0.1.9") (hash "1ld9yd8gc6c1dzlxkbs5aj4mg6cp6brskmixmhh33jvmbcpj2zb5") (yanked #t)))

(define-public crate-closest-sum-pair-0.2 (crate (name "closest-sum-pair") (vers "0.2.0") (hash "1zpp6wz0wkxvc1jvcdfcc35gbczq4c81scmld3fybfsspcw8359m") (yanked #t)))

(define-public crate-closest-sum-pair-0.3 (crate (name "closest-sum-pair") (vers "0.3.0") (hash "12adgz7435ka1y8hgkzlky0mvr9bayq8a336racvz9f4p83h29zm") (yanked #t)))

(define-public crate-closest-sum-pair-0.4 (crate (name "closest-sum-pair") (vers "0.4.0") (hash "0bfrmi4cyb62knf9yj9a7dnrq2m97365kg5skb8718qdkakzwajz") (yanked #t)))

(define-public crate-closest-sum-pair-0.4 (crate (name "closest-sum-pair") (vers "0.4.1") (hash "0jkwbm2i9vr6cw8ni6fsnrpbj7h428gan2bvp16bbg3xkmrwdg6r") (yanked #t)))

(define-public crate-closest-sum-pair-0.5 (crate (name "closest-sum-pair") (vers "0.5.0") (hash "0bb7n4rg8ay55z5qcqdpmd3x1j3lqvfnrf76h7bm1y8v7wgsj2c8") (yanked #t)))

(define-public crate-closest-sum-pair-1 (crate (name "closest-sum-pair") (vers "1.0.0") (hash "1hqa991si9qv3w6vvxywnkfmydhs984wwar4d125zpwacwpagf9d")))

(define-public crate-closestmatch-0.1 (crate (name "closestmatch") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1ayyykyznhv2v16r3s578lzg19zvafpipq9g5h93nvnp5b43b8zh")))

(define-public crate-closestmatch-0.1 (crate (name "closestmatch") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "06zpiny9blc5qy5yk3pg29b419gi7ksdirpps2l2f3ry5cdpyi1g")))

(define-public crate-closestmatch-0.1 (crate (name "closestmatch") (vers "0.1.2") (deps (list (crate-dep (name "rayon") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "04qzdz500svag5dk9dkmd86xaxgmr3iw0bww4ls4na4k5wq7kl3a")))

(define-public crate-closet-0.1 (crate (name "closet") (vers "0.1.0") (hash "1iy6mkn5r3337bcmg2pibl250pxxb6nmr7lm438yl1k8akxr1n77") (yanked #t)))

(define-public crate-closet-0.1 (crate (name "closet") (vers "0.1.1") (hash "148awnk33kfqby2blhxbi179hjrvlhxj2jl2sp58c5wbqnymj6nm") (yanked #t)))

(define-public crate-closet-0.1 (crate (name "closet") (vers "0.1.2") (hash "08vp3vbiz86inhc0ij0r0dgddvj3i64fmh81f0bdlrlsddrdyr2l")))

(define-public crate-closet-0.1 (crate (name "closet") (vers "0.1.3") (hash "08bk5j3fq2yya4ql1wnn0gp8d7612y4kbx5x7xblfrn2zkyq6vmy")))

(define-public crate-closet-0.2 (crate (name "closet") (vers "0.2.0") (hash "0ylzrwiqw5p5zrpbdh8whpm4c3vsriqhcri3gq78rka4nxkpsvjn")))

(define-public crate-closet-0.2 (crate (name "closet") (vers "0.2.1") (hash "1l1v9ljipnq0kxr6qlzp5f7476lnj4jlfbrh8qm6y2mki247ibjh")))

(define-public crate-closet-0.2 (crate (name "closet") (vers "0.2.2") (hash "1iqiwkrbyjc2mb4ih9sziraxqrld2a2yn0gbig2668yp8922nws9")))

(define-public crate-closur-0.1 (crate (name "closur") (vers "0.1.0") (hash "1wl09hdkggsfpkq2fna7c71a99qrihyfdlxc173ril4a1fmr0d4w") (yanked #t)))

(define-public crate-closur-0.1 (crate (name "closur") (vers "0.1.1") (hash "1yd76i99a25ksaz6dpk72a7zvbq77wrq7h164kcb5hir56sgh4n3")))

(define-public crate-closur_adder-0.1 (crate (name "closur_adder") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0gsypc6jjxfsr6cx1340jpxcy80qfcgixbz6cfjk731mzsskakhp")))

(define-public crate-closure-0.1 (crate (name "closure") (vers "0.1.0") (hash "12wrgyjis1rn6dsvm5h7payxricly4z990zd5n4vb34k71bxmns1")))

(define-public crate-closure-0.1 (crate (name "closure") (vers "0.1.1") (hash "1mxvyyidp78pr1522f403c1r438wvw2lyaqa7gj40qvlp6a1x0vv")))

(define-public crate-closure-0.1 (crate (name "closure") (vers "0.1.2") (hash "10b74mjwnijrvdci7h9s7zpwkpkghs0nk78y52frqcsn7h37h8y6")))

(define-public crate-closure-0.2 (crate (name "closure") (vers "0.2.0") (hash "0rm06rahlmin3rwyflimflvs7faksw88gv8vnwyjzfli7ixj2a87")))

(define-public crate-closure-0.3 (crate (name "closure") (vers "0.3.0") (hash "10mp4hxbjz710s4sr7dbqi0p8qkm0xibgmvdaskia3b13gb3y5yn")))

(define-public crate-closure-demon-0.1 (crate (name "closure-demon") (vers "0.1.0") (hash "15py7jypkwvvzgh4x06bvg73g2b2bf3ay18x1891r4vznn2q3rxm")))

(define-public crate-closure-future-0.1 (crate (name "closure-future") (vers "0.1.0") (deps (list (crate-dep (name "async-oneshot") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)))) (hash "1yiblzbhji7zfizcyi9xyvk5d8wk0crf59db53h17nsimh5lamsv") (features (quote (("nightly-docs"))))))

(define-public crate-closure-pass-0.1 (crate (name "closure-pass") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "186gv0748z019z4gw504vn1qwcwrc2s334kivsjphzr5aqnh9s66")))

(define-public crate-closure_attr-0.1 (crate (name "closure_attr") (vers "0.1.0") (deps (list (crate-dep (name "closure_attr_core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fy3njgp3qrzqafq1fwvsx172d18mrpd5crf889l0izmrm9sf1wk")))

(define-public crate-closure_attr-0.2 (crate (name "closure_attr") (vers "0.2.0") (deps (list (crate-dep (name "closure_attr_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1q1rhwkhaa8qmwqpfnh66xj3p8ghw9xqzn1h5cma0m8ybcy7plh3")))

(define-public crate-closure_attr-0.3 (crate (name "closure_attr") (vers "0.3.0") (deps (list (crate-dep (name "closure_attr_derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qx5677pzqf3ys68fvi0p58wl4514s5k5651a7xnc4cpyy7gq7cz")))

(define-public crate-closure_attr-0.4 (crate (name "closure_attr") (vers "0.4.0") (deps (list (crate-dep (name "closure_attr_derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ilgz8c12v9p6qr0nxf6aw7p9mzd1kk3vwccnw0c7k6y4kfig5db")))

(define-public crate-closure_attr_core-0.1 (crate (name "closure_attr_core") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.31") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1a0ydq3894lzbn3407rcxqswy93rwwjjqi1l6nl4qg1vczxzvr6n")))

(define-public crate-closure_attr_core-0.2 (crate (name "closure_attr_core") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.31") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1fs7hi6sgxkyrj94p6rf4lw0bx4410mz3k1332bbs1m8lrnkg4j2")))

(define-public crate-closure_attr_core-0.3 (crate (name "closure_attr_core") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.31") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0iin3lhc1kza53f18gqimajhf0zfcvm5fa05xq0m3af9rnwm7w95")))

(define-public crate-closure_attr_core-0.4 (crate (name "closure_attr_core") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.31") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0q0flmw5am6z5d1l5lwmpbkmy1d0vxaw73zci0bnjkadwv734i87")))

(define-public crate-closure_attr_derive-0.2 (crate (name "closure_attr_derive") (vers "0.2.0") (deps (list (crate-dep (name "closure_attr_core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0p4ifzjap1nhr04yl26ah87m7jcfl6piydgj4jgdbigy4hrbs2kh")))

(define-public crate-closure_attr_derive-0.3 (crate (name "closure_attr_derive") (vers "0.3.0") (deps (list (crate-dep (name "closure_attr_core") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1272dnkaridbsgbcwzv5rbkrxzg2w4qnf8d18g2am685gragkif9")))

(define-public crate-closure_attr_derive-0.4 (crate (name "closure_attr_derive") (vers "0.4.0") (deps (list (crate-dep (name "closure_attr_core") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "11qsml7ngxz7jswdc9ykgrz8mprk1vyyxhxhkj9hfcz0arg36dgp")))

(define-public crate-closure_cacher-0.1 (crate (name "closure_cacher") (vers "0.1.0") (hash "0n4gi9mv4g00f59ny7cylpc7sr6dh8vzna2ynsclrpjmfk9dnwa5") (yanked #t)))

(define-public crate-closure_cacher-0.1 (crate (name "closure_cacher") (vers "0.1.1") (hash "1b9dvyrya3fhbmvvj9mxcb4hgakwhf9c4ddrzvj9rmvgawd6x2pp") (yanked #t)))

(define-public crate-closure_cacher-0.1 (crate (name "closure_cacher") (vers "0.1.2") (hash "1mxf31z13k3agd0k99417nhwhi7zkivzy0146r3ahwpi1bqh58n2") (yanked #t)))

(define-public crate-closure_cacher-0.2 (crate (name "closure_cacher") (vers "0.2.0") (hash "0qkaapkwmgpx6fcwh9zax08cihgkgf966axin71s5i6wq5kmk46h") (yanked #t)))

(define-public crate-closure_cacher-0.2 (crate (name "closure_cacher") (vers "0.2.1") (hash "0nwsg26v1sbdafzk8qksvx4j88z69pwlwwn5ji1q8h56945rjvl4")))

(define-public crate-closure_calculus-0.1 (crate (name "closure_calculus") (vers "0.1.0") (hash "0mf2jv5fxwg41waaqha7054jsjcfl7z9jg0j9q81qyv44cn6hxcm")))

(define-public crate-closure_example-0.1 (crate (name "closure_example") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02wki968d5jlh07n2klq3w00c0vhdpjaa0y0lzsvaqfxsqxf914c")))

(define-public crate-closure_manager-0.1 (crate (name "closure_manager") (vers "0.1.0") (hash "12k7gk8isi011r6a16yihclsnlqvvx5vgav8ri5l4irmzpd2v999")))

(define-public crate-closure_study-0.1 (crate (name "closure_study") (vers "0.1.0") (hash "1vms19rvi4frgblk7cm7jamr62jbnqra87jaihbfr7k0ds0r7rqr") (yanked #t)))

(define-public crate-closure_study-0.1 (crate (name "closure_study") (vers "0.1.1") (hash "0ji09wwjfdi78dgxik6zajq63j2vdi9qlq3w9i60dqdas9lx84jh") (yanked #t)))

(define-public crate-closure_study-0.1 (crate (name "closure_study") (vers "0.1.2") (hash "1s5wx1kf647rnzch51rzlag4vqnxka12hkjlxjf07frfv5dvxzlv")))

(define-public crate-closure_with_lee_study-0.1 (crate (name "closure_with_lee_study") (vers "0.1.0") (hash "11mfp677ghjqw1qi3bxqbp63gqn8dvz3byd12582bxacnvb9m168")))

(define-public crate-closures-0.1 (crate (name "closures") (vers "0.1.0") (hash "0wkbi6ln04f3fwaqs8wgiwbkssx1pjcild338910gsny0zmksbql")))

(define-public crate-closures-0.1 (crate (name "closures") (vers "0.1.1") (hash "18inph7hyl5y2q9jhdcpladv6w8ciwz5lpahc2b5y45hcxfgsrh0")))

(define-public crate-closures-0.1 (crate (name "closures") (vers "0.1.2") (hash "0gchfpxggw9vwcgchjakxasdg0zjh7k50vkd5xmy9lhg35zl42rp")))

(define-public crate-closures_test-0.1 (crate (name "closures_test") (vers "0.1.0") (hash "0xzxb9cwb31y61klzhhpkbivvx5qz360lcvlb7g2k33ny1m3rf7x")))

