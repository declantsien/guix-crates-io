(define-module (crates-io cl il) #:use-module (crates-io))

(define-public crate-clilib-0.1 (crate (name "clilib") (vers "0.1.0") (hash "1zijgrn117pmhc64p060xyf715dgb2da6pj0psr2kp597p7mi5wg")))

(define-public crate-clilib-0.1 (crate (name "clilib") (vers "0.1.1") (hash "0lgwq166qidjvha95j2rdp1xhl4rh0xpk5ikkyvfb7fwxmf9c322")))

(define-public crate-clilib-0.1 (crate (name "clilib") (vers "0.1.2") (hash "0ahg933fmk7d1c1iis46xkzgj1mrqcsqq24676cx16n8rk1akvi3")))

(define-public crate-clilib-0.1 (crate (name "clilib") (vers "0.1.3") (hash "0zz57hw6fv2fi9hvyz6impjavqpvbj8mvjanpq7jwywj5nxckys9")))

(define-public crate-clilog-0.1 (crate (name "clilog") (vers "0.1.0") (hash "0rvr68lmzx1czlxsfc4wq40wvddmcj0gbmw0ajsrvykbgxxd64v4")))

(define-public crate-clilog-0.2 (crate (name "clilog") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "logging_timer") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0zrbf7hfazqaxsarmr2ysmnxia4z6adzsn8qgc2liwabslds3w3v")))

(define-public crate-clilog-0.2 (crate (name "clilog") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "logging_timer") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0zfxn0vvg7gzdzbjw71dlwsr4fkgamlw3q7s8g1jhnn942i7p5in")))

(define-public crate-clilog-0.2 (crate (name "clilog") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "01mb5gnjg6xqy2sa8lwql3dlbdsa3v43bg9ivx7jribwyv8z2bfs")))

(define-public crate-clilog-0.2 (crate (name "clilog") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1z528mq2m5dvxsva0ljr4c2i2wq47sn5rns9asj1w92dd162n771")))

