(define-module (crates-io cl em) #:use-module (crates-io))

(define-public crate-clementine-0.0.1 (crate (name "clementine") (vers "0.0.1") (deps (list (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "0b9mg4dpy83sdh2isc8ry76d1f84wbv5g3zxis9lnxp7qgd9s7a5")))

