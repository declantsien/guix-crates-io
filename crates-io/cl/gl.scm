(define-module (crates-io cl gl) #:use-module (crates-io))

(define-public crate-clgl-0.1 (crate (name "clgl") (vers "0.1.0") (hash "1zydc1r6gmjx2p61c1qwi095z7g8an12xsmfykk0pm2z0nydznv5")))

(define-public crate-clgl-0.2 (crate (name "clgl") (vers "0.2.0") (hash "0pilp24i4i423s4nfp76rwp9y69014q9rrykxfxn2pz0ywfckklg")))

(define-public crate-clgl-0.2 (crate (name "clgl") (vers "0.2.1") (hash "0rqc4669zb0kf3nnrcznj14wi1741jkjhswjfcilzl6xg4lfjfdw") (yanked #t)))

(define-public crate-clgl-0.2 (crate (name "clgl") (vers "0.2.2") (hash "0sl5fbp5av4b3f9ikbkvdr6ci85s9lmwznngrbkrm0rn7csny5sa")))

(define-public crate-clgl-0.2 (crate (name "clgl") (vers "0.2.3") (hash "1l6zakxl2mlfb9j1d6nrbz6cm2w4hfsjpk68j825pp0pqpmwb5zh")))

(define-public crate-clgl-0.2 (crate (name "clgl") (vers "0.2.4") (hash "0vf0riryixs08g2rk2cssh6376yb2cpl6ck87pkn7sn9kpy4aw6i")))

(define-public crate-clgl-0.2 (crate (name "clgl") (vers "0.2.5") (hash "0335f6kzwvf0nn9ihim48va9d4sqh15k7gfp12rw0ncycx68zxzk")))

