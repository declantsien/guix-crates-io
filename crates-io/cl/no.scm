(define-module (crates-io cl no) #:use-module (crates-io))

(define-public crate-clnooms-0.5 (crate (name "clnooms") (vers "0.5.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1f8al1qbrzjsvqizc4bggfwq8s50dqw10jiwqbm1k29i7fs32i94")))

