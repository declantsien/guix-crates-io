(define-module (crates-io cl ud) #:use-module (crates-io))

(define-public crate-cludex-0.0.1 (crate (name "cludex") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1ij4zilqpwzrfxlfvxzfdk724ml34b29fla1l3wq5xwfja2c06d5") (yanked #t)))

