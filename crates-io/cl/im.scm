(define-module (crates-io cl im) #:use-module (crates-io))

(define-public crate-clima-0.0.1 (crate (name "clima") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "02mwznm48dk8b02s81mwzj2lyri72c934lw1r6rmaz7c7wh2yf69")))

(define-public crate-clima-0.0.2 (crate (name "clima") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0g0jpkrrd4827dh1jnqy92vvd4jkhsvgvr9yryr005ph0h0v5d4g")))

(define-public crate-clima-0.0.3 (crate (name "clima") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1c2iqsfvvd0fyw1nnh78clg3k10hfsx32yw1niz710iahqx4n7z8")))

(define-public crate-clima-0.0.4 (crate (name "clima") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0cbw4jfp3y6s5j686k7rpfl1v08z5dhbhbsic0bc7nppn0y91x1s")))

(define-public crate-clima-0.0.5 (crate (name "clima") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0a82jxyb046x1d28c55ik92x41r6wghwpgiqp3s92c35ijggfclq")))

(define-public crate-clima-0.0.6 (crate (name "clima") (vers "0.0.6") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.8.15") (default-features #t) (kind 0)))) (hash "01kd1rzgdb8i9sbgvqq0hjm774jsy3jk2q3mhypf0r6cfswxy9pv")))

(define-public crate-clima-0.0.7 (crate (name "clima") (vers "0.0.7") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "= 0.16.0") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "= 1.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "= 0.6.3") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "= 0.8.15") (default-features #t) (kind 0)))) (hash "1dp9hax688yvxxjf9c37xbj20dz02cjwz5q0agxahx8ivjrmpn4y")))

(define-public crate-clima-0.0.8 (crate (name "clima") (vers "0.0.8") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "=0.20") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1jbgdy5wh5xhagykfgpvigka09yxfwvnk2ab0pzhq3k3nh2fkap8")))

(define-public crate-clima-0.0.9 (crate (name "clima") (vers "0.0.9") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "terminal-light") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qzpclavcq24947v2p18srzhbr3261w4zd049i3s61mxpycs2qdw")))

(define-public crate-clima-0.0.10 (crate (name "clima") (vers "0.0.10") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minimad") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "terminal-light") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jx0yqchmg0ak2n6252wfa2b23w4qmrqsv7zg3dmvh74jayi7q81")))

(define-public crate-clima-1 (crate (name "clima") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "cli-log") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.9") (features (quote ("blocking" "rustls-tls"))) (kind 0)) (crate-dep (name "termimad") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "terminal-light") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0rnm3d7ha2pwrvbvyyyhi26aw54lz269fvwyxnxwm2lrjyhdjii1") (rust-version "1.58")))

(define-public crate-clima-1 (crate (name "clima") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "cli-log") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "deser-hjson") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.9") (features (quote ("blocking" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "terminal-light") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0pq1zkzn8llf1zva0n685gjc32l6a51kpz2k688k3dvyk2hcbf0k") (rust-version "1.58")))

(define-public crate-climage-0.1 (crate (name "climage") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19.4") (default-features #t) (kind 0)))) (hash "13m324j0wcy6r4w2sl1pklm79p8xbkvcf5xsg65dkjvnbm9mpf8a")))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.0") (hash "06n0my0r67b0p800jxj9j3gyv9gnj25al4zlmvpx09n1af2hxcp0") (yanked #t)))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.1") (hash "0irpxgfhyp96x85y2x77szz5i1pni62n4lvszpraym1i41wymvwd") (yanked #t)))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.2") (hash "012ldl7hfgln9n595pjrpfd5qarnv4iqpv4b6cm516ly4y6l38y3") (yanked #t)))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.3") (hash "1dh9dmxxwvixzdrffp6zxz97s5h7hprwd90fpz3i6zj592s9varm") (yanked #t)))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.4") (hash "0nmf83gfx4c2md9y6lp6ng3lrszbdqjhfp0abdi6z38pnm222yvi") (yanked #t)))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.5") (hash "0mv40q14yv3ngrrpx9h0avnfcl1iv3j742jark8nnvijk0z6209x") (yanked #t)))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.6") (hash "1q08l6677kf5qv3ggxy7106vazs6a03hq3qxmag8m1aj5blr2kbh") (yanked #t)))

(define-public crate-climake-0.1 (crate (name "climake") (vers "0.1.7") (hash "1wsfv6awqyx9xk0b0psqdjsya4jvk9a6dzlrjj2pig65fkk0m47a") (yanked #t)))

(define-public crate-climake-0.2 (crate (name "climake") (vers "0.2.0") (hash "1kyx92l9qi6wik4inh3qs1gvdxncc5alfxzbpxarglr32i7f734d") (yanked #t)))

(define-public crate-climake-1 (crate (name "climake") (vers "1.0.0") (hash "1a0l4d355bh4nrpf12867glj9g1j68wsgr999wnl3p8b7bxr5iiw") (yanked #t)))

(define-public crate-climake-1 (crate (name "climake") (vers "1.0.1") (hash "1n8qzwx4iyfpfsy5iszvqwsrvfmxr6rm4bwj9wghbjl4i0wjxxq6") (yanked #t)))

(define-public crate-climake-1 (crate (name "climake") (vers "1.0.2") (hash "0y33hql9pmj24hqxmjf468719nca916l0dp7i4g5hb24rk20xxpr") (yanked #t)))

(define-public crate-climake-1 (crate (name "climake") (vers "1.0.3") (hash "182mz5l9sd6h07kirhxsgf5rg1rhr4h7c4fmsh29r8360s2rjaba") (yanked #t)))

(define-public crate-climake-2 (crate (name "climake") (vers "2.0.0") (hash "1xcn06a9zrznd8hfbhdmn9rdkywp0b2d592j56ggqc4nf01hbpdl") (yanked #t)))

(define-public crate-climake-2 (crate (name "climake") (vers "2.0.1") (hash "15bx4cdny96sj41c7i0cvpfxkvvg4aargm7j9f41avh299y7f4zj") (yanked #t)))

(define-public crate-climake-2 (crate (name "climake") (vers "2.1.0-pre.1") (hash "0qv4q2rsqqvmazyg1jr06wjs2848n96rkqaqpjfbxha2p2klrmll") (yanked #t)))

(define-public crate-climake-2 (crate (name "climake") (vers "2.1.0") (hash "153lpmf8hi2gi4xh9q2nkvxi6kzgzc3npz730h0m8kmw1p131pdq") (yanked #t)))

(define-public crate-climake-2 (crate (name "climake") (vers "2.2.0") (hash "1s82566844339ppbds0i6hsizqxh8b2mjh1g5bzsbywp3kqcmpfn") (yanked #t)))

(define-public crate-climake-2 (crate (name "climake") (vers "2.2.1") (hash "1nfgxzzjmg548a9q5xdabf8rzkf0rzl5wzmdjrl6xrdr53dissqn") (yanked #t)))

(define-public crate-climake-2 (crate (name "climake") (vers "2.2.2") (hash "0g84cvkv0644g187h1rm3q50amnikyslk6a5qqsax438miyk9qni") (yanked #t)))

(define-public crate-climate-0.1 (crate (name "climate") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zblnvflrdql2skb6a306gf0542hfil82qz26qvddmryyls3wih4") (yanked #t)))

(define-public crate-climate-cli-0.0.1 (crate (name "climate-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "1q84szx8z4z1s90jhpdk66rk1pp9z24r3isbxv24lqggadxf4ifz")))

(define-public crate-climate-si7020-0.1 (crate (name "climate-si7020") (vers "0.1.0") (deps (list (crate-dep (name "tessel") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0nd7jpskcrsdly7fn5jda8z8wmsi6bw72cx5y1qc26bk94z4p4b4")))

(define-public crate-climatempo-0.1 (crate (name "climatempo") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.4") (default-features #t) (kind 0)))) (hash "030pgxxwz3js72ljq2ydcaxh4pvf11gjprifxdx200167krvv4mv")))

(define-public crate-climb-0.1 (crate (name "climb") (vers "0.1.0") (hash "0qpifbl92afbzixfq69cymqghqm0q0gjjcc6xdjd1wcal31c8557")))

(define-public crate-climb-0.1 (crate (name "climb") (vers "0.1.1") (hash "0plshfxfylasqvqzh3210rzrjllx8v7lp0r4acn31xvfsm6qd7hn")))

(define-public crate-climb-1 (crate (name "climb") (vers "1.0.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0d3nllfq0swliqkcb15axfcv70akmb63bw5srv1zj7xpc8mx7c84")))

(define-public crate-climb-1 (crate (name "climb") (vers "1.0.1") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0jzy93pp4rn54ccj3pwq96vlikfv6zg6xx3flw1xa53z3rh9fa5d")))

(define-public crate-climb-1 (crate (name "climb") (vers "1.0.11") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "16jz18c86vbvrcmvvknr0hv7addp5kgyhnm4dskcwcn998x7akaw")))

(define-public crate-climb-1 (crate (name "climb") (vers "1.0.2") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1mhy9nvrpy2x67l9fd1m3cqsi87dh8l0cq3gamdc71awf4r9vhp5")))

(define-public crate-climb-1 (crate (name "climb") (vers "1.0.21") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0zf6ix1fk6d68z09mq56qg9s1i7zz6bd5705y0a59ilgh22463jr")))

(define-public crate-climb-1 (crate (name "climb") (vers "1.0.22") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1lllv696clvfd4w1idr7x87cv9wdl4b6335brs0kh72s1bcrcsm1")))

(define-public crate-climb-2 (crate (name "climb") (vers "2.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0wbh5h21cipd4n6fanx3z8zk5is09a0mp50ids1ggvd5pss52rdy")))

(define-public crate-climb-2 (crate (name "climb") (vers "2.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0dbqy9gmggwmxa7m3p9k5sq9ysca2vpf2wl096n9fvh2k44hn64k")))

(define-public crate-climb-2 (crate (name "climb") (vers "2.0.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0gf278vvdsis9w91q5szqdrh7y2i8rc8wvw7kzjspcxgylpfg8r4")))

(define-public crate-climb-2 (crate (name "climb") (vers "2.0.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1npiqzb7lqlrfcz7dak5dcgmm6g79k65p7kvm1chmrhvqaq7y3wr")))

(define-public crate-climb-2 (crate (name "climb") (vers "2.0.4") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0khkxq125sk1d62rrwrfwhn3w7l3nwj36lv1qzp36wn0bmnf2gnb")))

(define-public crate-climb-2 (crate (name "climb") (vers "2.0.5") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1jl5rqa6yzz2g7zsg9h40dsh7yc6if6pmkmkqha29wda0rymig64")))

(define-public crate-climer-0.0.3 (crate (name "climer") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1xxzr5h4170fsqypg7dv44qcw8wy0zslha57mki7cw307jdab8b2")))

(define-public crate-climer-0.0.3 (crate (name "climer") (vers "0.0.3-1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0y0plq57is904xz5fq5mphib3949a2fr3c92l6pmh0jma7ik409h")))

(define-public crate-climer-0.0.4 (crate (name "climer") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1f133j85clhh3svdx1052rhp5vsdhf173dh3hvj42spwrxhryg8x")))

(define-public crate-climer-0.1 (crate (name "climer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ahy374i77bss6pmg50zz2g9yd58p4ma5j24xc694kxjgsbll0hj")))

(define-public crate-climer-0.2 (crate (name "climer") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0c33025bprsqas3i983szxja9b6gjwvz12lnz9y2jb2ik3vx7s84")))

(define-public crate-climer-0.3 (crate (name "climer") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1fh61g33qspjgc70nkljg0043bvkq3x6dzfn15agrwh13c8cigfa") (features (quote (("cli" "clap"))))))

(define-public crate-climer-0.3 (crate (name "climer") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0n51rdznaamsc4lhd0rma3rzrw4440xrrsccs8r831v0h3lv0rd8") (features (quote (("cli" "clap"))))))

(define-public crate-climer-0.3 (crate (name "climer") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0n69g377vvbyy3lw2gwm0lv7m8b9him0rrkj38q3a1kiy5d84hdm") (features (quote (("cli" "clap"))))))

(define-public crate-climer-0.3 (crate (name "climer") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "043036p945xhwzh84brbmg07w8zmycv7h82c52czjix5znbn3dyz") (features (quote (("cli" "clap"))))))

(define-public crate-climer-0.3 (crate (name "climer") (vers "0.3.4") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "07xq7zf4dhv99klqgkzbn6vsr6szj9h8fwcxnvmyzbx21y7v0v4w") (features (quote (("cli" "clap"))))))

(define-public crate-climer-0.3 (crate (name "climer") (vers "0.3.5") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "1ay60drddfkj7zsivby9fag7v293jq747as8dz6gnmg5pl7912ik") (features (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.3 (crate (name "climer") (vers "0.3.6") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "1vza7p1rhl82bcb07lvc4pk7595h1vkn1vikxrprbbx6l19zir5h") (features (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.4 (crate (name "climer") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "0bqhh81hbz42ll91nrdhbjqpgbc3glc3kj438zrs2sj6l25mk38j") (features (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.5 (crate (name "climer") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "0rd4jk1mcndgr7176macnsbrnlald08qrzmjc33vk8a1by55hf8c") (features (quote (("serialize" "serde") ("cli" "clap"))))))

(define-public crate-climer-0.6 (crate (name "climer") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "0caxdw5lkf4hsg8l5h22qc582cw66zw52s5bjm1c5n8db7a1x3bv") (features (quote (("serialize" "serde") ("parser" "regex") ("default" "cli") ("cli" "clap" "parser"))))))

(define-public crate-climer-0.7 (crate (name "climer") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "1zsnf2i5667446czcjibj000hqf0gr89abz1i3wc7h24p9g0hi3i") (features (quote (("serialize" "serde") ("parser" "regex") ("default" "cli") ("cli" "clap" "parser"))))))

(define-public crate-climer-0.7 (crate (name "climer") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "climer_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "095xw8nlm5z78ap72j9cv781f4a03473n3qybk5v6r12ygrc10vd") (features (quote (("serialize" "serde") ("parser" "regex") ("default" "cli") ("cli" "clap" "parser"))))))

(define-public crate-climer_derive-0.1 (crate (name "climer_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (default-features #t) (kind 0)))) (hash "1k2chcqk1n3zvrax68yd9gjdiy5pjlfn14cad8q452nn5qzzwqfw")))

(define-public crate-climeta-0.1 (crate (name "climeta") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "elsa") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "13mw38vc9wi7lrd86pr5mpwlpl3s6729wp7idxyngn4q40r93az7")))

(define-public crate-climm-0.0.1 (crate (name "climm") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)))) (hash "0wnmsxvqsqsa8557mi0nv7qk6dcig9qjcznz463lmk71bzpf6siq")))

(define-public crate-climm-0.0.2 (crate (name "climm") (vers "0.0.2") (hash "1sb7h8vqws1zchhbl3ppcqi04xm3n7zcgsmvzy79wmfw7vbcqh9a")))

