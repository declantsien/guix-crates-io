(define-module (crates-io cl ov) #:use-module (crates-io))

(define-public crate-clova-webhook-service-0.0.1 (crate (name "clova-webhook-service") (vers "0.0.1") (deps (list (crate-dep (name "lambda_http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lambda_runtime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1") (default-features #t) (kind 0)))) (hash "0kv4kc4qlsks1np8ipz7gnr7rjwlmj52diskar9n4v5lgdl0iwpm") (yanked #t)))

(define-public crate-clova_webhook_utils-0.0.1 (crate (name "clova_webhook_utils") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1") (default-features #t) (kind 0)))) (hash "0dq0nga4i2nzzg6cxwakx7pfsk7cfgxhbpgjf553fim6i8v9vds7")))

(define-public crate-clova_webhook_utils-0.0.2 (crate (name "clova_webhook_utils") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1") (default-features #t) (kind 0)))) (hash "0yxnyzy885v2jvysrfz091cl0nfn74szlnnnfk1zpp6zxaw9g5zb")))

(define-public crate-clova_webhook_utils-0.0.3 (crate (name "clova_webhook_utils") (vers "0.0.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1") (default-features #t) (kind 0)))) (hash "1bhizj4k5dnd403ajdwhlxz6asfhpb1lgz9gm204qpsxdgfs3p4d")))

(define-public crate-clova_webhook_utils-0.0.4 (crate (name "clova_webhook_utils") (vers "0.0.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1") (default-features #t) (kind 0)))) (hash "0hfc3ca4zq7yv6msg8011xp19k1hfgp4ni0x7saf9yxkpipzr00w")))

(define-public crate-clover-0.0.1 (crate (name "clover") (vers "0.0.1") (hash "1vcsp20lfn7n53jlb830f0g5qalp4mnyrlhmb93292bw259lni9q")))

(define-public crate-clover-0.0.2 (crate (name "clover") (vers "0.0.2") (hash "1g8mvdrj4ljzcwrifh2hx16xxs83mq4ndz5jip4jihr94nqpnmdy")))

(define-public crate-clover-0.1 (crate (name "clover") (vers "0.1.0") (hash "1rl9k4sl4za78rqvkvay7bvagiyq2ca73qc69z3vl9612zxmvlyw")))

(define-public crate-clover-0.1 (crate (name "clover") (vers "0.1.1") (hash "0hfi6301r1hjvh79j7y30zbkh10c1vr2zz2mp662xbch1gqzly8p")))

(define-public crate-clover-0.1 (crate (name "clover") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0ykf1r51arark12j9284c0bgfk30xlxmspn26k8pk6zc2x6wh38k")))

(define-public crate-clover-0.1 (crate (name "clover") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0nb5xmwlv8d81701fcfym5wahl7bwzick0hiins26xivz141p3kl")))

(define-public crate-clover-cli-0.1 (crate (name "clover-cli") (vers "0.1.0") (deps (list (crate-dep (name "clover") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clover-std") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hhsv7j8fvyxxyny4zba7i17y0hic7hb3nvbvlx06mn6v7dvkhpy")))

(define-public crate-clover-cli-0.1 (crate (name "clover-cli") (vers "0.1.1") (deps (list (crate-dep (name "clover") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clover-std") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1v9jwxzjy5i9s9d3z4wy21yqlx24ayh3mlz68qrm0qkcnsj80a1g")))

(define-public crate-clover-cli-0.1 (crate (name "clover-cli") (vers "0.1.2") (deps (list (crate-dep (name "clover") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "clover-std") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0d68ig2cz7ankc4sn1f6m4rzjdii4bjwrbkzgmrdiknpr4s9yild")))

(define-public crate-clover-cli-0.1 (crate (name "clover-cli") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clover") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "clover-std") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1aj2q6zizh2iqz69bhp5xajn7k0q04ix207gf5hf1my8f90y6mbk")))

(define-public crate-clover-std-0.1 (crate (name "clover-std") (vers "0.1.0") (deps (list (crate-dep (name "clover") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "13qyb35q872z61g4lgi95jla78ccxrnsw0bam6vlyjqhxspc81p5")))

(define-public crate-clover-std-0.1 (crate (name "clover-std") (vers "0.1.1") (deps (list (crate-dep (name "clover") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "02zfvz3n1dddlz0wvvpvgdc216h78crrqi21dxwwrhgpsqij1v0w")))

(define-public crate-clover-std-0.1 (crate (name "clover-std") (vers "0.1.2") (deps (list (crate-dep (name "clover") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0i4f0ggchgwkxg9jiyq805lq1vqc2hl246y8g29jjbf884m086cw")))

(define-public crate-clover-std-0.1 (crate (name "clover-std") (vers "0.1.3") (deps (list (crate-dep (name "clover") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1sld4nkps7vk8j4cp36vi96333cz65fwj3my2ifzcmcksmkyrha9")))

