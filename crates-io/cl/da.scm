(define-module (crates-io cl da) #:use-module (crates-io))

(define-public crate-cldap-0.1 (crate (name "cldap") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0g9qn997xrzx05ydm0k4gq0q1m2wy4vzjs2b9ii2i1gj8n1xwyw8") (yanked #t)))

