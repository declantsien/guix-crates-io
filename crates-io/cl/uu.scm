(define-module (crates-io cl uu) #:use-module (crates-io))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.0") (deps (list (crate-dep (name "clucstr") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "1agq3sr1k5yxi1bccngk6dpx1cq555svjvn7hadrj4q7j0n5d0h3") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.1") (deps (list (crate-dep (name "clucstr") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "00sacwrliv3pawc4w946422i9czjh68zjczvvhhh6fnfa8rsamb6") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.2") (deps (list (crate-dep (name "clucstr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "095f7xnj4kgbc8waay0k3p34gfahdq39gvsxyd8ky1v6jlqp8s9j") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.4") (deps (list (crate-dep (name "clucstr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "0mq2vw74ziddmc6aiix7w16jzzb3zpnibvv6p7q8caw6vm0zf69g") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.5") (deps (list (crate-dep (name "clucstr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "0n5x2g7pzjggswx03qpbr4yihsc14hc5dmmhyjkbsz7w5ypq63ma") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.6") (deps (list (crate-dep (name "clucstr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "1a2bkj5cmh8iqiv361f4pr9d2pbv7a73cwild9pjinvnl7xj0qf3") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.7") (deps (list (crate-dep (name "clucstr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "0wvmzpidcf9nnr81gcbbrvp2i0ilkmr7ia07qddqqnrhkjdx1nmd") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.8") (deps (list (crate-dep (name "clucstr") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (kind 0)))) (hash "0fgkvsxv6x38ri4yci5dw0bk4b5qkzdzfdf35w2kyn8yapcn0bj7") (features (quote (("enable_domainname"))))))

(define-public crate-cluuname-0.1 (crate (name "cluuname") (vers "0.1.9") (deps (list (crate-dep (name "clucstr") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)))) (hash "0q8l3skqn6szz14nl9irnfljjlnai15d52ggvhaql78zsrnnpiwv") (features (quote (("enable_domainname"))))))

