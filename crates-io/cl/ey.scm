(define-module (crates-io cl ey) #:use-module (crates-io))

(define-public crate-cleye-0.1 (crate (name "cleye") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "loggerv") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1aihfjg7z0p7181nj8gaxffrif7c65qaa36a0aan4nyaa1yd2zcd")))

