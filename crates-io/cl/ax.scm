(define-module (crates-io cl ax) #:use-module (crates-io))

(define-public crate-claxon-0.0.1 (crate (name "claxon") (vers "0.0.1") (hash "16lp5cd8qxv2cdlhz84cfsbrk87vjb4ibqbv09xiir47arynj086")))

(define-public crate-claxon-0.1 (crate (name "claxon") (vers "0.1.0") (deps (list (crate-dep (name "hound") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1h4cvhb9cdnwysa5w03wan6b39irkrr2j7ycv11px0nzd4qlsq67")))

(define-public crate-claxon-0.2 (crate (name "claxon") (vers "0.2.0") (deps (list (crate-dep (name "hound") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1zaf7lf92cb1x9lr3p8sqi5qss5hzr3f8xgdx01zx63jy61ynv19")))

(define-public crate-claxon-0.2 (crate (name "claxon") (vers "0.2.1") (deps (list (crate-dep (name "hound") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "03gp83v4b864w2jfypm54dz2rh7k1s90b32yvvgla0m3js9n7hh0")))

(define-public crate-claxon-0.3 (crate (name "claxon") (vers "0.3.0") (deps (list (crate-dep (name "hound") (req "^3.0.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 2)))) (hash "122z9vb5l1r3fic6ikpbqwvsnhgiaghripagm0yqqgrvjk4rz76r")))

(define-public crate-claxon-0.3 (crate (name "claxon") (vers "0.3.1") (deps (list (crate-dep (name "hound") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1.0") (default-features #t) (kind 2)))) (hash "1iw4kq6551zv4y9sphbiwz5237jas2r1a01sacwxm2jri5rj7lc4")))

(define-public crate-claxon-0.4 (crate (name "claxon") (vers "0.4.0") (deps (list (crate-dep (name "hound") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "mp4parse") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "ogg") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1.0") (default-features #t) (kind 2)))) (hash "1awkp1llz99wk6hdbwvbl4graza3zwmm4x1missap73jm13d0kpa")))

(define-public crate-claxon-0.3 (crate (name "claxon") (vers "0.3.2") (deps (list (crate-dep (name "hound") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1.0") (default-features #t) (kind 2)))) (hash "1b53ysazizyrjjmvgdkrs2lcmjbins74qxz6r90jg34mrrf6zyzd")))

(define-public crate-claxon-0.4 (crate (name "claxon") (vers "0.4.1") (deps (list (crate-dep (name "hound") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "mp4parse") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "ogg") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1.0") (default-features #t) (kind 2)))) (hash "0gyalrfka16cg39il657j70l3rpp2zgx8ycl9dmjxvfj9k7vmv9p")))

(define-public crate-claxon-0.3 (crate (name "claxon") (vers "0.3.3") (deps (list (crate-dep (name "hound") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1.0") (default-features #t) (kind 2)))) (hash "1j4p47y8hs88ysh4z8k9sa2bda3ni2lbfrjv2dg90v44zybka69m")))

(define-public crate-claxon-0.4 (crate (name "claxon") (vers "0.4.2") (deps (list (crate-dep (name "hound") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "mp4parse") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "ogg") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1.0") (default-features #t) (kind 2)))) (hash "001vqidga7984s79lgbrvna934qsxvyvl2dgmfkvv5d44wkrav7q")))

(define-public crate-claxon-0.4 (crate (name "claxon") (vers "0.4.3") (deps (list (crate-dep (name "hound") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "mp4parse") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "ogg") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1.0") (default-features #t) (kind 2)))) (hash "1206mxvw833ysg10029apcsjjwly8zmsvksgza5cm7ma4ikzbysb")))

