(define-module (crates-io cl iq) #:use-module (crates-io))

(define-public crate-cliq-0.1 (crate (name "cliq") (vers "0.1.0") (deps (list (crate-dep (name "toml") (req "^0.8.13") (default-features #t) (kind 0)))) (hash "03sgklbxy3pkx3m2h0sf0s25qhp0r3dgp24bzz2613gb1fnxndnz")))

(define-public crate-clique-0.0.0 (crate (name "clique") (vers "0.0.0") (hash "019cncypj3gzm18s3gldpppwd0jpafkx0mdp1bviz6rbxfny3648")))

(define-public crate-cliquers-0.0.1 (crate (name "cliquers") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ad0b9ixiba2cmvr0sarl274gp5x3hbz9rbxjgjg5ip1mgssxp4x")))

(define-public crate-cliquers-0.1 (crate (name "cliquers") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hv4a9vlpx9ly15hmjdj6lswwsk4p0z37pkgi5xavqqnbq4lviz2")))

(define-public crate-cliquers-0.2 (crate (name "cliquers") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "10irvwliw9ffqlrwjb11djjig5md7wvsq9zyp5yybmg6wghnx5bd")))

(define-public crate-cliquers-0.3 (crate (name "cliquers") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "1js1113vys5f6c34i3l3riz9zh0507qlbnvpfk64xavrx7rbaq7y")))

