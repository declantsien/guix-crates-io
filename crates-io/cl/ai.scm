(define-module (crates-io cl ai) #:use-module (crates-io))

(define-public crate-clai-0.1 (crate (name "clai") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08j26fw9kbsb9pnsm2fzg6xcb4h98bjyy0xhzgcfl5xid26kfc78")))

(define-public crate-claim-0.0.0 (crate (name "claim") (vers "0.0.0") (hash "0hqps1i2hnwiqxrarxfa2xck8bawllvs9d5k243wh49igr9yrzdb")))

(define-public crate-claim-0.1 (crate (name "claim") (vers "0.1.0") (deps (list (crate-dep (name "autocfg") (req "~1.0") (default-features #t) (kind 1)))) (hash "18m1dlrc732yngx81aqw0dmzfh2pxq415bykq74wy3145rmdz3vx")))

(define-public crate-claim-0.1 (crate (name "claim") (vers "0.1.1") (deps (list (crate-dep (name "autocfg") (req "~1.0") (default-features #t) (kind 1)))) (hash "0g2ps4f4jv6x91cmz8wjmd350bxzj6z6fl3nccs5f5ix6jiq1wjb")))

(define-public crate-claim-0.2 (crate (name "claim") (vers "0.2.0") (deps (list (crate-dep (name "autocfg") (req "~1.0") (default-features #t) (kind 1)))) (hash "0rdzdi3kv0n99v5arj52fknvsi8f84qiw201kkq6zfqmz2pbfb7d")))

(define-public crate-claim-0.3 (crate (name "claim") (vers "0.3.0") (deps (list (crate-dep (name "autocfg") (req "~1.0") (default-features #t) (kind 1)))) (hash "1anhhcdqy1kbgjszamhhgsh2z365y737za3qq2lcyz219wwajrz5")))

(define-public crate-claim-0.3 (crate (name "claim") (vers "0.3.1") (deps (list (crate-dep (name "autocfg") (req "~1.0") (default-features #t) (kind 1)))) (hash "192vi55kav92259qmv04liywpjbv95rfmkkw8mqjgwcbwqz8jbib")))

(define-public crate-claim-0.4 (crate (name "claim") (vers "0.4.0") (deps (list (crate-dep (name "autocfg") (req "~1.0") (default-features #t) (kind 1)))) (hash "1fcla6qz6c2i350q8gr09ji7ds1ginb6i4whi1q9mcjminakgbb8")))

(define-public crate-claim-0.5 (crate (name "claim") (vers "0.5.0") (deps (list (crate-dep (name "autocfg") (req "~1.0") (default-features #t) (kind 1)))) (hash "1s1wmdqa1937x3dhczxv284nfsk69ci4f8xva1nxzqbjpgb9j47q")))

(define-public crate-claim-ip-1 (crate (name "claim-ip") (vers "1.0.0") (deps (list (crate-dep (name "nix") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1g00b1c1idbfi65nranhn3hvm4d1sidjhn0k82vx11krfpva2gbx")))

(define-public crate-claim-ip-1 (crate (name "claim-ip") (vers "1.0.7") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "19kbpdihk4abq79z27d9cddyjh5hh0gfgsphpk4r8ns3qw3zvfb7")))

(define-public crate-claim-ip-1 (crate (name "claim-ip") (vers "1.0.8") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "04n4s8cdpk8cq4ypvy3qs984jyji5h8hl75547pjz336pngkm663")))

(define-public crate-claim-ip-1 (crate (name "claim-ip") (vers "1.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1dnj3ydd81flj0913pkxwc7vmlrcd6iyppc96hz347dfcr8dyp67")))

(define-public crate-claim-ip-1 (crate (name "claim-ip") (vers "1.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "182nj6a42v5mqdrjz84hs1g7z4m27ll3y8g996jcrwprv904rsli")))

(define-public crate-claim-itch-bundle-0.1 (crate (name "claim-itch-bundle") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fantoccini") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bs3cgdjbxnwbvxy7jbjrf9cq15aa6wpw5z31lmrbfng9y8i6gzy")))

(define-public crate-claims-0.6 (crate (name "claims") (vers "0.6.0") (deps (list (crate-dep (name "autocfg") (req "^1.0") (default-features #t) (kind 1)))) (hash "1vwqmbqqf0ml4vkjzys3cqmais0m35h6m5xvfids43hvhckphhbh")))

(define-public crate-claims-0.7 (crate (name "claims") (vers "0.7.1") (deps (list (crate-dep (name "autocfg") (req "^1.0") (default-features #t) (kind 1)))) (hash "1da6z2r4zz4fw4a69286s54jzr7g7sz3dspq0xiw6mk432z5p6dn")))

(define-public crate-CLAiR-testing-0.1 (crate (name "CLAiR-testing") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1.5") (default-features #t) (kind 2)))) (hash "0wnb3lwyahznifbrqgml8lzarc2sz80ghcizj7xamadnwacncb9v")))

(define-public crate-claire-0.0.0 (crate (name "claire") (vers "0.0.0") (hash "0q07qlpbrh9smcvna5hgkxrrb7lkiklqjh37x068carviypydnsd")))

