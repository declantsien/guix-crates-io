(define-module (crates-io cl ef) #:use-module (crates-io))

(define-public crate-clef-0.0.0 (crate (name "clef") (vers "0.0.0") (hash "1qyzvciknzl0mq4li2k16x0m1psniwqvlwvy4p0z9rmiyxs84qwi")))

(define-public crate-clef-0.0.1 (crate (name "clef") (vers "0.0.1") (hash "0s9yvawmcld98n0dnzig8nslq2vhzh57g2badb60cyx8gdn6cfxa")))

(define-public crate-clef-0.0.2 (crate (name "clef") (vers "0.0.2") (deps (list (crate-dep (name "contracts") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0fw7m8r902fbg13q4gxc47kc3nl0ihvv7g3738xkqg73a42vn8mg")))

