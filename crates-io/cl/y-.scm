(define-module (crates-io cl y-) #:use-module (crates-io))

(define-public crate-cly-impl-0.1 (crate (name "cly-impl") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "isnt") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "repc-impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1drbaasyfhscc7c6mn99xxh6ahsyqfzzzccz400n1iywxp1gf990")))

(define-public crate-cly-impl-0.1 (crate (name "cly-impl") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "isnt") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "repc-impl") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0zay7clxdarrmkzdi9b3hkvn1xz7f8jq0195jfl6kxmqp5rwskmm")))

