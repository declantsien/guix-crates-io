(define-module (crates-io cl ua) #:use-module (crates-io))

(define-public crate-cluatoi-0.1 (crate (name "cluatoi") (vers "0.1.3") (hash "0sww3k5hyl3vdifps1fsd5jypnqws6b2hdwx16aiv7ik4q8bl644")))

(define-public crate-cluatoi-0.1 (crate (name "cluatoi") (vers "0.1.4") (hash "1kc7vj98f53xl1h29yxl9nzxwd63r1c8gas0pdy9pmz2v0fvk6ps")))

(define-public crate-cluatoi-0.1 (crate (name "cluatoi") (vers "0.1.5") (hash "0i2qxwrrzvnfhp4lpdkdqh8av8z1z6605j8zlrjx16r68lrcxxab")))

(define-public crate-cluatoi-0.1 (crate (name "cluatoi") (vers "0.1.6") (hash "07rvl6lias7r05mx56rmqy26c6g5ljil3nbidaqcmsirzv5kpm8p")))

(define-public crate-cluatoi-0.1 (crate (name "cluatoi") (vers "0.1.7") (hash "05hi1bxc7dbzhn891bchkd9110qz8bfa5bcm5figd4w78r0m261h")))

(define-public crate-cluatoi-0.1 (crate (name "cluatoi") (vers "0.1.8") (hash "0fysm2ss3v9cm1zzi82h6zjqhfi8zd9qxml49lvfhb7r8cxrcqkn")))

(define-public crate-cluatoi-0.1 (crate (name "cluatoi") (vers "0.1.9") (hash "0xaviv91a5sr6gmxjzpp880lg0gnw2vmmaygh2z3n51cj38kx34c")))

(define-public crate-cluatoi-0.2 (crate (name "cluatoi") (vers "0.2.0") (hash "0rs0lzjz9cv6gk1w9rjjfdgsmq435sga4niya23c4cjp471vqaph")))

(define-public crate-cluatoi-0.2 (crate (name "cluatoi") (vers "0.2.1") (hash "1hwmfvq6j1p8z62dk5j49wsaajkilypmda6c6cgi0mq3hirr6hnc")))

