(define-module (crates-io cl me) #:use-module (crates-io))

(define-public crate-clmerge-0.1 (crate (name "clmerge") (vers "0.1.0") (deps (list (crate-dep (name "semver") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0a613snygwr675x2c26hki5hfjhycsf1i5jilb6gj2xvzr0043ym")))

(define-public crate-clmerge-0.1 (crate (name "clmerge") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "1hzr0gcfbx489457q9dyl6a1yd2jydirmyai1qalxbh998c189f1")))

(define-public crate-clmerge-0.1 (crate (name "clmerge") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "0v07k4ryrarckgxh4mjd1gvi0m6mc56cikakfv23jcl9njylpsp3")))

