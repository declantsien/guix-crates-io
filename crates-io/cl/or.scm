(define-module (crates-io cl or) #:use-module (crates-io))

(define-public crate-clorange-0.1 (crate (name "clorange") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "13dasaj519923ihz49dnf694g9pjf33b4x03zmkaqgrm12wvz5pk")))

(define-public crate-clorange-1 (crate (name "clorange") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "1bkqqy59fi2ii00sp29kxc5fbmmihby5j6kcargc8gq160qhd0hw")))

(define-public crate-clorange-1 (crate (name "clorange") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "01ac4inwawyhzqngx5fx9blz7lcmjzvh3jb27qx12sgg87vkjzlp")))

(define-public crate-clorm-0.1 (crate (name "clorm") (vers "0.1.0") (hash "1k2aqs3wr5kfflk4hz77wvv5a63rzzzmjydqbsxalf65vmsl2hk7")))

(define-public crate-clorm-macros-0.1 (crate (name "clorm-macros") (vers "0.1.0") (hash "12z9afzcp7xgvg4rigq0q227r2r6bawkcc40miw8xbvrlc2azd43")))

