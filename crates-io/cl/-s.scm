(define-module (crates-io cl -s) #:use-module (crates-io))

(define-public crate-cl-search-0.1 (crate (name "cl-search") (vers "0.1.0") (hash "181prb8yhw2s30ys7vwp4sbh6cwdgqz6f44xihg9xih7mb1wj907")))

(define-public crate-cl-sys-0.1 (crate (name "cl-sys") (vers "0.1.0") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0iam1a3cwv8ql0hv2fn89vyz3pdfn6myxshpq9wmdn6f36abwpmp")))

(define-public crate-cl-sys-0.2 (crate (name "cl-sys") (vers "0.2.0") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kdnryxs8z25kzw5p2xwa39d2k7m3yn27iwircrsqq6jagr3ngcs")))

(define-public crate-cl-sys-0.2 (crate (name "cl-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ahlgbm430xy22m8dgr8rvnmvj9aya0fnc289lgqq09csvi8fzyy")))

(define-public crate-cl-sys-0.2 (crate (name "cl-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zmjw625wdp8hxb7v31bvjwmrnr6x6mjc3c2hcrvlfyv0r8kf1d0") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2")))) (yanked #t)))

(define-public crate-cl-sys-0.3 (crate (name "cl-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05gsbx6szikmmq7l87plsllk1v7jxwlfd50dm8kmqbldi35zmbdm") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.3 (crate (name "cl-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0galjxbjah5id8vkk8kxp3v6k81d4mvxm4sa7gjgf2vjfl2f088w") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.3 (crate (name "cl-sys") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fx5xigykbwjr97zv5hbfynkqbdzydyc08vab6bgmj4ssf5nw40n") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4 (crate (name "cl-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19am2d96zd9phvay3h1jz1mhz82zp3qyfzc7gg6kq0vhbs4wc15v") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4 (crate (name "cl-sys") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "198bi84biqzkd77smy6llr4q5rpcjwnrgj0bk1c20vyrv877abh5") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4 (crate (name "cl-sys") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i4fwg6351l40xiwmilndcvq4zi7ain2j4z1x14nrkcazyikymz8") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

(define-public crate-cl-sys-0.4 (crate (name "cl-sys") (vers "0.4.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15b57q51zsczqlykyxs06ziwwnzd5cmzgyw0c438qqspm4jdissg") (features (quote (("opencl_version_2_2") ("opencl_version_2_1") ("opencl_version_2_0") ("opencl_version_1_2") ("opencl_version_1_1") ("opencl_vendor_mesa") ("default" "opencl_version_1_1" "opencl_version_1_2"))))))

