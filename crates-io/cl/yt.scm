(define-module (crates-io cl yt) #:use-module (crates-io))

(define-public crate-clytia-0.1 (crate (name "clytia") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0vzlr4jczqsmp704p4i8b0q1i6zjsysmy944v6jmmz1a4paqxb6m")))

(define-public crate-clytia-0.1 (crate (name "clytia") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0ph8mh4djzqbbplhan9rv6idh8nxvp8fw2qrh18z9jphgw5bmbjr")))

(define-public crate-clytia-0.2 (crate (name "clytia") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "19w2bzmyl658hayx11snzz5b9qk0wr7lxhsmx2nz6gckp963xbdv")))

(define-public crate-clytia-0.2 (crate (name "clytia") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1jrjg5748w3494r3pgyf0gvwq4by23ngm145g9y4p991adz1k30i")))

