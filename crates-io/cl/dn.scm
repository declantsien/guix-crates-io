(define-module (crates-io cl dn) #:use-module (crates-io))

(define-public crate-cldnn-0.1 (crate (name "cldnn") (vers "0.1.0") (hash "1nzys6fhp8asz26p784vfkwn9qh0w1xjm1knb0wxmvldhrllz42a")))

(define-public crate-cldnn-sys-0.1 (crate (name "cldnn-sys") (vers "0.1.0") (hash "0cl7mwxf5pw5dvm0r3d67c3idq5m0ginkk8qp2x2dc43bqf99clw")))

