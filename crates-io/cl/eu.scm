(define-module (crates-io cl eu) #:use-module (crates-io))

(define-public crate-cleu-orm-0.1 (crate (name "cleu-orm") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (kind 0)) (crate-dep (name "cleu-orm-derive") (req "^0.1.0") (optional #t) (kind 0)) (crate-dep (name "sqlx-core") (req "^0.5") (features (quote ("postgres" "runtime-tokio-native-tls"))) (kind 0)))) (hash "1wffs0nw5rsh0i3rr4ac0cj8cgw2wzpbkb2z85nnyyyxg5w0dvzq") (features (quote (("derive" "cleu-orm-derive") ("default"))))))

(define-public crate-cleu-orm-derive-0.1 (crate (name "cleu-orm-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (kind 2)))) (hash "1czwc8h1yqy846nzhmn2sb1943a3na7kxs0d9ksp1ckiisk3zp4w") (features (quote (("default"))))))

