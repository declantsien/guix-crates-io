(define-module (crates-io cl uc) #:use-module (crates-io))

(define-public crate-cluck-0.1 (crate (name "cluck") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.72") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.181") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "03wj1crimla9ijyhgph9yfkmqp797fdg30xbxrj5cmgsdc835f0g")))

(define-public crate-cluck-0.2 (crate (name "cluck") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.72") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.181") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1g34x2ldfy2l5dy2jv2n0lhlg0km5xn2v0fbqyj0509p16z0jkzq")))

(define-public crate-cluck-0.3 (crate (name "cluck") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.72") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.181") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1nacdp3a7vax5mc6bghp9z1fd9ffm7lab46siql8p4lb2jp91i64")))

(define-public crate-cluck-0.4 (crate (name "cluck") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.72") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.181") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1ad02wv4afiyylhxyw17w4mqzkc9xks9066kwh97l2wjszw44s36")))

(define-public crate-cluColor-0.1 (crate (name "cluColor") (vers "0.1.0") (hash "1b6lh676vgrakri155zg8qwswllmqj02p32llpz2d7ywv8aa47l4")))

(define-public crate-cluColor-0.1 (crate (name "cluColor") (vers "0.1.1") (hash "00n143cl6rfl7qgkhbzlr0zkc69kkpan5gdx6mzyfjhqnm4clw98")))

(define-public crate-cluColor-0.1 (crate (name "cluColor") (vers "0.1.2") (hash "0ybl4d7vsdh5r38wpglkzmryqrsgfmksfgifba20mk1zs2nb8bpd")))

(define-public crate-cluColor-0.1 (crate (name "cluColor") (vers "0.1.3") (hash "1mgm70r8qi9dhhlm5w8l1vqq8ch5h3c749jspd4kaic6za7h3yci")))

(define-public crate-cluColor-0.1 (crate (name "cluColor") (vers "0.1.4") (hash "089rz6sh8bnxpazmqsg6aza7wvssiijimb51hs2pqk9l55cl4w7d")))

(define-public crate-cluColor-0.1 (crate (name "cluColor") (vers "0.1.5") (hash "08275lrigfz50hzywpdh0ndaj9pj8avfmlxqicqhfk232338bn4x")))

(define-public crate-cluConcatBytes-0.1 (crate (name "cluConcatBytes") (vers "0.1.4") (hash "1jsqfj074c690i6lg6z9l5gzshz4lq03zb0h4lp788f1xmx4gb34")))

(define-public crate-cluConcatBytes-0.1 (crate (name "cluConcatBytes") (vers "0.1.5") (hash "1mkmkjg2nw2crdpcm0ncy0gxjf9dh8ykqr4q4a1y6cv5zm6jm3yk")))

(define-public crate-cluConstConcat-1 (crate (name "cluConstConcat") (vers "1.2.2") (hash "05w3gycfam5cblh1sbzdb3b800g8lmmv5vwwa7g7hp92k523bq2r")))

(define-public crate-cluConstData-1 (crate (name "cluConstData") (vers "1.2.3") (hash "0fv9kdkp4n27pcy9iccd2jhmckszsc46cwgyj01xqb6cdsjbrsv6")))

(define-public crate-cluConstData-1 (crate (name "cluConstData") (vers "1.2.4") (hash "12mfbk48aknl1c29qrb5r1mjj8ghyfhs3sj9kismjrf5wcxirrr4")))

(define-public crate-cluConstData-1 (crate (name "cluConstData") (vers "1.2.6") (deps (list (crate-dep (name "cluFullTransmute") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0096ks9a2ccj8vgqqybg42b9m7jsjjisw8jz7kn6m3h0b1ynx0gd") (yanked #t)))

(define-public crate-cluConstData-1 (crate (name "cluConstData") (vers "1.2.7") (deps (list (crate-dep (name "cluFullTransmute") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0br9nyyx0rrpckbm4q7p0pq5wmksxw5cyjgrl9w8z6xh379xxpc7")))

(define-public crate-cluConstData-1 (crate (name "cluConstData") (vers "1.2.8") (deps (list (crate-dep (name "cluFullTransmute") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "00rijw4mqig3cdp005pr59090w129m0hgw9dnxpp05xj9rjf4inx")))

(define-public crate-cluConstData-1 (crate (name "cluConstData") (vers "1.2.9") (deps (list (crate-dep (name "cluFullTransmute") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0vim6lw64b9ss1rjbv3pf5nmmqrkzikrs1gp6vifkz8isr6zf7kk")))

(define-public crate-cluConstData-1 (crate (name "cluConstData") (vers "1.3.0") (deps (list (crate-dep (name "cluFullTransmute") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0kdag2j1sq9fs5j4kf165xpf5xxg32vljqgz9wqgb2xjzpwg6sbj")))

(define-public crate-clucstr-0.1 (crate (name "clucstr") (vers "0.1.0") (hash "1g7gmdpz9hlf2lggk1ljb5lf6dckl5jmj74jjpwxyi3awrk9khlm")))

(define-public crate-clucstr-0.1 (crate (name "clucstr") (vers "0.1.1") (hash "0lapcvdfsxargc92d0ps6hfqrpz8gylbkagkzq0vp1gw32lfj5cc")))

(define-public crate-clucstr-0.1 (crate (name "clucstr") (vers "0.1.2") (hash "1j57m23rmxanm4r9rx9ds3f31b0blb89i1vajn54kkigsf40avda")))

(define-public crate-clucstr-0.1 (crate (name "clucstr") (vers "0.1.3") (hash "0l9xwlkmvj36z8lp11mbx86i1ya7x5xzbyrdl20l3zzf0q0dgb89")))

(define-public crate-clucstr-0.1 (crate (name "clucstr") (vers "0.1.4") (hash "1wyk4ax1fxrlf85r4wd9m54z4bcxb11wld3bp8glkypgxsiabw65")))

(define-public crate-clucstr-0.1 (crate (name "clucstr") (vers "0.1.6") (hash "0r8is2id9spfn5nnc0zygkfqx2ribk16491dq40kvppn89ip3djs")))

(define-public crate-clucstr-0.1 (crate (name "clucstr") (vers "0.1.7") (hash "1h74pqi6d6icb28k6avk398nzpbvpmmagq1nnndrh1vimyfpl9gv")))

(define-public crate-clucstr-1 (crate (name "clucstr") (vers "1.1.9") (hash "0hrz7fjjaj8jwnp6znnjbljls24c734mrgwhc1jqcmfvhbv2q8sn")))

(define-public crate-clucstr-1 (crate (name "clucstr") (vers "1.1.91") (hash "08bzn5pmhz4c82sazmc8j3cyp5hcdygna5a30y679px506vibzyr")))

(define-public crate-clucstr-1 (crate (name "clucstr") (vers "1.2.0") (deps (list (crate-dep (name "memchr") (req "^2.7.2") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)))) (hash "0d3z0qfapvhf5vvlnwrf16m009xnz7y7ah93c6n5nxhdqvw62gab")))

