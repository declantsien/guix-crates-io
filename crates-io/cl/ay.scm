(define-module (crates-io cl ay) #:use-module (crates-io))

(define-public crate-clay-0.0.1 (crate (name "clay") (vers "0.0.1") (hash "00gsg17p2sa8abs7lc912z28l86hw6rcvkhr1d3j06q3vk5y7g7y")))

(define-public crate-clay-0.1 (crate (name "clay") (vers "0.1.1") (deps (list (crate-dep (name "clay-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clay-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "clay-viewer") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "ocl-include") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "0z5fvqg99brbb079hb5mhm7paqmb405rhbn0njr270j5pfi08xgk")))

(define-public crate-clay-0.1 (crate (name "clay") (vers "0.1.2") (deps (list (crate-dep (name "clay-core") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "clay-utils") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "clay-viewer") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "ocl-include") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "0axln1gm5l3265anp3wz18z82zg64wiqsc6jdj6lndzrc7qk579b")))

(define-public crate-clay-core-0.0.1 (crate (name "clay-core") (vers "0.0.1") (hash "1752f0jw945n1gb74vrhavl82l7vq882x1vvgjf3h0f9wn1rb30m")))

(define-public crate-clay-core-0.1 (crate (name "clay-core") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "ocl-include") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "0lx3h1zz8an2cvc3lri0wiyp08kgwqb34xvazdznynj25v6dmkpg")))

(define-public crate-clay-core-0.1 (crate (name "clay-core") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "ocl-include") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "1p67w9gxqp594lr8j4wh0y2mlkpf72vpspzmqcwdd49969xd61f6")))

(define-public crate-clay-core-0.1 (crate (name "clay-core") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "ocl-include") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "0ckbwbwj83lm9s63sz7khqib570sj6cfmhgwjv232hikr3cd6zgr")))

(define-public crate-clay-core-0.1 (crate (name "clay-core") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "ocl-include") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 1)))) (hash "1blfbidccxmj076r88afqpz1daqkc0afr18h1w9928ahza2ayd9k")))

(define-public crate-clay-hyper-0.0.1 (crate (name "clay-hyper") (vers "0.0.1") (hash "08d3cb94r51dxriypdgnbh1f0bbwxcldhgi3i8ag6yjx6a1n2iby")))

(define-public crate-clay-utils-0.1 (crate (name "clay-utils") (vers "0.1.0") (deps (list (crate-dep (name "clay-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)))) (hash "16f3pm745k0qv8cfg0sblpwgm2vrawjwbbfijxyrb497qp3l9ybg")))

(define-public crate-clay-utils-0.1 (crate (name "clay-utils") (vers "0.1.1") (deps (list (crate-dep (name "clay-core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ocl") (req "^0.19") (default-features #t) (kind 0)))) (hash "0yrz7l39lxcf44l515lfv015byfwhamk32d22hdpysmpi1g1f8a1")))

(define-public crate-clay-viewer-0.1 (crate (name "clay-viewer") (vers "0.1.1") (deps (list (crate-dep (name "clay-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rental") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.32") (default-features #t) (kind 0)))) (hash "0qajsahdp9f7sjjhcfmz5897z19yq8asna66i0ayyrhnims05qyd")))

(define-public crate-clay-viewer-0.1 (crate (name "clay-viewer") (vers "0.1.2") (deps (list (crate-dep (name "clay-core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "clay-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "rental") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.32") (default-features #t) (kind 0)))) (hash "0m8mccviw62isqj8qc88xryx2f4bd7az5dcvaq0hd47lgga1gfj7")))

(define-public crate-claym-0.2 (crate (name "claym") (vers "0.2.0") (hash "14hm8gv724xk9xc9v4x99h1z9p166zik1m894zz86s723hmrxabi") (rust-version "1.56")))

(define-public crate-claym-0.3 (crate (name "claym") (vers "0.3.0") (hash "1zz5cjyjf2xcgddn2s26rvi4aa7mrrpcy07g0i4xdc3h3i3jps94") (rust-version "1.56")))

(define-public crate-claym-0.4 (crate (name "claym") (vers "0.4.0") (hash "0hmazvws9r9mjgxm9k155mdyxnwzfjh62n8jfcss6fqkhq143cm2") (rust-version "1.56")))

(define-public crate-claym-0.5 (crate (name "claym") (vers "0.5.0") (hash "0w96ixv21mv6m8pmmf2apf162kh8dqwslvy7ihfkb22c94d8ds9f") (rust-version "1.56")))

(define-public crate-claym-0.5 (crate (name "claym") (vers "0.5.1") (hash "0hlqgxx5n927bb8r2wqlwvk752k0fblhxxawj18aya3znddfml2p") (rust-version "1.56")))

(define-public crate-claym-0.6 (crate (name "claym") (vers "0.6.0") (hash "0x81cbdqkiwf1a16w15dbx8mssamnfmi5i24svmsjs7c38qdq619") (rust-version "1.56")))

(define-public crate-claymore-0.0.1 (crate (name "claymore") (vers "0.0.1") (hash "0qiwsrkgsabqzz8mavha0x4x15lj9kd6zgk8gz35h71qxjp3ch80") (yanked #t)))

(define-public crate-claymore-0.1 (crate (name "claymore") (vers "0.1.1") (hash "1x8hmbmkjm4zahr8xnjs1gz4w803z0liyx8ldfqf6swy7ndzxa76") (yanked #t)))

(define-public crate-claymore-0.1 (crate (name "claymore") (vers "0.1.3") (hash "05shzkjfqi57qgcmwnlhg2p8gdrani5cd0hyrwam56z7nzd80ag7") (yanked #t)))

(define-public crate-claymore-0.1 (crate (name "claymore") (vers "0.1.4") (hash "0jzb4dpxphp1jn34ms7ig8kblcc3cgy07knw12qn10prrr9f82cg") (yanked #t)))

(define-public crate-claymore-0.1 (crate (name "claymore") (vers "0.1.77") (hash "1gpw5yw9xj1sj0s16fh3xbvxzniyfnr9dllc4f7bks4nlpmzqs8y") (yanked #t)))

(define-public crate-claymore-0.77 (crate (name "claymore") (vers "0.77.0") (hash "1hll297xdvkfc4crp2rxd7148bbxsb8zmsrjd868y2y332n355py") (yanked #t)))

(define-public crate-claymore-7 (crate (name "claymore") (vers "7.7.18") (hash "1fyrdpg5ybb79a2xwkfrdqm8m9h7bccydrink5ghdp982dz1i0c3") (yanked #t)))

(define-public crate-claymore-2018 (crate (name "claymore") (vers "2018.7.7") (hash "1kb0agx5vdx8sc7kqjpjbjqzh18p55gyaf67wmiwyzyvhc9vb1zm") (yanked #t)))

(define-public crate-claymore-2019 (crate (name "claymore") (vers "2019.12.13") (hash "0jg2i67j0n2c3g4nppcyymdhv3b7fq58n4070a8hlhb79pb0b1rq") (yanked #t)))

(define-public crate-claymore-9999 (crate (name "claymore") (vers "9999.999.99") (hash "0ynihncyric0krp7aimkan2hpkvsqwl57rf73c295cyc0w0bfqb6") (yanked #t)))

(define-public crate-claymore-9 (crate (name "claymore") (vers "9.9.9") (hash "02ndma727fsdy11hdga4kcj0z7zhmk9jqbsy6samf4qfw9kxp4hs") (yanked #t)))

(define-public crate-claymore-99999 (crate (name "claymore") (vers "99999.99999.99999") (hash "1cjg36rshqj7m0w4gypsd1cyrkcm7k6gvivjc0qmgfykwjckmx7m") (yanked #t)))

(define-public crate-claymore-9999999 (crate (name "claymore") (vers "9999999.9999999.9999999") (hash "1lngvrf8iwvgwng7mklxcsl4dybrn3hjh5924b99bsi3x4gzw49x") (yanked #t)))

(define-public crate-claymore-999999999 (crate (name "claymore") (vers "999999999.999999999.999999999") (hash "14rbpx1xl159ilr9a1b4ycb0mhcgr5p26fqhny5ngx3nx8h0w4p5") (yanked #t)))

(define-public crate-claymore-999999999 (crate (name "claymore") (vers "999999999.999999999.9999999991") (hash "0sh5gnks9lig4c55swqgpqyqc91xwfdn97sf7403hvwdnsd32q9b")))

(define-public crate-claytip-0.1 (crate (name "claytip") (vers "0.1.0") (hash "0cb2n31mvqnxhrdwn7rcp80qxb5bacsdinb6ca8xcx0cjrbij3rs")))

