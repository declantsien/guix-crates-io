(define-module (crates-io cl ul) #:use-module (crates-io))

(define-public crate-cluLamansh-0.1 (crate (name "cluLamansh") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0sw2c5mn4bzm7hmn6iaan7g98rnknr790l10gkpxb7x6sbk3has6")))

(define-public crate-cluLamansh-0.1 (crate (name "cluLamansh") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "1psn4ia2mf89g7d758m92rrbcccplhw3rk2d3r3k1zhwfsxcf5cm")))

