(define-module (crates-io cl d2) #:use-module (crates-io))

(define-public crate-cld2-0.0.0 (crate (name "cld2") (vers "0.0.0") (hash "13h1qb5p3z2ycvxlhjh837hjzvj1jqlhcaaxn4d5pkywv0w62qrs") (yanked #t)))

(define-public crate-cld2-0.0.2 (crate (name "cld2") (vers "0.0.2") (deps (list (crate-dep (name "cld2-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1c5db692gs78mkrxzyqyp1x7b3jg7kjax4d9j54y5ba0q3w0930h")))

(define-public crate-cld2-0.0.3 (crate (name "cld2") (vers "0.0.3") (deps (list (crate-dep (name "cld2-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0ajymh13y9wm0kdnp915dwwrj4y75ds5lv6i543wvid7cbprjpb9")))

(define-public crate-cld2-0.0.4 (crate (name "cld2") (vers "0.0.4") (deps (list (crate-dep (name "cld2-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0gi0d1fl55gvpz2006k28wm3nxcxmz3kmrdk0n1rqhyqvzlkvkh6")))

(define-public crate-cld2-0.0.5 (crate (name "cld2") (vers "0.0.5") (deps (list (crate-dep (name "cld2-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "15j2bgrbbn7npx8jdj4x7b92vravlr9xnjla6yjshbhpd6nq7mdw")))

(define-public crate-cld2-0.0.6 (crate (name "cld2") (vers "0.0.6") (deps (list (crate-dep (name "cld2-sys") (req "*") (default-features #t) (kind 0)))) (hash "10pz00ys4isxsm6win5s69mbggs7dvgg1y971yq4gxyri44mjw4g")))

(define-public crate-cld2-0.0.9 (crate (name "cld2") (vers "0.0.9") (deps (list (crate-dep (name "cld2-sys") (req "*") (default-features #t) (kind 0)))) (hash "0pvxmxfyr3rwmcnpl866gs9s2li28fdl63grhh0hvilpgpsnc85w")))

(define-public crate-cld2-0.1 (crate (name "cld2") (vers "0.1.0") (deps (list (crate-dep (name "cld2-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0fij8k09xpa9vjlpwf9alnlcs2w5q75arbvrmjwnyirxkyhcw1jq") (features (quote (("unstable"))))))

(define-public crate-cld2-1 (crate (name "cld2") (vers "1.0.0") (deps (list (crate-dep (name "cld2-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0iradl74ynxilg1dnx990r1zdq0s7kyl5mg9zyk4b901f4wls1hn") (features (quote (("unstable"))))))

(define-public crate-cld2-1 (crate (name "cld2") (vers "1.0.1") (deps (list (crate-dep (name "cld2-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1rbyaqk80ipp454r38b2bnlg1zrlknr4shffrkcm74mpzhyp082q") (features (quote (("unstable"))))))

(define-public crate-cld2-1 (crate (name "cld2") (vers "1.0.2") (deps (list (crate-dep (name "cld2-sys") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1vazwn4fyfvrjic8ngvkbvz7m8yab0fqxvmjg609j0ak5di8p4c0") (features (quote (("unstable"))))))

(define-public crate-cld2-sys-0.0.0 (crate (name "cld2-sys") (vers "0.0.0") (hash "1zsmlv46pfi9ayz4qshlwjh1kmbzvpp0vcdkqv46jbijpryf7w3b") (yanked #t)))

(define-public crate-cld2-sys-0.0.2 (crate (name "cld2-sys") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "~0.0.1") (default-features #t) (kind 0)))) (hash "1wdqnf9p92wl9rpjy2y9hdcp0x9pf0ibv648hmh01br929y8yi7c")))

(define-public crate-cld2-sys-0.0.3 (crate (name "cld2-sys") (vers "0.0.3") (deps (list (crate-dep (name "gcc") (req "~0.0.1") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "~0.1.2") (default-features #t) (kind 1)))) (hash "1fsjj28lfd3wkghaccz5l1dvlxrg9szqf542zyq209i0g38c5cax")))

(define-public crate-cld2-sys-0.0.5 (crate (name "cld2-sys") (vers "0.0.5") (deps (list (crate-dep (name "gcc") (req "~0.0.1") (default-features #t) (kind 1)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "~0.1.2") (default-features #t) (kind 1)))) (hash "0yd9gavi4a8gqxncplj7xr8d761dpwi1hlkyjhjw52vhnxjpzg7c")))

(define-public crate-cld2-sys-0.0.6 (crate (name "cld2-sys") (vers "0.0.6") (deps (list (crate-dep (name "gcc") (req "*") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 1)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "*") (default-features #t) (kind 1)))) (hash "175pib16z111kl8qn4c826mki0ih6w93lpmszh7x6zcwvbambr0b")))

(define-public crate-cld2-sys-0.0.7 (crate (name "cld2-sys") (vers "0.0.7") (deps (list (crate-dep (name "gcc") (req "*") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 1)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "*") (default-features #t) (kind 1)))) (hash "0x89wn40symx87q7zqkpqlqv85ak6hx0yca3jl3zaskbgknvm368")))

(define-public crate-cld2-sys-0.0.9 (crate (name "cld2-sys") (vers "0.0.9") (deps (list (crate-dep (name "gcc") (req "*") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 1)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "*") (default-features #t) (kind 1)))) (hash "1339qj1gm3sxm3dwngpk13z5il8sz5kzjspqqnlf1s6fzsxrcs53")))

(define-public crate-cld2-sys-0.1 (crate (name "cld2-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.19") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.1.23") (default-features #t) (kind 1)))) (hash "0qqcli36jg0qh770hn8zl0809kg31hy8l8asdi1hc8yall2py30n")))

(define-public crate-cld2-sys-1 (crate (name "cld2-sys") (vers "1.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.1.23") (default-features #t) (kind 1)))) (hash "1lly8ddl2a0vgjq9sjl4vf9ygps7bh6qvkdr8mhqg0k37hkmh4ws")))

(define-public crate-cld2-sys-1 (crate (name "cld2-sys") (vers "1.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.1.23") (default-features #t) (kind 1)))) (hash "0f8k6ns77hsx11yzjfvykyhdyj5nnpq3vi3x8yzqp464qhliq201")))

(define-public crate-cld2-sys-1 (crate (name "cld2-sys") (vers "1.0.2") (deps (list (crate-dep (name "gcc") (req "^0.3.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.1.23") (default-features #t) (kind 1)))) (hash "13w01dy4ccz219hdqrbiisbjhkqisxmnbsddfiw2ii524pj8bfxz")))

