(define-module (crates-io cl -g) #:use-module (crates-io))

(define-public crate-cl-generic-read-buf-0.1 (crate (name "cl-generic-read-buf") (vers "0.1.0") (deps (list (crate-dep (name "cl-generic-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0djhsg3n0gqwadfh1kyfwli91ppncsqwaba59cc34gm9wcwx8251")))

(define-public crate-cl-generic-read-buf-0.1 (crate (name "cl-generic-read-buf") (vers "0.1.1") (deps (list (crate-dep (name "cl-generic-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0j4mvy2dvsp41lv2qyv4sqgyawdgchrcbpi5n4i3l187qpvndjaz")))

(define-public crate-cl-generic-read-buf-0.1 (crate (name "cl-generic-read-buf") (vers "0.1.2") (deps (list (crate-dep (name "cl-generic-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "11h5cq345hl9fpxwab3cny956pkwwb5j09y626x4nq2m8n5nchqq")))

(define-public crate-cl-generic-read-buf-0.1 (crate (name "cl-generic-read-buf") (vers "0.1.3") (deps (list (crate-dep (name "cl-generic-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1zarjnsvm68yjnn7z8l35hc6qnjx8xmraschpz2d641a2ajflfvy")))

(define-public crate-cl-generic-vec-0.1 (crate (name "cl-generic-vec") (vers "0.1.4") (deps (list (crate-dep (name "mockalloc") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "06v7vyi3vrjs4a8grj9ib3pa8g381xf1298s07l6cbnswch1b2jq") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.2 (crate (name "cl-generic-vec") (vers "0.2.0") (deps (list (crate-dep (name "mockalloc") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "02d023vw99yrzm3pq9k6bg14kqxrlvfrzlcq7i4zwwjg2vsxfcvy") (features (quote (("std" "alloc") ("nightly") ("default" "std" "nightly") ("alloc"))))))

(define-public crate-cl-generic-vec-0.2 (crate (name "cl-generic-vec") (vers "0.2.1") (deps (list (crate-dep (name "mockalloc") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "022gq1v71ffbxzfz2sl3j5bv9q6zk1i45z2qkp8gz102b2jzc168") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3 (crate (name "cl-generic-vec") (vers "0.3.0") (deps (list (crate-dep (name "mockalloc") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1265fh58xg1lqpa3x7q5zbbc9rqnkq0s7bic6wp0mcfz1n9g1076") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3 (crate (name "cl-generic-vec") (vers "0.3.1") (deps (list (crate-dep (name "mockalloc") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "06saxq2gfy5rwl1v638i3z7x2ar0q4ffyn6c0dd2vjqsvj6crhwg") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3 (crate (name "cl-generic-vec") (vers "0.3.2") (deps (list (crate-dep (name "mockalloc") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1n463h4igl46yj88v9zbq006qlr16wrl1h48pffdhc0n3cmz4h86") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3 (crate (name "cl-generic-vec") (vers "0.3.3") (deps (list (crate-dep (name "mockalloc") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1ff7v4q67d3vqi4209nvchd3wxflxf719w75cai1pljw931bm2wj") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.3 (crate (name "cl-generic-vec") (vers "0.3.4") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "15rf69xy30s0pwfjinvcfiik7lzpgza2ym33579ya0gdrlym7vas") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.4 (crate (name "cl-generic-vec") (vers "0.4.0") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1rxhgrczzn6jr185pyhava6yrjgg7g58hnxgw4hx1jp350jmwz9j") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-cl-generic-vec-0.1 (crate (name "cl-generic-vec") (vers "0.1.0") (deps (list (crate-dep (name "cl-generic-vec") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "16bm1vika820fzz1n10jhv9x22kxwb3p649b9yk10f13v5vsbgss") (yanked #t)))

