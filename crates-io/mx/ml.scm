(define-module (crates-io mx ml) #:use-module (crates-io))

(define-public crate-mxml-0.0.1 (crate (name "mxml") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1m4lzkifybd3i1vcgn39x62v3hiz8dc7cklswhwnndwb7m8gar2q")))

(define-public crate-mxml-0.0.2 (crate (name "mxml") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00qkw2nkhm457pal8030cnrqgks2p2vn59v9clq8799ya319jzcv")))

(define-public crate-mxml-0.1 (crate (name "mxml") (vers "0.1.0") (deps (list (crate-dep (name "mxml_dep") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16l2dz9sz4maaqs11nf1z56g8ra43ic9qc0mqq4a0lv0cq2pvdqr") (yanked #t)))

(define-public crate-mxml-0.1 (crate (name "mxml") (vers "0.1.1") (deps (list (crate-dep (name "mxml_dep") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b64jwjn9vyi4n63sh3sr0lkl2hrs7pvva65s7pgwcwb9r8v57ns") (yanked #t)))

(define-public crate-mxml-0.1 (crate (name "mxml") (vers "0.1.2") (deps (list (crate-dep (name "mxml_dep") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lkn3gpjd4v42v6w7bbfc0wixns4kxrmgx5l6cr1h7r8ilhzygbb") (yanked #t)))

(define-public crate-mxml_dep-0.0.1 (crate (name "mxml_dep") (vers "0.0.1") (hash "0a5l8lbg9sgrdfikzncqk5ybi9v5za4gp5himv0r8540r9fsjvw0") (yanked #t)))

(define-public crate-mxml_dep-0.1 (crate (name "mxml_dep") (vers "0.1.0") (deps (list (crate-dep (name "html_parser") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "060wgwal8nxk452w50jz8lba8fsagvrkjchxc0dr7k99mcv5vk7p") (yanked #t)))

(define-public crate-mxml_dep-0.1 (crate (name "mxml_dep") (vers "0.1.1") (deps (list (crate-dep (name "html_parser") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0r0pm4md6spp8l1whzj8b28b8r2cmi33p1gfc1nalrazrgimny4g") (yanked #t)))

(define-public crate-mxml_dep-0.1 (crate (name "mxml_dep") (vers "0.1.2") (deps (list (crate-dep (name "html_parser") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1paj2dj3xvxsf1s21297bax1iv8wsf87wpjpgfrzbj19jqix1ggr") (yanked #t)))

(define-public crate-mxml_dep-0.1 (crate (name "mxml_dep") (vers "0.1.3") (deps (list (crate-dep (name "html_parser") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1daghy3hi8ymnyfw5zs02i0xa1mxzis31nk2kgy543z0xa7z67d1") (yanked #t)))

