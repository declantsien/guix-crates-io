(define-module (crates-io mx to) #:use-module (crates-io))

(define-public crate-mxtool-0.0.1 (crate (name "mxtool") (vers "0.0.1") (hash "187wmymqdpwplxdg6rg7jiid3rdpxv9q9hxpq4myy9w44qhil5m3")))

(define-public crate-mxtool-0.0.2 (crate (name "mxtool") (vers "0.0.2") (hash "0b6vf0n5lcl1b07w5p8jm8r9yvha5n142vca6nbmlgamqjqskzay")))

