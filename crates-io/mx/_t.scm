(define-module (crates-io mx _t) #:use-module (crates-io))

(define-public crate-mx_tool-0.0.2 (crate (name "mx_tool") (vers "0.0.2") (hash "0l9ncf24rmgn4fq1ygshcj6lk1svmhc0pxiz758vjrzry7dyqhq5")))

(define-public crate-mx_tool-0.0.3 (crate (name "mx_tool") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "16zi4lgmlq6gc9r22f452rpmbl6iqsdxv9rbwg97h3iy61ky2q67")))

(define-public crate-mx_tool-0.0.4 (crate (name "mx_tool") (vers "0.0.4") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "144gvhv1dy52ffy0pzqzqfcchl1bq1mqncwapjsrpnkkviddcxqm")))

(define-public crate-mx_tool-0.0.5 (crate (name "mx_tool") (vers "0.0.5") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "0d0yyvg8cfdvizf2ma46pac7rrq5lpyaw2kwd98nlfg14x3jf0g5")))

(define-public crate-mx_tool-0.0.6 (crate (name "mx_tool") (vers "0.0.6") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "13108cvixkkypbmnhqn1w2iay9w2b2lk3i807lbsbsm1zax2d0im")))

