(define-module (crates-io mx ru) #:use-module (crates-io))

(define-public crate-mxruntime-0.1 (crate (name "mxruntime") (vers "0.1.0") (deps (list (crate-dep (name "magenta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "magenta-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mxruntime-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "16h8jbkkd2dmshpli1vjvbdkjwz7y0ha0d182xazpkl535bglryj")))

(define-public crate-mxruntime-sys-0.1 (crate (name "mxruntime-sys") (vers "0.1.0") (deps (list (crate-dep (name "magenta-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1f4y1l7fql6jh4jca28iwyq55xa9r6yw7kqm4njf634m2gy2lgsv")))

