(define-module (crates-io mx o_) #:use-module (crates-io))

(define-public crate-mxo_env_logger-0.1 (crate (name "mxo_env_logger") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0xc432g49r2f6k8yggw8gh5r7kwxkiszy6vvgl33j8gz1b9f91rd")))

