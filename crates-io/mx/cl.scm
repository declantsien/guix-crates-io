(define-module (crates-io mx cl) #:use-module (crates-io))

(define-public crate-mxclear-0.1 (crate (name "mxclear") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.2") (features (quote ("env"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ckgz5lrnhgxnilspj69bcs8drc6lzb4ac35m00db2ldr6nnz302")))

