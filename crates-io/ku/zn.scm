(define-module (crates-io ku zn) #:use-module (crates-io))

(define-public crate-kuznechik-0.1 (crate (name "kuznechik") (vers "0.1.0") (deps (list (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "00dfg5hcwcl47k37m17lkq14ifj62y5c4ikvj6ibq8wh8k2w61r3")))

(define-public crate-kuznechik-0.1 (crate (name "kuznechik") (vers "0.1.1") (deps (list (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0fgqiqd1wmpiah5986p27mb92q7p1zyjiz644m4bnjxg1rvl1sm8")))

(define-public crate-kuznechik-0.2 (crate (name "kuznechik") (vers "0.2.0") (deps (list (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0a5rj1hgczbgjyzlnvlfmx90gvnjy2rkzkcswq6qipbkclpl8w1k")))

(define-public crate-kuznechik-0.3 (crate (name "kuznechik") (vers "0.3.0") (deps (list (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1w1yjn54f84cw5rbi2ndcgk35jh3hczlpx9b96d0kwd8k4wdcm8w")))

(define-public crate-kuznechik-0.4 (crate (name "kuznechik") (vers "0.4.0") (deps (list (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0lfpp6b9w3g00g6qvwc8drkmk006n4w473cbh7pc3s9snckwnkwk")))

(define-public crate-kuznyechik-0.0.0 (crate (name "kuznyechik") (vers "0.0.0") (hash "1jjxshg7fkp2nrshmx1jd08sbghmh6wh5nikkyzb74i281fg8wqk")))

(define-public crate-kuznyechik-0.1 (crate (name "kuznyechik") (vers "0.1.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "crypto-tests") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.6") (default-features #t) (kind 0)))) (hash "0a2fxgs71ybx2vrg9pyrxf9r2wz3mrhg1fv1m6ph0cqhbcrfjs51")))

(define-public crate-kuznyechik-0.1 (crate (name "kuznyechik") (vers "0.1.1") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "crypto-tests") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.6") (default-features #t) (kind 0)))) (hash "02j79v9rf213f0lg8zn3w2yy00x4rhplv0w7jlnvxzc7772i24av")))

(define-public crate-kuznyechik-0.2 (crate (name "kuznyechik") (vers "0.2.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "opaque-debug") (req "^0.1") (default-features #t) (kind 0)))) (hash "174id4w7bvhfgk0g5aixvrw4vql8rblr66sc9n7szvd03xnzqi9x")))

(define-public crate-kuznyechik-0.3 (crate (name "kuznyechik") (vers "0.3.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "block-cipher-trait") (req "^0.6") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wqjxab9qzrbphkmfjbygmr01ap93ff2r3r3mlkjx8zrpzdkgyvg")))

(define-public crate-kuznyechik-0.4 (crate (name "kuznyechik") (vers "0.4.0") (deps (list (crate-dep (name "block-cipher") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.7") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zbyk1pbnp2shcxyjaphpi5b2iqmn6gawvv663xh8913adgn500y")))

(define-public crate-kuznyechik-0.4 (crate (name "kuznyechik") (vers "0.4.1") (deps (list (crate-dep (name "block-cipher") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.7") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "0frldhp4sgyxry1kgrwak0pgghiw619if9cnh8k65p637sb25zxx") (features (quote (("no_unroll"))))))

(define-public crate-kuznyechik-0.5 (crate (name "kuznyechik") (vers "0.5.0") (deps (list (crate-dep (name "block-cipher") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "block-cipher") (req "^0.8") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bbaxwa2h0gm2z89s7ckgd1sgg6jp69v5i6qvaq9xx6pw1gbn51v") (features (quote (("no_unroll"))))))

(define-public crate-kuznyechik-0.6 (crate (name "kuznyechik") (vers "0.6.0") (deps (list (crate-dep (name "cipher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.2") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "0phi8n03bkdhyp0cmdabzhv28ib3pc2zdsbvy40hiabpvzp3j492") (features (quote (("no_unroll"))))))

(define-public crate-kuznyechik-0.7 (crate (name "kuznyechik") (vers "0.7.0") (deps (list (crate-dep (name "cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "opaque-debug") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c1hzwm09zk0kj8kih40azcqaxbi4bq9l9wb8ypkljzx951xcr88") (features (quote (("no_unroll"))))))

(define-public crate-kuznyechik-0.7 (crate (name "kuznyechik") (vers "0.7.1") (deps (list (crate-dep (name "cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)))) (hash "1i9jswpiyl9q19icblp54wlgjrkkxka2dgr00ml36h3b69za680h") (features (quote (("no_unroll") ("force-soft"))))))

(define-public crate-kuznyechik-0.7 (crate (name "kuznyechik") (vers "0.7.2") (deps (list (crate-dep (name "cipher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.3") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0zij18ngpqvc88pwrlxc1clsakfzv3nbj4bi976wvxvc622hlkks") (features (quote (("no_unroll") ("force-soft"))))))

(define-public crate-kuznyechik-0.8 (crate (name "kuznyechik") (vers "0.8.0") (deps (list (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0rbzlc73mvnh3znbyqijbhkfi9k2lp91z622wm8y1gmysmhagf66") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-kuznyechik-0.8 (crate (name "kuznyechik") (vers "0.8.1") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "0av39qh65xchvpfjkcwh861h9bzmmrgcrzl5h0sa5b692xabd0w4") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-kuznyechik-0.8 (crate (name "kuznyechik") (vers "0.8.2") (deps (list (crate-dep (name "cipher") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.2") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "0by12awlby61ihp097gz193h8any0dkq5z46svg6130r4jjrjy6a") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-kuznyechik-0.9 (crate (name "kuznyechik") (vers "0.9.0-pre.0") (deps (list (crate-dep (name "cipher") (req "=0.5.0-pre.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "=0.5.0-pre.4") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "02bql4g26wvpvalpg1iab65rbzyjssy9bkhcl23z8fcw6cyl6md1") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.65")))

