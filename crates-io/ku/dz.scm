(define-module (crates-io ku dz) #:use-module (crates-io))

(define-public crate-kudzu-0.1 (crate (name "kudzu") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1j2h0y8hbj22g78pv46fja774rdmrnq475vlmd0k124nnchmvlb0")))

