(define-module (crates-io ku du) #:use-module (crates-io))

(define-public crate-kudubot-bindings-0.1 (crate (name "kudubot-bindings") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0iqq3bxq0xdn6jxh5b42brd6dqaa41fa90vi7lxsl26hc9zv8mqy")))

(define-public crate-kudubot-bindings-0.1 (crate (name "kudubot-bindings") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d0p6vkwkrr4bssnbyz9vfdj6vdmb3pyz652ls94vm5qxk1sv7y3")))

(define-public crate-kudubot-bindings-0.14 (crate (name "kudubot-bindings") (vers "0.14.3") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "114kvcyf7rnl6plb8fc8qyq9vd55yp8b546ly32rnd0pdflb4vhp")))

(define-public crate-kudubot-bindings-0.15 (crate (name "kudubot-bindings") (vers "0.15.0") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1njqagalbmqhm5a2cq8k2qgqwayl33m6nh7bf76yv1vf9iikz4a4")))

(define-public crate-kudubot-bindings-0.16 (crate (name "kudubot-bindings") (vers "0.16.0") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hg7wbdk4hwbpzmizr79jb73jgj2gblf8ln0bsbwk8fji3n38b7y")))

(define-public crate-kudubot-bindings-0.16 (crate (name "kudubot-bindings") (vers "0.16.1") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z8g2z9in835d8pvqia8gzi4yfs5zg81wwxv2c1c60xm3f9wp93w")))

(define-public crate-kudubot-bindings-0.18 (crate (name "kudubot-bindings") (vers "0.18.2") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qiz6p1yc0f033ngndlj5cyfilj6qg43qwsgil5wbncimb10h73p")))

