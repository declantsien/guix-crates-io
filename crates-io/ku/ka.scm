(define-module (crates-io ku ka) #:use-module (crates-io))

(define-public crate-kuka-0.1 (crate (name "kuka") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.194") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mm22yhhb3km41sk76gx9k2wbpfhi1c86y3i6iyaaayv688z1gx0")))

(define-public crate-kukan-0.1 (crate (name "kukan") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1ps763ipli1wipykg8id01d7iinzr8lrv2qa98ngj375vl9j1amm")))

(define-public crate-kukan-0.1 (crate (name "kukan") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "10x720g6jmyxw6zv6k6ml7y9b9fp7lzczzjvm6kqlwx6i72ad9z0")))

(define-public crate-kukan-0.1 (crate (name "kukan") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0jnprvmizafzgmh2azsvy5k1nxvxm5npk82q4sfyh5wpyz4v78cc")))

