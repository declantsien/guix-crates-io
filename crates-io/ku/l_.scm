(define-module (crates-io ku l_) #:use-module (crates-io))

(define-public crate-kul_core-0.1 (crate (name "kul_core") (vers "0.1.0") (hash "1civdghddkh001m60gdpshi8g92gf53azm91gdlm1nbm12r02awm")))

(define-public crate-kul_core-0.1 (crate (name "kul_core") (vers "0.1.1") (hash "07jpvczhxhgxdgf99jr4dbx64b5byqh3s61k67ad0f7srchp2cwd")))

(define-public crate-kul_core-0.1 (crate (name "kul_core") (vers "0.1.2") (hash "00xzz8hj9rrlffzp7p5yqsgm6dkw0jqb32j2yw9avwljj7mkqblm")))

