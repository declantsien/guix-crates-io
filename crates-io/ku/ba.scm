(define-module (crates-io ku ba) #:use-module (crates-io))

(define-public crate-kuba-0.1 (crate (name "kuba") (vers "0.1.0") (hash "1q2yk43j5bard4lhqwdr75rf9vw2snqp76a0vh5w0p7cps0dgia9")))

(define-public crate-kuba-0.1 (crate (name "kuba") (vers "0.1.1") (hash "1i5lzsd0sz7r4ncpzwdh51jxcppg24scji2kaavbsx65hk6wdx7l")))

(define-public crate-kuba-0.1 (crate (name "kuba") (vers "0.1.2") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ljvv0cckrbb0zp1d0kz7150xiqiql04kl6mjlr68vwnk5px1h10")))

(define-public crate-kuba-0.1 (crate (name "kuba") (vers "0.1.3") (hash "1hi06i5bysjl8rkv44mrfhy2mkz7wm32jy6jkjj6rg999smmzxn0")))

