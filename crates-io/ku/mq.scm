(define-module (crates-io ku mq) #:use-module (crates-io))

(define-public crate-kumquat-0.1 (crate (name "kumquat") (vers "0.1.0") (deps (list (crate-dep (name "syntect") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1fqgilg8qld6yf2z00mfkn8245xdi00s3m5wsbicxn320f4wasc8")))

(define-public crate-kumquat-1 (crate (name "kumquat") (vers "1.0.0") (deps (list (crate-dep (name "syntect") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0qr1sbycv36sk3qcrdf3n17ib1z4vivmm1kl9l2kczl2xa16n4f8")))

(define-public crate-kumquat-1 (crate (name "kumquat") (vers "1.0.1") (deps (list (crate-dep (name "syntect") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1afz2d1ji01fbkfl8d81mvja006ah5g9mnq5ds2ajbv4db9nf5qa")))

