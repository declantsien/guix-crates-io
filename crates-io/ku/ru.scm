(define-module (crates-io ku ru) #:use-module (crates-io))

(define-public crate-kurumi-0.1 (crate (name "kurumi") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glib-build-tools") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "gtk") (req "^0.8") (features (quote ("v4_12"))) (default-features #t) (kind 0) (package "gtk4")) (crate-dep (name "poppler-rs") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5") (default-features #t) (kind 0)))) (hash "1kgcmcgynbw565b93wxc2za7ayydfm9wyiazwzxy6w3q4ji5056l")))

