(define-module (crates-io ku ge) #:use-module (crates-io))

(define-public crate-kugel-0.0.1 (crate (name "kugel") (vers "0.0.1") (deps (list (crate-dep (name "gl") (req "*") (default-features #t) (kind 0)))) (hash "15gjd62rhi9vk5y72wx3200nz1997d88c6g7n885ssi4slxrwq1f")))

(define-public crate-kugel-0.0.2 (crate (name "kugel") (vers "0.0.2") (deps (list (crate-dep (name "gl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0r11fpyizhg3iis6bwavk86hcaphk9hv2jk6vazayvicl4h602p3")))

