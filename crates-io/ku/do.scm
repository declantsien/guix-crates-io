(define-module (crates-io ku do) #:use-module (crates-io))

(define-public crate-kudo-0.1 (crate (name "kudo") (vers "0.1.0") (hash "0mp3jbvrl87xqxmxzbzni6v3xa2gly6gkwdryfd53hhph9b1f6ai")))

(define-public crate-kudo-0.2 (crate (name "kudo") (vers "0.2.0") (hash "1ckngbcnjdqdhr5lmfpc192k6s3169h28bqpza8l19p0vpbf3md3")))

(define-public crate-kudo-0.3 (crate (name "kudo") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1insfgbmr7yl419js9gck07azwk1qy0gj4016kn4jr4bb667figc")))

(define-public crate-kudo-0.4 (crate (name "kudo") (vers "0.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0522ak73bhnwmn3k7sbw7rb3h9wbymv1p3231m4m0m312ml0dn25")))

