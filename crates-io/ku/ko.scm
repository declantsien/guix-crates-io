(define-module (crates-io ku ko) #:use-module (crates-io))

(define-public crate-kukoo-0.1 (crate (name "kukoo") (vers "0.1.0") (deps (list (crate-dep (name "clippy-utilities") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-epoch") (req "^0.9.7") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1wahjig837jajpqilw9wwpxyxpdf5anlmhkbkk2ym89rs9zkbrwj")))

