(define-module (crates-io ys _d) #:use-module (crates-io))

(define-public crate-ys_differential_evolution-0.1 (crate (name "ys_differential_evolution") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "19vynckmm1lk33i43w53z1fgvylbhhzh2la0sr15bqwfh9bfj5zn")))

(define-public crate-ys_differential_evolution-0.1 (crate (name "ys_differential_evolution") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "09fk7s27kbvnb98nnp6jwfz8335hzig5q5n8ilz2yvvf1idm0h7a")))

(define-public crate-ys_differential_evolution-0.1 (crate (name "ys_differential_evolution") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1qvkb1cil73r49xpgv3j91syl1yf8s393sggxl9lzzbmp84grydv")))

(define-public crate-ys_differential_evolution-0.2 (crate (name "ys_differential_evolution") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1j38bxk68saxsfr8x41xgksczjaapn9q4qfi78bs7k0wmkgxsxvb")))

(define-public crate-ys_differential_evolution-0.3 (crate (name "ys_differential_evolution") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "170v9rgx511gqb327y8zs9szc9w88irk1cs5p5g17r9yns4k7128")))

(define-public crate-ys_differential_evolution-0.4 (crate (name "ys_differential_evolution") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ys_simple_db") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j5sai32xbw4hlzlg142grpmv5lmi2isc154h1dp2p9420nr2nsf")))

(define-public crate-ys_differential_evolution-0.5 (crate (name "ys_differential_evolution") (vers "0.5.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ys_simple_db") (req "^0.1") (default-features #t) (kind 0)))) (hash "18by5ymxyjbzw7dsv1vakzlmrn56v4c2gicmzpdq9xg0pynjp54b")))

(define-public crate-ys_differential_evolution-0.5 (crate (name "ys_differential_evolution") (vers "0.5.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ys_simple_db") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z6akn0h6pr3db43d2q6a3r1n2r8fy8nw6z2x8qy6z1jdf71nb94")))

(define-public crate-ys_differential_evolution-0.5 (crate (name "ys_differential_evolution") (vers "0.5.2") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ys_simple_db") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nv8qya1igpfvjjyx84k41wlxfwzzv0zs5rpf0c7w08ri8s7piic")))

