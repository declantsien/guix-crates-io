(define-module (crates-io ys cl) #:use-module (crates-io))

(define-public crate-yscl-1 (crate (name "yscl") (vers "1.0.0") (hash "0brjx9g1w8xxc78gg8lhabyijlypp66lndkwk5hcrh3g2ks1h1sd")))

(define-public crate-yscl-1 (crate (name "yscl") (vers "1.0.1") (hash "1x0b5dig4vjii0x6xkp0sahrx9yfazvkc748iw9nbjgqmav8c0fx")))

(define-public crate-yscl-1 (crate (name "yscl") (vers "1.0.2") (hash "065nih3mic0nfy65f3dbl275sbxvipmw9v32rfsr7pla46a6h2dw")))

(define-public crate-yscl-1 (crate (name "yscl") (vers "1.1.0") (hash "0am4zn1ib4w38k3v94wysy6hw76vwg8zd98zcsnz7d9x9in90k32") (yanked #t)))

(define-public crate-yscl-1 (crate (name "yscl") (vers "1.2.0") (hash "0snw8l3vhm7r4mfvwzm7az7dm96f1xn28qmgv2wmwl4wklylwq8a")))

