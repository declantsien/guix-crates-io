(define-module (crates-io ys _s) #:use-module (crates-io))

(define-public crate-ys_simple_db-0.1 (crate (name "ys_simple_db") (vers "0.1.0") (hash "1ri3g3anm9wa1xy3c44vcclcdvywzwbyvc6alhryc2wlmaajxf69")))

(define-public crate-ys_simple_db-0.1 (crate (name "ys_simple_db") (vers "0.1.1") (hash "0cr3cs7adnsiss2aw0wssyqj35bgwjxvigki07nwwy5bwmm8q3s9")))

(define-public crate-ys_simple_db-0.1 (crate (name "ys_simple_db") (vers "0.1.2") (hash "1yj4lim6v06702dw1k48h7zky6k8l8ai1llm6k453k7d6gfm9jq8")))

