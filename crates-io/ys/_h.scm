(define-module (crates-io ys _h) #:use-module (crates-io))

(define-public crate-ys_hello-0.0.3 (crate (name "ys_hello") (vers "0.0.3") (hash "1kcwqg3hscwqpfp8zqnsfaxl26pjqxw90b6ailj4vim6p39kajrb")))

(define-public crate-ys_hello-0.0.4 (crate (name "ys_hello") (vers "0.0.4") (hash "1767kxz3sjikx3v56c8b26fmy1gb2i4hjkn721kpz5byf4ang9n5")))

