(define-module (crates-io ys fe) #:use-module (crates-io))

(define-public crate-ysfed-0.1 (crate (name "ysfed") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cocoon") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1z99wmvlm3qw1brf1974hi5k5pqxh37apsah0avcp99nwx1wx7n5")))

(define-public crate-ysfed-0.1 (crate (name "ysfed") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cocoon") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0bflfxnmmc72nvwyha9lcriy76panw07jfpvpizjh5hkhna8lhgs")))

(define-public crate-ysfed-0.1 (crate (name "ysfed") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cocoon") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.3.1") (default-features #t) (kind 0)))) (hash "0q4kp15y3amacn45hb5rwggb73jnskakdjqh9s22kfrycsm2hn0w")))

