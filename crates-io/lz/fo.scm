(define-module (crates-io lz fo) #:use-module (crates-io))

(define-public crate-lzfoo-0.1 (crate (name "lzfoo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (kind 0)) (crate-dep (name "lzfse_rust") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pnw0qqry516wc7y9zqwik5q14hbgrnp6mnbgl6s9455plafhqbz") (yanked #t)))

(define-public crate-lzfoo-0.2 (crate (name "lzfoo") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (kind 0)) (crate-dep (name "lzfse_rust") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0zahalii991440w9w4v4a1pigj0zrxh054qi8nsmxiwgjgddby6i")))

