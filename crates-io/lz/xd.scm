(define-module (crates-io lz xd) #:use-module (crates-io))

(define-public crate-lzxd-0.1 (crate (name "lzxd") (vers "0.1.0") (hash "05hpm2llflwjia5bw25r7wq5ha1bgkc9szrx7vl9jxmvgk395bpm")))

(define-public crate-lzxd-0.1 (crate (name "lzxd") (vers "0.1.1") (hash "1a39gws6f2z8f3zmd1kjinqypn14rg4dzy8diyl2r8ksxr8mlnm4")))

(define-public crate-lzxd-0.1 (crate (name "lzxd") (vers "0.1.2") (hash "1y8x40fwzj6yc5hcvlixkyynm9sv6187rc9bzqsrh53b40rs331r")))

(define-public crate-lzxd-0.1 (crate (name "lzxd") (vers "0.1.3") (hash "0pgkig1rfjgircay9qs25br6gxirdcfwyg562idcybz8j1ywi396")))

(define-public crate-lzxd-0.1 (crate (name "lzxd") (vers "0.1.4") (hash "04xjqs0n9yrl5ifsr2bq1aqqqa2amnj3z5ny8pdxznfx1pr64i3q")))

(define-public crate-lzxd-0.2 (crate (name "lzxd") (vers "0.2.0") (hash "1x2x11a2y8awlc44bzb07lpm550k602b1kan4jmxclxnyd1aw7ry")))

(define-public crate-lzxd-0.2 (crate (name "lzxd") (vers "0.2.1") (hash "0pcwa6h6gxz9mpz3zzxmhsb7svyl2bzk5jh66xgj59gxgl908xb1")))

(define-public crate-lzxd-0.2 (crate (name "lzxd") (vers "0.2.2") (hash "0g0a8gl1vrqlw0c2p0x3rx2m9fm66p05v8dn1y0fr2a7rnilgxmk")))

(define-public crate-lzxd-0.2 (crate (name "lzxd") (vers "0.2.3") (hash "020nffy6akp2jflcbs6zcx8wn4xs80vc15j6ibfjk2lii42m198w")))

(define-public crate-lzxd-0.2 (crate (name "lzxd") (vers "0.2.4") (hash "0v4ril3kqwr9z6dfmf2qlhvpb6apy0lfn8ni4arvbxs9k11n6kyy")))

(define-public crate-lzxd-0.2 (crate (name "lzxd") (vers "0.2.5") (hash "0090w6yjgcg5267mmf7n2qllm76widnxa4bdssd440ri31m37rsx")))

