(define-module (crates-io lz y_) #:use-module (crates-io))

(define-public crate-lzy_pbkdf2-0.1 (crate (name "lzy_pbkdf2") (vers "0.1.0") (deps (list (crate-dep (name "hmac-sha512") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "03mvvj4dv18ghammsrnw04z5f83rsdcdjggxf68f0v94mv0yb0jv")))

