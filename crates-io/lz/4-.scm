(define-module (crates-io lz #{4-}#) #:use-module (crates-io))

(define-public crate-lz4-builder-1 (crate (name "lz4-builder") (vers "1.0.0") (hash "16r5axbhg1l0axyhnhkg8r1y8afwi2bi3vm378bvg89jqnpls5df") (yanked #t)))

(define-public crate-lz4-builder-1 (crate (name "lz4-builder") (vers "1.0.1") (hash "1kkf5c4v7r13wz2w5hvblgn09y2cnccxbfmf0qnq9n5b8cywwwnv")))

(define-public crate-lz4-compress-0.1 (crate (name "lz4-compress") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "1jfycwj5vi21dhafblfcmzmxanfw5x29r3i2z5zyng2bzjm4r8ln")))

(define-public crate-lz4-compress-0.1 (crate (name "lz4-compress") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1") (default-features #t) (kind 0)))) (hash "14cb8rpdfk6q3bjkf7mirpyzb6rvvcglqnayx6lvpa92m4rnb5hg")))

(define-public crate-lz4-compression-0.6 (crate (name "lz4-compression") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0iba90miy47asy7gxpahvxxf3qrdx7qx8rkj6gl7flabdn2aavxh")))

(define-public crate-lz4-compression-0.6 (crate (name "lz4-compression") (vers "0.6.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "13m4whip682h29x89wkhz1jz44jdi78y1ndghj3j8i3ffkgh6j4n")))

(define-public crate-lz4-compression-0.7 (crate (name "lz4-compression") (vers "0.7.0") (hash "0w42pg1kzxk94sn8n510s800v8b00y54j8nq8ypkqfpijyzh84bn")))

(define-public crate-lz4-java-wrc-0.2 (crate (name "lz4-java-wrc") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lz4-sys") (req "^1.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.11") (features (quote ("std" "safe-encode" "safe-decode"))) (optional #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6") (kind 0)))) (hash "03bdsz1wg60fir1095iw3rvjgfwnfvj5rfwca38aazriplfw3l9y") (features (quote (("use_lz4_flex" "lz4_flex") ("use_lz4-sys" "lz4-sys" "libc") ("default" "use_lz4_flex"))))))

(define-public crate-lz4-rs-1 (crate (name "lz4-rs") (vers "1.0.127") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "1vbjhmdkmwpkcbgzxmzlyvzn4xzpkby6p0lanaqfcrv0lraigb9r") (yanked #t)))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.0.1+1.7.3") (deps (list (crate-dep (name "gcc") (req "^0.3.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "16imwc0vrph4lkj0ammmyzl8mjm5wvlzr0x7pnxv9ib7wzdn05kz")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.0.1+1.7.5") (deps (list (crate-dep (name "gcc") (req "^0.3.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1vh16cz5ny5vpnqy4qabnrby7apsd0x9ndvlzj8k9macbngjbnq0")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.7.5") (deps (list (crate-dep (name "gcc") (req "^0.3.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "16wy4178sgwaa5yx568d6d3x9azcjd7v7jfvvm89n5n4md8s9s1r")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.8.0") (deps (list (crate-dep (name "gcc") (req "^0.3.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1db2sjm3ckb0wpypxrh1jlhj4yclqpbdsw12mb9g751rpb1l9455")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.8.2") (deps (list (crate-dep (name "cc") (req "^1.0.24") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (kind 0)))) (hash "0dz97hm35mc9ckz40y4m95wxd9a80g5bchzjdjr0wlv496yff7x4") (links "lz4")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.8.3") (deps (list (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)))) (hash "1znsrhpkl8bg4sw3fs5cb3haqnq8k1mxvb3ksdc1qcz948l05ar0") (links "lz4")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.9.2") (deps (list (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)))) (hash "1bmc82bddx2lm0r9bn422cxbwlwq6qld6m6l78hjcclbbnlrm9yw") (links "lz4")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.9.3") (deps (list (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)))) (hash "05vybnw0ccxf7qv3gr7xzd9nrqq35ybgmadq5p032vzdw848kgnp") (links "lz4")))

(define-public crate-lz4-sys-1 (crate (name "lz4-sys") (vers "1.9.4") (deps (list (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)))) (hash "0059ik4xlvnss5qfh6l691psk4g3350ljxaykzv10yr0gqqppljp") (links "lz4")))

