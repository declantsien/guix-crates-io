(define-module (crates-io lz _f) #:use-module (crates-io))

(define-public crate-lz_fnv-0.1 (crate (name "lz_fnv") (vers "0.1.0") (deps (list (crate-dep (name "extprim") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "extprim_literals") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "10dbn23lhj6zwmknpqj1lgdn4fy8n55mx3vr0d59hij3yldk5pji") (features (quote (("u128" "extprim" "extprim_literals") ("nightly") ("default"))))))

(define-public crate-lz_fnv-0.1 (crate (name "lz_fnv") (vers "0.1.1") (deps (list (crate-dep (name "extprim") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "extprim_literals") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jk9gg1djn6s13sgvgw3g8xwcnw4q23lw5yq1xayp8gwphkic8ja") (features (quote (("u128" "extprim" "extprim_literals") ("nightly") ("default"))))))

(define-public crate-lz_fnv-0.1 (crate (name "lz_fnv") (vers "0.1.2") (deps (list (crate-dep (name "extprim") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "extprim_literals") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0f4fn8axxj6f1krjdvla03l6z18ivh5lwvs6m9p9gw2ipq6ipfwv") (features (quote (("u128" "extprim" "extprim_literals") ("nightly") ("default"))))))

