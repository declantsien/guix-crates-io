(define-module (crates-io lz f_) #:use-module (crates-io))

(define-public crate-lzf_headers-0.1 (crate (name "lzf_headers") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0q274av3w4ifiwmxi1vf5nf6ljmxi7jvb63498drb6gq062v2als")))

(define-public crate-lzf_headers-0.1 (crate (name "lzf_headers") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1n5pb6lk3q2pcw8mkc35b39zj6js7dz5j3qlhx336naljah6gh55")))

