(define-module (crates-io lz #{4u}#) #:use-module (crates-io))

(define-public crate-lz4util-0.1 (crate (name "lz4util") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1") (default-features #t) (kind 0)))) (hash "15sbsrzpb687bk8fk76sqc94yh4w2zf7901203ggrnh8znqcf8bn")))

(define-public crate-lz4util-0.1 (crate (name "lz4util") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1") (default-features #t) (kind 0)))) (hash "0cfa2vfjy33g11nlz6amld1zyqxrsazlbp8slg08qfdjxxnjpvbi")))

