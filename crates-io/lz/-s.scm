(define-module (crates-io lz -s) #:use-module (crates-io))

(define-public crate-lz-str-0.1 (crate (name "lz-str") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "js-sys") (req "^0.3.46") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen") (req "^0.2.69") (optional #t) (default-features #t) (kind 0)))) (hash "132vdlalivgqj302f748yfhc3jrsganlh30a9bpiy3swwpqiw21s") (features (quote (("wasm-bindgen-support" "wasm-bindgen" "js-sys") ("nightly" "criterion/real_blackbox"))))))

(define-public crate-lz-str-0.2 (crate (name "lz-str") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0w79casxjk05vji62f0kl4kkmbs83dp2m9563dyrvmp7ciz8l3w0") (features (quote (("nightly" "criterion/real_blackbox"))))))

(define-public crate-lz-str-0.2 (crate (name "lz-str") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1k3h7k104xvmymzka7sa6xw6wwjkb8l338syszp90w12fwnxgwrr") (features (quote (("nightly" "criterion/real_blackbox"))))))

(define-public crate-lz-string-0.1 (crate (name "lz-string") (vers "0.1.0") (hash "0ygyr1l092ijd7mj6r5lvxr9wa1raf0c9qnrw0p2b8nyq4drf2q8")))

(define-public crate-lz-string-0.1 (crate (name "lz-string") (vers "0.1.1") (hash "03jwa71gk6s9yl3yvbzr6c833f7an0vb1gnrhl4zsxgv0a35k8gp")))

