(define-module (crates-io lz _i) #:use-module (crates-io))

(define-public crate-lz_interval_tree_clock-0.1 (crate (name "lz_interval_tree_clock") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.80") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "11mlylr5mnw7njwa1n3kdc3fggdnciq8x3zl63b7flk42mcmj6lb")))

