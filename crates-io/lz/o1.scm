(define-module (crates-io lz o1) #:use-module (crates-io))

(define-public crate-lzo1x-0.1 (crate (name "lzo1x") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lzo-sys") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 2)))) (hash "0xs1lykn9ig4dcfjd9b9rkjc7j794pv4g40a4cyxf64v3lphrkb6") (rust-version "1.75.0")))

(define-public crate-lzo1x-0.2 (crate (name "lzo1x") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lzo-sys") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "04mvhfsvf4n9d6adsgmjxrg8i7zhb7knvwdsrhhhs13wxckn06yi") (features (quote (("std") ("default" "std")))) (rust-version "1.75.0")))

(define-public crate-lzo1x-1-0.1 (crate (name "lzo1x-1") (vers "0.1.0") (deps (list (crate-dep (name "iai") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1gr8d13zy1mhsyx8vh5zl6z0ysrhcvahy1ia84irzbahaq4hcn7n") (features (quote (("std") ("default" "std")))) (yanked #t) (rust-version "1.56")))

