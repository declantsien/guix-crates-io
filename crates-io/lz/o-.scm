(define-module (crates-io lz o-) #:use-module (crates-io))

(define-public crate-lzo-sys-0.1 (crate (name "lzo-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "1kwl6m2nyk8wnjvmacdw5rz8g1l68cpm1zvs2gjpx4i2zpqys67n") (rust-version "1.66.1")))

(define-public crate-lzo-sys-0.2 (crate (name "lzo-sys") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "11cljanp0s6rqc989972g4qkdp6rvymilil8m7w1cq8axmfmmk1j") (rust-version "1.66.1")))

(define-public crate-lzo-sys-0.3 (crate (name "lzo-sys") (vers "0.3.0") (deps (list (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "0gn8nl6psvvyqb26v3pvw2zcp8hswaxzr8w3fkmndhj1mbc8xqxz") (rust-version "1.66.1")))

(define-public crate-lzo-sys-0.3 (crate (name "lzo-sys") (vers "0.3.1") (deps (list (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "0iqlp85pl460lc9np7pdc3rhxyn8cky4wj3jn8v469bnwwggqaqh") (rust-version "1.66.1")))

