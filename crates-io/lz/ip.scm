(define-module (crates-io lz ip) #:use-module (crates-io))

(define-public crate-lzip-0.1 (crate (name "lzip") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lzip-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.13") (optional #t) (default-features #t) (kind 0)))) (hash "09abhsfkcwjg8y7mhb729dbkjrwxgybvwkyyvhyva5zdnsyw6v93") (features (quote (("tokio" "futures" "tokio-io") ("static" "lzip-sys/static"))))))

(define-public crate-lzip-0.1 (crate (name "lzip") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lzip-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio-io") (req "^0.1.13") (optional #t) (default-features #t) (kind 0)))) (hash "0ydfg4m3nz91v9smvzrr920pdaqj0g8h94d6wffwacdf31gr8d3v") (features (quote (("tokio" "futures" "tokio-io") ("static" "lzip-sys/static"))))))

(define-public crate-lzip-sys-0.1 (crate (name "lzip-sys") (vers "0.1.0+1.13") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.120") (default-features #t) (kind 0)))) (hash "1hggvwgq5b7r3f1jqv4sr3z0qjzj1cbjipmwvknqlc1kh7zcbk70") (features (quote (("static")))) (links "lzip")))

(define-public crate-lzip-sys-0.1 (crate (name "lzip-sys") (vers "0.1.1+1.13") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.120") (default-features #t) (kind 0)))) (hash "1ycwkgzjcfqvw31rb9hbahaim2yzys0m9kpq0jll4q3wj36avahd") (features (quote (("static")))) (links "lzip")))

(define-public crate-lzip-sys-0.1 (crate (name "lzip-sys") (vers "0.1.3+1.13") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.122") (default-features #t) (kind 0)))) (hash "1jmbbikaa3ka9z764nsqawrird8fyjy3v6zax9slw4l4p6bq2d8n") (features (quote (("static")))) (links "lzip")))

(define-public crate-lzip-sys-0.1 (crate (name "lzip-sys") (vers "0.1.4+1.13") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.125") (default-features #t) (kind 0)))) (hash "007rcn6iap96k9gaamw47fcmlpwv3jpkax6q4wnr9zm6kndc312x") (features (quote (("static")))) (links "lzip")))

