(define-module (crates-io lz f-) #:use-module (crates-io))

(define-public crate-lzf-sys-0.1 (crate (name "lzf-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.66") (optional #t) (default-features #t) (kind 1)))) (hash "1v1ab0rsy3gfvx7fzjrn43dzk7wjmcmfjvfrgv3y4189rqix1607") (features (quote (("static" "cc") ("paranoid" "bindgen") ("default" "static")))) (links "lzf")))

