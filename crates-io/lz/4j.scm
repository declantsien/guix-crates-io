(define-module (crates-io lz #{4j}#) #:use-module (crates-io))

(define-public crate-lz4jb-0.1 (crate (name "lz4jb") (vers "0.1.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.8") (features (quote ("std" "safe-encode" "safe-decode"))) (kind 0)) (crate-dep (name "twox-hash") (req "^1") (kind 0)))) (hash "048i50jk783qdrnir9s72xl3kvdjr51ggw2xkg8lmlbjp54rja0m")))

