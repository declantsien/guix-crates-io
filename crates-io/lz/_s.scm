(define-module (crates-io lz _s) #:use-module (crates-io))

(define-public crate-lz_shared_udp-0.1 (crate (name "lz_shared_udp") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "tokio-core") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1xr6m215pgisvp3gr5z3ijm9f5nh3n54qdap85xkk3yswl44ph78")))

(define-public crate-lz_shared_udp-0.1 (crate (name "lz_shared_udp") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "tokio-core") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0s41dmig1nm0ci733wm0vfgvmxbpgg51m89533d0k4afgzfbdhzy")))

(define-public crate-lz_shared_udp-0.1 (crate (name "lz_shared_udp") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "tokio-core") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1h4r3698837rl2czdjiw4wynvxzi73zjsc4i7wphrh15m16z7frp")))

(define-public crate-lz_stream_io-0.1 (crate (name "lz_stream_io") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1kcr11phpc6rldn658ph69y65fg0d5wvhv0zm0zdjhqx69rd9ixd")))

