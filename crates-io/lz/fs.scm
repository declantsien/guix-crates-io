(define-module (crates-io lz fs) #:use-module (crates-io))

(define-public crate-lzfse-0.1 (crate (name "lzfse") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lzfse-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0b5jk6xlpvp1pckssqjayb9b1gbq0p6wq4h3fs583w8ndphg5rz5")))

(define-public crate-lzfse-sys-1 (crate (name "lzfse-sys") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ybad5kl6pfw1fai3wg3ffg81xnbwgvkwwrr8jxylhw3y7v76am3") (links "lzfse")))

(define-public crate-lzfse_rust-0.1 (crate (name "lzfse_rust") (vers "0.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "02mml99gxmb6vbsm9x3358k9088xnbprkacz4iw1hcblpsaj1z2g") (yanked #t)))

(define-public crate-lzfse_rust-0.2 (crate (name "lzfse_rust") (vers "0.2.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "1kxz04x6zvlx2y18rlpc2h9am9827fqfnwbb6iaz521fki0k6gzq")))

