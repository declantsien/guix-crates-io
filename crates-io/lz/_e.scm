(define-module (crates-io lz _e) #:use-module (crates-io))

(define-public crate-lz_etzynger_tree-0.1 (crate (name "lz_etzynger_tree") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1jxcxgji29ifa738svgibgldggw3xlcza70qcn9lgxbwjfrlxypa") (yanked #t)))

(define-public crate-lz_eytzinger_tree-0.1 (crate (name "lz_eytzinger_tree") (vers "0.1.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0gib9fi78pm8l2jvhb8jmrdphzj12ijjb74wijxgwsnq8j7299l5")))

(define-public crate-lz_eytzinger_tree-0.2 (crate (name "lz_eytzinger_tree") (vers "0.2.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0c3cqa4x4vnffipm1l6s5m7wxnkxw55xw5ciya0ncxkqc7rkcyb1")))

(define-public crate-lz_eytzinger_tree-0.3 (crate (name "lz_eytzinger_tree") (vers "0.3.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1ddww1csk06krcs2xf1kg4sc29wgq13ix2bkhh08lw6ay35b7i5z")))

