(define-module (crates-io lz ha) #:use-module (crates-io))

(define-public crate-lzham-0.1 (crate (name "lzham") (vers "0.1.0") (deps (list (crate-dep (name "lzham-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1vnnm42fdv8zzi92vg0p6s0ym9dq0slmsjyiq4ll19dybkrh14pl") (features (quote (("static" "lzham-sys/static") ("dynamic" "lzham-sys/dynamic"))))))

(define-public crate-lzham-0.1 (crate (name "lzham") (vers "0.1.1") (deps (list (crate-dep (name "lzham-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "12kxkmfb645skfsy4kp70nb0z6s7skz7l1wfzhxgzaxvaxd2ibcn") (features (quote (("static" "lzham-sys/static") ("generate_binding" "lzham-sys/generate_binding") ("dynamic" "lzham-sys/dynamic"))))))

(define-public crate-lzham-alpha-sys-0.1 (crate (name "lzham-alpha-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "1bkxbax660wj66qfxchy32rvg494za89drwrhl5nxc1w6r5g35qb") (features (quote (("generate_binding" "bindgen"))))))

(define-public crate-lzham-alpha-sys-0.1 (crate (name "lzham-alpha-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "10m08w169ycs9f7zp4hyvldly4gjrmc6rvcik6vanilibff1kl3s") (features (quote (("generate_binding" "bindgen"))))))

(define-public crate-lzham-sys-0.1 (crate (name "lzham-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0g5ycq5lnk1lqx5pa6m0v50wqmab551ya7y2crwv5qj76jq8c3wj") (features (quote (("static") ("generate_binding" "bindgen") ("dynamic")))) (yanked #t)))

(define-public crate-lzham-sys-0.1 (crate (name "lzham-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "0mnpxsxpbwjbhkkd6mdpsmnjjw4bk8rk4s3lql927gyj0vrcr5sy") (features (quote (("static") ("generate_binding" "bindgen") ("dynamic"))))))

