(define-module (crates-io lz ss) #:use-module (crates-io))

(define-public crate-lzss-0.8 (crate (name "lzss") (vers "0.8.0") (deps (list (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "1mxsjg8z5bhimc89l5g64fcniw0nbbzlydrhym2jcvrxp64vgb74") (features (quote (("std" "void/std") ("default" "std") ("const_panic"))))))

(define-public crate-lzss-0.8 (crate (name "lzss") (vers "0.8.1") (deps (list (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "0d0jhih2kna2y10lcs337lhmf2pn9dcvr5g0wnbvp0h86wpnff57") (features (quote (("std" "void/std" "alloc") ("default" "std") ("const_panic") ("alloc"))))))

(define-public crate-lzss-0.8 (crate (name "lzss") (vers "0.8.2") (deps (list (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "1swpr8v9w0qh528m3dk9zwdlafzr9c9d3idm7g0h0ljf2akbkqir") (features (quote (("std" "void/std" "alloc") ("default" "std") ("const_panic") ("alloc"))))))

(define-public crate-lzss-0.9 (crate (name "lzss") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "14j0lz661syp2510hmj08arjwmii8x7dhm4zx8i39rl5jp4k5dj4") (features (quote (("std" "void/std" "alloc") ("safe") ("default" "std" "safe") ("alloc"))))))

(define-public crate-lzss-0.9 (crate (name "lzss") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "00bs4px0in02rvgn4dzah0gkja18606vzm1p9r6mfyy2gzldkjzz") (features (quote (("std" "void/std" "alloc") ("safe") ("default" "std" "safe") ("alloc"))))))

(define-public crate-lzss-cli-0.8 (crate (name "lzss-cli") (vers "0.8.0") (deps (list (crate-dep (name "lzss") (req "^0.8") (default-features #t) (kind 0)))) (hash "1r5d5a5qb5m7akhiggc0kqnvamdn9fc8w1kgc7lanflq46yri4ql")))

(define-public crate-lzss-cli-0.9 (crate (name "lzss-cli") (vers "0.9.0") (deps (list (crate-dep (name "lzss") (req "^0.9") (default-features #t) (kind 0)))) (hash "0g3gshyvk2lbwvj6hw45afxcm1iwnalgaq8gr2bcln9d857r7z3c")))

