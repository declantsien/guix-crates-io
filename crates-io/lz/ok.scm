(define-module (crates-io lz ok) #:use-module (crates-io))

(define-public crate-lzokay-1 (crate (name "lzokay") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 1)))) (hash "1063i42c2317xk7lbk1nviffnw66ni8riw4zi50krjnav24ii29q") (features (quote (("std" "alloc") ("default" "compress" "decompress" "std") ("decompress") ("compress") ("alloc"))))))

(define-public crate-lzokay-1 (crate (name "lzokay") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 1)))) (hash "0sc0ccfsbi81kkzlybx3nn5va9q6v1j94mgqbcd304rhv8vf526g") (features (quote (("std" "alloc") ("default" "compress" "decompress" "std") ("decompress") ("compress") ("alloc"))))))

(define-public crate-lzokay-native-0.1 (crate (name "lzokay-native") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "minilzo-rs") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "sha1") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0c475icg27hinwjlh11mqz5va8dr9q7n73lq7rn8qyfjmmkscavr") (features (quote (("default" "compress" "decompress") ("decompress" "byteorder") ("compress"))))))

