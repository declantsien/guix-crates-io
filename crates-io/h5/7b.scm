(define-module (crates-io h5 #{7b}#) #:use-module (crates-io))

(define-public crate-h57bank-0.1 (crate (name "h57bank") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rustbreak") (req "^2") (features (quote ("ron_enc"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hjblh4nnc3wmxifx4byfbm9sfnhpb0r14k159fbggrj3a3n4z6s")))

(define-public crate-h57bank-0.1 (crate (name "h57bank") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rustbreak") (req "^2") (features (quote ("ron_enc"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xpaffdqq43glrsyfaxjl197m3kjp6mhdrzsf56iq95s5bs1i7xq")))

