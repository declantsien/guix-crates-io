(define-module (crates-io ky f_) #:use-module (crates-io))

(define-public crate-kyf_log-0.1 (crate (name "kyf_log") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "09nnhvss395nbdy79aa8zg42585yzj85923a6lv2bfsg92jpm75m")))

