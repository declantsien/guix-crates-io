(define-module (crates-io ky ra) #:use-module (crates-io))

(define-public crate-kyrax-stm32f401re-0.1 (crate (name "kyrax-stm32f401re") (vers "0.1.0") (hash "0prsxpvjjy4dj3wmz9ymmcyvqd13xrl199gwksyra6kvrzp5ag91")))

(define-public crate-kyrax-stm32f401re-0.2 (crate (name "kyrax-stm32f401re") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (kind 0)))) (hash "1zis7ws0ccr1kfd63glsj3ywmkhm3s03a4ap8vhmd97bywam726q")))

(define-public crate-kyrax-stm32f401re-0.3 (crate (name "kyrax-stm32f401re") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (kind 0)))) (hash "0ry8gd9b5fii4l22400nm6hpy5qlqb3pxs4vpir1w73gqw7rklb8")))

(define-public crate-kyrax-stm32f401re-0.3 (crate (name "kyrax-stm32f401re") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (kind 0)))) (hash "0mpz9cgd10vj7ll1hz5igynnx805wdi0vxjpsp828p2xrqjsfh6v")))

(define-public crate-kyrax-stm32f401re-0.3 (crate (name "kyrax-stm32f401re") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (kind 0)))) (hash "077lml0acrgmhz7sy5g6f067y1qn57z728jwqdsnx2l7dsv3ww9r")))

