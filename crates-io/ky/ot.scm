(define-module (crates-io ky ot) #:use-module (crates-io))

(define-public crate-kyoto-0.0.1 (crate (name "kyoto") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "14hyyrk5n18rw588i67a1wiy8ss5igk6qmc7yzri0zbd2nyx25hh")))

(define-public crate-kyoto_data-0.1 (crate (name "kyoto_data") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_protocol") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06p04pd5i860apra7z8j0nm2rwvdzn4gz71g0rxsa97sxj40plv0")))

(define-public crate-kyoto_machine-0.1 (crate (name "kyoto_machine") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_data") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_protocol") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lrgl8m05n5kzpzvpyy1a4yxs07693rkkgxdlfnqzha32g28ma27")))

(define-public crate-kyoto_main-0.1 (crate (name "kyoto_main") (vers "0.1.0") (deps (list (crate-dep (name "kyoto_data") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_network") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_protocol") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1551piydcql4ijb598msn288b3m247bz43is80605pypy92x3fgc")))

(define-public crate-kyoto_network-0.1 (crate (name "kyoto_network") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_data") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_machine") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "kyoto_protocol") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0ns9m1jfzplv2ky9nr2px5552l06bafklb5sr9zxa16izs1f4cwg")))

(define-public crate-kyoto_protocol-0.0.1 (crate (name "kyoto_protocol") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kpaikcrk7mvla8y3zypgwn1gkhsw6ax51n8jgaar3kjsns8chvr")))

(define-public crate-kyoto_protocol-0.1 (crate (name "kyoto_protocol") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n4v0ak720gh7dhafafrb36bass4w6a8wfmd6r8bnkakhmiwyl7n")))

(define-public crate-kyotocabinet-0.0.1 (crate (name "kyotocabinet") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0zs2j88yv4hqlmlvva2h9hnnv4c8w1wcriam7x0b0yy38lca26ax")))

(define-public crate-kyotocabinet-0.0.2 (crate (name "kyotocabinet") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "08l62m9j9k2sj8njl8kiwc8i0lxsy1s61kd3ywzm99z24cc1mi68")))

(define-public crate-kyotocabinet-0.0.3 (crate (name "kyotocabinet") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "12sbbngi6k91mv1p9g7vbj5nnxgr1lf51hpglk9jkh8by7fi070y")))

(define-public crate-kyotocabinet-0.0.5 (crate (name "kyotocabinet") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0qh71xybrinqrk4k613y9lsbh33fgcs5ghdr48djahgilymplb4x")))

(define-public crate-kyotocabinet-0.0.6 (crate (name "kyotocabinet") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0ykd4ihhyw36c6b4mka55n49hi49hrf0pc62xay2sridj8cby417")))

