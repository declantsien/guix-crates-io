(define-module (crates-io ky li) #:use-module (crates-io))

(define-public crate-kylin-0.1 (crate (name "kylin") (vers "0.1.0") (hash "0pksj4xb10d7931vk4wkxmssjddn7raj1sl9651q40q9jq6asfb2")))

(define-public crate-kylix-0.1 (crate (name "kylix") (vers "0.1.0") (hash "1ppaw7dh4rqzx3nqaj8i690mq36la30mv9azrbb5h46s1rsn0vvq")))

