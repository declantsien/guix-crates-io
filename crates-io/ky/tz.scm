(define-module (crates-io ky tz) #:use-module (crates-io))

(define-public crate-kytz-0.1 (crate (name "kytz") (vers "0.1.0") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "bessie") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "z32") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "09qj9p828ysk7sxmcq5dgaix90x5y2v6lgzn6rcyhdrj6g674dsk")))

