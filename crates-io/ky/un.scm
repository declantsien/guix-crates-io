(define-module (crates-io ky un) #:use-module (crates-io))

(define-public crate-kyun-0.2 (crate (name "kyun") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1") (default-features #t) (kind 0)))) (hash "0j44f7f60m3jbg69kcnim5ka1in8d1caam8n2bdrzanldsjd7nyh")))

(define-public crate-kyun-0.2 (crate (name "kyun") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1") (default-features #t) (kind 0)))) (hash "1z9sxsnxbdvp6d7lj6nk05pkls7869jkn09fm86fxyyr0w1myk6r")))

