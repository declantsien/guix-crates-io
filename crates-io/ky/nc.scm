(define-module (crates-io ky nc) #:use-module (crates-io))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.0") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "18bpifk7fkbaji3ym8qv8cysy93ak1yk1px1mm8i0ww6plwg625n") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.1") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1cqyzfjkvfjz83hqcmdrr1s8dc1sh9mwm9q3bljpxbv0l670acx9") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.2") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ayyrlq4sl07qbs09z2jiaflaxij172xr4d3dmzmgz8rymxmlcpp") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.3") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0mc30pwz231h3rsmd52imlaga6q2raqcv2q1kcpkidwmx555ly6n") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.4") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "159rxddapi74p1jpi2s9j8h3nw62mk326068x79hi4aplj3dr6jy") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.5") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "19jrzfag5ar9x5n4iz2w1qixl2cxq53q8xpsgld5rs6x4dn4xs3j") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.6") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "15z8lsvfi3fn9lzr04k3qcxw91bk3r4ppy4101ydyqm53c52p4f0") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.7") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0b4biw7iaaklywms8v2605gz6zassj3v64dvzsw3z1mngah37mip") (yanked #t)))

(define-public crate-kync-0.1 (crate (name "kync") (vers "0.1.8") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1m8k35jahzys2bxyj4c1l03g8wdk2n3qic2vhq7lr5h8nmnfcibp") (yanked #t)))

(define-public crate-kync-0.2 (crate (name "kync") (vers "0.2.0") (deps (list (crate-dep (name "kync_test_plugin") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.5") (default-features #t) (kind 0)))) (hash "1i97sawib3j9arkqaq7jbln88fbjdqxnfhf7flrwa84nyasvdniq") (yanked #t)))

(define-public crate-kync_test_plugin-0.1 (crate (name "kync_test_plugin") (vers "0.1.0") (hash "0j6143saqi6km370nd8w99v2j3bj1xkj8m9j6wdbwxyicfvrbygh") (yanked #t)))

(define-public crate-kync_test_plugin-0.1 (crate (name "kync_test_plugin") (vers "0.1.1") (hash "0x9djkyp9j7sgjznmrpzig8kpajl18x0lmhby13blkvkxfxlnk2s") (yanked #t)))

(define-public crate-kync_test_plugin-0.1 (crate (name "kync_test_plugin") (vers "0.1.2") (hash "1iac1611m0d96kh57nma2bjp1hwf7d36ndm8ncylsbzbwgihsqqn") (yanked #t)))

(define-public crate-kync_test_plugin-0.1 (crate (name "kync_test_plugin") (vers "0.1.3") (hash "16sxbplp4l8pigqw9a60pyqk33i322hkl5bb0q5kbgs8yjdiyi9g") (yanked #t)))

(define-public crate-kync_test_plugin-0.1 (crate (name "kync_test_plugin") (vers "0.1.4") (hash "18iiky57njxp8ri1g47c1dxngdmqg85yysj3adrdavcz0w232b7i") (yanked #t)))

(define-public crate-kync_test_plugin-0.2 (crate (name "kync_test_plugin") (vers "0.2.0") (hash "188l6b16mpr5c48cylni4acq127zcdimc973d5dicn1mss73y28g") (yanked #t)))

(define-public crate-kync_test_plugin-0.2 (crate (name "kync_test_plugin") (vers "0.2.1") (hash "1wn5gnp82ba6fgj6fsn0gs9ra7jrmfylxvfg4a3dvmpwjkxnslv6") (yanked #t)))

