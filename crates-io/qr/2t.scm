(define-module (crates-io qr #{2t}#) #:use-module (crates-io))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.6") (features (quote ("style"))) (kind 0)) (crate-dep (name "qrcode") (req "^0.9") (kind 0)))) (hash "0agn6acfninccsli49br4a54yx2kyxr4302c966h8kp69b9nykqd")))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.7") (features (quote ("style"))) (kind 0)) (crate-dep (name "qrcode") (req "^0.9") (kind 0)))) (hash "0j8kg2m7kqn9nn2q74x8lmf6if8drr78cms4iwprcn5hg847b09m")))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.9") (features (quote ("style"))) (kind 0)) (crate-dep (name "qrcode") (req "^0.10") (kind 0)))) (hash "0h27ryilykbbssl0a7cal4hf85dkjkcbamz1kzfdwd70b5m7gpn1")))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.10") (features (quote ("style"))) (kind 0)) (crate-dep (name "qrcode") (req "^0.10") (kind 0)))) (hash "0ziffas1nhgmamn8rvpc0x4n3dqyr9zn2pxslgnfjnlnwjc0k2qm")))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.10") (features (quote ("style"))) (kind 0)) (crate-dep (name "qrcode") (req "^0.11") (kind 0)))) (hash "0jll0a33pixxsry2l385va6j6dbfh9jgnlbqnfj10vn3s0xcrfs5")))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.5") (deps (list (crate-dep (name "crossterm") (req "^0.13") (features (quote ("style"))) (kind 0)) (crate-dep (name "qrcode") (req "^0.11") (kind 0)))) (hash "09da9q0jx2zhkzzkpnzp6q4jzxf0zmwv8vmc4w7vlik9izcb9vfx")))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.7") (deps (list (crate-dep (name "crossterm") (req "^0.14") (kind 0)) (crate-dep (name "qrcode") (req "^0.11") (kind 0)))) (hash "1vi69w5irjx9ra4bfhmylixp4apv2fvxq4zrr5p11gl9r9h29zn1")))

(define-public crate-qr2term-0.1 (crate (name "qr2term") (vers "0.1.8") (deps (list (crate-dep (name "crossterm") (req "^0.16") (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (kind 0)))) (hash "0r9i5717lv419vqv0gwd9cpz55qgva688yc6r2xp5s1bxz7s41vx")))

(define-public crate-qr2term-0.2 (crate (name "qr2term") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.17") (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (kind 0)))) (hash "1mnn4l8z9bmnvbgh9ky6v06nmx7kcmb1mj3ndxzijqsljq47xj8i")))

(define-public crate-qr2term-0.2 (crate (name "qr2term") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.17") (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "1a3zxphycjic9w0zvj746mi2d6zga58jpsv9r0b97iz5p37x83c4")))

(define-public crate-qr2term-0.2 (crate (name "qr2term") (vers "0.2.2") (deps (list (crate-dep (name "crossterm") (req "^0.19") (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "0d3wkcngslixdwlag2m4spjppcfi19kgimhh5p2m6hadhy74fd4n")))

(define-public crate-qr2term-0.2 (crate (name "qr2term") (vers "0.2.3") (deps (list (crate-dep (name "crossterm") (req "^0.23") (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "0zaddmw5mkiaz02caz5f2s1pwhbwcndvrg9f9nhf8xv4b5nxd7hx")))

(define-public crate-qr2term-0.3 (crate (name "qr2term") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.23") (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "1sdldrwq2wicl1b97g4fs9fh3ix4n1fa58fyw07d68bqmy7vn2nw")))

(define-public crate-qr2term-0.3 (crate (name "qr2term") (vers "0.3.1") (deps (list (crate-dep (name "crossterm") (req "^0.25") (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std"))) (kind 2)))) (hash "0k0fd77ipyfy961089c4sphiz6pyr2vi5nbs4h24nwfdnmviwajc") (rust-version "1.56")))

(define-public crate-qr2text-0.1 (crate (name "qr2text") (vers "0.1.0") (deps (list (crate-dep (name "qrcode") (req "^0.4") (kind 0)))) (hash "0w0xhhns3gpqk7lzk6m6kcm7n5fsywhdy58qag3v9ir9kgskgj77")))

