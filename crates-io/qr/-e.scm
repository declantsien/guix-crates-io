(define-module (crates-io qr -e) #:use-module (crates-io))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vp4l87fs8j68nc14k2xaw2q9nlmj7mc031q3yclb9ynf5j4xj6y") (yanked #t)))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1gm6wi6maxcfzh030w8xj0ifj9l5ldxfh00yd92grvximrgn2zji")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "18z6qf3v3p8ml7iqr0m1nxziqa1w4r2r2n9lnp8r3sh5zahyifhs")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0v99cpiy50mqk7ks93gqx8afaa1dzql8fz0a2aysnnkb9fpbclv8")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "155r3bcsrv999ymj5k6yb7h1kn0h6d2k6ky0rcbs7fbq3m3dfs0a")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00agc7m76kj0iagrl9d2nald4i5x2sz8hp2fws23ahlifn6724s9")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0c469cnmsv2grx19fznk5v2gl8d3qvv539f365mhrv2rbj04fgzn")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.7") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1b11p3qh3mcq76ji5fv0bbilqxf5n2xrcjx9q2lg3mfgh79bi69p")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.8") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1a7n7hm8lkkc5hrl3azqaajq2zyphyc6821j2zksyvbyac0x5qq1")))

(define-public crate-qr-encode-0.1 (crate (name "qr-encode") (vers "0.1.9") (deps (list (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0ixdgq0yjdpcbwvi886r3h6b8319swz2p558axw44hwnrjs9kf7v")))

