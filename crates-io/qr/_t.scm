(define-module (crates-io qr _t) #:use-module (crates-io))

(define-public crate-qr_terminal-0.0.1 (crate (name "qr_terminal") (vers "0.0.1") (deps (list (crate-dep (name "qrcode") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "terminal_graphics") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1b147wkflbycy4y9p2dmxs83jwjlm96x3709n859jrzpczi7xnl8")))

