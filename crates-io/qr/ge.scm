(define-module (crates-io qr ge) #:use-module (crates-io))

(define-public crate-qrgen-0.1 (crate (name "qrgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "qrcodegen") (req "^1") (default-features #t) (kind 0)))) (hash "055xhmwyc8s19h1ad296g4ma6fn6jz0qzslmy2ml1h4dk936r0m1")))

