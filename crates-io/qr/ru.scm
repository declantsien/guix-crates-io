(define-module (crates-io qr ru) #:use-module (crates-io))

(define-public crate-qrru-0.1 (crate (name "qrru") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rqrr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1zv4f3m6rwnbxpl476l853j7nsqfklzd6pv8dgydysr561pfghal")))

(define-public crate-qrru-0.1 (crate (name "qrru") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rqrr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1p5kkpkjjkai2h81p125yfn1blf7s8v9ffqlbjqzg54g8lpq8ii4")))

