(define-module (crates-io qr -a) #:use-module (crates-io))

(define-public crate-qr-api-0.1 (crate (name "qr-api") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.7") (features (quote ("json"))) (kind 0)) (crate-dep (name "rqrr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "03hd7sxin2c3ycazchnbq0rhb71813w9l0g6gf0qakrywirlwhn5")))

