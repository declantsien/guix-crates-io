(define-module (crates-io qr #{2c}#) #:use-module (crates-io))

(define-public crate-qr2cairo-0.1 (crate (name "qr2cairo") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req ">= 0.1.2, < 1.0.0") (kind 0)) (crate-dep (name "cairo-rs") (req ">= 0.1.2, < 1.0.0") (features (quote ("pdf"))) (default-features #t) (kind 2)) (crate-dep (name "qrcode") (req ">= 0.4.0, < 1.0.0") (kind 0)))) (hash "02jkn8iq13fxf508hxgjzz5l5hd9anxllhhin6km92d7bcw3pbxx")))

