(define-module (crates-io qr _f) #:use-module (crates-io))

(define-public crate-qr_from_str-0.1 (crate (name "qr_from_str") (vers "0.1.0") (deps (list (crate-dep (name "qrcode") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0ncriv71jq7gfklyr9bdh2nlv2ggscrwnfmfigk1823w1c20ncgc")))

