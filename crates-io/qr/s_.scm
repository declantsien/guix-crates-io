(define-module (crates-io qr s_) #:use-module (crates-io))

(define-public crate-qrs_detector-0.1 (crate (name "qrs_detector") (vers "0.1.0") (deps (list (crate-dep (name "micromath") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sliding_window") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13z8v43smsqrfbrid4draq4imgvbjv12wzifsqg0hn1g5543khyn")))

(define-public crate-qrs_detector-0.1 (crate (name "qrs_detector") (vers "0.1.1") (deps (list (crate-dep (name "micromath") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sliding_window") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pjhc95lvjj12gx8i8bgdpzrx2kksss7k5fr9k1700mfss11f5af")))

(define-public crate-qrs_detector-0.1 (crate (name "qrs_detector") (vers "0.1.2") (deps (list (crate-dep (name "if_chain") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "micromath") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sliding_window") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00yc6bqrhacfk7ar3b63bm9qjzkwrf5zdy9l4a6mg7kzqa518r5l")))

(define-public crate-qrs_detector-0.2 (crate (name "qrs_detector") (vers "0.2.0") (deps (list (crate-dep (name "if_chain") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "micromath") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sliding_window") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06pdzh5lw2wbp2hr4w3v54nzs2nckdrdapkc2y0l38fclyy6h8gn")))

