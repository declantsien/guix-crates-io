(define-module (crates-io qr qr) #:use-module (crates-io))

(define-public crate-qrqrpar-0.1 (crate (name "qrqrpar") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.36.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0pjf6i0a66v11h90kjn04g3jrky0jy72kzw77x96llbc41hnk57w")))

(define-public crate-qrqrpar-0.1 (crate (name "qrqrpar") (vers "0.1.2") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.36.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0mkw8znqq3cvd5fd7jlmjrakgz67nlam8hia43lq9fmgi7c4vh0x")))

(define-public crate-qrqrpar-0.1 (crate (name "qrqrpar") (vers "0.1.3") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.36.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "051kw1fsj78hxm28yxapldmfmhx084ja8mk4zmickmgk0y7x1igs")))

(define-public crate-qrqrpar-0.1 (crate (name "qrqrpar") (vers "0.1.4") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.36.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "027syc5hi66afdyybjs43q9cvm9l1lr59i4z8dy4ayf90kgv0iyh")))

(define-public crate-qrqrpar-0.1 (crate (name "qrqrpar") (vers "0.1.5") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.36.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "1zplgrlhva259zsjbafsn9jd0yb266ps9jpyl7k476fk62d68a1g")))

