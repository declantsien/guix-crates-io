(define-module (crates-io qr #{-i}#) #:use-module (crates-io))

(define-public crate-qr-image-core-0.2 (crate (name "qr-image-core") (vers "0.2.0") (deps (list (crate-dep (name "image") (req ">=0.23.0, <0.24.0") (default-features #t) (kind 0)) (crate-dep (name "pix") (req ">=0.13.0, <0.14.0") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req ">=0.12.0, <0.13.0") (default-features #t) (kind 0)))) (hash "0gdnhshcb2mhg58m536wg0jrngsnsfwzxd1wr2qpk30c6k0jc3jp") (features (quote (("default"))))))

(define-public crate-qr-image-core-0.2 (crate (name "qr-image-core") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0wfd26afjfgh19zp1dg0zg3xh3p9cqmzmb1nzdwbly8aw5vvy46h") (features (quote (("default"))))))

