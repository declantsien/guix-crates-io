(define-module (crates-io qr eg) #:use-module (crates-io))

(define-public crate-qregister-0.1 (crate (name "qregister") (vers "0.1.0") (deps (list (crate-dep (name "qdowncast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "13f673bdin1j73n8bd8f105hi4fh0mnfwjcyhgf6izfnbsdvifng")))

(define-public crate-qregister-0.1 (crate (name "qregister") (vers "0.1.1") (deps (list (crate-dep (name "qdowncast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hjkwzk8zlay0q1nxlh41r0gsnas47b07lp2bpp8yxybn6v0mzz4")))

(define-public crate-qregister-0.1 (crate (name "qregister") (vers "0.1.2") (deps (list (crate-dep (name "qdowncast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qbgqrac9v2v0fnpx10j94rrx3la8h9cdmc7lwqv3jykc54kwaii")))

(define-public crate-qregister-0.2 (crate (name "qregister") (vers "0.2.0") (deps (list (crate-dep (name "qdowncast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dyfywgc82c5vbff69j33y76dpf0qhyyxdvkxbz2rhd2si52znhg")))

(define-public crate-qregister-0.2 (crate (name "qregister") (vers "0.2.1") (deps (list (crate-dep (name "qdowncast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bk6kp84pxh98d0ygsw987zw9kqq9brjs3xiicv6lk4vx1jq6zfq")))

(define-public crate-qregister-0.2 (crate (name "qregister") (vers "0.2.2") (deps (list (crate-dep (name "qdowncast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qindex_multi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0y20k7y80b681b5dbw7rf0w8dx252678lrsc7lif280p9d531ig2")))

