(define-module (crates-io qr po) #:use-module (crates-io))

(define-public crate-qrport-0.1 (crate (name "qrport") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "qrcode-generator") (req "^4.1.7") (default-features #t) (kind 0)))) (hash "1yxbz7pnhs6m501vdrd2aa77frq42ij06fx2w3klvyc73xvwcdpx")))

