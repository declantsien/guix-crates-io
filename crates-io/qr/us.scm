(define-module (crates-io qr us) #:use-module (crates-io))

(define-public crate-qrus-0.1 (crate (name "qrus") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (features (quote ("rgb"))) (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "qrcode-png") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rqrr") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "16cqv777h99jbnkghaakn8whdp3gs3z51iik2d58i3hdcchf1q3i")))

(define-public crate-qrust-0.1 (crate (name "qrust") (vers "0.1.0") (hash "0qw8qvscx1xhm1jwz6ah83xpjksl9kjrnwrm7g39hdycwr0f4yq2")))

(define-public crate-qrusthy-0.1 (crate (name "qrusthy") (vers "0.1.0") (deps (list (crate-dep (name "fitsio") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1wn8abs0x7d4848fqh2l94g8fwjwvwglkxd9qxy54zhjza1nmni6")))

