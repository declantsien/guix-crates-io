(define-module (crates-io qr en) #:use-module (crates-io))

(define-public crate-qrencode-0.14 (crate (name "qrencode") (vers "0.14.0") (deps (list (crate-dep (name "checked_int_cast") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (optional #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 2)))) (hash "1hgp0lchnp3zx79j3799nm445rvqg7x62x2x7926ky22lqhv23d6") (features (quote (("svg") ("default" "image" "svg") ("bench"))))))

