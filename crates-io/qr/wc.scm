(define-module (crates-io qr wc) #:use-module (crates-io))

(define-public crate-qrwcell-0.1 (crate (name "qrwcell") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "05pwzn9q9n3d3wqwh8hx2vkz907svqpvnzimghvs56ix65ss3a7h")))

(define-public crate-qrwcell-0.2 (crate (name "qrwcell") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "15hlgq4ngzv1ysyd2bfnx2rzhzsnkhpv0pdz1klfnbbbmxf4p0h7")))

