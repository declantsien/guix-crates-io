(define-module (crates-io qr _c) #:use-module (crates-io))

(define-public crate-qr_cli-0.1 (crate (name "qr_cli") (vers "0.1.0") (deps (list (crate-dep (name "checked_int_cast") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rqrr") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1vm35wp4rp53xdv5bl2fm7rw5a0sgq8n4w20dgkzla5f2i2dzwmh")))

(define-public crate-qr_code-0.1 (crate (name "qr_code") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (optional #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 2)))) (hash "1jd2qihsiv510r0y83hzsanr9kqcq7ngirk6fsr3h4mla6vaf53l") (features (quote (("svg") ("default" "image" "svg") ("bench"))))))

(define-public crate-qr_code-0.2 (crate (name "qr_code") (vers "0.2.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.9.0") (optional #t) (kind 0)))) (hash "0b0c30l7mq00anfmvwrvwvb2wfkwpinry97a2wskmmj5kc14z2rb") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.3 (crate (name "qr_code") (vers "0.3.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.9.0") (optional #t) (kind 0)))) (hash "110ik2pmi7na0rmfzmw3md3szii7pd7pdn5j7a2g61885l31gdsb") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.4 (crate (name "qr_code") (vers "0.4.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.9.0") (optional #t) (kind 0)))) (hash "1557g4dijbf5bl3wxvimnk17b81czm0pfdxlhxhvr3q978f5wlsx") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.5 (crate (name "qr_code") (vers "0.5.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.9.0") (optional #t) (kind 0)))) (hash "14awj8bcgyzx507px48zx15dplmcra7kkcfw4aj3v2vk41h9p7f6") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.6 (crate (name "qr_code") (vers "0.6.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.12.0") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "01s1w035w45fjfs1ikk20awdb1x013h5z805ydb37abv245r0ljh") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.15 (crate (name "qr_code") (vers "0.15.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.12.0") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0vh5sg8h3d7br9j8agzdwk2vn0mlwx8838jb3pw2z6301pz6vppf") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.16 (crate (name "qr_code") (vers "0.16.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.12.0") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1f1qw9rl9jnlx9px0lz8zn47q3ln2qf4ys1nykpqzg4si12amsyz") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.17 (crate (name "qr_code") (vers "0.17.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.13.0") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0r68ca91x4a390rphgqmc83p4gkslsi93mnbpkfkjhk4frgr661n") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.18 (crate (name "qr_code") (vers "0.18.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.15.0") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "02cr7lvfgh6s8kqc4aq477mvy7xd8bfdi5q1q9ihvm35ym2l548j") (features (quote (("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-0.19 (crate (name "qr_code") (vers "0.19.0") (deps (list (crate-dep (name "bmp-monochrome") (req "^0.15.0") (optional #t) (kind 0)) (crate-dep (name "g2p") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "10kyfx5x3znc2znmafyrggv2sr8qzw5lxdi8w1gyap2mis0z9bpq") (features (quote (("decode" "g2p") ("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-1 (crate (name "qr_code") (vers "1.0.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "bmp-monochrome") (req "^1.0.0") (optional #t) (kind 0)) (crate-dep (name "g2p") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "12qzf715jjf74w6gk2y5yb5j7i1ny4b2ki8fmf4zajcbkkzi4468") (features (quote (("fuzz" "arbitrary") ("decode" "g2p") ("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-1 (crate (name "qr_code") (vers "1.1.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "bmp-monochrome") (req "^1.0.0") (optional #t) (kind 0)) (crate-dep (name "g2p") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "09krdiw69x63zs655acakr709bbz3hx56nhw4r4s8lm1gp6zn82m") (features (quote (("fuzz" "arbitrary") ("decode" "g2p") ("bmp" "bmp-monochrome"))))))

(define-public crate-qr_code-2 (crate (name "qr_code") (vers "2.0.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "bmp-monochrome") (req "^1.0.0") (optional #t) (kind 0)) (crate-dep (name "g2p") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "004aww7807b9k32q3lbjijfqsacsrf5mpcqjnnng7ajzmr55dlj3") (features (quote (("fuzz" "arbitrary") ("decode" "g2p") ("bmp" "bmp-monochrome"))))))

