(define-module (crates-io qr -m) #:use-module (crates-io))

(define-public crate-qr-maker-0.1 (crate (name "qr-maker") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nymy7js5wqbajgf0zlvp5nyaicy8706qpz105wbvyrlgfx98vj6")))

(define-public crate-qr-maker-0.1 (crate (name "qr-maker") (vers "0.1.1") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.4") (default-features #t) (kind 0)))) (hash "1i4a63m2b4f365jrv6w85lkqvsxz7hjjkjj2hhj1gswd85g0kn8s")))

