(define-module (crates-io qr wl) #:use-module (crates-io))

(define-public crate-qrwlock-0.1 (crate (name "qrwlock") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.4") (features (quote ("ticket_mutex"))) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1xr270y9ylmiy6chg5b4cz2p2wir2hiyx92bvip0cyfa5pq5ibzs")))

(define-public crate-qrwlock-0.2 (crate (name "qrwlock") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.4") (features (quote ("ticket_mutex" "spin_mutex"))) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1gnc3wnb7xn74s3zhkqsq2pijxk49c73bsb5nh4m3wc65grw2ybl")))

