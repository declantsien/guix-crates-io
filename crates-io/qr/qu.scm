(define-module (crates-io qr qu) #:use-module (crates-io))

(define-public crate-qrquick-0.1 (crate (name "qrquick") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "qrcodegen") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0nvnwgsglzp22yzm7z48fr6vsmpk25ppz7ja70pk1ls8w0zb2623")))

