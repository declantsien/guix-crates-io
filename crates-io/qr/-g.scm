(define-module (crates-io qr -g) #:use-module (crates-io))

(define-public crate-qr-gen-1 (crate (name "qr-gen") (vers "1.0.0") (deps (list (crate-dep (name "qrcodegen") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "usvg") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1wrqnjj6clq49b8bayhvz1bk17rygkxfm9kn23bd38wmwknqzdsx")))

(define-public crate-qr-gen-1 (crate (name "qr-gen") (vers "1.0.1") (deps (list (crate-dep (name "qrcodegen") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "usvg") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0s6pflsrw9m6xz4hhbny3qnnls5y766b86rsi2p33vabwpc2flgv")))

(define-public crate-qr-generator-0.1 (crate (name "qr-generator") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1jkylqjqkj4fb4v1ibps8kzmjgsarbbfkj91a5d29m14wyf3dmcr")))

(define-public crate-qr-generator-0.1 (crate (name "qr-generator") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1z273ij5yvjw49k5clr2scd7y921mc8r31134ay8gxffpvgz6pys")))

(define-public crate-qr-generator-0.1 (crate (name "qr-generator") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "07ngi3s9c0knsq7w5n2qf9nqgil1ir1b28hwpxbi8pvclk1kc3yw")))

(define-public crate-qr-generator-0.1 (crate (name "qr-generator") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "1isn0v4nghlwbcn9345h91h8yqzs5f7ld52yjzx8fsvjvgks539g")))

(define-public crate-qr-generator-cli-0.1 (crate (name "qr-generator-cli") (vers "0.1.0") (deps (list (crate-dep (name "qrcode") (req "^0.12.0") (kind 0)))) (hash "1bh0gb0v8yr6v1bixdjzqfxh1lfcpff8j76w46r50jid1gfaszbc")))

