(define-module (crates-io ja me) #:use-module (crates-io))

(define-public crate-jamendo-0.1 (crate (name "jamendo") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (features (quote ("serde-serialization"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s8a4r0hl2da9dfiv6yizjdmvcmami9dd2z08gc4jwd1viwg8qa8")))

(define-public crate-jamet-0.0.0 (crate (name "jamet") (vers "0.0.0") (hash "1srgc1hm5232fwvhxy35qfpq689y7zbfydnfmyry67jyqvlwr7d2")))

