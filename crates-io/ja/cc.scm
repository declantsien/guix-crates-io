(define-module (crates-io ja cc) #:use-module (crates-io))

(define-public crate-jacc-0.1 (crate (name "jacc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ii89b6b1j6psybf7vhvzxrhh6ad456a7wp7k4iws51c5xw7f9pi")))

