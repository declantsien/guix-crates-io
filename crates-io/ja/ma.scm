(define-module (crates-io ja ma) #:use-module (crates-io))

(define-public crate-jamal-0.1 (crate (name "jamal") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kp2bqs9j2rr61q49d7798hwk9xmf3srvsw8az9q2kbglig8z9xk")))

