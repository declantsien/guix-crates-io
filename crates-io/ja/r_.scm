(define-module (crates-io ja r_) #:use-module (crates-io))

(define-public crate-jar_conflict_detecter-0.0.1 (crate (name "jar_conflict_detecter") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0npf4gvfwkqi3ac33prmbwv1n5ivsx3nqymsnxivw9l9ka3v7zzm") (yanked #t)))

(define-public crate-jar_conflict_detecter-0.0.2 (crate (name "jar_conflict_detecter") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "08gs4jc01996zz391nyjga39a9bkw4m13f8hnr20rxym9q8annfn") (yanked #t)))

(define-public crate-jar_conflict_detector-0.0.1 (crate (name "jar_conflict_detector") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0jhh8n7vd0ypd94ip6gj1rm1g7zqczj1bmz440c726kadj2khm2h")))

