(define-module (crates-io ja rm) #:use-module (crates-io))

(define-public crate-jarm_rs-0.1 (crate (name "jarm_rs") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0wigsl8vikyd0lgryp91g0hfy9a28gvr9lxw2ywl7aqxl6zvfi0r") (yanked #t)))

(define-public crate-jarm_rs-0.1 (crate (name "jarm_rs") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0wc26wx1msbz8219f45anzyrrf7q24jdxcsgc91w4wjhljafrjig") (yanked #t)))

(define-public crate-jarm_rs-0.1 (crate (name "jarm_rs") (vers "0.1.2") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1la80b187pgjizlnkyk4qb8xppbwz7pwsdxp4hqdljvbmlz8kk6a") (yanked #t)))

(define-public crate-jarm_rs-0.1 (crate (name "jarm_rs") (vers "0.1.3") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "16kgg69lqr782s37b114fm18yivqqx1j2670rjnqf670vadyf06i") (yanked #t)))

(define-public crate-jarm_rs-0.1 (crate (name "jarm_rs") (vers "0.1.4") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "09a706r3hwb2k6lv3vchlc766l9j9lyw7wqa8qrh47m63mdyq56y") (yanked #t)))

(define-public crate-jarm_rs-0.1 (crate (name "jarm_rs") (vers "0.1.5") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "06gzslwjd9dj8f8ik8xw7ixblgih9f7sqs9n7v7cfwm5srwi3x48") (yanked #t)))

(define-public crate-jarm_rs-0.1 (crate (name "jarm_rs") (vers "0.1.6") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1wmd1r4vlhsd29a2xcd7ync4ihgzaw7wqa0l8wm19aqw3iqragd3")))

