(define-module (crates-io ja la) #:use-module (crates-io))

(define-public crate-jalali-0.1 (crate (name "jalali") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d5xpsyazd5w2n5sxjj1xcvigb3cl8q3kdi2gr6vmijmfz67wbsv") (features (quote (("use-shared-jalali"))))))

(define-public crate-jalali-date-0.1 (crate (name "jalali-date") (vers "0.1.0") (hash "03r8azvim9493cn0li3bffzrdfc2r9vgfsg3p2i0jpp6isxyqbwk")))

(define-public crate-jalali-date-0.2 (crate (name "jalali-date") (vers "0.2.0") (hash "01zc7nw2y0wp8b9k9af2ygmzhvhk0k2r6b3hbciw7zzc5jyqkpnl")))

