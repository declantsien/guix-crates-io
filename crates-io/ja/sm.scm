(define-module (crates-io ja sm) #:use-module (crates-io))

(define-public crate-jasm-0.1 (crate (name "jasm") (vers "0.1.0") (hash "1albmvgdzbldlvg86gjzlw1vxahp6l3zq7r046snka8285qc9kmk")))

(define-public crate-jasmine-0.1 (crate (name "jasmine") (vers "0.1.0") (hash "0gr0fjf14lz3lg9qdnh1yida9jrips549n2s321swhp67xyz2bky")))

(define-public crate-jasmine-db-0.1 (crate (name "jasmine-db") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.126") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shuttle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1y2fr78h9w82b8nbzd5svh2vwxsqqc57d1hcg63417yy1r60fxhf") (features (quote (("shuttle") ("mmap" "libc"))))))

