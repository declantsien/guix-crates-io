(define-module (crates-io ja ry) #:use-module (crates-io))

(define-public crate-jarylo-0.1 (crate (name "jarylo") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.32.1") (default-features #t) (kind 0)))) (hash "0rhy9lbbhs9qa23qxfa9311np0q7zf8l96yqq3k9z15jan1180j0") (yanked #t) (rust-version "1.68")))

(define-public crate-jarylo-0.1 (crate (name "jarylo") (vers "0.1.1") (deps (list (crate-dep (name "glium") (req "^0.32.1") (default-features #t) (kind 0)))) (hash "0iy6rzh4zyd5y903xcrsndxzk4fkbxsvy277y094f2j5k9gn6rhk") (yanked #t) (rust-version "1.68")))

(define-public crate-jarylo-0.1 (crate (name "jarylo") (vers "0.1.2") (deps (list (crate-dep (name "glium") (req "^0.32.1") (default-features #t) (kind 0)))) (hash "1za9789flvhxbysclyk1yjc74r8k584gwvh9inlqi2wak75jsqs4") (yanked #t) (rust-version "1.68")))

(define-public crate-jarylo-0.1 (crate (name "jarylo") (vers "0.1.3") (deps (list (crate-dep (name "glium") (req "^0.32.1") (default-features #t) (kind 0)))) (hash "1rh6dx3h47n4bx9224grvvvxg1wcdv8ln34hx9nrb11s1ql154r1") (yanked #t) (rust-version "1.68")))

