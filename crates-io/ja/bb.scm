(define-module (crates-io ja bb) #:use-module (crates-io))

(define-public crate-jabba-0.1 (crate (name "jabba") (vers "0.1.0") (deps (list (crate-dep (name "async-lock") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.59") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1fj8x099rlbs3vsb8cmlw41xl55gq6cz63bl8xcz87m991qj8q7m")))

(define-public crate-jabba-cpc-0.1 (crate (name "jabba-cpc") (vers "0.1.0") (deps (list (crate-dep (name "jabba-lib") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1pza9q1cp8y4cfy6f9cjyrglv9xryahfqwl7z656aw82hvwis0r1")))

(define-public crate-jabba-cpc-0.1 (crate (name "jabba-cpc") (vers "0.1.1") (deps (list (crate-dep (name "jabba-lib") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1r4jircgqvy90qjx5rbwi09skdxgs8vfbad4mwc7064w42aps2qc")))

(define-public crate-jabba-cpc-0.1 (crate (name "jabba-cpc") (vers "0.1.2") (deps (list (crate-dep (name "jabba-lib") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "066kps23qcjilsqjwwh1jgdsk1957ziqalhs9pcl4r9x1rhpsm4q")))

(define-public crate-jabba-ctc-0.1 (crate (name "jabba-ctc") (vers "0.1.0") (deps (list (crate-dep (name "jabba-lib") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "13n8zlcrgldwr8ns9014hkmglr31bhim8861lk93mrvgvz56jhcr")))

(define-public crate-jabba-ctc-0.1 (crate (name "jabba-ctc") (vers "0.1.1") (deps (list (crate-dep (name "jabba-lib") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1cmgr6n9y1arckdbq35i1nclgjvkkqh8lyg6cylg4w8d5fq3aaac")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18ldnkznmk204jfcsyyhh69m7iwih04cpb8fwarr6jhlwymqvf6z")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "077g3qdg2akqjvmgb06izvkbniaxpkmm1d53qzg6bag3hpy9bbbw")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard-win") (req "^4.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 0)))) (hash "0as1drl26r01yiiqnkdn0xf2241akd42cn567l2ncmb05dmdg780")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard-win") (req "^4.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 0)))) (hash "0c346qivrplwkvhrr04a96kralvmsyy8pjp41xnbhvvcpc3cmyp4")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.4") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard-win") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 0)))) (hash "1pmd25pn5lpqgkip7lniakfpq951067blcpki0ikm22mn8fslvab")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.5") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard-win") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 0)))) (hash "1rz3ssfygdnrln3zyl4p5z880gm3kmayb87rfd0mdp32y7740f3p")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.6") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard-win") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 0)))) (hash "1bbfix1dxj8w6dlp7jk2hvv0na5rsm75kad5vwh9qkfhg2yshrln")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.7") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard-win") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 0)))) (hash "1s70hlq2sg8qywkaxkhy0ymrhc9ykvbd8khx9jh2w9wwx3cs3v2r")))

(define-public crate-jabba-lib-0.1 (crate (name "jabba-lib") (vers "0.1.8") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clipboard-win") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "shlex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.5") (default-features #t) (kind 0)))) (hash "1l5xc2vwrv9ln9irqpgdknkr39qfyn5djy3mf87qwvf186213fkl")))

(define-public crate-jabberwock-0.1 (crate (name "jabberwock") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "hatter") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (optional #t) (default-features #t) (kind 0)))) (hash "0sz3fzhrpvls9iyzy0xvff545vakfgf9qs7ipqp026lqq7r4x43a") (features (quote (("transpile") ("templates") ("default" "copy" "markdown" "templates" "toml" "transpile") ("copy")))) (v 2) (features2 (quote (("toml" "dep:toml") ("markdown" "dep:markdown"))))))

