(define-module (crates-io ja rv) #:use-module (crates-io))

(define-public crate-jarvis-0.1 (crate (name "jarvis") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1n8fnzn0k79byjy421gh6lxpnkfhiv5a6bd3pvp1vhmz1fkw5qwd")))

(define-public crate-jarvis-0.2 (crate (name "jarvis") (vers "0.2.0") (deps (list (crate-dep (name "config") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1szin1rk0g8cdirzaarl9fxhms5c3nqq2nvrs0waabx1x0rgi1zm")))

(define-public crate-jarvis-0.3 (crate (name "jarvis") (vers "0.3.0") (deps (list (crate-dep (name "config") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "19yg9wls0dmpyz8c1ab6s7qksyikwbvhqkpfzi4gb799f08lk9bi")))

(define-public crate-jarvis-rust-0.1 (crate (name "jarvis-rust") (vers "0.1.0") (hash "05h5i9slmz87y5y6q5h770z8blj7a8b0gr3sygm6nz1n21pkl8g8")))

