(define-module (crates-io ja ns) #:use-module (crates-io))

(define-public crate-jansson-sys-0.1 (crate (name "jansson-sys") (vers "0.1.0") (deps (list (crate-dep (name "cstr-macro") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0akywspm3vrz24abh7n3ynab919wlpyrqmfqh3mnl927rlmvcwcc")))

