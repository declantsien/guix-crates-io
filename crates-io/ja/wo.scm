(define-module (crates-io ja wo) #:use-module (crates-io))

(define-public crate-jawohl-0.1 (crate (name "jawohl") (vers "0.1.0-dev") (deps (list (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)))) (hash "0hpp8cbxlr8vvjd3vxbcbg6134ipb54bkggycjqmysszmfijqb3l")))

(define-public crate-jawohl-0.1 (crate (name "jawohl") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)))) (hash "0z7ci49qflki333sgas71igfslqpai61qcd5y7y8yydm023ym8hd")))

