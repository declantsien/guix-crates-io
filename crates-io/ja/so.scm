(define-module (crates-io ja so) #:use-module (crates-io))

(define-public crate-jaso-1 (crate (name "jaso") (vers "1.0.0") (deps (list (crate-dep (name "async-recursion") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rlimit") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("sync" "rt" "rt-multi-thread" "parking_lot" "macros" "time" "fs"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1qal6wpqhqdics7qc3w0yrsjkm0mbxkmqmr2rswd4sf0fjqmmc6g")))

(define-public crate-jason-0.1 (crate (name "jason") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-stream") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("tcp" "http1" "client"))) (optional #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("tcp" "http1" "client" "server"))) (kind 2)) (crate-dep (name "hyper-tls") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync" "net" "macros" "io-util"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-tungstenite") (req "^0.13") (features (quote ("connect"))) (optional #t) (kind 0)))) (hash "1sj9f1mqh0p60fgahjdws53lqzfl7raf0pymdnbvs4dh4g2hxagq") (features (quote (("ws" "transport" "tokio-tungstenite") ("transport" "tokio" "async-stream") ("ipc" "transport") ("http-tls" "http" "hyper-tls") ("http" "transport" "hyper"))))))

(define-public crate-jason-0.2 (crate (name "jason") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-stream") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("tcp" "http1" "client"))) (optional #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("tcp" "http1" "client" "server"))) (kind 2)) (crate-dep (name "hyper-tls") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync" "net" "macros" "io-util"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-tungstenite") (req "^0.13") (features (quote ("connect"))) (optional #t) (kind 0)))) (hash "0835c4vmpqsvz8kb58xxi419mdd37zwv80zlk9ay78g363ik2bxi") (features (quote (("ws" "transport" "tokio-tungstenite") ("transport" "tokio" "async-stream") ("ipc" "transport") ("http-tls" "http" "hyper-tls") ("http" "transport" "hyper"))))))

(define-public crate-jason-minigrep-0.1 (crate (name "jason-minigrep") (vers "0.1.0") (hash "1phhm0anw01p13i31wkqvmfrk6jns2xj37n3zx9xvh77kal06qj9")))

(define-public crate-jason-minigrep-0.1 (crate (name "jason-minigrep") (vers "0.1.1") (hash "09b63a6by0dx304nr6nrqwpayxkm3p4hsda57rrs0ha3cln3x2g5")))

(define-public crate-jasondb-0.1 (crate (name "jasondb") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.33") (kind 0)))) (hash "1v8riklf07h624qpjw8r1c9icbss2x2psijvk4l15xv5v4a2c40i") (features (quote (("validation" "serde" "serde_json") ("default" "validation"))))))

(define-public crate-jasondb-0.1 (crate (name "jasondb") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "08fc6832akmnp9kgh0lf95h9q79kayz4x1vm3yzp60zszlqsw45b") (features (quote (("validation" "serde" "serde_json") ("default" "validation"))))))

(define-public crate-jasondb-0.1 (crate (name "jasondb") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "11w709vy70vkh6yfwj7brzzjbk01c996dskbaw3984rx9ml1dibv") (features (quote (("validation" "serde" "serde_json") ("default" "validation"))))))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.0") (deps (list (crate-dep (name "humphrey_json") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1sin7bh5m7lig164pfrrrch6bv3amqdjdf2gdc61v8fbjsydzh1i")))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.1") (deps (list (crate-dep (name "humphrey_json") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "00b1rkgkx0shhk5j05q9axxkskfcc1qmp95mjsdrc24r4snd3ffc")))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.2") (deps (list (crate-dep (name "humphrey_json") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1lw6d66gsa4zal76vqmnaxsc83gwraqzav5yjrc1szxh9lfirp4d")))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.3") (deps (list (crate-dep (name "humphrey_json") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1l18k1rfjnd1cprnml6svmlsyn6xs837xxmjpf50r2sj8asrpv4a")))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.4") (deps (list (crate-dep (name "humphrey_json") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0dqnrxwm4k8yzhij76w3dzy59i9z4xkrakw13yh9qirb0vxcl9kr")))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.5") (deps (list (crate-dep (name "humphrey_json") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0i8ayixnzy70z065pjs161ck9qmmx1gm6ivmsa2jgfbwh1gw6fih")))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.6") (deps (list (crate-dep (name "humphrey_json") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "13wsyyfcjjw6f3yalsb0pf7jyw43szclab68njcwl2bnwb9rm2l9")))

(define-public crate-jasondb-0.2 (crate (name "jasondb") (vers "0.2.7") (deps (list (crate-dep (name "humphrey_json") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "119ccjwp9bp30ha0d0i36il14jn8j53kdnvivn68p5013lqmi9h1")))

(define-public crate-jasonqwu-0.1 (crate (name "jasonqwu") (vers "0.1.0") (hash "0hk2r064vgwpjisayfqlq85kwxirgpld0r6mxda9vshb1jkjjysm")))

(define-public crate-jasonruan-0.1 (crate (name "jasonruan") (vers "0.1.0") (hash "01gfxxnqgf58jcyxaaqqin3755smzzgn8kaaycmbp8hbfyl5ggww")))

