(define-module (crates-io ja wn) #:use-module (crates-io))

(define-public crate-jawn-0.1 (crate (name "jawn") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "057xfimwly51p9kjcskwpsykwv94wa1sx272c2h24x9hwvyxd102")))

(define-public crate-jawn-0.1 (crate (name "jawn") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "184b8qzi7q27wvqwl0395wiydqyl4zh7cgvfysnxdp6451msbyaj")))

