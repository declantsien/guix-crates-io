(define-module (crates-io ja ff) #:use-module (crates-io))

(define-public crate-jaffi-0.1 (crate (name "jaffi") (vers "0.1.0") (hash "1ralbp87zqwqiqd33gzaql0aszph3264vhl2aid5k10q8sh3ncaj") (yanked #t)))

(define-public crate-jaffi-0.2 (crate (name "jaffi") (vers "0.2.0") (deps (list (crate-dep (name "cafebabe") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "enum-as-inner") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "jaffi_support") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1445w5hb594ni42ys6301g7ci414r7zzwbzpmsa3z7j2wyw2xh9r")))

(define-public crate-jaffi_support-0.2 (crate (name "jaffi_support") (vers "0.2.0") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "17lb4vv61xyy0ka7mq6fbb1cw62pkmfd511x7l043689l0mvx11z")))

