(define-module (crates-io ja de) #:use-module (crates-io))

(define-public crate-jade-0.0.1 (crate (name "jade") (vers "0.0.1-pre") (hash "1hv60chr1bfbgh9d2y3zsfhs5i9qjy86h4dcmp4lamsyi6862f70")))

(define-public crate-jaded-0.1 (crate (name "jaded") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0yl8mimjpy4mk3bfh2z0wa935mad5ypjc560r10rcddic6yb5da6")))

(define-public crate-jaded-0.1 (crate (name "jaded") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0x9rayk0c9xyvjwpipnys15w321b8yfz8p6a922iw1pdmixbl6pd")))

(define-public crate-jaded-0.2 (crate (name "jaded") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1hddpygdp50wm0zspdgcnaj6ral3a36s0nnjbincmqw0ay0zz4gv")))

(define-public crate-jaded-0.3 (crate (name "jaded") (vers "0.3.0") (deps (list (crate-dep (name "jaded-derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0xs4pcs71zycsn0ahrzjr8c22nvfy7k8xwy5n31yyzh1m5281smx") (features (quote (("renaming" "jaded-derive/renaming") ("derive" "jaded-derive"))))))

(define-public crate-jaded-0.4 (crate (name "jaded") (vers "0.4.0") (deps (list (crate-dep (name "jaded-derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1zwa9ag9rcbz988shhwjprxlsm6x5k9wmgcb5fxwpxgrm21gn5ym") (features (quote (("renaming" "jaded-derive/renaming") ("derive" "jaded-derive"))))))

(define-public crate-jaded-derive-0.1 (crate (name "jaded-derive") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "07qn01w398k2bqbmkadbsi29zd25515f0ry5sha3j0fi5bn116l4") (features (quote (("renaming" "convert_case"))))))

(define-public crate-jaded-derive-0.2 (crate (name "jaded-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "005rhrbl4wl39x5ylbbbrnv3n8z5s54bfn3hjz30dam3ih8y3b01") (features (quote (("renaming"))))))

(define-public crate-jadep-0.1 (crate (name "jadep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l52f3isyyaa8zccv8kpyqwjqzsj3gxxbmdlq4avzff36qycak7p")))

(define-public crate-jadesrandomutil-0.1 (crate (name "jadesrandomutil") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14rhlrcry1l57ljhw9cq3080b8dx4gvf9ywhwh22gamhf4h2wkk8")))

