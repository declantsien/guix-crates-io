(define-module (crates-io ja rs) #:use-module (crates-io))

(define-public crate-jars-0.1 (crate (name "jars") (vers "0.1.0") (deps (list (crate-dep (name "zip") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "01qsy75kcv0ww5ryxfsn94p0jl290y8mkbfclz99ji7zsj1sk59g")))

(define-public crate-jars-0.1 (crate (name "jars") (vers "0.1.1") (deps (list (crate-dep (name "zip") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "019rgdq944cjvf8y69ar5x9v5jih72nq7yr0dp6rlmiw1gzidlfr")))

(define-public crate-jarsgrep-0.1 (crate (name "jarsgrep") (vers "0.1.0") (hash "17y5cnhy31fcfhsyra997ssabph8r1b9g2v64j5m6hz1yqlcijy9")))

