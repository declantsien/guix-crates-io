(define-module (crates-io ja mp) #:use-module (crates-io))

(define-public crate-jampack-1 (crate (name "jampack") (vers "1.0.0") (deps (list (crate-dep (name "gloo-net") (req "^0.2.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "image") (req "^0.24.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.2.5") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "sha1") (req "^0.10.5") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "1zdkh3r2l7xj8r97fq4fbmjs78g02rq66g84m7pcbvmarm5k7rb8") (features (quote (("default"))))))

(define-public crate-jampack-0.1 (crate (name "jampack") (vers "0.1.0") (hash "0mvf77274yqr8x6nh7sdma0vaqm31m5zxns9sjqvaw4vddqlmrqi")))

(define-public crate-jampack-1 (crate (name "jampack") (vers "1.0.1") (hash "0xrcdxsa7apjlg4nmpn99n3s6snjxljfy3mga5dd6kc3k23rrk0l")))

