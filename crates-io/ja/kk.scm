(define-module (crates-io ja kk) #:use-module (crates-io))

(define-public crate-jakkunight-ali-0.0.1 (crate (name "jakkunight-ali") (vers "0.0.1") (hash "0gc5n56zlx9r3ymmhv82hnnsfzg5v1hg0r1k1sxwyvgpv8bhw5mi")))

(define-public crate-jakkunight-ali-0.0.2 (crate (name "jakkunight-ali") (vers "0.0.2") (hash "0vddkh6snhwj8hz2p2hx3ka18hnqd8ar06lc95209f68a7x58wnq")))

(define-public crate-jakkunight-ali-0.0.3 (crate (name "jakkunight-ali") (vers "0.0.3") (hash "1vyc4mbbjkvimngw9wpnmnhny298iq70aizq2mfb3d3w70wbf7sl")))

(define-public crate-jakkunight-ali-0.0.4 (crate (name "jakkunight-ali") (vers "0.0.4") (hash "18rgaydjslj4nm6rw76d99g3l0qbcbl6jdzacqqg2yl15nqd87c6")))

