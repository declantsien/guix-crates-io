(define-module (crates-io ja rg) #:use-module (crates-io))

(define-public crate-jarg-0.1 (crate (name "jarg") (vers "0.1.1") (deps (list (crate-dep (name "aerr") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "sonic-rs") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "01s39kgj3mqlax1dah8xyjf1g02bf8qkcwyysfch9lkdswpywfbs")))

(define-public crate-jarg-0.1 (crate (name "jarg") (vers "0.1.2") (deps (list (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "sonic-rs") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0x57c47hry3jyc0sj91x4fgb8wmf156nv262dhcrn90diwmviqqf")))

(define-public crate-jarg-0.1 (crate (name "jarg") (vers "0.1.3") (deps (list (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "sonic-rs") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1z0chvc98fa3rrjpyy5pxvf3bbqwk5s79bp62pmn2qjyibcj6czb")))

(define-public crate-jarg-0.1 (crate (name "jarg") (vers "0.1.4") (deps (list (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "sonic-rs") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1qmgvjglspz6i0szhnfnp6rfmacmy2p8jp97vgwcz2vpvli5m3kc")))

(define-public crate-jarg-0.1 (crate (name "jarg") (vers "0.1.5") (deps (list (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "sonic-rs") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1dippw5nvnw4vgg7frgk5855m2amichaxsm805hk2ry6c72b737c")))

(define-public crate-jarg-0.1 (crate (name "jarg") (vers "0.1.6") (deps (list (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "sonic-rs") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "19i6dnavr6w6x2pc3iw0sgcbxqvzjqv7s4il7bll654vz85wxwlg")))

(define-public crate-jarg-0.1 (crate (name "jarg") (vers "0.1.7") (deps (list (crate-dep (name "axum") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "sonic-rs") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1zz62w87q1mhbm4867cxng5g6bqkbdbmmnszmli9vpm0wnr6g74p")))

(define-public crate-jargon-0.2 (crate (name "jargon") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 1)))) (hash "046408k32i4qxx763yn8ynm1parg23mghdmq9lds0yq0z9rchkzc")))

(define-public crate-jargon-0.2 (crate (name "jargon") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 1)))) (hash "0gsvlavzxqdlzisp5pn3gcla8y1kkyypihz61ya9xmzj1nfw9k7r")))

(define-public crate-jargon-0.2 (crate (name "jargon") (vers "0.2.3") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 1)))) (hash "12p9x03p4m3h09fxwy4j52dlknpz5pa6grkfjdqy4gmpa6djx19s")))

(define-public crate-jargon-0.2 (crate (name "jargon") (vers "0.2.4") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 1)))) (hash "0x32b0p63cz1z8c1bql005gycb8xrvf4bm2zg7biblcypr095qaj")))

(define-public crate-jargon-0.2 (crate (name "jargon") (vers "0.2.5") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 1)))) (hash "13abbhn527z3fc1vyq93agvnvxlrk5g87pz3bmpps8f9mmqkynx1")))

(define-public crate-jargon-0.3 (crate (name "jargon") (vers "0.3.0") (deps (list (crate-dep (name "argh") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0dvz6rxcdvq631kyfmmnjs0dj8910y1lhldy11dzpn7s2n0z6ysj")))

(define-public crate-jargon-0.3 (crate (name "jargon") (vers "0.3.1") (deps (list (crate-dep (name "argh") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0y907np7dqxfc4p5g5xrhj8n03ipq4yafv5b8hgi5qmbs663ghzi")))

(define-public crate-jargon-0.3 (crate (name "jargon") (vers "0.3.2") (deps (list (crate-dep (name "argh") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0c3qky2yrmviqrj46v7rdq227vxpzyhiy7l6nyj75g0lv1fc0v8l")))

(define-public crate-jargon-0.3 (crate (name "jargon") (vers "0.3.3") (deps (list (crate-dep (name "argh") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "00hbsa9d753ns7nhjj2v3fd94mjhxxg67c1i1jhng802lk5r4dl6")))

(define-public crate-jargon-0.3 (crate (name "jargon") (vers "0.3.4") (deps (list (crate-dep (name "argh") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0b8zjlzckci3ya5aknha83mn6w1vyn8a43lnacc2kskvv1q8ax8j")))

(define-public crate-jargon-0.3 (crate (name "jargon") (vers "0.3.5") (deps (list (crate-dep (name "argh") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1fq2blxmmlckx1sqqyqnlzlmnz0y2adjhamz85hgcx2ws1mm19pg")))

(define-public crate-jargon-args-0.1 (crate (name "jargon-args") (vers "0.1.0") (hash "0vjms4hzff6q2mhbh3c5lm75jblm8lf2xv51hdr2akx2rhn81455")))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.0") (hash "0798xk3kxgvqm1xhzpkazf23dnbmlwabh419lmp6cwfmqazgjg4x")))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.1") (hash "1fr30gr9lghjllbhdxyn42pqszkmz4pd9ng39qm61fxd5npa960w")))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.2") (hash "03ikyy5wz936nm8ar697ryq74cg1sicv8p53p7g8bdjimbdl179x") (features (quote (("no_mut"))))))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.3") (hash "0hc3kq7n7rkh9bhdpkfwclzaakngnihxhq6aln021px084sgj8pq") (features (quote (("no_mut"))))))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.4") (hash "1bdr9gl8m1ivrl3r0h3clfjlpn53jva93y5a66sihlwxg1syi0v6") (features (quote (("no_mut"))))))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.5") (hash "19w8yv3rfdzq3r8z9i69vd3dzwnjays8a219s3kv7jqr50ks9qg1") (features (quote (("no_mut"))))))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.6") (hash "125m0hk4r1m2m6cbhdcw2f11nmm9d0qsfpvw7j05a85zkqhxhfnv") (features (quote (("no_mut")))) (yanked #t)))

(define-public crate-jargon-args-0.2 (crate (name "jargon-args") (vers "0.2.7") (hash "17l4nacrj8gvi0kskiw06dpdxslg7j0b2czbrb1b17z229bqxlxr") (features (quote (("no_mut"))))))

(define-public crate-jargoncli-0.1 (crate (name "jargoncli") (vers "0.1.0") (hash "0ni4n5kz4d3b6bryvbbn3bmp4zd9zfim7cva9aiwlmgkcgik5i4x")))

