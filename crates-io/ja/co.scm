(define-module (crates-io ja co) #:use-module (crates-io))

(define-public crate-jacob-0.1 (crate (name "jacob") (vers "0.1.0") (deps (list (crate-dep (name "bitreader") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1kj4cp1hm85lliiqz5myw5r0qfva0lf7x3bk9cm7gq5zas5fai71")))

(define-public crate-jacob-0.1 (crate (name "jacob") (vers "0.1.1") (deps (list (crate-dep (name "bitreader") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0gxpgfxdnl1cz66y4z18fc4wlaq2ysyiryzzh7a61715vl65qw54")))

(define-public crate-jacob-0.2 (crate (name "jacob") (vers "0.2.0") (deps (list (crate-dep (name "bitreader") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "13q07yi0b00iwkz5sb3pqx0ry0a84xp3d0kndkxkq3kk302h649b")))

(define-public crate-jacopone-0.1 (crate (name "jacopone") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1swk7v23w91g46zx5d4njhlpiwk7r670j3c1b0hi0gna3aiwxisq")))

(define-public crate-jacopone-0.1 (crate (name "jacopone") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "027f5jls5z3mr09gpd7bcjq3s0ivhhb6y1al40prmlhm3mcm4nnq")))

(define-public crate-jacopone-0.2 (crate (name "jacopone") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "crunchy") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0033kvny629grjbs45ww9wvn4f913kih1yfhlhxdwz3ab8pbifa9")))

