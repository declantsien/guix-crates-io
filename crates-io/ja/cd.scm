(define-module (crates-io ja cd) #:use-module (crates-io))

(define-public crate-jacderida-exp-add-lib-0.1 (crate (name "jacderida-exp-add-lib") (vers "0.1.0") (hash "14hi995yghvla6fl2yzwqpal3hccc913aq2kl50wm4dlkkimf2r9")))

(define-public crate-jacderida-exp-add-lib-0.1 (crate (name "jacderida-exp-add-lib") (vers "0.1.1") (hash "1binzlzjzalwsnqbp2jw0kspi8g9qf3qjz9bd2yazs4452xljndf")))

(define-public crate-jacderida-exp-adder-0.1 (crate (name "jacderida-exp-adder") (vers "0.1.0") (deps (list (crate-dep (name "jacderida-exp-add-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1adxm5n09kkick1jrp86myvrc4nqs76wcj60kb8ry9cy6rdpymcx")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.0") (hash "0i3gqpr2vry493by4bi3ha9z9rzqa94ig8q814n228vfaarh4jlf")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.1") (hash "0c1fl345fvh4ss2xfc2ckl8l8llpsx1p4c0xh3f3hw1wkzfz9c24")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.2") (hash "15pdf4rxbgh186snihhrw1gj925p1n1fsacxpd7hwmq591c2rwb5")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.3") (hash "183cx64jd3h6hi2msxpmfxv03axk43k1p52af6vxniw4fk1rbidl")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.4") (hash "1dma14p9vkyy5x00q2673zx3c7xpc4akvvk38fbd5pckkys4mqqp")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.5") (hash "10fkzsygg6bq5jcc98yhyp29yq218pi98h109b8bv1764wkbslqm")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.6") (hash "1y938gvcnh5vwm5lp2gm72l1alcaj7rrn036aas9xd3yl2syq15l")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.7") (hash "0f4avzcl3iw76ancg4cs9675pbk11n9346iq572h7h7aax9hw8av")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.8") (hash "1vc46h0wmqpqfkzpn2bcgn50x7is3qz0nw0qpzplf8x9q6fr5xhw")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.9") (hash "03pl9m7r5xblv9wbbgsw0c145h9q01bv4vb4qvd3jz1va6pi6ihv")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.10") (hash "18vbg0zk2bzkjj1fv5dbllsxwf4kg6rassdkvwb35jfppd865k42")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.11") (hash "1qk975v5ahgbc3a9khqfjc1nlp2703jyzv3k4xiyzqbxvl1z36zr")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.12") (hash "0gh9d9d4v6d82gswrkx5l4qg4yzph3c85in5bz092jkvgxh7b487")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.13") (hash "0n96v1vwmvdv168xmd4wihxzlf2hbzgzhgbv10bsmihbxsf23dr2")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.14") (hash "0m3qvs64jvks237vf5hlpwqv1m7yskvrqp44fmvffz82rzf469wx")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.15") (hash "0d9ynmk6gczhz4bg32wnasa739fgzyyk2w19z2aqcpfzrsvhv76w")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.16") (hash "0ma6f9f9q11571vkzzrhb3kdvfj164ih549q92m19kga8q9nyyx3")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.17") (hash "0mq68y8millgymya9bagm8vfskcchdmc8zd73lm61jk6m56dp3g1")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.18") (hash "1yv6b485iy199zs3qjjpi5w4y17xwah8vb1z9wk8f99bikgigc0j")))

(define-public crate-jacderida-exp2-add-lib-0.1 (crate (name "jacderida-exp2-add-lib") (vers "0.1.19") (hash "08hclv9cmz8m129mmyn3d9zd2zw71j50dxq0mnbc04c1yvmrz2nr")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.0") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0dkj51sqbixgd95rwn2ngmp2i6szc2pp6ddbkg6w9xnbdmzld9n5")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.1") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0pgbm54sc6k65dp4hkmbln4nlls10awl1ik18a9d9wkjs3b3i1cw")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.2") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1d056gw2na1hglq27dfyplf92r4pz6bmwckrw6669gpxx7q6ncws")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.3") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1vbz8na695x9r01prq8g0am2734lzkjlqlv1j1cfi78w8677nkn4")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.4") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1bkdzmiqqbxzk286c4sxj0rzams8wbrmwfz3fg8yixzny9lqiwv8")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.5") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1jgc671027xc2mal3wwdp1hp3v1g85qkb57k37srfj4xvah30f42")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.6") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0h1gc4rmbay9hhadzfi3yv98150p2diaj1f9xnk2kmkag7vralmx")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.7") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0la0arl1xdcm546a5wdg8xnq7k03x9gjhiaqgfff43az87k13rqb")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.8") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "04q591hgcp3xzxxa749j21jcw0pmydkbcxbvkxdj5zgpyg5cxijl")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.9") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "18k9066n0d7slnh65brfgpbnnn70hji5pazahipaz78c9rprxm0r")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.10") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "045sry1qp84p9s1qqds4vz86hcnlbs87nr777rb6gvmkfpmq6hhw")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.11") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "0xlds6m3s8zddznj18wx9z8a68wz1v3xnvwxhvin3i9d74p0qgi2")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.12") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0l56j5am1djys85wkfcl7a2j9wjkwnsikmyjzgy0jzh1r4a3fq2y")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.13") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "12bxc6nd1ncvp3cdxvhgwlvsyl25dkma6hld0nbxk7zbjxjmlr95")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.14") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0n2bisv3fccj0zbk5ph47zii3pwi8rlidm2gny4xi5ml55pdgria")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.15") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1p1d9qaa518qnib39f9r6b6zgli6pas54lx3bic1fjzrbqy1wwd4")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.16") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1h2bdwh13b8fi2l82nf8g1jybp684z50sgglajy3da6bgqlhsrr3")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.17") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "1f9lpn6jmxbbp8h5b4f198lxkn015zi74m60iy848470z1qdmhmy")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.18") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "062a49bwl1d9i6baiwkx0rrdz7yx8s0whk4x5svrhh5mrkdda43l")))

(define-public crate-jacderida-exp2-adder-0.1 (crate (name "jacderida-exp2-adder") (vers "0.1.19") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "122xa9xswg7sqx95yynizwjlscdm3gv401ka5aab5rpcqbbh798b")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.0") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1b3bg3b7q2wr8v2vp4d6k1j2b4ccsn7m2ad26sg3a4npsjmzwsxg")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.1") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1cflsd1gs20d0lwarmqgckdniqi58kgjjxpqa652jwvbsk2ch93w")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.2") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0a93sl71flk84aai5vh1ffvhzp2pg20jds1hvcl65lchdwkxicnk")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.3") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0hbhlgk0vvplp9xd3h6z47b7hl3jx2w839wfha7sf6242ax45s9k")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.4") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "12hbgck21ivi3i2ghc5azn099d92xnikbcc6xpyhirsdbc8gglk4")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.5") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "0ry1hgz83yxg5mhhz9qr7xs4rzpp8645lvs57is22k4q5798b4ds")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.6") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1dwz9bjw34rr9299dfq2cwbc1w7ipdnvd5lwzpry02pdsyap3sqk")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.7") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "1hpspc3nrafnmrn992vizml5c9nh94sb2qg94al080pb1xll8frg")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.8") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "18n7z8wqrvkcbjj08q3vm3040cmzk8jp9540b30pjp41hf4rp6nc")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.9") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0ry6gym38l5y69x50ldh1sm04ygnkp9c4xzszlfnyz4wyn339r5x")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.10") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "0478nnkcl85jy9wk428x14bdigj7rrdc1dv862j5jlym5sxvb7kb")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.11") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "0bwgyrsi84ppn8amldhqq8s8b37hbkmd672ccdjk0s40r4h38xb5")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.12") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1z8anr83gw76idnjc001ny3jckf8jyyf4rdf5f5x3hms2f8ghwh9")))

(define-public crate-jacderida-exp2-adder2-0.1 (crate (name "jacderida-exp2-adder2") (vers "0.1.13") (deps (list (crate-dep (name "jacderida-exp2-add-lib") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "08mcxi2lzf7lq48k0ghbnmgk6x74axzqyg6fxq9v6v5b0dvadzrf")))

