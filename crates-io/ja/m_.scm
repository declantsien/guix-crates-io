(define-module (crates-io ja m_) #:use-module (crates-io))

(define-public crate-jam_derive-0.0.1 (crate (name "jam_derive") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vwdxvvrkvj1mcfbg4h7aklqmf310b2jsvm4q12w4ynw5ck9aj68")))

(define-public crate-jam_theme_picker-0.1 (crate (name "jam_theme_picker") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.6") (kind 0)) (crate-dep (name "bevy") (req "^0.6") (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "randomorg") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1kwma93w3ys6ydpfm5gqy5fbf13217iji2qxm6py74ivpvpibgx1")))

(define-public crate-jam_theme_picker-0.2 (crate (name "jam_theme_picker") (vers "0.2.0") (deps (list (crate-dep (name "bevy") (req "^0.6") (kind 0)) (crate-dep (name "bevy") (req "^0.6") (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "randomorg") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1hfk6axxqj38fm3s55i1i9mjvahvs5xfi7p9f9aq0c1k4y28fpyv")))

