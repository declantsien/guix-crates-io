(define-module (crates-io ja sc) #:use-module (crates-io))

(define-public crate-jascpal-0.1 (crate (name "jascpal") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.0.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "rgb") (req "^0.8.13") (default-features #t) (kind 0)))) (hash "1r913k3ya2rdi58s7i6796mmkpiyy3rg3nary1a4qikfclcirrr1")))

(define-public crate-jascpal-0.1 (crate (name "jascpal") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.27") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "rgb") (req "^0.8.13") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.13") (default-features #t) (kind 0)))) (hash "0zsyhnjig0mwdccsv7la53sh5spzj1xpvvpksc5qbslfmqd54h49")))

