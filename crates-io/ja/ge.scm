(define-module (crates-io ja ge) #:use-module (crates-io))

(define-public crate-jagermeister-0.1 (crate (name "jagermeister") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6.3") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "surf") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1z4s3gcrsfzra8mr8rs08hr3sxzqpzgdfnh82b05kr34xjhy33rk")))

