(define-module (crates-io ja nd) #:use-module (crates-io))

(define-public crate-jandering_engine-0.1 (crate (name "jandering_engine") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.25.0") (features (quote ("bytemuck"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.8") (features (quote ("png" "jpeg"))) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.0") (features (quote ("Document" "Window" "Element" "Location"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-time") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.18.0") (features (quote ("webgl"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "winit") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "1w8cdsd5rfmwi9mlpz7kwigyg47kjqbdyalhd1kdps04i9pkrq3x") (rust-version "1.65")))

(define-public crate-jandom-0.1 (crate (name "jandom") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1fm4zjpwdc8mr0hqr9jsddrvcn3m7crdbi9v5s9i2nsawcw957mj")))

(define-public crate-jandom-0.2 (crate (name "jandom") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0drlpl6q6i9dmynq45ppipzq5zby7wn2k35j1snlf2gvn94iczmz")))

(define-public crate-jandom-0.3 (crate (name "jandom") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0yf1ws51p58w4nqn1lwp9i3x5prisr03vp99dv7s9k2fmk7039np")))

(define-public crate-jandom-0.3 (crate (name "jandom") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "125hz0q1wwsvqn0198hd9n1wq5ickfjgznzi1ifac0hwkf61x9jn")))

(define-public crate-jandom-0.3 (crate (name "jandom") (vers "0.3.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "15a4k503kj1bk8qnq5463dp0dj7gc0ms6lyxplhlgmbh2cid6pw1")))

(define-public crate-jandom-0.3 (crate (name "jandom") (vers "0.3.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "0xzl41wfnarf7vmddlyin06cg6nyz26rwx43ncnlfmylg9i2mcm3")))

(define-public crate-jandom-0.3 (crate (name "jandom") (vers "0.3.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "0x7h7mdvra89h7hsx0mkghqd8b1ddsb93kk1xq7nrs3mknggxp2x")))

