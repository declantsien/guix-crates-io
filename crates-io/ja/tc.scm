(define-module (crates-io ja tc) #:use-module (crates-io))

(define-public crate-jatch-0.1 (crate (name "jatch") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ppy48hl8wca5z5fhmv9h1rkdykxqbfhl72zmd4mfm1m8kvvgbv3")))

(define-public crate-jatch-0.1 (crate (name "jatch") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jnllr8bnfrmngj8jhgi1bcbmszmshd3a8pyzvczb113g1iskdyh")))

