(define-module (crates-io ja bi) #:use-module (crates-io))

(define-public crate-jabi-0.1 (crate (name "jabi") (vers "0.1.0") (deps (list (crate-dep (name "deku") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)))) (hash "1lrsrf9z7vg7hglhksw7bar0himhb01qrhgzlgv4m9zalhi3s2gm")))

(define-public crate-jabi-0.1 (crate (name "jabi") (vers "0.1.1") (deps (list (crate-dep (name "deku") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)))) (hash "0vvpqy57p3h7dry13hndwrmrr9ljfvb2xdsblaw366cqybh3xcy4")))

