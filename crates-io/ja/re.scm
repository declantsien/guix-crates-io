(define-module (crates-io ja re) #:use-module (crates-io))

(define-public crate-jaredh159-test-adder-0.1 (crate (name "jaredh159-test-adder") (vers "0.1.0") (hash "0xxnz8kz81pv96yaklfbyd2hx5spwadq16whp26y1vm3nc3s8ix7")))

(define-public crate-jaredh159-test-adder-cli-0.1 (crate (name "jaredh159-test-adder-cli") (vers "0.1.0") (deps (list (crate-dep (name "jaredh159-test-adder") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18n82l9glv9xnq1pypadfhk8gw7rb2jmk7210lbwxs8c9qds5dyl")))

