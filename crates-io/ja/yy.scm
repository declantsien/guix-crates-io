(define-module (crates-io ja yy) #:use-module (crates-io))

(define-public crate-jayy_minigrep-0.1 (crate (name "jayy_minigrep") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)))) (hash "142q65l21mmhq6cpkshr434xj8ry5w43xp46qclh02yl1iiqir98")))

