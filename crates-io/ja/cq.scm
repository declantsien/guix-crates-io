(define-module (crates-io ja cq) #:use-module (crates-io))

(define-public crate-jacques_a_dit-0.1 (crate (name "jacques_a_dit") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-gen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yldma1ydp1gm3ibm5dpwnxz559n2yzpah2qy3lg98zr27lqj872")))

