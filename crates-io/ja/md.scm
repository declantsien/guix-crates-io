(define-module (crates-io ja md) #:use-module (crates-io))

(define-public crate-jamdb-0.0.0 (crate (name "jamdb") (vers "0.0.0") (hash "0ziil11n54chxrhj2ifj144ffk62cb6pnqnr03npwbd1siamlflg")))

(define-public crate-jamdocs-0.0.1 (crate (name "jamdocs") (vers "0.0.1") (hash "1q4ai23sb98mlmdzwdnf66rn0695q8dnr5q6654h8jwh32cw4j3c")))

