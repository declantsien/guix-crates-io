(define-module (crates-io ja ph) #:use-module (crates-io))

(define-public crate-japhonex-0.1 (crate (name "japhonex") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "05k03c084cj058mbqny2r3qcj5m3yb4jsyzp7m0v660yc1l0cdax")))

(define-public crate-japhonex-0.1 (crate (name "japhonex") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1myh7khzfhkh2k54zrcn43clq3ng7a81pdcn17d5yz9rf45jjig0")))

