(define-module (crates-io ja y_) #:use-module (crates-io))

(define-public crate-jay_lib-0.0.1 (crate (name "jay_lib") (vers "0.0.1") (hash "0qz6qzdgl6y89kg7knby949qvchliz5gjhm4lsqwpwics4bxc61r") (yanked #t)))

(define-public crate-jay_lib-0.0.2 (crate (name "jay_lib") (vers "0.0.2") (hash "05sgj80byjkjd337pfmkad3s3icy8nydm07sf700b57vkb7lq5vd") (yanked #t)))

(define-public crate-jay_lib-0.0.3 (crate (name "jay_lib") (vers "0.0.3") (hash "16z6r8jqfpj27zzsy8c3jlggrycg218zzssmil1jkfd8vvy1jilf") (yanked #t)))

(define-public crate-jay_lib-0.0.4 (crate (name "jay_lib") (vers "0.0.4") (hash "0dlm7dvm8bvj3q9xr89ap4am9fz244gpd20259my5cxrlay71nmc") (yanked #t)))

(define-public crate-jay_lib-0.0.5 (crate (name "jay_lib") (vers "0.0.5") (hash "1dgny5h6apivb6k1b9sgg628vkizg00nf9pk6bb5gnri9qicsnab") (yanked #t)))

(define-public crate-jay_lib-0.0.6 (crate (name "jay_lib") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1v9xp5b9aq1sss62kw9h1lm0dr990abixm3z0xapf44ch2lwgfgz") (yanked #t)))

(define-public crate-jay_lib-0.0.7 (crate (name "jay_lib") (vers "0.0.7") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "102xm6ivl1rc4qmlh6rpybixgjl9br2p5djzikigzvrv5krf0vh8") (yanked #t)))

(define-public crate-jay_lib-0.0.8 (crate (name "jay_lib") (vers "0.0.8") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "025mbld5nw4zx1m1fz6fmk9g40cxjhwbf84zgmcn44hh6lss8gdp") (yanked #t)))

(define-public crate-jay_lib-0.0.9 (crate (name "jay_lib") (vers "0.0.9") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "10hl3pbbv17xvcd32151705ib7n9xbfrdd5579q3w9qcajddnaih") (yanked #t)))

(define-public crate-jay_lib-0.0.10 (crate (name "jay_lib") (vers "0.0.10") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "01bzd6mh0h6p6zvlzrz4srlb1ah6q6qxlwmizzhp362n828d6rcf") (yanked #t)))

(define-public crate-jay_lib-0.0.11 (crate (name "jay_lib") (vers "0.0.11") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "01xkq8g51kylbr52hpiznzrmmx3vswjlwdphhx70yq42aig6szis") (yanked #t)))

(define-public crate-jay_lib-0.0.12 (crate (name "jay_lib") (vers "0.0.12") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "043cqybvxds3vbwk2q4a7dnn7kydrdz0bp1kylfyn6316mjcm0zb")))

(define-public crate-jay_lib-0.0.14 (crate (name "jay_lib") (vers "0.0.14") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1cyz6mrqwixw0x5h2nvqp576vh58j6iyajizfzr53kqvdx48mblv") (yanked #t)))

(define-public crate-jay_lib-0.0.15 (crate (name "jay_lib") (vers "0.0.15") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1ljpjx6sgnz4ybcj9mwz5dimh23kgl4nxj9kllypbv815936lq7z") (yanked #t)))

(define-public crate-jay_lib-0.0.16 (crate (name "jay_lib") (vers "0.0.16") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0671vy8pm1qsn75im4c2bdb476rcjnb81017dxjk1z8h9nbqxa0h")))

