(define-module (crates-io ja bc) #:use-module (crates-io))

(define-public crate-jabcode-0.1 (crate (name "jabcode") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)))) (hash "0l2lglfp6ha03w8md7p0i04wirrbz68x6gzqzymhzs3aic82qa0w")))

(define-public crate-jabcode-1 (crate (name "jabcode") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "02d3wfc77cwzp9byp0fb11g8xikbhbx893xw4g5vs8sxwpvgmisd")))

(define-public crate-jabcode-1 (crate (name "jabcode") (vers "1.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "03ws1j75m3l88xhdlr667vx00mp4yc84nn7rpwzxmxhkp595hqkk")))

