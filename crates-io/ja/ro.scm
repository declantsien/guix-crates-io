(define-module (crates-io ja ro) #:use-module (crates-io))

(define-public crate-jaro_winkler-0.1 (crate (name "jaro_winkler") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "eddie") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "0pvbydv3yqn9x4ajy56w19j451g6cqp2gvsyshwpa82wnzb06scn")))

