(define-module (crates-io ja pi) #:use-module (crates-io))

(define-public crate-japi-0.1 (crate (name "japi") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1p9lj45qrm4dikd8xh5aqlhnngkqaq5jgvclkl04x93nv4zfa7sy")))

(define-public crate-japi-0.2 (crate (name "japi") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sh8240rwn2ar6yjjvifk7i8d8gfz1qbg7i75in6z4h174bcgj9w")))

(define-public crate-japi-0.3 (crate (name "japi") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04ivhkagzih1009a7hiap6cay2vai6618ph26vbs7b89avgglbma")))

(define-public crate-japi-0.3 (crate (name "japi") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "129sgx73wpmnwajwf8zn4zx1rr1vc888lcy42bwx9q3qi7h7p7fk")))

(define-public crate-japi-0.3 (crate (name "japi") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hnv5g8v0ngik7by5v9xg6n8lxcasnn51p3by3pr61f9hsh82ik7")))

