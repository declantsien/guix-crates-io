(define-module (crates-io ja st) #:use-module (crates-io))

(define-public crate-jast-0.1 (crate (name "jast") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "0cm0n4zsag0lwsnbsgcws459nrgpijk79631bdj2w0q9vx77nmp5")))

(define-public crate-jast_lib-0.1 (crate (name "jast_lib") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "0709dj0ha5chv8z95hqiyr4zmmg4g12kd4v3asj5qb1x4niqmih7")))

(define-public crate-jast_lib-0.1 (crate (name "jast_lib") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "04pn9g4yampj1rp6pib4rv0amjwsmdigkkr2qgwvbdykd3r8zjdw")))

