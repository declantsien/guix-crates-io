(define-module (crates-io c- cl) #:use-module (crates-io))

(define-public crate-c-cli-0.1 (crate (name "c-cli") (vers "0.1.0") (hash "1b29bp14nanb63ajcyw3rpsbyb6dlsifpw1n9k50zs6y69i3xmh0")))

(define-public crate-c-cli-0.2 (crate (name "c-cli") (vers "0.2.0") (hash "1p92p3wdhxap5b5dn76xwr0xvdycbrpvnshzvimhdq4szcm4f1hp")))

(define-public crate-c-cli-0.2 (crate (name "c-cli") (vers "0.2.1") (hash "1gqhbbjkgs8xwbpy6j0rdy1lkkgkjqdv76apfqsa0lvhlqb7jr84")))

(define-public crate-c-cli-0.2 (crate (name "c-cli") (vers "0.2.2") (hash "0873nn2kp2y1kp009bg9r2n3l3p1y4y7rb6ghykc9lwc84wygwa3")))

(define-public crate-c-closures-0.1 (crate (name "c-closures") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0g4s1ha8c0n3hs0m3ghijn02yxlfa94p12721kgsq9d20n2fa533")))

(define-public crate-c-closures-0.1 (crate (name "c-closures") (vers "0.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0saabi4n5g53z53x5lkbgwn2mjgssc0b7a7sfhnzjf7ai7nzy8ah")))

(define-public crate-c-closures-0.1 (crate (name "c-closures") (vers "0.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0xqsahmv8hvyyjz0lrpxly2anvnq1sm3z9nmv5jzf4l9qzi8klcg")))

(define-public crate-c-closures-0.1 (crate (name "c-closures") (vers "0.1.3") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qj764g7f7pp3l8fjirrkkhx5hp6ziizgymxwhcv4n4yh87h72yy")))

(define-public crate-c-closures-0.1 (crate (name "c-closures") (vers "0.1.4") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0agq79pi03ppshmnd0xl5p3jq5z90hp0n7lvg5fbid7v3n97j6y2")))

(define-public crate-c-closures-0.1 (crate (name "c-closures") (vers "0.1.5") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "07yq119bk5haw1qwzv6hj9qnarca90b1xk9hr2iyn7rg3fmcw2qs")))

(define-public crate-c-closures-0.2 (crate (name "c-closures") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ksrashql5zfr94xk51wir5jd4ch9xpk2xj0ccq5crj3371jmcqc")))

(define-public crate-c-closures-0.3 (crate (name "c-closures") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1p026y7djfhrll7jqkfrw553dr73i1idx0z3ml8z38zaji63gkr3")))

(define-public crate-c-closures-0.3 (crate (name "c-closures") (vers "0.3.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1nzmig17m4h49qlqbw3vziiy2mb91silv7ma5ciinkqw5rrg1s4y")))

(define-public crate-c-closures-build-0.2 (crate (name "c-closures-build") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.9") (default-features #t) (kind 0)))) (hash "1j132667zzjz9dnrrl1wbb7b017phj7ranz235555s1f28rmr984")))

(define-public crate-c-closures-build-0.3 (crate (name "c-closures-build") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.9") (default-features #t) (kind 0)))) (hash "1lfyk0jls5ghgzk93vlpv2i57bcg4wska4sf77qnqa28y8m8jwhm")))

(define-public crate-c-closures-build-0.3 (crate (name "c-closures-build") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 0)) (crate-dep (name "cargo_metadata") (req "^0.9") (default-features #t) (kind 0)))) (hash "12vaf1jxxr06a32bl1z1ip8fsf8dbk5qcrqg5fdjxvp8ghncfi50")))

(define-public crate-c-closures-build-1 (crate (name "c-closures-build") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fcdvpfhz3crkgp6qhhxjn3a9pgvf7m9ap733rscy2ipk45cdpzh") (features (quote (("no_std"))))))

(define-public crate-c-closures-build-1 (crate (name "c-closures-build") (vers "1.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mzj4nvwpsraq6jwk7nxxak433bqqsq99n5i25cf03rnq7s979z0") (features (quote (("no_std"))))))

(define-public crate-c-closures-build-1 (crate (name "c-closures-build") (vers "1.0.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "006wdcgwbyrip0fhxsh6768p65xdsnrz3b268nm1q4dv7fk1ccds") (features (quote (("no_std"))))))

(define-public crate-c-closures-build-2 (crate (name "c-closures-build") (vers "2.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1hqh9crfz1g7azn09zk185nir9f42xpwryaz69n4yd74gwgm9r10") (features (quote (("no_std"))))))

(define-public crate-c-closures-build-2 (crate (name "c-closures-build") (vers "2.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fafw53r11dlbvx54j05iqw56ds5z2fs7fk66q515lkzmglmngc6") (features (quote (("no_std"))))))

(define-public crate-c-closures-build-3 (crate (name "c-closures-build") (vers "3.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "04d816mrzf3d78hfk7ksnsasnnnw1dw89zhqm4hzzyw9b7z6arra") (yanked #t)))

(define-public crate-c-closures-build-3 (crate (name "c-closures-build") (vers "3.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "070p8nk8g4xssdg4rxq9hb20sgnnnck2ihaqv2yhand78awdfl37")))

(define-public crate-c-closures-build-4 (crate (name "c-closures-build") (vers "4.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0nlr3fn9vd86hgv1lc0zaiwxg0rf3s7xp1irin17l7kplf1y0mix")))

