(define-module (crates-io c- ty) #:use-module (crates-io))

(define-public crate-c-types-0.1 (crate (name "c-types") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0fq2qsy1vlnrxq25r47wkgfryinvxw6ch4r6r475gk5088xybvcg")))

(define-public crate-c-types-0.1 (crate (name "c-types") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0l1nf2qanj6d7lh329wdyjkwgyzrfq4ab52ll5b0syxvx9in7ig3")))

(define-public crate-c-types-0.1 (crate (name "c-types") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "05qimh07yav8fhwpf3p5gj40h36riv570nlwjx8jrd3hqdklx5hd")))

(define-public crate-c-types-0.1 (crate (name "c-types") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1plmvlz1q4ip0f0xlzhm0vfq8jfm4brz3m32ljf2x2z45v4nfb6g")))

(define-public crate-c-types-0.1 (crate (name "c-types") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.6") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0wi6d56zfaj0p78hh6q45w8lgv78nb3js4qc1iqmw3246a3xr5iz")))

(define-public crate-c-types-1 (crate (name "c-types") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1dyq9rkjsx9mxfinmas2ygffisxi6wkvzaw32zrzvz5wmrdd2nq4")))

(define-public crate-c-types-1 (crate (name "c-types") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "11i0vqdkd1bywwll8kpg4v1hv8npmknk4q8bvcx4gjbpi46ixlf0") (yanked #t)))

(define-public crate-c-types-1 (crate (name "c-types") (vers "1.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1lw8n8kzbylll1isr9vxgi707wcyppc0f7xg90r6cbsin5rpqrrx")))

(define-public crate-c-types-1 (crate (name "c-types") (vers "1.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0qi1v39qqv4p84xgiz2jlz0npxz6ljsxis5f96wqbyihzzb17y6g")))

(define-public crate-c-types-2 (crate (name "c-types") (vers "2.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.3") (features (quote ("winsock2" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "13bf0p39dkdjq7gwf9xwf1sri7drgdcq109cdxl39mb24p7xv3s8")))

(define-public crate-c-types-2 (crate (name "c-types") (vers "2.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.3") (features (quote ("winsock2" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "063a51gzxga99vswd48fs1ma65m7gkp0ybl3zv2m6qcmzw5nws5y")))

(define-public crate-c-types-2 (crate (name "c-types") (vers "2.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.3") (features (quote ("winsock2" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0q84g3zj1kgy8n5mkfkppjm842j3q6175j3s1rvwsgi6ivp4hgmr")))

(define-public crate-c-types-3 (crate (name "c-types") (vers "3.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0bxixriahrj1sz5hklvsfm2s1aafbh5i85l6k8z1cs2p2aislji3")))

(define-public crate-c-types-3 (crate (name "c-types") (vers "3.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "windows-sys") (req "^0.52.0") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0z719mzg9wvfcd7ll1sqlpfpivlc38vxj7w7a2b0qahzcff38ysm")))

