(define-module (crates-io c- as) #:use-module (crates-io))

(define-public crate-c-ast-0.1 (crate (name "c-ast") (vers "0.1.0") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "04is510xqbpyk1r0hiixj2rp8mvf17k0gnbczfd5lylyp2lacmbv")))

