(define-module (crates-io c- em) #:use-module (crates-io))

(define-public crate-c-emit-0.1 (crate (name "c-emit") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1nzs2dfbp6qmcfylpdrf5qp57b9b1hw6qjizwiflk4psk1i67gc9")))

(define-public crate-c-emit-0.1 (crate (name "c-emit") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "00lqxdjs30bfac0qlkskiqsvamqm5yiq25h195aj4ai7hphhv2vz")))

(define-public crate-c-emit-0.1 (crate (name "c-emit") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0q1gpskm61g9qj2jyg8hmrjvmkkn6s53dhrgi4jh0g357k867jxd")))

(define-public crate-c-emit-0.2 (crate (name "c-emit") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0y0yjw3m3hpzrv25nwh683vyrx3mpn5rwb8sn2bzrkm66gpc46ac")))

(define-public crate-c-emit-1 (crate (name "c-emit") (vers "1.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0cvcf1w744xb3g00z20p6ahw5vd322qqm5rgjmwac5bara5qg08y")))

(define-public crate-c-emit-1 (crate (name "c-emit") (vers "1.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1fjprjhjhiq2mr1wha9b20yhdkpr201y53pzcih8x2i25yfk4v86")))

(define-public crate-c-emit-2 (crate (name "c-emit") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1wcirjbkx1rk5gzdp2951i4injwfp0hw3rs93g2i5nqdzsi4wyrv")))

(define-public crate-c-emit-3 (crate (name "c-emit") (vers "3.0.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0pvpr57va5qlwh4pg19yjw3f579639yvmwhdab9ir6plnpdf1r7s")))

(define-public crate-c-emit-3 (crate (name "c-emit") (vers "3.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "07fjr6f1jrawv0fii2y22lvs0a41vs52fla1g1bpbq63gb6qq6l8")))

