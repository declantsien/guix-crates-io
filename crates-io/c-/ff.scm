(define-module (crates-io c- ff) #:use-module (crates-io))

(define-public crate-c-ffi-0.1 (crate (name "c-ffi") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)))) (hash "0ah2x8pcilrha29ikl42c340f0hps0bs6221l4jqd7hilybz64vq") (features (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.2 (crate (name "c-ffi") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)))) (hash "14lxj73ridc5hv5pihxcg5i7zgc7qfwvd6zfimlm06h6i2najwbg") (features (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.3 (crate (name "c-ffi") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.3") (default-features #t) (kind 0)))) (hash "1sf3z6laffx1spkn5fb2gfqri5wfpvzik52wm8j61kck0yalwkh4") (features (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.3 (crate (name "c-ffi") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (default-features #t) (kind 0)))) (hash "0247bypdc0qpiwxpk8kj10p3fy1pphpznp8gv21x3p6sh767n603") (features (quote (("default" "libc"))))))

(define-public crate-c-ffi-0.4 (crate (name "c-ffi") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "184vg6svl9r2kpand524kmsld362s8jnskvfhbwcw7g10lwppsyx") (features (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4 (crate (name "c-ffi") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0ljwjbhq9yzhcx3x4s6yx9y3va8lzjlci3nw5irls3ln0svq9k6i") (features (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4 (crate (name "c-ffi") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0bbsfsy7l0rcnls68dj39n4lwd48lsb4scc61637wmgqnffba94j") (features (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4 (crate (name "c-ffi") (vers "0.4.3") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0rgmv39qx38jzm6zr9lll5mqxlkvf23l00lnxwf8cpclb42w8qis") (features (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4 (crate (name "c-ffi") (vers "0.4.4") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0czyl1d8f8hy7c8flgzfjd3jxv2w04n09f2ngmzvlbfh8cas98jx") (features (quote (("memory" "smart-ptr" "libc") ("default"))))))

(define-public crate-c-ffi-0.4 (crate (name "c-ffi") (vers "0.4.5") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1k97dc2n5y4q66x0a8yx2ylw562aafkgdb0ykcl6zp8d2xpqxxbq") (features (quote (("memory" "smart-ptr" "libc") ("default")))) (yanked #t)))

(define-public crate-c-ffi-0.4 (crate (name "c-ffi") (vers "0.4.6") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "smart-ptr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0cx07xfypwd00f771mb0qv2kchix4inzkm6v9lrxgvmk7i79jabw") (features (quote (("memory" "smart-ptr" "libc") ("default"))))))

