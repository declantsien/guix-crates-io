(define-module (crates-io c- la) #:use-module (crates-io))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.0") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.66") (default-features #t) (kind 0)))) (hash "0dzcycn4sj092am9hqml4hjs8plzg1b0kj40h7h1d58m3l633cmr")))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.1") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.66") (default-features #t) (kind 0)))) (hash "0v53401xy44rwihr35w86ym4qbfcjl60y6p762vc8q0g3816rk9j")))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.2") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.66") (default-features #t) (kind 0)))) (hash "02v1xfx63827mb7dzmwy7zxqjb0xpfiijkfzkq1nxf1q8lvnszzq")))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.3") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.66") (default-features #t) (kind 0)))) (hash "0n7x77y9bx9xbsxygjsi9yvnjzkha5h2h6npxfq6a4vy7mdwr9r8")))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.4") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.71") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0d7amqk4k1wli7ah2z76n98j0prj6k8wr3jm246qh270z9j5j491")))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.5") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.71") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16icrmjdqpr4mkcd9k3javdxza1cbjcgf5ys8snidmshglwg9q7x")))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.6") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.71") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xm79swy8yq8iy4a54jh605zqqrbdgdpvhsdi22zvc50wqihnfap")))

(define-public crate-c-lang-0.1 (crate (name "c-lang") (vers "0.1.7") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.71") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1cxbp0s2k85yxda9sygs8m1jgv5g04d7ap2bx9sjzy6qhp825fyk")))

