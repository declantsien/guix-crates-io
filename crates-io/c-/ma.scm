(define-module (crates-io c- ma) #:use-module (crates-io))

(define-public crate-c-main-1 (crate (name "c-main") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "047ld2srgz08585cqh9p9fmk56f16qhwky3n1b9wpdcw8mnqp78m")))

(define-public crate-c-main-1 (crate (name "c-main") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0xcz458hzz2zva39ls3la70xlsk8k19vaf761rzxpk9bppwbyyvr")))

(define-public crate-c-map-0.1 (crate (name "c-map") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "1415i7cvf7k6ndxs8krwia4l46r934m931l9mjg0mc338fsgadj5")))

(define-public crate-c-map-0.2 (crate (name "c-map") (vers "0.2.0") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "1vhqqplc0rbwzv779fg36p6n4slvdx9bbi8y1dhp3hd4iiaw7d85")))

(define-public crate-c-map-0.2 (crate (name "c-map") (vers "0.2.1") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1wbdppcjy2xh3h5215291dsvcqr6q2w3sl1bj5zpb592kyp69mql") (features (quote (("parking-lot" "parking_lot"))))))

