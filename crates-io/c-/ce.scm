(define-module (crates-io c- ce) #:use-module (crates-io))

(define-public crate-c-certitude-1 (crate (name "c-certitude") (vers "1.0.0") (deps (list (crate-dep (name "certitude") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "020a18yj5jxsbglr1g01s5ivxzg1wic9d6bk1va5jhcrxbkx4q9m")))

(define-public crate-c-certitude-1 (crate (name "c-certitude") (vers "1.1.0") (deps (list (crate-dep (name "certitude") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pg5cd4sqi0b0364cqam0qzb4yyw7f5zjvw7br3bq8fph691rd2w")))

