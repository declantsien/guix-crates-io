(define-module (crates-io c- ex) #:use-module (crates-io))

(define-public crate-c-expr-0.1 (crate (name "c-expr") (vers "0.1.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "15g9fz95h1d6j76542scaq2jsxi1ha4gsrdrgdsw4yp28sm0zsqb")))

(define-public crate-c-expr-0.2 (crate (name "c-expr") (vers "0.2.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0kahh72r1s5dxbxh8sh3mshq830w2xkxay9qcqyia4sj6hr3680b")))

(define-public crate-c-expr-0.3 (crate (name "c-expr") (vers "0.3.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "03hhx68dpyjfyhap53r4fcysmibcaw2nxb5icgyd6jbyl611hi8h")))

(define-public crate-c-expr-0.3 (crate (name "c-expr") (vers "0.3.1") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1f5n7f5kfjmjjml9v2wc84ba6ms8p4m3a3bar7wp5rk96xz1ipk9")))

