(define-module (crates-io c- le) #:use-module (crates-io))

(define-public crate-c-lexer-stable-0.1 (crate (name "c-lexer-stable") (vers "0.1.1") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1y9v9r4swg78i1hldgxfmhx7zprmaiv3sik5lz090ag5ym488pvy")))

(define-public crate-c-lexer-stable-0.1 (crate (name "c-lexer-stable") (vers "0.1.2") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1k6g2pq5zvbf3hd2yvp8lv9dxgfql1zpc5v28l6nlws1zafpm0zi")))

(define-public crate-c-lexer-stable-0.1 (crate (name "c-lexer-stable") (vers "0.1.3") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1l5b3g662wap5vcb89m0nbqx8qvcjcj6jzjk0grv9lfld2f0zhfn")))

(define-public crate-c-lexer-stable-0.1 (crate (name "c-lexer-stable") (vers "0.1.4") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "11mlkhffx1m45ggsz9a79iahjz9jbyg1hkcvfxrkvjaalcsdp9gk")))

