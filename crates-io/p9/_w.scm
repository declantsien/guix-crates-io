(define-module (crates-io p9 _w) #:use-module (crates-io))

(define-public crate-p9_wire_format_derive-0.2 (crate (name "p9_wire_format_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "07pakzbzjh9y2rc7fp4276y368jpmbp3jscbylvmhcjn7k1vvmm4") (yanked #t)))

(define-public crate-p9_wire_format_derive-0.2 (crate (name "p9_wire_format_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0lar43c1nd4mmiz1vka57vdwhm6cv5igsll01x7if773qhp5lmvv") (yanked #t)))

(define-public crate-p9_wire_format_derive-0.2 (crate (name "p9_wire_format_derive") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "18qgkl999biapl3px847iayhyj3z2mm9la76149fsgmg7qd26q8s") (yanked #t)))

(define-public crate-p9_wire_format_derive-0.2 (crate (name "p9_wire_format_derive") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0kiiymwsk31qcn5y9sliybrpf67grixykj5m73gwp6zcv0854276")))

