(define-module (crates-io p9 #{81}#) #:use-module (crates-io))

(define-public crate-p9813-0.1 (crate (name "p9813") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.11") (features (quote ("hal"))) (default-features #t) (target "cfg(not(target_os = \"macos\"))") (kind 2)))) (hash "06jcpbgw162xwbd9ihh9dgngg22zjracbg0h6p2wikmnfjssdf0d")))

(define-public crate-p9813-0.2 (crate (name "p9813") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.13") (features (quote ("hal"))) (default-features #t) (target "cfg(not(target_os = \"macos\"))") (kind 2)))) (hash "12yzvjrqjvw65lg6qxb984mvssgnbgcd1vbnvzsjqzcq56j1ibgr")))

