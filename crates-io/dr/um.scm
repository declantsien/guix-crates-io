(define-module (crates-io dr um) #:use-module (crates-io))

(define-public crate-drum-0.1 (crate (name "drum") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1id6nrmfzbjrhcyp330kv5cip43ajqwl2wpl5d952qfv9ma1pa4h")))

(define-public crate-drum-0.2 (crate (name "drum") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "00ql83v31qc4f9mz7gfn17xxi51dq9g0lhj67x7afw0740wr0dnv")))

(define-public crate-drum-0.2 (crate (name "drum") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "1.1.*") (default-features #t) (kind 2)))) (hash "0xbj4ir6s3378qg0gywbssl2h1xmwn4bcq74rjqhhqfwndig2gab")))

(define-public crate-drum-0.4 (crate (name "drum") (vers "0.4.2") (deps (list (crate-dep (name "bincode") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "0.6.*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "0.6.*") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "2.0.*") (default-features #t) (kind 2)))) (hash "1d7nm0mclsycif1m1m9g9cs6kh9fj4yccr02d52sm3l0w5g5ssvf")))

(define-public crate-drum-0.4 (crate (name "drum") (vers "0.4.3") (deps (list (crate-dep (name "bincode") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "0.6.*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "0.6.*") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "2.0.*") (default-features #t) (kind 2)))) (hash "0z0llgvswszjhjyq2xi896rg14f4517219xnwvm3ffhbab597gwk")))

(define-public crate-drum-0.4 (crate (name "drum") (vers "0.4.4") (deps (list (crate-dep (name "bincode") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "0.6.*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "0.6.*") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "2.0.*") (default-features #t) (kind 2)))) (hash "0gj8wzfiy301lbc917bdm90mx4qfhh4drxcihmwxi758lmasy8sk")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.0") (hash "0592f5343mqwqbnsc3jdfkfrff8w59v8v4b90v7ql68wcfd0kwsl")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.1") (hash "0ys8v8rq8wldr6yfry8vdgn3assjpik5n5nxpwb2rxqxg2xgq69a")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.2") (hash "10i2a5ydhmbnb9x3yjmx7dl53mfq903m2imhqfmglb8pnx7zy3kr")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1lbqrv5v6bg0vzjnsi2ny4fl3m40crg091dx1dcwrpvxipg9c5wg")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13xszh5j0a5z7p89f9mg97pgvxiwgnzhhb4991msgz8avhjncky7")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.5") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "01dc7lhl5gm9qgysqhw4y5496lp0l9pk27gw7pq8qxyrd9cm0c7j")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.7") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11v5kc4pmk22v1qqq7pvgszpyjqa5qf7j205mcbiq2d0fp8x4clw")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.8") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1mja5mr3z0l371fl2q8hq7pc9xbazl3mfyk4i1djvzgwlqapzj1s")))

(define-public crate-drumatech-0.1 (crate (name "drumatech") (vers "0.1.9") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1wc5d7b852rhgvq5q6h2337y64l1wnfzqyy8dl5xa5vh4wgyfky3")))

(define-public crate-drumbeat-0.0.1 (crate (name "drumbeat") (vers "0.0.1") (hash "1s0xi8rg978szzwhk91qlvi0c4qq9nqiga50qqsp8a4lb5y90fha")))

(define-public crate-drumbeat-0.0.2 (crate (name "drumbeat") (vers "0.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "08zf7dbvfdw0izgwcw1q6xf2jg4asvw218j0i753h7yvxfiphcnr")))

(define-public crate-drumbeat-0.1 (crate (name "drumbeat") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "1iab9bnaigvwjc4djcjbs4vav9bng7jny2ikk7xwggnz1s8yx6r5")))

(define-public crate-drumbeat-0.1 (crate (name "drumbeat") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0r56kk0gs69axa4qli0il636i2f0dvb34rvjadd10gswn4ivsgfl")))

(define-public crate-drumroll-0.1 (crate (name "drumroll") (vers "0.1.0") (hash "1gdk6mjqyai1zqbnmplyfqylann36wg2x466a8dkczp3yq6hrlca")))

