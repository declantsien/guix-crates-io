(define-module (crates-io dr av) #:use-module (crates-io))

(define-public crate-draven-1 (crate (name "draven") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16as6qr94381lsl658mwd8pwjp8im0b2i4b7q3zf1ssj1n82c259")))

(define-public crate-draven-1 (crate (name "draven") (vers "1.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h7fg2ic8n3q6mp55gq34g48j731vxjxwphrk4p4n81r0gjvrdb8")))

(define-public crate-draven-1 (crate (name "draven") (vers "1.0.2") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0m41d2q4ip5z01fmv3r9vq8s1yircm5fzqsc4drd58469fxla985")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.0.0") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02p3nnz20vwq1hv1b29c2409qwnphk6l9lah3sczxjakkdk4fy3a")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.0.1") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "012l5y953nk4lx3624sqf8cvygwgaa61hmwy2r292zlwqr5rpqix")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.0.2") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "130xn6qb3gnl790zpr7dvn1a4fkwga9w68byhhvyw9sz1b9ncb0q")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.0.3") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qwd8ikb8f2hddm93p1gychvlgsfs8j5rdxs5bq3qq3fjp8cz267")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.0.4") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n1vzn5c4jr3zf3iilbk42fgcj28d155yr7dacpcg3si1f0ixm2q")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.1.0") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18kl3mzyc36rcgv8sy8mwhpfm9q4fdabjvbf51jx7k793jlsrjwc")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.2.0") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05qfvy1ysqhvkz0k8fhhw02kqj83ysw3xm32ckmd36307xjldn6l")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.3.0") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s6rfqs96zv0v2w5lnrr3jk60ip9f9dggin6ih3cdiqjbq3580yg")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.3.1") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17sh8493cvz4fhm17hfn2a6hwgdhk33ny30p9ggjc4j13i36142g")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.5.0") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mf8q22pviva6vlsvbzwljv5mrscgx7fk0cqlbnvpy6bz0v2x8hi")))

(define-public crate-draven-2 (crate (name "draven") (vers "2.5.1") (deps (list (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.65") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0k2zfkb28x68pngl1pw2bc76h5zzbv44ac8pjjyvqllqnca1zl8f")))

