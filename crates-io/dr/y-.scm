(define-module (crates-io dr y-) #:use-module (crates-io))

(define-public crate-dry-mods-0.1 (crate (name "dry-mods") (vers "0.1.0") (hash "0jw51c00dfw0nh7jkvs1y5k34palwggiv3xipv9kmnhf8nhjvffg")))

(define-public crate-dry-mods-0.1 (crate (name "dry-mods") (vers "0.1.1") (hash "0v3d6992d6hwx74m8ailsga86gjhmwvppbi0szb4826j3lkkhfqs")))

(define-public crate-dry-mods-0.1 (crate (name "dry-mods") (vers "0.1.2") (hash "0n0w2fnsgx3gcby0p5iy3q9b42j36w0wljajpr07bflg6pj638ci")))

(define-public crate-dry-mods-0.1 (crate (name "dry-mods") (vers "0.1.3") (hash "0lwgxy2lr43mlck85hab45f68dvp4r6lqksyw1js436gvpbinfb7")))

(define-public crate-dry-mods-0.1 (crate (name "dry-mods") (vers "0.1.4") (hash "1f88fvwr6g30jn9d1gbfls3divv7rc99fmwzp5pfq6cg8ggrqs6x")))

(define-public crate-dry-mods-0.1 (crate (name "dry-mods") (vers "0.1.5") (hash "0gbjn96wv1rn58dzdn6f3rmpzrazxbqz9mpqv63snmbmyba2hy50")))

