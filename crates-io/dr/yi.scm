(define-module (crates-io dr yi) #:use-module (crates-io))

(define-public crate-dryice-0.0.1 (crate (name "dryice") (vers "0.0.1") (hash "0c7p909nzhgv5ny1mg35kdxj8jrglx54xqqg5k1nwiw39nn4kzsm")))

(define-public crate-drying_paint-0.1 (crate (name "drying_paint") (vers "0.1.0") (hash "0sxbxcnbaaxbniv5jq0pjwqm9knpldggsyfvli5a21dn424lsxa0")))

(define-public crate-drying_paint-0.1 (crate (name "drying_paint") (vers "0.1.1") (hash "1rxyvhhl9fkhrx6v2qb79kf772mbav83zzw642jlzn5gcii9vj17")))

(define-public crate-drying_paint-0.1 (crate (name "drying_paint") (vers "0.1.2") (hash "0sz65nrdpw3xp9lrag5z9gvlyjmmx64kp4wfcky0iry1ak7f8pch")))

(define-public crate-drying_paint-0.1 (crate (name "drying_paint") (vers "0.1.3") (hash "13n23hc85gciav9j0l2kc5ciy74q8hxqz8mcmnkkqxzzrrlz9cqj")))

(define-public crate-drying_paint-0.1 (crate (name "drying_paint") (vers "0.1.4") (hash "0xwh4h1kd3pyvzzqcj3swcc80scqql81hwyln8m1msa0295pbs29")))

(define-public crate-drying_paint-0.2 (crate (name "drying_paint") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0p0001hyb0wrvbyivq8sz84wx5smi446gmw9z25b87fs1hj7vwxw")))

(define-public crate-drying_paint-0.3 (crate (name "drying_paint") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "059wxksmync3blla1g9dh2qdjxpxxfhfhnjvbwkh1f487ca0mk6p")))

(define-public crate-drying_paint-0.3 (crate (name "drying_paint") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ly2kqygl2ki3fhmm0s4v8h5mq1ngb1353wzjjximrfrmbqb7mw1")))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "08yig5lqwq16xrb0wcaba9mbrczafzz9gpbqnq54vqw0zvklz6y1") (yanked #t)))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "04b5qj9cr9c1zzs75d1v2crmsdw6fxaj41vzyp215dzjzhl78fax") (yanked #t)))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ipndd4vkm6h9bgqhmcbacf350k35h7n573v40wh5g4f5p3152sr") (yanked #t)))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "18n9x6ixizfysphayc6fiipxxwis0ab35lvvp93fr6i17nb7danp") (yanked #t)))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "09vivyfbjn97byv66mcwnmr6jxg5wk0rm4p3pzc7y9z1dqpm8ndd") (yanked #t)))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.5") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "09p4rlly3ybrcyll97kyil86x0g4n7srx3j99ndd58kdqvm1fwgf") (yanked #t)))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.6") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "17w7vwq19cbcs6b58vfpynz1cny10iyf7li3hnjq1pb4sbiq5rs7") (yanked #t)))

(define-public crate-drying_paint-0.4 (crate (name "drying_paint") (vers "0.4.7") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0klxc1nxwfi56dk5c8w7d9qf1wnh3lsilp2gvlpzjwgmcdw6qmlf") (yanked #t)))

(define-public crate-drying_paint-0.5 (crate (name "drying_paint") (vers "0.5.0") (hash "1x10j8f2mrgq6vlwd1ax25zxw0qcyr38n94vlymyrz1vh5xnx5yz") (features (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5 (crate (name "drying_paint") (vers "0.5.1") (hash "14cjhz3rbwkxgi9c0dk2ydsa80ksv83jwidv0cp5bm13mx6k75z8") (features (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5 (crate (name "drying_paint") (vers "0.5.2") (hash "0kjbimcgwzs2b8s7g14hiq3xpmddd2j0wzk4jfj07xiw0gnnccnb") (features (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5 (crate (name "drying_paint") (vers "0.5.3") (hash "1n3cnr8522bkwvnvsrvm48annznsafmp1fmifjjym08rxdz723l1") (features (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5 (crate (name "drying_paint") (vers "0.5.4") (hash "1qfgj8is4fpl2399sr01znp3v5caaj3aq26nmsbq0fa63vi6fnxl") (features (quote (("std") ("default" "std"))))))

(define-public crate-drying_paint-0.5 (crate (name "drying_paint") (vers "0.5.5") (hash "1x352705vr45q04i26mhvx37lfz0p5jxbiv5srvawynrqi0xisyl") (features (quote (("std") ("default" "std"))))))

(define-public crate-dryip-0.1 (crate (name "dryip") (vers "0.1.5") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "06319qhk0yd1r5wggbh6k3hq65dynkp1qdq5z6yp165ahx29jrna")))

(define-public crate-dryip-0.1 (crate (name "dryip") (vers "0.1.7") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0anh5ism34awwbr4pbkxiirsv5z4y9bjqsa9jw17jf9g5833kpz7")))

(define-public crate-dryip-0.1 (crate (name "dryip") (vers "0.1.8") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "01dlij8mmppggj7fb48b1l9v34260bbdx0ksn64grqx11byiv4rz")))

(define-public crate-dryip-0.2 (crate (name "dryip") (vers "0.2.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "16h1hgqkvjd0vidi4djg72riyjwqs8fhbdsff3d0iwpgkf3sixcy")))

(define-public crate-dryip-0.3 (crate (name "dryip") (vers "0.3.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0b5b6ycfa16bc0ql8808ka53pf7l3g0305wka7afnhk9l5dhf6i1")))

(define-public crate-dryip-0.4 (crate (name "dryip") (vers "0.4.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1z3lzqmkhxx499rfbjki092xfgxkvi1br4mai30j0rbh9id494q1")))

(define-public crate-dryip-0.5 (crate (name "dryip") (vers "0.5.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "11wrac3avd4ns82m84b76gm1s2bsw4kgqvn4xrraw5iksqg1ygs1")))

(define-public crate-dryip-0.6 (crate (name "dryip") (vers "0.6.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0jgm397lwhjzkd5hxy4sjz1hnplm6ghkdkrhgzlc9knd23n3j6wn")))

