(define-module (crates-io dr s-) #:use-module (crates-io))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)))) (hash "0n4lfad00rxjpjp6mnvzmb5gm7bpwl7gfz9y5a921xg74gzas1ss")))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)))) (hash "1hqmziab4pykld407q77ky57jspd8ad9xljq3p04b2fdas1m9yn3")))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)))) (hash "0bvqzrkg8bg0m1zfd14fyvzhb643qhajcff2lh36hffwg01if2hf")))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)))) (hash "0vpd1xgxjc3i7z38s0j7k56imfgbyqdk9rd3cqanvkwab6w8yz5s")))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.4") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)))) (hash "1j4x0swdwkfdzc6wqr5ass4wz76bnpgcpmlmajac7qw4pfyk68b6")))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.5") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)))) (hash "12v3yabld9wawvv9y4kvd6bkq1kjffrzp5gpdgwvd3gc3b0fs8wd")))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.6") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)))) (hash "0kvjn1fm3qz2aidblmxyrx065sm3rac7mfxzw71ds2sadmjzrsih")))

(define-public crate-drs-0x01-0.1 (crate (name "drs-0x01") (vers "0.1.7") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)))) (hash "0y5iplmlqnh7fi0y3av49bw9skn6vl5kq3xf7px82p3kvm90pcjq")))

(define-public crate-drs-0x01-0.2 (crate (name "drs-0x01") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "try_from") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "00dkjip6j8hslf01w9wazdg97dsdq2cjjschrc2mp4hqxaigbknw")))

(define-public crate-drs-0x01-0.2 (crate (name "drs-0x01") (vers "0.2.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "try_from") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "08jcjwx1laas1x81p4kg97xpb65v94jixpyfcfwxdmqbm4kwd24m")))

(define-public crate-drs-0x01-0.2 (crate (name "drs-0x01") (vers "0.2.2") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "try_from") (req "^0.3.0") (features (quote ("no_std"))) (default-features #t) (kind 0)))) (hash "1bgllaz7gfx6d35rl7063vv37imnrpxrbxr0rfgxyjyj2rp50cv7")))

(define-public crate-drs-0x01-0.3 (crate (name "drs-0x01") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "docmatic") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "try_from") (req "^0.3.0") (features (quote ("no_std"))) (default-features #t) (kind 0)))) (hash "1908prkzqrlddz9shhx82fllbss6rfjjqflhpw0yllwpzddr1kcz")))

