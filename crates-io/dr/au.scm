(define-module (crates-io dr au) #:use-module (crates-io))

(define-public crate-draum-0.0.0 (crate (name "draum") (vers "0.0.0") (hash "07435hflvjfzkymv02h3lplinwyvfxk510a7cw7q6mfqmhibn7yk")))

(define-public crate-draumaz-butterfly-0.16 (crate (name "draumaz-butterfly") (vers "0.16.0") (deps (list (crate-dep (name "pancurses") (req "^0.17") (features (quote ("win32"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "savesys") (req "^3.5.9") (default-features #t) (kind 0)))) (hash "1q5hzj5p0r0njgvdjpgwdgzl1nzlk600ng805df7nc5asy56jysh")))

(define-public crate-draumaz-butterfly-0.16 (crate (name "draumaz-butterfly") (vers "0.16.1") (deps (list (crate-dep (name "pancurses") (req "^0.17") (features (quote ("win32"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "savesys") (req "^3.5.9") (default-features #t) (kind 0)))) (hash "0plaq221ijb497rrm0akzaf47q80w0jc39jmq2wqcnbry52wx5qs")))

(define-public crate-draumaz-butterfly-0.16 (crate (name "draumaz-butterfly") (vers "0.16.2") (deps (list (crate-dep (name "pancurses") (req "^0.17") (features (quote ("win32"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "savesys") (req "^3.5.9") (default-features #t) (kind 0)))) (hash "1gn1f5ylw8yx6j91wz8lxis2ps4n9bv6mj8mys8fz4kyjx7298n8")))

(define-public crate-draumaz-butterfly-0.16 (crate (name "draumaz-butterfly") (vers "0.16.3") (deps (list (crate-dep (name "pancurses") (req "^0.17") (features (quote ("win32"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "savesys") (req "^3.5.9") (default-features #t) (kind 0)))) (hash "1apd1qf1k9sqgbxbvv5x42dagwsbwlm76w10371a79wr8yr2ill3")))

(define-public crate-draumaz-butterfly-0.16 (crate (name "draumaz-butterfly") (vers "0.16.4") (deps (list (crate-dep (name "pancurses") (req "^0.17") (features (quote ("win32"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "savesys") (req "^3.5.9") (default-features #t) (kind 0)))) (hash "0z8a8xpy90l1fdjf3mfn9wvvh720niynnw4l79dich9klaw12hn8")))

