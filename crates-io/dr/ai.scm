(define-module (crates-io dr ai) #:use-module (crates-io))

(define-public crate-drain-0.0.0 (crate (name "drain") (vers "0.0.0") (hash "0j8901xdvrbwz4r6307a5ag1q2z89kaxn8hkv4bwxfbf6z3mp4pn")))

(define-public crate-drain-0.0.1 (crate (name "drain") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tower") (req "^0.4.7") (optional #t) (kind 0)))) (hash "0n65db4nmp4nyiapzgxdk0qhwvb326rqwlglw8i1i9cv4yphzkms") (features (quote (("retain" "tower"))))))

(define-public crate-drain-0.1 (crate (name "drain") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.15") (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tower") (req "^0.4.7") (optional #t) (kind 0)))) (hash "062agrdmpdjqm9prdg7p44498zbmm8wncgavbpr0kwyza473h0cp") (features (quote (("retain" "tower"))))))

(define-public crate-drain-0.1 (crate (name "drain") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.15") (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tower") (req "^0.4.7") (optional #t) (kind 0)))) (hash "0bb3sxdpv2cvn5mix80lhjv153j0lw3l4h8fvnsdkynf7yzhl6ig") (features (quote (("retain" "tower"))))))

(define-public crate-drain-0.1 (crate (name "drain") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.15") (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tower") (req "^0.4.7") (optional #t) (kind 0)))) (hash "07mwv29y9mxxy7zdzn2d00vas7h08nj7kz8q6frzqp9bpll5044x") (features (quote (("retain" "tower"))))))

(define-public crate-drain-rs-0.1 (crate (name "drain-rs") (vers "0.1.0") (deps (list (crate-dep (name "grok") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0lcgw7ndxqa4dqi6zpjipn05x7l9kg5426wrxswc7b5w5p2q5apw")))

(define-public crate-drain-rs-0.2 (crate (name "drain-rs") (vers "0.2.0") (deps (list (crate-dep (name "grok") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1aiyh0ii8lwkqa6j05218l9vdisqv2n2nxq7qn1nv07nbs07g0h3")))

(define-public crate-drain-rs-0.3 (crate (name "drain-rs") (vers "0.3.0") (deps (list (crate-dep (name "float-cmp") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "grok") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "18dk6nndmqc9d04kwb0hirzyn58psypqpvabgk0g02vxic2fkxaf")))

(define-public crate-drain-while-0.1 (crate (name "drain-while") (vers "0.1.0") (deps (list (crate-dep (name "easybench") (req "^0.1") (default-features #t) (kind 2)))) (hash "02yvc7p2i6hwhysi5s7wq2b8az100vh7l11bzhgff4463l5yp182")))

(define-public crate-drain_at_sorted_unchecked-0.1 (crate (name "drain_at_sorted_unchecked") (vers "0.1.0") (hash "0s7963d3ppwih2aijh3h6sbzya7bf8b6ld6n4laf3lyh82y44d5l")))

(define-public crate-drain_filter_polyfill-0.1 (crate (name "drain_filter_polyfill") (vers "0.1.0") (hash "00jkhjfmkl8vnnxl53fbw7msv7xjyfm907hvpcdbmbvwkmq4gnai")))

(define-public crate-drain_filter_polyfill-0.1 (crate (name "drain_filter_polyfill") (vers "0.1.1") (hash "0y47dn85yw2ys8qy6jibqjw1las4p385b68ps8y6gml3z8i5y05a")))

(define-public crate-drain_filter_polyfill-0.1 (crate (name "drain_filter_polyfill") (vers "0.1.2") (hash "10kzqav8018vdml2ys6lcxkhxkyl5b88816bxrj8vz3dv2ypd7ya")))

(define-public crate-drain_filter_polyfill-0.1 (crate (name "drain_filter_polyfill") (vers "0.1.3") (hash "02040m2yrqzlsin6kndpkfy8870awxihpzh63fdwdi94wxg496k6")))

(define-public crate-drainrs-0.1 (crate (name "drainrs") (vers "0.1.0") (deps (list (crate-dep (name "indextree") (req "^4.5.0") (default-features #t) (kind 0)) (crate-dep (name "json_in_type") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^4.0.0") (default-features #t) (kind 2)))) (hash "0llvmc8zj5v6j54igx9saqmpxmbivizdz6j0s1gk7zb6xl87dldh")))

