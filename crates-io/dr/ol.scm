(define-module (crates-io dr ol) #:use-module (crates-io))

(define-public crate-droll-0.1 (crate (name "droll") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "13f8ggwzn2vjv8mn2xrxj6mcpln2whwnlpi0gxnkkn5i52c5pvid")))

(define-public crate-droll-cli-0.1 (crate (name "droll-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "droll") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19f3pqg9702gxc62a0g5v6ciz3xd58km7zsyihznf1j334rfiprs")))

