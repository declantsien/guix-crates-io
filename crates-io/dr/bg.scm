(define-module (crates-io dr bg) #:use-module (crates-io))

(define-public crate-drbg-0.1 (crate (name "drbg") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)))) (hash "02q6623k5i4fhiym7al08prnslmggbsk7xiyinbylzkvgzcrqc7k")))

(define-public crate-drbg-0.1 (crate (name "drbg") (vers "0.1.1") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)))) (hash "1pb1ni5mbq766dm2mla3dinib46lc18m9f98j47an0mqi9xwn4jd")))

(define-public crate-drbg-0.1 (crate (name "drbg") (vers "0.1.2") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)))) (hash "06nr57xnca7p0zxp9axlzdp4ql1abzl2cpj90qndrbwzkxd6a6di")))

(define-public crate-drbg-0.1 (crate (name "drbg") (vers "0.1.3") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)))) (hash "0igxcg1bnkc5casgkr1129s0fmgddqgfais31zbld3dgnx7cbn6r")))

(define-public crate-drbg-0.2 (crate (name "drbg") (vers "0.2.0") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)))) (hash "0l493rdjnnncs1fqb4i50klxmdvn3z9wbi01n329kj8676r0yhkd")))

