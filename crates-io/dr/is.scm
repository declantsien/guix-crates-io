(define-module (crates-io dr is) #:use-module (crates-io))

(define-public crate-driscoll-0.1 (crate (name "driscoll") (vers "0.1.0") (hash "097k40xzhi41xw8f8870ghj3hs0llk5i9rlznfjgg8dg5kdzqlq2")))

(define-public crate-driscoll-0.1 (crate (name "driscoll") (vers "0.1.1") (hash "1g50mwiir72jz5azk01viga80f8ngqnn5q594i8c9b44xss8drsq")))

(define-public crate-driscoll-0.1 (crate (name "driscoll") (vers "0.1.2") (hash "027g95f87iz64gw2phw3fssmn0ykk8jgdwf8fcflv1mwz3daz639")))

