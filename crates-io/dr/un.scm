(define-module (crates-io dr un) #:use-module (crates-io))

(define-public crate-drunken-diver-0.1 (crate (name "drunken-diver") (vers "0.1.0") (deps (list (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "0vsvzmh1hg7fpm5qiny7pa4086kv8j4ii85izzll0n6jbixmlisk") (yanked #t)))

(define-public crate-drunken-diver-0.1 (crate (name "drunken-diver") (vers "0.1.1") (deps (list (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "0pgm5whkzj1cz2gnxjl7k81y9bpqkyrgapi1xwswli1j9rxw7pv4")))

(define-public crate-drunken_bishop-0.1 (crate (name "drunken_bishop") (vers "0.1.0") (hash "0fbi24pi2hkw8f9bi0in3hyi73bs478i8x147ryph365cdfmqzjf")))

(define-public crate-drunken_bishop-0.1 (crate (name "drunken_bishop") (vers "0.1.1") (hash "1qdlk7ig0rqki825mf4yq01dzwi5c6258cldllkx2my5xplgqmh6")))

(define-public crate-drunken_bishop-0.1 (crate (name "drunken_bishop") (vers "0.1.2") (hash "0iwbpl33mngjp9ffwpyn8r6pqdabsx0022d1fvg5d6sdjang1ysn")))

(define-public crate-drunkenbishop-0.1 (crate (name "drunkenbishop") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1i6sjd9ndbm0s12cm7l3ajrsmc88m29n5rqxyjyb665s7mca66zi") (features (quote (("default")))) (v 2) (features2 (quote (("hexparse" "dep:hex") ("hash" "dep:sha2"))))))

(define-public crate-drunkenbishop-0.1 (crate (name "drunkenbishop") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1mckqcxlybg9fj2d5jj9vxfbih564kh7lgixgmxy5yd61f6q8vpg") (features (quote (("default")))) (v 2) (features2 (quote (("hexparse" "dep:hex") ("hash" "dep:sha2"))))))

(define-public crate-drunx-1 (crate (name "drunx") (vers "1.7.0") (deps (list (crate-dep (name "assert_cmd") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)))) (hash "0w43b179rva1l3ijmwaklm9ncanv9whhkkjyigb6z75h3jbgqf05")))

(define-public crate-drunx-1 (crate (name "drunx") (vers "1.7.1") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)))) (hash "1lipd2ipqjsn9i71shb5bi41w292dy31wbhvhfzysgs6q190gj7v")))

(define-public crate-drunx-1 (crate (name "drunx") (vers "1.7.2") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)))) (hash "0b7z96z5w93izd44lij40z1raw9wbgmjk47bjhzqj07b16z9xggn")))

