(define-module (crates-io dr ib) #:use-module (crates-io))

(define-public crate-dribble-0.1 (crate (name "dribble") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0b502328v26h1kvq5dcqbdqpg4zpq3nfb7119hiy9j7gmji4pgs4") (features (quote (("unstable"))))))

(define-public crate-dribble-1 (crate (name "dribble") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "09kamynihi3i8n74pfmlbla059apb7asy36s7jamcxiawpgjqac8") (features (quote (("unstable"))))))

