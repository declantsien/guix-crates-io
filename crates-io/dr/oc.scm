(define-module (crates-io dr oc) #:use-module (crates-io))

(define-public crate-drocker-0.1 (crate (name "drocker") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0lgajh387azq5frikr2c0bk7i36laca1q2bl0kcf3nllfxj7hfr6")))

