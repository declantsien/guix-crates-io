(define-module (crates-io dr af) #:use-module (crates-io))

(define-public crate-draff-0.1 (crate (name "draff") (vers "0.1.0") (hash "0kjrrcdv3dzvq361gaq4di7fkrqgnsbi9kihv8kqzv4iy04ck43k")))

(define-public crate-draffle-0.1 (crate (name "draffle") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 2)) (crate-dep (name "bytemuck") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "solana-program-test") (req "^1.7.11") (default-features #t) (kind 2)) (crate-dep (name "solana-sdk") (req "^1.7.11") (default-features #t) (kind 2)) (crate-dep (name "spl-associated-token-account") (req "^1.0.2") (features (quote ("no-entrypoint"))) (default-features #t) (kind 2)) (crate-dep (name "spl-token") (req "^3.1.1") (features (quote ("no-entrypoint"))) (default-features #t) (kind 2)))) (hash "0av1kn3da1bfs9r7i4hq41281vgrx2jiy1cwcmb31qpjbrmkksk5") (features (quote (("production") ("no-idl") ("no-entrypoint") ("default" "production") ("cpi" "no-entrypoint"))))))

(define-public crate-draft-0.0.0 (crate (name "draft") (vers "0.0.0") (hash "1qviadljqx4lwqshlql3db4lffyzaxkgllr057dvgzijhv5yr7y7")))

