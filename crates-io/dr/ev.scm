(define-module (crates-io dr ev) #:use-module (crates-io))

(define-public crate-drevnehulin_guessing_game-0.1 (crate (name "drevnehulin_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15d2lj1xw7jamr69bhg2i93gvax889mc5pj8dn2r1lrgk1n6rdbl")))

(define-public crate-drevnehulin_guessing_game-0.1 (crate (name "drevnehulin_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "10b6d1xkh3bi92jlz3cc9nrwg17wa9c3h9pjxl0wsqb44r3798mf")))

