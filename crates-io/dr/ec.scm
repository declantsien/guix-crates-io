(define-module (crates-io dr ec) #:use-module (crates-io))

(define-public crate-dreck-0.1 (crate (name "dreck") (vers "0.1.0") (deps (list (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "1s5ml7kh262wihzypzkcld460pajj4vfqhmddlsc17v3k887rk9j")))

(define-public crate-dreck-0.1 (crate (name "dreck") (vers "0.1.1") (deps (list (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "04lxycw3gjvbcr9c1im140zrv1cp7425km4szbkh0f8bx7va3xg8")))

