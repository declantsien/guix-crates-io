(define-module (crates-io dr oo) #:use-module (crates-io))

(define-public crate-droom-ui-0.0.1 (crate (name "droom-ui") (vers "0.0.1") (deps (list (crate-dep (name "iup-sys") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rn93y2l037xnlnmww02dffdl5kmhhphyj9hamnf7yqc7s4a7a9m") (yanked #t)))

