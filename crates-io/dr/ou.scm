(define-module (crates-io dr ou) #:use-module (crates-io))

(define-public crate-drought-0.0.0 (crate (name "drought") (vers "0.0.0") (deps (list (crate-dep (name "drought_macros") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)))) (hash "10mpvhhhrgb2hhg3zpr8qzshzq4l4lli28yk8997l6yx5l75pf6c") (features (quote (("default" "macro")))) (v 2) (features2 (quote (("macro" "dep:drought_macros"))))))

(define-public crate-drought-0.0.1 (crate (name "drought") (vers "0.0.1") (deps (list (crate-dep (name "drought_macros") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0f2xd8wz14vk2fwwmcsnnlsd3sgp896xzp41978vsw7y15qppnm6") (features (quote (("default" "macro" "test")))) (v 2) (features2 (quote (("test" "dep:tokio") ("macro" "dep:drought_macros"))))))

(define-public crate-drought_macros-0.0.0 (crate (name "drought_macros") (vers "0.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "194bgvz96c9fl7n41jmy0iy2wsiicz4d0cfm3wyqrhc35x8n5nmk")))

(define-public crate-drought_macros-0.0.1 (crate (name "drought_macros") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f5w1bb56a0hr68m5pjxnlwr1kki6syhv6bxns32bv32prqjrfhl")))

(define-public crate-drought_macros-0.0.2 (crate (name "drought_macros") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0ki7z0crsb8qkzfhq7zcfhbz01y9zi2z9vk04xqfpaj3ff0sffv5")))

(define-public crate-drout-0.1 (crate (name "drout") (vers "0.1.0") (hash "0ilkgd9r4h7nxdczxj566zl9y3w2c5p4y39ah0krz3b9mpi8ap4z") (yanked #t)))

(define-public crate-drout-0.1 (crate (name "drout") (vers "0.1.1") (hash "1cs745k0id01603svgraj7px5a3ll29452ibxnp8zqp2dlbvhihj") (yanked #t)))

