(define-module (crates-io dr ea) #:use-module (crates-io))

(define-public crate-dreadfort-0.1 (crate (name "dreadfort") (vers "0.1.0") (deps (list (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_trace" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "volmark") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0byq8j55mj88bb7giniyphpdsxnsfkwacn985dpkxn1ld67xz4gn")))

(define-public crate-dream-0.0.0 (crate (name "dream") (vers "0.0.0") (hash "0lkdyl3sjxg21i4xmbx36cndip3zl32xczp81106nai7n2mghda9")))

(define-public crate-dreamer-0.1 (crate (name "dreamer") (vers "0.1.0") (hash "146fnpkmv2gfgh30dx84bmgm4238j66z5lvdbqmlf6cpyqklvhsv")))

(define-public crate-dreamers-0.1 (crate (name "dreamers") (vers "0.1.0") (hash "1l9inapcz1b50801k5gfnavlb3icyjqvlbplsyd5amlpp3bf2viq")))

(define-public crate-dreamlogic-0.1 (crate (name "dreamlogic") (vers "0.1.0") (hash "1hiwifcablwfpgdnr1rjjfrirp6qgglr2zqqp8ysqjgr0h84aijs")))

(define-public crate-dreams-0.0.1 (crate (name "dreams") (vers "0.0.1") (deps (list (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)))) (hash "03f2srpmfpvjkx8dgv4gqjyxim7pcrnwdls90ppvqrf4faz6mw8r") (features (quote (("web_warp" "warp" "url")))) (yanked #t)))

(define-public crate-dreamstone-0.1 (crate (name "dreamstone") (vers "0.1.0") (hash "0mbml9s531zf2pq3aiarzyz6kb7k5f2gpj4nvw8s4xladmjckwlp")))

(define-public crate-dreamy-0.0.1 (crate (name "dreamy") (vers "0.0.1") (hash "1fdm62qdzbc5ww7qd68yhh9lpryjw4ybmn05h533mgmjgy53lfh3")))

