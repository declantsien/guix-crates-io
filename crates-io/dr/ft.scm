(define-module (crates-io dr ft) #:use-module (crates-io))

(define-public crate-drftr-0.1 (crate (name "drftr") (vers "0.1.0") (deps (list (crate-dep (name "poise") (req "^0.5.5") (features (quote ("chrono"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1q71lk32v6mj4xvpcigshd6s97618995wi3cc4q824ckgrswi0xr")))

(define-public crate-drftr-0.1 (crate (name "drftr") (vers "0.1.1") (deps (list (crate-dep (name "poise") (req "^0.5.5") (features (quote ("chrono"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1zpmws4vsb3ajxy968zzn5lxxpaqvr5q7syjsy30kz20acychk91")))

