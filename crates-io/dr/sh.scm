(define-module (crates-io dr sh) #:use-module (crates-io))

(define-public crate-drsh4dow_testing_mini_crate_that_do_nothing-0.1 (crate (name "drsh4dow_testing_mini_crate_that_do_nothing") (vers "0.1.0") (hash "1i1clwlcf9agir90096qrhvxk9av8cd0z4x82ar0r7ky5km6ipga")))

(define-public crate-drsh4dow_testing_mini_crate_that_do_nothing-0.1 (crate (name "drsh4dow_testing_mini_crate_that_do_nothing") (vers "0.1.1") (hash "0x26zia9mfsdxjly240rknhpyim767ihnnjxn6zcs9vaqb9jzx7f")))

(define-public crate-drsh4dow_testing_mini_crate_that_do_nothing-0.1 (crate (name "drsh4dow_testing_mini_crate_that_do_nothing") (vers "0.1.2") (hash "1xjf30nhdig2g6nsx9wrhydnn58xv1fv01yd7cn95gr592bkbhm2")))

