(define-module (crates-io dr ep) #:use-module (crates-io))

(define-public crate-drep-0.1 (crate (name "drep") (vers "0.1.2") (deps (list (crate-dep (name "notify") (req "^4.0.12") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "07xqcpg6jq4qdpn3bn58zbwq0c26ir8hhpyqxkv798jbwqy0c27p")))

(define-public crate-drep-0.1 (crate (name "drep") (vers "0.1.3") (deps (list (crate-dep (name "notify") (req "^4.0.12") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "000a0sgdlincq0fidfgf8n0qvm7f7qwsmh8mqkbs3wmrlh3q7nbc")))

