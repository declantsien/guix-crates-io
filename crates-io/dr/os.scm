(define-module (crates-io dr os) #:use-module (crates-io))

(define-public crate-drosera-0.1 (crate (name "drosera") (vers "0.1.0") (hash "0ar95gshxj1mng44yqvdvc5wjw4033zikpyivrsyif3dhrq20lll")))

(define-public crate-drosera-0.2 (crate (name "drosera") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.3") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zvm2fxn9zys8ddsk7d7qw0jy162bfds7gx6dhncra611k8rc6yv")))

(define-public crate-drosera-0.3 (crate (name "drosera") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0acma6k2wjncnr4jp620ysl461fi9xi8dkr8inq57mw5kddmjyr7")))

(define-public crate-drosera-0.3 (crate (name "drosera") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ph2qpqq4nnpambi10iqlszsmb89a24yzs82x32b7vl6qd10l873")))

(define-public crate-drosera-0.3 (crate (name "drosera") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0k3vd31s9xjn8k8dh041kzc02c1qm2wndgpgfwyki3dr2jyrvjjj")))

(define-public crate-dross-0.1 (crate (name "dross") (vers "0.1.0") (hash "037xgdgv618sh21m6f4ds4qvx385hb20kd9mgfhb2daha66khmf3")))

(define-public crate-drossel-0.1 (crate (name "drossel") (vers "0.1.0") (deps (list (crate-dep (name "drossel-journal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "leveldb") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "strand") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12053fsdq240z17ll136kg9w4zfppb9qydbngszc1fyac6cxd8k3")))

(define-public crate-drossel-journal-0.1 (crate (name "drossel-journal") (vers "0.1.0") (deps (list (crate-dep (name "db-key") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "leveldb") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "015nv5mlk9dybnizibyncnw54l1z9vk506c579nc3096ahm9c9v0")))

(define-public crate-drossel-journal-0.1 (crate (name "drossel-journal") (vers "0.1.1") (deps (list (crate-dep (name "db-key") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "leveldb") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1psp6hjh3gn8lzk0gf9xcyyihmqwr5bl3nnyfhar1xdwniymlx46")))

(define-public crate-drossel-journal-0.1 (crate (name "drossel-journal") (vers "0.1.2") (deps (list (crate-dep (name "db-key") (req "*") (default-features #t) (kind 0)) (crate-dep (name "leveldb") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0hpd3d0d190vzxpvrcgrdmh95fihyk7apkarnsv1418a7i69sqhf")))

