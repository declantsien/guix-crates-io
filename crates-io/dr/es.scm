(define-module (crates-io dr es) #:use-module (crates-io))

(define-public crate-dresscode-0.1 (crate (name "dresscode") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0ri35lrdni77xlsalcm0d5jhwh956gq23cm756p7zpfy1ghhpp9x")))

