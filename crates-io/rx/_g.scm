(define-module (crates-io rx _g) #:use-module (crates-io))

(define-public crate-rx_gtk-0.1 (crate (name "rx_gtk") (vers "0.1.0") (deps (list (crate-dep (name "gdk") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0") (features (quote ("v3_10"))) (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rxrs") (req "^0.1.0-alpha2") (default-features #t) (kind 0)))) (hash "1am6bzzvm290h5cv0lfad226fg9jcf8facf02r3hh956wm1lsj4z")))

(define-public crate-rx_gtk-0.1 (crate (name "rx_gtk") (vers "0.1.1") (deps (list (crate-dep (name "gdk") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0") (features (quote ("v3_10"))) (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rxrs") (req "^0.1.0-alpha3") (default-features #t) (kind 0)))) (hash "0by0r3zc983vp89134wmibs3rk5yagm6mqp7hmrkh82k8nb93fj5")))

