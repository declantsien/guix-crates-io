(define-module (crates-io rx cv) #:use-module (crates-io))

(define-public crate-rxcv-0.1 (crate (name "rxcv") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0q2x8icyzhcnmsl4n5r2az99z647gjvmh48icsbshsgghl7skx6p")))

(define-public crate-rxcv-0.1 (crate (name "rxcv") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0np3qsllacnydihpyws1k5k5bazksjjkikfn4q4k5j7w1j9xdhyj")))

(define-public crate-rxcv-0.1 (crate (name "rxcv") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0zpcyd7czqs6cmp0x1z21s0mpmv03xqkk4g4jkxq8vyzzfvqk68y")))

(define-public crate-rxcv-0.1 (crate (name "rxcv") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ip7hpj8wwh6s2dx5qvfimwdmgzds234kdg8vyr5k0q81307xc3w")))

(define-public crate-rxcv-0.1 (crate (name "rxcv") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1fs27gy69s48650kh91cbbk54yfcaiikbmjvk0mcpcz5pc04jpms")))

