(define-module (crates-io rx sc) #:use-module (crates-io))

(define-public crate-rxscreen-0.1 (crate (name "rxscreen") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)))) (hash "1vsmmb4cxrsali2xk4d88w3ip0w9ffxvqbnqdx075mwlc40i6jhk") (features (quote (("save" "image"))))))

(define-public crate-rxscreen-0.1 (crate (name "rxscreen") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)))) (hash "1gjrwvymbvk84mpa4wikdl3manbh53vn06ql20wxf2vlvavlanr7") (features (quote (("save" "image"))))))

(define-public crate-rxscreen-0.1 (crate (name "rxscreen") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)))) (hash "1azl35vmrkisbgfc8lgscy2xpapilds76c6qzdrr98v08krp4x29") (features (quote (("save" "image"))))))

(define-public crate-rxscreen-0.1 (crate (name "rxscreen") (vers "0.1.4") (deps (list (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jdfvr1hr9br90z1c7j6dhfnsvj7ssdr5pmkc5y7lr0ayi8nnx4l") (features (quote (("xrandr") ("shm") ("save" "image"))))))

(define-public crate-rxscreen-0.1 (crate (name "rxscreen") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cm5mcmvkcik3iff6hi0g8lapl650kn4wh7gdaahnk0ljhgwdgyy") (features (quote (("xrandr") ("shm")))) (v 2) (features2 (quote (("save" "dep:image"))))))

(define-public crate-rxscreen-0.1 (crate (name "rxscreen") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qmkivvblm7qa32r6vfsblvx4h2377ci60ajj4ka4la24z7jz7zn") (features (quote (("xrandr") ("shm")))) (v 2) (features2 (quote (("save" "dep:image"))))))

(define-public crate-rxscreen-0.1 (crate (name "rxscreen") (vers "0.1.7") (deps (list (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15pwv6x7bb7zv3swm799kk2zcb4rjis4h38mnxr84fjhhczv3fmg") (features (quote (("xrandr") ("shm") ("mouse")))) (v 2) (features2 (quote (("save" "dep:image"))))))

