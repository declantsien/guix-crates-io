(define-module (crates-io rx sy) #:use-module (crates-io))

(define-public crate-rxsync-0.1 (crate (name "rxsync") (vers "0.1.0") (deps (list (crate-dep (name "adler") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1s7z22r4bkr0nihkjvdcar2cxclas6427bj3fn7k8kixj1cygrcn")))

