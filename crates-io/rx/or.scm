(define-module (crates-io rx or) #:use-module (crates-io))

(define-public crate-rxor-0.1 (crate (name "rxor") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "1qlkky25z666z6bx1ff1xc7k362ahq61amxxcfw1c9ihd7fza19a")))

(define-public crate-rxor-0.2 (crate (name "rxor") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "0kdgsmynn68zby69sqi71f48x4ackz77p6lvi0f6k9g5xdq4dpvz")))

