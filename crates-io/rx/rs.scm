(define-module (crates-io rx rs) #:use-module (crates-io))

(define-public crate-rxrs-0.1 (crate (name "rxrs") (vers "0.1.0-alpha0") (hash "1gplgkfdfvjwv0q681slqi8lwrfi7yajr3srd0dx18sd19kzib31")))

(define-public crate-rxrs-0.1 (crate (name "rxrs") (vers "0.1.0-alpha1") (hash "14dy9brzfvndi5ys1zl55agi59x3zdxzqmq1i8d3c56xplnpxaxg")))

(define-public crate-rxrs-0.1 (crate (name "rxrs") (vers "0.1.0-alpha2") (hash "1lbqhp7rv7132sdvlmdv1bczc67v8whwbiprlnf4xbc48699b2ax")))

(define-public crate-rxrs-0.1 (crate (name "rxrs") (vers "0.1.0-alpha3") (hash "1rq0qjlsjcx8rsxn0nl7kz2gbkr0f70lsi3nv5r32lk7mrrhkbkm")))

(define-public crate-rxrs-0.1 (crate (name "rxrs") (vers "0.1.0-alpha4") (hash "0r4v7vqv6s2xkwfxl47n52lnh2aygkhkblq4vx7pjmqn9msb2ly9")))

(define-public crate-rxrs-0.2 (crate (name "rxrs") (vers "0.2.0-beta0") (hash "1qk05732jlvilr74falyaqml0653hdlc5k5fjca8vlnzz0a9ahlg")))

(define-public crate-rxrs-0.2 (crate (name "rxrs") (vers "0.2.0-beta1") (hash "0g7665g3n267bxv2hhf28jh4d96smay0qsxj00bk2nbikhfzvhvh")))

(define-public crate-rxrs-0.2 (crate (name "rxrs") (vers "0.2.0-beta2") (hash "06h917x5al7z5wwphg0nxl22vbjcykp80m3yi1k7vd0r75sn4a45")))

(define-public crate-rxrs-0.2 (crate (name "rxrs") (vers "0.2.0-beta3") (hash "0si9r79bbkf27454q9zlnwv3y9zn3gh91h4wm3mpfafv47ybp7lf")))

