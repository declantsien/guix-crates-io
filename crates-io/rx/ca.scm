(define-module (crates-io rx ca) #:use-module (crates-io))

(define-public crate-rxcalc-1 (crate (name "rxcalc") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0wip46zpai3s5mlyn1m06xlnv9268z1qs0xilkqwi0g6h5niwyb1") (yanked #t)))

(define-public crate-rxcalc-1 (crate (name "rxcalc") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "11pma1gj311c9cjnxxnckyl197my17qg7wqpidw5264h5kz2kxas") (yanked #t)))

(define-public crate-rxcalc-1 (crate (name "rxcalc") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0g6w3p5qq6k7xwn3mcd34is4v727y6mmshdbma318d1kzc075kq2") (yanked #t)))

(define-public crate-rxcalc-1 (crate (name "rxcalc") (vers "1.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1jfsahb8bif5ljw15r405rrp1hcknwnsgqr9rginxpqbzh89zkhn") (yanked #t)))

(define-public crate-rxcalc-1 (crate (name "rxcalc") (vers "1.0.4") (hash "1q63r74606c9484b5dg1xbdf29ax6zqrkgjya9ds39xvpr9ww3hn") (yanked #t)))

(define-public crate-rxcalc-1 (crate (name "rxcalc") (vers "1.0.5") (hash "1wmcyx2pci54s662pf72i78i47yj3i3kxjph9h2ls4kacyafr682")))

