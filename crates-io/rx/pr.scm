(define-module (crates-io rx pr) #:use-module (crates-io))

(define-public crate-rxpr-0.1 (crate (name "rxpr") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1mhf5pc1b3w8ky3d2n78v2raqk4lwgh96bxz93zwrkkx3razdsh9")))

(define-public crate-rxpr-0.1 (crate (name "rxpr") (vers "0.1.1-dev.1") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "15yjs8h5x5k2820c9kjk5gqfvh7ccgws56zhbdz7incyiw3ghmgh")))

(define-public crate-rxpr-0.1 (crate (name "rxpr") (vers "0.1.1-dev.2") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1is7hqsr3k6c4fnvlfsdsdfy2ha4qkh6cwxk05l752vgvhrphha7")))

(define-public crate-rxpr-0.1 (crate (name "rxpr") (vers "0.1.1") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "06g8x9f7l1p1bi9skglfvln17nc8m3r8hmsmszqwsrvn4g36zrq9")))

(define-public crate-rxpr-0.1 (crate (name "rxpr") (vers "0.1.2") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0ipvz1m5x4d81gp0kjkiwwxz62byj7rcwf843n2sf9kfz3f5gylw")))

(define-public crate-rxpr-1 (crate (name "rxpr") (vers "1.0.0") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "17jpqaa40fzw6s6b006fq1r5dnkzr09a53ninfpvw7s398iqcd1v")))

(define-public crate-rxprog-1 (crate (name "rxprog") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ihex") (req "^3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "srec") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "0qbq1rpdcrfy3alg4dzgargsfycf8llz6qign1lhaaxswibsagi8") (features (quote (("rxprog-cli" "clap" "ihex" "srec"))))))

(define-public crate-rxprog-1 (crate (name "rxprog") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ihex") (req "^3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "srec") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "14pc5i9liqbzaxvwr5nkqv27rhmzs71sy3ws0hv157s5i72w40am") (features (quote (("rxprog-cli" "clap" "ihex" "srec"))))))

(define-public crate-rxprog-1 (crate (name "rxprog") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ihex") (req "^3.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "srec") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0spsvx7rmz025imkc2q8zx4a301d6c79lcmm1k712fq0dbi4srcs") (features (quote (("rxprog-cli" "clap" "ihex" "srec"))))))

