(define-module (crates-io k3 #{2l}#) #:use-module (crates-io))

(define-public crate-k32l3a60-cm0plus-0.1 (crate (name "k32l3a60-cm0plus") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7.3") (features (quote ("device"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0bjy5wp47sizy5yi68vgaqfz45dhjpa7n1z6b2aqgapzr49f0hia") (v 2) (features2 (quote (("rt" "dep:cortex-m-rt"))))))

(define-public crate-k32l3a60-cm4-0.1 (crate (name "k32l3a60-cm4") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7.3") (features (quote ("device"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0qh5hj95rfm5phn1m61x9ldwcza2rg9a3fqgmdskddbc4xcwsc5c") (v 2) (features2 (quote (("rt" "dep:cortex-m-rt"))))))

