(define-module (crates-io iu li) #:use-module (crates-io))

(define-public crate-iuliia-0.1 (crate (name "iuliia") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0bab64x9m6x5d6pnd99cm0d3bd6csmlvsxzxjjqkh2kvfv1n98zg")))

(define-public crate-iuliia-0.1 (crate (name "iuliia") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "05pmgj8v6vzbwv76hjmwwq91lbxnxirrd66gzg8a36g02ryjs8gk")))

(define-public crate-iuliia-0.1 (crate (name "iuliia") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "084hs18zrj5qg4spflx2w8zx1vd3brdgj4zcz0n2gh48dcwl06bj")))

(define-public crate-iuliia-rust-0.1 (crate (name "iuliia-rust") (vers "0.1.0") (deps (list (crate-dep (name "include_dir") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11jw8yk7yxwdpax9la81mbs1g58a324hazvhhy0hhfj5xxaqdgxc")))

(define-public crate-iuliia-rust-0.1 (crate (name "iuliia-rust") (vers "0.1.1") (deps (list (crate-dep (name "include_dir") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1svc9lqz232dc8zdqpms36bmm94i7v5idpjhrhxiwqi6r8mi2gxh")))

