(define-module (crates-io iu no) #:use-module (crates-io))

(define-public crate-iunorm-0.1 (crate (name "iunorm") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0caq9qd81v66g069dy9v4qbai419sx3p4rj479pf4xiqnbqybia0") (rust-version "1.58.1")))

(define-public crate-iunorm-0.1 (crate (name "iunorm") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1ycqpk0x3iazcmm7g0shddsxs1rj29mqsch8py9r0h1k5wgzl0l6") (rust-version "1.58.1")))

(define-public crate-iunorm-0.1 (crate (name "iunorm") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "twofloat") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "1a7xsgz3m89fd823p6s6i8gwyfcqpx33nw9my1bfbay8l05y6ylm") (features (quote (("std" "twofloat")))) (rust-version "1.58.1")))

(define-public crate-iunorm-0.2 (crate (name "iunorm") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (kind 2)) (crate-dep (name "paste") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "1v79vshnkd4ydlb98wkdxbnw94l36czdfjsi5mnih9g28ghlxm91") (features (quote (("std") ("default" "std")))) (rust-version "1.58.1")))

(define-public crate-iunorm-0.2 (crate (name "iunorm") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (kind 2)) (crate-dep (name "paste") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "0jb0i25c3ywwg6nnwav4qhax1gvkqmh8qnjdfply8xpq314hp634") (features (quote (("std") ("default" "std")))) (rust-version "1.43.1")))

(define-public crate-iunorm-0.2 (crate (name "iunorm") (vers "0.2.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (kind 2)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "18is7cdry6m6rcglm90m2714y23khcsw200h5svcxfgzn7zaix2s") (features (quote (("std") ("nightly_docs" "std") ("default")))) (rust-version "1.43.1")))

