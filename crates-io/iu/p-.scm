(define-module (crates-io iu p-) #:use-module (crates-io))

(define-public crate-iup-sys-0.0.1 (crate (name "iup-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "02n4bvpsd6za08hwh9l3nh5lkccm1arnyynll5hw3xczsqa79z9f")))

(define-public crate-iup-sys-0.0.2 (crate (name "iup-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ngps0zx1dli24cc5q46mz9d1fr5d6r2aqs9cdfl5j60yvprc734")))

(define-public crate-iup-sys-0.0.3 (crate (name "iup-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "13xn8dlbix33knc6j2xqx4qcx9wr6l19wphnv4z6qfv4vjjx3ngj")))

