(define-module (crates-io el r_) #:use-module (crates-io))

(define-public crate-elr_primes-0.1 (crate (name "elr_primes") (vers "0.1.0") (hash "00ij62zd019925r3nni7aq7xbkb40nv7gypz03v0c4iyrbvzxc44")))

(define-public crate-elr_primes-0.1 (crate (name "elr_primes") (vers "0.1.1") (hash "110qjc7416sn269fqw18i9d43mmd25slfy0wxw8ia1v2bc1287s4")))

(define-public crate-elr_primes-0.1 (crate (name "elr_primes") (vers "0.1.2") (hash "1i61i6dkbx77k3g34ql1ijv8rl91sxwvmi0hnjnn7r6gi2p2l187")))

