(define-module (crates-io el fp) #:use-module (crates-io))

(define-public crate-elfpromote-0.1 (crate (name "elfpromote") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "atomicwrites") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0bj5798vi24mcm1x9pcgnmh8zlwcadfw12823v2vlcskxv0rll59")))

(define-public crate-elfps-0.1 (crate (name "elfps") (vers "0.1.0") (hash "1063kd8v0nqw5vb5vzdfwnh6p5n9qi0cs11g15pifwfky1cnngis") (yanked #t)))

