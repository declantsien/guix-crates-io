(define-module (crates-io el ma) #:use-module (crates-io))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0nwp8r2sf0wzffw2hw7kkxr2lzwmb00ff6nnpjp8kv2z01pycvqz")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0izd492aas5jd4g8fdd3wp7fbdw7p54g45cc3l5465sdrqqmh4nf")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0y94dhn654x2wnl7rwd4kg8rqs43ygaqp6977w6nk01mnbm58dfw")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "077gbdskn0bz1j9xdnqy88wbpydj4r2pmkd4wqhz8fys5gxx6qgs")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "134a2kxgakjwh87yjk5iskjkw8isx12l33kgv8aq3kchsfvg4pxx")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0l57nbcfyiaj1h292iri8fd7f357w2wz3gk1vnl0g23jq4q2gmdg")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pvp1s6g6by1q12jn0d3rw1vqviwpmrl3xj0myyxqm8p8jzb17jk")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "19ib5zbsikbqrxan4wxx28z0sj768iw9j0hcflcyfhmwi6qrq9v2")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.8") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1w2bsxbdn4a0i6jrqjf3rlqymkmph7hy7b9g4f95m9096ik952k3")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.9") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0iz2gd23n6ykwrpgkjmwv2hkvx17wzhppvhg1brjqjipjkxamknk")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.10") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0csr3wh5xyn1mmxzrs19z9z12ianvnzk7a5siwv4918amv30wmzl")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.11") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fkyb4prgklywc7ib3fwh2c3lrpzc03wr8rf403azijrxrlp0yyv")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.12") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0cciqfqx6hvxi547d0xg49nhnb56sb5j4xrzrk9br7fkc2mip3bf")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.13") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0lwn8zqbsnhjfks1qwn9v2d2n2qisx8c6lhfhd8y3y729qqlv2j2")))

(define-public crate-elma-0.1 (crate (name "elma") (vers "0.1.14") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.3") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1d1akkw87dshj7p7yr5q7xy28l5farv4c6pkliygv268h6l058ji")))

(define-public crate-elma-lgr-0.1 (crate (name "elma-lgr") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "1sbifq9di85n18nb5xkrpf0hsv76k26mxzsi99jhkaxvf47y4svj")))

(define-public crate-elma-lgr-0.2 (crate (name "elma-lgr") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pcx") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0n3j250dsb6lp876yzhld4wmxkjkj746fhvhaylgy22r4pvv3y8c")))

(define-public crate-elmar-mppt-0.1 (crate (name "elmar-mppt") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "116bzq35sbg1izqm5phwfm7zkvbdgmmznzzprif210da7818h5sk")))

(define-public crate-elmar-mppt-0.1 (crate (name "elmar-mppt") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bxcan") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1m4riwm2gqnr9jhs7x1pwl0bckf12n8jkzwmj12whpymrkmwy40p")))

