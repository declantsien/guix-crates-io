(define-module (crates-io el if) #:use-module (crates-io))

(define-public crate-elif-0.0.1 (crate (name "elif") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.3") (features (quote ("chrono"))) (default-features #t) (kind 0)))) (hash "1f02ks1irvr4b5jdbv7yfzv16mq0ah9s7hb47y855jdwakb7kqga")))

(define-public crate-elifim-0.1 (crate (name "elifim") (vers "0.1.0") (hash "040bpdiymc8vp0fixjj70jkql85mp44vv75fwghl36j2gzgialss")))

(define-public crate-elifim-0.2 (crate (name "elifim") (vers "0.2.0") (deps (list (crate-dep (name "bevy") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1hikiq3yjrw0r28nmh58kxkq0iihv1m610s4bx4nz1bs7m1c0spn")))

