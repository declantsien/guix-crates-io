(define-module (crates-io el fl) #:use-module (crates-io))

(define-public crate-elflib-0.1 (crate (name "elflib") (vers "0.1.0") (deps (list (crate-dep (name "binary_serde") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "elflib_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "116m8n7p2k464lzyd8rzf6hrknam8af6rkdpa0y1vbncfhmcps35") (features (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

(define-public crate-elflib-0.1 (crate (name "elflib") (vers "0.1.1") (deps (list (crate-dep (name "binary_serde") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "elflib_macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0n5sj2nsvw9f0drci1q4plr0by7zfzl9dwk5psp95c8k5m7k73gr") (features (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

(define-public crate-elflib-0.1 (crate (name "elflib") (vers "0.1.2") (deps (list (crate-dep (name "binary_serde") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "elflib_macros") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0vdzwv2yhir4r65rcqrw8av7sxb9nfynhb6hwzwx2h91jbljvrx5") (features (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

(define-public crate-elflib-0.1 (crate (name "elflib") (vers "0.1.3") (deps (list (crate-dep (name "binary_serde") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "elflib_macros") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "1873gdiwqz69k6ghwnmxilnk4lbbcxkf87l1q0b5dafcskgrd6wb") (features (quote (("std" "binary_serde/std" "thiserror-no-std/std"))))))

(define-public crate-elflib_macros-0.1 (crate (name "elflib_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ax57r0p5hsh194av2rq978xgg0fsmzab1id96100zmp4k7sx8m1")))

(define-public crate-elflib_macros-0.1 (crate (name "elflib_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "196yd0pq3bq5rdaa1k1n1fkcknjqwypcr3n333r5rnvg10v00dbp")))

(define-public crate-elflib_macros-0.1 (crate (name "elflib_macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19161wqsf0kp157vi5d44nwc1j51g0p3mhzbcvs99b3pi2g37ncf")))

(define-public crate-elflib_macros-0.1 (crate (name "elflib_macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ajdm7f95schhn943i251zjvd5gj77fn7kzb4nl3mn9v83ywszf3")))

(define-public crate-elfloader-0.0.1 (crate (name "elfloader") (vers "0.0.1") (hash "10grrywsplibq0ch7csyiqy143kzkvghg22bd7mpg7p0y7y0n97h")))

(define-public crate-elfloader-0.0.2 (crate (name "elfloader") (vers "0.0.2") (hash "0p01i7iwqhiyrdqlbjkn5r1iiv69qa9rfzgbf8h248w5x2kinvx4")))

(define-public crate-elfloader-0.0.3 (crate (name "elfloader") (vers "0.0.3") (hash "1z6qhf4y3lq3v5sk2basrwsimgwqb8ypkwx96sri66g1vihksv24")))

(define-public crate-elfloader-0.0.4 (crate (name "elfloader") (vers "0.0.4") (hash "0l0c9pjrd5fjy8p1rzr98qnrj9hlm67i8cp6hjzl738gvzaq87y8")))

(define-public crate-elfloader-0.0.5 (crate (name "elfloader") (vers "0.0.5") (hash "14cs36qs3ws2b989xx84h0j6dmivlgks3ljzbsmydmpzaym9m1ry")))

(define-public crate-elfloader-0.9 (crate (name "elfloader") (vers "0.9.0") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.7") (default-features #t) (kind 0)))) (hash "0disacc5baz9zp5yvx19325rkhfr885lvbknc7dss7aa0j3m4fyd")))

(define-public crate-elfloader-0.10 (crate (name "elfloader") (vers "0.10.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.7") (default-features #t) (kind 0)))) (hash "0n6zgfg0q59q5kyrrk52a3rhjb6mxgalgl7k1jnj8h36v115gasx")))

(define-public crate-elfloader-0.11 (crate (name "elfloader") (vers "0.11.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.7") (default-features #t) (kind 0)))) (hash "14m1nzcgjjwqzrd29hfcivnzz59g6d1rjlzbkqj4n6dla7jil55i")))

(define-public crate-elfloader-0.12 (crate (name "elfloader") (vers "0.12.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ixibzmkx5laxzw4lq2dd0d57fz8843pn3ji20wpv87c5iafys73")))

(define-public crate-elfloader-0.13 (crate (name "elfloader") (vers "0.13.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.7") (default-features #t) (kind 0)))) (hash "16pqbm2qqkaf0khqj1qrc82fxh9yjzqjsysvlnp8bxyj4spimqki")))

(define-public crate-elfloader-0.14 (crate (name "elfloader") (vers "0.14.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.8") (default-features #t) (kind 0)))) (hash "017z8r0q8kh8idn9kdbpdjfdijl997l7bzbvrfizafqgsnlxcjd2")))

(define-public crate-elfloader-0.15 (crate (name "elfloader") (vers "0.15.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.8") (default-features #t) (kind 0)))) (hash "10cw47j2haqi5masrrh780ax4khy2v48wfc5hb01pqbbvkaipq13")))

(define-public crate-elfloader-0.16 (crate (name "elfloader") (vers "0.16.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.8") (default-features #t) (kind 0)))) (hash "0fwgwww6xl2lqz0ajhhbw1jffihv3z7jkv63b6n3pv7qbg9ihyva")))

(define-public crate-elfloader32-0.0.3 (crate (name "elfloader32") (vers "0.0.3") (hash "1gxgysdhishf09s84gv9hwcaz70k13v4ipfqqq27217ax5qqvjvj")))

