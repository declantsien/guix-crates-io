(define-module (crates-io el fi) #:use-module (crates-io))

(define-public crate-elfio-0.3 (crate (name "elfio") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1a585hgw962ppfjsxj9as9byqqnc0wwivf5r92q86d6kn5j935z6")))

(define-public crate-elfio-0.3 (crate (name "elfio") (vers "0.3.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0wyqm9z6xiigmy63cxbywwss76xdpki39pxdj2bh13kachqq4vl8")))

(define-public crate-elfio-0.3 (crate (name "elfio") (vers "0.3.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1qq405xr6wf98kf6q4i01b547s7w6m10qnxmn8zy7vgp7w7r547j")))

