(define-module (crates-io el tr) #:use-module (crates-io))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.1.0") (deps (list (crate-dep (name "gio") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "08niijxdbfm45pj1ib3rc05fzxvrcmc2xngn07z6lh5csbc2a7j2")))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.1.1") (deps (list (crate-dep (name "gio") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1mj0w30p9pf7rddhy6yfskwjyr5fjjmrdk0pijisfzbvr6d8d070")))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.2.0") (deps (list (crate-dep (name "gio") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "15mh9zya59ill72b06s4dfm07381kp55w53fqj51caca82i9jhnh")))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.3.0") (deps (list (crate-dep (name "gio") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1h4kxbynx88z4pj6jyvphs6l4jikgsnbnwvpdj4phx27r322n4a2")))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.4.0") (deps (list (crate-dep (name "gio") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0kjaihhvf9j4qgk0mhvak9ww90ydhx18y35y5r3j5iv0p74x8piy")))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.5.0") (deps (list (crate-dep (name "gio") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0d2lwbbkswsxr2hj4g2gmqjxv357d18p686xph4bj8plcydijzzz")))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.6.0") (deps (list (crate-dep (name "gio") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0z74fhnnczm6cqb725ivaf8c7lzw2pd2cxf7ma9ay8si4kc0lwjp")))

(define-public crate-eltrafico-1 (crate (name "eltrafico") (vers "1.7.0") (deps (list (crate-dep (name "gio") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "017iv08nhw29gg9ikgkg6byh1v0w7lcq8p9ivmc9064in3cv504l")))

(define-public crate-eltrafico-2 (crate (name "eltrafico") (vers "2.0.0") (deps (list (crate-dep (name "gio") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0fpzr2dx0gqxmyrmlp016gp5qzbjnbmf1apwwbb9fp1nmmc5zfi5")))

(define-public crate-eltrafico-2 (crate (name "eltrafico") (vers "2.1.0") (deps (list (crate-dep (name "gio") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "10p24j5f8s5m4z65w8xbmdj21gvbpqiy3rwpbi56jcwfad46ik21")))

(define-public crate-eltrafico-2 (crate (name "eltrafico") (vers "2.1.1") (deps (list (crate-dep (name "gio") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1hc2cammy9nwmqzs7px45mzap6b5fg8izif320hrdjzd1syf0sm4")))

(define-public crate-eltrafico-2 (crate (name "eltrafico") (vers "2.2.1") (deps (list (crate-dep (name "gio") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1agmzffj9m1lsk1x9wczifxmysn6cjjigibfxm1b9nmqdxi36kbm")))

(define-public crate-eltrafico-2 (crate (name "eltrafico") (vers "2.2.3") (deps (list (crate-dep (name "gio") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0xnsn38m961r7xh0hf2a4czwwpblrlydpa2rs10xm3gnp5qq5bzn")))

(define-public crate-eltrafico-2 (crate (name "eltrafico") (vers "2.2.4") (deps (list (crate-dep (name "gio") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1v54ykfh43s9ww75jq53f0hvrk755ssz5yy0gj35cww63ndl85kg")))

(define-public crate-eltrafico-2 (crate (name "eltrafico") (vers "2.3.0") (deps (list (crate-dep (name "gio") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "19pq1p16k7813ba98w7l27gppvmwvx89xqdn7fibkwm060ghgjji")))

