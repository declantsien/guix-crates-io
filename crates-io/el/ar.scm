(define-module (crates-io el ar) #:use-module (crates-io))

(define-public crate-elara-math-0.1 (crate (name "elara-math") (vers "0.1.0") (deps (list (crate-dep (name "elara_log") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0cnr205wa2n6wka1m74wjb74zddq96j3fjmccvg5p8xmpyzbacyc")))

(define-public crate-elara-math-0.1 (crate (name "elara-math") (vers "0.1.1") (deps (list (crate-dep (name "elara_log") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.3") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0jjdv99vkjp696dww4qp9ngxagmsm5hccql18aqmrbbv4h91ll1r")))

(define-public crate-elara_log-0.1 (crate (name "elara_log") (vers "0.1.0") (deps (list (crate-dep (name "sys-time") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0bc8rya44l6ajc1yyaf9bjycf81vifzzksv6xa64mczlhnn5b2az")))

(define-public crate-elaru-0.1 (crate (name "elaru") (vers "0.1.0") (hash "1hsn9cpbk4rkf68vykdrvykh1f1pa7ksiml759bh2bp47mlvjjbp")))

(define-public crate-elaru-0.1 (crate (name "elaru") (vers "0.1.1") (hash "1haskmc70gi9p1hw0jd62j6ncn8skk1ndm4a72n90chab03lpjrd")))

(define-public crate-elaru-0.1 (crate (name "elaru") (vers "0.1.2") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0f4qhgpi09qv18hi6521110cifi10mnncsivsvmh4al40y43z2z8") (features (quote (("default"))))))

(define-public crate-elaru-0.2 (crate (name "elaru") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0gwg71k4kncb1s2p2wy43vsv92zsdb623448yxwvbyshnxxhm2fh") (features (quote (("default"))))))

(define-public crate-elaru-0.2 (crate (name "elaru") (vers "0.2.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0wn79p5cfhmg64d03s7wkxaw2d80phzlx3g4j5q0cbh0nq1d4prk") (features (quote (("unbound") ("default"))))))

