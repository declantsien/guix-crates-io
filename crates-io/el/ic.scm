(define-module (crates-io el ic) #:use-module (crates-io))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.0") (hash "0bmgza6j2p9sd1w33sz2cpd8gmc3grpknxj2w3kpsz13qkq3wkds")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.1") (hash "1wa81bqri0x3vb6ydyz7m1dxnjm66m0qfax36r3whhq2k28h86rb") (yanked #t)))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.1+nightly") (hash "0p04w8401i8lm6fwqz7baqcwjxy4n40b7smi2airxv1j06mh148a") (yanked #t)))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.2") (hash "013lnbkb8wkwkqdr9n2r0rzw1fw064ydjl82kcx8m65c09dbgj4a")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.3") (hash "0kv7f2zg5sjsm4ymnl58a6qk5nclckb1xpizjqjr2kl98c5mkdvk")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.4") (hash "16sb5b6w51s1vsak2bwh9m0dp4bpp9m5dpyciaw6ygpw8i6y0dwg")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.5") (hash "0xqgl6cm445pan97s4qzbbqzb32gs9sk852f1lmqdz5sdi61bfrs")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.6") (hash "11cpimccfjsh3jz1882nw0zqfzxdxs86vgab71swp9cjjs1rzgm8")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.8") (hash "0cj3gggi4ba57v915rcxcnycpmdp3pm53g9gzs2c302y0r4m3j7g")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.10") (hash "093vacnpadjsmr4s4241hzccxq9wfjgsiymqm062rnvv57y62dmf")))

(define-public crate-elicit-0.7 (crate (name "elicit") (vers "0.7.11") (hash "0zgl1x45vp1ck7blykq361iq0b84g3qjfy7f7in5g1pjhh7g9jnf")))

(define-public crate-elicit-0.8 (crate (name "elicit") (vers "0.8.0") (hash "1m6nm77yd2hip5nm04pwpff4gpi1i48pkmqip65h3yiivpk1y3ra")))

(define-public crate-elicit-0.8 (crate (name "elicit") (vers "0.8.1") (hash "0x38krn6372yryb8yqnzcl5cb453r2z6358njfy7504ry6f9hdk0")))

(define-public crate-elicit-0.9 (crate (name "elicit") (vers "0.9.0") (deps (list (crate-dep (name "elicit_macro") (req "^0.1") (kind 0)))) (hash "1bg80lh51v7v1f5yfbm8qgxyp5ri6lxpdm8r6dvfwmydpj4bahqy")))

(define-public crate-elicit-0.9 (crate (name "elicit") (vers "0.9.1") (deps (list (crate-dep (name "elicit_macro") (req "^0.1") (kind 0)))) (hash "01pz02q3x13v2mnb919gdj4zl0hgmvkxcbni7c7vdhi0zpb5gf4a")))

(define-public crate-elicit-0.10 (crate (name "elicit") (vers "0.10.0") (deps (list (crate-dep (name "elicit_macro") (req "^0.2") (kind 0)))) (hash "1837w72ncx612n1r9pdixjdq3dmkjxqiwp3hv0pv3hp6qw5hcvv1")))

(define-public crate-elicit-0.11 (crate (name "elicit") (vers "0.11.0") (deps (list (crate-dep (name "elicit_macro") (req "^0.2") (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (optional #t) (kind 0)))) (hash "1ln1x55l6ia0djxs8v3n4m63vx129i9d9ls63gmyjn7v7m6ppaf8") (features (quote (("default")))) (v 2) (features2 (quote (("parking_lot" "elicit_macro/parking_lot" "dep:parking_lot"))))))

(define-public crate-elicit_macro-0.1 (crate (name "elicit_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "quote") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "parsing" "printing" "proc-macro" "extra-traits" "full"))) (kind 0)))) (hash "1w7hp9s8zv8qp2xpig9an573045rdlcz7qbck846dzvc0il11mf2")))

(define-public crate-elicit_macro-0.1 (crate (name "elicit_macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "quote") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "parsing" "printing" "proc-macro" "extra-traits" "full"))) (kind 0)))) (hash "0578qqwa97i8ww81gilkd49f323nb1x5iv7rywp7v8hbrdbqb7yk")))

(define-public crate-elicit_macro-0.2 (crate (name "elicit_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "quote") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "parsing" "printing" "proc-macro" "extra-traits" "full"))) (kind 0)))) (hash "1isfkajn23jyjx8a146f3d6fd7akhsrx5j3kwh3fy04wvq0v0d5i")))

(define-public crate-elicit_macro-0.2 (crate (name "elicit_macro") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "quote") (req "^1") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "parsing" "printing" "proc-macro" "extra-traits" "full"))) (kind 0)))) (hash "0rwzg48hjb9ly83z7fb3hpivk2mjhr4rn6r8qkcjd7jw4imnbw94") (features (quote (("parking_lot") ("default"))))))

(define-public crate-elicznik-0.1 (crate (name "elicznik") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "0.10.*") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "0.19.*") (features (quote ("with-chrono-0_4"))) (default-features #t) (kind 0)) (crate-dep (name "postgres-openssl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.9") (features (quote ("cookies"))) (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.11.2") (features (quote ("paris" "ansi_term"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bvkfn1x420kncq70l2qpv5wvcrqnp8qpnd8p8y6fyibwh2gvplx")))

