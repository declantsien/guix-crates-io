(define-module (crates-io el fy) #:use-module (crates-io))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.0") (hash "07vvr3qglcjn60h9va808znxnlx47dfmaxh6yhf7pq5bl2g52wqk")))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.1") (hash "0nabbf3db7z73q812dvnrxgmybrh73lmq9q3qllaqi4nzzk8pa09")))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.3") (hash "0dm5fpd12li6jixq91f03agba32q15w3ik6pibdi7ddw9amvaia6")))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.4") (hash "1i37z31gf37r17f32zhh0cr5g17ysfmigsg82r27v4xjfgjynd81")))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.5") (hash "0h14hsdb7vzjm0vjg7470sn5pih53zshdbhfi8iql4sc97ws515j")))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.6") (hash "0f7gbg372fki51v32s0wn4pqc94771f9zx8h88kha5f38icswlg9")))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.7") (hash "0isz8gzncapfpbr2c7q7w7dzr77kn23fwghf93wmgpx3r3nylfji")))

(define-public crate-elfy-0.1 (crate (name "elfy") (vers "0.1.8") (hash "18w6ryhqfqicd00icm9fp206vpcmisadmh4c13grsrm0i5dj3brw")))

(define-public crate-elfy-0.2 (crate (name "elfy") (vers "0.2.0") (hash "0lvljspn7qbcdc181qg0cjr54gn92fk3rqwn3h529d780p8vdwpp")))

(define-public crate-elfy-0.2 (crate (name "elfy") (vers "0.2.1") (hash "1zhisc8ns4yf79mjy0nhsw3q1v917rxajq4ad0mz1a2vrbp4ar69")))

(define-public crate-elfy-0.2 (crate (name "elfy") (vers "0.2.2") (hash "0va3xviiy845iw86s9nc32iq08852g9idxbnxnlv6fcnm0sj39rc")))

