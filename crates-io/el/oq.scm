(define-module (crates-io el oq) #:use-module (crates-io))

(define-public crate-eloquent-0.1 (crate (name "eloquent") (vers "0.1.0") (hash "1a4hrsvxz4cyqxhgnxhwwvd68697wkw51gg19cfi8d3i9mw1ss1n")))

(define-public crate-eloquent-0.1 (crate (name "eloquent") (vers "0.1.1") (deps (list (crate-dep (name "eloquent_core") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11vxdkn8gkfxnysknw54p2ysfijv1b19byrpw734xzflpjqldx72")))

(define-public crate-eloquent-0.1 (crate (name "eloquent") (vers "0.1.2") (deps (list (crate-dep (name "eloquent_core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "165zigldf1lw41as8df55dssylsyn59zpcx8g7gl6a1xv1q32nab")))

(define-public crate-eloquent-0.1 (crate (name "eloquent") (vers "0.1.3") (deps (list (crate-dep (name "eloquent_core") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1nmf2zz07g0whsr48z4aa1ms0wwvx5jxrym82q52x94h4sgi4nrs")))

(define-public crate-eloquent-0.1 (crate (name "eloquent") (vers "0.1.4") (deps (list (crate-dep (name "eloquent_core") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1zmbphfwiazi0kyvv404bj8rwgkxww5mcgysjgny0ni2c6s26kmm")))

(define-public crate-eloquent-0.2 (crate (name "eloquent") (vers "0.2.0") (deps (list (crate-dep (name "eloquent_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "07wwi7caxmv9vq4q3f5c12iw7y65yiyd4fzdqhvcr08hvvb7fg5f")))

(define-public crate-eloquent-0.2 (crate (name "eloquent") (vers "0.2.1") (deps (list (crate-dep (name "eloquent_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q009bsv7v9lh74hyvfw4hf5cmynhwfliz1rvwf10nfvkw03d9hn")))

(define-public crate-eloquent-1 (crate (name "eloquent") (vers "1.0.0") (deps (list (crate-dep (name "eloquent_core") (req "^1.0") (default-features #t) (kind 0)))) (hash "05q0ws49d8laqc8f3790ir92i7mh1kvvi0abr8bdwdkjgysdfah3")))

(define-public crate-eloquent_core-0.1 (crate (name "eloquent_core") (vers "0.1.1") (hash "046p6hyz69ywswnhigxdf8kzf7i3swh9bv9ik4c742q8kzbp59jv")))

(define-public crate-eloquent_core-0.1 (crate (name "eloquent_core") (vers "0.1.2") (hash "1wj4ddkj8kanf3sb0qc3dn9sl00fkndiwplk42yzjhz8jc3mnscb")))

(define-public crate-eloquent_core-0.1 (crate (name "eloquent_core") (vers "0.1.3") (hash "14s1i1mbjsixlrbsjw96xy0nd8kjdakk8kfv9fr10i6silhq1q1j")))

(define-public crate-eloquent_core-0.1 (crate (name "eloquent_core") (vers "0.1.4") (hash "1rz7ldn3nr6afhxbhxj1z6djrxvgkrg3q7minnyc42ip98i9wgns")))

(define-public crate-eloquent_core-0.2 (crate (name "eloquent_core") (vers "0.2.0") (hash "1vj5r0g3r7d3g19ddiz6vbjab3w6r6hpkm1j9bfjz6116yz0y4id")))

(define-public crate-eloquent_core-1 (crate (name "eloquent_core") (vers "1.0.0") (hash "1xdcxz9rs4wrjinff5vkk9i67aasjdpyb32vyqfm0ac8sgzd8ym9")))

