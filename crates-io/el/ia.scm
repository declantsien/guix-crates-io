(define-module (crates-io el ia) #:use-module (crates-io))

(define-public crate-elias-fano-0.1 (crate (name "elias-fano") (vers "0.1.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0dslsv2xkc8s2c77sxirkapnipgvhshvpk66zz0c61c2s9yvrr6m")))

(define-public crate-elias-fano-0.2 (crate (name "elias-fano") (vers "0.2.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "03frja8vly8pps42s9pqk63iclbmwxx3jdshfxk28bcq5j3d14yr")))

(define-public crate-elias-fano-0.2 (crate (name "elias-fano") (vers "0.2.1") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1p4fj7i35kxllyz5innf7qg9mnxhssf8f6177hnxrwmcgykv96rf")))

(define-public crate-elias-fano-0.2 (crate (name "elias-fano") (vers "0.2.2") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1yh3mcmlg7pm7yryr81kl7wc3pir8x92kb03fbaw7j93s88dd86d")))

(define-public crate-elias-fano-0.2 (crate (name "elias-fano") (vers "0.2.3") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "08f4wq01kykcam3qn5yzz5dzavf2083dbch09g3h3j2yf6hfw01w")))

(define-public crate-elias-fano-0.2 (crate (name "elias-fano") (vers "0.2.4") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0i282mp0mjx3xhrn92cbg1h2lpfz94b8zdvmq0jckk8gq25yy056")))

(define-public crate-elias-fano-0.2 (crate (name "elias-fano") (vers "0.2.5") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1fgxpy2wmd85lpc2721iq8nxyhvcllriw9km69hdpnpxi0dap1sx")))

(define-public crate-elias-fano-0.2 (crate (name "elias-fano") (vers "0.2.6") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "09vflzmzh4lbnrfzz2840fdwywgrzqvzv3sy186img3fnwqzqj0z")))

(define-public crate-elias-fano-1 (crate (name "elias-fano") (vers "1.0.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "06r40id0afkixb4ipgvdf0lzkgx1ds2iwf6yra23ff5bhdbvy0zz")))

(define-public crate-elias-fano-1 (crate (name "elias-fano") (vers "1.0.1") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0ikpw51b1kz6ijs8g1qyqssvb9bk46d82f8fk2ka2xwdkn19dald")))

(define-public crate-elias-fano-1 (crate (name "elias-fano") (vers "1.1.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0ia0igd0zdrlq3z6wn7l9h8ifdnxnx5dd93qvc6l0r657qs0ykgx")))

(define-public crate-elias_fano_rust-0.1 (crate (name "elias_fano_rust") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bio") (req "^0.32.0") (default-features #t) (kind 2)) (crate-dep (name "bv") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "fid") (req "^0.1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fid") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "indexed_bitvec") (req "^4.0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rsdict") (req "^0.0.4") (features (quote ("simd"))) (default-features #t) (kind 2)) (crate-dep (name "succinct") (req "^0.5.2") (default-features #t) (kind 2)))) (hash "0af468p4s5k7gpi6jg7az0vvi2mby0k320lkkx3c8ils96wy3d2a") (features (quote (("unsafe") ("fuzz" "arbitrary" "fid") ("default"))))))

(define-public crate-elias_fano_rust-0.1 (crate (name "elias_fano_rust") (vers "0.1.1") (deps (list (crate-dep (name "arbitrary") (req "^0.4.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bio") (req "^0.32.0") (default-features #t) (kind 2)) (crate-dep (name "bv") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "fid") (req "^0.1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fid") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "indexed_bitvec") (req "^4.0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rsdict") (req "^0.0.4") (features (quote ("simd"))) (default-features #t) (kind 2)) (crate-dep (name "succinct") (req "^0.5.2") (default-features #t) (kind 2)))) (hash "1p77jqqpzyb8wzm8ziwvmsip2irsdj8qs3lcj5zy4jj6daj0hyim") (features (quote (("unsafe") ("fuzz" "arbitrary" "fid") ("default"))))))

