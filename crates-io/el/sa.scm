(define-module (crates-io el sa) #:use-module (crates-io))

(define-public crate-elsa-0.1 (crate (name "elsa") (vers "0.1.0") (hash "09lv3xrhrrsnh1h170x7zhn2x2gwmzilibbij5ayz9nd0425ja4d")))

(define-public crate-elsa-0.1 (crate (name "elsa") (vers "0.1.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "076hpd3bi8a2l5wym5f6vscrir3jy6jw7pqbdhxssivglvfb5q89")))

(define-public crate-elsa-0.1 (crate (name "elsa") (vers "0.1.2") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1w6cys257qa71ydpngjj2s8p4v5c2zzj4vfp3702g6jdivvj4phh")))

(define-public crate-elsa-0.1 (crate (name "elsa") (vers "0.1.3") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1n7zyafn75z8xdsl8pqbbkymfk0yv4hr9gdwbwmsfqkry8aqmkbw")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.0.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1nhwhl1mh4217n2capc868f98wd53w04ngavyijkwnh7rh1q0sw9")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.0.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1qa9vydqfdc43nwzkwifhwmhxyjgw8fg2dw05zldm5jc2154wws6")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.0.2") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "17g490yrk1aqd128jcyxzprlc526hkciahs1n2gws3y51byhr61z")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.1.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "04ch2d4b0kxdm8wm1qxmgvca845j9n2qmb2ynb2laqwny9rayzxp")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.2.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1kgnjr72ngr5aisqilalljwr0zwl91vjgh1d5jnfs9r6dps726l2")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.2.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1p2zqx7s1h60kg7hiyanadb0ckpi4jx0d7ssnnlv1kacmh38rww3")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.2.2") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1xwadgvcgmiyymdldfj76vp2s8nni3pjkh50g9v0v7ndc3ffrs6w")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.3.2") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1rvqsv710ss3gnway47xwb7cf5ssrng8mcc64g5qb53whpi15bqd")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.4.0") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "19bn30qn87fml954fq8lm8pgg84dcl1fqfxb03f6lx85c8ll90w4")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.5.0") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1931mps1zqyza4nzf8qh2d53rq4pq3469881i15hsff3bjxi2yny")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.6.0") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1064wpcvsl5b4phf9mcnclw7ck9ml8cx8s0hn27yrdwxjcglb2rl")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.7.0") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "02bf031qna560519vz8f0ad8qmlfr55gmaj0hbb4hsbbxlimsjrb")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.8.0") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0fs5s2q47pcbrznmwgm1ir8n4rjrhn16j69rd2i9knxfqg1pfh7p")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.7.1") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1va7kkc8n05xljz7n80njiyy6ckd208abjldwsd4v9xhzcayd3w4")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.8.1") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "10v2y8mv31jbh9723jfc6m137qsdlsv17gcm0516x1g8vjlarq5m")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.9.0") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1501g4abn4g4mh2cf9bwa64s8i9lrhzi7bbn8xz4xd2n6mppckvi")))

(define-public crate-elsa-1 (crate (name "elsa") (vers "1.10.0") (deps (list (crate-dep (name "indexmap") (req "^2.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "041yf5a6mm2kck1hbapwvp39408c4f8cprd2h90j2zgm9np733nr") (features (quote (("default")))) (v 2) (features2 (quote (("indexmap" "dep:indexmap"))))))

(define-public crate-elsa-cli-0.1 (crate (name "elsa-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1bik6c11m67krzgh170cqnk4i1jaacr8nhgdmhlx7ilfly1r2917") (yanked #t)))

(define-public crate-elsa-cli-0.2 (crate (name "elsa-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1n1s042rg5z6ffh0bkvfaxinjqi6rzl4ydjij1ih8lw61rgnl78g")))

(define-public crate-elsa-cli-1 (crate (name "elsa-cli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "122zri8b88gwvy23l486dayzqzy569ybxl53v5im5kxwwbmpiyfn")))

