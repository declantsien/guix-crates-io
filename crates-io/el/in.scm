(define-module (crates-io el in) #:use-module (crates-io))

(define-public crate-elina-0.1 (crate (name "elina") (vers "0.1.0") (deps (list (crate-dep (name "elina-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x1q0p63f69q9xrcmhjv0gmzc5fv3p5b6b0m8lf4317acdpncy8i")))

(define-public crate-elina-0.1 (crate (name "elina") (vers "0.1.1") (deps (list (crate-dep (name "elina-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ljp7my18ni23hzc7gi3zbgblwfdy56zd48qlsrbir9v2pa1xq6h")))

(define-public crate-elina-0.2 (crate (name "elina") (vers "0.2.0") (deps (list (crate-dep (name "elina-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1iibi828375x63fj0l3rvlq7b94hviwrvj599lkm5k2p3awrp2d0")))

(define-public crate-elina-0.3 (crate (name "elina") (vers "0.3.0") (deps (list (crate-dep (name "elina-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09333vnflga76rpmmsg5vs7fa5lwppk1r4l47sxl0lnj0ipw8iwm")))

(define-public crate-elina-0.3 (crate (name "elina") (vers "0.3.1") (deps (list (crate-dep (name "elina-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dcrpj0rhx4gxc5b7dfwwa2ah5fa5ra5bgfrznibpx82f9c1k0fw")))

(define-public crate-elina-sys-0.1 (crate (name "elina-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)))) (hash "1ykyfsxg0f4kasg1zbvd26pma1jkrrgwazixmn58v4hv8ca3q8xr")))

(define-public crate-elina-sys-0.1 (crate (name "elina-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)))) (hash "043y5yywg7ilsh8xk2n640r6qxhkpxzv6a5dybcnj2iijsmdcjbd")))

