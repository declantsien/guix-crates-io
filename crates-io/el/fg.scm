(define-module (crates-io el fg) #:use-module (crates-io))

(define-public crate-elfget-0.1 (crate (name "elfget") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "14lnn8vp14514ln8k8wlql394cgx75fqk761nnyl0z4hhjpifn6i") (yanked #t)))

(define-public crate-elfget-0.1 (crate (name "elfget") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "1ygczifw3mlj2nshkhzmnprq98lr7fk8lcnnf25m5xcfifrdrgqi") (yanked #t)))

(define-public crate-elfget-0.1 (crate (name "elfget") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "037kwrchc27zfh0cz47wmv57i08p40ix011asjj7vryas3l0f45x")))

