(define-module (crates-io el at) #:use-module (crates-io))

(define-public crate-elatec-twn4-simple-0.1 (crate (name "elatec-twn4-simple") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0xcaqsyfass3ykqqprf8i1a2qxkzwc9sp2dc4cr006vlda05phl8")))

