(define-module (crates-io el ab) #:use-module (crates-io))

(define-public crate-elabs-crypto-0.1 (crate (name "elabs-crypto") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.21.2") (features (quote ("std" "recovery"))) (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "1kn1p209jprpjpkjdpngw5xzn0sgzmsgakjd0p0j0p2vlfizrkyb")))

(define-public crate-elabs-k256-0.1 (crate (name "elabs-k256") (vers "0.1.0") (deps (list (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "0vd3qcbj1kmal5qnq8c4g3j0wgzbwjm3zw7rvc7bps7lbgw6gdj0") (yanked #t)))

(define-public crate-elabs-k256-0.1 (crate (name "elabs-k256") (vers "0.1.1") (deps (list (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "01bb7w8n12hgmax5vkgj42ykmll9w4aq2rzyspb3pp822y9bj8pz")))

(define-public crate-elabs-solc-0.1 (crate (name "elabs-solc") (vers "0.1.0") (hash "11pijiq3dzprdzmbqw5ll8dzv7w8xvwv7dkviq51c454wj9bwgmc")))

(define-public crate-elabs-solc-0.1 (crate (name "elabs-solc") (vers "0.1.1") (hash "1kykrbgcnsnh1s00d2nngzyql3sz8mv4qv564434knw3iiv6wlsa")))

