(define-module (crates-io el o-) #:use-module (crates-io))

(define-public crate-elo-multiplayer-0.1 (crate (name "elo-multiplayer") (vers "0.1.1") (hash "15ckr3fyhz0kdawq13ddq3z6gjqkj81gvwr23c6qy3r8vf4mcfq1")))

(define-public crate-elo-multiplayer-0.1 (crate (name "elo-multiplayer") (vers "0.1.3") (hash "1kdaqcd6ni0vc4xmz08mljxdn56zc62lq3hd73m436g35j6c1gyn")))

(define-public crate-elo-multiplayer-0.1 (crate (name "elo-multiplayer") (vers "0.1.4") (hash "11m0ivcywjrd5vnbkfkgcg3xlbahd0k47hrsn0pvhhaznxlps0ax")))

(define-public crate-elo-multiplayer-0.1 (crate (name "elo-multiplayer") (vers "0.1.5") (hash "123w5z9djag1rnmgg187rc8asq19smfv27na9wa4bwya3xlh95qb")))

(define-public crate-elo-multiplayer-0.1 (crate (name "elo-multiplayer") (vers "0.1.6") (hash "1z60hr1pqpprbkm4w89aj6pcr4rcmvykff76v7cnm2wz4nykkw1y")))

(define-public crate-elo-multiplayer-0.2 (crate (name "elo-multiplayer") (vers "0.2.0") (hash "006mdkj953k6xhqpnad0hgd5vcn3z25k2yzhy4jrjg4sr6ppgdap")))

(define-public crate-elo-multiplayer-0.2 (crate (name "elo-multiplayer") (vers "0.2.1") (hash "01kpvw3c5f62h6ikk7lznc6j19viwvp2a7hgnjrrjd3gb3bmb3x0")))

(define-public crate-elo-multiplayer-0.2 (crate (name "elo-multiplayer") (vers "0.2.2") (hash "0mla8zhf63v3bkj4avfk79nsmij45zjr5hdq3n90k6y9hx2qqxh3")))

(define-public crate-elo-multiplayer-0.2 (crate (name "elo-multiplayer") (vers "0.2.3") (hash "0gfmfihkvpz5cwhbj381fgq116h9ykmnhq8nmz60xbzvgdmzhl1w")))

(define-public crate-elo-multiplayer-0.2 (crate (name "elo-multiplayer") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0mk4y24cwbgsp8hw0k5ygf2v9099ck3lk3j57y3c99k0ckmr25kz")))

(define-public crate-elo-test-manager-0.1 (crate (name "elo-test-manager") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "043d0b56assbpp15wq9kggja5g2my12wwgdch0f8a6sqjvk7y6pn")))

