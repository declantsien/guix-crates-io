(define-module (crates-io el md) #:use-module (crates-io))

(define-public crate-elmdoc-0.1 (crate (name "elmdoc") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "13w3syn9l70ql0ij32pxj44l5wfhjm5rx1pcz3yayagy3ir0f614")))

(define-public crate-elmdoc-0.1 (crate (name "elmdoc") (vers "0.1.2") (deps (list (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "025kh4wvnnami8yzsr8mj79ncc4ismahl69rxfs3dfxmp3qn3cf4")))

(define-public crate-elmdoc-0.1 (crate (name "elmdoc") (vers "0.1.3") (deps (list (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1klbndy7zvpfch5pvc19446xp90fzyah2s7rx3h51yhwyb6xzan2")))

