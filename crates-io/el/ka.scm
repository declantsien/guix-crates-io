(define-module (crates-io el ka) #:use-module (crates-io))

(define-public crate-elkai-rs-0.1 (crate (name "elkai-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 2)))) (hash "0k9qk6ynf0k9k4abw3rfvfzsq1i6kc4xki2xdrpyrbbl5fljj5vi")))

(define-public crate-elkai-rs-0.1 (crate (name "elkai-rs") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 2)))) (hash "11wjajfl8zgsyl3zz0yp2kbcwf3sa0g10vgayzwlddxpriq7sp7r")))

(define-public crate-elkai-rs-0.1 (crate (name "elkai-rs") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 2)))) (hash "11j4dsz9fnlnkhdx3i73g0v5xv5rhi849rqbxv5km6qsx86afjw1")))

(define-public crate-elkai-rs-0.1 (crate (name "elkai-rs") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 2)))) (hash "1gs3d8xfhqvpljbdn7lq8l3ii6sxhcll4nq4033ybmxbk24x1rk3")))

