(define-module (crates-io el it) #:use-module (crates-io))

(define-public crate-elite-0.1 (crate (name "elite") (vers "0.1.0") (hash "0zb93brjbv559yb0bhrcyfhpbzjimkibf2bjzj6r3wnlxflq3klm")))

(define-public crate-elite-0.1 (crate (name "elite") (vers "0.1.1") (hash "045xfpxji6j3knq73hznxdqc952whaq6qdsl7nyfh3s45hl8x9zq")))

(define-public crate-elite-0.1 (crate (name "elite") (vers "0.1.2") (hash "13h6p8gkkcq7hv6jii28zfim18pz0z48g8viymfpdgs61mqnia42")))

(define-public crate-elite-0.1 (crate (name "elite") (vers "0.1.3") (hash "1vf2dv3v4bfv5mz7hrxm858w6xjk4287y07q7qcxzzhq5gxiwgsf")))

(define-public crate-elite-0.1 (crate (name "elite") (vers "0.1.4") (hash "016v61sm535fi6lnwnq88bgbraipiazzyhaxzr5i06jvkwyqp0mz")))

(define-public crate-elite_journal-0.0.1 (crate (name "elite_journal") (vers "0.0.1-dev+alpha") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "geozero") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "145c0pprva9y4r36psi726m8hz8pbm1zfr1i3zmskav5wp83j11k") (features (quote (("with-sqlx" "sqlx/runtime-async-std-native-tls") ("with-postgis-sqlx" "with-sqlx" "geozero/with-wkb" "geozero/with-postgis-sqlx"))))))

(define-public crate-elite_journal-0.0.1 (crate (name "elite_journal") (vers "0.0.1-dev+alpha-0x01") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "geozero") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.5") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0k1ps4dgq0a1mpk7gdx5dnk9h4ab64z3xy41871rxa251pi6shj3") (features (quote (("with-sqlx" "sqlx/runtime-async-std-native-tls") ("with-postgis-sqlx" "with-sqlx" "geozero/with-wkb" "geozero/with-postgis-sqlx"))))))

