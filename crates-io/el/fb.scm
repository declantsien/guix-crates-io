(define-module (crates-io el fb) #:use-module (crates-io))

(define-public crate-elfbin-0.1 (crate (name "elfbin") (vers "0.1.0") (deps (list (crate-dep (name "binbin") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "elf") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0wwynlha6pihsg4z7nm85qxbi15jz064h5b8f7zc7m9pahl1d8pk")))

(define-public crate-elfbin-0.2 (crate (name "elfbin") (vers "0.2.0") (deps (list (crate-dep (name "binbin") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "elf") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0ss4b3mv9ia97cj2m1486pw8zc0mwd3lzr2jn6ajv0ixjxss95fa")))

(define-public crate-elfbin-0.3 (crate (name "elfbin") (vers "0.3.0") (deps (list (crate-dep (name "binbin") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "elf") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1l5vhafl5g1wxfcq734306k9cb2jxgr35j1a1yp96jv0pfhh5j1w")))

(define-public crate-elfbin-0.4 (crate (name "elfbin") (vers "0.4.0") (deps (list (crate-dep (name "binbin") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "elf") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0h7nw8610132k96jnadqkddbsbkgp8rpf0sc8kmidyb18cz0gi2d")))

