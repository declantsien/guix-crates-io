(define-module (crates-io el ft) #:use-module (crates-io))

(define-public crate-elftools-0.1 (crate (name "elftools") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1jlgwz962h9wph4j1zshnag7vgh19hmpibfz7zyfihbyrc4ya19s")))

