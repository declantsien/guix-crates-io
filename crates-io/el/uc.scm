(define-module (crates-io el uc) #:use-module (crates-io))

(define-public crate-elucidate-0.1 (crate (name "elucidate") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "01m9v3h4dv6nzh8vwn11vyfmdjd0hjvw9p5f7fx0zpda03fppc1n")))

(define-public crate-elucify-0.1 (crate (name "elucify") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.8.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1bqfxs050anzlr16m18dhxk8gh2lb2qh7pf1d4i2vhbib1c5vp1r")))

