(define-module (crates-io el og) #:use-module (crates-io))

(define-public crate-elog-0.1 (crate (name "elog") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ifj4a0c365x07izhwdn4a786d92hkv3sm6xg98qp52p428phcw4")))

(define-public crate-elog-0.2 (crate (name "elog") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y43zccvpvqvjd9dybr6wimd0ca3mchygvhavc60nx4xbsl7dvyd")))

(define-public crate-elog-0.3 (crate (name "elog") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rj1dj0ydbplmi5ykn3jz5w0m3sk5jcczw0986qdjayn1jz7nva9")))

(define-public crate-eloggr-0.1 (crate (name "eloggr") (vers "0.1.0") (deps (list (crate-dep (name "i-o") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (kind 0)) (crate-dep (name "unix") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "unix-tty") (req "^0.2") (default-features #t) (kind 0)))) (hash "14fmflbv01j58w2sp6z2ysxvp9z3y34g381vg0f9blpi90nhkc8q")))

(define-public crate-eloggr-0.1 (crate (name "eloggr") (vers "0.1.1") (deps (list (crate-dep (name "i-o") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (kind 0)) (crate-dep (name "unix") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "unix-tty") (req "^0.3") (default-features #t) (kind 0)))) (hash "0swdaz9i35g5y779jma2zik1kwnk7rn3vhmcrh4hd7l6nwhl0b46")))

(define-public crate-eloggr-0.1 (crate (name "eloggr") (vers "0.1.2") (deps (list (crate-dep (name "i-o") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (kind 0)) (crate-dep (name "unix") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "unix-tty") (req "^0.3") (default-features #t) (kind 0)))) (hash "0j1kx7k8vp3b19wkf6dq57mzs9dijd33vayc64mhsxnbyzlb2n3d")))

