(define-module (crates-io el ai) #:use-module (crates-io))

(define-public crate-elain-0.1 (crate (name "elain") (vers "0.1.0") (hash "1has9v97dy3labwxk6678sfn77mymljb9cmsy8nah14g1kxsga7w")))

(define-public crate-elain-0.2 (crate (name "elain") (vers "0.2.0") (hash "00i7iv7gr62zrr5d0lwzrjdyvk1lcmckp1ip7hgf165ysjhiw3bf")))

(define-public crate-elain-0.3 (crate (name "elain") (vers "0.3.0") (hash "0wgpyy0m48vdnrip0f4x1h8w4bp2lxgy8pqk78qwhrbxmr7hj8im")))

(define-public crate-elaine-0.1 (crate (name "elaine") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^0.99") (default-features #t) (kind 0)))) (hash "05qmwrsld0q684p4rg6ml3vpmgjip4zgg1p2a2h7j5dqnvpx09s7")))

(define-public crate-elaine-0.1 (crate (name "elaine") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^0.99") (default-features #t) (kind 0)))) (hash "15vd66cmsp436c1awrspl36g4jz3gdhy3924d25mxqx8g7wj5adw")))

(define-public crate-elaine-0.2 (crate (name "elaine") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^0.99") (default-features #t) (kind 0)))) (hash "1cniqagmgpdr5p8zxq7h60xvnskcwcmsplfrxdbi2fxmw0qbddbf")))

(define-public crate-elaine-1 (crate (name "elaine") (vers "1.0.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 0)))) (hash "05cp7m6sk8q6xwxpnp2pc62pmf5xm3pnk4xhyydlkr7az7n5yl22")))

(define-public crate-elaine-1 (crate (name "elaine") (vers "1.1.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hzc89v0c8ly4mgjyxkjl1566h9vcghvgsj7jawwra95v1xl66aa")))

(define-public crate-elaine-1 (crate (name "elaine") (vers "1.2.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p0mv3v3kkyqxmwnh2xdz2ich5xisil6mws4ffswhwhjfniv81db")))

