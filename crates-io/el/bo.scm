(define-module (crates-io el bo) #:use-module (crates-io))

(define-public crate-elbow-grease-0.0.1 (crate (name "elbow-grease") (vers "0.0.1") (hash "0sqwypbdjjal4qv48wsrlwa2sb3y9p9dmxc3i5cqjlwvm7n3v0ll") (yanked #t)))

(define-public crate-elbow-grease-0.0.2 (crate (name "elbow-grease") (vers "0.0.2") (hash "1qrwbg9yssk6n33r7yd0k3ygfldmm8xl4w9gmdc791sdydcbiyi8") (yanked #t)))

