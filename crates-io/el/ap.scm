(define-module (crates-io el ap) #:use-module (crates-io))

(define-public crate-elaphos-0.1 (crate (name "elaphos") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.10.1") (features (quote ("dynamic_linking"))) (default-features #t) (kind 0)))) (hash "1s605vnbn84j83visz0mc4ryhsa1cw0nf307h5sqn9kxi2h3jlya")))

(define-public crate-elaphos-0.1 (crate (name "elaphos") (vers "0.1.1") (deps (list (crate-dep (name "bevy") (req "^0.10.1") (features (quote ("dynamic_linking"))) (default-features #t) (kind 0)))) (hash "0adqzap1jh41x0y2fjldqa9nyh371h10916xa6sbsvavrvhn6y1w")))

(define-public crate-elapsed-0.1 (crate (name "elapsed") (vers "0.1.0") (hash "0dbzq64q97qj5ynybhbz7mc70w219qh9cz5wqy82bv2a1g8a8vv9")))

(define-public crate-elapsed-0.1 (crate (name "elapsed") (vers "0.1.1") (hash "1p0r73cmcm54nka42pqvcv5x65kkkhdwh5jv6a9na84jpr9wx0jy")))

(define-public crate-elapsed-0.1 (crate (name "elapsed") (vers "0.1.2") (hash "0acwnk47zaf88pfz1r8205ah51mjd1zx8qmdq90hgzfs4vqmlkkg")))

(define-public crate-elapsed-printer-0.1 (crate (name "elapsed-printer") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.82") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lvq1hpadx3ni2arqnva1f32rixb39d2p2wz7acbh3gwiv6qwgn7")))

(define-public crate-elapsed-time-0.1 (crate (name "elapsed-time") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10rqphz0m7jnis5zhcgv5ycwjv57grhai3i1fsgabpgrmcjzzjf3")))

(define-public crate-elapsed-time-0.1 (crate (name "elapsed-time") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06vx5mi4g688jfz6scydx2zkg3gng5hsxzcrdp7y6pcaf94q8nin")))

