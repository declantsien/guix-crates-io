(define-module (crates-io el un) #:use-module (crates-io))

(define-public crate-eluna-0.1 (crate (name "eluna") (vers "0.1.0") (deps (list (crate-dep (name "float_extras") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "106zd8npvn490ffpxx4p2shgixhrdi5jpc8aapbgkar3nhw1wp3j")))

(define-public crate-eluna-0.1 (crate (name "eluna") (vers "0.1.1") (deps (list (crate-dep (name "float_extras") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0m4ha7qa1kra5vyddxzilgpwpgyp54r63rkpai3aw604fv9zdn00")))

(define-public crate-eluna-0.1 (crate (name "eluna") (vers "0.1.2") (deps (list (crate-dep (name "float_extras") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "158ba0dc6i6y5wfixg9bkjnx9ksikc114ia6kdk4kb5m9b3kwqv3")))

(define-public crate-elune-0.1 (crate (name "elune") (vers "0.1.0") (hash "1l3kl599cn6rk4ivzvjy3qj378hwdsby6346n45knf4ilw3qg8r0")))

