(define-module (crates-io el f_) #:use-module (crates-io))

(define-public crate-elf_parser-0.1 (crate (name "elf_parser") (vers "0.1.0") (hash "1xsi6s22npdhx6160kdbc1j2mwady3bhhmxmawrfp226xj94lxzv")))

(define-public crate-elf_parser-0.1 (crate (name "elf_parser") (vers "0.1.1") (hash "1p3f1mjc0v73hczyvhniiyb3lksdwi8m0a3qfbqzq8xiz70sp9bl")))

(define-public crate-elf_rs-0.1 (crate (name "elf_rs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "enum-primitive-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hqlqyy7x2b4xq4jx736qvmhq2v744rwcyrbfa5ng1r5vdr91ak4")))

(define-public crate-elf_rs-0.1 (crate (name "elf_rs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "15nci3wxx35dgcdk9isis7hkyc0p2jsdgx1mslbwws2dqka8s316")))

(define-public crate-elf_rs-0.1 (crate (name "elf_rs") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1ikfxkbjfakryf0d119zncvfmy2j4z2n4axjr0bqgdd3l5ggwql6")))

(define-public crate-elf_rs-0.1 (crate (name "elf_rs") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0f1yg1d1hxcjc7zvymgws1zv54m98nxw9c14jnjc41gvcai4r811")))

(define-public crate-elf_rs-0.2 (crate (name "elf_rs") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "00l6767b6hqlnxya15p3ganig6d4blv1bb1kg2yvql1m36szn8hq")))

(define-public crate-elf_rs-0.3 (crate (name "elf_rs") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1s1kb9g59gmj4vwyxj77v2xsvh8ay8y987ahnb0760zv1vvj7xww")))

(define-public crate-elf_rs-0.3 (crate (name "elf_rs") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1pxgmss8m4p1lg71l1vzviqkyfps5zn2g4lzwrff5nh7dc5p2kc9")))

