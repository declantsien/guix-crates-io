(define-module (crates-io el f6) #:use-module (crates-io))

(define-public crate-elf64-0.1 (crate (name "elf64") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)))) (hash "1fw96nx1v2crqp9yshichsq2v79siszy3344x964hdfbibnr2g1y")))

(define-public crate-elf64-0.1 (crate (name "elf64") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)))) (hash "02i51l56b2d9l1p5iicw6b021ckfcva0p0chbdw7dc2wfj87fnm3")))

(define-public crate-elf64-0.1 (crate (name "elf64") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)))) (hash "097vwfss0zfm2c93myjhkzx25kj1ly61fx1q9w6sys0l7g1aiyl0")))

