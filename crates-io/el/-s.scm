(define-module (crates-io el -s) #:use-module (crates-io))

(define-public crate-el-slugify-0.1 (crate (name "el-slugify") (vers "0.1.0") (deps (list (crate-dep (name "deunicode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "18pm4h11fmphq7dh95vdgmgw2qm9ch64j27v38df2gdcsbnl7b80")))

(define-public crate-el-slugify-0.1 (crate (name "el-slugify") (vers "0.1.1") (deps (list (crate-dep (name "deunicode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "14d646n2i3fpbjk2nqy9azy8z7qqg43cj96fp31f5fh2npc5pn67")))

