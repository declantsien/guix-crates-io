(define-module (crates-io el sy) #:use-module (crates-io))

(define-public crate-elsys-0.1 (crate (name "elsys") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "08dk52b4388kn127jg7g552dv04pms01lk58kcc4rdmb3xb1s775")))

(define-public crate-elsys-0.1 (crate (name "elsys") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "1g23kjlvk5chmq6axv5npvp8a42fl41q4gpnabfhkk8f6r0vdv4k")))

