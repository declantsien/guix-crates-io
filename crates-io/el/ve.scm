(define-module (crates-io el ve) #:use-module (crates-io))

(define-public crate-elve-0.0.1 (crate (name "elve") (vers "0.0.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)))) (hash "1vbilbvl38j88m5rqdj87nam8y7fw4zsi2hn6j8pd944w9gw93p2")))

(define-public crate-elven-0.1 (crate (name "elven") (vers "0.1.0") (hash "0g2266phsj0rbdh6dkrbmw9dv35plpd7qfkm8pn2n8d7fnc0c7ph")))

(define-public crate-elves-0.0.1 (crate (name "elves") (vers "0.0.1") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "10l17wbvxp06l58kn3qxqi3r1igyl1xqnmdh6cgv453pg1p9b2gm")))

(define-public crate-elves-0.0.2 (crate (name "elves") (vers "0.0.2") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1p3phizp6bls9x40fs0zb83ylsqnknhs56d88ixbfhlj9x1mv1fw")))

(define-public crate-elves-0.1 (crate (name "elves") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1jnj00kmlh3gd2dhg1r05va0i5i7hj812cvrhkx07ldw5sjzh4l7")))

(define-public crate-elves-0.1 (crate (name "elves") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1j3gjv1626agv1p8kiy34v19fpdh8630crnnrqb1pcnr4h8459hm")))

(define-public crate-elves-0.2 (crate (name "elves") (vers "0.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0zh08zlbymz7yd9nsn0g5637dklah7lfzdrzbc105j337ig0cjj4")))

(define-public crate-elves-0.2 (crate (name "elves") (vers "0.2.1") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0cig7sm74wb1kjksvx7z0f6pr8mybysnp5iyaa9ynivn5gpnhmjs")))

