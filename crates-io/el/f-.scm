(define-module (crates-io el f-) #:use-module (crates-io))

(define-public crate-elf-info-0.1 (crate (name "elf-info") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "gimli") (req "^0.27.2") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.6.1") (features (quote ("elf64"))) (default-features #t) (kind 0)) (crate-dep (name "iced-x86") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1g11aflf9ihzd4ka9qdhjq2lacnl4pqgix2p5ncp58bawqc2ayib")))

(define-public crate-elf-info-0.2 (crate (name "elf-info") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "gimli") (req "^0.27.2") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.6.1") (features (quote ("elf64"))) (default-features #t) (kind 0)) (crate-dep (name "iced-x86") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "01vyg8mplsjlx7928avc1z97yqqqh8pba2bigh8gjzr64dvdp6kl")))

(define-public crate-elf-info-0.3 (crate (name "elf-info") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "gimli") (req "^0.27.2") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.6.1") (features (quote ("elf64"))) (default-features #t) (kind 0)) (crate-dep (name "iced-x86") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0libsswf2kw6kr38pzvjbdqad8ir9436v78y8qcqs4bska89bcdq")))

(define-public crate-elf-riscv32-0.0.1 (crate (name "elf-riscv32") (vers "0.0.1") (deps (list (crate-dep (name "memchr") (req "^2.5.0") (kind 0)))) (hash "01n7nvi13pfaamjqjv1prg9qqr80ihpp2f6xwzarf5mfxnbhwphx")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.0") (hash "0rf7r96dzxn8x5raj5c5l7qk1bwp2ksnfnj1g3wpjw8i1y6d1ypn")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.1") (hash "1za9hff3hygy6gsppzgfn98az8af4111s3i6jbhqnlf0nmwfivwf")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.2") (hash "16qp6maj1p50gn40x95w656wy5hk5fxb2wgzmirvsprbslzqxqjf")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.3") (hash "1v7ks0mbkl5q1rgscplvsjwg4kii1x3y5ivgjvykhpj2l2bbl45i")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.4") (hash "0n3s038qwvkjjcfc8mqr5dqaf577w13m7bsj5rm29w1cbj5ibdq3")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.5") (hash "1nz6n03vr5ag70c8mdp50ssskdwyryy290sxmbv200z8sscnhm9f")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.6") (hash "0nqrnl5hissdn9rhn36qmvyhlzk9m77drnvkwyxgj5bz452wq369")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.7") (hash "1hmh16za8633kjxhypp7g7m945qsxglvhi35dy31bwmnpk0wgyrw")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.8") (hash "01ppdlxlc27g788wy0n3v5xhr39p5ypm9rjck7jqv7rkx2kq98y8")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.9") (hash "0nxfv7j5lb4nmi2whcwxkzngj36m0hwynkqy7vvdqs4w6zndfvp1")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.10") (hash "04ma4rg09yl0jk8370ariwbp1fjb0yla33v0cnv2c1prq0sazj69")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.11") (hash "0q8654mj2amz0ijylzfyzxih4s81wx6n2xhy6pc9mdx3q0g0annl")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.12") (hash "1qjdilk9jqjzwi2ficlalyhhxlxyvfmgdlp45sgsy0s9w5lzjy7r")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.13") (hash "0nmlyy25szgkdhdfbbwf4aami5xxla9n9s8j3b1823iwxgdb2w2n")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.14") (hash "0zb47n4s0qcrs4g93ai2xsgakxsqmlkqswvaf0d9nmdgbrmb37s6")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.15") (hash "04gicpbikp8327i9mkp63mbzkqd0ya4729wvfhbmn9fgmd75agjl")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.16") (hash "0xx2afmjli9gxj60imcp91l5v859xcrys6klhdj4w1mipvsd1g07")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.17") (hash "0sp8ixrdsgslhfxzaw5xipp21y0sdya4zv29n8nippms8nahmff7")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.18") (hash "1p83671i45kpkvqvywkxkzk5r59y5300222a6qiqlxyvb4nvg3f3")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.19") (hash "0psq2nj5r8n2bdgvcw2bw1hk8l537711g74xdgwnf5605l08hrp5")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.20") (hash "1r6c1ai6gy00bk1g4sj6xqajhdc1dhpks4zasqh9zaw6wm152p22")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.21") (hash "1wqjn0rah3m2ajkd5nd3ck5h0hgn1syzlhifjqicw394f2sakyar")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.22") (hash "1n1fmwi0a3r078i2cjxiwlxwgj702lw479s8jw6jkrabpjvvh525")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.23") (hash "0wf1k2hc5m5np46xkxph9hhcppb6g818m86pjfkygmcg0zcl5z87")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.24") (hash "1d34nkpg82ip1msyclpj3rkjq1phrhfj64rfldp5hsd0x6w37qkj")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.25") (hash "17rx8c9q2yd5mkfvq5gqabqvixpwxdcdami46rjxipgbfp22njdp")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.26") (hash "0x2765glrwysryncd7sava9b8a107wvq7wp6c58w6i0va3d8dqgk")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.27") (hash "1hm4zrmh88i8jfzj2wqh5qhx17a69j8ghswyk0szsvcmnm2wav3v")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.28") (hash "18mkv1fcha634dckdllj5clk1a7d4rpbdr49j8xw13qclfl3w843")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.29") (hash "1sj53cnzaijrh5c01rm4abmc3nc6sgh7fbdki2b9vwbad7yr9z85")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.30") (hash "1ahdfvvwpmrvvfhfh4jqnxd21fzs9wwv233wx2wwhgdsqa70x5dj")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.31") (hash "0k264sqi8r46hi77gs189riqpvgdyv6r1y4w590krhlv1avgjlkb")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.32") (hash "11m4ckrl5cg0xsaawy170gn8701z3n5l9pvs0qz4khnd0yfa1chr")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.33") (hash "0lkv67vkkwp6nk8c5h252a3fpaizk56wc4gk6s8lak668fzikw84")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.34") (hash "0s1w4d4bipljy47ask5kg7gr35d1x6w65qk7zq35045ph2fd1adl")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.35") (hash "0s3cddbwmifcxhq1lx3s7a88qqr7qnwk2krnxsb5k7iksadgn5yp")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.36") (hash "1ch1rczzp1pwhr36156qr2ydyii5pjwbcayms03w2m7qh05y2zm2")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.37") (hash "16r55r73494c10pf7g2d7ph9mx8kidy0czkcmy813v01izs1rr4k")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.38") (hash "1s1nwq7q3zw2zz1wif10jpifjjrhjkg9jz25j4zqfgs4hbzxfrfi")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.39") (hash "0hjww93hssvsp47arvdbn81bnnpyi9wvbclslicbc7a6zgh1bg8d")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.40") (hash "0j087wyvps8wfba6yb43j347vvwwr8i3rfg3i3x0cq8acgfzpiw2")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.41") (hash "00qsca5qp5vfiaj4gcr0ck8iwz2p9zlfsvv3znrd8bm77g1sv2q2")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.42") (hash "1as57gxi5rgyf7805j37fjm4i8hxx7mx2jzksm4xb8d0jmw5b3gb")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.43") (hash "1lr1lqd4x637z88i8pd8a8xi86dg63gpmpz9q9nwyqkf7m2razvn")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.44") (hash "0w6mhy2jpn3vlda7ljccdaj8m15vpbblhwzx7asifxyy8b3ky8b3")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.45") (hash "09cri1r9xiqbhzlakaqys75acagjqnv0dcd4pvdzn4ngxx5133rk")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.46") (hash "1c57wc1vamcah1b1c009d48d4ys7r09zrjgn56550ib64j82p1ys")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.47") (hash "1s2yz5xxxj2yf03sh9gak681z9kjlnd4n5ddq6vz30dy8zi0c69n")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.48") (hash "0q86jdzy7x2kfjbbr4jakp4m582vbldk8vjy19a66dyy5svm9xpp")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.49") (hash "1hs81wgh2z39nihjry908n66ncj6fs4y299ag3cfqkfib6cmnyb0")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.50") (hash "0hm1x7y4b6mgmwh4lls4w02g97akkdpkzmsd39inw6qgp0xscswk")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.51") (hash "13izkwpp5zvxck3vmdw5qf7xc1nx2xwzcanifjmhqqmc8kd0zcy4")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.52") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19nxdgrydi7in8sn3a6i6wlqrg659nvd97ia8lq7ihl7w6v9fb9m")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.53") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0d746vayicvfrmnp8y64qmwfsr211p2qc4s95ycx53rxmfn06gsy")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.54") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1h315xrk2qpwpafakn11rjfxap344wdfdsi1jk4dqiid3qbxnyq6")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.55") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wpvkyln7k4liw6y9dk2i4a8ibbs7wvpgfj3mcsprzhzwhxi4adr")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.56") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17j7w6kw2nraws0nq5b38sr0p2nhh96nvrgrfjp9rfyfz6nyx602")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.57") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "18lvl59rzfw26hhzrxy49nybydi551pyx939bi9ndh6kjkpngrqb")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.58") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0f11rxcl0pnnzwbkid87pfrzj5nlssq4x3d4vlckz3n0pv5n3944")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.59") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1aiqiqpsjf91pdzy3gqj0y43hjx4szypqsyfxyqm0nxrvn1y92j4")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.61") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0c32qmmadzipys4glcf07rahzlh61b9ijjnjy4pcvzxgzkcshmv4")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.62") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1bbm31rx0l3zapy6vhj158qj3zmdl9dbwfg1z6zffm4ld4j4h137")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.63") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0q63m1rmd123zszk2v4bz1m3kdnnk4a7i5sf5bxkabsm2z4s13zm")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.64") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1d8d8xxgbxyiwim5j8bqfmf1j86p6ygj7mbkjipx32a4z9dz3psq")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.65") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "07f1xlxg0pfy8a39d6fi9pps9lkx0dsyh9wimk4c0686pl489rn4")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.66") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "03sfx4kzqy15bxzh8p026rsyhvz2s5b8a6vcg0v4ml00p9z4h528")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.67") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0bv3l6g12hm29d7ihh5rcghg6r1h2zqpg894fwlrqcm6dj4splac")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.68") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0si70mmv9c3i0d7ls5zhx5x541qpk89ryxi81b7h4bfbdr263ndj")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.70") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "08702qmi292q9rl6qnrxiydhy6zpwy7vzxh4vc58qpmdd339vmpk")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.71") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "077hk6c5axnpfhgydpmvxpi1x85gfqhwa5rfznzknrppkrx6sc2w")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.72") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0q1byasd6zfljnbkx5wi5dvvyj54m86rpbfd2k98l17f7nw0nfif")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.73") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1kzvw37z52p5kkib2hgdpq9bibn24rhlbjdbnpj5y2inrg7hqf54")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.74") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0p9zn92k7a6fsvppdw9b2hd6cg1gjfl615v99svbswb1cp6naq8w")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.75") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1i4yjfm2nflvwz398d6hpn03a2zv4gks1gbsb443w8bhcsj7ijvk")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.76") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "18b9yw8rgzvyjr6157k801pki6n7806b3yc4n6andfqdxa6n6jnq")))

(define-public crate-elf-utilities-0.1 (crate (name "elf-utilities") (vers "0.1.77") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0qqv370l839gj3qdcqvlk1caz82x15fqf49aiiwwbylr1qf1zjk8")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "191gxayr2sssm8jywjg7rdwrxy4b8vvlyab83mywxw05p4ryfyx5")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1lxyq1wdpf01qpcfh3jbzqp88dndnq1rjql43h2yjxngbv502v5k")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1j774836d1a270l6s9775aldrsgyqbc7yir8516ad6alqi1z5939")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.4") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "17bhs4skl8fnz986hl4ssv49j3818nvwbr9h8xg24d7rz82lmblm")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.5") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0jdjvbivivk4qqwlfld85p0q8bdd053dy4fpyr9zp5ldc8f9aql7")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.6") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "14jgzww5srf94bw328dpmch4gp16i27xshqr8rfsy06qssmgc23f")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.7") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1bqb11jwg9diklgw8s91j2ig8w4vzxlzaq55633wgk4am6z9hbiz")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.8") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1h9dpbvywq56m3lzp1vnc2l1qfz9skxgh8qcn01l7kfvxwvcdn2l")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.9") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0g0s1gzjx16cwvj2fpxygm9i8qam7zl2ib44l59j85hwk12bzkpi")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.10") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "07bqmq53alcq4f2rhrv44ic3hz5jgwq0pqvr324cn1qxz8dykkkl")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.11") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0zi0hacl5r1jr2kz1djlpzndgk6709s3q7i6wsdzh3p5k7z18vyg")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.12") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "15nl28wpgld4rcnfnzkrqqfh5acghnjk14dlfq1bnab1q4hc6q3j")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.13") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1p4v8nkk4rz95x3zp3nznxwqm9sc7g6n7bxbwlskac42dmdlv0qd")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.14") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "17jalxd1bw2mrry1wcz30spv5fr7slizl393rwhiyfqmkh85nna7")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.15") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0y89hnkijq6xr18j9c9x63x4yba14z6l6lllwwwja5py66fys4fp")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.16") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0spv3rw18bz2q467g2sqhas8568baf2yrqfms3cbhl3s498ppqjp")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.17") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1c22l2xkj2nc6yq0myr0ryamj1s4km34iw8afv0asji29q5rjfrc")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.18") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "19m46fndnpwi4z2g70wh3dz369s55dfvhf978i7pvdbalrsjnwnp")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.19") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0jnabcfv3pfxxnmp40vsjx2di5wka6zk31z3nd1iw1cjckg80ph2")))

(define-public crate-elf-utilities-0.2 (crate (name "elf-utilities") (vers "0.2.20") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1zhlzaawpwjg67s9yi428nniqbqq5mhjiddw7cz2s8dl25f8734j")))

