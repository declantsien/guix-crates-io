(define-module (crates-io el es) #:use-module (crates-io))

(define-public crate-eles-1 (crate (name "eles") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 2)))) (hash "1q5n3gizcawgphyr1qxxy4qgq8c49sfshcwhqiv2bc7wdm2mc67n")))

