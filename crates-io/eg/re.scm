(define-module (crates-io eg re) #:use-module (crates-io))

(define-public crate-egress-0.1 (crate (name "egress") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "fs2") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1mz60h58vlf087ljx5k7jsjgpy67pv73ivl8vh2cqqk4fz62habs")))

(define-public crate-egress-0.1 (crate (name "egress") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "fs2") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0mgzw83w6sl40m3vq7ibxb3rbw7a47vjk40kcwf1s0csnwghc3j6")))

