(define-module (crates-io eg -s) #:use-module (crates-io))

(define-public crate-eg-seven-segment-0.1 (crate (name "eg-seven-segment") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 2)) (crate-dep (name "embedded-graphics") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics-simulator") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1kq8nqnjjyirfpyzsvlj5xkl79jcaqi9w6g806gl2als2md57fhj")))

(define-public crate-eg-seven-segment-0.2 (crate (name "eg-seven-segment") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 2)) (crate-dep (name "embedded-graphics") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics-simulator") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "0yxq4jsp9vcdbfx38r9npxc0snhp0qcn8p46f4y36jr3xma2yzby")))

