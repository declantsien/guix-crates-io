(define-module (crates-io eg ak) #:use-module (crates-io))

(define-public crate-egak-0.1 (crate (name "egak") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.20.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0xblmnpfqd1wc006911aaj2raxfs00xm18ss7bvb9022pkry75hf")))

(define-public crate-egak-0.1 (crate (name "egak") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.20.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0ayw4n2fsfn800vhjj0r4qs64biyaf6nzasrvfg06px8y5mrsswv")))

(define-public crate-egak-0.1 (crate (name "egak") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.20.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1mrcjlhi6f5506zcizxh3f8hqabsxvyn77svdqmgmb86snw2zvkd")))

(define-public crate-egak-0.1 (crate (name "egak") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.20.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1d4f2qa9h6vls763bmxm0x4g4gizcpfp1f1hfpsrqrsa0j489ay5")))

(define-public crate-egaku2d-0.1 (crate (name "egaku2d") (vers "0.1.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0l647b2hrp3vdqah68vhd48lyjwl0563avc3mmpmh4m0v7x8wk71") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.1 (crate (name "egaku2d") (vers "0.1.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "17c2r7n5lmhbi3szpvqzvn72ib58wdkfpi0q9vy76jbnzb3y6jza") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.1 (crate (name "egaku2d") (vers "0.1.2") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0pigqjq6vav86za44sd01mn1w09rw0i9mcjshqrk88v0rprjxji7") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.1 (crate (name "egaku2d") (vers "0.1.3") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "10ps07ydgrrm9qr564xknrq6ra2garhz3d8sy3f1f7vcx995fiwq") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2 (crate (name "egaku2d") (vers "0.2.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0a6a9xrgmn4xbd453sqj00s2bzlfhqqfsrrz5j1x582159xhs0hv") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2 (crate (name "egaku2d") (vers "0.2.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "11izdagg7pmfcinckp53w3mfqs7l5zfl56jrvqkbda2ilx5icrw6") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2 (crate (name "egaku2d") (vers "0.2.2") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0rfgqahfd9hhnwav1m405q738l7vnz6ixyis7s3fh6piq6dkafl5") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.2 (crate (name "egaku2d") (vers "0.2.3") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "1qcidyymjzp826wgjh0aqpjh9mfmwrc8nrr3pf46wq88wqwqnhpc") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.3 (crate (name "egaku2d") (vers "0.3.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "1w5dbi05x5dwycv9kvbr56ihrm0bqc0vmwp3xlgn8qyq29mj5xdd") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.3 (crate (name "egaku2d") (vers "0.3.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "1ncb5jk7p2qcw9xkf2qygdf4w7xf9vz8gij8dqn8b2y8yrb8xzrp") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.4 (crate (name "egaku2d") (vers "0.4.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0bb6z7is3nxy7mlh54crb3p00ywlb5211wjiffqrk3f3dcqfzibn") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.4 (crate (name "egaku2d") (vers "0.4.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0vngwnm00shb1xzcyykd03kpmvmi694r1gpldi1zf9rwwysrz66g") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5 (crate (name "egaku2d") (vers "0.5.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0rbikgraxlx08dvg4jq2gyvgahpsyndjlhhjwzfx2avi0bic2b5l") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5 (crate (name "egaku2d") (vers "0.5.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "egaku2d_core") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0f8mhw3al56wacxbybgdgjvfh6xdlb482c8ribf4m0jhy379zg5h") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5 (crate (name "egaku2d") (vers "0.5.2") (deps (list (crate-dep (name "egaku2d_core") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "0895zdcp2xn22na9kg0idp0hrw5b47cq9zg9yql27zipx46qaw75") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5 (crate (name "egaku2d") (vers "0.5.3") (deps (list (crate-dep (name "egaku2d_core") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "11dgfamrc1z65svvzaxgl6x0m3zlnh9ibmblrc95mpy37v6r3k92") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d-0.5 (crate (name "egaku2d") (vers "0.5.4") (deps (list (crate-dep (name "egaku2d_core") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "1cwpnjfikg6nixqqgqamp716wn3zz65nmrgdd29r1r3p1mm8v078") (features (quote (("fullscreen"))))))

(define-public crate-egaku2d_core-0.1 (crate (name "egaku2d_core") (vers "0.1.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "0fz19j590xj1nppijl74mr7bbffwy8b7g2b3lyi6m0gscpjd2zzq")))

(define-public crate-egaku2d_core-0.1 (crate (name "egaku2d_core") (vers "0.1.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "0dm6nv4fsbh3gwnmmd5y0cdxnaj0j8mz69bwhyfjajvcrq7gfp0x")))

(define-public crate-egaku2d_core-0.1 (crate (name "egaku2d_core") (vers "0.1.2") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "1bd9h3n0v5sbfirjy5159jy4mhrab628vz97fb21q4zxynmzsl3s")))

(define-public crate-egaku2d_core-0.1 (crate (name "egaku2d_core") (vers "0.1.3") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "03pmf7bffc19vhavxg8al0c5l9bwl21p4jfw8gnxs57g964c5kvk")))

(define-public crate-egaku2d_core-0.2 (crate (name "egaku2d_core") (vers "0.2.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "0hc0km9qxs65pfxwk0lxd8hkg8y2xajch7s3yzv2ic15lyzya8av")))

(define-public crate-egaku2d_core-0.2 (crate (name "egaku2d_core") (vers "0.2.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "1lhzm3rkbxbsm9a2yi50wafbly87lvwyaiij8ld0grdrvkc1p7rr")))

(define-public crate-egaku2d_core-0.2 (crate (name "egaku2d_core") (vers "0.2.2") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "1d91szakcrialv1d402lra6i2p65cdfmkpds3pc1if6nlcz85nbj")))

(define-public crate-egaku2d_core-0.3 (crate (name "egaku2d_core") (vers "0.3.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "1z489r2sngh8f2l3k2d9xa2fwkf7rli6i01my91ykfdd9q10v27z")))

(define-public crate-egaku2d_core-0.3 (crate (name "egaku2d_core") (vers "0.3.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "1cn4mns267c02a9v4j9w41l27x7l6c9r5qrbgjn9wknczxqhwasc")))

(define-public crate-egaku2d_core-0.4 (crate (name "egaku2d_core") (vers "0.4.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "0c1dhiih6cq4p3b7zy09km1k5gianzijp195fqd1rr1f0nhhzsri")))

(define-public crate-egaku2d_core-0.4 (crate (name "egaku2d_core") (vers "0.4.1") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "1fhz46dyf4g7im4vn9jg8q5lmg2a16yfnddy4238w07gmwxwalwn")))

(define-public crate-egaku2d_core-0.5 (crate (name "egaku2d_core") (vers "0.5.0") (deps (list (crate-dep (name "axgeom") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "16h1nf8zxcc0955xn54ljvmvl7bwiaqm38b1rz1ll2dsrglxmlws")))

(define-public crate-egaku2d_core-0.5 (crate (name "egaku2d_core") (vers "0.5.1") (deps (list (crate-dep (name "axgeom") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "065s27qxd1jk6zshhcgq0lbmm7rjqhkbfv5wmdiq9zlmpi2msk5y")))

(define-public crate-egaku2d_core-0.6 (crate (name "egaku2d_core") (vers "0.6.0") (deps (list (crate-dep (name "axgeom") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "12jxsa0cl8wh30q2j1j7hv4n9dyam35q907wr17yvmfjawqdh1s4")))

