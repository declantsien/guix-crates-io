(define-module (crates-io eg ot) #:use-module (crates-io))

(define-public crate-egotism-0.1 (crate (name "egotism") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "ringbuf") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "07fwc1qnfijyagscj900mganiz2fbbl9gmz416w48yj849ad5lw0")))

