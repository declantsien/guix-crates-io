(define-module (crates-io eg m2) #:use-module (crates-io))

(define-public crate-egm2008-0.1 (crate (name "egm2008") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "16w7iladhwfdzs7wq6106snym8vvwi31g2s88b1xdfm06crd1jrc")))

