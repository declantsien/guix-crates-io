(define-module (crates-io eg o-) #:use-module (crates-io))

(define-public crate-ego-binary-tree-0.1 (crate (name "ego-binary-tree") (vers "0.1.0") (deps (list (crate-dep (name "ego-tree") (req "^0.6") (default-features #t) (kind 0)))) (hash "0xfvhzfil356j6ypfyjklqpaic0y9vpq088khiqxd3ihif2i2bcl")))

(define-public crate-ego-tree-0.1 (crate (name "ego-tree") (vers "0.1.0") (hash "13cpnxc22yjdbyy0l3yyhwlgg4f6mdaa3f28ljx3gffd3i5vinj0")))

(define-public crate-ego-tree-0.1 (crate (name "ego-tree") (vers "0.1.1") (hash "0kdcr7mgyj6n8r9axcjj1rm0vpraq6k470az3lvrmzvj9m6c956g")))

(define-public crate-ego-tree-0.2 (crate (name "ego-tree") (vers "0.2.0") (hash "0bdx0k46pc077y5sayn2xir8p4149vahz9zv744k0lmlfib6rb24")))

(define-public crate-ego-tree-0.3 (crate (name "ego-tree") (vers "0.3.0") (hash "1fjgwq3apfyfzqniy9fxklj11lp6rsf631iysy4mn6k81vbcb10x")))

(define-public crate-ego-tree-0.4 (crate (name "ego-tree") (vers "0.4.0") (hash "0nzbh6lpf4kihvv6ndjw5gd6dk0cla41k6zw0zlp0wlgfkk5gdgm")))

(define-public crate-ego-tree-0.5 (crate (name "ego-tree") (vers "0.5.0") (hash "1dhgpjazlyqdz0khr79r151006j3rklsrbppwr4p7b7ck66dnmgx")))

(define-public crate-ego-tree-0.5 (crate (name "ego-tree") (vers "0.5.1") (hash "001ws2ysc548v3j5hqxdjaqk6l7i40fysdavq9062wnirbqck8ib")))

(define-public crate-ego-tree-0.6 (crate (name "ego-tree") (vers "0.6.0") (hash "0x00g2l6p43l540pdpd2f5wjdswgqxg8bcik81dv4k3kl6nzccwp")))

(define-public crate-ego-tree-0.6 (crate (name "ego-tree") (vers "0.6.1") (hash "1hxwfrrzmzbhqh5zxq98maj761xy4qmmircfpvlrsd5w52hql187") (yanked #t)))

(define-public crate-ego-tree-0.6 (crate (name "ego-tree") (vers "0.6.2") (hash "149mkc6j7bx653s39rpnsnpl5dvd1rj18czcil57w54k868a8s1s")))

