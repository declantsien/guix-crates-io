(define-module (crates-io eg gt) #:use-module (crates-io))

(define-public crate-eggtimer-0.1 (crate (name "eggtimer") (vers "0.1.0") (hash "1qzp6hwvrwcp4d5sbrjqqlsfyhhkid0z8w7kypr1v0zl535w7sjx")))

(define-public crate-eggtimer-0.1 (crate (name "eggtimer") (vers "0.1.1") (hash "1vjx5zcfsy2prh566130gs67pbxs8x1sa42q4gwlz7jkfjqvg5in")))

(define-public crate-eggtimer-0.2 (crate (name "eggtimer") (vers "0.2.0") (hash "1672llwjffvk3xfalm8f4hsalv0600w44h9bi04zhacnbmisbwmz")))

(define-public crate-eggtimer-0.3 (crate (name "eggtimer") (vers "0.3.0") (hash "08ibllc6n9wzcrhrmk31nk4gjpflfzkf7vz6mg83d115i9igz7mq")))

(define-public crate-eggtimer-0.3 (crate (name "eggtimer") (vers "0.3.1") (hash "1vxxpizv7xq9dl8vqj6pcg2xyv445gbk55cbd4j3fwhw812qz29d")))

(define-public crate-eggtimer-0.3 (crate (name "eggtimer") (vers "0.3.2") (hash "0yf9mqlgb2r93vjqx5qqrl8cdvzf94pfk6c6a8a4p6dbrynh8wmc")))

(define-public crate-eggtimer-0.4 (crate (name "eggtimer") (vers "0.4.0") (hash "0vgq7lfrk450wzxszr9qr2nln5cgq8vf283k8xk15acnkrxnhzh0")))

(define-public crate-eggtimer-0.5 (crate (name "eggtimer") (vers "0.5.0") (hash "0mml8w17dj67mp20gyh5nd3hqc0ic7829flfmbddzhdsfxxal7bc")))

(define-public crate-eggtimer-0.6 (crate (name "eggtimer") (vers "0.6.0") (hash "0r2j6qj15f466nbwa62830z4dzp9cjhyrjrm68c01iafy28x3bym") (features (quote (("f64") ("default"))))))

