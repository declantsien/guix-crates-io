(define-module (crates-io eg zr) #:use-module (crates-io))

(define-public crate-egzreader-0.1 (crate (name "egzreader") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pj6y2svdc9wljzrnj9jbsfdjybsg9qm0cbn8c637ca9c609hzgv")))

(define-public crate-egzreader-1 (crate (name "egzreader") (vers "1.0.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p2jqbdd6ld7vfxw0i1c6hdwpwdy8hsgk9i2jax11dib92i6nw7f")))

(define-public crate-egzreader-1 (crate (name "egzreader") (vers "1.0.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sifs75666rkby00hn6aqjdyhyryhm814h3g5h31pm1m0yfqlf3z")))

(define-public crate-egzreader-1 (crate (name "egzreader") (vers "1.0.2") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rqijmjwc2jqy75iaq72i0v29g0mlbn6m7sy9c3jqf0lj4jpw0dl")))

(define-public crate-egzreader-1 (crate (name "egzreader") (vers "1.0.3") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zdcgxhanyhg0fr5fa497z7yrdh2if60knrj237rmv0aj2nzin84")))

(define-public crate-egzreader-2 (crate (name "egzreader") (vers "2.0.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nffwpab9d1h0y0q1w0k247ciq7gng8fm2x3bfddikv5hdx3amdx")))

(define-public crate-egzreader-2 (crate (name "egzreader") (vers "2.0.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xzmvgx4wjba89xywrafar2vkbj5a3p6wkyk2b8v7rx7nbkyb5fi")))

(define-public crate-egzreader-2 (crate (name "egzreader") (vers "2.0.2") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "16b61kh56iwm014bvzrkr6z36ib023nr7q1cdfrkm4y2gicrwxib")))

(define-public crate-egzreader-2 (crate (name "egzreader") (vers "2.0.3") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "032yrwqdiy7fkibnwjgf4x4ahil8dpf1xalkbzh23nl408nvql40")))

(define-public crate-egzreader-2 (crate (name "egzreader") (vers "2.0.4") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zd52a6q57wwhbgijm5ac5dnbhypc8h6yjgzx5r2k8dabqg6b3s5")))

