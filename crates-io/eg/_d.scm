(define-module (crates-io eg _d) #:use-module (crates-io))

(define-public crate-eg_derive-0.1 (crate (name "eg_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1csqdjri8c9ifbpqry72kn75pgy3njwwdw1hamm9kfcqcs8rrsil")))

