(define-module (crates-io bv re) #:use-module (crates-io))

(define-public crate-bvreader-0.1 (crate (name "bvreader") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "07vw1p75cqbbfhhjff62fax4ca7acins9nsjhj571lpvifi58hps")))

(define-public crate-bvreader-0.1 (crate (name "bvreader") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0fmvzi06i15iks4ln5jiax28f9qaznm1zkiyklp87368f75dh6ck")))

(define-public crate-bvreader-0.1 (crate (name "bvreader") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0wygklkn6zbprl0fnqqf8lpyzl3wwa5rqfz88b25q5ibygynag4p")))

(define-public crate-bvreader-0.1 (crate (name "bvreader") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "1dn00yc50akj8g3z09j5x0f73h3ihd18r62c0v1935d1jy6p8iwl")))

(define-public crate-bvreader-0.1 (crate (name "bvreader") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0zll70m79lkyj58zwjkpwgahv401xmzaw897swk7bkg1w1vizna9")))

(define-public crate-bvreader-0.1 (crate (name "bvreader") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "11a09rdzrd20g926c6g2z9vfll5m29fvgg9zshb18yjxfhv1bbmj")))

