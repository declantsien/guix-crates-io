(define-module (crates-io bv h2) #:use-module (crates-io))

(define-public crate-bvh2d-0.1 (crate (name "bvh2d") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.21") (default-features #t) (kind 0)))) (hash "17dmk73mn73azad6yw6d8zb5ii39jiy56f7jp3icz1gf2z6ay9gh") (yanked #t)))

(define-public crate-bvh2d-0.1 (crate (name "bvh2d") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.21") (default-features #t) (kind 0)))) (hash "1r3xrbi3cj8gp5wkiz8h5akkq7hsdlf1mp19sig6rqhn1az7wzi5")))

(define-public crate-bvh2d-0.1 (crate (name "bvh2d") (vers "0.1.2") (deps (list (crate-dep (name "glam") (req "^0.21") (default-features #t) (kind 0)))) (hash "0jhwhni62hibyimxkh8kw7sdh9wy288pas3asd8jyfahy967zk2v")))

(define-public crate-bvh2d-0.2 (crate (name "bvh2d") (vers "0.2.0") (deps (list (crate-dep (name "glam") (req "^0.22") (default-features #t) (kind 0)))) (hash "1flnnl52vfl7plpwcq8y7ryqmiv14zr2ygchbfzl73kfb0pj0w65")))

(define-public crate-bvh2d-0.3 (crate (name "bvh2d") (vers "0.3.0") (deps (list (crate-dep (name "glam") (req "^0.23") (default-features #t) (kind 0)))) (hash "08nrv3x4h87n7gxmad3sd6bwlqk4i2y86383x358h1wmbqmwywa8")))

(define-public crate-bvh2d-0.3 (crate (name "bvh2d") (vers "0.3.1") (deps (list (crate-dep (name "glam") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16p8hx2id65brj2xj3lwj6s372mynwdxpnr6zzczv3a3825yh75j") (v 2) (features2 (quote (("serde" "glam/serde" "dep:serde"))))))

(define-public crate-bvh2d-0.4 (crate (name "bvh2d") (vers "0.4.0") (deps (list (crate-dep (name "glam") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0g4rwjv8px28ishy0in08p902vpmy3m4av0xr9vmhlz3nyj6ya4l") (v 2) (features2 (quote (("serde" "glam/serde" "dep:serde"))))))

