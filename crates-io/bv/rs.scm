(define-module (crates-io bv rs) #:use-module (crates-io))

(define-public crate-bvrs-0.1 (crate (name "bvrs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dil1qk803d039qwha5vnhra0z43mzng0x78gbrbnrg48y74hkk2")))

(define-public crate-bvrs-0.1 (crate (name "bvrs") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ymcxpfjc0p7zyq81cv0zxshpk2sc2x76dmk2bmyf7ws4yc5k4qa")))

