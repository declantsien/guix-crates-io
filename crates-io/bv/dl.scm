(define-module (crates-io bv dl) #:use-module (crates-io))

(define-public crate-bvdl-0.1 (crate (name "bvdl") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.3") (features (quote ("gzip" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03dmvdczzsyi5vr5rb6pryqw1xqhxwghpzm6ncbpm07k7gm26fzw")))

