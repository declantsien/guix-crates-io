(define-module (crates-io bv ar) #:use-module (crates-io))

(define-public crate-bvarint-0.1 (crate (name "bvarint") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "12vjhh81sjan0ss7i7phrshr83bv7sw5w400zda8z7xqgby5jxbb")))

