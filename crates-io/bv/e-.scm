(define-module (crates-io bv e-) #:use-module (crates-io))

(define-public crate-bve-derive-0.0.1 (crate (name "bve-derive") (vers "0.0.1") (deps (list (crate-dep (name "darling") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1bsj48q5gbin7ficlfnmnpnbl58bjadn7l1h8f67qn07x50fv8a0")))

(define-public crate-bve-native-0.0.0 (crate (name "bve-native") (vers "0.0.0-Placeholder") (hash "0inhrncvhb8i43fig5r6px6vdssijd6hrj1vmk2fsdfbk9zjf797")))

(define-public crate-bve-native-0.0.1 (crate (name "bve-native") (vers "0.0.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.42") (default-features #t) (kind 0)) (crate-dep (name "bve") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "bve-derive") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "1k7k8a8ymh8z7pdg06wndbbpwljqclwrm7hgsw6826v3j57spj6m")))

