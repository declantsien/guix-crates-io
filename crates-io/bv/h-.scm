(define-module (crates-io bv h-) #:use-module (crates-io))

(define-public crate-bvh-arena-1 (crate (name "bvh-arena") (vers "1.0.0-beta.1") (deps (list (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)))) (hash "13p1sxqhz5nfa1maqg6ghf2rmi37hg3rwdvhp3k1359wp97dcpl1") (rust-version "1.58")))

(define-public crate-bvh-arena-1 (crate (name "bvh-arena") (vers "1.0.0-beta.2") (deps (list (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)))) (hash "0la91ja0d5aj159hayshf4f3xav8i3pzbqc5lmqaxwkj33mw999l") (rust-version "1.58")))

(define-public crate-bvh-arena-1 (crate (name "bvh-arena") (vers "1.0.0") (deps (list (crate-dep (name "glam") (req "^0.20.2") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)))) (hash "1aw7vnx8mizcb4v9si56khjbxw96i47h63z6vwvajjnxxycjyh5x") (rust-version "1.58")))

(define-public crate-bvh-arena-1 (crate (name "bvh-arena") (vers "1.1.0") (deps (list (crate-dep (name "glam") (req "^0.20.5") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)))) (hash "00sq08y6bqz8hw9wgmgkkdc8n8hahnvf1m6x5pqlxh1bi09gfnaq") (features (quote (("std" "slotmap/std") ("default" "std")))) (rust-version "1.58")))

(define-public crate-bvh-arena-1 (crate (name "bvh-arena") (vers "1.1.1") (deps (list (crate-dep (name "glam") (req "^0.20.5") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)))) (hash "0z6yxz8q7synmapcwa1ls6sxadpm6mra4n3vhnc3022r9cvzmmvm") (features (quote (("std" "slotmap/std") ("default" "std")))) (rust-version "1.58")))

(define-public crate-bvh-arena-1 (crate (name "bvh-arena") (vers "1.1.2") (deps (list (crate-dep (name "glam") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)))) (hash "0fcqi8plq28gfydrqvmhakbq38nyk28pa1pc7hgbqj95348y2ni8") (features (quote (("std" "slotmap/std") ("default" "std")))) (rust-version "1.58")))

(define-public crate-bvh-arena-1 (crate (name "bvh-arena") (vers "1.1.3") (deps (list (crate-dep (name "glam") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)))) (hash "1g8ynfqbhigwyn3y5x4n1dn3gr6z8q9kfj9068adw63dy223sdqa") (features (quote (("std" "slotmap/std") ("default" "std")))) (rust-version "1.56")))

