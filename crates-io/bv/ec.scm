(define-module (crates-io bv ec) #:use-module (crates-io))

(define-public crate-bvec-0.1 (crate (name "bvec") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.24") (default-features #t) (kind 0)))) (hash "1phlc46506ciy0g1ymd77yl1d2bv2pb5xbl5pbyrrmn13gcachix")))

(define-public crate-bvec-0.1 (crate (name "bvec") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.24") (default-features #t) (kind 0)))) (hash "1ar2sxlccg51plbw9kysm00v5ygzklzwxkyzh41c7armscsbxr91")))

(define-public crate-bvec-0.1 (crate (name "bvec") (vers "0.1.2") (deps (list (crate-dep (name "glam") (req "^0.24") (default-features #t) (kind 0)))) (hash "09zynsvg295fxhwi33iah0x1qynwvf3741gh6gwlz996llkr4p6n")))

