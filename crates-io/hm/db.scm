(define-module (crates-io hm db) #:use-module (crates-io))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.0") (hash "0p44myp55cam4bfwmlvvp20ps8gwidml2hcc976q4zsi5xwkybsl")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gcr1xgwpm1qk3ijqy5vl8lkxvd03zn2v4fs9cl5cwr1fxcby9l2")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0kgyrg541yvb0xbplawjb0p16r9d0ivjz326n4s7rs73r78dprw0")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.3") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0z21i8hw6shx1ymd5b902xavqmnxadypnwfqil8p1gs9cr4a7h4p")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fwk5srbk828rsx61gpaqci7hdc73ids0q1psmhiam4vawj2xhdk")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.5") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1s9li7z103pgwc7f4ar9b2n5mx4p2avc2f3y3qdwzvhklah3flxd")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.6") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18rl6g2bx0rpsr7b457ychv6bz8928198kjx1ykngjifj0npw22p")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.7") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "169xy7j3jdjvr4r44dj5fkl1bbnf1mnqr0igs26sjn587yvsaxa8")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.8") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ixnry0473kky5s1zsw0lhxqkbnr7dh74qn215a5h66l87sgmzpv")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.9") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12vwdwsz1jwhn4s5yyjbnxxfrdp5ql6g6c6zwc6d4avqsry3nrs8")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.10") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "1dq2lgjb403mab0dcpbsy2zdspc5z30f8ychb908qbh2xcgwi2di")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.11") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "1i7k5i1w9msqk77b62nqqfwwyv02a07xmb74vipdaghzxi8cx0a3")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.12") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "0p9rfs3fcpnp908skrr1vvihni9dz791rqv4796ix1xagv7qrszg")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.13") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "1hm58mdq1l7kasm473c10yl6jwi3952ai780v178zdm92acrm2nq")))

(define-public crate-hmdb-0.1 (crate (name "hmdb") (vers "0.1.14") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "0igjan91ci3qkch14caxknv4zqxdsick9qkjr8pizc77d0b8i2yv")))

(define-public crate-hmdb-0.2 (crate (name "hmdb") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "1g3x704lfrisdp1vrkkl8n0ljm38l9qd5zqwlk4d33vvav2r3nl5")))

(define-public crate-hmdb-0.2 (crate (name "hmdb") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "0k1md9mkhjl43g1hlamcwk9grxxaiq5s9cc04y60ri9bzf9vvlrk")))

(define-public crate-hmdb-0.2 (crate (name "hmdb") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 2)))) (hash "1d2m6apsv3lhd2ph8f3zq31gj25c41qaxkp82jczdxsdj106yld3")))

