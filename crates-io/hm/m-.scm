(define-module (crates-io hm m-) #:use-module (crates-io))

(define-public crate-hmm-rs-0.0.1 (crate (name "hmm-rs") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "0rz9pmcsqx5mbb4glmk2k0bakl1h2mcf53rzv6hi807zndwhdl49")))

