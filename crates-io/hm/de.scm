(define-module (crates-io hm de) #:use-module (crates-io))

(define-public crate-hmdee-0.1 (crate (name "hmdee") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "hmdee_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "psvr") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ffrwh92zvx1avk5925fmr5vjjp1wnxa5hky0xbr1vigfrcvmqnf") (features (quote (("default" "psvr"))))))

(define-public crate-hmdee_core-0.1 (crate (name "hmdee_core") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (default-features #t) (kind 0)))) (hash "0pcv3yy06v0kcxazxxk8k267i0p89bnf9dqq1bvyy39y8sb2310s")))

