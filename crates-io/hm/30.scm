(define-module (crates-io hm #{30}#) #:use-module (crates-io))

(define-public crate-hm305p-0.1 (crate (name "hm305p") (vers "0.1.0") (deps (list (crate-dep (name "serialport") (req "^4.0") (default-features #t) (kind 0)))) (hash "09zig4aql4lv6nj1cy1979j1pvyc0njqhq63vqq9hk3hj8vzfbwh")))

(define-public crate-hm305p-0.2 (crate (name "hm305p") (vers "0.2.0") (deps (list (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "163fjxiacfr7xm4h1jm670piy2y28g4dpcx4vs2z99avfa8049lx")))

(define-public crate-hm305p-0.3 (crate (name "hm305p") (vers "0.3.0") (deps (list (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gkrzyf6fh2ajda6rf03awmnfj88i5qhdkl3zx4ryn6p5g0zcwxn")))

(define-public crate-hm305p-1 (crate (name "hm305p") (vers "1.0.0") (deps (list (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "05yg68jcvvq393cliyhpqw28jrr8271knpgrsnijmwi0jpac4sy2")))

(define-public crate-hm305p-1 (crate (name "hm305p") (vers "1.1.0") (deps (list (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fzjhw437l16555zyirqivqgak4v4nfq9z4iyiva8xmnk7l3xlzs")))

(define-public crate-hm305p-1 (crate (name "hm305p") (vers "1.1.1") (deps (list (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "070jwfdpwmsjiy6aa6f171dj10v1czj508dz29dkmac1lj6188pb")))

