(define-module (crates-io hm at) #:use-module (crates-io))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0gdjrwzb2n04xx66hs7g0vf2fsrz36phgs7mrqsncnigrcjnav0c") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "169i89ii3dsbwqhw4xammvbg710w5b73qs8kg2yygjqf0v2g59mh") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0vcpzfr4d3q055wvwkc3b6rgwq74z76h3rrrqf0szazrfzxsrl8r") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1ks8xaivlmmyz7hzy8m84xkc7n2z6kl2kran93xcb3d5yzzcv926") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0gb9mcxyjf9ncaim8c8bj5k1rhnfv6d0as258ai35f222b76fmw6") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1gva15gjgy9a3rv7bh3iqkv13k5w7g7j3dvidl160jhxy37avrp0") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0y3v8d2pylsfkj1946plvkb05lq9ivivx0qnnqx4a762l1hbm8lx") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1ilwazyd6a1lsr7xvw4x41k0ymjv3pv5vn50cxqjjr6c65jvpijh") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1b56h6h90afkjmkkbif9znsvsi8fcr3iy72dzivyafprnj6gl5yl") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.9") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1npyaxrc6pgx4d3ffhlb3n828181f2x0635d92n67d9wx52ij5xy") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.10") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "046qxqi1p14db75d2k5li7jz7d6y2djjsv2c454xg2z0vhdvzcvp") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.11") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0bsbl4vbv4vv82g2y3kc7i2bd8rd8j3yqxpdkgysbl0xz175s8yh") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.12") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1il8q40421ngjc9bf7kz4amajbxv3vkg2zd5ac7fpnksd2299vl7") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.13") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "13xqhghb0x9x848aj91srhvc53f6a2arzwr5b123vf9g7z1qf0fv") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.14") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1aqdh509kajadlg15w3bldnar2ifq0ag7rh54msh16dzqclrrv9b") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.15") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "11mywp3n0dy1vhlfikwy9isphxl3yhhvzswhybn3lfdrr0gnrxym") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.16") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1z9f7hi19l0ry8ial1nlp4nwm7nry3nawyb8zh5ncbc9mk7l5db6") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-hmath-0.1 (crate (name "hmath") (vers "0.1.17") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1hzv0aw78dlqpzlmrgs7n51brchnypz2wrsj82fi14vq026cg7s8") (v 2) (features2 (quote (("rand" "dep:rand"))))))

