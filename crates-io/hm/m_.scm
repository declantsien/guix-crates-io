(define-module (crates-io hm m_) #:use-module (crates-io))

(define-public crate-hmm_tblout-0.1 (crate (name "hmm_tblout") (vers "0.1.0") (hash "15x24p8vy4kgcwaq73lzivblsrg6rs5xhi17wb59m3f6pgkp9byd")))

(define-public crate-hmm_tblout-0.2 (crate (name "hmm_tblout") (vers "0.2.0") (hash "1vpkaabv1l6q3siwznk79j8dza5na3jv5dmpkrr4gzlj6dpi29y5")))

(define-public crate-hmm_tblout-0.2 (crate (name "hmm_tblout") (vers "0.2.1") (hash "1m5cr39ly86wsrapcba7zi96r9wbs3alifx53b3zqan2v1a1mssf")))

