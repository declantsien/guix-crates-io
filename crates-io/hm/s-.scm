(define-module (crates-io hm s-) #:use-module (crates-io))

(define-public crate-hms-common-0.1 (crate (name "hms-common") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "08p03r28g6dj8lpb5a2hcsb8g7m5hr9pa9hrap46h1hpvp4y1na3")))

(define-public crate-hms-db-0.1 (crate (name "hms-db") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^2.1.4") (features (quote ("chrono" "returning_clauses_for_sqlite_3_35" "sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "hms-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "0ljghsxv22hpb37xs1bkpfjrm05rri5aapwvxi9l4rgm0qzdgsj2")))

(define-public crate-hms-test_utils-0.1 (crate (name "hms-test_utils") (vers "0.1.0") (deps (list (crate-dep (name "hms-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 0)))) (hash "1ygdhx62zspqbb79llm3xia5jn2x2kav8i856fdyiikgsx2b815f")))

