(define-module (crates-io hm l-) #:use-module (crates-io))

(define-public crate-hml-rs-0.0.2 (crate (name "hml-rs") (vers "0.0.2") (deps (list (crate-dep (name "utf8-read") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "12lydak15x0s0hhi9r0yq5hfna8w6aiqqwshxlrkrz2grjzs5al1")))

(define-public crate-hml-rs-0.3 (crate (name "hml-rs") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "~4.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lexer-rs") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "utf8-read") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "1xg0fsknc2wiq1vahppnkdv6kvyp7cjq9hwvzq513kd13hd0v5k8") (features (quote (("default" "xml")))) (v 2) (features2 (quote (("xml" "dep:clap" "dep:xml-rs"))))))

