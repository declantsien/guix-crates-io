(define-module (crates-io hm me) #:use-module (crates-io))

(define-public crate-hmmer-rs-0.1 (crate (name "hmmer-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "libhmmer-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)))) (hash "0bx68ri3p073jyhr0jy1b9f0mvldnpsnjpqj0m8y1jmmn2md3an0")))

(define-public crate-hmmer-rs-0.2 (crate (name "hmmer-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "libhmmer-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)))) (hash "02gmp595rw9xxjghgrc78h3mfparqwdff4ghafz6xk4pfz2hsqgy")))

