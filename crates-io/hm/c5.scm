(define-module (crates-io hm c5) #:use-module (crates-io))

(define-public crate-hmc5883-async-0.1 (crate (name "hmc5883-async") (vers "0.1.3") (deps (list (crate-dep (name "defmt") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "02ydgv4kb26ckqdi1m8hxzi5k08hxg5i5xdp1vvcny68hz1fq5ny") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-hmc5883l-1 (crate (name "hmc5883l") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1pax72pjwpnip7vhz47qa9sl8q1ci878lz3bwzimh1dml69nhlqa")))

(define-public crate-hmc5883l-1 (crate (name "hmc5883l") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0i4hb9v2521611khddvyyh3zcpl03hpi2lppbi07kif1sp2pszzq")))

(define-public crate-hmc5883l-1 (crate (name "hmc5883l") (vers "1.0.2") (deps (list (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "046p530mf8fkdr2b44zp6h303w1vc87n9m25vlk3n8q5ksnm95yc")))

(define-public crate-hmc5983-0.1 (crate (name "hmc5983") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "16asfjqygmhy7y83yv978rrd2gr9g1qanisgv06dd9in9wx6d9bb") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-hmc5983-0.1 (crate (name "hmc5983") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "12da75f9gn1ljdsvbf6apcndc63fx395w6a4cyh6a4wqz83zzrn0") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

