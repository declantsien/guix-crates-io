(define-module (crates-io hm ap) #:use-module (crates-io))

(define-public crate-hmap-0.1 (crate (name "hmap") (vers "0.1.0") (hash "1khsz88r5202gjsdkh9whzqibyb8fvbblgvanycw06nsbd2d1ywj")))

(define-public crate-hmap-serde-0.1 (crate (name "hmap-serde") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "frunk_core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "13nmwfz8vr5n8078l17hfl1aiirwqksr6v4rg9yqshmy88arr327")))

(define-public crate-hmap-serde-0.1 (crate (name "hmap-serde") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("deref" "deref_mut"))) (kind 0)) (crate-dep (name "frunk_core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0nsllxwm07cm4j2z6z2fv5adsfsz6b3yiw3zh94xwvsw1wnapxk2")))

(define-public crate-hmap-serde-0.1 (crate (name "hmap-serde") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "frunk_core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "12h9mzwb5l0aaq3a407rpqj7acvhdwf1km278arkns4pjymm6sad")))

(define-public crate-hmap-serde-0.1 (crate (name "hmap-serde") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "frunk_core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1bfzpccn3x8jgdzfv0p4g27kf6jg8v7l9l7cyf465l3ijs94lx2b")))

