(define-module (crates-io nn di) #:use-module (crates-io))

(define-public crate-nndi-0.0.0 (crate (name "nndi") (vers "0.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "13d95cwa2j6l9ffma5b4zli0l842rxm29vw3y3ls3b0h6k40r6fy")))

