(define-module (crates-io nn er) #:use-module (crates-io))

(define-public crate-nnera-23 (crate (name "nnera") (vers "23.214.42") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clia-tracing-config") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ri8h1r61ing2iaci2v01jp5qfrlla3dy04xb9askg6mzqm1yqw4")))

