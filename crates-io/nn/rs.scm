(define-module (crates-io nn rs) #:use-module (crates-io))

(define-public crate-nnrs-0.1 (crate (name "nnrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (features (quote ("std" "backtrace"))) (default-features #t) (kind 0)))) (hash "10yqa24b8pw6icmk30vark5h61azgz202qgbdyjcyib134hnk0jd")))

(define-public crate-nnrs-0.2 (crate (name "nnrs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (features (quote ("std" "backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "1qd800anig90xxhppp9hhnr61zjyp1v6k4maa0vwbzqjv8zxw1dh")))

(define-public crate-nnrs-0.2 (crate (name "nnrs") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (features (quote ("std" "backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "0kri3dbnf1wg8rjwv6mr4g04gf6a38myfpr26vlvx6cg4g3cm35y")))

(define-public crate-nnrs-0.2 (crate (name "nnrs") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (features (quote ("std" "backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "078bbyndq8609l2qk59zb0k2lkb8mpjq49nvpvk8f0grakjfx20x")))

(define-public crate-nnrs-0.2 (crate (name "nnrs") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (features (quote ("std" "backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "07wlcxr38g5gv9gl6dm4nljpnsrd8fxm2zx9p3nl2awhglb53chw")))

