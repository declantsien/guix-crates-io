(define-module (crates-io nn mf) #:use-module (crates-io))

(define-public crate-nnmf_nalgebra-0.1 (crate (name "nnmf_nalgebra") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "10zrylra997rand1rsyspchc13xa3k1mv0dig6s8kl5qcqx377j1")))

