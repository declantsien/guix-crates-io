(define-module (crates-io nn et) #:use-module (crates-io))

(define-public crate-nnet-0.0.1 (crate (name "nnet") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0fnwyyyd7px8ka1lp8kva6l9kgs3nb9xmjp3zd39bqywsw0h9r4i")))

(define-public crate-nnet-0.0.2 (crate (name "nnet") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1pjlw2144bjmk7pnqw1x8wv367dkig9lhwk173ql48wh66cghyp5")))

