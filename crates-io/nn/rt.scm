(define-module (crates-io nn rt) #:use-module (crates-io))

(define-public crate-nnrt-1 (crate (name "nnrt") (vers "1.0.0") (deps (list (crate-dep (name "k_board") (req "^1.1.10") (default-features #t) (kind 0)))) (hash "1l3y1zv4639k5w6h845aky6wgvbni9x9hzz2inskg05d41rmbw8g")))

(define-public crate-nnrt-1 (crate (name "nnrt") (vers "1.0.1") (deps (list (crate-dep (name "k_board") (req "^1.1.10") (default-features #t) (kind 0)))) (hash "1h3rq2mbgffcg4sivhm0b1sg2xp4xcfw1qa6js8qiv620gwh1d6k")))

(define-public crate-nnrt-1 (crate (name "nnrt") (vers "1.0.2") (deps (list (crate-dep (name "k_board") (req "^1.2.1") (features (quote ("lower_letter" "upper_letter" "ctrl_lower_letter" "f" "standar"))) (default-features #t) (kind 0)))) (hash "1g35k4104wzqa9n8h8q5nzpkdqqpcaqdhn3arnxxj5a5amprg94q")))

(define-public crate-nnrt-1 (crate (name "nnrt") (vers "1.0.3") (deps (list (crate-dep (name "k_board") (req "^1.2.2") (features (quote ("lower_letter" "upper_letter" "ctrl_lower_letter" "f" "standar"))) (default-features #t) (kind 0)))) (hash "0mmbaadaigyj5sw7irj0q3q0r3y9xphqvgdyj13126d923npg2mk")))

