(define-module (crates-io nn -r) #:use-module (crates-io))

(define-public crate-nn-rs-0.1 (crate (name "nn-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29.0") (features (quote ("rand" "serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kkj85sjwnf3ibsqrd9q0njizx84d17jbjjyf8birl3mm3m8dhbg")))

(define-public crate-nn-rs-0.1 (crate (name "nn-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29.0") (features (quote ("rand" "serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s25hhnv2qqbjjf6fp0239g9vbafdy0m547pamvcci00mic5qgzj")))

(define-public crate-nn-rs-0.1 (crate (name "nn-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29.0") (features (quote ("rand" "serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "173x9q7zb2as47f1dncp34d1r4l04k402v39jck1k18vrgmrqq3f")))

