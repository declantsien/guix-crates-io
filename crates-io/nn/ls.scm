(define-module (crates-io nn ls) #:use-module (crates-io))

(define-public crate-nnls-0.1 (crate (name "nnls") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15") (features (quote ("approx-0_5"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-stats") (req "^0.5") (default-features #t) (kind 0)))) (hash "0dy8pp1l924d2wsyh08wjwkd0l4aly2a9p3xv83ssy5wgyqnk29y")))

(define-public crate-nnls-0.2 (crate (name "nnls") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15") (features (quote ("approx-0_5"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-stats") (req "^0.5") (default-features #t) (kind 0)))) (hash "1pm6yvc3ybcc8ii83874kqva7ldi6biywpmja5bwmwbrszvsiy36")))

(define-public crate-nnls-0.3 (crate (name "nnls") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15") (features (quote ("approx-0_5"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-stats") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ys3026iimqfnh2ab67x5dafwvb3n5p5sgx128jhzbfrxmcz0jfw")))

