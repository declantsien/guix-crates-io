(define-module (crates-io nn ap) #:use-module (crates-io))

(define-public crate-nnapi-0.1 (crate (name "nnapi") (vers "0.1.0") (deps (list (crate-dep (name "nnapi-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dyb4k5ns3xwp1viyr2vxy19lh22z4cixhpqdv85vnf1ha94dbkq")))

(define-public crate-nnapi-0.2 (crate (name "nnapi") (vers "0.2.0") (deps (list (crate-dep (name "nnapi-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g9x50y01lrmg01v0javfcx1sfhp41h1kh90alvjdp64zbkfibhb")))

(define-public crate-nnapi-sys-0.1 (crate (name "nnapi-sys") (vers "0.1.0") (hash "1pn45hf29bgbngxv0qm53hvg6bn1lc4446c31w0x42qjlchiqyfl")))

(define-public crate-nnapi-sys-0.1 (crate (name "nnapi-sys") (vers "0.1.1") (hash "107ra22y9xbahq7npgdj17z36p0bqm48b5q1lr2fn1lwm0y2f9s3")))

