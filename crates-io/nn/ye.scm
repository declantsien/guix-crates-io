(define-module (crates-io nn ye) #:use-module (crates-io))

(define-public crate-nNye-user_server-gateway-business-0.1 (crate (name "nNye-user_server-gateway-business") (vers "0.1.0") (hash "1s8ynwlzbpr6wlgwvkr2hb07c1q5y9p18qmsyi8bywsfn66iza7b")))

(define-public crate-nNye-user_server-gateway-business-0.1 (crate (name "nNye-user_server-gateway-business") (vers "0.1.1") (hash "05k0xx8x5jz9vkmwwcw3mka1w0z758lp31x0vq2v8h6nkb96fqkl")))

(define-public crate-nNye-user_server-gateway-business-0.1 (crate (name "nNye-user_server-gateway-business") (vers "0.1.2") (hash "0vs6wfn811fvi59b9cxj4qbc5svzgbi813g63a20zcclxz1q0mzl")))

(define-public crate-nNye-user_server-gateway-business-0.1 (crate (name "nNye-user_server-gateway-business") (vers "0.1.3") (hash "06wajapn5mvdfi11wvzxmdy0kqxlqj85f1vijjj6nr6d9rz03k6a")))

(define-public crate-nNye_solana_communication_layer-0.1 (crate (name "nNye_solana_communication_layer") (vers "0.1.0") (hash "1wq0h3az206ahb0dyz78dki5svgd6c65f26bvka8b13l1510ys4z")))

(define-public crate-nNye_solana_communication_layer-0.1 (crate (name "nNye_solana_communication_layer") (vers "0.1.1") (hash "1xaj65wmbq2ciwjqyagc3kvgk3h7dpbypajnf77dahrsqmv80wsr")))

(define-public crate-nNye_solana_communication_layer-0.1 (crate (name "nNye_solana_communication_layer") (vers "0.1.2") (hash "0lp22zqbxpx9vy5kdpb3p3jzq9ycfa0cjswjvlyp1qw1j2ibmadz")))

(define-public crate-nNye_user_business-0.1 (crate (name "nNye_user_business") (vers "0.1.0") (hash "1327835fmwinxnb6czkgcwcchalycfbh3png2whpxhi9wx000h7c")))

(define-public crate-nNye_user_business-0.1 (crate (name "nNye_user_business") (vers "0.1.1") (deps (list (crate-dep (name "nNye_solana_communication_layer") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "14dpp2qba11gc2rl61c2d8yk5nbim5h5j8qfw7a61cf7y1prs330")))

(define-public crate-nNye_user_business-0.1 (crate (name "nNye_user_business") (vers "0.1.2") (deps (list (crate-dep (name "nNye_solana_communication_layer") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0fg79bk7yrz5qd7z07r5cdbkk6bwp5dqmg2kjp7q9ybdmgfkr38h")))

(define-public crate-nNye_user_business-0.1 (crate (name "nNye_user_business") (vers "0.1.3") (deps (list (crate-dep (name "nNye_solana_communication_layer") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "08x9zppbpdc0vr9vv3kknwinx8bxhk5lip87fz9rq32xxmyfc5yg")))

(define-public crate-nNye_user_business-0.1 (crate (name "nNye_user_business") (vers "0.1.4") (deps (list (crate-dep (name "nNye_solana_communication_layer") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0za90v0a4iijqpxkzyci7p2vqxc3w8pr18sy61xdzy3ncvprqvla")))

(define-public crate-nNye_user_persistence-0.1 (crate (name "nNye_user_persistence") (vers "0.1.1") (deps (list (crate-dep (name "diesel") (req "^1.4.4") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1rikyha7rscdb2ql6z6vcl231yv04qw8m9m46y5rda1dkfpkpxg6")))

(define-public crate-nNye_user_persistence-0.1 (crate (name "nNye_user_persistence") (vers "0.1.2") (deps (list (crate-dep (name "diesel") (req "^1.4.4") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "002w66sxlrxbab05i61qv55dpz81pj16vv66l50bjk0vvxmv915k")))

(define-public crate-nNye_user_queue_persistence-0.1 (crate (name "nNye_user_queue_persistence") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "1il1bygz65j03rx8zady0xzawwm97s8b9c3n7rflirx2sbfb7i5c")))

(define-public crate-nNye_user_queue_persistence-0.1 (crate (name "nNye_user_queue_persistence") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "1fyh06wnc7g8adxs1knbp4vyqpyi0pbn46bplcsy16d8wi6dpdwl")))

(define-public crate-nNye_user_queue_persistence-0.1 (crate (name "nNye_user_queue_persistence") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "039sbczwd4dr0rvzy0h18j5baj79gh8x82jn2q2a94n7616c9f8p")))

(define-public crate-nNye_user_queue_persistence-0.1 (crate (name "nNye_user_queue_persistence") (vers "0.1.3") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "1k07mp4yz5vwqayplq5x7wjilmixpyayhcpc3xbvbxx4w8a4x7rk")))

(define-public crate-nNye_user_queue_persistence-0.1 (crate (name "nNye_user_queue_persistence") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_solana_communication_layer") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "0cvrxya3izdd105b46dv71rpfwx55ny468h8dy6w0psap4hgzngv")))

(define-public crate-nNye_user_queue_persistence-0.1 (crate (name "nNye_user_queue_persistence") (vers "0.1.5") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nNye_solana_communication_layer") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nNye_user_business") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "1vprv7f9vrkfsri9wg19cqw8biagb69lv120cd4lldjlrrh1xq20")))

