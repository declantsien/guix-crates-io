(define-module (crates-io xt p-) #:use-module (crates-io))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0p9qnm8lsasq5wcblkjyv8frnp4nkw8jyzjjmw0s44vw5ailhmdn")))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1-rc2") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0jxwr5hj4cb2kkmqgs7qcdghmwb47gkbrdq6l1fn7xgzlqdhp7wi")))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1-rc3") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "04343lp41jk1vg66cqyzy8iqb9ls8fas3xwjdqnrivngk9dadg28")))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1-rc4") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1m9fxw7yxl3h8bkcnw8x0xw361jpy19ad8f4j8dggbbf5cins0k4")))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1-rc5") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 2)))) (hash "00wv1x6yv37p4avjnix8cpy1m7q1cj704xgdf4x0z7g2qnvm8ari")))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1-rc6") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 2)))) (hash "0l8x5k11j2r7b0cy0xrk2mm21dvk901ki1c5jgr81ssfqbwa1866")))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1-rc7") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 2)))) (hash "157k378fvcbh6m9ykxj0mpm4k014lkgpyazmims4m7n0rnyjxwsc")))

(define-public crate-xtp-test-0.0.1 (crate (name "xtp-test") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 2)))) (hash "1wkygn8dfrszp5g1fwch47901vfh60x7qp6smx7m1gyp3nkgc6dg")))

