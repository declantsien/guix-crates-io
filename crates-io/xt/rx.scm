(define-module (crates-io xt rx) #:use-module (crates-io))

(define-public crate-xtrx-sys-0.1 (crate (name "xtrx-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)))) (hash "0yh67h3a4lf5x00dk22yvkg76pxb488ng6ny7q9av1clar051azs")))

