(define-module (crates-io xt ok) #:use-module (crates-io))

(define-public crate-xtoken-0.1 (crate (name "xtoken") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "166nh8i2wsfxp7zi6777myxflwkbi5djbila65ydhjvk1jcca3r1")))

(define-public crate-xtoken-0.1 (crate (name "xtoken") (vers "0.1.1") (deps (list (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1h3n8kn698g85c4371c1fhn5047ahh49gmqkaxyr5bvm57sf30lr")))

