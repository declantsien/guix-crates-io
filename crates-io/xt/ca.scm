(define-module (crates-io xt ca) #:use-module (crates-io))

(define-public crate-xtcat-0.2 (crate (name "xtcat") (vers "0.2.0") (hash "1p0rjfp1b6nfkkidsr7m68cyy2cbwbp1fzqjf0np29ifsykl0glj") (yanked #t)))

(define-public crate-xtcat-0.3 (crate (name "xtcat") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0msdd25ab9vpir47v7mnxhp5rcf3w9maix9zzkpwz0x6aq5a0dlz") (yanked #t)))

(define-public crate-xtcat-0.3 (crate (name "xtcat") (vers "0.3.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 2)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "03nkxgvwpzcq0ifq947cyg7wfc82kiqdsq732na1shqlmg8276vx")))

(define-public crate-xtcat-0.3 (crate (name "xtcat") (vers "0.3.2") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 2)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0cxqcn379ni2xby59zmrzn3mkhkhmfb3y41f6c247r9zblr4bp6k")))

