(define-module (crates-io xt sn) #:use-module (crates-io))

(define-public crate-xtsn-0.1 (crate (name "xtsn") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.26") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mal0zd8s9kgvn8q6zmw12m14nwmqvv9izw74k0r01qf54whqy1w")))

(define-public crate-xtsn-0.1 (crate (name "xtsn") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.26") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qy6m77ba9cm7vj30myvzhyrklbkcv04dhvwb6spnxis42y29s6g")))

