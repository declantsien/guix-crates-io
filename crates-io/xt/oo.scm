(define-module (crates-io xt oo) #:use-module (crates-io))

(define-public crate-xtoolbox-0.1 (crate (name "xtoolbox") (vers "0.1.0") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1wzyjdjygy02ybplxdqcw1fhqgnvh3rmkm8nbmab98g6p8wi0xd6")))

(define-public crate-xtoolbox-0.1 (crate (name "xtoolbox") (vers "0.1.1") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "16xy9y8hn464wkc554qx9pa904nbnypk1ylfnbszkk3w2hqh86ij")))

(define-public crate-xtoolbox-0.1 (crate (name "xtoolbox") (vers "0.1.2") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1fhpvrh815ifidmzybiynag9cl8vd3hv4nzpjv5v7r4q3qqzdrzp")))

(define-public crate-xtools-0.0.0 (crate (name "xtools") (vers "0.0.0") (hash "08my10xh5l38i33f9vjas6i6874r3irpixlwp47hjlm9kgqilh5b")))

