(define-module (crates-io xt ea) #:use-module (crates-io))

(define-public crate-xtea-0.0.0 (crate (name "xtea") (vers "0.0.0") (hash "1wmnbcvm44gg9kswq59yfwwdaya0ax725yb2831spyg0pahm26mz")))

(define-public crate-xtea-cipher-0.0.1 (crate (name "xtea-cipher") (vers "0.0.1") (hash "0qrfdc76wvm14hq59g5j6c16b61la3nfrhn5wqhvy0kfsiq0bxrl") (rust-version "1.60")))

(define-public crate-xtea-cipher-0.0.2 (crate (name "xtea-cipher") (vers "0.0.2") (hash "0f3s5k3qcm6h5hnrfv0ipag08slszd64qdpxrq3lcnqkh63bq3lw") (rust-version "1.60")))

