(define-module (crates-io xt om) #:use-module (crates-io))

(define-public crate-xtoml-0.0.0 (crate (name "xtoml") (vers "0.0.0") (deps (list (crate-dep (name "indexmap") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "06223pc9xjmvxi9znfbnkn3cl95mr24qybizsqm74f4j5azxij1b") (features (quote (("default"))))))

