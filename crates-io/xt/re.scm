(define-module (crates-io xt re) #:use-module (crates-io))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.0") (hash "1d6srnr8z5a62b74xwjz7syx5h44i6agrl15jspzdl02i7s8gcaf")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.1") (hash "1bkgvw4vhhnmjjzh9fw7ii939bznhjii84z9l5n7xqiwikf42zbq")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.2") (hash "012c8fv4jdsvykvzcvmz78zphf6a8gbjxm533r2n8xvqijix3gyx")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.3") (hash "0x5hgimi9az9w2vn5429d3vg6ff9xrww21sipqh7nqkdhp1ipckx")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.4") (hash "1k9yzd88qy94g0hgwiyzm6y1d6m9148kynh2gjdk1vip4hc1prgi")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.5") (hash "1qa48qx91hlddb90w1h7qb7g9wc4k0050jb65g2mxbmkk0rh56zy")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.6") (hash "03zjyr9yqaa79v5mvssqfrh981bw1cl7b5f1ypr96fg92fcmgnng")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.7") (hash "018vcgfygvcqdzzyf3l5kbdjh7720086bfglz9qjq2aiwzicqfx4")))

(define-public crate-xtree-0.1 (crate (name "xtree") (vers "0.1.8") (hash "1qq9c21z9pzvqdhzzbsmi3cmvzf197i6xcrjyy00zflyn282agyj")))

