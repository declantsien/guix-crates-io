(define-module (crates-io xt yp) #:use-module (crates-io))

(define-public crate-xtypes-0.1 (crate (name "xtypes") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "jens") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "jens_derive") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0zwylkl4185qvnx8cg2zw6bd3w1qik9cjfkr5pjmgs6j729yy810")))

