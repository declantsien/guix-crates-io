(define-module (crates-io xt ld) #:use-module (crates-io))

(define-public crate-xtld-0.0.1 (crate (name "xtld") (vers "0.0.1") (deps (list (crate-dep (name "psl") (req "^2.1.4") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1sjsqflhpay7pjy8hhmkh29mi881av7ml39fs6yjwj9p94xl6qp5")))

(define-public crate-xtld-0.0.2 (crate (name "xtld") (vers "0.0.2") (deps (list (crate-dep (name "psl") (req "^2.1.4") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "00xq7rc8yfbr27n53aah255m36z01h72yqvcbxmwd0qj32nkml4s")))

(define-public crate-xtld-0.0.3 (crate (name "xtld") (vers "0.0.3") (deps (list (crate-dep (name "psl") (req "^2.1.4") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0phl1nf8phzskx06z6jbd2vkgm1whmxrfmg9pcl481w41kc3wfhx")))

(define-public crate-xtld-0.1 (crate (name "xtld") (vers "0.1.0") (deps (list (crate-dep (name "psl") (req "^2.1.4") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wmp25r8jmwdr54y3dr3r88c69jsxfnc6jpdys6rcfp67vmd0b3z")))

(define-public crate-xtld-0.1 (crate (name "xtld") (vers "0.1.1") (deps (list (crate-dep (name "psl") (req "^2.1.9") (default-features #t) (kind 0)) (crate-dep (name "xstr") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xy8zzplm5mqd979vb25l1bkzdyj8gahwhzq6qjjnbh5yisnrvq0")))

