(define-module (crates-io xt b_) #:use-module (crates-io))

(define-public crate-xtb_api-0.0.1 (crate (name "xtb_api") (vers "0.0.1") (hash "095m3lxcdwxnkhiq48dg1nyk7bff1hkmly7c7bi3j15a6clbdzp3")))

(define-public crate-xtb_api-0.0.2 (crate (name "xtb_api") (vers "0.0.2") (deps (list (crate-dep (name "native-tls") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "02w5ggpmmbg9znv0kh2psyg706ilavfim8lcvl5yj18x1k95220k")))

(define-public crate-xtb_api-0.0.3 (crate (name "xtb_api") (vers "0.0.3") (deps (list (crate-dep (name "native-tls") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "0d1fdssz040daycn19bgh3lr12wx3zrpxyayzzf5iqn2jlv11rwb")))

(define-public crate-xtb_api-0.0.4 (crate (name "xtb_api") (vers "0.0.4") (deps (list (crate-dep (name "native-tls") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "007920xvn3si9cp98ans8ga6vkij7am0vhdy5gpjsxy90flm6jh4")))

(define-public crate-xtb_api-0.0.5 (crate (name "xtb_api") (vers "0.0.5") (deps (list (crate-dep (name "native-tls") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "0ch5ksmd26c1xd5awanjx0r2w4hbsiah293hh14skildhv2slr97")))

