(define-module (crates-io xs s-) #:use-module (crates-io))

(define-public crate-xss-evil-0.1 (crate (name "xss-evil") (vers "0.1.0") (hash "0a7iszkzcd7sq3r5a7xnmk0ji6ffy0pn9ng4k1ld80wn74rdlg18")))

(define-public crate-xss-probe-0.1 (crate (name "xss-probe") (vers "0.1.0") (hash "01xznailg0l7hvgcx4dcczbs48qb2af7c9l10hj7gmgcyx8qq0kh")))

(define-public crate-xss-probe-0.2 (crate (name "xss-probe") (vers "0.2.0") (hash "1fn5969zndnk6fs7a0z17ykf4y8f7g5fxsgipf3cw5qj3s94wgwi")))

