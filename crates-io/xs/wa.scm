(define-module (crates-io xs wa) #:use-module (crates-io))

(define-public crate-xswag-base-0.2 (crate (name "xswag-base") (vers "0.2.0") (deps (list (crate-dep (name "term-painter") (req "^0.2") (default-features #t) (kind 0)))) (hash "0a3qpjp4wp362v506mw2vi3ckqr11z3j8fqiysrqi9s62c4l97lz")))

(define-public crate-xswag-base-0.3 (crate (name "xswag-base") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "term-painter") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zinpyacdqz63isa7bwc4hq0lgxdhjnhhn10sv8z6nwqi2xm8xdq")))

(define-public crate-xswag-base-0.3 (crate (name "xswag-base") (vers "0.3.1") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "term-painter") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xdlgi05c37qx7jqaxvvvpq2hxxqm77s536n6s0p9mi4gk543cd9")))

(define-public crate-xswag-syntax-java-0.2 (crate (name "xswag-syntax-java") (vers "0.2.0") (deps (list (crate-dep (name "lalrpop") (req "^0.10.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "xswag-base") (req "^0.2") (default-features #t) (kind 0)))) (hash "09v8gnhra70ld6g6gdmshdygmw581qpm5p4gs2qnhlvvhkmgi6av")))

(define-public crate-xswag-syntax-java-0.3 (crate (name "xswag-syntax-java") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lalrpop") (req "^0.11.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "xswag-base") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qiq8mvwg4aadxs97x89bczv9g2bqvmcacjhan7wss292y1nw29r")))

