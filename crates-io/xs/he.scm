(define-module (crates-io xs he) #:use-module (crates-io))

(define-public crate-xshe-0.1 (crate (name "xshe") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0avffrmi8xxs9lxfqhifignkj8882pyrfa04csj03gvqbig8svw1")))

(define-public crate-xshe-0.2 (crate (name "xshe") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "19q6c4855y1bsmjv2541q1kjn0w0iv1ghy2dvadaflhgy4v7x6vf") (yanked #t)))

(define-public crate-xshe-0.2 (crate (name "xshe") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1gckhn30f0xhk8kjyqc8q7mfcqrg4hn6zfw6ay9g6blzj4lyh0j0") (yanked #t)))

(define-public crate-xshe-0.2 (crate (name "xshe") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "103n6wzq4rkwanjbvp4dbd4vf6yjalwsxhxyzbl8w8qjkvs0qi25") (yanked #t)))

(define-public crate-xshe-0.3 (crate (name "xshe") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1z7pqnq3nchdqhhpnn6pi6pkahkh32qafwm681dm1fmqfgn9vb96") (yanked #t)))

(define-public crate-xshe-0.3 (crate (name "xshe") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "031marif9jdngbv18i5dlmavsril12axbgc130jifxdnx6151abm") (yanked #t)))

(define-public crate-xshe-0.3 (crate (name "xshe") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0g4qvkdrvpnfv01i0a52vlnrpx7ln67c3ihmki989i10yyz7jl65")))

(define-public crate-xshe-0.4 (crate (name "xshe") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0if5k99iqbvjn131bcvvn0fj1pcfb39vh4m98d12lmrx5qlz944w")))

(define-public crate-xshe-0.4 (crate (name "xshe") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 1)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^3.1.1") (default-features #t) (kind 1)) (crate-dep (name "clap_mangen") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1lhvax5qmhmqm0cn2nq1nhg82zhd0lp9441r6mp5nkcgj1pn7cpc")))

(define-public crate-xshe-0.4 (crate (name "xshe") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 1)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^3.1.1") (default-features #t) (kind 1)) (crate-dep (name "clap_mangen") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "12fv69lqizphhbq0hbgqgn9zsqrzi943dhvwz1sv86rjin6dbdi8")))

(define-public crate-xshe-0.5 (crate (name "xshe") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.2.5") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.5") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 1)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.1") (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^3.2.1") (default-features #t) (kind 1)) (crate-dep (name "clap_mangen") (req "^0.1.8") (default-features #t) (kind 1)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0w2swwb2mqlmkcbh3z0x4id6g99fwaikq5z8dqz05vss26n2a8dd")))

(define-public crate-xshe-0.5 (crate (name "xshe") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^3.2.7") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.7") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 1)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^1.0.1") (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^3.2.3") (default-features #t) (kind 1)) (crate-dep (name "clap_mangen") (req "^0.1.10") (default-features #t) (kind 1)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "09vdiyfjh5s1ik5c7jc7d5x4ihr8b001awr4xi89nchq806dnf14")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.0") (deps (list (crate-dep (name "xshell-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d6ascdjj8ih0mf8fjnf9fg2rawqcmqxni6v03dkmdxa5xppazqz")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.2") (deps (list (crate-dep (name "xshell-macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1qjl1p5xlv8hs799xpv29jgj3aapfh76apszm6pf6cdn613w6liy")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.3") (deps (list (crate-dep (name "xshell-macros") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0r5smq07d4r32jszvnp5a1x0ffbsgm7mqgrhzy3dk3wy2p961bxf")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.4") (deps (list (crate-dep (name "xshell-macros") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "119ciwjp7h0zdjs6krx94jssx32bifn05dk7qngkjka6d3zd392b")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.5") (deps (list (crate-dep (name "xshell-macros") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1bnsw0ihcyw1n3lb17vxbdi9yhfkxx6r0i2f3abk9ssq278hdpv1")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.6") (deps (list (crate-dep (name "xshell-macros") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0zf3r6jyas77jj1mx6i4kqp406l1n3a70y1v8yqfccr2pg6bz6ry")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.7") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.7") (default-features #t) (kind 0)))) (hash "16073m6bva8xjphhfvfhmv357flaf8sj9ndpj0257bpji38jh1zd")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.8") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.8") (default-features #t) (kind 0)))) (hash "1q4dhw1yfvkyjx34g2zcdzqgn3m83gp4hbzj1a63x86f63g3wdzd")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.9") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.9") (default-features #t) (kind 0)))) (hash "1hf0hjajzdclf38p70iayw73mixlik3lllawx9q8yg25g0i1063g")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.10") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.10") (default-features #t) (kind 0)))) (hash "0wsl85rkg9shx3sf0dkd4qf47f2ywscql8w9g0frk7rgd1y229da")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.11") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.11") (default-features #t) (kind 0)))) (hash "0sfcpi7gpj4z48l77dhrvrmsz3qbks7l2y93dh9nq6qfa5fn9822")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.12") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.12") (default-features #t) (kind 0)))) (hash "1gy6xvj6w3wh8vryfnpch0l3s785rq9rcm044rdkxiv7wzar6gsf")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.13") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.13") (default-features #t) (kind 0)))) (hash "1iilg7cjgz3342f3na500dp3c371jk198qh053kfy57b84dvn5gb")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.14") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.14") (default-features #t) (kind 0)))) (hash "08vwavxq7dnxhk0zql6caqfjyyqsn3j0d1p8frmih3hm3cpkch66")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.15") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.15") (default-features #t) (kind 0)))) (hash "0rqqc083mj6lghmm653fi3f25dfvvvdgkgniaicrf83rpxw6p807")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.16") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.16") (default-features #t) (kind 0)))) (hash "07fpnvpm2dqbc2qmydz13lq6qpg9cdkaanlbc5bp7ydikmm7vi6w")))

(define-public crate-xshell-0.1 (crate (name "xshell") (vers "0.1.17") (deps (list (crate-dep (name "xshell-macros") (req "=0.1.17") (default-features #t) (kind 0)))) (hash "054zbbs1sjm1azhi14avyfjn0g4hbzd7yk9xaw2xlmjc4hsj1bga")))

(define-public crate-xshell-0.2 (crate (name "xshell") (vers "0.2.0-pre.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "xshell-macros") (req "=0.2.0-pre.1") (default-features #t) (kind 0)))) (hash "0xrwsqx30q32iqwrwr6papdxcglz8bhc324j01gzr460ahi47384") (rust-version "1.59")))

(define-public crate-xshell-0.2 (crate (name "xshell") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "xshell-macros") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "1glnrimqq0am4b23dzs3rkgidga4afs4sjdnmhm8m6g21fwwlcik") (rust-version "1.59")))

(define-public crate-xshell-0.2 (crate (name "xshell") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "xshell-macros") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "1al6vimvq6cbq17fa4pj2gkss9lm2ybwgvjgy6zklvc8d5v43128") (rust-version "1.59")))

(define-public crate-xshell-0.2 (crate (name "xshell") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "xshell-macros") (req "=0.2.2") (default-features #t) (kind 0)))) (hash "10d9xi5751p3bjpb8dfxxwxrplfn5m1b6l8qwjqk8ln8qmyhjivd") (rust-version "1.59")))

(define-public crate-xshell-0.2 (crate (name "xshell") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "xshell-macros") (req "=0.2.3") (default-features #t) (kind 0)))) (hash "147yvh1ai2ak0dp6lz9f853pbh45cmy3jj22k97cy5kv7adh6b4n") (rust-version "1.59")))

(define-public crate-xshell-0.2 (crate (name "xshell") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "xshell-macros") (req "=0.2.5") (default-features #t) (kind 0)))) (hash "1b93f33r723051116m3cypx2xn4fslk7dbbi9hxkan750gz0f8ff") (rust-version "1.59")))

(define-public crate-xshell-0.2 (crate (name "xshell") (vers "0.2.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "xshell-macros") (req "=0.2.6") (default-features #t) (kind 0)))) (hash "0dv4igym5whcr8fws0afmhq414a1c38x7a2ln38yyfg7xa3apc3d") (rust-version "1.59")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.0") (hash "009hihxmvdydp2382fkik146447jy1cbqviggq338aqkyg121c2i")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.1") (hash "037806xhgqxpx0yz4cjs8rs1qkcyljcg2207sj9gwl9hzzcy18ka")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.2") (hash "0h02kjm6j78jkds4nhc29dywhmciskbqi4d4s99jb8knrja6wps9")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.3") (hash "0mpfh6h1gns0yn6mgdhpwjpjv8671hl7bji5rw8hkig1b9s441h0")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.4") (hash "0ggyx3xpllshsinkj18k114mmki294q27iala8zs709fz4x63is8")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.5") (hash "1496whp5lm0pcxj417psxykrdlg9a5j8dyg2kv4h0wf6il0zigj0")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.6") (hash "0d314143b54lixq975d5rw9xggjgqyj53i8xn821f2kk5miiqkxr")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.7") (hash "05wn1w52r9v7zgq0r8sizcgi4glydkx7vm004i5lzg7qqcy90zrn")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.8") (hash "08dkim9kvbp8w3dg7mnr4bwk7kkqqmrrm60505xnj14i27wgjskz")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.9") (hash "0rz7s9pybgmbq4b8pzx77sl40pnan05wgb2zi1zh0wj50rhc94v0")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.10") (hash "0wjw712zzsnxgiw20xd83gqpnvbxnfvnwcjq3jilzbqk44yxa124")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.11") (hash "093kk2k85ck6kim93zy4apm3b8833k1ngw8hlmdj0gqnz2zx12d4")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.12") (hash "0c5g040zjh6jk8kq94bf55193wbvbia3bn6js3rzk1kihgb1bljq")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.13") (hash "0lkym5kfq446xymimxr226kppr3ynpi9n8iif32vrfbm5i5djzng")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.14") (hash "0nahq3zlrw500ld556xmbqyxf5gvah4ql0aarqh887bcr6aw01q6")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.15") (hash "1hixsyv0qn7ij298d6289jn4rwiikmdjn18jfgxsy36b9jz5bffj")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.16") (hash "13wdp3vdmbh2i25ahf7h5ia46fzr680n8a3m9bqq5vfbr0g5rw8k")))

(define-public crate-xshell-macros-0.1 (crate (name "xshell-macros") (vers "0.1.17") (hash "0ffgljmfjz79vlkkf5h6g8nidk25jnzj61b2lfcy8nfprais85j9")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.0-pre.1") (hash "1qc3s7aibhyh4m5igzqr6c2f35vcjm6qzvi03rlmvmvv8brcq96d") (rust-version "1.59")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.0") (hash "1701j6fz8mn615cnrf0ad6hr6vql3sxv72ij85v0n03nzb7m8zpl") (rust-version "1.59")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.1") (hash "13xn5ydhpn6d6q9vmg5jgvy22m4rbd3wbsln7wr12dhwf1jj1n9p") (rust-version "1.59")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.2") (hash "06n406clanafsg2zppk78xvggwynha7m6n6q8dfbznbdq9b1nc48") (rust-version "1.59")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.3") (hash "035qx85z45bjldg1bfmkgbw4lzbpqrfb7mly5p8xd88mplfbpfhx") (rust-version "1.59")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.4") (hash "1vqjvv06yp2qvw8cja05iq9mhx18ajimvvvim3pbg5rkgksl6kg4") (yanked #t) (rust-version "1.59")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.5") (hash "03nx2svy28vb60jkrwfwbczfk9l7sarb3hma06azn0dmb4bl2b3y") (rust-version "1.59")))

(define-public crate-xshell-macros-0.2 (crate (name "xshell-macros") (vers "0.2.6") (hash "0lnqicgd9r2mh8p9yz4yidiskip9cp3wqfg4dvqf4xpc7272whlx") (rust-version "1.59")))

(define-public crate-xshell-venv-0.1 (crate (name "xshell-venv") (vers "0.1.0") (deps (list (crate-dep (name "xshell") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hg1qwcl8rxdka7rbgs30ylxmqv3s8985p1kdg2iih1c7myjp9cn")))

(define-public crate-xshell-venv-0.2 (crate (name "xshell-venv") (vers "0.2.0") (deps (list (crate-dep (name "xshell") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p05zj0013zdr9182l7l24is8f9drf9wplqr63k3dilvkcnglzgg")))

(define-public crate-xshell-venv-0.3 (crate (name "xshell-venv") (vers "0.3.0") (deps (list (crate-dep (name "xshell") (req "^0.2") (default-features #t) (kind 0)))) (hash "182gjvkh143p89z3av7s96afqg47bckvyy8lb8dfyq048h90fmwn")))

(define-public crate-xshell-venv-1 (crate (name "xshell-venv") (vers "1.0.0") (deps (list (crate-dep (name "xshell") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v2636hrgcnz13l9ga914nmgh3pxq1s7sz8z6s4s50qgnnnhiaxc")))

(define-public crate-xshell-venv-1 (crate (name "xshell-venv") (vers "1.1.0") (deps (list (crate-dep (name "xshell") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z4im04zkvyqqk7970kmkb3gf697xxrw1wjy5cwhvm5z6s92wg7l")))

(define-public crate-xshell-venv-1 (crate (name "xshell-venv") (vers "1.2.0") (deps (list (crate-dep (name "fd-lock") (req "^3.0.10") (default-features #t) (kind 0)) (crate-dep (name "xshell") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k3xsw1q91kxykfnpk5n805gxwv0y83l335f2a28fdqnnd8mrcv4")))

(define-public crate-xshell-venv-1 (crate (name "xshell-venv") (vers "1.3.0") (deps (list (crate-dep (name "fd-lock") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "xshell") (req "^0.2") (default-features #t) (kind 0)))) (hash "00jbvhijqbfyirw9k957ly5d03wvpi5j2414agg6wwdrsqjzncvr")))

