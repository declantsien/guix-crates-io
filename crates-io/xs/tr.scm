(define-module (crates-io xs tr) #:use-module (crates-io))

(define-public crate-xstr-0.0.1 (crate (name "xstr") (vers "0.0.1") (hash "0jiclwbg656q2hrffrnpfyqh93gavyf22v2cvzrv31k2g90f6z35")))

(define-public crate-xstr-0.0.2 (crate (name "xstr") (vers "0.0.2") (hash "06zkrvlzm3kx024y0jzgk159fvdnj0h49rsaradwxg7cba2y9sn4")))

(define-public crate-xstr-0.0.3 (crate (name "xstr") (vers "0.0.3") (hash "1cyx2ka5f78lzh5hsj715j4i5byrg2mmbjda0p51jlkj28d24m4k")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.0") (hash "1lijnp20y6vl86x51rmk5gwjmj7bay76v2vrar3d6lwrggf1qs26")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.1") (hash "1dlacwpyc008qc90bbf04fk8sffdlr20wcv0xdw3abmc6n0jnfx2")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.2") (hash "1gxc2ysld8j6bp480sw1lmkw3pkps3xhnlr1azdlqdd9r1jjqsl6")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.3") (hash "1p1zlnf01pnqq4s6fhr0k692yr4901gglylfs17d82l058786xpf")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.4") (hash "0aaac26b77vzfkjpkbwbj5c3wy8xxkmz018fzf0b2li7nh838ng3")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.5") (hash "0n456q9y8dv1b3jasb5a75bkcp25gbpgki4cxd323v5l24c3x7yr")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.6") (hash "0w3p60yvh3jpck9qf38m0fwm0skw2ihz8knmxq8pzm851qz3z83f")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.7") (hash "1p58h6wj9z45na8shcf2ym7bv8ki2z0xlf8rjdyzgqdrdw6p3jdg")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.8") (hash "07ld53fcc1px9bx43yaj1pfj1mygkqlm9w764r51cvzsqygynd4a")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.9") (hash "1wi87vd1q4dbl2g6540j2wmdjysz79swsmkmmjc737n23bjk69iy")))

(define-public crate-xstr-0.1 (crate (name "xstr") (vers "0.1.10") (hash "1da00zd56jw5114wwwadwarr46r4w6rgd9335hy9pl98qzqx5cdk")))

(define-public crate-xstream-0.0.0 (crate (name "xstream") (vers "0.0.0") (hash "0391ikgaadzwrynp3mwc7a9ca97m4xwdsbbmal11p9kzs508k70a") (yanked #t)))

(define-public crate-xstream-0.1 (crate (name "xstream") (vers "0.1.0") (deps (list (crate-dep (name "clear_on_drop") (req "^0.2") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "digest") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "miscreant") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^0.1") (default-features #t) (kind 0)))) (hash "06w5prlfcpznaxw24zvi5aylm56f0nvc1i1bwf747kkbca7ly3sj") (yanked #t)))

(define-public crate-xstream-0.1 (crate (name "xstream") (vers "0.1.1") (hash "1yfsp7v33s7xfqja64f83xax4wsba1bcsa3g953q7xggg2ivcxby") (yanked #t)))

(define-public crate-xstream-util-0.1 (crate (name "xstream-util") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)))) (hash "1xlcxw0rlnhhnq7r4qczjjbi2rcx54x1g43hr4dqfpf528w2dar0")))

(define-public crate-xstream-util-0.1 (crate (name "xstream-util") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)))) (hash "0kmwq21q2py3j70qilx2likmyy82z8rwcbrzpcjn5mqs4iwj37ww")))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)))) (hash "1jpzw84mw2g1hr5sp4144h2jfs8crjq5kxw4bkjz1v7lxvjmrlhs")))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "1imvjqhd7lhyhkklx8m9lpw7iyfpab2bk80bzlkdj2nw395jj3sn")))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "1zq3s2aibjgbkbqlz7s0cam8yx9sf9zxvnjrgvph5p69x2k3r0db")))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "^2.34") (default-features #t) (kind 0)))) (hash "1w208ndmc7zv0x5yfngc0gi1dk27jqpkschd1jd03xfz0xgss28b")))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.3.0") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "14na0jbajfg6yxfqvkv4h4zdkrr5dyj69f1dqk2hff0rhasj0mll") (features (quote (("binary" "clap"))))))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.3.1") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "07qxw9vf0khrsmizab7f0vagb5kxab23wigrsq23y1gw1k6hca6z") (features (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.3.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "wrap_help"))) (optional #t) (default-features #t) (kind 0)))) (hash "110wgwnzb3vshlzpks2hd3bxp2a88wi6xwpinjiflxxdkmsvxq54") (features (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.3.3") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "wrap_help"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ld9i4ixxp5v763mqr1wmlzd562hh5k2gmlbayf6vy9ck2pj09sh") (features (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-1 (crate (name "xstream-util") (vers "1.3.4") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "wrap_help"))) (optional #t) (default-features #t) (kind 0)))) (hash "15jcrzgh4glcvbjp1pb6lh980dqrlc5r1yq6axmry15yqvlpbjgq") (features (quote (("default" "binary") ("binary" "clap"))))))

(define-public crate-xstream-util-2 (crate (name "xstream-util") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "wrap_help"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xli6g01rb4rf8v1yxil7j033wiicldizpck1kbdnx389hrd4i33") (features (quote (("default" "binary") ("binary" "clap"))))))

