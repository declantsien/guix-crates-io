(define-module (crates-io xs td) #:use-module (crates-io))

(define-public crate-xstd-0.1 (crate (name "xstd") (vers "0.1.0") (hash "0hcxzllgylxvvi4j7gs29nfa2pwdshkyaqxd19papzzh346mmsx5")))

(define-public crate-xstd-0.2 (crate (name "xstd") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("executor"))) (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha.4") (optional #t) (default-features #t) (kind 0)))) (hash "18nl9x5ibxd9m4y3yxyrrwbgw7gif7yz8jjmifdhnsrv7nm5zfch") (features (quote (("stream" "futures-core" "pin-utils") ("all" "stream"))))))

