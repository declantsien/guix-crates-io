(define-module (crates-io xs et) #:use-module (crates-io))

(define-public crate-xsettings-0.1 (crate (name "xsettings") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.3") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0k9acqngpsnaxsipg7sjmddbyz4mr8ixhd0jxmw9d91jwsh123wp")))

(define-public crate-xsettings-0.2 (crate (name "xsettings") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.2") (default-features #t) (kind 0)))) (hash "16d443kghv3z503gvfi8ama5daa2qp200d7qiqv70xwgnrxpz32d")))

