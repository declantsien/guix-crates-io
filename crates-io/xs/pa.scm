(define-module (crates-io xs pa) #:use-module (crates-io))

(define-public crate-xsparseset-0.1 (crate (name "xsparseset") (vers "0.1.4") (hash "08iq7xj4h06q4h3wl5h1vz2y379277621y6s7kzg7nd6clhbccrs")))

(define-public crate-xsparseset-0.1 (crate (name "xsparseset") (vers "0.1.5") (hash "0vnr4z63vqcivr0qv0p8al0x2nsr0fms4jdsp82s3ap3gil066yb")))

(define-public crate-xsparseset-0.2 (crate (name "xsparseset") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0fmkmazazglk9827h75rv7cklpadzkdc8shnfjbl5p15qav8dbmh")))

(define-public crate-xsparseset-0.2 (crate (name "xsparseset") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1my1y3hw4bj7fs6b0scb7jkqrfzyz9wnyjvwrlm8lg968rn0va4s")))

(define-public crate-xsparseset-0.2 (crate (name "xsparseset") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "120az9prk7chvdn392d4lh13ksvrgwajy5yw3p7adharmxc0w31i")))

(define-public crate-xsparseset-0.2 (crate (name "xsparseset") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "114z6q8sydpn0q6mm5nlykrw8009x66da24lhny3fx8q4h09n3kg")))

(define-public crate-xsparseset-0.2 (crate (name "xsparseset") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "04rwwhkld5n4gad4dwc6b7sffsp13b5m80fd0qa32c444xhrvmlh")))

(define-public crate-xsparseset-0.2 (crate (name "xsparseset") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0dcrb79bbm5hf13gigyasz9bcd05amjqs8kvwwrqf7ycrjdd02dp")))

