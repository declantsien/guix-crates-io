(define-module (crates-io xs av) #:use-module (crates-io))

(define-public crate-xsave-0.1 (crate (name "xsave") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "const-default") (req "^0.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dvyb7ychpaq8397rlzf22sgl795b419kamv7pngl65x2zxks412")))

(define-public crate-xsave-0.1 (crate (name "xsave") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "const-default") (req "^0.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1s8sa5sgl889kf9cw7vkkwbnhyicfwxj4pxhx0rxrgdy6dd7fdlb") (features (quote (("default" "asm") ("asm"))))))

(define-public crate-xsave-1 (crate (name "xsave") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "const-default") (req "^0.2.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1n3aqagia617b6lyxnd87rx8pfvfg3ifm0671avcb66q1qg0p60g") (features (quote (("default" "asm") ("asm"))))))

(define-public crate-xsave-2 (crate (name "xsave") (vers "2.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "const-default") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0c5261f0i6mxip5gjcjvnj11vvmpxzqcmyhz6wk03wci6s697jmj") (features (quote (("default" "asm") ("asm"))))))

(define-public crate-xsave-2 (crate (name "xsave") (vers "2.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "const-default") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1c2hk291hakrpi0zl6v21cbwz2bs15jsi900kaskyjq91xg88sv1") (features (quote (("default" "asm") ("asm"))))))

(define-public crate-xsave-2 (crate (name "xsave") (vers "2.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "const-default") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0kgzp8fixxxh0c2xdfgva53zfk81iapkyf1hwbkgd7y7wma5q77q") (features (quote (("default" "asm") ("asm"))))))

