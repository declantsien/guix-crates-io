(define-module (crates-io xs ha) #:use-module (crates-io))

(define-public crate-xshade-parser-0.1 (crate (name "xshade-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "14dkrxxp6h8n3zy3m6ahmrmq73skc3l6lsbhd29f1w6d5yjpvj7v")))

(define-public crate-xshade-parser-0.1 (crate (name "xshade-parser") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fcjvygrzvgc2ndw3h5m1l785pi6ygyn0950j1van46awav2pzbi")))

(define-public crate-xshade-parser-0.1 (crate (name "xshade-parser") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mpbkrigzjalgsb34mbisk67zlmp4amvc913b3jpdx2051kll17w")))

