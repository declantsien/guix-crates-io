(define-module (crates-io xs l-) #:use-module (crates-io))

(define-public crate-xsl-rs-0.1 (crate (name "xsl-rs") (vers "0.1.0") (deps (list (crate-dep (name "allocator-api2") (req "^0.2.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "allocator-api2") (req "^0.2.16") (default-features #t) (kind 2)) (crate-dep (name "libc-print") (req "^0.1.22") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1n08r3z9l8b3hjphj0r5s7kly15zsgvnbb4rq4pb6kbvmxnq6xfj") (features (quote (("std") ("default" "allocator-api2")))) (v 2) (features2 (quote (("allocator-api2" "dep:allocator-api2"))))))

