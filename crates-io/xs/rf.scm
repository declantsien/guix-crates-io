(define-module (crates-io xs rf) #:use-module (crates-io))

(define-public crate-xsrf-0.1 (crate (name "xsrf") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "subtle") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xaczch72803nrs9fa626ib72pikqa8wdsxmhi2k4jyd9a4pwl7r")))

