(define-module (crates-io xs o_) #:use-module (crates-io))

(define-public crate-xso_proc-0.0.1 (crate (name "xso_proc") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "17b6xinksy6znrkbvwaf55a20vnz1fmdwczdm2qazsm77fxm0vhr")))

