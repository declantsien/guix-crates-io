(define-module (crates-io xs pf) #:use-module (crates-io))

(define-public crate-xspf-0.3 (crate (name "xspf") (vers "0.3.3") (deps (list (crate-dep (name "xml") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "1agbm29vl6aszsw4ia0wx03hx4ib2nrjlfj0d0hysjp7vbxbwbww")))

(define-public crate-xspf-0.4 (crate (name "xspf") (vers "0.4.0") (deps (list (crate-dep (name "xml") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "1yi2vx34rbhvzd8iarvqw0ahssspl2d8q3fnnba28dsinlcnq5hp") (features (quote (("setters") ("print") ("parse") ("default" "print" "parse" "setters"))))))

