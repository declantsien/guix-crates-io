(define-module (crates-io oe is) #:use-module (crates-io))

(define-public crate-oeis-0.1 (crate (name "oeis") (vers "0.1.0") (hash "1m8bvh58jks3hbfbb3whc2mpkmjx5y90kkl37p9212ygpqsdh7sy")))

(define-public crate-oeis-tui-0.0.0 (crate (name "oeis-tui") (vers "0.0.0") (hash "076qgrafrs1y0zwpj5incm3lj2bm87mw6h8k5w7sh1ii8wlyx8qn")))

(define-public crate-oeis-utils-0.1 (crate (name "oeis-utils") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "05wdgjw8dk89i1yfl1wijvmbgk6p2im0lgx70bh30xwp7vsdwfj2")))

(define-public crate-oeis-utils-0.2 (crate (name "oeis-utils") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0brjq0hyg7yrk199dhjzz6zqy37cy4wc8d2yjyr2a52rqr32s2jg")))

(define-public crate-oeis-utils-0.2 (crate (name "oeis-utils") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0k0rk3g3ma2y4b1s8ravvnd7jm5mip13k37fr6ph2xfa9r07kawd")))

(define-public crate-oeis-utils-0.3 (crate (name "oeis-utils") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1jcbh2qvba0hpsjm6aj1shhms2z2rn3dmn4vh43vy4si3rx9kx3g")))

(define-public crate-oeis-utils-0.3 (crate (name "oeis-utils") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0xby6ali757qqskj860k9yracwcsc6gc76k8ryjp73lg8swnhb3g")))

(define-public crate-oeis-utils-0.3 (crate (name "oeis-utils") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0mg30v498854ac2wk6ig4palb3qgil6c7d1w7x2a8hb55vzjb3jn")))

