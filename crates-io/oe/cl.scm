(define-module (crates-io oe cl) #:use-module (crates-io))

(define-public crate-oecli-0.1 (crate (name "oecli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14dd494nvxp827ir1m83awsnkcand0zagyndry0n352s3wjwnhb9")))

(define-public crate-oecli-0.1 (crate (name "oecli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1109i9ns94kribgafksva3jc88gj3cjhndrqxd6jkrh1i6195sss")))

