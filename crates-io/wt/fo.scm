(define-module (crates-io wt fo) #:use-module (crates-io))

(define-public crate-wtforms-0.0.0 (crate (name "wtforms") (vers "0.0.0") (hash "18v0m5mxmx12l632wb4as0ny76ibvbfms5jw2clxjn2xlhnk95fm")))

(define-public crate-wtforms-0.0.1 (crate (name "wtforms") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wtforms-derive") (req "^0.0") (default-features #t) (kind 0)))) (hash "1bap9gr1kbcb9gppjl3kd5almak8xqnahgcj2bjydkng2qi8j735")))

(define-public crate-wtforms-derive-0.0.0 (crate (name "wtforms-derive") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "0v4wi0l59myrp793hgi0av48hmhndkb33m59mvbqr7c8s6xfswwr")))

