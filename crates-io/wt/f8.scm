(define-module (crates-io wt f8) #:use-module (crates-io))

(define-public crate-wtf8-0.0.1 (crate (name "wtf8") (vers "0.0.1") (hash "188fzbsbzvdv2cj29713an1hy54g7cg699i7d0qyq8bk35azk7f7")))

(define-public crate-wtf8-0.0.2 (crate (name "wtf8") (vers "0.0.2") (hash "1al67xragms05sw0d3bipbkln91ayqs1bkk004v04lmjjipvp63s")))

(define-public crate-wtf8-0.0.3 (crate (name "wtf8") (vers "0.0.3") (hash "0mkrjy25payinns0sjm9n3zcjz0jnlf37ddjis54i733hsd31ffn") (features (quote (("std") ("default" "std"))))))

(define-public crate-wtf8-0.1 (crate (name "wtf8") (vers "0.1.0") (hash "01ia1dkq2zcb8mnj5h1r3vgz7g5njh4pv8fkxxv27x9q5i4yh6n0")))

(define-public crate-wtf8-rs-0.0.0 (crate (name "wtf8-rs") (vers "0.0.0") (hash "1wa65arbim9002jnw16djxaajklq9xni65czsv9w6nc95m3y2yjq")))

(define-public crate-wtf8-rs-1 (crate (name "wtf8-rs") (vers "1.0.0") (hash "04wy3463nhqgqrzrcwg7646788qfnwzqx243bp1zp90zsmcz74fm")))

(define-public crate-wtf8-rs-1 (crate (name "wtf8-rs") (vers "1.1.0") (hash "0i4sbh6vn6n7qqqbsyaq2x4axh737r7xvbymjglh6qjr323y69jx")))

