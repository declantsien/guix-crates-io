(define-module (crates-io wt fl) #:use-module (crates-io))

(define-public crate-wtflip-0.1 (crate (name "wtflip") (vers "0.1.0") (hash "0jaswz2k47vj7pjl28kkp5v3r9vyfs0ij4hmyxd17c1mvl5vhxnx")))

(define-public crate-wtflip-0.1 (crate (name "wtflip") (vers "0.1.1") (hash "1gbfzkz4qg2x55jldm6snyg52vjf6flpvwn1bwvj59r46nlaw56x")))

(define-public crate-wtflip-0.1 (crate (name "wtflip") (vers "0.1.2") (hash "0bch3pwms4nwh43xv6nvigkwjiz6i4zlmb20zllyap4dc8dnmh0i")))

(define-public crate-wtflip-0.1 (crate (name "wtflip") (vers "0.1.3") (hash "009ixlxqihbwm5y91y4ln9yw6lx4md004ary56f8n8q7m2gy5nvq")))

(define-public crate-wtflip-0.1 (crate (name "wtflip") (vers "0.1.4") (hash "0hrgb3bgza2fzc23zxldp63pkx909j3g5vqg5lysqpk1z29n7r33")))

(define-public crate-wtflip-0.1 (crate (name "wtflip") (vers "0.1.5") (hash "0a1nic0zfrza67xgq5c5pdgscnbz3c4ab153r5cz8lkv57f25j0v")))

