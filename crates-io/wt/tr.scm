(define-module (crates-io wt tr) #:use-module (crates-io))

(define-public crate-wttr-0.2 (crate (name "wttr") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "05n2mn22zyvrdbckbk57kvb8pvww5v710xg1d4ghrmxm7x4f17yg")))

