(define-module (crates-io wt -b) #:use-module (crates-io))

(define-public crate-wt-battle-report-0.1 (crate (name "wt-battle-report") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.180") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "13vjiyjxd69cwqnw9raxaxndg7kf6py1sgxb4bkxc7736y9w18js")))

(define-public crate-wt-battle-report-0.1 (crate (name "wt-battle-report") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.180") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0rshgavqa1iswfsixzyp8lrdj3jpb33x031rszhmry5hfdb81h33")))

