(define-module (crates-io wt x-) #:use-module (crates-io))

(define-public crate-wtx-macros-0.1 (crate (name "wtx-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (features (quote ("proc-macro"))) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing" "printing" "extra-traits" "proc-macro"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (kind 2)))) (hash "1cxdpa04b0b3b1bj5ymlslj8pp641g6617c2zlm8432cy8y584fg") (features (quote (("default"))))))

