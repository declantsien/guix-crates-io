(define-module (crates-io wt ns) #:use-module (crates-io))

(define-public crate-wtns-file-0.1 (crate (name "wtns-file") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1zd1vryr3jbawcx89146905d67mnjay39z3n0bjmg5znzfqzhms0")))

(define-public crate-wtns-file-0.1 (crate (name "wtns-file") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1jv5qhy1dcx0assdgkjrg6a0vj4hsdhy8mmmbmv0jnjrcrn6gkv8")))

(define-public crate-wtns-file-0.1 (crate (name "wtns-file") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "14r7sb6b1k99ccwz779vpfbm2ichwyx6zdix1qf0zx58f3m0dbwp")))

(define-public crate-wtns-file-0.1 (crate (name "wtns-file") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "193hb56xrfxmhkzwfzavmlb4sqr7zrlrr7mqh4x4jy65396ssnzf")))

(define-public crate-wtns-file-0.1 (crate (name "wtns-file") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0qbxg491y48k1w2fbnwl22dbhfaj81y6g90wbnyll06cq8r8x3ff")))

(define-public crate-wtns-file-0.1 (crate (name "wtns-file") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "02mzv8jksym3hflyhjzsm4y7apdcif8h2fcyhyjni3r9a9j8afrx")))

