(define-module (crates-io wt in) #:use-module (crates-io))

(define-public crate-wtinylfu-0.1 (crate (name "wtinylfu") (vers "0.1.0") (deps (list (crate-dep (name "bloomfilter") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "count-min-sketch") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "0pz44b8pd6k003iqrl442h83389v8ifmrl5g8hpl93g6lfvd17m2")))

