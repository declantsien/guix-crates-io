(define-module (crates-io wt fi) #:use-module (crates-io))

(define-public crate-wtfis-0.1 (crate (name "wtfis") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "16cfajrxb1a3i3ll4h2rzsc4wbba0ixh58b67q0khgc6677qrz2k")))

