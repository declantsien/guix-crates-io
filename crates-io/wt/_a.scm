(define-module (crates-io wt _a) #:use-module (crates-io))

(define-public crate-wt_afterburner-0.1 (crate (name "wt_afterburner") (vers "0.1.0") (hash "0sl7q77d5668d26fb9gniyh2kvpcncsgbljglayaagmycfrszsj5")))

(define-public crate-wt_afterburner-1 (crate (name "wt_afterburner") (vers "1.0.0") (hash "1q74gp8r8w296kza9fpj8phgxg9lsya90vdc6zryhmrmjm9d0waz")))

(define-public crate-wt_afterburner-1 (crate (name "wt_afterburner") (vers "1.1.0") (hash "0ylz4kvz9axrhymawp8igzg9vgqlbzshz5frsf60aq2xrssl1a10")))

(define-public crate-wt_afterburner-1 (crate (name "wt_afterburner") (vers "1.2.0") (hash "05hrf0sq53qvigv9mq6vhlxinl7hz3s6j9lgi3g7d998iwij4jak")))

(define-public crate-wt_afterburner-1 (crate (name "wt_afterburner") (vers "1.3.0") (hash "1m61mky3v736nv3a98lg8q34vhzfwygq5cr7b6h3ac1m9wamz40n")))

