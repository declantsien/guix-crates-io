(define-module (crates-io wt ho) #:use-module (crates-io))

(define-public crate-wthor-0.1 (crate (name "wthor") (vers "0.1.0") (deps (list (crate-dep (name "magpie") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09pnicp9pzvk96p9rq7xap00jibk961584gpaxhdxf3x1x5an8hz")))

(define-public crate-wthor-0.2 (crate (name "wthor") (vers "0.2.0") (deps (list (crate-dep (name "magpie") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17a7ywkc394c5hhsdvagwlrp2p74v34j4qnh7v5dpkn9pfxxxmad")))

(define-public crate-wthor-0.3 (crate (name "wthor") (vers "0.3.0") (deps (list (crate-dep (name "magpie") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0c7q1qi3yss0jr52wg74lrb684scinndxnnpirk41rfqldg8mh8p")))

(define-public crate-wthor-0.4 (crate (name "wthor") (vers "0.4.0") (deps (list (crate-dep (name "magpie") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08fvvmpiyzvckq3hsw0m9gvmr1mxwhwb8pcvhy6ma12ylz27y4nc")))

(define-public crate-wthor-0.4 (crate (name "wthor") (vers "0.4.1") (deps (list (crate-dep (name "magpie") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0kzx1prsq08z8xzx1733d33v667j3rghf0bh1n498fcnjckfwhxd")))

(define-public crate-wthor-0.4 (crate (name "wthor") (vers "0.4.2") (deps (list (crate-dep (name "magpie") (req "^0.9") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "19w32vjq23ssbl3bbkvql99i0v25fq3qq28c4fzlyqdc6qjpqmgm")))

(define-public crate-wthor-0.5 (crate (name "wthor") (vers "0.5.0") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "magpie") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0hqy8lwzd46az6cg66xxf0cnc1v27nvchx5jndn2vdqhb2hjq2ka")))

