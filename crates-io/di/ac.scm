(define-module (crates-io di ac) #:use-module (crates-io))

(define-public crate-diacritics-0.1 (crate (name "diacritics") (vers "0.1.0") (hash "15dl07sqih5b7497k2kabv4rvwv01vyz8rcjsl1l8iay9ydpz1gb")))

(define-public crate-diacritics-0.1 (crate (name "diacritics") (vers "0.1.1") (hash "0x0bh4msmxjbnxgchpqz45h0f9v719kyyj4x8cdcls6djchrpyf4")))

(define-public crate-diacritics-0.1 (crate (name "diacritics") (vers "0.1.2") (hash "007jiy163jhqanmxdypr8bdb6wxh8ms55a7bv52x5ndj7bfkgnns")))

(define-public crate-diacritics-0.1 (crate (name "diacritics") (vers "0.1.3") (hash "0ld1l8fn6xk98i8lh7slrpfcxix94xxy323d5f6majh09xvhf23j")))

(define-public crate-diacritics-0.2 (crate (name "diacritics") (vers "0.2.0") (hash "1qn7i56fymxydy07m7xkj300x5r377p1vrfm1kql9kiibd749ipy")))

(define-public crate-diacritics-0.2 (crate (name "diacritics") (vers "0.2.1") (hash "1qxq6py4ppszw9p4kg7s1pp5f8apfsvxd6rlfbpbaxzvmh204diy")))

(define-public crate-diacritics-0.2 (crate (name "diacritics") (vers "0.2.2") (hash "19nxjscpcnz1bbnbzhgxqciq2ds635zfxz1n134rfza33rlq56p3")))

