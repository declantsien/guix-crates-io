(define-module (crates-io di zp) #:use-module (crates-io))

(define-public crate-dizpacho-0.1 (crate (name "dizpacho") (vers "0.1.0") (deps (list (crate-dep (name "macrotest") (req "^1.0.8") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.96") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0dr57c17h6mrfy8y3k239mf199cr642p1cr6mid3vxhf9kh78qp5")))

