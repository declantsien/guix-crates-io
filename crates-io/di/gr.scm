(define-module (crates-io di gr) #:use-module (crates-io))

(define-public crate-digraph-rs-0.1 (crate (name "digraph-rs") (vers "0.1.0") (deps (list (crate-dep (name "graphviz-rust") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1cdrfmzvj8nqj5ih53q85b7ld0kjkxjfdbcwzvv1hi7wsj0xhp7c")))

(define-public crate-digraphx-rs-0.1 (crate (name "digraphx-rs") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.109") (default-features #t) (kind 0)))) (hash "0hppf4ad5ls82x8426bh5080lf4j46js3pjkpz2qrwk3sjrd455d")))

