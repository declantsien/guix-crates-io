(define-module (crates-io di ll) #:use-module (crates-io))

(define-public crate-dill-0.1 (crate (name "dill") (vers "0.1.0") (deps (list (crate-dep (name "dill-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "~0.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1pz0vkimnk9blgzxcgc5s1lvpxg5g7pq8s0iw9apadf6zdljvvg9")))

(define-public crate-dill-0.2 (crate (name "dill") (vers "0.2.0") (deps (list (crate-dep (name "dill-impl") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "~0.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ha1l0wx4qqdbjcmipsbgmnnyiippnsdw7jdvxdbcbh50gx4m82x")))

(define-public crate-dill-0.3 (crate (name "dill") (vers "0.3.0") (deps (list (crate-dep (name "dill-impl") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "~0.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1x24gfqdmd37zqfbnylzb4h60nnkiv5h8d1icpda4ldndav3b5yd")))

(define-public crate-dill-0.4 (crate (name "dill") (vers "0.4.0") (deps (list (crate-dep (name "dill-impl") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "02riw159ky0nfl9ni7fm8n8zq8aqaz75l0wqfvk6yw6265b2xwfc")))

(define-public crate-dill-0.5 (crate (name "dill") (vers "0.5.0") (deps (list (crate-dep (name "dill-impl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "01qgrr7vkbqasjzkmdbclqlcgfy6kqqwylrjlyk3q6wgdyqd3kai")))

(define-public crate-dill-0.5 (crate (name "dill") (vers "0.5.1") (deps (list (crate-dep (name "dill-impl") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07mg33nigavx8lfp8r8hrc080pxii18xrvpxg11l8pb9nlxgf43g")))

(define-public crate-dill-0.5 (crate (name "dill") (vers "0.5.2") (deps (list (crate-dep (name "dill-impl") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ibipfr58kgq0nhqqdzac6xhxxps3b13zi9al53rinzmbf61gxmy")))

(define-public crate-dill-0.5 (crate (name "dill") (vers "0.5.3") (deps (list (crate-dep (name "dill-impl") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1lwminzhp06g0w38qy6rcmadp7hn0q5nkghq5q8pkcl2hc1s217d")))

(define-public crate-dill-0.6 (crate (name "dill") (vers "0.6.0") (deps (list (crate-dep (name "dill-impl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "14b24j11dkq7h9j796x3sw765qv1yhis2rqaz6qy17ya0vc3nh12")))

(define-public crate-dill-0.6 (crate (name "dill") (vers "0.6.1") (deps (list (crate-dep (name "dill-impl") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0cv5jk4z3pci0r7vlcmnb9k31qm6ysjqj1sbihkbc0cqi46cdv0k")))

(define-public crate-dill-0.7 (crate (name "dill") (vers "0.7.0") (deps (list (crate-dep (name "dill-impl") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1f4j8463lhkb4f1g36sr7by1xpcfjh1dsm2ciwdbnd80mikgxcz9")))

(define-public crate-dill-0.7 (crate (name "dill") (vers "0.7.1") (deps (list (crate-dep (name "dill-impl") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1b30bhrjgilvfdi86yh2w7zqbvk1hh83a11l2rgjmdlkf8hszjy6")))

(define-public crate-dill-0.7 (crate (name "dill") (vers "0.7.2") (deps (list (crate-dep (name "dill-impl") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0js418bpzgbgsnxrvck2hgaqc51sncknxs863rxnkm4d1ggxiv7k")))

(define-public crate-dill-0.8 (crate (name "dill") (vers "0.8.0") (deps (list (crate-dep (name "dill-impl") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1936fnamz6xxmf9r1c4xcgnnr0ldz2wza9ifbffkiw7hsvsmqin8")))

(define-public crate-dill-impl-0.1 (crate (name "dill-impl") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "~0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y1zmf0673q0x78r8pdzzfizc7sdfv5w8yh23wlyzsd9kx74hp1b")))

(define-public crate-dill-impl-0.2 (crate (name "dill-impl") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "~0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0slkad7fwqx0b7mkjbgylwsqkg9i31ilap1afnkwhwlsikrl797a")))

(define-public crate-dill-impl-0.3 (crate (name "dill-impl") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "~0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "018xdg5gp4g74scrqs511knw0k5nnlvnk0k8yj64xnc5hhjhg9w5")))

(define-public crate-dill-impl-0.4 (crate (name "dill-impl") (vers "0.4.0") (deps (list (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vbmm6wf0x4q7zb7y8iffg7dzqxv7sy35f2j9rwl4ld5y2arh7jv")))

(define-public crate-dill-impl-0.5 (crate (name "dill-impl") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "027vs3zfgx1r53wp9nf02bda06c4bb3yzyx8q7zr29821lrg00lx")))

(define-public crate-dill-impl-0.5 (crate (name "dill-impl") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kvpnriick8lnxn8x6msq0g4baqnzg01pndc82x4gi0jxz786c6l")))

(define-public crate-dill-impl-0.5 (crate (name "dill-impl") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z0xl8jrp23kjrrby5138pm3002z5nipzbqls7qkr2jpw04744w5")))

(define-public crate-dill-impl-0.5 (crate (name "dill-impl") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dglihf0bp5nfcj54wdbzi717hns6xxvxbh7k9pi3z5dc5x8vdys")))

(define-public crate-dill-impl-0.6 (crate (name "dill-impl") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fpk4ixa0nfbf4zp85l0qnsqfbqqs96g0xgz8jagm8g0dks8yrxp")))

(define-public crate-dill-impl-0.6 (crate (name "dill-impl") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00fcp1ivq9083kl5cpv5zcf003r2q725ls7ak5rshwly556cgr25")))

(define-public crate-dill-impl-0.7 (crate (name "dill-impl") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dwqj5f2k2jpn8nqmg7mp5barnp99h6yr6vzy5zbjjg0586kba8a")))

(define-public crate-dill-impl-0.7 (crate (name "dill-impl") (vers "0.7.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s6cz79c72cjcb0h47kdak8aaam314575zawakfskx2wfrxs7kgz")))

(define-public crate-dill-impl-0.7 (crate (name "dill-impl") (vers "0.7.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zhdnrh79aybl3mdb9jf6xsv52paaaq555hhmxqah3zm4kz76lc0")))

(define-public crate-dill-impl-0.8 (crate (name "dill-impl") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i3ll66p14si8vnph40hv58z90jgh4gama5y6inad15nv4zjc3sd")))

(define-public crate-dilla-0.1 (crate (name "dilla") (vers "0.1.0") (hash "1jmngnscyrbfgvwxdmy9k62h8abc9gv9r4v60df1fpxdzir0ckjn")))

