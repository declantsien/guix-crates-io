(define-module (crates-io di c-) #:use-module (crates-io))

(define-public crate-dic-server-0.1 (crate (name "dic-server") (vers "0.1.0") (deps (list (crate-dep (name "dic") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p27mk2bhyzppyrdpq2pwdxq6y87967wymzzl7wxnabgr03bfqdk") (yanked #t)))

(define-public crate-dic-server-0.1 (crate (name "dic-server") (vers "0.1.1") (deps (list (crate-dep (name "dic") (req "^0.1") (default-features #t) (kind 0)))) (hash "11g1cgj7c7nyzdy28jv58h21b7wxm3cfhryabp0lllbkyd3d9gdr") (yanked #t)))

