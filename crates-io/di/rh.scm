(define-module (crates-io di rh) #:use-module (crates-io))

(define-public crate-dirhash-0.2 (crate (name "dirhash") (vers "0.2.0") (deps (list (crate-dep (name "seahash") (req "^3.0.6") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.7") (default-features #t) (kind 0)))) (hash "1m7nhzhj6rjqv6l3y62nrx3hxkv1vwr0zz7lbd0008sn2qkpns98")))

