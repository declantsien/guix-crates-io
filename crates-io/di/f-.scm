(define-module (crates-io di f-) #:use-module (crates-io))

(define-public crate-dif-presentation-exchange-0.1 (crate (name "dif-presentation-exchange") (vers "0.1.0") (hash "1yxxrix2xg1127xgslskzagrxpfr7q33sh9aiq7z1kiijdaz3rml")))

(define-public crate-dif-presentation-exchange-0.2 (crate (name "dif-presentation-exchange") (vers "0.2.0") (deps (list (crate-dep (name "getset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jsonpath_lib") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "jsonschema") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1yfk4g3931v3ksmxaks9kkjjkbl8lkgfn2vnkz7cvigp4lp9pgzd")))

