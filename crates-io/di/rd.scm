(define-module (crates-io di rd) #:use-module (crates-io))

(define-public crate-dirdiff-0.1 (crate (name "dirdiff") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "atty") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)))) (hash "0p8kxszwgk5nzj7r6fpqllgz5hb82m38fi0bav8389gvi70gbxrc")))

(define-public crate-dirdiff-0.1 (crate (name "dirdiff") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "atty") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kz2xwkq56i5i8cdbbg94id7zmn3cjqnarkkd7lfcnq223c7syi5")))

(define-public crate-dirdiff-0.1 (crate (name "dirdiff") (vers "0.1.0") (hash "15ig2zxk6g00xq7z2fwh01yziq1db6q7jifvj4w2a531ny5i7v5m")))

(define-public crate-dirdiff-ocamlpro-0.2 (crate (name "dirdiff-ocamlpro") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-deque") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.12") (default-features #t) (kind 0)))) (hash "026r52wwv2ikgvax9vmr38z5r070y8nkpdpd9a627s0b2v2awcnq")))

