(define-module (crates-io di ys) #:use-module (crates-io))

(define-public crate-diysh-1 (crate (name "diysh") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "12b22sdlxf05lw1i9qvq2p075037hbnadbq1fj2ad01jjpz2fms2")))

(define-public crate-diysh-1 (crate (name "diysh") (vers "1.1.1") (deps (list (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "0zafx3h8p84jzwk8hd28w86nmiq782wr1j55scy24rm2rdrnq9k0")))

(define-public crate-diysh-2 (crate (name "diysh") (vers "2.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "1faian465zfiazzs8sg6yk4blq8vpam283phpgjjsd2p1m2k26ii")))

(define-public crate-diysh-2 (crate (name "diysh") (vers "2.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "0fjfqy1d22kcgv6g7klwqqc7sz6q0vdk6f3bd5b5lmx71kqybqg1")))

(define-public crate-diysh-2 (crate (name "diysh") (vers "2.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "0haynx069an55j5a3478ynvfgn5y4lwzm6hvy6frvjn7k0dmhpjf")))

(define-public crate-diysh-2 (crate (name "diysh") (vers "2.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "198gm82iavxgykpjrmgcyjawz583pchcl4lq520s7rip5bcb2q2w")))

