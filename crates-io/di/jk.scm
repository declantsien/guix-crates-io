(define-module (crates-io di jk) #:use-module (crates-io))

(define-public crate-dijkstra-0.1 (crate (name "dijkstra") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0bhbr7gpwy9x21wpvmm3ghcvdz63rs2swadzvsya8gjsl5911ygy")))

(define-public crate-dijkstra-0.1 (crate (name "dijkstra") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1mi2bbn1vwvagy4cmrchvbzm27fqbrk5j9yzqarj1154qlqmvfv2")))

