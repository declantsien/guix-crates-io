(define-module (crates-io di pa) #:use-module (crates-io))

(define-public crate-dipa-0.1 (crate (name "dipa") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "dipa-derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ng9vikf6720a2mrjcrrj8n2n9wzagyckxicdi1kq72nk89sglr4") (features (quote (("impl-tester" "bincode") ("derive" "dipa-derive"))))))

(define-public crate-dipa-0.1 (crate (name "dipa") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "dipa-derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0257r00aibs9bj43215g4r4gpfh8pbfqva4qxkzzm13vjb4d0zq3") (features (quote (("impl-tester" "bincode") ("derive" "dipa-derive"))))))

(define-public crate-dipa-derive-0.1 (crate (name "dipa-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1j6d85pmw5hj50z6bcfn0zv3g565kr1dq5clnvldlwb8sv1dacyx")))

