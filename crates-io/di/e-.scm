(define-module (crates-io di e-) #:use-module (crates-io))

(define-public crate-die-exit-0.3 (crate (name "die-exit") (vers "0.3.0") (hash "090f2jvzw40k16b3pkijqqiwdr1bsp7fjj0x079hj8llclymlvjb") (features (quote (("test")))) (yanked #t)))

(define-public crate-die-exit-0.3 (crate (name "die-exit") (vers "0.3.1") (hash "1rv4xawrr84rf42lm97vj7c2cnaqnbnf8r7b183clzam7g0a9i3a") (features (quote (("test")))) (yanked #t)))

(define-public crate-die-exit-0.3 (crate (name "die-exit") (vers "0.3.2") (hash "041m812cw8c1yvqgvxgmyxgl64k2vrgm78a2gjz095wvdqrni5g8") (features (quote (("test")))) (yanked #t)))

(define-public crate-die-exit-0.3 (crate (name "die-exit") (vers "0.3.3") (hash "12spip9d318rmf3v9w5p6jy1d81lqiy45bificc2zvi1wyy358vk") (features (quote (("test"))))))

(define-public crate-die-exit-0.4 (crate (name "die-exit") (vers "0.4.0") (hash "0jcz97kdzsshg451v30zdwb8kl157g8yn2n9y4z7nrgwa50xxirq") (features (quote (("test"))))))

(define-public crate-die-exit-0.5 (crate (name "die-exit") (vers "0.5.0") (hash "19ysgx4ca57qmy9099fnhr9q67gpqywbrpdy3ib8g6v7jidqki7f") (features (quote (("test") ("red"))))))

(define-public crate-die-exit-2-0.4 (crate (name "die-exit-2") (vers "0.4.0") (hash "0z4b8bn0224ww5zf8px2041g85f8nfg9zz1fbsbg8kkr9kbc76iy") (features (quote (("test"))))))

(define-public crate-die-exit-2-0.4 (crate (name "die-exit-2") (vers "0.4.1") (hash "1crbiy5rn3l3nvpymavjpshdv40y1raka7i94nd61crw6x8f5xr4") (features (quote (("test"))))))

