(define-module (crates-io di vb) #:use-module (crates-io))

(define-public crate-divbuf-0.1 (crate (name "divbuf") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1gr00dvgm4947h4ja1spxsc4ky0d8p7dilpksyp6bhkil1c2lxny")))

(define-public crate-divbuf-0.2 (crate (name "divbuf") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0fbvz1s3hv95ngnix7x20dn1rlzijg8hjpadydc5kfq4zxk2bi7q")))

(define-public crate-divbuf-0.3 (crate (name "divbuf") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "1gf9f6kp85r45s6nbby6cisslpxc5j9fddqsbc99jw9lpijzsbs8")))

(define-public crate-divbuf-0.3 (crate (name "divbuf") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "092nj17y839y34ikinlk65p7hv61iqhw9038i4rnm2p7vfg760pv")))

