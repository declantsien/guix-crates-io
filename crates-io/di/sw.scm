(define-module (crates-io di sw) #:use-module (crates-io))

(define-public crate-diswh-0.1 (crate (name "diswh") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ygiishif1dgx7cwx5igas0b3kn9lpv1ykvvyy84c5kaxmy4jxf5") (yanked #t)))

(define-public crate-diswh-0.1 (crate (name "diswh") (vers "0.1.1") (deps (list (crate-dep (name "curl") (req "^0.4.44") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "1wp5hp68bnqqbxd3lf30hqrm8s665mr4kgbwl9cii53za5w77s00")))

(define-public crate-diswh-0.2 (crate (name "diswh") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "0gskbxq2ifjk4zvxa7pswzfh10l45xfg4nz1495xq13482v7rx8z") (features (quote (("async"))))))

