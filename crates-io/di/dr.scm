(define-module (crates-io di dr) #:use-module (crates-io))

(define-public crate-didrte-0.1 (crate (name "didrte") (vers "0.1.2") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1p82r102inh5428ny0n62qy8hwy5lnqqnmc2zab0b2b7aii3gh9w")))

(define-public crate-didrte-rs-0.1 (crate (name "didrte-rs") (vers "0.1.0") (hash "0fy6q324g57m0cncgnws20b12mwrvr9pf1p2kcddysf1glx916c9") (yanked #t)))

(define-public crate-didrte-rs-0.1 (crate (name "didrte-rs") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1sslamwcz54r2jrxzqknn9xvi3lyvk4qdfc24fnlqyp1vmw23p34") (yanked #t)))

