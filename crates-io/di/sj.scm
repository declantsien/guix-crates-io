(define-module (crates-io di sj) #:use-module (crates-io))

(define-public crate-disjoint-0.1 (crate (name "disjoint") (vers "0.1.0") (hash "144k5gib2lzy59mvqiw3k6j1vd5wyyk634w3aqlgcna4g35jbksk") (rust-version "1.56")))

(define-public crate-disjoint-0.2 (crate (name "disjoint") (vers "0.2.0") (hash "0m951g8zdfwklf2sjdjmpvyrj1qa7rsihvhng18bncvnp268g630") (rust-version "1.56")))

(define-public crate-disjoint-0.3 (crate (name "disjoint") (vers "0.3.0") (hash "0l1vbi1wa95dl16hli5dhlh0w6d83ss51cxyd9snygk528x9z70j") (rust-version "1.56")))

(define-public crate-disjoint-0.4 (crate (name "disjoint") (vers "0.4.0") (hash "12hjcaclbs3rg4xdnh4hz9n0590110zzjkygx5qrnwvl1f7c4naw")))

(define-public crate-disjoint-0.5 (crate (name "disjoint") (vers "0.5.0") (hash "147jqkwq8hqssp0f8fxsjdbchca0s6z7zf45ccwh76pnxq02fyvh")))

(define-public crate-disjoint-0.6 (crate (name "disjoint") (vers "0.6.0") (hash "06ihbnlv4q5n7za2i1zdma5rpb0a64x20jxhq8s7bbhn2im9fdqd")))

(define-public crate-disjoint-0.7 (crate (name "disjoint") (vers "0.7.0") (hash "168w9bz96h2nmvk759hl30s0zxa6fpqg2xbr25pph9b5324zfvqp")))

(define-public crate-disjoint-borrow-0.1 (crate (name "disjoint-borrow") (vers "0.1.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.22") (features (quote ("stable"))) (default-features #t) (kind 2)))) (hash "0lnq51hr4s31dxxki5bdl1mh2mv1livdr4ji87xnqdkln62p2z9d")))

(define-public crate-disjoint-borrow-0.1 (crate (name "disjoint-borrow") (vers "0.1.1") (deps (list (crate-dep (name "trybuild") (req "^1.0.11") (default-features #t) (kind 2)))) (hash "0iqdilc8v8azvb8hpbkp4a3c3cdnhhyzwjiy7ighfxgy65bkgq49")))

(define-public crate-disjoint-hash-set-1 (crate (name "disjoint-hash-set") (vers "1.0.0") (hash "04xwsma1j1r9rvly1hl6jvr3487l6bk01q1rrbc1xhc3frbjkwr5")))

(define-public crate-disjoint-set-0.0.1 (crate (name "disjoint-set") (vers "0.0.1") (hash "02g1nfdj809rxzr0l59ndq16n7bx9chz2nlrf5m7hdjq9x2sfrhy")))

(define-public crate-disjoint-set-0.0.2 (crate (name "disjoint-set") (vers "0.0.2") (hash "11bmpd96rc166wrk08i6kdsd58klq036rm6ni37dvkgxcajg20ni")))

(define-public crate-disjoint-sets-0.1 (crate (name "disjoint-sets") (vers "0.1.0") (hash "1m27as3ljy6hdpilmm2i3q1gqdxdvqskpmr6kcniraw7z3fbf8mw")))

(define-public crate-disjoint-sets-0.1 (crate (name "disjoint-sets") (vers "0.1.1") (hash "1g8pc4ignjq8dnhf2aqc9dda5pqw6ydxq2h105laq82scb508fx5")))

(define-public crate-disjoint-sets-0.2 (crate (name "disjoint-sets") (vers "0.2.0") (hash "00cg0kzzqsl2lp9fzam3aa037iazfcdpgr02mchcnh340gmcsvkk")))

(define-public crate-disjoint-sets-0.2 (crate (name "disjoint-sets") (vers "0.2.1") (hash "03ldy3pjfky44mk2q6xl0g0s8k14q3q72i1xsccdgif422vlk2dh")))

(define-public crate-disjoint-sets-0.2 (crate (name "disjoint-sets") (vers "0.2.2") (hash "0v4dm40zps3zvk87gfaryj7gdsab0xx4gswc35vhbyylhl5f9k2m")))

(define-public crate-disjoint-sets-0.2 (crate (name "disjoint-sets") (vers "0.2.3") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1ac5lhklxhck93ng58yw634p8qjinn1ib33cxhcdqbb9pgc6pcv5")))

(define-public crate-disjoint-sets-0.3 (crate (name "disjoint-sets") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "01p0mv8yic2djvgs3gawpq7n914lkzsnp6ma327fwk5kl501dyjg")))

(define-public crate-disjoint-sets-0.3 (crate (name "disjoint-sets") (vers "0.3.1") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0fnja7qvzp3n75v7ag3q74dmgy1bphx048symgx8lr86ssyzgcym")))

(define-public crate-disjoint-sets-0.3 (crate (name "disjoint-sets") (vers "0.3.2") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1r6mwq6zk0rrxs8jd6ygcb9a3hh62k2xl63mqsk2p61rm6fqfxsv")))

(define-public crate-disjoint-sets-0.3 (crate (name "disjoint-sets") (vers "0.3.3") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1s8691qpgih9cb22x8wg7svi6miqqhk7c2ynzn44xlcbk9fy05h4")))

(define-public crate-disjoint-sets-0.3 (crate (name "disjoint-sets") (vers "0.3.4") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0r81zbsdl6mbzszcnd6cpnfdm10cr7vcv5akvz1x73yxqw85hndc")))

(define-public crate-disjoint-sets-0.3 (crate (name "disjoint-sets") (vers "0.3.5") (hash "0y95scrr1002zmqkkc5zdikajjj2in6syqjd3z6d8hhaq96qkha0")))

(define-public crate-disjoint-sets-0.4 (crate (name "disjoint-sets") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "02nclwzliyyp6n33qzj0w1x1bm87m218pgzz5rgqxvk2097v359a")))

(define-public crate-disjoint-sets-0.4 (crate (name "disjoint-sets") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1q09aa3ljhl2jqv59skf6zqkchvp57r5jy4js8dwszkb0mnfh5nm")))

(define-public crate-disjoint-sets-0.4 (crate (name "disjoint-sets") (vers "0.4.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0d1x5k2qhszrf6323w3f2ynzr9dl9dnn2p2kl9y00ix2sskvrjsc")))

(define-public crate-disjoint_impls-0.1 (crate (name "disjoint_impls") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1yy7inx0db5gb9h970kzfm3jjc0y72q9qkqli6788x0qnz9xmxax")))

(define-public crate-disjoint_impls-0.2 (crate (name "disjoint_impls") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1wds7vi4dhvqgjj9c27d2xjxyj36hp9ljnp5n63hfkp8vn11lax4")))

(define-public crate-disjoint_impls-0.3 (crate (name "disjoint_impls") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1hb17jpwf84yabk6fdbp15d9y6b11c2sd9mbhmw9bx3p1dfs57gl")))

(define-public crate-disjoint_impls-0.4 (crate (name "disjoint_impls") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0v88rbkf09krks34fi2gfvcrg4b2abwygakh8q25rcnkywcma70i")))

(define-public crate-disjoint_impls-0.5 (crate (name "disjoint_impls") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0iil8dl9nbkdf1j1dsl2p22k55zsyk4z1s6hxirn7228v5rqrvc6")))

(define-public crate-disjoint_impls-0.6 (crate (name "disjoint_impls") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0pjsp96q86bap1l2nk54iqpapdv3y25xz3b1j22q49zgcxisn1il")))

(define-public crate-disjoint_impls-0.7 (crate (name "disjoint_impls") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1ag6jlisdlm14042f5mhsbr1li7bh7qpc4n0b08cmji7df89fqim")))

(define-public crate-disjoint_impls-0.7 (crate (name "disjoint_impls") (vers "0.7.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "055m1fyj4yfrr8fmzqhlc8qbswq885sx92n8y76b360f8iba1vh0")))

(define-public crate-disjoint_set_forest-0.1 (crate (name "disjoint_set_forest") (vers "0.1.0") (hash "0xxk3x0i2bfxxj0j5nzjg1i58mpfgfjq6y1idpnhywxb9451csz2")))

