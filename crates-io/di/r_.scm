(define-module (crates-io di r_) #:use-module (crates-io))

(define-public crate-dir_cleaner-0.1 (crate (name "dir_cleaner") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0j60mwiwfrmm4xcljf90d08z60gbpxi8ii9qal78jka3a9vqi88w")))

(define-public crate-dir_contents-0.1 (crate (name "dir_contents") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0qzycs8cci6936rswmvl378fs6viisaxgvdwf0jv70z2qikn5rvh")))

(define-public crate-dir_contents-0.1 (crate (name "dir_contents") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0df7w8xpm874g420aw0m4g1wf8zhcd396bywn1vfa8qflmfci09g")))

(define-public crate-dir_indexer-0.0.1 (crate (name "dir_indexer") (vers "0.0.1") (hash "12lff431qklrdbpzmnq7r7dh84236yjngn0bpvvb5a6cllrw2yh1") (rust-version "1.56.0")))

(define-public crate-dir_indexer-0.0.2 (crate (name "dir_indexer") (vers "0.0.2") (hash "02may0xxhdb2g0nfsw02n2w87nrkakwa4jind5x3in5qgj142din") (rust-version "1.56.0")))

(define-public crate-dir_lines_streamer-0.1 (crate (name "dir_lines_streamer") (vers "0.1.0") (deps (list (crate-dep (name "alphanumeric-sort") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "15l12rfwlzw2pkxydvjs4dzr9yjga7plnc487ahn0231m6ng57vb")))

(define-public crate-dir_lines_streamer-0.2 (crate (name "dir_lines_streamer") (vers "0.2.0") (deps (list (crate-dep (name "alphanumeric-sort") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (features (quote ("rust_backend"))) (optional #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "06qf3k3jplm1vn8qpgns6qrjmdw4kc023682sg82yqjqjzgf9w07") (features (quote (("gz" "flate2") ("default" "gz"))))))

(define-public crate-dir_update-0.1 (crate (name "dir_update") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.2") (features (quote ("suggestions" "wrap_help"))) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0a5hjcf1ndp08lyvkfz9bngjhc51zwy0r0s272grwr2ls1yckp64")))

(define-public crate-dir_update-0.2 (crate (name "dir_update") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.2") (features (quote ("suggestions" "wrap_help"))) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "17fwp22sbzwg0jgxzr64w7lbj7msvy3z79vay5p0g362wd8ibcan")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.1") (hash "1v200y1429j7l8dksd0fmpl5bcr7n21rhd03gsg93mdyw9jncbm4")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.2") (hash "1f71v2bdc1zmncwmmxaw64g97vdxdrzw4w5v448m1wp70yfdsw2x")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.3") (hash "13m6f9a5sbljz3sdrwr3qx4sfm2f386vp3p15zfz88rzkxxqnar9")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.4") (hash "0p9k3l3ddxh0xjb2bh7q4ffrrbazs6iqy2ikan5l7h51qki87gac")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.5") (hash "111hgj4ldvpz89z132qyv5xib541zql3zp753padsnq4hfabkadh")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.6") (hash "0mvn9wqj1y7pbr8zrijski56a7n2rk9y50h4r9ndc59wjm0m8d0k")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.7") (hash "1bnjqnc9pizgkm7bna9q709d8y3gigr6aavcaz4q9px0djbx66vb")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.8") (hash "1c4g4h9qfsp3g8jq2bhdyl3ijql6b7m896h478lxhky8hn3flma9")))

(define-public crate-dir_walker-0.1 (crate (name "dir_walker") (vers "0.1.9") (hash "182367gsrrc3d5w6qx17gaqqffh9l41ybc9pgjcvg07rwc4k5jz1")))

