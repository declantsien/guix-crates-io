(define-module (crates-io di mi) #:use-module (crates-io))

(define-public crate-dimi-0.1 (crate (name "dimi") (vers "0.1.0") (deps (list (crate-dep (name "flume") (req "^0.10") (features (quote ("async"))) (kind 0)) (crate-dep (name "lookit") (req "^0.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "pasts") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "smelling_salts") (req "^0.5") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1qf54cdif1pp0qn0frgc7rnd6ffgb2l73n1f19zrpdsr2qlyd382")))

