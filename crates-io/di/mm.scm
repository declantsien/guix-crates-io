(define-module (crates-io di mm) #:use-module (crates-io))

(define-public crate-dimmed-0.1 (crate (name "dimmed") (vers "0.1.0") (hash "0d1i8yr7by0cv37qdgryjkrph4655nwl9x7rkn39gvjrww2xk5zk")))

(define-public crate-dimmer-0.1 (crate (name "dimmer") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5") (default-features #t) (kind 0)))) (hash "1jqrs9wd20ygggrmcs749vw1gxj1gwzpzcg4g1gqz8zd5mqdx5ly")))

(define-public crate-dimmer-0.2 (crate (name "dimmer") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5") (default-features #t) (kind 0)))) (hash "1gb81cirmyi9pyh6rx50ylxamcxz7pbrqns8viw5mm6ky9qlpphv")))

(define-public crate-dimmer-0.2 (crate (name "dimmer") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5") (default-features #t) (kind 0)))) (hash "1n9lig9g8yydszcwpk7dgj0mrk1w1xb20k9cphq7snk5zyvx14nx")))

