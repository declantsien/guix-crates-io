(define-module (crates-io di sq) #:use-module (crates-io))

(define-public crate-disqrust-0.1 (crate (name "disqrust") (vers "0.1.0") (deps (list (crate-dep (name "disque") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5") (default-features #t) (kind 0)))) (hash "0z70nbnr2i3f4x4sb8kqk89s850d78igj10dv7vrsm9b3pzx0gv7") (features (quote (("nightly"))))))

(define-public crate-disque-0.1 (crate (name "disque") (vers "0.1.0") (deps (list (crate-dep (name "redis") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1fcyzq6yww34n13isqr5vfm3yv7x11pxvrx17lji52b4pa179i2n")))

(define-public crate-disque-0.2 (crate (name "disque") (vers "0.2.0") (deps (list (crate-dep (name "redis") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0kncwsph6pvcyl54c8s0fq10kl0bf2gq4gj7f7601i66w4hybvzh")))

(define-public crate-disque-0.2 (crate (name "disque") (vers "0.2.1") (deps (list (crate-dep (name "redis") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ybbz6yws722walg59h73dy3j5k2igg95z3kskiajhdpn6najgcp")))

(define-public crate-disque-0.2 (crate (name "disque") (vers "0.2.2") (deps (list (crate-dep (name "redis") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0nhfkk1wzkrz6v3hiyw5hvh7mi1vbk8p1845n0ls0a1wgyhhh19v")))

(define-public crate-disque-0.2 (crate (name "disque") (vers "0.2.3") (deps (list (crate-dep (name "redis") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "13ynplczhj59wlln03j8zjd8vdq2q2w1ycl2vgmf791pphy1z7x5")))

(define-public crate-disque-cli-0.1 (crate (name "disque-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.1.1") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "resp") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1d9rfdjqibmalphxcgfwhcn759fa1c4079nsh6zmh6d90hzk8jrq")))

(define-public crate-disque-cli-0.1 (crate (name "disque-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.1.1") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "resp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0ldccmlwpkpyncza88ww7y2ws4p4mcqw7zl9fic05i28r0mcvq7k")))

(define-public crate-disque-cli-0.2 (crate (name "disque-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.1.2") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "resp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0m733l0fnpmjdj8zx70mkxmdyyk8vqbai68qgi1prz5qfzs8i0fx")))

(define-public crate-disque-cli-0.2 (crate (name "disque-cli") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.5.2") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "resp") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1dyws3zxk9ix5lp6jaxvar25qcs818z54i74sif7464g52695axn")))

