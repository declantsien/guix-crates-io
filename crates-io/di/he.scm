(define-module (crates-io di he) #:use-module (crates-io))

(define-public crate-dihedral-0.0.1 (crate (name "dihedral") (vers "0.0.1") (hash "1z6vnx04984vyp8lpixfn723n4cnabs715lkpvawrwc4whfid448")))

(define-public crate-dihedral-0.0.2 (crate (name "dihedral") (vers "0.0.2") (hash "1vynh28rc1v8i1ws05diw01vl8s7cnqc0zbh0h90zfhmphqjsr34") (features (quote (("f32"))))))

(define-public crate-dihedral-0.0.3 (crate (name "dihedral") (vers "0.0.3") (hash "0gipbns58kb663zqb7bm7s6gc3bzj6hx7cz7w3794a9h3bzwqm01") (features (quote (("f32"))))))

(define-public crate-dihedral-0.0.4 (crate (name "dihedral") (vers "0.0.4") (hash "1ynh1v6rwqsilnlrcpq5w9na57nmf7vi0adgmbnc9mpa1ygxisrg") (features (quote (("f32"))))))

