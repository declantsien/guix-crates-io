(define-module (crates-io di -e) #:use-module (crates-io))

(define-public crate-di-exchange-0.1 (crate (name "di-exchange") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.10.4") (default-features #t) (kind 1)))) (hash "0ajqjisnd6fp43fwklm3yj5y6p1rldpkmifp7bpk4xqdli0ffmm9") (yanked #t)))

(define-public crate-di-exchange-0.1 (crate (name "di-exchange") (vers "0.1.1") (deps (list (crate-dep (name "prost") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.10.4") (default-features #t) (kind 1)))) (hash "148v1qhrrvd3xjyqi4syvha4v64ywpzh4vd6lhinbvnax7gcy7n3")))

