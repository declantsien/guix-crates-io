(define-module (crates-io di rx) #:use-module (crates-io))

(define-public crate-dirx-0.1 (crate (name "dirx") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "^0.26") (features (quote ("dir" "fs"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.6") (default-features #t) (kind 2)))) (hash "01lwhcdkj0cmbyvpxgv6qiavb8sx6pkprgs8biqcg9y71s82gcqv")))

