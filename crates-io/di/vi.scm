(define-module (crates-io di vi) #:use-module (crates-io))

(define-public crate-divide-0.1 (crate (name "divide") (vers "0.1.0") (hash "09m1fz7493p8pxmgznd1xiv05fpyrd8a9arqyvklprk980kqslx7")))

(define-public crate-divide-0.1 (crate (name "divide") (vers "0.1.1") (hash "0ykr3r0ryaly1vbfmb7d8jnj0dbbn7fzrggxi9g13hrkf0j6707f")))

(define-public crate-divide_range-0.1 (crate (name "divide_range") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s5v6qgmakl674sk0hvpzn037wcssqs7g4hswrdmbn8j5h61jqrn")))

(define-public crate-dividebatur-0.1 (crate (name "dividebatur") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0n8zv8yvl46zz65yygqnx88cmjxh116d59grrw9pp7mzjyhcjr1g")))

(define-public crate-dividebatur-0.2 (crate (name "dividebatur") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0fpp741gjqddr3gaw944vriphhz5pw5qbny6fccxf3w7n9w9cmlz")))

(define-public crate-dividebatur-0.2 (crate (name "dividebatur") (vers "0.2.1") (deps (list (crate-dep (name "csv") (req "^1.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0bjpazcgzbha09dnbzwbmvfa7ypx908fnx8jxi19wr4bim9zyyva")))

(define-public crate-dividend-0.0.0 (crate (name "dividend") (vers "0.0.0") (hash "02jx6n3ric8n15xjfk6qcl60r0dm4y553kpj315m9j7j10p0p0b7")))

(define-public crate-divideo-0.0.0 (crate (name "divideo") (vers "0.0.0") (hash "1c97kkskxngqvcf2pc3n8xg0xdzh6s54yb5bi7kx4sfhgkyr1gh7")))

(define-public crate-divider-0.1 (crate (name "divider") (vers "0.1.0") (hash "13by88q3nhr159d4yg9rhqrliqkp26g744520fyipbljflhqq564")))

(define-public crate-divider-0.1 (crate (name "divider") (vers "0.1.1") (hash "0bl7rcgczi4kph5wg2vjfdvlqk4fz0qp7hb3f22vj3plindxb5s5")))

(define-public crate-dividite-0.1 (crate (name "dividite") (vers "0.1.0") (hash "1991aapwngjarb0j12w5rqf8jv9hpsdjf3q7xrril3qr5k6m4vvc")))

(define-public crate-divina-0.1 (crate (name "divina") (vers "0.1.0") (deps (list (crate-dep (name "divina_compile") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "divina_config") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "divina_git") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "divina_util") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "jemallocator") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "mimalloc") (req "^0.1.26") (target "cfg(windows)") (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.3.7") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1d9b2kny0pw6zs040waqchldm3w0bq89l666lxydkc9fy809l3sx")))

(define-public crate-divina_compile-0.1 (crate (name "divina_compile") (vers "0.1.0") (deps (list (crate-dep (name "divina_config") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "shellfn") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0igvn29wr2rpwwqar1ayyinscl8hw67g9j1q7kjggxjrf92lwmx8")))

(define-public crate-divina_config-0.1 (crate (name "divina_config") (vers "0.1.0") (deps (list (crate-dep (name "divina_util") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0kyjd5rf6nng5jsppv65ngiqkzjspszhjx3vb8i296a75zkc71vb")))

(define-public crate-divina_git-0.1 (crate (name "divina_git") (vers "0.1.0") (deps (list (crate-dep (name "git2") (req "^0.13.25") (default-features #t) (kind 0)))) (hash "15p4j366fcawwkl6snfjhmmjwqfjr9m3wzkqp1r8qyba1wnsy5n2")))

(define-public crate-divina_util-0.1 (crate (name "divina_util") (vers "0.1.0") (hash "0in0n06k3jn4dg6vy0glv203llmqq6n9i8qq81lbk4kzik7284pn")))

(define-public crate-diviner-0.1 (crate (name "diviner") (vers "0.1.0") (hash "0x8nlh6l2pg902svbiz3cm0jckgb3l84lz8zjnd1a5s36h91ci2v")))

(define-public crate-diviner-0.2 (crate (name "diviner") (vers "0.2.0") (deps (list (crate-dep (name "async-task") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0d68gdibj6w29jznm6xh98kfhi9c3prjxd32v7nxkasxjgkxin1k")))

(define-public crate-divines-0.1 (crate (name "divines") (vers "0.1.0") (hash "1n0b2k5qgrf4h3q4mfyhsgv1y51dvkviy5100kwsb9jica4rqxn7")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0shsa03604h3v1ph68m22128axav8595myqmk38yqg8wjk8ai8k0")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0793c9sy30zz3m0b3p4p160ws8ngyjvir9v0vg8z453nynz6s3n0")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qr8c138ylmkcmc8sam0fxhnn69c8p7wnn8m7ix1sb0hk5pqaq30")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "18sab72m70g71al47cy7a8fmjywxs1jgzjgjy13g7ih66p5pg3jf")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h8mdi2agkwwa47kqqni365gx30dsjad2328rdf8m2zc53jns033")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09m95lm505h1mxz570p8fi79l9dlc8ks7xaasrhf6l98p9v45cyj")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "12p758jdkygfx90m73spv0y79xaf4w4zyzr8s98jyr5l7gd36lgx")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dqkimvvmzjnxs5lrcghjadpy01fs5jpp3gh8m0p5l81xdw57b2g")))

(define-public crate-diving-decompression-0.1 (crate (name "diving-decompression") (vers "0.1.9") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kglyrj69crq9ymr90hyjp5fk247fcacs8iaswgrlipvzwcdljh7")))

(define-public crate-divisibility_check-0.1 (crate (name "divisibility_check") (vers "0.1.0") (deps (list (crate-dep (name "check") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qkw0lbl770rz8rhh8l4lv6wbfv6pxfzn4xw6a5nzw9c5w3zsqc1")))

(define-public crate-divisors-0.1 (crate (name "divisors") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1y0rrxn5366n6c9ihlfnla9056hcsf0ljs61w7yhha68g2khcrlm")))

(define-public crate-divisors-0.1 (crate (name "divisors") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1caa4himc0haf53cj8bshyqmhsjjp9rxm1avprxy8qc9q1r2qhza")))

(define-public crate-divisors-0.1 (crate (name "divisors") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1pdx76ikyfbah1fwl22579clbrbql3y0wnjrxs84ylvw098j2w2y")))

(define-public crate-divisors-0.1 (crate (name "divisors") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1mf2j695pxixs8xm3hnxzj9x4mmimh19h1gkh118xkilcccar776")))

(define-public crate-divisors-0.1 (crate (name "divisors") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1hlv7c45wp2h50a7vfbvhqb6zzn0pc9683bkmj960b1d6qkfj5zf")))

(define-public crate-divisors-0.1 (crate (name "divisors") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1nrq1zcgqjl3nv2iji65n2hxk2a18d590gsvc4737l7haykjkhgs")))

(define-public crate-divisors-0.2 (crate (name "divisors") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0ig7jwv3gs2xv133lh6bfp480jjb0ni3j6zm1b3v8mxwc6n64s2k")))

(define-public crate-divisors-0.2 (crate (name "divisors") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0vhkp7fyyy5rxjbw335di2gz69fc1nki6zg0y0i1sysnjhxldcdv")))

(define-public crate-divisors_fixed-0.3 (crate (name "divisors_fixed") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "05n0273bfrjv2ppc1c0ma5vrfimrww3bsx23lr33q0946xm9nm9w")))

(define-public crate-divisors_fixed-0.4 (crate (name "divisors_fixed") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "02779bn8lzpaylb01yjl8r0db7jplp3vx8wjy28bsrnf01cv8v4z")))

