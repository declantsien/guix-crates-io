(define-module (crates-io di ra) #:use-module (crates-io))

(define-public crate-dira-0.1 (crate (name "dira") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.83") (default-features #t) (kind 0)))) (hash "0zf5q0kdcx3pfada7rpbsql7aslsjg77kmbzcix26sddvvxlywik")))

(define-public crate-dira-0.2 (crate (name "dira") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.83") (default-features #t) (kind 0)))) (hash "0bckx1mhyjiyc57wjr3b0fki3rcnldk1gl6razm0ghqla60isa8g")))

(define-public crate-dira-0.3 (crate (name "dira") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.83") (default-features #t) (kind 0)))) (hash "11if7qyh8wzl4c298y5pny9ap7v9kjwx1wkbiyzcpjzylg6cr3bd")))

(define-public crate-dirac-0.0.0 (crate (name "dirac") (vers "0.0.0") (hash "0432ag0hlbvr9g639347g9mwkv82s5zgpvwx1zgl2mzmwilpzfci")))

