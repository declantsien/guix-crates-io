(define-module (crates-io di ve) #:use-module (crates-io))

(define-public crate-dive-0.0.0 (crate (name "dive") (vers "0.0.0") (deps (list (crate-dep (name "whoami") (req "^0.5") (default-features #t) (kind 0)))) (hash "1174695s554ncyzrrcghgg2sxn2mmkl45mgri2f8n08q51d4hpj0")))

(define-public crate-dive-0.0.1 (crate (name "dive") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "stronghold") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wavy") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^0.5") (default-features #t) (kind 0)))) (hash "07069bkkxxfcli3civyybqa8hmng219bm6yi8qyq5bnj6yvywrba")))

(define-public crate-dive-deco-0.1 (crate (name "dive-deco") (vers "0.1.0") (hash "094fbd7h4z6j6jrb24iq6lgar4dzg1paxpc7i1rfh31d367zhsd8")))

(define-public crate-dive-deco-0.1 (crate (name "dive-deco") (vers "0.1.1") (hash "12vrmly04w72wv337s5hrphrwwrisig7hcyr15lvpsib0vya2qr4")))

(define-public crate-dive-deco-0.2 (crate (name "dive-deco") (vers "0.2.0") (hash "0ry6j4nqpgia02c3np1cgqh6sccvicc9q697qhdpagw87vda1950")))

(define-public crate-dive-deco-0.3 (crate (name "dive-deco") (vers "0.3.0") (hash "1dyfv02w6ig4dj0dz8lxcw9ynyfg02rb15hifvfxk6ja1lybw3b1")))

(define-public crate-dive-deco-0.4 (crate (name "dive-deco") (vers "0.4.0") (hash "09zggc3iw6p6a1nd9hf140lcblqn3v3r602njlyy582jr0srjixp")))

(define-public crate-dive-deco-0.5 (crate (name "dive-deco") (vers "0.5.0") (hash "1bf51c36pn48s96rs69zh7x61n8f76rvdf1y31hn3xw7qkm593fg")))

(define-public crate-dive-deco-0.5 (crate (name "dive-deco") (vers "0.5.1") (hash "16297pwn9ipy8ad76hihgng57rr6ihs2i17rvs6jsnwjkvn9qhjs")))

(define-public crate-dive-deco-0.6 (crate (name "dive-deco") (vers "0.6.1") (hash "0c282b8y3a8fppwcmxz67479ghcjnr636s1nri1r4s12257h0qq4")))

(define-public crate-dive-deco-1 (crate (name "dive-deco") (vers "1.0.0") (hash "115k6wz719rm9fqjzi4lqcic1w24nv8394i23i420m6bq767awv2")))

(define-public crate-dive-deco-1 (crate (name "dive-deco") (vers "1.1.0") (hash "1k5k1wv9m4x2cxm7cfyqk8brb8ch6g37nwck7vzzf62kad9kcikm")))

(define-public crate-dive-deco-1 (crate (name "dive-deco") (vers "1.2.0") (hash "1gc6dy883vcy103kn929cn6c629cccmcdi8cap3r1fqhl8ja8b9x")))

(define-public crate-divecli-0.1 (crate (name "divecli") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "jwalk") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "05smcgcwn6hk23gss7p79nwpmv1nd7vc2qh4qbmy49fmz55a7895")))

(define-public crate-divecli-0.1 (crate (name "divecli") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "jwalk") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0syd4rx4gak107s8lpbar73vwvpqhhr3qz3xpqi8hc8cyd8rfv36")))

(define-public crate-diver-0.0.0 (crate (name "diver") (vers "0.0.0") (hash "0h35r8pwnvwzy15i5bh15q78fsn4pa1glpkvm8k2l2z2l0fql7ps")))

(define-public crate-diverge-0.0.1 (crate (name "diverge") (vers "0.0.1") (hash "0gqbv1a48fxc15143c57ab8zbzlwimvl3jpna6b42b8fyi5gr225")))

(define-public crate-diverge-0.0.2 (crate (name "diverge") (vers "0.0.2") (hash "1s31aclcy44jpyq25qjmh09p0rrk7r569syp45v4si217cn1j7v8")))

(define-public crate-diverge-0.0.3 (crate (name "diverge") (vers "0.0.3") (hash "0h56lvyys54a39f3jhcmw55zz54n2wmgihsxava5d0zxib445kp8")))

(define-public crate-diverge-0.0.4 (crate (name "diverge") (vers "0.0.4") (hash "1jwg12qy6jwq1664ga5ci537h7wig42hl638y4vzn3ypliwjax5g")))

(define-public crate-divert-0.1 (crate (name "divert") (vers "0.1.0") (hash "1w4cn8lflw70ds4jpqgvr0r2mp9zbqihgapfz6inwb7mkb2y3vjz")))

(define-public crate-divert-0.2 (crate (name "divert") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0v6hxsrflwwgyza8q7lgd6y3pmglpx2xk4q1zy23lb48cl7m5h34")))

(define-public crate-divert-0.3 (crate (name "divert") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1r22yviz7n3q4g1s9nrwb7gysdnsn24gx5mi31fv6npkd2bxnl7l")))

(define-public crate-divert-0.4 (crate (name "divert") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.71") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 2)))) (hash "08vdf94lw95ir28b6s9qpq9h4pknh958v821afxcb0zqpmcqqzx7")))

(define-public crate-divert-0.5 (crate (name "divert") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "recastnavigation-sys") (req "^1.0.1") (features (quote ("detour_large_nav_meshes"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 2)))) (hash "0ljwmw07v5j2vbw78wkxdfzjcwh6v4fwzb5lq97cfi0pfiyslw69")))

(define-public crate-divert-0.6 (crate (name "divert") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "recastnavigation-sys") (req "^1.0.3") (features (quote ("detour_large_nav_meshes"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 2)))) (hash "0ic4q39izv29psygjklag2avayim9km1xycdg1qhkj4fabb9d7fg")))

(define-public crate-diverter-0.1 (crate (name "diverter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0wqi18ypyldbbnv56r7my963acmdkl1hq23vpyv9cmabn66yxp61")))

(define-public crate-diverter-1 (crate (name "diverter") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winreg"))) (default-features #t) (kind 0)))) (hash "144lxh0ivzrywpmg6k07v1j0qlqxf4vli010n67rm4sz7pc2x95b")))

(define-public crate-diverter-1 (crate (name "diverter") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.78") (default-features #t) (kind 1)))) (hash "0g4af21p5vjqzdbkm9lbbjb6vk00ixfvc5rmd46mc7i20s0zwk7l")))

(define-public crate-diverter-1 (crate (name "diverter") (vers "1.3.0") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.78") (default-features #t) (kind 1)))) (hash "1ha08cgn1p473scca4ja6y44c8z6diqff8vw0mdgzgra93mk7vc3")))

(define-public crate-diverter-1 (crate (name "diverter") (vers "1.3.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.78") (default-features #t) (kind 1)))) (hash "0n9lsgzmn4sqh1z21n6fj1l5kyp13iks3jgd2yylw8r7fky3qd09")))

(define-public crate-diverter-2 (crate (name "diverter") (vers "2.0.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.78") (default-features #t) (kind 1)))) (hash "1r3can9fbgv0kjafaxpf3xyiggrcx5r77yavdbh0g7hh05g5vl8l")))

