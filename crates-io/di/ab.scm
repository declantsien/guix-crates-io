(define-module (crates-io di ab) #:use-module (crates-io))

(define-public crate-diablo-0.1 (crate (name "diablo") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.7") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.10") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1.5") (default-features #t) (kind 2)))) (hash "1g0njzb7jnpvl6bk17gv9f4n9yxqygdqq5z4bvfg7rwamzyhgplh")))

