(define-module (crates-io di la) #:use-module (crates-io))

(define-public crate-dilate-0.3 (crate (name "dilate") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.78") (default-features #t) (kind 2)))) (hash "0gzp6rb4d3jrzcd5hfbrzybs5p3m72xw2fnnkflm2kw73m9gklbp")))

(define-public crate-dilate-0.4 (crate (name "dilate") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "054sccvqbpc1y9vby42dp4m4kjivk5n8c93mbkm9w8lykls3npqs")))

(define-public crate-dilate-0.5 (crate (name "dilate") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "0h8sm2sl20h0rqyr7p82jz3ynmfcw49d15pcvnk2xd6sqpx2bisp")))

(define-public crate-dilate-0.6 (crate (name "dilate") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "12jli12gqwcnhn96hv29nyxdj1cfp5zgdqbz0kn96cb7wdgklhxw") (features (quote (("std"))))))

(define-public crate-dilate-0.6 (crate (name "dilate") (vers "0.6.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "18jsijk4p0pqc256jf5yxjwx30xn2d25j7f7djdjj3nsss3j0201") (features (quote (("std"))))))

(define-public crate-dilate-0.6 (crate (name "dilate") (vers "0.6.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "1dcs3aihr8ddfp22vvw7nly6b1g7nr74a40h3zsg74l90lhibb9q") (features (quote (("std"))))))

(define-public crate-dilate-0.6 (crate (name "dilate") (vers "0.6.3") (deps (list (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "1w1lgz4dvjhgf4g4xymfzm8pprj289j7yqw6galljliiawbmnvkj") (features (quote (("std")))) (rust-version "1.70")))

