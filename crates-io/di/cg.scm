(define-module (crates-io di cg) #:use-module (crates-io))

(define-public crate-dicgen-0.1 (crate (name "dicgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "1ylf5mb5k3dckns5z1wy2bghqxi7inwcpjjc5f2j6i2gi8mwyvb6")))

(define-public crate-dicgen-0.2 (crate (name "dicgen") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "0r2ma4xcy296a3c4dhw5czn2dyrq4nfhp6f69d87krvimavk6glx")))

(define-public crate-dicgen-0.2 (crate (name "dicgen") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "1ryv5mrj34ddn3gfa5ngia105sqb571cjkr983886k8v0m8nzrpc")))

(define-public crate-dicgen-0.3 (crate (name "dicgen") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0wiycmxcg561z8xgjp4y5hd0al5ij2078xbkwrpng5p1wqgg48hf")))

