(define-module (crates-io di su) #:use-module (crates-io))

(define-public crate-disunity-0.0.0 (crate (name "disunity") (vers "0.0.0") (hash "0jchyrs90fy6cdff3ymfk95mcp461p3a16iqxrrabbdw5hdjmb99")))

(define-public crate-disuse-0.0.1 (crate (name "disuse") (vers "0.0.1") (hash "17rmlk5j5pq374vzh5mvg8qspv3my7qkvsbm6929ir63xpdw5j3a")))

(define-public crate-disuse-0.0.2 (crate (name "disuse") (vers "0.0.2") (hash "0pjjc1b86iz5l6vlzii6f0lzg688mqkzff6kw057apbcfxp9rwid")))

