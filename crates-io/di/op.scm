(define-module (crates-io di op) #:use-module (crates-io))

(define-public crate-dioptre-0.1 (crate (name "dioptre") (vers "0.1.0") (deps (list (crate-dep (name "dioptre-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0sc4fxnfp2wizdllxqjqam2kcwmzsbpc601vgyzw28rf46nazgmg")))

(define-public crate-dioptre-derive-0.1 (crate (name "dioptre-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x755kkhsip7g1gww5gippw7piwzbhvi5w12rkdh0p6wdw9k73pa")))

