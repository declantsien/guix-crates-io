(define-module (crates-io di nz) #:use-module (crates-io))

(define-public crate-dinzai-datni-0.1 (crate (name "dinzai-datni") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2") (default-features #t) (kind 0)))) (hash "03qyv2bkbkp0nzwfmbsx0wpbz6fbs4x9sqdqmm99k97v3qx3fshz") (features (quote (("nightly") ("default"))))))

(define-public crate-dinzai-datni-0.1 (crate (name "dinzai-datni") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2") (default-features #t) (kind 0)))) (hash "04gk5hj8cbisffiyc7ibxa0sb96hpzhcl0qz52jk4k5j2s862x9j") (features (quote (("nightly") ("default"))))))

