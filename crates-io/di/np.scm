(define-module (crates-io di np) #:use-module (crates-io))

(define-public crate-dinput-0.3 (crate (name "dinput") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0bz5mdcghsavw9anbbcrq57ax05z957rpkawgsjdz5pmf6bqynhl") (yanked #t)))

