(define-module (crates-io di t-) #:use-module (crates-io))

(define-public crate-dit-as-91896-0.1 (crate (name "dit-as-91896") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.4") (default-features #t) (kind 0)) (crate-dep (name "map-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "154dr43piik62hrgbsnbw2k91wnmy2ywq7dczaka5xpnizkn0x2v")))

(define-public crate-dit-as-91896-0.1 (crate (name "dit-as-91896") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.4") (default-features #t) (kind 0)) (crate-dep (name "map-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1mxpmzkbfsdapv8q4v7af4qxka9hxlghw82rjkyzczffgp451slb")))

(define-public crate-dit-as-91896-0.1 (crate (name "dit-as-91896") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.4") (default-features #t) (kind 0)) (crate-dep (name "map-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0gma9h5w4chi71im5gcdh1p65bjrczrb7gfsh22s8bnin39a1ip9")))

(define-public crate-dit-as-91896-0.1 (crate (name "dit-as-91896") (vers "0.1.3") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.4") (default-features #t) (kind 0)) (crate-dep (name "map-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0f0g3m09fp659b9i6hhb1z8bapr9719r428gazr6qc9dv4m5k7ch")))

