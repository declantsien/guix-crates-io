(define-module (crates-io di rk) #:use-module (crates-io))

(define-public crate-dirk-0.1 (crate (name "dirk") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_ssm") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "078r767ypmszgjyyxsxf8plrq1brqz8bab9hbv4hl2fqj9i2a634")))

