(define-module (crates-io di du) #:use-module (crates-io))

(define-public crate-didu-2 (crate (name "didu") (vers "2.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "093wly9rf9arcdvbij4caslgb514a45p7x9bl44yldbcydj97p4v")))

(define-public crate-didu-2 (crate (name "didu") (vers "2.2.2") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "1g75dj4bky5ryjmsfh7l99rc3nx5rjkg0srlywih43b5b95rsyn5")))

(define-public crate-didu-2 (crate (name "didu") (vers "2.2.3") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "1ih0wwp2228lrxh5nvmy2s6hgni3s20619jzbm8w3ywd3hrd7s56")))

(define-public crate-didu-2 (crate (name "didu") (vers "2.3.1") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "0gjjbgahlssbgd42dpl5fw1n1wc59b5lamhpncp0wrzd5a6r59gy")))

(define-public crate-didu-2 (crate (name "didu") (vers "2.4.0") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "0rz7fsdddfsrmm98x99nwwjx2avk6121q785vw9d2q3qxql7prch")))

(define-public crate-didu-2 (crate (name "didu") (vers "2.5.0") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "recolored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "19gcjvdmmih3zqv9pfhfczzgfqqr5dacw6dbh0jgrfpgva59nhc7")))

(define-public crate-didu-2 (crate (name "didu") (vers "2.5.1") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "recolored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "05gbqpqwb5nfc6fvl8kmx9ig790sdlifsq6pzzgdh1pjyqfmhf0y")))

(define-public crate-didu-2 (crate (name "didu") (vers "2.5.2") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "recolored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "unit-conversions") (req "^0.1.11-alpha.5") (default-features #t) (kind 0)))) (hash "1xmya1fa9bxhjkf1zfahqddm05bdhywh7gs8800h4k1lqs3720n6")))

