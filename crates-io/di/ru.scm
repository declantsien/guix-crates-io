(define-module (crates-io di ru) #:use-module (crates-io))

(define-public crate-diru_grrs-0.1 (crate (name "diru_grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)))) (hash "1hc6n3gzcgqvmqb75wdwnh8ci56wx5h35kaf0ll39i9la4fmg0jm")))

(define-public crate-diru_grrs-0.1 (crate (name "diru_grrs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)))) (hash "138ngnkbmj8a45ik7snxsk0apwvpfs1nhf91gaqg9yy25f6xg1mb")))

