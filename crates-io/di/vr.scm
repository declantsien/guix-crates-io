(define-module (crates-io di vr) #:use-module (crates-io))

(define-public crate-divrem-0.1 (crate (name "divrem") (vers "0.1.0") (hash "1ff0vqmr0rmwmxr3is6vn09ad6ldzyxkc9g8zsyr365rvha8k7xw")))

(define-public crate-divrem-1 (crate (name "divrem") (vers "1.0.0") (hash "10nx8ipssl505knk1g42hb0w1r367nq2j2aysv0i4ppgiwgfbpb9")))

