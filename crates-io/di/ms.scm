(define-module (crates-io di ms) #:use-module (crates-io))

(define-public crate-dims-0.2 (crate (name "dims") (vers "0.2.1") (deps (list (crate-dep (name "dims_core") (req "^0.2.1") (kind 0)) (crate-dep (name "dims_derive") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "dims_macro") (req "^0.2.1") (kind 0)))) (hash "1lpwcyzp21yj6959rncvq1z5skxcd1pd7vxpfx94g8salkzfa1qq") (features (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("si") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "us" "si" "str"))))))

(define-public crate-dims-0.3 (crate (name "dims") (vers "0.3.0") (deps (list (crate-dep (name "dims_core") (req "^0.3.0") (kind 0)) (crate-dep (name "dims_macro") (req "^0.3.0") (kind 0)))) (hash "09b9ylsf8rzfv94yn71l1dv8qch6xdgg1jlxyj9cgjqbi67an16h") (features (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("si") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "us" "si" "str") ("debug_us"))))))

(define-public crate-dims-0.3 (crate (name "dims") (vers "0.3.1") (deps (list (crate-dep (name "dims_core") (req "^0.3.1") (kind 0)) (crate-dep (name "dims_macro") (req "^0.3.1") (kind 0)))) (hash "1gfrwy8r2a1p1zcwjws1rfmmy1rdfy0iaabrgcvymzcms6zw53k3") (features (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("si") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "us" "si" "str") ("debug_us"))))))

(define-public crate-dims-0.4 (crate (name "dims") (vers "0.4.0") (deps (list (crate-dep (name "dims_core") (req "^0.4.0") (kind 0)) (crate-dep (name "dims_macro") (req "^0.4.0") (kind 0)))) (hash "0cshjh1gd9g423maynhms84l1hlsf0lnqdd3n6bd4gynnx5h0d17") (features (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("std" "dims_core/std") ("si") ("f64") ("default" "us" "si" "str" "std") ("debug_us"))))))

(define-public crate-dims-0.4 (crate (name "dims") (vers "0.4.1") (deps (list (crate-dep (name "dims_core") (req "^0.4.1") (kind 0)) (crate-dep (name "dims_macro") (req "^0.4.1") (kind 0)))) (hash "0pw5aigkic7q135q1n2akhisizis5jysnjl7hbn08zshswyvck68") (features (quote (("us") ("str" "dims_core/str" "dims_macro/str") ("std" "dims_core/std") ("si") ("f64") ("default" "us" "si" "str" "std") ("debug_us"))))))

(define-public crate-dims_core-0.2 (crate (name "dims_core") (vers "0.2.1") (hash "1npsqr0f6yzi4xkhx4q0y3g5q8alpd4s8qvf9nmn3pqcj59qni1g") (features (quote (("str") ("no_std") ("f64") ("default" "str"))))))

(define-public crate-dims_core-0.3 (crate (name "dims_core") (vers "0.3.0") (hash "1hjfzwr8qzlrnn27aigj6nhhf6f8cxlzbmhv48x7fw9xzln64s6q") (features (quote (("str") ("no_std") ("f64") ("default" "str"))))))

(define-public crate-dims_core-0.3 (crate (name "dims_core") (vers "0.3.1") (hash "0l24gsjncsxnx8gv2qqf3nqshf192bpd0wmqwx0ag3akwcxlv7yk") (features (quote (("str") ("no_std") ("f64") ("default" "str"))))))

(define-public crate-dims_core-0.4 (crate (name "dims_core") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0g05g2dzhn9sy4kjgp0fgg28v6sba88fx32n44102mxwrbbjw5rq") (features (quote (("str") ("std" "num-traits/std") ("f64") ("default" "str" "std"))))))

(define-public crate-dims_core-0.4 (crate (name "dims_core") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0asx90326yvllx8g8psv1pkhin1zdmr70478hpgh03dh3g4m7hrn") (features (quote (("str") ("std" "num-traits/std") ("f64") ("default" "str" "std"))))))

(define-public crate-dims_derive-0.2 (crate (name "dims_derive") (vers "0.2.1") (deps (list (crate-dep (name "dims_core") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rrrhwvgnh8pigcpd040wg1lgl1mddjjc2zhwx0809rcxhxfwd4h")))

(define-public crate-dims_macro-0.2 (crate (name "dims_macro") (vers "0.2.1") (deps (list (crate-dep (name "dims_core") (req "^0.2.1") (kind 2)) (crate-dep (name "dims_derive") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0rw5bki60026hzs87yg3bh033knlav20iak2l0pybl2cqxx8lr3d") (features (quote (("str" "dims_core/str") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "str"))))))

(define-public crate-dims_macro-0.3 (crate (name "dims_macro") (vers "0.3.0") (deps (list (crate-dep (name "dims_core") (req "^0.3.0") (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0rc32rrgfxgmfmv1ihhb0kbg4jn4g0ml9c9ikb0xaf9z6pri00n5") (features (quote (("str" "dims_core/str") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "str"))))))

(define-public crate-dims_macro-0.3 (crate (name "dims_macro") (vers "0.3.1") (deps (list (crate-dep (name "dims_core") (req "^0.3.1") (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "01imvkdiw4lfi0h062f5x8ldmshmy4bfd29vdsh6m1bm1zbihywd") (features (quote (("str" "dims_core/str") ("no_std" "dims_core/no_std") ("f64" "dims_core/f64") ("default" "str"))))))

(define-public crate-dims_macro-0.4 (crate (name "dims_macro") (vers "0.4.0") (deps (list (crate-dep (name "dims_core") (req "^0.4.0") (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0mz6vzdr629i5kdmzgnk2wasjls2bij4ghnrba3216bqh448xm4y") (features (quote (("str" "dims_core/str") ("std" "dims_core/std") ("f64" "dims_core/f64") ("default" "str" "std"))))))

(define-public crate-dims_macro-0.4 (crate (name "dims_macro") (vers "0.4.1") (deps (list (crate-dep (name "dims_core") (req "^0.4.1") (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "11k4ng4fxfb7b2658sab8bssz2a9cbvacnilx10zafi1byiskh3l") (features (quote (("str" "dims_core/str") ("std" "dims_core/std") ("f64" "dims_core/f64") ("default" "str" "std"))))))

(define-public crate-dimsum-0.1 (crate (name "dimsum") (vers "0.1.0") (hash "0mrfx77np8gbwmhdnyyqlxli2kigb7qj9slfxi4bmnxnbf1cld55")))

