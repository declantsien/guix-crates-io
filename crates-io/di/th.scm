(define-module (crates-io di th) #:use-module (crates-io))

(define-public crate-dither-0.2 (crate (name "dither") (vers "0.2.0") (deps (list (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jakg909shgv0c21mnaizhfxbfxanpzvswlfghnn0lskppkggam8")))

(define-public crate-dither-0.2 (crate (name "dither") (vers "0.2.1") (deps (list (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wncq3rnv7ffbff9rc1jdnrgf7i5djixsk048ay41mmsh7s5g287")))

(define-public crate-dither-0.3 (crate (name "dither") (vers "0.3.0") (deps (list (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hxf195rnsfa6czgbp96pv98w4rl8wryar6lmslc9f906b2mgck0")))

(define-public crate-dither-0.3 (crate (name "dither") (vers "0.3.1") (deps (list (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "035w1mcm3rw8hj2xpmcyj916m0bxc1zch7ilqxdgp2mnxcfbs4s6")))

(define-public crate-dither-0.3 (crate (name "dither") (vers "0.3.2") (deps (list (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fq8v9629sky8jjlqy0nr6bff6pyyr8j7ab9g1b2gggf0rqnrbfq")))

(define-public crate-dither-0.3 (crate (name "dither") (vers "0.3.3") (deps (list (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "06fz1r4qvn7p4l5g0y3g5gx90h4gpawc56b39k38bzcvf5q8a07m")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.0.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fjq15rv9yb42ar5v1d7k86b2khdrcys9x2bpyhk7wmj19w6l0yk")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.0.1") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cmylpl3nvlhr4jvbhqgbr48xh0wg8miam2mv1zs1d3zzs2zk316")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.0.2") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "15qzl9xqypliaxhgnxw6f0gla9s9pfbdrxwy6p2k8ifham0rbnln")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.0.3") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hyb9l4sqajs5m3jsz3i97b1fc1zd3f4802bs24kgs41dmncyq5a")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.0.4") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r1nnviqyphcnl0xrn7kl11rq82qs5vmp4cbjkd6b73rd1748c48")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.0.5") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b2vd8shr4hpg22hgd9dv8adrann1pg3i8v8dnym7ghfjgk1xpb8")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.1.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "11lwwj0z73pq4wb88d4ff4vgb2fy1minzc7hadddpfibj7qmibhf") (yanked #t)))

(define-public crate-dither-1 (crate (name "dither") (vers "1.1.1") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dsg74qgnc07insyph51xq1nmdms56pdwspklvh08bv32qi5xb5m") (yanked #t)))

(define-public crate-dither-1 (crate (name "dither") (vers "1.2.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a8hdn7dprqlch60hy89wf309j03cb21w5y6ldxh2yxf738ia0dr")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.2.1") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bd7sv5f79m5v48zasrxmskbw4p9p21pp1k0w719gc34r94fa9xi")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "03zshgsirza6n19z0wx0v94b0h45bn0vgasz8dgg49qrnpn6cvns")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.1") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m9a92gp2gxiwi11mq9xskjzngx76cgpnhvpby8nw3q1mbgvwh5d")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.2") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k9pii7c95gq1cazqmfir1ks4m4iq42q9rpgfw3s13sn1brkyvq9")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.3") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0blxqwgiizvs0w4x10yvvbqih4wq78svyqpsjxqb69zabg62ka9w")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.4") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fyjl167gk1s12fa0yzmgx95xg5pxdakgdl8r81dcrcdsbmmmhyc")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.5") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rgmir8criy9lf3x02947zj4h5hqvbaxxc6m85y6c46b5w7cwhxc") (yanked #t)))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.6") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "08hsjncja32fcdc5xsnz8qm4h1jxlndgnkfcwblh5rxbakjasc1v")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.7") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "03bjl76qkksvjxqhl0dc9n2z4ljndlcps5r1fwckffq74z8jzm7d")))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.8") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)))) (hash "0828z6k49hgjr937xw4qcwch510cndnb866wasxl7b9gwhskg91h") (yanked #t)))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.9") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)))) (hash "0zz45g216nk5r3wqm8xaxy8d8j28hk5aqjdk22ymsxygsjx32mry") (yanked #t)))

(define-public crate-dither-1 (crate (name "dither") (vers "1.3.10") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)))) (hash "1qnm0n71r1wrxbld2pln15h4z42h2kk4zllqqhyci8h60vh5sszg")))

(define-public crate-dithor-0.1 (crate (name "dithor") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "03xpg59pkpaflkf66zyh8490hlm4g3cxb8vc2qaxwbgv5bdzdr1l")))

(define-public crate-dithor-0.2 (crate (name "dithor") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "1gpqk84190mgsviz70vj5p6jp6nx4dldyrnx3l7k6nwbx2s4w76s")))

(define-public crate-dithor-0.2 (crate (name "dithor") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "1ry3a4s8biapqvyh8g9bl5v5r612pq6rs7xw62pm0fsaj89gj4cn")))

(define-public crate-dithor-0.3 (crate (name "dithor") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "0whb8b90shmckqzy1n5f79fv51z7fvy4vwb9fd6i94ny0sr6n3nv")))

(define-public crate-dithor-0.3 (crate (name "dithor") (vers "0.3.1") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "1wilp0ikr6x548ia1i4i1vkx8zhg1ri1nqarwrd7f3bvakli0phl")))

(define-public crate-dithord-0.1 (crate (name "dithord") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "0mv5isan8d9gc6pd88da55933pxra51sx4p3pyrzdgi6kfxplc19")))

(define-public crate-dithord-0.1 (crate (name "dithord") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "0fjkly4jwnsb8pv1yr01x9jl9b94l4mq8lm5hmi7dwb9nz64pyi8")))

(define-public crate-dithord-0.1 (crate (name "dithord") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1c7vzai21dbfall8ds9xdxzzz0nq60wmjwjnzg54sngmpwnasdkq")))

(define-public crate-dithord-0.2 (crate (name "dithord") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1dskdlb9ch3w75y7i4d45a5m5zcj1sz47i8fsw9w74w0xn5cmjbw")))

(define-public crate-dithord-0.3 (crate (name "dithord") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1mg5k2m3s9njhzf3n0ymyl52ckc5gahkw7bz9v0bqzdxx3vhhaj6")))

(define-public crate-dithord-0.4 (crate (name "dithord") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1g2abvy5v5sxrk71wdwpk4lv8ipz03vq2w63imhd2clgl3596iw1") (features (quote (("default") ("cli" "clap"))))))

(define-public crate-dithord-0.4 (crate (name "dithord") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "171iv996s5lyz82fhgm67mvb45ghg412gkfk7cxqa06nm24gs3f9") (features (quote (("default") ("cli" "clap"))))))

