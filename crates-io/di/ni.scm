(define-module (crates-io di ni) #:use-module (crates-io))

(define-public crate-dining_philosophers-0.9 (crate (name "dining_philosophers") (vers "0.9.0") (hash "1ksy9nzbh37x4x5isnw51jr48ijppk7miv0phql29kszsf4k1c1x")))

(define-public crate-dining_philosophers-0.10 (crate (name "dining_philosophers") (vers "0.10.1") (hash "1lngv7svdp0i84rn6whccy5nzxqszj1p9mf5p6kc30p1681dbwgb")))

(define-public crate-dining_philosophers-0.10 (crate (name "dining_philosophers") (vers "0.10.2") (hash "041fdpm0gj5kb8yzjbh60i6qlbwzc1pfg5s1xajgbhxy1pra64li")))

(define-public crate-dining_philosophers-0.11 (crate (name "dining_philosophers") (vers "0.11.0") (hash "130ay7h0kdvvfvw1kdv9241v7alrrgidss2fjcskjkni1x3k37xd")))

(define-public crate-dining_philosophers-0.11 (crate (name "dining_philosophers") (vers "0.11.1") (hash "1vscbjvqg00x4ckxdk8vavr5sd64fzbpmam0bql3p5lc7hvynkxh")))

(define-public crate-dining_philosophers-0.11 (crate (name "dining_philosophers") (vers "0.11.2") (hash "17gw7a5aka5nv2n0qxq549dh3hsrh8f88dwd998q1mgalc6h191n")))

