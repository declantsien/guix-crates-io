(define-module (crates-io di vs) #:use-module (crates-io))

(define-public crate-divsufsort-1 (crate (name "divsufsort") (vers "1.0.0") (deps (list (crate-dep (name "once_cell") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sacabase") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ji57b6r8qam9hk1val3i7n2j6rryjjd1x7hf43ai4wmzgf3qsr5") (features (quote (("crosscheck" "once_cell"))))))

(define-public crate-divsufsort-1 (crate (name "divsufsort") (vers "1.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sacabase") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13z4zmb8lzvqwvlzz66znbbjgqbni6f9lvv20h93gx4pskzklhx8") (features (quote (("crosscheck" "once_cell"))))))

(define-public crate-divsufsort-1 (crate (name "divsufsort") (vers "1.0.2") (deps (list (crate-dep (name "once_cell") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sacabase") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10b5jrx2y5m4pk8y0k0nmiix4m978da334rqm71ck25xjgl28rlp") (features (quote (("crosscheck" "once_cell"))))))

(define-public crate-divsufsort-2 (crate (name "divsufsort") (vers "2.0.0") (deps (list (crate-dep (name "once_cell") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sacabase") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "08783gfacg6jnwxwjfxyyy9w2vglwb6563k9pkwlw2zy8mnf6msf") (features (quote (("crosscheck" "once_cell"))))))

