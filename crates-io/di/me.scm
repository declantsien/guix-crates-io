(define-module (crates-io di me) #:use-module (crates-io))

(define-public crate-dimension_shiftable_buffer-1 (crate (name "dimension_shiftable_buffer") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1r5i5m9bx1sfx04rja3a66sda7jjiisglh96zpq5p3nnbshplrnn") (features (quote (("default" "csv") ("csv"))))))

(define-public crate-dimensional-0.1 (crate (name "dimensional") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "115k4irdjinir4p1h5fv27ybriy523kz298hl2nhf464m0p9y9ac") (features (quote (("si") ("default" "si"))))))

(define-public crate-dimensional_quantity-0.0.1 (crate (name "dimensional_quantity") (vers "0.0.1-alpha.1") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "1gdi9njjwp8idb2hqv62yhj59rhwbdcigchikzrrwlfqv38dvdmr")))

(define-public crate-dimensional_quantity-0.0.1 (crate (name "dimensional_quantity") (vers "0.0.1-alpha.2") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "1gyibj18aqlwd5rwb8g68gz7fkkw6z95q3vf6whbbf46wzgw4vaj")))

(define-public crate-dimensional_quantity-0.0.1 (crate (name "dimensional_quantity") (vers "0.0.1-alpha.3") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "1wcqayvaw9a0sdns8qpxzscdx53bvzlmhmndwavb4wrs8p9b6q0i")))

(define-public crate-dimensional_quantity-0.0.1 (crate (name "dimensional_quantity") (vers "0.0.1-alpha.4") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "08wgz0xsq9rkjr25lbgyla10iima1d6b4nc3fabjs9sm3ms50w16")))

(define-public crate-dimensional_quantity-0.0.1 (crate (name "dimensional_quantity") (vers "0.0.1-alpha.5") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "0bmanpkickclcclxdahr4pgp5k1g5sc74jk00cxqfc10fbj45cc3")))

(define-public crate-dimensional_quantity-0.0.1 (crate (name "dimensional_quantity") (vers "0.0.1-alpha.6") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "0cyzdqm97kqpq3q28yqdn3qq0h05naihgfni52nlwkhg4y3x9hp5")))

(define-public crate-dimensional_quantity-0.0.1 (crate (name "dimensional_quantity") (vers "0.0.1") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "1qaizziqlf59vf50r7nlpwkicjs8hqfppxcyman0gxjnisgvk94m")))

(define-public crate-dimensional_quantity-0.0.2 (crate (name "dimensional_quantity") (vers "0.0.2") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.5.*") (default-features #t) (kind 1)))) (hash "0sj1473riz06kpz5wyg7m6mls1x1h573gy75h2066vz7kk21416j")))

(define-public crate-dimensional_quantity-0.0.3 (crate (name "dimensional_quantity") (vers "0.0.3") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.7.*") (default-features #t) (kind 1)))) (hash "1r6g39g4ac8n1nl2w1kwdvllp7vrsjcmard3ya83i9bhxghf828y") (v 2) (features2 (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.0.4 (crate (name "dimensional_quantity") (vers "0.0.4") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "0.7.*") (default-features #t) (kind 1)))) (hash "0srv3n5j1daqjykyh776jqa4dhdqypkkh6vnw3g21p77b47bsnr2") (v 2) (features2 (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.0.5 (crate (name "dimensional_quantity") (vers "0.0.5") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 1)))) (hash "0hvla6lzkay5q123pnbs15bnlca4ryfs6f0izv7dj6239b1899wd") (v 2) (features2 (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.1 (crate (name "dimensional_quantity") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 1)))) (hash "1zh7lk4f7134mhfjdmf12hp3jyfmqxcign6cp6la7ys9lp699lmy") (v 2) (features2 (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.1 (crate (name "dimensional_quantity") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 1)))) (hash "1xdqx6ns4mnwwkbz61qwq91fniiafdgiiy1mrb14b0jgnqpkl3bz") (v 2) (features2 (quote (("use_serde" "dep:serde"))))))

(define-public crate-dimensional_quantity-0.1 (crate (name "dimensional_quantity") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "rust_decimal") (req "^1.35") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 1)))) (hash "02xg9k2i7zil9xf95533q6r2ifsi4gwx15cwpifqnsvvlxb50d5k") (v 2) (features2 (quote (("use_serde" "dep:serde") ("decimal" "dep:rust_decimal"))))))

(define-public crate-dimensional_quantity-0.1 (crate (name "dimensional_quantity") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "rust_decimal") (req "^1.35") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 1)))) (hash "13m337db8nagxc41wh58w6sqlmmrbar7f2pwhp0jfhzqx93znnab") (v 2) (features2 (quote (("use_serde" "dep:serde") ("decimal" "dep:rust_decimal"))))))

(define-public crate-dimensioned-0.0.2 (crate (name "dimensioned") (vers "0.0.2") (hash "1m9j5mnnyr9nj42kbhyy83x8dcnfhfyxv6p2f2h574kjj44n2jnz")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.0") (hash "0ydvhvsnkwczqjsz8icfb4w9g9baydf20bhhkl32i3k0p21mwfl3")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.1") (hash "0xmaxskfxf36d9qaag4zjz54v5k7hivffr5z8zhp2nx1dgfkzfcl")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.2") (hash "0ywk064w6843zpsmmmqlbvm94x6nv84cc13pq48iv8h6irqixrq0")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.3") (hash "0s16yz128mwbfawv2245qx2xmzafixgxadsy85s4vizfy08c8hna")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.4") (hash "0j8slqp4qr3rxv5fsbfy4rbgk6vhggvrmlmmnrqk2l2pb0zp4a52")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.5") (hash "1hjvs4nq6m2qg54lxd1wicv9xc1sf291m9vbq66110aqrpd7wz1w")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.6") (hash "0s05pw22kc8w9bw2ajjlcsxswwmd308x3bcrckzc9cc4whyg5r0z")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.7") (hash "0jp7r1rh99pqyihy91z5f79xadfsmidfybwm0rc3ciwc4rd8k8iv")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.8") (hash "1ax0rp1akwjiasfjyvdky6dhgpnvdcj4hhnpd9v8yixw4mp41zbk")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.9") (hash "1h8vjdgkcrd4az8y18z6by0asn7g0wny1rnfvfss6k7f97hx1nkk")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.10") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "01yb3xjfg3nkq4r3nslwsyjhj2gim03l5k21ba8gjfh63zzl55g4")))

(define-public crate-dimensioned-0.1 (crate (name "dimensioned") (vers "0.1.11") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1fdnbcz737jcdg8crs6sm5qm8m14wsxgigs0q92dd23lp9b2gb97")))

(define-public crate-dimensioned-0.2 (crate (name "dimensioned") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0807hg3bl9pw16icr9q4dhh8dv40vya2pf8i44vbv8vj6fh8mavr")))

(define-public crate-dimensioned-0.2 (crate (name "dimensioned") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1wpx7ix74ippzmv2x6ywbj581whl644i3ngr1s8m2mrddsaisbhs")))

(define-public crate-dimensioned-0.2 (crate (name "dimensioned") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1rc717bzlqv4n5njwfay1xlapyjnj362wmfxkw765lbsh8ff0ay1")))

(define-public crate-dimensioned-0.2 (crate (name "dimensioned") (vers "0.2.3") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "06swmmkgffbcvkjwnsp0n9m06jzdwdvgqz2pidfmm46sikvrc9wz")))

(define-public crate-dimensioned-0.2 (crate (name "dimensioned") (vers "0.2.4") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0s6qjvq1ql33shpv91w11si1apkbw1dfg5dy6gs7rcrc88z0x8hl")))

(define-public crate-dimensioned-0.3 (crate (name "dimensioned") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0banjyjkfbkcmqr8b50hk5719pzxzqp6b3wmdlphblpz1nsvy5ra")))

(define-public crate-dimensioned-0.3 (crate (name "dimensioned") (vers "0.3.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1q2bkkb1m4v1sy7v1xqp1xyh135hmz49ax6pw2gdjjq80h6yf2yd")))

(define-public crate-dimensioned-0.3 (crate (name "dimensioned") (vers "0.3.2") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1i64rqkmlwm735838nkavyiqaf7z8cgrk77i6xf8k2670assmlcb")))

(define-public crate-dimensioned-0.4 (crate (name "dimensioned") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "peano") (req "*") (default-features #t) (kind 0)))) (hash "1jyhxr4nmb6jm6dhixjcl7m6lj82dbbkx8kqpxbr44666q0rargm")))

(define-public crate-dimensioned-0.5 (crate (name "dimensioned") (vers "0.5.0") (deps (list (crate-dep (name "num") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1nwh06d84vmww0w7ni3jl6rqab5biiq1149jbpb1kdq0ngz94xlm")))

(define-public crate-dimensioned-0.6 (crate (name "dimensioned") (vers "0.6.0") (deps (list (crate-dep (name "approx") (req "^0.1.1") (features (quote ("no_std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.117") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0v5w56934fcnlni511qrr13wmrv8xriqmkrxzvyqxckpf2b5sn5h") (features (quote (("test" "clippy" "quickcheck" "quickcheck_macros" "approx" "oibit" "spec") ("spec") ("oibit") ("default"))))))

(define-public crate-dimensioned-0.7 (crate (name "dimensioned") (vers "0.7.0") (deps (list (crate-dep (name "approx") (req "^0.1.1") (optional #t) (kind 0)) (crate-dep (name "clapme") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.5") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "09ky8s3higkf677lmyqg30hmj66gpg7hx907s6hfvbk2a9av05r5") (features (quote (("test" "approx" "ci" "clapme" "quickcheck" "serde" "serde_test") ("std") ("spec") ("oibit") ("default" "std") ("ci"))))))

(define-public crate-dimensioned-0.8 (crate (name "dimensioned") (vers "0.8.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (optional #t) (kind 0)) (crate-dep (name "auto-args") (req "^0.2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clapme") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.5") (kind 0)) (crate-dep (name "quickcheck") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "15s3j4ry943xqlac63bp81sgdk9s3yilysabzww35j9ibmnaic50") (features (quote (("test" "approx" "clapme" "serde" "serde_test" "rand") ("std") ("spec") ("oibit") ("nightly") ("default" "std"))))))

(define-public crate-dimensions-0.1 (crate (name "dimensions") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0iywwq0ifsbymy9m1b8il2km1bwak8pmgb6lq0ab0m233l29jv88")))

