(define-module (crates-io di -d) #:use-module (crates-io))

(define-public crate-di-derive-0.1 (crate (name "di-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.20") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1l9v4jll1jdgl0jrch6b5gb4jck22brcydjha4990xi9qknnp6y1") (yanked #t)))

