(define-module (crates-io di gu) #:use-module (crates-io))

(define-public crate-digu-0.0.1 (crate (name "digu") (vers "0.0.1") (hash "0p0c55j8yy32zx2152jwmm7k0hh0fv4rdhj8s9ywic3v4d55cjb0")))

(define-public crate-digu-0.0.2 (crate (name "digu") (vers "0.0.2") (hash "19g29yn7bscj0n2fmyhjdbab5qrfrw16wxym99z3gzhh219sx9gc")))

(define-public crate-digu-0.0.3 (crate (name "digu") (vers "0.0.3") (deps (list (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "13rndyxcp5y4kwwn97bd2wimay1wpmjrbp60yqvmjif9i1q3mlf2")))

(define-public crate-digu-0.0.4 (crate (name "digu") (vers "0.0.4") (deps (list (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dsysxkzqk2g3na6lnmd0dwjkjr7psizkwfacnvf984mmv8wwd19")))

(define-public crate-digu-0.0.5 (crate (name "digu") (vers "0.0.5") (deps (list (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qlgp1d5c4n7rqdl6d96mqlcv7shzhsxq9x4ly12d0c3scp4j3c8")))

(define-public crate-digu-0.0.6 (crate (name "digu") (vers "0.0.6") (deps (list (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wy0qfjvm7la6ybgrd2ixslrk8vxsy8aind0bgqbbw6m8jpdzgi9")))

(define-public crate-digu-0.0.8 (crate (name "digu") (vers "0.0.8") (deps (list (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "dropshot") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8.8") (features (quote ("uuid"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lgaz0jnlc94j38n25j5m39364zi88g3sjydxrkfihhdj7p3yfyd")))

