(define-module (crates-io di ce) #:use-module (crates-io))

(define-public crate-dice-0.1 (crate (name "dice") (vers "0.1.0") (hash "1mbzh3vzwbb3931frh17l43q2cb38p27i8rh9ks3ggp53fzdpwmp")))

(define-public crate-dice-bag-0.1 (crate (name "dice-bag") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)))) (hash "0wfrshq4r92yk6vwp8k2nq111bbvmgqjaj1q4bklz6badg9xmbmm")))

(define-public crate-dice-bag-0.1 (crate (name "dice-bag") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)))) (hash "1sncs6gaw94c1hd05wyj527lq2hrsw2mkwg6h1x5zymr37d9jfbj")))

(define-public crate-dice-bag-0.2 (crate (name "dice-bag") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)))) (hash "0n5z3lx2sib9m8x6qpc05jmd2bc3bbw022ql9qc5qa587mznb1wp")))

(define-public crate-dice-command-parser-0.3 (crate (name "dice-command-parser") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qas3wvs5i5mppak8xk6zv94jjzcj4msv8m281fcayi4x5ak3pdq")))

(define-public crate-dice-command-parser-0.4 (crate (name "dice-command-parser") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sj65dfyqdmvirs7d6q6nqc492dhjbfwb88957a3hhpqlh231i9y")))

(define-public crate-dice-command-parser-0.5 (crate (name "dice-command-parser") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qf1vzf9f4vk2hx2gcqay7adgwx1aqjmzfk3s9kwrjlzl2bqlfbj")))

(define-public crate-dice-command-parser-0.5 (crate (name "dice-command-parser") (vers "0.5.1") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jdviswa0alwchk5c0by43k8g7bz6rs0vd1l1fcj3v724nl2hafy")))

(define-public crate-dice-command-parser-0.6 (crate (name "dice-command-parser") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "08gvj57g757l3x4pp7c5mrlnv5933prm0qmlcrbyrwyi9vy2m6h6")))

(define-public crate-dice-command-parser-0.7 (crate (name "dice-command-parser") (vers "0.7.0") (deps (list (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "04yspg5i51sc02r9q41cs5fd7ph8smimp411xykkj1i736r2h95p")))

(define-public crate-dice-command-parser-0.7 (crate (name "dice-command-parser") (vers "0.7.1") (deps (list (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vb9ywj57igm2cr41sd9wm6wjgnbsqrdr5jc9bl14x7z6b338lgy")))

(define-public crate-dice-command-parser-0.7 (crate (name "dice-command-parser") (vers "0.7.2") (deps (list (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "02dwcwhj1yhbzrrilk6jhcsgh6qz3nm90ali5c20biryd7q5csc4")))

(define-public crate-dice-command-parser-0.7 (crate (name "dice-command-parser") (vers "0.7.3") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0a2rgxvwx9q2xr66jcf9nnkhw9hps38qd9wcgw0w415wnj2dcw8h")))

(define-public crate-dice-command-parser-0.8 (crate (name "dice-command-parser") (vers "0.8.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "11cyi3l5km78g3f0dv7nd58rwixd2wix0ngaj9iixhys9cwnnl1z")))

(define-public crate-dice-me-0.1 (crate (name "dice-me") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "07mfgapzsfd6k1dzpls06brdhjwfbj0w8wj70jh2wgjhmrmz1fjs")))

(define-public crate-dice-me-0.1 (crate (name "dice-me") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "00y7axbv5gzh7b50nmb7wqpw2h25pwyl7vhcrf4q49c6nxx77ikd")))

(define-public crate-dice-roll-0.1 (crate (name "dice-roll") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1niz1h9a770917kz2bqd931yhy7nc87mrnapa86frm6dwjahwvyl") (features (quote (("roll" "rand") ("parser" "nom") ("logging" "log" "rand/log"))))))

(define-public crate-dice-roll-0.1 (crate (name "dice-roll") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ml7971nlp4jl0h3rv4wb7klwj87zsnnbfiv65vbx4zfgzs8hvbh") (features (quote (("roll" "rand") ("parser" "nom") ("logging" "log" "rand/log") ("default" "parser" "roll"))))))

(define-public crate-dice-roller-0.1 (crate (name "dice-roller") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.5") (default-features #t) (kind 0)))) (hash "101qa73cgyvqvy2hi0rb746w9ghx9ixq6f9ng40sp4hjmmgmp5dy")))

(define-public crate-dice-roller-0.1 (crate (name "dice-roller") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.5") (default-features #t) (kind 0)))) (hash "0d6yprf482g3bh6fmymi9kwska7lbyg7pp060fsw05cakznrpaf8")))

(define-public crate-dice-roller-0.1 (crate (name "dice-roller") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.5") (default-features #t) (kind 0)))) (hash "00bbg7ph6akznidg58jnmq8ciywm1c9xk3xky07rj7k0p3r7vips")))

(define-public crate-dice_forge-0.1 (crate (name "dice_forge") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "006vvhm9w89lpbxarhiv31aa9gbaknmfpsd5jj72gpj1312yiq4c")))

(define-public crate-dice_forge-0.1 (crate (name "dice_forge") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0892263yic37j9fs28ibdsx2xw550jg0zmhybsba55ad4785w5xx")))

(define-public crate-dice_forge-0.2 (crate (name "dice_forge") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1568lz7g908dfichjmpv1dl1ps29xzphv0basm1czvmnqqqnpf2m")))

(define-public crate-dice_forge-0.3 (crate (name "dice_forge") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0b1v79zynwsl416wrjf1savsaj3y3nzgj1rjhbjnzms1ach3knz5")))

(define-public crate-dicecloud_sheet_printer-0.1 (crate (name "dicecloud_sheet_printer") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "genpdf") (req "^0.2.0") (features (quote ("images" "hyphenation"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "owned_chars") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("hyphenation"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "157pysykyc6m2vd8ka3jgd0ascrczwkgim0hsrslvqhrp4ii580g")))

(define-public crate-dicegen-0.1 (crate (name "dicegen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "17skhc0l151zacnnr7rsjciw2g18hp7ky9axl2dr2bqgfa593mm6")))

(define-public crate-dicenotation-0.1 (crate (name "dicenotation") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "num-iter") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dj77xcjyalbbj2pq0qkng11g7721q3h51rip9m5g1piz291krfq")))

(define-public crate-dicenotation-0.2 (crate (name "dicenotation") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "num-iter") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z34x1jjripjil58pjm6dwj1ijylxi30dixlp9pjxfcazwm4fijj")))

(define-public crate-diceprop-0.1 (crate (name "diceprop") (vers "0.1.0") (deps (list (crate-dep (name "dicetest") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "12hcxbcr6x8fq0wdj01im1kl7ang69z0hs9n56nmrb62517pmp4q")))

(define-public crate-diceprop-0.2 (crate (name "diceprop") (vers "0.2.0") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "dicetest") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ajncv8j5d1y0gh945vp9rgchlckjavhfbv51ybzi4f3qf0d0hd4")))

(define-public crate-dicer-0.1 (crate (name "dicer") (vers "0.1.0") (hash "1lypmab2njnaxgcgynkalf6ivbwc41mqs28dsfh55lbkfliqz0ss")))

(define-public crate-dicer-0.2 (crate (name "dicer") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)))) (hash "1s6hj5zm3a418064f91wb854fp6xnn1rp532x4ws0196nragvi8m")))

(define-public crate-dicer-0.2 (crate (name "dicer") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)))) (hash "1hji96sv6mqd056jlhf74cjrgn0j34p537c3w6kqlasxkskldyzr")))

(define-public crate-dicer-0.2 (crate (name "dicer") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)))) (hash "1afla4hg8fj27d0anyi3vqchkrlmg87k939l3aq1w3zg8an1wka8")))

(define-public crate-dicer-1 (crate (name "dicer") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)))) (hash "1qv3rc6cvajlxvmhmd6dr2yj8i7z50sfli7gcmjvjl4nwsk03kci")))

(define-public crate-dicer-1 (crate (name "dicer") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)))) (hash "0kksw7xwxn21pwvc58gy065ymhw084yrdf7wzg1v61i8xzibwmxz")))

(define-public crate-dicer-1 (crate (name "dicer") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)))) (hash "1dz48lnv99ddg094grnsd71w5cq1jpgjmlpwa1rfxkhc08xz0j8l")))

(define-public crate-diceroll-0.1 (crate (name "diceroll") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "120k6372nqddh7jfnzaiak6fpvsfzjxk9ilwqarwbm547zk9p3wb")))

(define-public crate-dicerolling-0.1 (crate (name "dicerolling") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0hcl93i08hqfgwll16m4r3qmr91x6xkqzajr1517rcsm7z6q7bpd")))

(define-public crate-dicers-0.2 (crate (name "dicers") (vers "0.2.0") (deps (list (crate-dep (name "lambda_runtime") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)))) (hash "0kzclb45djajf5ah9wjdvg4fxb6c3408s1nk7cg23b5wpkqixjs3") (features (quote (("lambda" "lambda_runtime" "serde" "serde_derive") ("default"))))))

(define-public crate-dicers-0.2 (crate (name "dicers") (vers "0.2.1") (deps (list (crate-dep (name "lambda_runtime") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)))) (hash "081fqg6ff6y4lny7vc1c0mig5qw7fh0k7n17kw7bilgfp7457w4v") (features (quote (("lambda" "lambda_runtime" "serde" "serde_derive") ("default"))))))

(define-public crate-dices-0.1 (crate (name "dices") (vers "0.1.0") (deps (list (crate-dep (name "fraction") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "03pjfwgwsa2fk975d5m8qgciz5mc9l1z8211pqwp7lzwp2mwd3l1")))

(define-public crate-dices-0.2 (crate (name "dices") (vers "0.2.0") (deps (list (crate-dep (name "fraction") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "19bcbwp76cfv135040gg6llwk5vxv04sjjycr5bx5p6bp3rmp8kn")))

(define-public crate-dices-0.3 (crate (name "dices") (vers "0.3.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fraction") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rounded-div") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde-wasm-bindgen") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.63") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("Window" "Performance"))) (optional #t) (default-features #t) (kind 0)))) (hash "1i1xadmb0s4mhsx5wmxmzgrcga4i8scdzgmawzmyly6gm2xjs7bp") (features (quote (("default" "nowasm")))) (v 2) (features2 (quote (("wasm" "dep:wasm-bindgen" "dep:serde-wasm-bindgen" "dep:serde" "dep:web-sys") ("nowasm" "dep:rand") ("console_error_panic_hook" "dep:console_error_panic_hook"))))))

(define-public crate-dicetest-0.1 (crate (name "dicetest") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1wf9pv4imaiad1ii4qn56dmqpwmqw56jsw7p16pwchxanq9kcb41") (features (quote (("stats") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.2 (crate (name "dicetest") (vers "0.2.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0) (package "rand")) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0alx9rviijm7b18qmjagxhgsy0h1jgmcxvpmw85avv551chq39i2") (features (quote (("stats") ("rand_full" "rand_core" "rand") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.2 (crate (name "dicetest") (vers "0.2.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "02hqhqdqmphcyxr66cbfa7h8jinz5snhj28xcaiw00v8256hr2w3") (features (quote (("stats") ("rand_full" "rand_core" "rand") ("quickcheck_full" "rand_core" "quickcheck") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.3 (crate (name "dicetest") (vers "0.3.0") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0n7w08w4p21a5zqfg10za9mv6pmmk4sl9k7qvgrvsx8bs5wba37x") (features (quote (("stats") ("rand_full" "rand_core" "rand") ("quickcheck_full" "rand_core" "quickcheck") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest-0.3 (crate (name "dicetest") (vers "0.3.1") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1zdg1l4h5brdpbbbq3mnl4gxabrjzh26iajvmcyllk7jqvn1cdy9") (features (quote (("stats") ("rand_full" "rand_core" "rand") ("quickcheck_full" "rand_core" "quickcheck") ("hints") ("default" "hints" "stats"))))))

(define-public crate-dicetest_examples-0.1 (crate (name "dicetest_examples") (vers "0.1.0") (deps (list (crate-dep (name "dicetest") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ni5g2178pka8r5kxh5y70r6280grk4yry1h2knkjb5rlc7r885i") (yanked #t)))

(define-public crate-dicetest_examples-0.2 (crate (name "dicetest_examples") (vers "0.2.0") (hash "0l3qaqacjna9wrpql3fzrb462jx840wsilwsp9s5rgzcqah3aaj9")))

(define-public crate-diceware-0.1 (crate (name "diceware") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1gsk0x9wn2l58f2blimr693k9v556iid6w0kqlbzb4ah7f82mbj0")))

(define-public crate-diceware-0.2 (crate (name "diceware") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jj7xc3f0q1mk4pw41ryscyk6qgxpyj3aar0gaw39nbf4bwvnz1w")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0n4llnzl8c3ryhjy6ghiyyip696mfbrp9g6g9489dpf2im3lpbsn")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hdw1m82n5rkz3hv9g9kzfb7g445sx0bdfmqa58x9wp8c5698d3a")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "04sfmhvlnmw893782lmp8fdvllkkfqj603mi3s1bxnbzdmgx74vz")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1jhj0x21kk0gs02vjwkd0dryx9qgxvva6843r2w97v3jzsv2iayf")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jdka3vxd4wgb8cv8mv6zhzbwwdjsmvgzpylp0w1bi4sh7k8hhb9")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.5") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0n1y7jhqr9hbssyzzpb6sjxpiq4qw0ixm40575ngl90ryp4yabcn")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.6") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0yl4jd7qyrlaj53953dw67gqixj65654nl4wmzfi00i44n7zn23y")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.7") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1mqv00rx2iw1yl93srpxjvl01xzwnl7adk354dkvpx0zab16d6x5")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.8") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "025i9ljdsh04isna6k8diz3jsfr8hvic3zx06r4hvbvc20vl8hvf")))

(define-public crate-diceware-0.3 (crate (name "diceware") (vers "0.3.9") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "09br2kl4kvi28n3wij0jr5z4q0san0859557swdphqlf7nawbfv7")))

(define-public crate-diceware-0.4 (crate (name "diceware") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "04pi4yj0pg3l9qwm0gyij6jsywgr25fhjj4vi0gazx0jkzs8wb57")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0m08wy6zbqln8nvbd62hjkvgxz78bk64bsaanfy4nsl3c6iv7f6w")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0gqf0h5cnsfnm0hjhgn2pffvdwifpzqal6hk7yyfi4w9h7lli03s")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1m77ka2rlf9m84bh21hqnsbikixl7nwl8y7j6faj0jkbf234adzw")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "12z1678x81n33ifbpx4kwl04s8wkwjnydpp54h3l01nyarrv90qw")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.4") (deps (list (crate-dep (name "diceware_wordlists") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "081jcdswc6xn242j1jqbnnq26rz4z4rxq46zmfg0l4z609z51asc")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.5") (deps (list (crate-dep (name "diceware_wordlists") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "14yqr1vlv92y2nf9i6mfa2qddwf8cwxzjd6xzlnkrivapdpz37pz")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.6") (deps (list (crate-dep (name "diceware_wordlists") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0v3w7v2hfg330rgp6xs1ks6qyx2g7kzna89f49jb9j1r5snxx19a")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.7") (deps (list (crate-dep (name "diceware_wordlists") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1db424w6f64q07r6vn773bbhjn38bhs6g52myb1q25ym7ascwcak")))

(define-public crate-diceware-0.5 (crate (name "diceware") (vers "0.5.8") (deps (list (crate-dep (name "diceware_wordlists") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 0)))) (hash "082l4a444qq9scmnc25kqp11v02gv3yms1q8ysrhsirb04fvjy6y")))

(define-public crate-diceware-gen-0.1 (crate (name "diceware-gen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1srgl4s26qmxijaad5xpc66klrn0ad21v5ms3aiahnqkjqv6xgdm")))

(define-public crate-diceware-gen-0.2 (crate (name "diceware-gen") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1i46ff1wp2fsp5sjklg0xidbs1jg4a0bwh2wjibsj089hxzykh0v")))

(define-public crate-diceware-gen-0.2 (crate (name "diceware-gen") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "055qzpxv1hz030gyik48pmrf1x4svq0jdr8cl6m8ypwz12g7w0sz")))

(define-public crate-diceware-gen-0.2 (crate (name "diceware-gen") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "00xbsmq4wjgzl5mf04p6bqy25pxh46kb7527jzxn0m37z970727f")))

(define-public crate-diceware-rs-0.1 (crate (name "diceware-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0wh2xxfa73vhxdsi45mgm18ivbhwqg8vqsmm0rwkd7nm3fbwrnfj")))

(define-public crate-diceware-rs-0.1 (crate (name "diceware-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1354xwf6n35qbfx8slqdf8ipply61kb4ni9nqp7bhkr3b2v1rswl")))

(define-public crate-diceware_wordlists-0.5 (crate (name "diceware_wordlists") (vers "0.5.4") (hash "1ym21vd9iwbsvqkdqiv5r212in9yz69jbygnfw99i54qgk4qp46h")))

(define-public crate-diceware_wordlists-0.5 (crate (name "diceware_wordlists") (vers "0.5.5") (hash "1i12ihv0xihjgprmbh5dh8pzxc157pvv6wi65dklh0z50mxrzrjz")))

(define-public crate-diceware_wordlists-0.5 (crate (name "diceware_wordlists") (vers "0.5.6") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0ssl1shsr4v037zsmz7mc4k5gzcrz94hbycfdk4lv4xxlr0fiw5v") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1 (crate (name "diceware_wordlists") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1kh52ddpn47zsxnbxj05lpggd7i6x19h2qwvacp8lr6rc3bpi0gy") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1 (crate (name "diceware_wordlists") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1ndkx2ymz4xsk2shkpi3ajyhs41j7b4vj6wr75sgpjixdi5108vr") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1 (crate (name "diceware_wordlists") (vers "1.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0vxmmqfcv7kzah8b299m8gvvy1ndb18a9qc52hng0dlpqpx2p3qb") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-diceware_wordlists-1 (crate (name "diceware_wordlists") (vers "1.2.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1sx1lbw5z0bvm9d1z0mkpz5d0k7b6f2wwga2il78jfnk7j2f6sw2") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-dicexp-1 (crate (name "dicexp") (vers "1.0.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 2)))) (hash "1rsg8nj6573wm3shwig91gfcfaqs1bw2ml0w0qwwk19s5nm9383v") (features (quote (("serde_support" "serde" "serde_derive") ("app" "clap"))))))

(define-public crate-dicexp-1 (crate (name "dicexp") (vers "1.0.1") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 2)))) (hash "1glx2j39zi3lymy7pa9n9idhxy58x2yhypww2iq67h8p92h0gcfj") (features (quote (("serde_support" "serde" "serde_derive") ("app" "clap"))))))

(define-public crate-dicexp-1 (crate (name "dicexp") (vers "1.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 2)))) (hash "1w9misfmvlazfnm04sig3fciizvv2ngljazbnq3vz9bjxi0jvsdk") (features (quote (("serde_support" "serde" "serde_derive") ("app" "clap"))))))

(define-public crate-dicexp-1 (crate (name "dicexp") (vers "1.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 2)))) (hash "099jkyvzgx82vxxa5mjzd8ws9j89c8c3syb6c550a6psgiqn6iys") (features (quote (("serde_support" "serde" "serde_derive") ("app" "clap"))))))

(define-public crate-dicey-1 (crate (name "dicey") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1ncbkn76nazbvwar5yyqgvqf965l7dmyrna8alsghb2ga0xkvd1a")))

(define-public crate-dicey-1 (crate (name "dicey") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1qf7sb8gc2d4gd83gmw65g5r5j00j0hp5p8533g5gbhmj5wd4mgx")))

(define-public crate-dicey-1 (crate (name "dicey") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0f3yz1qqql7gk5sl1w6q5dlfyqzj9cc84y8rvyn03i7hqzlf3xbb")))

