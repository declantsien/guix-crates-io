(define-module (crates-io di s_) #:use-module (crates-io))

(define-public crate-dis_lib-0.1 (crate (name "dis_lib") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "dis_rs_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0ncrrwmzb17zn0xi4pz0nkcprsida1sfj1x07iv95iwd9120iyzf") (features (quote (("default"))))))

(define-public crate-dis_rs_macros-0.1 (crate (name "dis_rs_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h78yy081h1zd56g8q4m02idmkfvxmzzyj3dyybkf5d76s5mphvr")))

