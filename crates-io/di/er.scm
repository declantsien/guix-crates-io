(define-module (crates-io di er) #:use-module (crates-io))

(define-public crate-dierckx-sys-0.1 (crate (name "dierckx-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dklhm2fv0b722zkff00zaa95wq9i9hibps03ksn2kqp2w29lq22")))

