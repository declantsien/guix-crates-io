(define-module (crates-io di e_) #:use-module (crates-io))

(define-public crate-die_parser-1 (crate (name "die_parser") (vers "1.0.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1axdm66if9w9f4rh8sg7h3jkqkvx0q4bicwhp7aq2f2y5l2a8iqj")))

(define-public crate-die_parser-1 (crate (name "die_parser") (vers "1.0.1") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "02h4r9px20dxln7j8i8jizn9j98gndg4m0y896qv3yijc5ypvksq")))

