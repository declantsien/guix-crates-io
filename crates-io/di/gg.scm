(define-module (crates-io di gg) #:use-module (crates-io))

(define-public crate-digger-0.1 (crate (name "digger") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "1wpwanrlsf6gzbdhzgq32hdsph1fr8wdcxmq2f35ki7q80lhbl50")))

