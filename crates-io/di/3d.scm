(define-module (crates-io di #{3d}#) #:use-module (crates-io))

(define-public crate-di3d-0.0.1 (crate (name "di3d") (vers "0.0.1") (hash "1kismdls7jfwkjzqd1zprzqhv11m23lfmjhwlkjwm4c4nx22aija")))

(define-public crate-di3d-test-0.0.1 (crate (name "di3d-test") (vers "0.0.1") (hash "1qwdppvr900q8wdc0br0ddnbb6578k7liks9dsig57ifldzp2592")))

