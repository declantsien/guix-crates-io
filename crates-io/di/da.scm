(define-module (crates-io di da) #:use-module (crates-io))

(define-public crate-didaskalia-0.0.0 (crate (name "didaskalia") (vers "0.0.0") (hash "112bs7ifiaf3gv6q10jki5bkaslz0q56nx9k3zr1gbazrb55kzks")))

(define-public crate-didaskalia-0.1 (crate (name "didaskalia") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 2)))) (hash "1raskxjhdrl87zincp4bvi0k0gac5li705mlkgmb4bfp2db0hg54")))

(define-public crate-didaskalia-0.1 (crate (name "didaskalia") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 2)))) (hash "1510vpvk9ipzh5dm55bp8ggk2hrf2b0ik7zzhy89c3fmsb1jp4sc")))

(define-public crate-didaskalia-0.1 (crate (name "didaskalia") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 2)))) (hash "0lj5hzlfb7ni0ij0zdf7wj7iij49x9prbq8w5x2yxh6iz9v3w412")))

