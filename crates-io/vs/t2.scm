(define-module (crates-io vs t2) #:use-module (crates-io))

(define-public crate-vst2-0.0.1 (crate (name "vst2") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "12k21px6kyqa9hf2146w94ji6l6qsnj7i1jgs4yk2449nl7zvdp6")))

(define-public crate-vst2-sys-0.1 (crate (name "vst2-sys") (vers "0.1.0") (hash "12yyf4zmk33kn26iir76ybkarn799w7idahs9jpn4143rs13fwpd")))

(define-public crate-vst2-sys-0.1 (crate (name "vst2-sys") (vers "0.1.1") (hash "0z8cdp8k8iz8l0kxl3158dpcf4lhi2wspy6ligfhfivsc6pmzank")))

(define-public crate-vst2-sys-0.2 (crate (name "vst2-sys") (vers "0.2.0") (hash "1nnss4jdg9n0pibd3knarwyv5fw58a4g31czsbnzyvvi6mwgs7zh")))

