(define-module (crates-io vs ta) #:use-module (crates-io))

(define-public crate-vstatus-0.0.1 (crate (name "vstatus") (vers "0.0.1") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "09da1m3ajywdgxsy7kf6v9r3803bkrqsrjp4xx5liry2hihd2437")))

