(define-module (crates-io vs or) #:use-module (crates-io))

(define-public crate-vsor-0.0.1 (crate (name "vsor") (vers "0.0.1") (hash "0ba57nk3gn3z5mcn3v6wnch1qiknjcijagz9idkai6qc9y6kfy40")))

(define-public crate-vsort-0.1 (crate (name "vsort") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "05nxznxypg4yyg3799xp0a6q9s6rbc43rn7lfhsab6dm1zksibw4")))

(define-public crate-vsort-0.2 (crate (name "vsort") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1y7b0x18a2mpn7hk3qfybl2ksy08klwl3bps4yxsd7pr90dilcqi")))

