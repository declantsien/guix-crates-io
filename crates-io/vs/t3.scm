(define-module (crates-io vs t3) #:use-module (crates-io))

(define-public crate-vst3-0.1 (crate (name "vst3") (vers "0.1.0") (deps (list (crate-dep (name "com-scrape-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vst3-bindgen") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "15n0f016sbn2m0il5j92pp7927cbjvs9fsclpc56ahbnxycygj5j")))

(define-public crate-vst3-0.1 (crate (name "vst3") (vers "0.1.1") (deps (list (crate-dep (name "com-scrape-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vst3-bindgen") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "1jd8jh54sxr5wi70acngr2m9ds6cvsdck0034xalxs85kwsxxix5")))

(define-public crate-vst3-0.1 (crate (name "vst3") (vers "0.1.2") (deps (list (crate-dep (name "com-scrape-types") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "vst3-bindgen") (req "^0.2.2") (default-features #t) (kind 1)))) (hash "1rq1hjcx6zrla921159r402xxhbsmgawrmm0gp1ikxshcf9n71sz")))

(define-public crate-vst3-bindgen-0.1 (crate (name "vst3-bindgen") (vers "0.1.0") (deps (list (crate-dep (name "com-scrape") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "com-scrape-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1h1jp01hvdzarxngj5kc4118w5n6fq9qxdzfr0hr9rhi0sq3l7gl")))

(define-public crate-vst3-bindgen-0.1 (crate (name "vst3-bindgen") (vers "0.1.1") (deps (list (crate-dep (name "com-scrape") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "com-scrape-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1c3qrm67n8dr0y7dllqldaslkxb63fglfszbqv5n34206jpnqlja")))

(define-public crate-vst3-bindgen-0.2 (crate (name "vst3-bindgen") (vers "0.2.0") (deps (list (crate-dep (name "com-scrape") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0wm4mw2miqxg3ficah3cil09x92kw88f8g9ci15qxwwiwqjcxq4p")))

(define-public crate-vst3-bindgen-0.2 (crate (name "vst3-bindgen") (vers "0.2.1") (deps (list (crate-dep (name "com-scrape") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1cbpikk6nnyn1cb7yiidjaza935bfv0c000w0cpd7nvqqdyffipg")))

(define-public crate-vst3-bindgen-0.2 (crate (name "vst3-bindgen") (vers "0.2.2") (deps (list (crate-dep (name "com-scrape") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1rpf56k1gwh25x0gaxp3aad6lxpafhgppsnmvhfhlb8b9clc3hip")))

