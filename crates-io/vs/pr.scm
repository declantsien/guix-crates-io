(define-module (crates-io vs pr) #:use-module (crates-io))

(define-public crate-vsprintf-1 (crate (name "vsprintf") (vers "1.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r9nicq1aikiq4kbhpjwvaax0j3xjicfanh51n0cbrgsf38b6kbq")))

(define-public crate-vsprintf-1 (crate (name "vsprintf") (vers "1.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yn0nh8wd128cv1z7i37hfdc4zmziwrdjg2az3dvflshdfip4fby")))

(define-public crate-vsprintf-1 (crate (name "vsprintf") (vers "1.0.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ai7g87zjf4vylb0jakr1nn7c7zn4mqs0gzkyv8vxc57gp7k73bg")))

(define-public crate-vsprintf-2 (crate (name "vsprintf") (vers "2.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qsx4qq955l9inf186rb3kl5l78xly6pwkvbfya341nafldzihmf")))

