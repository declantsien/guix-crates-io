(define-module (crates-io vs qx) #:use-module (crates-io))

(define-public crate-vsqx-0.1 (crate (name "vsqx") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.18.1") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.6") (features (quote ("deflate"))) (kind 0)))) (hash "0m5p7l41rdadc5wnr8wyjpryrl669r4f1pc4f5lqpm8wvgcsfsnn")))

