(define-module (crates-io vs op) #:use-module (crates-io))

(define-public crate-vsop87-0.1 (crate (name "vsop87") (vers "0.1.0") (hash "0qbi275vi64wgclxdxmrn9xblwpipja71w5b9pzmrsms8hqlhh40")))

(define-public crate-vsop87-0.1 (crate (name "vsop87") (vers "0.1.2") (hash "1a0z92a6r85vskf5s6nwdfqnp9vgyx4hmf4sxyrc17g30lqb7gpa")))

(define-public crate-vsop87-0.2 (crate (name "vsop87") (vers "0.2.0") (hash "1jk2rgs030y8brg0gmgw87n8zgwrb4x7grp2frh7ai0f2b5j1d20")))

(define-public crate-vsop87-0.3 (crate (name "vsop87") (vers "0.3.0") (hash "0mw9jxiqff71iy217x5f9ivkhy72a8q49cvlx3hnlaxgc367sg28")))

(define-public crate-vsop87-0.4 (crate (name "vsop87") (vers "0.4.0") (hash "05q2c0gz7py9vf61p9c31z7f8il8r168r3n61v7g5a7mwli3m13h")))

(define-public crate-vsop87-0.5 (crate (name "vsop87") (vers "0.5.1") (hash "0q8hbk2c2xq76k83yy41cyqgas745a92g8yd7masq9abirj44j57")))

(define-public crate-vsop87-1 (crate (name "vsop87") (vers "1.0.0") (hash "0d56174fcmyb1wfh6kfsm3l7sp8vmp75raw9p0jp052092ai9qfz")))

(define-public crate-vsop87-1 (crate (name "vsop87") (vers "1.0.1") (hash "082mwghzq983x2f16lj6z8lpc829dzsq3y4ch040s1z1akr83jck")))

(define-public crate-vsop87-1 (crate (name "vsop87") (vers "1.0.2") (hash "0add6bqi2y8jcij0r79g9pbkh5qbb7gjlz0l8i3nyhss1wiyrxxz")))

(define-public crate-vsop87-1 (crate (name "vsop87") (vers "1.0.3") (hash "15nakmjrazwzs13gz6aj5n312w4qgi26k3y5hjrvjfs97c522gp5")))

(define-public crate-vsop87-1 (crate (name "vsop87") (vers "1.0.4") (hash "04wmjcvzwlpvqkrj05a0xz2mg3p99fr2y88jmkwvqjl0syzdkl6l")))

(define-public crate-vsop87-2 (crate (name "vsop87") (vers "2.0.0") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "1jyivhgzlq58h784qk722p315n6l0xy3i1l8psixc4c7w0a3jrz0")))

(define-public crate-vsop87-2 (crate (name "vsop87") (vers "2.0.1") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "1haxnzvhr7yvxwy3zhk3r38vf42iq01ycpyi5l54bk8v5z1nb6x3")))

(define-public crate-vsop87-2 (crate (name "vsop87") (vers "2.0.2") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "1ixjh39bc0xzzmrpd8swz4kfvl7n9y8f4p2hsc0bajn7fns2n0n6")))

(define-public crate-vsop87-2 (crate (name "vsop87") (vers "2.0.3") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "1jyz3qpqc4baknd216cw0x4dskvga9jbbjk7jhnfmpfrspx324fz")))

(define-public crate-vsop87-2 (crate (name "vsop87") (vers "2.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1yrg41hcxydywa7dnp7ckm9190cnl2h92ym9z0x75w8v08r3whr3") (features (quote (("simd") ("no_std" "libm") ("default" "simd"))))))

(define-public crate-vsop87-3 (crate (name "vsop87") (vers "3.0.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0xamfyg5rybq1ck0jszaaly6jqxhbmlai9j858x62ff63xzy9k9p") (features (quote (("simd") ("no_std" "libm") ("default" "simd")))) (rust-version "1.70")))

