(define-module (crates-io vs im) #:use-module (crates-io))

(define-public crate-vsimd-0.0.1 (crate (name "vsimd") (vers "0.0.1") (hash "0xrd4azbp3i0abw8zvj7iq1j6hzvfa734rr46mf4a1km1fqf3r9c") (features (quote (("unstable") ("std" "alloc") ("detect" "std") ("alloc")))) (yanked #t) (rust-version "1.63")))

(define-public crate-vsimd-0.8 (crate (name "vsimd") (vers "0.8.0") (deps (list (crate-dep (name "const-str") (req "^0.5.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.8") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.33") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0r4wn54jxb12r0x023r5yxcrqk785akmbddqkcafz9fm03584c2w") (features (quote (("unstable") ("std" "alloc") ("detect" "std") ("alloc")))) (rust-version "1.63")))

