(define-module (crates-io h4 x_) #:use-module (crates-io))

(define-public crate-h4x_core-0.0.1 (crate (name "h4x_core") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "minwinbase" "winerror" "fileapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gl6iz62gwl76hfmqh3q5jfx9aza32crpn9r32bxkbdjza39hnky")))

(define-public crate-h4x_core-0.0.2 (crate (name "h4x_core") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "minwinbase" "winerror" "fileapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0b581my128p7kjhaqi9way6vk1gffq4vvivpwb31jpih1i9rqvis")))

(define-public crate-h4x_re-0.1 (crate (name "h4x_re") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "0z55kzd50pz55rkg6cxi2d4q505x5hr7s9i7xywihdcbzd2sn699")))

(define-public crate-h4x_re-0.2 (crate (name "h4x_re") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "1xdhm1r7glmwvj3f32jzfrx2afi59syy9z5n5hhbvqjm710hzqcp")))

(define-public crate-h4x_re-0.2 (crate (name "h4x_re") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "1j4q2cllv9kmhnw9mff3n4xmry8rhilv24b281qc23ziqjnh2qsh")))

(define-public crate-h4x_re-0.2 (crate (name "h4x_re") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "1x0zfwdvrm4xd2mmc70f357x4a5bz10mrhqnz202knnisr3jj2sl")))

(define-public crate-h4x_re-0.2 (crate (name "h4x_re") (vers "0.2.3") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "0lf654lx6wjg92l5jk04187fz78ljr7jnimg2fwpl94jgdawzpf2")))

(define-public crate-h4x_re-0.2 (crate (name "h4x_re") (vers "0.2.4") (hash "1cxlslhnzdinyhp80jkvf3sx554dsz6rg4gg13c226g1gd8kgym9")))

