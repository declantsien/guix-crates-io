(define-module (crates-io du al) #:use-module (crates-io))

(define-public crate-dual-0.2 (crate (name "dual") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "19z4naljs8xfhq0hzbimackrh43m2sz0cnjhmh01dz41fh8gx1rf")))

(define-public crate-dual-airdrop-0.0.1 (crate (name "dual-airdrop") (vers "0.0.1") (deps (list (crate-dep (name "anchor-lang") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.13.5") (default-features #t) (kind 0)))) (hash "1d7l6jlhkhv5xwh59c6pwqj7bn67z8bfw9nv6znwlvp5bx7rvk1w") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-dual-airdrop-0.0.2 (crate (name "dual-airdrop") (vers "0.0.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.13.5") (default-features #t) (kind 0)))) (hash "16ypzi9n1bw57w95hihk2bkp7bzbrkrfaz5av9fv7ymr5fa9hv2j") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-dual-airdrop-0.0.3 (crate (name "dual-airdrop") (vers "0.0.3") (deps (list (crate-dep (name "anchor-lang") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.13.5") (default-features #t) (kind 0)))) (hash "16sj7q7hsvhaaihyy06pc0krrrj7krlwc8444p6sdyczmm3022li") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-dual-shock4-controller-0.1 (crate (name "dual-shock4-controller") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "14yxrbdw9xhwn1n73adwzkvq6y5fjbqfhix2nlj0cb8danrgwafh")))

(define-public crate-dual-shock4-controller-0.1 (crate (name "dual-shock4-controller") (vers "0.1.1") (deps (list (crate-dep (name "hidapi") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0fb9avblsa47kd0y1dwdmr3nxk88gsqz4fij7xl52mws8qzk9xqs")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a1") (hash "1jw60vzbclj9a0cwb16fk0d6i53bpi0wb1yww3fxywb74fkhyi7z")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a2") (hash "1l8slw5ap08v4107i88fyq0gkjic0mdx3anz132prwb1i4jgrbfx")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a3") (hash "0h4hb4bjyxdbi4q0287njb6g6qmcm03q116ha4cpwrhbzri3i3cw")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a4") (hash "08lmyfydjhr6bg8ra5y3s040lg1lrh8naj930yfv9qinb3zrs8zh")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a5") (hash "0z0prcbrsb2d5ygji686r6zvq55hsfigdyijhshjbans6cfk8ny8")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a6") (hash "0wybsvk7fq65ydzl0rih2ny340x8mybyp7zacgqyaxmhg95iy09l")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a7") (hash "1j0xv6hzx66hw263p0hcz21mcxzmjf8bdzviz1g0c2vc7qzzlhs6")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0-a8") (hash "11yid1pvbcs7k8s7lj3baiw9xvpmjx3vjp81id0839yqa5d1dzw7")))

(define-public crate-dual_balanced_ternary-0.1 (crate (name "dual_balanced_ternary") (vers "0.1.0") (hash "1y0qmhabzna6x76rf178w3j5zrhv89g0nqa4p8k6adkmjd4bg4a0")))

(define-public crate-dual_num-0.1 (crate (name "dual_num") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1ihda47k8rskmgiandzbaxafydsif35jhcs1h30x71232kmgw4lf")))

(define-public crate-dual_num-0.1 (crate (name "dual_num") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1nvw1x15dy5m0jncw1g0wg2fnsh8yczpkplb6yiaiv8xqp47c62f")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "078zm3nmlb52q3prf17mlqjdv5ppwy0n0gqa6dq5qfd6gpqid0rq")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.1") (deps (list (crate-dep (name "nalgebra") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "09rd8w9j4g9khh0awrahmvzai5369fzypgcfqij9agdgvlnywhf4")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.2") (deps (list (crate-dep (name "nalgebra") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "01ji4p7zm8ppccq2sp5jykhlw5y7rliq5k20q3dl5alnd8w6fj14")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.3") (deps (list (crate-dep (name "nalgebra") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qkjlnrdjphf1fvxzzq8j03zsfn8yppl2ai6awj892019mappr02")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.4") (deps (list (crate-dep (name "nalgebra") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "15233wiy5c4zvbxglk0syl8z9d0hgi9dv3pihjln36kjjawjkgym")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.5") (deps (list (crate-dep (name "nalgebra") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vq6yq36vvyz4gy9fjmcrv054bicqxiifav2g5bnlh7js4sndp9n")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.6") (deps (list (crate-dep (name "nalgebra") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0a60szrxfs07b0vfjl75iys6rv5nc1aw2z0ypgr8f4g3j20gi5ww")))

(define-public crate-dual_num-0.2 (crate (name "dual_num") (vers "0.2.7") (deps (list (crate-dep (name "nalgebra") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ildxx060r6p2lyghm3cp3z21klbs2h5dba27iyz2cp1nxq41vzs")))

(define-public crate-dual_quaternion-0.1 (crate (name "dual_quaternion") (vers "0.1.0") (deps (list (crate-dep (name "quaternion") (req "*") (default-features #t) (kind 0)) (crate-dep (name "vecmath") (req "*") (default-features #t) (kind 0)))) (hash "0xdf3ifzcf51j49i9p0jgkjvjbvj2bwig16khf9b1fv5jhysp7gq")))

(define-public crate-dual_quaternion-0.2 (crate (name "dual_quaternion") (vers "0.2.0") (deps (list (crate-dep (name "quaternion") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0p2pcp26xk6hfclf28sp5n81jzgsrrqv5dxakv31ymgyf98q2xh7")))

(define-public crate-dual_rewards_distribution_recipient_crate-0.2 (crate (name "dual_rewards_distribution_recipient_crate") (vers "0.2.0") (deps (list (crate-dep (name "casper-contract") (req "^1.4.4") (default-features #t) (kind 0)) (crate-dep (name "casper-types") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "casperlabs-contract-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "cryptoxide") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (kind 0)))) (hash "0qs98h5i3xayqzai6iwrbx6hamq1mgpl469g6wqgpavgbxhmk8hl")))

(define-public crate-dualhashkey-0.1 (crate (name "dualhashkey") (vers "0.1.0") (deps (list (crate-dep (name "const-fnv1a-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "19y98kqa14ayr6yj0mwkrd4jx62r9fcqnh07k6vsn0f96ngpcylz")))

(define-public crate-dualhashkey-0.1 (crate (name "dualhashkey") (vers "0.1.1") (deps (list (crate-dep (name "const-fnv1a-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0v9y8g50ppi8226d7hl1k00lmr1kv7hw5kbl78mbx0kcdp2j7id1")))

(define-public crate-duality-0.0.0 (crate (name "duality") (vers "0.0.0") (hash "0z72lkywmv68pwxm6qjlcp21zss5lr0c86h3iwyfg4hn9i8f8902")))

(define-public crate-dualnum-0.1 (crate (name "dualnum") (vers "0.1.0") (deps (list (crate-dep (name "num-dual") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.13") (features (quote ("extension-module" "abi3" "abi3-py36"))) (default-features #t) (kind 0)))) (hash "04w0w7xvy55bqcis2brsv4y7y0k10fqws579h1p5x6rjb07nvrc2") (yanked #t)))

(define-public crate-dualnum-0.2 (crate (name "dualnum") (vers "0.2.0") (deps (list (crate-dep (name "num-dual") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.14") (features (quote ("extension-module" "abi3" "abi3-py36" "multiple-pymethods"))) (default-features #t) (kind 0)))) (hash "0kfxqpfl74jq3pn7y23d1s1mngyqrbgph4914dhxg7dr7rnwj4rb") (yanked #t)))

(define-public crate-dualnum-0.2 (crate (name "dualnum") (vers "0.2.1") (deps (list (crate-dep (name "num-dual") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.14") (features (quote ("extension-module" "abi3" "abi3-py36" "multiple-pymethods"))) (default-features #t) (kind 0)))) (hash "0wi4s6ghwmlaqgylqbh3ffxq9kvxp922lh3lm5400n9yjr4wl50c") (yanked #t)))

(define-public crate-dualquat-0.1 (crate (name "dualquat") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.21.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.0") (optional #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "nanoserde") (req "^0.1.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jz4ywxdm5wf3l9dpfcgpl792wz2ai65ymcyas0phi4bj5gaqlir") (features (quote (("default")))) (v 2) (features2 (quote (("speedy" "dep:speedy") ("serde" "dep:serde") ("nanoserde" "dep:nanoserde") ("nalgebra" "dep:nalgebra") ("glam" "dep:glam"))))))

(define-public crate-dualquat-0.1 (crate (name "dualquat") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.21.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.0") (optional #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "nanoserde") (req "^0.1.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "073jjvw5wvwc8lklwscm0119vpazfyxdadnlb8igc49wmcqvs1az") (features (quote (("default")))) (v 2) (features2 (quote (("speedy" "dep:speedy") ("serde" "dep:serde") ("nanoserde" "dep:nanoserde") ("nalgebra" "dep:nalgebra") ("mint" "dep:mint") ("glam" "dep:glam"))))))

(define-public crate-dualquat-0.1 (crate (name "dualquat") (vers "0.1.2") (deps (list (crate-dep (name "glam") (req "^0.21.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.0") (optional #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "nanoserde") (req "^0.1.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)) (crate-dep (name "speedy") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "0icj0c9xdkmybr00w9ksh7h06vdlhma2q48s50siny10lp37k4qb") (features (quote (("default")))) (v 2) (features2 (quote (("speedy" "dep:speedy") ("serde" "dep:serde") ("nanoserde" "dep:nanoserde") ("nalgebra" "dep:nalgebra") ("mint" "dep:mint") ("glam" "dep:glam"))))))

(define-public crate-dualquat-0.1 (crate (name "dualquat") (vers "0.1.3") (deps (list (crate-dep (name "glam") (req "^0.21.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.0") (optional #t) (kind 0)) (crate-dep (name "nanoserde") (req "^0.1.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)))) (hash "0z70k52qzrfdd1r1gn1v736qylrj3ljrnys0835warxrby0k8911") (features (quote (("default")))) (v 2) (features2 (quote (("speedy" "dep:speedy") ("serde" "dep:serde") ("nanoserde" "dep:nanoserde") ("nalgebra" "dep:nalgebra") ("mint" "dep:mint") ("glam" "dep:glam"))))))

(define-public crate-duals-0.0.1 (crate (name "duals") (vers "0.0.1") (hash "0vlw0gqdlsaxqp49gn0gh1bfzyby2idy79lxgwsivbjg0k1w8mhy")))

(define-public crate-duals-0.0.2 (crate (name "duals") (vers "0.0.2") (hash "11bi348plrxa9nh04l1fxfqf5yqzvng4gllpm7ni3mmyg29mwck5") (yanked #t)))

(define-public crate-duals-0.0.3 (crate (name "duals") (vers "0.0.3") (hash "0dv7xrvyi5j5k7gk7zbzmzfpndvm6fc44khackzpgh00gis0ibv3")))

(define-public crate-dualsense-0.1 (crate (name "dualsense") (vers "0.1.0") (hash "157h8b3m62xwdlvldiv42hsl0z6lxnmhrgxad0q8skzvj8bbd198") (yanked #t)))

(define-public crate-dualsense-rs-0.1 (crate (name "dualsense-rs") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1v1q9gklwj8v0ixn2inmf5j3ryx7lqzg59p8vkv60z7if58pjrsf")))

(define-public crate-dualsense-rs-0.2 (crate (name "dualsense-rs") (vers "0.2.0") (deps (list (crate-dep (name "hidapi") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1xlry6zg6d8av7dpyfz7avg0d1xx2rp146zwcmi3wjb0aqvr72c4")))

(define-public crate-dualsense-rs-0.3 (crate (name "dualsense-rs") (vers "0.3.0") (deps (list (crate-dep (name "hidapi") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "191bd7j1ji199aidljjlbyx6pradh9a3r2cav2iyc8pcf7x7f3mg")))

(define-public crate-dualsense-rs-0.4 (crate (name "dualsense-rs") (vers "0.4.0") (deps (list (crate-dep (name "hidapi") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1n9nd5cj7mggi3ybbn0h7p8xp926541c67fwlj9a9nfm9rl2drv9")))

(define-public crate-dualsense-rs-0.5 (crate (name "dualsense-rs") (vers "0.5.0") (deps (list (crate-dep (name "hidapi") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "14jjxpfhlxaivf4j1959ipvx308biim69rp7inrcd112ihxnwl0h")))

(define-public crate-dualsense-rs-0.6 (crate (name "dualsense-rs") (vers "0.6.0") (deps (list (crate-dep (name "hidapi") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0j7szh5ip8rdi0kfvhhiy8rv0vnazljxy48i5hy7hzga8101z07l")))

(define-public crate-dualshock3-0.1 (crate (name "dualshock3") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.2.1") (features (quote ("linux-static-hidraw"))) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stoppable_thread") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "053x579ap4yzb20qqf1daq3zgw36a7438wn64ijnkgjnwcrihz5k")))

(define-public crate-dualshock4-0.1 (crate (name "dualshock4") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.187") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0v52mzf5r8a0zhbzjdxmgsyhnvnrfp0cv64w31ifrasw0dbhki9p")))

(define-public crate-dualshock4-0.1 (crate (name "dualshock4") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.187") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1jhnchcm8kc3nx1k054jfkl20zln67b7w9wzs8minf6nhmn7i4ai")))

