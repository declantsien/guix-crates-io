(define-module (crates-io du kb) #:use-module (crates-io))

(define-public crate-dukbind-0.0.1 (crate (name "dukbind") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.47.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.28") (default-features #t) (kind 1)))) (hash "1dzwzla9ab5yl3j1h8m6v1lka749spx106y1f7b1frk3hlviq132")))

(define-public crate-dukbind-0.0.2 (crate (name "dukbind") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.47.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.28") (default-features #t) (kind 1)))) (hash "05gdhh8rfl4hkgxm43qr3ifich62p79b8k648vbi8yl00g5k2bzw")))

(define-public crate-dukbind-0.0.3 (crate (name "dukbind") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.47.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.28") (default-features #t) (kind 1)))) (hash "11gfpwj0c0fn2748dajgv4ag13851s6w0j0ra7z8zadnh0n6inhr") (yanked #t)))

(define-public crate-dukbind-0.0.4 (crate (name "dukbind") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.47.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.28") (default-features #t) (kind 1)))) (hash "0zvjy619n1alfxvvsq6nvg4741b21z42ys0y5mvd6bb0vnvx9p00")))

