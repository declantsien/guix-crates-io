(define-module (crates-io du rd) #:use-module (crates-io))

(define-public crate-durduff-0.1 (crate (name "durduff") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "atty") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)))) (hash "05qry0nlg5p849f3g5m4dw264lfd1vrxr29mdhka5qwbrf5zix97")))

