(define-module (crates-io du pf) #:use-module (crates-io))

(define-public crate-dupfind-1 (crate (name "dupfind") (vers "1.0.0") (deps (list (crate-dep (name "opener") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0wg8nq0frf069wbxra3b2ydsqda7s0va46mvmnfikpfz0mjmizsk")))

(define-public crate-dupfinder-0.1 (crate (name "dupfinder") (vers "0.1.0") (hash "01ai29dnc19l8j5p4cnhc3q62b44sdn7v5q2kyr82w0bmsyxmc0q")))

