(define-module (crates-io du cc) #:use-module (crates-io))

(define-public crate-ducc-0.1 (crate (name "ducc") (vers "0.1.0") (deps (list (crate-dep (name "cesu8") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ducc-sys") (req "^0.1") (features (quote ("use-exec-timeout-check"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z8sh683dsyji3nwm6jczq7xkwfy2aq5h4mss2dlw6ib9knw20yh")))

(define-public crate-ducc-0.1 (crate (name "ducc") (vers "0.1.1") (deps (list (crate-dep (name "cesu8") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ducc-sys") (req "^0.1.1") (features (quote ("use-exec-timeout-check"))) (default-features #t) (kind 0)))) (hash "0gg48chp4bf2fpyqsyxk6jy6w6yb5r51g0ygk4f9d39q90wjq2br")))

(define-public crate-ducc-0.1 (crate (name "ducc") (vers "0.1.2") (deps (list (crate-dep (name "cesu8") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ducc-sys") (req "^0.1.2") (features (quote ("use-exec-timeout-check"))) (default-features #t) (kind 0)))) (hash "1h9snm92rnjm8czck460fwp04xa3gfyl909kmrag7q57ashks0qw")))

(define-public crate-ducc-0.1 (crate (name "ducc") (vers "0.1.3") (deps (list (crate-dep (name "cesu8") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ducc-sys") (req "^0.1.2") (features (quote ("use-exec-timeout-check"))) (default-features #t) (kind 0)))) (hash "15l5vj4vp90n5mk20pkq1ixq2i68i8dnrqlh7zx700zxnpiiln6y")))

(define-public crate-ducc-0.1 (crate (name "ducc") (vers "0.1.4") (deps (list (crate-dep (name "cesu8") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ducc-sys") (req "^0.1.2") (features (quote ("use-exec-timeout-check"))) (default-features #t) (kind 0)))) (hash "1a1ya1578gj67b6l3d65mzfa2xz3x10qrcsn02bkgi0nl07zqyhs")))

(define-public crate-ducc-0.1 (crate (name "ducc") (vers "0.1.5") (deps (list (crate-dep (name "cesu8") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ducc-sys") (req "^0.1.2") (features (quote ("use-exec-timeout-check"))) (default-features #t) (kind 0)))) (hash "01wjhbzxl62gihx25fvks50zp7cd45zp91ag8nkvcbki6251zg21")))

(define-public crate-ducc-serde-0.1 (crate (name "ducc-serde") (vers "0.1.0") (deps (list (crate-dep (name "ducc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0582vsr6spx2njg7bsd8dqxfdp7jckdf6cgkkzpvzk7whc35z607")))

(define-public crate-ducc-sys-0.1 (crate (name "ducc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.36") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "06mk075l1qw5zgddagdswy3yjb1mb1rsw6bx24z77gq9k809060g") (features (quote (("use-exec-timeout-check") ("build-ffi-gen" "bindgen"))))))

(define-public crate-ducc-sys-0.1 (crate (name "ducc-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.36") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1rkdgdn5wc4515pg10hjrm8gwk393g1qnl8ql8x3n6m1gqhx9946") (features (quote (("use-exec-timeout-check") ("build-ffi-gen" "bindgen"))))))

(define-public crate-ducc-sys-0.1 (crate (name "ducc-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.36") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1vgzsjmylxldhzr19s0fzijh8pswd4249nvl4d9dw3vapwsaiphc") (features (quote (("use-exec-timeout-check") ("build-ffi-gen" "bindgen"))))))

(define-public crate-ducc0-0.28 (crate (name "ducc0") (vers "0.28.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1i4vi2p2dkfif5825k2zcfhq923hm6ic002cm0p55fd4najlkj8y") (yanked #t)))

(define-public crate-ducc0-0.30 (crate (name "ducc0") (vers "0.30.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndrustfft") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1387cply47gh1y74g0pshhh2ba3m5fzflrclb882spz85gqv1r1f")))

(define-public crate-ducc0-0.30 (crate (name "ducc0") (vers "0.30.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndrustfft") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1n4zsvfz9h2n8vhjcij49d58psrmvsrfags803m772ayvq1k15hb")))

(define-public crate-ducci-1 (crate (name "ducci") (vers "1.0.0") (hash "0hpv3q24bjf7qq213984l11aj70d7fzyn45xvvl5idg15jpn07r1")))

(define-public crate-ducci-1 (crate (name "ducci") (vers "1.1.0") (hash "0al3jn5p3jihz77qlf8cbmw59rz1h9bglj52p8sviizjv4pw4f9g")))

(define-public crate-ducci-1 (crate (name "ducci") (vers "1.2.0") (hash "1zg34wqwc8995k3a82q9srlvm7iynsl4l37bnkpav887f384j07c")))

