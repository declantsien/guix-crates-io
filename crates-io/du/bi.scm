(define-module (crates-io du bi) #:use-module (crates-io))

(define-public crate-dubins_path-0.0.1 (crate (name "dubins_path") (vers "0.0.1") (hash "0jiwn25dqjfwlwpcy3ccljl0phs7yaqb2x07n3llddxji0hf6dcr")))

(define-public crate-dubins_path-0.0.2 (crate (name "dubins_path") (vers "0.0.2") (deps (list (crate-dep (name "euclid") (req "^0.20.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0daxg9lig9q0kay3xqd89f81vdyl6qd13rcf6pbi6k7ngn30aawf")))

(define-public crate-dubins_paths-0.1 (crate (name "dubins_paths") (vers "0.1.0") (hash "0zpkh81fcjc9dbs48c1h6qngi1sdl8xxvzgngnnkhw39ax96mz2n")))

(define-public crate-dubins_paths-0.2 (crate (name "dubins_paths") (vers "0.2.0") (hash "13hkvw7hsmp497skqwc32yadx6gf6zqxna942mbajxr1aqdzpw20")))

(define-public crate-dubins_paths-0.2 (crate (name "dubins_paths") (vers "0.2.1") (hash "045chzm87bhsfsq1rdhk1md33afazjf7xi58glplnhaa2wk9yhg2")))

(define-public crate-dubins_paths-0.2 (crate (name "dubins_paths") (vers "0.2.2") (hash "0v64yg983s6fmqcks89d7y8jh6pr4iwx8pwg62nqn6k2jrsvzm1w")))

(define-public crate-dubins_paths-0.2 (crate (name "dubins_paths") (vers "0.2.3") (hash "0s4vqa4mzla8h0074yfyncj2nqbavzj2g4125zd3n2p0kgzxjqmr")))

(define-public crate-dubins_paths-0.2 (crate (name "dubins_paths") (vers "0.2.4") (hash "0xm009zhpfgy59nwvsdg6kwzfh39h6n4zr25sbmixvic5nc0ypsj")))

(define-public crate-dubins_paths-0.2 (crate (name "dubins_paths") (vers "0.2.5") (hash "1dcsddyzbi0629d6jbvkl61h6g8y3fpcwhi31gdi9084cwdark78")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.0.0") (hash "1z1cpd6dhgaz8nwlqja8vzyc716zwj5hl26zw9n9qwf75rixmk6y")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.0.1") (hash "1i4zbx7c0q5bylvnffslms3sqrpx5zazkdnyxi90ybfrnh8vvj43")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.0.2") (hash "16vqdabkap7vxwz6f9a44mi7mkpz79cpbihr82rc83813c01yfl1")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.0.3") (hash "01n4ld2ba4726v9n0dz65p7h9j9lk5q42r2wjb1wl9p8r9ahrhy8")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.0.4") (hash "1dwh1fih812qg8f6jrz06xh2lgr3d0xrmbambrilaypgzblpmh7a")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.1.0") (hash "0rpgxh8j74kfrfqlx684p9248nr9i207rgsmfa8f35spp2a00jla")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.2.0") (hash "0i4szlfzbh7782fz2qpxmaa8kxcpb0wljq4c3c1wii7mmkx6r43c")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.2.1") (hash "02b67k0a3mq5aplm3bvxk6pp05dfq13ilzxs1h4whivi07fhl8ya")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.2.2") (hash "0ac84h7h7lfddxxs9i32q74idmn89fns2cmmp5x5l2pd1gwnadvj")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.2.3") (hash "0jgq6c5y9m9dc6gib5vkjlwf52399q62g3in45fb5zxylzbdl75w")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.3.0") (hash "1lcj1z8qkid99ji3i299757gm9awlmzm2kidg569pfw7spxrs4cc")))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.4.0") (deps (list (crate-dep (name "glam") (req "^0.21.3") (optional #t) (default-features #t) (kind 0)))) (hash "15v4ych96x3ixvza7rhk34akx118ma00b5j2gdkmrq6j4i13wl40") (features (quote (("fast-math" "glam" "glam/fast-math"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.4.1") (deps (list (crate-dep (name "glam") (req "^0.21.3") (optional #t) (default-features #t) (kind 0)))) (hash "0zkiscb2pg9sfs1iw3zr28idrn83vknq7ygwkl9kj0a860rdyxx8") (features (quote (("fast-math" "glam" "glam/fast-math"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.4.2") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "00mj4dcaf6r7hlz5wck2mgi49ninpwpv85id6mdr5lad697dn6lz") (features (quote (("fast-math" "glam" "glam/fast-math")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.4.3") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0zvj28zqvfr6w8i3wa29n7nn2ffxnypfjq5ckfxszhzq8cqahr86") (features (quote (("fast-math" "glam" "glam/fast-math")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.4.4") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1bvlfin6h02532p36dh0n4jdg5j2s30zdwqhxm7gmrm75466ym80") (features (quote (("fast-math" "glam" "glam/fast-math")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.4.5") (deps (list (crate-dep (name "glam") (req "^0.22.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1kwkvz7l74ad4dx75zyp8hq91adc0vqisjn3dhs6w48ih1ld8pwl") (features (quote (("fast-math" "glam" "glam/fast-math")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.5.0") (deps (list (crate-dep (name "glam") (req "^0.23.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0wpmcav1jlsq73qb11aanysnvnm9r3h2a0dxshhgz0flbc7rdj4j") (features (quote (("fast-math" "glam" "glam/fast-math")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.6.0") (deps (list (crate-dep (name "glam") (req "^0.24.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0kx8hkjq2zpg235avp0rizg6j6m4lndxgcnih99dvfg8hn0fjjz2") (features (quote (("fast-math" "glam" "glam/fast-math")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-1 (crate (name "dubins_paths") (vers "1.7.0") (deps (list (crate-dep (name "glam") (req "^0.24.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1a2wkm6fi6ga12y55f72hscxjzma6zxyblsxbfwg5rbny2xps61q") (features (quote (("fast-math" "glam" "glam/fast-math")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-2 (crate (name "dubins_paths") (vers "2.0.0") (deps (list (crate-dep (name "glam") (req "^0.24.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1hqr9lr8q2nfrlyb8abjppiyjpxw60i4pckp34mb8cpcnn0d8pf7") (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-2 (crate (name "dubins_paths") (vers "2.0.1") (deps (list (crate-dep (name "glam") (req "^0.24.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1car6x4hx5k3jzi4c7cyr651fi4rdacrhljxqb0x592f8pdflxr9") (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-2 (crate (name "dubins_paths") (vers "2.1.0") (deps (list (crate-dep (name "glam") (req "^0.25.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "00bj3kjh3d29fzpc34cvnzixmzjjwyz868jz9wcjpnai4aafmzka") (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-2 (crate (name "dubins_paths") (vers "2.1.1") (deps (list (crate-dep (name "glam") (req "^0.25.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0w6qap7f1djgzbr86y48wvk49wwk60mp4q2p863j855l0fcdsx6x") (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-2 (crate (name "dubins_paths") (vers "2.2.0") (deps (list (crate-dep (name "glam") (req "^0.26.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1fd3b935lf120dwz60rjcvy3ilml5b1j91l07rlfsgv8s369v8lf") (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-dubins_paths-2 (crate (name "dubins_paths") (vers "2.3.0") (deps (list (crate-dep (name "glam") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0dfi9ljv919ml8vrm9sxsqb15zc5635xbfzmnw034c7pfwaj8ili") (v 2) (features2 (quote (("glam" "dep:glam"))))))

