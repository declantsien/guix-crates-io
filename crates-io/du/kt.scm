(define-module (crates-io du kt) #:use-module (crates-io))

(define-public crate-dukt-0.1 (crate (name "dukt") (vers "0.1.0") (deps (list (crate-dep (name "dukt-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dukt-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "075xkiffz1d5mx5awj16if02pqbk77lqvxxwl19ly0p9jid5908j")))

(define-public crate-dukt-macros-0.1 (crate (name "dukt-macros") (vers "0.1.0") (deps (list (crate-dep (name "inflections") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "019hsycdimg1pfl383flbrlj39dxz86b3hckbavs98wsi2l44f8b")))

(define-public crate-dukt-macros-0.1 (crate (name "dukt-macros") (vers "0.1.1") (deps (list (crate-dep (name "dukt") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "inflections") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c7fvff899bih5v3pbz3vbzrxdq1523yzfvixwv3qpaqg5f0bd1y")))

(define-public crate-dukt-sys-0.1 (crate (name "dukt-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "093rd6hyrnymcw9mg91a8ndvwnm38q7f24kp23i0yhp10sqawrm7")))

(define-public crate-duktape-0.0.1 (crate (name "duktape") (vers "0.0.1") (deps (list (crate-dep (name "duktape_sys") (req "*") (default-features #t) (kind 0)))) (hash "1qcrhb4ax6galsd39fb35jmqjk27q5kiq2ih4215ga3x1cnycc7s")))

(define-public crate-duktape-0.0.2 (crate (name "duktape") (vers "0.0.2") (deps (list (crate-dep (name "abort_on_panic") (req "*") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "*") (default-features #t) (kind 0)) (crate-dep (name "duktape_sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1r5cjp9x6inns5gfaq39f0xf8f78z6wj8blqx9grdllcjr45npgx")))

(define-public crate-duktape-macros-0.1 (crate (name "duktape-macros") (vers "0.1.0") (deps (list (crate-dep (name "inflections") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ai9a56495hr8f4w3kamxk1p4hvnvvl4pq7v0g8ai7s3zrqa9v62")))

(define-public crate-duktape-rs-0.0.1 (crate (name "duktape-rs") (vers "0.0.1") (deps (list (crate-dep (name "dukbind") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0dbymb5hqinhf209ikd5sgary57vm1pdrazcywlzijg8cnshs36h")))

(define-public crate-duktape-rs-0.0.2 (crate (name "duktape-rs") (vers "0.0.2") (deps (list (crate-dep (name "dukbind") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "089wkliz85mimwzg680qmj3khpirlmwwv3dnxfwd55kky29qw7gs")))

(define-public crate-duktape-rs-0.0.3 (crate (name "duktape-rs") (vers "0.0.3") (deps (list (crate-dep (name "dukbind") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0qqg0kqpwynszi2gzwjqz7k2c24ajyg568vf81h17kv2rw2pmizb")))

(define-public crate-duktape-rs-0.0.4 (crate (name "duktape-rs") (vers "0.0.4") (deps (list (crate-dep (name "dukbind") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "011ypgn58xkkzdn39z5y6wv90kvy5rbyyb9z8dn92s27rwm5gb76")))

(define-public crate-duktape_ffi-0.0.1 (crate (name "duktape_ffi") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "*") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "10gdi058lq2p4ijfn3q9x9nvk4ls2hb18yfm0a8krvyb220gknm3")))

(define-public crate-duktape_ffi_raw-2 (crate (name "duktape_ffi_raw") (vers "2.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "07dy0d40d721py9qrhvcw5ryka16ivywh1pqnw6a31v5jxpssa1v") (links "duktape")))

(define-public crate-duktape_ffi_raw-2 (crate (name "duktape_ffi_raw") (vers "2.30.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "113dblwzy102f1c87kgqfq89c9kgs3c7wbrhhjljx32c8j3792mi") (links "duktape")))

(define-public crate-duktape_sys-0.0.1 (crate (name "duktape_sys") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "~0.0.1") (default-features #t) (kind 1)))) (hash "1qr07fqy1q4ihapapnws492mwkfiw3ri92w7hqyw5qz5y54jli25") (yanked #t)))

(define-public crate-duktape_sys-0.0.2 (crate (name "duktape_sys") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "*") (default-features #t) (kind 1)))) (hash "0fjp7r5rnhb1ia602cph6z7jnchcxz5vjrzd9vs88wv3b6k9vpfl") (yanked #t)))

