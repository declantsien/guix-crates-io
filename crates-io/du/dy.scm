(define-module (crates-io du dy) #:use-module (crates-io))

(define-public crate-dudy-malloc-0.1 (crate (name "dudy-malloc") (vers "0.1.0") (deps (list (crate-dep (name "mimalloc-rust") (req "^0.2") (default-features #t) (target "cfg(not(target_os = \"linux\"))") (kind 0)) (crate-dep (name "tikv-jemallocator") (req "^0.4") (features (quote ("disable_initial_exec_tls"))) (default-features #t) (target "cfg(all(target_os = \"linux\", target_env = \"gnu\", any(target_arch = \"x86_64\", target_arch = \"aarch64\")))") (kind 0)))) (hash "0sfkxdaw5ag2wia1dhvik44496cndvc0q0yv03rvifps81r4b4vn")))

(define-public crate-dudy-malloc-0.1 (crate (name "dudy-malloc") (vers "0.1.1") (deps (list (crate-dep (name "mimalloc-rust") (req "^0.2") (default-features #t) (target "cfg(not(target_os = \"linux\"))") (kind 0)) (crate-dep (name "tikv-jemallocator") (req "^0.4") (features (quote ("disable_initial_exec_tls"))) (default-features #t) (target "cfg(all(target_os = \"linux\", target_env = \"gnu\", any(target_arch = \"x86_64\", target_arch = \"aarch64\")))") (kind 0)))) (hash "0j75r9377vxgr8z9vmxgmv1g96wdyprn0qvjyq12lzd1gqnn65sg")))

