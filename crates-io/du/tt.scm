(define-module (crates-io du tt) #:use-module (crates-io))

(define-public crate-dutty-1 (crate (name "dutty") (vers "1.0.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "16qg1lp869l09v7183bb1aks6ykqksx2gn7n7w0ymjhm84h31z8s")))

(define-public crate-dutty-1 (crate (name "dutty") (vers "1.0.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0j30wq1m5b061kgi8x5ci8zj7sf4rvkqihbncbagsvw42yg0424l")))

(define-public crate-dutty-1 (crate (name "dutty") (vers "1.0.3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1dazs6l4ixm4hallrbghfl58lvjpnq1sw767374nii5gjcg93xia")))

(define-public crate-dutty-1 (crate (name "dutty") (vers "1.0.4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1bfjax6lf5bv876714gp1mdl7asknz7vjfjqnzyg8h9762722dzi")))

