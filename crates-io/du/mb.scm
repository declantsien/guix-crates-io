(define-module (crates-io du mb) #:use-module (crates-io))

(define-public crate-dumb-container-0.0.1 (crate (name "dumb-container") (vers "0.0.1") (hash "03f12xvh54x2ynsb4syldx345m0nazcisgrhmysr18xih7xgm46f") (yanked #t)))

(define-public crate-dumb-container-0.0.2 (crate (name "dumb-container") (vers "0.0.2") (hash "1advl55yfvxnsi36qd1akmgs76pabm5hnw93m0hkbn7v2al2v1gx") (yanked #t)))

(define-public crate-dumb-crypto-1 (crate (name "dumb-crypto") (vers "1.0.0") (hash "005yn965i6kq8vg9zvxf60574968p7fq5k52q1nsx9xgzgg8b7np")))

(define-public crate-dumb-crypto-1 (crate (name "dumb-crypto") (vers "1.0.1") (hash "0130yp90kwfm0jagl5lxydmn138gkl5qq2cgm84f0hh9is44z208")))

(define-public crate-dumb-crypto-1 (crate (name "dumb-crypto") (vers "1.0.2") (hash "054ylri5j442z3ml2fbgr24ni2gczy2g6xl0rl36mbar369yayd5")))

(define-public crate-dumb-crypto-1 (crate (name "dumb-crypto") (vers "1.0.3") (hash "0bfsx0piphiv0d8psrka7qnrbsrhgvnvmn8z84v5ch79phc7fmm9")))

(define-public crate-dumb-crypto-1 (crate (name "dumb-crypto") (vers "1.0.4") (hash "0gd219ydgi4yfbxcm6mdvf5zw6j3s85vkq0py38s4l5sk7ig6vw1")))

(define-public crate-dumb-crypto-1 (crate (name "dumb-crypto") (vers "1.0.5") (hash "1afmpwsd4lb8mpkqy39hnd2fjskzbg5zjzhmvj4zq31a0v6kzayk")))

(define-public crate-dumb-crypto-2 (crate (name "dumb-crypto") (vers "2.0.0") (hash "0kx1b1yba159yvr2z6ziy0v31sp5xf9mlvxf7yznrqrnnxlrdr6r")))

(define-public crate-dumb-crypto-3 (crate (name "dumb-crypto") (vers "3.0.0") (hash "1lc46alp9ra82y59w50znp8b2bl9lla39991dq46m47cizb37v73")))

(define-public crate-dumb-crypto-3 (crate (name "dumb-crypto") (vers "3.0.1") (hash "17x6xhpj8f8pv2r3pklwqzsz8jhgccc0bkkmil16gbp30mz3g4qx")))

(define-public crate-dumb-crypto-3 (crate (name "dumb-crypto") (vers "3.1.0") (hash "0rx9q3rvqqk938xhq94nkkzyz2yyan26gwb458glrfbzamllm0vx")))

(define-public crate-dumb-exec-0.0.1 (crate (name "dumb-exec") (vers "0.0.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("nightly"))) (kind 0)))) (hash "06h0vzqv959h6w5q5ccc4hrdnjhydmkjsprwbybkjjma4i8wl15k")))

(define-public crate-dumb-exec-0.0.2 (crate (name "dumb-exec") (vers "0.0.2") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("nightly"))) (kind 0)))) (hash "0y25lbpyfmkls7cw4b2qdq6f9glfx1yanbxzkrhkfgvb77hp3jm2")))

(define-public crate-dumb-exec-0.0.3 (crate (name "dumb-exec") (vers "0.0.3") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("nightly"))) (kind 0)))) (hash "0m8dfa5w0vwyb807br7115svvc6pjz210vdjcg8y244b4zvk9chy")))

(define-public crate-dumb-exec-0.0.4 (crate (name "dumb-exec") (vers "0.0.4") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("nightly"))) (default-features #t) (kind 0)))) (hash "0cwkxwr3fmnrqrhlxrb973li50id6r3612bv7iym1pj6vh5rxsi8")))

(define-public crate-dumb-exec-0.0.5 (crate (name "dumb-exec") (vers "0.0.5") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.7") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.9") (default-features #t) (kind 0)))) (hash "1nia8z4ld31k2af30w3az1z5kwxjfp1cqzf4ram8z9jhiaqv0f2w") (features (quote (("default") ("alloc"))))))

(define-public crate-dumb-exec-0.0.6 (crate (name "dumb-exec") (vers "0.0.6") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.7") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.9") (default-features #t) (kind 0)))) (hash "03hwbscshas9bsgv8c2g0izjkmdacjc36rlm9yahm8kajqba9sia") (features (quote (("default") ("alloc"))))))

(define-public crate-dumb-exec-0.0.7 (crate (name "dumb-exec") (vers "0.0.7") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.7") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.9") (default-features #t) (kind 0)))) (hash "1b7ph3gsakic57zb11sh0g2pml0c4b17v3fa003rk89x4s6nwqv5") (features (quote (("default") ("alloc"))))))

(define-public crate-dumb_ai-1 (crate (name "dumb_ai") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "0cxvkgi9y9q91crb5qzfq7p95p7dpnaf0g9knh1q6n27bhn90d4y") (yanked #t)))

(define-public crate-dumb_ai-1 (crate (name "dumb_ai") (vers "1.0.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "115zshpz43bffds2nwsd0gg5v0yi93wdshcn6zl118k8c6hh5m7x") (yanked #t)))

(define-public crate-dumb_ai-2 (crate (name "dumb_ai") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "0njdvc2xygpbnxs9s8aqa81svqg22k235pxgp14yhnnk730vh7bb") (yanked #t)))

(define-public crate-dumb_ai-2 (crate (name "dumb_ai") (vers "2.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "16dxzjshg8pi0l8pjz5vp03qd6i2cwwwvkll1q84r1awp3755kbb") (yanked #t)))

(define-public crate-dumb_ai-2 (crate (name "dumb_ai") (vers "2.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "1pwzgba6x62yqkvaxcjb43i772ncy143bnrfhpa22nb6zgj4207h") (yanked #t)))

(define-public crate-dumb_ai-2 (crate (name "dumb_ai") (vers "2.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "1p85isiwzlj9v3v1gxadd3ghy4y1hp5j8smry1xh7mr4g1x8fkv9") (yanked #t)))

(define-public crate-dumb_ai-3 (crate (name "dumb_ai") (vers "3.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "1q8lnbp0vsymrlwvr4vi3jcap76flxlfhi9pzi50ww4ph2amg3ni") (yanked #t)))

(define-public crate-dumb_ai-3 (crate (name "dumb_ai") (vers "3.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "0nwh8pwxpsl41yzapqqg63fikxif3n5bh7zji8rmr5v5vc00971p")))

(define-public crate-dumb_cgi-0.5 (crate (name "dumb_cgi") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "09xxa16nfv6ypprjmr4gxqwmsqqj488zl5n11cibagzwzrsqchk4") (v 2) (features2 (quote (("log" "dep:simplelog"))))))

(define-public crate-dumb_cgi-0.6 (crate (name "dumb_cgi") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0942gsx3njcrrwid5i9ga29a25c9x97q9qaqbzac92sfl8x7d86y") (v 2) (features2 (quote (("log" "dep:simplelog"))))))

(define-public crate-dumb_cgi-0.7 (crate (name "dumb_cgi") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1dnvp9xxpdn1h4v254wqd5ghfn5d5xjqk02b8q6dy6jy9kxjv2aj") (v 2) (features2 (quote (("log" "dep:log" "dep:simplelog") ("default" "dep:log"))))))

(define-public crate-dumb_cgi-0.8 (crate (name "dumb_cgi") (vers "0.8.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1pdms67vk120r97y3varvnnccvf48vaz7xhx9r0pq8qmmq5np29d") (features (quote (("default")))) (v 2) (features2 (quote (("log" "dep:log" "dep:simplelog"))))))

(define-public crate-dumb_http_parser-0.1 (crate (name "dumb_http_parser") (vers "0.1.1") (deps (list (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1fkgirq5c8prabljvj8fha7fqkpa4b3m1h9836dik7d2f313b2dx")))

(define-public crate-dumb_http_parser-0.1 (crate (name "dumb_http_parser") (vers "0.1.2") (deps (list (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "086kwdfkxjswrpvgxvlvahskvr78x81wgvxvqy7c8a8h1587vdrr")))

(define-public crate-dumb_logger-0.1 (crate (name "dumb_logger") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0v7va12s5ikmpqxfggnkh497m4ijf3pvlihbbmjlnxv4vrba0rn4")))

(define-public crate-dumbfuzz-0.1 (crate (name "dumbfuzz") (vers "0.1.0") (hash "0236grf15l8bf3pmg602nkbj5m2km6c14vqgd3g0649gh727wbg2")))

(define-public crate-dumbfuzz-1 (crate (name "dumbfuzz") (vers "1.0.0") (hash "1d5ldk0bi88xg4ljsv94avfbaf31741h02adsnnalzmqf5ymqksw")))

(define-public crate-dumbhttp-0.1 (crate (name "dumbhttp") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.17") (default-features #t) (kind 0)) (crate-dep (name "routerify") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p5nw7a8l4yk3i9hq9snmpz4abxbvi8rappj9h80nsaik4cizmy2")))

(define-public crate-dumbhttp-0.1 (crate (name "dumbhttp") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.17") (default-features #t) (kind 0)) (crate-dep (name "routerify") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "116657bcpykbk4lzsc51bkd6sm7a2zg6am53x4xq0rsjk5pkkbaf")))

(define-public crate-dumbhttp-0.1 (crate (name "dumbhttp") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.17") (default-features #t) (kind 0)) (crate-dep (name "routerify") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lphwpvpgpl14sxqdxd5z3j0inbj6c9ikdrs0g71hcphb4a87xii")))

(define-public crate-dumbhttp-0.1 (crate (name "dumbhttp") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.17") (default-features #t) (kind 0)) (crate-dep (name "routerify") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a4i9bkr72qn6j7pynq8kfnd30p9dqsjxig75w9gnbqhdb5i3x30")))

(define-public crate-dumbhttp-0.1 (crate (name "dumbhttp") (vers "0.1.4") (deps (list (crate-dep (name "hyper") (req "^0.14.17") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kvrd35mj7icvkmwcrl2zpfdpdqr62h4mpsxjnx9rl60q4xx874h")))

(define-public crate-dumbledore-0.1 (crate (name "dumbledore") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1yq1cmwss5j3081r284q6xx52ipz9fs5arh92ymcnrrl4xdz7l7v")))

(define-public crate-dumbmath-0.0.1 (crate (name "dumbmath") (vers "0.0.1") (hash "1ylmd6w4151kjbwpdca8963hppgdjx1nz8mn22nzhxkkw1j3byq0")))

(define-public crate-dumbmath-0.0.2 (crate (name "dumbmath") (vers "0.0.2") (hash "0clxg4k5fn5bwl0104va9m9ph0jp6zd2w3alndh518jv3wz5h7s4")))

(define-public crate-dumbmath-0.0.3 (crate (name "dumbmath") (vers "0.0.3") (hash "0nx3mjaaqidbwiay48lzk0qf4ysp1h38j0p2da7fr65i9iyg7sv6")))

(define-public crate-dumbmath-0.0.4 (crate (name "dumbmath") (vers "0.0.4") (hash "0nzb3g4hgf1mc5gkjcbbcj0divrhx4cgzrcsg1cf9rgsmwy75h58")))

(define-public crate-dumbmath-0.0.5 (crate (name "dumbmath") (vers "0.0.5") (hash "0yp3m4yr75d5v6kfcdydxvalbwbpx03w5j143fq35h78iyhgnn3f")))

(define-public crate-dumbmath-0.0.6 (crate (name "dumbmath") (vers "0.0.6") (hash "0iddpz67s2a8c3is5ya6bv0jax2iv495a380xkzy47aldfz12vxf")))

(define-public crate-dumbmath-0.0.7 (crate (name "dumbmath") (vers "0.0.7") (hash "073h0kxcwwcd6sjmm87356nwf14g9jsk0r6yi3cnw8s829jnsbby")))

(define-public crate-dumbmath-0.0.8 (crate (name "dumbmath") (vers "0.0.8") (hash "1w7g2vw2ib5dfh1sg9649xw70wbbajlqf3a2r0qpnml6ph9zxp43")))

(define-public crate-dumbmath-0.1 (crate (name "dumbmath") (vers "0.1.0") (hash "0i1rzvzazramc9ca83a3m2k7yd5v92z5fgcj44rm1vsq5f55kzhv")))

(define-public crate-dumbmath-0.1 (crate (name "dumbmath") (vers "0.1.1") (hash "1fq255ip1ks9qhasaww8ci21w2b76jjsvnamqncxdkpshz5h8lzf")))

(define-public crate-dumbmath-0.1 (crate (name "dumbmath") (vers "0.1.2") (hash "0zkb9bd9xnlapjlqxizc6d4vygwvrdgac1zhsq1ibv7hpif4ksbf")))

(define-public crate-dumbmath-0.1 (crate (name "dumbmath") (vers "0.1.3") (hash "03xa6plx98mcn84b64as2j3gixnawasvcxdq9gypi5gfwawy8jyq")))

(define-public crate-dumbmath-0.1 (crate (name "dumbmath") (vers "0.1.4") (hash "1hs8d74snbycqc2f145h0rwby7qvyrvwwlr7370xv0mlify14vh3")))

(define-public crate-dumbmath-0.1 (crate (name "dumbmath") (vers "0.1.6") (hash "098qw1w3dca61r65wpba77cv1ymj65f67dkx23vq1diksyj4xsap")))

(define-public crate-dumbmath-0.1 (crate (name "dumbmath") (vers "0.1.7") (hash "0n111p529lwn4aw3ag6n41yr7smng4slgalpfhkb47vva9m3nv3x")))

(define-public crate-dumbmath-0.2 (crate (name "dumbmath") (vers "0.2.0") (hash "1c7jdnfnrrgkp7aszjirngdj7gb742izlrxw1j5jabrc86a61khn")))

(define-public crate-dumbmath-0.2 (crate (name "dumbmath") (vers "0.2.2") (hash "101nnbb3n17wjq4sadandm57grf0hp6sj4vj72yrlv38fxfmd3cy")))

(define-public crate-dumbmath-0.2 (crate (name "dumbmath") (vers "0.2.3") (hash "0zfx917hh9bqr7yym6aary0n2l9g666vgwvbfrngsdmac6f13fvv")))

(define-public crate-dumbnet-0.1 (crate (name "dumbnet") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.13") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.1") (features (quote ("dummy"))) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.32") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (kind 0)) (crate-dep (name "numeric-array") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("getrandom"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "10zffc59ynbcmfxylpajqv0zav6m2ymssb7aimga55nklvskxnrx")))

(define-public crate-dumbo-0.1 (crate (name "dumbo") (vers "0.1.0") (hash "0wfbnybd7wviblf0grgn3x0v7i8rbb4cg0s2yxcbq1yrc3b9y0y8")))

(define-public crate-dumbpipe-0.1 (crate (name "dumbpipe") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0q674fjvafpiqb43lr0qw6pamfg03m9dnndk9v22nl85fzqlfby0")))

(define-public crate-dumbpipe-0.2 (crate (name "dumbpipe") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "iroh-net") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1y56j0qbkd6kadciwlaz1fvcbbxb617k2qi1f5zp9cj96l703p7r")))

(define-public crate-dumbpipe-0.2 (crate (name "dumbpipe") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "iroh-net") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "04dzs4qkvj3lapn3648w1xkkygbf6zsyd6qb23s8l2hnvdqm3h7s")))

(define-public crate-dumbpipe-0.3 (crate (name "dumbpipe") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1hn1cnl5sd33bpv9arlf0chcls64ax1j2ja23dqgmz386f101qbb")))

(define-public crate-dumbpipe-0.4 (crate (name "dumbpipe") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1rvw14chniq9amyi6vzl7h4q7mm6jv8z8lg024imhnazgygm4725")))

(define-public crate-dumbpipe-0.5 (crate (name "dumbpipe") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "01phrr00ydrzvrxi44zzgf3jkc8ljs2g3z46awwpfpw7pnz2d20q")))

(define-public crate-dumbpipe-0.6 (crate (name "dumbpipe") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1wbln7ppjjkndrg88ijhi6wkfwv7gjhq4i71wa5xa3b10bk7rk0r")))

(define-public crate-dumbpipe-0.7 (crate (name "dumbpipe") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1w0qk5w8hb074nkrmydaf0g36yvd894gccjzngh0sxgqa3zzy8yj")))

(define-public crate-dumbpipe-0.8 (crate (name "dumbpipe") (vers "0.8.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "quinn") (req "^0.10") (default-features #t) (kind 0) (package "iroh-quinn")) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "0qq6dxscm228jdg4kys9z92mrhhjc2nd7yn18kgdqhsl1j5fk2v9")))

(define-public crate-dumbpipe-0.9 (crate (name "dumbpipe") (vers "0.9.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "iroh-net") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("signal" "process"))) (default-features #t) (kind 2)) (crate-dep (name "quinn") (req "^0.10.5") (default-features #t) (kind 0) (package "iroh-quinn")) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1zk7x8lm6626h7h7i41xb7whn949fnj50h4pnipwcvky7dh86ijz")))

(define-public crate-dumbshitpost-0.1 (crate (name "dumbshitpost") (vers "0.1.0") (hash "0rr3jwdh01acq8zf393z7mx693pz0m6b5x688gmazydb44qz209n") (yanked #t)))

(define-public crate-dumbshitpost-0.1 (crate (name "dumbshitpost") (vers "0.1.1") (hash "1sdwfaggj5lj5nnp0bbskcwp5dyd0d938znl9ps5xdidzzihqs14") (yanked #t)))

