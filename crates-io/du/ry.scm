(define-module (crates-io du ry) #:use-module (crates-io))

(define-public crate-durylog-0.1 (crate (name "durylog") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("formatting" "macros"))) (default-features #t) (kind 0)))) (hash "1rrz9l9ndhj5fpiqjhk0g830105p9ikxz5ni1q2p3h3lgj1gwmjn")))

(define-public crate-durylog-0.1 (crate (name "durylog") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("formatting" "macros"))) (default-features #t) (kind 0)))) (hash "1zrqg09pj5z41gp3jgdjq0sdlm7k7hcbfsnk9jc4bx51gal5hr5c")))

(define-public crate-durylog-0.1 (crate (name "durylog") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("formatting" "macros"))) (default-features #t) (kind 0)))) (hash "1pkqpm125mp8a6mlr7mkhh72mi3dmhpv20w2jnvnjpv753f56khq")))

