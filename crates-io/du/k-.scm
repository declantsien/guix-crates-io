(define-module (crates-io du k-) #:use-module (crates-io))

(define-public crate-duk-sys-0.2 (crate (name "duk-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.10") (optional #t) (default-features #t) (kind 0)))) (hash "06div3r350nqmxxj2dxpsk48hq9110yahw6dag9fyblfj1lzcj5l") (features (quote (("trace" "log") ("spam" "log") ("debug" "log"))))))

(define-public crate-duk-sys-0.3 (crate (name "duk-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.10") (optional #t) (default-features #t) (kind 0)))) (hash "0a5g4vbs6q5xp75ipvr576h66433z70v3l96yxsg3b6y2pqngjmp") (features (quote (("trace" "log") ("spam" "log") ("debug" "log"))))))

