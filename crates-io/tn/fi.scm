(define-module (crates-io tn fi) #:use-module (crates-io))

(define-public crate-tnfilt-0.1 (crate (name "tnfilt") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "0ky5fqzz9jal5gjkwwg9c42y4p3iikb4h75y3wc3kq85ay4yrs04")))

(define-public crate-tnfilt-0.1 (crate (name "tnfilt") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "13jm2xbpvb9jbp0l5bmcyx6g22d46c4qi6y10y9khp1x9swqj7rd")))

