(define-module (crates-io tn od) #:use-module (crates-io))

(define-public crate-tnodetree-0.1 (crate (name "tnodetree") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0vpzdl1s8hvls5whlblf1c2rqcmshxm9p2bsi5miqi72s8sxsyaj")))

