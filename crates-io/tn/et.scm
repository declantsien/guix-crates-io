(define-module (crates-io tn et) #:use-module (crates-io))

(define-public crate-tnet-0.0.1 (crate (name "tnet") (vers "0.0.1") (hash "14mw30w5mb9ki7hd4n36azihbfciy9abkdr5ma126l1i3dgnjfz2") (yanked #t)))

(define-public crate-tnetstring-0.1 (crate (name "tnetstring") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "05na1pk3p765jr66yr8qhk6agq3sbh2z0lyrix29ikjywsa0r78s")))

(define-public crate-tnetstring-0.2 (crate (name "tnetstring") (vers "0.2.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "008bglc47chw1a73krkkb2gci8p94zgkpc04py66rdhd29hvyzh8")))

