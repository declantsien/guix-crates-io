(define-module (crates-io tn ap) #:use-module (crates-io))

(define-public crate-tnaps-0.1 (crate (name "tnaps") (vers "0.1.0") (deps (list (crate-dep (name "arrrg") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "guacamole") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "statslicer") (req "^0.1") (default-features #t) (kind 2)))) (hash "09zyxj526hgn2bnjdl1kj21gh1kxpxpizl79y0fz4cndyjw2gjaz")))

