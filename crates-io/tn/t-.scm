(define-module (crates-io tn t-) #:use-module (crates-io))

(define-public crate-tnt-lib-0.1 (crate (name "tnt-lib") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.32") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1dbx473pjsaxfpj7m2pwnayc1yxl3wmyviglv5wijy3n6gnw201n") (links "NTL")))

(define-public crate-tnt-lib-0.1 (crate (name "tnt-lib") (vers "0.1.2") (deps (list (crate-dep (name "cxx") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.32") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "180yx6n6s1znqr5xhx1i0sjbbs56ys61jjdpa8aw918w8xlmwnay") (links "NTL")))

(define-public crate-tnt-lib-0.1 (crate (name "tnt-lib") (vers "0.1.3") (deps (list (crate-dep (name "cxx") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.32") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "08x2h7gzij9q87hgdlnv5p64zj228rs5i0cxcn95rc4pklj60kmz") (links "NTL")))

(define-public crate-tnt-lib-0.1 (crate (name "tnt-lib") (vers "0.1.4") (deps (list (crate-dep (name "cxx") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.32") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "04w1rx7dzdxjyjsk68d3zzhi3q1s3l02cr50iav0pf4iba4pbzvh") (links "NTL")))

(define-public crate-tnt-note-0.1 (crate (name "tnt-note") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "lexopt") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13ak1s1birp5g7lk2qc3d9j3wmpsxg2zgwir0vgm1zg2gdfkr83q")))

(define-public crate-tnt-note-0.1 (crate (name "tnt-note") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "lexopt") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "01awmbgnzb9m8lq55z2mawk3lyx68s7a14a3jhhwirdbrywh6zar")))

