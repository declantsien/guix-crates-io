(define-module (crates-io tn ef) #:use-module (crates-io))

(define-public crate-tnef-0.1 (crate (name "tnef") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "codepage") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "07vi7bcwspjrf0sq9a63jfrbqxgrmaxkd5ybssrjfrs2i5vb2wqb") (yanked #t)))

(define-public crate-tnef-0.1 (crate (name "tnef") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "codepage") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1cknwd0dn5inm9ldmi2kqzzsw71fjc3qzwk1a1qc17wz2x99kxhn")))

