(define-module (crates-io tn p-) #:use-module (crates-io))

(define-public crate-tnp-extensions-0.10 (crate (name "tnp-extensions") (vers "0.10.1") (deps (list (crate-dep (name "torrent-name-parser") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0w2vm2fgxpagxlbjdq5hv8b68zvigd7ifk0g4afjg8q7lkpxbhml")))

