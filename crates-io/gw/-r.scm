(define-module (crates-io gw -r) #:use-module (crates-io))

(define-public crate-gw-rust-programming-tutorial-0.1 (crate (name "gw-rust-programming-tutorial") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "stdext") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1mihjb2cy3jq3zi62glpgc6r0cjffwlfqhb4vjw6py3ir2rf0fp1")))

