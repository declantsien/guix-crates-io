(define-module (crates-io gw or) #:use-module (crates-io))

(define-public crate-gwordle-0.1 (crate (name "gwordle") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0vp1rxbv7npp6kb9j9jvqfk4kgm9vgzxwqkqy6bl9hxikycj2p2v")))

(define-public crate-gwordle-0.1 (crate (name "gwordle") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0v7y3vg6if8s1fgjdwi8138dhck0a58pd50mfmhx67m12840vjc7")))

(define-public crate-gwordle-0.1 (crate (name "gwordle") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0ngjmm7xhxzk02za14mkdm5pzn3az4lg1crbg7sw77wi52k78cqz")))

(define-public crate-gworld-0.1 (crate (name "gworld") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "06lgjlkd1ipd2f9nqfq3n0wi3y3x94hirkxir1m3gfg4a6mq1sgg")))

