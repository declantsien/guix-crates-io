(define-module (crates-io gw #{2t}#) #:use-module (crates-io))

(define-public crate-gw2timers-0.1 (crate (name "gw2timers") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "12b8k2wr3aasrp6bcdhksghzvpcp54ixxl2w2nmymhhcjklzmqih")))

(define-public crate-gw2timers-0.2 (crate (name "gw2timers") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "119rvafif5jfb4svvi51x3r61sfrssiyy6pfiidsi8wzmysn078k")))

(define-public crate-gw2timers-0.3 (crate (name "gw2timers") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1pmfffg55kgrnb6hzj9xpbkdkxl7rc6ak4rnxlc0il9j49myawf1")))

(define-public crate-gw2timers-0.3 (crate (name "gw2timers") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1q792hk0hy5c73d3fvmz8r01acdzfla862iclyf26vnf62181s61")))

(define-public crate-gw2timers-0.4 (crate (name "gw2timers") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "14xg9ddl0i6cz457x7i0cvci76hg2mkki36w2a5f8amjp5bzjrzm")))

(define-public crate-gw2timers-0.4 (crate (name "gw2timers") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1dcalvrqcjm86kzmah81is2qn1mwfh7llkw3biacf920r6yvrd1m")))

