(define-module (crates-io gw -d) #:use-module (crates-io))

(define-public crate-gw-dsl-parser-0.1 (crate (name "gw-dsl-parser") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.36.0") (default-features #t) (kind 0)))) (hash "0xf7wdcd9s5gzzkq6ah53i143ssc3j625a3g0ac79df4xanmk0xy") (yanked #t)))

