(define-module (crates-io ez ra) #:use-module (crates-io))

(define-public crate-ezra-0.1 (crate (name "ezra") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1whipykiv95vy09yip8biwcm9xf09qyf2zqxvxg3zpv13nbmgyfc")))

(define-public crate-ezra-0.1 (crate (name "ezra") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1xhh912mv4jrn89jqkw6pvriq1fxr8wyqmlj0r0j6m70hc9mwbcz")))

(define-public crate-ezra-0.1 (crate (name "ezra") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0m0xqyfm0zdzimw5iy91np5603zfyqc3p4g4hnq1978dvf7z5lw7")))

(define-public crate-ezra-0.1 (crate (name "ezra") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1vbsqr40sbrawg3fw4n1p82fxk8xkirxv2a0bs9nf19sz3k79gph")))

