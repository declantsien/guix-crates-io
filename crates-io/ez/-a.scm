(define-module (crates-io ez -a) #:use-module (crates-io))

(define-public crate-ez-audi-0.1 (crate (name "ez-audi") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "00kbhi9wsbqfw4ajsdvbqxck6m4j86pjyk0f7qk3v0k6n09gpdam")))

(define-public crate-ez-audi-0.2 (crate (name "ez-audi") (vers "0.2.0") (deps (list (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "1xalnn8739sq466qigcysas4n8cg5if3ahs99xacvgiwgg8g6vcp")))

(define-public crate-ez-audi-0.2 (crate (name "ez-audi") (vers "0.2.1") (deps (list (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "124vdvv42b82syyln2shm65kydk7y1ig7hxl7h5hbmmfjz7mck5j")))

(define-public crate-ez-audi-0.2 (crate (name "ez-audi") (vers "0.2.2") (deps (list (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "0ywikrjqldv5xy31m91gp3issy5kc2lplqvd9wc24dww951i4z0j")))

(define-public crate-ez-audi-0.2 (crate (name "ez-audi") (vers "0.2.3") (deps (list (crate-dep (name "cpal") (req "^0.15") (default-features #t) (kind 0)))) (hash "1siyb2gmyyq2r9rk3pw932x7b3ihlr2l1g6y89z2hg6x3dd9mwyx")))

(define-public crate-ez-audi-0.3 (crate (name "ez-audi") (vers "0.3.0") (deps (list (crate-dep (name "cpal") (req "^0.15") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "0mkfmli3jlj21n5yk9qjd55g25fk1qind5qa7ma29zb9yhr8g0r2")))

