(define-module (crates-io ez io) #:use-module (crates-io))

(define-public crate-ezio-0.1 (crate (name "ezio") (vers "0.1.0") (hash "0cm7hjjyspgy7wffl0q5iin531s8knm3ysymi1difgw0fsfgilr8")))

(define-public crate-ezio-0.1 (crate (name "ezio") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1ilhkjdzics7jxq58m4b5sgx8sagz5gkdfdm1ca6fg5gf7z31b7f")))

(define-public crate-ezio-0.1 (crate (name "ezio") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "19xw3ldrnmf8jzx9g85fhpfzch5bmccjwrlwy5fb77l81krdyl5v")))

