(define-module (crates-io ez -r) #:use-module (crates-io))

(define-public crate-ez-rust-0.1 (crate (name "ez-rust") (vers "0.1.0") (hash "1108yravmq4wpkasfly1vi8d8587sj578180b4ah687jjciks7xb")))

(define-public crate-ez-rust-0.1 (crate (name "ez-rust") (vers "0.1.1") (hash "13gn4gv4nbslnz9xlfx9dl5qlcrc6zd9rqrvmb9p2nblybrdfva3")))

(define-public crate-ez-rust-0.1 (crate (name "ez-rust") (vers "0.1.3") (hash "1bm5njh8ij5qgaiil0w504xy5pii236cyakx1qh6v0qfnd5f9h59")))

(define-public crate-ez-rust-0.1 (crate (name "ez-rust") (vers "0.1.4") (hash "0n28ggr2zjba0p8al8w5ma0kvcip178wqhpwr6wfzy47lbpc7knk")))

(define-public crate-ez-rust-0.1 (crate (name "ez-rust") (vers "0.1.5") (hash "0jkm2kdb6h72xdffq8sgbqzg9ax2h5mpd5kw0hgagyiig3fq28y2")))

(define-public crate-ez-rust-0.1 (crate (name "ez-rust") (vers "0.1.6") (hash "1ifv69nzhywjzz9n4zypbajy7dzc75d4lylx4mjkv94jix6pd5xi")))

