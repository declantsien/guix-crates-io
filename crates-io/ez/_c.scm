(define-module (crates-io ez _c) #:use-module (crates-io))

(define-public crate-ez_colorize-0.1 (crate (name "ez_colorize") (vers "0.1.0") (hash "1iis8i8va6d6kmfx5f5xl59d400p5zz0rswassqv6g7cmjh6fcg1")))

(define-public crate-ez_config-0.1 (crate (name "ez_config") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1825r7z9w2a2vw5prn6fjp83g8876hjkb2c05si9plbqwyw2wwj5")))

(define-public crate-ez_config-0.1 (crate (name "ez_config") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1f1ad9chxiyxzz6insc9xg4y75c3gzwpmc8whzcd3sn9z1wxqgql")))

