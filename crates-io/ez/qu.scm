(define-module (crates-io ez qu) #:use-module (crates-io))

(define-public crate-ezquadtree-0.0.1 (crate (name "ezquadtree") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1va3c9ms1v25ncsgmw2wdl1nlc1piqw5brq4ncapld6fgkzxqd22")))

