(define-module (crates-io ez wo) #:use-module (crates-io))

(define-public crate-ezwordle-0.1 (crate (name "ezwordle") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "0wgnf1hjnj3kp6xbni0y857hh96j36j5zv7qsdqh26sp4z6dfs9x")))

(define-public crate-ezwordle-0.1 (crate (name "ezwordle") (vers "0.1.1") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "17l9m8v24xq9g2ix4n2p3miy4gyqbzswwy5dwbqv4ai3bkvnfjac")))

(define-public crate-ezwordle-0.2 (crate (name "ezwordle") (vers "0.2.1") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "0gvq4v19n456l9k6qm84jvnc2n9aj9d8nmwjfkl73ifvcmfmz08g")))

(define-public crate-ezwordle-0.3 (crate (name "ezwordle") (vers "0.3.0") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "08nadd6bb49xkfzqn7g4w750ks9d4a1j29y0r83ivhkgh2jzvcpw")))

(define-public crate-ezwordle-0.4 (crate (name "ezwordle") (vers "0.4.0") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "0i1zkxh54kwv208lwswf79wp1k83lgldc0c0z11mf1i8klvwvj9x")))

(define-public crate-ezwordle-0.4 (crate (name "ezwordle") (vers "0.4.1") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "0spa8vjhmxgrnqr8qa5fb7fvmhhw54mzprl9swjp5s6j87piwmsh")))

