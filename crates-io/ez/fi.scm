(define-module (crates-io ez fi) #:use-module (crates-io))

(define-public crate-ezfile-0.1 (crate (name "ezfile") (vers "0.1.0") (deps (list (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "08lahwg87pmsz7vlfhcglqns240ll2c6wrdrz604n9k3yl2f1b7z")))

(define-public crate-ezfile-1 (crate (name "ezfile") (vers "1.0.0") (deps (list (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "18s1hrh97b7s6j4lkhswdjb182gqr4kszm7j9z19ay0jy4wf2y5w")))

