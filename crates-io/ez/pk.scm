(define-module (crates-io ez pk) #:use-module (crates-io))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.2") (deps (list (crate-dep (name "pkt") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1bx11qb2xgzrr0fzxiwjmmzqj3xnfdvinzh7lxv4rmckwzbjmr76")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.3") (deps (list (crate-dep (name "pkt") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1j82dkycspxcicllbcchk31vz3j2r75h6kpg0nva22gcwwkj371n")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.4") (deps (list (crate-dep (name "pkt") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1xg20lxv7mi918bbw4dyl2rplmqgilwgjpvfpvms43602a0sbqch")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.5") (deps (list (crate-dep (name "pkt") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0pl45gvi6w80fcdva9gkixmhb5iyw87kdvjnil8idwgg2hxpdihq")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.6") (deps (list (crate-dep (name "pkt") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1qh452zbdhdd926hhnvgfr9sggdj7xs7v5865iy6rcf82pc2g5zm")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.7") (deps (list (crate-dep (name "pkt") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0d99d3nzp62svg9hn1mwzqs5c4kh77k1j1vpy50bagk3v03q6hg3")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.8") (deps (list (crate-dep (name "pkt") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "17w5nbzm9aw31ww8par7www21wr25nzncm1rkqprin0vagv5axff")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.9") (deps (list (crate-dep (name "pkt") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1pfn9n1ghkx84ijfzaqqxylbwqws2yycngswsihiqnrww2cbq0r7")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.10") (deps (list (crate-dep (name "pkt") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1mq3758haf4bnll981dxkcgrsvk9ch9z8g99dyi6s5yas45h55md")))

(define-public crate-ezpkt-0.1 (crate (name "ezpkt") (vers "0.1.11") (deps (list (crate-dep (name "pkt") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "0ybr17swwsqpc3nyhrqq6lqlmiqd86v8w1k7kwrjx06yh1l2p324")))

(define-public crate-ezpkt-0.2 (crate (name "ezpkt") (vers "0.2.0") (deps (list (crate-dep (name "pkt") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wjfmd0rx4mrn5900vxaqrn1nxh8dfskzq7mszqsms1ykmb0cv87")))

