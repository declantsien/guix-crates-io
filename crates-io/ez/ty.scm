(define-module (crates-io ez ty) #:use-module (crates-io))

(define-public crate-ezty-0.1 (crate (name "ezty") (vers "0.1.0") (deps (list (crate-dep (name "mopa") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0dmbm8349gy2f15y1fspwy3j44m0796y1xx5w41v56sn7pcidsqb") (features (quote (("layout") ("default" "any_debug")))) (v 2) (features2 (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1 (crate (name "ezty") (vers "0.1.1") (deps (list (crate-dep (name "mopa") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "07l5g7ly30ilkwrignqmrz8zzvnaqxjj4myqvjwhzvaz0kxmz5i1") (features (quote (("layout") ("default" "any_debug")))) (v 2) (features2 (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1 (crate (name "ezty") (vers "0.1.2") (deps (list (crate-dep (name "mopa") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "16zq6qrqq59cvx8c3cjwd9d7hpif4a8c5lk2rqm8m01lhz917ky6") (features (quote (("layout") ("default" "any_debug")))) (v 2) (features2 (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1 (crate (name "ezty") (vers "0.1.3") (deps (list (crate-dep (name "mopa") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0v2gvwfhj9fyh0450dl81n8nvwmbdadw1vyfqlbn3c30ial60rn9") (features (quote (("default" "any_debug")))) (v 2) (features2 (quote (("any_debug" "dep:mopa"))))))

(define-public crate-ezty-0.1 (crate (name "ezty") (vers "0.1.4") (deps (list (crate-dep (name "mopa") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "12hbbbiwx6a6c2fd09x29w89kqycfbj1xk88dy26hyjrz172lnrh") (features (quote (("default" "any_debug")))) (v 2) (features2 (quote (("any_debug" "dep:mopa"))))))

