(define-module (crates-io ez it) #:use-module (crates-io))

(define-public crate-eziter-0.1 (crate (name "eziter") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "04k1qcd9j3fy29pzcwxf38aiybs8jdkn6606amc5yp03f5c2nlqs")))

(define-public crate-eziter-0.1 (crate (name "eziter") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1yv55jqj5a6n280yws2apxg9ih1jsqycj19wwjrgfwni9svdh9cj")))

(define-public crate-eziter-0.1 (crate (name "eziter") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0dbqig6q029jdqwj2pwbdlmr2zm0if4islbiqkvw4wfbh8msbz1y")))

