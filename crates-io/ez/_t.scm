(define-module (crates-io ez _t) #:use-module (crates-io))

(define-public crate-ez_term-0.1 (crate (name "ez_term") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "meval") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1w7wy7rhsqn40qhdlkdxhxfm1vcxil5sjxny58zcfif8jcas4slx") (rust-version "1.61.0")))

(define-public crate-ez_term-0.1 (crate (name "ez_term") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "meval") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "0h566pvh04zymz7jb99ydqc8alp1bk1m6vp3rj7s0k6xp0r18s76") (rust-version "1.61.0")))

