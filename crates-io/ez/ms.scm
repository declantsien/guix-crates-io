(define-module (crates-io ez ms) #:use-module (crates-io))

(define-public crate-ezmsg-0.6 (crate (name "ezmsg") (vers "0.6.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0wr20k1hq7z4m6crhmsykqk3di3qnh2ld4965r2dd7m1ci5i5nrh")))

(define-public crate-ezmsg-0.6 (crate (name "ezmsg") (vers "0.6.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "11a2ajcx7mvpcz8i4r8virgxhfaq7c44pcvm0j5vvcbwmpj2pwnn")))

(define-public crate-ezmsg-0.6 (crate (name "ezmsg") (vers "0.6.2") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1pmbdvy2axgcy6h80hfjxxbg8g2xh3s37hvh86vv2702zl5x5izn")))

