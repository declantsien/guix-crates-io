(define-module (crates-io ez me) #:use-module (crates-io))

(define-public crate-ezmem-0.1 (crate (name "ezmem") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset"))) (default-features #t) (kind 0)))) (hash "0wys9y3cya5rzmb5fn7sw9xfainwx7v8nkdkjwqlv28q0zn7xrih")))

(define-public crate-ezmem-0.1 (crate (name "ezmem") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset"))) (default-features #t) (kind 0)))) (hash "0sksfchyxzjlj7r22zzaaf8p42jajzmlha8b3cvld9i6bkdijpz1")))

(define-public crate-ezmem-0.1 (crate (name "ezmem") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset"))) (default-features #t) (kind 0)))) (hash "1fm5lvc157shpf6izwgdazbasq4xx60n8c13f1xyrwnlhv4lp4wh")))

(define-public crate-ezmem-0.2 (crate (name "ezmem") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("tlhelp32" "stralign" "handleapi" "winbase" "memoryapi" "wow64apiset" "errhandlingapi"))) (default-features #t) (kind 0)))) (hash "140rappddrpdm6rjgswx734qn3s1k7npkdvlyr8k9nga4hiwc7vn")))

(define-public crate-ezmenu-0.1 (crate (name "ezmenu") (vers "0.1.0") (deps (list (crate-dep (name "ezmenu-derive") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0g29fxrikkp5s93pnzplk8dblz2rr5a1c395d02ks9q8khd57zyf")))

(define-public crate-ezmenu-0.1 (crate (name "ezmenu") (vers "0.1.1") (deps (list (crate-dep (name "ezmenu-derive") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "1ahkyffdhrpwd352yr3syl5i5mmqs1ih0bzwwlvpnb4i3mh03f03")))

(define-public crate-ezmenu-0.2 (crate (name "ezmenu") (vers "0.2.0") (deps (list (crate-dep (name "ezmenu-derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0vvwrp4iw0wap0wjwi7wnx6yvbb376129xvng5dcwr633fzn0h49") (features (quote (("custom_io" "ezmenu-derive/custom_io"))))))

(define-public crate-ezmenu-0.2 (crate (name "ezmenu") (vers "0.2.3") (deps (list (crate-dep (name "ezmenu-derive") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "063p1dba34ixpxg2spjx8a7jxsqc0cwyjb6k6wh6492249raly0k") (features (quote (("derive") ("default" "derive") ("custom_io" "ezmenu-derive/custom_io"))))))

(define-public crate-ezmenu-0.2 (crate (name "ezmenu") (vers "0.2.4") (deps (list (crate-dep (name "ezmenu-derive") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "12h7b9pjhaiyrsp9rprn21j3nbqav8lq0i1i4yci445baq5g1cv6") (features (quote (("parsed_attr" "ezmenu-derive/parsed_attr") ("derive" "ezmenu-derive/derive") ("default" "derive" "parsed_attr"))))))

(define-public crate-ezmenu-0.2 (crate (name "ezmenu") (vers "0.2.5") (deps (list (crate-dep (name "ezmenu-derive") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "09p83v8kaq2i3nch6vmlghfqy0ibwkf20msv7qlps0z0j5la2c8q") (features (quote (("parsed_attr" "ezmenu-derive/parsed_attr") ("derive" "ezmenu-derive/derive") ("default" "derive" "parsed_attr"))))))

(define-public crate-ezmenu-0.2 (crate (name "ezmenu") (vers "0.2.7") (hash "047qik360c720j9sm4r1ak89vp9jl0fszqhbgidqhilzj1vf5lrw")))

(define-public crate-ezmenu-0.2 (crate (name "ezmenu") (vers "0.2.8") (deps (list (crate-dep (name "ezmenu") (req "=0.2.7") (default-features #t) (kind 0)) (crate-dep (name "ezmenu-macros") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1xprvcpmp685xrbzdqlyprjll84x6a59a2yfay21zs4q8l65dyb4") (features (quote (("parsed" "ezmenu-macros/parsed") ("derive" "ezmenu-macros/derive") ("default" "derive" "parsed"))))))

(define-public crate-ezmenu-0.2 (crate (name "ezmenu") (vers "0.2.9") (deps (list (crate-dep (name "ezmenu-macros") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "ezmenulib") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0wrsznm529s55b6smyn8pn73d3847zf6h86g5fj3sb7w2gbhximw") (features (quote (("parsed" "ezmenu-macros/parsed") ("derive" "ezmenu-macros/derive") ("default" "derive" "parsed"))))))

(define-public crate-ezmenu-derive-0.1 (crate (name "ezmenu-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0q048n5hfr3597l5nprgcp8hb6blym6f3sdlhm1cks1f3i5wm0yl")))

(define-public crate-ezmenu-derive-0.2 (crate (name "ezmenu-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1rly7d0z91ljz5pj9f10isn73msx75psfwv8zz689ahva6ckbcmd") (features (quote (("custom_io"))))))

(define-public crate-ezmenu-derive-0.2 (crate (name "ezmenu-derive") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1cj54mfj572h94ghlpqx3538cx99iyllfjxgvivjapjh3ni420cy") (features (quote (("custom_io"))))))

(define-public crate-ezmenu-derive-0.2 (crate (name "ezmenu-derive") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1s8wdhskkpdbjwl5v7s5zrbp6dq8j9nkc8lfl0c3nr8xwnpvr0g5") (features (quote (("parsed_attr") ("derive") ("default" "derive" "parsed_attr"))))))

(define-public crate-ezmenu-derive-0.2 (crate (name "ezmenu-derive") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1mgka2f7k79vrqq0h8v8aqgb49qdz01rlqys5kzpzgk20jpqx79p") (features (quote (("parsed_attr") ("derive") ("default" "derive" "parsed_attr"))))))

(define-public crate-ezmenu-macros-0.2 (crate (name "ezmenu-macros") (vers "0.2.7") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "18fifd9dilw17nxwccbr55d533x38rdy4akwrg7g83ksz7nvznfa") (features (quote (("parsed") ("derive") ("default" "derive" "parsed"))))))

(define-public crate-ezmenulib-0.2 (crate (name "ezmenulib") (vers "0.2.7") (hash "0fy3r0jpb0q5lpi3bdrias3ksqv89yp82vnjh7ady4z23v36jph6")))

(define-public crate-ezmenulib-0.2 (crate (name "ezmenulib") (vers "0.2.9") (hash "0xnqcw9mfb3i54qms8pxblx19hiwzvnpyc87g41xk1ifhiw0q426")))

(define-public crate-ezmenulib-0.2 (crate (name "ezmenulib") (vers "0.2.10") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (optional #t) (default-features #t) (kind 0)))) (hash "0wf1nfxsc5adcjlc8diinnahnpv3rwdiy4g1wqww2i3nchv462d7") (features (quote (("default") ("date" "chrono"))))))

