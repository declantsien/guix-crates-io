(define-module (crates-io ez _a) #:use-module (crates-io))

(define-public crate-ez_al-0.1 (crate (name "ez_al") (vers "0.1.0") (deps (list (crate-dep (name "hound") (req "^3.5.1") (default-features #t) (kind 0)) (crate-dep (name "linear_model_allen") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13qnzybviyg0wz7x0vpylqsc9cijpgp8bqhx11mpk8ndwx0k6nkf")))

(define-public crate-ez_al-0.1 (crate (name "ez_al") (vers "0.1.1") (deps (list (crate-dep (name "hound") (req "^3.5.1") (default-features #t) (kind 0)) (crate-dep (name "linear_model_allen") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "15vq7ys59a9bgcxkas94p91mk60z6vdvby7mpi7qg0rp223chlhx")))

(define-public crate-ez_al-0.2 (crate (name "ez_al") (vers "0.2.0") (deps (list (crate-dep (name "hound") (req "^3.5.1") (default-features #t) (kind 0)) (crate-dep (name "linear_model_allen") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1n634gv8gzxl794j4xjzvirip01ssg67504w6dd296xnx45k2djp")))

(define-public crate-ez_al-0.3 (crate (name "ez_al") (vers "0.3.0") (deps (list (crate-dep (name "linear_model_allen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "wavers") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1wyh2b7jg5350bdk7i5m76akkn5qi5r95gb1fj2ipm31hl2v82il")))

(define-public crate-ez_alphabet-0.1 (crate (name "ez_alphabet") (vers "0.1.0") (deps (list (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.175") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0q9kdz6jd5z40f92rp3kiqvffb90ny6i9a6p2gh1mv76gf55vsfg") (features (quote (("default")))) (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ez_alphabet-0.1 (crate (name "ez_alphabet") (vers "0.1.1") (deps (list (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.175") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1y3nh9afjivldrdi1cpz1xwsjbd4s4chns5sgjm1jj19p9izqzwv") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ez_audio-0.1 (crate (name "ez_audio") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "18768saplwaq744hzn2yn982lzds7c6766d3b49q9h4f9i9kr6lq")))

(define-public crate-ez_audio-0.1 (crate (name "ez_audio") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1vm6x1p7csxmw1904x6674hq06j7gwyi95jfysaz35w9nvh9rjbz")))

(define-public crate-ez_audio-0.1 (crate (name "ez_audio") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1msrnknl6vzn3v1c14ji7hyay37rkjsrchakhfbpr1nvc0ai9jr4")))

(define-public crate-ez_audio-0.1 (crate (name "ez_audio") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "152y8qrq2kg29c82kfpjsqcv91r2s6vhmfn5hanl4xv7q2rnh7al")))

(define-public crate-ez_audio-0.1 (crate (name "ez_audio") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1d7ns8550w89zxz9s4jzfyhwkiy567chp9nsjhng1lcbdnc61mz5")))

(define-public crate-ez_audio-0.1 (crate (name "ez_audio") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0jxzx7w7iv3gyfklfw8ykqxcrpbvdwhdvrxmjd504d4k49myar34")))

(define-public crate-ez_audio-0.1 (crate (name "ez_audio") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0hyavbxqlqwg28q670ybhgirxlnvm9bglf8dyyhv9n4aiqmviz9b")))

