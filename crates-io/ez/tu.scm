(define-module (crates-io ez tu) #:use-module (crates-io))

(define-public crate-eztui-0.0.1 (crate (name "eztui") (vers "0.0.1") (deps (list (crate-dep (name "buffy") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)))) (hash "1sy2ak37q64vkpi8xfa1qs02yw0zh4qd6090hza9ffy55x0lbwwq")))

