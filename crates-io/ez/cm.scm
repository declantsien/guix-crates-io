(define-module (crates-io ez cm) #:use-module (crates-io))

(define-public crate-ezcmd-0.1 (crate (name "ezcmd") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "shell-words") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "10xkd1cjxwg6b3xrg039plg4dny6jvmni57apn2xnv0kxbb535nl")))

(define-public crate-ezcmd-0.2 (crate (name "ezcmd") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "shell-words") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0113c4zxgwwqakk474g2glhqzpbp1m47rhbfdqm44qw2c2ii6skm")))

