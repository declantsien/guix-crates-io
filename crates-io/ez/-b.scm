(define-module (crates-io ez -b) #:use-module (crates-io))

(define-public crate-ez-bitset-0.1 (crate (name "ez-bitset") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ibbivsds8xpy9ld7pqi4wc9izbr2lj1jjcvfj6xqwp4v6dc1pjf")))

(define-public crate-ez-branch-0.1 (crate (name "ez-branch") (vers "0.1.0") (hash "14hwnfkxrdv2k05jy3dg9n21b85j104qi85kkadpv4r6q68l05sz") (yanked #t)))

