(define-module (crates-io ez ex) #:use-module (crates-io))

(define-public crate-ezexec-0.1 (crate (name "ezexec") (vers "0.1.0") (deps (list (crate-dep (name "ebacktrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nn9a9xpzp0j3bq1d5mdcmq61lgrkial4sbln598qar0dqvhsacp") (features (quote (("default"))))))

(define-public crate-ezexec-0.1 (crate (name "ezexec") (vers "0.1.1") (deps (list (crate-dep (name "ebacktrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fa8f7x839i62rm7kpwy3q912swvm7iznfbkfwbq17bcdj5nz527") (features (quote (("default"))))))

(define-public crate-ezexec-0.2 (crate (name "ezexec") (vers "0.2.0") (deps (list (crate-dep (name "ebacktrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mc2qri8dzf1l9db0y6s8wn56cwrfkqxqxdg6wxx5xvkpkxlqvf8") (features (quote (("default"))))))

(define-public crate-ezexec-0.3 (crate (name "ezexec") (vers "0.3.0") (deps (list (crate-dep (name "ebacktrace") (req "^0.5") (default-features #t) (kind 0)))) (hash "1daq6m5h6cjy3xfadwfwkkslyf8c77lkym5wajawq6m65l0gp4s5") (features (quote (("default"))))))

(define-public crate-ezexec-0.4 (crate (name "ezexec") (vers "0.4.0") (hash "0mychbyvid81gh13msbpm7qf4dvy5ns5a9bm82wkqllmg835y415") (features (quote (("default"))))))

(define-public crate-ezexec-0.4 (crate (name "ezexec") (vers "0.4.1") (hash "09m42cm7w4ly5jxld67mc39brv1ik2ccpqil0vr517qzmannnd7s") (features (quote (("default"))))))

