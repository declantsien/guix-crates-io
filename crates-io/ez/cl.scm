(define-module (crates-io ez cl) #:use-module (crates-io))

(define-public crate-ezcli-0.1 (crate (name "ezcli") (vers "0.1.0") (hash "12amxsx1dg7hwl0x0z9fg27m4anfhiplr8ssh4x7cjsas2s6hdp0")))

(define-public crate-ezcli-0.2 (crate (name "ezcli") (vers "0.2.0") (hash "0nd1gr82fjnwazyn0iimqdx5k7nmf6ndl2jcr1ccjmd6bmps1bhi")))

(define-public crate-ezcli-0.3 (crate (name "ezcli") (vers "0.3.0") (hash "1ini2dav1dywarczlpdlqfikh52jvisrfn9yx9gb093anvf8wi8w")))

(define-public crate-ezcli-0.3 (crate (name "ezcli") (vers "0.3.1") (hash "1slhjxgyjjsv8cazm1wj1p8cqbysgr5ap6ynd3n6axjbafijirhh")))

(define-public crate-ezcli-0.3 (crate (name "ezcli") (vers "0.3.2") (hash "1rkqsgch7i6n9ns64lg0sc2p4sr4drl6j07fsh6sgzy45i5sijfl")))

(define-public crate-ezcli-0.3 (crate (name "ezcli") (vers "0.3.3") (hash "1j162zmz6br2dpmbni1n2nzb66h5j74ah9k2qa9wg2m76zmjg0as")))

(define-public crate-ezcli-0.3 (crate (name "ezcli") (vers "0.3.4") (hash "01297dfp942fd23karpml5w901ha4pzgjr7z7cclry03vg05i32l")))

(define-public crate-ezcli-0.4 (crate (name "ezcli") (vers "0.4.0") (hash "02n174msjznjc7zsnm1pkvfrbfdgh1kvy6gvszbk8az6csg4dlw6")))

