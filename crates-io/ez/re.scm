(define-module (crates-io ez re) #:use-module (crates-io))

(define-public crate-ezregexp-0.0.1 (crate (name "ezregexp") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 2)) (crate-dep (name "regex-syntax") (req "^0.6.21") (default-features #t) (kind 0)))) (hash "19c3wvakb7b3x3xniaabx0kpqqqsp7iq8n0ccdh0p4n4yrzakvps")))

(define-public crate-ezrest-0.0.1 (crate (name "ezrest") (vers "0.0.1-beta-1") (deps (list (crate-dep (name "rocket") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9.2") (default-features #t) (kind 1)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 1)))) (hash "0rvnylgg11prbmdbhyhy85hpffps6rf6z0dafrg2blapa4ma3pdy")))

(define-public crate-ezrest-0.0.1 (crate (name "ezrest") (vers "0.0.1-beta-2") (deps (list (crate-dep (name "rocket") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9.2") (default-features #t) (kind 1)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 1)))) (hash "02bhw139v78ffn1yi5k7i31ypavrjqzyyinx87778ijkbywyjq7b")))

