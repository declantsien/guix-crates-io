(define-module (crates-io ez -e) #:use-module (crates-io))

(define-public crate-ez-err-0.1 (crate (name "ez-err") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "0xzr3j7ywiw95rymb9vvbdjdhpk26j3hqdi9xbpqyl4knkpvm0d0") (features (quote (("no_stacktrace") ("default")))) (yanked #t) (v 2) (features2 (quote (("log" "dep:log")))) (rust-version "1.31")))

(define-public crate-ez-err-0.1 (crate (name "ez-err") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "0ap9q0x3xzfqpyv1jx4kmlwxwqld0bayjzb3vi20p10bilmzaf0h") (features (quote (("no_stacktrace") ("default")))) (yanked #t) (v 2) (features2 (quote (("log" "dep:log")))) (rust-version "1.31")))

(define-public crate-ez-err-0.1 (crate (name "ez-err") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "1mh9px2fz4dlmsmqaf7m0hpdcnka6jz4fkn9hgj5rayl5s47k9nh") (features (quote (("no_stacktrace") ("default")))) (yanked #t) (v 2) (features2 (quote (("log" "dep:log")))) (rust-version "1.31")))

(define-public crate-ez-err-0.1 (crate (name "ez-err") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)))) (hash "0ks5crl01qspmk7bjp01v49kzkbknsqf745dwpvz5cc29vzk9qxb") (features (quote (("no_stacktrace") ("default")))) (v 2) (features2 (quote (("log" "dep:log")))) (rust-version "1.31")))

(define-public crate-ez-ezio-0.0.5 (crate (name "ez-ezio") (vers "0.0.5-dev") (deps (list (crate-dep (name "eyre") (req "^0.6.6") (default-features #t) (kind 0)) (crate-dep (name "ez") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "082m16pxqf5sz6s56iisf9mpvjl7y1ryz424i1y2vhswa0xwa7wk") (yanked #t)))

(define-public crate-ez-ezio-0.0.5 (crate (name "ez-ezio") (vers "0.0.5-dev.1") (deps (list (crate-dep (name "eyre") (req "^0.6.6") (default-features #t) (kind 0)) (crate-dep (name "ez") (req "^0.0.5-dev") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "001laprcrkl67cxz81fc3jhqljgs9b4v1yby4qdkc7zl8kk7ib30") (yanked #t)))

(define-public crate-ez-ezio-0.0.5 (crate (name "ez-ezio") (vers "0.0.5-dev.2") (deps (list (crate-dep (name "eyre") (req "^0.6.6") (default-features #t) (kind 0)) (crate-dep (name "ez") (req "^0.0.5-dev") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1zka4bzgcaxbfhv54by6rwbgavbcsy1kxgrsnry8xr69w1v0f0za") (yanked #t)))

(define-public crate-ez-ezio-0.0.5 (crate (name "ez-ezio") (vers "0.0.5") (deps (list (crate-dep (name "eyre") (req "^0.6.6") (default-features #t) (kind 0)) (crate-dep (name "ez") (req "=0.0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1dbas2wyf7l9p1cvy7jb2gjna2h3hkz79r1wpkvzjh11nr355jd3") (yanked #t)))

(define-public crate-ez-ezio-0.0.6 (crate (name "ez-ezio") (vers "0.0.6") (deps (list (crate-dep (name "eyre") (req "^0.6.6") (default-features #t) (kind 0)) (crate-dep (name "ez") (req "=0.0.5") (default-features #t) (kind 0) (package "ez")) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1piqr5wv59cp428j77j8qgx1lpjaf8xq28262fv0sd6q0z4s525s") (yanked #t)))

(define-public crate-ez-ezio-0.0.0 (crate (name "ez-ezio") (vers "0.0.0--") (hash "0lzy27za32ji9k5yk0686xbl82aq7m2jggvpb2735ia5lnkwhg3j") (yanked #t)))

