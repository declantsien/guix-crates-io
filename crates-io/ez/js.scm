(define-module (crates-io ez js) #:use-module (crates-io))

(define-public crate-ezjs-0.1 (crate (name "ezjs") (vers "0.1.0") (hash "1hy0msij8k65c669gjaj7k10kjh42qjay9iknqgcc5q47if7ybvv")))

(define-public crate-ezjs-0.1 (crate (name "ezjs") (vers "0.1.1") (hash "1qv53jdbwfi20mljp9i34spy0nmvznrnmn1qx5j0qch74fwx1ppl")))

(define-public crate-ezjson-0.1 (crate (name "ezjson") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1dvmn117k7mk1lxc650cx172bi0dkkdapmg12p9g87kmd6gvcw1i")))

(define-public crate-ezjson-0.1 (crate (name "ezjson") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1v0b2iaghsi5k5qx2c4bwr8cxdifr13pbmj00jnbkv05c0c0b5kg")))

(define-public crate-ezjson-0.2 (crate (name "ezjson") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "090h2cm0x6pffjas94w50yqw6x90wh04dy3yc8xvarda4xp78rk7")))

(define-public crate-ezjsonrpc-0.1 (crate (name "ezjsonrpc") (vers "0.1.0") (deps (list (crate-dep (name "ezjsonrpc-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.35") (features (quote ("raw_value"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1l18hzzsxg05dnkj9ga5657w9xf67pkd7sb59hbmq0iiivm2kr8j")))

(define-public crate-ezjsonrpc-0.1 (crate (name "ezjsonrpc") (vers "0.1.2") (deps (list (crate-dep (name "ezjsonrpc-macros") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.35") (features (quote ("raw_value"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "063vh9zagxb83kaff1c1pxcwkjfg8ksffyfzvy2gbl2v3bqskyg1")))

(define-public crate-ezjsonrpc-macros-0.1 (crate (name "ezjsonrpc-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1zc6riqr3fhvglg9mdab9qs6d9pchhk1g6gw4sgrbm73h69vm3dp")))

(define-public crate-ezjsonrpc-macros-0.1 (crate (name "ezjsonrpc-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0hx8z5yv4h3favr1pd9db3vym8nzlq42qkahn2cif69gbq8rfy1p")))

