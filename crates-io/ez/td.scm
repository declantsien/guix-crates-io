(define-module (crates-io ez td) #:use-module (crates-io))

(define-public crate-eztd-0.0.1 (crate (name "eztd") (vers "0.0.1") (deps (list (crate-dep (name "eztd-core") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0s88i334d16k9ah6jx7kz3hb5najknvqj96diwckv9k892hrxnv1")))

(define-public crate-eztd-0.0.2 (crate (name "eztd") (vers "0.0.2") (deps (list (crate-dep (name "eztd-core") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "19srmb8jvn5pv4djmzd7ahm6a0h4bys9j40xx3vg5qpma8zdvvdp")))

(define-public crate-eztd-0.0.3 (crate (name "eztd") (vers "0.0.3") (deps (list (crate-dep (name "eztd-core") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1zrxrdf5615wqq277jhadgdhhg73znqhxvss05mhzqh4w7vzbl4x")))

(define-public crate-eztd-core-0.0.1 (crate (name "eztd-core") (vers "0.0.1") (hash "1rzrm6x6rp0djwgwvn6vik47amwdak5kwlzg75jk9k91c96n6fk2")))

(define-public crate-eztd-core-0.0.2 (crate (name "eztd-core") (vers "0.0.2") (hash "1lqgwaxm8y2vmsf9lhkjgpi4pss5qd96g7ryi7ql1nimjbn6q0j8")))

(define-public crate-eztd-core-0.0.3 (crate (name "eztd-core") (vers "0.0.3") (hash "06c9jk2pfl0dqqznq8wj2xpkzxqkbjv4fkkrak7ybzznzjgrx8fj")))

