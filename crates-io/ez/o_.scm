(define-module (crates-io ez o_) #:use-module (crates-io))

(define-public crate-ezo_i2c_rs-0.1 (crate (name "ezo_i2c_rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0h0d7jbkbgngbhnbl9jila5zpbs7jhz60wpdpxw898sf7r3mg11n")))

(define-public crate-ezo_i2c_rs-0.2 (crate (name "ezo_i2c_rs") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0hfxpcdmqrv0jz7c2narr4nhw6cq0fc7j6q02d8mb0k8vmd1pc8g")))

