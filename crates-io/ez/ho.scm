(define-module (crates-io ez ho) #:use-module (crates-io))

(define-public crate-ezhook-0.1 (crate (name "ezhook") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (target "cfg(unix)") (kind 2)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 2)))) (hash "0x0jgxjb18n1znhqv00hj8ysnhcs4l7zbbi4gj9q475w745cb9v2")))

(define-public crate-ezhook-0.2 (crate (name "ezhook") (vers "0.2.0") (deps (list (crate-dep (name "lde") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (target "cfg(unix)") (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 2)))) (hash "0ixkbcb7ar4lh9rfgid71g6f9ww8ja16vcdiqp3njrxz3cjyzr8n") (features (quote (("trampoline" "lde"))))))

(define-public crate-ezhook-0.2 (crate (name "ezhook") (vers "0.2.1") (deps (list (crate-dep (name "lde") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (target "cfg(unix)") (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 2)))) (hash "1gz358d6wfd6k6kmp6x7x9b6kmyg4gkg143c5mxbxn1rxs4w112z") (features (quote (("trampoline" "lde"))))))

(define-public crate-ezhook-0.2 (crate (name "ezhook") (vers "0.2.2") (deps (list (crate-dep (name "lde") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi"))) (default-features #t) (target "cfg(windows)") (kind 2)))) (hash "066ihmby2p2ni4wa77akyx6ssrwybm0rp0pv6mmha9k5mjykl5r3") (features (quote (("trampoline" "lde"))))))

