(define-module (crates-io ez oa) #:use-module (crates-io))

(define-public crate-ezoauth-0.1 (crate (name "ezoauth") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "oauth2") (req "^4.0.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "04krsfyxq90p4mwbyw5xfxrpi4nx6cnvdiwpq3n4wzm24021xfar")))

(define-public crate-ezoauth-0.2 (crate (name "ezoauth") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "oauth2") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "15dx1sg7hmi0zvia1x5h8gqg1ds4yswlffwyif7nrpymn5ljcxhc")))

