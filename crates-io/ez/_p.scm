(define-module (crates-io ez _p) #:use-module (crates-io))

(define-public crate-ez_pqcrypto-0.1 (crate (name "ez_pqcrypto") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "pqcrypto") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0cyjfj5djbr3jsmgnn3fn017cq61607r74lcv4lbjlxsc2hyxbxd") (yanked #t)))

