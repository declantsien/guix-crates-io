(define-module (crates-io ez co) #:use-module (crates-io))

(define-public crate-ezconf-0.1 (crate (name "ezconf") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "toml-query") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0b8sqf1hy0d6b2bmj9ihca1khv47cky1y1w622sc94sbgrxws4s1")))

(define-public crate-ezconf-0.2 (crate (name "ezconf") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "toml-query") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1kiajhv78rvpmp0jhsfl44dmamcb8qg4qryl6fv0hr1ssb28aq0c")))

(define-public crate-ezconf-0.3 (crate (name "ezconf") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.5.13") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "toml-query") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "15x85awqx7k2sz9vd9av4js08qrvmmcblfv9smr1adk2xwwb84wa")))

(define-public crate-ezconfig-0.1 (crate (name "ezconfig") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "05kjjnaczpbc7hikmzwhh19sgqy518b6hddxc1pqwfmrfi8pn18k") (yanked #t)))

(define-public crate-ezconfig-0.2 (crate (name "ezconfig") (vers "0.2.0") (deps (list (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1g0y0g14p3j7rrh9mby9kglx4rhlcmiada5gf6dkcga6qc7i10vg") (yanked #t)))

(define-public crate-ezconfig-0.2 (crate (name "ezconfig") (vers "0.2.1") (deps (list (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0c4j2ng8q9l1y79zcfihkchn6q5rg1sfwsrd2w5jsaakn7pk725d") (yanked #t)))

