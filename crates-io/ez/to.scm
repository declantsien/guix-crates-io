(define-module (crates-io ez to) #:use-module (crates-io))

(define-public crate-eztotp-0.1 (crate (name "eztotp") (vers "0.1.0") (deps (list (crate-dep (name "google-authenticator") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jpvxn1sl8ilx9255yp5lmsl0y63dgpdkssaz2fmq4kxqkinyd6i")))

