(define-module (crates-io si gq) #:use-module (crates-io))

(define-public crate-sigq-0.9 (crate (name "sigq") (vers "0.9.0") (hash "09804gf84hllr84smmi9b9l7s9l4spyqgm813ma511pq2banqgv3")))

(define-public crate-sigq-0.9 (crate (name "sigq") (vers "0.9.1") (hash "1g0ckys33i2b03hnwqkqxiadq7n56lc1cb6jv1rljs3hrfkyjvjv")))

(define-public crate-sigq-0.10 (crate (name "sigq") (vers "0.10.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "1hwzwgffrwnkn3cdn378sbb8kjc6gvzhjkg5wwglxhslzakk0m86") (yanked #t)))

(define-public crate-sigq-0.10 (crate (name "sigq") (vers "0.10.1") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0chxcqbi8r76gxxjkny82wcb5hwpgyj7szws5qg355aln6kw5jis")))

(define-public crate-sigq-0.10 (crate (name "sigq") (vers "0.10.2") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "19v2fjy8ikp6inzniz04ir6cdrwsyfn3iq2m4vp7f0syfadla7fz")))

(define-public crate-sigq-0.11 (crate (name "sigq") (vers "0.11.0") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "09vk3cxrfi68iyqcpc71d62p8hjysm8zn6r6dsnh9k9lvmprkjwn") (rust-version "1.36")))

(define-public crate-sigq-0.12 (crate (name "sigq") (vers "0.12.0") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0h5gnjpbx4ckc0pxynawy04jgxyj8b561y6g0jm79djk2dd2k3cs") (features (quote (("inline-more") ("default" "inline-more")))) (rust-version "1.64")))

(define-public crate-sigq-0.13 (crate (name "sigq") (vers "0.13.0") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "108xjmdcrfxpippbphh0ym64g13m3r3c4abh5adb063v33g6divc") (features (quote (("inline-more") ("default" "inline-more")))) (rust-version "1.64")))

(define-public crate-sigq-0.13 (crate (name "sigq") (vers "0.13.1") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0i9rlmi2b4smpwjf8dpv8bxmgsnq3rdaa4vq0zwkp5b0qyx9143d") (features (quote (("inline-more") ("default" "inline-more")))) (rust-version "1.64")))

(define-public crate-sigq-0.13 (crate (name "sigq") (vers "0.13.2") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0n4kz7b95bx4w0hf8giidczgnr3iwbakihpb5wbfqva9pjh9r5ql") (features (quote (("inline-more") ("default" "inline-more")))) (rust-version "1.64")))

(define-public crate-sigq-0.13 (crate (name "sigq") (vers "0.13.3") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1p0sf63jlpn2f49jldzkihf6j04lqvpa99vy9r41x9sc20wqwaf0") (features (quote (("inline-more") ("default" "inline-more")))) (rust-version "1.64")))

(define-public crate-sigq-0.13 (crate (name "sigq") (vers "0.13.4") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0hq6m1j4gfc5y1lmb1aq8z9pq4q9zy4qynbk3ckzfp62lf7nqhlr") (features (quote (("inline-more") ("default" "inline-more")))) (rust-version "1.64")))

