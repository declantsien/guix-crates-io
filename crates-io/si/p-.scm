(define-module (crates-io si p-) #:use-module (crates-io))

(define-public crate-sip-codec-0.1 (crate (name "sip-codec") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)) (crate-dep (name "tokio-codec") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17jsjlc53x99vddynzj8fm35lm6q2vbfxpsrm19ylqm1jfw8l6z4")))

