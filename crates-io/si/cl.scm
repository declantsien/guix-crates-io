(define-module (crates-io si cl) #:use-module (crates-io))

(define-public crate-siclibrs-0.1 (crate (name "siclibrs") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1wvhb5v00ww7lnd54sb9qmvh8qprn95wxgrf517wrns69mdzxa2k")))

