(define-module (crates-io si ph) #:use-module (crates-io))

(define-public crate-siph-0.1 (crate (name "siph") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3.10") (default-features #t) (kind 0)))) (hash "19r3dvljfp9d7v4yx4gxmzi7yphp9shs0np64vssfa6xgg1kqh90")))

(define-public crate-siphan-0.1 (crate (name "siphan") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1372l7qwjlqsv1ds6zfxppxvxva1avh67947aacnb7diy9vp5nqa")))

(define-public crate-siphan-0.1 (crate (name "siphan") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0mg7dvigdqx0yh7ms29lgr5pa0j2ky0b3czirm64hn7ih3hd1mk6")))

(define-public crate-siphash-0.0.0 (crate (name "siphash") (vers "0.0.0") (hash "1rf3y3g3bxzys9vc5ipc3q1crack8z6f2l94rpf8s7lg7sjg1706")))

(define-public crate-siphash-0.0.2 (crate (name "siphash") (vers "0.0.2") (hash "0n2h8cgm1zjrlgrbwysfdqxnsz5p3wvmjwk68cy40ixasg9g39a7")))

(define-public crate-siphash-0.0.4 (crate (name "siphash") (vers "0.0.4") (hash "02anvappm6vsz22m9d8sra8bvilc1fha9y6j69sjh31idviry47n")))

(define-public crate-siphash-0.0.5 (crate (name "siphash") (vers "0.0.5") (hash "1bqmkb2d9yrjzrvzzbahyc335c1nksb3ddfj4p9ypr6q63m51clp")))

(define-public crate-siphash_c_d-0.1 (crate (name "siphash_c_d") (vers "0.1.0") (hash "11afxz6d0pg3q92rww6r28i3sl73zddrg9b8503vqln66l76x5k3") (rust-version "1.56")))

(define-public crate-siphasher-0.1 (crate (name "siphasher") (vers "0.1.0") (hash "14g0p15imr2ijamfg75vd2jwx7xdnyrjgr79nbsi2fzcmv8jxgj1")))

(define-public crate-siphasher-0.1 (crate (name "siphasher") (vers "0.1.1") (hash "1k9zaglh4pngccsz6axy0rc15hr7ri07gkw94iwaidc7l4py8i2w")))

(define-public crate-siphasher-0.1 (crate (name "siphasher") (vers "0.1.2") (hash "1whp0v772nvg6jwymy1b9kv149mf27pxhszy344m6g64ka6cbhxi")))

(define-public crate-siphasher-0.1 (crate (name "siphasher") (vers "0.1.3") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)))) (hash "0v9z3lsn7gppcv6q2qx05wqava95sfcwccldfwb8zn3bab512c43")))

(define-public crate-siphasher-0.2 (crate (name "siphasher") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)))) (hash "0f5kgypcpk16ss2f0mhn0922a9sdmxp5ika98ppb7m5fh44168ir")))

(define-public crate-siphasher-0.2 (crate (name "siphasher") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)))) (hash "112vnr8382vdgcylr8r21vvnpfjn7gifarpzpjiwjavgfadndz1g")))

(define-public crate-siphasher-0.2 (crate (name "siphasher") (vers "0.2.2") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)))) (hash "0dv5sf92lskk8fzirngbknnwidvxsi0h9rb7b8ix1s3kh1w0my8d")))

(define-public crate-siphasher-0.2 (crate (name "siphasher") (vers "0.2.3") (hash "1b53m53l24lyhr505lwqzrpjyq5qfnic71mynrcfvm43rybf938b")))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.0") (hash "1a03vzazh0wpds6nmclpnkq3z1mj1dmh3738z81lmn2pyrfwf4wr")))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.1") (hash "17cj2ynbv5zs7fa8ylrw7a6pb260q53ccj091mj9xa6ix0745nl3")))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "08xvk3yi4vawppm1f81s4zrkksf95psz8gczh36y808candgi24f") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1dw648sij9rarz5rk58zrjmxm70c6xl60d6rkd8pabipqx0kg3zs") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1hblmvdv32qmndczdjlbxal6xxvpic7pzsbx6pn96mimabcv2pja") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "09zzzdshn4rj1qkhsb9hz9qwp42jx5b6whwi42ba7r670x2nvknb") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "18bivn9rpmpxcdb04znpwz9x52m8zm25v5a7rdl6rc3jgp0jb6kj") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "12wkdsvxq8ljckpsh92ri52w8z0gh32cclxb4lvd695pz6l98d2k") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.8") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "12lh4h6q2x3pg73mf7bsms3gc1x0jsgq7rz95pha59aax7cyl7ms") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.9") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ghadn6g0mmmmcak07336fn7lsrz0yky9nidgxyjhwgsc2mk4qm8") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.10") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1pi5sb2j2wi92zfqj6qxnk11vk1qq2plya5g2a5kzbwrd0hf7lvv") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-0.3 (crate (name "siphasher") (vers "0.3.11") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "03axamhmwsrmh0psdw3gf7c0zc4fyl5yjxfifz9qfka6yhkqid9q") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-1 (crate (name "siphasher") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zk1415ihmzvig5y56gsakv126ck84nl2nz53dr91lybkhllbb2l") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphasher-1 (crate (name "siphasher") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "17f35782ma3fn6sh21c027kjmd227xyrx06ffi8gw4xzv9yry6an") (features (quote (("std") ("serde_std" "std" "serde/std") ("serde_no_std" "serde/alloc") ("default" "std"))))))

(define-public crate-siphon-0.0.1 (crate (name "siphon") (vers "0.0.1") (hash "1fzzdby3053nwcsmz98s43l6mrn3401sik6n8vy11q5wf9h20z3b")))

