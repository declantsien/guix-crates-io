(define-module (crates-io si gr) #:use-module (crates-io))

(define-public crate-sigrok-0.1 (crate (name "sigrok") (vers "0.1.0") (deps (list (crate-dep (name "sigrok-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1gij3aha7f3rr5d7cwmmwq4dir66r41ym4kqi90phr70mk45bx5g")))

(define-public crate-sigrok-0.2 (crate (name "sigrok") (vers "0.2.0") (deps (list (crate-dep (name "glib-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sigrok-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1s1almxbplggjh55xbkv727xr24q26y05h1fdr1nhgl3c559hvdy")))

(define-public crate-sigrok-0.3 (crate (name "sigrok") (vers "0.3.0") (deps (list (crate-dep (name "glib-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sigrok-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vdg130jcjgvbpbjmaby51y2g39hcj76y5ijpjzf0vn6rmx328xq")))

(define-public crate-sigrok-0.3 (crate (name "sigrok") (vers "0.3.1") (deps (list (crate-dep (name "glib-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sigrok-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lwxsv2mjr4mph49xy5574xdq1q6fbdi4zlww11r36jzl8vcjp7z")))

(define-public crate-sigrok-sys-0.1 (crate (name "sigrok-sys") (vers "0.1.0") (deps (list (crate-dep (name "glib-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1fqm09530bha3fsbpqf9xy0q7jrx0ygb3phx0nh00yd6yk9zph48")))

(define-public crate-sigrok-sys-0.1 (crate (name "sigrok-sys") (vers "0.1.1") (deps (list (crate-dep (name "glib-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1gsdf2fpa75ydsdcgs9kpgzacz2izn56brqsc79vaz6i5amr5rpd")))

(define-public crate-sigrok-sys-0.2 (crate (name "sigrok-sys") (vers "0.2.0") (deps (list (crate-dep (name "glib-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "0snhiz7ybw5p7yy15zvdskvjycjd0mgcxys59fmnl37yiqxddgzg")))

(define-public crate-sigrs-0.1 (crate (name "sigrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.3") (default-features #t) (kind 0)) (crate-dep (name "grep") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "promkit") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "strip-ansi-escapes") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.11") (default-features #t) (kind 0)))) (hash "1nvhbimrncn7430nrvbwqjrydl7gxnvkwc3b528xnijl2gqwpz99")))

