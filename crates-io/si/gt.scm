(define-module (crates-io si gt) #:use-module (crates-io))

(define-public crate-sigtransplant-0.1 (crate (name "sigtransplant") (vers "0.1.0") (deps (list (crate-dep (name "goblin") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "scroll_derive") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "08hn6v5j6x78nmhr6j6k9z0ym4aa7r27iya5qh7dnrvcgsm9zw0j")))

