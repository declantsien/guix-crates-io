(define-module (crates-io si d-) #:use-module (crates-io))

(define-public crate-sid-cli-0.2 (crate (name "sid-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sid2") (req "^0.2.0") (features (quote ("uuid"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "02z09wazbi7pvgpqf0fd361b4kjqvwp6i951cm2ynmr3jak2kpz4")))

(define-public crate-sid-cli-0.3 (crate (name "sid-cli") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sid2") (req "^0.3.0") (features (quote ("uuid"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1aar2ahagrpfq0y8dib83xhgpp67x2hafbp4ifn06bp6spbr5xlv")))

(define-public crate-sid-encode-0.1 (crate (name "sid-encode") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1885j8hlaqwl0r5z852x5qxywzn94pq9fvd28bfcfpld53616ri6")))

(define-public crate-sid-encode-0.1 (crate (name "sid-encode") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0bvm1gd6sl4isdcvjyapay4sxbr5y0vq3kq6ncfbpc6nabgy58x5")))

(define-public crate-sid-encode-0.2 (crate (name "sid-encode") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1q22v0044zmcjkgv2dgcmr9mk88ln28p4a83qwwidi8m1v9g3lj8")))

