(define-module (crates-io si #{47}#) #:use-module (crates-io))

(define-public crate-si4703-0.1 (crate (name "si4703") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "004f5xzqnacjn8jw9pbx9y16lqf8cmwna851snjj5dhc6y8qqxnx")))

