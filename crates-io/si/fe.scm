(define-module (crates-io si fe) #:use-module (crates-io))

(define-public crate-sife-0.1 (crate (name "sife") (vers "0.1.0") (hash "01c6pqnlmnyvczcflmxjzbnvnc42yb1nj70i5dy92g9as3ir4h1j")))

(define-public crate-sifed-0.1 (crate (name "sifed") (vers "0.1.0") (hash "09kxgil3a9b703cqc351aknnx3p57zkq0g1v9vld0r8avmnsiywj")))

(define-public crate-sifer-0.1 (crate (name "sifer") (vers "0.1.0") (hash "1rnvczfgfsm1n5fh55hj9yad18i01lr8d4ycvjw8fzns95hh8h4a")))

