(define-module (crates-io si rp) #:use-module (crates-io))

(define-public crate-sirp-0.1 (crate (name "sirp") (vers "0.1.0") (hash "1xdg2dqdfmxhwyin978541av4szz1w89vc0fjrlnik822xm10nhr")))

(define-public crate-sirp-0.1 (crate (name "sirp") (vers "0.1.1") (deps (list (crate-dep (name "number-theory") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0av4h7vw6yn7yk2irwa0x1wl5n8dp1bcf395pyannncn4qycxb2a")))

