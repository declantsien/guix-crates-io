(define-module (crates-io si x_) #:use-module (crates-io))

(define-public crate-six_viewer-0.1 (crate (name "six_viewer") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "nu-ansi-term") (req "^0.45.1") (default-features #t) (kind 0)))) (hash "0m1w4wdrimbrv8mz6cdjbfwi61ip7ir0a60vmhcgbzq47b0jf5dl")))

(define-public crate-six_viewer-0.1 (crate (name "six_viewer") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "nu-ansi-term") (req "^0.45.1") (default-features #t) (kind 0)))) (hash "15sd98a3ab314a1qa5xcdnll2ci8s05pi5viqznlcr34w7rixc5c")))

