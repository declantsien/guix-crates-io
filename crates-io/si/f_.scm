(define-module (crates-io si f_) #:use-module (crates-io))

(define-public crate-sif_macro-0.1 (crate (name "sif_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0p68j8vsh6f7mnnfffylv00p0vxnlrwpx29l3gzbvxwjhrqma9b3")))

