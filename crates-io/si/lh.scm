(define-module (crates-io si lh) #:use-module (crates-io))

(define-public crate-silhouette-0.1 (crate (name "silhouette") (vers "0.1.0") (deps (list (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "try_default") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0cspmanij7nxnyyyc3zr5906cf6n43zwjmgxj0ba0v4s721lkn1j") (features (quote (("default")))) (v 2) (features2 (quote (("nightly" "dep:try_default"))))))

