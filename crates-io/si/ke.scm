(define-module (crates-io si ke) #:use-module (crates-io))

(define-public crate-sike-1 (crate (name "sike") (vers "1.0.0") (hash "08cp61xgz15zdv97482ir4va3w33sz5mhr70q1qc6qshnzabls7i")))

(define-public crate-sike-2 (crate (name "sike") (vers "2.0.0") (hash "1cjmz25sq1az21am5dixcygyncgrkc4wgcprbr8haawpi6s1vql2")))

(define-public crate-sike-2 (crate (name "sike") (vers "2.1.0") (hash "0lh9qdanfqicwjsrzxjv0rla0z9l1iyzm0ij9wipv07gys9qyynd")))

