(define-module (crates-io si in) #:use-module (crates-io))

(define-public crate-siin-0.1 (crate (name "siin") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (default-features #t) (kind 0)) (crate-dep (name "trauma") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "validator") (req "^0.16") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1a5nnfc8pg1w31pjyaakks3lkym4i6rm59s5ybkvj6plab2fqxwj")))

