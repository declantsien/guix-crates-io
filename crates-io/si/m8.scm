(define-module (crates-io si m8) #:use-module (crates-io))

(define-public crate-sim800-0.0.1 (crate (name "sim800") (vers "0.0.1") (hash "1c5b3jdza6nkpy7f9r6qig6wlspr74z2kxh8wzxnh78214shlpv4")))

(define-public crate-sim800-0.0.2 (crate (name "sim800") (vers "0.0.2") (hash "1f48dw1fzjqgdqg4ksx5ldn19dl60vccmfwgd5d8zkb1bhrhg8ch")))

(define-public crate-sim86-0.1 (crate (name "sim86") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "console") (req "=0.15.5") (default-features #t) (kind 2)) (crate-dep (name "similar") (req "=2.2.1") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "=1.3.0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1vpwv6hqbqn175y3pd3ah9wkkxnm85g3sv8k14va80i6dbk6iwfa")))

