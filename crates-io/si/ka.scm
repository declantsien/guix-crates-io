(define-module (crates-io si ka) #:use-module (crates-io))

(define-public crate-sika-0.0.0 (crate (name "sika") (vers "0.0.0") (hash "1y0ri07ignhq5672942mn5qb47lgm89mbl9l4v83v0klnswbx4mr") (yanked #t)))

(define-public crate-sika-admin-0.0.0 (crate (name "sika-admin") (vers "0.0.0") (hash "145jbzdhq44zc4nr1qxrx7b04r0dckgws3wchs10pcvf7d8dvbwv") (yanked #t)))

(define-public crate-sika-admin-0.0.1 (crate (name "sika-admin") (vers "0.0.1") (hash "0yz12z148nxj9yld33kjs416y6c7bnpdks5kpfpsvg2p9a0w5hnh") (yanked #t)))

(define-public crate-sika-asset-loader-0.1 (crate (name "sika-asset-loader") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "walker") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0z0d1filrdnsmsyqg6n4rjz2fqhri57prhcjvwr84qxshv34aqsf") (features (quote (("default" "actix-web") ("actix-web")))) (yanked #t)))

