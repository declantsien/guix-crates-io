(define-module (crates-io si lt) #:use-module (crates-io))

(define-public crate-silt-lua-0.1 (crate (name "silt-lua") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "1x29gm6p3vkjd5a10kb14p3bzcg8qwxj5hk45v66vqgshgpv3776") (features (quote (("under-number") ("silt" "bang" "under-number" "global" "implicit-return" "short-declare") ("short-declare") ("implicit-return") ("global") ("dev-out") ("default") ("bang"))))))

(define-public crate-silt-lua-0.1 (crate (name "silt-lua") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "1imxa85j78xx4m3q95yyri775fxwpagn8kaqw48qkjxm5mpyyw1f") (features (quote (("under-number") ("silt" "bang" "under-number" "global" "implicit-return" "short-declare") ("short-declare") ("implicit-return") ("global") ("dev-out") ("default") ("bang"))))))

