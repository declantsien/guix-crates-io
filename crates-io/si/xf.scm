(define-module (crates-io si xf) #:use-module (crates-io))

(define-public crate-sixfiveohtwo-0.0.1 (crate (name "sixfiveohtwo") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "15n9h30l5pnx54x0hqsv137fqiyi1k37hgby9as5nlbr5ybc7p78")))

