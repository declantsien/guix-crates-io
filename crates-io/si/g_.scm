(define-module (crates-io si g_) #:use-module (crates-io))

(define-public crate-sig_fig_histogram-0.1 (crate (name "sig_fig_histogram") (vers "0.1.0") (hash "1zxyms14azzjdwg23fswci9v0gzj1p9hqzxxn5jf9byrm9lc31ql")))

(define-public crate-sig_fig_histogram-0.2 (crate (name "sig_fig_histogram") (vers "0.2.0") (hash "01idbafvn9i307qihb9dgm15hmvj6qwwbkhbbgq9s0zc9644kbyb")))

