(define-module (crates-io si #{70}#) #:use-module (crates-io))

(define-public crate-si7021-0.1 (crate (name "si7021") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "i2csensors") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "138bb7rskmz92xrcsc343mcz3qir6f9a5acl9z5acf65kra09zgb")))

(define-public crate-si7021-0.2 (crate (name "si7021") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "i2csensors") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "00drkg6b3kg4lw1x2124rbn9zk4rpdd61l5fnsqcynv3xywh3nix")))

(define-public crate-si70xx-0.1 (crate (name "si70xx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "=1.0.0-rc.1") (optional #t) (default-features #t) (kind 0)))) (hash "0yf7rd9idm4v384wfxy0knh5ad7fq8insvgqsl453gxb4ik0abcw") (features (quote (("si7013") ("default")))) (v 2) (features2 (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-si70xx-0.2 (crate (name "si70xx") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "=1.0.0-rc.3") (optional #t) (default-features #t) (kind 0)))) (hash "02p8sfwi3nnck93mbvb51nmamgp5vwb61m6dnas190ibkxrdgmm5") (features (quote (("si7013") ("default")))) (v 2) (features2 (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-si70xx-0.3 (crate (name "si70xx") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "00hvyak3i8xdng9w8m7lwcjy3nwdgsr6raxpi23c59b1799i4gqf") (features (quote (("si7013") ("default")))) (v 2) (features2 (quote (("async" "dep:embedded-hal-async"))))))

