(define-module (crates-io si #{53}#) #:use-module (crates-io))

(define-public crate-si5351-0.1 (crate (name "si5351") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i5ixr5cc6vvrkc89zwsvmyy0qldblzx5n0ym5gyks5qgyl25z4d")))

(define-public crate-si5351-0.1 (crate (name "si5351") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1") (default-features #t) (kind 0)))) (hash "04s992bywcrva30hakf1gji3nqzf03hj0bnsqb76mrjdaz44zgyv")))

(define-public crate-si5351-0.1 (crate (name "si5351") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rxd78qmzdn7aa8g5an9cbppnacy9gprw84kr4nrhajvdc9ln838")))

(define-public crate-si5351-0.1 (crate (name "si5351") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1") (default-features #t) (kind 0)))) (hash "17441l616yxla835vgin90fnzlg27zs6vv0bpkxivfj5ddgdrp3g")))

(define-public crate-si5351-0.2 (crate (name "si5351") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1f5zx86bfvci6gns2fkj5dmiib03acx9bzzxwly9cr8150giyv60")))

