(define-module (crates-io si nf) #:use-module (crates-io))

(define-public crate-sinfo-0.1 (crate (name "sinfo") (vers "0.1.0") (hash "0zhqmn860hklifr8wb544bkb3p64jdn0kfj67vymha01pyfljf5j")))

(define-public crate-sinfo-0.1 (crate (name "sinfo") (vers "0.1.1") (hash "0xgx3gvxjypyi093nhzcbrll8i0s36jqwi6qb8jmn5zwh86yg543")))

(define-public crate-sinfo-0.1 (crate (name "sinfo") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0dzmdgs6s5992lsm70x8fyrpqklmab2pgknr6fsg2d8n5wvg43jv")))

(define-public crate-sinfo-0.2 (crate (name "sinfo") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0dl9v4a5zrglgsq14cjwi1yhgrdcrdamk3cisl8c4g9jg4h3qx71")))

