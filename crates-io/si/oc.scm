(define-module (crates-io si oc) #:use-module (crates-io))

(define-public crate-sioctl-0.0.1 (crate (name "sioctl") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "sndio-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "08hi6ax2ldkcbzcknk3sx5xlykck376sgrk797z648qljs9fihzg")))

(define-public crate-sioctl-0.0.2 (crate (name "sioctl") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "sndio-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "15ijavg8qim9pf2jpycrf0qdv9zig1dn0lsdx8d0d4zqlhm0wkfi")))

