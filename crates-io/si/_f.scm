(define-module (crates-io si _f) #:use-module (crates-io))

(define-public crate-si_format-0.1 (crate (name "si_format") (vers "0.1.0") (deps (list (crate-dep (name "bounded-integer") (req "^0.5.7") (features (quote ("types"))) (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "01gd7yiv2w6fazyk1vmxwlxnddih76fimi7zva5j20y8pp4zhm10") (features (quote (("std") ("float64" "float32") ("float32") ("default" "std" "float64"))))))

(define-public crate-si_format-0.1 (crate (name "si_format") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)))) (hash "0plnnk647g1aq62mm63k6bmh6hm3srl0idz0la5vpm9nnyvlsq4n") (features (quote (("std") ("float64" "float32") ("float32") ("default" "std" "float64"))))))

