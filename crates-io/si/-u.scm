(define-module (crates-io si -u) #:use-module (crates-io))

(define-public crate-si-unit-prefix-1 (crate (name "si-unit-prefix") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "133mvnk63m7987fqmj6m8d78dmyiflwm1iq2kg031jh5g1sgfw4y")))

