(define-module (crates-io si ri) #:use-module (crates-io))

(define-public crate-sirius-0.0.0 (crate (name "sirius") (vers "0.0.0") (deps (list (crate-dep (name "abscissa") (req "^0") (default-features #t) (kind 0)))) (hash "0nvyp01bzn3hsxy7f4bpyv3akx1qfhr8dh21ls2vmpl3hxw09bd3") (yanked #t)))

(define-public crate-sirius-bindings-0.1 (crate (name "sirius-bindings") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)))) (hash "0fkk5inn71zq05ir1bh4yvm3ilg8bdzlwcwy2vhs166a0qak95i7") (v 2) (features2 (quote (("fuzz" "dep:arbitrary"))))))

