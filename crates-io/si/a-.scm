(define-module (crates-io si a-) #:use-module (crates-io))

(define-public crate-sia-macro-0.0.2 (crate (name "sia-macro") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11faq8qqsyszsmxkx8zjasn6ms6inb00m9jn00lq347437w27ih9")))

