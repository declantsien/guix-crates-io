(define-module (crates-io si mo) #:use-module (crates-io))

(define-public crate-simon-0.1 (crate (name "simon") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "10ibzbqbj9x9yz1gzcjlz6r0s7raylhdqghk6limgfhjr5aqyn5w")))

(define-public crate-simon-0.2 (crate (name "simon") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hj7dqrhk8lq5gcp99a6dgqys8x455bp6yisgpvwsyvnxsm8db5g")))

(define-public crate-simon-0.2 (crate (name "simon") (vers "0.2.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "12jq98l3pk9s0fc8ymzq0zjgx0rij2z5msv4zfgd59xvlwwfd2iw")))

(define-public crate-simon-0.2 (crate (name "simon") (vers "0.2.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "18rhvnvnyddn6x68r3cx4gncq0s6dffz6i2zz4i8x89jpc6qrs5g")))

(define-public crate-simon-0.2 (crate (name "simon") (vers "0.2.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qp1sg5d87iyxhw7xvl3q3xjp68rlcrsg2z64hxcz6290sgh2m2y")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "174f6i57gfamrbh1l2z5mkz0969vx83r0rnq19xg03apvr2k8d75")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vwmghzf0fdwa09zrppr012rxikp46inl6p01cdga8iygz9vxsch")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ia80zbnbkwqi3x42dck6nz9ni3a2q5qvds5k7wxrz5wipa0mwy0")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1nrdxikkg178my0ng6d86khvm3c0q28hmmgixlhi21jpnmazzv02")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0km13y7nsazvsl0xwlfhm9zw6bllc9h3qh30qdga256zvzyzlzgg")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.5") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yjn66y8blwgm8r08xd9mzn04mqny4ni02lk0mb1vqddqg0i7rry")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.6") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "06p4r64pvaib1y0n9dn57075k8x3hg5pfc2f44m4sklb3n0n6wbp")))

(define-public crate-simon-0.3 (crate (name "simon") (vers "0.3.7") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1slz3a7x50nizp22x5s3z2j1r79x77xbj20p2d2q9njdz3qyx8fz")))

(define-public crate-simon-0.4 (crate (name "simon") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "08xs7xanyrw8540vpbxh1byhmklc0yzk64653imkwfrqrvfnqf5j")))

(define-public crate-simon-cipher-0.0.0 (crate (name "simon-cipher") (vers "0.0.0") (hash "0qx4yjhc9z3zjvjz8j393x31gyhgdvmzgyhfcsr29g3rrpa3xa1g")))

(define-public crate-simoncli-1 (crate (name "simoncli") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0gsdkd81gcb87fbkjgn1r5yrv8mq7dlfjjc1jva2w4dq9a07wrkr")))

(define-public crate-simoncli-1 (crate (name "simoncli") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0xvmmr1jm5j90mbk0fp9vzfzk4yvhlk4qx1glyv46byk33k53g0z")))

