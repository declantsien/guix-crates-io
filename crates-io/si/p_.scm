(define-module (crates-io si p_) #:use-module (crates-io))

(define-public crate-sip_rld-0.1 (crate (name "sip_rld") (vers "0.1.0") (hash "1vd4rj0alb37kkc4k34wr8344j8zwx1sdmsd36bwnj1w004fbrjz")))

(define-public crate-sip_rld-0.2 (crate (name "sip_rld") (vers "0.2.0") (hash "0pwi0nlk53rz2fg3jq2n5yz0nmv74inm8dciy5bb4y1im6f4f9nx")))

(define-public crate-sip_rld-0.2 (crate (name "sip_rld") (vers "0.2.1") (hash "18pb6f81mnsb7hjbd3npp7yp6ddd9sn5p7dm9hc34m352bbia283")))

(define-public crate-sip_rld-0.2 (crate (name "sip_rld") (vers "0.2.2") (hash "13y6n1p9c5c5f627v1dgbnfyavlgvv50xf0vf1c96vqvjfihxgbi")))

(define-public crate-sip_rld-0.2 (crate (name "sip_rld") (vers "0.2.3") (hash "0c8bj0qp6bfa6rqaagrs1g7iv3lyamdhk9fi6qda31yzprvizs4r")))

(define-public crate-sip_rld-0.2 (crate (name "sip_rld") (vers "0.2.4") (hash "0d1fny5aipj5gc4vw6flpjf385mapfy23vlzv71lpkmiyfdhkxn7")))

(define-public crate-sip_rld-0.3 (crate (name "sip_rld") (vers "0.3.0") (hash "0mywvba7bzl1i3jkfx5minsiml42zbv5as7qb5xpvy5lrfkk8flc")))

(define-public crate-sip_rld-0.3 (crate (name "sip_rld") (vers "0.3.1") (hash "1ax499a1cbj4xknd6b6rwmngr7ikwvyp0cqhryx0mkfcy0ha04g8")))

(define-public crate-sip_rld-0.3 (crate (name "sip_rld") (vers "0.3.2") (hash "1qyar1ql96xllhjlnzvvaz1mqs74nak7axa8y17sh46fkprgl2ky")))

(define-public crate-sip_rld-0.3 (crate (name "sip_rld") (vers "0.3.3") (hash "04jsx97z6fz212fa65yxg3c7xlayxk61g5mphsyhf0mk10fsxs22")))

(define-public crate-sip_rld-0.3 (crate (name "sip_rld") (vers "0.3.4") (hash "0qdlbrv67phs6mzsdcsl8bvzfffk3sfiys2acc315vgmdywv50hw")))

(define-public crate-sip_rld-0.3 (crate (name "sip_rld") (vers "0.3.5") (hash "1cwblwdhnkjz2cdkz59pic1fvbd1slb303y9aimc5xlkdfbjfvhm")))

(define-public crate-sip_rld-0.3 (crate (name "sip_rld") (vers "0.3.6") (hash "0f7nfkyjd3q4fj6jx83b3w48g1qb5hm08khx3q5rxdasbcvwjvsp")))

