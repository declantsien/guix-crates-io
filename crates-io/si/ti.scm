(define-module (crates-io si ti) #:use-module (crates-io))

(define-public crate-sitix-0.0.2 (crate (name "sitix") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yai2h0kf0h1pvjv4n6ps2n7v6xh96r4mbyfsanvi7jwkdxyqk20")))

