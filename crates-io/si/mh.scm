(define-module (crates-io si mh) #:use-module (crates-io))

(define-public crate-simha-bst-0.1 (crate (name "simha-bst") (vers "0.1.0") (hash "1l9mhn05nlq43xq5n5h4hyi6fd256gm57zl9r057br0ya3aj38nl")))

(define-public crate-simhash-0.2 (crate (name "simhash") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "0952scrbdkqsz7v5cwxzb0mldxy9af7y7lh7cp1cvkqji7xik3nd") (features (quote (("mutually_exclusive_features" "hasher-sip" "hasher-fnv") ("hasher-sip" "siphasher") ("hasher-fnv" "fnv") ("default" "hasher-sip"))))))

