(define-module (crates-io si mn) #:use-module (crates-io))

(define-public crate-simnode-runtime-api-1 (crate (name "simnode-runtime-api") (vers "1.6.0") (deps (list (crate-dep (name "codec") (req "^3.1.3") (kind 0) (package "parity-scale-codec")) (crate-dep (name "sp-api") (req "^26.0.0") (kind 0)) (crate-dep (name "sp-std") (req "^14.0.0") (kind 0)))) (hash "0v2hn7cqbmgbb7canyiybcgyssf8hqcrwmb38jcl9c1l8sfngzyn") (features (quote (("std" "codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-simnode-runtime-api-1 (crate (name "simnode-runtime-api") (vers "1.8.0") (deps (list (crate-dep (name "codec") (req "^3.1.3") (kind 0) (package "parity-scale-codec")) (crate-dep (name "sp-api") (req "^28.0.0") (kind 0)) (crate-dep (name "sp-std") (req "^14.0.0") (kind 0)))) (hash "0gf44gi3g14ajx9fn6cnmidck0pw09jcfyyh7nnanr8428z1v8jw") (features (quote (("std" "codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

(define-public crate-simnode-runtime-api-1 (crate (name "simnode-runtime-api") (vers "1.9.0") (deps (list (crate-dep (name "codec") (req "^3.1.3") (kind 0) (package "parity-scale-codec")) (crate-dep (name "sp-api") (req "^29.0.0") (kind 0)) (crate-dep (name "sp-std") (req "^14.0.0") (kind 0)))) (hash "02rdpxkfyaqijljaid9xpc5an161w6h5jid91c2cmhmydslriz5h") (features (quote (("std" "codec/std" "sp-api/std" "sp-std/std") ("default" "std"))))))

