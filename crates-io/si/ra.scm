(define-module (crates-io si ra) #:use-module (crates-io))

(define-public crate-siraph-0.1 (crate (name "siraph") (vers "0.1.0") (deps (list (crate-dep (name "dynvec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0v3fsxajdpqx0nnp4yvcddrg444v14m8h2x5x47ba1n9y93cj6mx") (features (quote (("random" "nodes" "rand") ("nodes") ("math" "nodes" "num-traits") ("default" "nodes"))))))

(define-public crate-siraph-0.1 (crate (name "siraph") (vers "0.1.1") (deps (list (crate-dep (name "dynvec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "12ky2fpjqk8d54s0474qqjb0flk58qrdir6mgs1mf8cg0jc7l5zv") (features (quote (("random" "nodes" "rand") ("nodes") ("math" "nodes" "num-traits") ("default" "nodes"))))))

(define-public crate-siraph-0.1 (crate (name "siraph") (vers "0.1.2") (deps (list (crate-dep (name "dynvec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1a7vqp56hhd57jnxp6xx9ddmgrzn6brimwwncp96xmxfjm082hvz") (features (quote (("random" "nodes" "rand") ("nodes") ("math" "nodes" "num-traits") ("default" "nodes"))))))

