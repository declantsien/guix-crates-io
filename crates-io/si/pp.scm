(define-module (crates-io si pp) #:use-module (crates-io))

(define-public crate-sipp-0.1 (crate (name "sipp") (vers "0.1.0") (hash "1w3drd37fr5qnnhzr493xpyqvmjp0pfpvy8abb7rfmmkv3lc9szy")))

(define-public crate-sipp-0.1 (crate (name "sipp") (vers "0.1.1") (hash "0s630pnihvhfkah57dl7hh0k7p2zshjaydzmnw2fzmfxk0mvx4r1")))

(define-public crate-sipp-0.2 (crate (name "sipp") (vers "0.2.0") (hash "0h7fwhaabk5q4lqr3dndy126961nl97mc8zq339j7jb7bx20p1dm")))

