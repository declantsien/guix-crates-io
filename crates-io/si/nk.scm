(define-module (crates-io si nk) #:use-module (crates-io))

(define-public crate-sink-0.1 (crate (name "sink") (vers "0.1.0") (hash "0zbr3kaam1w1721sqa904bm1yg8w2v6mckxr3w406f1w03rsqljc")))

(define-public crate-sink-rotate-1 (crate (name "sink-rotate") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0g1yczq050r84qbic993czqq7ff43v6mfk93x54ziij4z1avvjvq")))

(define-public crate-sink-rotate-1 (crate (name "sink-rotate") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1c1nws4sac60qmw9bn709qx494r97mpnjs09aml2b2by4pm76pxg")))

(define-public crate-sink-rotate-1 (crate (name "sink-rotate") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1kd4c952vrlhy4a9c0sf2svhvvig6n4hlf58c1jh3rsbd065ahi0")))

(define-public crate-sink-rotate-1 (crate (name "sink-rotate") (vers "1.0.4") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "058ds2kx7ln9wi7w4nz79rgw1mwp1rn1fq093sh3wggzxvhqi197")))

(define-public crate-sink-splitter-0.1 (crate (name "sink-splitter") (vers "0.1.0") (deps (list (crate-dep (name "error-test-sinks") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1lwizrslwhmjb5sx1mqxlwn0w9ilsdhljgdgbm6wqagzy3pbrhdn")))

(define-public crate-sinkhole-0.0.1 (crate (name "sinkhole") (vers "0.0.1") (hash "0qcm79ndzi4adz3s0p5ia8ph5p0ar615w3jfgciqmibfwm47shk7")))

(define-public crate-sinkhole-core-0.0.1 (crate (name "sinkhole-core") (vers "0.0.1") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^2.0.0") (features (quote ("serde"))) (kind 0)) (crate-dep (name "elgamal_ristretto") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5.1") (kind 0)))) (hash "0wjmz8njdwqj32dkqmvf2pb5pbm97950rc9892lc75djgnwvq2sk")))

(define-public crate-sinkhole-elgamal-0.0.1 (crate (name "sinkhole-elgamal") (vers "0.0.1") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "elgamal_ristretto") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "sinkhole-core") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "122bhhvx4w3q1m0hz44c623l2q8m4cnamjjcf504602y879j3r3b")))

