(define-module (crates-io si co) #:use-module (crates-io))

(define-public crate-sicompiler-0.1 (crate (name "sicompiler") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10rshipzj5rip8lzp5i5s7wq0fv3jxp03rkqpalnkf6a5lgrwj32")))

(define-public crate-sicompiler-0.1 (crate (name "sicompiler") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13ifzz90324cq239qv7asbmp80lvq7yyl9npak0cnpndyy06r7np")))

(define-public crate-sicompiler-1 (crate (name "sicompiler") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09nbn2zgxidc4xqd9zp5c9i7hc9pds35mfr7ckw9cm5gmcb9w818") (yanked #t)))

(define-public crate-sicompiler-1 (crate (name "sicompiler") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.13") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03zmmb5x8wnpslpqbsza6n1ic1ykv0zvylx2aq31bdixb34w4hrr")))

