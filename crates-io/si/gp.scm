(define-module (crates-io si gp) #:use-module (crates-io))

(define-public crate-sigpipe-0.1 (crate (name "sigpipe") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lykrkgag291i5ypgv6z6jvp327qnk8dm7284zrp3flx83z9havs")))

(define-public crate-sigpipe-0.1 (crate (name "sigpipe") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03jsj05x84m5zkw2p4656a8wlr2m391vi5y7yhrqrgsgwbz8b4sg")))

(define-public crate-sigpipe-0.1 (crate (name "sigpipe") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rnqcgbx2mv3w11y6vf05a8a3y6jyqwmwa0hhafi6j6kw2rvz12m")))

(define-public crate-sigpro-0.1 (crate (name "sigpro") (vers "0.1.0") (hash "0bhd91vs2d9h7a0nh2fy6vqj0n6pxs6bzyhmc45hxn2d0lz1sffq") (yanked #t)))

(define-public crate-sigproc_filterbank-0.1 (crate (name "sigproc_filterbank") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g6cayzv1pygyibfj6iy33rqhkmjclnn0asc32qlcmk5aa1il7aq") (rust-version "1.57.0")))

(define-public crate-sigproc_filterbank-0.1 (crate (name "sigproc_filterbank") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ql9zcpr2639hcjr8g4yxpdbj11m3lfwvvxnf40r0njn5ncr7fnb") (rust-version "1.57.0")))

(define-public crate-sigproc_filterbank-0.2 (crate (name "sigproc_filterbank") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z06gy2n873fiia9vcf9whh6a0avhhxlndknm9pgpzxgjl48wpfm") (rust-version "1.57.0")))

(define-public crate-sigproc_filterbank-0.2 (crate (name "sigproc_filterbank") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1") (default-features #t) (kind 0)))) (hash "12z7q993q9nxx80i5p2vgilbvmm4sgdkidl5sqgslka5f0xkf4l9") (rust-version "1.57.0")))

(define-public crate-sigproc_filterbank-0.3 (crate (name "sigproc_filterbank") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1") (default-features #t) (kind 0)))) (hash "06fq0g63ah094qsv3g58cd8x7q17nkcnd7yci1pyhs01mzq435ak") (rust-version "1.57.0")))

(define-public crate-sigproc_filterbank-0.4 (crate (name "sigproc_filterbank") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y38f70aa2kdxq1m925gfiilpsannlcjsvfpq5xqg4xd9g33z0cb") (rust-version "1.57.0")))

