(define-module (crates-io si se) #:use-module (crates-io))

(define-public crate-sise-0.1 (crate (name "sise") (vers "0.1.0") (hash "1icrx5l845n3waibgk4za0dd35j5cygp106zf0813avrbx3qvxiv") (yanked #t)))

(define-public crate-sise-0.2 (crate (name "sise") (vers "0.2.0") (hash "0qqs7brm8ckz26digh7sx3whdf1faj3fd39gcw3i2hawvqxlydkv") (yanked #t)))

(define-public crate-sise-0.3 (crate (name "sise") (vers "0.3.0") (hash "03zfz2j5hh11c9rx8083z6sv2khcshwzv6di0wi2yj2vkz7srx5m")))

(define-public crate-sise-0.3 (crate (name "sise") (vers "0.3.1") (hash "0n6hqy64fcqfl3l9iwgmckkiyw2crlikmmxl9q1w9sbrj2dv93zh")))

(define-public crate-sise-0.4 (crate (name "sise") (vers "0.4.0") (hash "0m5qqkpcs0hr3p91p6w3sjx2ivhgss23jhbg6vd5ys3i372wi848")))

(define-public crate-sise-0.5 (crate (name "sise") (vers "0.5.0") (hash "1ha8rpr2p7jibvrfnz34y0l219i4ripkvya1hbpyxpgi09c0j6pz")))

(define-public crate-sise-0.5 (crate (name "sise") (vers "0.5.1") (hash "13lia96qfp11dc2zpns9gjkhnwv1b4ys1piq0an5rl9vfgj0gpy6")))

(define-public crate-sise-0.6 (crate (name "sise") (vers "0.6.0") (hash "1dqs8g4zl4y1a0p9bhb5sr7d4jgw6lpwzpy5l5q627a6jf2lgqam")))

(define-public crate-sise-0.7 (crate (name "sise") (vers "0.7.0") (hash "0danqks5r5flc236w4lqy6dachvng631d723j3rrfsca15kdfjkc") (features (quote (("std") ("default" "std"))))))

(define-public crate-sise-0.8 (crate (name "sise") (vers "0.8.0") (hash "170dmhp1byjkf28d89nqwmn46wyq682v9cxx8mda2hy8n6y43gmn") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-sise-atom-0.1 (crate (name "sise-atom") (vers "0.1.0") (hash "1vmdbk3bi8ricqg3n3q5nj4ldxb4z5wqif64zk7m4skh0x9wiqci") (yanked #t)))

(define-public crate-sise-atom-0.2 (crate (name "sise-atom") (vers "0.2.0") (hash "0vxlkv1s72sqclsm1x42x7v021i5vm6nfq1525l9vw6rbzxs9xp5") (yanked #t)))

(define-public crate-sise-atom-0.3 (crate (name "sise-atom") (vers "0.3.0") (hash "12x4cn0ig8c6d8kixm6yxnzxcyhzgyiqvbgjpidx71bb2zfishj7")))

(define-public crate-sise-atom-0.3 (crate (name "sise-atom") (vers "0.3.1") (hash "0l1vd2m8flzryigf0gyskx5zcckdzh5g518srlswg1lr8zbycam9")))

(define-public crate-sise-atom-0.3 (crate (name "sise-atom") (vers "0.3.2") (hash "1p7ys4i2fsag8qrkbdn8z0x0za22b2xn7mif2v4l0x6rf5a90l9q")))

(define-public crate-sise-atom-0.3 (crate (name "sise-atom") (vers "0.3.3") (hash "1ffacnpn9cyiw15z2k9lcdin847rww6sym0s1c90b9z8llk5mh5l")))

(define-public crate-sise-atom-0.3 (crate (name "sise-atom") (vers "0.3.4") (hash "0fyd04wqrras2lqs9wn29iwm46l1994jy0jylydzmpyz03ns1drd")))

(define-public crate-sise-atom-0.4 (crate (name "sise-atom") (vers "0.4.0") (hash "00cmirga9ps2f4ymzhlnmxfjjzif4cbszwq2m6wz3943i8zs481m") (yanked #t)))

(define-public crate-sise-atom-0.5 (crate (name "sise-atom") (vers "0.5.0") (hash "0g6kfs9683l8m4l4pwghvippv3h85vbw0rfd496hiv076k8m4935")))

(define-public crate-sise-decoder-0.1 (crate (name "sise-decoder") (vers "0.1.0") (deps (list (crate-dep (name "sise") (req "= 0.1.0") (default-features #t) (kind 0)))) (hash "0r2fknbbs4xg0pbhs7nxf2vczikap8hgf3c13gsqfigqql3gvs9j") (yanked #t)))

(define-public crate-sise-decoder-0.2 (crate (name "sise-decoder") (vers "0.2.0") (deps (list (crate-dep (name "sise") (req "= 0.2.0") (default-features #t) (kind 0)))) (hash "169by4m5g8f99f67vpx9za6mfc1258x6n25rrbpbj9dh9rfhxpxv") (yanked #t)))

(define-public crate-sise-encoder-0.1 (crate (name "sise-encoder") (vers "0.1.0") (deps (list (crate-dep (name "sise") (req "= 0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sise-decoder") (req "= 0.1.0") (default-features #t) (kind 2)))) (hash "08nhr8in4jdh5ffq0fac7v383avg8vfg3qp3kzhgy41d5a9br9b7") (yanked #t)))

(define-public crate-sise-encoder-0.2 (crate (name "sise-encoder") (vers "0.2.0") (deps (list (crate-dep (name "sise") (req "= 0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sise-decoder") (req "= 0.2.0") (default-features #t) (kind 2)))) (hash "10v6i788nwbh4v553xgpmj1r9l56iqisz0iwmzsp1dpigp0i124y") (yanked #t)))

(define-public crate-sise-read-util-0.2 (crate (name "sise-read-util") (vers "0.2.0") (deps (list (crate-dep (name "sise") (req "= 0.2.0") (default-features #t) (kind 0)))) (hash "1ahg69splj617sbr4wynr0ihl64qdxfxvm24b2rmk41xdaw237i5") (yanked #t)))

