(define-module (crates-io si ii) #:use-module (crates-io))

(define-public crate-siiir-bevy_fancy_cursor-0.4 (crate (name "siiir-bevy_fancy_cursor") (vers "0.4.0") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "04f8zn25wn2j8xwa9l17cr4hvwirqxbkw52i54kb4fld38dqbs5i") (yanked #t)))

(define-public crate-siiir-bevy_fancy_cursor-0.4 (crate (name "siiir-bevy_fancy_cursor") (vers "0.4.1") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "028p89b63ib7z0s4hbba028zrd0zccv28mgcrvyh3kx34w6k8kz1")))

(define-public crate-siiir-bevy_fancy_cursor-0.4 (crate (name "siiir-bevy_fancy_cursor") (vers "0.4.2") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1nrx9dybnb5rnj4pwsgcg3c870br6sxqwn5axppm10r7zwxjplk1") (yanked #t)))

(define-public crate-siiir-bevy_fancy_cursor-0.4 (crate (name "siiir-bevy_fancy_cursor") (vers "0.4.3") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "17yl97nk6cvk20waip4phiv56fnilrcprghns2rq6b9099ik7xgg")))

(define-public crate-siiir_modular_power-1 (crate (name "siiir_modular_power") (vers "1.0.6") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ss4pag1wqqyrgi52pvc556i928r72rkq8w0hgkaghwzrmcyim8x")))

(define-public crate-siiir_modular_power-1 (crate (name "siiir_modular_power") (vers "1.0.7") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ci0v3mybbxv4a4z0xnn0agdy7sssp2mg6j6sl4kjjzflkj36l48")))

(define-public crate-siiir_points-0.1 (crate (name "siiir_points") (vers "0.1.0-beta") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "09n5kfsc94142i0gnra5ywjkr9zv58jfzj024hdczmgqygylkfyq") (yanked #t)))

(define-public crate-siiir_points-0.1 (crate (name "siiir_points") (vers "0.1.1-beta") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "0pxyswi3a5x39r8kakmgp31i563nc8c5h6pzx8n64kwsnzq85rpi")))

(define-public crate-siiir_points-0.1 (crate (name "siiir_points") (vers "0.1.2-beta") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "106899rvagwml1cpg2hgf7dy8yzi1z1c2xihbns4kj65naf5397f")))

(define-public crate-siiir_points-0.1 (crate (name "siiir_points") (vers "0.1.3-beta") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "0aqyd8ff6jq5jzajjwdwqlclx6jfqzwpbdckky4cq8hmynb9cpmn")))

(define-public crate-siiir_points-0.1 (crate (name "siiir_points") (vers "0.1.4-beta") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "0ip4w7y98bjbl4yjh196ghjnylsm7j037yyavmckiv2n6l51xvqs")))

(define-public crate-siiir_points-0.1 (crate (name "siiir_points") (vers "0.1.5-beta") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "1vjmsnd944k4xiygcadlb0z97gjfv1zqbg4p0jg0sy0an9hvaypm")))

