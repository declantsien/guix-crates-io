(define-module (crates-io si nn) #:use-module (crates-io))

(define-public crate-sinner-0.1 (crate (name "sinner") (vers "0.1.0") (hash "0b2959davcx5ybx2pxrk0vwb1x8h21y3ywdaw21g0cw3h66mkm92")))

(define-public crate-sinner-0.1 (crate (name "sinner") (vers "0.1.1") (hash "00zwcx8jwz58c1bwhrl160pfmwh1r0why6nq7zrrhzd4ca0mdw5c")))

(define-public crate-sinner-0.1 (crate (name "sinner") (vers "0.1.2") (hash "1r4g7fxa3hnmab7y8w8gvbn07brn0i4k1i4fpmqr6nlha28bn2yh")))

