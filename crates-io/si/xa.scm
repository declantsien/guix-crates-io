(define-module (crates-io si xa) #:use-module (crates-io))

(define-public crate-sixarm_assert-1 (crate (name "sixarm_assert") (vers "1.1.0") (hash "14avs8j9q7a6hfnac4ppm5109fh07m2xv1di5c0395s6vhghh564")))

(define-public crate-sixarm_assert-1 (crate (name "sixarm_assert") (vers "1.1.1") (hash "0c69z638amm12pkil8bhji4wjy54x01js6z68zf3ka8y27xggg4z")))

(define-public crate-sixarm_collections-1 (crate (name "sixarm_collections") (vers "1.1.0") (hash "0vp23lhqnd29dx5zbhbm9mmcsn8nmvw94m61z4l6yj91hjh5pp3h")))

(define-public crate-sixarm_collections-1 (crate (name "sixarm_collections") (vers "1.1.1") (deps (list (crate-dep (name "sixarm_assert") (req "^1") (default-features #t) (kind 0)))) (hash "0qf9wj70gazidmjrii5r6n8plzd57zp77hag43zybz870agxnhrv")))

