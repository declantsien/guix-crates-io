(define-module (crates-io si dx) #:use-module (crates-io))

(define-public crate-sidx-0.1 (crate (name "sidx") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "10xv4xn955fcghdcy7rrcxmfzfx22l6gsdhlv3gbvccqcpqk4lzz") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sidx-0.2 (crate (name "sidx") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0q42ayh5xxpsi5a47lppicrx7m5zsjx4h4x19ji7wjj7diwv9cih") (v 2) (features2 (quote (("serde" "dep:serde"))))))

