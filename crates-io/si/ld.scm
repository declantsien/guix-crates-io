(define-module (crates-io si ld) #:use-module (crates-io))

(define-public crate-sild_test_doc_build_fine-2024 (crate (name "sild_test_doc_build_fine") (vers "2024.3.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1") (optional #t) (default-features #t) (kind 1)))) (hash "16aggp4msmwnpd2cngrlwkwxvzi4cx07x4q2i0bcgx8mm8liks8h") (features (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (yanked #t)))

(define-public crate-sild_test_doc_build_fine-2024 (crate (name "sild_test_doc_build_fine") (vers "2024.3.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1") (optional #t) (default-features #t) (kind 1)))) (hash "1wzkc7fmy88aqy7nsb9czadl52f4860h0sf6wn80hgjwi405ssbr") (features (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (yanked #t)))

(define-public crate-sild_test_doc_build_fine-2024 (crate (name "sild_test_doc_build_fine") (vers "2024.3.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1") (optional #t) (default-features #t) (kind 1)))) (hash "0faqv7p2m2jv9pbcq8dn6q2z4fiinsvqmwmfsq08wvbahqjc5r47") (features (quote (("shared-tonlib") ("default" "cmake" "num_cpus")))) (yanked #t)))

