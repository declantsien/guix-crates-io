(define-module (crates-io si d_) #:use-module (crates-io))

(define-public crate-sid_vec-0.1 (crate (name "sid_vec") (vers "0.1.0") (deps (list (crate-dep (name "sid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18b86vfvlgxxqgazj2ryg86ac44mr24z7vq5zsvxmkblzpqg9wv2")))

(define-public crate-sid_vec-0.2 (crate (name "sid_vec") (vers "0.2.0") (hash "11sgjc44560pyzijvihyr5lfjid53wwy61y9hz91sndmdhpii2lj")))

(define-public crate-sid_vec-0.2 (crate (name "sid_vec") (vers "0.2.1") (hash "1wvhfc3qngi91n13hyxq12nk11izvlcqa9ma5pqnf96xx5dhs9iq")))

