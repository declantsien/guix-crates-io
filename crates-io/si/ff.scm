(define-module (crates-io si ff) #:use-module (crates-io))

(define-public crate-siffra-0.2 (crate (name "siffra") (vers "0.2.2") (deps (list (crate-dep (name "astro-float") (req "^0.9.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "periodic-table-on-an-enum") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (features (quote ("grammar-extras"))) (default-features #t) (kind 0)))) (hash "09x3nv1q4b0aw30j0ngp9c7gvs8ynpjfdvpd7vgikz1pw34a1p6z")))

