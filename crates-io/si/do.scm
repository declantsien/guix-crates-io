(define-module (crates-io si do) #:use-module (crates-io))

(define-public crate-sidoc-0.1 (crate (name "sidoc") (vers "0.1.0") (hash "0gykmb14dqz81d5zapjg9nllsma9d0hd5nl1cc38zkpy4z5qd6lh")))

(define-public crate-sidoc-html5-0.1 (crate (name "sidoc-html5") (vers "0.1.0") (deps (list (crate-dep (name "html-escape") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "sidoc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "173df92kh7mrnc0vp2krs87nwi59vjxdfl8ygzdv5idy1qa5y4mi")))

