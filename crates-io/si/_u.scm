(define-module (crates-io si _u) #:use-module (crates-io))

(define-public crate-si_units-0.1 (crate (name "si_units") (vers "0.1.0") (hash "0i270pimvsr2wrr4k6lh2dva18isx5axha11dlwamaxpcwzaj01y")))

(define-public crate-si_units-0.1 (crate (name "si_units") (vers "0.1.1") (deps (list (crate-dep (name "prse") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "13pmdvs65wrl9q5sx8scxxz2c7wg6fifximij1l53sw2wrdkx7mm")))

(define-public crate-si_units-0.1 (crate (name "si_units") (vers "0.1.2") (deps (list (crate-dep (name "prse") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0lx2nh5xh6wzvvj7b6b8by9s21awjzgfj87vfg3dwdqldc4wi2pi")))

