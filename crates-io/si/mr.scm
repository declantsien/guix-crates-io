(define-module (crates-io si mr) #:use-module (crates-io))

(define-public crate-simra-0.0.1 (crate (name "simra") (vers "0.0.1") (hash "0bkad78ql5wmlx53cx7f5lcp2gkx0qgnr4yvdl696wy7c8i1rsbj")))

(define-public crate-simrpg-0.1 (crate (name "simrpg") (vers "0.1.0") (hash "0fkaq1w4ranjzg4x8qv0swfh9lb0ac8gb0krbr7pn73f8j4s76bg")))

(define-public crate-simrs-0.1 (crate (name "simrs") (vers "0.1.0") (hash "1dsa1mpsj1cvmmln1y53gfj97pyahv1j8yvgp5ds17ikbnx18j18")))

(define-public crate-simrs-0.1 (crate (name "simrs") (vers "0.1.1") (hash "0x3z6b7cqbg7mpln6yp5jqcrrsgkwj116xj86d1b9hh1s3dg6y9c")))

(define-public crate-simrs-0.1 (crate (name "simrs") (vers "0.1.2") (hash "1wl5alykxszwpzn2zbaz4jrgdhbjywjdaxavjy28y34fn1d2gjg5")))

(define-public crate-simrs-0.2 (crate (name "simrs") (vers "0.2.0") (hash "1gc2wqgkh1jwkn8f0zjmna87zzshlbyqn2wqxv5pqjhycs8774z8")))

