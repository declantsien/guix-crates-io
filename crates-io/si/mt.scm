(define-module (crates-io si mt) #:use-module (crates-io))

(define-public crate-simt-0.0.1 (crate (name "simt") (vers "0.0.1") (hash "10q7h2jfbx6fqkwksbpm3bjimlinkrxlgss06kgsaiw8h960k6id")))

(define-public crate-simt-0.2 (crate (name "simt") (vers "0.2.0") (deps (list (crate-dep (name "simt_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_hip") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0y1dg6pa3h9pb25likahrrnnwvvra07wq5rgfph8wwmdi875y244")))

(define-public crate-simt-0.2 (crate (name "simt") (vers "0.2.1") (deps (list (crate-dep (name "simt_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_hip") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0rhrxd1ccg5flszai2wzq0d0w6qxbw3kgzn4js1kqkqasbz3f55d")))

(define-public crate-simt-0.2 (crate (name "simt") (vers "0.2.2") (deps (list (crate-dep (name "simt_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda_sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simt_hip") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1b1ychcm3s6m2vbscgwhc9knmhi1jjhgp9054yrr2b7c2lq3ml3b")))

(define-public crate-simt-0.2 (crate (name "simt") (vers "0.2.3") (deps (list (crate-dep (name "simt_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda_sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simt_hip") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0jszw3klnjfhnpnbffyx5hxya9qiifyi48dmph7gz2gikw8ra266")))

(define-public crate-simt_core-0.2 (crate (name "simt_core") (vers "0.2.0") (deps (list (crate-dep (name "half") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "104zcw5ppbh7zah1y52j5g0xppxvxh92j20ddfkl7cxiv5s28a6a")))

(define-public crate-simt_cuda-0.1 (crate (name "simt_cuda") (vers "0.1.0") (hash "0fh2rws57i7q6vdq1l0s59ssnbzla430dpl06vklygi3h2nqps9l")))

(define-public crate-simt_cuda-0.2 (crate (name "simt_cuda") (vers "0.2.0") (deps (list (crate-dep (name "half") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "simt_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1qyvh4fdc0vpaw7grwhddz8822kj9l1rxfqq9z0dzgxq7l6x7n2z")))

(define-public crate-simt_cuda-0.2 (crate (name "simt_cuda") (vers "0.2.1") (deps (list (crate-dep (name "half") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "simt_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "17wnzs8s24lkkf3l5rsd5fl216gdr392p9iyxjvhpfi9fbqvd6pk")))

(define-public crate-simt_cuda-0.2 (crate (name "simt_cuda") (vers "0.2.2") (deps (list (crate-dep (name "half") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "simt_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_cuda_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0p4ixwmxi8bar26nlhpkf1kbqwzmp93v9kz5mfjkfdqpg1yh8p74")))

(define-public crate-simt_cuda_sys-0.1 (crate (name "simt_cuda_sys") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1gq5f2zwwlyqfvm6br9cya54lc65p8hh1m7pbwz16cb8fzcvmkfi")))

(define-public crate-simt_cuda_sys-0.1 (crate (name "simt_cuda_sys") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0kjzgmincas2d3mz5gf4zlg2rpjjz1viij288jfas4j4p9kxacb7")))

(define-public crate-simt_cuda_sys-0.2 (crate (name "simt_cuda_sys") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1mmf7z6k0fjapfmrr6slkx6zbkl5a9l12hqknm2x3mlzmkydklz7")))

(define-public crate-simt_hip-0.1 (crate (name "simt_hip") (vers "0.1.0") (deps (list (crate-dep (name "simt_hip_sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "01sr9arv7smw6gdvraac9994a1dm3lxg8r4pmq0nszimc59mafj5")))

(define-public crate-simt_hip-0.1 (crate (name "simt_hip") (vers "0.1.1") (deps (list (crate-dep (name "simt_hip_sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0yigxyn3fjcg3iaici7pcchxsgk8p0345wy0x7ax71ws7ajbaapc")))

(define-public crate-simt_hip-0.1 (crate (name "simt_hip") (vers "0.1.2") (deps (list (crate-dep (name "simt_hip_sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1shcpakp74kb1svs5w5x40xnvq87mndbda1dp0msc89lwg184irm")))

(define-public crate-simt_hip-0.1 (crate (name "simt_hip") (vers "0.1.3") (deps (list (crate-dep (name "half") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "08rhmv62b1z9s269nz3hijfq2czniw1gc5cwqr43xrnra2sp94s9")))

(define-public crate-simt_hip-0.1 (crate (name "simt_hip") (vers "0.1.4") (deps (list (crate-dep (name "half") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1y030k8kz4wpn6nf2nn4p10v8grx73d7jfdqffw8nkjgqzzwlwj2")))

(define-public crate-simt_hip-0.1 (crate (name "simt_hip") (vers "0.1.5") (deps (list (crate-dep (name "half") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0apvp0ypi60qv9gi24wk0fnpb3h0j397y650f7s49jpjmriy8mbw")))

(define-public crate-simt_hip-0.1 (crate (name "simt_hip") (vers "0.1.6") (deps (list (crate-dep (name "half") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0xzc6hbgzfbm2dazxafg4rgldfznyymvz41d8vn157d9dpvrx4y4")))

(define-public crate-simt_hip-0.2 (crate (name "simt_hip") (vers "0.2.0") (deps (list (crate-dep (name "half") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "simt_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simt_hip_sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "188hdv57rpdw0yyz2ri6z7jl4dpfmqs3w6rnxbxd2b56w23jxy68")))

(define-public crate-simt_hip_sys-0.1 (crate (name "simt_hip_sys") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1xvxk7bmaz87rz3qc6x8hfd77fbdly86bd93s65pw9xqvrxj40rs")))

(define-public crate-simt_hip_sys-0.1 (crate (name "simt_hip_sys") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1931lzp34apsw7i6q3zm45yvrkknsd7jvlnavjl8600lma4v5i2f")))

(define-public crate-simt_hip_sys-0.1 (crate (name "simt_hip_sys") (vers "0.1.2") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0xf3vd1i83grphf5g73ksaz35zc9kj3s4l2c1mi0d1chm2i0k1d1")))

(define-public crate-simt_hip_sys-0.2 (crate (name "simt_hip_sys") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0mmif5l37xizfx6khl51pbzidq7rdf82qf04ys6mqajs3rdkd864")))

(define-public crate-simt_rocblas_sys-0.1 (crate (name "simt_rocblas_sys") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0aq0v9cq56wb5cl0nwk8cf03byqd43sh7xnc75mgh7l6wrr9wsv6")))

(define-public crate-simt_rocblas_sys-0.2 (crate (name "simt_rocblas_sys") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "04pdhihlnqmm3vwxi04c1iiz084bdzs5cprb78lwpdk3arx1ld7s")))

(define-public crate-simt_vulkan-0.1 (crate (name "simt_vulkan") (vers "0.1.0") (hash "0llzgrxmar6i3hyf4fvbhr34b8i2y35byv28miz985ainpqllwvv")))

(define-public crate-simtricks-0.2 (crate (name "simtricks") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "extism") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "matricks_plugin") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "rfd") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.181") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0nniwg12k8db05r2z9qks5vrgi6ig3r0azz6np1rm6v3lwms8b2q")))

(define-public crate-simtricks-0.3 (crate (name "simtricks") (vers "0.3.0-beta.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "egui_extras") (req "^0.23.0") (features (quote ("image"))) (default-features #t) (kind 0)) (crate-dep (name "extism") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (features (quote ("png"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "00gsfim1blf1583cw7cfxhc7vvwz27vbp3hjnld18nm7nmfwjf5p")))

(define-public crate-simtricks-0.3 (crate (name "simtricks") (vers "0.3.0-beta.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "egui_extras") (req "^0.23.0") (features (quote ("image"))) (default-features #t) (kind 0)) (crate-dep (name "extism") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (features (quote ("png"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "09bn75p9237vfw47zx4sapqdabnix4593cfr3vjfh8jvr06k30lm")))

(define-public crate-simtricks-0.3 (crate (name "simtricks") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "egui_extras") (req "^0.23.0") (features (quote ("image"))) (default-features #t) (kind 0)) (crate-dep (name "extism") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (features (quote ("png"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "01d6lxh40mwjirj8qwvwyf21mzlyk8vxcyjmgaz5rprj8zdynl7p")))

