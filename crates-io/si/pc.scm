(define-module (crates-io si pc) #:use-module (crates-io))

(define-public crate-sipcore-0.0.0 (crate (name "sipcore") (vers "0.0.0") (hash "06845gmxpal1d50ivz1sakgisj29nlhvscs6y29yb63y1n9mgsdn")))

(define-public crate-sipcore-0.0.1 (crate (name "sipcore") (vers "0.0.1") (deps (list (crate-dep (name "sipmsg") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09p3ma2g9g0vf4pc1vgl2yd43w9in8lq59fz6lk4wkvvlh1y041h")))

(define-public crate-sipcore-0.0.2 (crate (name "sipcore") (vers "0.0.2") (deps (list (crate-dep (name "sipmsg") (req "^0.2.0-beta") (default-features #t) (kind 0)))) (hash "004is97ypg4ln5db1dkvg2va395xg0mrfnhxsq0b89a9ks301npm")))

