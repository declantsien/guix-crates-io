(define-module (crates-io si eg) #:use-module (crates-io))

(define-public crate-siege-0.1 (crate (name "siege") (vers "0.1.0") (deps (list (crate-dep (name "line_drawing") (req "^0.7") (default-features #t) (kind 0)))) (hash "040ygh0q69f8bfgdgqp4d9y9lfahgidrpr2wq4hzsmxlc0ch7mkg")))

(define-public crate-siege-0.1 (crate (name "siege") (vers "0.1.1") (deps (list (crate-dep (name "line_drawing") (req "^0.7") (default-features #t) (kind 0)))) (hash "110jkk83hyglvlqx6mq20zfybj0gmqpy11z5xx6r1dgmyhb23wly")))

(define-public crate-siege-0.1 (crate (name "siege") (vers "0.1.2") (deps (list (crate-dep (name "line_drawing") (req "^0.7") (default-features #t) (kind 0)))) (hash "1217ipfyn7dmxrh8zga05zkirpxylg4rg6xffqy29mxkp4a6ydg2")))

(define-public crate-siege-color-0.4 (crate (name "siege-color") (vers "0.4.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "siege-math") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jkyn8j5p6g82y7ykjn09y163i9jkkifd80scw22qdvsiyn8ank6")))

(define-public crate-siege-editor-0.1 (crate (name "siege-editor") (vers "0.1.0") (deps (list (crate-dep (name "direct-gui") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "line_drawing") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "siege") (req "^0") (default-features #t) (kind 0)))) (hash "0rjj7grn78qcxcds2xirfnysglnrvczwfbhxr8dr6zv1xcyi3zp9")))

(define-public crate-siege-editor-0.1 (crate (name "siege-editor") (vers "0.1.1") (deps (list (crate-dep (name "direct-gui") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "line_drawing") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "siege") (req "^0") (default-features #t) (kind 0)))) (hash "0414r8j9kizw6d6gjj542svz4a0fzhz3cj215y9ns42jwvdmlhz6")))

(define-public crate-siege-editor-0.1 (crate (name "siege-editor") (vers "0.1.2") (deps (list (crate-dep (name "direct-gui") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "line_drawing") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "siege") (req "^0") (default-features #t) (kind 0)))) (hash "0ysb8bk2vhgsml7njkj8zvpaxy20lp18l0s6kk1pjdp4jjpmwi57")))

(define-public crate-siege-math-0.5 (crate (name "siege-math") (vers "0.5.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "13z4fxc8mylm8i79fq3kxpy1jgky3693zqk2jlzpdpdvjld0i1nz")))

(define-public crate-siege-math-0.5 (crate (name "siege-math") (vers "0.5.1") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "06qy6pb4n940ig5sfb3zbicir0lzn1pfx2pqm16zqnj1rsdw51xl")))

