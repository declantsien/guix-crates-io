(define-module (crates-io si pm) #:use-module (crates-io))

(define-public crate-sipmsg-0.0.0 (crate (name "sipmsg") (vers "0.0.0") (hash "0wclsyrihzp8mznbwjbg9qx7py0ylnb090gzrmjyjpci2hj5zi9n")))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "1grs4y632ddplvrii5wcxmg9b9n75clgmhr66qxqdap6mbvz2m7s") (features (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "1ssxmszawzzbn7p69g9vjbv3sqhxy4hfqcry6fhgz17mmampdll2") (features (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "0jpdb1320xabsz1kc0wam2dl8mxnibh6shsgq4xa7vz7j0x8ag2d") (features (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)))) (hash "1knrlfrdhhjdda3v6610242ns65xbpgmir5gs3pdsbl4vd71dc80") (features (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "0px95zna9mbglxy5g1wajbwz5c5kj575vl3jcvkl6yqsf8n25dgp") (features (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.5") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "1l0azw173js1542lzniw65pqhz3vz8qdlyzcl3prgbzbaf8rdkhg") (features (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.6") (deps (list (crate-dep (name "nom") (req "^5") (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "0pd07h9p9jcmkr25bmfwd6annm7fghdxv4g6wvrq30wnrmgl53hx") (features (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-sipmsg-0.1 (crate (name "sipmsg") (vers "0.1.7") (deps (list (crate-dep (name "nom") (req "^6.0.0-alpha1") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "1hc5liyvr4rdphgl69gkdsbf23jaka3299l3l4f77lirappnjqyf")))

(define-public crate-sipmsg-0.2 (crate (name "sipmsg") (vers "0.2.0-beta") (deps (list (crate-dep (name "nom") (req "^6.0.0-alpha2") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "1magfakgqh7lllnv8d21l8hcff1npyx1dgi6p1zgp3fzyxm88jrk")))

(define-public crate-sipmsg-0.2 (crate (name "sipmsg") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^6.0.0-alpha2") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "1429frp0nhnv26l3fnj97cvvmif93pa7gm7ghx4hi2dvv3j2iaib")))

(define-public crate-sipmsg-0.2 (crate (name "sipmsg") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "11p7i0wrd21xfsxyr6fcx5lkm8ksggv7rjdl3cadb5pgr3n1iwfh")))

(define-public crate-sipmsg-0.2 (crate (name "sipmsg") (vers "0.2.2") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "11ay29fgfs3xrz6jw24z4xhn933xchisxn5g4s11jkxpmaf0172c")))

(define-public crate-sipmsg-0.2 (crate (name "sipmsg") (vers "0.2.3") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "1hapwk7hx718z5iyccgrlagddz253ana1mca8z1jvcf5hsdn4bdn")))

