(define-module (crates-io si cx) #:use-module (crates-io))

(define-public crate-sicxe-0.1 (crate (name "sicxe") (vers "0.1.0") (hash "1gawh94ar2xf5g2kgralpx9nc3xd2zf63sdgajrc888jzp38wn79")))

(define-public crate-sicxe-cli-0.1 (crate (name "sicxe-cli") (vers "0.1.0") (deps (list (crate-dep (name "sicxe") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0g5wwsp8619wiybgr8qp2sda2483fspzdpvh05j92c9jxxwl9mwp")))

