(define-module (crates-io si pu) #:use-module (crates-io))

(define-public crate-sipua-0.0.1 (crate (name "sipua") (vers "0.0.1") (hash "0758r5p76c4vp4qyn0c46ixb23vf5gxw8wxqp4g25vm51srixwg1")))

(define-public crate-sipua-0.0.2 (crate (name "sipua") (vers "0.0.2") (deps (list (crate-dep (name "parsip") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yx8ijg81vj6y9ffsjnllf7lg1pzamd6z2lnfx89yyi38ks4gqsb")))

(define-public crate-sipua-0.0.3 (crate (name "sipua") (vers "0.0.3") (deps (list (crate-dep (name "sipcore") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0aifzmqza4fxy8c5ia2gwsn8w8dhqpp7ibdrp96433s0qcv03r00")))

