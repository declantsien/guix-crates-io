(define-module (crates-io si ar) #:use-module (crates-io))

(define-public crate-siarne-0.4 (crate (name "siarne") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 0)))) (hash "16dlv8xwkysxy2cgw4arfskynvzam6my9sqk8lbfr93981hx6kq2")))

