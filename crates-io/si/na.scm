(define-module (crates-io si na) #:use-module (crates-io))

(define-public crate-sinabro-0.0.1 (crate (name "sinabro") (vers "0.0.1") (hash "1mss6px5sllw510zhhd6r3acvxdfs7xxz2fy8mm27b9phbyhv8hi")))

(define-public crate-sinais-0.1 (crate (name "sinais") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)) (crate-dep (name "random_name_generator") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "sinais_macro") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0whr557y34fxb96aw5f0v702w5wm0xcba41s5g82yrilnmb0bnd6")))

(define-public crate-sinais_macro-0.1 (crate (name "sinais_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bg5cq449g4kmxg4bs07gm9kiwqm9n9n7gf23ba59bv8nlsyiqbi")))

(define-public crate-sinatra-0.0.0 (crate (name "sinatra") (vers "0.0.0") (hash "0z93zvismbxd823d7szfznqxnqp9d02bw3nlqx83hfb78llgk78y")))

(define-public crate-sinatra_derive-0.0.0 (crate (name "sinatra_derive") (vers "0.0.0") (hash "1zcj41vbbw0j7fgrs30a8mn731dq1m68hrnwbf7pgbka4bfa45kr")))

