(define-module (crates-io si ni) #:use-module (crates-io))

(define-public crate-sinistra-0.0.0 (crate (name "sinistra") (vers "0.0.0") (hash "0vzia111b1v17i84whnv3srl5sshzfv33pyqczldjvrdq97pkylw")))

(define-public crate-sinit-0.1 (crate (name "sinit") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "01ymqqyk92i7nry7q6zjk7h7qvpkxbzzmc90gph2amp4c7n62xcz")))

(define-public crate-sinit-0.1 (crate (name "sinit") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1syy409hkdicqxrjhk39q4cmfdpwhvbm2bq7nkc2m7sh7kh4260n")))

(define-public crate-sinit-0.1 (crate (name "sinit") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "11bazg2gkxycx0x83zd0k4jh832bgal1vv1s45qh7212i1c9zcl1")))

(define-public crate-sinix-0.1 (crate (name "sinix") (vers "0.1.0") (deps (list (crate-dep (name "pickledb") (req "^0.4") (default-features #t) (kind 0)))) (hash "1a20k7dvymljn7qf7wlabab1d6rsrgzsqg7q1yrqls6fchk1r1g3")))

