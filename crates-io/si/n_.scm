(define-module (crates-io si n_) #:use-module (crates-io))

(define-public crate-sin_cos_ln_sqrt-0.1 (crate (name "sin_cos_ln_sqrt") (vers "0.1.0") (hash "1gfwpsy3c66xrbxf0l431v0fj9pcl5gscj5mr4d4xddz7n7b8v2n")))

(define-public crate-sin_cos_ln_sqrt-0.1 (crate (name "sin_cos_ln_sqrt") (vers "0.1.1") (hash "1mmq4mkcb4d0pb1gv2fgzhfsj11fyac00j8pjnl618hbmff5ii73")))

