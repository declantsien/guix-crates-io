(define-module (crates-io si xb) #:use-module (crates-io))

(define-public crate-sixbit-0.1 (crate (name "sixbit") (vers "0.1.0") (hash "0qf058b3iyzb75kr0zxvvdphjcp0rmz6mvfq2bd62zk7wpjmgvw1")))

(define-public crate-sixbit-0.2 (crate (name "sixbit") (vers "0.2.0") (hash "013xp1whdyj1zd5ryhk3lcs7rnbprlwisq0bhbzw7gd95z5y4958")))

(define-public crate-sixbit-0.3 (crate (name "sixbit") (vers "0.3.0") (hash "1ac5p75v1j3d5ln841aixh1fl9266sgnsa2yfcm7iz9xj2wl6b91")))

(define-public crate-sixbit-0.4 (crate (name "sixbit") (vers "0.4.0") (hash "0qngvmzxj4kfzngcbagy7dwk4l0nhmprlab5ilmhk00apj88py1v")))

(define-public crate-sixbit-0.5 (crate (name "sixbit") (vers "0.5.0") (deps (list (crate-dep (name "arbitrary") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0c1rzp7iwmi8b3j625r7bn447zs3pk6chhk9pvzf00bsqsivpl8n")))

