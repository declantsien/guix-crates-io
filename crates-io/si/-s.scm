(define-module (crates-io si -s) #:use-module (crates-io))

(define-public crate-si-scale-0.1 (crate (name "si-scale") (vers "0.1.0") (hash "1dm72zbhyljkjz350fhmm04vbrgqrxj1jrmx7yhcnnz0k7lnw5r9")))

(define-public crate-si-scale-0.1 (crate (name "si-scale") (vers "0.1.1") (hash "08iqn9hviss11glai2lp61ii83g4dz6nwg4sqv4365cxbl5dnskj")))

(define-public crate-si-scale-0.1 (crate (name "si-scale") (vers "0.1.2") (hash "106pdicq86wps3zxscmld3pcql16n6z9nkb4qhwkjhm12qgphhdf")))

(define-public crate-si-scale-0.1 (crate (name "si-scale") (vers "0.1.3") (hash "140pp9rcwlli8rxpc3v4vjdsy68v9vbnc3q8xvaggpxiwhmvsi9x")))

(define-public crate-si-scale-0.1 (crate (name "si-scale") (vers "0.1.4") (hash "0l0h4bl49ba47dsy64qbbzshhlhpbiz52km9c1l54myss90j7y8f")))

(define-public crate-si-scale-0.1 (crate (name "si-scale") (vers "0.1.5") (hash "14iv0b8x0nkplmq3ndghm2k9v530gyaacypi20dckaf9aksaxn6b")))

(define-public crate-si-scale-0.2 (crate (name "si-scale") (vers "0.2.0") (hash "1g2fcnh2y4afgjbpl39abmpalpiqg9p5kay4wpqnn8siwxqfmchk")))

(define-public crate-si-scale-0.2 (crate (name "si-scale") (vers "0.2.1") (hash "1xh138a8xmm8xj3hsk2kgrr7qn1h6iic739pcf79k2b0qzkhkwp9")))

(define-public crate-si-scale-0.2 (crate (name "si-scale") (vers "0.2.2") (hash "0vv6sxz4zvfmp17fgbnznrc9lmg6spax2x5xvl9knd48yj5vdgj4")))

