(define-module (crates-io si ck) #:use-module (crates-io))

(define-public crate-sick-0.0.0 (crate (name "sick") (vers "0.0.0") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "08kcnscj5snvp8q5nis3y3yc2zmfpvjkyg78p1vk84csy4dsw9sb")))

(define-public crate-sickOS-0.0.0 (crate (name "sickOS") (vers "0.0.0") (hash "0ziwxfbilkyfiryf53pgv0gq8xbka2q93d5d3h7y7075xx995j8k")))

