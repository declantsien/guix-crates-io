(define-module (crates-io dl lv) #:use-module (crates-io))

(define-public crate-dllvoid-0.1 (crate (name "dllvoid") (vers "0.1.0") (deps (list (crate-dep (name "cural") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winnt" "winbase" "winuser" "synchapi" "memoryapi" "handleapi" "libloaderapi" "processthreadsapi"))) (default-features #t) (kind 0)))) (hash "17hlsdyfvlikkxzhm1mc5jxj7r5cpmwdq57imwlcibjfjlybrgln")))

