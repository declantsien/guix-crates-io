(define-module (crates-io dl e-) #:use-module (crates-io))

(define-public crate-dle-encoder-0.1 (crate (name "dle-encoder") (vers "0.1.1") (hash "1arkhmjl2ix8zly6nzic832s0i4hhgz7jjp5sdzf6hmhf3wfs64w")))

(define-public crate-dle-encoder-0.1 (crate (name "dle-encoder") (vers "0.1.2") (hash "048675wmgk4as9kpnqznghjvkixbgmk4g3ip336zw1vmf4b7h4gq")))

(define-public crate-dle-encoder-0.1 (crate (name "dle-encoder") (vers "0.1.3") (hash "1zrshan5q4psz1jgb2ypxqy77vd0c1ib5w8m0v894hxrig7rzq0v")))

(define-public crate-dle-encoder-0.1 (crate (name "dle-encoder") (vers "0.1.4") (hash "06z63cnvw6qvz7ym8wr2c74cvg4fq4z41p3jv5g53bqvhcnpm4i7")))

