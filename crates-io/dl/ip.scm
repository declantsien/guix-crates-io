(define-module (crates-io dl ip) #:use-module (crates-io))

(define-public crate-dlipower-0.1 (crate (name "dlipower") (vers "0.1.0") (deps (list (crate-dep (name "md-5") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("cookies"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.18") (default-features #t) (kind 0)))) (hash "0makhxg92620kwggmdgjagv9m2fmql9lc1c8mvslk24d38x0abbf")))

