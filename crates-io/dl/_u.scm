(define-module (crates-io dl _u) #:use-module (crates-io))

(define-public crate-dl_uuid-0.4 (crate (name "dl_uuid") (vers "0.4.0") (deps (list (crate-dep (name "arc-swap") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nanorand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "089wnqd5vn94ynpz3i47w3njjc14ms92b52wf0bhd8x1l09bkbgh")))

(define-public crate-dl_uuid-0.4 (crate (name "dl_uuid") (vers "0.4.1") (deps (list (crate-dep (name "arc-swap") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0wx0492fl3rik0ir0m423bs2fykmm1y7kighs8ks0wmsps20s7hn")))

