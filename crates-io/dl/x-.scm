(define-module (crates-io dl x-) #:use-module (crates-io))

(define-public crate-dlx-rs-0.0.0 (crate (name "dlx-rs") (vers "0.0.0") (hash "1qqic9xnps2bd21c325k8p8aqqrcsfd2qi79d4pnxg4lnss13c3j")))

(define-public crate-dlx-rs-0.0.1 (crate (name "dlx-rs") (vers "0.0.1") (hash "18kd5wyc3nk0acnz8v6ffr45nvfbr3z4rwc30818p99dj40g07ri")))

(define-public crate-dlx-rs-0.0.2 (crate (name "dlx-rs") (vers "0.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "1af5a999v0wl5494ir54kz35dpw26bkm76bhkg9qgv3jf1g4ibji")))

(define-public crate-dlx-rs-0.0.3 (crate (name "dlx-rs") (vers "0.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "00120v5vn9avc613g7id8lj8l3z05appl78r6xx94m36wbas1cy0")))

(define-public crate-dlx-rs-1 (crate (name "dlx-rs") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "058zlicvg728vvr20jid65lwf7vbws39lh5lczx8722kicj9lfn1")))

(define-public crate-dlx-rs-1 (crate (name "dlx-rs") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1xh4svkr320jprppk0gyggfginx0w4y64ax3zpb131n97q1ds5m4")))

