(define-module (crates-io dl fc) #:use-module (crates-io))

(define-public crate-dlfcn-0.1 (crate (name "dlfcn") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qnla77jb55ka493w2vqwlwnmrj8lawj0ha18qrmwqqa9i89krxl")))

