(define-module (crates-io dl pa) #:use-module (crates-io))

(define-public crate-dlpack-0.1 (crate (name "dlpack") (vers "0.1.0") (hash "1ini3jsn0wn71drizayyl8apygr81yn3s0grdyfq6yz54nmkibiz")))

(define-public crate-dlpack-0.1 (crate (name "dlpack") (vers "0.1.1") (hash "1p2c1b7ygxygwlfg0yawyb1s3pgqykz0wgjib3m5vzmnk8vbmvai")))

(define-public crate-dlpack-0.1 (crate (name "dlpack") (vers "0.1.2") (hash "0s16hj53mvla0fm2zv0is4fh1g3mhkhbbqaddh310yyh7flj8l9q")))

(define-public crate-dlpack-0.1 (crate (name "dlpack") (vers "0.1.3") (hash "1id1aylmpkmm04p6xxy1c3w6dish7jsk7aba4vd27ysc2kwbbbrv")))

(define-public crate-dlpack-0.2 (crate (name "dlpack") (vers "0.2.0") (hash "19ihf97w6wf6vajs3cx96d5hrsjpghqk7xfsgjawakmjvzpw3grm")))

(define-public crate-dlpack-rs-0.1 (crate (name "dlpack-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req ">=0.60.1") (default-features #t) (kind 1)))) (hash "00w0m3pisb6mw5520lq57h1w6xkq6153c07b0yrir79imqz5hm98")))

(define-public crate-dlpack-rs-0.1 (crate (name "dlpack-rs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req ">=0.60.1") (default-features #t) (kind 1)))) (hash "1vgp27lx2jp1mlqydavh1mzhxa70swcmc8l0sfsppm7k3bcnnpyf")))

(define-public crate-dlpack-rs-0.1 (crate (name "dlpack-rs") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req ">=0.60.1") (default-features #t) (kind 1)))) (hash "16n76rdw0ha7j7aragdbj7ikp3nkzff2b5hbrqvanc9bk9ic5ass")))

(define-public crate-dlpack-sys-0.1 (crate (name "dlpack-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "0cgyh0dxp99pl3wnciq0i260lvnsn0pap7q82df1mdhh09fwvh1y")))

(define-public crate-dlpack-sys-0.1 (crate (name "dlpack-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "1gs5b9yill885rjd48g3lfpf3vbdgrxa34kfyd5p2kp8y3ibip2y")))

(define-public crate-dlpackrs-0.1 (crate (name "dlpackrs") (vers "0.1.0") (deps (list (crate-dep (name "dlpack-sys") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "enumn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z4z83xnmdj39p474q43wnszgcwz6x0ics2dqsav8qnkprsm8gh6") (yanked #t) (rust-version "1.57.0")))

(define-public crate-dlpackrs-0.2 (crate (name "dlpackrs") (vers "0.2.0") (deps (list (crate-dep (name "dlpack-sys") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "enumn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jp6017mcbyhlgk1jhwxvm4009wkhsqja325w94p3hakic49fzyw") (rust-version "1.57.0")))

(define-public crate-dlpark-0.1 (crate (name "dlpark") (vers "0.1.0") (deps (list (crate-dep (name "pyo3") (req "^0.18.3") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "1g282x5m1a25d6wnvj9czl4gr0dfy0vyvfa9v0whqvy7i2yr67cn") (features (quote (("default" "python")))) (v 2) (features2 (quote (("python" "dep:pyo3"))))))

(define-public crate-dlpark-0.2 (crate (name "dlpark") (vers "0.2.0") (deps (list (crate-dep (name "pyo3") (req "^0.18.3") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)))) (hash "1csi6ixs80ajdh2g07jixvs9dzbrfi2xpnpn8gp774drxm1dd2ja") (features (quote (("default" "pyo3")))) (v 2) (features2 (quote (("pyo3" "dep:pyo3"))))))

(define-public crate-dlpark-0.2 (crate (name "dlpark") (vers "0.2.1") (deps (list (crate-dep (name "pyo3") (req "^0.18.3") (optional #t) (default-features #t) (kind 0)))) (hash "1ni5qldsd56sjn61ay6azrb47bffjysj8jr3sprf2vnxkppyrc9a") (features (quote (("default" "pyo3")))) (v 2) (features2 (quote (("pyo3" "dep:pyo3"))))))

(define-public crate-dlpark-0.2 (crate (name "dlpark") (vers "0.2.2") (deps (list (crate-dep (name "pyo3") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vgdwwzg9m06dy7hwiy3dcxpd4xyjmk6mfw3hr1ydg4900yibs3b") (features (quote (("default" "pyo3")))) (v 2) (features2 (quote (("pyo3" "dep:pyo3"))))))

(define-public crate-dlpark-0.3 (crate (name "dlpark") (vers "0.3.0") (deps (list (crate-dep (name "half") (req "^2.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "0y4arddwb80rbpy661486mh215bxgfnf4jbrvxlx5ai4vkhba985") (features (quote (("default")))) (v 2) (features2 (quote (("pyo3" "dep:pyo3") ("half" "dep:half"))))))

(define-public crate-dlpark-0.4 (crate (name "dlpark") (vers "0.4.0") (deps (list (crate-dep (name "half") (req "^2.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19") (optional #t) (default-features #t) (kind 0)))) (hash "1n1bh4c5r2d9k3zcn3k96s3mk4kwlq11qqspzznp66maj1sm6hgr") (features (quote (("default")))) (v 2) (features2 (quote (("pyo3" "dep:pyo3") ("half" "dep:half"))))))

(define-public crate-dlpark-0.4 (crate (name "dlpark") (vers "0.4.1") (deps (list (crate-dep (name "half") (req "^2.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21") (optional #t) (default-features #t) (kind 0)))) (hash "130ws4y4wv96l7i1v55zs81pijf375y190qn85wwpr9csbapb4k4") (features (quote (("default")))) (v 2) (features2 (quote (("pyo3" "dep:pyo3") ("half" "dep:half"))))))

