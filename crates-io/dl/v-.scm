(define-module (crates-io dl v-) #:use-module (crates-io))

(define-public crate-dlv-list-0.1 (crate (name "dlv-list") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "05bqd0gb108ywhw137l4frab3hnsx5pzb2ig6qhr87yv0j0x1j3n")))

(define-public crate-dlv-list-0.1 (crate (name "dlv-list") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "08wpcw0jiwgqlci2dh5g30yvf1hqczydlqwwy8schxc8c2fv9dm5")))

(define-public crate-dlv-list-0.1 (crate (name "dlv-list") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0740y7qi9sph5i7wskxf3k6yya2nah90r78zcpix5mb71531cjhd")))

(define-public crate-dlv-list-0.1 (crate (name "dlv-list") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1qwqy454hinl5wp1r37d5a3chbf0fcjb796q361mmig3lymmn6lj")))

(define-public crate-dlv-list-0.1 (crate (name "dlv-list") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0pb6ckvd75gs7l961pd6mryam6710i5jsns4lnz62wvmbhr3bqc5")))

(define-public crate-dlv-list-0.1 (crate (name "dlv-list") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "01ph1ydc9vfa837alm6lgjpkiy2a0b413xx2r89m3ab4lvs5p4h4")))

(define-public crate-dlv-list-0.2 (crate (name "dlv-list") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0j5vmr3rgkz37d0ac7rw1hyypdd3kzfiagjyklymdh7g85nykvpg")))

(define-public crate-dlv-list-0.2 (crate (name "dlv-list") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1s5j7zafi5gvjxdfqffyrvqj7hjl90w82isfg2sv982lgc41gv20")))

(define-public crate-dlv-list-0.2 (crate (name "dlv-list") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0v42n28mx9r6vr43dsqkh5dgvb9m0wyjp7fb20m331m7p48ijf8v")))

(define-public crate-dlv-list-0.2 (crate (name "dlv-list") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "06r1nskj3x56p5wqz2bgl6q3rpyymrb0k0zpbvk8c6qcd4mkzpv8")))

(define-public crate-dlv-list-0.2 (crate (name "dlv-list") (vers "0.2.4") (hash "0sqfbwmbbx5gj0dvspnx3g1vdl40q9ps15z4pikiij7nwzj1m3hg") (yanked #t)))

(define-public crate-dlv-list-0.3 (crate (name "dlv-list") (vers "0.3.0") (hash "0mqj5rdkcjksw3kvjj0nga6rzcpppx0kimjwi527yhifz6kw5206")))

(define-public crate-dlv-list-0.4 (crate (name "dlv-list") (vers "0.4.0") (deps (list (crate-dep (name "coverage-helper") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "0smrlmfa3rmwb2jrxy5ins4r85rrqrqdbc3qjfm6ils9xqa4l7mx")))

(define-public crate-dlv-list-0.5 (crate (name "dlv-list") (vers "0.5.0") (deps (list (crate-dep (name "const-random") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "coverage-helper") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "0wxd92136d5fir8h7lgba7dk8q748krv7k4qspgkwrj4sdrzsafm") (features (quote (("std") ("default" "std"))))))

(define-public crate-dlv-list-0.5 (crate (name "dlv-list") (vers "0.5.1") (deps (list (crate-dep (name "const-random") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "coverage-helper") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "1x2cyvbs0brrj8zhdy2ma38m2narkrfg473j4ly2cpvbqi6x1sla") (features (quote (("std") ("default" "std"))))))

(define-public crate-dlv-list-0.5 (crate (name "dlv-list") (vers "0.5.2") (deps (list (crate-dep (name "const-random") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "coverage-helper") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "0pqvrinxzdz7bpy4a3p450h8krns3bd0mc3w0qqvm03l2kskj824") (features (quote (("std") ("default" "std"))))))

