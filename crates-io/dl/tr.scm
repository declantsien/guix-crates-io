(define-module (crates-io dl tr) #:use-module (crates-io))

(define-public crate-dltree-0.1 (crate (name "dltree") (vers "0.1.0") (hash "01d5d8kwyf6kdcn6vzxriqrfc7iyf8bllbcizvankkawmiznibv7")))

(define-public crate-dltree-0.1 (crate (name "dltree") (vers "0.1.1") (hash "1qjyq02ygsx1lgr7i0lfd9g9jfj55kn4lxvi4z60cpf70yminrfi")))

(define-public crate-dltree-0.1 (crate (name "dltree") (vers "0.1.2") (hash "0mvnai9c898ki4mjr2c43rjrj59513l2xkw6wn11pxcjz7cy2j42")))

(define-public crate-dltree-0.1 (crate (name "dltree") (vers "0.1.3") (hash "1gbym4zh9zhkj7fplwlsszh63jhxl3alf2pxjvrwqq3y97issqw9")))

(define-public crate-dltree-0.1 (crate (name "dltree") (vers "0.1.4") (hash "019861j56m67p6f2nfjrzrn3pk3kp2zlzb9gzl0j3zizrpj0vdsp")))

(define-public crate-dltree-0.1 (crate (name "dltree") (vers "0.1.5") (hash "026b7ncx11f3blgq86h29axac8v5lhfjpma6ng5fr7n64ijbw30b")))

