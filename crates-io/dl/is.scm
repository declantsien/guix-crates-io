(define-module (crates-io dl is) #:use-module (crates-io))

(define-public crate-dlist-0.1 (crate (name "dlist") (vers "0.1.0") (deps (list (crate-dep (name "lipsum") (req "^0.7") (default-features #t) (kind 0)))) (hash "1b55ikkj88midxhi09zf3kanhfhs0xg7ka99fzlil19rapkcywrk")))

(define-public crate-dlist-0.1 (crate (name "dlist") (vers "0.1.1") (deps (list (crate-dep (name "lipsum") (req "^0.7") (default-features #t) (kind 2)))) (hash "004jzch3lcfi2za07j259nxw879k4w5iigkc6rayjjqn7rcncdld")))

(define-public crate-dlist-0.1 (crate (name "dlist") (vers "0.1.2") (deps (list (crate-dep (name "lipsum") (req "^0.7") (default-features #t) (kind 2)))) (hash "1s21gqz4dv01wmxc7601f0rkxb83pl5mv2r5xq7ssgsyf5yx2lcx")))

(define-public crate-dlist-0.1 (crate (name "dlist") (vers "0.1.3") (deps (list (crate-dep (name "lipsum") (req "^0.7") (default-features #t) (kind 2)))) (hash "0i3wkf4sq6xwrmv0x03c6pv7l3f69ym3bq1974lqdav3sx65fv9c")))

(define-public crate-dlist-0.1 (crate (name "dlist") (vers "0.1.4") (deps (list (crate-dep (name "lipsum") (req "^0.7") (default-features #t) (kind 2)))) (hash "18w1gzr3j6dxhqqsbxmjz177k6jg7xhkm1sirwr6w5q1zi0za1wq")))

(define-public crate-dlist-top-0.1 (crate (name "dlist-top") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "futures-channel") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.15.0") (features (quote ("rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1bkypfj45jl23h1w2wgwdg4jqsh53c7h60gn2pi4kbln15pmzv4p")))

