(define-module (crates-io dl an) #:use-module (crates-io))

(define-public crate-dlang-0.1 (crate (name "dlang") (vers "0.1.0") (hash "10wn8n8aipd2d584vccm093s9lkh02j3j964d6qmqx3rhmb6qkkj")))

(define-public crate-dlang-0.1 (crate (name "dlang") (vers "0.1.1") (hash "04p7b1pgplhpm0jrfv41z023j9168kpb3lzfsn9zmf7ydq5svbsy")))

(define-public crate-dlang-0.1 (crate (name "dlang") (vers "0.1.2") (hash "1zfxskfi2zv05apdcv4pasmfpc2qh5r32ah0p2znqkayykdn8lvr")))

(define-public crate-dlang-0.1 (crate (name "dlang") (vers "0.1.3") (hash "0hlcq7xb3zq0d8nlmlry3zzrxhsmppkamj6v2p138kmdkx68dlny")))

