(define-module (crates-io dl zi) #:use-module (crates-io))

(define-public crate-dlzip-0.1 (crate (name "dlzip") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "number_prefix") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0v2l2hqxmzcx7hw68sh93aq7mvxc1a0cspn0dxp7842jwg7hgdxv") (yanked #t)))

(define-public crate-dlzip-0.1 (crate (name "dlzip") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "number_prefix") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0hg5csn0prc74b6g9wis7kh7kp7i007naclxb9fspan3nl3lrxvw") (yanked #t)))

