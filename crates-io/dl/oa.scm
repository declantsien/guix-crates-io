(define-module (crates-io dl oa) #:use-module (crates-io))

(define-public crate-dload-0.1 (crate (name "dload") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.30.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0wczvmq1rg76hvd34rg9x2a7gqklsxbbf2920w9w8bz4v95ipq27")))

(define-public crate-dload-0.1 (crate (name "dload") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.30.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1lcwx1zwpxjmimk9sxbprxiyblp3xhdjwzp6ns224c73q6xpy5vj")))

(define-public crate-dload-0.1 (crate (name "dload") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.30.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "10iswlr2shdm693x4c6kqvbnq6ikd87c8pacp3jfg02mi0axgylc")))

