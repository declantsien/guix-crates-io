(define-module (crates-io lm b_) #:use-module (crates-io))

(define-public crate-lmb_engine_simulator-0.1 (crate (name "lmb_engine_simulator") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.34") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jnlsnk2jb2yh48s2vmx1fnzv6qqc6y2p7a57dz4p3d95fmj238f")))

