(define-module (crates-io lm at) #:use-module (crates-io))

(define-public crate-lmaths-1 (crate (name "lmaths") (vers "1.0.0") (hash "04zm9s0h6dllinwb4694qidaxaij9q378vp26a1yhkgfz84fwrmn")))

(define-public crate-lmaths-1 (crate (name "lmaths") (vers "1.0.1") (hash "1p3z17vn2s2pjwjq89nf8vwbqv6p6bi9r8r7w0azx53664cdrc8b")))

(define-public crate-lmaths-1 (crate (name "lmaths") (vers "1.0.2") (hash "0kyzdnsllkqi68k79ng2bnpfwx5ylripgqiig49sm3l7zmpikv7l")))

(define-public crate-lmaths-1 (crate (name "lmaths") (vers "1.0.3") (hash "1xcd0irr7wgvhchdyz3rq1zzs10mgz1m38ri0x7c0qsy12h1jhcb")))

(define-public crate-lmaths-1 (crate (name "lmaths") (vers "1.0.4") (hash "0csidvxh69mywdb08mxa5g8w76mzww07ssh7ciqbs2p6gl01x53q")))

(define-public crate-lmaths-1 (crate (name "lmaths") (vers "1.0.5") (hash "0zdjilani9aqpr5pw21vvjdjax48xrxrzqv2w6lgpr4xvvaxrx52")))

