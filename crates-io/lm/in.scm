(define-module (crates-io lm in) #:use-module (crates-io))

(define-public crate-lminc-2 (crate (name "lminc") (vers "2.0.1") (deps (list (crate-dep (name "uuid") (req "^1.2.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 2)))) (hash "16cb4ml9vnz5y0lk4h4ns3hgx663s12fxffvdj3nbm1fxrwbflw6") (features (quote (("std" "alloc") ("extended") ("default" "std" "extended") ("alloc"))))))

