(define-module (crates-io lm nk) #:use-module (crates-io))

(define-public crate-lmnkjgkj-std-backtrace-anyhow-0.0.1 (crate (name "lmnkjgkj-std-backtrace-anyhow") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 2)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.66") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1plil35bksnpfc9fv4mx36xxb2s7vfack46wdyk2ifljylwjxj3p") (features (quote (("std") ("default" "std")))) (yanked #t) (rust-version "1.39")))

