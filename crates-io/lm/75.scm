(define-module (crates-io lm #{75}#) #:use-module (crates-io))

(define-public crate-lm75-0.1 (crate (name "lm75") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "138mahjbchyxjba9p9p36wwvxbsl087h8g2aqix4p1a1mmh4l7bb")))

(define-public crate-lm75-0.1 (crate (name "lm75") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "070bfg7lh8xn3s8vby41bqkdanwdkjya2a9zxqg4qw688a32m9yi")))

(define-public crate-lm75-0.1 (crate (name "lm75") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1g91nx5m4sh0mbvfh5iyxfjfbd8640z104lrdmrvrlnykpz14v5w")))

(define-public crate-lm75-0.2 (crate (name "lm75") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1dhdbp8plvdfnj1alr1mm39kfzbmz7i1sbccwjdppnnnlz3wsnph")))

(define-public crate-lm75-1 (crate (name "lm75") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10") (features (quote ("eh1"))) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.4") (default-features #t) (kind 2)))) (hash "1w6xl3prw2h6v5sg35snlmf978n80wggklhcxp1k4gscd6ajs323")))

