(define-module (crates-io lm br) #:use-module (crates-io))

(define-public crate-lmbr-0.1 (crate (name "lmbr") (vers "0.1.0") (deps (list (crate-dep (name "lmbr_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lmbr_sys") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)))) (hash "016c0dlzgavvz4jb3s1xz239pfh6l0xhm8la5wm0d1a2vn5fvzgw") (features (quote (("default" "lmbr_sys"))))))

(define-public crate-lmbr_build-0.1 (crate (name "lmbr_build") (vers "0.1.0") (hash "1aivflxszx8q8nn4zsrbk3l61y088jd1djhgxp3v59absypz5nh5")))

(define-public crate-lmbr_logger-0.1 (crate (name "lmbr_logger") (vers "0.1.0") (deps (list (crate-dep (name "lmbr_sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qwi2mfvnnmn38i27v6gwyws79b8girz8yrk8i7xcg64hnf72sac")))

(define-public crate-lmbr_sys-0.1 (crate (name "lmbr_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "lmbr_build") (req "^0.1") (default-features #t) (kind 1)))) (hash "0m7rg19xh1gxyq8z40ac9bn797mm6wa28r30qibf9w5xmqffrdxp") (features (quote (("lmbr_fw_azcore") ("lmbr_editor") ("default" "lmbr_editor" "lmbr_fw_azcore"))))))

