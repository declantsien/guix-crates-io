(define-module (crates-io lm #{36}#) #:use-module (crates-io))

(define-public crate-lm36011-0.1 (crate (name "lm36011") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "152sifsz73rd9a23k1w3asz0rqik7r2nljkgddrcdbckbhpy1277")))

