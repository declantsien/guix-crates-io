(define-module (crates-io lm bd) #:use-module (crates-io))

(define-public crate-lmbd-0.1 (crate (name "lmbd") (vers "0.1.0") (hash "1c8n8zwhshch4aivncz5zb56zdv3f51f3pm5nvmj4j837nqxz77c")))

(define-public crate-lmbd-0.1 (crate (name "lmbd") (vers "0.1.1") (hash "1san55q70d9cz96mhvfj9kmvg4qyhn94ir6shbn5cbxl6iimj5kc")))

(define-public crate-lmbd-0.1 (crate (name "lmbd") (vers "0.1.2") (hash "0lc1ihjq7wpyjbapsdnc4k9c3ggyl8kk1s7m59cm3jx6bqhfil82")))

(define-public crate-lmbd-0.1 (crate (name "lmbd") (vers "0.1.3") (hash "1ypvhv595zwnnyp6rm8n455ai10sb409h4g50xgg7ssrxcjmyvr7")))

(define-public crate-lmbd-0.1 (crate (name "lmbd") (vers "0.1.4") (hash "0b2pxd804pwzmcsar7p9lkybyqvgplp6nmr00w1a989fdl3qyzri")))

