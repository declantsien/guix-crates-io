(define-module (crates-io lm ml) #:use-module (crates-io))

(define-public crate-lmml-0.0.0 (crate (name "lmml") (vers "0.0.0") (hash "1rs098fyrhkbwj4k75dydqashdn9wfq4b6mxr3f9wxm2q5rld9hn")))

(define-public crate-lmml-0.1 (crate (name "lmml") (vers "0.1.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "07gvkhlpyafw6n95nj2811ilmzqln4i75qzagvzbsdiyqlvcmcnf")))

(define-public crate-lmml-0.2 (crate (name "lmml") (vers "0.2.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1v134vh7cxqf5c4zy1ld24x59wx8dy6zcmv346q8gmxa74nc0czm")))

(define-public crate-lmml-0.3 (crate (name "lmml") (vers "0.3.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1bay9112wvwncyxw5s2aqybp7v7a0ypyar1dik9zazrzbprzdvfn")))

(define-public crate-lmml-0.4 (crate (name "lmml") (vers "0.4.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1yfqwpqc50lak9wi25y1jg8clhjcansizwgjii5sbla9p5mvbqsn")))

(define-public crate-lmml-0.4 (crate (name "lmml") (vers "0.4.1") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "16bzgkcxd3b4zziv2lrykqkfmn2hg9w2v1aw5i5cp6f50p2d9y2v")))

(define-public crate-lmml-0.5 (crate (name "lmml") (vers "0.5.0") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "17dzl3b0rzimqfr6j0nldc8514qhi6jcb8x6q6ja1n4bdag0gggz")))

(define-public crate-lmml-0.5 (crate (name "lmml") (vers "0.5.1") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "11p8d1fwp25ipw8nnrlkwxqhzn6hwzy1dfvykg2r6xzvk96dhwfc")))

(define-public crate-lmml-cli-0.0.0 (crate (name "lmml-cli") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "01anc01mblikbhd6sdn00ir0y76kcy47qbm13mmdcawmbg0xbggc")))

(define-public crate-lmml-cli-0.1 (crate (name "lmml-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "013yglifx6x8kc1pq6k00ax3q57jy3cnjsj2191alpjh1h8qkzml")))

(define-public crate-lmml-cli-0.2 (crate (name "lmml-cli") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1dsicx090ga5x61v76k4jq3ncidldmfbam9pjdi84mb3rhwxwds9")))

(define-public crate-lmml-cli-0.3 (crate (name "lmml-cli") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1snszknwafd1klxz3nnpjdsm753zszizlj37ic8g529wqz7hixpz")))

(define-public crate-lmml-cli-0.4 (crate (name "lmml-cli") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1nqzwbcgri763kx39qzpdg9g0f9z2yxvad6ljwhwz3k185qllf4w")))

(define-public crate-lmml-cli-0.4 (crate (name "lmml-cli") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "0pd2q62ry3rsmhx35yg2k0p27qb6wr5dyxb5j80717pzf00vb0ps")))

(define-public crate-lmml-cli-0.5 (crate (name "lmml-cli") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1h207sin6p9z8a6cf3zydiy5q0c2f9m3ffhc0qgdprxxdp9c8hp3")))

(define-public crate-lmml-cli-0.5 (crate (name "lmml-cli") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmml") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "lmml-parser") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0yf7rak71cxzg5lzcmcqldk05apfsgs3p4h0l73qyisiqipsfqdy")))

(define-public crate-lmml-parser-0.0.0 (crate (name "lmml-parser") (vers "0.0.0") (deps (list (crate-dep (name "lmml") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1p63wyzjwnbk5bc9n0pcsacajfxdckfl406a6i37iblpk9v4f32l")))

(define-public crate-lmml-parser-0.1 (crate (name "lmml-parser") (vers "0.1.0") (deps (list (crate-dep (name "lmml") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "03hvf3hznjf5aj7s0b54v2ck5mas1kqwvpsb65hw9mx714q6dhmv")))

(define-public crate-lmml-parser-0.2 (crate (name "lmml-parser") (vers "0.2.0") (deps (list (crate-dep (name "lmml") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0l2h6pq6zknapyzn66n2fvbs949klph609nr4byg28dlrff59l2s")))

(define-public crate-lmml-parser-0.3 (crate (name "lmml-parser") (vers "0.3.0") (deps (list (crate-dep (name "lmml") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0qmpxvp95nwsi5p27arwzz712q0bv6lgymxcpiq1yydp75hk70n8")))

(define-public crate-lmml-parser-0.4 (crate (name "lmml-parser") (vers "0.4.0") (deps (list (crate-dep (name "lmml") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0mpjz8ld3njhi0g5fcvs9vljhdrnll7if5xyzpm4v0i9xi38vvj3")))

(define-public crate-lmml-parser-0.4 (crate (name "lmml-parser") (vers "0.4.1") (deps (list (crate-dep (name "lmml") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1vvkdnwaw5yywk1rchlsnbkzd8vsdf4kihkxdka490gd08axllmj")))

(define-public crate-lmml-parser-0.5 (crate (name "lmml-parser") (vers "0.5.0") (deps (list (crate-dep (name "lmml") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0xjnri3krgjbmjr27wnknjl7gqfmcks0vgvpqb9ky32yqfsfmh53")))

(define-public crate-lmml-parser-0.5 (crate (name "lmml-parser") (vers "0.5.1") (deps (list (crate-dep (name "lmml") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0rn98r26c59bvwk8hfn3gn3xqrg75hb6g08lqqk57xq6fhk949hs")))

