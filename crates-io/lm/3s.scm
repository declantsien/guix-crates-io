(define-module (crates-io lm #{3s}#) #:use-module (crates-io))

(define-public crate-lm3s6965-0.1 (crate (name "lm3s6965") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.5") (features (quote ("device"))) (default-features #t) (kind 0)))) (hash "1f0ddrffrhhf0b3006f7dnrp53hxzq094b1vvbb7jm3a170cp2fx")))

(define-public crate-lm3s6965-0.1 (crate (name "lm3s6965") (vers "0.1.1") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.5") (features (quote ("device"))) (default-features #t) (kind 0)))) (hash "11r6m38qv7sizgqxgln94xj5blzq0m1qx6zsvyss0fbjf88q3yim")))

(define-public crate-lm3s6965-0.1 (crate (name "lm3s6965") (vers "0.1.2") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.5") (features (quote ("device"))) (default-features #t) (kind 0)))) (hash "1zb6196yzvkdp51i4n7sbrig4hv76ywndylgqwvz6b9qdhm7s1v2")))

(define-public crate-lm3s6965-0.1 (crate (name "lm3s6965") (vers "0.1.3") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.5") (features (quote ("device"))) (default-features #t) (kind 0)))) (hash "1bg02nwsdw5lk5d6yxhgg396mfyxs4na763jkyn0w5lmfhm09646")))

(define-public crate-lm3s6965-0.2 (crate (name "lm3s6965") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7") (features (quote ("device"))) (default-features #t) (kind 0)))) (hash "0ijl97sqvnir78w393jkpdx6jhmdszqmx6gp9i1zvs7yc19yvmqk")))

