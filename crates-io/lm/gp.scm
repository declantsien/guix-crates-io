(define-module (crates-io lm gp) #:use-module (crates-io))

(define-public crate-lmgpt-0.0.1 (crate (name "lmgpt") (vers "0.0.1") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0") (default-features #t) (kind 0)))) (hash "1qvq14mrg29xmkx0ayj1fyz0pdhnzqypqax8aqjn1pkn77vn43jg")))

(define-public crate-lmgpt-0.0.2 (crate (name "lmgpt") (vers "0.0.2") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0") (default-features #t) (kind 0)))) (hash "087k53b80pqlg0x7ind0fk9gbd5pik33rw04jpkjznirg11h7sdl")))

(define-public crate-lmgpt-0.1 (crate (name "lmgpt") (vers "0.1.0") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "16cswz4b8s50d707m2g2pf3fpk4i6m4y50kh7543a9g7xfzy61xs")))

(define-public crate-lmgpt-0.1 (crate (name "lmgpt") (vers "0.1.1") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0ch16m76jdggxvs227lq3sdkz0xppwa0w8y9b0nvvxmcvyprb48x")))

(define-public crate-lmgpt-0.1 (crate (name "lmgpt") (vers "0.1.2") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "11l1kj3rvns476xyldv69grbs6qlhm5cnb51zjvxhm4j1rq63iqj")))

(define-public crate-lmgpt-0.1 (crate (name "lmgpt") (vers "0.1.3") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "15jjyracwjp3wzvdfa1mj5iv6yrwzic9v9r3v0nvgsmqpr1jk58l")))

(define-public crate-lmgpt-0.1 (crate (name "lmgpt") (vers "0.1.4") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1pan6fkq9338x6x80x4dd6vrhn8r4f2j0a0mm5b9wkjcx6rk2z4k")))

(define-public crate-lmgpt-0.1 (crate (name "lmgpt") (vers "0.1.5") (deps (list (crate-dep (name "lambda_mountain") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0ll5h3ahpik9wvhwgx8cc7b9l0k0zcx4s9gq63qcpr918amy4h6l")))

