(define-module (crates-io lm #{73}#) #:use-module (crates-io))

(define-public crate-lm73-0.1 (crate (name "lm73") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "i2cdev") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4") (default-features #t) (kind 2)))) (hash "0lpyqx6nlhahvknnsdjawi3dxg2kixym19pf59hipal1n3dpzh0f")))

(define-public crate-lm73-0.1 (crate (name "lm73") (vers "0.1.1") (deps (list (crate-dep (name "cast") (req "^0.2.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fpa") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "163fq4p5xigssj7g2l5k3c78nqjl78cigxnpjp12llapychz20a7")))

