(define-module (crates-io lm ar) #:use-module (crates-io))

(define-public crate-lmarkov-0.1 (crate (name "lmarkov") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gmkqssjyxssxxvb5ib52zmj1zmj1y9khqsxw7gnbq7c35dd8pxw") (features (quote (("serialization" "serde" "serde_json"))))))

