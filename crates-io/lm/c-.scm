(define-module (crates-io lm c-) #:use-module (crates-io))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.0") (hash "1wj556l75mla9azpglh479wk1v4i4h5m9i88ws7qfpdr3hrw1wl9")))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.1") (hash "1cvivwy1zws2amhzwml7czp3w32sg1dlcyswj9zzjacjglncmkz9")))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.2") (hash "087xpjy5bxhd45x6akd1gi69kir07rfc7f57psrz7609lkwl4vz7")))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "104fskpgf4svpzqjicx5lh9ld7h00wd9q9s5j2x3g7fx91k2pyh8") (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0hc9f5j8b83n97c17dipz4y46liq22246cx1lv163qz430hsq21c") (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "07dl30cs3mrwnnm8z7siv6kv6d2yaabfvck9fiqzmkf5lnqph587") (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zx7wmx5sg84p4biv8qqmlpcsb7yf6n4bal10wfp59nm9ig5ngpv") (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-assembly-0.1 (crate (name "lmc-assembly") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "03cp0gv1ryzgfjsryi72391vbai8rl140365ym6j9j8xb4ljs6a0") (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_derive" "dep:serde_arrays"))))))

(define-public crate-lmc-cli-0.1 (crate (name "lmc-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmc-assembly") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nk4fynqvyfhl61lh564wickh7sqrh1ypviw4yin7pgfazv2z0zc")))

(define-public crate-lmc-cli-0.1 (crate (name "lmc-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmc-assembly") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1gfh6b6s2r75ssfzvdz6caqmpjv673zs9i2x0z4lj4vmr4sw078l")))

(define-public crate-lmc-cli-0.1 (crate (name "lmc-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lmc-assembly") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1cxgjg18r9drv5kym80y8i4w2wyfvc9bscdpy38il9kmrvz69qwn")))

