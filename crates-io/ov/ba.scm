(define-module (crates-io ov ba) #:use-module (crates-io))

(define-public crate-ovba-0.1 (crate (name "ovba") (vers "0.1.0") (deps (list (crate-dep (name "cfb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "codepage") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)))) (hash "0ib1ai86bxfjk1srd2vvg1v27nc0aiksdpcvb0qcx2jpc8m0bf1k")))

(define-public crate-ovba-0.2 (crate (name "ovba") (vers "0.2.0") (deps (list (crate-dep (name "cfb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "codepage") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)))) (hash "1ndxd0khg9a6p8v1fc7yfv5li9kvcjgajmk5gk1x0y542h0qbnys")))

(define-public crate-ovba-0.3 (crate (name "ovba") (vers "0.3.0") (deps (list (crate-dep (name "cfb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "codepage") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)))) (hash "1bmrk74fs1bldxlqwg4d0aaqrzc2i1bdrdlfl1nr408g2ds9q5pc")))

(define-public crate-ovba-0.4 (crate (name "ovba") (vers "0.4.0") (deps (list (crate-dep (name "cfb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "codepage") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)))) (hash "1njw2qhlqghh819hf0az48lyvbkgyss91hbn6h6bbjk9g57q36id")))

(define-public crate-ovba-0.4 (crate (name "ovba") (vers "0.4.1") (deps (list (crate-dep (name "cfb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "codepage") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.3") (default-features #t) (kind 0)))) (hash "11d7m08564r03hn93bpmiqcl2jlzicwa0qj9yiqv8pbqqaplhyr5")))

