(define-module (crates-io ov pn) #:use-module (crates-io))

(define-public crate-ovpnfile-0.1 (crate (name "ovpnfile") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6") (default-features #t) (kind 2)))) (hash "022pnrmkg7f0nm7g5jq4c12jc7sjmjyrfk0764iqdnr15g8zvpb4")))

(define-public crate-ovpnfile-0.1 (crate (name "ovpnfile") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6") (default-features #t) (kind 2)))) (hash "027al82cilsid24ds0bq1hnmlvijsyilacv4gqccdg5kpwrl6br7")))

(define-public crate-ovpnfile-0.1 (crate (name "ovpnfile") (vers "0.1.2") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6") (default-features #t) (kind 2)))) (hash "1wn5pscs4lc1raf37lnl12g5s717jzmqslpgdmgfrpy74d3p5wvw")))

