(define-module (crates-io ov m-) #:use-module (crates-io))

(define-public crate-ovm-core-0.1 (crate (name "ovm-core") (vers "0.1.0") (hash "0xcc7n5nnazq2c2f6xzscyj12lhr9p467lciccw37v14f9r845sj")))

(define-public crate-ovm-risc0-0.1 (crate (name "ovm-risc0") (vers "0.1.0") (hash "05ifdf5q857a57k4ny7dixq93cyk8j1vjsg42x8y29kb9w0ram8z")))

(define-public crate-ovm-sp1-0.1 (crate (name "ovm-sp1") (vers "0.1.0") (hash "1ddmakg4yrjyg5r3v5k3rraa1mnhfdgj3qcn19j71a0w3sjj3a8y")))

