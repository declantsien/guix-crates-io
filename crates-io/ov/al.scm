(define-module (crates-io ov al) #:use-module (crates-io))

(define-public crate-oval-1 (crate (name "oval") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req ">=0.0.1, <0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1f51q5ycc96s9h13nnk0r95djjc26x82r51n410q2lbmljc5wgas")))

(define-public crate-oval-1 (crate (name "oval") (vers "1.0.1") (deps (list (crate-dep (name "bytes") (req ">=0.0.1, <0.6") (optional #t) (default-features #t) (kind 0)))) (hash "02xqzzahx2jh5iy7cyw5jxwz7z10rjy6vjhi6vl7zb8r2wlqavw9")))

(define-public crate-oval-2 (crate (name "oval") (vers "2.0.0") (deps (list (crate-dep (name "bytes") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0a78bm0zw4yzhd2ryp7qysxs5jwvnsq9024i1m2lcrqcf8rfyp0k")))

