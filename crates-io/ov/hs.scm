(define-module (crates-io ov hs) #:use-module (crates-io))

(define-public crate-ovhsms-0.1 (crate (name "ovhsms") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l22wpr4yrwhx1bbrdpb3c7zbk75iniqrhwi16wwibmxhbvrgyva") (features (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/default-tls") ("default" "rustls-tls"))))))

