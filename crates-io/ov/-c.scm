(define-module (crates-io ov -c) #:use-module (crates-io))

(define-public crate-ov-config-0.1 (crate (name "ov-config") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qyq9p8ax3kgdyd0p3hvgaxpj7s3pxm29fz3d3vinkbc073qjicm")))

(define-public crate-ov-config-0.1 (crate (name "ov-config") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "07a7mf0fmrzby8vr33win6p8szwly770fjg6acgwmr2vmnhx2b6p")))

