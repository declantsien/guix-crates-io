(define-module (crates-io ov en) #:use-module (crates-io))

(define-public crate-oven-0.1 (crate (name "oven") (vers "0.1.0") (deps (list (crate-dep (name "cookie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1qqzav2j51vzl2by33rfdvhhrpz0f0a7g19q67p23f63bfgyrynr")))

(define-public crate-oven-0.1 (crate (name "oven") (vers "0.1.1") (deps (list (crate-dep (name "cookie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0yn89jgxb14bfp5gw78jzamm3pn74nrj4hkcwsh4dshsq17ddfd0")))

(define-public crate-oven-0.1 (crate (name "oven") (vers "0.1.2") (deps (list (crate-dep (name "cookie") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1pkg4qxgqv8bxhkgny9wm7031ya3g5z75is3ah65mc916jxnf228")))

(define-public crate-oven-0.1 (crate (name "oven") (vers "0.1.3") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0q56227llq0hx7j6ydf2ykgw8vknlv1x3ylad1s7mj5dnvw0pi38")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.0") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "05gw3m74z62bcdjnbabl0q8nigmjhk8xcf0a0ywdgbwlzgv07wdq")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.4") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "186fwfh55ginzz6ksm883z7xky694g9sgz5naxn4gqfh4pd1dprk")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.5") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1whx1r8s5mc59adyn9k164wvdqzl5gj8x291vhvc3i5472hjzr9r")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.6") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0znvc9aiwjgnch5fgfkmbgpak34sjyprkmwls58ywidmm7r9lsdb")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.7") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1fyvlshxysgzhxccw79lwhj9y8nlkrjygj8yawzrh6c9v3dfjfy2")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.8") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0v6k94ycjzr253qv0wplijf76fls8jvs26l8wxannnpc9fv8y0az")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.9") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1wdp9jf76872pdkcsv9v4qg6x0w82ds684nb2ccxy64aa7gjvw66")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.10") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0xjmjas3vymxk7j845k0vbpk55zg5jbrhwy43j5g3hdn5rjw9saa")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.12") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1fhc6z932ssccwgrxxrd9f2x92q1igz65k1dh0iw33h2x1z6mdq2")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.13") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "19yxixzjwkz1xvylq7v4l2i7bl8vvc64vp66b7dq17x2311r7vx7")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.14") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0g6niimx3r9p9j0gqcknc9g9krjnka0skxz8yvg3dip9814gfwir")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.15") (deps (list (crate-dep (name "cookie") (req "^0.1.21") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1qfb67ynxsaxlgmn2pwazqg6lc19g06384djh00q4fg61iniv2d0")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.16") (deps (list (crate-dep (name "cookie") (req "^0.2.2") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1a46niankfswqm0bb6981350k8l6yr0pckdsjvxa0dn59v40f27g")))

(define-public crate-oven-0.2 (crate (name "oven") (vers "0.2.17") (deps (list (crate-dep (name "cookie") (req "^0.2.2") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0k7k2c7sy3kr0xzml9gfmhkav7bwl9c7av558hdfklvn1f6hk4lm")))

(define-public crate-oven-0.3 (crate (name "oven") (vers "0.3.0") (deps (list (crate-dep (name "cookie") (req "^0.2.2") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "06d7jih6whv7xydiapipc9ywhpabwd5kkhi6qj9s6wymq7kjgwb1")))

(define-public crate-oven-0.4 (crate (name "oven") (vers "0.4.0") (deps (list (crate-dep (name "cookie") (req "^0.2.2") (features (quote ("secure"))) (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "plugin") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0yxzdazg1na40jdmd9y6r5y0hd75lvz29wjmmz6ik044xy8rxsi5")))

