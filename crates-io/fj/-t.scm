(define-module (crates-io fj -t) #:use-module (crates-io))

(define-public crate-fj-text-0.1 (crate (name "fj-text") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "bezier-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "fj") (req "^0.48.0") (default-features #t) (kind 0)) (crate-dep (name "font") (req "^0.29.1") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "12w4l9bds13z3lahdy582v5gq3rvviy4wnhqd7fzp9a7mv99mwrb")))

