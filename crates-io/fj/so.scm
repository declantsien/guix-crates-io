(define-module (crates-io fj so) #:use-module (crates-io))

(define-public crate-fjson-0.1 (crate (name "fjson") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 2)))) (hash "1zbj2hgm2gb5w35dx6w3jvmyva3ks5mc3770qyw3bgp9cs892l96")))

(define-public crate-fjson-0.2 (crate (name "fjson") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (features (quote ("preserve_order"))) (default-features #t) (kind 2)))) (hash "06mvhwmgdgfdvmidxjlk5zaim0ig901vnyb433l8dpaan809qx8b")))

(define-public crate-fjson-0.2 (crate (name "fjson") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (features (quote ("preserve_order"))) (default-features #t) (kind 2)))) (hash "0i0xmv1q14n29f7zhq728n8g8ln6bj11k1lc4h6h3hpi1wmmy5k4")))

(define-public crate-fjson-0.3 (crate (name "fjson") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (features (quote ("preserve_order"))) (default-features #t) (kind 2)))) (hash "0r5cl307pg2c57l6hnp4mrkh2xqpx3fb1az0475byn900x06acv3")))

(define-public crate-fjson-0.3 (crate (name "fjson") (vers "0.3.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (features (quote ("preserve_order"))) (default-features #t) (kind 2)))) (hash "0pfb8k6vd8aqb0yrwaz5cznwgg392grdyh0al73j0g3snb19sx5r")))

