(define-module (crates-io in -p) #:use-module (crates-io))

(define-public crate-in-place-0.1 (crate (name "in-place") (vers "0.1.0") (deps (list (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "tmp_env") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0ipnbz6sv6874cmgs3m66g28r44whd1xa890cdarhpb0bjg80g09") (rust-version "1.65")))

(define-public crate-in-place-0.2 (crate (name "in-place") (vers "0.2.0") (deps (list (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "tmp_env") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1h584yx2j3mf49n57havjvwpnh908sgpkknf20r4jpzxmprnknqw") (rust-version "1.70")))

(define-public crate-in-place-string-map-0.1 (crate (name "in-place-string-map") (vers "0.1.0") (hash "0dgdpmzs6xd9gfaqxfhphbnf9zy632ad3ny1xci77jx0bf05zl9j")))

