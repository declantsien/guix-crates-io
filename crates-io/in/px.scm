(define-module (crates-io in px) #:use-module (crates-io))

(define-public crate-inpx-0.1 (crate (name "inpx") (vers "0.1.0") (hash "0i5zq7v4jagh33sa8kby0xryfl0kmnbq5xkn34j302hd4q61m21d") (rust-version "1.58.1")))

(define-public crate-inpx-0.1 (crate (name "inpx") (vers "0.1.1") (hash "01jx4nlfm19smsi6864kaqvsq84d7hdpdiapsnhfl69z32y1b1ha") (rust-version "1.58.1")))

(define-public crate-inpx-0.1 (crate (name "inpx") (vers "0.1.2") (hash "16licjlnrhzd3v45vf578h9gxx6flrwnrla7n6b25pfqfdpkgpy7") (rust-version "1.58.1")))

