(define-module (crates-io in wx) #:use-module (crates-io))

(define-public crate-inwx-0.0.1 (crate (name "inwx") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "xmlrpc") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "070mn7ghgzrf525dp6ba53zqgm90d2zxa4f88pdil2i03zfks06v")))

(define-public crate-inwx-0.0.2 (crate (name "inwx") (vers "0.0.2") (deps (list (crate-dep (name "reqwest") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "xmlrpc") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0zhlhjscwa38z2hqz46q9v6wsbl0k93hgkgnb8rg8cqy7ay60g1n")))

(define-public crate-inwx-0.0.3 (crate (name "inwx") (vers "0.0.3") (deps (list (crate-dep (name "igd") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 2)) (crate-dep (name "xmlrpc") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0bsavgh30svnh5ylmr8n4bra71vbkwrnmc30afbrp346gkv9385w")))

(define-public crate-inwx-0.1 (crate (name "inwx") (vers "0.1.0") (deps (list (crate-dep (name "igd") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlrpc") (req "^0.13") (default-features #t) (kind 0)))) (hash "1pr97448n9m6kv9abfvsagq035ip3c0lhq3n4v5s6a9qry5vjh8l")))

