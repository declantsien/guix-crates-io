(define-module (crates-io in id) #:use-module (crates-io))

(define-public crate-inid_rs-0.1 (crate (name "inid_rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1w7fmsm8vnvzbpvmcc94hfzmchy3cmk9bgpr4hn3bijh38bxfcc6")))

(define-public crate-inid_rs-0.1 (crate (name "inid_rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "011b3jnm4g07zcv8pnivv93h93m4xapzi0mk3y3k2kmig4jglld8")))

(define-public crate-inid_rs-0.1 (crate (name "inid_rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1v3ifnzvxfspllwg45vbsgzamj4n6svz9j8k4ik53wcp69hmqz91")))

