(define-module (crates-io in fr) #:use-module (crates-io))

(define-public crate-infrared-0.1 (crate (name "infrared") (vers "0.1.0") (hash "1s4hdi2lw78lb0a5r6l5ql759m5763w4s4b1ry9givxzwrdbaw3g")))

(define-public crate-infrared-0.2 (crate (name "infrared") (vers "0.2.0") (hash "14qb7c549n55z4wh4d04l3b5f0gmipfpq8z9vb54d6rfic283fkk")))

(define-public crate-infrared-0.3 (crate (name "infrared") (vers "0.3.0") (hash "1gvk4dcsjfaf6v62j5qg8bbyk793hj5db15lg9j2pds2999wnw9d")))

(define-public crate-infrared-0.3 (crate (name "infrared") (vers "0.3.1") (hash "0plyasgvfx3b7rrffjvbywk82dxpcs583qqfqg0ybznhbbsz4kn8")))

(define-public crate-infrared-0.3 (crate (name "infrared") (vers "0.3.2") (hash "0sihhy5wg50pasy57jj6al57rgl87wpv5h5v2czd5wg6na0siyp9")))

(define-public crate-infrared-0.4 (crate (name "infrared") (vers "0.4.0") (hash "17p16y4690gfb4hb67vgk186qpgsp12ymwrzk0fxyd9axrq0lal7") (features (quote (("rc6") ("rc5") ("protocol-dev") ("nec") ("default" "nec" "rc5" "rc6"))))))

(define-public crate-infrared-0.4 (crate (name "infrared") (vers "0.4.1") (hash "02ndb96bwgnvjp5db7hhd14kf3z2wxgkmbi1vbaapg5m7s1d4ir8") (features (quote (("rc6") ("rc5") ("protocol-dev") ("nec") ("default" "nec" "rc5" "rc6"))))))

(define-public crate-infrared-0.5 (crate (name "infrared") (vers "0.5.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)))) (hash "04la1gvkcv4kg104fwddjy5s4rz889yspb43nhi0hc6a1zbwfnls") (features (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("protocol-dev") ("nec") ("default" "nec" "rc5" "rc6" "sbp"))))))

(define-public crate-infrared-0.6 (crate (name "infrared") (vers "0.6.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)))) (hash "11pkx0ywdn9yahv5a1d36fs1wp2zz4pcjy19m9759xyr73b97bq0") (features (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("protocol-debug") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.7 (crate (name "infrared") (vers "0.7.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rw4715735hg6hzr4ycnxlajn3m2s2gbckhsc8jjjxiydfa2v3r5") (features (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.8 (crate (name "infrared") (vers "0.8.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1sahqv0vix3k5p2b62ih6sm4b5jic27v3waw60v5bdv1lky5sqw7") (features (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.9 (crate (name "infrared") (vers "0.9.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1a60sin8804422izrxpk7ajrb6zhzl5wxccn64xfff51by4k0plc") (features (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.10 (crate (name "infrared") (vers "0.10.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0pj07m4djg8mnk26vqbk5pny7wlias2f10w4w4453d3gr21la9wy") (features (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.11 (crate (name "infrared") (vers "0.11.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1a7dgnpylvhabj2x2fdikgskv4i997a6ag1pp2gzr3i1iracca33") (features (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.12 (crate (name "infrared") (vers "0.12.0") (deps (list (crate-dep (name "dummy-pin") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcd") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "164m0azxx3m33w0r55y9qqxq9qvfxb7ivcn71bik3p6jp3njy3pq") (features (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded-hal"))))))

(define-public crate-infrared-0.13 (crate (name "infrared") (vers "0.13.0") (deps (list (crate-dep (name "defmt") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dummy-pin") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (optional #t) (default-features #t) (kind 0)))) (hash "0p8a5nsgyfapkb39cdfkbrpwh0ih98226lc1ciac3yz2pa1jmr2x") (features (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("defmt-warn") ("defmt-trace") ("defmt-info") ("defmt-error") ("defmt-default") ("defmt-debug") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded-hal"))))))

(define-public crate-infrared-0.14 (crate (name "infrared") (vers "0.14.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dummy-pin") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0zbdsalxh3z226f17mmawi0c4wvv7dgl5xfdhh14vb858kjs4lzq") (features (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("embedded" "embedded-hal" "fugit") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded"))))))

(define-public crate-infrared-0.14 (crate (name "infrared") (vers "0.14.1") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dummy-pin") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0lql95dvpll1qi7yy30rsaqrzs60sp716h39jc5ngmpv87i44nvd") (features (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("embedded" "embedded-hal" "fugit") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded"))))))

(define-public crate-infrared-0.14 (crate (name "infrared") (vers "0.14.2") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dummy-pin") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1vdvji7m0d3bfb5rgvbkys0b85nkrgs06r350zgm4vsbam2i138p") (features (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("embedded" "embedded-hal" "fugit") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded"))))))

(define-public crate-infrared-rs-0.1 (crate (name "infrared-rs") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "1g04bil4wy4hl4av8f69y7wvh0q1qlr6j6akvxiqwaq9ly5dp7lb")))

(define-public crate-infrared-rs-0.2 (crate (name "infrared-rs") (vers "0.2.0") (deps (list (crate-dep (name "rppal") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "1gj6slvdq5wrzkwqqs6p72qlz1gkgdij4mrlph6j416phashdx4j")))

(define-public crate-infrared-rs-0.3 (crate (name "infrared-rs") (vers "0.3.0") (deps (list (crate-dep (name "gpio-cdev") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1gnbgjkb73l1x69ma85ax83j0ny862krqr17p82qn6viljnyx4dz")))

(define-public crate-infrared-rs-0.3 (crate (name "infrared-rs") (vers "0.3.1") (deps (list (crate-dep (name "gpio-cdev") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "17klmjgs0r962jxhwiqx76k5af77xlal777bclky8630wbbsbr4k")))

