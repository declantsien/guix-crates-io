(define-module (crates-io in tc) #:use-module (crates-io))

(define-public crate-intc-0.1 (crate (name "intc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "06nrldbbmjfvaflwllvg4m7yd7gzn84jm47nqvfja1pgj0m7j2sd")))

(define-public crate-intc-0.1 (crate (name "intc") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0k394xvhxqyq05qrmy6nh4l5pgp52b5j8vccbafkwcnxzs71rqa0")))

(define-public crate-intc-0.1 (crate (name "intc") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "073q5l81hq5wf4b5h186j7rsf9spg3ddd9vrjlk7zmaiva76w03r")))

(define-public crate-intc-0.1 (crate (name "intc") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0wg0haph364n56axkdia25rd39fccc4xg83g72wpkhcxy14cli5z")))

(define-public crate-intc-0.2 (crate (name "intc") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "039m24zmv8prlxhs44kbh2ckiampjh8fp2lkrb4mp86hi2z7mhaf")))

(define-public crate-intc-0.2 (crate (name "intc") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "08bzkbswikk49k3hlzhmwkwygdhrrg96y3ympi2p61p69prsm5fa")))

(define-public crate-intc-0.3 (crate (name "intc") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "15hwy7934b17v02qvkzz8g3hmvpvfnd9cvjmmbgrvhw3gpl3lsf7")))

(define-public crate-intc-0.3 (crate (name "intc") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1c09ym52nhaqgmrkdpb5hhwyrxv2kdla32b9qam26pxpn88aaai3")))

(define-public crate-intc-0.3 (crate (name "intc") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "109xdrii9j0m10dwijzyw5ii4321pi73pksqrr8wkaikv52lhmlm")))

(define-public crate-intc-0.3 (crate (name "intc") (vers "0.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0xxijx8pkflxdbnbakzx6n4lk64h9cmxxa2idjma2wlh22qvjdrx")))

(define-public crate-intcode-0.1 (crate (name "intcode") (vers "0.1.0") (hash "1wk7895b354267rgfcj08cfpd4fxr322j2hjngszpc6k6ynz8m9g")))

(define-public crate-intcode-0.1 (crate (name "intcode") (vers "0.1.1") (hash "08ssrgvzm186ajrikm78j86ar2nks8rdx49j0acfs6mkwz7nw1f6")))

(define-public crate-intcode-0.1 (crate (name "intcode") (vers "0.1.2") (hash "0z00vb4wlziiijbb51chakywnd0n9mpcr2mg3ywkgc8q0bj5mpp7")))

(define-public crate-intcode-0.2 (crate (name "intcode") (vers "0.2.0") (hash "0hlxvil25v8hvnyx1zwbgwkwkd8g5f1gg223ggiw29bc83qdpz48")))

(define-public crate-intcode-0.2 (crate (name "intcode") (vers "0.2.1") (hash "0mwhf9f906l6rd5p363mq8xhs0yiryslaj0npj8lf3cfxb6aydhd")))

(define-public crate-intcode-0.3 (crate (name "intcode") (vers "0.3.1") (hash "0p25m1769577m3d31sv22ac3w17l6v482m3vpmb2b5whr6lqyz4a")))

(define-public crate-intcode-0.3 (crate (name "intcode") (vers "0.3.2") (hash "1dp29nybjwh334qmipajpav7kvnckb8dzsznhr1sdh1049fa13mj")))

(define-public crate-intcode_compiler-0.1 (crate (name "intcode_compiler") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0b38kfwjha9ycz2xr3glgvn53lal5m8blxlz7ih8sszwg0sipc1x")))

(define-public crate-intcodeint-0.1 (crate (name "intcodeint") (vers "0.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0848w96x0lg9rffw652c5mrv3z8wmac22iligmxp7f41dz5m3bk7")))

(define-public crate-intcodeint-0.1 (crate (name "intcodeint") (vers "0.1.1") (hash "0854f9hks6firsqpzs0r6d78ddrn2jif4bzi87jbkhgl4zvnlil5")))

(define-public crate-intcodeint-0.2 (crate (name "intcodeint") (vers "0.2.0") (hash "1wzgz3jnn9swa05x4qyvpw5r4gw0abjxj4vgmhxxs3dgmnfd8f63")))

