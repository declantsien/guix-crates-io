(define-module (crates-io in -d) #:use-module (crates-io))

(define-public crate-in-dir-exec-0.1 (crate (name "in-dir-exec") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "exec") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "03ma9q1ay161mv5kj62am4x9w53l7lb5h516k9dvp0pw8s5brzxl")))

(define-public crate-in-dir-exec-0.1 (crate (name "in-dir-exec") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "exec") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "1h8k9j6839wb8b4ikm56927ijn8y5ipxpq6hdfbglrq379fyqzki")))

(define-public crate-in-directory-1 (crate (name "in-directory") (vers "1.0.0") (deps (list (crate-dep (name "joinery") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.78") (default-features #t) (kind 0)) (crate-dep (name "shell-escape") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1yl0wqc21w45xklaphl5jadnivf6pipl5142bgwwgvf3bhbbqi5y")))

(define-public crate-in-directory-1 (crate (name "in-directory") (vers "1.0.1") (deps (list (crate-dep (name "joinery") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.78") (default-features #t) (kind 0)) (crate-dep (name "shell-escape") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1h77xq956105xzdrrsqbgf9sprk892bnirdpkbn9466nlayqfxjv")))

