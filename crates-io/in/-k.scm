(define-module (crates-io in -k) #:use-module (crates-io))

(define-public crate-in-keys-0.1 (crate (name "in-keys") (vers "0.1.0") (deps (list (crate-dep (name "derived-deref") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0scq5zazzq233c8n99166336q3sbw2786ldna30midrpizygsk64")))

(define-public crate-in-keys-0.1 (crate (name "in-keys") (vers "0.1.1") (deps (list (crate-dep (name "derived-deref") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fhvlc3jy6hhmkf4gs43zqy8lgayrxpl2rxv6axds4hqfxfgmdrp")))

(define-public crate-in-keys-0.2 (crate (name "in-keys") (vers "0.2.0") (deps (list (crate-dep (name "derived-deref") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "100c35nkqx4d89l73arknd7lxvfg4x93pg6xabi5vr5swpv21am2")))

