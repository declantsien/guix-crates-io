(define-module (crates-io in -s) #:use-module (crates-io))

(define-public crate-in-situ-0.1 (crate (name "in-situ") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)))) (hash "1p2mk5249jp4cr6wqmbr0czhdbzv9f78vh6329qdskz0jxv1938p") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-in-situ-0.2 (crate (name "in-situ") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)))) (hash "0z72zy4z2d29vym8zcwwbn9ssjblj1magsdxbsqinf2v7897wmyn") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-in-situ-0.3 (crate (name "in-situ") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bsikmd08m3w9ibhm06h6l9frms47bsnzphyidzbvg78bjv1vsm3")))

(define-public crate-in-situ-0.4 (crate (name "in-situ") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ixb26y0mh3plyg9kak4cjkkwbvbj0hj2prywyqz2gpy6axd6xj1")))

