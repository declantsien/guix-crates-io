(define-module (crates-io in c-) #:use-module (crates-io))

(define-public crate-inc-sha1-0.1 (crate (name "inc-sha1") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "sha") (req "^1.0") (default-features #t) (kind 0)))) (hash "132274cwc672gvaj0m667j5vg48ilnhgk7vhr6mrsafkminrmp9x")))

