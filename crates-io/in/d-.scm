(define-module (crates-io in d-) #:use-module (crates-io))

(define-public crate-ind-file-0.1 (crate (name "ind-file") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)))) (hash "09fkrwr5wcmic3fgyll7zr2fqsk39hm1crjcbm3xwk8f4laas89j") (yanked #t)))

