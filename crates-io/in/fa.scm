(define-module (crates-io in fa) #:use-module (crates-io))

(define-public crate-infallible-sonyflake-0.1 (crate (name "infallible-sonyflake") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rwgydhwm8w0qz0m9qkmvx4p725h1c97y990ggv0jxgq7fs1h8ba")))

(define-public crate-infallible-sonyflake-0.1 (crate (name "infallible-sonyflake") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0p3g7xlr8hfnxcyg30zrxl8jnqg16q1gz590xg4w38qyn8ylpzcy")))

