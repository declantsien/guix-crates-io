(define-module (crates-io in sy) #:use-module (crates-io))

(define-public crate-insync-0.1 (crate (name "insync") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "196happp2fifg8agnd9psynx85qqp0gjl9vwcawwcv3z91kwsb58")))

