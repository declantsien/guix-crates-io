(define-module (crates-io in t_) #:use-module (crates-io))

(define-public crate-int_cmp-0.1 (crate (name "int_cmp") (vers "0.1.0") (hash "07ir01jwnfbpfrp3mgdx1c358acm8rgf28j1l9v9lkbm16kr132a")))

(define-public crate-int_cmp-0.2 (crate (name "int_cmp") (vers "0.2.0") (hash "16p3vznp5vkjdpl5ma51h2a7m33kcv6i1drrzv4m69hm5g6y5127")))

(define-public crate-int_cmp-0.2 (crate (name "int_cmp") (vers "0.2.1") (hash "059xkjbrdf6ppq6j1brias04c4f646plg7sblxf5sijxl7sg512i")))

(define-public crate-int_hash-0.1 (crate (name "int_hash") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)))) (hash "0dgkrdjxgqvcrfjycj4csay82ci980y4r846zrzwgg9gwkzg81j3") (yanked #t)))

(define-public crate-int_hash-0.1 (crate (name "int_hash") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)))) (hash "0qg9mz2dwbff7a1q7lzbjvv9bhysfbbba4m33i62dkad4yqhzcc2") (yanked #t)))

(define-public crate-int_hash-0.2 (crate (name "int_hash") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)))) (hash "0jwf543rpqr1jrg1q47i2qrmnikfm60c8wpxwfm55hydc0p721lp") (yanked #t)))

(define-public crate-int_hash-0.2 (crate (name "int_hash") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)))) (hash "1828fgxynndp54ily84lpnc4s4kagfy29x6vm1qyp6rpv0ia6wm1") (yanked #t)))

(define-public crate-int_range_set-0.1 (crate (name "int_range_set") (vers "0.1.0") (deps (list (crate-dep (name "tinyvec") (req "^1.1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "1943fkafb1d085y07fvnsvdybnn01i70rvzxcsd1i80yp8p4c35y")))

(define-public crate-int_ranges-0.1 (crate (name "int_ranges") (vers "0.1.0") (hash "0dzlagvw8ya7rr7q4r04jnxa79k0fbaf6iyapa1ghp51clxrkgp1")))

(define-public crate-int_ranges-0.1 (crate (name "int_ranges") (vers "0.1.1") (hash "151s4w7hqn5shln7znyhy2v5lbcp0iqgkz922yhrgm06nkg9x8mk")))

(define-public crate-int_to_str-0.0.0 (crate (name "int_to_str") (vers "0.0.0") (hash "1qk29p2q4c5mi9z6qap8v079zx6ss47hm6jjwfpg7r5rqnzhvqm3") (yanked #t)))

(define-public crate-int_traits-0.1 (crate (name "int_traits") (vers "0.1.0") (hash "1s1ah3fnyln3dn1rnxl24j1gb4ldc14ns8m9ni1zhkqsr2djr46k")))

(define-public crate-int_traits-0.1 (crate (name "int_traits") (vers "0.1.1") (hash "0ki09xrs25w7d1fch81wfv8ibxsfdcdyn9fwqi8x0rwxb5f9lg5k")))

