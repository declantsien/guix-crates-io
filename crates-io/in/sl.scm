(define-module (crates-io in sl) #:use-module (crates-io))

(define-public crate-inslice-1 (crate (name "inslice") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)))) (hash "0wm4ry7p73d147na3yxxdsa87fzmx0vvglda5pj68z85259mvvxl")))

(define-public crate-inslice-1 (crate (name "inslice") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.5") (default-features #t) (kind 0)))) (hash "0b07w7smlaqyxpvb1qj7zrz7wmw2bbdw17mlypkyw9v4wkmyh92v") (yanked #t)))

(define-public crate-inslice-1 (crate (name "inslice") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.5") (default-features #t) (kind 0)))) (hash "00cwfnv1lcpwc0nsjiv0r21fqqb4nvafrclawv96py9p8djsf29i")))

