(define-module (crates-io in ks) #:use-module (crates-io))

(define-public crate-inksac-0.1 (crate (name "inksac") (vers "0.1.0") (hash "1y963w6f1p9cyzafiv0rhqi3ld1cdrbjsrfvhl2iw6ic52p57jn8") (yanked #t)))

(define-public crate-inksac-0.1 (crate (name "inksac") (vers "0.1.1") (hash "057wx6z7kvi9dcn74388x73vx1hwzpxgdy17vh01153w6r3xfvid")))

(define-public crate-inksac-0.1 (crate (name "inksac") (vers "0.1.2") (hash "0fjhd2h92rbka8nlmy1xn3hb26y01q14jrazrpvbyiy07hmlg0ch")))

(define-public crate-inksac-0.1 (crate (name "inksac") (vers "0.1.3") (hash "0kpxdfl79fngcwi3wqgnnf25f9jahdcqdyk5yq0k6x5jdn6xfw4i")))

(define-public crate-inksac-0.1 (crate (name "inksac") (vers "0.1.4") (hash "0p6gb6qsygcavfcads7qfz8lapv93p24v8gkj7kcjd1wim272khr")))

(define-public crate-inksac-0.2 (crate (name "inksac") (vers "0.2.0") (hash "146kn1pp7zp1wghf5igv48bjd0dx6prwbwapi3c5bpyl726g3k5g")))

(define-public crate-inksac-0.3 (crate (name "inksac") (vers "0.3.0") (hash "1hk5dkfg0mwajj6cssjm8wn01bfsay75phq2r7dg7pp1i0xkri2x")))

(define-public crate-inksac-0.3 (crate (name "inksac") (vers "0.3.1") (hash "1kg8w18xqwh9gazyyx9mccy857b0419sxhyrhqpjasngbsl4aqkb") (yanked #t)))

(define-public crate-inksac-0.4 (crate (name "inksac") (vers "0.4.0") (hash "12hjisbrlry9r95mmxfz1aq9panbpzipxn5nxnw5w2ldpv9sg4xj")))

(define-public crate-inkscape-figures-manager-0.1 (crate (name "inkscape-figures-manager") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cocoa") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "global-hotkey") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "tao") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "tfc") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0806w4fxy33615c12zmgzwk7a39d014m1j070kjabnv9p2sk1wsa")))

(define-public crate-inkscope-fuzzer-0.1 (crate (name "inkscope-fuzzer") (vers "0.1.0") (hash "123663ys4m8gbnbiccm8g64i80kpgpqrq000nagkfxkwb1d7ai9y")))

(define-public crate-inksec-0.0.0 (crate (name "inksec") (vers "0.0.0") (hash "08j1jy65cg45h4mc0ff5pa5zn63h36ghfm00fzj6iwjp3mwgn41i")))

(define-public crate-inkstory-0.1 (crate (name "inkstory") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (features (quote ("paw"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0i4plpdngxzyqal6kh34251c5c9ngw94wbn9rgfv6ckp8kqqw77d")))

(define-public crate-inkstory-0.1 (crate (name "inkstory") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (features (quote ("paw"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1azv4253fk87jnjv0sisxk87y6l99a0icjn0wr7j4gi6avva2k88")))

(define-public crate-inkstory-0.2 (crate (name "inkstory") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (features (quote ("paw"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0g11qsq0c5ji4bhb94ly2l5bjy386d1z36lghgl12z6qq7vm6c9x")))

