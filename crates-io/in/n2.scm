(define-module (crates-io in n2) #:use-module (crates-io))

(define-public crate-inn2-0.1 (crate (name "inn2") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0p4smm4pwzs31iqralnwpibyg3318di2i4y0rdlm0wsjbg1ybbws") (yanked #t)))

(define-public crate-inn2-0.0.1 (crate (name "inn2") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0rl540q996nvx10mwxbvlnyskpbaka9fn04jxwhfyrlfid2dvvav") (yanked #t)))

(define-public crate-inn2-0.0.2 (crate (name "inn2") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0bya0q9mgsd9lrifnr24d4cqasna738grzz996qbpparaj4s7b6g") (yanked #t)))

