(define-module (crates-io in cd) #:use-module (crates-io))

(define-public crate-incdec-0.0.0 (crate (name "incdec") (vers "0.0.0") (hash "0412kv5qnxjl6khj11nr07fvn3grmga07b837aaz9spdf4hyd17w")))

(define-public crate-incdir-0.1 (crate (name "incdir") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0vq9r8zijpc9wcj8k00k7p95jv63fyfl1yqlhapjw7w4dpnnchdp")))

(define-public crate-incdir-1 (crate (name "incdir") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0w575h2j2kv93a1ckd9lizhl90w0k06lwr2w38rbpziwvsrh2i36")))

