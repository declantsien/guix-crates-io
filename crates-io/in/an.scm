(define-module (crates-io in an) #:use-module (crates-io))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.0") (hash "1f47lca2lpcdd49m026ahr0asfmmbr2r3gn6ak66rav4f9mlfn23")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.5") (hash "1wc0y6qv6pwnhqhfan5aqiwpa7idczqb1228adr85p1ikacycj0h")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.6") (hash "1mp8s9g4ba35fn60x9b2f6qy8iv0gk5dqn7w63g99pqvdqdan1vj")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.8") (hash "1zar0wfr1byh4v1bq07jyvps1jgyx8h8p745dd5vz21wgq9y1s36")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.9") (hash "152grvdjcsm8qqimjxr6nfvngkq08az5f8kmb19jxmxfnq8g0xsx")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.10") (hash "04h3f1xjz2224wi8g5nhvadrck2hd8ifjdw3sb91hswp00z2cxm6")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.11") (hash "15bgw5rar39qkdhw8k3ynxg6ya7zrmf67ay60zvnbmcpj3fzdilq")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.12") (hash "18ki4n21fz0j2vfvww1nla47n3rhjfarirg4sk1dknz03a8f65cg")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.13") (hash "1za9dbjjinvkl5f985d0ik5bacawnm7r5kdzhjdhisfm80m7rnr9")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.14") (hash "18p1f85f5q18qk1h1wry518xbknq6fv3a0dlxpfm54k7xvq4wibw")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.15") (hash "0rmk73s52rn0hajx27ik76xg6i7dsc3ffrpbhszhz5nw6vhmsq0d")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.16") (hash "1n6xzg2nrsw26vl0d32mb1p1dxkzylidyjqcvg2zqm44rqpzqg3q")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.17") (hash "1njnzrxc935iqdy9zggbqrvy2x0rg2yl2mgwnd2pmdhj6kzdzm0z")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.18") (hash "0rncldxihz277x5k4z5a3zrfypxfzzv7rszijcn3qpr5lmprcyds")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.19") (hash "1xdf5kr13gjhjz556ix12mg3dwc3yx0688qxds3j7lwklcxk1bgl")))

(define-public crate-inane-1 (crate (name "inane") (vers "1.0.20") (hash "1f1513n25r48lh8a10ijflaccl79ji84d12y8njbf3mqb128s9kb")))

(define-public crate-inane-crypto-0.1 (crate (name "inane-crypto") (vers "0.1.0") (hash "0g0bh23v18wdwc936il4wr0454idhaa5mbdjgjg0mwm6ad22wn2c") (yanked #t)))

(define-public crate-inane-crypto-0.2 (crate (name "inane-crypto") (vers "0.2.0") (hash "1xj6c0fy3h1g6zhjvzv4r0n26jp43zwfx0gy4nlvf6fbc0v1dlir") (yanked #t)))

