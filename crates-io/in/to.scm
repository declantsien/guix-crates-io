(define-module (crates-io in to) #:use-module (crates-io))

(define-public crate-into-0.1 (crate (name "into") (vers "0.1.0") (hash "0i11qibwcp0c22aq08cv5xkhdxif0si7k6y7babya77fgdkyflpc")))

(define-public crate-into-0.0.0 (crate (name "into") (vers "0.0.0") (hash "0m8j3a12b3zmhiws87aq57b074b8ikqznvpy938rfgx70gdqk4cm")))

(define-public crate-into-a-byte-1 (crate (name "into-a-byte") (vers "1.0.0") (hash "14pj2hwynfsmyzb77laqbjyfd7d694p5qsxzx6s0ildw0hyrcmg5")))

(define-public crate-into-a-byte-1 (crate (name "into-a-byte") (vers "1.0.1") (hash "02pp7xb3dn24r2bhckqy7dgnf6grg2s239y9jx4g2i3d8pwqirv0")))

(define-public crate-into-attr-0.1 (crate (name "into-attr") (vers "0.1.0") (deps (list (crate-dep (name "dot-structures") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sb84k6bz41m4isf9x7xrg5whshmzf28k0bkvnajcpf093c0vv66")))

(define-public crate-into-attr-0.1 (crate (name "into-attr") (vers "0.1.1") (deps (list (crate-dep (name "dot-structures") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0xmkz2rq6hh2fmva5sm1c538b9nvlx9kgv6ag3k0k9s9gr9qrd0q")))

(define-public crate-into-attr-derive-0.1 (crate (name "into-attr-derive") (vers "0.1.0") (deps (list (crate-dep (name "dot-generator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "dot-structures") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "into-attr") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i9i1i7kh76ci3rz6ldmii3rzi84p6h6kbil160sf6i7hk3c723b")))

(define-public crate-into-attr-derive-0.2 (crate (name "into-attr-derive") (vers "0.2.0") (deps (list (crate-dep (name "dot-generator") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "dot-structures") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "into-attr") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0api0mxxp1kb3ibjdg0qhgx1xbvfsvkmxjwsipa3cfq00knz7yzq")))

(define-public crate-into-attr-derive-0.2 (crate (name "into-attr-derive") (vers "0.2.1") (deps (list (crate-dep (name "dot-generator") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "dot-structures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "into-attr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cfmf1fink7504iim0vaq9igylkzvj5il1nichx6lb6dwqd7rb7c")))

(define-public crate-into-bytes-0.1 (crate (name "into-bytes") (vers "0.1.0") (deps (list (crate-dep (name "impl-for") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1.0") (default-features #t) (kind 0)))) (hash "00ll3iqrs0j5sy3sfz76hk3lkmixlqy2xy417x3py3c7aw2icz78")))

(define-public crate-into-bytes-0.1 (crate (name "into-bytes") (vers "0.1.1") (deps (list (crate-dep (name "impl-for") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yrd482zi1ibyiarhayq8cp8xvb8by7yx90zrn2s0cpbjij9nmxi")))

(define-public crate-into-owned-0.1 (crate (name "into-owned") (vers "0.1.0") (hash "1wpwjkk1gj4vahsgfk79pc62hf14d7b5z60xqw3il0g9f07s8sap") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-into-owned-0.2 (crate (name "into-owned") (vers "0.2.0") (hash "01k8hbrdjrlhqmynf1czx4sb1k22rk5049in4ds7vdw0igwk2cpm") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-into-owned-0.2 (crate (name "into-owned") (vers "0.2.1") (hash "1pm217bn70m530d9x7al1k433ix16j354xgi79y75z4vbxl9nkza") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-into-owned-0.2 (crate (name "into-owned") (vers "0.2.2") (hash "0s11nf47wpbglhffb14bk7kff2xp33acchpps5qxa8slx8mmhm8w") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-into-owned-0.3 (crate (name "into-owned") (vers "0.3.0-alpha") (hash "1xcqzlw67v1dmx9q2597magyanzzjyvr0ravl99v99wqhyp1wwf5") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-into-owned-0.3 (crate (name "into-owned") (vers "0.3.0-alpha.2") (hash "14m6lf7ps1f23vpc0ghyc8nz6x3sy7ybn2pl1109rn6qczaxrdqx") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-into-owned-0.3 (crate (name "into-owned") (vers "0.3.0-alpha.3") (hash "02kpncrhbb0qwc8x25mh4adkkniky3qs56cgi972w0a70kf9k9s1") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-into-result-0.1 (crate (name "into-result") (vers "0.1.0") (hash "0psbv7870vk7icfzrpffbrl2q5wczxsbkvjk6lpnz1m1qisbabi5") (features (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.1 (crate (name "into-result") (vers "0.1.1") (hash "1ckv75ca0cwkxd0wpx205438b3cbgvgwph167k9s20bg0ry1snks") (features (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.2 (crate (name "into-result") (vers "0.2.0") (hash "148y79hq6hzg760gh6nypcr9fv718q1nkibkccsncdr216b8bh60") (features (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.3 (crate (name "into-result") (vers "0.3.0") (hash "1ykvq7dx01zj5z9d2ywps23cslpcsdmad5zih2y3j56632la3s9y") (features (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.3 (crate (name "into-result") (vers "0.3.1") (hash "1yxrddxlj8wn02ky963q43d6skcbjya7ya8lp14p64scpn8k0i3a") (features (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into_ext-0.1 (crate (name "into_ext") (vers "0.1.0") (hash "19v5q7hlxircl60d4l6axvl9hcgp3mq3ryk6j9h3223jcj43rbnv")))

(define-public crate-into_ext-0.1 (crate (name "into_ext") (vers "0.1.1") (hash "1f6bazd0hyjq1a6n2pcvli7v2256yaxyc9s9ijk4ldavic64yxwa")))

(define-public crate-into_ext-0.1 (crate (name "into_ext") (vers "0.1.2") (hash "1dnvx4j0xk8c2gqwnsxgfnd26lcngsa1r7bhyc29icii2kcqsr6y")))

(define-public crate-into_index-0.1 (crate (name "into_index") (vers "0.1.0") (hash "1c505rsbqzafmn3yk1hg6ddp6lansklwcjf5pzbdw2v3v6q0bzya") (rust-version "1.56.1")))

(define-public crate-into_inner_drop-0.1 (crate (name "into_inner_drop") (vers "0.1.0") (deps (list (crate-dep (name "dropcheck") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1fvghl0kmp1wchxbmzqr87s1rqx566flr0m6kjq3478r327bqx7f")))

(define-public crate-into_query-0.1 (crate (name "into_query") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (features (quote ("mysql"))) (default-features #t) (kind 0)) (crate-dep (name "into_query_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0hp14vbzi7iry3nwcijz86ycg5d9m95j7hkgpfj18shdfhl708l2") (features (quote (("derive" "into_query_derive") ("default"))))))

(define-public crate-into_query-0.2 (crate (name "into_query") (vers "0.2.0") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (features (quote ("mysql"))) (default-features #t) (kind 0)) (crate-dep (name "into_query_derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0gaxin6gwdlmqv8gn7afxdw181i02r408lqxcpq1n29biihaaqdw") (features (quote (("derive" "into_query_derive") ("default"))))))

(define-public crate-into_query-0.2 (crate (name "into_query") (vers "0.2.1") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "into_query_derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0pwpgpq34mqxam1b184y44gmdvigr295hf8ik9bsgmfsfpkp7ph5") (features (quote (("sqlite" "diesel/sqlite" "into_query_derive/sqlite") ("postgres" "diesel/postgres" "into_query_derive/postgres") ("mysql" "diesel/mysql" "into_query_derive/mysql") ("default" "diesel/mysql" "into_query_derive"))))))

(define-public crate-into_query-0.2 (crate (name "into_query") (vers "0.2.3") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "into_query_derive") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "0jvpjyzlzfd234fgf0cs6vbc1crnbbg8gsnn04g5giymw214fyy4") (features (quote (("sqlite" "diesel/sqlite" "into_query_derive/sqlite") ("postgres" "diesel/postgres" "into_query_derive/postgres") ("mysql" "diesel/mysql" "into_query_derive/mysql"))))))

(define-public crate-into_query-0.2 (crate (name "into_query") (vers "0.2.4") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "into_query_derive") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "0bxaijazkfynya0xdbx6abp7cr0c24y6h4k19i7s31b366h8qqkw") (features (quote (("sqlite" "diesel/sqlite" "into_query_derive/sqlite") ("postgres" "diesel/postgres" "into_query_derive/postgres") ("mysql" "diesel/mysql" "into_query_derive/mysql"))))))

(define-public crate-into_query_derive-0.1 (crate (name "into_query_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing" "proc-macro" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0369dx1cdm4vqj2qp5hf5hy1vnr8gvyghfvfcnrqqjjwy95w76qc")))

(define-public crate-into_query_derive-0.1 (crate (name "into_query_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing" "proc-macro" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1fnk9c7br3bk9jshs2k56jdxwqg0bcg95f4mdj6wrccmk9z5ggj8")))

(define-public crate-into_query_derive-0.2 (crate (name "into_query_derive") (vers "0.2.0") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing" "proc-macro" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1mgwq1q1cczfil6ldfi4m26wdy4bzcp7m3mmyqlkqrb8a0w4gbb5") (features (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("mysql" "diesel/mysql") ("default" "mysql"))))))

(define-public crate-into_query_derive-0.2 (crate (name "into_query_derive") (vers "0.2.2") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing" "proc-macro" "extra-traits"))) (default-features #t) (kind 0)))) (hash "01xwpbrz9cnasf56wvg5j6jg3nls4wyc0y5m9spiqd8phmigqbcd") (features (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("mysql" "diesel/mysql"))))))

(define-public crate-into_query_derive-0.2 (crate (name "into_query_derive") (vers "0.2.3") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing" "proc-macro" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1mmic6ycmqifcy0g3alfs8qk7qa4mndkmpq694d3kgi20ndkn7zy") (features (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("mysql" "diesel/mysql"))))))

(define-public crate-into_response_derive-0.1 (crate (name "into_response_derive") (vers "0.1.0") (deps (list (crate-dep (name "axum") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)))) (hash "1c1y63lfij81kmjnc4vzj32an4810nb3ni29jkyf0b8pg6v680yy")))

(define-public crate-into_response_derive-0.2 (crate (name "into_response_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)))) (hash "1lmbyh3zs4w69zbbcvycbgaqg222yhkpys57vnd5zxmnrncp26kc")))

(define-public crate-into_string-1 (crate (name "into_string") (vers "1.0.0") (hash "140hlqqxz6ysnxkr117zzfdgpgamdns3kf6pjqx9awggfh6gbz1s")))

(define-public crate-into_string-1 (crate (name "into_string") (vers "1.0.1") (hash "14dqpb6zknz9r1s7f2f2k9w419k3vv7giky6fmhf18mm16135f3c") (features (quote (("std") ("max") ("default" "std"))))))

(define-public crate-into_string-1 (crate (name "into_string") (vers "1.0.2") (hash "0rrp52jy4mpv93gzzzfy1qcrxd4vx2zqvv8f691lkwjs0b2mk5j5") (features (quote (("std") ("max") ("default" "std"))))))

(define-public crate-into_string-1 (crate (name "into_string") (vers "1.0.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "08ni02pd53rn5a7bi58ry3aj2q76cszpnnnavlmxbmmm1y4hj1rb") (features (quote (("std") ("max") ("default" "std"))))))

(define-public crate-into_string-1 (crate (name "into_string") (vers "1.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1g269j8prrnqsr9xdsjabfxcc917s8z0c1hm170r6f6ds8fnsmxg") (features (quote (("std") ("max") ("default" "std"))))))

(define-public crate-into_string-1 (crate (name "into_string") (vers "1.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0q7nwq8f4lnaxr9snsmn3gbjawqx1mfl4zycp2r3b8ylzm11m266") (features (quote (("std") ("max") ("default" "std"))))))

(define-public crate-into_string-1 (crate (name "into_string") (vers "1.2.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "110ciy7migb3qfxyd0sgip0bwv82r70jci65pnxnrnwgcd9p9772") (features (quote (("std") ("max") ("default" "std"))))))

(define-public crate-into_string-2 (crate (name "into_string") (vers "2.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12wi6kd5z22afj4igd1a3hlz6rlq7xgzbv2bbq8l8f0nk8p9jnv1") (features (quote (("std") ("max") ("default" "std"))))))

(define-public crate-into_variant-0.1 (crate (name "into_variant") (vers "0.1.0") (deps (list (crate-dep (name "into_variant_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0f87sfsnb721ymcimmfg79a6hkjvpynpfsk6a7y5nawfjh2bxnw2")))

(define-public crate-into_variant-0.1 (crate (name "into_variant") (vers "0.1.1") (deps (list (crate-dep (name "into_variant_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0pcvzns0ksh66d6ryilqpkc7h7l1j6kwr3mlfz07s1rl17xg1mg4")))

(define-public crate-into_variant-0.2 (crate (name "into_variant") (vers "0.2.0") (deps (list (crate-dep (name "into_variant_macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "1axivw9bgh4hkp3y3nwd0xmvfa6pgv9z9h8ilsl52j0kg5c7igxj")))

(define-public crate-into_variant-0.3 (crate (name "into_variant") (vers "0.3.0") (deps (list (crate-dep (name "into_variant_macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0fgfbxn5a0xpi8fak96f5liz4p8fq8r32lmw6rkkp68nzg8d0k3j")))

(define-public crate-into_variant_macro-0.1 (crate (name "into_variant_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00c0cjwkr51m5qnwfydrkicb7dcspaqxfsy0d2x3yinqvf6a7ing")))

(define-public crate-into_variant_macro-0.2 (crate (name "into_variant_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zs7h9wn17f3nfbw3hckr0fxm57fg1bc3ad31pfrbh5vc66zjmcz")))

(define-public crate-into_variant_macro-0.3 (crate (name "into_variant_macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cv5p43xs14mnf7m0bi3iwsw6a3bwl8vxfm55afja10ap2iv6gxs")))

(define-public crate-intoh-0.1 (crate (name "intoh") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15dnvk728rl2bn332a20hbj1n1fg21cv4h66ssyxlig95r6vp7sm")))

(define-public crate-intoif-1 (crate (name "intoif") (vers "1.0.0") (hash "0yvk5328wqcncfbaml96h8bd1ddpyxldcr5xxdzld3ms157d3ya4")))

(define-public crate-intovec-0.0.1 (crate (name "intovec") (vers "0.0.1") (hash "0zc7izwrqihbkdan7fc9ycxpykza6xsl1nqzl9ck451c23famh79")))

(define-public crate-intovec-0.0.2 (crate (name "intovec") (vers "0.0.2") (hash "0xqixr8g3zbdzdxzy0ij3hwg72q651k1n9nk5nkwrgqyvb73rbpa")))

(define-public crate-intovec-0.0.3 (crate (name "intovec") (vers "0.0.3") (hash "0fgsjmjfdndpwmx0yg7wm7njgj6scfk8ajcx7d49j52yzkx5mn3g")))

(define-public crate-intovec-0.0.4 (crate (name "intovec") (vers "0.0.4") (hash "1mw3jgg9fhpsr5c8gj31hl3pfajj307f32yhqq86yczxpc7p4dww")))

(define-public crate-intovec-0.0.5 (crate (name "intovec") (vers "0.0.5") (hash "1aqj686q3n9y5phbdjk7zppvrb078kacmp4m1ks80mpbsd62f9sv")))

(define-public crate-intovec-0.0.6 (crate (name "intovec") (vers "0.0.6") (hash "051g7014l26xkba4nkxfmxrcqr31h6f4yq779wxgz8g3hb7szw72")))

