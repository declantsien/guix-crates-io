(define-module (crates-io in #{3-}#) #:use-module (crates-io))

(define-public crate-in3-sys-0.0.1 (crate (name "in3-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vfcf9lf13y1wgwf5fgdnf1pkiv71lmsyzyvri6vpik8msxm0fm6") (links "in3")))

(define-public crate-in3-sys-0.0.2 (crate (name "in3-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vs7amlrqlqy73pgv3zfv4cb5mpcbbgwq4knizidq50b52s828l3") (links "in3")))

(define-public crate-in3-sys-0.1 (crate (name "in3-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1593xcic5nd88pi5s1w4zpjqhzfsmspf7jgh4s16w4gn5c5npsyl") (links "in3")))

(define-public crate-in3-sys-0.1 (crate (name "in3-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "049mjraq983hk8drzn40jdqn6b3smkbmlwg2pk1fwpd6jv73rky4") (links "in3")))

(define-public crate-in3-sys-0.1 (crate (name "in3-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "146rbw3mnw4bfvng4763naw4pz0xbyg6gknar3mdvcilf67a66g3") (links "in3")))

(define-public crate-in3-sys-3 (crate (name "in3-sys") (vers "3.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i013n64wrs43lrqscwmiz79z9lgyhbnb19l3v8qj4b8jqjvd5pg") (links "in3")))

(define-public crate-in3-sys-3 (crate (name "in3-sys") (vers "3.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gv9ds2hcyvf1dbd1hbhspd64zfs2ajqgi49fbyyc9cj49px4s9d") (links "in3")))

(define-public crate-in3-sys-3 (crate (name "in3-sys") (vers "3.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h9bgvwg4lpp79pj9xbd8xbnz1ls0rg454ijrp4j1xx6bbh5vknd") (links "in3")))

(define-public crate-in3-sys-3 (crate (name "in3-sys") (vers "3.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.53.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gwapis67cxxg44jmdkrh4pxzb2cnznxpwavlpp3lbzrl82lqxi3") (links "in3")))

