(define-module (crates-io in ci) #:use-module (crates-io))

(define-public crate-incinerator-0.0.1 (crate (name "incinerator") (vers "0.0.1") (deps (list (crate-dep (name "atomic_enum") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nested") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "thread_local") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1dqv0jdmrbadsl3c21s0dm8y4zgdzqnvsi378lf9ckwk5y6kf0fb")))

