(define-module (crates-io in ne) #:use-module (crates-io))

(define-public crate-inner-0.1 (crate (name "inner") (vers "0.1.0") (hash "1b0x9inv3awz94bh118yq8a5w8fslwqn0xbqz86baa0sf65ckvb8") (yanked #t)))

(define-public crate-inner-0.1 (crate (name "inner") (vers "0.1.1") (hash "1036irf6wkp3mlifar7m80d1ylfkzbf2vvbql1f1q9rg6a9qwdnr")))

(define-public crate-inner-mem-cache-0.1 (crate (name "inner-mem-cache") (vers "0.1.0") (hash "0aynxw8n95c5kg8wn5iwcmp9hvc295kpksg2vf8dirjasx13qv2h")))

(define-public crate-inner-mem-cache-0.1 (crate (name "inner-mem-cache") (vers "0.1.1") (hash "1va41p5l4zymdm0z77haxkrlccs48dmr5w3gsszm7i8wgqcag8yw")))

(define-public crate-inner-mem-cache-0.1 (crate (name "inner-mem-cache") (vers "0.1.2") (hash "1f57vr5r22ykrzk7iiicfam5qi1xgjn4h2xwdv0p0p64b0k0yd22")))

(define-public crate-inner-mem-cache-0.1 (crate (name "inner-mem-cache") (vers "0.1.3") (hash "1aqb7al5xfs65cjqb1yvj7jbpnl9ddiva8ns11xqr71pr0bi2d0j")))

(define-public crate-inner-mem-cache-0.1 (crate (name "inner-mem-cache") (vers "0.1.4") (hash "15y3d4rj80yd1an8pwq6a42ynmsqnadwr1yc2lfr6pjn72l3xsdj")))

(define-public crate-inner-mem-cache-0.1 (crate (name "inner-mem-cache") (vers "0.1.5") (hash "0v94zfb37rbh01svk8al4z6d8fxxhbic876wcnf59kzwk0l8g7hm")))

(define-public crate-inner-mem-cache-0.1 (crate (name "inner-mem-cache") (vers "0.1.6") (hash "0rcn3rwfsnghi82qj43kdmh74i2fzy7jl900cma7agvapj2iz8sr")))

(define-public crate-innerput-0.0.1 (crate (name "innerput") (vers "0.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("impl-default" "winuser" "processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0djmy462i1yilc64k1s9k7dvw9jjmbhh4xh926y29nkxyfglcx91")))

(define-public crate-innerput-0.0.2 (crate (name "innerput") (vers "0.0.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("impl-default" "winuser" "processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "16sr0xyyg3jihi1rwrvy5a2s0h2n47509xrlhxzj7c1kcbmx0jx5")))

