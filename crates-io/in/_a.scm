(define-module (crates-io in _a) #:use-module (crates-io))

(define-public crate-in_addr-0.1 (crate (name "in_addr") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "19gdm7bg32l6ysp45641vjx2yb7jdlj7f78zv40p6c3k9f1p07vw") (features (quote (("no-std") ("default"))))))

(define-public crate-in_addr-0.1 (crate (name "in_addr") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "006klff8b948zzx0hjc9vhv5v4ik0dvhfi2a4bisff7n3kxsyndr") (features (quote (("no-std") ("default"))))))

(define-public crate-in_addr-0.1 (crate (name "in_addr") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "05n7353hkln8jp1q0cilwyydr69vq80l4ndk71kjmpgfs4zcvkpl") (features (quote (("no-std") ("default"))))))

(define-public crate-in_addr-0.2 (crate (name "in_addr") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1j2qyqglb6yfa2if5a8ik255nkf01pac7hfqfy47r92pm5hxw66k") (features (quote (("std") ("default" "std"))))))

(define-public crate-in_addr-1 (crate (name "in_addr") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0m614icsc433vg3kf2f5jzv2y1rabccy3r1d8sxndiwk70dy288r") (features (quote (("std") ("default" "std"))))))

