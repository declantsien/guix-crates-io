(define-module (crates-io in -o) #:use-module (crates-io))

(define-public crate-in-out-0.1 (crate (name "in-out") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hflxvxm5m66ygqpb48iwhqbfzr7iz4yjp3qmwz5ym5mlsfya9kk") (features (quote (("default") ("ctx"))))))

