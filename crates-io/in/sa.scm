(define-module (crates-io in sa) #:use-module (crates-io))

(define-public crate-insa-0.1 (crate (name "insa") (vers "0.1.0-dev") (deps (list (crate-dep (name "fontdue") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "19pjvms907ah0g3a4pc17cy68rv6bh6j51arrx9ddx3lsyy6qjvi")))

(define-public crate-insa-1 (crate (name "insa") (vers "1.0.0") (deps (list (crate-dep (name "fontdue") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "1maqlpir7w5p4vsy94lk1mv2y28sy0j1p696sq7vf3hb1cjmkwyk") (features (quote (("default" "fontdue"))))))

