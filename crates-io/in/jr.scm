(define-module (crates-io in jr) #:use-module (crates-io))

(define-public crate-injrs-0.1 (crate (name "injrs") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("memoryapi" "minwindef" "ntdef" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default"))) (default-features #t) (kind 0)))) (hash "07z7458f9s908v723q9bfn658zi1ivyliyg4lrfwr0vjva6pzs92") (yanked #t)))

(define-public crate-injrs-0.1 (crate (name "injrs") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("memoryapi" "minwindef" "ntdef" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default"))) (default-features #t) (kind 0)))) (hash "010apqbriy9qny79dbiz6k3wfm9pzf3by73jazsqvh09px6bsdkv")))

(define-public crate-injrs-0.1 (crate (name "injrs") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default"))) (default-features #t) (kind 0)))) (hash "0lpqj0flrhk45nkblccfglklg2hxp409cff2z99b8p8giph2alpl")))

