(define-module (crates-io in vo) #:use-module (crates-io))

(define-public crate-invoca_rest_api-0.1 (crate (name "invoca_rest_api") (vers "0.1.0") (hash "1f42pcw522kfy8pa9a2fy6idijz6k7bkqcb2f7yh9gwkanj33hhl")))

(define-public crate-invoca_rest_api-0.1 (crate (name "invoca_rest_api") (vers "0.1.1") (hash "1v0c9l3f89gia5cb3vclpaa49c3zww271dl2qn0am6bjry61kx3w")))

(define-public crate-invoice2storage-0.4 (crate (name "invoice2storage") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "backoff") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("cargo" "string" "derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "clap-serde-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "imap") (req "^2.4.1") (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "maildir") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "mailparse") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "object_store") (req "^0.5.4") (features (quote ("aws" "gcp" "azure" "http"))) (default-features #t) (kind 0)) (crate-dep (name "resolve-path") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.20.8") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "rustls-native-certs") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("rustls-tls" "blocking"))) (kind 2)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "1sm1ikcc9gn38jn7xd6k3rid8zy9aawsiamk7aqawmsxsxv1852s")))

(define-public crate-invoicer-0.0.0 (crate (name "invoicer") (vers "0.0.0") (hash "14s624z2184yhf6b5np5cpnl6f4nimw1lkwidwy4q9w5pr44mqb1")))

(define-public crate-invoke-0.1 (crate (name "invoke") (vers "0.1.0") (hash "1si0ws1042ichwqqlkk2b53009glh65a49kvdlnddbwmarfq0lap")))

(define-public crate-invoke-rs-0.1 (crate (name "invoke-rs") (vers "0.1.0") (hash "14bm16dw7cc0b9prg951p4qca00rnangkazyrvxv80rhgfb1n75f")))

(define-public crate-invoke-script-0.1 (crate (name "invoke-script") (vers "0.1.0") (deps (list (crate-dep (name "bind-args") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1s9l3sjcq4pvyvm8mby7hj6bn0vw9gr8wdv8cjm4fl120y6c0dsa")))

(define-public crate-invoke-script-0.2 (crate (name "invoke-script") (vers "0.2.0") (deps (list (crate-dep (name "bind-args") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0pg6k9rza5m2dfwfqpw87ffgnys8p0whwcppdp5likcagg5pqilw")))

(define-public crate-invoke-script-0.3 (crate (name "invoke-script") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "038nm2rwar8gys25j3gci5cvxqna5csdvlqprkk69rg72hk2z831")))

(define-public crate-invoke-script-0.4 (crate (name "invoke-script") (vers "0.4.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "18p17lk5pa8q2bil2kzwdmj9ivir7fvx6m72apffbiamh9yknafx")))

(define-public crate-invoke-script-0.5 (crate (name "invoke-script") (vers "0.5.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "watercolor") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1igq5hgb53w6nkhqw9b28jsrwpn5qnqha8y7xzmd482ncwy8ppnf")))

(define-public crate-invoke-script-0.6 (crate (name "invoke-script") (vers "0.6.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "watercolor") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18qjnvbjz9l8caapvhwi231316s91wh0ls45agxjxhnr3psn4ga1")))

(define-public crate-invoke-script-0.7 (crate (name "invoke-script") (vers "0.7.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "watercolor") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bjxgb64imbsixa1nkma88095jjlflrnl335fsfnn7c5mh30y2rv")))

(define-public crate-invoke-witc-0.1 (crate (name "invoke-witc") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0jcv9kldq6akgsjdwqvkwn35xs9v2gd3fw1r3zpilkk4j4y68q74")))

(define-public crate-invoke-witc-0.2 (crate (name "invoke-witc") (vers "0.2.0") (deps (list (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "19d19fljwwc1h5rgkfshinp7zgrfwmhpjc9akkrwwmk52s3bv523")))

(define-public crate-invoke-witc-0.2 (crate (name "invoke-witc") (vers "0.2.1") (deps (list (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "08dc837a7pjcm7xgh251dw0h16sg27zp73nr5n0iqj0xnpi9m46g")))

(define-public crate-invoke-witc-0.3 (crate (name "invoke-witc") (vers "0.3.0") (deps (list (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "06gagnj5b34qahncgzfln5rhv4015ffn76wgi3vrpmdlhcpw8l1j")))

(define-public crate-invoke-witc-0.3 (crate (name "invoke-witc") (vers "0.3.1") (deps (list (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "13lqvqm1x29akg5j8472lds244c1sd80xm2dmds05s09d2fn2zw9")))

(define-public crate-invoke_impl-0.1 (crate (name "invoke_impl") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.96") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0gj48xhg6giw3p95fmjhl8ay53rdkm5gxck1s2d5z5nw7g9g1cy2")))

(define-public crate-invoke_impl-0.1 (crate (name "invoke_impl") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.96") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1fwx9h2nm8qwi1hrxh6bdbp4k0ymhvd1v0hkh2zwmy85s9fyn4vz")))

(define-public crate-invokedynamic-0.0.0 (crate (name "invokedynamic") (vers "0.0.0") (hash "1f0hw09pimdm28lyb9wwyrzd1x5ayrlbxc02gl02ml1i4xhygab4")))

(define-public crate-invoker-0.1 (crate (name "invoker") (vers "0.1.0") (hash "1n005kxxqq4n1fg1rifv58mg2xch38rp1hkg4m980nlnhm56m1vs")))

