(define-module (crates-io in tn) #:use-module (crates-io))

(define-public crate-intname-0.1 (crate (name "intname") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0jmb76kfxff8wd6m1lxk2dan1gjk06f189yymil0cqwr0rj3dhi4")))

(define-public crate-intname-0.2 (crate (name "intname") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1wskl37jrgyslx43ch4k140vxkim385aixqfjdw6yfkxr62nbn57")))

