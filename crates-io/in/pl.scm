(define-module (crates-io in pl) #:use-module (crates-io))

(define-public crate-inplace-0.0.1 (crate (name "inplace") (vers "0.0.1") (deps (list (crate-dep (name "arraystring") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 2)))) (hash "0yjfkybcp714fl4rw7jr60a7vjz6qnqbcfm49dbwk3fziqii842x") (yanked #t)))

(define-public crate-inplace-0.0.2 (crate (name "inplace") (vers "0.0.2") (deps (list (crate-dep (name "arraystring") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 2)))) (hash "0aipw5nhlkzkv2kvk4r84pl089q2fdq0rjpfd5xskv3psnmdcxhy") (yanked #t)))

(define-public crate-inplace-0.0.3 (crate (name "inplace") (vers "0.0.3") (deps (list (crate-dep (name "arraystring") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 2)))) (hash "0kmcqfwsiwpjp68kj3n3dgnvr2496b7wclkyvjgcpbqaijdrdmax") (yanked #t)))

(define-public crate-inplace-0.0.4 (crate (name "inplace") (vers "0.0.4") (deps (list (crate-dep (name "arraystring") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 2)))) (hash "0sar1py5pz89lmhwpfzipv6ms49x1b2k85wzlb95bs4s2cj2wlxk") (yanked #t)))

(define-public crate-inplace-0.0.5 (crate (name "inplace") (vers "0.0.5") (deps (list (crate-dep (name "arraystring") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 2)))) (hash "1vqzqavb0bm5qkxv2z8di3jzd59m4ijgsbi8x9r5pn5y3ipyd0sm") (yanked #t)))

(define-public crate-inplace-vec-builder-0.1 (crate (name "inplace-vec-builder") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0v26rix6aizc01a3skfaabd0x3hwfwq1m9n9gl9dvpp9yp6j72bv") (features (quote (("stdvec") ("default" "stdvec"))))))

(define-public crate-inplace-vec-builder-0.1 (crate (name "inplace-vec-builder") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "01ykl6cis06m2hw88413jhmjs4qvhsi8fx8j3ykr2s12r3nw4r6g") (features (quote (("stdvec") ("default" "stdvec"))))))

(define-public crate-inplace_it-0.1 (crate (name "inplace_it") (vers "0.1.0") (hash "095kyqg3r0a0qrf6z52fh90xgbpdqrk0z3vwz6hpnqbm56r9bci1")))

(define-public crate-inplace_it-0.2 (crate (name "inplace_it") (vers "0.2.0") (hash "1k2npdmvjwxhfpcc9xb2z23d42vp5glqfyg4vbif3l5ba9nbc9pw")))

(define-public crate-inplace_it-0.2 (crate (name "inplace_it") (vers "0.2.1") (hash "0kpg1jv6f4z9n6vlljymb09ppl5y9layra5d46cnwcpc97vfq16g")))

(define-public crate-inplace_it-0.2 (crate (name "inplace_it") (vers "0.2.2") (hash "17maz4f0nijlabfnnkgvgawjqprxmmvw0a5rf8lps7x53r8yk37n")))

(define-public crate-inplace_it-0.3 (crate (name "inplace_it") (vers "0.3.0") (hash "02qjvg8wrwfvdnxz5csrciapfbawhvxb2hg03k0h5bd5gh7zvpb7")))

(define-public crate-inplace_it-0.3 (crate (name "inplace_it") (vers "0.3.1") (hash "1pqgag9z78d30j3p0gxym6mlwqiycg4bgx0zjcwa1pgpxnx306jr")))

(define-public crate-inplace_it-0.3 (crate (name "inplace_it") (vers "0.3.2") (hash "0d5mbkff2ziwmqqpaki84ridgx3yd3m8ip12dgwrsf9g7yks40fx")))

(define-public crate-inplace_it-0.3 (crate (name "inplace_it") (vers "0.3.3") (hash "1jpppakqdazlzmlqx8vmb78drzgv93w52gk4lii6vzkri8q3z5ch")))

(define-public crate-inplace_it-0.3 (crate (name "inplace_it") (vers "0.3.4") (hash "02q7jav7xbhvcdq0nc8is9b4ajqw5ggavzby3qn3dxpk6rw39w37")))

(define-public crate-inplace_it-0.3 (crate (name "inplace_it") (vers "0.3.5") (hash "0y5znpw0f42pf2q0ksdli299p7qkh2rhjbkhjxrvrm7ka264crz5")))

