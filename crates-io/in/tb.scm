(define-module (crates-io in tb) #:use-module (crates-io))

(define-public crate-intbin-0.0.5 (crate (name "intbin") (vers "0.0.5") (hash "0pk5zxnr52yfzl11wsr8kdczi6pr077xf9wnx50yqaid2icwvl7p")))

(define-public crate-intbin-0.1 (crate (name "intbin") (vers "0.1.1") (hash "19gy8byvd87zfj196ygm1jmshwb3lshm9kwy3p4c8kmrii0k8imp")))

(define-public crate-intbin-0.1 (crate (name "intbin") (vers "0.1.2") (hash "1bw094d4f0v64nld0hs7952xnr27p4clj4hnbmkdxf3ln93gqfjk")))

(define-public crate-intbits-0.1 (crate (name "intbits") (vers "0.1.0") (hash "0cr48aal9qbi82rj6p0yg9ajwb1gki8pc241qal9ljb0yxg2s2hq") (yanked #t)))

(define-public crate-intbits-0.1 (crate (name "intbits") (vers "0.1.1") (hash "1zq8d29br68apn8li06k0hs2bzxkk8fzqp8r940hh50p5a1jlaq8") (yanked #t)))

(define-public crate-intbits-0.1 (crate (name "intbits") (vers "0.1.2") (hash "05w77f51qmzhq09in6kj7kxqgfyzc7mrr2qv5r88v2q7siq2m136")))

(define-public crate-intbits-0.2 (crate (name "intbits") (vers "0.2.0") (hash "1r9hcgagj6g7hxh9q36whjh5zivv22zcr5cxrfyc2afsxk4c4w2i")))

