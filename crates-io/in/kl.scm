(define-module (crates-io in kl) #:use-module (crates-io))

(define-public crate-inkling-0.9 (crate (name "inkling") (vers "0.9.0") (hash "04bw5x8qh27gpyls7ksiyn0gy43fbc2md3w31in4n6929hiz3mms")))

(define-public crate-inkling-0.9 (crate (name "inkling") (vers "0.9.1") (hash "1bbgrgvvikgvyrcx9j5apv5chsnz6kna3fhjxi2cw035zzhawvka")))

(define-public crate-inkling-0.9 (crate (name "inkling") (vers "0.9.2") (hash "1abqvm7lzc45cg2ggiryr6hd79dpsl2c5lyykb511ccjif2ir4qj")))

(define-public crate-inkling-0.9 (crate (name "inkling") (vers "0.9.3") (hash "065j194hpwsci05bhcs5z9b9c1d35b6526g6y9xzb1c2vvs9va6b")))

(define-public crate-inkling-0.10 (crate (name "inkling") (vers "0.10.0") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0m6gibj2ssf80v3srnfzadcbhpckqbzi878hm9sfc1fp4jqpx43h") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10 (crate (name "inkling") (vers "0.10.1") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1qkyqngw2q2y8f2qw6abnl7lpa8l2jgiv3vn1jkjz8nwx2phq311") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10 (crate (name "inkling") (vers "0.10.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1adj1fqz1a6mc1sqxkwikpz4bfkyqnr5lrq8vrxqp95xc99yz48r") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10 (crate (name "inkling") (vers "0.10.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "08chyiil7przsik5k069pzqg0cjhc392whspkhd8mgry133zsj18") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10 (crate (name "inkling") (vers "0.10.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "12q80f255hcwdwycl7v8mj8ja7v00g98snbvmb19h74fkzs4vs3x") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "11z7nrrdvnwik1vma3f3ls87vganl94rf6q0y42qv23kjfxr71vl") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mxxhfahixhr6mpql605417wdbjm8vacszb0jcxp36k3d65vbivl") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1wmwsknsqsv8wpc1m2zmyz73dwb8yglp2pi03wg0pzgjll25alf8") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0q39qzyw5mr2z5m0n82a3sclfx0blkff0w0avm4hl70342f26ddh") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1z4rqd12l6qxr1fh3kndzbd13iq926p0sl244rqdk7i71flm71x5") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "09rjrlfnscvy61a9isnv2dhinna9v0989bhl7fh2s4q78sj66ci4") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "18h5lpzb617r4vl2j1z4crczlb126z4nxkn7cdsmp9hng20zfsp8") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11 (crate (name "inkling") (vers "0.11.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1nssd8zkdc4acapk3kn6i8as0xjcliipchgv5yvkdqylb05fxza5") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12 (crate (name "inkling") (vers "0.12.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0g79h6k8cfwj2625hi789kw9ymkax2hh9610l4r2m312wkk9g75l") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12 (crate (name "inkling") (vers "0.12.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0h4v92rcgmv67ag1jdaj6vd1q9w54102x29kqyazrkypg7v88x5h") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12 (crate (name "inkling") (vers "0.12.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0g0w5plxk7s0bvg7scdc2afsi0ippbgqihvlylbbfhv4yz8svvgg") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12 (crate (name "inkling") (vers "0.12.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1wcz06jfksmnc8g6ydmr1jfk3b3wvkklxvfkmpc9hh6qgjhs66kr") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12 (crate (name "inkling") (vers "0.12.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1b2rij5n3njf12i6w2wvzril9zjg002y4vhr0axywgl5f9p99wy3") (features (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12 (crate (name "inkling") (vers "0.12.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1swba3271ijnz0g1xxk8614c5sb5amhv8j70mk8ybxhgvfzjyd6b") (features (quote (("serde_support" "serde/derive"))))))

