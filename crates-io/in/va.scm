(define-module (crates-io in va) #:use-module (crates-io))

(define-public crate-invade-0.0.1 (crate (name "invade") (vers "0.0.1") (deps (list (crate-dep (name "invade_derive") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1m1nviif2yrsvlkzv4z9hw7q45f1vqj2biws5833x90khzbaz79a")))

(define-public crate-invade-0.0.2 (crate (name "invade") (vers "0.0.2") (deps (list (crate-dep (name "invade_derive") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0k4blkxk698l6ijzlbi694cdm264gyihr19k8i0myalz8ni7jk48")))

(define-public crate-invade-0.0.3 (crate (name "invade") (vers "0.0.3") (deps (list (crate-dep (name "invade_derive") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "152klvwcxav9di3hx0k533r4gfgicy8iviip7vh50jd1flh3nph9")))

(define-public crate-invade-0.0.4 (crate (name "invade") (vers "0.0.4") (deps (list (crate-dep (name "invade_derive") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0qxk0cjp89vi3jmini54w0wmjsvky6pw669kk0pn3x5f1z487sja")))

(define-public crate-invade-0.0.5 (crate (name "invade") (vers "0.0.5") (deps (list (crate-dep (name "invade_derive") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1gx059r9vmblrg0dqxcibcfyiync0p2nb5svil92nzikxgiyq6c7")))

(define-public crate-invade-0.0.6 (crate (name "invade") (vers "0.0.6") (deps (list (crate-dep (name "invade_derive") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "030yyznd4k982si0y12lydrm4src4iw08k87fdr3m5lzbvjccz5c")))

(define-public crate-invade_derive-0.0.1 (crate (name "invade_derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "022h3vq5d2m7kicckv2l41bdcccw7a673xwm0d1iqrdfg0fmz7mk")))

(define-public crate-invade_derive-0.0.2 (crate (name "invade_derive") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0b9ccngphlc6xlhbxh05dawis83wiwwjm0mcyj4a24ml4dz7zjxb")))

(define-public crate-invade_derive-0.0.3 (crate (name "invade_derive") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07icjy5n2m0qpq38fs0g7n9xjxx5cfayzcrpdnyykrqphn0aa3vp")))

(define-public crate-invade_derive-0.0.4 (crate (name "invade_derive") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dqxh9r1jjph838p6lz5g5l7jak7id67g9hib5c0jj5n42gp25x5")))

(define-public crate-invade_derive-0.0.5 (crate (name "invade_derive") (vers "0.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15bnyv3rf1h2dhi85qq0zpmmmzk6qmayk1xmj8kh9qvh1ljbapji")))

(define-public crate-invade_derive-0.0.6 (crate (name "invade_derive") (vers "0.0.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rmmhvy1ib7gj2vp7rsdbq3r236vd2i5368xy2z8m8ysli25lksf")))

(define-public crate-invalid-mutations-0.1 (crate (name "invalid-mutations") (vers "0.1.0") (deps (list (crate-dep (name "move-binary-format") (req "^0.1.0") (default-features #t) (kind 0) (package "mv-binary-format")) (crate-dep (name "move-core-types") (req "^0.1.0") (default-features #t) (kind 0) (package "mv-core-types")) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ngq8di444nb0ycyqikkjzc4masvgnx96rpgfksph5izv4npxjg6") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.1 (crate (name "invalid-mutations") (vers "0.1.1") (deps (list (crate-dep (name "move-binary-format") (req "^0.1.0") (default-features #t) (kind 0) (package "mv-binary-format")) (crate-dep (name "move-core-types") (req "^0.1.0") (default-features #t) (kind 0) (package "mv-core-types")) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0g67z8xnir1sgi1h16l6yz6rz634i2s2i81my8d1wm0w3pjamfp1") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.1 (crate (name "invalid-mutations") (vers "0.1.2") (deps (list (crate-dep (name "move-binary-format") (req "^0.1.0") (default-features #t) (kind 0) (package "mv-binary-format")) (crate-dep (name "move-core-types") (req "^0.1.0") (default-features #t) (kind 0) (package "mv-core-types")) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1k571baq53ad94ns79x142mahcy10a5jxd9pygafcca96jxyk8g2") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.1 (crate (name "invalid-mutations") (vers "0.1.4") (deps (list (crate-dep (name "mv-binary-format") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0jw20sgmcy38rgzy8ilnh1x0r6lxzqsl889fl444h3z6qi89k2il") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.1 (crate (name "invalid-mutations") (vers "0.1.5") (deps (list (crate-dep (name "mv-binary-format") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1pdscncr42msqi40sbgyijklaalmfa5cnc3sw71y17n1v6gb7qmg") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.1 (crate (name "invalid-mutations") (vers "0.1.6") (deps (list (crate-dep (name "mv-binary-format") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0c00s250vbfh7jf23j53ccm8pdfwh4ksyd2w4nfrzlncappb9agv") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.2 (crate (name "invalid-mutations") (vers "0.2.0") (deps (list (crate-dep (name "mv-binary-format") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1xdqq9c1z6jldx6m78j26s73lvy54ifwh7lkbgr9814rdlvm1i8w") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.2 (crate (name "invalid-mutations") (vers "0.2.1") (deps (list (crate-dep (name "mv-binary-format") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1hicvr1maspb1b31ki5l9p4l028fyc47ayn60kq19p5p7w3h28yz") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.3 (crate (name "invalid-mutations") (vers "0.3.0") (deps (list (crate-dep (name "mv-binary-format") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "15c9gwc6ig2b9cwgdma7m46fhlb9snlb5y462aycklp43nklab7k") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.3 (crate (name "invalid-mutations") (vers "0.3.1") (deps (list (crate-dep (name "mv-binary-format") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0vzhfyd4s5sq1iw22aqbsxzc8a6z6ncclivi1a3qn2p94652fwzi") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalid-mutations-0.3 (crate (name "invalid-mutations") (vers "0.3.2") (deps (list (crate-dep (name "mv-binary-format") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mv-core-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0y3vgyyylbivmfbcrrbdsfypw3dfhwphjihsplsf7ircb1wx8d0b") (features (quote (("default")))) (yanked #t)))

(define-public crate-invalidstring-0.1 (crate (name "invalidstring") (vers "0.1.0") (hash "15sr7nhdvjprzz9lsd1dfx3yn9a24ayvzfbrw7x5grymvafckpji")))

(define-public crate-invalidstring-0.1 (crate (name "invalidstring") (vers "0.1.1") (hash "1kg63zkjx7vpp8fmmvpzpwwwz3f75xi8s2bwj845jbycv8arslc4")))

(define-public crate-invalidstring-0.1 (crate (name "invalidstring") (vers "0.1.2") (hash "1k7061smkxgjmix16wim2c4qb8gkdggaxilkd9yxhmza5lgpsmi2")))

(define-public crate-invalidstring-0.1 (crate (name "invalidstring") (vers "0.1.3") (hash "0vxin67ybys7ihmwr5xvvwqv3q7lcn2pxg1skw14xr8kjpa0z5nj")))

(define-public crate-invariant-0.0.1 (crate (name "invariant") (vers "0.0.1") (hash "0qr1aw3wghzm5np57k2jnagh2qhdzk5hf1irnps0yqnc1r9fhw79")))

(define-public crate-invariant-0.0.2 (crate (name "invariant") (vers "0.0.2") (hash "1mxk6s1yi9w0x6ysk62nj2a4857mp4i3sns06105lqwkdda2505y") (features (quote (("all-dependencies"))))))

(define-public crate-invariants-0.1 (crate (name "invariants") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0v5sy56zyjpn404rja6y3ccnx8d234c0g7cb0syf6bhqr97zrpy7")))

(define-public crate-invariants-0.1 (crate (name "invariants") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "02jrlh7kzv3m2kdn8l6fdvxm6473n535q4vsxnina3zysyc03vfk")))

(define-public crate-invariants-0.1 (crate (name "invariants") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0xi3mm9gpzghim156kx433wn9xsvq199nnq5kpyzbi8v96zz62v5")))

(define-public crate-invariants-0.1 (crate (name "invariants") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0kl28sqamm7lx4fh8sh1j23zm228vw9di2fv095fplrp2pg4cav4")))

