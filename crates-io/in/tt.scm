(define-module (crates-io in tt) #:use-module (crates-io))

(define-public crate-inttype-enum-0.1 (crate (name "inttype-enum") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.20") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ys3nndj8qjyikpapl03p1fnlnifb2rxdwz7ml0wawwz5zpkyr68") (yanked #t)))

(define-public crate-inttype-enum-0.1 (crate (name "inttype-enum") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.20") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05pgfszfcnnzbxljba2dfqnkyr6x99dlky09l03w9r795gyb0hqs")))

(define-public crate-inttype-enum-0.1 (crate (name "inttype-enum") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.20") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gywy2qkdvhqk5byjp386qqrwxiphp6xdl9x8sj6ahhy8d9w7bs2")))

