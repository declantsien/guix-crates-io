(define-module (crates-io in f_) #:use-module (crates-io))

(define-public crate-inf_vec-0.1 (crate (name "inf_vec") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "~1.19.0") (default-features #t) (kind 2)))) (hash "1y67mq9r0kimdki6fr0fz9jca2n89fh8z6nzxpvyzkawq67g2c8r") (yanked #t)))

