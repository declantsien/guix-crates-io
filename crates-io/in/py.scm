(define-module (crates-io in py) #:use-module (crates-io))

(define-public crate-inpyt-0.1 (crate (name "inpyt") (vers "0.1.0") (hash "0c0zij42cvgn6vkcpq4pqv0gk5jdwssnl44mwfckrvyflkz1vkwi")))

(define-public crate-inpyt-0.1 (crate (name "inpyt") (vers "0.1.1") (hash "0dpwl37f1g1p6kbbplhnvc1743fhh9gxwc0q3m40cpmb312vs63c")))

(define-public crate-inpyt-0.1 (crate (name "inpyt") (vers "0.1.2") (hash "156ls57nf505f5w4abz3786mdzl5n81gpdsc4lj63h56k5c1jfyb")))

(define-public crate-inpyt-0.1 (crate (name "inpyt") (vers "0.1.3") (hash "08kpv2n4n941hcg18baxwai9183zgp8nv2pv47cy64p7l2ixpv0v")))

