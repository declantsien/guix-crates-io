(define-module (crates-io in tm) #:use-module (crates-io))

(define-public crate-intmap-0.1 (crate (name "intmap") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0kgz75v329ncnd0ybp0m9pnjf3hc2fcrrsmycziw0j08dfjll8js")))

(define-public crate-intmap-0.1 (crate (name "intmap") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0c2lb5mi4pa9bizc8wilswc4ca48y6qypxalxyf0kd626xa5imyp")))

(define-public crate-intmap-0.2 (crate (name "intmap") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1vwgcbw7ivapja75aai90bysj1lfwc2hfg769x8y00rb9dfafqdg")))

(define-public crate-intmap-0.2 (crate (name "intmap") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1qlx7r8cpmqal2cm81mv3ysjsr50xpyzv9ykcn017vd00jvfcjmv")))

(define-public crate-intmap-0.4 (crate (name "intmap") (vers "0.4.0") (deps (list (crate-dep (name "ordermap") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0f8mf2w5p35pf50z58kakjgjxc1wvp2np42sybd5ai0xzsvsph00")))

(define-public crate-intmap-0.5 (crate (name "intmap") (vers "0.5.0") (deps (list (crate-dep (name "ordermap") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0knz5s6q9s23awbjklwkrq3q6389n3564jcgkbvx0zsl89hw11b9")))

(define-public crate-intmap-0.6 (crate (name "intmap") (vers "0.6.0") (deps (list (crate-dep (name "ordermap") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0xjxxrnsdq1hdk69q9prf9v1nj3fjbwbmskidn1prkx8v1vwkr7x")))

(define-public crate-intmap-0.7 (crate (name "intmap") (vers "0.7.0") (deps (list (crate-dep (name "ordermap") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1iiyabmiq78fnb2ifvn3as27hh6cmmadcgczp6hc9xjnb4w302g5")))

(define-public crate-intmap-0.7 (crate (name "intmap") (vers "0.7.1") (deps (list (crate-dep (name "ordermap") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ffph34qfda5zxdvy2pvjnip9hgzbjcxw53pvdpcjaxc8n7z4lmf")))

(define-public crate-intmap-0.8 (crate (name "intmap") (vers "0.8.0") (deps (list (crate-dep (name "ordermap") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1mgkmqmvhv56a703im394g5ll5fzqz0b92mk7l4bx8hva9p5rlc6")))

(define-public crate-intmap-1 (crate (name "intmap") (vers "1.0.0") (deps (list (crate-dep (name "indexmap") (req "^1.8.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0d9ngjgd5fj729j8qdfzz1hc7zzl4a52z5i58vc97zfgj6c8inja")))

(define-public crate-intmap-1 (crate (name "intmap") (vers "1.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.8.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0x180ggxrxxp3lmqs1zpl4kscaam4fangq3r7fizh00k256mcmxk")))

(define-public crate-intmap-2 (crate (name "intmap") (vers "2.0.0") (deps (list (crate-dep (name "indexmap") (req "^1.8.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1mi4x5nm365p5n93w8pvd5lzn9q2pfhgl96gp9s40d336l4zv1zf")))

