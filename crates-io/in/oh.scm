(define-module (crates-io in oh) #:use-module (crates-io))

(define-public crate-inohashmap-0.1 (crate (name "inohashmap") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "vint32") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0cdmlb4vb7xxash6zyvng8ry0k2r809s8pfqrk46d62zrbrrjj7h")))

(define-public crate-inohashmap-0.1 (crate (name "inohashmap") (vers "0.1.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "vint32") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q2kfn702l110mja63vr591xnwkhdavdbkzqxfff71j9vvr0lrj2")))

(define-public crate-inohashmap-0.2 (crate (name "inohashmap") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "vint32") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fgbhrib3axczqlkmaiyv8br00qfwv2fs2zsld0wdn6rwy26y501")))

(define-public crate-inohashmap-0.2 (crate (name "inohashmap") (vers "0.2.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "vint32") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gazvhdsxgp3gnl73l2vp3nbdd5kh79afvaf1cv05pykvws37i8h")))

(define-public crate-inohashmap-0.3 (crate (name "inohashmap") (vers "0.3.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "vint32") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "063vnjzpzg3chr16qgarbzv2nhb1lm57dvfzmdyni9jm14s78mb0")))

