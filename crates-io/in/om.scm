(define-module (crates-io in om) #:use-module (crates-io))

(define-public crate-inom-0.1 (crate (name "inom") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0q6bb2wfcbfy8al6pvwjnkwyjjlf5cxjsak6faci0wbmwvv9nmyv")))

