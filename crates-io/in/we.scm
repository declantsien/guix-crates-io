(define-module (crates-io in we) #:use-module (crates-io))

(define-public crate-inwelling-0.1 (crate (name "inwelling") (vers "0.1.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pals") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hw6rw31pc616vxskdwi44jbhh3f5s4y1fg28a562j4v6p0xbjdr")))

(define-public crate-inwelling-0.1 (crate (name "inwelling") (vers "0.1.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pals") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11y6dcs7lw681z40a7lqvwni02dklf52ld0kc69away7vvj4mrj0")))

(define-public crate-inwelling-0.1 (crate (name "inwelling") (vers "0.1.2") (deps (list (crate-dep (name "cargo_metadata") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pals") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g3a9cfvqf02zsqznc47d3fgbda93svwwji7m6xrqb2aksp2dvi0")))

(define-public crate-inwelling-0.2 (crate (name "inwelling") (vers "0.2.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pals") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s9vmmh2mg61idzgzi3imqh8kgb8i24y9qn7v8qkby5m72jasqwc")))

(define-public crate-inwelling-0.3 (crate (name "inwelling") (vers "0.3.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pals") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i1h9igr4dgy5nr6kxr9k60x77yqdfr0790vq3v2kw62mj7vb98x")))

(define-public crate-inwelling-0.4 (crate (name "inwelling") (vers "0.4.0") (deps (list (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0dkj21zf0wrbcx6b49g0cz05c1b7vn2amlgrnsdic9w3h3b5xnqz")))

(define-public crate-inwelling-0.5 (crate (name "inwelling") (vers "0.5.0") (deps (list (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "040gncrgsn1ccan16fsqrghkc26dlwkg0ns7izazpbs7bwr1bxha")))

(define-public crate-inwelling-0.5 (crate (name "inwelling") (vers "0.5.1") (deps (list (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "16jwfbiab9f2w3s7wil9klq5iblavqq9jg2qgskxx8h4dbwsqhiy")))

(define-public crate-inwelling-0.5 (crate (name "inwelling") (vers "0.5.2") (deps (list (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1fv9bmbzq6g288g34l9yxhx6m2xqpfac7phs5f4zpsd7g576f26z")))

(define-public crate-inwelling-0.5 (crate (name "inwelling") (vers "0.5.3") (deps (list (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0yffbnv6ppmvw1pnyvar3h61hgzmlrshwjnvhj1pc0zz3nf5n81i")))

(define-public crate-inwelling-0.5 (crate (name "inwelling") (vers "0.5.4") (deps (list (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1m9kpx2vqmgmddj7rlaxn7dm72n00383cdmdzgxyc6i1h7s2skrw")))

(define-public crate-inwelling-0.5 (crate (name "inwelling") (vers "0.5.5") (deps (list (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "135grwkcsivaxmyd7wbpcgcpjpc8s9dcm5cgraagm8gwiz394qig")))

