(define-module (crates-io in ip) #:use-module (crates-io))

(define-public crate-inip-0.1 (crate (name "inip") (vers "0.1.0") (hash "163scrl5zmsdc0zjjyih01sdp33w57f9yx95qgshzzg9q33p01d4")))

(define-public crate-inip-0.1 (crate (name "inip") (vers "0.1.5") (hash "06646czq76h11vzdra9h0d1dwf4cypsp63w69h68shj6xjbxvbzq")))

(define-public crate-inip-0.2 (crate (name "inip") (vers "0.2.0") (hash "15lvqhlgn30hg6j6h3j12sfix7dd8zag9yyaa3ifjs8lhnahpiir")))

(define-public crate-inip-0.2 (crate (name "inip") (vers "0.2.5") (hash "05k89pc6qr1yay84dsii8rkyb2hiz0yja92naq2xq8y8idjaj2ai")))

(define-public crate-inip-0.2 (crate (name "inip") (vers "0.2.6") (hash "0mlrcgwa200w9q7wg3h5khc0hf47i1f7m8ax15q6a62cklp1gqny")))

(define-public crate-inip-0.2 (crate (name "inip") (vers "0.2.7") (hash "0qap8fjr12kx2bjbqschjf7q4fjyicpqzq1wqr93misapixfk9ix")))

