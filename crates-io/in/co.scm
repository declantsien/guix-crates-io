(define-module (crates-io in co) #:use-module (crates-io))

(define-public crate-income-0.1 (crate (name "income") (vers "0.1.0") (deps (list (crate-dep (name "deku") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "10c3xc2k99sa67wrb25xsv8s949px80cwqam10f4vxzh2jannps9") (rust-version "1.60.0")))

(define-public crate-incoming-0.1 (crate (name "incoming") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("tcp" "uds" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("tcp" "uds" "io-util" "rt-core"))) (default-features #t) (kind 2)))) (hash "1b8v301w7sbmkawdbms4rngw1q1ysniq0s46rybld8dci42mxicj")))

(define-public crate-incoming-0.1 (crate (name "incoming") (vers "0.1.1") (deps (list (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("tcp" "uds" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("tcp" "uds" "io-util" "rt-core"))) (default-features #t) (kind 2)))) (hash "0vsx57ky0yycpk1yjpc50rdba8rqk8swz4bc5xpbz2h9ng0cnxh3")))

(define-public crate-incomplete-0.1 (crate (name "incomplete") (vers "0.1.0") (hash "1df8mf474952znwxn6c0a86ylcxz3x7j8cbsi7p1b5j3ag16fzsa")))

(define-public crate-incomplete-0.1 (crate (name "incomplete") (vers "0.1.1") (hash "0p604shjk44hs0a15mphq5xrcs2yiy0kglkmc4zina8v8jpdr5a2")))

(define-public crate-incomplete-0.1 (crate (name "incomplete") (vers "0.1.2") (hash "09x6db78mi3mbzx1sxzc5j54s9hhqs53c4jmywva81q3dnv93fzx")))

(define-public crate-incomplete-0.1 (crate (name "incomplete") (vers "0.1.3") (hash "0cm1p2c05964bn0wb3czgf9w5wkls0qpgv67h90cnbm2vbblqbqi")))

(define-public crate-inconceivable-0.9 (crate (name "inconceivable") (vers "0.9.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0623asg4ykmqr2s2rl5x1x7ng4khzb804gv5b4wiv5j6nk2ff446") (features (quote (("ub_inconceivable") ("std") ("default" "std") ("RUSTC_VERSION_GE_1_27"))))))

(define-public crate-inconel-0.1 (crate (name "inconel") (vers "0.1.0") (hash "1s8dc6l6n35bpck5b9gvqqx3sq63pjcxcndjzyahhc2vik93ynsm")))

