(define-module (crates-io in me) #:use-module (crates-io))

(define-public crate-inmemdb-0.1 (crate (name "inmemdb") (vers "0.1.0") (hash "0zhcqq1yrlm60mly5w5rbngbb4a6nk8irsajic5r20jfwd6kj9p2")))

(define-public crate-inmemdb-rs-0.1 (crate (name "inmemdb-rs") (vers "0.1.0") (hash "0qcqkz7b7wip50sm5rizrsl108gal4vsvqp793vw2pqm9czn3jmk")))

(define-public crate-inmemory-keyvalue-0.4 (crate (name "inmemory-keyvalue") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "wascc-codec") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "wasmcloud-actor-core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "wasmcloud-actor-keyvalue") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1j5jrmq6jak2nbayyh7b65a6arzvzypqw79yv61z7nhqmqpcy21r") (features (quote (("static_plugin"))))))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.0") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "14mz820aispwwhhj29scblipl95kyx2rnh36ra9lbas0dmfbsfpl") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.1") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "182ghvjv2qcgd42wpwwvwa701cdhgjavba7pkdl7a0m9aawza387") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.2") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "threadbound") (req "^0.1") (default-features #t) (kind 0)))) (hash "1p3kw8ds0cj9nkafnp26h8g77940gs1pny9giy0lp3vn1f5i9v7a") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.3") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w0xx78q31qcwcylanslwp2liqzpq1cxfihhcm60bkb60wji8jyi") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.4") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "14542hzrkkyl4q1fa19vsmfv8lc4wm8svpq29y23yfhxyix1qh1l") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.5") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vapjkwdrmbzvcm9aycf7qpy4nf3m1w31cxyp7y6zrijqfndsil4") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.6") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mcc1wk1h6hlhb2w27rravzv6v963j7cvi3863b7wl22131zbb65") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.7") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "00dmw914kinklvlddn9wxfz3yawxy0rdmjqzddlwblfk5l83d084") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.8") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pxjcrqdwfk9sdrbhx9isk34231gfszb7b9sa6ch8bbayh47z8hb") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.9") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fp1lzbwlhw85l5m6q3s7lzd6wj6k9an22mq6fw5gz99wkg0cnvz") (yanked #t)))

(define-public crate-inmemory-sqlite-0.1 (crate (name "inmemory-sqlite") (vers "0.1.10") (deps (list (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fbzd5piv53p3bfmg68igyj2s8lg8lkmb0hqnklhphizc43jp7r8") (yanked #t)))

