(define-module (crates-io in v-) #:use-module (crates-io))

(define-public crate-inv-sys-0.1 (crate (name "inv-sys") (vers "0.1.0") (hash "0gxrzd03akja2yd81fvy0sx9pp3ka95hvvhb0b3bdiwb01kcryja")))

(define-public crate-inv-sys-0.1 (crate (name "inv-sys") (vers "0.1.1") (hash "1bh60mr87gs6qid1avixbvpzvxq3503m929vihizm82ilkfdkc3k")))

(define-public crate-inv-sys-0.1 (crate (name "inv-sys") (vers "0.1.2") (hash "06ga3z8gd8s52xr32d7863nskwvvhys0nxnyfv18dibr1zwrngw3")))

(define-public crate-inv-sys-0.2 (crate (name "inv-sys") (vers "0.2.2") (hash "017q25gnlfpmwq66cy89zbvcjv6x735fpskzgpg6m9qjv3cnm4nn")))

(define-public crate-inv-sys-1 (crate (name "inv-sys") (vers "1.1.0") (hash "0iwz56jz0n6bmp35qzdl7wzh4xvgxbqbyq374j4cd7xwh9hc55p2")))

(define-public crate-inv-sys-1 (crate (name "inv-sys") (vers "1.2.0") (hash "1yv8c2cm5rwkixa8kxwf3x64dyygwq4np4adxii5bcxp6ph8dcmr")))

(define-public crate-inv-sys-1 (crate (name "inv-sys") (vers "1.2.1") (hash "0pxkkfbrhjadg5syj8mrmirhpw06dxyry190z9gpgichcya7wrzm")))

(define-public crate-inv-sys-1 (crate (name "inv-sys") (vers "1.2.2") (hash "0aimibgjwcgf4dwzbh4gm7wbrav3sf8dq64y4s1sbjsr9lykr05x")))

(define-public crate-inv-sys-1 (crate (name "inv-sys") (vers "1.3.0") (hash "03yfar5nrjm259rx8gl3a84dss67d4c6g8s3yj5q25ly6vvvc654")))

(define-public crate-inv-sys-1 (crate (name "inv-sys") (vers "1.4.1") (hash "1pzyvvknr48id39jqmxzaqiyvvn2n2rc7i9smq7j2nn7mmv1whkc")))

