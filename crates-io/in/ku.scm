(define-module (crates-io in ku) #:use-module (crates-io))

(define-public crate-inku-0.1 (crate (name "inku") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 2)))) (hash "0nsc6mf22phvymdcc4hw4pb47q7xnw32q9vrvp69xlppikp8qi27")))

(define-public crate-inku-0.2 (crate (name "inku") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 2)))) (hash "09pwz09c3r6fvziksyhma8gw3fxsaw63yqb4w3lmj15izi48c96l")))

(define-public crate-inku-0.3 (crate (name "inku") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 2)))) (hash "1bh0759xfm55blcpzcz9x7bpdmlqvvlyyga4rkb48axf8iq491ba")))

(define-public crate-inku-0.4 (crate (name "inku") (vers "0.4.0") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 2)))) (hash "0v9cdx7114yngfnr13hnkbd5hjwjk784fn7crsssn0pylrlyfwhi") (features (quote (("color_debug" "crossterm"))))))

