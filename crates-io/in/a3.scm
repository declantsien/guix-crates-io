(define-module (crates-io in a3) #:use-module (crates-io))

(define-public crate-ina3221-0.1 (crate (name "ina3221") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)))) (hash "0sx7ki197q5xmpf7mbhrincy1mk11bipil809jl9159ah86pdcs1")))

(define-public crate-ina3221-0.2 (crate (name "ina3221") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)))) (hash "12xn19561fk16hvmjffimr84an61b9a0cshw2apcdp0xcb343w81")))

(define-public crate-ina3221-0.2 (crate (name "ina3221") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)))) (hash "04kxcinbbxsibk1q00ajn8gbfay1b46xx5lh0jqn07nwy57l0024")))

(define-public crate-ina3221-0.3 (crate (name "ina3221") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)))) (hash "1l3zzdlqkadh8xsg615ai0r1gj5g4a08836ribky5wy3glar6yf4")))

(define-public crate-ina3221-0.4 (crate (name "ina3221") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "ohms") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10gq0c6032563lfhh5d645bxdf6yy50zkakvi5fnaavk379pndw3")))

(define-public crate-ina3221-0.4 (crate (name "ina3221") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "ohms") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1s0wink4mfkip086lc4x45z7d1lki802yzpkdgz7aw4n1naqj0qn")))

(define-public crate-ina3221-0.4 (crate (name "ina3221") (vers "0.4.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "ohms") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fi7rrw306p63hsw88vkxapgf8skysmw51x7vq20gasfd2iagfih")))

(define-public crate-ina3221-0.4 (crate (name "ina3221") (vers "0.4.3") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "ohms") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0831kb30mc3zm1bd8jrf1y4fkh44mc88mrcp6w5l2pqra5igcbw5")))

(define-public crate-ina3221-0.4 (crate (name "ina3221") (vers "0.4.4") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "ohms") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1ba8q6cys0c2rv2p5vzggs3knhlg8yky7977hija315bi1iphjmr")))

(define-public crate-ina3221-0.4 (crate (name "ina3221") (vers "0.4.5") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "ohms") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dyr04g2indkrwhq7n71lp1flq9kv1j5752d3r52xdymqidvjlvm")))

