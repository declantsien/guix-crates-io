(define-module (crates-io in pu) #:use-module (crates-io))

(define-public crate-input-0.1 (crate (name "input") (vers "0.1.0") (deps (list (crate-dep (name "input-sys") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hvs7ddbq5w3wx1ylf3r54fndy2l1ps7a3sa268dldhyqx288y0i")))

(define-public crate-input-0.1 (crate (name "input") (vers "0.1.1") (deps (list (crate-dep (name "input-sys") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "16rgx9sxgxil7x9farhf8v4sq141xzkqk9k74cx21w9vv2kvd5gw")))

(define-public crate-input-0.2 (crate (name "input") (vers "0.2.0") (deps (list (crate-dep (name "input-sys") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00ibw9xy26jpp3cbs55sc4irdx6k2qzs4wqphwvh356i3kw0nvia") (features (quote (("gen" "input-sys/gen"))))))

(define-public crate-input-0.2 (crate (name "input") (vers "0.2.1") (deps (list (crate-dep (name "input-sys") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kcy3mdim2qna0r094ma7588rchy2rijm2hkbsszkmx8brkskqgn") (features (quote (("gen" "input-sys/gen"))))))

(define-public crate-input-0.3 (crate (name "input") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14p7l91mx7hymm15yi3q7w34sp7kbca1jjyfcwk213597q25nqn5") (features (quote (("gen" "input-sys/gen"))))))

(define-public crate-input-0.3 (crate (name "input") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "192rq8gyfcv193b32qz165phxxx8cj6syax18r6vzjrpfy0msxc0") (features (quote (("gen" "input-sys/gen"))))))

(define-public crate-input-0.4 (crate (name "input") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "udev") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0ai791frwfpqv96hqhq9djnjgjwr8rggwks0pfywn4gdzb84rxqk") (features (quote (("gen" "input-sys/gen") ("default" "udev"))))))

(define-public crate-input-0.4 (crate (name "input") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "udev") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0s24vslw1c0sknj9r6r1nwajiv2lvv5h4pddlshx4xjq15bjm9nv") (features (quote (("gen" "input-sys/gen") ("default" "udev"))))))

(define-public crate-input-0.5 (crate (name "input") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.15.0") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0v7y76yshbrgsa9fs444chxaf03f4nlayz4fd4gk0581rzh0qmvv") (features (quote (("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "libinput_1_15"))))))

(define-public crate-input-0.6 (crate (name "input") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.15.1") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1l89pbzvx6j5019sfdzvf9hv4r8ndc0x3b26sh1mlsfmfpjjp37n") (features (quote (("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "libinput_1_15"))))))

(define-public crate-input-0.7 (crate (name "input") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.16.0") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0w01jxz7qwady8jz8njb4avnwgjikbyjz3h7dn4sjl5q5wbbnkfw") (features (quote (("libinput_1_19" "input-sys/libinput_1_19" "libinput_1_15") ("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "log" "libinput_1_19"))))))

(define-public crate-input-0.7 (crate (name "input") (vers "0.7.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.16.1") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0pw17g2kj7z7j21w38j346mkrkl8gb361mzvy4zb5jfs4zpl0mpr") (features (quote (("libinput_1_19" "input-sys/libinput_1_19" "libinput_1_15") ("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "log" "libinput_1_19"))))))

(define-public crate-input-0.8 (crate (name "input") (vers "0.8.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.17.0") (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1sdg927axs66yvjqw80qp1aidscyli19h1f2fsf76za6mmabgvdi") (features (quote (("libinput_1_21" "input-sys/libinput_1_21" "libinput_1_19") ("libinput_1_19" "input-sys/libinput_1_19" "libinput_1_15") ("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "log" "libinput_1_21")))) (yanked #t)))

(define-public crate-input-0.8 (crate (name "input") (vers "0.8.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.17.0") (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1qs5bhmip9dc5j33c0jjjj79v8g6h2a3pwiz6bmy471hzq3rqrnl") (features (quote (("libinput_1_21" "input-sys/libinput_1_21" "libinput_1_19") ("libinput_1_19" "input-sys/libinput_1_19" "libinput_1_15") ("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "log" "libinput_1_21"))))))

(define-public crate-input-0.8 (crate (name "input") (vers "0.8.2") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.17.0") (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0dw9xi2x3ih49gcl2fb8jn4ifad6gbwflm5k0a044dw13zgszczf") (features (quote (("libinput_1_21" "input-sys/libinput_1_21" "libinput_1_19") ("libinput_1_19" "input-sys/libinput_1_19" "libinput_1_15") ("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "log" "libinput_1_21"))))))

(define-public crate-input-0.8 (crate (name "input") (vers "0.8.3") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.17.0") (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0ndhy09m3n2wphn784ic9n61i3y4plvq6aklg3dndkgd5kc4rrz6") (features (quote (("libinput_1_21" "input-sys/libinput_1_21" "libinput_1_19") ("libinput_1_19" "input-sys/libinput_1_19" "libinput_1_15") ("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("gen" "input-sys/gen") ("default" "udev" "log" "libinput_1_21"))))))

(define-public crate-input-0.9 (crate (name "input") (vers "0.9.0") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "input-sys") (req "^1.18.0") (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustix") (req "^0.38") (features (quote ("event"))) (default-features #t) (kind 2)) (crate-dep (name "udev") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1l204zfxhxb1wndisdzps1xq56jzi9vsyjawlfs5l361p4yww4br") (features (quote (("use_bindgen" "input-sys/use_bindgen") ("libinput_1_21" "input-sys/libinput_1_21" "libinput_1_19") ("libinput_1_19" "input-sys/libinput_1_19" "libinput_1_15") ("libinput_1_15" "input-sys/libinput_1_15" "libinput_1_14") ("libinput_1_14" "input-sys/libinput_1_14" "libinput_1_11") ("libinput_1_11" "input-sys/libinput_1_11") ("default" "udev" "log" "libinput_1_21"))))))

(define-public crate-input-actions-0.1 (crate (name "input-actions") (vers "0.1.0") (deps (list (crate-dep (name "gilrs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (optional #t) (default-features #t) (kind 0)))) (hash "0idia8dyld20r6gwaf6zcaswh31yzndbs4c9fx0jfjrr6gf94v8w") (features (quote (("default" "log" "winit"))))))

(define-public crate-input-actions-0.1 (crate (name "input-actions") (vers "0.1.1-alpha.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "enumset") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (optional #t) (default-features #t) (kind 0)))) (hash "1vf6acap9k0q2a65jlphsl8m1hi43p4b0hqknqh2xkr3gckhlh0y") (features (quote (("default" "log" "winit")))) (yanked #t)))

(define-public crate-input-actions-0.1 (crate (name "input-actions") (vers "0.1.1") (deps (list (crate-dep (name "gilrs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (optional #t) (default-features #t) (kind 0)))) (hash "11g1bk3d0wz9ryqbhfdy8ks1s7byvpms5z4v9dh0p414rdi59brc") (features (quote (("default" "log" "winit"))))))

(define-public crate-input-device-0.1 (crate (name "input-device") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "poller") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "195mhxz6nasdqg0fk7gg4av6qfzx6gck90l2sjfrhgim2030lai4") (features (quote (("linux-mousedev") ("linux-evdev"))))))

(define-public crate-input-device-0.2 (crate (name "input-device") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "poller") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jpwb27rm3k25442ishszysd4ngpxjcv4ymivjjdmav3sip4mjyr") (features (quote (("linux-mousedev") ("linux-evdev"))))))

(define-public crate-input-device-0.2 (crate (name "input-device") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "poller") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mjyvr2qvcxs5asswxid16x0mig78b1bpzckpa6lpl8b19l3cvpa") (features (quote (("linux-mousedev") ("linux-evdev"))))))

(define-public crate-input-device-0.2 (crate (name "input-device") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "poller") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mqbi0gq7xspn925pbyjasi0nxi0gn93x30r03v9412n98l3ywsf") (features (quote (("linux-mousedev") ("linux-evdev"))))))

(define-public crate-input-event-codes-0.1 (crate (name "input-event-codes") (vers "0.1.0") (hash "0msnmbrpz68nnbx6w5xhyxx61cim8b2b80pcbsqdb59c0sxvjvn6")))

(define-public crate-input-event-codes-0.2 (crate (name "input-event-codes") (vers "0.2.0") (hash "06x1b8yq5zab4wrviya06wx0b8n8pwasravb4im1ashd7y6bvkqv")))

(define-public crate-input-event-codes-0.2 (crate (name "input-event-codes") (vers "0.2.1") (hash "04h7zamv0nsh187n23pn12pw7cn2w1f8mn4zlfwwf083zjh1vddc")))

(define-public crate-input-event-codes-0.2 (crate (name "input-event-codes") (vers "0.2.2") (hash "1gda7y0xjavjy8bijnsp1qwxa9vpjp6pqpdgc3r9c1xmkimyap1y")))

(define-public crate-input-event-codes-5 (crate (name "input-event-codes") (vers "5.16.8") (hash "0dk65hzncgbkis2cfmm92sg1830dqhaj05rgb7ds55pmay2hysvb") (rust-version "1.56")))

(define-public crate-input-event-codes-5 (crate (name "input-event-codes") (vers "5.17.0") (hash "017qgx1qlmsjic0k4k0bl56nv0lb7wbaysxq0wiy4ii8ffb22ssl") (rust-version "1.56")))

(define-public crate-input-event-codes-5 (crate (name "input-event-codes") (vers "5.18.0") (hash "1jni3klb7hanl2k0knwslh6l6s8nwwkj74y6f4z1d4gh8z6abc8i") (rust-version "1.56")))

(define-public crate-input-event-codes-6 (crate (name "input-event-codes") (vers "6.1.0") (hash "0dn51pzl0ffpy3jidhifkanp8mw9wvglqdsxq8xcdqfzk218fwzk") (rust-version "1.56")))

(define-public crate-input-event-codes-6 (crate (name "input-event-codes") (vers "6.2.0") (hash "1pcj6wi7w7fp8fhfij7fnh1jzk9hsqldxql0lcjhbhsnd1dvhdyl") (rust-version "1.56")))

(define-public crate-input-event-codes-sys-0.1 (crate (name "input-event-codes-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)))) (hash "1086kdfnjxpvyxc6aa0wqgxiyk1xdwj1bwxpl684hmgqdccvn41f")))

(define-public crate-input-linux-0.0.1 (crate (name "input-linux") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "input-linux-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "0fsjkp4nr3bm81zg9bigqhvsx74qpz651hm8c958zyxks2faxwgn") (features (quote (("with-tokio" "tokio-io" "bytes") ("with-serde" "serde" "serde_derive") ("unstable"))))))

(define-public crate-input-linux-0.2 (crate (name "input-linux") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "input-linux-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.3.1") (features (quote ("codec"))) (optional #t) (kind 0)))) (hash "1i0z0klxb9a557nvcyyi3pvhzcsnfxy9vgxlklmibavycxhlnbbl") (features (quote (("with-tokio" "tokio-util" "bytes") ("with-serde" "serde" "serde_derive") ("unstable"))))))

(define-public crate-input-linux-0.3 (crate (name "input-linux") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "input-linux-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.3.1") (features (quote ("codec"))) (optional #t) (kind 0)))) (hash "0p2k26z05r3kk768qw2v5gxvwwhfbnf3b9vr8wn9bq4f2ci5cp79") (features (quote (("with-tokio" "tokio-util" "bytes") ("with-serde" "serde" "serde_derive") ("unstable"))))))

(define-public crate-input-linux-0.4 (crate (name "input-linux") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "input-linux-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.5.0") (features (quote ("codec"))) (optional #t) (kind 0)))) (hash "1mkaq07niyq9nyavvxhmgyd5rlsq9khicwn213jz25yq0vv8z0mz") (features (quote (("with-tokio" "tokio-util" "bytes") ("with-serde" "serde" "serde_derive") ("unstable"))))))

(define-public crate-input-linux-0.5 (crate (name "input-linux") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "input-linux-sys") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.0") (features (quote ("codec"))) (optional #t) (kind 0)))) (hash "0l2lsn6h0hxza2dksjds17r5s083vxd4k9vf0j45s9fcwz09bp2h") (features (quote (("with-tokio" "tokio-util" "bytes") ("with-serde" "serde" "serde_derive") ("unstable"))))))

(define-public crate-input-linux-0.6 (crate (name "input-linux") (vers "0.6.0") (deps (list (crate-dep (name "bytes") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "input-linux-sys") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-util-0_6") (req "^0.6") (features (quote ("codec"))) (optional #t) (kind 0) (package "tokio-util")) (crate-dep (name "tokio-util-0_7") (req "^0.7") (features (quote ("codec"))) (optional #t) (kind 0) (package "tokio-util")))) (hash "01jnwfb5nm7gz4wr5jd75fa0wp47v4v3lvk9h9hvx2l0x8j34h3g") (features (quote (("unstable") ("dox") ("codec" "bytes")))) (v 2) (features2 (quote (("tokio-util-0_7" "dep:tokio-util-0_7" "codec" "bytes") ("tokio-util-0_6" "dep:tokio-util-0_6" "codec" "bytes") ("serde" "dep:serde"))))))

(define-public crate-input-linux-sys-0.0.1 (crate (name "input-linux-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "07x21a6pyf28y5fqhnvrln9mvrh65ym5aphrc8p2pcyl73wdy5wl")))

(define-public crate-input-linux-sys-0.0.2 (crate (name "input-linux-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1yazz5dj0r4ycfxj6y2fd9v69ip89rkv0h0yjm0n39p5dnb43zba")))

(define-public crate-input-linux-sys-0.0.3 (crate (name "input-linux-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0bk9yqlcqynd6jmqhwvwzf88m1a2phamjrmfqba8pf11df6qcdia")))

(define-public crate-input-linux-sys-0.1 (crate (name "input-linux-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0bwz16b4rjv539dky3597fyj055ag929hyj1s8knapr7v3c7bll1")))

(define-public crate-input-linux-sys-0.1 (crate (name "input-linux-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1ac4vj1a2mfv207qf8idfcsja54bwv5xwns642glinb04h8c897h")))

(define-public crate-input-linux-sys-0.1 (crate (name "input-linux-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0cz3d5qjfmd08dvz4qj9l2v6rk46ljrkm7ljn8gsdfykv778k0n8")))

(define-public crate-input-linux-sys-0.1 (crate (name "input-linux-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "17mz6syzg2xnq8vfl30j7xnb7s2b61xhlmcb0hzcs0m25l6igy4p")))

(define-public crate-input-linux-sys-0.1 (crate (name "input-linux-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "08sdbim8v7saqg9zvmm7wh1k4213685vhycfixjjmr04rqi0qglh")))

(define-public crate-input-linux-sys-0.1 (crate (name "input-linux-sys") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0b628v2z0741srljw1x5g7x7r15m670n348m40vdivzw73h66sn4")))

(define-public crate-input-linux-sys-0.2 (crate (name "input-linux-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "11iyw48w1q9jc1y1s887ps502hy0hcjmivmqy9kbbbrf720jmg8k")))

(define-public crate-input-linux-sys-0.2 (crate (name "input-linux-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0irw66kcib3s0gq06wd9aizrr1kk88zmdzszzzry7shjfk98ynii")))

(define-public crate-input-linux-sys-0.3 (crate (name "input-linux-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0ihs9kvx5lvcmp4gmfhk3jd6br73qxfnnw66igiwaghamg0ni33c")))

(define-public crate-input-linux-sys-0.3 (crate (name "input-linux-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "115r28lcw24i1hlhr24ng3yr7a9x4b3ccsimrk74wz5ha0cbwwsq")))

(define-public crate-input-linux-sys-0.4 (crate (name "input-linux-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0cahiajr3py7cfxhmf5n76mxhg0kby0s7cddgasbnxrakhx8jp8n")))

(define-public crate-input-linux-sys-0.5 (crate (name "input-linux-sys") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0y4jf7zy9q367wnkadzrq1z825z3xrs6kcmypllrz3ssb1wmis0v")))

(define-public crate-input-linux-sys-0.6 (crate (name "input-linux-sys") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1xggr00jnvv1bqfqm3f0w2x5h1qzcyr9dp17h22n6n5jhgihcaq6")))

(define-public crate-input-linux-sys-0.7 (crate (name "input-linux-sys") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "19hrbh9fmbwmij9m914h2pxvrmgx7fajj404x98g8aq8hgzkhnqw")))

(define-public crate-input-linux-sys-0.8 (crate (name "input-linux-sys") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 0)))) (hash "1f4s55jsw1jnx97a757nb3ly2zzri5aklxakj7gjfc4plhjpls0s")))

(define-public crate-input-macro-0.1 (crate (name "input-macro") (vers "0.1.0") (hash "1gkvd9jr35ynh9c3x1fyj5ay7wsnsljxg454msrpa5z6mj90rrbm") (features (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1 (crate (name "input-macro") (vers "0.1.1") (hash "0lagfkav99zdg4zzbniy8hb964ih6ayrr3xh71r73kqc4nzj6bi2") (features (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1 (crate (name "input-macro") (vers "0.1.2") (hash "1f4ldy2jgf9z1j9abi5rm6qv8i4qyg26c48l9wbgz2bsydks06j2") (features (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1 (crate (name "input-macro") (vers "0.1.3") (hash "10rpb3r0vkzs0k4107ifx89l22nmvqd7mdmhgm6251q1pvc8qs4i") (features (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1 (crate (name "input-macro") (vers "0.1.4") (hash "0gxqbdb38j74q29xqr58mldswaj2v57ynjcmf9951n3jw6jnjq5a") (features (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1 (crate (name "input-macro") (vers "0.1.5") (hash "00rm5zq8h1yk1gd8fidn55wsp19i3mgpsn9zzaldqijm5rv10s0k") (features (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.2 (crate (name "input-macro") (vers "0.2.0") (hash "1hbnmrnimypm924pxil8llinkjdla7p0395zk5frk4nj94qn6hah")))

(define-public crate-input-painter-0.1 (crate (name "input-painter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "15xn97svqmv246z9bbhmkkjvm2dhjmpphkv0mwkfg27pgmlmn2wp")))

(define-public crate-input-painter-0.1 (crate (name "input-painter") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "156bxcmq39h3bg7mfsf3m99nkwm3vxbdpqv6garq9lj70yb5dgd1")))

(define-public crate-input-painter-0.1 (crate (name "input-painter") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1phb0mg2wznkv9zx1j8whx3181h3lrmp2whky63hj7hrl9b1v4ha")))

(define-public crate-input-painter-0.1 (crate (name "input-painter") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1pzl8s9k4g5qb7jsrrzqj56y78vgmgbgsbrnqmlmwm4681yim2ga")))

(define-public crate-input-painter-0.1 (crate (name "input-painter") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1h17h1g6q5cv3c6943zmzyrjc45li6df19zqy4vbcw4dcpwa1xq6")))

(define-public crate-input-stream-0.1 (crate (name "input-stream") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "04fxha30dkvbn55rgvk797mmfh25sbyjisjvp8z1fva0nimij7b3")))

(define-public crate-input-stream-0.1 (crate (name "input-stream") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "07rhqwc3s4778jg07hir9429a6pcdzsgk8l89iscpj1fnfr5d1n1") (features (quote (("default"))))))

(define-public crate-input-stream-0.2 (crate (name "input-stream") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (kind 2)))) (hash "0xjk6xx27kmr8jk91q2cbjnl1alkzs1cg2qi93ab89z98x9w7rjc") (features (quote (("default"))))))

(define-public crate-input-stream-0.3 (crate (name "input-stream") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (kind 2)))) (hash "1z29zly35rngvj06sxivyfrp55cc0dxbd7ymav4brsvgxfgqvcl5") (features (quote (("default"))))))

(define-public crate-input-stream-0.4 (crate (name "input-stream") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1k2gdhd0w35ad5xb18m06sls7vsyyk38p82aia4bbd6gvzcz1yzp") (features (quote (("default"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.22") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cp2c5c0rhd92hsaz6z3fgk9jp1vy68s9c04bqi9bmmfa1vmv772")))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.7.1") (deps (list (crate-dep (name "bindgen") (req "^0.22") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f79b1xdq2y7r613r8w2zhshj5nk6s62v88w6yn8irxb56psndm8") (features (quote (("gen" "bindgen"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.9.0") (deps (list (crate-dep (name "bindgen") (req "^0.31") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y3q3d7sllcg3n8dgj46n6s3va2mj0nw7zrhchhcvzmnk77n8axf") (features (quote (("gen" "bindgen"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.15.0") (deps (list (crate-dep (name "bindgen") (req "^0.31") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mbajhvbs2l39a43l8gpwdpr1z8bl2xh6libv75s9ak5rxzxy1gy") (features (quote (("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.15.1") (deps (list (crate-dep (name "bindgen") (req "^0.57") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "111a2ps21y0hda0ifinps19p4d7pvgajn2fjk1cvlm1vcdlw9c8b") (features (quote (("update_bindings" "gen") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.16.0") (deps (list (crate-dep (name "bindgen") (req "^0.57") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wyaj418h0mwbd04dpcbfblpps82n68c9viz1kahqzfxqmkr9han") (features (quote (("update_bindings" "gen") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.16.1") (deps (list (crate-dep (name "bindgen") (req "^0.57") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cqrar5zid2vdb0rb40ybycpfc0rwpd6q3v03h4mvfsym6fq97g0") (features (quote (("update_bindings" "gen") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.17.0") (deps (list (crate-dep (name "bindgen") (req "^0.57") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hnx7xh8dcvnxwh8cjqxh6nyn3vvz0x8cchfcqbp5flagshw5xh5") (features (quote (("update_bindings" "gen") ("libinput_1_21") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_21"))))))

(define-public crate-input-sys-1 (crate (name "input-sys") (vers "1.18.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0.76") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.10") (optional #t) (default-features #t) (kind 1)))) (hash "1c4y24wf0jixi52js4f7cjspbgi0bzzaqfhn8m91qcq03i6mnkxx") (features (quote (("use_bindgen" "bindgen" "proc-macro2" "regex") ("update_bindings" "use_bindgen") ("libinput_1_21") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("default" "libinput_1_21"))))))

(define-public crate-input-utils-0.1 (crate (name "input-utils") (vers "0.1.0") (hash "05ys9kvxxyr6hh5aa1hs9lzkxsps2c6wkvbsh0ls3zrvriwlf5ak")))

(define-public crate-input-utils-0.1 (crate (name "input-utils") (vers "0.1.1") (hash "0gzd092h4xbdm12y2sn2a9x2w3f10v4c23wxzm0cc6md178ddcc8")))

(define-public crate-input_buffer-0.1 (crate (name "input_buffer") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1bpw2an1sz723w0i8vmjl79qq3drldmbzfx7hlrr2si84slksfzl") (yanked #t)))

(define-public crate-input_buffer-0.1 (crate (name "input_buffer") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1cgr39wahmld7cf56k84431ahyj5iyjsskiycqlcxrqm5zfm5z34")))

(define-public crate-input_buffer-0.2 (crate (name "input_buffer") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1gyzxv0wgns29lmlx2h7q5whmxfmirh82vqxjd8mb424r0n846wf")))

(define-public crate-input_buffer-0.3 (crate (name "input_buffer") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "1a8byjsa6ab1xij0mmbn499l86w5zx192aydfh0i7qhb1azkp43d")))

(define-public crate-input_buffer-0.3 (crate (name "input_buffer") (vers "0.3.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "0m4pamqvr00z90cmrgjj25iwpqy6fyac53k1ms63k86m8d9aka0r")))

(define-public crate-input_buffer-0.4 (crate (name "input_buffer") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "04yl6pdjawq5grl946hn3imfs2cx0r0vrc0jvdyim3s4bybnfygr")))

(define-public crate-input_buffer-0.5 (crate (name "input_buffer") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "044qxqdkcq6mv07bsvm35hl7hy3rmf87lrxjyz8zaq57i0xngvmc")))

(define-public crate-input_conv-1 (crate (name "input_conv") (vers "1.0.0") (hash "0cvd3cn1j0rk54cdb0r12apwcqx2n2gkpznjnyg8ydqqpmpjzai2")))

(define-public crate-input_conv-1 (crate (name "input_conv") (vers "1.0.1") (hash "1hb2vmkhh88ipq5x1j5y59v28p9j9shjpag5rgyp43rvv4mhpp54")))

(define-public crate-input_conv-1 (crate (name "input_conv") (vers "1.1.0") (hash "176qkpy90vblh2yq8x7iafrii5chb1pvdzrllnid589ckkd60rpb")))

(define-public crate-input_conv-1 (crate (name "input_conv") (vers "1.1.1") (hash "0h5xxi2fgvy9bj7v20q9xfhmdxa07hjvsq6wim54zfpgf1848v9b")))

(define-public crate-input_conv-1 (crate (name "input_conv") (vers "1.2.0") (hash "1qzd7akkqjsmrrmdlfwib02qxvlz2v1r3hb9hq6yyc53rxsrpg10")))

(define-public crate-input_crate-0.1 (crate (name "input_crate") (vers "0.1.0") (hash "0g4i1qzfvwwfa5kpr1ka6wkj6qi2xmkhwzr7m3mvj37jbr2n3njm")))

(define-public crate-input_event_codes_hashmap-0.1 (crate (name "input_event_codes_hashmap") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "15k61yz4wcw35jvnwzhic1lwwnmjwrkzmlb2xi1dwc37kx0srycy") (yanked #t)))

(define-public crate-input_event_codes_hashmap-0.1 (crate (name "input_event_codes_hashmap") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0ajxs60ac6l0cjvfaawkij7yc2c9d95w7xyhdqg8bygk7ycrrqjp")))

(define-public crate-input_helper-0.1 (crate (name "input_helper") (vers "0.1.0") (deps (list (crate-dep (name "event_feed") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (optional #t) (default-features #t) (kind 0)))) (hash "055arpdnk9crxb4yhg6cglqkwl5jyqf7qa5lwnfpfbb6vjd01cg4") (features (quote (("default")))) (v 2) (features2 (quote (("winit" "dep:winit") ("sdl" "dep:sdl2") ("gilrs" "dep:gilrs"))))))

(define-public crate-input_helper-0.1 (crate (name "input_helper") (vers "0.1.2") (deps (list (crate-dep (name "event_feed") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (optional #t) (default-features #t) (kind 0)))) (hash "0rmdbg07vc3zm1qgqxd3pxd2fx7cnqcc1xazb9arg2ldw5ml0xrk") (features (quote (("default")))) (v 2) (features2 (quote (("winit" "dep:winit") ("sdl" "dep:sdl2") ("gilrs" "dep:gilrs"))))))

(define-public crate-input_manager-0.1 (crate (name "input_manager") (vers "0.1.0") (hash "1hmicqk99pkl4ryacc2sz1imzx0jng8il36ssbd90hd1g2wljjc9")))

(define-public crate-input_opr-0.1 (crate (name "input_opr") (vers "0.1.0") (hash "1vn3ng38ri4xwhyag2d1z08jyvl9za6ldamwp3qcndcqgprjgcfx")))

(define-public crate-input_py-0.1 (crate (name "input_py") (vers "0.1.0") (hash "1hzc7nsaxj4d60465dq2z60a16p3m524kbjd3rc294wa4hs5mzj9")))

(define-public crate-input_py-0.2 (crate (name "input_py") (vers "0.2.0") (hash "1v5spc0ka1d9651zcl4j8vk9wfg54nc2zqa35k6qj9r1ggnn5mx7")))

(define-public crate-input_replace-0.1 (crate (name "input_replace") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.135") (default-features #t) (kind 0)))) (hash "0nll4nsa226j34jy8wybz2pjkp4wd227rmppf1g1qml07pxq6lpl") (yanked #t)))

(define-public crate-input_replace-0.1 (crate (name "input_replace") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.135") (default-features #t) (kind 0)))) (hash "1n98xw6qa1hhw8vpia7d334fks6b5sj0x6sp35m8m9dvnkhzayf3") (yanked #t)))

(define-public crate-input_table-1 (crate (name "input_table") (vers "1.0.0") (hash "0x2rpxqn3m277p1pfa9mrjcrldnjgpczh8v0ibrmr3nfalk9vd11")))

(define-public crate-input_validation-0.1 (crate (name "input_validation") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1shl00q2j57a6fzjsmdysd742yvfdpvdlc67dizhzvmcijkxp1l7")))

(define-public crate-input_validation-0.1 (crate (name "input_validation") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1h64s78x46vfcf9cbv0zciriw121bj1qn7vvfghs2pjjqcxrp6my")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.0") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "18fcawnjni1gl2214xzkp7z2ldwxpzrzkg7yr2927jh15gpsq344")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.1") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "0rwr25j6rs5giw27d4wrz7wajvbhd689hpg9g1h2hyhq01vcag2v")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.2") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "0gjz87b00n4fwfv82n27l422s3ar21zrakrrsmzjf3vqrfcn16bl")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.3") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "147vy9cc6lnz530qm26bd04szqbndvp13k7fjqyndz1fyz955jg3")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.4") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "0pcjjlsgkbfjcxg78nwfic6jabi04fx8lywk302lyzppbnzw3zs8")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.5") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "1fac69zwrmv66xa335cp2hj3a0j8590mh8mz05qksyn73xa17n1j")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.6") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "0vsf0xncmh2cfidwqhy0lpmm2fqr90r21zkg4mi1s4x8fpdwzbmn")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.7") (deps (list (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.20.0") (kind 0)))) (hash "1rbgq070bx3wyvgiwxs04vim7vb0sm6pb85fymwdc6akkrir2rrq")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.8") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.21.0") (kind 0)))) (hash "00f4yxr1n2mkxfd8svxrjc4m1gv4dw9xx7j1giv0dl0byp71bmd3")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.9") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.21.0") (kind 0)))) (hash "17b03rryzv57va282dj964zqkdwjjbbp6s2xbib17j4h8fwrds38")))

(define-public crate-input_yew-0.1 (crate (name "input_yew") (vers "0.1.10") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.64") (kind 0)) (crate-dep (name "yew") (req "^0.21.0") (kind 0)))) (hash "0v6s2k7d0kqpvb76f1d21w8rqdwrrsb022aydxjjddywg1xy1a9n")))

(define-public crate-inputbot-0.1 (crate (name "inputbot") (vers "0.1.0") (deps (list (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "03xq7v215bcxar3g7kl2xwd2lrjfsbj7nvvyxbvqjvyp95ils56i")))

(define-public crate-inputbot-0.1 (crate (name "inputbot") (vers "0.1.1") (deps (list (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "13b0c3kkh3kbxzlqrkljcny3mphnq2p0rz4dpiwlshpif4jmk4ia")))

(define-public crate-inputbot-0.1 (crate (name "inputbot") (vers "0.1.2") (deps (list (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0ig8k890xzy9ngprwzzardi4q78m0x1njcrvdb9g6p23fj6igg3f")))

(define-public crate-inputbot-0.2 (crate (name "inputbot") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1md62q2bgya7d83l1yk9xffhk44g3icd54ncv82i50ng09cfjbmr")))

(define-public crate-inputbot-0.2 (crate (name "inputbot") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.14.0") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "020js503ajl7mmi729dk6hwi6wmvlk7hli14c2lmk9anfw8qrp15")))

(define-public crate-inputbot-0.2 (crate (name "inputbot") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.14.0") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1r0vsf6qrhvl660ylhz8jqq2c3xs6sriwlg3sam417g3bhgrmvhr")))

(define-public crate-inputbot-0.2 (crate (name "inputbot") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.14.0") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1fap6nrclf506pxndc389vw3w7as9xfp129l8j9nlzlxqwz8wyaa")))

(define-public crate-inputbot-0.3 (crate (name "inputbot") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.14.0") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "127bksw4fvnsjz9kcklcmchavxv9ayd7lqp6ng58j2c3x6qaj156")))

(define-public crate-inputbot-0.3 (crate (name "inputbot") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.17.0") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1rqf1j8g17zgi5wcg6a449vhabqm8al74ly760rkbh96chn31b4j")))

(define-public crate-inputbot-0.4 (crate (name "inputbot") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.17.0") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "15syzr3imnsnvqh80r9vdqqqi0szgbz5jyinmv7mnvh8il9rmjx8")))

(define-public crate-inputbot-0.5 (crate (name "inputbot") (vers "0.5.0") (deps (list (crate-dep (name "input") (req "^0.5.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "nix") (req "^0.17.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "once_cell") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "uinput") (req "^0.1.3") (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0262pch31v37dsgcn9c0gbvjnj524gq2hwyzjkjqbxqg3m7lx60k")))

(define-public crate-inputbot-0.5 (crate (name "inputbot") (vers "0.5.1") (deps (list (crate-dep (name "input") (req "^0.6.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.101") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "nix") (req "^0.22.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "uinput") (req "^0.1.3") (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1122lki137nngz79yvpd4vf4jivjmis5zbl7y0fyw2gzva205jcb")))

(define-public crate-inputbot-0.6 (crate (name "inputbot") (vers "0.6.0") (deps (list (crate-dep (name "input") (req "^0.7.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "nix") (req "^0.24.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.2") (default-features #t) (kind 0)) (crate-dep (name "uinput") (req "^0.1.3") (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11") (req "^2.19.1") (features (quote ("xlib" "xtest"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1bv2hkkx95g4601681586xv4qybvqbxlaz4k3c2hv8i6nhqvj35l")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.0") (hash "1is8jlqmis4i3div6dfihccg9iz4bsfrjrhwlpmk10g3nbhxlklr")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.1") (hash "0f8h60l5rqfygq08ir9ks70gw7qn4pqd9sh4cl3ld9gczhw1b7n3")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.2") (hash "1lwwqzs49aqkngsixmxxma99dzw6l0cn9ixrhslwvap13564cj3z")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.3") (hash "0jjsi33kypgqda3218f8g2x2kamwvkdi6fvqlb845fr5qc8pwb1c")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.4") (hash "0j32wdx2ixb885qncaxaqxmjw64gmq2vp3yivhjnzrrlqfnp2sq0")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.41") (hash "0mwx6avqzlnnkxd97fafa029qkgqy2szqp4qr00llwdxjh1mhzzx")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.5") (hash "0ss4klfn7426qxg5cz6fbx479sibigdd7r1p1xqdjsy902n3jn6z") (yanked #t)))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.51") (hash "12xjdbad5i1xffnv9fhk8maii22pvhlvwc0y1aplb8nh4x5wy2xm")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.52") (hash "0iaq7mn55m9nbxq3y14hrf5ziazfcx5754vm330wzjpby4w1q4h4")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.53") (hash "1xpfkidl8pcg2v5i0gmqsjyixzxwd4n25ky5911qkwdilg41191z")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.6") (hash "0sv4rmr3hi3h7g0p0wkygnyjq4yi31mw7rjzgmrljilcsn2a96hy") (yanked #t)))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.60") (hash "09rxyqry3gwm14phj2jznv8h2lf9d91g42q02dd0klw3p036kfpn")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.70") (hash "1qa7akgf5825an2nzcp08dl2js3spn2jclx8maxkwmgj87qjmj7c")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.71") (hash "13hj6if4fxq94jvv6ig5jw9r67n6j0ssm38pw9r37rmy5l78v92d")))

(define-public crate-inputparser-0.1 (crate (name "inputparser") (vers "0.1.72") (hash "1clas1h4was7caqjwjl1w3hpg0x0c0ka71przxf1bmp7wfzry917")))

(define-public crate-inputplug-0.4 (crate (name "inputplug") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req ">=0.19, <1.0") (default-features #t) (kind 0)) (crate-dep (name "pidfile-rs") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "x11rb") (req "^0.8") (features (quote ("xinput"))) (default-features #t) (kind 0)))) (hash "001fhhgzqbhbmb9kbrgyf5apicyy4jg08gbajmv64k8ij7qffz97") (features (quote (("pidfile" "pidfile-rs") ("default" "pidfile"))))))

(define-public crate-inputstat-0.1 (crate (name "inputstat") (vers "0.1.0") (deps (list (crate-dep (name "evdev") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "1zn2m1iaswq0xmm3vc9jx0sd2p8j8vzlwxkk9j87r12rbm6m7pnc")))

(define-public crate-inputstat-0.1 (crate (name "inputstat") (vers "0.1.1") (deps (list (crate-dep (name "evdev") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "11f5djh08csr0hj1hiikgz1ryrhhbb0acmxzc1xrsnk0ga2p15v4")))

(define-public crate-inputstat-0.1 (crate (name "inputstat") (vers "0.1.2") (deps (list (crate-dep (name "evdev") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "0frrlld9fhqzlqxf0dnga7m66fyxv6y39w1fvmkdnhy276wbcj1l")))

(define-public crate-inputsynth-0.1 (crate (name "inputsynth") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "x11rb") (req "^0.9.0") (features (quote ("xtest" "xkb" "allow-unsafe-code" "xinput"))) (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "xkbcommon") (req "^0.5.0-beta.0") (features (quote ("x11"))) (default-features #t) (kind 0)))) (hash "09jcqj5yy15xjhqf2zg7vw23r8rkkk2dv6qfasdmslfl9qsslc0v")))

(define-public crate-inputsynth-0.1 (crate (name "inputsynth") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "x11rb") (req "^0.9.0") (features (quote ("xtest" "xkb" "allow-unsafe-code" "xinput"))) (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "xkbcommon") (req "^0.5.0-beta.0") (features (quote ("x11"))) (default-features #t) (kind 0)))) (hash "0v4x7iqng0gw27xnn0jqj4b580h3k3yg3yl1cqphn94x3bi35ygj")))

(define-public crate-inputsynth-0.1 (crate (name "inputsynth") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "x11rb") (req "^0.11.1") (features (quote ("xtest" "xkb" "allow-unsafe-code" "xinput"))) (default-features #t) (kind 0)) (crate-dep (name "xkbcommon") (req "^0.5.0") (features (quote ("x11"))) (default-features #t) (kind 0)))) (hash "0mg1q1g66ks1n9c1hrkh8hkgzjwvmaapmjvirq3lb25y89dx144p")))

