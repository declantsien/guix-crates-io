(define-module (crates-io in ew) #:use-module (crates-io))

(define-public crate-inew-0.1 (crate (name "inew") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cwxx4an1g5k7sf6kv634yshm61v1klvk4fkk8jhznpsiyjg24xy")))

(define-public crate-inew-0.1 (crate (name "inew") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gvxna8v3nsclbv79432whv4k812qph44rlbwjskhxfy5w76f2qn")))

(define-public crate-inew-0.2 (crate (name "inew") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wrlpm00z50wdmv6innbzdyg52vcncxqzlcx8zgrxyxw3k3macc6")))

(define-public crate-inew-0.2 (crate (name "inew") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c4xgqc1ivdhaacy3zs9wnnzz2aldv1faxg6vdccs65ga6ywpkfy") (features (quote (("std") ("default" "std"))))))

