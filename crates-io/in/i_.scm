(define-module (crates-io in i_) #:use-module (crates-io))

(define-public crate-ini_core-0.1 (crate (name "ini_core") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "15bb9q71cvycg9hwldsyd227nk84bpjgkccjm4kqkdfr17k47ik2")))

(define-public crate-ini_core-0.2 (crate (name "ini_core") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q9sqxz6bjdml84mlgbh4izzbgrp8l41g7595wkbafglm4qpliks")))

(define-public crate-ini_lib-0.1 (crate (name "ini_lib") (vers "0.1.0") (hash "056cna3s0cy7wi81a8cwzzdf4l2j3ik17dqcd5wlanx431mjpyqa") (yanked #t)))

(define-public crate-ini_lib-0.1 (crate (name "ini_lib") (vers "0.1.1") (hash "19dabpjinkjvf0jlr2d3h29m13v468hxfpahwa76p66hzff1lxa2") (yanked #t)))

(define-public crate-ini_lib-0.1 (crate (name "ini_lib") (vers "0.1.2") (hash "05l3s706v8k8xp9bn4fgp7kcxlh761qm3r7f9986j7nsambhi9id") (yanked #t)))

(define-public crate-ini_lib-0.1 (crate (name "ini_lib") (vers "0.1.3") (hash "0k1xbmkz6x02qx6690k38l2jdgbxbmgay7fn1fyp28i4azjd96db")))

(define-public crate-ini_lib-0.1 (crate (name "ini_lib") (vers "0.1.4") (hash "070sw8hlq5qnyx84xh1cfjac3i703xqrxwi5211izxzrvrnwi1zp")))

(define-public crate-ini_puga-0.1 (crate (name "ini_puga") (vers "0.1.0") (hash "17snjcwksiq8k5azi7v0xsi1pplgv2rn3p56lnlj7hn3pk8p0gyp") (yanked #t)))

(define-public crate-ini_puga-0.1 (crate (name "ini_puga") (vers "0.1.1") (hash "16dpjkk8dswyfd8p9vnj9p93c8m6n6n4mq6b2bps748b3849fkjv")))

(define-public crate-ini_puga-0.2 (crate (name "ini_puga") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1ac42v779kyjmwxw1vfqi7jia337jxvcr9scpfxq5xz28fcp3k87")))

(define-public crate-ini_puga-0.2 (crate (name "ini_puga") (vers "0.2.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1707i1020lv5qa8h7ckifq47l7275gai7mddm4x77ypfhccl3dqd")))

