(define-module (crates-io in tl) #:use-module (crates-io))

(define-public crate-intl-0.0.0 (crate (name "intl") (vers "0.0.0") (hash "0hyf8bhk28n8lfybry9v6sa9fqq4dzm0rvx13s9n073y1ay0qxlr")))

(define-public crate-intl-memoizer-0.2 (crate (name "intl-memoizer") (vers "0.2.0") (deps (list (crate-dep (name "fluent-langneg") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "intl_pluralrules") (req "^6.0") (default-features #t) (kind 2)) (crate-dep (name "type-map") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.8") (default-features #t) (kind 0)))) (hash "1yiclvbq87jz6k4h0gx4755kcv8lalp5fny7rcx3qvqczipilphv")))

(define-public crate-intl-memoizer-0.3 (crate (name "intl-memoizer") (vers "0.3.0") (deps (list (crate-dep (name "fluent-langneg") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "intl_pluralrules") (req "^6.0") (default-features #t) (kind 2)) (crate-dep (name "type-map") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.8") (default-features #t) (kind 0)))) (hash "038wkssvx362b05f3vdaz5cl5vjrmjrrid18yfsahmm975wkwlzz")))

(define-public crate-intl-memoizer-0.4 (crate (name "intl-memoizer") (vers "0.4.0") (deps (list (crate-dep (name "fluent-langneg") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "intl_pluralrules") (req "^6.0") (default-features #t) (kind 2)) (crate-dep (name "type-map") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.8") (default-features #t) (kind 0)))) (hash "07rr0p3n3j22h4apc1isx6247a9rns3hzv8p8brnx4w2bpbf4rwq")))

(define-public crate-intl-memoizer-0.5 (crate (name "intl-memoizer") (vers "0.5.0") (deps (list (crate-dep (name "fluent-langneg") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "intl_pluralrules") (req "^7.0") (default-features #t) (kind 2)) (crate-dep (name "type-map") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (default-features #t) (kind 0)))) (hash "02v3v52a4ps67l7jgvs0vyf03qmrr5py3md7m7w4k788ls5xa3la")))

(define-public crate-intl-memoizer-0.5 (crate (name "intl-memoizer") (vers "0.5.1") (deps (list (crate-dep (name "fluent-langneg") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "intl_pluralrules") (req "^7.0.1") (default-features #t) (kind 2)) (crate-dep (name "type-map") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (default-features #t) (kind 0)))) (hash "0vx6cji8ifw77zrgipwmvy1i3v43dcm58hwjxpb1h29i98z46463")))

(define-public crate-intl-memoizer-0.5 (crate (name "intl-memoizer") (vers "0.5.2") (deps (list (crate-dep (name "fluent-langneg") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "intl_pluralrules") (req "^7.0.1") (default-features #t) (kind 2)) (crate-dep (name "type-map") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (default-features #t) (kind 0)))) (hash "1nkvql7c7b76axv4g68di1p2m9bnxq1cbn6mlqcawf72zhhf08py")))

(define-public crate-intl-rs-1 (crate (name "intl-rs") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0swxfzfkqgjxb362wp8bpxb0rzwvacn08zfgsyrzyxh3pdryxh9r")))

(define-public crate-intl-rs-1 (crate (name "intl-rs") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "string_template") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00hbcjdjzgwmm8mr2xyy5nzr7wiz1z5hqdixjvi5jaac72mm2zjm")))

(define-public crate-intl-rs-1 (crate (name "intl-rs") (vers "1.2.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "string_template") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0bplfm9qn0y80c86jaagiiw7sq4xjf67snjxwyvpwhasf8pyh8dd")))

(define-public crate-intl-rs-2 (crate (name "intl-rs") (vers "2.0.0") (deps (list (crate-dep (name "accept-language") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "string_template") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "036sfilgjhnlnnk1p8arajp6kv9vfmd9m03kqz1zywfhy10pa96l")))

(define-public crate-intl_pluralrules-0.8 (crate (name "intl_pluralrules") (vers "0.8.0") (deps (list (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "087hl8vm26d86g5f1dy7m88f5zw48kkcj0x0ynvzx1hyrhg7rfif")))

(define-public crate-intl_pluralrules-0.8 (crate (name "intl_pluralrules") (vers "0.8.1") (deps (list (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1x0yjmnfxwhs33ma6vxpgi4xcm5prb3cr9k97hs7ny6mh5xl0am6")))

(define-public crate-intl_pluralrules-0.8 (crate (name "intl_pluralrules") (vers "0.8.2") (deps (list (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0np0l9a81vahknxzrv5w2f5psjs0hcszqnr3ykzns4acj5nrcqg2")))

(define-public crate-intl_pluralrules-0.9 (crate (name "intl_pluralrules") (vers "0.9.0") (deps (list (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0i0mam1lxdy2w71j5nd4fgfwz93p8hawkldxd6kw772wqxdq3pix")))

(define-public crate-intl_pluralrules-0.9 (crate (name "intl_pluralrules") (vers "0.9.1") (deps (list (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1kjgs5nwikrcc89qppbjzirkl174ml8jnskirh4ln2ff00y3zyf1")))

(define-public crate-intl_pluralrules-1 (crate (name "intl_pluralrules") (vers "1.0.0") (deps (list (crate-dep (name "matches") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.22") (default-features #t) (kind 0)))) (hash "0mp5n1iwdcv68vc72d79jd6mzxq52q56lyzf3wpfdb2gdfms90iv")))

(define-public crate-intl_pluralrules-1 (crate (name "intl_pluralrules") (vers "1.0.1") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 0)))) (hash "0z6d2xyppvx04ak0hy0zjcxhv0inv7r21q4d37hlzaxdx5xnzcdf")))

(define-public crate-intl_pluralrules-1 (crate (name "intl_pluralrules") (vers "1.0.2") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)))) (hash "1k1a8j7wrvqnxxnfxykv7zp7wjxdvl9hzfnyfmsn9hn0gxc1yg76")))

(define-public crate-intl_pluralrules-1 (crate (name "intl_pluralrules") (vers "1.0.3") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)))) (hash "1bhq3c9jf6ygys1rfyvisa2bj3phv6mas4jwrs8q33yk9j8czs24")))

(define-public crate-intl_pluralrules-1 (crate (name "intl_pluralrules") (vers "1.1.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "11l23sjaxzh7dr65hrnwb2bjlqp8x2dqjqmabqzflzs155jf5c2q") (yanked #t)))

(define-public crate-intl_pluralrules-2 (crate (name "intl_pluralrules") (vers "2.0.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.4") (default-features #t) (kind 0)))) (hash "0x3i13anz9jdfc2fj7bkp27xllaqlcv900b0j71gxc4cgdkbyk4g")))

(define-public crate-intl_pluralrules-3 (crate (name "intl_pluralrules") (vers "3.0.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jal659csddfqi4xs4qlmrnj0p8d0r9cs7ijvlvx4ykf7im5p943")))

(define-public crate-intl_pluralrules-4 (crate (name "intl_pluralrules") (vers "4.0.0") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.6") (default-features #t) (kind 0)))) (hash "03shwcijzk7gw0k5dnh70hj523qyb0c9g2fbjj8crkpigd31rqkb")))

(define-public crate-intl_pluralrules-4 (crate (name "intl_pluralrules") (vers "4.0.1") (deps (list (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.6") (default-features #t) (kind 0)))) (hash "1kpd0qzvfj0kq1crrch7x4fakr4bk29ai4cf20rb64pcmwqgskci")))

(define-public crate-intl_pluralrules-5 (crate (name "intl_pluralrules") (vers "5.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tinystr") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "0waq6g7qvxlwq7966pramn55rq4zkb6lbzq111pn9wh8m50j26m3")))

(define-public crate-intl_pluralrules-5 (crate (name "intl_pluralrules") (vers "5.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tinystr") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "12n727yjq9cifjq9wnsbn37f93n9vhbl5pjy7m0jpxnxy0zbirlv")))

(define-public crate-intl_pluralrules-5 (crate (name "intl_pluralrules") (vers "5.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tinystr") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "1gpi7bjbhajyqzd71k1591pfmzvqbjx87qr1g5nq6m05baicnbkm")))

(define-public crate-intl_pluralrules-6 (crate (name "intl_pluralrules") (vers "6.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tinystr") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "00jhm0a64v6a5nb4s7j0xwf1yyzrsfj8dkp0acrw0hnfxvc18b6q")))

(define-public crate-intl_pluralrules-7 (crate (name "intl_pluralrules") (vers "7.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tinystr") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "1hwbm7vzkqdbsr0p6xgnf3r73yc1wqz9qq8pl2rzxa8j3zdiq9vc")))

(define-public crate-intl_pluralrules-7 (crate (name "intl_pluralrules") (vers "7.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tinystr") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "1ksy3hxqs8if3nbvcin0a8390lpkzbk2br1brik70z96hj1ri3xi")))

(define-public crate-intl_pluralrules-7 (crate (name "intl_pluralrules") (vers "7.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unic-langid") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unic-langid") (req "^0.9") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "0wprd3h6h8nfj62d8xk71h178q7zfn3srxm787w4sawsqavsg3h7")))

