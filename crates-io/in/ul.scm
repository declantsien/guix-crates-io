(define-module (crates-io in ul) #:use-module (crates-io))

(define-public crate-inullify-0.1 (crate (name "inullify") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wx41jibgjq088bfaqrn3swdfpaa3f7prcr59n4a8r7zsdc7cjyw")))

(define-public crate-inullify-0.1 (crate (name "inullify") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1c0m3plpgpvvdlpy04bjmxkn3gah8ldhbh5r5ni04l6vsn5xp4v1")))

(define-public crate-inullify-0.1 (crate (name "inullify") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "09k0wwfk42kmp0fx9n5sxvphw5hixb76nhqnmqn14cppkgs6rglb")))

(define-public crate-inullify-0.1 (crate (name "inullify") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "01ckhscr7qk8v6cdhawid2i9xsa26ayd1vxk5pra5p297qwjac1b")))

(define-public crate-inullify-0.1 (crate (name "inullify") (vers "0.1.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sslh6c110sy3jhrlfylizng0fdf59r5mig9alill43kgn8dqpka")))

