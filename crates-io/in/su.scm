(define-module (crates-io in su) #:use-module (crates-io))

(define-public crate-insult-1 (crate (name "insult") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "06iyn1manm08xmy1yj4s7c230g4g4azpdaz3pmrvyipdsjy6c589")))

(define-public crate-insult-1 (crate (name "insult") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jcvfw7lvnrj8h1mvqyy65zfpxhjh76jkrbzw1pay3k3f39dj94f")))

(define-public crate-insult-1 (crate (name "insult") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0m84905cqj2hhifinzqg94yk3khqkjiyjnlg81yyil6kv6mcvv3n")))

(define-public crate-insult-1 (crate (name "insult") (vers "1.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "14kjhz43fwrmh36fiz11606nn2jwicb5hqskdfi1wys6laqp93d0")))

(define-public crate-insult-1 (crate (name "insult") (vers "1.1.3") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qja4b226d5qypg4z1y80lx2spcq0q2p9mj0yd5nrrb387y7nyaf")))

(define-public crate-insult-1 (crate (name "insult") (vers "1.1.4") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1f4582xgc22s5bj5pi3m3z3gmjisvbay923hm139v8ncrm18f3ih")))

(define-public crate-insult-2 (crate (name "insult") (vers "2.0.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "05zyqk0hlgi0g098mzaacyclkrimmpn84r5hd8brmwff30w3bhih")))

(define-public crate-insult-2 (crate (name "insult") (vers "2.0.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "02qpfrg9rjb1m4m8jz8zk0bpblcakwlfajwghj5say7gqaivzjll")))

(define-public crate-insult-2 (crate (name "insult") (vers "2.0.2") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0shwcqbmgl6yjxdrh97b256l75skr1knfj334a8nnqm3282x2xzz")))

(define-public crate-insult-2 (crate (name "insult") (vers "2.0.3") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1ngldv38fdcsj2xpcnh4js55hwp5f51mgzb1b3vf2kg5ydnzy7rh")))

