(define-module (crates-io in gl) #:use-module (crates-io))

(define-public crate-ingl_macros-0.1 (crate (name "ingl_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (default-features #t) (kind 0)))) (hash "03kx613rx33ddi58g5gj1hlxldk4nr5sgyx1z5zwwxp1q0pc1l9a")))

