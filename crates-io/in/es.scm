(define-module (crates-io in es) #:use-module (crates-io))

(define-public crate-ines-0.1 (crate (name "ines") (vers "0.1.0") (hash "1ynyvrpi7j1gh5l8jjs8mi2g057zqj1jnxbz99yc8yvccgg1195c")))

(define-public crate-ines-0.1 (crate (name "ines") (vers "0.1.1") (hash "0dc1sbnnplqqgmbv8pm3k3bil9c0x412x75lmj5xyfsqipgklknx")))

(define-public crate-ines-0.2 (crate (name "ines") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1zfgaj5g6mcgkir2cq66v1sqkafzam5cchl1mad02q3qpysyzfa7")))

(define-public crate-ines-0.3 (crate (name "ines") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pyaw0044gshbx535fk44raz7cc7lgf3pkndmzkp98ab58r8p6lj")))

