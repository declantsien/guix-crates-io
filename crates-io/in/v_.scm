(define-module (crates-io in v_) #:use-module (crates-io))

(define-public crate-inv_manager-0.2 (crate (name "inv_manager") (vers "0.2.0") (deps (list (crate-dep (name "derive_builder") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1775bkx9av3zwx5pzmw55vx99s71h9r8z5jndq88cm4ljv0434b8")))

