(define-module (crates-io in le) #:use-module (crates-io))

(define-public crate-inle_diagnostics-0.1 (crate (name "inle_diagnostics") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "14zhbbdfb912vzzwrlanc136vhp4mhmw5p4r5jkhmbzl0pcxmbsq") (features (quote (("tracer" "rayon") ("default" "tracer"))))))

(define-public crate-inle_diagnostics-0.1 (crate (name "inle_diagnostics") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1rnp0y1srm5w79ys1yc4188c8xyydq2invbn44s9x8dca3fvvw0m") (features (quote (("tracer" "rayon") ("default" "tracer"))))))

