(define-module (crates-io in oc) #:use-module (crates-io))

(define-public crate-inochi2d-0.1 (crate (name "inochi2d") (vers "0.1.0") (deps (list (crate-dep (name "glfw") (req "^0.47") (kind 0)) (crate-dep (name "glow") (req "^0.11.2") (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("tga"))) (kind 0)) (crate-dep (name "nom") (req "^7.1") (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 0)))) (hash "0j5wccd9mmq52p0g93mh7dcrvpky8ah7g2mlbhix5h366pdwcmxp")))

(define-public crate-inochi2d-0.1 (crate (name "inochi2d") (vers "0.1.1") (deps (list (crate-dep (name "glfw") (req "^0.47") (kind 0)) (crate-dep (name "glow") (req "^0.11.2") (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 0)))) (hash "0vmhspr9msvlyblxpvkfy4i7bdvza2n058jr711yyc33fs9cq8vy") (features (quote (("parallel") ("default" "parallel"))))))

(define-public crate-inochi2d-rs-0.1 (crate (name "inochi2d-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qdwcfhxdprj038bfyr9vcl8ywqcfcjqrimzcj7fc79v8gjz9cqq") (features (quote (("opengl") ("default")))) (yanked #t)))

