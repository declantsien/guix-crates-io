(define-module (crates-io in tp) #:use-module (crates-io))

(define-public crate-intpack-0.1 (crate (name "intpack") (vers "0.1.0") (hash "124ibwwiqkbm3m6xj9s713jsw51mick4x35ng7554kyanii83kfg")))

(define-public crate-intpackit-0.1 (crate (name "intpackit") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1nhwp7sncwqkffrz9z2kmq0hx7jf75bdgyklncqydxm72nzgyfwf")))

(define-public crate-intptr-0.1 (crate (name "intptr") (vers "0.1.0") (deps (list (crate-dep (name "dataview") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1hikjkila4ziv372jpl98vwph9d6hi76fj5ybpskcyq1zarcdnl6")))

(define-public crate-intptr-0.1 (crate (name "intptr") (vers "0.1.1") (deps (list (crate-dep (name "dataview") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "08qyr72kp4ijy9qwmfyd7k7xa5g9nhi826fw8s89087987d77551")))

(define-public crate-intptr-0.1 (crate (name "intptr") (vers "0.1.2") (deps (list (crate-dep (name "dataview") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1wcg9mz7yv13j1g02q694glk8kyqkb7pr1fvhf03b2ba7kf5wpmn") (features (quote (("nightly"))))))

(define-public crate-intptr-0.1 (crate (name "intptr") (vers "0.1.3") (deps (list (crate-dep (name "dataview") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0i8gf5sq905az2gcznh4zfw2q76vbi6yhgnlah96krk2sf617cgv") (features (quote (("nightly"))))))

(define-public crate-intptr-0.1 (crate (name "intptr") (vers "0.1.4") (deps (list (crate-dep (name "dataview") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1asgv4ms0vr92i6k373malzx7k0ykmw0q30wd7ysmf2vfv8ysjmv") (features (quote (("nightly"))))))

(define-public crate-intptr-0.1 (crate (name "intptr") (vers "0.1.5") (deps (list (crate-dep (name "dataview") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1m2r3hpnck265lr85w5p4az8rh82p95ny6lisgaihywwzyrn0vdf") (features (quote (("nightly") ("int2ptr"))))))

(define-public crate-intptr-0.1 (crate (name "intptr") (vers "0.1.6") (deps (list (crate-dep (name "dataview_0_1") (req "^0.1") (optional #t) (kind 0) (package "dataview")) (crate-dep (name "dataview_1") (req "^1.0") (optional #t) (kind 0) (package "dataview")) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0zg7527nvbyb2gq3rvhva39xd9h7wk66kks2zqw9lc4qa3c0aw18") (features (quote (("nightly") ("int2ptr") ("dataview" "dataview_0_1" "dataview_1"))))))

