(define-module (crates-io in se) #:use-module (crates-io))

(define-public crate-insert-0.1 (crate (name "insert") (vers "0.1.0") (hash "0l83wd7070qkccnyfbdm3gwv95bzdzvq7ps6p49p21z3x4532kbm")))

(define-public crate-insert-only-set-0.1 (crate (name "insert-only-set") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13m72b5k886i1sydslg1ipn5pdkgzaxf3di13gqj33ra14jkknxd")))

(define-public crate-insert-only-set-0.2 (crate (name "insert-only-set") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r3aq1vxl5bqpszsa0p766nam85rgsvd7zdllrwaj6fi7h37fwjj")))

(define-public crate-insert-only-set-0.3 (crate (name "insert-only-set") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hpnd3lc1147sxzjins3yrfipsnang7jgdsza02fi5lb7kpsfisd")))

(define-public crate-insert-only-set-0.3 (crate (name "insert-only-set") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0nln6qskks2k82ffqnwnsihvmxqdsnly607dnbi2b91jdvgb1v8k")))

(define-public crate-insert_many-0.1 (crate (name "insert_many") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0xg3zyc8qc10m3j4mr8y8fsfa63b1jypj1sr6zdaq4jdjnvpcl2v") (features (quote (("default" "smallvec"))))))

(define-public crate-insert_many-0.1 (crate (name "insert_many") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "09ih6qg2b5h6q73w3qd067kb6iiizq386n6r9lvc644q4ydw050m") (features (quote (("default" "smallvec"))))))

(define-public crate-insert_multiple-0.1 (crate (name "insert_multiple") (vers "0.1.0") (hash "0rrhxmqsbphvkmzsbh05gic2najz5i3d22r3w9iaxsjbpj1763qg")))

(define-public crate-insert_multiple-0.1 (crate (name "insert_multiple") (vers "0.1.1") (hash "08fpb7glwsw1k2f8krdgxv6p0b52n5ld6jwr3mf7pz4v8s45agb8")))

(define-public crate-insert_multiple-0.2 (crate (name "insert_multiple") (vers "0.2.0") (hash "079sq8vdy4lvkzhnfz98fbz593fys009px36h2jcbar9pr05vxhb")))

(define-public crate-insertfmt-1 (crate (name "insertfmt") (vers "1.0.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ll4877kn7ni8q9wy37a1izy3vivn58va2dh1dmxxab828q552rl")))

(define-public crate-insertfmt-1 (crate (name "insertfmt") (vers "1.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xg68snlkhcg9rlqwfs2f9q5psjl97m9nhyavgg93r1pmqnzzjkr")))

(define-public crate-insertfmt-1 (crate (name "insertfmt") (vers "1.0.2") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)))) (hash "14jhq4haj47h0gpvfgy1dkvvlq90wywi6baxbrgp6l5k4d3vfj6z")))

(define-public crate-insertfmt-1 (crate (name "insertfmt") (vers "1.0.3") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.33.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)))) (hash "0vzz458dpql5lf5qdyzbshyakm4w76r4gjxi190v2vjj9jji75x4")))

(define-public crate-insertfmt-1 (crate (name "insertfmt") (vers "1.0.4") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "0ywkjhqrgdiz8zqvl0xqdv336hcz0lxsg1xdkkm1fjyrhbpcji65")))

(define-public crate-insertion-sort-0.1 (crate (name "insertion-sort") (vers "0.1.0") (hash "1lcry5gq76zw8nv57m4r5qmsz3v0cwllw0h9n7k5kbv3yi4x7gqv")))

