(define-module (crates-io in so) #:use-module (crates-io))

(define-public crate-insomnia-0.1 (crate (name "insomnia") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "dbus") (req "^0.8") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "enumset") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("errhandlingapi" "impl-default" "handleapi" "winbase" "winnt"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "12djdkm3zhiygb0adkpb2x102xi71368ry6nsfpigakrp07z9p0p")))

(define-public crate-insomnia-0.1 (crate (name "insomnia") (vers "0.1.0-alpha2") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "wasmer_enumset") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("errhandlingapi" "impl-default" "handleapi" "minwinbase" "minwindef" "winbase" "winnt"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1af6fi1hdbjgbdi6sghas434p2vzw6snp3mqfn0xdsh06g8zd3w9")))

(define-public crate-insomnia-0.1 (crate (name "insomnia") (vers "0.1.0") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "enumset") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("errhandlingapi" "impl-default" "handleapi" "minwinbase" "minwindef" "winbase" "winnt"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1qn52wr93w0126rzvrkgq9bf0zkripjx8l9q93zhbs3z8crxr5zs")))

(define-public crate-insomnio-0.0.0 (crate (name "insomnio") (vers "0.0.0") (hash "15dlfk7xwvcv2xzy07v8xr80fcpkp0k0l0plqwaj7ck6h65xgqyd")))

