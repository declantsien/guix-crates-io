(define-module (crates-io vn cs) #:use-module (crates-io))

(define-public crate-vncserver-0.1 (crate (name "vncserver") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0gmw47klxdk77pn48vy9qlfnsi9q49348pryhrrc97nl1hlg8cg8")))

(define-public crate-vncserver-0.2 (crate (name "vncserver") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1763phdpv4wsh4721pd8x82mnbdxy065layf5r1c401hxs3xz6kr")))

(define-public crate-vncserver-0.2 (crate (name "vncserver") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0ds5psdp3rb6rbnlb78pjy5sf2gq81vkvw0dy7bijdc9krzwsnbr")))

(define-public crate-vncserver-0.2 (crate (name "vncserver") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "12pgpz06qghqx1lwgw3k38gznycczrpyrk7imkpj9w9b4kfjiraa")))

