(define-module (crates-io vn st) #:use-module (crates-io))

(define-public crate-vnstat_parse-0.1 (crate (name "vnstat_parse") (vers "0.1.0") (deps (list (crate-dep (name "miniserde") (req "^0.1.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1zzy695wz4zajvw8ig5h0jmc0kjs6y0300nsp2slb2wil9vl6cvm") (features (quote (("serde_support" "serde") ("miniserde_support" "miniserde") ("default"))))))

