(define-module (crates-io vn et) #:use-module (crates-io))

(define-public crate-vnetod-0.1 (crate (name "vnetod") (vers "0.1.0") (hash "019zk33yblx9a0qvjcm728x0mxzyfr6ifgs225zjdf6gbnrzrmdv")))

(define-public crate-vnetod-0.2 (crate (name "vnetod") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cg39cs3xs2cnda0vr8miz8v19bw48jqahx1jk1vz0kfil66l506")))

(define-public crate-vnetod-0.2 (crate (name "vnetod") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1icdd34aajabhw53zj9g065ryjiwcg6hj2ycr0mc8bh07d2hfxaf")))

(define-public crate-vnetod-0.2 (crate (name "vnetod") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ryrjly5hksc7vyk4c67ikyjcg8qbrks39w4db9f08ghc4h2gxbh")))

(define-public crate-vnetod-0.3 (crate (name "vnetod") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1p246frjbly6ff0dz8kzgwwiqa7c6jknjr3rf53f34lah04rzhk3")))

(define-public crate-vnetod-0.3 (crate (name "vnetod") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14mdwrsb02l439v148k0si4mspyy25qx2yql7wxdryig6r3g6skm")))

(define-public crate-vnetod-0.3 (crate (name "vnetod") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("std" "derive"))) (kind 0)))) (hash "0pbbif1yzgllwwvq5dhsyk9d8rfixr2d18s7p0npm8xd6yji766g") (features (quote (("color" "clap/color"))))))

(define-public crate-vnetod-0.3 (crate (name "vnetod") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("std" "derive"))) (kind 0)))) (hash "0nib77xs033144r0cm93zc22dyw8m66yywqrpdz9zliy1jpvkcry") (features (quote (("color" "clap/color"))))))

(define-public crate-vnetod-0.4 (crate (name "vnetod") (vers "0.4.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.15") (features (quote ("std" "env" "derive"))) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "04dz9kpypi523jncmv38pnss3bg240d3x3rjw9j2r03y1v984hss") (v 2) (features2 (quote (("color" "clap/color" "dep:atty" "dep:termcolor"))))))

