(define-module (crates-io vn d_) #:use-module (crates-io))

(define-public crate-vnd_siren-0.1 (crate (name "vnd_siren") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "08250k2xikvc62w6s5kfrmc2wmzbqjnzkj870v2yay4da4wdwn92")))

(define-public crate-vnd_siren-0.2 (crate (name "vnd_siren") (vers "0.2.0") (deps (list (crate-dep (name "http") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1233xqclrg3rh3rj1nh4lrjmf1nri8d5fmm7nxv7n9ay22jjawfa") (features (quote (("http-compat" "http") ("default" "http-compat"))))))

(define-public crate-vnd_siren-0.2 (crate (name "vnd_siren") (vers "0.2.1") (deps (list (crate-dep (name "http") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "07gm15iyb9i1k6xvv3zn153rhwzjhig5vnij8asjxy6vi5qzbprw") (features (quote (("http-compat" "http") ("default" "http-compat"))))))

