(define-module (crates-io zh i_) #:use-module (crates-io))

(define-public crate-zhi_enum-0.1 (crate (name "zhi_enum") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "zhi_enum_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r8jd5brrc2rh1a3ri7lhpxzvb40lswdwcjmya6v6zn27h06cnw9") (yanked #t)))

(define-public crate-zhi_enum-0.1 (crate (name "zhi_enum") (vers "0.1.1") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "zhi_enum_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00y9wf736imv0316jzrbhqsgw5rnhcmkl4xp0znd85pan2mai9jr")))

(define-public crate-zhi_enum-0.1 (crate (name "zhi_enum") (vers "0.1.2") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "zhi_enum_derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "08ywaxz6iqrkvadd5p6ql1gbl0hszwwicvpmi4nzbr19mhawvpq9")))

(define-public crate-zhi_enum_derive-0.1 (crate (name "zhi_enum_derive") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1jc8rnks1lh8b1j4d24x768acfb39k2bjcysp2iws60f1pdc84sv") (yanked #t)))

(define-public crate-zhi_enum_derive-0.1 (crate (name "zhi_enum_derive") (vers "0.1.1") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "127gss1f11gqhwp157yzsyrl7wpkks049560v6s09vhzfk9bjxjp")))

(define-public crate-zhi_enum_derive-0.1 (crate (name "zhi_enum_derive") (vers "0.1.2") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "13c98alnw7h0ljixfb4r03qg7xwjasha3dmd0sx0l8lc10hy63kr")))

