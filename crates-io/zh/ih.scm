(define-module (crates-io zh ih) #:use-module (crates-io))

(define-public crate-zhihu-link-0.0.0 (crate (name "zhihu-link") (vers "0.0.0") (deps (list (crate-dep (name "ego-tree") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "htmler") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.16") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1biwvrnkncaqag4qxzfd2aqg1ymihcbj3hfrafrjfpvg2495fxn7") (features (quote (("default"))))))

