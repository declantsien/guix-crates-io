(define-module (crates-io zh ou) #:use-module (crates-io))

(define-public crate-zhou-0.1 (crate (name "zhou") (vers "0.1.0") (hash "18cdsqwvwq8xrg4kwvmmwk19q9awv5z36hghykn5lkmv82641yv5") (yanked #t)))

(define-public crate-zhou-0.1 (crate (name "zhou") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ascii") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "httpdate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (features (quote ("std" "formatting" "macros" "parsing"))) (default-features #t) (kind 0)))) (hash "1qjqjijib7wnfyjg648m3xz41b1ibm40x1ngvavzd22f82gcrxv1") (yanked #t)))

(define-public crate-zhou-0.1 (crate (name "zhou") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ascii") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "httpdate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (features (quote ("std" "formatting" "macros" "parsing"))) (default-features #t) (kind 0)))) (hash "1f3vaa967wphvyjapvha97zksja8pcchfj5xf6m4d9x93ljx7xfb")))

(define-public crate-zhouchen-0.1 (crate (name "zhouchen") (vers "0.1.0") (hash "1pkkn07qs6sviix7xsgc9px27w0a44kcry9lf7pxg0k8c9bxbvvn")))

(define-public crate-zhouchen-0.1 (crate (name "zhouchen") (vers "0.1.1") (hash "0zr9nncpysiqa4ah0m0ypbscm619fcqjm20v5bzklllnm5snl7vq") (yanked #t)))

