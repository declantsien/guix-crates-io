(define-module (crates-io zh an) #:use-module (crates-io))

(define-public crate-zhang-0.0.0 (crate (name "zhang") (vers "0.0.0") (hash "1xfffv47ay3j5lbvbl21p1sca9fzib2xgxsknwsyf6nkn71m1f8h")))

(define-public crate-zhang_hilbert-0.1 (crate (name "zhang_hilbert") (vers "0.1.0") (deps (list (crate-dep (name "array") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (kind 0)) (crate-dep (name "sdl2") (req "^0.32.1") (default-features #t) (kind 2)))) (hash "0j2ns4qarvw8cms1vnlmv7w73xwh54flvm5q77731p1l2nrbvzjb")))

(define-public crate-zhang_hilbert-0.1 (crate (name "zhang_hilbert") (vers "0.1.1") (deps (list (crate-dep (name "array") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (kind 0)) (crate-dep (name "sdl2") (req "^0.32.1") (default-features #t) (kind 2)))) (hash "1hkbirs5j7bakp73awga8i7frfgcx1ihkmfbm9wzkjiyh1hgxnsw")))

(define-public crate-zhangvgrrs-0.1 (crate (name "zhangvgrrs") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1bqng95aldmw9ri64wzfx78wx1vh4kzkqgiqi4zdyyg0hfmkmvy5") (yanked #t)))

