(define-module (crates-io zh uk) #:use-module (crates-io))

(define-public crate-zhukoview-0.0.1 (crate (name "zhukoview") (vers "0.0.1") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "pingus") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)))) (hash "0gh1wrby94y8g8wpnn342a1vy8cbz422gmig2c5xlvn8fhckm8p5") (v 2) (features2 (quote (("rng" "dep:rand") ("png" "dep:pingus"))))))

