(define-module (crates-io ss ml) #:use-module (crates-io))

(define-public crate-ssml-0.0.1 (crate (name "ssml") (vers "0.0.1") (hash "1bxdifbjhggzaqdnxikynbffzrlwxw60j0w0m35i1b6kh1j0vkfp")))

(define-public crate-ssml-0.0.2 (crate (name "ssml") (vers "0.0.2") (hash "0xpgkpsv7smv53mky6y1v4hajdm0p9pii1n56m0ax58sj3hs17gj")))

(define-public crate-ssml-0.0.3 (crate (name "ssml") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nv73gkbm7pdam3kklbzhd4mbr1y9f8vb2c5r45m89mpmpdn0rjy")))

(define-public crate-ssml-0.0.4 (crate (name "ssml") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "10adldr94i5x61b0rzpf6mjn7n7r47cqccyw0ramii6b28ckh45b")))

(define-public crate-ssml-0.0.5 (crate (name "ssml") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0") (default-features #t) (kind 0)))) (hash "02jqq0rycqhrk6kbs33b4049b4s02s83vdjlh4sf76mmyhm8wcsc")))

(define-public crate-ssml-0.1 (crate (name "ssml") (vers "0.1.0") (deps (list (crate-dep (name "dyn-clone") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v7zbz1dfvgvw7rqnb2k03p1inhcwysi6yj0b2jvjzwhrksiqhyy")))

(define-public crate-ssml-parser-0.1 (crate (name "ssml-parser") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "async-trait") (req "^0.1.68") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "fake") (req "^2.7.0") (features (quote ("derive" "http"))) (default-features #t) (kind 2)) (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "mediatype") (req "^0.19.13") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0j8cacl2cbp89zm0sqw65xmdqp2xgwxpl0ksjxsg8gn1b65vxmds") (features (quote (("async" "async-trait"))))))

