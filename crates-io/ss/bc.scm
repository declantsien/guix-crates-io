(define-module (crates-io ss bc) #:use-module (crates-io))

(define-public crate-ssbc-0.1 (crate (name "ssbc") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)))) (hash "010a7yfrarpyiazz9vwadkhc657r44gnkc227v5wq9kxhyyfqd0w") (features (quote (("signedmagnitude_sub"))))))

