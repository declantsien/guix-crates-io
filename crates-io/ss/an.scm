(define-module (crates-io ss an) #:use-module (crates-io))

(define-public crate-ssandbox-0.1 (crate (name "ssandbox") (vers "0.1.0") (deps (list (crate-dep (name "caps") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "cgroups-rs") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libscmp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0gr70c5c3yf89fj0wwnpbxsjzhk7fbbrci3020i9fkiqqh04rdqj")))

