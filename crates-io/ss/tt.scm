(define-module (crates-io ss tt) #:use-module (crates-io))

(define-public crate-SSTT-0.1 (crate (name "SSTT") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1iscca58aijcxbxrc5qsag33h2av4mg6anwhj88w91hk2438jrqm") (yanked #t)))

(define-public crate-ssttt-0.1 (crate (name "ssttt") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ai0f1yzn7ipzd8aava3gb7hxcl5z1xkfsymh7ksj28p6psvr4s4")))

