(define-module (crates-io ss hr) #:use-module (crates-io))

(define-public crate-sshrs-0.1 (crate (name "sshrs") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1m44r60giq8yhp0g68qznaif6k1b83pv2ibdy20l5icywykc5g8k")))

