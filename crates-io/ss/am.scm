(define-module (crates-io ss am) #:use-module (crates-io))

(define-public crate-ssam-0.1 (crate (name "ssam") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "039hzci3mxdr3adx1bni114qm6kmqws6l62fxmxympkprxxvw889")))

(define-public crate-ssam-0.1 (crate (name "ssam") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "17k5ysmvjn8kq5jfvca3whzy4g3bq4iw5r6g26rzq7iqqbnadxfn")))

(define-public crate-ssam-0.1 (crate (name "ssam") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0sngklcc0n7d1287cd0kjk51mk5lix10nryzfqhmwmfcfnjz01q5")))

(define-public crate-ssam-0.2 (crate (name "ssam") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0dvqvn1fy6r08k7jg2k4vybac3v0p83ljclwb5s53zivp5bam8x1")))

