(define-module (crates-io ss db) #:use-module (crates-io))

(define-public crate-ssdbench-0.1 (crate (name "ssdbench") (vers "0.1.0") (deps (list (crate-dep (name "byte-unit") (req "^4.0.18") (default-features #t) (kind 0)) (crate-dep (name "fstd") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ba5l8dpfvj35v437vlydgr31g7bd6m2yw28gjm9cx19abgddqil")))

