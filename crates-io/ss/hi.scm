(define-module (crates-io ss hi) #:use-module (crates-io))

(define-public crate-sshish-0.1 (crate (name "sshish") (vers "0.1.0") (deps (list (crate-dep (name "ssh2") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0sc895clq2gaffpbba5v7ypi69qvql321v2sb89d9ynmqr9yxqb4")))

(define-public crate-sshish-0.1 (crate (name "sshish") (vers "0.1.1") (deps (list (crate-dep (name "shell-escape") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1y9752w6fsdh80jys7sjz9vfmsmcb2fzs9cihicx55j5k9spmjs7")))

(define-public crate-sshive-0.1 (crate (name "sshive") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1m9isi0garxc1xahzs4n74qrrr62ci9c08wnv353fb510pdx505v") (yanked #t)))

(define-public crate-sshive-0.1 (crate (name "sshive") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1d4l16xi06nm2bl50kg1x4hxx35zdbjvakxx4kvjr2qybpy1c0a2") (yanked #t)))

(define-public crate-sshive-0.1 (crate (name "sshive") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1alfi16iwmxfnlj7hyysa6z4nzaw8lx5y96cvwjvqdy4kr0krrhj") (yanked #t)))

(define-public crate-sshive-0.1 (crate (name "sshive") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1g02jd8r0hq6n7j4ybmypw0g7y5l85kc631rqqssx4jwps92h8h1") (yanked #t)))

