(define-module (crates-io ss sf) #:use-module (crates-io))

(define-public crate-sssf-0.1 (crate (name "sssf") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0g80xd325iw2p70aax75m30qgfdl4kw0l5klnq308vmhp0zqprn9")))

