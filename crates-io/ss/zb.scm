(define-module (crates-io ss zb) #:use-module (crates-io))

(define-public crate-sszb-0.4 (crate (name "sszb") (vers "0.4.0") (deps (list (crate-dep (name "ethereum-types") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "sszb_derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1qihrkjc06d73qil0rmn0faqx16gprdh7fq31cdkp6m5sv6zpfk2") (features (quote (("arbitrary" "ethereum-types/arbitrary"))))))

(define-public crate-sszb_derive-0.3 (crate (name "sszb_derive") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.42") (default-features #t) (kind 0)))) (hash "0md4c541nqgqd46kji8972qr70dmgzhwrrllbbm731666pzzgnfy")))

