(define-module (crates-io ss sd) #:use-module (crates-io))

(define-public crate-sssd-1 (crate (name "sssd") (vers "1.0.0") (deps (list (crate-dep (name "sysinfo") (req "^0.29") (default-features #t) (kind 0)))) (hash "1ck7mcy1gb1wdim262gxq7bdfcv8nv9jg1ldz4f3c4j3c1c63m7l")))

(define-public crate-sssd-1 (crate (name "sssd") (vers "1.0.1") (deps (list (crate-dep (name "sysinfo") (req "^0.29") (default-features #t) (kind 0)))) (hash "07m9gyagjcjn90618lfvh345xyjj4m09f5fhqbr773a5bl5w5q27")))

