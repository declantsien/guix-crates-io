(define-module (crates-io ss -t) #:use-module (crates-io))

(define-public crate-ss-trees-0.1 (crate (name "ss-trees") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ss-web-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14a7v1l13sy6p6wp6bvfwdm4nxxk4izpaqp03iaqcfqwsjqyrxv1") (yanked #t)))

(define-public crate-ss-trees-0.1 (crate (name "ss-trees") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ss-web-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00igik7y49x19la22vksn2lw56bmf961spih8csvh71mx7pnxgfp") (yanked #t)))

