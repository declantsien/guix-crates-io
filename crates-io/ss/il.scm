(define-module (crates-io ss il) #:use-module (crates-io))

(define-public crate-ssilide-0.1 (crate (name "ssilide") (vers "0.1.0") (deps (list (crate-dep (name "bevy_ecs") (req "^0.11.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bevy_ecs_macros") (req "^0.11.2") (optional #t) (default-features #t) (kind 0)))) (hash "0x0bkj4n2g1nbis5xli5q1khr328v19n78n594hn437kfszj0f75") (features (quote (("bevy" "bevy_ecs" "bevy_ecs_macros"))))))

(define-public crate-ssilide-0.2 (crate (name "ssilide") (vers "0.2.0") (hash "10ibmbq1yn1vcxz6fmw8b0hm5llz1ma4jmpxw18p3v1irg765z1a")))

