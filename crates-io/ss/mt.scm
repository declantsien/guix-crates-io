(define-module (crates-io ss mt) #:use-module (crates-io))

(define-public crate-ssmtp-1 (crate (name "ssmtp") (vers "1.0.0") (hash "0scvmg5vmmkgw436i8h0p3g8x4159knjcsmg03fpv84w7rn01y0i") (yanked #t)))

(define-public crate-ssmtp-1 (crate (name "ssmtp") (vers "1.0.1") (hash "1pqcm3x76r9hh4qm6f1hvrk1nxvky2w5i7rnminmaq3znj5d9sqw")))

