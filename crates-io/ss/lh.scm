(define-module (crates-io ss lh) #:use-module (crates-io))

(define-public crate-sslhash-0.1 (crate (name "sslhash") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0jjbwdda1rn3dbf8f94vlvv57dchbbsha7pif9csn42dwjz6kvrw")))

(define-public crate-sslhash-0.1 (crate (name "sslhash") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1z4wvbd650z79wi6m6x59p1ig5a6k25cm2aak1bskbzp4x9ry35w")))

