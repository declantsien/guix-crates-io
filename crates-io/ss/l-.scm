(define-module (crates-io ss l-) #:use-module (crates-io))

(define-public crate-ssl-expiration-0.1 (crate (name "ssl-expiration") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9") (default-features #t) (kind 0)))) (hash "0y31bf4ikypfwsk8bxqmqzpgczxn1jkc1mspgx0sgiakwr753inj")))

(define-public crate-ssl-expiration-0.1 (crate (name "ssl-expiration") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9") (default-features #t) (kind 0)))) (hash "0iz7h2riqyw6c60vi62jlvnnf97mnppan59my9z428hsf5bdp12z")))

(define-public crate-ssl-expiration-0.1 (crate (name "ssl-expiration") (vers "0.1.2") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "foreign-types-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9") (default-features #t) (kind 0)))) (hash "122z18fvdyvmipdmc7dlsq01y85hchdhwxy2cl7kkba1yxwlm2rc")))

(define-public crate-ssl-expiration2-0.2 (crate (name "ssl-expiration2") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.30") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9.58") (default-features #t) (kind 0)))) (hash "03vvjx53l7c8vx7dxbxzqf0hjg0wxh0c0v08l09xgqdz2l0ws2dp")))

(define-public crate-ssl-expiration2-0.2 (crate (name "ssl-expiration2") (vers "0.2.1") (deps (list (crate-dep (name "error-chain") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.30") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9.58") (default-features #t) (kind 0)))) (hash "0379z89smlr7s9liqy9k82ys1m87b46wpa6hizsl3f1byvswqil6")))

(define-public crate-ssl-expiration2-0.2 (crate (name "ssl-expiration2") (vers "0.2.2") (deps (list (crate-dep (name "error-chain") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.30") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9.58") (default-features #t) (kind 0)))) (hash "0gszh1i6fnsmkcdfpw95dg571gss8xz6vd1y2lv8pljjjajm65a4")))

(define-public crate-ssl-expiration2-0.2 (crate (name "ssl-expiration2") (vers "0.2.3") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.32") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9.60") (default-features #t) (kind 0)))) (hash "0db84g285a6csrssgxv6w7xqg5s72zi4rsmkv95rg61rjvzbxdip")))

(define-public crate-ssl-expiration2-0.3 (crate (name "ssl-expiration2") (vers "0.3.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.32") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9.60") (default-features #t) (kind 0)))) (hash "0mbckpm1gqn11s57lrbj3fc8y3rz06zcxzagi23bzwq947aqa38d")))

(define-public crate-ssl-expiration2-0.4 (crate (name "ssl-expiration2") (vers "0.4.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.34") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9.63") (default-features #t) (kind 0)))) (hash "1gg4ihw5mv651496ddhfz1q4gx4nbggxvixmi3v8mbrz48hrba72")))

