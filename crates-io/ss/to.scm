(define-module (crates-io ss to) #:use-module (crates-io))

(define-public crate-ssto-0.1 (crate (name "ssto") (vers "0.1.0") (hash "0psmmd1rzi640xpql0gz2m6c94zhhgn08a7pbm5r0j7w09rfwj2p")))

(define-public crate-sstop-0.1 (crate (name "sstop") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3.6.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0m2a858130a2zns6lcf3byy5wyqiksvvzb70n7qw84br4h50fp5k")))

(define-public crate-sstop-0.1 (crate (name "sstop") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3.6.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0rfgjl0x21mlcpi6l21bdf8spq4p0iajm95x0b9rc07n0ly8k1ki")))

(define-public crate-sstop-0.1 (crate (name "sstop") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3.6.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "03xpxbs8kh6njrynl53110kz3d4v6jrrf9rl2hflvb8pl5jkqkd7")))

(define-public crate-sstop-0.1 (crate (name "sstop") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3.6.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "1lq7sifqbkb0amhzidi3glwmilqscr2frj69jqrylbscsq1jygfn")))

(define-public crate-sstop-0.1 (crate (name "sstop") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3.6.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "10n0665656d8xs7zj7m5lbalf3v9vcihvwixc4d2dlqmkdz1ydj7")))

(define-public crate-sstop-0.1 (crate (name "sstop") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3.6.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "19amgjd35x7xjr2sk24lnc2w9kfvy72l2h071y0zpj96wfzsaiws")))

(define-public crate-sstorrent-0.1 (crate (name "sstorrent") (vers "0.1.0") (hash "141jmrajkbfkc2sk3rbxak70a185pqsfj1da6cmd40pnhqz4mwb7")))

