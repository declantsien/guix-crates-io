(define-module (crates-io ss ag) #:use-module (crates-io))

(define-public crate-ssage-0.1 (crate (name "ssage") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "iter_tools") (req "^0.1") (kind 0)) (crate-dep (name "priority-queue") (req "^1.2") (kind 0)) (crate-dep (name "unicase") (req "^2.6") (kind 0)))) (hash "06ym934ygmap59h6gwpgg5hkmkh8m4laqhqbpbfsy2hn4dmv6x77")))

