(define-module (crates-io ss fs) #:use-module (crates-io))

(define-public crate-ssfs-0.1 (crate (name "ssfs") (vers "0.1.0") (deps (list (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.3.1") (features (quote ("rustls"))) (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "1rrw5rrg75ayb46yi0gjx94bdk7qvssnikjd1x7fbxrrb2kwdziz")))

