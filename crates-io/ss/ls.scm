(define-module (crates-io ss ls) #:use-module (crates-io))

(define-public crate-sslscan-1 (crate (name "sslscan") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "19fihq0xm0xgf2s9xcvz78bsvhi6dbncv1aakwlzw6147nzb4rf0")))

(define-public crate-sslsert-0.0.1 (crate (name "sslsert") (vers "0.0.1") (hash "0im0nwl0sjpc00s7vlyjfhrvax5al9bawy1vf79p9cmvirj71f3b")))

