(define-module (crates-io ss ed) #:use-module (crates-io))

(define-public crate-ssedit-0.1 (crate (name "ssedit") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1mzkw0c9xwpsd52h61vc2fj3xz103lf2pal71vrwliby508shvnf")))

(define-public crate-ssedit-0.2 (crate (name "ssedit") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0xnxc7j3grk5aznpbnxjq236g0zl1fxqayjv3bxyiaql3d419w1k")))

