(define-module (crates-io ss lr) #:use-module (crates-io))

(define-public crate-sslrelay-0.4 (crate (name "sslrelay") (vers "0.4.2") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0xzsyljxy5w25kam6nyn3zcq5y9wq1b4my8xdn1bg0mp08bbc7as")))

(define-public crate-sslrelay-0.4 (crate (name "sslrelay") (vers "0.4.3") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "03bbbxpqgqvrvkhjqw8yiqgc5mwqsmgdg1bmx9blbqwa73sn4k7v")))

(define-public crate-sslrelay-0.4 (crate (name "sslrelay") (vers "0.4.31") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1l83clc3c8706kzb8wm3393jzzjl0qp3155nb9fnql40bw5nlljz")))

(define-public crate-sslrelay-0.4 (crate (name "sslrelay") (vers "0.4.4") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0lqpv3k47jxac5jpwdq207kvvzialyy0kxgcn0i1i72sfxah5hd6")))

(define-public crate-sslrelay-0.5 (crate (name "sslrelay") (vers "0.5.0") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "14la3sr3wmk42qj432axd9smshyllhb3gmq8ihkpyr5mnxn73nm9")))

(define-public crate-sslrelay-0.6 (crate (name "sslrelay") (vers "0.6.0") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)))) (hash "1lrpcdlk532qssixh4f4gdjhfqr4bw0m3n5mkrxnq1phrkx2hb5l")))

(define-public crate-sslrelay-0.6 (crate (name "sslrelay") (vers "0.6.1") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)))) (hash "1za7yqf843la4bb4bl7qd5dn45sd04ib2mdz47zk2mfbv932nkxg")))

(define-public crate-sslrelay-0.6 (crate (name "sslrelay") (vers "0.6.2") (deps (list (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)))) (hash "02can66fmpr5z0cc3xprinjwy6b2dsd7hd8m1smqa6sgwsm7ypcl")))

