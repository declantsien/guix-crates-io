(define-module (crates-io ss cc) #:use-module (crates-io))

(define-public crate-ssccpp-0.1 (crate (name "ssccpp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jpqrydxjayhzphavcd67ajs5ggyn1all66ivkrqycgs3cp993hn")))

