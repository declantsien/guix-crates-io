(define-module (crates-io ss ss) #:use-module (crates-io))

(define-public crate-ssss-0.1 (crate (name "ssss") (vers "0.1.0") (deps (list (crate-dep (name "getset") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "169ydziqri6g86q6q5y7mz724z8fz33binjnzjzia07adpb9vccv")))

(define-public crate-ssss-0.1 (crate (name "ssss") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.12") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1bxlg4pkqvb71y1j67y5nh55hprd0af20cm6b4cq8c0bjcsb62y8") (features (quote (("unstable") ("fuzz" "arbitrary"))))))

(define-public crate-ssss-0.2 (crate (name "ssss") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.12") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "11l40z009ysxc0bl14g8aclwnddcf1az1s4dr14ywdq572apk3yw") (features (quote (("unstable") ("fuzz" "arbitrary"))))))

(define-public crate-sssstd-0.1 (crate (name "sssstd") (vers "0.1.0") (hash "02lzr8vrlj9jpmij5yviffi3kjizpmv6rpan0kvnn15n8jmm868p")))

(define-public crate-sssstd-0.1 (crate (name "sssstd") (vers "0.1.1") (hash "1khhgk7sgrp0yp2gx7567xx11in9f5v138w2lj8dy97vf5gqyzlj")))

(define-public crate-sssstd-0.1 (crate (name "sssstd") (vers "0.1.2") (hash "1xkdy01abazxfcbw2sygw45bb1rmqbfpli9948x6asmn2lh0bz4y")))

