(define-module (crates-io ss -c) #:use-module (crates-io))

(define-public crate-ss-css-types-0.1 (crate (name "ss-css-types") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03gpklq5jqj0bgb7j4kcz8h6y87mlr95491355djq46r9jy7a3an") (yanked #t)))

(define-public crate-ss-css-types-0.1 (crate (name "ss-css-types") (vers "0.1.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00j7fqq1qinx8j70q6k317zpil1hfpaaxa3x771v7nk1krsl2xgh") (yanked #t)))

(define-public crate-ss-cssom-tree-0.1 (crate (name "ss-cssom-tree") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "ss-css-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x7q7lhf037qglnpdn7fpmb0v1hxxc159775llfw8bk3wmvkv277") (yanked #t)))

(define-public crate-ss-cssom-tree-0.1 (crate (name "ss-cssom-tree") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "ss-css-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03qk54jvhg2mk9sqviz87bdvvkpgzbrgn09296wxwzx6rl4n962p") (yanked #t)))

