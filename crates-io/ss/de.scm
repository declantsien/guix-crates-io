(define-module (crates-io ss de) #:use-module (crates-io))

(define-public crate-ssdeep-0.1 (crate (name "ssdeep") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "libfuzzy-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09jyzhfc167d7h7vdwmadm8s93jy15nfpl7kcgjmw7w2c0jcz489")))

(define-public crate-ssdeep-0.2 (crate (name "ssdeep") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "libfuzzy-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16arrd350llfv66k7wpgsnxp4n2f3i75zxln9paip89dsl9337vv")))

(define-public crate-ssdeep-0.3 (crate (name "ssdeep") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libfuzzy-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0igd8yak3wr3fdxfsblw77rpa8zs19n8c3xqg2k8z9xbaccla60d")))

(define-public crate-ssdeep-0.4 (crate (name "ssdeep") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libfuzzy-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1h33iiyyhhpgzpw40pdfhjx7wiyis57nwhsmnm1k31vzc08qjb4p")))

(define-public crate-ssdeep-0.5 (crate (name "ssdeep") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libfuzzy-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0p4jsxcy03wih738hwizjss6gi41i0xh28fqxyyschm66c4k65qp")))

(define-public crate-ssdeep-0.6 (crate (name "ssdeep") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libfuzzy-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0ll7k3hd85akxxp8y70rmjrjlxxh5yj6pbw6dl0q7dmaaz9jj45w")))

