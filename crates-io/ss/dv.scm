(define-module (crates-io ss dv) #:use-module (crates-io))

(define-public crate-ssdv-fec-0.1 (crate (name "ssdv-fec") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ssdv-fec-gf-tables") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0rhaigds1xcrx2wpn712p254bb6flxhdz3sy5yd1sa0f3zhq4ymn") (features (quote (("std" "thiserror") ("default" "cli" "std") ("cli" "anyhow" "clap" "std"))))))

(define-public crate-ssdv-fec-gf-tables-0.1 (crate (name "ssdv-fec-gf-tables") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h84k7ra4yrwzm2lki7vm3g3i91f4rai0vlc433qaafbj570hvbg")))

