(define-module (crates-io ss -u) #:use-module (crates-io))

(define-public crate-ss-uri-0.1 (crate (name "ss-uri") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1q3jjky57887am4kl9pq9bc7fcav55zpy72frzii2q01wg3ayqmv")))

(define-public crate-ss-uri-0.2 (crate (name "ss-uri") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0y8ln7xbi4aqjscg6zkpy6r7h72ckxqbl9pj4lvrijanmqry81ni")))

(define-public crate-ss-uri-0.3 (crate (name "ss-uri") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "13h5fbkybinypqi5v6cgmkhrykgsh3ln9zaw6nr9cdmrr0qd87f8")))

(define-public crate-ss-uri-cli-0.1 (crate (name "ss-uri-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.135") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "ss-uri") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "11bj2i5cn13ky6lkvxmfisl82bmrkjgynawd4lj2c8zmjbpdh0mm")))

