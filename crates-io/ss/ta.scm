(define-module (crates-io ss ta) #:use-module (crates-io))

(define-public crate-sstable-0.1 (crate (name "sstable") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nr9jnr6v9rh9nvja44fcxdbxmq76nd00gawk91vjpgvqxv94pjh")))

(define-public crate-sstable-0.2 (crate (name "sstable") (vers "0.2.0") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pm0dvr8q9ji2szgapix2ziqw1bkbj0kfixl441q660kpnnm718v")))

(define-public crate-sstable-0.2 (crate (name "sstable") (vers "0.2.1") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pr7p9zvb12079ir2zzw2cs2zp4sd9s6x5rbny1yqphnd5ayi90m")))

(define-public crate-sstable-0.3 (crate (name "sstable") (vers "0.3.0") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g5p6k9p2bqlqbn3h5rc3fraasagg5z31jv3xxq91c2qjla5jn9x")))

(define-public crate-sstable-0.3 (crate (name "sstable") (vers "0.3.1") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yhiawa6cfvmbl3yvj7jmibakfddx5pr7yv8393dm1zcvhs0fkgd")))

(define-public crate-sstable-0.3 (crate (name "sstable") (vers "0.3.2") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nz7jk2m4hvmflmy6x3qcnm56q4ha8vhw89gx030y10a2xja8kqh")))

(define-public crate-sstable-0.4 (crate (name "sstable") (vers "0.4.0") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q0cl3rvnsf0v355xr0wwk8d8zjgfvh8b9k7v5g67xzkckbg7xii")))

(define-public crate-sstable-0.4 (crate (name "sstable") (vers "0.4.1") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zd4n5rjscxmppdx7xarjgi2f9sw8arn59lrwvdzlqdqjhvrw19x")))

(define-public crate-sstable-0.4 (crate (name "sstable") (vers "0.4.2") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1a47gflnn1x5yxhbfmf0kibn7b4xyhhmh8lpc371n2wgfv5xi2s9")))

(define-public crate-sstable-0.4 (crate (name "sstable") (vers "0.4.3") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lb8hyxqmd14r8j344bkd1biznl5hl8bmsx1y2ij0yxvc91rdygi")))

(define-public crate-sstable-0.4 (crate (name "sstable") (vers "0.4.4") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zjnapdfh3i6fw9lajbmmpay246xn80ykym7ivfkdwx1m9gx5wka")))

(define-public crate-sstable-0.5 (crate (name "sstable") (vers "0.5.0") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "1fihf546y0q847lyvl9ksn1a699z1whfpi2ak5cy6d2i8cimpc1w")))

(define-public crate-sstable-0.5 (crate (name "sstable") (vers "0.5.1") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "1rdjzxx57gwks4bv2l0k5lri1kzkkkkwdz1hyg4k9rm1n5awwl1x")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.0") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0j0ffibpdasbbanv946lj3a8wwh6xycza174gf2610ld8ilxf5mg")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.1") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0wklaq28ycmya23k6jix2pzaxd8hk4vlgd02r8rf5gmhkm2ix9wj")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.2") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0x3d4001hbm2waqw7zfgy7p50qpqq2ybwkx1s20xcav1cx1dhsjd")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.3") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "1i9yxccm8dsi3q42g2lnxs16z64czdyi02msz44y4yp5hxgg2jgz")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.4") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "1kvf0gcjs1hwqiijs0cn9jqz98wfdgjyk7w8miv3j416c0swybq0")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.5") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0q1z8dljychiy6amxhvc96613sl75c5v0z2ailk6x3jcnmgxmghd")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.6") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "03616aylp7hp9vnsbm35xcx49ica82lf2l0y4xx449jjxyrrj6i6")))

(define-public crate-sstable-0.6 (crate (name "sstable") (vers "0.6.7") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0g644gcziazsbnlqby62yf0a169byssa1s1pd10jlahnbzi19j3g")))

(define-public crate-sstable-0.7 (crate (name "sstable") (vers "0.7.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0xx75j57frdahq3084ddap0wxgnqxav11ia72rf1wd3vdyrf6csl")))

(define-public crate-sstable-0.8 (crate (name "sstable") (vers "0.8.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "155f26qy2gv9wnac6cqmsfzdd9bd3b9lkkrnqx2zc4yrkm313b2x")))

(define-public crate-sstable-0.8 (crate (name "sstable") (vers "0.8.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "1kfzmh4bsp9dwg08p7c8y37da1srii758a19c0dv03cwpchzza8x")))

(define-public crate-sstable-0.8 (crate (name "sstable") (vers "0.8.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "1lrxwl6pp1j643zdyhnmjaxq62xb580q8czwy0i7aljycfvicgw5")))

(define-public crate-sstable-0.9 (crate (name "sstable") (vers "0.9.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "17ksh4q8dmkz06wwpiszp60wzp78b3l48p0hksc159fmn237snnx")))

(define-public crate-sstable-0.10 (crate (name "sstable") (vers "0.10.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0wb4qs7bab3nhfywnlqiin12fxn4wxfslsngbhj3ndq7zcj4s9zj")))

(define-public crate-sstable-0.11 (crate (name "sstable") (vers "0.11.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0a2r4glzrsygd5y8yhapny9mlpysg12vfh4zfldbm2v05xx7apf7")))

(define-public crate-sstable-0.11 (crate (name "sstable") (vers "0.11.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "integer-encoding") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time-test") (req "^0.2") (default-features #t) (kind 2)))) (hash "0pp0n1sn1ik7saq10ngf7zbk7nkg2x3dyj85nh2v3h1z014zagvy")))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.0") (hash "1g08g0fv6w2lzgwhcipcsldg5129czw823k0gakfs0aq070hvi8g") (yanked #t)))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1nzxz3gz046aq296n13n30vsgnnq19avrvk248arz7dmxqijbfcz") (yanked #t)))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1mz6w2nplhbh6w1010p5f5lmqy2bklg3pa2pswcr3l12rlc5azs8")))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1xqlajjinl1b8ckwjp8v9dgm7q7wqchka6wby8d8ksanmkmgs5sh")))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "13xn5hpfi8w7g0d6xphd93bv98yxf7g7lbjj417jh6amjnlmp7jn")))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "shaderc") (req "^0.8") (default-features #t) (kind 1)))) (hash "1527malfkz3nm9rncbdzmnl84lwhn68z6qvxf4jlik2bjxp0z1rd") (features (quote (("with-default-shaders") ("default")))) (yanked #t)))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0vanyq9af1rf9klx9b3cv8bpq7617pm1jw77bjg5alcgsjk07wd3") (features (quote (("with-default-shaders") ("default"))))))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0zv0dp1vv4l9hbn54j73zah3bm773ikwrcfp2zf5vljnw4pa1bq3") (features (quote (("with-default-shaders") ("default"))))))

(define-public crate-sstar-0.1 (crate (name "sstar") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "12za6wxqfsznzvqqfgdsw2ywr8ff063p2kdl0i4jzk4l8lpyfh8p") (features (quote (("with-default-shaders") ("default"))))))

