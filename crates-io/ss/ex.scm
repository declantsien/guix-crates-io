(define-module (crates-io ss ex) #:use-module (crates-io))

(define-public crate-ssexp-0.1 (crate (name "ssexp") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "14klbzvwx0i85drg9090ph3xg1wpdf9f7i5yjp49z3mswzhwvkpz")))

(define-public crate-ssexp-0.1 (crate (name "ssexp") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "1rr5xr6i6y69hbnq180f50kyw3bfkrfqh6mqi3xyqlxzkc5vgr2k")))

(define-public crate-ssexp-0.1 (crate (name "ssexp") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "1amlb0vri7jxxqf2h5nhb9n3mr4g9l2h5434qcjx2vylm47i47yg")))

(define-public crate-ssexp-0.1 (crate (name "ssexp") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "141rh2c9syjc8qg2617jgps4vd1xsnvlbp8v0n2n5vi8b5admkz1")))

(define-public crate-ssexp-0.3 (crate (name "ssexp") (vers "0.3.0") (deps (list (crate-dep (name "derive_destructure") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "0rfqn7nwxg8fzcfnx13cl2f2jlnl2q3gwdishswbb3ip7hiqrg7v")))

(define-public crate-ssexp-0.3 (crate (name "ssexp") (vers "0.3.1") (deps (list (crate-dep (name "derive_destructure") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 2)))) (hash "0f4cd343jjz970y6wjsb6qzpdaf1hy9l73bzlmgzl5q7lbl9fa2f")))

