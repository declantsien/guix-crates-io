(define-module (crates-io ss _e) #:use-module (crates-io))

(define-public crate-ss_ewasm_api-0.11 (crate (name "ss_ewasm_api") (vers "0.11.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "qimalloc") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4.4") (optional #t) (default-features #t) (kind 0)))) (hash "1wfprdqvlkhiaafy9xaa64va4ibkpfsxf9r1nwrw7va6py16r3ar") (features (quote (("std") ("experimental") ("eth2") ("default" "std" "wee_alloc") ("debug"))))))

