(define-module (crates-io ss vm) #:use-module (crates-io))

(define-public crate-ssvm-evmc-client-7 (crate (name "ssvm-evmc-client") (vers "7.4.0") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "evmc-sys") (req "^7.4.0") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "evmc-vm") (req "^7.4.0") (default-features #t) (kind 0) (package "ssvm-evmc-vm")) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)))) (hash "0y9460dwrqa9h2djhynl0i4khy91bhj3x5ydvffzy5f89n5dz9gn") (yanked #t)))

(define-public crate-ssvm-evmc-client-7 (crate (name "ssvm-evmc-client") (vers "7.4.1") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "evmc-sys") (req "^7.4.0") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "evmc-vm") (req "^7.4.0") (default-features #t) (kind 0) (package "ssvm-evmc-vm")) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)))) (hash "0sn65jvcq7b6dkaan6gf047r8a0a7yyhmykqpl0asfwcg1wylxpd") (yanked #t)))

(define-public crate-ssvm-evmc-client-6 (crate (name "ssvm-evmc-client") (vers "6.3.1") (deps (list (crate-dep (name "evmc-sys") (req "^6.3.1") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "libloading") (req "^0.5") (default-features #t) (kind 0)))) (hash "12zyvy34x8dwvq09krabshnwpgi41q94i0zrc9qv8k2y37d4k175") (yanked #t)))

(define-public crate-ssvm-evmc-client-6 (crate (name "ssvm-evmc-client") (vers "6.3.1-rc1") (deps (list (crate-dep (name "evmc-sys") (req "^6.3.1-rc1") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "libloading") (req "^0.5") (default-features #t) (kind 0)))) (hash "0sda63cj4zccf78g7qyhjzkdrqgcairfbpv1ax7qihk0jxrgwr6j") (yanked #t)))

(define-public crate-ssvm-evmc-client-7 (crate (name "ssvm-evmc-client") (vers "7.4.0-rc1") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "evmc-sys") (req "^7.4.0") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "evmc-vm") (req "^7.4.0") (default-features #t) (kind 0) (package "ssvm-evmc-vm")) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)))) (hash "01za6dc51d8f62my6kp9bl3cl1k9v2k89v6kl2dkpyi8s2xn4ylb") (yanked #t)))

(define-public crate-ssvm-evmc-client-6 (crate (name "ssvm-evmc-client") (vers "6.3.1-rc3") (deps (list (crate-dep (name "evmc-sys") (req "^6.3.1-rc3") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "libloading") (req "^0.5") (default-features #t) (kind 0)))) (hash "0c3773adxc7qvi2cnlwlhl7v65izivhdhk33fbx87mrs9p65w2dz") (yanked #t)))

(define-public crate-ssvm-evmc-client-7 (crate (name "ssvm-evmc-client") (vers "7.4.0-rc2") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "evmc-sys") (req "^7.4.0-rc2") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "evmc-vm") (req "^7.4.0-rc2") (default-features #t) (kind 0) (package "ssvm-evmc-vm")) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kw7whf6disbymp1w9slw3hfm6rimj49kl0c9v8d7f2j9whw92n0")))

(define-public crate-ssvm-evmc-client-6 (crate (name "ssvm-evmc-client") (vers "6.3.1-rc4") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "evmc-sys") (req "^6.3.1-rc4") (default-features #t) (kind 0) (package "ssvm-evmc-sys")) (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)))) (hash "1n5km1dixb95myjyhn20fsjksvjykarna314ka22c71rfmrnpcjv")))

(define-public crate-ssvm-evmc-declare-6 (crate (name "ssvm-evmc-declare") (vers "6.3.1-rc3") (deps (list (crate-dep (name "evmc-vm") (req "^6.3.1-rc3") (default-features #t) (kind 0) (package "ssvm-evmc-vm")) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.33") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12h1xzw00s4hcylxa9j84f5hmif7a6dqbayd4b23q6arbp3ki238")))

(define-public crate-ssvm-evmc-sys-7 (crate (name "ssvm-evmc-sys") (vers "7.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "00sv4hp0hzanhi93yx00v40qx8cwnrck1qp3l5k2jbl34pqwppzf") (yanked #t)))

(define-public crate-ssvm-evmc-sys-6 (crate (name "ssvm-evmc-sys") (vers "6.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.51.0") (default-features #t) (kind 1)))) (hash "1q42m31vz49d0aqc0sy7mizkr5i61vzzd05fiy2imwlrwr8nph45") (yanked #t)))

(define-public crate-ssvm-evmc-sys-6 (crate (name "ssvm-evmc-sys") (vers "6.3.1-rc1") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "0lppls4pflriv4b8h1hlkzfxvh2syy9v7h06qai2haxy425q3pq2") (yanked #t)))

(define-public crate-ssvm-evmc-sys-6 (crate (name "ssvm-evmc-sys") (vers "6.3.1-rc3") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "0jihbkswpq2s8chs0v2dnjd00jkhdjzmlzmyh8dss20wwgn43mfb") (yanked #t)))

(define-public crate-ssvm-evmc-sys-6 (crate (name "ssvm-evmc-sys") (vers "6.3.1-rc4") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "05ax9cd6xmy1awydi5r7rnyc7ck2c88y9wfjs1cikn8g326k5r6f")))

(define-public crate-ssvm-evmc-sys-7 (crate (name "ssvm-evmc-sys") (vers "7.4.0-rc2") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "1shw8cz8wxlyhnxa0afg1kkb55iw5d2ycl5i7507pzjj8li5c8ig")))

(define-public crate-ssvm-evmc-vm-7 (crate (name "ssvm-evmc-vm") (vers "7.4.0") (deps (list (crate-dep (name "evmc-sys") (req "^7.4.0") (default-features #t) (kind 0) (package "ssvm-evmc-sys")))) (hash "1hi94n1j0fjnaa561pj4rgny25qqjckclsrb2bqfrbhqq4giqspy") (yanked #t)))

(define-public crate-ssvm-evmc-vm-6 (crate (name "ssvm-evmc-vm") (vers "6.3.1-rc3") (deps (list (crate-dep (name "evmc-sys") (req "^6.3.1-rc3") (default-features #t) (kind 0) (package "ssvm-evmc-sys")))) (hash "0a5kx0nla1irckliz4bn1qqazk01ppa9ywyxfbqdm8f7sp4y1ivn") (yanked #t)))

(define-public crate-ssvm-wasi-helper-0.1 (crate (name "ssvm-wasi-helper") (vers "0.1.0") (hash "0zwnbm32vx99xd2xjwfw920gnawfvf8hd16p1zdnh0ifpgii1mck")))

(define-public crate-ssvm-wasi-helper-0.1 (crate (name "ssvm-wasi-helper") (vers "0.1.1") (hash "10zky01xy1jc38j8fgrzcq7j5g17gvgj1bq6klcsgys9659pxzzd")))

(define-public crate-ssvm-wasi-helper-0.1 (crate (name "ssvm-wasi-helper") (vers "0.1.2") (hash "0fgfkgwx64zabryc5d74cjmlqjg09sm1rj9f8zgs4rbgmnk8qz8y")))

(define-public crate-ssvm-wasi-helper-0.1 (crate (name "ssvm-wasi-helper") (vers "0.1.3") (hash "0ql0sms93kklasyymkpz1y0qmaqgf1g3v5vx7fwr52kc3w2l76jf")))

(define-public crate-ssvm_container-0.1 (crate (name "ssvm_container") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "leveldb") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1aldczl14x5b8rlw39107qyq3b8la09aj1wqz9l9513p3g272g35")))

(define-public crate-ssvm_container-0.1 (crate (name "ssvm_container") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "leveldb") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "16sy6b4q2gjjs70p96k1qbgvgrpqiiq9154am74s516ls4yn5im8")))

(define-public crate-ssvm_container-0.1 (crate (name "ssvm_container") (vers "0.1.2") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "leveldb") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0vwwnnc69g1kl9ahhy1b5xijp2zcfhmrwyizlwr0m5ryn4lq5jam")))

(define-public crate-ssvm_container-0.1 (crate (name "ssvm_container") (vers "0.1.3") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0cmq88fhclmjfgczm8ldbz90ks0c4j78841cjchibqjkm4syyjcd")))

(define-public crate-ssvm_process_interface-0.1 (crate (name "ssvm_process_interface") (vers "0.1.3") (hash "121rrhm6rfqnszfiv628jry01m2vba8iigbdzf4a7d8mijq4lymz")))

(define-public crate-ssvm_process_interface-0.1 (crate (name "ssvm_process_interface") (vers "0.1.4") (hash "1l5grli9cdwwyhc4washpbikjh8fz2y9zwjclxy4jak0ikrrc1as")))

(define-public crate-ssvm_storage_interface-0.1 (crate (name "ssvm_storage_interface") (vers "0.1.29") (deps (list (crate-dep (name "bincode") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serialize_deserialize_u8_i32") (req "^0.1") (default-features #t) (kind 0)))) (hash "03diq6p4a7v8ysy0hrc9n5f2cf45c1zgdz6r9p0c0wqqvxfhxa2v")))

(define-public crate-ssvm_storage_interface-0.1 (crate (name "ssvm_storage_interface") (vers "0.1.30") (deps (list (crate-dep (name "bincode") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serialize_deserialize_u8_i32") (req "^0.1") (default-features #t) (kind 0)))) (hash "150kgn6d0hw3mvxqy8jw5rlh731mh4gwlvppp33wkq1amj2wnamf")))

(define-public crate-ssvm_tensorflow_interface-0.1 (crate (name "ssvm_tensorflow_interface") (vers "0.1.0") (hash "0nymc0z1rkigrd9rm6i9bl414i8m53dk6040pjqsyx9k95nqwdwa")))

(define-public crate-ssvm_tensorflow_interface-0.1 (crate (name "ssvm_tensorflow_interface") (vers "0.1.1") (hash "1xf3sxnifibwmqzw9qd7qh9ga6bqd8z45w21rnf8zd9i5qigkms2")))

(define-public crate-ssvm_tensorflow_interface-0.1 (crate (name "ssvm_tensorflow_interface") (vers "0.1.2") (hash "0cfzaxfmvy0s21xwja8h9xn4jbf23azfxjizk75dbqpa4jra2fal")))

(define-public crate-ssvm_tensorflow_interface-0.1 (crate (name "ssvm_tensorflow_interface") (vers "0.1.3") (hash "1x0gyb8yfvaqklqjxpc1mvaxgz44z7f3k3ixgjiwbfp3syhmjbbr")))

(define-public crate-ssvm_tensorflow_interface-0.1 (crate (name "ssvm_tensorflow_interface") (vers "0.1.4") (hash "14rgzmhypzsw1fzig32y027innd4f7a41m4m7xn78v5sxky328fd")))

