(define-module (crates-io ss hh) #:use-module (crates-io))

(define-public crate-sshh-0.1 (crate (name "sshh") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33") (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0n2zfbx503kx78g2y67gdzypq1ndia1gfvzn5ss5sar1wk64xnlh")))

(define-public crate-sshh-0.1 (crate (name "sshh") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "~2.33") (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qp4s2k1kfvi4gkwadp0ndbmgghwfh4j7y5x7kqxp4sydycf4yl0")))

(define-public crate-sshh-0.1 (crate (name "sshh") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "~2.33") (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "03hjdq3k84dsn6c63ppxnqv238613kgb46hfk4812kfnxbvfr7j6")))

