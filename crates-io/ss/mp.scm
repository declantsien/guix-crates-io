(define-module (crates-io ss mp) #:use-module (crates-io))

(define-public crate-ssmp-0.1 (crate (name "ssmp") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req ">= 0.4.0") (default-features #t) (kind 0)))) (hash "1wwyb37sdghj3b89a81q24lncl7vaihzq25bpmh872ghnbp0c5wz")))

(define-public crate-ssmpuller-0.1 (crate (name "ssmpuller") (vers "0.1.0") (deps (list (crate-dep (name "aws-config") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-ssm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "aws-types") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n6bvw7q83acvki11q957k0ybnk31m9lc8bs2w5a4qfg0cmlzk1m")))

