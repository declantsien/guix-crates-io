(define-module (crates-io ss si) #:use-module (crates-io))

(define-public crate-sssignals-0.1 (crate (name "sssignals") (vers "0.1.0") (hash "1ddgdjwka8scvxx075s15sz32hca73c2fr8rz653n9nk8b7cyvjd")))

(define-public crate-sssignals-0.2 (crate (name "sssignals") (vers "0.2.0") (hash "1i65nzfk51f4sk953lpnp1jhaqdb4sngycand39i2dcjdjrfl4r2")))

(define-public crate-sssignals-0.2 (crate (name "sssignals") (vers "0.2.1") (hash "1m3k6dgjcgn407xy9p7rw6kizl98iyqy6yx0l8w0k5205cfz5q7z")))

