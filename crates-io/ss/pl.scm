(define-module (crates-io ss pl) #:use-module (crates-io))

(define-public crate-ssplit-1 (crate (name "ssplit") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "13c68ih3plnwdsd5dn2nmaa3c5k6nh85rp2q30gj7f1302isc4q7")))

