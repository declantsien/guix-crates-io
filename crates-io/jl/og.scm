(define-module (crates-io jl og) #:use-module (crates-io))

(define-public crate-jlog-0.1 (crate (name "jlog") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1d9pdgi0fpbsy7fbp7jz89mby192fvnl7i9hnbv6qn396sn6ahjs")))

(define-public crate-jlog-0.1 (crate (name "jlog") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "083aq8j49nx5c5qnx7i8vncx6fjjwsww87fpyjqx3ri7xpn9hywc")))

(define-public crate-jlog-0.1 (crate (name "jlog") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0v3fpgypdmy5hns61asb18xd0wn91hp2xl6ppldzs26xx28pb639")))

(define-public crate-jlog-0.1 (crate (name "jlog") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1vfq8ki1sqqb9zg3sinh38icpdizjk11va0k7kg9jk2zr6n5j3sc")))

(define-public crate-jlog-0.1 (crate (name "jlog") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "14rhpp4azzbqsyz2r505b4sg5sa4rwp9xq7m6zyyx768xxinbkfx")))

(define-public crate-jlog-0.1 (crate (name "jlog") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1f6s3rc76cm6qsa9s4jf73a3w2x3gbkcwdi4ldvcnn7850d52id9")))

(define-public crate-jlog-0.2 (crate (name "jlog") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0x0vpq1l7rsv52q6gsn4c6v6si943zwa4kdbq6lsy7wiq4pk8ghd")))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1lmhy85idp6981c1gj1if4wskcyajpl881na1q22vjcys3mpy2g8") (yanked #t)))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1k456qfxsv2nz3mpybhmglpnqakwvjdfvkpcf5kfhk8zq8x89hmr")))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1107pz1ky7hadyz7vhy5in6qha2vx0xpwx9vgx5pp0625i3vbfs5")))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "05pmarix0fxgcyr48hpxw3yc3891s2ip4zdydp85bf8hxjgbhpr2")))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1p3m4yzng5570qnbr5k1brjm8q454pnwvg82d2psdg3qmyi4ija4")))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "17sm55d6ksc8v9hh54431winfxinkv686kbp517sj61y41bc48g1")))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0ivr8p21rjfsya807yzixi7kkqsk859sca549x3is6ih8g0kkpn8")))

(define-public crate-jlogger-0.1 (crate (name "jlogger") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0d6zai44pjp1yk56gk4jh4sw51kjwvqwjcinz54bacd21c4265w3")))

(define-public crate-jlogger-tracing-0.1 (crate (name "jlogger-tracing") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "1jhlbxispgz7mvqins24kahhy4zn4kilfyx645ivkr9lqvp1pnqk")))

(define-public crate-jlogger-tracing-0.1 (crate (name "jlogger-tracing") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "064p3lgc3k9ns8hak42rpmsw0nfsgq8phl982zr51qlvgkqmalwi")))

(define-public crate-jlogger-tracing-0.1 (crate (name "jlogger-tracing") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0km4cp94nps1lxjijwpd26s09839k1mr0k73mvbrd8r7cns6kfh7")))

(define-public crate-jlogger-tracing-0.1 (crate (name "jlogger-tracing") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1r2gpnpqwxlmxa4dhxfx8yx7ni9pg0hbd8ngww0flym4vh369hcn")))

