(define-module (crates-io jl ic) #:use-module (crates-io))

(define-public crate-jlic-0.1 (crate (name "jlic") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.17") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde_toml") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1s2qbcjjgmi8632bvcj7fkra1m54pya506yfmlm8wxya0b7lwv11")))

