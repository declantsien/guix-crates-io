(define-module (crates-io jl in) #:use-module (crates-io))

(define-public crate-jlink_rtt-0.1 (crate (name "jlink_rtt") (vers "0.1.0") (hash "1w87x0ij387ahjs1mdvsbfl6a2d256rxnbaq95mnlf66lbhp3439")))

(define-public crate-jlink_rtt-0.2 (crate (name "jlink_rtt") (vers "0.2.0") (hash "1hnzmshgmykccy5b6cn45059pygy6dlms95mjmqbsr2g2m0azxj2") (features (quote (("rtt_in_ram"))))))

