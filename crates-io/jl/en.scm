(define-module (crates-io jl en) #:use-module (crates-io))

(define-public crate-jlens-0.0.1 (crate (name "jlens") (vers "0.0.1") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1ln5k72929vzcpd16kkyy7i0ads3yffmv4jrwkvjyhamm4r5lyzz")))

