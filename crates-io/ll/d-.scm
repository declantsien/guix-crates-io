(define-module (crates-io ll d-) #:use-module (crates-io))

(define-public crate-lld-pg-0.1 (crate (name "lld-pg") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "06mjnpr6glml4m030pvkl5rxh8gpwv9ymq4manzwk5bqz1jpn8pz")))

(define-public crate-lld-pg-0.1 (crate (name "lld-pg") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "1zgz4zxy583digynr3cnr6yf4b50a6kkskp815slz24qjqwhg0jw")))

(define-public crate-lld-pg-0.1 (crate (name "lld-pg") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0xn94vzq9mryqcj877mn2a5rp183p7knc4l1rs32zl2gv484az9a")))

