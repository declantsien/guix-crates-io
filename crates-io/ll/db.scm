(define-module (crates-io ll db) #:use-module (crates-io))

(define-public crate-lldb-0.0.1 (crate (name "lldb") (vers "0.0.1") (deps (list (crate-dep (name "lldb-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0q6ld11pxq51iyvvpdbmf4sqd9vr8d7pabdvijnjvgk1xlq267yq")))

(define-public crate-lldb-0.0.2 (crate (name "lldb") (vers "0.0.2") (deps (list (crate-dep (name "lldb-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1b2gc942bdy22cnwbc7hlnf32z1ycjg5p08xq7jsrkvrqcvdpl6d")))

(define-public crate-lldb-0.0.3 (crate (name "lldb") (vers "0.0.3") (deps (list (crate-dep (name "lldb-sys") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0a3qv54cnivzbmrsx5s4ymnqs3n6cszbhh2xmff1a2qm5bvqzldd")))

(define-public crate-lldb-0.0.4 (crate (name "lldb") (vers "0.0.4") (deps (list (crate-dep (name "lldb-sys") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "13w0683rdc54hkfnpwz19lxziipfbm2p9pcs9d4qnailin4jdjqc")))

(define-public crate-lldb-0.0.5 (crate (name "lldb") (vers "0.0.5") (deps (list (crate-dep (name "lldb-sys") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "10fa6rsh8jczh6x1kfhdmw7ncisiipg54jgdi6mkmjd6iswv52pl")))

(define-public crate-lldb-0.0.6 (crate (name "lldb") (vers "0.0.6") (deps (list (crate-dep (name "juniper") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lldb-sys") (req "^0.0.14") (default-features #t) (kind 0)))) (hash "1flarfdmhvbkps4dnzcq44ayhks75m07flx6mzxfg1cfs8nff7i0") (features (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.7 (crate (name "lldb") (vers "0.0.7") (deps (list (crate-dep (name "juniper") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lldb-sys") (req "^0.0.18") (default-features #t) (kind 0)))) (hash "021x0xg0ykxgfv5vng8vaj9nqqrxmh4jlhy28r5n3bqsq1lb0882") (features (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.8 (crate (name "lldb") (vers "0.0.8") (deps (list (crate-dep (name "juniper") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lldb-sys") (req "^0.0.20") (default-features #t) (kind 0)))) (hash "1l7nclmk6wlnjixdhlx6q2nm0zln1q6xx1182jkhx4lql43qwn5s") (features (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.9 (crate (name "lldb") (vers "0.0.9") (deps (list (crate-dep (name "juniper") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lldb-sys") (req "^0.0.27") (default-features #t) (kind 0)))) (hash "0665y9kiyis0csq8a1bf3ncbsrs13dq4mad6m49hijqg5gcrizj2") (features (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.10 (crate (name "lldb") (vers "0.0.10") (deps (list (crate-dep (name "juniper") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lldb-sys") (req "^0.0.27") (default-features #t) (kind 0)))) (hash "01ic49d7gwrifjq8wdn8a2vzgzqww841mi1b39sfjid03mppcykl") (features (quote (("graphql" "juniper"))))))

(define-public crate-lldb-0.0.11 (crate (name "lldb") (vers "0.0.11") (deps (list (crate-dep (name "juniper") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lldb-sys") (req "^0.0.29") (default-features #t) (kind 0)))) (hash "1cll6hdwb9zmkgy99y2l6dbmn796497n6z370hw08vza476kmbbf") (features (quote (("graphql" "juniper"))))))

(define-public crate-lldb-sys-0.0.1 (crate (name "lldb-sys") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "1ybky0r9pymxzr2rcrr34q5iyygx5v0mnwm9bypvm5nn9229m7yp")))

(define-public crate-lldb-sys-0.0.2 (crate (name "lldb-sys") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1z418mdpvblxifimrc4ldfrhaganf98ppimqchbxl4z82pilwp3v")))

(define-public crate-lldb-sys-0.0.3 (crate (name "lldb-sys") (vers "0.0.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "10kxw2mma3bv2pm09nnpgzsdz29rxs3mfqqd6i22wyw8grlqjam5")))

(define-public crate-lldb-sys-0.0.4 (crate (name "lldb-sys") (vers "0.0.4") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1nnkac8jpw51z0c35nfdn58ypjy36srvy4vlm8jb1690lkm9bcp4")))

(define-public crate-lldb-sys-0.0.5 (crate (name "lldb-sys") (vers "0.0.5") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1sfv969y1s0q9q86ribqvlyr1crm8jqamm85bcygrz5brqa3bz9k")))

(define-public crate-lldb-sys-0.0.6 (crate (name "lldb-sys") (vers "0.0.6") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1d2mqvgqf6z0y00in5l3ppf6l0hizb5pgywj7971q5zm10k3v0hp")))

(define-public crate-lldb-sys-0.0.7 (crate (name "lldb-sys") (vers "0.0.7") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0vhr406zpxbwhk8fh7f4gc86rz61834yxqvc36ik54x91v2xjdin")))

(define-public crate-lldb-sys-0.0.8 (crate (name "lldb-sys") (vers "0.0.8") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0zzc1fp2mmyv1vm9qwcrk1avfxc1kf3pqna9xq449ynxcs4mbjz2")))

(define-public crate-lldb-sys-0.0.9 (crate (name "lldb-sys") (vers "0.0.9") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "07axagwmllxkmhmshkcwgqia1diny1sx5ky4zw820sw4pl57hmvc")))

(define-public crate-lldb-sys-0.0.10 (crate (name "lldb-sys") (vers "0.0.10") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "13byi5qisvmfl3azj2hf671gf6mhcwpgk1q2m0srda1lwr9wjn7s")))

(define-public crate-lldb-sys-0.0.11 (crate (name "lldb-sys") (vers "0.0.11") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0gslkxvacnzmm4g0i4fddfcrrw6l1c32mr6dkqbqnyqr6dkbn89s")))

(define-public crate-lldb-sys-0.0.12 (crate (name "lldb-sys") (vers "0.0.12") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "00a5ndflvr2z9w5nl5jmb710ppn7lwq4p01gr01ykn31zwm3ami5")))

(define-public crate-lldb-sys-0.0.13 (crate (name "lldb-sys") (vers "0.0.13") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1yg45k7lbrnlwj4y3v757vk42k5ib587b88i4ik05cxjlz95ya7m")))

(define-public crate-lldb-sys-0.0.14 (crate (name "lldb-sys") (vers "0.0.14") (deps (list (crate-dep (name "bitflags") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n1q67x3vgcy6y0r005126cjyrb2907dyks5b99vd9x1naa89i1i")))

(define-public crate-lldb-sys-0.0.15 (crate (name "lldb-sys") (vers "0.0.15") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "044wqdizlvp9vdnd46xn25px0b0gambw96xych33sb48j4vjs69i")))

(define-public crate-lldb-sys-0.0.16 (crate (name "lldb-sys") (vers "0.0.16") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mp1m87aakfl4ajq7lvx567ldvzhkhyb83p30j234ksg9b9hdml1")))

(define-public crate-lldb-sys-0.0.17 (crate (name "lldb-sys") (vers "0.0.17") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12zsrkmpycqkn5mxysy37primi9izw1bg2l97r158pigiclbabwy")))

(define-public crate-lldb-sys-0.0.18 (crate (name "lldb-sys") (vers "0.0.18") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "16xky5gjm39spvplcdsqrhygcxkgj6wn5yqbx6iskdhp9iglq8nj")))

(define-public crate-lldb-sys-0.0.19 (crate (name "lldb-sys") (vers "0.0.19") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "055y5y6dph5dvh2dhiz12w91s5l3v14ds3d2jx3dn64w4xrmqj90")))

(define-public crate-lldb-sys-0.0.20 (crate (name "lldb-sys") (vers "0.0.20") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12czrz71p00icx7am866z5fd58jc5jpd5b6ana7sl0bpsvlpd6ih")))

(define-public crate-lldb-sys-0.0.21 (crate (name "lldb-sys") (vers "0.0.21") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ph1vgsdkn3qns0drx3lx2i7k4flkw9hy0qsxn5y8rsg1d6sk3bk")))

(define-public crate-lldb-sys-0.0.22 (crate (name "lldb-sys") (vers "0.0.22") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wlgpf7ymr0x9kn5f63djw2529bq159hqzw5ns0q01gdsnx5rsy4") (features (quote (("docs-rs"))))))

(define-public crate-lldb-sys-0.0.23 (crate (name "lldb-sys") (vers "0.0.23") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z21sf48r00qn520dk00wpi6j5l0w9v9wrd3fx0zrnq0ifh7ly0q") (features (quote (("docs-rs"))))))

(define-public crate-lldb-sys-0.0.24 (crate (name "lldb-sys") (vers "0.0.24") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vzw5yy7fwzb0nq3cidgnjn2qg3zrhq2gjp3pwgg0z6i23ard2jm") (features (quote (("docs-rs"))))))

(define-public crate-lldb-sys-0.0.25 (crate (name "lldb-sys") (vers "0.0.25") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1smwg93m4ywwwvfkzx0gqa6drinpgdphhal5lgp99y3hbinixajp") (features (quote (("docs-rs"))))))

(define-public crate-lldb-sys-0.0.26 (crate (name "lldb-sys") (vers "0.0.26") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bjdzsris40iq7j580xn9b5dkidpbqqsj47jhnvgyrnbckacwcir") (features (quote (("docs-rs"))))))

(define-public crate-lldb-sys-0.0.27 (crate (name "lldb-sys") (vers "0.0.27") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iqr7f31g9d44mdpp1ys8by1d2p3kvwgknj1zdq1gqhqggms9xgc")))

(define-public crate-lldb-sys-0.0.28 (crate (name "lldb-sys") (vers "0.0.28") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05n320q2kal0zgry4dlp52ih33iifg7ddrgdl7wqfln1ridbsdq4")))

(define-public crate-lldb-sys-0.0.29 (crate (name "lldb-sys") (vers "0.0.29") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0psfds8sc6y2ys5chgw6c8ivxmycb5knds95n1dwf288v693k0p4")))

(define-public crate-lldb-sys-0.0.30 (crate (name "lldb-sys") (vers "0.0.30") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v6dink5y5ivz6cdnr54xvq5k16rwnvyaf6dw0dcs6w8gxq0yfqb")))

