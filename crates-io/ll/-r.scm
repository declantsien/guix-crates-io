(define-module (crates-io ll -r) #:use-module (crates-io))

(define-public crate-ll-rs-0.0.0 (crate (name "ll-rs") (vers "0.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "0jxkv136dp5pxklh95j0jxqasfswnawsv1fqg2gb7ijc3fp74js3") (yanked #t)))

(define-public crate-ll-rs-0.0.1 (crate (name "ll-rs") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "15jfkv1gfwlhigh05xji62qlmzhwnds6bwaj7gpbi38z2a0la0w3") (yanked #t)))

(define-public crate-ll-rs-0.0.2 (crate (name "ll-rs") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1nc36y1krvyf3312fhwk2h89xag3pair3syq3cmhz2bhd3230dhm") (yanked #t)))

(define-public crate-ll-rs-0.0.3 (crate (name "ll-rs") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "00sv4v866yw5jmspc0xvq2si5vsd19ha0xzii9mx4wsk6kpmxmi6") (yanked #t)))

(define-public crate-ll-rs-0.0.4 (crate (name "ll-rs") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "17ns0j0dlx0bdqiqk97p31nzn264hmm0b1w2hc56irn2wj0aqjag") (yanked #t)))

(define-public crate-ll-rs-0.0.5 (crate (name "ll-rs") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1isjq6j5a8a4vj01579hhmn5rm1cj348v103kbc6211jssk3nwk5") (yanked #t)))

(define-public crate-ll-rs-0.0.6 (crate (name "ll-rs") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "159zjrbrg1hiblmr41r10yxn13idhxql71pjy1y4wiqab3gwcp09") (yanked #t)))

(define-public crate-ll-rs-0.0.7 (crate (name "ll-rs") (vers "0.0.7") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1xb1fr9z5lb4ng5jw0jqcxs67g7sfpkym1w9yf85gf6nlds1p5zc") (yanked #t)))

(define-public crate-ll-rs-0.0.8 (crate (name "ll-rs") (vers "0.0.8") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1iard1mihyzzqlkyb75gj2l16lhnafzzx89j9p86qiar95hay9ml") (yanked #t)))

(define-public crate-ll-rs-0.0.9 (crate (name "ll-rs") (vers "0.0.9") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "0pwq3q85120glyfx4cb0m8fcg2a40fdjzkj06gaw5qwff978amqn")))

(define-public crate-ll-rs-0.1 (crate (name "ll-rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "is-terminal") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "17har558a4zidll6bi1k4xjy2wjzq61mjqlac2xl7k2hj6bgjv4a")))

