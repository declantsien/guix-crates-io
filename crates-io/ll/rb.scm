(define-module (crates-io ll rb) #:use-module (crates-io))

(define-public crate-llrb-0.0.1 (crate (name "llrb") (vers "0.0.1") (hash "1qh1c0y7dj8xvwi4y30md1f5jncq9vww89m0ngn46yfwqsjvhh8r")))

(define-public crate-llrb-index-0.1 (crate (name "llrb-index") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "07iifkmaf7ywxbcmp4j44brps0ayag1l4hph26lihy6fg03njvbs")))

(define-public crate-llrb-index-0.2 (crate (name "llrb-index") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "0anxmv5av0gd48x912nkzbgdg8gb6m7wqrr15dyj9w9cshsg20rr")))

(define-public crate-llrb-index-0.3 (crate (name "llrb-index") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "1z3cpzbahjwr1r5nbrhbs7x7ld22pkpj4r14p6vg5lkzxmgmyw7x")))

(define-public crate-llrb-index-0.4 (crate (name "llrb-index") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "1hbwyg7z8zz5qba7gg2aaz8is3lbjqzhl0nh8nbin0dkg2wgx9bz")))

