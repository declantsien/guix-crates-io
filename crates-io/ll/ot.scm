(define-module (crates-io ll ot) #:use-module (crates-io))

(define-public crate-lloth-0.1 (crate (name "lloth") (vers "0.1.0") (hash "09afcffp01rsqcs0j9hjm9rk1iv0ppl9198gyziqs5qqlg1jlb6v") (yanked #t)))

(define-public crate-lloth-core-0.1 (crate (name "lloth-core") (vers "0.1.0") (deps (list (crate-dep (name "alkahest") (req "^0.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures-task") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scoped-arena") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h11b2r60p31b018456j3jqcn0b5sx3hg13kxcgxwjs153qvz210") (features (quote (("tcp" "tokio/net" "futures-task")))) (yanked #t)))

