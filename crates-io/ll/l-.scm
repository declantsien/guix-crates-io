(define-module (crates-io ll l-) #:use-module (crates-io))

(define-public crate-lll-rs-0.1 (crate (name "lll-rs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rug") (req "1.*") (default-features #t) (kind 0)))) (hash "0ilid6nalxqvshlcfk3bsf194mrjda9fhiylk6kk3qbmw8qpmpd8")))

(define-public crate-lll-rs-0.2 (crate (name "lll-rs") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rug") (req "1.*") (default-features #t) (kind 0)))) (hash "1dsvs3yqi1n16ahn80m1py1c0bqfmpxc3yg3pfyngqs1lmqiz1cr")))

