(define-module (crates-io ll uv) #:use-module (crates-io))

(define-public crate-lluvia-0.0.1 (crate (name "lluvia") (vers "0.0.1") (hash "045iv7nq4vs4j6qmlq86iwmh0xn9lr0scbwamf84dz9px623n6yv")))

(define-public crate-lluvia-0.0.2 (crate (name "lluvia") (vers "0.0.2") (hash "0rcdxzxv3ij46f15azzd1vnck96rsabp4xnvcml6h974lk5kwrfy")))

