(define-module (crates-io ll og) #:use-module (crates-io))

(define-public crate-llog-0.1 (crate (name "llog") (vers "0.1.0") (hash "1q1zlb8gapz4fdzminljhh0fnl6091dzyxviaw2dlaf01q29iifl") (yanked #t)))

(define-public crate-llog-0.1 (crate (name "llog") (vers "0.1.1") (hash "1s9j9wg4qfwxr15gfgrd338kax7l4r26d1ii522wi000qs4xq8v4")))

(define-public crate-llogin-0.1 (crate (name "llogin") (vers "0.1.0") (deps (list (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0zi6d8rmlzlbmbsfncyaswqg0myrr0ayxvpqvszkp96c6i2zy91c") (yanked #t)))

(define-public crate-llogin-0.1 (crate (name "llogin") (vers "0.1.1") (deps (list (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0hg5lnwh6xgj5l1vryqs7pjwyvvqmshrxiylqmhqxp9vlrqdsdf0") (yanked #t)))

(define-public crate-llogin-0.1 (crate (name "llogin") (vers "0.1.2") (deps (list (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0cl41h8jwpizhlybd7jh8v11xg7pqf1gi6kyvl4dyshdwn395vac")))

