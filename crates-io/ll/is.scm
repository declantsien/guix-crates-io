(define-module (crates-io ll is) #:use-module (crates-io))

(define-public crate-llist-0.1 (crate (name "llist") (vers "0.1.0") (hash "0xvckncrf1ch4awbgh406r2z3d9jwm7cxm9lfvgfm364sfz1hsvk") (rust-version "1.75")))

(define-public crate-llist-0.1 (crate (name "llist") (vers "0.1.1") (hash "1d0x26qkhgmshr1s0gnj5lwd9sq0kjgad5ijvrrdysqr56g9l4dw") (rust-version "1.75")))

(define-public crate-llist-0.1 (crate (name "llist") (vers "0.1.2") (hash "10a6dvg3h4xi5x7dviwfy5pp48ihbl99sx9cd2gpbvicbpvvnwmr") (rust-version "1.75")))

(define-public crate-llist-0.2 (crate (name "llist") (vers "0.2.0") (hash "0gr3air2vqff5jdm4lb166rwmv6i513fb1b46zdn5hmv5cibgd7x") (rust-version "1.75")))

(define-public crate-llist-0.2 (crate (name "llist") (vers "0.2.1") (hash "0nd0axk7p45b1yyyr54nmgj3hqrcv076ah44vfpyyqi5w1fhxayq") (rust-version "1.75")))

(define-public crate-llist-0.2 (crate (name "llist") (vers "0.2.2") (hash "0icf7n3yakpr33f7s9ld2wzdm8plswg6n97rnshsq4h4b3b6md99") (rust-version "1.75")))

(define-public crate-llist-0.2 (crate (name "llist") (vers "0.2.3") (hash "1vjrq55yxcj5dzg9lkjg0ppr8zjfaj0r18nnm80b0bx8s2rfv8dl") (rust-version "1.75")))

(define-public crate-llist-0.2 (crate (name "llist") (vers "0.2.4") (hash "0bbmbmh1ihvrv7r8a6skim9jg1p4q3xh077s7dskd5rhilwvb829") (rust-version "1.75")))

(define-public crate-llist-0.2 (crate (name "llist") (vers "0.2.5") (hash "114k31bgf41g3izf9zzqkjry14pn7ryvwg72jjvg82aws7sfj4y4") (rust-version "1.75")))

(define-public crate-llist-0.3 (crate (name "llist") (vers "0.3.0") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "0p1368ch8m1rjf2hydgba1p1jx45agsjvdgm4jx3jxqygwg4hbh7") (rust-version "1.75")))

(define-public crate-llist-0.3 (crate (name "llist") (vers "0.3.1") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "09yykgbqnvkasl041sqa85lq5r9k7w53mv22kfpn3q3cadrjsswa") (rust-version "1.75")))

(define-public crate-llist-0.3 (crate (name "llist") (vers "0.3.2") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "0sywhpi4x5bbs58hx8wd0k501kk93rfxfjj0qpwnx90nb25ram2v") (rust-version "1.75")))

(define-public crate-llist-0.4 (crate (name "llist") (vers "0.4.0") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "00qcnbrmsacq4sl133yrv1dxavzk5lihc5b5lbhkklkjcbfa6zw1") (rust-version "1.75")))

(define-public crate-llist-0.5 (crate (name "llist") (vers "0.5.0") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "1hswh3yah0q8jilghmby8x9sbbzrk5xvgy534x5166ck83xf7bg6") (rust-version "1.75")))

(define-public crate-llist-0.6 (crate (name "llist") (vers "0.6.0") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "12w03rg3ljpmf2wnl2akdq35x4zdx47dm547pa7h8ykm2mz329zb") (rust-version "1.75")))

(define-public crate-llist-0.6 (crate (name "llist") (vers "0.6.1") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "0zgqghpm9mghc0pr44ahc3zfmghcsgy8q46kv2p2nlxisdwh1l9w") (rust-version "1.75")))

(define-public crate-llist-0.6 (crate (name "llist") (vers "0.6.2") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "0wr9ppzy04ffd6qvq1kmhdpjzs6zyh4shmqlx24zbimm87bfny79") (rust-version "1.75")))

(define-public crate-llist-0.6 (crate (name "llist") (vers "0.6.3") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "0rvy6lqqjk2z3ga5dmc9afcc1lbxn1jajzlzr41995vyxbk5b7hq") (rust-version "1.75")))

(define-public crate-llist-0.7 (crate (name "llist") (vers "0.7.0") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "175cnfipfxw901kkxgdw2madkxfw6jrq79mhzg949ip99dv7dyx9") (rust-version "1.75")))

(define-public crate-llist-0.7 (crate (name "llist") (vers "0.7.1") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "1vwc6kvw34mj0ihl15km02lg8vgnnd821h78jmhn5xzbnk61spjn") (rust-version "1.75")))

(define-public crate-llist-0.7 (crate (name "llist") (vers "0.7.2") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "1d5f4ya5k7hi1qs1z4b73d8jwibfsdc2ib6ckxw16niz098qr4gv") (rust-version "1.75")))

(define-public crate-llist-0.7 (crate (name "llist") (vers "0.7.3") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (default-features #t) (kind 0)))) (hash "0addg4jzx6ii99ifykhghdp6kn1gmvb4w8slb0f0r1jhclchn203") (rust-version "1.75")))

