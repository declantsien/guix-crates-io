(define-module (crates-io ll ma) #:use-module (crates-io))

(define-public crate-llmaker-0.0.1 (crate (name "llmaker") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0wm4lyrhwjs5k8r4mb6l5ggxbxz04wvj1zj39mphvcg63n00s2qg")))

