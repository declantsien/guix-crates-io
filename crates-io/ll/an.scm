(define-module (crates-io ll an) #:use-module (crates-io))

(define-public crate-llang-0.0.1 (crate (name "llang") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.23.3") (default-features #t) (kind 0)) (crate-dep (name "llvm") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "^36") (default-features #t) (kind 0)) (crate-dep (name "nom-lua") (req "^0.0.2") (features (quote ("graphviz"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1m5wq8iwmysgkkzyiypcjjvyzbg81pyb6djg6mknrhpl20nm7v4x")))

