(define-module (crates-io ll iw) #:use-module (crates-io))

(define-public crate-lliw-0.1 (crate (name "lliw") (vers "0.1.0") (hash "1nxhxzs33ikrzm3dii9kkfz46dl573i5xylwky3dyfhmf7sj9q7i")))

(define-public crate-lliw-0.1 (crate (name "lliw") (vers "0.1.1") (hash "0qbdbc4f427dyxx0bpq0sxia2cq4rz5b2jlrrgk7kxg23xnhv8gn")))

(define-public crate-lliw-0.1 (crate (name "lliw") (vers "0.1.2") (hash "0324fh5srn4ygyhdlw66dly5npf7nwlyr2f2c6bpkas29hszr8lm")))

(define-public crate-lliw-0.2 (crate (name "lliw") (vers "0.2.0") (hash "1xrsswsjczj3wjhnjb9cp6qk1fhmgg17myvzkb5gg91mrj5jql1d")))

