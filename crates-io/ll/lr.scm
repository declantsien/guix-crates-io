(define-module (crates-io ll lr) #:use-module (crates-io))

(define-public crate-lllreduce-0.0.1 (crate (name "lllreduce") (vers "0.0.1") (hash "1q1dbjzkyw0kq7fb10dh46mcw3yvnj7gqbwdg61ys1qchqv3dbwj")))

(define-public crate-lllreduce-0.0.2 (crate (name "lllreduce") (vers "0.0.2") (hash "1q0b12zp44fi4jflwkd8zc8n9yhc6dmsaklys1c6cfx8bcb7253j")))

