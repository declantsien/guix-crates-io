(define-module (crates-io ll -c) #:use-module (crates-io))

(define-public crate-ll-colors-0.1 (crate (name "ll-colors") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "0zj2qdaw2ib8ncb2zim58ckmbqp9s1chk4zvbrbxqdfmmmsa3zl7")))

