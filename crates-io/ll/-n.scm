(define-module (crates-io ll -n) #:use-module (crates-io))

(define-public crate-ll-neighbors-0.1 (crate (name "ll-neighbors") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1bam846qr36ij66d3bcaz6a9wyixac4c4bln08ydwwlx9kk4zxz3")))

(define-public crate-ll-neighbors-0.1 (crate (name "ll-neighbors") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1zasx11p4v173kbvrzx1frdc9q4h59h56h31npwcjj5v104376zi")))

(define-public crate-ll-neighbors-0.1 (crate (name "ll-neighbors") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0867ayk4k1bpvrhh8y0ijdlzsxp2fa55b38636ydb3mycvyxsy3s")))

(define-public crate-ll-neighbors-0.1 (crate (name "ll-neighbors") (vers "0.1.3") (deps (list (crate-dep (name "macaddr") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "08d3nb0sdvvp7asjjnyq2a594fm5m5byymxy3aaa70pkqljbkvdn")))

(define-public crate-ll-neighbors-0.1 (crate (name "ll-neighbors") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1cyx30p0xf9pmf0dqqcqqags1dwikqxj10cphvr1cgd5n52p21cv")))

(define-public crate-ll-neighbors-0.1 (crate (name "ll-neighbors") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "11pyd01cypp88l5aqndyacg34k76xczqqm2wdac57k5j75l8x2br")))

(define-public crate-ll-neighbors-0.1 (crate (name "ll-neighbors") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "12k5mpdq9nqghn0yj6ybvha8v5qpm8a8d4d1ff7g7ysq1dfb6671")))

