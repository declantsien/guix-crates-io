(define-module (crates-io ll np) #:use-module (crates-io))

(define-public crate-llnparse-0.0.1 (crate (name "llnparse") (vers "0.0.1") (deps (list (crate-dep (name "llnparse-macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0ln5yy5yaw4d06vf8i2lrh6rkbbfnj6pvpfv8zm2vqd9syp5bxzl") (yanked #t)))

(define-public crate-llnparse-macros-0.0.1 (crate (name "llnparse-macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)))) (hash "1p33bm2va0j2bfgmgff5dzw7w9xmwvvwg6ycdc49fw42h8jq4rv9") (yanked #t)))

