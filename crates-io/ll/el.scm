(define-module (crates-io ll el) #:use-module (crates-io))

(define-public crate-llel-0.0.0 (crate (name "llel") (vers "0.0.0") (hash "13r91aaa3mz4jiczkn42q0jyy0sfi529rn0zwyy8zk7cw92vrcn6")))

(define-public crate-llel-common-0.0.0 (crate (name "llel-common") (vers "0.0.0") (hash "063xk0mfnrxrq2ird8l9j8rcpkknchrizbdzrmxi4wn1hhj9w4j7")))

(define-public crate-llel-compile-0.0.0 (crate (name "llel-compile") (vers "0.0.0") (hash "05fll35nbqwg4y4gybk17x5l8a0c6s6vgr6zvsx62kmiyja4796m")))

(define-public crate-llel-deploy-0.0.0 (crate (name "llel-deploy") (vers "0.0.0") (hash "1jzykfs65jcydjfnj9daxxzv3c7zbjj00g6xvv72l1qgb5cq0wlh")))

(define-public crate-llel-interpret-0.0.0 (crate (name "llel-interpret") (vers "0.0.0") (hash "1d2nxbab3ninvychzp18786nyvcjz7ya8bn85l32s26dvb27kk2a")))

(define-public crate-llel-macro-0.0.0 (crate (name "llel-macro") (vers "0.0.0") (hash "18adk7k428adin6kjc54xx7acyrn5rm0byq8ar4pmny6acyjcy3s")))

(define-public crate-llel-provide-0.0.0 (crate (name "llel-provide") (vers "0.0.0") (hash "0bx7vbd8da8r8rw394i8i646bsy5vrldswm1r87rxjb78v3hxhsi")))

