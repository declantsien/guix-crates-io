(define-module (crates-io za pa) #:use-module (crates-io))

(define-public crate-zapalloc-0.0.1 (crate (name "zapalloc") (vers "0.0.1") (hash "1lmi0h52b60sr76i3j62wndqbjfk1i4ny02jcqhwqqbv8llzb66h") (yanked #t)))

(define-public crate-zapalloc-0.0.2 (crate (name "zapalloc") (vers "0.0.2") (hash "0ijfr2049hn3v11h0v767a4rsl5liyy7ck57rxq66zi46x5g1yrh") (yanked #t)))

(define-public crate-zapalloc-0.0.3 (crate (name "zapalloc") (vers "0.0.3") (hash "0fj1wp1qp53m8xwg12igd549gpw3gj3pj3f8iidgfvrn8d6807i6") (yanked #t)))

(define-public crate-zapalloc-0.0.4 (crate (name "zapalloc") (vers "0.0.4") (hash "020w0qfs3g3qc2cmgvlqm64pmwcnyz3qw16mrlnp932y3540xzrb") (yanked #t)))

