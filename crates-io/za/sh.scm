(define-module (crates-io za sh) #:use-module (crates-io))

(define-public crate-zash-0.1 (crate (name "zash") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline-derive") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.1.5") (default-features #t) (kind 0)))) (hash "1sz10qmagw3jh9agd52160rwh3zqm80ny28xzm073yz3xylvzr48")))

