(define-module (crates-io za ki) #:use-module (crates-io))

(define-public crate-zaki-yama-bicycle-book-wordcount-0.1 (crate (name "zaki-yama-bicycle-book-wordcount") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sdmmhb6c00x8s76gamrq4ric0lyc3rw87k6r6fx1bra7sqrpx0w")))

(define-public crate-zaki-yama-bicycle-book-wordcount-0.1 (crate (name "zaki-yama-bicycle-book-wordcount") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rh2z9mlb9xf8gr30aj68c60hps14ajmxbrsnyqrk58fp4nvnp9s")))

