(define-module (crates-io za ut) #:use-module (crates-io))

(define-public crate-zauthrs-0.1 (crate (name "zauthrs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "yubico_manager") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0z9s5643zald9nqd0p87jkl3xw8777a3bwqzqmwndbgz99ddayx7") (yanked #t)))

(define-public crate-zauthrs-0.1 (crate (name "zauthrs") (vers "0.1.21") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "yubico_manager") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0bvhrhr9pb5c0fx2438pm3fr5n07r87fzww2cfnm69if5nhzpb93") (yanked #t)))

(define-public crate-zauthrs-0.1 (crate (name "zauthrs") (vers "0.1.22") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "yubico_manager") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1s3m09g0nfrr948v951kh4m1q8y5p8jl60c7hqzrly0h2225kkcb") (yanked #t)))

(define-public crate-zauthrs-0.1 (crate (name "zauthrs") (vers "0.1.23") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "yubico_manager") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1whghnzb5jilbps11gmdbds21rm3ys0819nsfsqkaj1dcnnjbb71") (yanked #t)))

(define-public crate-zauthrs-0.1 (crate (name "zauthrs") (vers "0.1.24") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "yubico_manager") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "162qmb9chjmvm6n3l9r3zxmflfys8lw22bhv0lzh25bx762lxfjr") (yanked #t)))

