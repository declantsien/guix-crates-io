(define-module (crates-io za rg) #:use-module (crates-io))

(define-public crate-zargs-0.1 (crate (name "zargs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1shsmp6lsaii8w1m7qb3cbccsv1cc2h4yxqh20nnqrj8hh1rjh0f")))

(define-public crate-zargs-0.1 (crate (name "zargs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1r14d4af4jbcdrvgrwqygqvshpmj4l0vjrrmzziqdgs92k7lm51l")))

