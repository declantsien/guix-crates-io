(define-module (crates-io za ra) #:use-module (crates-io))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "06lxzmxpzhqrhsm8n94b4wjmcdz4dm1g14yl5aa8vhclycxpqdmy")))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1kalmjg1wwak3wn9by5a1vf5hxrhsw3qzg72ksr4zd8pxf2smpl8")))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "08gi51xwbi2w8g6112rmc35n9pvpwpammbszfcmscsc3m6ay00n1")))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "12i5scps01fmfqq0b5yrpi6fjb5jiv1ykdkali1p31v7r7skk82d")))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.4") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "08px665dbmy3m3c295h1kvi6pibwnyfv8ysskqz81r2429qm0m60")))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.5") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0b6wi7cdpz69ny6l5169kqbh0qv11gkw9720bygyxwqn9vz8b4id")))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.6") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0qmzr2c9v3rfpi5k0kfqvnfn46a9b8px5ckn8aavdwkcvclhqxyp")))

(define-public crate-zara-1 (crate (name "zara") (vers "1.0.7") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1anx2pb3b3fykvqwcrlw4r1z0mamrmc4is5l72fkcax1562w3vqx")))

(define-public crate-zarang-0.0.0 (crate (name "zarang") (vers "0.0.0") (hash "0pdnn706nv1yk6nhkhk99isy14ijz1i0pn2b26dd998x8m7k7bsb")))

