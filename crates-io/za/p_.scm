(define-module (crates-io za p_) #:use-module (crates-io))

(define-public crate-zap_api-0.0.1 (crate (name "zap_api") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ll2d2dldp0aijys98c227sk9nhgn87m35jgpiqcxcb3kyja7dsl")))

(define-public crate-zap_api-0.0.2 (crate (name "zap_api") (vers "0.0.2") (deps (list (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1knvrs5kyx2q7r2w91xi6m9fimis1xbcl6qmh75jgvbdacsnmsw5")))

