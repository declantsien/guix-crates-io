(define-module (crates-io za cl) #:use-module (crates-io))

(define-public crate-zacli-0.1 (crate (name "zacli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zaif-api") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0ksh8hl3dsgsibyg9cb7li5g4djpjpqlp70ksal3qhsf7jn0j33g")))

