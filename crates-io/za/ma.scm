(define-module (crates-io za ma) #:use-module (crates-io))

(define-public crate-zama-0.1 (crate (name "zama") (vers "0.1.0") (hash "00axvhwz8v4m99hh1i370zhw0jl392yifg0c4yyb7h8l8p6f5g6y")))

(define-public crate-zama_sdk-0.1 (crate (name "zama_sdk") (vers "0.1.0") (hash "1sf0dm0vayn60nagi1ynwwf6gh39fwl7cjbczfxw5ppwqgrhsrbb")))

(define-public crate-zamac-0.1 (crate (name "zamac") (vers "0.1.0") (hash "06rmqyw98rzr3ilsj86gl3r6k20n9iqqzcvxjlp9v1lwqpv9s0q0")))

