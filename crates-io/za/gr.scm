(define-module (crates-io za gr) #:use-module (crates-io))

(define-public crate-zagreus-0.1 (crate (name "zagreus") (vers "0.1.0") (hash "1gvc3x4aqwzp7lr9gjhb474fh09hx51z402g5vdj9c6ic71li4l8") (features (quote (("nightly_can_backoff"))))))

(define-public crate-zagreus-0.1 (crate (name "zagreus") (vers "0.1.1") (hash "1hrsp8q5wma6v0j0lzp25z5bllfrd00j34x8dmh3d5ajm96hsagl") (features (quote (("nightly_can_backoff"))))))

