(define-module (crates-io za ch) #:use-module (crates-io))

(define-public crate-zach_test_crate-0.1 (crate (name "zach_test_crate") (vers "0.1.0") (hash "144ra27sp2nlkr95w9qniyzk3l9lfkp70vkncms5aam0v40vvf7g") (yanked #t)))

(define-public crate-zachs18-stdx-0.1 (crate (name "zachs18-stdx") (vers "0.1.0") (hash "11n09wn5yr4gbflfi2qggrph2kwm0a6mvhgbs1kx8ibfjxlf4crp") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zachs18-stdx-0.1 (crate (name "zachs18-stdx") (vers "0.1.1") (hash "1x00pc01b07bcwxh2bzpgwagqnw9nddakjqdpk5pnrdwyy7fyik4") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zachs18-stdx-0.1 (crate (name "zachs18-stdx") (vers "0.1.2") (hash "114z2z2nnrmg2ryrh815cpaq10brccyk8jr7mj1w5hasqxmxgv98") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zachs18-stdx-0.1 (crate (name "zachs18-stdx") (vers "0.1.3") (hash "0200v1fz70m1i859bzyhjbvrbnnc1222qpjz53q3fz3c3x1nhbw6") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zachs18-testing-0.1 (crate (name "zachs18-testing") (vers "0.1.0") (hash "1vfw33hnr9fgi3bmj7rjrx0y05jc4kl1dzim4xprxmkr4x4yxy84")))

(define-public crate-zachs18-testing-0.3 (crate (name "zachs18-testing") (vers "0.3.0") (hash "07jyj1ck0fllj4n7cx4inxd7i6n16xxs4bng9s26s0k61gf73n05")))

(define-public crate-zachs18-testing-0.2 (crate (name "zachs18-testing") (vers "0.2.0") (hash "1pp48vwnbv8kb9hwm56m389s4pddkrgakzn6623sf8b5v7f6r3p8")))

(define-public crate-zachs18-testing-0.4 (crate (name "zachs18-testing") (vers "0.4.0") (hash "0b3alv1rgp7gi9rp4cwqcknjsx71lz5zs2y8zfpi5c7997pjcys2")))

(define-public crate-zachsbot-0.0.0 (crate (name "zachsbot") (vers "0.0.0") (deps (list (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "0ryzb0rljz1a9ny35cz6y7dc7fcnmb357hjypxl5mfjq99ymzy1y") (yanked #t)))

(define-public crate-zachsbot-0.0.0 (crate (name "zachsbot") (vers "0.0.0-a") (deps (list (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "0r15ahyb711lj1sjrwy8l52f4z5mgbk7wn0732bsmsw9m9ppzwa3")))

(define-public crate-zachsbot-0.0.1 (crate (name "zachsbot") (vers "0.0.1") (deps (list (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "0yihix5hsvc51sqbb5qb5p1s091pgpgl95n64a4mdc4yhyv1ipbc")))

(define-public crate-zachsbot-0.0.2 (crate (name "zachsbot") (vers "0.0.2") (deps (list (crate-dep (name "axum") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1mccrjkcx20sgzvp3qz7c8mr635fcdh97df70dxs1sa240pgclg2")))

