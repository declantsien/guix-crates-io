(define-module (crates-io za ng) #:use-module (crates-io))

(define-public crate-zang-0.1 (crate (name "zang") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "02765gg7z5a30mq3v957j5zm1w8g592xh4wzjfpgcm12qji77mdy") (yanked #t)))

(define-public crate-zang-0.1 (crate (name "zang") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1gh5jr3gysa8nrc0a2n1sd5gip633sdm7kb9d5bszv8ghcilb860")))

(define-public crate-zang-macro-0.1 (crate (name "zang-macro") (vers "0.1.0") (hash "0409jnajszm8967gdhadk0xg56dqxzw7l3bqnkzi09ps7n3mlhfn")))

(define-public crate-zang-macro-0.1 (crate (name "zang-macro") (vers "0.1.1") (hash "0ch72zzf0f4wnjrzgbf1bz44bglyhqkjdlvhhrg8rz5xkm33476f")))

