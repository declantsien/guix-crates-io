(define-module (crates-io za rt) #:use-module (crates-io))

(define-public crate-zarthus_env_logger-0.1 (crate (name "zarthus_env_logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "09ligffn0r27di3j5gyr7caba3fknpcjp3azmvdmi4544mip9kky") (features (quote (("default" "chrono"))))))

(define-public crate-zarthus_env_logger-0.2 (crate (name "zarthus_env_logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0db2b3c6h8yra471cm59w6da90vjq0q46z42c7gc2kivwzffrlwk") (features (quote (("default" "chrono"))))))

(define-public crate-zarthus_env_logger-0.3 (crate (name "zarthus_env_logger") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (features (quote ("macros" "formatting" "parsing"))) (optional #t) (default-features #t) (kind 0)))) (hash "0is0msar2wq719hi2wljdhvcldbk3iccm53wdsbmm7vsm8nk0dw3") (features (quote (("default" "chrono"))))))

