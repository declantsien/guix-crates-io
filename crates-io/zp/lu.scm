(define-module (crates-io zp lu) #:use-module (crates-io))

(define-public crate-zplus-0.9 (crate (name "zplus") (vers "0.9.3-rc.1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thrift") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bdfxisjc1mni5063sd2ax6866brni0nzv47sbijn7bqimmk1gki")))

