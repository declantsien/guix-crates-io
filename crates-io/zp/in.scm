(define-module (crates-io zp in) #:use-module (crates-io))

(define-public crate-zpinger-0.1 (crate (name "zpinger") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)))) (hash "0an0g4cnnv74xfvknwnks55ssmqsq27v6rfvbmm92ms9rk9i2xaz")))

