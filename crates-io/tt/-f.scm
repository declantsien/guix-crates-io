(define-module (crates-io tt -f) #:use-module (crates-io))

(define-public crate-tt-framework-0.0.3 (crate (name "tt-framework") (vers "0.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fr8fisnswsgszw644bph7gikii1068rz9pansd4q5cyys1n6847")))

