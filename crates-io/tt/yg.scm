(define-module (crates-io tt yg) #:use-module (crates-io))

(define-public crate-ttygrid-0.1 (crate (name "ttygrid") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req ">=0") (default-features #t) (kind 2)) (crate-dep (name "termion") (req ">=0") (default-features #t) (kind 0)))) (hash "08zybm6ziacmxlibfg93x315nlz2jwip72694v3hmi5l89lihma6")))

(define-public crate-ttygrid-0.2 (crate (name "ttygrid") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req ">=0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">=0") (default-features #t) (kind 2)) (crate-dep (name "termion") (req ">=0") (default-features #t) (kind 0)))) (hash "0jrdznz3x1l6q3v9512b5s8kjjnmnl3m60h19i36pb2dqxcxy1r1")))

(define-public crate-ttygrid-0.2 (crate (name "ttygrid") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req ">=0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">=0") (default-features #t) (kind 2)) (crate-dep (name "termion") (req ">=0") (default-features #t) (kind 0)))) (hash "06q9wm6rasn8gc3pra8j45v4d2vwdm0aiwmsjp86p20zrvpmcnpd")))

(define-public crate-ttygrid-0.2 (crate (name "ttygrid") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req ">=0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">=0") (default-features #t) (kind 2)) (crate-dep (name "termion") (req ">=0") (default-features #t) (kind 0)))) (hash "11f2nqs2fmkx09x76r2434xrzwsj8qwqhyrc7x21x2smkwwf2q07")))

(define-public crate-ttygrid-0.3 (crate (name "ttygrid") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "rand") (req ">=0") (default-features #t) (kind 2)))) (hash "12nz5skm6khg0wgy267q6rjfrs51dj3s8an55illwsvdyn795m70")))

