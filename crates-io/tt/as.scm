(define-module (crates-io tt as) #:use-module (crates-io))

(define-public crate-ttasm-0.1 (crate (name "ttasm") (vers "0.1.0") (deps (list (crate-dep (name "mtk") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0yxs83lfm9msch2clfgy0b6m2fj0nkzkbjr1h552zdarf7rii6p7") (yanked #t)))

(define-public crate-ttasm-0.1 (crate (name "ttasm") (vers "0.1.1") (deps (list (crate-dep (name "mtk") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1yzp1kh4r55b0w3vzdjfjkp6jb1h3iwcs4qmr0133y5ird9rz401") (yanked #t)))

(define-public crate-ttasm-0.1 (crate (name "ttasm") (vers "0.1.2") (deps (list (crate-dep (name "mtk") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0qpcb93kslas840rl97b5zbw0d920nwy6yh3s3830ckrd6fc17zh") (yanked #t)))

(define-public crate-ttasm-0.1 (crate (name "ttasm") (vers "0.1.3") (deps (list (crate-dep (name "mtk") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1nggnpmiyv9fl2jnz7mwsm35nq6bibqhm1jkwx2v6yqk923jl1li") (yanked #t)))

(define-public crate-ttasm-0.1 (crate (name "ttasm") (vers "0.1.4") (deps (list (crate-dep (name "mtk") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0g67gm0ljmmm8j95ip0c2gy1rrgr9ri1v4yjbvmjn1g3rgw5ir7z") (yanked #t)))

