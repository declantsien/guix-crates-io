(define-module (crates-io tt yd) #:use-module (crates-io))

(define-public crate-ttydo-0.1 (crate (name "ttydo") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.15") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "commandstream") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gymyiwwi4wcgfyy3ml34m5g281jx0ynx5brf3fzhdvr42xa5f70")))

