(define-module (crates-io tt km) #:use-module (crates-io))

(define-public crate-ttkmd-if-0.1 (crate (name "ttkmd-if") (vers "0.1.0") (deps (list (crate-dep (name "bitfield-struct") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "luwen-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1xpw40bn6ggy0smz5pmzrivv0rj3v2a8c9jnfv2gvsbv1hpbjlfr")))

