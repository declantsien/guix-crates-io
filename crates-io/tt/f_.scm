(define-module (crates-io tt f_) #:use-module (crates-io))

(define-public crate-ttf_word_wrap-0.1 (crate (name "ttf_word_wrap") (vers "0.1.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0a45c2lq6hidf0z2z6khc9a9f80nafjw1qcg03gh7i0b1sxk144a")))

(define-public crate-ttf_word_wrap-0.2 (crate (name "ttf_word_wrap") (vers "0.2.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1068105g0qj2f9iqacfbx2wj0lnp5s86716q3gmfbhgvw2f0l881")))

(define-public crate-ttf_word_wrap-0.2 (crate (name "ttf_word_wrap") (vers "0.2.1") (deps (list (crate-dep (name "ttf-parser") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0vs3k4qazn7c5w95acyld1z3dzxpzdhlxcbbl7hbgm7wykb1zk3l")))

(define-public crate-ttf_word_wrap-0.2 (crate (name "ttf_word_wrap") (vers "0.2.2") (deps (list (crate-dep (name "ttf-parser") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1565n862lfg92ddygpl21hmxbcyjx424qqd56r0wj77pd5l735s6")))

(define-public crate-ttf_word_wrap-0.3 (crate (name "ttf_word_wrap") (vers "0.3.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0f986l1bchxgmsgllhgrvplirvwq7qy63fskai2a3qbfvg37xqkw")))

(define-public crate-ttf_word_wrap-0.4 (crate (name "ttf_word_wrap") (vers "0.4.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "ttf-parser") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1zs8ka28vvbq31s6i6xvbhgagv9ji65ffrrx2lj86bb50gg65rsz")))

(define-public crate-ttf_word_wrap-0.4 (crate (name "ttf_word_wrap") (vers "0.4.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "ttf-parser") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "02xliiham74cc49ig31s0hxp2ipma371v3n2bwa7nkbfsj3q6y0s")))

(define-public crate-ttf_word_wrap-0.4 (crate (name "ttf_word_wrap") (vers "0.4.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "ttf-parser") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1f4h9piyhh77m328diliz9mnrzwc8n08bq5bsrh47g291y5albf2")))

(define-public crate-ttf_word_wrap-0.5 (crate (name "ttf_word_wrap") (vers "0.5.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "ttf-parser") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "113yhhqjak15cr8dx1x455c4b6cjaa59gkny92gxr194dl549wwg")))

