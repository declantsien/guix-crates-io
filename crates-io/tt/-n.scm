(define-module (crates-io tt -n) #:use-module (crates-io))

(define-public crate-tt-num-0.1 (crate (name "tt-num") (vers "0.1.0") (deps (list (crate-dep (name "tt-call") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1mnbcr18xdnx936jw2dzfrvpqlz775mdfap3xmjyfwykvgz9p8vr")))

