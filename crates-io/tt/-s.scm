(define-module (crates-io tt -s) #:use-module (crates-io))

(define-public crate-tt-server-0.1 (crate (name "tt-server") (vers "0.1.0") (deps (list (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pphgvqg1iv35b9mprnsclnllf6hc9kbx6590xdrdhqkgi22zk1a")))

