(define-module (crates-io tt ye) #:use-module (crates-io))

(define-public crate-ttyecho-0.1 (crate (name "ttyecho") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "0775vfkcjy9c0q82z57xpmvzgyr0wp4xx4smfkpv2m31z80yz9bx")))

(define-public crate-ttyecho-0.1 (crate (name "ttyecho") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "06xlb9c0sx1ly63rb95zyfg7ih6gk1bilffiggdsqn1qb3m27dwy")))

(define-public crate-ttyecho-0.1 (crate (name "ttyecho") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "1fj0f9vbiai8w5yhcv594m9c0apa44g7mvx7yg4rdbcppnadklaa")))

