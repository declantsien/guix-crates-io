(define-module (crates-io tt -c) #:use-module (crates-io))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "1rd18kxngr94y8yd0al5ps9jrssbmrqnih8rpxzm2wc9wsvqf3rp")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "0r79rkgy3l9k5v51h1g2vwyk9xhf2wzfzc6d7bxdzr3cvh3r380x")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "1qynn11379n8izg9kzyfh9ad5h4fsm34k487l57iaxrrndsm2hid")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.3") (deps (list (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "1d8nhcwnn9gxfkpa7c84bvl609warf2vvqcqa2yr57smix5xvh0q")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.4") (deps (list (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "01jrm4nqxwabrcrw5697310qdlhc5glgg0645lixkv6m4r5060lm")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.5") (deps (list (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "0mkmvqds3nian5anlsc8170vhq7mrdz8k4fwh356a0yym3nkizq6")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.6") (deps (list (crate-dep (name "rustversion") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "19jb7b6drkn0ipwylaizj51gk5r66p4kvrk0xgj4w3045brm6x1n")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.7") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.19") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0vl324csl0b1hdvi58alkf6a1r7pp4v7znsv5wvyhjhpzcbzhyd8")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.8") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.19") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0md03vvmq18k3hsx8vcw237cg9cy8v17czn50gfnj319qjzdqrjy")))

(define-public crate-tt-call-1 (crate (name "tt-call") (vers "1.0.9") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1pw1k05bc73gdqryvl7w928kj1jg5fxw8n2sxr76408rhpyrbwgl") (rust-version "1.31")))

(define-public crate-tt-client-0.1 (crate (name "tt-client") (vers "0.1.0") (deps (list (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "0la83rc8idg3jfmcy96jqlr7nv5vw3bb5ivb33rg9q6rm2y0z8z3")))

