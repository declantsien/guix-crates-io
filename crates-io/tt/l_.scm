(define-module (crates-io tt l_) #:use-module (crates-io))

(define-public crate-ttl_cache-0.1 (crate (name "ttl_cache") (vers "0.1.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cdrbhh2nw0jrz9g2x1zmg4xz1rhy01jhr7h2x2ld8ggkwrwyl7x") (yanked #t)))

(define-public crate-ttl_cache-0.1 (crate (name "ttl_cache") (vers "0.1.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0k8kjjmm01c9fgslkkdkvq5sxmdzfma15r6r3wsj83kwk54pq2cc")))

(define-public crate-ttl_cache-0.2 (crate (name "ttl_cache") (vers "0.2.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lv44cdyri65px36m1ih34jc7984yyp3cmvvcsnbkkcvwan24vwv")))

(define-public crate-ttl_cache-0.2 (crate (name "ttl_cache") (vers "0.2.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xy1rl98k1a3xxpj7jnvn76625pi4vmc3zxzq6xj5vvg5ww1l41x")))

(define-public crate-ttl_cache-0.3 (crate (name "ttl_cache") (vers "0.3.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.4") (default-features #t) (kind 0)))) (hash "1n5vmi0ss0k90dar7w48g86c49cw51xbkksgh8harmclk1ri4fk9")))

(define-public crate-ttl_cache-0.4 (crate (name "ttl_cache") (vers "0.4.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)))) (hash "15m67lfyz4vm25fqagkn6qix24jdb8p2d3pk952wrmwhcmwabivc") (yanked #t)))

(define-public crate-ttl_cache-0.4 (crate (name "ttl_cache") (vers "0.4.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)))) (hash "12habcj2k4dn9hdmdjckh2zj2w3xclf7xqpm1sqv5rmifsmbc64l") (features (quote (("stats") ("default"))))))

(define-public crate-ttl_cache-0.4 (crate (name "ttl_cache") (vers "0.4.2") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)))) (hash "1nv8rifvc23plw2397rnzwx1j7ap0lhaj9lsivzxz5kqm779glnl") (features (quote (("stats") ("default"))))))

(define-public crate-ttl_cache-0.5 (crate (name "ttl_cache") (vers "0.5.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)))) (hash "1xmwhgqdkmyj1ibhhfkdwq8bcc3plrxrkapkpf71gn4zdhq3pqra") (features (quote (("stats") ("default"))))))

(define-public crate-ttl_cache-0.5 (crate (name "ttl_cache") (vers "0.5.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)))) (hash "0sh9jajbl9nb6g4yx31i2j5c8q0lvypcwrgfnq88f5ph4q2qk2a1") (features (quote (("stats") ("default"))))))

(define-public crate-ttl_cache_with_purging-0.1 (crate (name "ttl_cache_with_purging") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "sync" "time"))) (default-features #t) (kind 0)))) (hash "0k8qlwp8h4wczc5dj52ipvh1hvg960y7myh2aafhbcd6caasknqs")))

