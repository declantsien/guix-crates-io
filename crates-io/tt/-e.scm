(define-module (crates-io tt -e) #:use-module (crates-io))

(define-public crate-tt-engine-0.1 (crate (name "tt-engine") (vers "0.1.0") (deps (list (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "1c10vlrl6ydg7rs1a6qanqvffpi51bj8cld11hi0b62m2cgg10g0")))

(define-public crate-tt-equal-0.1 (crate (name "tt-equal") (vers "0.1.0") (deps (list (crate-dep (name "tt-call") (req "^1.0") (default-features #t) (kind 0)))) (hash "01izrip8spc6k7njdqmwrnkw184ai5wrk6npp2s4n7wvhibfz5mw")))

(define-public crate-tt-equal-0.1 (crate (name "tt-equal") (vers "0.1.1") (deps (list (crate-dep (name "tt-call") (req "^1.0") (default-features #t) (kind 0)))) (hash "05cabr3x3xp128v8j0l3z9b235826akfbjmvlzmc413mj9xpq2va")))

(define-public crate-tt-equal-0.1 (crate (name "tt-equal") (vers "0.1.2") (deps (list (crate-dep (name "tt-call") (req "^1.0") (default-features #t) (kind 0)))) (hash "16qj9az2fq3n2m5cmppg1d2xm3khx2q8qwkd1f135bqch30mlimy")))

