(define-module (crates-io tt #{21}#) #:use-module (crates-io))

(define-public crate-tt21100-0.1 (crate (name "tt21100") (vers "0.1.0") (deps (list (crate-dep (name "bondrewd") (req "^0.1.14") (features (quote ("derive"))) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1ipgi3jgkiw7ml7vmibdgf0mlq0qqw290fjgil3vh3h948ia78vg") (rust-version "1.58")))

(define-public crate-tt21100-async-0.1 (crate (name "tt21100-async") (vers "0.1.0") (deps (list (crate-dep (name "bondrewd") (req "^0.1.14") (features (quote ("derive"))) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "1h6cvv9ncrwp51701l18hxjgbdn0inj9v3hdrdyg7wkgfyxak23j") (rust-version "1.58")))

