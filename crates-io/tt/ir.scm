(define-module (crates-io tt ir) #:use-module (crates-io))

(define-public crate-ttir-0.1 (crate (name "ttir") (vers "0.1.1") (deps (list (crate-dep (name "mtk") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "01xmlvpi7pmbkgdmbhazwg3xm0d356p3088k5xd261kn70arw6h1") (yanked #t)))

(define-public crate-ttir-0.1 (crate (name "ttir") (vers "0.1.2") (deps (list (crate-dep (name "mtk") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "102yklcfkrsqrixmjlqpw747nd6n9zwgb8nijac9a9lgwvxsswkx") (yanked #t)))

(define-public crate-ttir-0.1 (crate (name "ttir") (vers "0.1.3") (deps (list (crate-dep (name "mtk") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0433zkv3i5grs9q53666l7ppdka4lj18n4r1nyb8hx6zz8pfy3gm") (yanked #t)))

(define-public crate-ttir-0.1 (crate (name "ttir") (vers "0.1.4") (deps (list (crate-dep (name "mtk") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rsap") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "ttvm") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "001hj6vrra34h1595pia3z7bccxshrajw2nx5ls4ya9i8ksm2wf2") (yanked #t)))

