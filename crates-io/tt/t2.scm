(define-module (crates-io tt t2) #:use-module (crates-io))

(define-public crate-ttt2023-0.1 (crate (name "ttt2023") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0zirb1yfqf60n810zsma6izxhlk3bw7vxh6jzjsvy5ylhmqqyp9z")))

