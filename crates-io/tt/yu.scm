(define-module (crates-io tt yu) #:use-module (crates-io))

(define-public crate-ttyui-0.1 (crate (name "ttyui") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "19fji19hijhdlg934y4ksgwbnpzycn7gn673r7052km2vqf9zkk5")))

(define-public crate-ttyui-0.1 (crate (name "ttyui") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1fsg9fw57kwmwhv594yry5xa76dn73hi4brmv81f16fgx92jyrac")))

(define-public crate-ttyui-0.1 (crate (name "ttyui") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0zzirz7mjm4mfi4pdy2k15xjmiqavgznxw42j7mnlgibd9b1k4l8")))

(define-public crate-ttyui-0.1 (crate (name "ttyui") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "08kx7lld4l8fkg82d86hzb5rzniww4dbxp22mrsnxhadkcjl85l4")))

(define-public crate-ttyui-0.1 (crate (name "ttyui") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0m7qrsp0cpr00q5n7vdxq9g251pw7ayhxqbxczx8j8schpkjj26b")))

