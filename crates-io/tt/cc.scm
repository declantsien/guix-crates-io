(define-module (crates-io tt cc) #:use-module (crates-io))

(define-public crate-ttcc-0.0.0 (crate (name "ttcc") (vers "0.0.0") (hash "0ph56nxzqqnqcvna1gpl6ldlyr1b4vzchnh660mc1b99abvn9d7v") (yanked #t)))

(define-public crate-ttcc-0.0.1 (crate (name "ttcc") (vers "0.0.1") (hash "05f4kmx2q0zca0jyvlqqpdmvq4vpvfas9wr5fzak3848n0x9s251") (yanked #t)))

