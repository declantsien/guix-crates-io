(define-module (crates-io tt s_) #:use-module (crates-io))

(define-public crate-tts_rust-0.1 (crate (name "tts_rust") (vers "0.1.0") (deps (list (crate-dep (name "gtts") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1gvasihsq4dnzxwbk6gqdby76nk9ybff6mnd0amj83li6zxhm9h2") (yanked #t)))

(define-public crate-tts_rust-0.1 (crate (name "tts_rust") (vers "0.1.1") (deps (list (crate-dep (name "gtts") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0f29kl3wy3jna5kqchdx1vf3bjyljrc71pw5cka574wdsn1lsvc4")))

(define-public crate-tts_rust-0.1 (crate (name "tts_rust") (vers "0.1.2") (deps (list (crate-dep (name "gtts") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "15va31h2vp7sf4r7lzvr680k9v2syzv89jc8nzipbhx4lxi2x09v")))

(define-public crate-tts_rust-0.1 (crate (name "tts_rust") (vers "0.1.3") (deps (list (crate-dep (name "gtts") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1jzmd0srb0ixfykabcnmcgd35mw40fl907l8hnn2icvyx45iv6b6")))

(define-public crate-tts_rust-0.1 (crate (name "tts_rust") (vers "0.1.4") (deps (list (crate-dep (name "gtts") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1dhw8a0ykbx57xq6n94rfp2msn48myzb7anwb0ydqn8w72742h03")))

(define-public crate-tts_rust-0.2 (crate (name "tts_rust") (vers "0.2.0") (deps (list (crate-dep (name "gtts") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0ilr2r3pg23jn04mnss5rzhf2lxaj1ws508m2grcc4mn16dwk3qi")))

(define-public crate-tts_rust-0.3 (crate (name "tts_rust") (vers "0.3.0") (deps (list (crate-dep (name "minreq") (req "^2.0.3") (features (quote ("https"))) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0gjqh4qrny9r51j61sm4wbz1bnr69dn9y4h25hqhvgiziggf16m5")))

(define-public crate-tts_rust-0.3 (crate (name "tts_rust") (vers "0.3.1") (deps (list (crate-dep (name "minreq") (req "^2.0.3") (features (quote ("https"))) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0d81fc4af0inp059d2i30xai4xvxdhb1kiq530idvdlyk438s8kh")))

(define-public crate-tts_rust-0.3 (crate (name "tts_rust") (vers "0.3.2") (deps (list (crate-dep (name "minreq") (req "^2.0.3") (features (quote ("https"))) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1z0lyggwd85bxwfhr7y04ixawzkmas6mhhwm964b8iwbpq9hrj62")))

(define-public crate-tts_rust-0.3 (crate (name "tts_rust") (vers "0.3.3") (deps (list (crate-dep (name "minreq") (req "^2.0.3") (features (quote ("https"))) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0kdym9rrcvjl6cfrmllbb13f26jknimspcigakvv291223ds4cc8")))

(define-public crate-tts_rust-0.3 (crate (name "tts_rust") (vers "0.3.5") (deps (list (crate-dep (name "minreq") (req "^2.0.3") (features (quote ("https"))) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1w75f64jmzzabdjzhrm2n5k3vq2msmpw0igxfkzi9rcspfnwvmrd")))

(define-public crate-tts_winrt_bindings-0.1 (crate (name "tts_winrt_bindings") (vers "0.1.0") (deps (list (crate-dep (name "winrt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winrt") (req "^0.7") (default-features #t) (kind 1)))) (hash "1h5ckj6am5f22gvfbwdsrycn90z2sm5342af4h41gf118w227992")))

(define-public crate-tts_winrt_bindings-0.2 (crate (name "tts_winrt_bindings") (vers "0.2.0") (deps (list (crate-dep (name "winrt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winrt") (req "^0.7") (default-features #t) (kind 1)))) (hash "0q8l3rm1glcgjlphmc92lfh4yzd9r7mb60b0g9phfry002r17ky0")))

