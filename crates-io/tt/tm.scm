(define-module (crates-io tt tm) #:use-module (crates-io))

(define-public crate-tttm-1 (crate (name "tttm") (vers "1.0.0-alpha.6") (hash "14b3p82dcbxwvb77ddd6f1ckjsva9adaxc8qmsz1bvm850rw02m1")))

(define-public crate-tttm-1 (crate (name "tttm") (vers "1.0.0-alpha.5") (hash "1a0ay6mzsp1ji5kfz765ym1jrl2yrmd8xccbjz1nxbg1bp2xzv9f")))

(define-public crate-tttm-1 (crate (name "tttm") (vers "1.0.0-alpha.7") (hash "1l5ljb13z5ca7q9xd1rxdrv0ax25km2pf26ckpw9wcbwlbxy6ycr")))

