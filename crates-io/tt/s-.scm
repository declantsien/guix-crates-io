(define-module (crates-io tt s-) #:use-module (crates-io))

(define-public crate-tts-cli-0.1 (crate (name "tts-cli") (vers "0.1.0") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)))) (hash "0ncky4qi0hp7wvbxwsl0grdcxpvyb7hq95vmcjmf8xs2cgf3qmzs")))

(define-public crate-tts-cli-0.1 (crate (name "tts-cli") (vers "0.1.1") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)))) (hash "05xd4fi6qd5mvc5yzldq6gwd7k7hz7fxhbflarvci25lgnbmihy9")))

(define-public crate-tts-cli-0.1 (crate (name "tts-cli") (vers "0.1.2") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)))) (hash "0b58hm62w1qgndihv1vgr2bql4npfi07i8r2y6yacv04q7bpn82s") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-cli-0.1 (crate (name "tts-cli") (vers "0.1.3") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)))) (hash "0r7kikl2qrdgy4sh13cs1fjkzwa7nbzksly4by1l5ra8ba3fgx3p") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-cli-0.1 (crate (name "tts-cli") (vers "0.1.4") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)))) (hash "04510l9ys4p023wisk11n4h0w4745xbgkch93qs0vb7ky9il38ls") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-external-api-0.1 (crate (name "tts-external-api") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "141grbydqdp8f5bqdnvcci43basp782q9d8zc5200wdqxvcnk3jl")))

(define-public crate-tts-external-api-0.1 (crate (name "tts-external-api") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "137z70aqpqf6wjccvjsbs1pqj2sys5zc35j3jgpnwz90gqsmg25x")))

(define-public crate-tts-external-api-0.1 (crate (name "tts-external-api") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "038iwlj6xjfi9x7a21xyvmdb856dxl3vz4shj8sv2jf1yfb8sdgp")))

(define-public crate-tts-external-api-0.1 (crate (name "tts-external-api") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1rlb49l30imq1vymh54pvr16kl1f1a0ln28rjybrqyp9cj46fi3z")))

(define-public crate-tts-external-api-0.1 (crate (name "tts-external-api") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0kqg8455sbrx65apfypwp6d9h07fnxac0712rdzm460mnp271w84")))

(define-public crate-tts-gui-0.0.0 (crate (name "tts-gui") (vers "0.0.0") (hash "0c2jlcn51yak6n1d2f4107disc5jlnc1786kch9l8saamc5imj80")))

(define-public crate-tts-gui-0.0.1 (crate (name "tts-gui") (vers "0.0.1") (hash "1pfir5r4y8c1g3551pbj1hqdm5fayp145vrzqhday6b5sgizvq6n")))

(define-public crate-tts-tui-0.0.0 (crate (name "tts-tui") (vers "0.0.0") (hash "1a1a1hd6mrdlpvq201x1aqkgbr04xxy8c9dvwfr22x9ix14bbm7y")))

(define-public crate-tts-tui-0.0.1 (crate (name "tts-tui") (vers "0.0.1") (hash "0j4qp88xvmlvliqca39c5pvigf4bhyiaiw1rdmx33hb8364qfi77")))

(define-public crate-tts-tui-0.1 (crate (name "tts-tui") (vers "0.1.0") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)) (crate-dep (name "tui") (req "^0.20.1") (default-features #t) (kind 0) (package "ratatui")))) (hash "1w3pgni9dals109m0z3rcdfszz8d4qaimb1cmw4lqzic3n5y1cif") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1 (crate (name "tts-tui") (vers "0.1.1") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)) (crate-dep (name "tui") (req "^0.20.1") (default-features #t) (kind 0) (package "ratatui")))) (hash "0q9dwr5v2ihb8ivx8yp7qbglpgbnjaycry1yqqnxdyqklkm4b6fv") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1 (crate (name "tts-tui") (vers "0.1.2") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)) (crate-dep (name "tui") (req "^0.21.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "1v822by85963iskvapnyf30i7rr32f4w9nm21nhzgrvv5ak5lm5h") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1 (crate (name "tts-tui") (vers "0.1.3") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)) (crate-dep (name "tui") (req "^0.21.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "1q3w64kw169hi6vzj28nsgn7nwhwbj7whf7ya9nx8dpyadp04d64") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-tui-0.1 (crate (name "tts-tui") (vers "0.1.4") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (kind 0)) (crate-dep (name "tts") (req "^0.25.5") (kind 0)) (crate-dep (name "tui") (req "^0.21.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "0ay8dnp667dhf0ls6h7zhffd6ywnb6zmi5c2cr7mgjha7a5bq2zd") (features (quote (("default" "11") ("9" "tts/speech_dispatcher_0_9") ("11" "tts/speech_dispatcher_0_11") ("10" "tts/speech_dispatcher_0_10"))))))

(define-public crate-tts-urls-0.2 (crate (name "tts-urls") (vers "0.2.0") (deps (list (crate-dep (name "percent-encoding") (req "^2") (default-features #t) (kind 0)))) (hash "0h8kaalfnzvlzszmm2klqgnkl7kwffaf04099i2s17cwkicq49cx")))

(define-public crate-tts-urls-0.2 (crate (name "tts-urls") (vers "0.2.1") (deps (list (crate-dep (name "percent-encoding") (req "^2") (default-features #t) (kind 0)))) (hash "0ln6mag1ip82chs5r5fqr75s4y1m9l5rd91wp73arkywjcfnyn8b")))

(define-public crate-tts-urls-0.3 (crate (name "tts-urls") (vers "0.3.0") (deps (list (crate-dep (name "percent-encoding") (req "^2") (default-features #t) (kind 0)))) (hash "0zbffxdv3q304c0dp5gsk3ps65plpil2hsyicwd8nmza2bhfkb42")))

(define-public crate-tts-urls-1 (crate (name "tts-urls") (vers "1.0.0") (deps (list (crate-dep (name "percent-encoding") (req "^2") (default-features #t) (kind 0)))) (hash "1zlpvg8x6s962zx19shp7qmsfzl5m0lljkwdb5m0hhlmd8am68g7")))

