(define-module (crates-io tt m-) #:use-module (crates-io))

(define-public crate-ttm-rs-0.1 (crate (name "ttm-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 1)))) (hash "0jl80cfwnnml2l5vihpawkjid3snsri6vkaqqalv9fij7hfnphvj")))

