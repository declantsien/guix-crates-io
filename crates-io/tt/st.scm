(define-module (crates-io tt st) #:use-module (crates-io))

(define-public crate-ttstack-0.0.1 (crate (name "ttstack") (vers "0.0.1") (deps (list (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pbvcwslnq16iidk2myphjiqjvw0hpv54kmzs6v03ql22jck5yqp")))

(define-public crate-ttstack-0.0.2 (crate (name "ttstack") (vers "0.0.2") (deps (list (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)))) (hash "0kq8lqjarfkzyrnaxff30kyzs2iilbhv3a3c2yif37bzf721rsz1")))

(define-public crate-ttstack-0.0.3 (crate (name "ttstack") (vers "0.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ruc") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ih9vbxj38xj6i2vwd3zwdw00v255mh0mcfkirl50sk0h6r0fv99")))

