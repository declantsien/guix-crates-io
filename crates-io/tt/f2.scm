(define-module (crates-io tt f2) #:use-module (crates-io))

(define-public crate-ttf2cp437-1 (crate (name "ttf2cp437") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8") (default-features #t) (kind 0)))) (hash "1r7cc87f5jmqrriv60smdj8yiw5p93x8wgn5adj3yn453v62dn2l")))

(define-public crate-ttf2mesh-0.0.1 (crate (name "ttf2mesh") (vers "0.0.1") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "175zirmh3kcb84i81n816680f7syyl2h25i433c99d5g27f8m4cc")))

(define-public crate-ttf2mesh-0.0.2 (crate (name "ttf2mesh") (vers "0.0.2") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "055val8ywn9dllypi63393dh86mp5dx1xm9sksx5qyab0x5mw37y")))

(define-public crate-ttf2mesh-0.0.3 (crate (name "ttf2mesh") (vers "0.0.3") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0nwiphzxx1c54baizad8wc91rklj1vxpq9fbmw23cayja2j962b6")))

(define-public crate-ttf2mesh-0.0.4 (crate (name "ttf2mesh") (vers "0.0.4") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0zgyz1hr065jxqn9a8m745sm5nnsvqxnisfk6gvmf2hgcbawi1pg")))

(define-public crate-ttf2mesh-0.0.5 (crate (name "ttf2mesh") (vers "0.0.5") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0inki38vxy9fvhnc9213z6388af9fqb8a5xjwgxsxdh80nhxnvrg")))

(define-public crate-ttf2mesh-0.0.6 (crate (name "ttf2mesh") (vers "0.0.6") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1dflka44hnz63vszdmi3va2bf7hpyp7yifcbp38mq3dirmkb8w6z") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.7 (crate (name "ttf2mesh") (vers "0.0.7") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1akfz8azg5qfdczsvi6cdii2ncfvmvh2zm32fc90783dp0vcdbw7") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.8 (crate (name "ttf2mesh") (vers "0.0.8") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1270srn3m3n01fw1645dfv9kmqa70hqm1hxfz85idf78lfkzq562") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.9 (crate (name "ttf2mesh") (vers "0.0.9") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1q43380ic6r46n7vnramds8wkj0aspm759qh166crc2c3bbfm39r") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.10 (crate (name "ttf2mesh") (vers "0.0.10") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1pj1nanaadyw8axhqvygwz0clmbp8qasgc11xgh8diw3kml4766y") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.0.11 (crate (name "ttf2mesh") (vers "0.0.11") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1bnzgf9p4gjfhmzh96wv96fkr83mdffrim1dzjj2m6xrm21194br") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.1 (crate (name "ttf2mesh") (vers "0.1.0") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0sc9qwp51hfggi6qp8c1f6x61ng6pcxq5hjjii5zfw5fyqsj4n26") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.1 (crate (name "ttf2mesh") (vers "0.1.2") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "071anqz3vs09n797v73qk6zb2zsymhccv3ha1njjpc1za13bnmir") (features (quote (("unstable") ("default"))))))

(define-public crate-ttf2mesh-0.2 (crate (name "ttf2mesh") (vers "0.2.0") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ywfrc31lpqvq1ikc9c8nflbg4jpcwkv187p4j9rxf82cjnq8abv") (features (quote (("unstable") ("default")))) (rust-version "1.43")))

(define-public crate-ttf2mesh-0.2 (crate (name "ttf2mesh") (vers "0.2.1") (deps (list (crate-dep (name "ttf2mesh-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "00dvx0x4225sd9azqi5xhlyxnczvfmp42hcssj7f259sjvnng3pa") (features (quote (("unstable") ("default")))) (rust-version "1.43")))

(define-public crate-ttf2mesh-sys-0.0.1 (crate (name "ttf2mesh-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "16c68mlfila1h6s0fiy4lhh9sb98wib8fcihzqhbxpsgnnb5py9f")))

(define-public crate-ttf2mesh-sys-0.0.2 (crate (name "ttf2mesh-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1wpywsrddqwm4kqayw1pnpd1m3ckvg0kpkj1k2pxhaciyqrrfxyc")))

(define-public crate-ttf2mesh-sys-0.1 (crate (name "ttf2mesh-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "00y0712i7zvzyjgcgdils4q510gmb7jcr8ml5qb7z0pkv6k5cfvx")))

(define-public crate-ttf2mesh-sys-0.1 (crate (name "ttf2mesh-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0rrpvdhndj2cz7m4865c4az3xb68k2n3ym8hgxwr4af1g32wpx1g")))

(define-public crate-ttf2mesh-sys-0.1 (crate (name "ttf2mesh-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1mhchaqaq1hb9qzw4bf3cafq3bfl1dmq8b5hch6q0pic4w80fpmg")))

(define-public crate-ttf2mesh-sys-0.1 (crate (name "ttf2mesh-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0krs4vfirkpk5skzrx5a18rqg7v480zdswp0xc928d37vfp2a7ij")))

