(define-module (crates-io tt yl) #:use-module (crates-io))

(define-public crate-ttyl-0.1 (crate (name "ttyl") (vers "0.1.0") (hash "1wwq4wy10rys1hg2m3dfs548qhqxf411c9ps4vjdd2f8xkcn5xzq")))

(define-public crate-ttyloop-0.1 (crate (name "ttyloop") (vers "0.1.0") (deps (list (crate-dep (name "aglet") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0l8h6li5qp7x886hcg6yhljaknz4q2jdbdr37hnv54cwskhf2wsn")))

(define-public crate-ttyloop-0.1 (crate (name "ttyloop") (vers "0.1.1") (deps (list (crate-dep (name "aglet") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "172b08dni2780jpynmzbym0qbhvdqawfc5dh2ysvhhkf5g4jhbqp")))

(define-public crate-ttyloop-0.2 (crate (name "ttyloop") (vers "0.2.0") (deps (list (crate-dep (name "aglet") (req "^0.5.1") (kind 0)) (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "13n818v5lfjc405a397yy39fsjlddizraxsnjlp29dq0w4rw7yl5")))

