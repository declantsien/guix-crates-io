(define-module (crates-io tt #{-i}#) #:use-module (crates-io))

(define-public crate-tt-identifier-0.1 (crate (name "tt-identifier") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1vddf9q94phlk9nl7d5fas4hln3wgxnzqkb9fj4i7h6q9jgrraiy") (yanked #t)))

(define-public crate-tt-identifier-0.1 (crate (name "tt-identifier") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0qzmmh0582axpagrq8x2j3l4cm97f0qbplmhyg9pngz448bd1n1c")))

