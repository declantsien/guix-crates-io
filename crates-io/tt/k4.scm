(define-module (crates-io tt k4) #:use-module (crates-io))

(define-public crate-ttk4145_elevator_server-0.1 (crate (name "ttk4145_elevator_server") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.34") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0zqqvnb35w8kzfs4ifqh0jirzhcpmkd79q3fx8y52xinxjfy5fnv")))

(define-public crate-ttk4145_elevator_server-0.1 (crate (name "ttk4145_elevator_server") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.34") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "109x61zqfib5ib6valvvmbsqjpklb6gxqmb0hhfjmfganmk6nczf")))

(define-public crate-ttk4145_elevator_server-0.1 (crate (name "ttk4145_elevator_server") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.34") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0xhkqbsqjfb4px1rmwax7rnlyn5iiw96r6257721d3d80rsqr4gi")))

(define-public crate-ttk4145_elevator_server-0.1 (crate (name "ttk4145_elevator_server") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.34") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1shgjmpxgz191g00wh263zfl4km7sjr6qn16mj0sb1z3ws2zi4mb")))

