(define-module (crates-io tt l-) #:use-module (crates-io))

(define-public crate-ttl-queue-0.1 (crate (name "ttl-queue") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("time"))) (optional #t) (kind 0)))) (hash "1pdxjzqzky3ihdxy3lpyx0dyw308qyv5dkwhhl89cdns1dy6fdn0") (features (quote (("vecdeque") ("doublestack") ("default" "vecdeque")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-ttl-queue-0.2 (crate (name "ttl-queue") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("time"))) (optional #t) (kind 0)))) (hash "1v6yk5f9yzm1ilr7fw0pnp1422jdhh63f4j0vs4mfay4q4nwvv3z") (features (quote (("vecdeque") ("doublestack") ("default" "vecdeque")))) (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

