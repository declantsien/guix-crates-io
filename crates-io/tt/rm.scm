(define-module (crates-io tt rm) #:use-module (crates-io))

(define-public crate-ttrm-0.1 (crate (name "ttrm") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tagged-hybrid") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rbyxzg2srx5kk5hws0y6s475ag81lianjgrk189npz57rrsfj29")))

