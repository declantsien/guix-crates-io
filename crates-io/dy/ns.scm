(define-module (crates-io dy ns) #:use-module (crates-io))

(define-public crate-dynsequence-0.1 (crate (name "dynsequence") (vers "0.1.0-alpha.1") (hash "1b1rcky7hkp1qi52brilbdlgpw778nydh9ww8vim2clsx38y7y27") (features (quote (("unstable") ("default"))))))

(define-public crate-dynsequence-0.1 (crate (name "dynsequence") (vers "0.1.0-alpha.2") (hash "0arqj0v2pkkmz3cf9spzhfhgy8h6005i1pwv9j1n40n3ml4rsaly") (features (quote (("unstable") ("default"))))))

(define-public crate-dynsequence-0.1 (crate (name "dynsequence") (vers "0.1.0-alpha.3") (hash "18dxlqgigfijjj820ml6d7kahn5j9minjjg4xgk8k1dfwhc0bp1m") (features (quote (("unstable") ("default"))))))

(define-public crate-dynsequence-0.1 (crate (name "dynsequence") (vers "0.1.0-alpha.4") (hash "1b0802w3li7cixqqigwmf585bh7jwddf25h18nnhxm6zhgd9y78p") (features (quote (("unstable") ("default"))))))

(define-public crate-dynstack-0.1 (crate (name "dynstack") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1723hls0gg887630idwwpycs4ivz9yjqm61bznjj93kn8ax8s07j")))

(define-public crate-dynstack-0.1 (crate (name "dynstack") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "073mqaz08h55jafd8l3fl77pkd783rghw63s4hm4sf8pknar3lyr")))

(define-public crate-dynstack-0.2 (crate (name "dynstack") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1jbqrik6wmr5xd6xj6gkpx0a1fw531swx4kzyjvidhhg8yq6yxzs")))

(define-public crate-dynstack-0.2 (crate (name "dynstack") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0c4f2c322n4dmcqsjh9sk551liidjzq2x9b1lzlggnmn8wfanzmg")))

(define-public crate-dynstack-0.3 (crate (name "dynstack") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "03rd6lxgkk26a8qiax9z3r06qk9769b1gc0f9xis5ybzh8pvmq7i")))

(define-public crate-dynstack-0.4 (crate (name "dynstack") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "04kd67hbnd84sz60nsl01gd2h43ypf5k52ivw9p431zsblqg0z1h") (features (quote (("std") ("default" "std"))))))

(define-public crate-dynstore-0.1 (crate (name "dynstore") (vers "0.1.0") (deps (list (crate-dep (name "inventory") (req "0.2.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "1.*") (default-features #t) (kind 0)))) (hash "0xaq2zg7nxbd6yf6f9sdmdb33h1h3rrrv6691zl7savdgs1xyf47") (v 2) (features2 (quote (("inventory" "dep:inventory"))))))

(define-public crate-dynstore-0.1 (crate (name "dynstore") (vers "0.1.1") (deps (list (crate-dep (name "inventory") (req "0.2.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "1.*") (default-features #t) (kind 0)))) (hash "1habivk8v6sgs487paxwnr2kfgmh815fxn613jk88ljbvq5xpdq3") (v 2) (features2 (quote (("inventory" "dep:inventory"))))))

(define-public crate-dynstore-0.1 (crate (name "dynstore") (vers "0.1.2") (deps (list (crate-dep (name "inventory") (req "0.2.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "1.*") (default-features #t) (kind 0)))) (hash "0a5gvb8sgx4vzihk5v32bbb7b6k8f7h0xlp4w89va58j3mb3d00p") (v 2) (features2 (quote (("inventory" "dep:inventory"))))))

(define-public crate-dynstr-0.1 (crate (name "dynstr") (vers "0.1.0") (hash "0qb1kgxzqr2akc5hq04m66s5lvymahp83d5v2qaz41hz3sz3rn5a") (yanked #t)))

(define-public crate-dynstr-0.1 (crate (name "dynstr") (vers "0.1.1") (hash "0zkqsj09iwp0fi1li6lig7a88sbfw1ly4k0jgaqyq8riiywcpj0k")))

