(define-module (crates-io dy ew) #:use-module (crates-io))

(define-public crate-dyeware-0.0.0 (crate (name "dyeware") (vers "0.0.0") (deps (list (crate-dep (name "color-span") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "diagnostic-quick") (req "^0.2.3") (features (quote ("tl"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "tl") (req "^0.7.7") (features (quote ("simd"))) (default-features #t) (kind 0)))) (hash "1f5hr4fjbb5jd1wx68h0q6wh15wpjx63lg3c1d517dmp0salwd5a") (features (quote (("highlightjs") ("default" "highlightjs"))))))

