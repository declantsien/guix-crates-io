(define-module (crates-io dy nl) #:use-module (crates-io))

(define-public crate-dynlib-0.1 (crate (name "dynlib") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0b849qla5znwvw4y8cc0hybhx5i8yqvi1hp77inbz2099v4xzkha")))

(define-public crate-dynlib_derive-0.1 (crate (name "dynlib_derive") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "19c2jd635aiwfqvly55nnmrslril2fhjj4icjq607zpjq90l63cr") (yanked #t)))

