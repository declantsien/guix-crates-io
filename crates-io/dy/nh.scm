(define-module (crates-io dy nh) #:use-module (crates-io))

(define-public crate-dynhash-0.1 (crate (name "dynhash") (vers "0.1.0") (deps (list (crate-dep (name "mopa") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "17n2fzpm97rrsgab0kzdggawqih60gfcvd9p3nc10qn64cf6vcck") (features (quote (("test" "sha2" "sha3") ("doc" "sha2" "sha3") ("default"))))))

