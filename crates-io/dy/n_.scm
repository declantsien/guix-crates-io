(define-module (crates-io dy n_) #:use-module (crates-io))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.0") (hash "1ykcq7nl6wy4sslfwd99g7zfkhsa9cvv7az61d4nilrs6s24jgn9")))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.1") (hash "1fxz2zwzd5l8wcnqrcv064mjjdsbfqx1kl06qzrn3n9g8b3bki72")))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.2") (hash "1a4mw1yic8m2aw021i8cjsc49f39abpbinvs2b05k1avl9sd3rqn")))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.3") (hash "0g1xbkmx4w89fnlcrg3ynifbmvqr4q3r80fpixdq54bpdq1m8a3f")))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.4") (hash "143w7b7hcrg07wm3vg28cdxds4nxm633lih4van40002864z0nl3")))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.5") (hash "1zhm983dfm30njcy19cvywj56ncjcrj25hhqfsmgdy4mzb6g3463")))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.6") (hash "03ykmssn73q90y90hfri83090ijgb3r2401a5rrs5f05g81ds0js")))

(define-public crate-dyn_array-0.1 (crate (name "dyn_array") (vers "0.1.7") (hash "0q6rx17l39kxradsvf8r9454aksi9qf6js6xgn6mlggr63ic5yx8")))

(define-public crate-dyn_bitmap-0.0.1 (crate (name "dyn_bitmap") (vers "0.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "13kk7fah2lzrvjhq5m5jc952l2jv7jig7jvm54kzr6b68bqyaz6q")))

(define-public crate-dyn_bitmap-0.0.2 (crate (name "dyn_bitmap") (vers "0.0.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "02nmb0m1px38cafqipp4hs2d6ni2c2b52wwhzv0880hzas08fvxg")))

(define-public crate-dyn_bitmap-0.1 (crate (name "dyn_bitmap") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "0x3cqkfy18csphwq2vx2456kx609y8i58im9cvy3fa0bwl5nzi6l")))

(define-public crate-dyn_bitmap-0.3 (crate (name "dyn_bitmap") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1x6bnk0fammdwbwpp84b1k9irv2xvd7d0yl1kr6igjrlwibp777a") (yanked #t)))

(define-public crate-dyn_bitmap-0.3 (crate (name "dyn_bitmap") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0g79k6i3l76l88i2axm9i1zs02v5z9si89v9xi4p3ql4qg4nrgk2") (yanked #t)))

(define-public crate-dyn_bitmap-0.3 (crate (name "dyn_bitmap") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07n6frb0627ysw0c76dcfkyjp652fvr5jk3m4jmc3w66ilwkwv8v")))

(define-public crate-dyn_buf-0.1 (crate (name "dyn_buf") (vers "0.1.0") (hash "0z3nfqrn0q8pync9sjk2yp3b615zgkmki6vqp6f3sxqmcywpmibl")))

(define-public crate-dyn_formatting-1 (crate (name "dyn_formatting") (vers "1.0.0") (hash "0gz93v9zid9z53y0cp2w7zwpyd12c1njb8hi4qsvyk0x0rfn6dvi") (yanked #t)))

(define-public crate-dyn_formatting-1 (crate (name "dyn_formatting") (vers "1.0.1") (hash "0nqwilz5k650k4r2ydszg037c5dxv7w9z45s13riih7hnkqwl0hz") (yanked #t)))

(define-public crate-dyn_formatting-2 (crate (name "dyn_formatting") (vers "2.0.0") (hash "04cf4zicwj4dqgnnnz9fqwkgwk2gvh3rdfxpd09vaj64cs63f1l6")))

(define-public crate-dyn_formatting-3 (crate (name "dyn_formatting") (vers "3.0.0") (hash "1b5i4dgprahgk9rjybj4hp5z983cy3dzvw0dlp94z1x3rj7n605j")))

(define-public crate-dyn_import-0.0.1 (crate (name "dyn_import") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "13vj1rgw6wfsizmvfvxpi7qrvmr95yq2xnzzhzkam5hrl8x7z56c")))

(define-public crate-dyn_inject-0.1 (crate (name "dyn_inject") (vers "0.1.0") (hash "0rf2hm0phlgjfx6j84jf6lvyc1jvqq0yfq6bj1p4s673pdpm0083")))

(define-public crate-dyn_object-0.1 (crate (name "dyn_object") (vers "0.1.0") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1878ybqpnfzwvg8valbvmx42rsipfm74nl4f6yvww2szqk48hzvq") (yanked #t)))

(define-public crate-dyn_object-0.2 (crate (name "dyn_object") (vers "0.2.0") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0yrsc8fn6q7xgcrmjmaxipfq7dl447v3j1krpc8sx24h6g1fawb6")))

(define-public crate-dyn_ord-0.1 (crate (name "dyn_ord") (vers "0.1.0") (hash "1pbv88q3d6hb8vqn8v551yzmc36ssdz31rph4z8mb4qqah11aac7")))

(define-public crate-dyn_ord-0.2 (crate (name "dyn_ord") (vers "0.2.0") (hash "14z1ddi569a5v17s82v5pg0a6y0k487dmih36b79jpf5by1sni3d")))

(define-public crate-dyn_ord-0.2 (crate (name "dyn_ord") (vers "0.2.1") (hash "0b1sfjm2cncbmpff61hk5jfj1y4dxy9klwqdmf2l9r6nph0cjm4x")))

(define-public crate-dyn_partial_eq-0.1 (crate (name "dyn_partial_eq") (vers "0.1.0") (deps (list (crate-dep (name "dyn_partial_eq_derive") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0izf0n0hsw77w8slhk6zqnym6qv621f8qj2gn0903xya8d1gqaix")))

(define-public crate-dyn_partial_eq-0.1 (crate (name "dyn_partial_eq") (vers "0.1.1") (deps (list (crate-dep (name "dyn_partial_eq_derive") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "0irgx1qd141qsrzc67sglzpavjzb94638cn96vxwyrbmbqf7q48f")))

(define-public crate-dyn_partial_eq-0.1 (crate (name "dyn_partial_eq") (vers "0.1.2") (deps (list (crate-dep (name "dyn_partial_eq_derive") (req "=0.1.2") (default-features #t) (kind 0)))) (hash "00a3w2fqxls3y628ajwa4laygd4g69jz3i0h72d4nv12jz8kjw50")))

(define-public crate-dyn_partial_eq_derive-0.1 (crate (name "dyn_partial_eq_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10rglyafjriv9wrbpi23s5sfmx5gp5pn52lapfndfd5wamxx399k")))

(define-public crate-dyn_partial_eq_derive-0.1 (crate (name "dyn_partial_eq_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15y7va87gvvwpk069vncxbczs8q2w728la7fqbkhx0dmy6fmxb3z")))

(define-public crate-dyn_partial_eq_derive-0.1 (crate (name "dyn_partial_eq_derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gg8r12y0x8yjlidsvjhmy72n69rsf4ladb2hswgksrm2in7q88f")))

(define-public crate-dyn_partial_eq_derive-0.1 (crate (name "dyn_partial_eq_derive") (vers "0.1.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qi1jj3rj099xydf6kqfjzbhw4wbqs6gmjnl51s7pjcb7snlhzyj") (features (quote (("std") ("alloc"))))))

(define-public crate-dyn_problem-0.1 (crate (name "dyn_problem") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "http-api-problem") (req "^0.57.0") (features (quote ("api-error"))) (default-features #t) (kind 0)) (crate-dep (name "iri-string") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "typed_record") (req "^0.1.0") (features (quote ("ext-http"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.0") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "176b6glnvg6drnyc3cj6k6acvsfmqm78pxjxs8d3byfmkv50af6z") (features (quote (("default" "anon-problem-type")))) (v 2) (features2 (quote (("ext-typed-record" "dep:typed_record") ("anon-problem-type" "dep:uuid") ("alias-future" "dep:futures"))))))

(define-public crate-dyn_problem-0.1 (crate (name "dyn_problem") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.28") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "http-api-problem") (req "^0.57.0") (features (quote ("api-error"))) (default-features #t) (kind 0)) (crate-dep (name "iri-string") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "typed_record") (req "^0.1.1") (features (quote ("ext-http"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.0") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "0h36siny4gywhp16fibhrcz1m4ya2q3ccnfxnrrrkkd5smjwfsws") (features (quote (("default" "anon-problem-type")))) (v 2) (features2 (quote (("ext-typed-record" "dep:typed_record") ("anon-problem-type" "dep:uuid") ("alias-future" "dep:futures"))))))

(define-public crate-dyn_safe-0.0.1 (crate (name "dyn_safe") (vers "0.0.1") (deps (list (crate-dep (name "proc_macros") (req "^0.0.1") (default-features #t) (kind 0) (package "dyn_safe-proc_macros")))) (hash "1xsqyxkyzjawvirdmr5ld9m6k7nzvk7q7lnkwnsmlpsa7l0l7d6l")))

(define-public crate-dyn_safe-0.0.2 (crate (name "dyn_safe") (vers "0.0.2") (deps (list (crate-dep (name "proc_macros") (req "^0.0.2") (default-features #t) (kind 0) (package "dyn_safe-proc_macros")))) (hash "1xrfb111w7dp291jnkrgyfb0pvhhvm6j55yzrir50j4rn0jnfabz")))

(define-public crate-dyn_safe-0.0.3 (crate (name "dyn_safe") (vers "0.0.3") (deps (list (crate-dep (name "proc_macros") (req "^0.0.3") (default-features #t) (kind 0) (package "dyn_safe-proc_macros")))) (hash "0fnkkwqal4pf5j1byvc1m2zdbx3qycm5f3xhrzhwh0akh1xf7m82")))

(define-public crate-dyn_safe-0.0.4 (crate (name "dyn_safe") (vers "0.0.4") (deps (list (crate-dep (name "proc_macros") (req "^0.0.4") (default-features #t) (kind 0) (package "dyn_safe-proc_macros")))) (hash "0dbc68b5xpp9i9an96ardd8wag500731mxdl12rgii2343l18bn6")))

(define-public crate-dyn_safe-proc_macros-0.0.1 (crate (name "dyn_safe-proc_macros") (vers "0.0.1") (hash "15zz24m57vvznlgra1rvnn60liha7vgr7dbzyfkiv8hbqd492rd5")))

(define-public crate-dyn_safe-proc_macros-0.0.2 (crate (name "dyn_safe-proc_macros") (vers "0.0.2") (hash "19lmbhcvkfmx9bb9200n3c16yxb2xf3w7hvb7aww3xlgnfqxcc8p")))

(define-public crate-dyn_safe-proc_macros-0.0.3 (crate (name "dyn_safe-proc_macros") (vers "0.0.3") (hash "00ywwjydj4m17qiv116almrkfphfy39iicsaz1563f3k5sgcql03")))

(define-public crate-dyn_safe-proc_macros-0.0.4 (crate (name "dyn_safe-proc_macros") (vers "0.0.4") (hash "00hc9ijanpg5h1ljp2cr55ks6wh3qf7ysxlg4hcpbcixrz0xyqrw")))

(define-public crate-dyn_size_of-0.1 (crate (name "dyn_size_of") (vers "0.1.0") (hash "18wjv9z05vr4dpfngw61qrs5zr49l10fri28vm2giysyrxz6jaqz")))

(define-public crate-dyn_size_of-0.1 (crate (name "dyn_size_of") (vers "0.1.1") (hash "0jy8113nmhps10khlq0h54xjr4fm7gc1y2jfdvf9clvbqk53g1aq")))

(define-public crate-dyn_size_of-0.2 (crate (name "dyn_size_of") (vers "0.2.0") (hash "01qzspyfr848lvf5q4km2vx8skyb4b3qmjdv6c0jpyvzz5myg39b")))

(define-public crate-dyn_size_of-0.2 (crate (name "dyn_size_of") (vers "0.2.1") (hash "1mwdm6r3y8ir9kyljsr3pnq6slszz9w8qqcywrq2kqfxcmicymsq")))

(define-public crate-dyn_size_of-0.2 (crate (name "dyn_size_of") (vers "0.2.2") (hash "01vdfx9wn1syndjcc8806j2h6p5a3viz3z5gbj5mb1z1nl23aba4")))

(define-public crate-dyn_size_of-0.3 (crate (name "dyn_size_of") (vers "0.3.0") (hash "1cf8fwhxqi3rb15vxj1kkajldy491zhi6c2yd5h15ac3ck8bj9wb") (yanked #t)))

(define-public crate-dyn_size_of-0.4 (crate (name "dyn_size_of") (vers "0.4.0") (hash "06gmdi272mizh3d22jypwhnls02b3w8kqslip36f9z33azmqm2rb")))

(define-public crate-dyn_size_of-0.4 (crate (name "dyn_size_of") (vers "0.4.1") (hash "1iq1y16y70jakmg33082x1283ybbsn7jdgcv6vhqmwgf577dr2kc")))

(define-public crate-dyn_size_of-0.4 (crate (name "dyn_size_of") (vers "0.4.2") (deps (list (crate-dep (name "aligned-vec") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0d2iimxcvjipi6jbvcxhcv1p7xx2gdhaznmgijzkbv5i825ggm1k")))

(define-public crate-dyn_sized-0.1 (crate (name "dyn_sized") (vers "0.1.0") (deps (list (crate-dep (name "fn_move") (req "^0.1") (default-features #t) (kind 0)))) (hash "190dwj2sl0sr9apmac0xcxzpx86imqz2zf2ahsk0g8xlsiji59rk")))

(define-public crate-dyn_sized-0.2 (crate (name "dyn_sized") (vers "0.2.0") (deps (list (crate-dep (name "fn_move") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gn1v0k9clfkvr5gdi4xjbmy052mafd9hxh0abxx45gb74p9bfv4")))

(define-public crate-dyn_struct-0.1 (crate (name "dyn_struct") (vers "0.1.0") (deps (list (crate-dep (name "dyn_struct_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0607cnxjnxmk089b4li9sj95bxm187i6bvcjdcyg4mkbqinhdpks") (features (quote (("derive" "dyn_struct_derive") ("default" "derive"))))))

(define-public crate-dyn_struct-0.2 (crate (name "dyn_struct") (vers "0.2.0") (deps (list (crate-dep (name "dyn_struct_derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0m9vxbmgfx521wvkvh75n240v1jp2w747nyp616pwa36q7b2y674") (features (quote (("derive" "dyn_struct_derive") ("default" "derive"))))))

(define-public crate-dyn_struct-0.3 (crate (name "dyn_struct") (vers "0.3.0") (deps (list (crate-dep (name "dyn_struct_derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1bg4y1ywldylwh0yrr8ghl3fy96d3mh13641x2cn6ffdbhk57qgv") (features (quote (("derive" "dyn_struct_derive") ("default" "derive")))) (yanked #t)))

(define-public crate-dyn_struct-0.3 (crate (name "dyn_struct") (vers "0.3.1") (deps (list (crate-dep (name "dyn_struct_derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0z1bn1rib3w4c3g18c1wbm8nbs2biasjfwgykjlh521y5d3d25mj") (features (quote (("derive" "dyn_struct_derive") ("default" "derive")))) (yanked #t)))

(define-public crate-dyn_struct-0.3 (crate (name "dyn_struct") (vers "0.3.2") (deps (list (crate-dep (name "dyn_struct_derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vcq8xsfydc62syp0l9fxqgcx4py3jb3bzfbxpb45pd4alqvawrk") (features (quote (("derive" "dyn_struct_derive") ("default" "derive"))))))

(define-public crate-dyn_struct2-0.1 (crate (name "dyn_struct2") (vers "0.1.0") (deps (list (crate-dep (name "dyn_struct_derive2") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "transmute") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1n2wa4ajsgac56vqcf32c05jcmpdl2znyp9yvgiq4fd9rxp6wiin") (features (quote (("derive" "dyn_struct_derive2"))))))

(define-public crate-dyn_struct_derive-0.1 (crate (name "dyn_struct_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0skx3qi2xg511mhz761h78n8iccmp49wh2isqb1d7hli217s30ck")))

(define-public crate-dyn_struct_derive-0.2 (crate (name "dyn_struct_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "11lim8s8bswcq2mwvpi0fg72678qwvm7ly08nc2xksynr6gzj5ln")))

(define-public crate-dyn_struct_derive-0.3 (crate (name "dyn_struct_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0rm7kympka0w9kdrf8synfnrxyhbac32v6fy1xs93h9w11kf12w2")))

(define-public crate-dyn_struct_derive2-0.1 (crate (name "dyn_struct_derive2") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0pwlilfwdnmnrcdwblqzrfnn9ixbkaxlpfl5cqc3sfcdl9d937gg")))

(define-public crate-dyn_vec-0.1 (crate (name "dyn_vec") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1w31m9pqgzn18gm5afiwybczabfafg9ck86k7iqin0fyn16xk87j") (features (quote (("unstable"))))))

