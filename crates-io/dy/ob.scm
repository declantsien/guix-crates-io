(define-module (crates-io dy ob) #:use-module (crates-io))

(define-public crate-dyobj-0.1 (crate (name "dyobj") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.5") (default-features #t) (kind 2)))) (hash "0rsps7rh4q26vk2i8d0fimdkqrwzq4fk9zc19k50br9cp2l5wpb2")))

