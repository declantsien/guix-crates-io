(define-module (crates-io dy ad) #:use-module (crates-io))

(define-public crate-dyadic-0.0.1 (crate (name "dyadic") (vers "0.0.1") (hash "0kxrij3a5sirpclqg4mkcsas7l5hck1i90a8i8aw15h1qmy9dsdq")))

(define-public crate-dyadic-0.0.2 (crate (name "dyadic") (vers "0.0.2") (hash "1h7v2a1ywlxlxyrmkf6ix3kjycibfynkk9i6sv2a0cwfkz4whmlz")))

(define-public crate-dyadic-0.0.3 (crate (name "dyadic") (vers "0.0.3") (hash "09i5b4vx509wh130m56qn0hrc6q7zagvrfp39lkhqcab0p0c4nnf")))

(define-public crate-dyadic-0.0.4 (crate (name "dyadic") (vers "0.0.4") (hash "0xsbr3vg3676fhvd6f3j0q6c3d1x5lir96818pmvkhbcfxyibp2i")))

(define-public crate-dyadic-0.0.5 (crate (name "dyadic") (vers "0.0.5") (hash "171lprpp8rwpxmclzbxx234lg0na90vw6w4gql3vdxsy82a2fpyw")))

(define-public crate-dyadic-0.0.6 (crate (name "dyadic") (vers "0.0.6") (hash "0vy9wfqgbs338bbphyw33ijjd1q1crya32qhjb3an04wm7pznm7s")))

(define-public crate-dyadic-0.0.7 (crate (name "dyadic") (vers "0.0.7") (hash "11wfhrv078a5ki51b5ijfq4365vv07w2sqkisxp9hvn1nmp1in8b")))

(define-public crate-dyadic-0.0.8 (crate (name "dyadic") (vers "0.0.8") (hash "0x360nmbnrzsbz3xjdxi634byfrjyv8mcgrapsbzdki3cjh1fkz5")))

