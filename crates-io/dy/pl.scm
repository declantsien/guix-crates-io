(define-module (crates-io dy pl) #:use-module (crates-io))

(define-public crate-dyplugin-0.1 (crate (name "dyplugin") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0f9pwrb4n0wyrxhdjy3harbhxhwzvmr3vwpfadwiaxkq9kz8rhid")))

(define-public crate-dyplugin-0.2 (crate (name "dyplugin") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0w0wy67k28j8k3mi1q644pl3n2xkfdc0ap5l4wl7mipbzbi5qg1d")))

