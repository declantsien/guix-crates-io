(define-module (crates-io dy e-) #:use-module (crates-io))

(define-public crate-dye-cli-0.1 (crate (name "dye-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "06jvk010j14jr3g02d2jyczkz8rf0wkkqvz5vkh2machb4cm97bi")))

(define-public crate-dye-cli-0.1 (crate (name "dye-cli") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0ahq9cmkp9jwyzgmakw4r6brb83wwpx819n8q8vmb29m6c0bykkh")))

(define-public crate-dye-cli-0.1 (crate (name "dye-cli") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0pq3a4qcs4cv0xw5izlz7qcmx211dq992w7kq7l462a8if0ywlcv")))

(define-public crate-dye-cli-0.1 (crate (name "dye-cli") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0nm9pz4fna9h614jblkbgxl54z65zd34w3phlwzwqdm56j1vy47l")))

(define-public crate-dye-cli-0.1 (crate (name "dye-cli") (vers "0.1.14") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0dsf64p46grkvwxk6xdbv1axvy6ygl4gnzjsfqpl1mpqyvypc6ms")))

(define-public crate-dye-cli-0.1 (crate (name "dye-cli") (vers "0.1.15") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "11jgx09g405a0z82la4g01mjh6kvhbzsrqdm7illlkw5aga20ngy")))

(define-public crate-dye-cli-0.1 (crate (name "dye-cli") (vers "0.1.16") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1gy4n0hbmg43qhnc5dnkm5v5wa2jb3cc4bw3rn1bwd81869r72mx")))

(define-public crate-dye-cli-0.2 (crate (name "dye-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0ys8rzhcvbn7idlllbfhbfp4m5vim3xd016scy8ggbyk1iplp2ps")))

(define-public crate-dye-cli-0.4 (crate (name "dye-cli") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1gyrw8bg9cyxlxaz9anphs5b0y42c8bilssld1qjp39w8p9lp4k7")))

(define-public crate-dye-cli-0.4 (crate (name "dye-cli") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "=3.0.0-beta.4") (features (quote ("color" "env" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "00yjdqbwhrkpd97nwpqyk3rkj25va33gh2kv2vscc4mq28bh0mic")))

