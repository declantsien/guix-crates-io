(define-module (crates-io dy mo) #:use-module (crates-io))

(define-public crate-dymod-0.1 (crate (name "dymod") (vers "0.1.0") (deps (list (crate-dep (name "sharedlib") (req "~7.0.0") (default-features #t) (kind 0)))) (hash "1rng36n4z1ps3hx43bc8xrmsrziijfgdsf14l8pgci2drmx80kgn")))

(define-public crate-dymod-0.2 (crate (name "dymod") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "~0.5.0") (default-features #t) (kind 0)))) (hash "0ra9aj6gkg5bjcq9xnnzxr96pr93ixrvg3kbs96b65s8hii0rmdz") (features (quote (("force-static") ("force-dynamic") ("default" "auto-reload") ("auto-reload"))))))

(define-public crate-dymod-0.3 (crate (name "dymod") (vers "0.3.0") (deps (list (crate-dep (name "libloading") (req "~0.5.0") (optional #t) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "0k8kqi1v813insbh6ammsy55srbhwl62dfj8ijyh5a2dn66ikdkc") (features (quote (("force-static") ("force-dynamic" "libloading") ("default" "auto-reload" "libloading") ("auto-reload"))))))

(define-public crate-dymod-0.4 (crate (name "dymod") (vers "0.4.0") (deps (list (crate-dep (name "libloading") (req "^0.5") (optional #t) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "0yffdcxajhrrzd8v2c5ylr8b9cbwycc7bqp6rm52rkpxglmb6d29") (features (quote (("force-static") ("force-dynamic" "libloading") ("default" "auto-reload" "libloading") ("auto-reload"))))))

