(define-module (crates-io dy ld) #:use-module (crates-io))

(define-public crate-dyld-trie-0.1 (crate (name "dyld-trie") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "leb128") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 2)))) (hash "01vx1s60g8jcgp7m25ddan1xabn316g8nbcl42vpzs101ka6xpl2") (features (quote (("std")))) (rust-version "1.65")))

