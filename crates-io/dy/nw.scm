(define-module (crates-io dy nw) #:use-module (crates-io))

(define-public crate-dynwave-0.1 (crate (name "dynwave") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "ringbuf") (req "^0.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "rubato") (req "^0.14") (default-features #t) (kind 0)))) (hash "1x9fg02i9rm5xmpj41ljp6ca89s0851mpr20a67b9q5pbl0y9ays") (rust-version "1.62.0")))

