(define-module (crates-io dy nv) #:use-module (crates-io))

(define-public crate-dynvec-0.1 (crate (name "dynvec") (vers "0.1.0") (hash "0j3qq0mnks20yrfx3s9dvxsa59jn66b3qiqbvxqp0hhimmz5habh")))

(define-public crate-dynvec-0.1 (crate (name "dynvec") (vers "0.1.1") (hash "0jinb2yspaw5z13gh4f4awbch8x8986s13492zyrckqwqajvhzn8")))

(define-public crate-dynvec-0.1 (crate (name "dynvec") (vers "0.1.2") (hash "1yklm6ka9c9w3wcykrxkpxv3ws5cqj3bdx6xm0w8lvkqkjglf0ra")))

(define-public crate-dynvec-0.1 (crate (name "dynvec") (vers "0.1.3") (hash "12d5pqxhrf2wapdsml8hlqhyjl3ch4dmfwlj53jkk17piqiyng8v")))

(define-public crate-dynvec-0.1 (crate (name "dynvec") (vers "0.1.4") (hash "0k6imzqbx35f4wvywh64l58bn72pfs0avjd00r7blch5nhs1wcd0")))

