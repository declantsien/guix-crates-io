(define-module (crates-io dy nj) #:use-module (crates-io))

(define-public crate-dynja-0.1 (crate (name "dynja") (vers "0.1.0") (deps (list (crate-dep (name "askama") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "dynja_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "minijinja") (req "^1.0") (features (quote ("loader"))) (default-features #t) (kind 0)))) (hash "0b1kbs24s8q1xb0hxg20xxa71fpw9q1xakqi9sawx33wbyzz51b0")))

(define-public crate-dynja-0.2 (crate (name "dynja") (vers "0.2.0") (deps (list (crate-dep (name "askama") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "dynja_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "minijinja") (req "^1.0") (features (quote ("loader"))) (default-features #t) (kind 0)))) (hash "0s6j3knq2ab0rvm1hms061f04z969nx1xxdd575vfcvnbjxv7rxr")))

(define-public crate-dynja-0.2 (crate (name "dynja") (vers "0.2.1") (deps (list (crate-dep (name "askama") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "dynja_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minijinja") (req "^1.0") (features (quote ("loader"))) (default-features #t) (kind 0)))) (hash "1sldzga7qdhkh2yja3dma3xrybv7ah18zbfb0a982qg7klgy3zyz")))

(define-public crate-dynja-0.3 (crate (name "dynja") (vers "0.3.0") (deps (list (crate-dep (name "askama") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "dynja_derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "minijinja") (req "^1.0") (features (quote ("loader"))) (default-features #t) (kind 0)))) (hash "1h6gqksvd0n4h4x4a7djp1fn9c8dwkampgsnmydgj4r08ag0k8lj")))

(define-public crate-dynja-0.3 (crate (name "dynja") (vers "0.3.1") (deps (list (crate-dep (name "askama") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "dynja_derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "minijinja") (req "^1.0") (features (quote ("loader"))) (default-features #t) (kind 0)))) (hash "0a8pxcn99f1v9im5r7dn23wvy56k2s5q8dwqmf08ccvhxb5qi3xc")))

(define-public crate-dynja-0.4 (crate (name "dynja") (vers "0.4.0") (deps (list (crate-dep (name "askama") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dynja_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minijinja") (req "^1.0") (features (quote ("loader"))) (default-features #t) (kind 0)))) (hash "0jmaqnizb78zy0x25lz5k18p9xk778dym851pk48b1n2hn60d1zk") (features (quote (("default")))) (v 2) (features2 (quote (("askama_release" "dep:askama"))))))

(define-public crate-dynja-0.4 (crate (name "dynja") (vers "0.4.1") (deps (list (crate-dep (name "askama") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dynja_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "minijinja") (req "^1.0") (features (quote ("loader"))) (default-features #t) (kind 0)))) (hash "0nwjcf0yg1y4digbf04p63ghh21hayyqqpm8jf275xz0k3mgbnii") (features (quote (("default")))) (v 2) (features2 (quote (("askama_release" "dep:askama"))))))

(define-public crate-dynja_derive-0.1 (crate (name "dynja_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0b2zwd28sf2a73y1gr4g1zbbc73zffq4fk2q0f2wxccig00c7bz3")))

(define-public crate-dynja_derive-0.2 (crate (name "dynja_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g6xm0ai5v5k4d8msbnm3dy3ay10ndssbw4lyz3nmff899zkf4m2")))

(define-public crate-dynja_derive-0.2 (crate (name "dynja_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0clvl441a9n4bm32b0gwjqgp4hahznnsmavxr4maik7cj95p9fv8")))

(define-public crate-dynja_derive-0.3 (crate (name "dynja_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13rxxkzbvcbz8faxyk7bl01fwxavm9dph41jhdxr6bza9ym9vs5p")))

(define-public crate-dynja_derive-0.3 (crate (name "dynja_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mwm5v915kcwlpz3gqy4z085k8x695h3wgh890nf5dlfjjmdqpjv")))

(define-public crate-dynja_derive-0.4 (crate (name "dynja_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06b32xibp5hj6q7v26yd4c9iqaqhlrh0fb5mr81dassw6mlp9wki")))

(define-public crate-dynja_derive-0.4 (crate (name "dynja_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1azn6pxzx1fyq5pnsfl1zd5lh75izbrg53bwj95kjda8q1ma91c1")))

