(define-module (crates-io dy ck) #:use-module (crates-io))

(define-public crate-dyck-0.1 (crate (name "dyck") (vers "0.1.0") (deps (list (crate-dep (name "dyck-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0z80fwdknlcqngjzvr2589w2byi187nkgy53khvy7ns3rkj92w65") (features (quote (("derive" "dyck-derive") ("default"))))))

(define-public crate-dyck-derive-0.1 (crate (name "dyck-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "0vchfz7h3zvbzf3f500mz7kzc5g8w6l3s3y7hiwyn83qd79rrp5w")))

