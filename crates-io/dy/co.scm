(define-module (crates-io dy co) #:use-module (crates-io))

(define-public crate-dycovec-0.1 (crate (name "dycovec") (vers "0.1.0") (hash "15l56li0bg4szmwdp20vy2jna6i775jizy61ckqp802kb4gj2xl5")))

(define-public crate-dycovec-0.1 (crate (name "dycovec") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "evmap") (req "^10") (default-features #t) (kind 2)) (crate-dep (name "evmap-derive") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "sharded-slab") (req "^0.1") (default-features #t) (kind 2)))) (hash "10bqx1sfjigwd6h0z3mrm3x2mpl9c4cgigabhdz51dy030kmxdz1")))

