(define-module (crates-io dy _t) #:use-module (crates-io))

(define-public crate-dy_tlsf-0.0.1 (crate (name "dy_tlsf") (vers "0.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1h2na314c5p3v8n1qdsfash4cr8rl5asjq7hhiys91sii9yjfycy")))

(define-public crate-dy_tlsf-0.0.2 (crate (name "dy_tlsf") (vers "0.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "09y8238aaa57z7yf0a7h7g4crvrlm23j3v1l5n6vpx0irnz39kf0")))

