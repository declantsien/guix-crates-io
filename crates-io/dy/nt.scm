(define-module (crates-io dy nt) #:use-module (crates-io))

(define-public crate-dyntable-0.0.0 (crate (name "dyntable") (vers "0.0.0") (hash "1pw3ap0inikd6y59fphf849p91spagalgbbbr3s3zqs7crk3svw9")))

(define-public crate-dyntest-0.1 (crate (name "dyntest") (vers "0.1.0") (deps (list (crate-dep (name "globset") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0z5924y2c765bgrb4id9z47cgq3k3wld391z5xli7j6bxj2b8gzr") (features (quote (("default" "glob")))) (v 2) (features2 (quote (("glob" "dep:globset" "dep:walkdir"))))))

(define-public crate-dyntest-0.1 (crate (name "dyntest") (vers "0.1.1") (deps (list (crate-dep (name "globset") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "1q9ri20gabs24k3yzn2hznrfhgiv1fy9zn6b47c5jxqcbx594z63") (features (quote (("default" "glob")))) (v 2) (features2 (quote (("glob" "dep:globset" "dep:walkdir"))))))

(define-public crate-dyntest-0.1 (crate (name "dyntest") (vers "0.1.2") (deps (list (crate-dep (name "globset") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "1prncw2jvhlwgmrjzjf6cck6iq6ir1c3rm9yz1xg0z3vc3q335l2") (features (quote (("default" "glob")))) (v 2) (features2 (quote (("glob" "dep:globset" "dep:walkdir"))))))

(define-public crate-dyntest-0.2 (crate (name "dyntest") (vers "0.2.0") (deps (list (crate-dep (name "globset") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mv86pg6k1bw3chwl1bbw6hk414fawa0h1844591rypx1kx92q2i") (features (quote (("default" "glob")))) (v 2) (features2 (quote (("glob" "dep:globset" "dep:walkdir"))))))

