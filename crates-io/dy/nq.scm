(define-module (crates-io dy nq) #:use-module (crates-io))

(define-public crate-dynqueue-0.1 (crate (name "dynqueue") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "04k8zxjgf22qycmpvxw684bxxnqizyx5jbr0xyyy71cmip5mvsr0")))

(define-public crate-dynqueue-0.1 (crate (name "dynqueue") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0pvmd9w7lb6q5083h5dlp3gwl1wwswfcaqpih0zknjrv4rd306gd")))

(define-public crate-dynqueue-0.1 (crate (name "dynqueue") (vers "0.1.2") (deps (list (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0bblxcq71mh632yr7byq615bq9adsfxfqijmmf9kn5ix4dlxbh3s")))

(define-public crate-dynqueue-0.1 (crate (name "dynqueue") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1p8lakvsa3wz0v86c7763m2l07dbkhywlq03wqlay2y441jgg0jx")))

(define-public crate-dynqueue-0.2 (crate (name "dynqueue") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (default-features #t) (kind 0)))) (hash "1ms42va5gd353mgl3m77myvi4d6p2ip4fn4zcq3ylpmnfdgq1mlp")))

(define-public crate-dynqueue-0.2 (crate (name "dynqueue") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (default-features #t) (kind 0)))) (hash "1ck4ajb57k76cbvsnk31h8707sxf96jrk80b4fc5a29q8712r6ii")))

(define-public crate-dynqueue-0.3 (crate (name "dynqueue") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (default-features #t) (kind 0)))) (hash "0ndxyspjn36q4fjvlbhd988n45cr2bvc8ycbzkhczgii1iswk6zz")))

