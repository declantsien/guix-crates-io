(define-module (crates-io dy wa) #:use-module (crates-io))

(define-public crate-dywapitchtrack-0.1 (crate (name "dywapitchtrack") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.5.0") (default-features #t) (kind 2)))) (hash "0k1pccvxjzqhr7kbliyxj4np4myargghxdrr8y8x5c5hcs9k9d4n")))

