(define-module (crates-io dy np) #:use-module (crates-io))

(define-public crate-dynp-0.1 (crate (name "dynp") (vers "0.1.10") (deps (list (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "0r28alh052xnq14gz6fy6mwhpd1hgzch88rnm7ns9prk23lnz2i6")))

(define-public crate-dynp-0.1 (crate (name "dynp") (vers "0.1.11-PullRequest0001.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "16lb3x02i3y6a072gsxcdkj3vizw7kffyxv7fbm7apkl7r7rd0sa") (yanked #t)))

(define-public crate-dynp-0.1 (crate (name "dynp") (vers "0.1.11-PullRequest0001.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1187n3c1w8liygg6d2xny1h521xq65jddfrxdj2ng46yggmbz71a") (yanked #t)))

(define-public crate-dynp-0.1 (crate (name "dynp") (vers "0.1.11-PullRequest0001.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "0zc0aqwl2p9nsca5nqb3d0xd0kz6ci568mb45lk42b6l6j0kzvvr") (yanked #t)))

(define-public crate-dynp-0.1 (crate (name "dynp") (vers "0.1.11") (deps (list (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "04m1nbvk9fbicb6d3rzgrndnfvyw7y8qdlaivkba1nb89z8g45s5")))

(define-public crate-dynp-0.1 (crate (name "dynp") (vers "0.1.12") (hash "1waivcnh9bf0zcf961g90l0zdzzq7hd0h4lg9jppynca0a2pwcnv")))

(define-public crate-dynparser-0.1 (crate (name "dynparser") (vers "0.1.0") (hash "1napxp62gcmn033r9mrqj8fpkika3qgfshxlqb8xnkbf7cp2rmlb")))

(define-public crate-dynparser-0.2 (crate (name "dynparser") (vers "0.2.0") (hash "0wqfz46av3xpsnkslkwlj88dblyyjssh62wnm6nzvrg5n231lya6") (yanked #t)))

(define-public crate-dynparser-0.2 (crate (name "dynparser") (vers "0.2.1") (hash "04nb0ilzf8nbc8yrycah3q7qb6z26al5vh27krv29v8mwqajlmxa")))

(define-public crate-dynparser-0.4 (crate (name "dynparser") (vers "0.4.0") (deps (list (crate-dep (name "idata") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14zqp1634ig233i5j4pyjhgfwmpjcsnbvj8kl0x7ymsvbipz1rmv")))

(define-public crate-dynparser-0.4 (crate (name "dynparser") (vers "0.4.1") (deps (list (crate-dep (name "idata") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "048shy0z7gvjxibc7jrv5knx4va3b836v9iqc14byrpv4vmx354l")))

(define-public crate-dynparser-0.4 (crate (name "dynparser") (vers "0.4.2") (deps (list (crate-dep (name "idata") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0cq05y033ac9pzxx7sxr5vd92jlnwmal7v2f6dcgkccr31sxhf5b")))

(define-public crate-dynpath-0.1 (crate (name "dynpath") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cdq9ap3zyaqki713iw0lv7ccb1cnr9wqmyd9p8gjqs4v2zq4bmk")))

(define-public crate-dynpath-0.1 (crate (name "dynpath") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mgm5y39fixyg2wxjzq54q3vndj667q4yckzrfj8r3qxpdyqnzad")))

(define-public crate-dynpath-0.1 (crate (name "dynpath") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vcn87jmyxqh9wfn22znnz2z3h7y6811kqc450z89mawm2psiy1g")))

(define-public crate-dynpath-0.1 (crate (name "dynpath") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hz4gc87z4m741bjk59ahk0wvd1zcmnz9ay6cn1y6dypa59qzn7r")))

(define-public crate-dynpath-0.1 (crate (name "dynpath") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bi431ggnnn7n2pch3d4b2wd1591x84nn9fgp7vsbmjc32j88s30")))

(define-public crate-dynpick-force-torque-sensor-0.1 (crate (name "dynpick-force-torque-sensor") (vers "0.1.0") (deps (list (crate-dep (name "easy-ext") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "pair_macro") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "02m16z9sfmzm4h7hzyasd6gm75911c4hq9w80jzmib2gbm62hbdi")))

(define-public crate-dynpool-0.0.0 (crate (name "dynpool") (vers "0.0.0") (deps (list (crate-dep (name "backoff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "00d09xcn78wjqy66xla2gvnaadjmmbwb5h3229ijbnfdhrdq65mi") (features (quote (("instrument"))))))

(define-public crate-dynpool-0.0.1 (crate (name "dynpool") (vers "0.0.1") (deps (list (crate-dep (name "backoff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "19bsgf94llnhvi73iw25m1ffjggj15dccmnlwf7m9v7vj0y7hzch") (features (quote (("instrument"))))))

(define-public crate-dynpool-0.0.2 (crate (name "dynpool") (vers "0.0.2") (deps (list (crate-dep (name "backoff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "02hvjb9vb0pf1f9fwzh7060wqz37bc1dkm6mb05sj873bk6y8zgm") (features (quote (("instrument"))))))

(define-public crate-dynprops-0.1 (crate (name "dynprops") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "0dx0q1i25i63n5k50gza1cmd0gjslyrjqdllj8qpbknr5rwkg85j")))

