(define-module (crates-io dy bs) #:use-module (crates-io))

(define-public crate-dybs-0.1 (crate (name "dybs") (vers "0.1.0") (hash "01xis22hhdlk5bc3i06j2a2n2kc5dyrk8dk4h3wp4747ilmdr5ij")))

(define-public crate-dybs-0.2 (crate (name "dybs") (vers "0.2.0") (hash "07vhr21bsak3if7f88j9nj0wx1lmq4fjjihqhdbk54k9isi2aw7p")))

(define-public crate-dybs-0.3 (crate (name "dybs") (vers "0.3.0") (hash "07yi9imhrvp781d9r4g4yp4j0xf6ap0pn7vhmmdbvqvm00cdjybs") (features (quote (("unstable"))))))

