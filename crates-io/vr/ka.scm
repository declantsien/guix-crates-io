(define-module (crates-io vr ka) #:use-module (crates-io))

(define-public crate-vrkansagara-0.0.0 (crate (name "vrkansagara") (vers "0.0.0") (hash "11zn969xky7h6j6cx5bqgvdm5fklm9k2f23h6w2gg35ngbvpfgkm")))

(define-public crate-vrkansagara-0.0.1 (crate (name "vrkansagara") (vers "0.0.1") (hash "0dwgsaxsnsh9kqkjgzkdrbj73y43wgfga0l2asvlzmnrrj80dhhm")))

