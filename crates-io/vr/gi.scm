(define-module (crates-io vr gi) #:use-module (crates-io))

(define-public crate-vrgit-0.1 (crate (name "vrgit") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (features (quote ("win32"))) (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.101.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0063gf3p006c43zd6251w12v0aqa4ldq4mm6g5swr9w2s2pcka8a")))

