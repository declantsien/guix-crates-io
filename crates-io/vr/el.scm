(define-module (crates-io vr el) #:use-module (crates-io))

(define-public crate-vrellis-0.1 (crate (name "vrellis") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17falbrijg9wn4s4h728f9747ca13vj9jab0wf1zb095vakd2ylm") (features (quote (("default"))))))

(define-public crate-vrellis-core-0.1 (crate (name "vrellis-core") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11l6f7bsah1qiq5psccykwbwyzhazg1zgdm4d7nxxh1yx3d3a8x8") (features (quote (("default"))))))

