(define-module (crates-io vr rp) #:use-module (crates-io))

(define-public crate-vrrp_packet-0.1 (crate (name "vrrp_packet") (vers "0.1.0") (deps (list (crate-dep (name "pnet_base") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "pnet_macros") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "pnet_macros_support") (req "^0.34.0") (default-features #t) (kind 0)))) (hash "0s5a1qd5jrqxbsdpxj0h2ziynj5g1gx38q684ckfkcwz5rx11kn4")))

