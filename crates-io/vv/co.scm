(define-module (crates-io vv co) #:use-module (crates-io))

(define-public crate-vvcore-0.0.1 (crate (name "vvcore") (vers "0.0.1") (hash "1wzzshxzyfz3fll9kg5z0cf7rzfygpc0lbhs1yxlckxx1vwg9zz2")))

(define-public crate-vvcore-0.0.2 (crate (name "vvcore") (vers "0.0.2") (hash "14sm9yifnhy2z6bd7s2fw1cdx10ag7zb40abawgfc97i28m99wki")))

