(define-module (crates-io my ma) #:use-module (crates-io))

(define-public crate-mymatrix-0.1 (crate (name "mymatrix") (vers "0.1.0") (deps (list (crate-dep (name "pyinrs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1qnkpbzlzcjqdg4gff5871l7zn1hhsb921rf39y9qysbisib6x4z")))

(define-public crate-mymatrix-0.2 (crate (name "mymatrix") (vers "0.2.0") (deps (list (crate-dep (name "pyinrs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "08prqqb2gd0h282b7qxgix9gnm5f7x2ki8msvqqfawlybvzphpl3")))

(define-public crate-mymatrix-0.3 (crate (name "mymatrix") (vers "0.3.0") (deps (list (crate-dep (name "pyinrs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "045i21sx76p0nr5pvd14s1g1l6yykwj0ix07h43sny0c9sgvax5i")))

