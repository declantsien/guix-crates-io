(define-module (crates-io my pk) #:use-module (crates-io))

(define-public crate-mypkg-0.0.1 (crate (name "mypkg") (vers "0.0.1") (hash "0qhk4bwyiaxhwd2m40fpjibiwkabhzac9alhsgskc1a1j7qbi5b6")))

(define-public crate-mypkg-0.0.2 (crate (name "mypkg") (vers "0.0.2") (hash "0k4gh5kx8si3y4psy7p7dy4i8adm4j65w6vh064gb7lb01csjr0n")))

