(define-module (crates-io my cr) #:use-module (crates-io))

(define-public crate-mycraft-0.1 (crate (name "mycraft") (vers "0.1.0") (deps (list (crate-dep (name "async-codec") (req "^0.4.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.3") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1zz5v2aiv3cc62q1w8bfjl87fma2j9if57186y5l3l55iniq657v")))

(define-public crate-mycrate-0.1 (crate (name "mycrate") (vers "0.1.0") (hash "12z38gx4ciqx6rysyhlai9kq937ml8xvli1hcas5jk6h6m5pys20")))

(define-public crate-mycrate-xp-0.1 (crate (name "mycrate-xp") (vers "0.1.0") (hash "15n44lq8iz5mz1pw10rs1hsfgkriw0i1gkhs8lx5hd88hab0q5gi") (yanked #t)))

(define-public crate-mycrate-xp-0.1 (crate (name "mycrate-xp") (vers "0.1.1") (hash "016c7nsjwyy8fr295r4cw7s0qawpm06j951h3cria7gqx5azfyr7")))

(define-public crate-mycrates1-0.1 (crate (name "mycrates1") (vers "0.1.0") (hash "06vi8114dhd95kln8vi65lds2pzj3wf9f389armqvmyf61cjh354")))

(define-public crate-mycrc-0.1 (crate (name "mycrc") (vers "0.1.0") (hash "1b50qvnpd1bys89zvv113hqps8ccy6vmwg2dnhi4sp625q87wynk")))

(define-public crate-mycrc-0.2 (crate (name "mycrc") (vers "0.2.0") (hash "0gsrqdks43yqfbn4izb26dq3h06s9ykl4p5phrhgdf2llkadym23")))

(define-public crate-mycrc-0.3 (crate (name "mycrc") (vers "0.3.0") (hash "09yh67xyrv8mvjip0s00yjdrf8pv8qcrxzw1ynv5a5k1s7q9vmp6")))

(define-public crate-mycrc-0.3 (crate (name "mycrc") (vers "0.3.1") (hash "0aibia47dgbzk0vgqik319fq53p6i13fahmdibblm9zs1d6axiy3")))

(define-public crate-mycroft-0.0.1 (crate (name "mycroft") (vers "0.0.1") (deps (list (crate-dep (name "combine") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ilm88hvvdyjslp4x02rgcf1dlj2vivjzclv94xv0r49bfaidpqn")))

(define-public crate-mycroft-macros-0.0.1 (crate (name "mycroft-macros") (vers "0.0.1") (deps (list (crate-dep (name "mycroft-macros-impl") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jcivh8l21a0n5sc6kn56vrckgab2hdl4g7a5bvy23zjmzl92y7j")))

(define-public crate-mycroft-macros-impl-0.0.1 (crate (name "mycroft-macros-impl") (vers "0.0.1") (deps (list (crate-dep (name "combine") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "mycroft") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (features (quote ("full" "printing" "parsing"))) (default-features #t) (kind 0)))) (hash "0ckcbzdy7jazasjp8ah618s20l5x5p7s68kiyisr558qrhqqnm58")))

(define-public crate-mycroft-support-0.0.1 (crate (name "mycroft-support") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0a7p66wwgpbskx6jwkwi2bq9s8r3xhdli4zfxnd0k8ns5m9r8bbx")))

