(define-module (crates-io my ga) #:use-module (crates-io))

(define-public crate-mygame-0.1 (crate (name "mygame") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "raylib") (req "^3.7.0") (default-features #t) (kind 0)))) (hash "0y6hdmkyn5qs9l3bmi7w4slywb90vqz1x9bkcw7mbz03d3i7i727")))

(define-public crate-mygame-0.1 (crate (name "mygame") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "raylib") (req "^3.7.0") (default-features #t) (kind 0)))) (hash "17hrcfr5j6vkjy2rpp6k9143lhpv18ffijinvfknmymq70m1x3nb")))

(define-public crate-mygame-0.1 (crate (name "mygame") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "raylib") (req "^3.7.0") (default-features #t) (kind 0)))) (hash "1541ag9naygrj9k80a8wf4jd1zfs3lrrs0vpnj0qqm5llxnf88cc")))

(define-public crate-mygame-0.1 (crate (name "mygame") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "raylib") (req "^3.7.0") (default-features #t) (kind 0)))) (hash "0ikp9f548y4bznphxl4pxg5riwwn92y174ny9h8jiila4r2zyikw")))

