(define-module (crates-io my ne) #:use-module (crates-io))

(define-public crate-myne-0.2 (crate (name "myne") (vers "0.2.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cursive") (req "^0.14.1") (features (quote ("crossterm-backend" "markdown"))) (default-features #t) (kind 0)) (crate-dep (name "epub") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "html2md") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0d8arlxvvz9zxzpyvh0hp043x5alrqna2fnz0ww2v9704jb6f78j")))

