(define-module (crates-io my hu) #:use-module (crates-io))

(define-public crate-myhumantime-1 (crate (name "myhumantime") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "1nfcr18iwzzj48b0lph08iqq550hpgp1mfbc7jmf83qk6qrmzzhi")))

