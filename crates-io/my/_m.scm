(define-module (crates-io my _m) #:use-module (crates-io))

(define-public crate-my_macro_crate-0.1 (crate (name "my_macro_crate") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "15p0alyhjswwybz075kk9ys8zpd9cbcz6q4lf983sz7psirk7mxm")))

(define-public crate-my_math_crate-0.1 (crate (name "my_math_crate") (vers "0.1.0") (hash "0j557n76b7s7abk94iysz6f5alm99x3p9gij5lii24x92fndv7sm") (yanked #t)))

(define-public crate-my_minigrep-0.1 (crate (name "my_minigrep") (vers "0.1.0") (hash "1xy5yx4x5wrr5szm3fm9x3smx1d14mjhxz8jc1hsy7s7p46d16n6") (yanked #t)))

(define-public crate-my_minigrep321-0.1 (crate (name "my_minigrep321") (vers "0.1.0") (hash "1z9qx615zj3q5mj5wijhgzmbp3l3cv9546gfjd8yiz66qcwanpg2")))

(define-public crate-my_minigrep321-0.1 (crate (name "my_minigrep321") (vers "0.1.1") (hash "1147fr2lrss3i8pw29di2lvc07dl5glkncch0yiz9mqp0mb0llhp") (yanked #t)))

(define-public crate-my_minigrep321-0.1 (crate (name "my_minigrep321") (vers "0.1.2") (hash "1490q6kbv9rgmlw4aaf0xx6hi2qs3d1cij1yqpv5s996cbhk1h1q")))

(define-public crate-my_minigrepio-0.1 (crate (name "my_minigrepio") (vers "0.1.0") (hash "0mpm276izffgrx8c1accyiqp7z6h30yq4gxijv9c98rkm07c5wps")))

