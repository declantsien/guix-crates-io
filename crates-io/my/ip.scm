(define-module (crates-io my ip) #:use-module (crates-io))

(define-public crate-myip-0.0.1 (crate (name "myip") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.24") (default-features #t) (kind 0)))) (hash "0nqybc9gd161djz19d9yiard1v98225a0vylrd7zamv9hnjy3ba5")))

