(define-module (crates-io my nu) #:use-module (crates-io))

(define-public crate-mynumber-0.1 (crate (name "mynumber") (vers "0.1.0") (hash "055pvdggbqygyid7y5kw7x9na8svljl5hadl3znzrr955qdsan67")))

(define-public crate-mynumber-0.2 (crate (name "mynumber") (vers "0.2.0") (hash "04yi55p3zr9mmcazm1zv8547h9mrswm7cmlpx2jdj97zcpmkaans")))

