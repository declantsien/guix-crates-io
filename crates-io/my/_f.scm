(define-module (crates-io my _f) #:use-module (crates-io))

(define-public crate-my_file_crate-0.1 (crate (name "my_file_crate") (vers "0.1.0") (deps (list (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "my_lib_crate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "my_macro_crate") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fy9y58zjljnqc5534c15a27b1qwfcgq2kq618f8z0pjx3cj617c")))

(define-public crate-my_file_crate-0.1 (crate (name "my_file_crate") (vers "0.1.1") (deps (list (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "my_lib_crate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "my_macro_crate") (req "^0.1") (default-features #t) (kind 0)))) (hash "04ji4zyqh3d08rfh8sppwg1zm7xxk86k3qddf2b4f2csvvazfz76")))

(define-public crate-my_first_cargo_library-0.1 (crate (name "my_first_cargo_library") (vers "0.1.0") (hash "15xw84yficfzqrlblm9v96zyk3ipklflm7qh0bzr2k7hzjkvy1jx") (yanked #t)))

(define-public crate-my_first_cargo_pacakge-0.1 (crate (name "my_first_cargo_pacakge") (vers "0.1.0") (hash "1n781qpnymf8jc1f5w69g5dm6hlskah020iy5zihnmv2js3dryay")))

(define-public crate-my_first_cli-0.1 (crate (name "my_first_cli") (vers "0.1.0") (hash "16xzwypih7516374fc2x69lrbphrgim5sqzara7pb7ncz7bs52nx")))

(define-public crate-my_first_cli-0.1 (crate (name "my_first_cli") (vers "0.1.1") (hash "1jjh16rv3l60xja7nc2yqvmzlihhwzixki9pv5cy7r0g5gpjlx1w")))

(define-public crate-my_first_cli-0.1 (crate (name "my_first_cli") (vers "0.1.2") (hash "1iinrp28xmy4kmfyxard4vrnsq9kfxksiaphrnbilhqcza15yj31")))

(define-public crate-my_first_cli-0.1 (crate (name "my_first_cli") (vers "0.1.3") (hash "11lyqchqymgncmw3yl8y4si6x01kjrq8wk0m6qvzs4x6mxf84884")))

(define-public crate-my_first_cli-0.1 (crate (name "my_first_cli") (vers "0.1.4") (hash "096y1jn9256brh8gy1wp4mc2yamvidy8g9ca22rycb6w404xyb6x")))

(define-public crate-my_first_cli-0.2 (crate (name "my_first_cli") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "05y03j9sam9g8n72yzs1g1ds2l6mdjd2i063v5r19k69yg4k6qjg")))

(define-public crate-my_first_cli-0.2 (crate (name "my_first_cli") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0cd884azynhxg84wl3j6f3ff9c3b8xws3abxvcwnwsf6ap8ms6hp")))

(define-public crate-my_first_crate_rpn_calc-0.1 (crate (name "my_first_crate_rpn_calc") (vers "0.1.0") (hash "0ypzfy3rkavi6wcakq08k7v1lsvsxqfpii7q3dld36fmljlh7syb")))

(define-public crate-my_first_lib_rust-0.1 (crate (name "my_first_lib_rust") (vers "0.1.0") (hash "0hbyg0nsnhs9iz0lbk498za8lb72zq3ljky7klfglwxj710wijvf")))

(define-public crate-my_first_package-0.1 (crate (name "my_first_package") (vers "0.1.0") (hash "09yf7k2kj89gv0fb7lb9qx5wxxwf8bfqbmxvd9si5rbic00nd4rv") (yanked #t)))

(define-public crate-my_first_package-1 (crate (name "my_first_package") (vers "1.1.0") (hash "1nvyz6zf9809w6s3kwpdk6yn7qsshcnxv5fr5lcp7mh01hb2ls8c") (yanked #t)))

(define-public crate-my_first_pkg-0.1 (crate (name "my_first_pkg") (vers "0.1.0") (hash "0axp7nvqz485rkgb849xd0cyi1m8yizm29hqrl6v4gh0ppz8fgg1") (yanked #t)))

(define-public crate-my_format-0.1 (crate (name "my_format") (vers "0.1.0") (hash "1hsfh3r32f6f5aa2m8zbhg3anmr7jb3nxzxlxqh1qn6pp0ahj7ir")))

(define-public crate-my_fpro-0.1 (crate (name "my_fpro") (vers "0.1.0") (hash "12sq4bfkzqal7xcwii0fy34cbjbrsjfg6mn2n19hixdg4hm7w53m")))

(define-public crate-my_fproject-0.1 (crate (name "my_fproject") (vers "0.1.0") (hash "1jfrh90mr0wbqxaybihrxdm9mp6q2lq58g0wyf80wn6yinnmphw5")))

(define-public crate-my_fun-0.1 (crate (name "my_fun") (vers "0.1.1") (hash "0grfa1g4g4l4rilhr3c5z0nyq16kbi0b898fvlr5qikvlc383kx7")))

(define-public crate-my_function-0.1 (crate (name "my_function") (vers "0.1.0") (hash "18j5giihhfwaxgaisrkslnjz4s59hsdzz6d7jz2bgln9sf2j48fg")))

(define-public crate-my_funny_cli-0.2 (crate (name "my_funny_cli") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1qcpmp03bbqmil32nni9c7jypyc1fhzcvcg1dz03zrn4yir9cr4b")))

