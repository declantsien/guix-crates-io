(define-module (crates-io my on) #:use-module (crates-io))

(define-public crate-myon-0.1 (crate (name "myon") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.19.2") (default-features #t) (kind 0)))) (hash "0nzkmrk7z1k325bhr8d7rvshlj9z9gclqsm9zlbrm2ik94bxpvsf") (yanked #t)))

