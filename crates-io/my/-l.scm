(define-module (crates-io my -l) #:use-module (crates-io))

(define-public crate-my-little-eval-0.1 (crate (name "my-little-eval") (vers "0.1.0") (hash "006b0bi45j5yfw8an9mj91h8cin0mn5960cbm16x2lwcs9yc9852")))

(define-public crate-my-little-eval-0.1 (crate (name "my-little-eval") (vers "0.1.1") (hash "08bn49cw08h17rwq1dgvxm89z70yqypdairpga0wjb31cxd7yfnm")))

