(define-module (crates-io my ru) #:use-module (crates-io))

(define-public crate-myrust_utils-0.1 (crate (name "myrust_utils") (vers "0.1.0") (hash "19frq8rmv76kriz8sk3bsr88832bczkb0ifhkhsrbkg7qyz8lcfv") (yanked #t)))

(define-public crate-myrust_utils-0.2 (crate (name "myrust_utils") (vers "0.2.0") (hash "0fx8wy0mq97aw4hx9nkq954j7ql66xgj0jdzr6m8navikz2lqv2i") (yanked #t)))

(define-public crate-myrust_utils-0.3 (crate (name "myrust_utils") (vers "0.3.0") (hash "1nyr7lr84dayhl5zffi9s86zpm7p8q0p2r3mw5jhb609nqva5m4c") (yanked #t)))

