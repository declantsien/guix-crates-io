(define-module (crates-io my -d) #:use-module (crates-io))

(define-public crate-my-desire-0.1 (crate (name "my-desire") (vers "0.1.0") (deps (list (crate-dep (name "my-desire-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ixbf7bcki3m3l3kijg6ixya622kysdvwhx2akahab5nz0sm85gc")))

(define-public crate-my-desire-macros-0.1 (crate (name "my-desire-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "00bvlsnbzpdrjs2xr91ylnvrljyic53cngp020nq835sdgmq1s87")))

