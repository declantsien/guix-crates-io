(define-module (crates-io my -f) #:use-module (crates-io))

(define-public crate-my-first-crate-0.1 (crate (name "my-first-crate") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "00a7224x7al25qzh6xjad2sdj9jj24ky4daf3ndz8k22i9fp3fa6")))

(define-public crate-my-first-lib-0.1 (crate (name "my-first-lib") (vers "0.1.0") (hash "0b048kiky9sdmclkg4n2r7djdsfnmlgv78akhgm3hbdr5j4w9lnz") (yanked #t)))

(define-public crate-my-first-lib-0.0.1 (crate (name "my-first-lib") (vers "0.0.1") (hash "1w6zqpwlc4wnrraabqizvmsknhpvr2fk1qmxhjhk8ibc4mfc0drr")))

