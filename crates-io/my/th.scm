(define-module (crates-io my th) #:use-module (crates-io))

(define-public crate-myth-0.0.0 (crate (name "myth") (vers "0.0.0") (hash "03g49zf3nx7bwvwmhf7rqhgbxjprmlvlalvkhaid5i8wbqfyz7fp")))

(define-public crate-mytheme-0.0.2 (crate (name "mytheme") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "prql-compiler") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.174") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)))) (hash "07gd7883i9wff6vjkfjzzslsa7n66dzqrsyixm91xqx2qcdb7d1y") (yanked #t)))

(define-public crate-mythical-0.0.0 (crate (name "mythical") (vers "0.0.0") (hash "1bdpn0q398hwqbrk5l9ps72qsmiqs5d8idbnhav2bqac2hpbfx27")))

(define-public crate-mythoji-0.1 (crate (name "mythoji") (vers "0.1.0") (deps (list (crate-dep (name "strum") (req "^0.24.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (optional #t) (default-features #t) (kind 0)))) (hash "1mv1p3ma8k8icyjj1w7lanyymim53jb9ns08y78xfi9404bx42f0") (features (quote (("iter" "strum" "strum_macros"))))))

(define-public crate-mythril-0.1 (crate (name "mythril") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (kind 0)) (crate-dep (name "num_enum") (req "^0.4.2") (kind 0)) (crate-dep (name "uefi") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "uefi-services") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0hz68b37ipcz12knkgyql5kdnpnh4vqr66x3a4w9lvyr8lrklfkb")))

