(define-module (crates-io my _v) #:use-module (crates-io))

(define-public crate-my_very_own_add_one-0.1 (crate (name "my_very_own_add_one") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1kvkp96vbs0pawn97hm6x5h19j895xmpw6dzk2js75yclfcak8ry")))

