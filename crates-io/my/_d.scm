(define-module (crates-io my _d) #:use-module (crates-io))

(define-public crate-my_demo-0.1 (crate (name "my_demo") (vers "0.1.0") (hash "1wgp4jq5dllgvf26zjwhlv4bry7qb7r1alk0wxfp9gvkdnqh871l") (yanked #t)))

(define-public crate-my_demo_adder-0.1 (crate (name "my_demo_adder") (vers "0.1.0") (deps (list (crate-dep (name "my_demo") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10srhxf1fgpa3jy1syqw1l8lb6s3yva6m4rc4192h2qx641bi1iv") (yanked #t)))

(define-public crate-my_dependencies-0.1 (crate (name "my_dependencies") (vers "0.1.0") (deps (list (crate-dep (name "cargo_toml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0wl0i5ziy4fqvnraw8d7p4c36mcqxdqhcyshq0k3cgq2bx66m6hk")))

(define-public crate-my_dev_tool-0.1 (crate (name "my_dev_tool") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1") (default-features #t) (kind 0)))) (hash "0730ipm3pki922q14hgq70mmjw3yzzxlkmgs825d1qkiqparwmfh")))

(define-public crate-my_dev_tool-0.2 (crate (name "my_dev_tool") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1") (default-features #t) (kind 0)))) (hash "1fn5vnd5kwnd6wli8z11m2rykhx62v1glw95vq70xx80kzswivdj")))

