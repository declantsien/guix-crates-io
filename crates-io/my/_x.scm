(define-module (crates-io my _x) #:use-module (crates-io))

(define-public crate-my_xml_parser-0.1 (crate (name "my_xml_parser") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "12gmvw9604qqqynv3bgnkrn4sipypw95jm39gyn6m15njqy3rds0")))

