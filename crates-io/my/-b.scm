(define-module (crates-io my -b) #:use-module (crates-io))

(define-public crate-my-bfgs-0.1 (crate (name "my-bfgs") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6") (default-features #t) (kind 2)))) (hash "1wsgwv9p2r8jwd789mcg4pbrydvajp7g1mgma6q79dx6rs5aqw8i")))

(define-public crate-my-bfgs-0.1 (crate (name "my-bfgs") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6") (default-features #t) (kind 2)))) (hash "1kzksl60xvcl2bms4plfqwj0rkhz94i12lngddv4d66xxm40wld5")))

(define-public crate-my-bfgs-0.1 (crate (name "my-bfgs") (vers "0.1.2") (deps (list (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6") (default-features #t) (kind 2)))) (hash "012lgiwvacgdcb8cbcqgk900icl4qd0wpxh25nrjimm1bqflmb84")))

