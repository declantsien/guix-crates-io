(define-module (crates-io my tr) #:use-module (crates-io))

(define-public crate-mytraits-0.1 (crate (name "mytraits") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1982vls7b2v1fnmd0vibv39nd4viy9lcvl8vsqyxagaxrj7i0aa7") (yanked #t)))

(define-public crate-mytrie-0.1 (crate (name "mytrie") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "04q06928xd9farvhip7riffky3mily19g1g2z7y2f5zawlpxkfqm")))

(define-public crate-mytrie-0.2 (crate (name "mytrie") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "10bjmh36xxrh7dzm2m1xv2xygz3skcpc1qhwaxr9bk4z1n8c926v")))

(define-public crate-mytrie-0.2 (crate (name "mytrie") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "00w6xv0c6j548w5xw3s1mjqa2hz50b8ibj1va53hn1yxhmba8pik")))

(define-public crate-mytrie-0.2 (crate (name "mytrie") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "00pfxyl6c2hlssj79m1lmb3867yrz0hmrii2n5k91j4siiny6q35")))

