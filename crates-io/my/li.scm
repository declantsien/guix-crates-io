(define-module (crates-io my li) #:use-module (crates-io))

(define-public crate-mylib-0.1 (crate (name "mylib") (vers "0.1.0") (hash "0mj41d7cdmfjqc682a182pgi15c2ixxa0l7z44y9a8zpprr9pyjs")))

(define-public crate-mylib-0.1 (crate (name "mylib") (vers "0.1.1") (hash "0w5485yzm887hvjjxhznpvwhfh0v90jzw5a536vgdsvd78v8gg1i")))

(define-public crate-mylib-hekang-0.1 (crate (name "mylib-hekang") (vers "0.1.0") (hash "1xdv7a25bgs64vlfiyax8pr80sckjq9s2n51rxzmd53j7f99xz3d") (yanked #t)))

(define-public crate-mylib-test-0.1 (crate (name "mylib-test") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0brdyxdq90xhdk4q8xdsqi4fk54sih7d1g12kzy51yn4icg6rgbj") (yanked #t)))

(define-public crate-mylibapp-0.1 (crate (name "mylibapp") (vers "0.1.0") (hash "1xdya2fbka7fzi7s3q7kf0wh7bbfs5albgn3nhczc80h0nf0kby1")))

(define-public crate-mylibary-0.1 (crate (name "mylibary") (vers "0.1.0") (hash "1lc1p275fd3dq6iag3m4zr2z1n3wiffvvkgy0zcws9lnn27wwaq8")))

(define-public crate-mylibrary-1 (crate (name "mylibrary") (vers "1.0.4+dev1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "python3-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0pdny220s1mql27r2m07x90zzxx6p2pp46yb0581x93ixmyc0py9")))

(define-public crate-mylibrary-1 (crate (name "mylibrary") (vers "1.0.4+dev2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "python3-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0pfkbcw930cln9659r86pcx36avhnicjxy2wjw4gam4hhvi4yc4v")))

(define-public crate-mylibrary_-1 (crate (name "mylibrary_") (vers "1.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kcwvfx0yyb4sq331izvz4pr9dvg7chav4q3z3jdp410cmd395w8")))

