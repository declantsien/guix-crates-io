(define-module (crates-io my wh) #:use-module (crates-io))

(define-public crate-mywheel-rs-0.1 (crate (name "mywheel-rs") (vers "0.1.0") (deps (list (crate-dep (name "svgbobdoc") (req "^0.3") (features (quote ("enable"))) (default-features #t) (kind 0)))) (hash "0wlgzvs140mffapfr1q3xjfd0his1cdsf23bhrzpcz60w7wp4qi8")))

