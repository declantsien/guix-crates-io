(define-module (crates-io my -w) #:use-module (crates-io))

(define-public crate-my-wgsl-0.0.1 (crate (name "my-wgsl") (vers "0.0.1") (deps (list (crate-dep (name "my-wgsl-macros") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.11.2") (default-features #t) (kind 0)))) (hash "0518hnhxa8qvmnq1pnwdn97a767lfzmr92qk3xc2vwmy42nsxbby")))

(define-public crate-my-wgsl-macros-0.0.1 (crate (name "my-wgsl-macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dyq38k4z9wa7vm0s6lprhy9pfwwwjpxmik6w0qlksp9sxp8dwxz")))

