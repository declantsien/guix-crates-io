(define-module (crates-io my _l) #:use-module (crates-io))

(define-public crate-my_lib-0.1 (crate (name "my_lib") (vers "0.1.0") (hash "0fj7i06zzkq23j4iqrq6bxrv9qvrvyprbypq0zz3s51armgwazhg") (yanked #t)))

(define-public crate-my_lib_akniet-0.1 (crate (name "my_lib_akniet") (vers "0.1.0") (hash "07lxzx38iblklvhiwzpzbqc88mg3if5n0zd59vaxpkhk5g6bgp1a")))

(define-public crate-my_lib_crate-0.1 (crate (name "my_lib_crate") (vers "0.1.0") (hash "1f0vf4h2hlabx4w4r0p2xsh09qpv0fbrwva7gxwq26lg8qnlqbww")))

(define-public crate-my_lib_hello-0.1 (crate (name "my_lib_hello") (vers "0.1.0") (hash "1b41q4jwc2nrnb16ax9f879gr8cpr4rjns54plwn5vryqnhb8yid") (yanked #t)))

(define-public crate-my_library-0.1 (crate (name "my_library") (vers "0.1.0") (hash "06cfymw27bhqlq104cqgzg9nb2q6sdv84nvg3rjgjymmiy28rz21")))

(define-public crate-my_logger-0.1 (crate (name "my_logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)))) (hash "14mpfz9fw603qq2xbb4h3kgcxph722bg7v3q2ncn4y1yqs2xqw71") (yanked #t)))

(define-public crate-my_logger-0.1 (crate (name "my_logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)))) (hash "1dpi7zg7q65z0kyyxlcn9i2fb29yff1crv12h9kmlm8hcczd2mq2") (yanked #t)))

(define-public crate-my_logger-0.1 (crate (name "my_logger") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.0") (default-features #t) (kind 2)))) (hash "1a8bi5p5z4y6rqsgqja7rbc0phb87f8vbcvckl20a43gyw9mk1my") (yanked #t)))

(define-public crate-my_logger-0.1 (crate (name "my_logger") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.0") (default-features #t) (kind 2)))) (hash "1jipzwhj1ra02428ab5rzx8ivsbkv4kva8bd0km9xjqsnb03pifi") (yanked #t)))

(define-public crate-my_logger-0.1 (crate (name "my_logger") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.0") (default-features #t) (kind 2)))) (hash "0f8l0r8f4q2nvxdzxy4a5yc682iqghzjb65wyby8fxxf0n1cn9xi")))

(define-public crate-my_logger-0.1 (crate (name "my_logger") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.0") (default-features #t) (kind 2)))) (hash "0d556x99d510p4xamkala8rwvj7k2qqjf7b91553x2x6r0vnfmaw")))

(define-public crate-my_logger-0.2 (crate (name "my_logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.0") (default-features #t) (kind 2)))) (hash "12rfmfampn7ricry6b7gc2bkbsz766ivq7f20zc5644p1w775j8r")))

(define-public crate-my_logger-0.2 (crate (name "my_logger") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.0") (default-features #t) (kind 2)))) (hash "1ivdyhxzrnji7hwnj2alz79xk8i34ik593rh1m5pfhs3w80z2mg6")))

