(define-module (crates-io my -c) #:use-module (crates-io))

(define-public crate-my-cargo-project2-0.1 (crate (name "my-cargo-project2") (vers "0.1.0") (hash "1qan0c1snc0zpczcs774kldg514rxq5qgcf9hdh81fmqjn01y64x")))

(define-public crate-my-cargo-project2-0.2 (crate (name "my-cargo-project2") (vers "0.2.0") (hash "08bcmpkz73d7pgwy00ph4hbb8q5pfws3qmikjsxjpygy2mra6wb6")))

(define-public crate-my-crate-kiana-123-0.1 (crate (name "my-crate-kiana-123") (vers "0.1.0") (hash "1n81rmpan31xsvknzww1hh10mdlr9riqc0lshldhpnws2nrnmamf")))

(define-public crate-my-crate-public-0.1 (crate (name "my-crate-public") (vers "0.1.0") (hash "08rkbyilj48lhjymazpdld1nhfdzn2cp0yafslnd3lvabi7vajfk") (yanked #t)))

