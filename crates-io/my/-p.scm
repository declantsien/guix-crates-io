(define-module (crates-io my -p) #:use-module (crates-io))

(define-public crate-my-password-0.1 (crate (name "my-password") (vers "0.1.0") (deps (list (crate-dep (name "bs58") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.3") (default-features #t) (kind 0)))) (hash "0bk1wjvp6bpgb2m5yyxfpcmvs9mv993nxxl2lrydykr3x9f118hw")))

(define-public crate-my-password-cli-0.1 (crate (name "my-password-cli") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "my-password") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1svxgglzw4bi1vc24yvikikjzp2l4080a4ibgyzmsa92x35bf1sk")))

(define-public crate-my-pretty-failure-0.1 (crate (name "my-pretty-failure") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "182p0k314kg8kswh4dq3gbwxm4zdjvfg5vsdss79g70g5ciraclx")))

(define-public crate-my-pretty-failure-0.1 (crate (name "my-pretty-failure") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1drik87k4j4z5x252ni1f2bbrcp4csl1n10wi3zryfn3xa27kmz4")))

(define-public crate-my-pretty-failure-0.1 (crate (name "my-pretty-failure") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0gp1cjxyjsxrm9h0c60nzbzpdp1vhfd3wjsfq879bipimvlfm0p3")))

(define-public crate-my-project-0.1 (crate (name "my-project") (vers "0.1.0") (hash "1mxhid144yb1xh1i4lfx2cdcpiwxixjwfvx43jcf0zs82f86gbwn") (yanked #t)))

(define-public crate-my-pwd-0.1 (crate (name "my-pwd") (vers "0.1.0") (hash "0416dsmax57z8zkaqrinzdpi7cdcfhixm2c6hbzl28564b787k8r")))

(define-public crate-my-pwd-0.1 (crate (name "my-pwd") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ga965vzz0acxk99fkqw0562fwvy6mvjxwzkzj8l9mapa279vdfg")))

(define-public crate-my-pwd-0.1 (crate (name "my-pwd") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0r0fvmagxlf5xy24fr6v47jbpxlr0abjlscs7qxz1z0x28lqy1wi")))

(define-public crate-my-pwd-0.1 (crate (name "my-pwd") (vers "0.1.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "097xcv9ay04plvpn7fjwdghkxd1gkym4q2aldcjh2snlsppzr698")))

