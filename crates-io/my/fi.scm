(define-module (crates-io my fi) #:use-module (crates-io))

(define-public crate-myfind-0.1 (crate (name "myfind") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0s014wjsdy0m2f0mn8fnxfyrlbgaf2pjfg3cr16p86mj0az8aiaa")))

(define-public crate-myfind-0.1 (crate (name "myfind") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1a82136r7qfz6r71q41fz57915l8aj73jwmrbrcl6fi58306ms27")))

(define-public crate-myfind-0.1 (crate (name "myfind") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0si0ml141zwv2vgl0yzbmk0pv9q3lirmwfb9i741vjbadmx15pag")))

(define-public crate-myfind-0.1 (crate (name "myfind") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pbw3n82x78qr1kh1y7d0w2mh4c7f66kjrbpipmd9jaaqc56z6rn")))

(define-public crate-myfind-0.1 (crate (name "myfind") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03yzr2bcb3mb95bpqnnn2bhv43wy13f8my48dy6zkjmafni24b9h")))

(define-public crate-myfind-0.2 (crate (name "myfind") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "jwalk") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "02d4c0fmm4lspj0nrljjp4dbvzafgcy7h7i42i3j7dq7i4r777hz")))

(define-public crate-myfirstlibrary-0.1 (crate (name "myfirstlibrary") (vers "0.1.0") (hash "0sbnvvgq0xz4zc7qk11d3qi35zm64lnlsrycp1m1bfclz2vy8q0g")))

(define-public crate-myfirstlibrary-0.1 (crate (name "myfirstlibrary") (vers "0.1.2") (hash "1k6v7xfi6d1xpvbh1r0yqj1nzmrbn6b1yskvyc83zssbvxs6awn7")))

