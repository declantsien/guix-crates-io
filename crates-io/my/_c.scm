(define-module (crates-io my _c) #:use-module (crates-io))

(define-public crate-my_cargo_test_crate_rr-0.1 (crate (name "my_cargo_test_crate_rr") (vers "0.1.0") (hash "0qxm36jj1xgksygqygbljrq6pc1zrb4sakwn3vyr6xs9my90cd90") (yanked #t)))

(define-public crate-my_cargo_test_crate_rr-0.1 (crate (name "my_cargo_test_crate_rr") (vers "0.1.1") (hash "0p8yr0b6zjr07c83xj0kgvxmg1an317bwqllcb7h6ff0acsnayql")))

(define-public crate-my_cart_ganu_doomer-0.1 (crate (name "my_cart_ganu_doomer") (vers "0.1.0") (hash "0nxj62ls8b5lg4b37478x73lqdi14arq89q2yphprg1jkw8kny1b")))

(define-public crate-my_chapter_14-0.1 (crate (name "my_chapter_14") (vers "0.1.0") (hash "1d2yj4saxq8jcg29rad0l87kz7b2wxpzs165f7y4ck3kbb17i0k2") (yanked #t)))

(define-public crate-my_chapter_14-0.1 (crate (name "my_chapter_14") (vers "0.1.1") (hash "0bpga6kc9ww7drgi4xsa5sxvr7qjfia6raayb8rxlxqvfppy4yx0") (yanked #t)))

(define-public crate-My_cli_tool-0.1 (crate (name "My_cli_tool") (vers "0.1.0") (hash "1yqql3iqg1phrhhgz384xjxm72fxnfhip9vhd1dl1xw44z910c74") (yanked #t)))

(define-public crate-my_closure-0.1 (crate (name "my_closure") (vers "0.1.0") (hash "1k4bsb7bpg24c1yhigpmhczi57pkx0vpkw5s7nz14s6dxk703ma9") (yanked #t)))

(define-public crate-my_crate-0.1 (crate (name "my_crate") (vers "0.1.0") (hash "0ky8ynssck6nzz5d3kijgp8kr0l52vjzacx9a0khl5hbg9hvd9db")))

(define-public crate-my_crate1-0.1 (crate (name "my_crate1") (vers "0.1.0") (hash "0smg5f6jipmbyrixvnxsfrvxf7x2b2f3h1rn6ym1c2clwryl9yks")))

(define-public crate-my_crate25-0.1 (crate (name "my_crate25") (vers "0.1.0") (hash "0r92sh9ms56nhl42zrc0q6pg0bg7d3qn23709b2yrdnphz2g153f") (yanked #t)))

(define-public crate-my_crate4774-0.1 (crate (name "my_crate4774") (vers "0.1.0") (hash "0l0ghhk3m1y4a1sl4nm4z3f0334ijgc13ymwd8nzvilnrr5f94w4") (yanked #t)))

(define-public crate-my_crate_006578-0.1 (crate (name "my_crate_006578") (vers "0.1.0") (hash "04796zlamvjpqpnj9vr1s5jq56h5cy39i9sr0cjk3860skskly41")))

(define-public crate-my_crate_1063-0.1 (crate (name "my_crate_1063") (vers "0.1.0") (hash "1h7sar8h114lx76xqcjy28q96acp2yg29yfld5sbglvcmj9394gl")))

(define-public crate-my_crate_2-0.1 (crate (name "my_crate_2") (vers "0.1.0") (hash "1g4gzrdsjz1p799fa61r0r17s93dbf72yynzbfvqcjs9a2bscsj8") (yanked #t)))

(define-public crate-my_crate_72a9eb54-5002-4540-a6e6-f2620f51b550-0.1 (crate (name "my_crate_72a9eb54-5002-4540-a6e6-f2620f51b550") (vers "0.1.0") (hash "15qxdqkaphf4lwvkzc220yfls2yilsgpszp4xinh0s88h1b0x9pc")))

(define-public crate-my_crate_carllhw-0.1 (crate (name "my_crate_carllhw") (vers "0.1.0") (hash "0kh0jl5b2l7b13wab2nq43l1wglwyinm0cfqc93vpi9zshgcvjcf")))

(define-public crate-my_crate_crate-0.1 (crate (name "my_crate_crate") (vers "0.1.0") (hash "1ws7vvnpwczsda580xz48wyrjmqvn5d8hrfli5pfi87jj7vdspip")))

(define-public crate-my_crate_crate-0.2 (crate (name "my_crate_crate") (vers "0.2.0") (hash "07bx9naz6iv6vll32jpic6i5kgxc08rk5w290za986s1irs81li1")))

(define-public crate-my_crate_demo-0.1 (crate (name "my_crate_demo") (vers "0.1.1") (hash "1cq72c87l0v8dm3z384pbpnz0lgjvcny3cji77wnya2j9r0ah83c") (yanked #t)))

(define-public crate-my_crate_demo-0.1 (crate (name "my_crate_demo") (vers "0.1.2") (hash "14gpihirmgmm1m4vx7cc7cwzns27y4g7vm910aark8x669izndfl")))

(define-public crate-my_crate_demo2-0.1 (crate (name "my_crate_demo2") (vers "0.1.0") (hash "0sb5f1diq0m19g5wbay5h04zz1x4f38i0ykxnry41bkfqjid9ksf") (yanked #t)))

(define-public crate-my_crate_denglitong-0.1 (crate (name "my_crate_denglitong") (vers "0.1.0") (hash "14hr3npibqbi2pfsrn0ldimyaw6m0fzrcmhfzcw5aalj0zazpcwf") (yanked #t)))

(define-public crate-my_crate_denglitong-0.1 (crate (name "my_crate_denglitong") (vers "0.1.1") (hash "0vxdf9j5r17gmgfnyp3hl45zbbhy7h6pd9lp8yzjbad99xd5hlgy")))

(define-public crate-my_crate_dewjjj-0.1 (crate (name "my_crate_dewjjj") (vers "0.1.0") (hash "0x1wxqip2ryd7aj5qrpc0chsrxka4xk28cni9k6ac4dxcbhkkh6s")))

(define-public crate-my_crate_genshin-0.1 (crate (name "my_crate_genshin") (vers "0.1.0") (hash "0hh2ssmpiv9ih8phdwrmhvz676fz2r7j3d7xh13lg8yiwkzmqmwh")))

(define-public crate-my_crate_home-0.1 (crate (name "my_crate_home") (vers "0.1.0") (hash "1vj2qlr5hyhq9shyksph7wr99g2py23iljnxz4if7lc56a3bw6j7")))

(define-public crate-my_crate_iamlearningrustgfkdsldsfsdrrzeczeczeacrzrgrdsfgds-0.1 (crate (name "my_crate_iamlearningrustgfkdsldsfsdrrzeczeczeacrzrgrdsfgds") (vers "0.1.0") (hash "1sc080rvpn8nhkkvpf5r4xswhbhw9ayhj3ic2pcd78g196vj1133")))

(define-public crate-my_crate_iamlearningrustgfkdsldsfsdrrzeczeczeacrzrgrdsfgds-0.1 (crate (name "my_crate_iamlearningrustgfkdsldsfsdrrzeczeczeacrzrgrdsfgds") (vers "0.1.9001") (hash "0bz3vjrhnvdhlfs5h6qslq79sq1vwj867vy31v8q0s3rb2ymnpdd")))

(define-public crate-my_crate_iamlearningrustgfkdsldsfsdrrzeczeczeacrzrgrdsfgds-0.1 (crate (name "my_crate_iamlearningrustgfkdsldsfsdrrzeczeczeacrzrgrdsfgds") (vers "0.1.9001900190011337808") (hash "16vky8q9ss0x4i01lpf7b94cq6jgwlvxczlicnvw7scd3f56x50f")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name1-0.1 (crate (name "my_crate_inside_a_repo_with_a_long_name1") (vers "0.1.0") (hash "1q0yxy93camzj6pwpcbby595a0fvdpr2b8likdzmqp7qqi56js8r")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name1-0.2 (crate (name "my_crate_inside_a_repo_with_a_long_name1") (vers "0.2.0") (hash "0ihgx9csvlic5qwds1xf8f9il3w2sllkk5q5p687zgy2f5n5qzyl")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name1-0.3 (crate (name "my_crate_inside_a_repo_with_a_long_name1") (vers "0.3.0") (hash "0h269j5dnb7v1gj0yqilhzm05vqihapn20p3rh0v9kb711hcwi9z")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name2-0.1 (crate (name "my_crate_inside_a_repo_with_a_long_name2") (vers "0.1.0") (deps (list (crate-dep (name "my_crate_inside_a_repo_with_a_long_name1") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i1an4npwb0kla141hmnh9xbmcvci824khfbd8l717yvqmp4gr38")))

(define-public crate-my_crate_inside_a_repo_with_a_long_name2-0.2 (crate (name "my_crate_inside_a_repo_with_a_long_name2") (vers "0.2.0") (deps (list (crate-dep (name "my_crate_inside_a_repo_with_a_long_name1") (req "^0.2") (default-features #t) (kind 0)))) (hash "19b13mvwdbz5j0rx21jlb4yyw7mgfayydxyyynh8kppbj1nw5b46")))

(define-public crate-my_crate_learn-0.1 (crate (name "my_crate_learn") (vers "0.1.0") (hash "027jismbp81k10kmq6swkfx4p6qqaram8zy5x4ha8xiba1wjs53f")))

(define-public crate-my_crate_lh_1-0.1 (crate (name "my_crate_lh_1") (vers "0.1.0") (hash "0l3ksd8p3zarkm1vy1vbs4fc0ljidl1g6fyyi3zysmamkyl4azgf")))

(define-public crate-my_crate_lib-0.1 (crate (name "my_crate_lib") (vers "0.1.0") (hash "1csmg3pxjw0pkwxfb624qvmkc5j3zh2i6r0lqjjzwqcr9xz53izs") (yanked #t)))

(define-public crate-my_crate_lyu-0.1 (crate (name "my_crate_lyu") (vers "0.1.0") (hash "08y1annfn74fn3qs7zb2mq5048m3ygnzzk6cj01p54vl6dlxypv6")))

(define-public crate-my_crate_mdberkey-0.1 (crate (name "my_crate_mdberkey") (vers "0.1.0") (hash "062jlclbfijd5zdgk18yb51jhkz9i6n3p54br92mayasp1il760l")))

(define-public crate-my_crate_meowing-0.1 (crate (name "my_crate_meowing") (vers "0.1.0") (hash "0iccqh544bygx6x18yc8gqkmsk45y5kj3g2swzxzql4y0qdk9pm1")))

(define-public crate-my_crate_new-0.1 (crate (name "my_crate_new") (vers "0.1.0") (hash "17csnsv2ny7bli6hvc6wac5wq0921kw7mkr9agm5rnvkd5622lzk")))

(define-public crate-my_crate_one_year_later-0.1 (crate (name "my_crate_one_year_later") (vers "0.1.0") (hash "1xyks7ad3w8p7jdkh9vbkzll639cr6mx4iyz9r35xgw7pczh4lga")))

(define-public crate-my_crate_study_chapter14-0.1 (crate (name "my_crate_study_chapter14") (vers "0.1.0") (hash "0l407mkzxs18ydk6mxrfjnggvmbbs5n27q1mp7djgzin0r4rnvkz")))

(define-public crate-my_crate_test-0.1 (crate (name "my_crate_test") (vers "0.1.0") (hash "1kxaask9006jv0h3ia7w0p935k6ci23aaz2b1mcmmh30mq6cc33c")))

(define-public crate-my_crate_test-0.1 (crate (name "my_crate_test") (vers "0.1.1") (hash "09bgjfzr9iyk7lly8jk7hqj3xlc8i4mximcl9ihgrn7969795rpk") (yanked #t)))

(define-public crate-my_crate_vg-0.1 (crate (name "my_crate_vg") (vers "0.1.0") (hash "1ia4x3iqnx0sh1qmg8cpwzz6a9w4bksy1hahpvc5cwi8x4xraqrn")))

(define-public crate-my_crate_w-0.1 (crate (name "my_crate_w") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0626q0xv6k98zlcsqa54sf8869bg293xcpxcq38zj9qmc31cz9ch")))

(define-public crate-my_crate_waleed-0.1 (crate (name "my_crate_waleed") (vers "0.1.0") (hash "1lll76d43in9785dsr8rlqdwar6zhixrl8a4gd3s5yhjj9s23q3k")))

(define-public crate-my_crate_wenye-0.1 (crate (name "my_crate_wenye") (vers "0.1.0") (hash "12grqbqq7dywnjjzlaqhghdlpia62md4hm447xm295fx9nan19d9") (yanked #t)))

(define-public crate-my_crate_xzlinux-0.0.1 (crate (name "my_crate_xzlinux") (vers "0.0.1") (hash "1jcribmr40yr5vi8bwbrfhl06dicfjqvjk00kjwn0wzwj1npr0yv") (yanked #t)))

(define-public crate-my_crate_zz43222-0.1 (crate (name "my_crate_zz43222") (vers "0.1.0") (hash "1mqnl095ys2l11snj2m3rpvg8kd37aw9bqn5dcn30akahz0nsk9y")))

(define-public crate-my_crateyyyc-0.1 (crate (name "my_crateyyyc") (vers "0.1.0") (hash "0hxr6y0w7d5wf5v7jm1avv2w38hhxif241pvy6pfrq356vvi0jrz")))

