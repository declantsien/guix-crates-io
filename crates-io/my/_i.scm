(define-module (crates-io my _i) #:use-module (crates-io))

(define-public crate-my_internet_ip-0.0.1 (crate (name "my_internet_ip") (vers "0.0.1") (deps (list (crate-dep (name "curl") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1jzz02b7h59fcbk7rxax3rgks74hcqga51k8ab937kaxhqp8axii")))

(define-public crate-my_internet_ip-0.0.2 (crate (name "my_internet_ip") (vers "0.0.2") (deps (list (crate-dep (name "curl") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "19lczy3qmp4wxv2yq6vidji6161w0ggiqg0h0796c3sg27yga0z6")))

(define-public crate-my_internet_ip-0.0.3 (crate (name "my_internet_ip") (vers "0.0.3") (deps (list (crate-dep (name "curl") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0gb3bwslvgahcsq84r6hd7ygqpa4q17r1pa0a3xarimh5kb4dl52")))

(define-public crate-my_internet_ip-0.1 (crate (name "my_internet_ip") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "14p4rs0hyc4sj16f87ygch7lmb3hbkf82cd3aw97l6z222ahjkav")))

(define-public crate-my_internet_ip-0.1 (crate (name "my_internet_ip") (vers "0.1.1") (deps (list (crate-dep (name "curl") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "05gn9dgyqv540dln71rhgjqvl9ki5jlsnaabqkffwffkj8vxic6d")))

