(define-module (crates-io my ex) #:use-module (crates-io))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.0") (deps (list (crate-dep (name "my3") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11wz0gnfy4znm0i7v940fz3za1jc1dy55az9c2dnd37ncirbwj31")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.1") (deps (list (crate-dep (name "my3") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yd6fd8vm51mzjwk6g97984mf4c5vgfvnz0l40im586bkrlsj47v")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.5") (deps (list (crate-dep (name "my3") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "11vs1irqzm0rp0c5cjckc17655v7bjf01i50sx85hkjin8agzd52")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.3") (deps (list (crate-dep (name "my3") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1bb3ww4mjws8x6m7s9vgqn9nxn67f27dgnlc77dyvwz0vkbb3hh9")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.4") (deps (list (crate-dep (name "my3") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1cksj4gk72gpjmghsznn3v9wyxnqg64nlpkfmlzm26bnv2mwqh39")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my3") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "1f11glr785ihf9l451z2axxykrnfi17fv36ssawx8ka9x06gzsyf")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my3") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "057zvnnf7jj5jbrb8gg8n1ws2w257i8mq7jg5pnv1a6cwcb4c6nd")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my3") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "144mqchqa6xkypllgqymir6hzqi4c5qrp8h974hsb7q7gdlvpqym")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.9") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my3") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "08rklsl15zp27jl10qhybn2ipk5s195jlca09ac5v5kx4w9s9m4i")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.10") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my3") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "0a7868ddzdwvzvpm896gw2giy4n0zs4zqwp0w12bam9i79c1ygi2")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my3") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "1s62yfviwkf707hcj5bxlyhnnkjfc4y6lisykmgb4cfr977bcn9f")))

(define-public crate-myexe-0.1 (crate (name "myexe") (vers "0.1.12") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my3") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "036lw490yqdr9rnnmyx2zspz6glylqrn51zp70sdamnh36n6bcz5")))

