(define-module (crates-io my _e) #:use-module (crates-io))

(define-public crate-my_encryption_lib-0.1 (crate (name "my_encryption_lib") (vers "0.1.0") (hash "057gfkfc9878chpdg3dbrwglv31l4s3vkmrhxc2hfx4hijg6lg5z")))

(define-public crate-my_example-0.1 (crate (name "my_example") (vers "0.1.0") (hash "1cdfq2ail9ippp3vxd64dc9y2yd9139w6l3cdxknm126baqrr70c") (yanked #t)))

(define-public crate-my_example-0.1 (crate (name "my_example") (vers "0.1.1") (hash "1ckac02x3bj2hbnkl278lr2k0jir3nvx162cw4qxzkcyznmbwmhm") (yanked #t)))

(define-public crate-my_example-0.1 (crate (name "my_example") (vers "0.1.2") (hash "01yxc2n5idmr2xxjpwg1s2srm0xnmc439lsb4z2fn4hxx2cgzmpa")))

