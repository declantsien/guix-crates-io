(define-module (crates-io my bi) #:use-module (crates-io))

(define-public crate-mybindgen-0.1 (crate (name "mybindgen") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1fgbkjbbfsh55xsnm0jyvqqv0njwabyfpadm7wpn4s8bnx5jxi4c")))

