(define-module (crates-io my _t) #:use-module (crates-io))

(define-public crate-my_test-0.1 (crate (name "my_test") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0ii35pisj1b5cf0jwg4pfdniy8282p43yn853xypwxk2pkqys9ll")))

(define-public crate-my_test_crate-0.1 (crate (name "my_test_crate") (vers "0.1.0") (hash "1dspaaffnhdlcidlgd6dmp09vkg798q8mqgvkbjgwwi9x4cx4i20")))

(define-public crate-my_test_crate-0.1 (crate (name "my_test_crate") (vers "0.1.1") (hash "1jgwr1vfyksq5n8nknc85zp85k3shyjghqwbmc105lmx0b37p0mk")))

(define-public crate-my_test_crate_hello_world-0.1 (crate (name "my_test_crate_hello_world") (vers "0.1.0") (hash "0s9ch8gd1apz63iw3fkhk6dd20nxdz93fqs5qkiqzws9h4cll60c")))

(define-public crate-my_test_for_crates_io-0.0.1 (crate (name "my_test_for_crates_io") (vers "0.0.1") (hash "15bm5m8jcphhcm0440n18fhw9hzz7qf3d8l7iiha7fnhakb9ca3c")))

(define-public crate-my_test_lib-0.1 (crate (name "my_test_lib") (vers "0.1.0") (hash "0czjshs33gs0sdxphrip3l8p8c1wqkiz9aqrznzsdq1d9mc0708k")))

(define-public crate-my_test_package_1234567-0.1 (crate (name "my_test_package_1234567") (vers "0.1.0") (deps (list (crate-dep (name "array_tool") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0xq417plz9y28namg6xnhq3w2x4y5lsp7apkmlyr2xzbfwfwkvm4")))

(define-public crate-my_threadpool-0.1 (crate (name "my_threadpool") (vers "0.1.0") (hash "0wwgmp5icfcldzxn6wkspqjba5c9wrs0g05pn6fvhlkfd0d27y31")))

(define-public crate-my_threadpool-1 (crate (name "my_threadpool") (vers "1.0.0") (hash "0s6zkjnjc89p2njmbgpx895ns8dihfjz1qi5sc5hb31lyjhk1cj4")))

(define-public crate-my_threadpool-1 (crate (name "my_threadpool") (vers "1.0.1") (hash "1qp988f9a7zrsxkxxmrfb456k6z4p228jjcnpf0n22g0j8ja9qh4")))

(define-public crate-my_tools-0.1 (crate (name "my_tools") (vers "0.1.0") (hash "1j1304q213kzz6v3v7ajf61ym1xnla1cvw4gvajc51klv2fbsdzy")))

(define-public crate-my_tools-0.1 (crate (name "my_tools") (vers "0.1.1") (hash "16gscnb7vil2wj3ald6p7b0nnaissi9nwhpfk2nd82dyw07wfa9s")))

(define-public crate-my_trpl_test-0.1 (crate (name "my_trpl_test") (vers "0.1.0") (hash "1smswj0v8ip07rxcxngdwk94xdxr19lv3mqki1jr9ghwzgfvri9b") (yanked #t)))

