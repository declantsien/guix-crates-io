(define-module (crates-io my pr) #:use-module (crates-io))

(define-public crate-myproject-0.1 (crate (name "myproject") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "16g7psb0ryfsnjr5kqcx1lbpqcrhk4bllwx3i1wdwnqqwb90fg17") (yanked #t)))

