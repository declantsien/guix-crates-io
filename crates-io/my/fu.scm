(define-module (crates-io my fu) #:use-module (crates-io))

(define-public crate-myfunc-0.1 (crate (name "myfunc") (vers "0.1.0") (hash "08idy772m1kjvbbl1bi9wximqsfa3di5iifjv5jrbkjghdik1sl5")))

(define-public crate-myfunct-0.1 (crate (name "myfunct") (vers "0.1.0") (hash "1pzkj59p5z0cbg5j3f68m4kh2jgmd7hhyb7smyvgpbmh9jknfzn9")))

(define-public crate-myfunction-0.1 (crate (name "myfunction") (vers "0.1.0") (hash "08gw512n9fj8d02vdiwx1zjwyad71wj68hdwwsla8ym2lxwsw1k9")))

