(define-module (crates-io my yr) #:use-module (crates-io))

(define-public crate-myyrakle-jwt-0.1 (crate (name "myyrakle-jwt") (vers "0.1.0") (deps (list (crate-dep (name "epoch-timestamp") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "jsonwebtoken") (req "^7.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "011hxm296xlppv3iw76i8w7h21x8hd4x91sxg91pmnkp84bj8bs6")))

(define-public crate-myyrakle_boom-0.1 (crate (name "myyrakle_boom") (vers "0.1.0") (hash "15yq8nkpxf85njqryd8lg3h9xwp26lsb88rncq6wy210psyagfi0")))

