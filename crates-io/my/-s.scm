(define-module (crates-io my -s) #:use-module (crates-io))

(define-public crate-my-sq-bro-0.1 (crate (name "my-sq-bro") (vers "0.1.0") (hash "0p77xcjmi24b0032i8kh7665dv8mb25d058z9v18ygp5aiy5bfgh")))

(define-public crate-my-stack-0.1 (crate (name "my-stack") (vers "0.1.0") (hash "0dghj8dba1185f0pzq0xdw5p3f7f9i8mb650zkmp77p0mj8zrvjs")))

(define-public crate-my-stack-0.1 (crate (name "my-stack") (vers "0.1.1") (hash "1lm1ac5jrwr6izp74qinqsq3h8fvjzsc0r2dwnidi7za4fb7c219")))

