(define-module (crates-io my ss) #:use-module (crates-io))

(define-public crate-myssh-0.0.1 (crate (name "myssh") (vers "0.0.1") (deps (list (crate-dep (name "cursive") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0mgzb7gqn2g0dcqiinkfqzvmiqd7gid9v3nvkqqijiqbp553i274")))

