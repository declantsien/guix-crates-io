(define-module (crates-io my -o) #:use-module (crates-io))

(define-public crate-my-own-uuid-0.1 (crate (name "my-own-uuid") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "16dpzwmzkw413y81dbpifhklsipj5fcwbfj6gnv04ksn21yqwiz7")))

(define-public crate-my-own-uuid-0.1 (crate (name "my-own-uuid") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1qvwg7d4l6kr6qvbijmghvzvvy55km9kypcv0cjj90yv8mnjidqq")))

(define-public crate-my-own-uuid-0.1 (crate (name "my-own-uuid") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "15p0b7r7q5hyrk6lizv1gl4704v8hy4350sx2ixs5b5fkagfk8ac")))

