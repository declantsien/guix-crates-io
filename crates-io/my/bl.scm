(define-module (crates-io my bl) #:use-module (crates-io))

(define-public crate-myblog-proto-rust-0.0.1 (crate (name "myblog-proto-rust") (vers "0.0.1") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3") (default-features #t) (kind 1)))) (hash "0jp3a4grmqpc5ch2wkmz5i4lnaa9srf9qy3j0hfw2iymrgl44cld") (yanked #t)))

(define-public crate-myblog-proto-rust-0.0.2 (crate (name "myblog-proto-rust") (vers "0.0.2") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3") (default-features #t) (kind 1)))) (hash "0rlhm63j2pad5r1scmyd0pvgjzzjhd4c3yfqrrc8why4m4bkdrxr") (yanked #t)))

(define-public crate-myblog-proto-rust-0.0.3 (crate (name "myblog-proto-rust") (vers "0.0.3") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3") (default-features #t) (kind 1)))) (hash "0lhpnqc26bqhhz4qha2205n3lz2ml0zwq8y19dj31nrdbqs246dv") (yanked #t)))

(define-public crate-myblog-proto-rust-0.0.4 (crate (name "myblog-proto-rust") (vers "0.0.4") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3") (default-features #t) (kind 1)))) (hash "1jm53rpyhzbr1rffnf9znyakzjfpaprbh4l2qnkj357v146bzrc0") (yanked #t)))

