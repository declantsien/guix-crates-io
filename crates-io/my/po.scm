(define-module (crates-io my po) #:use-module (crates-io))

(define-public crate-mypool-0.1 (crate (name "mypool") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4.8") (features (quote ("postgres" "r2d2"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)))) (hash "031627wyy8mrpk445vjpdk6kc3illhcp81547w295hrqp8bdivx7")))

