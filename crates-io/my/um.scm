(define-module (crates-io my um) #:use-module (crates-io))

(define-public crate-myumberbar-0.0.1 (crate (name "myumberbar") (vers "0.0.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "umberbar") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "16vvngsd6s5rgjc59nbi2higgkb8csyi7vhya14ady2fknlzcfn1")))

(define-public crate-myumberbar-0.0.2 (crate (name "myumberbar") (vers "0.0.2") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "umberbar") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "1m16wgbds3gypn8iqqz7fcmxishf520r4g15ndi0ni0pcc7gkw8v")))

(define-public crate-myumberwm-0.0.1 (crate (name "myumberwm") (vers "0.0.1") (deps (list (crate-dep (name "umberwm") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0fwckwbmalc1yc8i0hsgsf9s835x96skqvnw9hrs4mbvzb6rscrc")))

(define-public crate-myumerwm-0.0.1 (crate (name "myumerwm") (vers "0.0.1") (deps (list (crate-dep (name "umberwm") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1wj91l68x9jcvhw94i9lrk918s9frz0ylrbcwpffvl70a0ywjs04") (yanked #t)))

