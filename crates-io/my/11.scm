(define-module (crates-io my #{11}#) #:use-module (crates-io))

(define-public crate-my117-0.1 (crate (name "my117") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0434p3xwhhkkpqbsbpg1yfdd5mszy98b9r5dvm2q7frvgicwc6ds")))

