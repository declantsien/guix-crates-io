(define-module (crates-io my _g) #:use-module (crates-io))

(define-public crate-my_grep-0.1 (crate (name "my_grep") (vers "0.1.0") (hash "0mfik2bz12vwkppannf8mrnpw4ln0r7bxly6n634wsikj8ra01yi")))

(define-public crate-my_guessing_game-0.1 (crate (name "my_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1pynnlqbd0jn96nl17cy7zy4y3qapvr157rmhnj96ifk40z7b0i4")))

(define-public crate-my_guessing_game-0.1 (crate (name "my_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0156bq9rpd2ggpccnl107q0n9g14abg6xsxnr7mamcp4hgrdwjcr")))

(define-public crate-my_guessing_game-0.1 (crate (name "my_guessing_game") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1lhvhr5gh52sbyxrdpz06gdpjahypf82f452b7hzfjq7rbhgz91r")))

(define-public crate-my_guessing_game_crate-0.1 (crate (name "my_guessing_game_crate") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0n5aww18rfqci5fgqy8pm7c7999vcjh3f9nrhflh7ny4srs5d8yq")))

