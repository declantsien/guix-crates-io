(define-module (crates-io my -r) #:use-module (crates-io))

(define-public crate-my-rust-lib2-0.1 (crate (name "my-rust-lib2") (vers "0.1.0") (hash "06jlvr9mawnp7i4smv7sszihjm9i4bhmfvrz9ds6ssms6952a7cr")))

(define-public crate-my-rust-lib2-0.1 (crate (name "my-rust-lib2") (vers "0.1.1") (hash "0y12xgy17jyg31ij1dzk3qks0857vh6y3fn1wygpfh23xfzajzw8")))

(define-public crate-my-rust-lib2-0.1 (crate (name "my-rust-lib2") (vers "0.1.2") (hash "105cnxwb6ivkkqkfm57v2f0wgc5cd04cgfcy2shzjh4vmy951pys")))

(define-public crate-my-rust-lib2-0.1 (crate (name "my-rust-lib2") (vers "0.1.3") (hash "0vmilkdsc9mxd9104c0qh8fv6hxigk4fka440xsm1f1jhdkgmcgq")))

(define-public crate-my-rust-lib2-0.1 (crate (name "my-rust-lib2") (vers "0.1.4") (hash "0hw3sxbp83n6an79wf5r9fga69fl9kb40m8z55fhn8847jp8a7cx")))

(define-public crate-my-rust-lib2-1 (crate (name "my-rust-lib2") (vers "1.0.0") (hash "1fhd2gq5hp8aqm7wkan4bcwmx0wrdp5p2330c8ln85nc4raszcyh")))

(define-public crate-my-rust-lib2-1 (crate (name "my-rust-lib2") (vers "1.0.1") (hash "0807wnfihcmmsqjr9i0zxc6mqzw4z9s6yylzz78a0xn73b4idbr1")))

(define-public crate-my-rust-lib2-1 (crate (name "my-rust-lib2") (vers "1.0.3") (hash "1w1g8pjqmr4msszd2jnr29mmb6inm5m85kgbrkjhb82587qambn8")))

(define-public crate-my-rust-test-0.1 (crate (name "my-rust-test") (vers "0.1.0") (hash "1vsfh6qdvssb6n0wqzkmndbn7576vf3m7lr7ga587bj2ahmx0za5")))

(define-public crate-my-rust-test-0.1 (crate (name "my-rust-test") (vers "0.1.1") (hash "1p6d0ccdhqacgxfmgm52i8l08h4srbp4xqms34wn5f7njk34hlpa")))

