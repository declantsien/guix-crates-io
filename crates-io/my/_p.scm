(define-module (crates-io my _p) #:use-module (crates-io))

(define-public crate-my_package-0.1 (crate (name "my_package") (vers "0.1.0") (deps (list (crate-dep (name "array_tool") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1vzy5f7x5gka85ixjf8c28nfgnl6x6s7m9k10gg99k2q62kdk4mh")))

(define-public crate-my_package_danielgeek-0.1 (crate (name "my_package_danielgeek") (vers "0.1.0") (deps (list (crate-dep (name "array_tool") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "119m1xw1qsk9qxv6jqj6ghwr5xjr79fjnkhixrg5z29fjzqis9nd")))

(define-public crate-my_package_smarulanda97-0.1 (crate (name "my_package_smarulanda97") (vers "0.1.0") (hash "0bv4mbgqc08yws7phdqjp70wh9lh9wzzfwmcbz44as9r3imql4jn")))

(define-public crate-my_par_k-0.1 (crate (name "my_par_k") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1g5hiajcl0i4zxni9wsikv0jfr1nkyan8bkz038qvhl0dkmbvya9")))

(define-public crate-my_parser-0.1 (crate (name "my_parser") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1ck3z4ifb7zbzrg0d5v9sq2yv3mbxakhwxcwr9rbn06krimwnk66")))

(define-public crate-my_parser-0.1 (crate (name "my_parser") (vers "0.1.1") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1xfw6dfp158z05crxbxir4cfxsp1wxmbighj4lp0virmwxdh88yz")))

(define-public crate-my_parser_hVpo32Dds-0.1 (crate (name "my_parser_hVpo32Dds") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0ynfl3lp3p81hsc2hs63y4vgc6m7q14d5l5pd19g4qdkdpli0jls")))

(define-public crate-my_parser_hVpo32Dds-0.1 (crate (name "my_parser_hVpo32Dds") (vers "0.1.1") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1g25njkibr9zagzfmm8c6g4kx82kp7k6xg3h5z0dd7mq4ham0xvg")))

(define-public crate-my_parser_hVpo32Dds-0.1 (crate (name "my_parser_hVpo32Dds") (vers "0.1.2") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1xbmilw9s38ninzad0k784szwhmhampdf423ilcqd9ipd5aknazx")))

(define-public crate-my_parser_ishevchyk-0.1 (crate (name "my_parser_ishevchyk") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1mcl649xxvrwp8gil6pajq4x4c40kpvb2rkyp6lfhaadrdr756br")))

(define-public crate-my_parser_ishevchyk-0.2 (crate (name "my_parser_ishevchyk") (vers "0.2.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0d8c3qzk5r2fwjapvsv28857p9yvvk7lpicbzdhypcg6mfk4qa81")))

(define-public crate-my_parser_kma_group3-0.1 (crate (name "my_parser_kma_group3") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0z019iy25x877d6k0d80fyfaf1sa3im6c3w4x25yjpqi7ddqzix9")))

(define-public crate-my_parser_kma_group3_Smetaniuk-0.1 (crate (name "my_parser_kma_group3_Smetaniuk") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "17sdscjc0vl2r4ax5lwmcppxj68pklvxjn2jkbf4g7rlq6xss8g6") (yanked #t)))

(define-public crate-my_parser_kma_group3Kovalenko-0.1 (crate (name "my_parser_kma_group3Kovalenko") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "10fpk81vrxyl3p5r9vnnj243yd3rl34fscrrr6s7skfzikqb223x")))

(define-public crate-my_parser_kma_group_1-0.1 (crate (name "my_parser_kma_group_1") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "04pxzz0k0wkcf220pbvqv9ar2rbyf6lfplkd21jrx55aqr7mnqld")))

(define-public crate-my_parser_kma_group_1-0.2 (crate (name "my_parser_kma_group_1") (vers "0.2.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0nhq1izv5r2f76cx88w92z3s26rsx130ax7n8c17x770ihg63l9p")))

(define-public crate-my_parser_kma_group_2-0.1 (crate (name "my_parser_kma_group_2") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1h6gx7vhyzslaw6x4vdncp7805swj9jx02s0a8v5d3rg3cla7wwx")))

(define-public crate-my_parser_kma_group_3_DB-0.1 (crate (name "my_parser_kma_group_3_DB") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0sanwxc1n7iq3av1majf1n6crbsqjhxxcxq8ai7bkkq6khffim61")))

(define-public crate-my_parser_kma_group_3_Kharchenko-0.1 (crate (name "my_parser_kma_group_3_Kharchenko") (vers "0.1.0") (deps (list (crate-dep (name "my_parser_kma_group_1") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1ghad611fm9lhni5534sivsnxwb87ll7dk4gxm7b0amm4hvarrxd")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1qisxpxdmswdca3m1dkfgam8wmwgwnqz8gss631pflyms6i9hmba")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.1") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "04az89507c3zbr2hzv44qnrgrsw6mxb1li7w4v5kg2ad5c3ayvx1")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.2") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1sfhs6h87bksvrbgk3p38ckq4wnwbibdhmk0rkg5dfpcdrqvli0i")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.3") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0w4qk511dz4m7xbl5d34bzkh1422ndshsxx0c1346ml2c3xswbf3")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.4") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0dy99ia40f8j9vn7n44hvpw615l3zja3qgi9kxvyfbnfq8nm7l8g")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.5") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "02vywc8d1z6nkdc0q507h2v0l035d0j8ijljmfz44raf0hi5i5cv")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.6") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1khs7sr43i12cq2ng0zil05krpb52pcy9xwxiy4jy676hflbdf43")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.7") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "01cg958h5qwc76x5i826sc0pslnx1rp90kcn1fhikc65y356sh98")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.8") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0fwgln5miqk8937sim74fb57j5aa3s2pyzh65g09ny70w5g3a1yp")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.9") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0jaziqfjx4q6bq6n9d1j8cvc8syf0w0a9hwlp2jr5m27n2v47s84")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.10") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0vi3nwy93j3b6bmh87iyz0hihb30xb88fmpfyixqs4grza4yxj1x")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.11") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0019sdfj8lg5gwji5q20b47kzsm502gi0za63r9y8ycxxd412bzn")))

(define-public crate-my_parser_kma_group_test_1-0.1 (crate (name "my_parser_kma_group_test_1") (vers "0.1.12") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1ckvvwnx963pmf21f5njigv5c609w66scphnaswwbpllcq3a27zq")))

(define-public crate-my_parser_kma_group_test_2-0.1 (crate (name "my_parser_kma_group_test_2") (vers "0.1.13") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "074n39g25qzq4cni77f5qxl595jxzi4f2vj56gnbwdywcih6zr0r")))

(define-public crate-my_parser_kma_group_test_2-0.1 (crate (name "my_parser_kma_group_test_2") (vers "0.1.14") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1a3z2pb7h2zn7fz6barkkd6pnclbqpbq57xx2f2nyz36v4s26cwa")))

(define-public crate-my_parser_kma_group_test_2-0.1 (crate (name "my_parser_kma_group_test_2") (vers "0.1.15") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "06ii5vd9085hisl9x8kag4rk8h14bzmdw6b8fhbq4mxdrrldn9pc")))

(define-public crate-my_parser_kma_group_test_2-0.1 (crate (name "my_parser_kma_group_test_2") (vers "0.1.16") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0smh15yxjksz8x3mmynnkzm4cbgf47yl16zwgj1zr0xikdq3skaq")))

(define-public crate-my_parser_kma_group_test_2-0.1 (crate (name "my_parser_kma_group_test_2") (vers "0.1.17") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1s6pp2qf0n1s3nv0qlkk8khwf0rs1bp7m7pc8zxar18g9plgd0hi")))

(define-public crate-my_parser_kma_group_test_2-0.1 (crate (name "my_parser_kma_group_test_2") (vers "0.1.18") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1imiiym1jzvzxl9ga67878n05rviwi8jy2p45p7shq1wigawk3kg")))

(define-public crate-my_parser_kma_group_test_2-0.1 (crate (name "my_parser_kma_group_test_2") (vers "0.1.19") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0z5py4d9l54dal1p4v8g8yympxgl3gch33m5mxpr1qprmbl1s088")))

(define-public crate-my_parser_kma_makhynia-0.1 (crate (name "my_parser_kma_makhynia") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1razavx88v2a252j520wjwxf6ga209a78xvbr59i8j5g5lqfckyj")))

(define-public crate-my_parser_koder-0.1 (crate (name "my_parser_koder") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "06ygvmbbq7jq32ampq53mgzlr8nc2wb81fxkm6frzh22mzh44icx")))

(define-public crate-my_parser_Lakhtiuk-0.1 (crate (name "my_parser_Lakhtiuk") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1q0dqmjhd19y29hmcficyi4s11p9barai7nj8dpxm80nz9s516wf")))

(define-public crate-my_parser_oleksii_parser-0.1 (crate (name "my_parser_oleksii_parser") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0b1w5wgqhlrv5bg14f6vgc2aiiqbma4jkqbfc7gblyb5phzz0r4g")))

(define-public crate-my_parser_practice_3-0.1 (crate (name "my_parser_practice_3") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "06iimisizggdc137pwf3n7rf09pqj0k0glzfg00k1kir6ijprbc3")))

(define-public crate-my_parser_practice_3_1-0.1 (crate (name "my_parser_practice_3_1") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1lafxaksdhxpqdvig38kh9s42qz6863frw0ihc1whpry4qmnnrbq")))

(define-public crate-my_parser_practice_3_2-0.1 (crate (name "my_parser_practice_3_2") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1z4hjq8yc98b27sb6w6nwprkj5g42ndj7bflcz6m9f94vxc8yp4n")))

(define-public crate-my_parser_practice_3_3-0.1 (crate (name "my_parser_practice_3_3") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0msc0b67rjz6pdymf51b7z6h8ysmh315l1iqaj3nrba6hrwgxzy2")))

(define-public crate-my_parser_sofia-0.1 (crate (name "my_parser_sofia") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1qvk3v4xdkb9i9n2dnpnwx9g23k1zvx53vavk3rf1wm6w2ays055")))

(define-public crate-my_parser_ukma-0.1 (crate (name "my_parser_ukma") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1yp1s9rnzcnn76k44g9nhpnfwgi0z9m80mfgj02cfkxvjc5j3qyw")))

(define-public crate-my_parser_verbytska-0.1 (crate (name "my_parser_verbytska") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1ibrxk2s8ixnw58l7xldzarb4nqq91qjk27dfdqcczhp04lwrdll")))

(define-public crate-my_parser_yaroslav_fetisov-0.1 (crate (name "my_parser_yaroslav_fetisov") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0lv4rdizp0giskh2rzrx3q16699989h0mia4fcvv42gcqmwjy00d")))

(define-public crate-my_parser_yaroslav_fetisov-0.1 (crate (name "my_parser_yaroslav_fetisov") (vers "0.1.1") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "160x0g2gzfrzfyizggwp7x38ld9nkvi4aq61vvlh90fca4qxp4x3")))

(define-public crate-my_proc_macro-0.1 (crate (name "my_proc_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nyghzxsbrshjra3lx6jynmsfyxqbg0xdwm53vvypf11gg5q505q")))

(define-public crate-my_proc_marco-0.0.1 (crate (name "my_proc_marco") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i0xaqbzr6axdsb3r3b5kpvnm2h1fa2kng6kf1zzf11fzzjbvii8")))

(define-public crate-my_proj-0.1 (crate (name "my_proj") (vers "0.1.0") (hash "0g2c90axd4585nifi71d7vjjm3gvc0nrsmy03dchl0klj59b109s") (yanked #t)))

(define-public crate-my_proj-0.1 (crate (name "my_proj") (vers "0.1.1") (hash "1akwqsfmz7ygjf54rcp69x1hgf106i7r5pfsgcqdqfl68y1ixjfa")))

(define-public crate-my_project0-0.1 (crate (name "my_project0") (vers "0.1.0") (deps (list (crate-dep (name "test0") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1xl03j6p4dkcjmvc20yckpfkx62z8qiqipwx3g8al0yqkqwp6rm3")))

(define-public crate-my_project0-0.2 (crate (name "my_project0") (vers "0.2.0") (deps (list (crate-dep (name "test0") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "10ajqnxbkf123x8yl32ydxvadjnb5frrzxa9ihc5pvfcq62s2q0c")))

(define-public crate-my_project0-0.3 (crate (name "my_project0") (vers "0.3.0") (deps (list (crate-dep (name "test0") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0sxd3cv15qxy1b6han5ppfs9226xh2hrv2d21k64lnlgwrgd2ghp")))

(define-public crate-my_pub_lib_proj-0.1 (crate (name "my_pub_lib_proj") (vers "0.1.0") (hash "1ncj1v58x3p6d8jpij8dj78s94v4vrsnvwkql12brp4i04sm75m8")))

(define-public crate-my_public_crate_hh-0.1 (crate (name "my_public_crate_hh") (vers "0.1.0") (hash "1nd03d7a7rqx75xm7mg3y19krhk2kwwvxgdrhag6jkz7x3i6ydn5")))

(define-public crate-my_public_ip-0.1 (crate (name "my_public_ip") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-resolver") (req "^0.10") (default-features #t) (kind 0)))) (hash "1aj7f07l7b7hcvn71h3p2hjffmb512a3sj3ym7l0svraa9d14xcm")))

(define-public crate-my_public_ip_client-0.1 (crate (name "my_public_ip_client") (vers "0.1.0") (deps (list (crate-dep (name "my_public_ip_lib") (req "~0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09vgrs8zq7m2kd0zx8cyn89m93nzc566r97r2bj0jrs63y35ywss")))

(define-public crate-my_public_ip_client-0.2 (crate (name "my_public_ip_client") (vers "0.2.0") (deps (list (crate-dep (name "my_public_ip_lib") (req "~0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y1c9xm0cgr3zvbjd68sxqlyx9aq3lclw3vdcgg8l8kiaqnc9xxz")))

(define-public crate-my_public_ip_client-0.4 (crate (name "my_public_ip_client") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12pzyx5jjvsp9fsrmnd4f2z2fgf30wra46nk5cqsx4bi8qmqna8j")))

(define-public crate-my_public_ip_client-0.5 (crate (name "my_public_ip_client") (vers "0.5.0") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h6116zsgm4xmvm8ab45rqa30msvlazyr74s1risrr3fmbcj95xs")))

(define-public crate-my_public_ip_client-0.6 (crate (name "my_public_ip_client") (vers "0.6.0") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dz0ndwmvpdxy8919xhns2cidr1jjd5a8dlmh0whgccz566f8b5k")))

(define-public crate-my_public_ip_client-0.6 (crate (name "my_public_ip_client") (vers "0.6.1") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1w2dapw9b1j60g5llizlxlx0dw0ijyqqjkrx3nw4izynmcfdpx8c")))

(define-public crate-my_public_ip_lib-0.1 (crate (name "my_public_ip_lib") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qr6q90ibin4bbayyg3lfhmxxj1s0y2a2ds4l0zkv9lc1k4d11ib")))

(define-public crate-my_public_ip_lib-0.4 (crate (name "my_public_ip_lib") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r9gmglw9ix4bfs4r8qjjri4ck9ahdk166g54zcyvgv862rmw6vy")))

(define-public crate-my_public_ip_server-0.1 (crate (name "my_public_ip_server") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^3.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "~0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "03hfp749xiipc3p1hy249i43sklh8nnjvhy3fc3rxvvkgf9nf0kb")))

(define-public crate-my_public_ip_server-0.2 (crate (name "my_public_ip_server") (vers "0.2.0") (deps (list (crate-dep (name "actix-web") (req "^3.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "~0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0igbgsa1mixi5y83xkj48k0sy7llrqv43lwraln0jgibd7pyqjqp")))

(define-public crate-my_public_ip_server-0.3 (crate (name "my_public_ip_server") (vers "0.3.0") (deps (list (crate-dep (name "actix-web") (req "^3.3") (features (quote ("openssl"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "~0.1.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "138cbbvckwrha4vmmi7g2wvh3w4c48dlsijia9cgyfm1q8k87g4n")))

(define-public crate-my_public_ip_server-0.4 (crate (name "my_public_ip_server") (vers "0.4.0") (deps (list (crate-dep (name "actix-web") (req "^3.3") (features (quote ("openssl"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1327ww6g7i8r2qiasvs1sxfqg46073823v2s4fqfxnd489dw2ahy")))

(define-public crate-my_public_ip_server-0.5 (crate (name "my_public_ip_server") (vers "0.5.0") (deps (list (crate-dep (name "actix-web") (req "^3.3") (features (quote ("openssl"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "05n6cjnz98kzmv5vya6ic9462454r8r549vfvya24lf01135vllh")))

(define-public crate-my_public_ip_server-0.6 (crate (name "my_public_ip_server") (vers "0.6.0") (deps (list (crate-dep (name "actix-web") (req "^3.3") (features (quote ("openssl"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "my_public_ip_lib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0mskwf28hi49ldx3f088fiw120paay1xzmzaywyyzxyb8g2smhvw")))

