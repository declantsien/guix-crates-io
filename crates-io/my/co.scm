(define-module (crates-io my co) #:use-module (crates-io))

(define-public crate-mycobot-0.1 (crate (name "mycobot") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "1zy28fc1zbfrfwad89d62irybk67kpvcn7lrgjn7m1mnsbwjb1v9")))

(define-public crate-mycobot-0.2 (crate (name "mycobot") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "1bv6xh0xqha7522pbp35skim88qcz3dv4l888nqxf0kg14mjcr95")))

(define-public crate-mycobot-0.3 (crate (name "mycobot") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "15xcw58l1l3wn4gf1w0f0z5rrdzki352g1zap4g9d7fnvd47bsmp")))

(define-public crate-mycobot-0.4 (crate (name "mycobot") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "1i4vk7kjjpd16an0c43zjg4dpc72gfgrmfm7id45xamgf28n4zhq")))

(define-public crate-mycodee-enigma-0.1 (crate (name "mycodee-enigma") (vers "0.1.0") (hash "0h0iisxg0pqf952nwny59h1461l3pf5pz7lji1lmsdj8h858a44k") (yanked #t)))

(define-public crate-mycodee-enigma-0.2 (crate (name "mycodee-enigma") (vers "0.2.0") (hash "0rh4ypdyhl4w8hvb6bx21q26kpifdf68cfh9397cxrbiq5ifa4ar") (yanked #t)))

(define-public crate-mycodee-enigma-0.3 (crate (name "mycodee-enigma") (vers "0.3.0") (hash "1jdy84zv0j1ik87dw3wcqjy0qw7fhv2ic8z8nn4yc7rgjxq1v6bx") (yanked #t)))

(define-public crate-mycodee-project_manager-0.1 (crate (name "mycodee-project_manager") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1cd7zin79n58s304gz9n4mnw3cs0rdl2bb91iha08y44w3brrsfr")))

(define-public crate-mycodee-project_manager-0.1 (crate (name "mycodee-project_manager") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "05cyw7165900ps4xfl0bb9wm0j2c9dx11d2qrijskrsgqgla5na1")))

(define-public crate-mycon-0.1 (crate (name "mycon") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0k3r1rny943f7fqrki64ipddqhmp40dk229g13nin5d34vy2hvjf")))

(define-public crate-mycon-0.2 (crate (name "mycon") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0wfbmg0khyhg89jh4qlcm482cipz5z3j2zmk0nfbcwg23qpkqmvh")))

(define-public crate-MyConsoleClicker-0.1 (crate (name "MyConsoleClicker") (vers "0.1.0") (hash "0jxcxvgq8305nip9gsl309in4azd9azppkjxyq2zm73w4jgqxd0h")))

(define-public crate-MyConsoleClicker-0.1 (crate (name "MyConsoleClicker") (vers "0.1.1") (hash "1hhkv0bhgq54fz5x7i2spp2xmdjws3p55mwslvvk9id6lz208fdm")))

(define-public crate-mycorrh-0.1 (crate (name "mycorrh") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c1zbp96pbkqlvr6icdd9pk6avkjvgy3mwgavfdbwh9gc6pigxzj")))

(define-public crate-mycorrh-0.1 (crate (name "mycorrh") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f31j707cazf4i70fg19riaszcswf7xqmqfwag787w7v7l9f35p3")))

(define-public crate-mycorrhiza-0.0.0 (crate (name "mycorrhiza") (vers "0.0.0") (hash "0pf12gy1gqad9a69id614xpv07q8r88yvpxsv85farkcpsf9f46x")))

