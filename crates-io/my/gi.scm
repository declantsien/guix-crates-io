(define-module (crates-io my gi) #:use-module (crates-io))

(define-public crate-mygithub-0.1 (crate (name "mygithub") (vers "0.1.0") (deps (list (crate-dep (name "dialoguer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.101") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1d5fhp02ybavz5qmski8d1rnmb922jbbgc5kw5672ywj4da20w45")))

