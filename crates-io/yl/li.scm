(define-module (crates-io yl li) #:use-module (crates-io))

(define-public crate-yllininin_guessgame-0.1 (crate (name "yllininin_guessgame") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "14yyv198iqwzc06wicnaqa7y9p5n081hw49flfvgbk1p14ggylxa")))

(define-public crate-yllininin_minigrep-0.1 (crate (name "yllininin_minigrep") (vers "0.1.0") (hash "0sfzhq9gvvm6iqqz4fkafg0wywm56w7cgxpgyd516gh3bxn62cn7")))

