(define-module (crates-io f1 d2) #:use-module (crates-io))

(define-public crate-f1d2d2f924e986ac86fdf7b36c94bcdf32beec15-0.1 (crate (name "f1d2d2f924e986ac86fdf7b36c94bcdf32beec15") (vers "0.1.0") (hash "111i2pdwc35gjmiqnf0p4n4y6ipczsrmnlb8jbvnblq47ysrw423")))

(define-public crate-f1d2d2f924e986ac86fdf7b36c94bcdf32beec15-0.1 (crate (name "f1d2d2f924e986ac86fdf7b36c94bcdf32beec15") (vers "0.1.1") (hash "1sp5f5y7v4r0wdsvxpmjml5nvb2k920b2d3i5mfcmsqcgw9vp3wm")))

(define-public crate-f1d2d2f924e986ac86fdf7b36c94bcdf32beec15-0.1 (crate (name "f1d2d2f924e986ac86fdf7b36c94bcdf32beec15") (vers "0.1.2") (hash "010jyv14rw489gb1wxg1cj9kg40q1cijm46s7hq7s46lm624ib9x")))

(define-public crate-f1d2d2f924e986ac86fdf7b36c94bcdf32beec15-0.1 (crate (name "f1d2d2f924e986ac86fdf7b36c94bcdf32beec15") (vers "0.1.3") (hash "1yzc0xfv1izr5rdr60qff20f4kqx44665n0lavi3d06irzqmz6bk")))

(define-public crate-f1d2d2f924e986ac86fdf7b36c94bcdf32beec15-0.1 (crate (name "f1d2d2f924e986ac86fdf7b36c94bcdf32beec15") (vers "0.1.4") (hash "00mgsbd8f6xxrwd3f6r3y3ca0lk94qv8wz4sx78cza2nfhlfqwk5")))

(define-public crate-f1d2d2f924e986ac86fdf7b36c94bcdf32beec15-0.1 (crate (name "f1d2d2f924e986ac86fdf7b36c94bcdf32beec15") (vers "0.1.5") (hash "1dmh2fvwvgqaw2c56n3wvrn3hlmjpiqlmv5hhpdjbqr6v67pkl5j")))

