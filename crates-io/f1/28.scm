(define-module (crates-io f1 #{28}#) #:use-module (crates-io))

(define-public crate-f128-0.1 (crate (name "f128") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "11rpraiijd2jgi59zz7vlpr4jvxbx7ddj02cqr4bw0ib3ydsa4jw")))

(define-public crate-f128-0.1 (crate (name "f128") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "18zs8inkyv60p541514iyhnmrjn7g5030wxsz9h0y4dl5vnq7azv")))

(define-public crate-f128-0.1 (crate (name "f128") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pn1n1wyaylpx5b36anabwa7zziawiiks360kwkx7c7zc0x9hhdl")))

(define-public crate-f128-0.1 (crate (name "f128") (vers "0.1.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dq6wa9cyhp546vsgsq1mfqddid98mac9gq18g8kdjc964n0izm3")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1bh210zrycfh0jssfg42m19f6lbbcip4fqm2nhn5xyj12aqml0z4")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1sn8i8nps0wlma89pc29mmjx997dfaqjhxa4qpwnijl21b42a30p")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1cn0s9wip5vlqzs7i0kn37wlnnpc86jlh4w3386pn1p2szpf1mbk")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.3") (deps (list (crate-dep (name "f128_input") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f128_internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1w3gv4f4jwpziia146v4cyn30iap4i0xmfpwk8pyz8g8d0pdh7xa")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.4") (deps (list (crate-dep (name "f128_input") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f128_internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0spag0fm8acm6c1wwlgrkb2n8hxmwq0w0zs0hp9naiq66dh9c0i5")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.5") (deps (list (crate-dep (name "f128_input") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "f128_internal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "173845w3cxnx3b45amirmms7f3war1ih8hrrivvir0lmrvryd3pk")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.6") (deps (list (crate-dep (name "f128_input") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "f128_internal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0nicb9xzi8j8ick2c042rr1k7vr4qms5mch7jnqy0i9ycf5figgq")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.7") (deps (list (crate-dep (name "f128_input") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "f128_internal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "02dbz9vhvi9w4wswp88w1yh527phiplyfjxc5mv47sss9s10ypvb")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.8") (deps (list (crate-dep (name "f128_input") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "f128_internal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1inp8hp8fgw5i1ci1wb87k7nk81b8jjjkw6c13nv82pfggai6ncg")))

(define-public crate-f128-0.2 (crate (name "f128") (vers "0.2.9") (deps (list (crate-dep (name "f128_input") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "f128_internal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0b3dfs7y04wvcjr9rckykqpcg6n6mfahrcfckvjbkj440x9jjz8b")))

(define-public crate-f128_input-0.1 (crate (name "f128_input") (vers "0.1.0") (deps (list (crate-dep (name "f128_internal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03dmvd9aqlz1hlcaprnrvbravfp1awbcxx7hhrm0ia8c2n1fr92m")))

(define-public crate-f128_input-0.2 (crate (name "f128_input") (vers "0.2.0") (deps (list (crate-dep (name "f128_internal") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "109pskx1wxxfnfpy872bzvhck2b4p35f7bbvscyrhmfmbryyv0nx")))

(define-public crate-f128_input-0.2 (crate (name "f128_input") (vers "0.2.1") (deps (list (crate-dep (name "f128_internal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1fqh8nlz9v8iap4mwwk3xvbfgrhkynl35jcriix60ia7yyk23a0q")))

(define-public crate-f128_internal-0.1 (crate (name "f128_internal") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1czpv2ngldzgp05jwhsk5a898c4yn7lswd556zdc896paml3rj1w")))

(define-public crate-f128_internal-0.2 (crate (name "f128_internal") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1c84gvaalala41i5plka7bqx51cz64pbr6bmk8bcprdpir5h67m1")))

(define-public crate-f128_internal-0.2 (crate (name "f128_internal") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1yxs78rxf77hazf52alhsqah1gavwk92lq54jcv3xdfvgbqdjvym")))

(define-public crate-f128_internal-0.2 (crate (name "f128_internal") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "17d9l3fyk3y19shs3p8pp2f29zim2rbazhbhcriyd96dwcys624p")))

