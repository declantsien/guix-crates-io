(define-module (crates-io f1 -t) #:use-module (crates-io))

(define-public crate-f1-telemetry-client-0.1 (crate (name "f1-telemetry-client") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "byteorder_async") (req "^1.2.0") (features (quote ("futures_async"))) (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1i1gidnhddvxvmgzmm4rpiipzrppcnv93pkh5pc3c355kicsnbnx")))

(define-public crate-f1-telemetry-client-0.1 (crate (name "f1-telemetry-client") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "byteorder_async") (req "^1.2.0") (features (quote ("futures_async"))) (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "03k6p4v53lihiv4228gkxp1j5w5ispghiha4i1a1z7zg5vc5yf3p")))

