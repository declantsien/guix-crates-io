(define-module (crates-io ex un) #:use-module (crates-io))

(define-public crate-exun-0.1 (crate (name "exun") (vers "0.1.0") (hash "19ch9hmdjn9snzw5vk9pvl6m1vngn60vrhlcba1jcw21qqa74vj6") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.41.1")))

(define-public crate-exun-0.2 (crate (name "exun") (vers "0.2.0") (hash "1gvh5lakwy65igv0daqpvqyai1xnqllw15dbd23w9phj72pawhpk") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.41.1")))

