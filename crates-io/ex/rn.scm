(define-module (crates-io ex rn) #:use-module (crates-io))

(define-public crate-exrng-0.1 (crate (name "exrng") (vers "0.1.0") (deps (list (crate-dep (name "rand_core") (req "^0.5.1") (kind 0)))) (hash "0yiimz3drlln76ckx8rl5707yjq2yv37ss8jjxkln91inbvvqbk7")))

(define-public crate-exrng-0.1 (crate (name "exrng") (vers "0.1.1") (deps (list (crate-dep (name "rand_core") (req "^0.5.1") (kind 0)))) (hash "02afi7a5dxpbqgy12y7zh843216lcskqpx4n836kaflhgk1aaicd")))

(define-public crate-exrng-0.1 (crate (name "exrng") (vers "0.1.2") (deps (list (crate-dep (name "rand_core") (req "^0.4.2") (kind 0)))) (hash "00lzw4kw5mdfdj8qq2yf6spnm5l36pa105wl91sjx80z1ggmjw4k")))

