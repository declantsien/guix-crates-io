(define-module (crates-io ex ie) #:use-module (crates-io))

(define-public crate-exie-0.1 (crate (name "exie") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "08yyh9zrqwlycprlcwskyj7n3b76ayw2v3bdfbbbzi83cprlimvx")))

