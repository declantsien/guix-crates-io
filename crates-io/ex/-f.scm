(define-module (crates-io ex -f) #:use-module (crates-io))

(define-public crate-ex-futures-0.1 (crate (name "ex-futures") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g0pg3r8xxpjpb60r19jzqbp7n2yhj24a3c90xssw9ymgazkhg8k") (yanked #t)))

(define-public crate-ex-futures-0.1 (crate (name "ex-futures") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zl7fkbq9xzgsmb7l963dyxp8airmpv8wdd6y6331idp8lysfa0y") (yanked #t)))

(define-public crate-ex-futures-0.2 (crate (name "ex-futures") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1n8c304pg6wx9wb1bdn1q3k7shmnhpp94aakrggwxhpqh8chmf3l") (yanked #t)))

(define-public crate-ex-futures-0.2 (crate (name "ex-futures") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1syfh67gzgsxhwyf6xj0s42krxf837f4r1ss02mlk6x7jidqbp6v") (yanked #t)))

(define-public crate-ex-futures-0.2 (crate (name "ex-futures") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0d7gamgdlhiry26g2gqkcmy20kwmm3v7v1j9yybbknj8zj8k8ab7")))

(define-public crate-ex-futures-0.2 (crate (name "ex-futures") (vers "0.2.3") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1r8ay1p35nlm8fjiv4awmy8gw6z5yvw3hbr1kb478m5p29hx47ds")))

(define-public crate-ex-futures-0.2 (crate (name "ex-futures") (vers "0.2.4") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "08a7akh8ifwb3s217fgnjgzv0jzxspj11iqyc877ghd8lprix526")))

(define-public crate-ex-futures-0.3 (crate (name "ex-futures") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0k5gvrzkgznwahsb24qgnx632fw48lrarqqw1n7rk90npwgcqxpm")))

(define-public crate-ex-futures-0.3 (crate (name "ex-futures") (vers "0.3.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0bxh5l59xgv2igkg8jyzi7lww0h0444nrbhzqvdmi18bwf0knqqg")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "1i87sfmsxmrpya4fwqp2yah23g6hkw1zibqny256h3jq1nq7j1nx")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "0k3w34q2ps34fk44i1j05rmp3vmciajkl5k932aycyv9nw98aakf")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "0ypjf0yaxqzaa95frkvdkps9ll6fjyxpnn8spdb8x4a9w5vjkrz4")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.3") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "1v16nsziwagd092p02vvj1wkdyakj223bq6862m838k6y4v3xgwh")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.4") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "0617dgn0va6623nbxgh97m0y7sdjg1c3m91bn6hh5sar6qcslgwk")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.5") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "1ja75bw3v6rh597zh2gp2mmnzfbl8pvhz5nywz9pszz581mapf4s")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.6") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "0frdhphvpdkfbz6b5gxqf6xq7gpq7mjdxhgjm1nsdncnivh4s2hz")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.7") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "1c4c8k7qnm5p5zs7x6ij3xglnv22cvnqbk69n70cplkfa62kvkd6")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.8") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "16p49k2b19fjb2af1xh1866pg6bym9fzvqk9f67m40p09im2rhkq")))

(define-public crate-ex-futures-0.4 (crate (name "ex-futures") (vers "0.4.9") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "1sl2q91i5jr7i80kccc0vcl2x3vj0vd4yhkxf2w2ab5lvj9xxrjf")))

