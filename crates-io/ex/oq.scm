(define-module (crates-io ex oq) #:use-module (crates-io))

(define-public crate-exoquant-0.1 (crate (name "exoquant") (vers "0.1.0") (deps (list (crate-dep (name "lodepng") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.14") (optional #t) (default-features #t) (kind 0)))) (hash "13lli6vdswg0w3l3w8bal64sm815bxr0rnn1swhka0d301cp0n5j") (features (quote (("random-sample" "rand"))))))

(define-public crate-exoquant-0.2 (crate (name "exoquant") (vers "0.2.0") (deps (list (crate-dep (name "lodepng") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.14") (optional #t) (default-features #t) (kind 0)))) (hash "1ap4wm1f2jcqjnjvibgjrxqn279jlm6sajmlcdd4bdw3vvnawj2h") (features (quote (("random-sample" "rand"))))))

