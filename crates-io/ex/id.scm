(define-module (crates-io ex id) #:use-module (crates-io))

(define-public crate-exid-0.0.1 (crate (name "exid") (vers "0.0.1") (hash "13nmw8w3kwir18qi62wn04lw160pgid12vw4kan7xlv5x9vaiaz7") (rust-version "1.75")))

(define-public crate-exidot-0.1 (crate (name "exidot") (vers "0.1.2") (deps (list (crate-dep (name "xmlx") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "zipx") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0y52r3j1pp8dsj690ay62cqn6m4ra88951px2s2x8vlrfdd0whyw") (yanked #t)))

