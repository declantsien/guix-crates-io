(define-module (crates-io ex e2) #:use-module (crates-io))

(define-public crate-exe2swf-0.1 (crate (name "exe2swf") (vers "0.1.0") (hash "0r5z0p015pqq95j907mwias8kd4vbxv7whmmkzggwppa0hmc3fh7")))

(define-public crate-exe2swf-0.1 (crate (name "exe2swf") (vers "0.1.1") (hash "1504i781189klr8lm1k403kkkzdcby2m7xdgcqn410a5icjwxmr0")))

(define-public crate-exe2swf-0.1 (crate (name "exe2swf") (vers "0.1.2") (hash "1cvgnzqdpxxh1c9wkgjvqxs4yhv66697788j5j528qnaj4radpir")))

(define-public crate-exe2swf-0.1 (crate (name "exe2swf") (vers "0.1.3") (hash "1wiwwb8nc8h9h115rlhm336vdfj90kvh2lrx13bsrjyq2j9hpkqm")))

