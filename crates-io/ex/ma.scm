(define-module (crates-io ex ma) #:use-module (crates-io))

(define-public crate-exmap-0.0.1 (crate (name "exmap") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "rustix") (req "^0.36.5") (features (quote ("mm" "fs"))) (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1zizr196897a29fwyyb11w733pbvk19d8kbg2gd2mlard625wv6n")))

