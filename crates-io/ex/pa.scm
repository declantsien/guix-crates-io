(define-module (crates-io ex pa) #:use-module (crates-io))

(define-public crate-expand-0.1 (crate (name "expand") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.46") (features (quote ("proc-macro" "parsing"))) (kind 0)))) (hash "0ca6vang0z7rmzhpwjylg8c7k1liv7i45amc9hzyhzyd9jjzxacs")))

(define-public crate-expand-0.1 (crate (name "expand") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.47") (features (quote ("proc-macro" "parsing"))) (kind 0)))) (hash "16sldafcyj1k13b1m0f1dqcg5j776lfgir02pbmsxbx3cj09rk85")))

(define-public crate-expand-0.1 (crate (name "expand") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.48") (features (quote ("proc-macro" "parsing"))) (kind 0)))) (hash "07izgfdni02xa0rix7vdnbfb1w0cdbr14z8jy4y5zlaqscm85z53")))

(define-public crate-expand-0.1 (crate (name "expand") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.48") (features (quote ("proc-macro" "parsing"))) (kind 0)))) (hash "0dz16pghihw18vm39mgb5lwydpx52416kvy5frrzp8xv9lbjmgpy")))

(define-public crate-expand-0.2 (crate (name "expand") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.48") (features (quote ("proc-macro" "parsing"))) (kind 0)))) (hash "1z0rd46lg2pmw7r40ycwm0c8cdhi52dk6vw114lj79zv40dx7mhc")))

(define-public crate-expand-0.2 (crate (name "expand") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("parsing" "proc-macro"))) (kind 0)))) (hash "1g9y4nh2p0w38by3fgg05y5f1h00xkp7i4iyzf1n96vg0gfmpgw9")))

(define-public crate-expand-0.3 (crate (name "expand") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("parsing" "proc-macro"))) (kind 0)))) (hash "173zl3yc9dqq0g0z28sv3js95h74bf6nd78cjcyv56svkbskn1rx")))

(define-public crate-expand_any_lifetime-0.1 (crate (name "expand_any_lifetime") (vers "0.1.0") (hash "1bh8wrbdib41hpmnhy189gba5p51kjj2m14rx34a7fw9vvlpm1an")))

(define-public crate-expand_any_lifetime-0.1 (crate (name "expand_any_lifetime") (vers "0.1.1") (hash "1ri546kb29qzb65zip4r2wzd156sakw3fya3766wbb8k6sjhj0ad") (rust-version "1.54")))

(define-public crate-expand_any_lifetime-0.1 (crate (name "expand_any_lifetime") (vers "0.1.2") (hash "14i5xajyk50swb55hiavq4f0kv7zfd0l09azc94lwf61l0p5zlh0") (rust-version "1.54")))

(define-public crate-expand_array-0.1 (crate (name "expand_array") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.61") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18i2ixkx0pk7h6gj784aljixv0q3y6vfkpm8v4ksbgv84ifjp0y0")))

(define-public crate-expand_array-0.2 (crate (name "expand_array") (vers "0.2.0") (hash "05i7dq7g42wpf0dl7jpn4vcv3cbjksg3h0m6vicnqv4r4qcxh9bw")))

(define-public crate-expand_str-0.1 (crate (name "expand_str") (vers "0.1.0") (hash "17c9p67v5mmivara34azyq97fqq9n16sw43s4228aml2klbkamac") (features (quote (("env") ("default" "env"))))))

(define-public crate-expand_str-0.1 (crate (name "expand_str") (vers "0.1.1") (hash "1r5032gdcwifksw2xrnls1v0pgx71n38qfacw9jwlks5pngvrgzp") (features (quote (("env") ("default" "env"))))))

(define-public crate-expandable-0.1 (crate (name "expandable") (vers "0.1.0") (hash "09z46p7j5y21s1ys1q1n0kpb7pcz2yavnga1by6rqbxa6f823cls")))

(define-public crate-expanded-pathbuf-0.1 (crate (name "expanded-pathbuf") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4") (default-features #t) (kind 2)) (crate-dep (name "shellexpand") (req "^2") (default-features #t) (kind 0)))) (hash "18sjzsmfm5dqqdipfw8l22d80acrz1nwcaaih7yi3gm0g8i95m4d")))

(define-public crate-expanded-pathbuf-0.1 (crate (name "expanded-pathbuf") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4") (default-features #t) (kind 2)) (crate-dep (name "shellexpand") (req "^2") (default-features #t) (kind 0)))) (hash "0p5mf13bjnvvk1f2sv82kdapaars3sl0w11zpxbh3nbdhn4wg2za")))

(define-public crate-expanded-pathbuf-0.1 (crate (name "expanded-pathbuf") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5") (default-features #t) (kind 2)))) (hash "03plk4bn16k9h1i97qn25vl7xgrxfgfncy5yq31g00dp9mzym1sy")))

(define-public crate-expander-0.0.1 (crate (name "expander") (vers "0.0.1") (deps (list (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "1zvi65pzhrq1abnm7b5xaf0fbvs37bka92p8jyd15rv78ah0p2hv")))

(define-public crate-expander-0.0.2 (crate (name "expander") (vers "0.0.2") (deps (list (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "1rnmbn5q16a25l8wmidyh5s1x0jxyxvg7vxj8ry6w9vrhka51p3i")))

(define-public crate-expander-0.0.3 (crate (name "expander") (vers "0.0.3") (deps (list (crate-dep (name "blake3") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "1cmbkcifgcs6p30f8l28pdq1w5kc8yn9zdqjr1cwmh23swxqy4h3")))

(define-public crate-expander-0.0.4 (crate (name "expander") (vers "0.0.4") (deps (list (crate-dep (name "blake3") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "10aq0wj97gyghzpss710zb70dyjhq7iajkpzdybmyp2mbikw0657")))

(define-public crate-expander-0.0.5 (crate (name "expander") (vers "0.0.5") (deps (list (crate-dep (name "blake2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (optional #t) (kind 0)))) (hash "1ajsnrmqnan85i0d1vvwf16zrrd4sxdiazm0xnvf8f4fkv1j37rh") (features (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-0.0.6 (crate (name "expander") (vers "0.0.6") (deps (list (crate-dep (name "blake2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (optional #t) (kind 0)))) (hash "1diapm121xym5px0k9mbdgrfwgwipqpx66iij0b3sg7iblm1hx1p") (features (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-1 (crate (name "expander") (vers "1.0.0-alpha.0") (deps (list (crate-dep (name "blake2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (optional #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 2)))) (hash "08lr6lwasr4yv1c62pw2r29w3fy8127873zppjxdm9310dc2rg6p") (features (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-1 (crate (name "expander") (vers "1.0.0") (deps (list (crate-dep (name "blake2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (optional #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 2)))) (hash "117d2qpn7i9l1m21pwm0sccg85c9mwb7m6ac5vwm71bja28k8q7k") (features (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-2 (crate (name "expander") (vers "2.0.0") (deps (list (crate-dep (name "blake2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (optional #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 2)))) (hash "1xxhgyvs41vypqxrqpj6c6bcddcrqbv5wl8hdj392645rx4sg1jz") (features (quote (("syndicate" "syn") ("default" "syndicate"))))))

(define-public crate-expander-2 (crate (name "expander") (vers "2.1.0") (deps (list (crate-dep (name "blake2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "prettier-please") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (optional #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("extra-traits" "parsing" "full"))) (default-features #t) (kind 2)))) (hash "0kcdi583h1lpq7x2r2dmv170x9851jk4x5j759cz2dji0c13rs00") (features (quote (("syndicate" "syn") ("pretty" "prettier-please" "syn/parsing" "syn/full") ("default" "syndicate" "pretty"))))))

(define-public crate-expanding_slice_rb-0.1 (crate (name "expanding_slice_rb") (vers "0.1.0") (deps (list (crate-dep (name "slice_ring_buf") (req "^0.2") (default-features #t) (kind 0)))) (hash "12pmml2qklgqin86ysicb8kv89830yaz38xvr935cqs41dsgxrj4")))

(define-public crate-expanding_slice_rb-0.1 (crate (name "expanding_slice_rb") (vers "0.1.1") (deps (list (crate-dep (name "slice_ring_buf") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vlcj6ljspq6ivwr8xh8gvz49ggwrf44z9chzfcygi2yq105670i")))

(define-public crate-expanding_slice_rb-0.1 (crate (name "expanding_slice_rb") (vers "0.1.2") (deps (list (crate-dep (name "slice_ring_buf") (req "^0.2") (default-features #t) (kind 0)))) (hash "13r5fb6nb98mqlbds6qklsrp7rc8iw50aa3bmi60zlpqc6fdss6p")))

(define-public crate-expanding_slice_rb-0.1 (crate (name "expanding_slice_rb") (vers "0.1.3") (deps (list (crate-dep (name "slice_ring_buf") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ygylw0b716q389khgpwgkip5xxp3727gy3lj9wn11spsym5ham1")))

(define-public crate-expandtabs-rs-0.1 (crate (name "expandtabs-rs") (vers "0.1.0") (hash "0py2402d33bixaw7q7r7qyk010ngnk5xb3r7vdsyn3iab92m1b7x")))

(define-public crate-expanduser-1 (crate (name "expanduser") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "pwd") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1gkpc7942dyqa4jmrfb96r74abwr9ajs54z97pkq5r2r5zi256p8")))

(define-public crate-expanduser-1 (crate (name "expanduser") (vers "1.1.0") (deps (list (crate-dep (name "dirs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "pwd") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "13m2g4khgadi52afdb4zvx7r6dfgfar4j3hs2x1jyinnpkjf3nlc")))

(define-public crate-expanduser-1 (crate (name "expanduser") (vers "1.2.0") (deps (list (crate-dep (name "dirs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "pwd") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0cy83f7f0k6d5bk2f582xw9bya6v857p6dlxpdbpjpd0bapx641q")))

(define-public crate-expanduser-1 (crate (name "expanduser") (vers "1.2.1") (deps (list (crate-dep (name "dirs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "pwd") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "15481l2pg6bxcvrkplgvidl18ap61i5k2r8j6zck7850vp4cissi")))

(define-public crate-expanduser-1 (crate (name "expanduser") (vers "1.2.2") (deps (list (crate-dep (name "dirs") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "pwd") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1h0blhxw7p8h37ly6flp7bb6gs2xdspxkvibdimxnmys6n9bgq0l")))

(define-public crate-expanse-0.3 (crate (name "expanse") (vers "0.3.3") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hash32") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.6.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.10") (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 0)))) (hash "1lplpd542f2wsbvjnr1hdjs7m0y73gi9zbvkbcgpqyn71dzr413y") (features (quote (("std" "num-traits/std") ("serde_kebab_case" "serde") ("serde_camel_case" "serde") ("default" "std") ("alloc" "hashbrown"))))))

(define-public crate-expanse-0.3 (crate (name "expanse") (vers "0.3.4") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hash32") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.6.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.10") (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 0)))) (hash "0gcy6flc9xwvlmz191lpqw2ah0hryd6cx2pwnl5nwjajjh1sj7ij") (features (quote (("std" "num-traits/std") ("serde_kebab_case" "serde") ("serde_camel_case" "serde") ("default" "std") ("alloc" "hashbrown"))))))

(define-public crate-expansion-0.0.0 (crate (name "expansion") (vers "0.0.0") (hash "1505lmz1czl9s8bss75dxq82ypgcg9hw9haf89pvgwbmkna7dkhw")))

(define-public crate-expansion-0.0.1 (crate (name "expansion") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0yzhibbjs00rs0x7l1pwprkibcz8kf2niy0s5bwvz0hhfdzq9jfx")))

(define-public crate-expansion-0.0.2 (crate (name "expansion") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0z48mk0x3x5jk42r4h7wxkp8zzf1p54npnvk1ymj33dya6afzs8h")))

(define-public crate-expansion-0.0.3 (crate (name "expansion") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0wm2z4a42jy9gqmw4vmx9b2k4nhzwfjhka0sbp6lrjfbwqk1mbp1")))

(define-public crate-expansion-0.0.4 (crate (name "expansion") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "05nj1asx24lwm7nbpshj1r38vypdg789kqdr7sdn6rpwf9kj0zfl")))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.0-again") (deps (list (crate-dep (name "make-cmd") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "07zkjs8fihwkb1cqckf0ca3xy08akijd102wgs843y0926bn2dih") (yanked #t)))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.1-really.0") (deps (list (crate-dep (name "make-cmd") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1cazycgsaqnikvndv98vd0jjxxh9vc46f7rbnylmvjvmgwjsw012") (yanked #t)))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.1") (deps (list (crate-dep (name "make-cmd") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0f2hxjaxvvb29ndazhxx61lsab0k83drgyhq8rn4idgc9q136h85") (yanked #t)))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.2") (deps (list (crate-dep (name "make-cmd") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0gijyhfrp4wh0czrs9lg8wxziafgp6zgs2kg2wgmghclhn1nzksc")))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "13nknkzyvwfz4cl0yj4hcjr1mkr9g462yb4cxd08q0b3fphrmmp7")))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1745q2hqgp01h9pz6vg3m2w4r7kv4iipqd4p3nwjhbd0m38nrwyf")))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1cfmismhjw7zxxi9pgzzb9pl5acxxllq1nr3n14qa27jfawwqw64")))

(define-public crate-expat-sys-2 (crate (name "expat-sys") (vers "2.1.6") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1yj5pqynds776ay8wg9mhi3hvna4fv7vf244yr1864r0i5r1k3v5") (links "expat")))

