(define-module (crates-io ex e_) #:use-module (crates-io))

(define-public crate-exe_iza-0.1 (crate (name "exe_iza") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "= 2.33") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "= 0.1.28") (default-features #t) (kind 0)) (crate-dep (name "kotonoha") (req "= 0.2.0") (default-features #t) (kind 0)))) (hash "049iavri8shhvkh8h65mf3nkm98anq53pm6yf4d30pgr66aijg48")))

(define-public crate-exe_tools-0.1 (crate (name "exe_tools") (vers "0.1.0") (deps (list (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "1r8w3ki7kzh9bisk0kfxlmjv3z1855ndmvsfyxdabbcgsj5hvd3k") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

