(define-module (crates-io ex -w) #:use-module (crates-io))

(define-public crate-ex-web-api-rust-0.1 (crate (name "ex-web-api-rust") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.4") (features (quote ("serve" "json"))) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1mrwgfn07l7fm0f9856h49v8dim7bn2hcdrynb1vl67y1v32kqf5")))

(define-public crate-ex-web-api-rust-0.1 (crate (name "ex-web-api-rust") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.4") (features (quote ("serve" "json"))) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0qc0qmv97rcpzlaas0ld4vsghc5ixdhrd10kcz4igz911kwncavl")))

