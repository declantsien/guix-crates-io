(define-module (crates-io ex _m) #:use-module (crates-io))

(define-public crate-ex_merge_sort-0.1 (crate (name "ex_merge_sort") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1hbxh0p50gj97qy458l5alz76s365y8bsbmgkslqpnzdympki5cq")))

(define-public crate-ex_merge_sort-0.1 (crate (name "ex_merge_sort") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0irh5jm3v4nszgnq2g3p5q2dm3a0llpy18mnp5kv5fsf3h4jpg51")))

(define-public crate-ex_merge_sort-0.2 (crate (name "ex_merge_sort") (vers "0.2.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "134zg64yhq6cs0l8qq2walypd220cm46r1s93wmkvba2kxhgb3ry")))

(define-public crate-ex_merge_sort-0.2 (crate (name "ex_merge_sort") (vers "0.2.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1n5w3bkncmlbp8ima4pi0ki3z6sm43w48imlj8kvbjpi2z80pyk4")))

(define-public crate-ex_merge_sort-0.3 (crate (name "ex_merge_sort") (vers "0.3.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1b2fw9fvlqmvh4s01d5cd5pirsw2n8wvn186wqkkqgnahcxw1msz")))

(define-public crate-ex_merge_sort-0.3 (crate (name "ex_merge_sort") (vers "0.3.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1svqwkrjbazyza5ly7pm1rsvqvf6raplk6pkim2qzw9hcnmlz2kb")))

(define-public crate-ex_merge_sort-0.3 (crate (name "ex_merge_sort") (vers "0.3.2") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0rd6jk2mrqx1b5ngx7shd4wllhhrknw95dap2s7svd1iqsr6xr8w")))

(define-public crate-ex_merge_sort-0.4 (crate (name "ex_merge_sort") (vers "0.4.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0c2q04vz8hx1ag3vyg4xckz34w5x2lcw36hc4sky18sm0bbax8bc")))

(define-public crate-ex_merge_sort-0.5 (crate (name "ex_merge_sort") (vers "0.5.0") (deps (list (crate-dep (name "memoize") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0zfvfm6aq7b8k9xy1w7lq9qlla9mdbgmsn681mkbsxhid45xhzcs") (yanked #t)))

(define-public crate-ex_merge_sort_by_key-0.1 (crate (name "ex_merge_sort_by_key") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "17dmpj1im7smhyxnb5khxnapi18m0xcl1lgczpz6ns3v5k5rlxmj")))

(define-public crate-ex_merge_sort_by_key-0.1 (crate (name "ex_merge_sort_by_key") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1p5b0wrdvniykg5invkyf4kgxzcv2l1m8vxf1929v6g6qcpikww5")))

(define-public crate-ex_merge_sort_by_key-0.2 (crate (name "ex_merge_sort_by_key") (vers "0.2.0") (deps (list (crate-dep (name "indoc") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0fv2n0hjkqpskrjqviq7wn69vxy2xzqqsgab5p76bxx3nzzxcgl1")))

