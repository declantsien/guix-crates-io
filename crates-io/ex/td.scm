(define-module (crates-io ex td) #:use-module (crates-io))

(define-public crate-extd-0.1 (crate (name "extd") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "095nl83qivsygqd0yblxs82ck7y8n5vqvjqp2nbwlfc1rspf4dld")))

(define-public crate-extd-0.1 (crate (name "extd") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06zqbbj1s0714209jaazxm3v7w67wx8663rnnyplxca3m1hmdddj")))

(define-public crate-extd-0.1 (crate (name "extd") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rm6rvng5mfncjviwlx587gc05a82fn46rrrxminfqhmzqjvvvr7")))

(define-public crate-extd-0.1 (crate (name "extd") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1x6alncrgvp8njd2i6m0jc6kd41zy6if07v7709xz6zh1ln9pjbp")))

(define-public crate-extdivga-0.1 (crate (name "extdivga") (vers "0.1.0") (deps (list (crate-dep (name "faer-cholesky") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "faer-core") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "faer-lu") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "faer-qr") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "faer-svd") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0pcz3whq2fvkbfpfyhw9l663rn3bx0a84myby2whffgdmalwq2wx")))

(define-public crate-extdot-0.1 (crate (name "extdot") (vers "0.1.0") (deps (list (crate-dep (name "extdot-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "12ybcd5vbidqnakxrcwb2bx64qpl2i7zh3i8a82zzbbfj28dp7rv")))

(define-public crate-extdot-0.2 (crate (name "extdot") (vers "0.2.0") (deps (list (crate-dep (name "extdot-impl") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "15m4ljip2qcsrwqbwjp9wka75fmcjxf9s3v0cnhwxwlk3jmhady0")))

(define-public crate-extdot-impl-0.1 (crate (name "extdot-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1bya8xvx23y6kq88b0b7v0xiznmvbp60dv7n8a7vy3b8dqq5c2cd")))

(define-public crate-extdot-impl-0.2 (crate (name "extdot-impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1mh8y7zq7nl4k1rsv3zis8cs9gizqlbrismk0bi5rxxa192yjavr")))

