(define-module (crates-io ex it) #:use-module (crates-io))

(define-public crate-exit-0.1 (crate (name "exit") (vers "0.1.0") (hash "1cf1sn7pccbpr10n5zys5f7qnknafz48g5gplhbvy3r8g1ma7dmz")))

(define-public crate-exit-0.2 (crate (name "exit") (vers "0.2.0") (hash "0bgh8p8lr2y4xgmb6259lqv3y3qqzgv9z5bfvl4xa3yj4ibzyb63")))

(define-public crate-exit-code-1 (crate (name "exit-code") (vers "1.0.0") (hash "1i1xcj614n0597prfsxjkpfrpzlrc7c11ji5hmldgwlkf6bxnk5z")))

(define-public crate-exit-future-0.1 (crate (name "exit-future") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dwxz93lz0idvw1bjxnzscdnyca29yxdlvyxykx3inc31wbfqc3c")))

(define-public crate-exit-future-0.1 (crate (name "exit-future") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "0lp4hlpfjfjj7sd61l5695wy6x6y45cs5zpw314k1yhqkll19c9y")))

(define-public crate-exit-future-0.1 (crate (name "exit-future") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "0v9i5z03x9ffwrbjwhgrdj8y5xn9kkhyqb91nwniii38xxnbb9ws")))

(define-public crate-exit-future-0.1 (crate (name "exit-future") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.6") (default-features #t) (kind 0)))) (hash "0766aq6s541kfzh8ixj9fy2rad748dsj6zc6pcmsk0csx449nmc7")))

(define-public crate-exit-future-0.1 (crate (name "exit-future") (vers "0.1.4") (deps (list (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0j4gbq08da3cjy1zzh83sgcjnfjx3n7yqd3z1rkirqrq3r23y0fq")))

(define-public crate-exit-future-0.2 (crate (name "exit-future") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1i88800r9kc3xf4319dilzflkwvhvmplsiljapqk6knn6cc2ygz4")))

(define-public crate-exit-no-std-0.1 (crate (name "exit-no-std") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.126") (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0pm33kx931ch8ij09vg9j0ivv9ljjja0ipnm2xfxnndj9v3qna9x") (rust-version "1.59")))

(define-public crate-exit-no-std-0.1 (crate (name "exit-no-std") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.126") (target "cfg(all(not(dos), not(windows)))") (kind 0)) (crate-dep (name "pc-ints") (req "^0.0.2") (default-features #t) (target "cfg(dos)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(all(not(dos), windows))") (kind 0)))) (hash "074n3z26a84p7w4pp8bjl4za0rg4ql3lfmpy4gpvzb091kzq3pgp") (rust-version "1.59")))

(define-public crate-exit-no-std-0.1 (crate (name "exit-no-std") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.126") (target "cfg(all(not(target_os = \"dos\"), not(windows)))") (kind 0)) (crate-dep (name "pc-ints") (req "^0.1.0") (default-features #t) (target "cfg(target_os = \"dos\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(all(not(target_os = \"dos\"), windows))") (kind 0)))) (hash "1jbajnzjkn2l4hhm8nzib4sfxx4ghqar5n7hk42fpjs83pbx97gk") (rust-version "1.59")))

(define-public crate-exit-no-std-0.1 (crate (name "exit-no-std") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.126") (target "cfg(all(not(target_os = \"dos\"), not(windows)))") (kind 0)) (crate-dep (name "pc-ints") (req "^0.1.4") (default-features #t) (target "cfg(target_os = \"dos\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(all(not(target_os = \"dos\"), windows))") (kind 0)))) (hash "168x2rfs88kcj37ani0qa99y78znsrssv25a2qcqrizscz68qq2a") (rust-version "1.59")))

(define-public crate-exit-no-std-0.1 (crate (name "exit-no-std") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.126") (target "cfg(all(not(target_os = \"dos\"), not(windows)))") (kind 0)) (crate-dep (name "pc-ints") (req "^0.2.0") (default-features #t) (target "cfg(target_os = \"dos\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(all(not(target_os = \"dos\"), windows))") (kind 0)))) (hash "12fiwk93x5psmqfzvf5wfpk1g07gy0gra1s7b86z3adxzky6yq1a") (rust-version "1.59")))

(define-public crate-exit-no-std-0.2 (crate (name "exit-no-std") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.126") (target "cfg(all(not(target_os = \"dos\"), not(windows)))") (kind 0)) (crate-dep (name "pc-ints") (req "^0.3.0") (default-features #t) (target "cfg(target_os = \"dos\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(all(not(target_os = \"dos\"), windows))") (kind 0)))) (hash "074b50znhsr7ycky1g9vak31bp2awwapxvr5n3rnjjr9rsh7bvvx")))

(define-public crate-exit-stack-0.1 (crate (name "exit-stack") (vers "0.1.0") (deps (list (crate-dep (name "pin-utils") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wq8iwk4s186w3292jrgvcmvyxfd5id07wa1bivgihxxcw03sdqz")))

(define-public crate-exit-with-code-1 (crate (name "exit-with-code") (vers "1.0.0") (deps (list (crate-dep (name "yare") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "1635yshbdrhi6jyha0gqb0g1a04c8d1qznzdbaajvlwmz2bnzijd")))

(define-public crate-exit_status-0.1 (crate (name "exit_status") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "macrotest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zapx2b9vm98l2c5yyrwrgyvl33qy3yff4w5xcwnaqi3pvfn792g")))

(define-public crate-exit_test-0.1 (crate (name "exit_test") (vers "0.1.0") (hash "1nmp8p5xdfi9r20wbmbsingn3qz7k92hgbdx3dydp528i9hdc4hk")))

(define-public crate-exitcode-1 (crate (name "exitcode") (vers "1.0.0") (hash "150a8y6jm5a1i6qvgl844905wc8nx8fvbkam9xdlg900d8hj1f8b")))

(define-public crate-exitcode-1 (crate (name "exitcode") (vers "1.0.1") (hash "0rk57jdq7rnbcrl5qlymkz57mq2znd3rwfzkwkr7xl6p276bza4w")))

(define-public crate-exitcode-1 (crate (name "exitcode") (vers "1.1.0") (hash "0i4d1m0l1xaaylma1s9zmnzdk85599qagr766yaqf833v5hadaml")))

(define-public crate-exitcode-1 (crate (name "exitcode") (vers "1.1.1") (hash "0c2ivadsgykdx5v170qch747vqhhnizvwzry7q2qpzimyasqfmdr")))

(define-public crate-exitcode-1 (crate (name "exitcode") (vers "1.1.2") (hash "14x1pgwx86x1gfc5zqgj04akr9pzg14w75d9cblc49vhnij3g1fy")))

(define-public crate-exitfailure-0.1 (crate (name "exitfailure") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0n1r9g8kx6vm7cwxqa9s77sdv0i6whgdvmfnb1j6jx0q3qryfixx")))

(define-public crate-exitfailure-0.2 (crate (name "exitfailure") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0kc3ih4zd8vp2ihi8jb8c9jq5v3zblbkjlsgdrsblxmxy19h7ldr")))

(define-public crate-exitfailure-0.2 (crate (name "exitfailure") (vers "0.2.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0lhx4fm2wqxn5z9a6pq27q7njwgrrv1y7anp2x4902nlmah2yjpp")))

(define-public crate-exitfailure-0.2 (crate (name "exitfailure") (vers "0.2.2") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gspz756qvqrp4d8xp9fhx6i50hfcrd4kqdpv4xqnhfa9l9q2lm4")))

(define-public crate-exitfailure-0.2 (crate (name "exitfailure") (vers "0.2.3") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "08amp3zyh7g0mscj0a4adjv0ns025vgkl0w7mpvn52d6w935kcpv")))

(define-public crate-exitfailure-0.3 (crate (name "exitfailure") (vers "0.3.0") (deps (list (crate-dep (name "assert_cli") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gg2w38g5gslpsdi6jcy9v51zpnpawpqh2m234zjiyiybjyxxzh5")))

(define-public crate-exitfailure-0.4 (crate (name "exitfailure") (vers "0.4.0") (deps (list (crate-dep (name "assert_cli") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13n9g0idgqwmq2zbrrnvddrgiz29a59kbkqnicrxw14akvzag9lz")))

(define-public crate-exitfailure-0.4 (crate (name "exitfailure") (vers "0.4.1") (deps (list (crate-dep (name "assert_cli") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0iy6skfkybsgq3xfpj6rayj922svbhrpw1nblb5igfbp5higzs44")))

(define-public crate-exitfailure-0.5 (crate (name "exitfailure") (vers "0.5.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1nlk4q6vmg9ybpwq2i5qs60asrq6qkv3cln4jrsgd0cs3brhymzc")))

(define-public crate-exitfailure-0.5 (crate (name "exitfailure") (vers "0.5.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0585wix3b3pjjj90fkqj9x4ar46d24x82k8rdin3czzk5a1vvx9g")))

