(define-module (crates-io ex er) #:use-module (crates-io))

(define-public crate-exercise-0.1 (crate (name "exercise") (vers "0.1.0") (hash "1d3psrkg7iy8xpx6hkkrwrlwgp38clm6dm9hhmcapa8ghvzxd4fx")))

(define-public crate-exercise-sample-0.1 (crate (name "exercise-sample") (vers "0.1.0") (hash "1q7zv95j23q3bjc7bizfikvv4ba0wlxs3dpg51x9ra8bspby3h54")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.0") (hash "0501vq9dmg8lkg0zhf80vfmbi47av1m5ybdqf0k1p0d8fl4m9jc9") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.1") (hash "19kcdjhzhna5j18lxdsdwsykli29chj297g2qi6dhpvlyj8d72yq") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.2") (hash "1pjqynhkbmrf0lsj2hv577rkdpbbivhfqczw341a5mh4pk5bvr25") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.3") (hash "01x295806hwd99n27hylmfipfjgg0zjld9lyzaq68ajmdl40hzmq") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.4") (hash "0i3r2b4cvccgqydb684xjb2sky0ar0axwf5f8ryq8fh6y7j25848") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.5") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1lzl323z70r2izybzr3aj9q9irra9qd770kaz4swv02zxlmv47nc") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.6") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "17wmdd9xx5grd49rwavpvsyzyfa9lrqya3qgzs2a3nfnwy1gj6ij") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.7") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "12jkp5if75kna5g6dzhq5i7gdmzk7hl12cpmjj9b8wzlcj26hidi") (rust-version "1.70.0")))

(define-public crate-exercism-0.1 (crate (name "exercism") (vers "0.1.9") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0qv5wy62vrb3axzfhh3yawk1z5hn4b1d9wjn525b45jvr7a1w839") (rust-version "1.70.0")))

(define-public crate-exercism_prep_tests-0.6 (crate (name "exercism_prep_tests") (vers "0.6.0") (deps (list (crate-dep (name "clioptions") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "envmnt") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0rn7gn8h8cl6m90wnkgxg9m472wbnd2ps1zwk0gn89d9gjm3c6as") (yanked #t)))

(define-public crate-exercism_prep_tests-0.6 (crate (name "exercism_prep_tests") (vers "0.6.1") (deps (list (crate-dep (name "clioptions") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "envmnt") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xj707kp18yp3yscdk0mmwjywiz9bbbyxhr9g0vqbgw00sdqpa5v")))

(define-public crate-exers-0.1 (crate (name "exers") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-compiler-llvm") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-middlewares") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-wasix") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "0ng7l1wqcc5rbg736xkim29ydb4gmb4a3z4x530njpqjq2payj89") (features (quote (("wasm-llvm" "wasmer-compiler-llvm") ("wasm" "wasmer" "wasmer-wasix" "wasmer-middlewares") ("python") ("native") ("jailed" "native") ("default" "wasm" "native" "cpp" "python" "jailed") ("cython" "cpp") ("cpp"))))))

(define-public crate-exers-0.2 (crate (name "exers") (vers "0.2.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^4.0.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-compiler-llvm") (req "^4.0.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-middlewares") (req "^4.0.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-wasix") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "188cpvq8ix3mv1zf59qxxylimmnyq0jzl0j6fsxgs86ii5l6dp1z") (features (quote (("wasm-llvm" "wasmer-compiler-llvm") ("wasm" "wasmer" "wasmer-wasix" "wasmer-middlewares") ("python") ("native") ("jailed" "native") ("default" "wasm" "native" "cpp" "python" "jailed") ("cython" "cpp") ("cpp") ("all-runtimes" "wasm" "native" "jailed") ("all-languages" "cpp" "python"))))))

