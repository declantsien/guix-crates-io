(define-module (crates-io ex pe) #:use-module (crates-io))

(define-public crate-expect-0.0.1 (crate (name "expect") (vers "0.0.1") (hash "1sdrrykmxhs14b0riiqpf7z1cqkcm0z9qpg8jkjacbgm3r8sdwkn")))

(define-public crate-expect-dialog-1 (crate (name "expect-dialog") (vers "1.0.0") (deps (list (crate-dep (name "native-dialog") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "050k89zypglzg50dvjj91bwzjphp1qjjqs9cyb553k4wgzb0bl4j")))

(define-public crate-expect-dialog-1 (crate (name "expect-dialog") (vers "1.0.1") (deps (list (crate-dep (name "native-dialog") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "174dxfwpax0h32bc7vn471cq246im32nkgh96l7p9ibwd11fkzrc")))

(define-public crate-expect-exit-0.1 (crate (name "expect-exit") (vers "0.1.0") (hash "1y9kbc68qbvs8gkf98fiy57mvbw9g6lrlkfbdxm7a1nm9zk6j1h8")))

(define-public crate-expect-exit-0.2 (crate (name "expect-exit") (vers "0.2.0") (hash "18phmxwhr2i8v4b3w3vlq6cmwd57ybzg4cqmp2w5nqq9x83rz1h3")))

(define-public crate-expect-exit-0.3 (crate (name "expect-exit") (vers "0.3.0") (hash "0zgfm5sa507girrbiak874yxwa89h1rj4i6lkf1gnz4xikn0618q")))

(define-public crate-expect-exit-0.3 (crate (name "expect-exit") (vers "0.3.1") (hash "0975k55r3p86iks9z9wzn92ipn4p2bn22wcm8xz78v4ys51jzwp7")))

(define-public crate-expect-exit-0.4 (crate (name "expect-exit") (vers "0.4.0") (hash "19vh8bad2l031cyvl4ivf8k016nbh4z0a2by55r88chc0q5c0a47")))

(define-public crate-expect-exit-0.4 (crate (name "expect-exit") (vers "0.4.1") (hash "1yrvk227d7jw0jl42wbwsv5v5f70nwwb7hg9j3vxbv7zn2yfvzmn")))

(define-public crate-expect-exit-0.4 (crate (name "expect-exit") (vers "0.4.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "19fk9ap2mb04fkh67y2ydxih6w22b22bm3fr51hywfscravwiibs")))

(define-public crate-expect-exit-0.4 (crate (name "expect-exit") (vers "0.4.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1wi7mdfmbvlc8c106fg7r1yv2kxncaq6iljcba59v6r42f9vq7x8")))

(define-public crate-expect-exit-0.5 (crate (name "expect-exit") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0fhi9j34jnnzvkf0za0chmkh34l316kic4r57vf100wwyf1ibd8m")))

(define-public crate-expect-exit-0.5 (crate (name "expect-exit") (vers "0.5.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "160fvxda4r3r24jh5qawqin4zm83sa03k3wf8a3hv5cass6ymmj8")))

(define-public crate-expect-exit-0.5 (crate (name "expect-exit") (vers "0.5.2") (deps (list (crate-dep (name "once_cell") (req "^1.1.15") (default-features #t) (kind 2)))) (hash "1pyr3b8kkkf1bza6zgywsbzm2kzcbwlw8i5c4xyckmmbr7bka6n5")))

(define-public crate-expect-test-0.0.1 (crate (name "expect-test") (vers "0.0.1") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "0n4fwyi6z6lnxc9i6gwh2aq15dpk9lxfn4zbi50nkg6zwwi5507m")))

(define-public crate-expect-test-0.1 (crate (name "expect-test") (vers "0.1.0") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "1f5kjbpmdjpxi4a71jhzv6m3dg87r2ld2291axk2i6513rs87qx3")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.0.0") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "07fv1blmxrhj5k7ph2cxgf82c3isix111llryqfyd3cq2amv4g1s")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.0.1") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "0bhg5c1db4f6ndqn25rgxlbvi0q1nr7gvb1dqmlqfkhdm8z6zfff")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.0.2") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "0gpbjbnd3h5xpzb1jsvq50dj66kqxl0zq7wm83g7ka3zf968vdza")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.1.0") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "0cq651b3dcrw94bl03krxnvllr8kqx6vskqal0n8ydrsmdx4f013")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.2.0-pre.1") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "15riwd2i621wkzgv3cshkf6vqn4vmlq39zlbhx3qfr39pij1cbrs")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.2.1") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "1iwnx8d07nsdyhlkw8brj7j3iw6rcq2msz3rlx3y2fmgyj5mxz0g")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.2.2") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "05nv365xd5fqydmzbsvzqz0148a1vbxp2p0r8a3ivafdvhl6ngky")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.3.0") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "0wd2f9f8hh14xfyj9ddhlcrpk3cwbwwss6l4jlgj9qylvk4rbvfw")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.4.0") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "1lsd6vhy4f7wggdh1k60winn28424gj2iq9gqyvnx0ldlfn62ihx")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.4.1") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "1lzqx5hqh1g4llzqby4z1d18xmrjjx63c5l0na7ycf6mmpzfmn9h")))

(define-public crate-expect-test-1 (crate (name "expect-test") (vers "1.5.0") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "1q55nrkgzg345905aqbsdrwlq4sk0gjn4z5bdph1an1kc6jy02wy") (rust-version "1.60.0")))

(define-public crate-expect-tests-0.1 (crate (name "expect-tests") (vers "0.1.0") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "1bbzhidpsq3wiqxkxv18r018j0yyilf28cw3j3c4waxgzhn89v0r")))

(define-public crate-expect-tests-0.1 (crate (name "expect-tests") (vers "0.1.1") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "14wwwbcd487bcfaqlrqbw89fg4cx52p9mnxdcp1q3cynaii66y5r")))

(define-public crate-expect-tests-0.1 (crate (name "expect-tests") (vers "0.1.2") (deps (list (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "prettyplease") (req "^0.2.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "01gjchql4y56gkvmablsc6aji0qh7vrl684pvv6fbkpd5bm5qjhn") (features (quote (("default" "expect-tokens")))) (v 2) (features2 (quote (("expect-tokens" "dep:syn" "dep:proc-macro2" "dep:prettyplease"))))))

(define-public crate-expect_macro-0.1 (crate (name "expect_macro") (vers "0.1.0") (hash "0wgqnggblj11b62b7x7sf602107ipr4vmd4wfcmh8nix3pwiilhq")))

(define-public crate-expect_macro-0.1 (crate (name "expect_macro") (vers "0.1.1") (hash "028y7x68m5lkyfswhywkdqr98q1hlqd4w7j85x36wnhjx7vb7882")))

(define-public crate-expect_macro-0.2 (crate (name "expect_macro") (vers "0.2.0") (hash "04c1lg61xlr4s0d6jna0kzm51g7jvx0qlwn867xsk9pf803la03i")))

(define-public crate-expect_macro-0.2 (crate (name "expect_macro") (vers "0.2.1") (hash "04r3zcq1grpql6232q8md0kwjil23l6gi5vj30xzq8f8f5lkcxri")))

(define-public crate-expect_macro_derive-0.0.0 (crate (name "expect_macro_derive") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "021iyqj0wcr50wm82kmaz94v8339issw65dxh3gmjm4pn183a3b3")))

(define-public crate-expect_rs-0.0.1 (crate (name "expect_rs") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0byinj26ffxjvqd7x37p5rbzxsri4rp729vp0pndxr1r7z4wj869") (yanked #t)))

(define-public crate-expect_rs-0.0.2 (crate (name "expect_rs") (vers "0.0.2") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1isv3i4lsblbszbza75kc0h2kivbdnwj3nz46daihnb2lc508p5c")))

(define-public crate-expect_soft-0.1 (crate (name "expect_soft") (vers "0.1.0") (hash "0spa3z9qf3wm400r9w17zrdgbz2hb20iklq9r9cdjwx6pg9vy1bd")))

(define-public crate-expect_soft-0.1 (crate (name "expect_soft") (vers "0.1.1") (hash "15qc4m0d05wq105nv7wrx4msdavcnkcqim32lrbggkvxfvp7jk84")))

(define-public crate-expect_with-1 (crate (name "expect_with") (vers "1.0.0") (hash "1bbmb6bdhnb1ndjplpgc0229da9xh964h8zfjjh9ql5jd4bg9qcv")))

(define-public crate-expectation-0.1 (crate (name "expectation") (vers "0.1.0") (deps (list (crate-dep (name "walkdir") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0yyaw8dqhd82qa8rh179j4gb4n76jajfh750qpynai5gqjvl4y41")))

(define-public crate-expectation-0.1 (crate (name "expectation") (vers "0.1.1") (deps (list (crate-dep (name "diff") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "expectation-shared") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "image") (req "0.19.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*.*") (default-features #t) (kind 0)))) (hash "1213v3kdch3ip959n3bpwbhsmis3ywjrs45gxqwhi92dnryxwbpv") (features (quote (("text" "diff") ("default" "text" "image"))))))

(define-public crate-expectation-shared-0.1 (crate (name "expectation-shared") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0dfpjdn66hjibrg6wa7l559sq4nlzv54gv3k97rv4gqgdiwv7wvz")))

(define-public crate-expectation_plugin-0.1 (crate (name "expectation_plugin") (vers "0.1.0") (hash "1bwj4hhmd17bv9d7fh9xfal90hwv402n3b9qz0vc7rkzfdi61d6f")))

(define-public crate-expectation_plugin-0.1 (crate (name "expectation_plugin") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.9") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1sav272dx4bdjyskwvbr0x78zji5z6h6bbfgnd5r138sghcyj1zk")))

(define-public crate-expectation_plugin-0.1 (crate (name "expectation_plugin") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.9") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fflf39lwx27fffkx5w24prj9i3knc8v91da3mj02vimpbbkg412")))

(define-public crate-expected-0.0.1 (crate (name "expected") (vers "0.0.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-fmt"))) (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-executor") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "maybe-unwind") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0md4j4q9fpcqirkgca7r7kvl84i7l22jmjnrwz1kimg84m7frl31") (features (quote (("futures" "futures-core" "pin-project") ("default" "futures"))))))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.0") (hash "0g0040k3f6lk0zj3i7abvxrjxdpii6z0cdb13wfdcr9gg9h3d98x")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.1") (hash "0asplxhhvjskx3c4jprgjcgjqjcd2ss09q2w03cg44qghbzfdxwr")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.2") (hash "1pl2swb4nrn1gsmsm06mqkpn0jm0cjx1jjk77s1hji1xzhpyq648")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.3") (hash "15i63srfcm23bxl7dsiw4asbgkxm5fisj4p4xdk9qvw74sc7nsak")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "159gkq13ka7ssj77n2dlva8xx2kpb8khg8pig8hjaqqvjhchvi6n")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "19k9mpngspkrdx3qlrgmzzqq3wb69dn9wxy87cn6ky362qdy944c")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.6") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0kylxzsa7q9mpjnsm0sjqxrkcfwhk9w43pbz89lkzisal9cnqzr9")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.7") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1cgp00zdadm08n2xzfnwgnzvkfz07xl9yrjk5cjkgzqgij6rmql9")))

(define-public crate-expectest-0.1 (crate (name "expectest") (vers "0.1.8") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)))) (hash "072kwpq08w68ab4yiyy1vx56ajz2bngss84d65x0yywwmmdzxyba")))

(define-public crate-expectest-0.2 (crate (name "expectest") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0haqqcb6g4fmcj0hrzhnf7s1933csl3ymgrlvv92yv02czjdf86z")))

(define-public crate-expectest-0.2 (crate (name "expectest") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 0)))) (hash "165lrn3rsrjj0apkh2l3zzrlirq6axsxyhn2c73fc4930xnmgwxk")))

(define-public crate-expectest-0.2 (crate (name "expectest") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "~0.1.25") (kind 0)))) (hash "13qyh295vh2slqd9zcj3gi4izqfr3a1r24p6836gg6imwdmdvk57")))

(define-public crate-expectest-0.2 (crate (name "expectest") (vers "0.2.3") (deps (list (crate-dep (name "num") (req "~0.1.25") (kind 0)))) (hash "0aalnxvb4acg36frmi7nrpxh4150p2wpriw93qcyz174ghgrgxra")))

(define-public crate-expectest-0.2 (crate (name "expectest") (vers "0.2.4") (deps (list (crate-dep (name "num") (req "~0.1.27") (kind 0)))) (hash "1agr72bff9sizxcd4c5xf668g7cs3zqbxk90n71fhiy55a9palb9")))

(define-public crate-expectest-0.2 (crate (name "expectest") (vers "0.2.5") (deps (list (crate-dep (name "num") (req "~0.1.27") (kind 0)))) (hash "0bnc037gn7n042v2qwacy3768zjsza6nvpdd4n2i6c0zcdlpi3ip")))

(define-public crate-expectest-0.2 (crate (name "expectest") (vers "0.2.6") (deps (list (crate-dep (name "num") (req "~0.1.27") (kind 0)))) (hash "050gf23ilzzxxx088m0dy9mzzxrb90bsbgvyfl676mnyvnmy2a9r")))

(define-public crate-expectest-0.3 (crate (name "expectest") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "~0.1.27") (kind 0)))) (hash "0sdnrw46fizhwrcd9q462lm24k0qmby2sk97i63698r79v4jsp9b")))

(define-public crate-expectest-0.3 (crate (name "expectest") (vers "0.3.1") (deps (list (crate-dep (name "num") (req "~0.1.27") (kind 0)))) (hash "18z01s5h8iwgykmlxlzcrdxpmsggi3q5hy93cd8didl67razqlh4")))

(define-public crate-expectest-0.4 (crate (name "expectest") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.1.28") (kind 0)))) (hash "153y88h120jgd0r85b902gjbzkkhxyhir1i8sr8pg0a6jsz9604s")))

(define-public crate-expectest-0.4 (crate (name "expectest") (vers "0.4.1") (deps (list (crate-dep (name "num") (req "^0.1.28") (kind 0)))) (hash "1s3kcmfk1cy0k1j7q3xih1ayrphzy9hrnzwl3icwn2lcjh5wr7qw")))

(define-public crate-expectest-0.5 (crate (name "expectest") (vers "0.5.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0w1brkmqn041sj6lwwxxximjd4c8pj9sapp3ldjgd4d7p5h9bn2h")))

(define-public crate-expectest-0.6 (crate (name "expectest") (vers "0.6.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.5.0") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1gzry69zg7frjqj2wmj0amaxij0lnpvs5wmwzb3zs80swrwa6ppy") (features (quote (("nightly"))))))

(define-public crate-expectest-0.7 (crate (name "expectest") (vers "0.7.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.6.1") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "01yjy61rc5594hb0wlkyy4j6fphwy1s0w9p29xm3w88h02ix7azp") (features (quote (("nightly"))))))

(define-public crate-expectest-0.8 (crate (name "expectest") (vers "0.8.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0hmpdkc2cj5gps6ypjdzl38k75ym75k313nhrv063cksmwg0viyx") (features (quote (("nightly"))))))

(define-public crate-expectest-0.9 (crate (name "expectest") (vers "0.9.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0m8dskip5rsk62s2f2mh2fy9xvb8mms21c9fcshk2kzvlywwgfr1") (features (quote (("nightly"))))))

(define-public crate-expectest-0.9 (crate (name "expectest") (vers "0.9.2") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0f24q2a53x7sfmmrqjbwbk7pahzwkpd829fcr023kb7q5xnd6z4g") (features (quote (("nightly"))))))

(define-public crate-expectest-0.10 (crate (name "expectest") (vers "0.10.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0vwai8z7yakyl653wn9nbddipzadvy9pqvf75ds3kwwmsjxfq5pk") (features (quote (("nightly"))))))

(define-public crate-expectest-0.11 (crate (name "expectest") (vers "0.11.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "00dv47irmsyq7brzjhz4xns3p722gm98zp39h9hq2mrzd5marpgq") (features (quote (("nightly"))))))

(define-public crate-expectest-0.12 (crate (name "expectest") (vers "0.12.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y290ak3q5l8l8ajg00mqx1lx9f1pagk6ckmplzibf5ach5pr0bq") (features (quote (("nightly"))))))

(define-public crate-expecting-0.1 (crate (name "expecting") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "1r2q0hfm99is0f50kj3xhcv4gms8s6r7z8gp6lzjhw9ci29azz7d")))

(define-public crate-expecting-0.1 (crate (name "expecting") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "06fz8ss33np4rhgksgi5hgx2h8mq7bqbib9ddpqij4vyz88ih2z3")))

(define-public crate-expecting-0.1 (crate (name "expecting") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0xbf21hi7kbx290vi0da5fax6w1q4jraxwhjyixg34irx9a0j6f6")))

(define-public crate-expecting-0.1 (crate (name "expecting") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "15lh5rjn1w2hp0nsypwnq7pkv28zi2bp57836jgrpgybr3jspp2j")))

(define-public crate-expecting-0.2 (crate (name "expecting") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "102w58rly084j7g0lyi17hanlhpkqp7b3jlg9ggbh16qalvmcfbd")))

(define-public crate-expecting-0.3 (crate (name "expecting") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "1q95fna6jp5fxfmj276x90085yip47mwrzn5yp4k5yvr71difwxz")))

(define-public crate-expecting-0.4 (crate (name "expecting") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "03lx2sgffcgh1ajsx83rlsswvg70nwj3bcw3y8f9mvmfzbb4whrc")))

(define-public crate-expecting-0.5 (crate (name "expecting") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "13d1rvziwrv5xwj1gqgg2i22ndkk4h7kiin2ihqy7fangwz8r72p")))

(define-public crate-expecting-0.6 (crate (name "expecting") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "09cqpacf03injraq1jzhhirs4c38pn13gpdq67scl1p6k92fyrjj")))

(define-public crate-expecto-0.0.1 (crate (name "expecto") (vers "0.0.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.61") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1vrl58820w4fqx0blqxypqs71zg5bxfcc9rpijjxdpj0nhqcd120")))

(define-public crate-expecto-patronum-0.1 (crate (name "expecto-patronum") (vers "0.1.0") (deps (list (crate-dep (name "fastmurmur3") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0zzx6vkk6b5vrz9361i4q40fx0wgv8fysda4x1rs85wrizlph6dz") (features (quote (("std") ("default" "std"))))))

(define-public crate-expector-0.1 (crate (name "expector") (vers "0.1.0") (hash "1ax4ngwkz3hx27xri9fq995wcjh3bxvqq5psyqwc8q4zd78ym5dy")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.0") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "056804cv67078j9dafgfflsk5xwk3hz9vp5fc68mav6nfr4fn9ar")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.1") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z9pgfi780qfhb4kfbbs9nf91f07nf4m2f2zsxzz6bnpdpwgpyrs")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.2") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "130x1fcd3xxjr080jkdhak0mn57qsah3x2clpamvq8vpw69vj573")))

(define-public crate-expectorate-2 (crate (name "expectorate") (vers "2.0.0") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0l1spf29hz2z36dcfq926s1y07lhsh581sbi1jaxy0hnydpk2qmq") (yanked #t)))

(define-public crate-expectorate-2 (crate (name "expectorate") (vers "2.0.1") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bg1s0w72grgb4rbbj99by6w4jpbbvdcal7dlxr0bb0gg31gbafn") (yanked #t)))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.3") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1l7l0i16673ymj2pl7zgbib23viv0xbvwxk6m3y84si14gms3iqb")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.4") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0zp2bqm3avjrl6pb4vww8wwhavaxbgvnqjxbypndngd1m0g60kc0")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.5") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "similar") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "17l3rvlm8809ssv4zbc3yif7173vmm1ndyrr38rg0k5mg1qlb6sx")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.6") (deps (list (crate-dep (name "console") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "similar") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "12r9q6v61k0pyv993ifyvh24v0rvwb1g3g8968caiq069cs8793l")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.0.7") (deps (list (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "similar") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1485rck8ii03j8xq4bv8qhg7rlm57ypxay3gsqsshf3hsnibc2ki")))

(define-public crate-expectorate-1 (crate (name "expectorate") (vers "1.1.0") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "newline-converter") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "similar") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0pypl45889q7psak1j74sir147y3178prwvmwxx798nzbfr1jvyy") (v 2) (features2 (quote (("predicates" "dep:predicates"))))))

(define-public crate-expectrl-0.1 (crate (name "expectrl") (vers "0.1.0") (deps (list (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 2)) (crate-dep (name "ptyprocess") (req "^0.1.1") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0z07iw6d88gmcc272rvq030cq4cagqdqsxlkb70fn7g6a72p0mpz") (features (quote (("sync" "ptyprocess/sync") ("default" "sync") ("async" "ptyprocess/async"))))))

(define-public crate-expectrl-0.1 (crate (name "expectrl") (vers "0.1.1") (deps (list (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 2)) (crate-dep (name "ptyprocess") (req "^0.1.4") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1fjazka0c2jzjwbzdajmxmrafhqqxrb9km7fzlicdzzffswf311s") (features (quote (("sync" "ptyprocess/sync") ("log") ("default" "sync") ("async_log" "futures-lite") ("async" "ptyprocess/async"))))))

(define-public crate-expectrl-0.1 (crate (name "expectrl") (vers "0.1.3") (deps (list (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ptyprocess") (req "^0.1.9") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "19ybywzf5zgik4y646la9kvw7m1vlrypz13f1jsbdiibcrshlj69") (features (quote (("sync" "ptyprocess/sync") ("log") ("default" "sync") ("async_log") ("async" "ptyprocess/async" "futures-lite"))))))

(define-public crate-expectrl-0.2 (crate (name "expectrl") (vers "0.2.0") (deps (list (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "conpty") (req "^0.2.1") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.21.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.2.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0hnqzdcg73h8da59gn346l905zrd456z3f8gz5pj68p7bnp2qvik") (features (quote (("log") ("async" "futures-lite" "async-io"))))))

(define-public crate-expectrl-0.3 (crate (name "expectrl") (vers "0.3.0") (deps (list (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.3.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0wbi5l673q4lfvklqfgl20rqrcyg6maxc4icwin1xlmm33ikf6c1") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking"))))))

(define-public crate-expectrl-0.4 (crate (name "expectrl") (vers "0.4.0") (deps (list (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.3.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0h7z2ww2kiw92lwpy2ld7jd7zh1hnljdq9xdh6lh0r3z1fd19fck") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking")))) (rust-version "1.57")))

(define-public crate-expectrl-0.5 (crate (name "expectrl") (vers "0.5.0") (deps (list (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.3.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0555c3p1nqn4m2j44rw01g9chx0wdbs8yxgkf577kcb2180nyhpj") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking")))) (rust-version "1.57")))

(define-public crate-expectrl-0.5 (crate (name "expectrl") (vers "0.5.1") (deps (list (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.3.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0clc0csildwgkj6xwg79axfk55q748ix2vy58cmk114j488rkw6v") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking")))) (rust-version "1.57")))

(define-public crate-expectrl-0.5 (crate (name "expectrl") (vers "0.5.2") (deps (list (crate-dep (name "async-io") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.3.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1y021c2zdzj7n7r2wmjnv6k5ynjik745mwjlsj24j4p39qgy3597") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking")))) (rust-version "1.57")))

(define-public crate-expectrl-0.6 (crate (name "expectrl") (vers "0.6.0") (deps (list (crate-dep (name "async-io") (req "^1.9.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.6") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "polling") (req "^2.3.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.3.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0ippqvy2ciwa53svi9bd7dgg1qvgfgq8pygnmlxzq32akkga3qqs") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking")))) (v 2) (features2 (quote (("polling" "dep:polling" "dep:crossbeam-channel"))))))

(define-public crate-expectrl-0.7 (crate (name "expectrl") (vers "0.7.0") (deps (list (crate-dep (name "async-io") (req "^1.9.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.5.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.6") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "polling") (req "^2.3.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.4.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "14fb27l5j732q98xkq0dy42vj3waz8dkds4v1rdx022h6aq38vxk") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking")))) (v 2) (features2 (quote (("polling" "dep:polling" "dep:crossbeam-channel"))))))

(define-public crate-expectrl-0.7 (crate (name "expectrl") (vers "0.7.1") (deps (list (crate-dep (name "async-io") (req "^1.9.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "blocking") (req "^1.2.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "conpty") (req "^0.5.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.6") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "polling") (req "^2.3.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ptyprocess") (req "^0.4.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0ykiqkh2r3ky1244r4j0x49hqawdrfdh1mai6fdapz2kb6989rzd") (features (quote (("async" "futures-lite" "futures-timer" "async-io" "blocking")))) (v 2) (features2 (quote (("polling" "dep:polling" "dep:crossbeam-channel"))))))

(define-public crate-expedite-0.1 (crate (name "expedite") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "09kjy1nw07cy7piwh1qis32p8m33zl8jyh2dbq72zm4z7wl3zw16")))

(define-public crate-expedition-0.1 (crate (name "expedition") (vers "0.1.0") (deps (list (crate-dep (name "document-features") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ecolor") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "09j756har17yfvizdj8bjayjn2zwrgwp1pl8fwjq83bd3j7n14ks") (v 2) (features2 (quote (("termcolor" "dep:termcolor") ("serde" "dep:serde" "ecolor/serde" "egui/serde") ("egui" "dep:egui"))))))

(define-public crate-expedition-0.2 (crate (name "expedition") (vers "0.2.0") (deps (list (crate-dep (name "document-features") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ecolor") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "10is8614y04kvywq05dyfryhb8gvj7qpxvlq2gqzlaxrks36qs8z") (v 2) (features2 (quote (("termcolor" "dep:termcolor") ("serde" "dep:serde" "ecolor/serde" "egui/serde") ("egui" "dep:egui"))))))

(define-public crate-expedition-0.2 (crate (name "expedition") (vers "0.2.1") (deps (list (crate-dep (name "document-features") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ecolor") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "06l4j5s11vya72amifcgvk0vymr1v0grb5axmxfkgijbl30yyhy7") (v 2) (features2 (quote (("termcolor" "dep:termcolor") ("serde" "dep:serde" "ecolor/serde" "egui/serde") ("egui" "dep:egui"))))))

(define-public crate-experi-codec-3 (crate (name "experi-codec") (vers "3.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (kind 0)) (crate-dep (name "experi-codec-derive") (req "^3.1") (optional #t) (kind 0)) (crate-dep (name "experi-codec-derive") (req "^3.1") (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "0959safad9qqxy6fg6g3q6la8srgawvnzb5cs6yvshiv1p41ci22") (features (quote (("std" "serde") ("full") ("derive" "experi-codec-derive") ("default" "std")))) (yanked #t)))

(define-public crate-experi-codec-derive-3 (crate (name "experi-codec-derive") (vers "3.1.0") (deps (list (crate-dep (name "proc-macro-crate") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "1xh11wx4chc9fyphw9zr87pgz1d2lvfg30p921fx0pcckrwlf445") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-experiment-cargo-prerelease-semver-1 (crate (name "experiment-cargo-prerelease-semver") (vers "1.0.0-beta") (hash "03nfk2qfk7g74pl3v442akd8g612r2rsdhzmqqz5m8bdj8898xsf")))

(define-public crate-experiment-cargo-prerelease-semver-1 (crate (name "experiment-cargo-prerelease-semver") (vers "1.0.0") (hash "13r6makl44iaj7k5k1a5gy2j3gqwbnf739dkyha2nv6jr8w69fvq")))

(define-public crate-experiment-codec-3 (crate (name "experiment-codec") (vers "3.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (kind 0)) (crate-dep (name "experiment-codec-derive") (req "^3.1") (optional #t) (kind 0)) (crate-dep (name "experiment-codec-derive") (req "^3.1") (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "0fzkm07hb5pfh07w8pcx8xa74c7gc5p7lk58gdailpibybvca0vg") (features (quote (("std" "serde") ("full") ("derive" "experiment-codec-derive") ("default" "std")))) (yanked #t)))

(define-public crate-experiment-codec-derive-3 (crate (name "experiment-codec-derive") (vers "3.1.0") (deps (list (crate-dep (name "proc-macro-crate") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "1wv5gypqlqlpf4cydhnysy9d8k6mlryk9d9d29qn8hdvfhnn11r2") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-experimental-reactive-0.0.1 (crate (name "experimental-reactive") (vers "0.0.1") (hash "0wsb6g64q58150g33k4d182bi3plx5cigkn40zgyi4jhs3k3vg7q")))

(define-public crate-experimental-reactive-0.0.2 (crate (name "experimental-reactive") (vers "0.0.2") (hash "02nwb5vz3c5d491yh4kb5vcqb1fbh1jr43xsxgraxsy53nnjzxni")))

(define-public crate-experimental-reactive-0.0.3 (crate (name "experimental-reactive") (vers "0.0.3") (hash "04sk60kh35nxxmgwad2jjpm3m3rx75k47p2il4ynkhw7abncqwwx")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.0") (hash "0lrm1r6jz8w5wsp4kjmb7wfxpyqcjcs8nrqml0ms4lnda5ix07hj")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.1") (hash "088hfh6pw1cgxyndpl3h120iwfgpfq0nz645sjmkba6is6d74msx")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.2") (hash "11w7mrq3z5bkwb7rkfd4hjjh5cv6v61v324vzkf0iwdrlw9k00pa")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.3") (hash "0p4g9c0p6hd82qk5nibpbr7lvdqgyvz82w43q8v96f7fqn51in2y")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.4") (hash "0p989917qz3cd4lb6a58mjvm3bmx1ssxkvd4ib7lq387padiw9y0")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.5") (hash "1zfz08yf4d5f2nfmn9fnf839346hv0r1hj8fha88s65r3drjv4yy")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.6") (hash "18byqb2g9pvyl9nhh0w1psg7bjggxk3q46gqlfxvfqmmns1ik6w8")))

(define-public crate-experimental-test-0.1 (crate (name "experimental-test") (vers "0.1.7") (hash "142by5ka0rhbnx9sws53410ji5f4d5g12fv18dlxc84lhws8x71s")))

(define-public crate-experimental-tree-sitter-swift-0.0.1 (crate (name "experimental-tree-sitter-swift") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req "~0.20.0") (default-features #t) (kind 0)))) (hash "0c8pd6hjbrjvxaaghqbv1915br8bhn7qgz7wfbbfyx0slb3g1ygv")))

(define-public crate-experimental-tree-sitter-swift-0.0.2 (crate (name "experimental-tree-sitter-swift") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req "~0.20.0") (default-features #t) (kind 0)))) (hash "1kbkpw8dpy0liwmqyp7f024axs28m4dds3pg44bn74s6scrljifn")))

(define-public crate-experimental-tree-sitter-swift-0.0.3 (crate (name "experimental-tree-sitter-swift") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req "~0.20.0") (default-features #t) (kind 0)))) (hash "0nsyr41qsi35hcw9rxd1nq348rnzh3p01dvf6z2f94xh65chfbjh")))

(define-public crate-expert-0.0.1 (crate (name "expert") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "interpolate_idents") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "string-interner") (req "^0.6") (default-features #t) (kind 0)))) (hash "08iv9l2zl0fw7l572wrx1gayl5f6lnnnri1im33j6wfhrb7qbn9c")))

