(define-module (crates-io ex lo) #:use-module (crates-io))

(define-public crate-exlog-0.0.1 (crate (name "exlog") (vers "0.0.1") (hash "0n2hmdb9zgx1mg96gbpsjrz7g90yq003kqspihmryghfz13xy6b8") (yanked #t)))

(define-public crate-exlog-0.0.2 (crate (name "exlog") (vers "0.0.2") (hash "14svywdbiqz6l6qjh6mvaj63mq6czjr7nxqkj1ky3qzqk2vgwg1p") (yanked #t)))

(define-public crate-exlog-0.1 (crate (name "exlog") (vers "0.1.1") (hash "0f6x75zqgwf4gniyds1vakly6myfck7mf4fzkmhd9ca886p3yzp8") (yanked #t)))

(define-public crate-exlog-0.1 (crate (name "exlog") (vers "0.1.2") (hash "1cfw6gb047g6hnzsl2rkd2wpygjbr76vzsmrmrk5yxspgg75fvbc") (yanked #t)))

(define-public crate-exlog-0.1 (crate (name "exlog") (vers "0.1.3") (hash "0ah80d5a0mn6prq69baja0bi1agwkvkddsh0alxqz864ynxlrigm") (yanked #t)))

(define-public crate-exlog-0.1 (crate (name "exlog") (vers "0.1.4") (hash "0l5kif5c6dkx3sxbh6jfg1ylixixpzlcl53s788iyg6mrk8ysbl0")))

