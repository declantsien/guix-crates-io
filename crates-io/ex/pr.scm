(define-module (crates-io ex pr) #:use-module (crates-io))

(define-public crate-expr-0.0.1 (crate (name "expr") (vers "0.0.1") (hash "1pnp5l8bvcl6s76g4jydg6jik0wrgd6y3whz0dls4pmvslcmn9x8")))

(define-public crate-expr-child-0.1 (crate (name "expr-child") (vers "0.1.0") (hash "0fsi1zsix995kg59rca42xvvj25y1sy3gijra3w4y21kg4f9cnaj")))

(define-public crate-expr-child-0.1 (crate (name "expr-child") (vers "0.1.1") (deps (list (crate-dep (name "expr-parent") (req "0.1.*") (default-features #t) (kind 0)))) (hash "146diyk8jjnjyrs58jd2mb90dfx6hjz431mlqz2lj9sl58v6pm77")))

(define-public crate-expr-parent-0.1 (crate (name "expr-parent") (vers "0.1.0") (deps (list (crate-dep (name "expr-child") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1dksvk7gvlva5s9dgv43mz7g7q1rs50snvnk40ljipjsa9xh5yry")))

(define-public crate-expr-solver-1 (crate (name "expr-solver") (vers "1.0.0") (hash "0l9sxjcslpqjknnsqw6kssccsgk0ly5968q0fmfg0264ng1qpgmb") (yanked #t)))

(define-public crate-expr-solver-1 (crate (name "expr-solver") (vers "1.0.1") (hash "1xs82h3cdl02n1j5inlg8vd8gx3v2q9ckkfzpkf29q9wgisx7pa7") (yanked #t)))

(define-public crate-expr-solver-1 (crate (name "expr-solver") (vers "1.0.2") (hash "0ix49hnr4272ys6hda8wm2qj5vf033c2nfsnxmq5vgp4sb09rvqx") (yanked #t)))

(define-public crate-expr-solver-1 (crate (name "expr-solver") (vers "1.0.4") (hash "1gil6pbnrz240r98qjd3r4s4jh825kflqwx6b9jqpgjhiqzmm0gr") (yanked #t)))

(define-public crate-expr-solver-1 (crate (name "expr-solver") (vers "1.0.6") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0nv111nbvplzih8b5a1s12xynxjr58za1h2zjiwln936lm7z6d5q")))

(define-public crate-expr_parser-0.0.1 (crate (name "expr_parser") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)))) (hash "0f3b7z2w5aih5la1za8s5vg42dwviamly110wymizdjkn9iz17pl")))

(define-public crate-expr_rs-0.0.0 (crate (name "expr_rs") (vers "0.0.0") (deps (list (crate-dep (name "chumsky") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16.4") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.23") (features (quote ("maths"))) (default-features #t) (kind 0)))) (hash "001bvylck14c548bdavshq24b1iavc25lcl7ixp7hb5iliwy57d9") (v 2) (features2 (quote (("python" "dep:pyo3"))))))

(define-public crate-expr_tools-0.1 (crate (name "expr_tools") (vers "0.1.0") (deps (list (crate-dep (name "evalexpr") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "string_utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vector_utils") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rd8vi3a8fb6lip11cmr2ccbxa35amn9md03n666zx2bri59iacx")))

(define-public crate-expr_tools-0.1 (crate (name "expr_tools") (vers "0.1.1") (deps (list (crate-dep (name "evalexpr") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "string_utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vector_utils") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zlsnhlpmsh53byb0yvlpnryrcc5bxsjbak083b763br3nfm06gv")))

(define-public crate-expr_tools-0.1 (crate (name "expr_tools") (vers "0.1.2") (deps (list (crate-dep (name "evalexpr") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "string_utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vector_utils") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nglw2y1xiz4k04vylzqq0h61az0gqd4kpd9xmkakn4a8vbj9kfy")))

(define-public crate-expr_tools-0.1 (crate (name "expr_tools") (vers "0.1.3") (deps (list (crate-dep (name "evalexpr") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "string_utils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vector_utils") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ga2wkmbcb19gyvqvwm51b82ghz7nvh7w1rg6xjdhzcv1333i9i6")))

(define-public crate-express-0.0.1 (crate (name "express") (vers "0.0.1") (hash "1iqnimysyylngp9dzl4fyb25m03aa363lp3i2l8ns0syiyd8xnjh")))

(define-public crate-express-0.0.2 (crate (name "express") (vers "0.0.2") (hash "0p7zgzkidygwmbbj41ba0wzzrx0b4la2y85cclgnhhz4qs95ww9d")))

(define-public crate-express-rs-0.1 (crate (name "express-rs") (vers "0.1.0") (hash "02652nmgqi8qvqiwn747ab6qdr865bc2hgifb327fy875nwllvzi") (yanked #t)))

(define-public crate-express-rs-0.0.1 (crate (name "express-rs") (vers "0.0.1") (hash "1dlz0a81ix0ipryd8knpwmji4x2bmhjl160znl7n1biw047f96gf")))

(define-public crate-express-rs-0.0.2 (crate (name "express-rs") (vers "0.0.2") (hash "1f57dgz23bxa22y9b3css41daa5c05l6jkv44y5paav94i4j9lip")))

(define-public crate-express-rs-0.0.3 (crate (name "express-rs") (vers "0.0.3") (hash "1913ws1pz21h5slqfyyhbid6n74ywipscjgh0spfch1mi6sh0kpv")))

(define-public crate-express-rs-0.0.4 (crate (name "express-rs") (vers "0.0.4") (hash "0z12yf9y81yibsm37pyr0sd8l2nfykgmgn60s3257j1ransbksds")))

(define-public crate-express-rs-0.0.5 (crate (name "express-rs") (vers "0.0.5") (hash "0ghvx8dk1masz1washmjjlxc7gn9bg101q2s6f124a3zf8nspglj")))

(define-public crate-express-text-gui-0.0.0 (crate (name "express-text-gui") (vers "0.0.0") (hash "1j4m2bdpqhzcyrr3d1rkq8akfsl3b1kgx7ixia0zli3wl0nzsk9v")))

(define-public crate-expression-0.1 (crate (name "expression") (vers "0.1.0") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0iyapw9paa944xabjx15cbhzmbdmbl9ijr889b5jwmx8n64v199l")))

(define-public crate-expression-0.2 (crate (name "expression") (vers "0.2.0") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1laxp6sy184r4da3p266ff9gxpxadvllqyybgz4209lh4s46h8hj")))

(define-public crate-expression-0.3 (crate (name "expression") (vers "0.3.0") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ih9j9gsdwm54xi9kp5vxp2sy1c3m1amrhlwyygwhj6z1rmd1s2c")))

(define-public crate-expression-0.3 (crate (name "expression") (vers "0.3.1") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bx62ggs8by6nvxa6gfrbrkm1mpvbhjnrip88knd68c6iqva7a0k")))

(define-public crate-expression-0.3 (crate (name "expression") (vers "0.3.2") (deps (list (crate-dep (name "asexp") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "11lnznx4n3zg87hg8g0p3drwrxqii4909gjr91aszi9jxcbphs3j")))

(define-public crate-expression-0.3 (crate (name "expression") (vers "0.3.3") (deps (list (crate-dep (name "asexp") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "07j2gldzmfn4l9fyhnqvmz9j4adzjwgk5cb97znpsq4g2jlmfhyn")))

(define-public crate-expression-0.3 (crate (name "expression") (vers "0.3.4") (deps (list (crate-dep (name "asexp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1cxyd915z6ckc4n1sh3m14pv7p48szfdnxh1wgyrf42n9brvi3zs")))

(define-public crate-expression-closed01-0.1 (crate (name "expression-closed01") (vers "0.1.0") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "closed01") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "143n20jra416ix6srxccig4lvjzclyn83cgbx1fhc32xrg90767n")))

(define-public crate-expression-closed01-0.2 (crate (name "expression-closed01") (vers "0.2.0") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "closed01") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "00q1v1yfcsbv73cd7671grbpyy64xw9g28ghizxsc0x8g1g06ifl")))

(define-public crate-expression-closed01-0.2 (crate (name "expression-closed01") (vers "0.2.1") (deps (list (crate-dep (name "asexp") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "closed01") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "08ck6dngydxixbd84d0g6yc2qi9cq36g0qqry4qdxz234vd5v8rz")))

(define-public crate-expression-num-0.1 (crate (name "expression-num") (vers "0.1.0") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "13zsqjliygwlyhly0jy4ncmwsnx73i78q63pjss47h1za8zifja4")))

(define-public crate-expression-num-0.2 (crate (name "expression-num") (vers "0.2.0") (deps (list (crate-dep (name "asexp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1zs6g6nvsqnqkc293hjpliw249k3c3c4jax6sfgy6hrjl1nx1hgb")))

(define-public crate-expression-num-0.2 (crate (name "expression-num") (vers "0.2.1") (deps (list (crate-dep (name "asexp") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "03fdk7x3y0nk3dcgv25pv0dpi0wqdj3kkyji14vx2w939ixmb015")))

(define-public crate-expression-num-0.2 (crate (name "expression-num") (vers "0.2.2") (deps (list (crate-dep (name "asexp") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "07x5a2mvffwa68p36akaq20gnb66pzcigh3lrvzsp05hm0p66ihx")))

(define-public crate-expression-num-0.2 (crate (name "expression-num") (vers "0.2.3") (deps (list (crate-dep (name "asexp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "expression") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0053qza9444qhin972898q4c6cf5fdc0jcldba9drvb5bkng0v11")))

(define-public crate-expression_engine-0.1 (crate (name "expression_engine") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "15pf71by8yssbgafvclwa2q8krz60ix4xkxs363ncv53fm533144") (yanked #t)))

(define-public crate-expression_engine-0.2 (crate (name "expression_engine") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "0nfs11vsjcxarhh23m82n9614wm85y2vs8a2i582vcb8b2n1yrr4") (yanked #t)))

(define-public crate-expression_engine-0.3 (crate (name "expression_engine") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "17sy82gr79hnpaxx05nhl8wlk10i0dxpjjrr4693h6jcrj3gwy33")))

(define-public crate-expression_engine-0.4 (crate (name "expression_engine") (vers "0.4.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "1l8yvlpfxw7vzzg7qh6iv1ygqqdqxmb01kh3q3rxkknagr6y8rzk")))

(define-public crate-expression_engine-0.5 (crate (name "expression_engine") (vers "0.5.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "1yabcvj5sybw0d32nqmflq80x665s2a64bginqxs9brj20lj42c1")))

(define-public crate-expression_engine-0.5 (crate (name "expression_engine") (vers "0.5.1") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "1f86fni3g5g6nyq35xqdmzdbnnivajkidp600ljjybr3adwc7qdc")))

(define-public crate-expression_engine-0.5 (crate (name "expression_engine") (vers "0.5.2") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "181wxlbbgicmjaq8vhd0ij9cykxjzx5gbaxddqraq1ckysnzb4sr")))

(define-public crate-expression_engine-0.5 (crate (name "expression_engine") (vers "0.5.3") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "1hyiag7223zkq3v0yazkw6mxmir58gvmp7lcmkfiqf2mkkdlisyw")))

(define-public crate-expression_engine-0.6 (crate (name "expression_engine") (vers "0.6.0") (deps (list (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "rust_decimal") (req "^1.31.0") (default-features #t) (kind 0)))) (hash "11sj21ys58r3ys9f1mnncjp2mw934g68m941wvj1gac4v012jrkh")))

(define-public crate-expression_engine-0.7 (crate (name "expression_engine") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "rust_decimal") (req "^1.31.0") (default-features #t) (kind 0)))) (hash "0214y6za7b7syc2j7gzd8f355hwhpb82yc9yb9kyv9y6n8rvby3v")))

(define-public crate-expression_format-1 (crate (name "expression_format") (vers "1.0.0") (deps (list (crate-dep (name "expression_format_impl") (req "^1") (default-features #t) (kind 0)))) (hash "0zycgj76kydz3vmjrx5x9xpccpnm64601cwyffxxgzn5xyp1gs20")))

(define-public crate-expression_format-1 (crate (name "expression_format") (vers "1.0.1") (deps (list (crate-dep (name "expression_format_impl") (req "^1") (default-features #t) (kind 0)))) (hash "1rh6hvd9855p781l7a20hjkwr54byck4jhqfv0s9iarafww0vhaz")))

(define-public crate-expression_format-1 (crate (name "expression_format") (vers "1.0.2") (deps (list (crate-dep (name "expression_format_impl") (req "^1") (default-features #t) (kind 0)))) (hash "0f9bhbsqy4vvvjmwnnsii1dix5awbdgf9l2sb5zrff6a9f6l2ap8")))

(define-public crate-expression_format-1 (crate (name "expression_format") (vers "1.1.0") (deps (list (crate-dep (name "expression_format_impl") (req "^1") (default-features #t) (kind 0)))) (hash "0a2qvz1nmvcyjj2smkwp7zms7sba6dswpsww98x1zd5l5zhm5c5s")))

(define-public crate-expression_format-1 (crate (name "expression_format") (vers "1.1.1") (deps (list (crate-dep (name "expression_format_impl") (req "^1") (default-features #t) (kind 0)))) (hash "1msbk7gvp64xrj7gsvw3m79za7gdawvpkyz31s9i72nkiq7i4ab7")))

(define-public crate-expression_format-1 (crate (name "expression_format") (vers "1.1.2") (deps (list (crate-dep (name "expression_format_impl") (req "^1") (default-features #t) (kind 0)))) (hash "0g09dxrfp0676k7a1p8lpbiiyhzshn35gj1sp6cmbx4bk0sj6850")))

(define-public crate-expression_format-1 (crate (name "expression_format") (vers "1.1.3") (deps (list (crate-dep (name "expression_format_impl") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "00n3y4z1a8dncas9aqk24khbjjzwnjhy9vrgv35w8d3cz3d2x3fi")))

(define-public crate-expression_format_impl-1 (crate (name "expression_format_impl") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0s4w5dbjn8q9fs4nwlyfwbq92rf84a19nh20rw382vjylkfk8cq3")))

(define-public crate-expression_format_impl-1 (crate (name "expression_format_impl") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0r6whvay17g98ga9mk2hgm8vgighki1w8vyv23l84pa1ihgvsa1s")))

(define-public crate-expression_format_impl-1 (crate (name "expression_format_impl") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0s8cfgpvxbnplwmvp8mggkm0g7cdrc620hyhk8y7mz92lvvvjx8s")))

(define-public crate-expression_format_impl-1 (crate (name "expression_format_impl") (vers "1.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1p7b366lqdnasvq1blx99n41w4k93msmvqwx0pzfbl45pb80q1fi")))

(define-public crate-expressions-1 (crate (name "expressions") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0n9drsfjmd99l7dhly6jrnp9l158lbz5f3lgwnd6wci8fh7nl6pc")))

(define-public crate-expressions-1 (crate (name "expressions") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1sxf4yi9rsw64152rcgh4j4lbm9j3c9nvj5jmq06af5pngmy9m7l")))

(define-public crate-expressions-2 (crate (name "expressions") (vers "2.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1m8q3jcny3ds72cajy0f9057xfc1cf6rc3lkam2i6w3hf4z3ss3l")))

(define-public crate-expressive-0.1 (crate (name "expressive") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)))) (hash "0jds09a5c3d6wsmj9h669s2zpc77wwjap2wycm13gn3h2sxpsyna") (features (quote (("serde_support" "serde" "serde_derive") ("regex_support" "regex"))))))

(define-public crate-expressive-0.1 (crate (name "expressive") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)))) (hash "1y91f03pm4fzw40g332p4hmf0qq5crz9vb5lq600sj1669qazvns") (features (quote (("serde_support" "serde" "serde_derive") ("regex_support" "regex"))))))

(define-public crate-expressive-0.1 (crate (name "expressive") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.133") (optional #t) (default-features #t) (kind 0)))) (hash "0f3lggl66y01ni2az7a2ddi8nlpa4g95pqb47v3q9yxjzybghwdr") (features (quote (("serde_support" "serde" "serde_derive") ("regex_support" "regex"))))))

(define-public crate-expresso-0.1 (crate (name "expresso") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0glm25bym7yjmzria1c7mlj8kknvimxkr3faxnq2rhihfnsgzdpv")))

(define-public crate-expresso-0.1 (crate (name "expresso") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1h6npra7qszllp2ravhiq5770qqplsxfz1yxgksh1fmdkjpy8aan")))

(define-public crate-expresso-0.1 (crate (name "expresso") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1g83dj9v5yk120nb9837fpwx9wrbs4df4m0jk7wcnd21d6kfbhid")))

(define-public crate-expresso-0.1 (crate (name "expresso") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1gnpys5brqm31lma2fwif5jfmqwp8sjv3ddnjkvlrnlkh1kgward")))

(define-public crate-expresso-0.1 (crate (name "expresso") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0n4vnxc1df1fx3pnv56p8j5c1m2hvvki3wl8zr7jpw3vxfiwxkcw")))

(define-public crate-expresso-0.2 (crate (name "expresso") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1bhn4jz2kkcq7lpnnqcryf8p44rbj827r9qsqvb6222a0ssiv8gh")))

(define-public crate-expresso-0.2 (crate (name "expresso") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0wlh23nia7k4gv1qlbanp7pqk4h1fcyvi812wp96b4nig90v39x8")))

(define-public crate-expresso-0.2 (crate (name "expresso") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0b5zw8j0sl4xxm7nyaiz6h0di641j760knvf64arfvizn448c2lw")))

(define-public crate-expresso-0.2 (crate (name "expresso") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "094x6pnkvppa5m63spglgxnzw2px81v04clqn2acsypkj9xjaj8k")))

(define-public crate-expresso-0.2 (crate (name "expresso") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ml1fmzvcdwcnz4fhqv11pna5x70ljsxnjml987qp9qf3yvcafxb")))

(define-public crate-expresso-0.2 (crate (name "expresso") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "14p3kcaxl6j2fb1y64dxp8aah2f3mvh2bmyd7n2712zn0pwb5rxx")))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.0") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "0yc19whgmqhars6jahgwwhpiqrww1n1kbkmqjp4hnh1izgfcl3n6") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.1") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "0wdr5rai7w7l6bj97chwy4dkpya91ilxr0f324p2cwfwm68ryk4h") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.2") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "1wlaq1rwsvyp7njky2jz5qw2wgr6vfisxq3zq3j09varjh8p0s7n") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.3") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "10krchmk6vg7a31lgqr5598295vfgahj5mh1zrl2cv16h285x2b8") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.4") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "0b9achzd8i984lrvhkblh8irdzg1h9c975pr6drlrmxlamggfjx5") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.5") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "178mr65f06dsd1l93fjdlk8knchhsjnn9rkw2winlw0fjj827gi8") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.6") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "080i6p5bvlxq4byiqvpbs1i0gdkkazbkd9knlcr25kyk29blhwfj") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.7") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "0n36nqrdzf3rxdh3ydcny9kg305mdmlqlpii9dkds42709c3yi5b") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.8") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "1n8lqf345j2965lywwqr19vli812zhcrxlbqmvbwi3xccf8fbz9i") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.9") (deps (list (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)))) (hash "0difigs8aabfx3kypjl2q3qpk4jzkd7y534k85fiv9lvbp3spmf5") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "15fiwqw0b3mjfxcq0a39j29a2da8siyrkxz8jxav10jdsinjqwhp") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0svgf38nnblfrpaqa973mb8dsg05iv9xrk9ka4xa2nf0vjamdvwc") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.13") (deps (list (crate-dep (name "anyhow") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (default-features #t) (kind 0)))) (hash "05hcn6a46vnkd0x9ac1b3g8nbk88fl1gc1v8xlalhq1ppwvbl4y9") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.14") (deps (list (crate-dep (name "anyhow") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (default-features #t) (kind 0)))) (hash "0yhidr64pyqqn27fx6pm4glqzvjdv1hns2h1mfvkhw4ssb9sid1r") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprimo-0.1 (crate (name "exprimo") (vers "0.1.15") (deps (list (crate-dep (name "anyhow") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "rslint_parser") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "scribe-rust") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (default-features #t) (kind 0)))) (hash "1373hdfvka0n5dkhf0lzf7rd0ww728ns0wi3bg19sn0dn6bkqpx9") (features (quote (("logging" "scribe-rust") ("default"))))))

(define-public crate-exprint-0.0.0 (crate (name "exprint") (vers "0.0.0") (hash "0grc2b4mbxw93nk3qzj25zjysyc9ba4whzarc7llqla7bp6phjhz") (yanked #t)))

(define-public crate-exprs-0.1 (crate (name "exprs") (vers "0.1.0") (hash "1lnai8zfpas76z7j4mjmpjl4mn9m6nhgp9cwk0yx4v6p2ym88xrg")))

(define-public crate-exprs-0.1 (crate (name "exprs") (vers "0.1.1") (hash "0ybwww8wz9gjzqgxqbz47d4gz6dq83w3s3ynpd4iaq2brz449h6s")))

(define-public crate-exprt-0.1 (crate (name "exprt") (vers "0.1.0") (hash "1r0sd8lax78916313g2q3wgcp0hgzrxwr0w9br97kbwmflm9fy82")))

(define-public crate-exprtk_rs-0.0.1 (crate (name "exprtk_rs") (vers "0.0.1") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f86icsckarshszdsmnq6ayayp3p7k1zpfn62djx6qh06pgbaxrw") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "comments" "break_continue" "sc_andor" "return_statement" "rtl_io_file" "rtl_vecops" "enhanced_features" "superscalar_unroll" "caseinsensitivity") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue"))))))

(define-public crate-exprtk_rs-0.0.2 (crate (name "exprtk_rs") (vers "0.0.2") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gxydc9hj7x40c8i7gfpvh7nl3s8m9q6kis7vp785ix58z28k8j3") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "comments" "break_continue" "sc_andor" "return_statement" "rtl_io_file" "rtl_vecops" "enhanced_features" "superscalar_unroll" "caseinsensitivity") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue"))))))

(define-public crate-exprtk_rs-0.0.3 (crate (name "exprtk_rs") (vers "0.0.3") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "10ypz3ypfb03j3mkvhwjl54858cx2p2230appjrs5778ydgix8pv") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "comments" "break_continue" "sc_andor" "return_statement" "rtl_io_file" "rtl_vecops" "enhanced_features" "superscalar_unroll" "caseinsensitivity") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue"))))))

(define-public crate-exprtk_rs-0.0.4 (crate (name "exprtk_rs") (vers "0.0.4") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06jc23gsv228dbsg8xirrkrcxl4vy00c2x2dmnbj5cnvx6i8b11s") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "comments" "break_continue" "sc_andor" "return_statement" "rtl_io_file" "rtl_vecops" "enhanced_features" "superscalar_unroll" "caseinsensitivity") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue")))) (yanked #t)))

(define-public crate-exprtk_rs-0.0.5 (crate (name "exprtk_rs") (vers "0.0.5") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wkplj0ffca10qw4qk3shyhkpr4gj0lpm3mm1rk0p33p9igy2xvi") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "comments" "break_continue" "sc_andor" "return_statement" "rtl_io_file" "rtl_vecops" "enhanced_features" "superscalar_unroll" "caseinsensitivity") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue")))) (yanked #t)))

(define-public crate-exprtk_rs-0.0.6 (crate (name "exprtk_rs") (vers "0.0.6") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "031yyabpbnfi63wq4fzni7ry1j15v64ab3j7dxzlssl13dbdg84w") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "comments" "break_continue" "sc_andor" "return_statement" "rtl_io_file" "rtl_vecops" "enhanced_features" "superscalar_unroll" "caseinsensitivity") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue")))) (yanked #t)))

(define-public crate-exprtk_rs-0.0.7 (crate (name "exprtk_rs") (vers "0.0.7") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1imngks9ypp3rhh4x39z0lw44vcgj5a9gqcxgffz7zz249wfqlri") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "comments" "break_continue" "sc_andor" "return_statement" "rtl_io_file" "rtl_vecops" "enhanced_features" "superscalar_unroll" "caseinsensitivity") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue")))) (yanked #t)))

(define-public crate-exprtk_rs-0.1 (crate (name "exprtk_rs") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exprtk_sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "100nw7mnsczca6scvgm34rh8zr4c7fhf0x57f6wjixk9lbqgj80l") (features (quote (("superscalar_unroll" "exprtk_sys/superscalar_unroll") ("sc_andor" "exprtk_sys/sc_andor") ("rtl_vecops" "exprtk_sys/rtl_vecops") ("rtl_io_file" "exprtk_sys/rtl_io_file") ("return_statement" "exprtk_sys/return_statement") ("enhanced_features" "exprtk_sys/enhanced_features") ("default" "all") ("debug" "exprtk_sys/debug") ("comments" "exprtk_sys/comments") ("caseinsensitivity" "exprtk_sys/caseinsensitivity") ("break_continue" "exprtk_sys/break_continue") ("all" "exprtk_sys/comments" "exprtk_sys/break_continue" "exprtk_sys/sc_andor" "exprtk_sys/return_statement" "exprtk_sys/rtl_io_file" "exprtk_sys/rtl_vecops" "exprtk_sys/enhanced_features" "exprtk_sys/superscalar_unroll" "exprtk_sys/caseinsensitivity"))))))

(define-public crate-exprtk_sys-0.0.1 (crate (name "exprtk_sys") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fm3wgfxsaq4l7bzsrhk6si6xin0j6fwij9pbi1zpgv3cn2g8lgp") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.2 (crate (name "exprtk_sys") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ipklnp2cysa6mdy0bm5kpm9b73ds41x4z2sj7rg7m45az7qs012") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.3 (crate (name "exprtk_sys") (vers "0.0.3") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00zbxwla74mddh3m3j8d5zvms6lmgrd1k1mxg2c4air7y9is18j7") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.4 (crate (name "exprtk_sys") (vers "0.0.4") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "09lkydn91gcx8lfyjfdsfz2lh0m2vh1djkvjqrzpx07p85spkabq") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.5 (crate (name "exprtk_sys") (vers "0.0.5") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13kjq3qbwbif291pijm9g3i0gh2xbygd0lg658igndqhqn2164pp") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.6 (crate (name "exprtk_sys") (vers "0.0.6") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cvzxnfs6r6p6f4wwl124fkpjzfpq6cbsgi2l0rxvn5alwdvs853") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.0.7 (crate (name "exprtk_sys") (vers "0.0.7") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zq40pwln235dzhpy6whxfl052dvg3n03nvdipk7bfw28js1nlz0") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-exprtk_sys-0.1 (crate (name "exprtk_sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ifh2vb67hcf2xikaxw4rl0b98hnp51nr12szxxi5w2kgsdjnzsx") (features (quote (("superscalar_unroll") ("sc_andor") ("rtl_vecops") ("rtl_io_file") ("return_statement") ("enhanced_features") ("default") ("debug") ("comments") ("caseinsensitivity") ("break_continue"))))))

(define-public crate-expry-0.1 (crate (name "expry") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "011kn1m3z8ymqvip7ja2dfps305sxn72r926zv861fa3wa0mpx2n") (features (quote (("std") ("mini") ("default" "std") ("bin"))))))

(define-public crate-expry-0.1 (crate (name "expry") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "1rr5m6q7z7jwk9ik5k29h5rp7y1c8lf5gf8cwd67jvaswhx6hp8z") (features (quote (("std") ("mini") ("default" "std") ("bin"))))))

(define-public crate-expry-0.1 (crate (name "expry") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "0gkzxivx0rwp5a50s6drl2y17ganj2ljr4y68f8cys0kns4l5qkp") (features (quote (("std") ("mini") ("default" "std") ("bin"))))))

(define-public crate-expry-0.2 (crate (name "expry") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "0iv4xf8jx2l9pajk0pn7mhvnydwsrh5swrqck19233i2r9d2sq2s") (features (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin")))) (yanked #t)))

(define-public crate-expry-0.2 (crate (name "expry") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "03ibwm0zad77y6mp53kk2qrmbk3qjvf8rr26412930iwl9dgcm14") (features (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

(define-public crate-expry-0.2 (crate (name "expry") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "0p2ljk9ylarzph298nxf09kww2zgqgjd63y9ixwbcsfiimavqv4g") (features (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

(define-public crate-expry-0.3 (crate (name "expry") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "1xv2i3lf0xiia3i8hbyzz3v6q7vknwmqsx0q7lh3y3l1igz34m7p") (features (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

(define-public crate-expry-0.4 (crate (name "expry") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "15sh5bs5bw1kzi3s6abxfpz6v5i47ajyl91h0wjr6x6wv9sc23dd") (features (quote (("std") ("power") ("mini") ("default" "std" "power") ("bin"))))))

(define-public crate-expry_macros-0.1 (crate (name "expry_macros") (vers "0.1.0") (deps (list (crate-dep (name "expry") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "0khq74hcfpy41m3zwkplh487fdmqjhp9wdgwv5jr7s4bprfgihgh")))

(define-public crate-expry_macros-0.1 (crate (name "expry_macros") (vers "0.1.1") (deps (list (crate-dep (name "expry") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m22wipzyxbvd3wwxyf3bnr8pdj1ipyrx7qqy4pnfcs4697r6w6d")))

(define-public crate-expry_macros-0.1 (crate (name "expry_macros") (vers "0.1.2") (deps (list (crate-dep (name "expry") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "03v2ndzyd4yjq5v9b3s3kk214zn61dn27rslg0arn80wyxgpyy6q")))

(define-public crate-expry_macros-0.2 (crate (name "expry_macros") (vers "0.2.0") (deps (list (crate-dep (name "expry") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d6lhylb4iz0yv4ilqg0fpzg512xr4clwzrnnhhwzz1a10i0gsnb") (yanked #t)))

(define-public crate-expry_macros-0.2 (crate (name "expry_macros") (vers "0.2.1") (deps (list (crate-dep (name "expry") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xfzpm302p618dqrjgx5qqhk03mm7i7pkg1ab82mdip8i151c71h")))

(define-public crate-expry_macros-0.3 (crate (name "expry_macros") (vers "0.3.0") (deps (list (crate-dep (name "expry") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0flvb7slkzgggcwah5bh4j25cahvc0js6q9478s69wck3j3x4v09")))

(define-public crate-expry_macros-0.4 (crate (name "expry_macros") (vers "0.4.0") (deps (list (crate-dep (name "expry") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nq4myrc4vy4il200pkidpphw46pq0s15an8sny2m3vb3xr4f3fk")))

(define-public crate-exprz-0.0.0 (crate (name "exprz") (vers "0.0.0") (deps (list (crate-dep (name "exprz-core") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0xwi83yil0w8gbw0i7fjbmfbbc1jakqz7nyswhl3fdh8y2lzlc7k")))

(define-public crate-exprz-0.0.1 (crate (name "exprz") (vers "0.0.1") (deps (list (crate-dep (name "exprz-core") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0hbww77xpm3g9r6rm83qlfhlb2d13ppwk4d5vvizdimgbb3h1nqv")))

(define-public crate-exprz-0.0.2 (crate (name "exprz") (vers "0.0.2") (deps (list (crate-dep (name "exprz-core") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "173xqia2y0myc6dp194p6v9jla6p8rmjx6y3dwjj4f2d26nrfmyh")))

(define-public crate-exprz-0.0.3 (crate (name "exprz") (vers "0.0.3") (deps (list (crate-dep (name "exprz-core") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1wrk9zdv1d711dpg987sf0vjrrx1ghnzn33qhx00jyfj1gyvnzgj")))

(define-public crate-exprz-0.0.4 (crate (name "exprz") (vers "0.0.4") (deps (list (crate-dep (name "exprz-core") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0j4d0a0f0j56ljsas87gb2w9l72apcxfqwqag132ay3ndil31h0y")))

(define-public crate-exprz-0.0.5 (crate (name "exprz") (vers "0.0.5") (hash "0z5xw69v4i8kgvhf55lzm052s2837y6vwhnsns6z1ajxijmc917i") (features (quote (("std") ("default"))))))

(define-public crate-exprz-0.0.6 (crate (name "exprz") (vers "0.0.6") (hash "18zqzz3syipjlp8y80rvyq8vly4yv6cd1phf9146q8flj8f6nxgv") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.7 (crate (name "exprz") (vers "0.0.7") (hash "0xfax7y5km9a26q9l7nkhjgw5qpr27m7492p206zcdxmmv0ch8h7") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.8 (crate (name "exprz") (vers "0.0.8") (hash "006brnr84nypm3ksp67mxlx80r2msfiid38f6a6xsbga3l5cx0ma") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.9 (crate (name "exprz") (vers "0.0.9") (hash "1783kqd1cz3cr85bbg7q9r6jf9nxdlsw6hzqcyic3vgsx7ihsl4g") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.10 (crate (name "exprz") (vers "0.0.10") (hash "18z1qab8lwm1gi7fb8ps5a7gb6yzrz5ywdyrfp3xss9rv54k28sf") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.11 (crate (name "exprz") (vers "0.0.11") (hash "0grvlxpwlnk9c23rijf6c67937cakpz3slhjnm3p04w0h4fcpvb4") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.12 (crate (name "exprz") (vers "0.0.12") (hash "0j5iwmxb4abl38hp8crrs3li5xrgqmrl8kzxzxrr1s8gjlvk0nla") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.13 (crate (name "exprz") (vers "0.0.13") (hash "0dp5bn39afcsi7ssncf3qihyxx1626nsbnc9icaszkn7a91hb73g") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("experimental" "shape" "pattern" "buffered") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.14 (crate (name "exprz") (vers "0.0.14") (hash "1cd91hb5d4a2n3zicrd8m0kqf4amx0w0fssm647az2i039xk1xa5") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("multi") ("experimental" "buffered" "multi" "parse" "pattern" "shape") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-0.0.15 (crate (name "exprz") (vers "0.0.15") (deps (list (crate-dep (name "rayon") (req "^1.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (optional #t) (default-features #t) (kind 0)))) (hash "1233vm1925fglf591pl3jhlm9m7slqc2azyjd5h62yq2bd7wc6g7") (features (quote (("stable" "alloc") ("shape") ("pattern") ("parse") ("no-panic") ("multi") ("experimental" "buffered" "multi" "no-panic" "parse" "pattern" "rayon" "serde" "shape") ("default") ("buffered" "alloc") ("alloc") ("all" "stable" "experimental"))))))

(define-public crate-exprz-core-0.0.0 (crate (name "exprz-core") (vers "0.0.0") (hash "0h9g0jvrhdn3nj26qiz1nkgvbsz9wch1ycyx0a0jy2mxfk6iix9l")))

(define-public crate-exprz-core-0.0.1 (crate (name "exprz-core") (vers "0.0.1") (hash "1y45479dpchgb8zc124hnk4np6mj78xr46nlh4m0vigbwwjr3kr5")))

(define-public crate-exprz-core-0.0.2 (crate (name "exprz-core") (vers "0.0.2") (hash "04fh2qhc2cphng81jllyfw8waibi38vyspm56zcj0rda0zhcq1dx")))

(define-public crate-exprz-core-0.0.3 (crate (name "exprz-core") (vers "0.0.3") (hash "1q3irrlgwkmh87sdriiqyvp0iz8i9znkdbrwfphsi2is5xw19ds0")))

(define-public crate-exprz-core-0.0.4 (crate (name "exprz-core") (vers "0.0.4") (hash "061isap143hz1adhsa5w1wn4hfp5911i0y82n71zqnd73w4bd779")))

(define-public crate-exprz-core-0.0.5 (crate (name "exprz-core") (vers "0.0.5") (hash "17fd46d6ly1hhz9ryx2v1l6f92sjq1mqdxfd4fm955qdn8bx3ji7")))

(define-public crate-exprz-core-0.0.6 (crate (name "exprz-core") (vers "0.0.6") (hash "0lapiwrhpdxfws9m5w071npwsvwxx4n1140phdvd8bp883i1f0ym")))

