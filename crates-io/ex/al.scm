(define-module (crates-io ex al) #:use-module (crates-io))

(define-public crate-exalt-0.1 (crate (name "exalt") (vers "0.1.0") (deps (list (crate-dep (name "sfml") (req "0.15.*") (default-features #t) (kind 0)))) (hash "18a87s3afcp7vwd178y4wr8gb9239kaz3lzdn97iadwj7bdbbqrc")))

