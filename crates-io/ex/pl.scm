(define-module (crates-io ex pl) #:use-module (crates-io))

(define-public crate-explain-0.1 (crate (name "explain") (vers "0.1.0") (deps (list (crate-dep (name "rocket") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.5") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lqwqzbbrms78sh7zdc2wlhy167qb5lk209k353w5jykqa4biyk5")))

(define-public crate-explaincron-0.1 (crate (name "explaincron") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.8") (default-features #t) (kind 0)))) (hash "0lf60mgadz7l0r3r97l7izk8h99s7fhndvlfvpq8kh6bp5qk6y1s")))

(define-public crate-explaincron-0.2 (crate (name "explaincron") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (features (quote ("local-offset" "macros"))) (default-features #t) (kind 0)))) (hash "01i20rav2wh5s0iahwsrn7smskqsc24346bir0glqxwqw67w9y89")))

(define-public crate-explicit-discriminant-0.1 (crate (name "explicit-discriminant") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.82") (default-features #t) (kind 2)))) (hash "1fg4kz1mknsfd06f7wi9ji8pyy23pcfrpvqzn8mlf94kf4khx1y5")))

(define-public crate-explicit-discriminant-0.1 (crate (name "explicit-discriminant") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.82") (default-features #t) (kind 2)))) (hash "042l0w5zsj7a0yqsq6l6dqf4w7z6qj0d96gfyf4svibias2asb0m")))

(define-public crate-explicit-discriminant-0.1 (crate (name "explicit-discriminant") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.82") (default-features #t) (kind 2)))) (hash "0haf6rc33ncziz5jix4rd2i6dj7zdcgb6mz40z3xhjcrnjjr9873")))

(define-public crate-explicit-discriminant-0.1 (crate (name "explicit-discriminant") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.82") (default-features #t) (kind 2)))) (hash "00bmx08gcbwv0hvxzk45qvjkli083lxfv4jasylcnz21fxi26v41")))

(define-public crate-explicit-endian-0.1 (crate (name "explicit-endian") (vers "0.1.0") (hash "0khpxzwfd8ifbz5w0zk69k8n2a58a3qh3k4256jdrf2fzlbw7rqw")))

(define-public crate-explicit-endian-0.1 (crate (name "explicit-endian") (vers "0.1.1") (hash "1gdbn7pq7hyzw6vnk6b81zww2qbmkifx7wwphxh7kjxnmgkq9wm3")))

(define-public crate-explicit-endian-0.1 (crate (name "explicit-endian") (vers "0.1.2") (hash "0l9wfyabpyysgj0ajimqp419n94ylh11vhr0dh0wkb3r1hm7h6w2")))

(define-public crate-explicit-endian-0.1 (crate (name "explicit-endian") (vers "0.1.3") (hash "0ji7j6qi7sp1m2qr8pkymfyxggzhv1cv4fim3vx7g0gyqiqdhna9")))

(define-public crate-explicit-endian-0.1 (crate (name "explicit-endian") (vers "0.1.4") (hash "056n1myvgc3nli2d7x8pdbxbm13z4zr2vybn0zyavp86qmwzwi9h")))

(define-public crate-explicit-endian-0.1 (crate (name "explicit-endian") (vers "0.1.5") (hash "13dawbl4nb1mxs50zzdlv5frzha8d4vcxi745813nrsm22mhhf6r")))

(define-public crate-explicit_cast-1 (crate (name "explicit_cast") (vers "1.0.0") (hash "0kip0nc8jjv69j5byrn7dbfjzl3w7wbknhwwgq2aa0g0rffhlayw")))

(define-public crate-explo-0.1 (crate (name "explo") (vers "0.1.0") (hash "13g01n8r25lf98yk0sagcxsp3rd7ma9j7d7av58nnmbjkwbz58bn")))

(define-public crate-explode-0.1 (crate (name "explode") (vers "0.1.0") (deps (list (crate-dep (name "arraydeque") (req "^0.4") (default-features #t) (kind 0)))) (hash "05rmyclhprqsfq5kv6ys6ih6vqg4wycnc8vc37nh5mn1zxn9ymyr")))

(define-public crate-explode-0.1 (crate (name "explode") (vers "0.1.1") (deps (list (crate-dep (name "arraydeque") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bmks7jiq5qxlv2g6fijnl3xpc2ib1wwgd037vxa816qcagmxvqg")))

(define-public crate-explode-0.1 (crate (name "explode") (vers "0.1.2") (deps (list (crate-dep (name "arraydeque") (req "^0.4") (default-features #t) (kind 0)))) (hash "1b15h2kw3dl691fk5z5rhab6khiqjyyh81l0w31sd8mpl785ssa6")))

(define-public crate-exploit-0.1 (crate (name "exploit") (vers "0.1.0") (hash "0b1xfhrrh7fdvzykpvyi9llch79a1linknc661y0248fhj807jw5")))

(define-public crate-ExploitBuilder-0.1 (crate (name "ExploitBuilder") (vers "0.1.0") (hash "1ggq1cl8vxl569rqiyi35w7mhj632nyrkqsqblp4xx5igm2lmsp5")))

(define-public crate-explorer-0.1 (crate (name "explorer") (vers "0.1.0") (hash "052jdfd5vy2xrys4nvpq0vcakapmjllrkxkwqpmv0i6jjqadhll9")))

