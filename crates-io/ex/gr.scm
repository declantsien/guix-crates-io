(define-module (crates-io ex gr) #:use-module (crates-io))

(define-public crate-exgrid-0.1 (crate (name "exgrid") (vers "0.1.0") (deps (list (crate-dep (name "egui24") (req "^0.24") (optional #t) (kind 0) (package "egui")) (crate-dep (name "egui25") (req "^0.25") (optional #t) (kind 0) (package "egui")) (crate-dep (name "egui26") (req "^0.26") (optional #t) (kind 0) (package "egui")) (crate-dep (name "egui27") (req "^0.27") (optional #t) (kind 0) (package "egui")) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maybe-owned") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "09mrf08ax0sbd96si1hqy16bjcnhad7vj5z50r010gsdpj93pbp3") (features (quote (("default" "egui27"))))))

