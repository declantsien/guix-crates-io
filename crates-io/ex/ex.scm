(define-module (crates-io ex ex) #:use-module (crates-io))

(define-public crate-exex-0.0.1 (crate (name "exex") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.4") (default-features #t) (kind 0)))) (hash "1mazbid5vsi90qqrj1dcds6ma9szf5c153dy56p7dbd4bfm4nb6w") (yanked #t)))

