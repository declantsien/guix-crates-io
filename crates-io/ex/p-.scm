(define-module (crates-io ex p-) #:use-module (crates-io))

(define-public crate-exp-golomb-0.1 (crate (name "exp-golomb") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "01lbgbg1l6qv3b4vxpk8wj1l8mnllg11day0y7z26vac0qf0n5d8")))

(define-public crate-exp-semver-20 (crate (name "exp-semver") (vers "20.0.0-preview.9.0") (hash "0ch2afphi6v9iaivr82dln1fdnynv4rgsj5jnwwl2x9qhwncmip9")))

