(define-module (crates-io ex t_) #:use-module (crates-io))

(define-public crate-ext_auth-0.0.1 (crate (name "ext_auth") (vers "0.0.1") (hash "1a7r4mqv8k9b2is8w2wp1hzzj3mscf8g9qxcq3yk953i4pxzssz9") (rust-version "1.64")))

(define-public crate-ext_count-1 (crate (name "ext_count") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (default-features #t) (kind 0)) (crate-dep (name "incr_stats") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tree_magic") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1fziz6msjv9rga4yqkjd3lj40p0h11nws6jmg5783b5ans3r4qal")))

(define-public crate-ext_count-1 (crate (name "ext_count") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (default-features #t) (kind 0)) (crate-dep (name "incr_stats") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tree_magic") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0v4ch1yg28502mvrb66dzii18q5wijr0p02b0a8kkjqfmwmd8xp4")))

(define-public crate-ext_format-0.1 (crate (name "ext_format") (vers "0.1.0") (deps (list (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0zhj4msmb6x4m016lsry2gd7gc270f1l879ic07zk1vc5qndv7na")))

(define-public crate-ext_format-0.1 (crate (name "ext_format") (vers "0.1.1") (deps (list (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "13xygwnrrc7pc0v390lalvzjp4l7wn415w0hzcc7pw4wn5j3xjig")))

(define-public crate-ext_std-0.1 (crate (name "ext_std") (vers "0.1.0") (hash "17jad3r4j5fc3q96ab4ldsi6nxwgddxhpnpzr8lx84awwq2gr9cz")))

(define-public crate-ext_std-0.2 (crate (name "ext_std") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1flsd5zdcn7bn1v670hkkw9z4hp86v4fzfqyy0b5vbfl8v0mvsl5")))

(define-public crate-ext_std-0.3 (crate (name "ext_std") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0wly38iwwab5afskfsb7y4hs3q3b670wsvnssf56zj8b4i1np0xj")))

(define-public crate-ext_std-0.4 (crate (name "ext_std") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0fyazvfwxgbq8a800iq4yhc0dl1y4jwbvs0c8mpgk8k0z9dcm49x")))

