(define-module (crates-io ex fa) #:use-module (crates-io))

(define-public crate-exfat-0.1 (crate (name "exfat") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hmcnc13qnr2iadmym9f6di0jjig4w8lhc32q0d1d7jbg4dim6pg")))

