(define-module (crates-io ex ch) #:use-module (crates-io))

(define-public crate-exch_rate-0.1 (crate (name "exch_rate") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.167") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("rt" "full"))) (default-features #t) (kind 0)))) (hash "1fk20i26s8mb5apx4i5fyrbclxlv7s2rk81id7lfrfdlw03v0985")))

(define-public crate-exchage-0.1 (crate (name "exchage") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.20") (default-features #t) (kind 0)))) (hash "0ia7a2kjqlbw597fjngzjh77axv2h6xw6bgm9yb2371640926q18")))

(define-public crate-exchage-0.1 (crate (name "exchage") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.20") (default-features #t) (kind 0)))) (hash "14k4aa6vv06ai3p8ndkgyfvgbh7fcjm9vpj4x1zf8yic4arbw70z")))

(define-public crate-exchange-0.0.1 (crate (name "exchange") (vers "0.0.1") (hash "0qd241kpzxylahiwfqsnc0jia1xgiwbxpainx125cgri8w8b0wm8")))

(define-public crate-exchange-rate-0.1 (crate (name "exchange-rate") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "floyd-warshall-alg") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "safe-graph") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0zxcc2dnkprq76zqprym6z2wsr14whi4qzjv6qj39gvmf4dj9xl8")))

(define-public crate-exchange-rate-0.1 (crate (name "exchange-rate") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "floyd-warshall-alg") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "safe-graph") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0fyy2s3xiyqisn63s1bvkim2wca3xgci43d6hixmv5dd2r90fvzg")))

(define-public crate-exchange-rate-0.1 (crate (name "exchange-rate") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "floyd-warshall-alg") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "safe-graph") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1cbw5y25nak26h0cyn77ia3pqfq5z54wljqpcgy0w1mh7ajnzccb")))

