(define-module (crates-io ex ce) #:use-module (crates-io))

(define-public crate-excel-0.0.1 (crate (name "excel") (vers "0.0.1") (deps (list (crate-dep (name "actix-multipart") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^1.0.0-rc") (default-features #t) (kind 0)) (crate-dep (name "calamine") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "concat-string") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.4") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "05ji7g760jmj8clyw59v2zr8w9fdfw26v0ma2xc61ijc50afcaxz")))

(define-public crate-excel-emulator-0.1 (crate (name "excel-emulator") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "excel-emulator-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "libmath") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.23.0") (features (quote ("serde" "serialize" "encoding"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.143") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0q76yx1v4kxvqwhhs49cswgfmaiwzzw0w196af8nl997556xysf2")))

(define-public crate-excel-emulator-macro-0.1 (crate (name "excel-emulator-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1r0bywh6nrianfih19ib57ywippb5mzgqky0074f5f12n9q3jiiw")))

(define-public crate-excel-kit-0.1 (crate (name "excel-kit") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "0yj8ag6hx9vx3n7mbi1kig1bv3mrschx9swka5l153zqd7gy11cj") (yanked #t) (rust-version "1.64.0")))

(define-public crate-excel-kit-0.1 (crate (name "excel-kit") (vers "0.1.1") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "10q3hc9g1libd08cdw6b9wxh36qqa3sag47sxd50f61ld2637i0q") (rust-version "1.64.0")))

(define-public crate-excel-kit-0.1 (crate (name "excel-kit") (vers "0.1.2") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "16vwdzwd88gw9j08829i7w7imslxzy4q8801yvsngprvva8zx6b7") (rust-version "1.64.0")))

(define-public crate-excel_column_id-0.1 (crate (name "excel_column_id") (vers "0.1.0") (hash "0wgfj9x5r4nk84q603vl8rm0g0f07kryrz33gjp88q55klk6h9fz")))

(define-public crate-excel_column_id-0.1 (crate (name "excel_column_id") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0k6xsyw92xwrqly14lsrzmr11xxaiigiqp78nxlwgp02a6bd826b") (features (quote (("with-serde"))))))

(define-public crate-excel_column_id-0.1 (crate (name "excel_column_id") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vnqa5sqlzhl2sb7mh34av6lk6fzgr9bcb3wn9s4bah65bv9x1da") (features (quote (("with-serde"))))))

(define-public crate-excel_column_id-0.1 (crate (name "excel_column_id") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "19lc08zk3y1ps6q3nwzfp1bn61kj922ja0cnnrhqvb4n9dbydac0") (features (quote (("with-serde"))))))

(define-public crate-excel_column_id-0.1 (crate (name "excel_column_id") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0izyij8vrd4wq212lwr91088kxb6l6f19x2622i5npl5s8yxxhhs") (features (quote (("with-serde" "serde"))))))

(define-public crate-excel_writer-0.1 (crate (name "excel_writer") (vers "0.1.0") (deps (list (crate-dep (name "excel_column_id") (req "^0.1") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "0pinc9s5cm3xl1faddwmp4laxczg44nxk4wy9pb956g3lg41zj9a")))

(define-public crate-excel_writer-0.2 (crate (name "excel_writer") (vers "0.2.0") (deps (list (crate-dep (name "excel_column_id") (req "^0.1") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.0.0-beta.14") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "1962l6s9lys6062caas4aaxa2mh6nccsim14bhc602fdnzb5z8q6")))

(define-public crate-excel_writer-0.2 (crate (name "excel_writer") (vers "0.2.1") (deps (list (crate-dep (name "excel_column_id") (req "^0.1") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "1y33il9jam4yczicjxglagmdcyyfwzvijwkmrbam7r18ih3s5da5")))

(define-public crate-excel_writer-0.2 (crate (name "excel_writer") (vers "0.2.2") (deps (list (crate-dep (name "excel_column_id") (req "^0.1") (features (quote ("with-serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "0g9jxrg3gx8kif2x5r7l3dqqzl51g8lnpabj8s0vrf9bi0cl31rk")))

(define-public crate-excel_xmlwriter-0.1 (crate (name "excel_xmlwriter") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "1vs8fym990hiiphl4k5awx8pznya1dc1y493nr5rah21jrp6kyw2")))

(define-public crate-excelwriter-0.1 (crate (name "excelwriter") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "05fpshad290lsadn8f1hg30b4m20hb7skhv67db8sfzv1dvp1pfc")))

(define-public crate-except-0.1 (crate (name "except") (vers "0.1.0") (hash "0lspqkl85h67rj4s7s04wgl33v0gx1gjlixwf5xhywzmhl2r8l85")))

(define-public crate-except-0.2 (crate (name "except") (vers "0.2.0") (hash "0339dyc1i49a1m2b30mmhanvmjqy0sp9z5rfss90y29ydrkm8fsi") (rust-version "1.68")))

(define-public crate-except-0.3 (crate (name "except") (vers "0.3.0") (hash "0lsjrljk9kr166x95l2dchcd643bqa31a7bgmz6inmn9y3ngj249") (rust-version "1.68")))

(define-public crate-except-plugin-0.0.1 (crate (name "except-plugin") (vers "0.0.1") (hash "0ni5v5x9w4kx9jvn8qfhc2i1iwpxvjhv4rwf2ac91gxpd6b347ii") (features (quote (("macros"))))))

(define-public crate-exceptional-0.0.0 (crate (name "exceptional") (vers "0.0.0") (hash "0sw1fj16xj4h1b6qs7jlh4zklp6v4ph40w8q0iy077qan1nblr6q")))

