(define-module (crates-io ex ol) #:use-module (crates-io))

(define-public crate-exolvl-0.5 (crate (name "exolvl") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qdifrqwk18k8zhlvq8d6whvm0v6lirmhp7m4j30a9gz7a0wk3dy") (v 2) (features2 (quote (("serde" "dep:serde" "chrono/serde"))))))

(define-public crate-exolvl-0.6 (crate (name "exolvl") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0jarpcwrdpywkdwx9kiprdzxg1swjwwvbj3jc8kicbc60ff194zs") (v 2) (features2 (quote (("serde" "dep:serde" "chrono/serde"))))))

