(define-module (crates-io ex p3) #:use-module (crates-io))

(define-public crate-exp3-0.1 (crate (name "exp3") (vers "0.1.0") (deps (list (crate-dep (name "remain") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1abbwx17i4xvvl3g4h7dkzm7kq66144nb75bf3sd02666fwp763h")))

(define-public crate-exp3-0.2 (crate (name "exp3") (vers "0.2.0") (deps (list (crate-dep (name "remain") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "0yvfxiqxw92paa7ixdkan9jiv0dq6ndyhry7s0239q8sbl669liy")))

