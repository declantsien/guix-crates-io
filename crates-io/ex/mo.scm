(define-module (crates-io ex mo) #:use-module (crates-io))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "17h0bbwqwziq2r11xkbc4j9ybs7zfyjxcchzb8qggcvivzjwh3hs") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "1mi59dcxq6jxwjbd82apgzwh7x1z01xp78lxmylrqr9apga5hjp8") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "1jjpqsdmym0affzfpgwsr4fknln08wpz499pgvv2w58a8flzvs0j") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "1p9x37540bxxn440aa7x4ws0nj3kgk87dmch392cjydvbc1pak7y") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "1j2l5vnfcw81cpsbxbgarrzk0h2hkg43cs7zd6izpw5jwjkw1a9v") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "0lw69i739kr8z537w5py7f0rbyrx7xny1mwyspw6jzynsxvy3s4w") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "1vsxggjk9ihyabzg7nl0rqy3dh760jxfd62z7bd0p315ibism22g") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.7") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "0arvvdarkv117grqmz0sbw4sa175bxz1gb7zpa76xarfy06m7gn4") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.8") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "1h1b3xmf7m8bi2g5482y1akmbf0nnrb0svspgl05x8lvjwzy943g") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings")))) (yanked #t)))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.9") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "0igvqhj1kzr9k7l6w8fzdwmrkgs487rvb2sbfjp1ldq1jl4kr8cy") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings")))) (yanked #t)))

(define-public crate-exmod-0.1 (crate (name "exmod") (vers "0.1.10") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "0yrif3mn5vgd7af12p4jakq615vsmln82lsikzj00zxy19ac8hzm") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.2 (crate (name "exmod") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "04c1ki43jh4xiviwnxay8bcxc1g37cf4nrpljnp3qi2780j0k9bm") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.2 (crate (name "exmod") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)))) (hash "03rya05v32zywvdjph2b4axrr3cy6f19nbrx3qnl335q1m4nzbix") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3 (crate (name "exmod") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)) (crate-dep (name "sdl2") (req "^0.35") (kind 2)))) (hash "0rpj71vdd8qzpph8g5j2i3hg0d02gknmzyx894kqq6bb8gflsm42") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3 (crate (name "exmod") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)) (crate-dep (name "sdl2") (req "^0.35") (kind 2)))) (hash "1lrm3yjp2w35yv4dgyy20r9k509m3mdirh7l1f544hpqay5l5hnd") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3 (crate (name "exmod") (vers "0.3.2") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)) (crate-dep (name "sdl2") (req "^0.35") (kind 2)))) (hash "0phnjnmx2k2arjw246vf39b6vp9h9s33l8cdnizlz7cds2mgmx6x") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

(define-public crate-exmod-0.3 (crate (name "exmod") (vers "0.3.3") (deps (list (crate-dep (name "cc") (req "^1") (kind 1)) (crate-dep (name "cpal") (req "^0.13") (kind 2)) (crate-dep (name "sdl2") (req "^0.35") (kind 2)))) (hash "1azsf5447m8isc1vgrd019gs3ff24arwaw5lxqr925ai4yjrmx0v") (features (quote (("strings") ("ramping") ("linear_interpolation") ("defensive") ("default" "linear_interpolation" "ramping" "strings"))))))

