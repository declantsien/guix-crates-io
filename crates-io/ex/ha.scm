(define-module (crates-io ex ha) #:use-module (crates-io))

(define-public crate-exhaust-0.1 (crate (name "exhaust") (vers "0.1.0") (deps (list (crate-dep (name "exhaust-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0v3hiskqjdrgd4s0km8727sx7fbpjs4hw5n6wbprssd694shmvfi") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-exhaust-0.1 (crate (name "exhaust") (vers "0.1.1") (deps (list (crate-dep (name "exhaust-macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (features (quote ("use_alloc"))) (optional #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1d62pfwngc0zn1s7isdf3znhq8c7sjwjl0afhsmnn8377n2g245s") (features (quote (("std" "alloc") ("default" "std") ("alloc" "itertools"))))))

(define-public crate-exhaust-macros-0.1 (crate (name "exhaust-macros") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04g891g05pk7i39yvawvmi7f4acxa776im7fh2nh24iras47hgyx")))

(define-public crate-exhaust-macros-0.1 (crate (name "exhaust-macros") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qasml36ngqn5pc4m41w19936kx37yik1ib5i9mxwxcvhsyzizjy")))

(define-public crate-exhaustigen-0.1 (crate (name "exhaustigen") (vers "0.1.0") (hash "1djym5wjcib78fxspkkal0vdq8dd1vlllk6f6858m5haf53zg23x")))

(define-public crate-exhaustive-0.1 (crate (name "exhaustive") (vers "0.1.0") (deps (list (crate-dep (name "exhaustive_macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0f3gx85v04i4mccqc3hxk2iqd2d22va2gmjjvrs6dibxawybrn2w") (features (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

(define-public crate-exhaustive-0.1 (crate (name "exhaustive") (vers "0.1.1") (deps (list (crate-dep (name "exhaustive_macros") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0afjkrqlvaazzpwpmlzvc3hf32gm40bs0khfxqamam27pkd2m36c") (features (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

(define-public crate-exhaustive-0.2 (crate (name "exhaustive") (vers "0.2.0") (deps (list (crate-dep (name "exhaustive_macros") (req "=0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1psdb5b1y1fzv20klcmkbwxa3fzvcffl702a9kagc40mci07s0x3") (features (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

(define-public crate-exhaustive-0.2 (crate (name "exhaustive") (vers "0.2.1") (deps (list (crate-dep (name "exhaustive_macros") (req "=0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "1d3bqgm8akbkpr574zk74z1jjp264vw2m78mdk51xqn50a5r2zf1") (features (quote (("macros" "exhaustive_macros") ("default" "macros"))))))

(define-public crate-exhaustive-map-0.1 (crate (name "exhaustive-map") (vers "0.1.1") (deps (list (crate-dep (name "exhaustive-map-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rqdygf1i1l9wkb3nxmbiblqv2ra7bild49mr3jac41ghl0l894v")))

(define-public crate-exhaustive-map-0.2 (crate (name "exhaustive-map") (vers "0.2.0") (deps (list (crate-dep (name "exhaustive-map-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0airl25460nlld2amyxcmhywxzm9is8y2m4wwrygxf2z4g25jcf8")))

(define-public crate-exhaustive-map-0.2 (crate (name "exhaustive-map") (vers "0.2.1") (deps (list (crate-dep (name "exhaustive-map-macros") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1v1g40wda6ibc5c99v0pv0bh26skghgpvcv61fd7q6z7fhrxbixm")))

(define-public crate-exhaustive-map-macros-0.1 (crate (name "exhaustive-map-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "079nnz1qlzfqfa1vav4adlw6yja8lr6p181qjrb6balx8wp22l36")))

(define-public crate-exhaustive-map-macros-0.1 (crate (name "exhaustive-map-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "14xaj3bh448rvb8qqg4rhmwa3q9ngc2d84ynd1caddx63agg1v26")))

(define-public crate-exhaustive-map-macros-0.2 (crate (name "exhaustive-map-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "0ims6nwg3mm9ynifc8drwlr44phr12cc5vlcyqa6fzhvnpzzapk8")))

(define-public crate-exhaustive-map-macros-0.2 (crate (name "exhaustive-map-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "0lnvfnvqndqrbqxvjmsjy1cvifkvqx30p68lfh927g527whzs0c0")))

(define-public crate-exhaustive_macros-0.1 (crate (name "exhaustive_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16bdpx2wlrnbda7xllbfvr96rc0c8ql9dmvin8k10jhx05fpkhxf")))

(define-public crate-exhaustive_macros-0.1 (crate (name "exhaustive_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "005a8921y943y1hqdgvpkglg7b1k7pyqc5pk6yymrbcfhakfdamd")))

(define-public crate-exhaustive_macros-0.2 (crate (name "exhaustive_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0nhiac1ijfl8z31dk4b5m6np4iwdjx8n2cs3504xzkj97ca1m6xg")))

(define-public crate-exhaustive_macros-0.2 (crate (name "exhaustive_macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ivsk2lm9pq1g6a8pkp0i4pply99447rj753a23frww93c321ijs")))

