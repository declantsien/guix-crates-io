(define-module (crates-io xx te) #:use-module (crates-io))

(define-public crate-xxtea-0.1 (crate (name "xxtea") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1sri9qlzax971zdaa74m1vsc54rldbyixm1w7ymi6ddpbcpzi7ry")))

(define-public crate-xxtea-0.1 (crate (name "xxtea") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ic99jj8wb84185ff3p6b2z67jk7cnr2mqbz7835jwbgjlgyy3hk")))

(define-public crate-xxtea-0.2 (crate (name "xxtea") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "17628xpfmv99di5x4s928svmwkfk9yfyij8sv4h0bk07y2mma9p5")))

(define-public crate-xxtea-nostd-0.1 (crate (name "xxtea-nostd") (vers "0.1.0") (hash "16sb8fnlbxcdf65rgcci5cfrwypkjmfi21rnp39s2ymyybxsvbbd")))

