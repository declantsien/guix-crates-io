(define-module (crates-io xx ht) #:use-module (crates-io))

(define-public crate-xxhttp-0.1 (crate (name "xxhttp") (vers "0.1.0") (hash "104f7n3jqy6ck51kj52svld1labfyrx74nypbb1bkq2s0291r1k1")))

(define-public crate-xxhttp-0.1 (crate (name "xxhttp") (vers "0.1.1") (hash "0agcykvmd73a14ydzx18rpg630qkxk58jf5pvghbd83bd7n31jc4")))

(define-public crate-xxhttp-0.1 (crate (name "xxhttp") (vers "0.1.2") (hash "035jmklb2xvmxa4j7cr18x3k94hkcjlb3pgb9mmggnd9cnw24x8q")))

(define-public crate-xxhttp-0.1 (crate (name "xxhttp") (vers "0.1.3") (hash "01zpb4bgq8cx5y9p0b8idajd2xd5zakz9warswc04gmfvp0bp287")))

(define-public crate-xxhttp-0.1 (crate (name "xxhttp") (vers "0.1.4") (hash "1yj8nflghad1lv3rq3vdpzvq46b16ld1lgkilqzwmmmsprjwmh39")))

