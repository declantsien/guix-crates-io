(define-module (crates-io xx h3) #:use-module (crates-io))

(define-public crate-xxh3-0.0.0 (crate (name "xxh3") (vers "0.0.0") (deps (list (crate-dep (name "pyo3") (req "^0.16") (features (quote ("extension-module" "abi3-py37"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1c1lnaq811mlq7lzgk6fjxx5np317qwlxrhbxrlx6d38nlcv56pc") (features (quote (("python" "pyo3")))) (rust-version "1.59")))

(define-public crate-xxh3-0.1 (crate (name "xxh3") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pyo3") (req "^0.16") (features (quote ("extension-module" "abi3-py37"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "15fvrypvnf8hhmglnl2yhvprmvmpml4n7qclzka8y4lqixya41p1") (features (quote (("python" "pyo3")))) (rust-version "1.59")))

(define-public crate-xxh3-0.1 (crate (name "xxh3") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "pyo3") (req "^0.16") (features (quote ("extension-module" "abi3-py37"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "00gdb8g08gig4pfcvbq6mskimp8g69pdpyfvgsg7lclxli8iwaca") (features (quote (("python" "pyo3")))) (rust-version "1.59")))

