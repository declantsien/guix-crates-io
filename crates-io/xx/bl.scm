(define-module (crates-io xx bl) #:use-module (crates-io))

(define-public crate-xxblake3-0.0.1 (crate (name "xxblake3") (vers "0.0.1") (deps (list (crate-dep (name "blake3") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1l6h305skfxfyq0lmz2kbk5qnljrx07hh58x2ay36g80qw7hw733")))

(define-public crate-xxblake3-0.0.2 (crate (name "xxblake3") (vers "0.0.2") (deps (list (crate-dep (name "blake3") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0hmsr1xpvri9mswji2r234iskffbbqw2ng8gch2mcx19s1v2av14")))

(define-public crate-xxblake3-0.0.3 (crate (name "xxblake3") (vers "0.0.3") (deps (list (crate-dep (name "blake3") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0r5f89c4f31m89xr0yrarsibw0ygrdi08br798r7022k48x4fzql")))

(define-public crate-xxblake3-0.0.4 (crate (name "xxblake3") (vers "0.0.4") (deps (list (crate-dep (name "blake3") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0a6cgvnc498kv861rs3s5n143hz8h32wa3q417ka10lc0v82pdn1")))

(define-public crate-xxblake3-0.0.5 (crate (name "xxblake3") (vers "0.0.5") (deps (list (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.2") (default-features #t) (kind 0)))) (hash "15mm2r0czyj0d2n02hsjsd4hvmmiag67lkw3lnlhbvgmgw7f87fc")))

(define-public crate-xxblake3-0.0.6 (crate (name "xxblake3") (vers "0.0.6") (deps (list (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.3") (default-features #t) (kind 0)))) (hash "00v36gzi1yl2mqdk2j6vxylzfbfd4yhxblbqba2h5ncldqmk2rms")))

(define-public crate-xxblake3-0.0.7 (crate (name "xxblake3") (vers "0.0.7") (deps (list (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.3") (default-features #t) (kind 0)))) (hash "10shqkaag3f2m92ycbw0hpzmsk5j1d9x9g6piqk04d8iya7davlh")))

(define-public crate-xxblake3-0.0.8 (crate (name "xxblake3") (vers "0.0.8") (deps (list (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.5") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "0615x64daz25sz8hivksmn88hvjpqcr29csdkyj1iavq35ni5s6v")))

(define-public crate-xxblake3-0.0.9 (crate (name "xxblake3") (vers "0.0.9") (deps (list (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.5") (features (quote ("xxh3"))) (default-features #t) (kind 0)))) (hash "1vlrivkg146qnzav63z0n7lc37sqj5bx27f74i63fiiswljg8k5h")))

