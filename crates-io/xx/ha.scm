(define-module (crates-io xx ha) #:use-module (crates-io))

(define-public crate-xxhash-0.0.1 (crate (name "xxhash") (vers "0.0.1") (hash "0vx03fvsm5d9c0h00ddiby6mmf4sj5jzvlqvhffnv54gmlw1z0c0")))

(define-public crate-xxhash-0.0.2 (crate (name "xxhash") (vers "0.0.2") (hash "0ymbin8kyc8mdi45rb0n4ddzzw33mx3n7v6i0pqjjgmxidifygwm")))

(define-public crate-xxhash-0.0.3 (crate (name "xxhash") (vers "0.0.3") (hash "06d28v7v9fyr5vhzigy2cbjyz1zn714pnw2i31hjra1si3vqsl2s")))

(define-public crate-xxhash-0.0.4 (crate (name "xxhash") (vers "0.0.4") (hash "0343x43z3h6jsxq89dppg96gvbvgx7pa9zd5y8iv3lvkha5ssxc2")))

(define-public crate-xxhash-0.0.5 (crate (name "xxhash") (vers "0.0.5") (hash "1pj1rzgjb7xdb6i608qp6bw8zgmbv26fik2fhr4fp9z3v81s2mmr")))

(define-public crate-xxhash-0.0.6 (crate (name "xxhash") (vers "0.0.6") (hash "111j4rdypyzvij7lr7rz0bv4llwj6gyb2ygwdv2i4a59q4s83phg")))

(define-public crate-xxhash-0.0.8 (crate (name "xxhash") (vers "0.0.8") (hash "14azdq4irwyh8sq2z37nmmansimqcmg99bcrd7sq70scg8857ijw")))

(define-public crate-xxhash-c-0.8 (crate (name "xxhash-c") (vers "0.8.0") (deps (list (crate-dep (name "get-random-const") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "162a4zqzxqxd96lrzi0fqdn3w1hwga2y8b9kabm8dir993fzp25m")))

(define-public crate-xxhash-c-0.8 (crate (name "xxhash-c") (vers "0.8.1") (deps (list (crate-dep (name "get-random-const") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0rkg9f3gjj36dgninl9s45wm8h7pl6p4c96j89bn7dd4dkvig2im")))

(define-public crate-xxhash-c-0.8 (crate (name "xxhash-c") (vers "0.8.2") (deps (list (crate-dep (name "get-random-const") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1jc8y6xv9bnmcbzmdwm347rz48a17gx1a7i778ssn0vga7qzyzmb")))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1n9plnppyiirixyn47lcydkw779l14bn99dh4wbdw3hgx646v31y") (yanked #t)))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.0+1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1sp607h4znd4iri548knv5wmgisn57dcv9nd8h8b958zl0hlhgfp") (yanked #t)))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0ym6pk6fv8cmqqnnsppkhdxqxdjw517jil3zw1csvssvgjqd64w1") (yanked #t)))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1qza589qp0qy0z00506l2q3r2nrv6s6v08k1cpj406qwq9299gny")))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.3") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1rd3m2dz3qk20c734hcyjjlvciv4g37idqc5g9a57bxyz7wq4lib")))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.4") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0iv07nz05ynclrb18snmq5ry7y5jgl372qqg0fn43snklb6l8336")))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.5") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0rxa6zyp7faw9gdq66fy02c9dnh1js34kdanr84aflmy1894zkb3") (features (quote (("no_std")))) (yanked #t)))

(define-public crate-xxhash-c-sys-0.8 (crate (name "xxhash-c-sys") (vers "0.8.6") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "13g8cy4w91nwmficbmkrqp40axpwidcna588gzxr47m0ld99jh6j") (features (quote (("no_std"))))))

(define-public crate-xxhash-rs-0.1 (crate (name "xxhash-rs") (vers "0.1.0") (hash "0l19izpwidi7413mjc8g31qfpfsdcmyzy0s45nwyby1j4w06ak9z")))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.0-beta.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1xibinxz4kz0z72vj7fzgs2h4925ylk2ylihrbi2hgq4178wqdfz") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.0-beta.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1fga3x2acfhamv0cp3m0nknfzskzzkdswg3fyr49znd4jb85ipsg") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.0-beta.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0nfhihqxv4pspimv4lvyfsqcqv61pxm14xn0iml51x03sbls64nd") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.0-beta.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0hn5kvd611g1gga9ws70ka1b0spsnm4mf7grgqjljlnsgq8wa7m6") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.0-beta.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0blfh4m46mfzrjm53vq49yx5d9wm6j7xm7vwkrj0rbv1s0p0yha6") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.0-beta.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1qqfw3vxzwnjgws5cv4h5wwkjfpkq5vkasc8a31v8n0v3jvdypmq") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0fkpnsgm4zk2nbsfd4q1d31yj4axm2zir5m4cxl07ahz34aj89kd") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0pf8q4ky10w28rifxbsvbzpvrd9hfq9j64bgfg81ml0fv4b2xfcl") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3")))) (yanked #t)))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1v9dk6shls1rsmidf2dxdi3460bn7ingqgvn5mf7prgnxmdy2xg5") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0bbhci48fj83v08sbhk981xc26ckwb2wpnlzdbhbjcb73i222vw8") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "097gmzmbf47p0k80b8q8xi62rzx4961isq5vhh8xcxrk81xnp8c3") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.5") (deps (list (crate-dep (name "getrandom") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1ryjsyz6g8m4h9b1h6brgcghlhjga1l5fx7xs6w6wa7c9vm18j87") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.6") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0w0dx17w0ydp3n09a1bkh7m8rqwq5gf3zl149cfxfs2ddka72nkk") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.7") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0yz037yrkn0qa0g0r6733ynd1xbw7zvx58v6qylhyi2kv9wb2a4q") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.8") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0q9xl4kxibh61631lw9m7if7pkdvq3pp5ss52zdkxs6rirkhdgjk") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.9") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0wsfw9ni9a9xh65hxq9vgy0isqpglyyfin3864n1is55llmrr4pj") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-rust-0.8 (crate (name "xxhash-rust") (vers "0.8.10") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xxhash-c-sys") (req "^0.8.6") (default-features #t) (kind 2)))) (hash "00zfsfigb6zh0x8aaickkkyd3vyjgnrq36ym04lil7my4lgahzcj") (features (quote (("xxh64") ("xxh32") ("xxh3") ("const_xxh64") ("const_xxh32") ("const_xxh3"))))))

(define-public crate-xxhash-sys-0.1 (crate (name "xxhash-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l44slbnnzawyfalcwd928wx4bqbs827cjriigis7zjrw0d29dv5")))

(define-public crate-xxhash2-0.1 (crate (name "xxhash2") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xxhash-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "10xihhbnxpnn6mzqghsv3nm7dw8p8pnjbksk2jf3fw038f3qhdnx")))

