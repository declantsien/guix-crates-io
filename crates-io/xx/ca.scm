(define-module (crates-io xx ca) #:use-module (crates-io))

(define-public crate-xxcalc-0.1 (crate (name "xxcalc") (vers "0.1.0") (deps (list (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "040b58d30bb156vdxq007kahwcb2m56pbjn6vdc1rpp9ya2s9jns") (yanked #t)))

(define-public crate-xxcalc-0.1 (crate (name "xxcalc") (vers "0.1.1") (deps (list (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0s21jmbw1cnydhvxp5bqadf3v79blyr7s6bml0cac5clx4rwy1iv")))

(define-public crate-xxcalc-0.2 (crate (name "xxcalc") (vers "0.2.0") (deps (list (crate-dep (name "rustyline") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "170mvbg3ws000285889hqddib6nxh2l98awsl1c2q79iaw71ixgk") (features (quote (("interactive" "rustyline"))))))

(define-public crate-xxcalc-0.2 (crate (name "xxcalc") (vers "0.2.1") (deps (list (crate-dep (name "rustyline") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1g6drkcvc2xxhi5i34mh2vg059rhpi2hnwbzrl3cgf321x12a9sc") (features (quote (("interactive" "rustyline"))))))

