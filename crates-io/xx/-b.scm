(define-module (crates-io xx -b) #:use-module (crates-io))

(define-public crate-xx-bloomfilter-0.10 (crate (name "xx-bloomfilter") (vers "0.10.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.4") (default-features #t) (kind 0)))) (hash "17z6azhnbc856kx3zyg9bak2apx5sbvz5r5517vrap1il34s4d21")))

(define-public crate-xx-bloomfilter-0.11 (crate (name "xx-bloomfilter") (vers "0.11.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.4") (default-features #t) (kind 0)))) (hash "0q8mf4q9bccjsblcb0rp1ws0pyxqbly2rbgcr020l0v4siynw3qs")))

(define-public crate-xx-bloomfilter-0.11 (crate (name "xx-bloomfilter") (vers "0.11.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.4") (default-features #t) (kind 0)))) (hash "0ark597qqr915gb9qs7krmnzyw6wzviac578ig2311ipk8m3rd4d")))

