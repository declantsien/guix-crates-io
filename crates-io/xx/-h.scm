(define-module (crates-io xx -h) #:use-module (crates-io))

(define-public crate-xx-hash-0.1 (crate (name "xx-hash") (vers "0.1.0") (hash "1qins3z0hlrzlp2g9k8v0gjanx8my5agba5z9zcq1zzm7kfzlhfh")))

(define-public crate-xx-hash-0.2 (crate (name "xx-hash") (vers "0.2.0") (hash "0322pknl0np8gjlydg2i5dl6b8lrq2w13qphcazynpp40isqm3ng")))

(define-public crate-xx-hash-0.3 (crate (name "xx-hash") (vers "0.3.0") (hash "00l1rgamvjnxk30kfkyf1kacns0cmxlvxj25g6x1n0gqf9ap75qc")))

(define-public crate-xx-hash-0.3 (crate (name "xx-hash") (vers "0.3.1") (hash "1wrnipqcz9x9rf7qbg4dax0i86ymqf6rg5b0g7kpja57wsj057i7")))

