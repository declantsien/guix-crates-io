(define-module (crates-io mj b_) #:use-module (crates-io))

(define-public crate-mjb_gc-0.1 (crate (name "mjb_gc") (vers "0.1.0") (deps (list (crate-dep (name "mjb_gc_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14lnwqng0q8glr1x0z7x9sjzd2xjh46hr57d0d9k8f551l7aa0g1") (features (quote (("trace"))))))

(define-public crate-mjb_gc-0.2 (crate (name "mjb_gc") (vers "0.2.0") (deps (list (crate-dep (name "mjb_gc_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "14ljrdzbhdhgljdbdhkd85cz2g9dia9kxiylh5am5z0alg3v7348") (features (quote (("trace") ("derive" "mjb_gc_derive"))))))

(define-public crate-mjb_gc_derive-0.1 (crate (name "mjb_gc_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "1cmflkgcln9zyw2xrv06qkilqmm0qi720xwfy1y50fjm3pjhxxyl")))

(define-public crate-mjb_gc_derive-0.2 (crate (name "mjb_gc_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "0xw4xwpx6yc7pbg8d9ki0xq1w955sfi4vryj8wi2vvd8xnvmip4d")))

