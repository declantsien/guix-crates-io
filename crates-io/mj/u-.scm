(define-module (crates-io mj u-) #:use-module (crates-io))

(define-public crate-mju-bits-0.1 (crate (name "mju-bits") (vers "0.1.0") (deps (list (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0b4swlg4z6wdrbcr700jq5iigy5kjxfvkp0w7bzz3k1h45yz9wjc")))

(define-public crate-mju-bits-0.2 (crate (name "mju-bits") (vers "0.2.0") (deps (list (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "1i6vd0lz7pzd4xxw3xdajx3ivi18inqava5v8yj7scv544bc2d9z")))

(define-public crate-mju-bits-0.3 (crate (name "mju-bits") (vers "0.3.0") (deps (list (crate-dep (name "typenum") (req "^1.11.2") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "1x0d20v02w0j2i0v8xk2dycdkrgs0dbjizd0014rvxvbpkddd2gn")))

