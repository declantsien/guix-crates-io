(define-module (crates-io ye eh) #:use-module (crates-io))

(define-public crate-yeehaw-0.0.1 (crate (name "yeehaw") (vers "0.0.1") (hash "19xr6yqp9ygqysrqy0r74rv6vxyyn4d4m5xaykx036mlfbxvxqlz") (yanked #t)))

(define-public crate-yeehaw-0.0.2 (crate (name "yeehaw") (vers "0.0.2") (hash "1l23mgbb7dfgjc6zsbd17pv9gd8rcwxcx5gvkaqk74cbhwz9dazg") (yanked #t)))

