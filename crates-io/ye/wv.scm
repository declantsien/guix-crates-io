(define-module (crates-io ye wv) #:use-module (crates-io))

(define-public crate-yewv-0.1 (crate (name "yewv") (vers "0.1.0") (deps (list (crate-dep (name "yew") (req "^0.19") (default-features #t) (kind 0)))) (hash "13hd0wfqhgjjhw3wz0ya8qfzis0h7nppw1gym5a760lnw14dsgfm") (rust-version "1.56.0")))

(define-public crate-yewv-0.1 (crate (name "yewv") (vers "0.1.1") (deps (list (crate-dep (name "yew") (req "^0.19") (default-features #t) (kind 0)))) (hash "1dp4axpazhjpbckmlfd17d4345y1rbzccrs77qqzxqcxins40amg")))

(define-public crate-yewv-0.2 (crate (name "yewv") (vers "0.2.0") (deps (list (crate-dep (name "yew") (req "^0.19") (default-features #t) (kind 0)))) (hash "139dibbnmkfaw5dn5a0l6kzibg468zhm5sm3xv56n9n6n5n535sk")))

(define-public crate-yewv-0.2 (crate (name "yewv") (vers "0.2.1") (deps (list (crate-dep (name "gloo") (req "^0.6") (features (quote ("futures"))) (default-features #t) (kind 2)) (crate-dep (name "gloo-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yew") (req "^0.19") (default-features #t) (kind 0)))) (hash "138gxx7aa8zla43k0fkc9r52banj6wkqzzlxi3qan4m51s4kphi9")))

(define-public crate-yewv-0.2 (crate (name "yewv") (vers "0.2.2") (deps (list (crate-dep (name "gloo") (req "^0.6") (features (quote ("futures"))) (default-features #t) (kind 2)) (crate-dep (name "gloo-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yew") (req "^0.19") (default-features #t) (kind 0)))) (hash "1sxssy68iy4zya0m9azrxfzkmrx6gdqyss4nzbi88ack3dafv1fz")))

(define-public crate-yewv-0.2 (crate (name "yewv") (vers "0.2.3") (deps (list (crate-dep (name "gloo") (req "^0.6") (features (quote ("futures"))) (default-features #t) (kind 2)) (crate-dep (name "gloo-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yew") (req "^0.19") (default-features #t) (kind 0)))) (hash "1q7xpkxa4fnixa8qfq4yjkyjik932l4csjf098m5mw8gxc9fr4wf")))

