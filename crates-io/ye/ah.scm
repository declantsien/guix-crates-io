(define-module (crates-io ye ah) #:use-module (crates-io))

(define-public crate-yeah-0.1 (crate (name "yeah") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "0h7blzqihqh4zvdq3pwg56gw7bpy8xds04glwv4b2ri7mjxaqk5g") (yanked #t)))

(define-public crate-yeah-0.1 (crate (name "yeah") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "0rz2l55qra80fvqxc465rks7h8bybp51j7fljj0d1xmz6rx4cfgp") (yanked #t)))

(define-public crate-yeah-0.2 (crate (name "yeah") (vers "0.2.0") (deps (list (crate-dep (name "pico-args") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "17sk2k60zhfhv66marvmicrxja7fnsdp011ma9vs7bs7a225n1gh") (yanked #t)))

(define-public crate-yeah-0.2 (crate (name "yeah") (vers "0.2.1") (deps (list (crate-dep (name "pico-args") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0j2zw6pxh5k972dh2yvvbmqg6qa6qvizvmbympnzxsylwxgy5r6h")))

(define-public crate-yeah-0.2 (crate (name "yeah") (vers "0.2.2") (deps (list (crate-dep (name "pico-args") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "182gbb5srsdl4fsii9yi0wmcsv4py5zlvk6wwbiy22wv7m71indd")))

