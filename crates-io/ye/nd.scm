(define-module (crates-io ye nd) #:use-module (crates-io))

(define-public crate-yendor_lib-0.1 (crate (name "yendor_lib") (vers "0.1.0") (hash "09l9mz75b6n9k4wrxgqjjg0n776448zh3ciz3r6qvyq86887blm7") (yanked #t)))

(define-public crate-yendor_lib-0.0.0 (crate (name "yendor_lib") (vers "0.0.0") (hash "0fnzxc5vakgksx89yqiygycrqls84an9123qj58d0ma062r8jwxb")))

