(define-module (crates-io ye ar) #:use-module (crates-io))

(define-public crate-year-helper-0.1 (crate (name "year-helper") (vers "0.1.0") (hash "1sr3yjr98qhfdnj21lx8ipkjrkdjv43kqvxmqzvig9064r0g6qan")))

(define-public crate-year-helper-0.1 (crate (name "year-helper") (vers "0.1.1") (hash "0b9mv76in0m2as39jr1lapz5yj49546wh7hr1qwvc9a9lc4x67k3")))

(define-public crate-year-helper-0.2 (crate (name "year-helper") (vers "0.2.0") (hash "1dvb8dgb4gipzwsxz2vlir102n8jwv8ddw6xxqjf9xyl58d7dnvy")))

(define-public crate-year-helper-0.2 (crate (name "year-helper") (vers "0.2.1") (hash "1wq5ilsibh726zdw4p9772h2czska7gv4szi5cx1gr2c7ikv5kjx")))

(define-public crate-year_update-1 (crate (name "year_update") (vers "1.0.0") (hash "0dgs0p3wl90k53mnq1kxm60mkfbhjqidcxpqk4pdfbkqfzimg7lh")))

(define-public crate-year_update-1 (crate (name "year_update") (vers "1.1.0") (hash "19yz1clal7gmrsh3lj1icxyc932s2l8m5ybxkr5c4hdmy546glwh")))

(define-public crate-year_update-1 (crate (name "year_update") (vers "1.1.1") (hash "0rm731accac5r6fhmj0l7g7chs0ls91hmilsvhpykz9688zw3lbg")))

(define-public crate-year_update-1 (crate (name "year_update") (vers "1.2.0") (hash "0r5vn6qg20yjynkgkq1mygs552picjng6ibgb6z1mxk00qqar0mr")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0iy865mb9b8yqaqj77s3063z25c0vz22iv16gbzd1k3kqbind3kn")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "10pa48qy33s6nymhd5r6mhwnk7czhlvi3l08mx996l7j62hl91d4")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "049xq9hpqwm01a9gsgyimpw971y90zgxnyh274sp1ydzwvk2svdl")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0wma4lf29hw6k645chkh75ml2dlni1znbl0yf5pr75cxmz1mcjga")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "07yi6qmn8vshrhbsdxkwiw6fn9s46fk79mjswvbc722r5j7jh93m")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1z4nrfix3amgvynklyaqb440scff5cxyg8xjzfsaqynnhfsdfpl7")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "19zynk47lrbnpwzs0hxa53mcw7hjgly7rd4fb4kx4ipfcrqnbfkk")))

(define-public crate-yearfrac-0.1 (crate (name "yearfrac") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "utoipa") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1hxvxcmfd1rdyvlmxy7vbi9k97zda8c9s8vwl1angvqdr5gqzng9") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde") ("openapi" "dep:utoipa"))))))

(define-public crate-yearfrac-0.2 (crate (name "yearfrac") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "utoipa") (req "^3.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1yqf2ckgfdskdn2j08cjqcy860x6vnq2zipwpy4vhx49i7w5h3hv") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde") ("openapi" "dep:utoipa"))))))

(define-public crate-yearly-version-0.0.0 (crate (name "yearly-version") (vers "0.0.0") (deps (list (crate-dep (name "semver") (req "^1.0.22") (default-features #t) (kind 2)))) (hash "0ihjx3bsxr8yhplzjx7v17s40gjqn4gnx039l3nikdfdarln9gd6") (features (quote (("default"))))))

(define-public crate-yearly-version-0.0.1 (crate (name "yearly-version") (vers "0.0.1") (deps (list (crate-dep (name "semver") (req "^1.0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (optional #t) (default-features #t) (kind 0)))) (hash "1nf4f5hy880a3ix25r62hhzj8338r8rfzga84dahii6yfjsnpa36") (features (quote (("default"))))))

(define-public crate-yearly-version-0.0.2 (crate (name "yearly-version") (vers "0.0.2") (deps (list (crate-dep (name "schemars") (req "^0.8.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (optional #t) (default-features #t) (kind 0)))) (hash "1ca3jwsknqwsrfd6s7fjgk3fpzsk3ka4sx5y2r8jn33j45ks4qy7") (features (quote (("default"))))))

(define-public crate-yearly-version-0.0.4 (crate (name "yearly-version") (vers "0.0.4") (deps (list (crate-dep (name "schemars") (req "^0.8.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (optional #t) (default-features #t) (kind 0)))) (hash "117cbx23ijy7irnd1n7iyqfzwvawhj81wiq3mw30a33llwxj15jq") (features (quote (("default"))))))

(define-public crate-yearly-version-0.0.5 (crate (name "yearly-version") (vers "0.0.5") (deps (list (crate-dep (name "schemars") (req "^0.8.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (optional #t) (default-features #t) (kind 0)))) (hash "15ks3r14d4dnghvqdk6pbl1jhcfj47f2qdsvvhcn0j0hi8j1y2z2") (features (quote (("default"))))))

(define-public crate-yearly-version-0.0.6 (crate (name "yearly-version") (vers "0.0.6") (deps (list (crate-dep (name "schemars") (req "^0.8.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (optional #t) (default-features #t) (kind 0)))) (hash "10cxijkjvqgkv6gb8jwc3yx6qan5wdd49jc5nndx8g8z5zzy1dwz") (features (quote (("default"))))))

(define-public crate-yearsfx-0.1 (crate (name "yearsfx") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0br0pjsfzb0xrw9i1ny7447715df3dh4v5pili8ak1s1wsqqb3mx")))

