(define-module (crates-io ye te) #:use-module (crates-io))

(define-public crate-yeter-0.1 (crate (name "yeter") (vers "0.1.0") (hash "12ia222i22yiqnvwbdifz84xdh6wwwxyb112bvbx9wj9fiflj41s")))

(define-public crate-yeter-0.2 (crate (name "yeter") (vers "0.2.0") (hash "1ywqg6wf0z6g3byfhybrd2q1kwsmhka8m2dx89nh6s9j24qss3gx")))

(define-public crate-yeter-0.3 (crate (name "yeter") (vers "0.3.0") (deps (list (crate-dep (name "state") (req "^0.5") (default-features #t) (kind 0)))) (hash "1w948b3w15vp4fkia9p70fq1xmlykk1ijga14v6avsqai2rn4b92")))

(define-public crate-yeter-0.4 (crate (name "yeter") (vers "0.4.0") (deps (list (crate-dep (name "state") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yeter-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "014bsli7vrzqxk765lfjlm2plsd2qp3nqlnqlyzpafklhfbfc6l4")))

(define-public crate-yeter-0.5 (crate (name "yeter") (vers "0.5.0") (deps (list (crate-dep (name "state") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yeter-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0b3c68px5avran6im3ccn1fsmwk1vdnn2nnqa4k0cyn8473d6dz6")))

(define-public crate-yeter-0.6 (crate (name "yeter") (vers "0.6.0") (deps (list (crate-dep (name "anymap2") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^1.1.7") (default-features #t) (kind 0)) (crate-dep (name "yeter-macros") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1wgk2lyglcd5dlwl8nrqzfkj1nlqj0jqna0f3n050747m8rcf21y")))

(define-public crate-yeter-macros-0.1 (crate (name "yeter-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0waglk8a83w7df8kz5j7cvy7bnxa97kr7gchbvxk9qgw6vwx06lp")))

(define-public crate-yeter-macros-0.4 (crate (name "yeter-macros") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a4dk26bxd9z62vmm6q1j1zmr3rzrn2rpi3rvidinfb34pf7rb8g")))

(define-public crate-yeter-macros-0.5 (crate (name "yeter-macros") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i32kg6bbr6g5friw1fsfrq1c9bfjn5iz6vwnw7a1sc95bj4py4w")))

