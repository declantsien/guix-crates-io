(define-module (crates-io ye nc) #:use-module (crates-io))

(define-public crate-yenc-0.0.1 (crate (name "yenc") (vers "0.0.1") (hash "0lp78x5xsn5qpbs538rhbzawy5v81ha3ajp0q7pk8pzx49cjyhqf")))

(define-public crate-yenc-0.0.2 (crate (name "yenc") (vers "0.0.2") (hash "0rgnd2h90qkiw3qm78w55nrs9an80h6a4mn30gpwais6d5rf4yrj")))

(define-public crate-yenc-0.0.3 (crate (name "yenc") (vers "0.0.3") (hash "0jgfhmnqckvhbi7pbdilfq45ps8df68mpkh4ha6s64sxihc9yls5")))

(define-public crate-yenc-0.0.4 (crate (name "yenc") (vers "0.0.4") (hash "1dl45m807h6c7mf6lyb2574arlilrfflplbbkyy3dlhymw6vx40z")))

(define-public crate-yenc-0.1 (crate (name "yenc") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0rcjhpcd632wfqnw2v8v746yvw91xqyqin0zkxi9izzczlgfwdgf")))

(define-public crate-yenc-0.1 (crate (name "yenc") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "031d7q0wfc1miilbx309i5a3fibi2a672dbw57n5apb23palnbb1")))

(define-public crate-yenc-0.2 (crate (name "yenc") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0314k64adgczqx95xlpk06jyvggpdnxff0c2p07p1gi66r8l7f8j")))

(define-public crate-yenc-0.2 (crate (name "yenc") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "121dj0cc406gzqpjr40637mx0xyaj8n3n6vh29jqmx5nh47vm2ih")))

(define-public crate-yenc-0.2 (crate (name "yenc") (vers "0.2.2") (deps (list (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "08aba6idl26yl876c2hs0n7w16bzxzf1igb6k3d2pv7xs2dqvcgj")))

