(define-module (crates-io ye t_) #:use-module (crates-io))

(define-public crate-yet_another_guessing_game-0.1 (crate (name "yet_another_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "16qwikac1dgllfmdpw1vnanp2nbk7gsaxxg1j1iasqp8smpsj2zi")))

