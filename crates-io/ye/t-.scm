(define-module (crates-io ye t-) #:use-module (crates-io))

(define-public crate-yet-another-hello-world-0.1 (crate (name "yet-another-hello-world") (vers "0.1.0") (hash "0fn09zdrdblbfkiagqwr3kh0n0v52pms1ljk64crbinkzmifb392")))

(define-public crate-yet-another-md5-1 (crate (name "yet-another-md5") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "simplelog") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "07mhjvz87fwf9fdrqxcrkygy1k12vykc7zd0mz97wkr2979cig5p")))

(define-public crate-yet-another-md5-2 (crate (name "yet-another-md5") (vers "2.0.0") (deps (list (crate-dep (name "ctor") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "19vyqmls8nn3dsc375az8pj8908krcccni1vjrgn0x1rkj2f1adv")))

