(define-module (crates-io ev ap) #:use-module (crates-io))

(define-public crate-evaporate-0.2 (crate (name "evaporate") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.27") (default-features #t) (kind 0)))) (hash "0dsaa2f6z7kq5v2llazys4sybwmr5njqrdx122acsix0jzaz4r81")))

(define-public crate-evaporust-0.1 (crate (name "evaporust") (vers "0.1.0") (hash "1h0r2m5gbkbwyz6fhxf0m8z9nz8pxa730y709agpqbrqgkyk0l8k")))

