(define-module (crates-io ev sc) #:use-module (crates-io))

(define-public crate-evscript-0.1 (crate (name "evscript") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1i1lg3nahlp6yalzy1l3wflv82f61q49v5gx0jqxrjli5ymb4c93")))

