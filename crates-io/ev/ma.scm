(define-module (crates-io ev ma) #:use-module (crates-io))

(define-public crate-evmabi-0.0.0 (crate (name "evmabi") (vers "0.0.0") (hash "1w2ybmxfrssk9azim0b8j6cmdidp3vgyzlbv8dj3v2mjq8qpgidz")))

(define-public crate-evmap-0.1 (crate (name "evmap") (vers "0.1.0") (hash "05pf7h9ipq6wlr69yv1mgyhlhf4f1w5hz2hhlyq7qfhqi66njydl")))

(define-public crate-evmap-0.1 (crate (name "evmap") (vers "0.1.1") (hash "1w2m48zr48mvsvjiigawkb0qld70jml12m5lzv2hsgwgpkmqdyjh")))

(define-public crate-evmap-0.1 (crate (name "evmap") (vers "0.1.2") (hash "1fd2gp40hcbpvny2qd9wwr5q64bb3nsvxajkc8bk5hxjsymainax")))

(define-public crate-evmap-0.1 (crate (name "evmap") (vers "0.1.3") (hash "0wvq13m09f9cg5l6kk9zqpbf4zwj81q9l14x16yxgm9hwmhalyaa")))

(define-public crate-evmap-0.2 (crate (name "evmap") (vers "0.2.0") (hash "0fb7p0l9i2gfnpsw8xc0ncq9ppw4pj2v549fmbpr47w5gksl6kza")))

(define-public crate-evmap-0.3 (crate (name "evmap") (vers "0.3.0") (hash "13a0zp24y3hl2pddfa2i7ggprbryr8rly8ic6cs5phsvi7760jmh")))

(define-public crate-evmap-0.3 (crate (name "evmap") (vers "0.3.1") (hash "1m91jp1mbkymqd5mksy5y1p9kb34j5xfi6jiv82zz1nxn6fwxnid")))

(define-public crate-evmap-0.4 (crate (name "evmap") (vers "0.4.0") (hash "169fhh1h75is3bcan4p9adhkxxg0n2h3dg42mriw1mnp68yyspna")))

(define-public crate-evmap-0.5 (crate (name "evmap") (vers "0.5.0") (hash "0i4va2k0rb4cvaqm2f9xjz7g0yn7mdnnzl51q4klksn862wxiiwx")))

(define-public crate-evmap-0.6 (crate (name "evmap") (vers "0.6.0") (hash "1rabphcfn7x2h5zgpwnnzzl4czr9g3qldf14c2kznw512s02hzix")))

(define-public crate-evmap-0.6 (crate (name "evmap") (vers "0.6.3") (hash "139136nx5n5q7kkcidh50131l2zs3ly1mdga6s9wv1if0a9givf4")))

(define-public crate-evmap-0.6 (crate (name "evmap") (vers "0.6.4") (hash "0acq9dd73rhkmf3730d3l511x2rr5hncj5byc456z9ljmbianrsv")))

(define-public crate-evmap-0.6 (crate (name "evmap") (vers "0.6.5") (hash "1kqpy6jdhv5afvk5h5s9k6klm16s4k4ls7bmpz256gmg79ilw5vm")))

(define-public crate-evmap-1 (crate (name "evmap") (vers "1.0.0") (hash "03pmxrvfz11wvj6b7yp0lc3a5l5qs1jpri54xhzpwxvx3fds9bbb")))

(define-public crate-evmap-1 (crate (name "evmap") (vers "1.0.1") (hash "104cc0140vrgz9b6wx14qj8lxvqf0gd26mg8sgwadkfk27g9j1vp")))

(define-public crate-evmap-1 (crate (name "evmap") (vers "1.0.2") (hash "0v8mn6s8migxvq26rgw8hyfww5j0zcb85pvsd3xjninqdwzgz3lv")))

(define-public crate-evmap-2 (crate (name "evmap") (vers "2.0.0") (hash "02zz7k902hv5bzfsfnjq0w8nj8hifgj61fl32v2m99qxx02r2pqy")))

(define-public crate-evmap-2 (crate (name "evmap") (vers "2.0.1") (hash "1swp2dz4pf8jzisn21al4m5xw38sgrdac6wzrrmcnxn3vpv3c1nw")))

(define-public crate-evmap-2 (crate (name "evmap") (vers "2.0.2") (hash "1mynz54b0wipf23l9d71icmyk0fmkhsl53pl4a18lg7bqx8dx33s")))

(define-public crate-evmap-3 (crate (name "evmap") (vers "3.0.0") (hash "12m9wiwfkn561qdisd69jg0klhgba51a63h0kz6nnk8kka2954x5")))

(define-public crate-evmap-3 (crate (name "evmap") (vers "3.0.1") (hash "1zxxjj1g7j52wckzcpy8zq356pl7fbc5jnii8s1njca2qwcjybah")))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.0.0") (hash "15mdi22lasg8daq7s9g65dnnf95ir30cjwqvhyq9ihi0zzrc8xbn")))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.0.1") (hash "04sqhy4nxkhr6ihw3h6kfqgf4dssk0ghs472s7giiyfqba4alxws")))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "14h0ra1jhdbfcv8lvvy3kr0qqyji7h9hqwkkav33fs98pkdiaxjh") (features (quote (("nightly_smallvec" "smallvec/union") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "0pr9jxq2axrmafpq94gwdad7lqw6yg2j4dl2b4hdndrizvkvbfxf") (features (quote (("nightly_smallvec" "smallvec/union") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "02k86rxgsd3x912n6j8ay4zsdq6gw2qlhcv313p2qff4sqzylqsy") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.2.1") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "0g5c0vi7a7fv1d9cc1z47n1s1g58hwxfp95vil31f54pydjyj8a5") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec")))) (yanked #t)))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.2.2") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "0nfrsq0l33bflr38c5rccvh2bd995291bzryfdrfrks7b7lyg9aj") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4 (crate (name "evmap") (vers "4.2.3") (deps (list (crate-dep (name "hashbrown") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "1i1wfq13d8bcrrp1270d8l25m3yis1q0xll46sk041s38y7pmh8q") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5 (crate (name "evmap") (vers "5.0.0") (deps (list (crate-dep (name "hashbrown") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "1kaxsm2wjvmdzny0sw32q07m3v5vb9p8npq8dz8a2s4yrqivvj21") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5 (crate (name "evmap") (vers "5.0.1") (deps (list (crate-dep (name "hashbrown") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "0cnrpph9l4yw9cn9psk9xzva0sbss466957pbmzp9l5rny50vvk6") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5 (crate (name "evmap") (vers "5.0.2") (deps (list (crate-dep (name "hashbrown") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "1s81cg5dwwmd7k4mp3whzbagirys7m5rjbdf024qrs602zx1yanz") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5 (crate (name "evmap") (vers "5.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "1jivls4y9wiljcvsd6g6jal1vm3hfnx1dhv91gdbq15qq8rcjwbb") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5 (crate (name "evmap") (vers "5.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "0hicdsikdnn4z120dhngafwnb921yq8zrqwkmw2ncihc36plm032") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-6 (crate (name "evmap") (vers "6.0.0") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "1rlwvjka82jqkgc83imp8fvh49z1xf2xwn33j87jr6jqy1051s7a") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-6 (crate (name "evmap") (vers "6.0.1") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "03rqfb1rp824dgq3pxz7v9lbc8jxp18m7yh2hwgwk0lv9h3n1nvg") (features (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-7 (crate (name "evmap") (vers "7.0.0") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "0jzp19l5b5zh6nrdl6r3prmlz434wxzs8jkvwk1i5rlc4kn7c5p7") (features (quote (("default" "smallvec"))))))

(define-public crate-evmap-7 (crate (name "evmap") (vers "7.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "1cvshrcc2xsx0f3yp5k7ycv6wfl249w71d9wj9pnhhcjgbhsq23y") (features (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-7 (crate (name "evmap") (vers "7.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)))) (hash "14r7ws3jil8b0rr8m34cf18krg6hli5nvc5l0ky1w6kdrlgwhrjg") (features (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-7 (crate (name "evmap") (vers "7.1.2") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "09f2apllb5893w2cdrd9rbp1j7m52hml6jc5xalaz7dv049y65a1") (features (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-7 (crate (name "evmap") (vers "7.1.3") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "15azxbz0phrd2ww6vba1yx76ngc00mkdxxpkq9f4didzdc4wx2am") (features (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-8 (crate (name "evmap") (vers "8.0.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1037689c7ihxwld23vynzrd25770wmgw7b6h26k5cqndjf9cn4hq") (features (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-8 (crate (name "evmap") (vers "8.0.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0992mdb4wn9smrlkc92kj73imin2ycxl84n3xzypld37ks9zxp8m") (features (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-9 (crate (name "evmap") (vers "9.0.0-beta.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1lmh7s3jg9qym0337iw063qv73yx2hqyh1sgs7abnacplmib8m5v") (features (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-9 (crate (name "evmap") (vers "9.0.0-beta.2") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16wjph2n8ckvqg1i0pxhd70dplxafvxlf5h1xrflf236482k61g1") (features (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-9 (crate (name "evmap") (vers "9.0.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1f8nfal8mzpkrk90m83lnqijngvmhq909nbvqa25xx7pr0yzqy97") (features (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-9 (crate (name "evmap") (vers "9.0.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "09n7whfl0s29n4jcx3mav2h33yy9lvam36l65bcnywij3fki7bhw") (features (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-10 (crate (name "evmap") (vers "10.0.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "19nr17zyfrrjiza34c6qwbdrs4wnj7n7c8gb432v58qb8ka4yr6f") (features (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-10 (crate (name "evmap") (vers "10.0.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1i8j7qrfm1ya3q559wjjga2f75pg4f30vxdi4f9v2cpvxj73lw40") (features (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-10 (crate (name "evmap") (vers "10.0.2") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13amvib52g8fhxf8lmd52w3qyhcvf9x1xr86xg13szgrhdma0gkf") (features (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-11 (crate (name "evmap") (vers "11.0.0-alpha.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1l1vg6cljzisy9si7cw3j4j8wc7p369ixhhh9ybh4jgzb8hm9lar") (features (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11 (crate (name "evmap") (vers "11.0.0-alpha.2") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "left-right") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1qkv4cv2lhvq9jqlc5zyakizr04cpz4lf6mkrsbxhwn0gvp34i68") (features (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11 (crate (name "evmap") (vers "11.0.0-alpha.3") (deps (list (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "left-right") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0gjlvjx1dc0l82m0vn2y3zxvs4zpmdf68y2wrcxyknib0cckqb5y") (features (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11 (crate (name "evmap") (vers "11.0.0-alpha.4") (deps (list (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "left-right") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0z6f6agnw36l7cq456j9wgkg8ad987c11x44hg3cmg2sal5s9ylc") (features (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11 (crate (name "evmap") (vers "11.0.0-alpha.5") (deps (list (crate-dep (name "hashbag") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "left-right") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1glsjpd52vr97xmncvya4dmnbyvq12046121c1bg1cggbn16wbxm") (features (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11 (crate (name "evmap") (vers "11.0.0-alpha.6") (deps (list (crate-dep (name "hashbag") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap-amortized") (req "^1.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "left-right") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0hj5631ygdz2kv7dc7x1xa2jhdppc0rhgxfhrmb8d9fajzhk31d3") (features (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default") ("amortize" "indexmap-amortized" "hashbag/amortize"))))))

(define-public crate-evmap-11 (crate (name "evmap") (vers "11.0.0-alpha.7") (deps (list (crate-dep (name "hashbag") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap-amortized") (req "^1.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "left-right") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0djhn60ca0xhayrfnknyz1az09jkzl4iis7mxq9nfbjgg3ijhym4") (features (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default") ("amortize" "indexmap-amortized" "hashbag/amortize"))))))

(define-public crate-evmap-derive-0.1 (crate (name "evmap-derive") (vers "0.1.0") (deps (list (crate-dep (name "evmap") (req "^10") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.24") (default-features #t) (kind 2)))) (hash "0x535n3jsa9q6d2ml6a7zjbcbn7zp8hc02vyyrqdd3s1ya08sky4")))

(define-public crate-evmap-derive-0.2 (crate (name "evmap-derive") (vers "0.2.0") (deps (list (crate-dep (name "evmap") (req "^11.0.0-alpha.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.24") (default-features #t) (kind 2)))) (hash "06snhaczi0g05l2jr8qqmp91rx76k9704dv8wzyd4zjvf0vijark")))

(define-public crate-evmasm-0.1 (crate (name "evmasm") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "04i866f4x3c6w2wshrh7ydmi076182v1y98fnxa9cn0ygbm81rk4") (yanked #t)))

(define-public crate-evmasm-0.1 (crate (name "evmasm") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "104kq9p1d7sk65x6ziydfssssifxz8xc4hvlhv3dhghijn25794d")))

(define-public crate-evmasm-0.1 (crate (name "evmasm") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0wakjh2ga7y90ichb0bz3wdmri0nxnl8hpczwnk8kvz5kf4gyx21")))

(define-public crate-evmasm-0.1 (crate (name "evmasm") (vers "0.1.3") (deps (list (crate-dep (name "hex") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0y30qm8l2ccn6zl6sbgvbfaf78zmpfz2h2hs7w3cja2ql6a87wlw")))

