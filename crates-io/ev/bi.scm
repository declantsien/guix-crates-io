(define-module (crates-io ev bi) #:use-module (crates-io))

(define-public crate-evbindkeys-0.1 (crate (name "evbindkeys") (vers "0.1.0") (deps (list (crate-dep (name "evdev-rs") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1zlz28gn7gg3ic4zmjz6zcsgqxjvi89ib6mkk2aiyg4pb5x6pm7a")))

