(define-module (crates-io ev an) #:use-module (crates-io))

(define-public crate-evan-test-pallet-3 (crate (name "evan-test-pallet") (vers "3.0.0") (deps (list (crate-dep (name "codec") (req "^2.0.0") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "frame-support") (req "^3.0.0") (kind 0)) (crate-dep (name "frame-system") (req "^3.0.0") (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (default-features #t) (kind 2)) (crate-dep (name "sp-core") (req "^3.0.0") (kind 2)) (crate-dep (name "sp-io") (req "^3.0.0") (kind 2)) (crate-dep (name "sp-runtime") (req "^3.0.0") (kind 2)))) (hash "0miisq82wva4mw5xiy3b2v0bpaffqz4jkdz7rscn967kn8ki7sps") (features (quote (("std" "codec/std" "frame-support/std" "frame-system/std") ("default" "std"))))))

(define-public crate-evan_fibonacci-0.1 (crate (name "evan_fibonacci") (vers "0.1.0") (hash "1dxn6zv35mdc6zzsy8jar79wn8rsa5504f8hjpl47zf2xr2cm0mh") (yanked #t)))

(define-public crate-evan_toy-0.1 (crate (name "evan_toy") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0b72czmd8lksjmcmldpws7zpmqff02qjwm7dbmcknwqvysnn4zdx")))

