(define-module (crates-io ev eg) #:use-module (crates-io))

(define-public crate-evegfx-0.4 (crate (name "evegfx") (vers "0.4.0") (deps (list (crate-dep (name "evegfx-macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (kind 0)))) (hash "0bwwrkb8cannsi1k5ra8nicbb5rjsliaxfc2qscrinrfl4rsjzsp")))

(define-public crate-evegfx-0.6 (crate (name "evegfx") (vers "0.6.0") (deps (list (crate-dep (name "evegfx-macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (kind 0)))) (hash "1hi24xb70yz0398yqavsnrnc7npcplnb8imn6hhgz1mbj44m7cw1")))

(define-public crate-evegfx-cli-0.5 (crate (name "evegfx-cli") (vers "0.5.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "evegfx") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "evegfx-spidriver") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serial-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serial-embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spidriver") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06j1a2am7nqclb51bgy2fqw3n9c22n56hdjb0inlhav4g5w3hf66")))

(define-public crate-evegfx-cli-0.6 (crate (name "evegfx-cli") (vers "0.6.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "evegfx") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "evegfx-spidriver") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serial-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serial-embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spidriver") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b7381fw33ldsg2zzkaizx5mxma2gqcxk3qaakh0xyjbbrbm2900")))

(define-public crate-evegfx-hal-0.4 (crate (name "evegfx-hal") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "evegfx") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qgg5na99k282mibg1ihdiplhcaqyab6q34vwdl3kf2g5gdv6a9b")))

(define-public crate-evegfx-hal-0.6 (crate (name "evegfx-hal") (vers "0.6.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "evegfx") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1w2nnbanl1l6gqhhkczxazx1yq1l7fbg8ycw6q1k7jp2srfjnf14")))

(define-public crate-evegfx-macros-0.3 (crate (name "evegfx-macros") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gg0s06w5sf7vsfxiywn4f6gx1mh66ch1dv0igffivi70lsmbq3w")))

(define-public crate-evegfx-spidriver-0.4 (crate (name "evegfx-spidriver") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "evegfx") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "evegfx-hal") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "spidriver") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1nldb0kff2xbbljkz5fda1rh8kizbv2l34hw2kjhfgak524kgv5b")))

(define-public crate-evegfx-spidriver-0.6 (crate (name "evegfx-spidriver") (vers "0.6.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "evegfx") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "evegfx-hal") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "spidriver") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1c9lvbqfb380bfg0gzhabd5nw0hrmqx4a42c3k7bxv9r1cn4rwzp")))

