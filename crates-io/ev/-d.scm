(define-module (crates-io ev -d) #:use-module (crates-io))

(define-public crate-ev-dice-0.4 (crate (name "ev-dice") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "0km4yzdhg4al4d49nxpf0sb2if5xa5m73z9swz6q4n4c7idpg9kb")))

(define-public crate-ev-dice-0.4 (crate (name "ev-dice") (vers "0.4.1") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "1bqk5v9iyfmyfv8vvqpr7y2avd2072mb9jk90h9nnz8svhbdkfgn")))

(define-public crate-ev-dice-0.5 (crate (name "ev-dice") (vers "0.5.0") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0592avigv7cm35ay43gigwiardf4wlhqh63jihnrpxkpva5gvq8v")))

(define-public crate-ev-dice-0.5 (crate (name "ev-dice") (vers "0.5.1") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1qhpxwf9ag8x22m7l1pqziv7kkryflg3x1yqk6k433x3xsyjrj3i")))

