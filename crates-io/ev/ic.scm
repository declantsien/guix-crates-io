(define-module (crates-io ev ic) #:use-module (crates-io))

(define-public crate-evic-0.1 (crate (name "evic") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "173z1gnakkys57frchz08389sh02339wp76iwll4bjk5v7rs6s5c")))

(define-public crate-evic-0.1 (crate (name "evic") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1hs31z0xc719vygr92d60v71raz408bsj1pbc18w7ssvjydip141")))

(define-public crate-evicting_cache_map-0.3 (crate (name "evicting_cache_map") (vers "0.3.1") (deps (list (crate-dep (name "ordered_hash_map") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1q3w2kx0r3fzg0bcl3cqrpgr2k56wy564dhk2g9glvpb4h99v59g") (rust-version "1.65.0")))

(define-public crate-evicting_cache_map-0.4 (crate (name "evicting_cache_map") (vers "0.4.0") (deps (list (crate-dep (name "ordered_hash_map") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0af73zghhi9mpfdv58d3a05k69i5z473fl4qxw0zf8ssvjjnzaq4") (rust-version "1.65.0")))

