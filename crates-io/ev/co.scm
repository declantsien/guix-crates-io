(define-module (crates-io ev co) #:use-module (crates-io))

(define-public crate-evco-0.2 (crate (name "evco") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0.114") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "05zv1097gvrkhzybxhbcym09dv878qq3rxskwyqpyqrjq0c73hrp") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-evco-0.2 (crate (name "evco") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0rsxyyjx8s9f10dq4z8axk7hckjv21cy7flb46257952mjwcpxgc") (features (quote (("dev" "clippy") ("default"))))))

