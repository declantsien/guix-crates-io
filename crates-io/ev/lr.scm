(define-module (crates-io ev lr) #:use-module (crates-io))

(define-public crate-evlru-0.0.1 (crate (name "evlru") (vers "0.0.1-beta.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)))) (hash "1sm1lvfx998ap10cj9f35qdan64jlh8nq85yr5f35f11hi6ywbwr") (yanked #t)))

(define-public crate-evlru-0.0.1 (crate (name "evlru") (vers "0.0.1-beta.2") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "1v63rjich4wl8qma4pvb51915337zdbd8c6p6960922gjsq695jp") (yanked #t)))

(define-public crate-evlru-0.0.1 (crate (name "evlru") (vers "0.0.1-beta.3") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "0vldp7smcldml4ykr8pbicw376hdlcgwfc6gzc4j6g3icrg009mf") (yanked #t)))

(define-public crate-evlru-0.0.1 (crate (name "evlru") (vers "0.0.1-beta.4") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "1svsly71zrsj80srlyyhdaipfqji364sn7rm8byk9injy6d3qfr3") (yanked #t)))

(define-public crate-evlru-0.0.1 (crate (name "evlru") (vers "0.0.1-beta.5") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "1kchdfcn2spwf8pmb2fpc33bas6fibrha98f7r60qqdvf70084n7")))

(define-public crate-evlru-0.0.1 (crate (name "evlru") (vers "0.0.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "153dnldnbrcjifrvhjgyzr583w0h07mfqb16hq86cjkgkarrfhi9")))

(define-public crate-evlru-0.0.2 (crate (name "evlru") (vers "0.0.2") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1wkq1x56vgqhsg1zrxxv2pdxgmxqp1yf01808p3cmb7rf4yxwzh4")))

(define-public crate-evlru-0.1 (crate (name "evlru") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1j9pjx3dkjn5xx39g21axs8fgbzsggb8wmxz7dvzanxgdcpq42a0")))

(define-public crate-evlru-0.1 (crate (name "evlru") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "evmap") (req "^10.0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6") (default-features #t) (kind 0)))) (hash "14rwy44h6jl2qp4xc1hy4c0na1a2a28cn505rsk3ivb61lqlwil7")))

