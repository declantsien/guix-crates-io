(define-module (crates-io ev mo) #:use-module (crates-io))

(define-public crate-evmodin-0.0.1 (crate (name "evmodin") (vers "0.0.1") (hash "1ii20ddvafxv32ry3bqvimsfm4y979w6s7lyk2x5abv6r853qv5w")))

(define-public crate-evmole-0.3 (crate (name "evmole") (vers "0.3.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "ruint") (req "^1.11.1") (default-features #t) (kind 0)))) (hash "07z0wlni2j7v9jxhvma2fqzb04mg8b8gn8wfpyhq7vsqksad34pl")))

(define-public crate-evmole-0.3 (crate (name "evmole") (vers "0.3.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "ruint") (req "^1.11.1") (default-features #t) (kind 0)))) (hash "0agys0sbkwijl7jly47p26wprndhibc2f13n79n4ihrzy7y7vxcf")))

(define-public crate-evmole-0.3 (crate (name "evmole") (vers "0.3.3") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "ruint") (req "^1.11.1") (default-features #t) (kind 0)))) (hash "1zpmy8lwcdfhb0vxk6x82i1kziryqgm8yddx0yjwnsrh9jphaknd")))

