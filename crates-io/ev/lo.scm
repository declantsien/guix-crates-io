(define-module (crates-io ev lo) #:use-module (crates-io))

(define-public crate-evlog-0.1 (crate (name "evlog") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "16ysx9ifpakz4ah728rzy20rbl38mjk64qp0b5mdchl5zyn9k8hh")))

(define-public crate-evlog-0.2 (crate (name "evlog") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1j25lmqsn6iyrn45pnpipqc4k3afikv2w8ky98da140j8m09kxsg")))

