(define-module (crates-io ev se) #:use-module (crates-io))

(define-public crate-evse-0.1 (crate (name "evse") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "19r3zfay49wpwp6s2qzfy2w6bahf60yvqs1qw66zhrppnb03wgzy")))

