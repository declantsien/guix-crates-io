(define-module (crates-io ev mc) #:use-module (crates-io))

(define-public crate-evmc-0.1 (crate (name "evmc") (vers "0.1.0") (hash "14k2zjmhj90aj2hfwsf2j84pic4dp2b7sgybyyb36jc4vjkdl1sl")))

(define-public crate-evmc-declare-7 (crate (name "evmc-declare") (vers "7.1.0") (deps (list (crate-dep (name "evmc-vm") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0p3769h3ipid1lj11jzmy705y30l1r6s2gjylkxgpmscq444xi7y")))

(define-public crate-evmc-declare-7 (crate (name "evmc-declare") (vers "7.2.0") (deps (list (crate-dep (name "evmc-vm") (req "^7.2.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ydvyn6m5maf05gxbk50ax9abw2yhgfrxz5qk61wmkwpr4nljmj1")))

(define-public crate-evmc-declare-7 (crate (name "evmc-declare") (vers "7.3.0") (deps (list (crate-dep (name "evmc-vm") (req "^7.3.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hizjyih80228m4wjvlljls2h597p8awlm01xrvf15rnfs25bjv0")))

(define-public crate-evmc-declare-7 (crate (name "evmc-declare") (vers "7.4.0") (deps (list (crate-dep (name "evmc-vm") (req "^7.4.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0db2hg6w0kldnpcxjsa3bq49n38hyx4zfvhjj7z9dc7dw9prxr0a")))

(define-public crate-evmc-declare-7 (crate (name "evmc-declare") (vers "7.5.0") (deps (list (crate-dep (name "evmc-vm") (req "^7.5.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i2w4sd9km9g177bv2h83y9x7fyif273h6sl3fjcrz6ff30j19nn")))

(define-public crate-evmc-declare-8 (crate (name "evmc-declare") (vers "8.0.0") (deps (list (crate-dep (name "evmc-vm") (req "^8.0.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "134np663lv8s75bbw19pdgsclgaax1faana25xj46jw05ji8pg9k")))

(define-public crate-evmc-declare-9 (crate (name "evmc-declare") (vers "9.0.0") (deps (list (crate-dep (name "evmc-vm") (req "^9.0.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jsr3spd32sy79qma59li0i2xq4g3rn0vr444z8pbqjwv3ddkc66")))

(define-public crate-evmc-sys-7 (crate (name "evmc-sys") (vers "7.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "0hh22j815ff3a682yrggqisw35cmr57idc8fs47ci6zzxs9x60rn")))

(define-public crate-evmc-sys-7 (crate (name "evmc-sys") (vers "7.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "02v1xsiiscv3r32pnsjikhs326q1z7qx3ah12q7v309k7q7zdmlg")))

(define-public crate-evmc-sys-7 (crate (name "evmc-sys") (vers "7.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "0wn9iabr0p5c8myxqyfscc27zrigq2zb1f61vgrwl5h7yxv2ps3l")))

(define-public crate-evmc-sys-7 (crate (name "evmc-sys") (vers "7.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "03hd70m7v21mkynmsf6yayza3pjgja2fp8f8hkvcr8mfk1w9w050")))

(define-public crate-evmc-sys-7 (crate (name "evmc-sys") (vers "7.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "12ylya1jlm1r8rnqj28a91igairp8190nh37gc0vi0ij86rna9zv")))

(define-public crate-evmc-sys-8 (crate (name "evmc-sys") (vers "8.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "03wqnqyxcdxhhd3jy69jlqw9d7d0dxvzdqk5bsj11sls0iwga82q")))

(define-public crate-evmc-sys-9 (crate (name "evmc-sys") (vers "9.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "0fikwpz752hdbpvahl7mks1pla1i4dy9rqw2dhjh6yq06wq8xsaq")))

(define-public crate-evmc-vm-7 (crate (name "evmc-vm") (vers "7.1.0") (deps (list (crate-dep (name "evmc-sys") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "1dff37nhxjhdmr9fzapn1dnmvlbb7gwili02gy3qzj7iimfy4yzh")))

(define-public crate-evmc-vm-7 (crate (name "evmc-vm") (vers "7.2.0") (deps (list (crate-dep (name "evmc-sys") (req "^7.2.0") (default-features #t) (kind 0)))) (hash "1wv6y9xdvsb75q0dvf4838550k29jn4ddd6zv51dpc7rkmj68c9r")))

(define-public crate-evmc-vm-7 (crate (name "evmc-vm") (vers "7.3.0") (deps (list (crate-dep (name "evmc-sys") (req "^7.3.0") (default-features #t) (kind 0)))) (hash "1bmaczhvmd7yn5a1gib59yw2z8kx6cm9ndx91f9anxswh25rgfxc")))

(define-public crate-evmc-vm-7 (crate (name "evmc-vm") (vers "7.4.0") (deps (list (crate-dep (name "evmc-sys") (req "^7.4.0") (default-features #t) (kind 0)))) (hash "1blm44w7l7yhn3cs2h80svbk14qw8ysg3lnpq27s0vvg7mpamxwi")))

(define-public crate-evmc-vm-7 (crate (name "evmc-vm") (vers "7.5.0") (deps (list (crate-dep (name "evmc-sys") (req "^7.5.0") (default-features #t) (kind 0)))) (hash "04a2sj6wc5zr389mpqlgkwqpg4nr56clm6llwv0svsvsn06x4cj2")))

(define-public crate-evmc-vm-8 (crate (name "evmc-vm") (vers "8.0.0") (deps (list (crate-dep (name "evmc-sys") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "1iynkplvdwycymgsa2w9c92iy9dzdqkbcnlyxb2cjiks1j3n6z8d")))

(define-public crate-evmc-vm-9 (crate (name "evmc-vm") (vers "9.0.0") (deps (list (crate-dep (name "evmc-sys") (req "^9.0.0") (default-features #t) (kind 0)))) (hash "0z9755hdb5r55c5bnx880mfgxjx07rq740l576cqsj60g05wsndk")))

