(define-module (crates-io ev pk) #:use-module (crates-io))

(define-public crate-evpkdf-0.1 (crate (name "evpkdf") (vers "0.1.0") (deps (list (crate-dep (name "digest") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "md-5") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "sha-1") (req "^0.8.2") (default-features #t) (kind 2)))) (hash "1mhk8gkhlkmf0q65y3s11kqif5rakpssl92zpc8bksj3sj2hxiv5")))

(define-public crate-evpkdf-0.1 (crate (name "evpkdf") (vers "0.1.1") (deps (list (crate-dep (name "digest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "md-5") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "sha-1") (req "^0.9") (default-features #t) (kind 2)))) (hash "0nbkacgp4xval2j1bkdmq5x4rx09ma2ps1jww93ykim6a20vgkv3")))

(define-public crate-evpkdf-0.2 (crate (name "evpkdf") (vers "0.2.0") (deps (list (crate-dep (name "digest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "md-5") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "sha-1") (req "^0.10") (default-features #t) (kind 2)))) (hash "1ncqb5rshr2h1fqi1rc1kj7vxqkwr9k4954iy9c08m5lcqbngffz")))

