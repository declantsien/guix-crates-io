(define-module (crates-io ev md) #:use-module (crates-io))

(define-public crate-evmdasm-0.1 (crate (name "evmdasm") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "02n5yw6yzcimca5l4ky77wfyb35c89y74m2rmdx5p1i5az9vhm5w")))

(define-public crate-evmdasm-0.1 (crate (name "evmdasm") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0r3ay80qgvi2kzxsyng3pcm18pq21hqakwka1rqdwh78r0982zj7")))

