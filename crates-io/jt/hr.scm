(define-module (crates-io jt hr) #:use-module (crates-io))

(define-public crate-jthread-0.1 (crate (name "jthread") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1fpkxji08wzz9yr930nwiz00bqgb9pp4si1kmwhwxi0sgknch5rq")))

(define-public crate-jthread-0.1 (crate (name "jthread") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1hx7wpzi4g1sjpy7s1jnbiy7vqnj3zvlrwfi3vpc9azzyf72fsy1")))

