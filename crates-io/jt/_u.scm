(define-module (crates-io jt _u) #:use-module (crates-io))

(define-public crate-jt_util-0.1 (crate (name "jt_util") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zp8827g29hxgg0vaf1fy38x3jl2yk8pimmf6sn8lybh12h9z9zi")))

(define-public crate-jt_util-0.1 (crate (name "jt_util") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rhrwj664dda6ha8p4p5j9bm193k9vki77kyf2gp79a7mxqc9wpi")))

