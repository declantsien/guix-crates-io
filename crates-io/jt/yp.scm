(define-module (crates-io jt yp) #:use-module (crates-io))

(define-public crate-jtypes-0.1 (crate (name "jtypes") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "064bz5yrk06zaax46gqljc20h2pr983xqvc8r7p4hmih9fxi970j")))

