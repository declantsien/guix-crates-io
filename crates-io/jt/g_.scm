(define-module (crates-io jt g_) #:use-module (crates-io))

(define-public crate-jtg_gpt-0.1 (crate (name "jtg_gpt") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0gc0vwccr2gyqgbf23ms1ad3pa121wd3v6hs4w4z7swy3p83kcbc")))

(define-public crate-jtg_gpt-0.1 (crate (name "jtg_gpt") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0nphaysvwrl9h682h9aj0bhwgxqf7rcf928c9nddzgw23lb3nizh")))

(define-public crate-jtg_gpt-0.1 (crate (name "jtg_gpt") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1vbvn37ada7y780rg36i08rjvhzxsz28ymvkfsbwm6i1nnkxak2f")))

(define-public crate-jtg_gpt-0.1 (crate (name "jtg_gpt") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1z3knlb3lrr46x9d7x7xs6nb1g0r99jd5xgjaxs1vryng0vabphq")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1azikgj2dq7fvdmgjbzr835mwl581bv4c2msw1qqy5ibx259yf50")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "113ivlyfidp7335fj7caxw8nyk2a72b5siggqwqpkc531skghlnp")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0s17ibxna64all6smppgiwijykzzcsi3fi491cnw86mmgg0g02h7")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "148y747fvd79s7iymivi2r6gxj2b46ri4v1aqx22s304narrh0rb")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.4") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "16fpk4121s4scgf7rn7kxrqr7xjlfipc3yilrfcmxkghb2in63q2")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.5") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "00xlk1jh317bybzsjzcabfyw0bsml2n7xyp9ldn4c7jp4gk06pa3")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.6") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1c1rd6cfcra47ikj5pvbfcr3v9zn3zzadik63zhsj5nzhqxiv9wx")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.7") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0kp05bxfj8zc4z02vhsxsgngyn5pslrmhl3iindf53lb031db6vv")))

(define-public crate-jtg_gpt-0.2 (crate (name "jtg_gpt") (vers "0.2.8") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0pnyx7z5l2si05psdh8qdn2fplp7i6i5bv3d7pik92idwkky74hm")))

