(define-module (crates-io jt ar) #:use-module (crates-io))

(define-public crate-jtar-0.1 (crate (name "jtar") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "145g5s44yqczlhymsw3nd8n6m7rmsk8982jnid8wlqk2r0ragiqa")))

