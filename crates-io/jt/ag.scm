(define-module (crates-io jt ag) #:use-module (crates-io))

(define-public crate-jtag-0.0.1 (crate (name "jtag") (vers "0.0.1") (deps (list (crate-dep (name "ftdi") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ftdi-mpsse") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)))) (hash "0yswzkf433riki79dxips6ss4cj3bqjlm1sp6vvjxcm95ibkhkgb")))

(define-public crate-jtag-0.0.2 (crate (name "jtag") (vers "0.0.2") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ftdi") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ftdi-mpsse") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)))) (hash "17a4129dmqjjkkbdsqpq3j87s34bd3rmsi7yv9i444w15gymd30h")))

(define-public crate-jtag-0.0.3 (crate (name "jtag") (vers "0.0.3") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ftdi") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ftdi-mpsse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)))) (hash "0y07798pqij47srnv475fhn6ix5acj8yac7v0a5adckkd32zxz61")))

(define-public crate-jtag-adi-0.1 (crate (name "jtag-adi") (vers "0.1.0") (deps (list (crate-dep (name "jtag-taps") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "099whzvax42fsi5yrswgz5gbm598iic4pb3pffm18lw4dm23ap6n")))

(define-public crate-jtag-adi-0.2 (crate (name "jtag-adi") (vers "0.2.0") (deps (list (crate-dep (name "jtag-taps") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pmnjgcwdd3la44cdq39sk327cgq92brzk390x9mifsqbd6k3iba")))

(define-public crate-jtag-adi-0.3 (crate (name "jtag-adi") (vers "0.3.0") (deps (list (crate-dep (name "jtag-taps") (req "^0.5") (default-features #t) (kind 0)))) (hash "0mabgi1alpg6xcdns7f2kw36hz9pbrml4qwvnrsfqixpi6g6di7x")))

(define-public crate-jtag-taps-0.1 (crate (name "jtag-taps") (vers "0.1.0") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)))) (hash "17970bnlqdj7d3krvq7mkfii4x3v6wy8hk782s5165irmv8g48sj")))

(define-public crate-jtag-taps-0.1 (crate (name "jtag-taps") (vers "0.1.1") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)))) (hash "0hmkc5vdawrdl3ccx7x38p54yy56gy5236qd9vp0j7zciai8f61y")))

(define-public crate-jtag-taps-0.2 (crate (name "jtag-taps") (vers "0.2.0") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)))) (hash "08pmv7k48lnzywfzd2nmm7bgq6rvy31993dqf4scm5zldqv5j46s")))

(define-public crate-jtag-taps-0.2 (crate (name "jtag-taps") (vers "0.2.1") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)))) (hash "0zw41z11rmskrvlmmvkif40vy1fssyrsk0743ffg5l1yxfa7wlfh")))

(define-public crate-jtag-taps-0.2 (crate (name "jtag-taps") (vers "0.2.2") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)))) (hash "0q6gbmp5p7fmlc3d3lfj0ds3v3y9spppavi9kilx5yinh6nafx5m")))

(define-public crate-jtag-taps-0.2 (crate (name "jtag-taps") (vers "0.2.3") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0s2d3xw5d91mbzj8sm87lr9jis9x5cwvprw9vzivyhz09n6d070i")))

(define-public crate-jtag-taps-0.3 (crate (name "jtag-taps") (vers "0.3.0") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0gdniwjajm6wf80a32lf7ygm97h14ypp32zxmx5f1ir3b5384i79")))

(define-public crate-jtag-taps-0.4 (crate (name "jtag-taps") (vers "0.4.0") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1qqgr22wxy8c5cmga1jd3xk95mka5z5zkl0xvs5pb0il0g4xl0r3")))

(define-public crate-jtag-taps-0.5 (crate (name "jtag-taps") (vers "0.5.0") (deps (list (crate-dep (name "ftdi-mpsse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libftd2xx") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "14r3mf8farzgyfp2yb479j1vhxcc2xib5f272h6ljkrl83zc8rpl")))

(define-public crate-jtagdap-0.1 (crate (name "jtagdap") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "jep106") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "1b7gnmxyw6s9kh6w13xwcy5cwhysg0l0lhlnvz82him0iaa4dv74")))

(define-public crate-jtagdap-0.1 (crate (name "jtagdap") (vers "0.1.1") (deps (list (crate-dep (name "hidapi") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "jep106") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "15fp5nj7nqcz23vji6kr1xggny4bvks304svylnlr2n335nlygz9")))

(define-public crate-jtagice_mkii-0.1 (crate (name "jtagice_mkii") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.3.0") (default-features #t) (kind 0)))) (hash "0zbhlrq0k1p0j633isy5sjiszdn3257g7a9v7wwrmbasfass0ljs")))

(define-public crate-jtagice_mkii-0.1 (crate (name "jtagice_mkii") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.3.0") (default-features #t) (kind 0)))) (hash "02xsdgz1mnrjpfrknmfx30f84gxm28512phss7r2y65h3ajll2rh")))

