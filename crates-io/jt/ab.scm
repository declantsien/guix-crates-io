(define-module (crates-io jt ab) #:use-module (crates-io))

(define-public crate-jtable-0.1 (crate (name "jtable") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0vg93i8nxxbl13g1vvcb55r6ch1mg5yvv2vbf0mbnkkjarlmmq48")))

(define-public crate-jtable-0.1 (crate (name "jtable") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0x0zqrpf361zfd7aa9cff48j5v98pq1yh3mdd5dpy4d6rqngvw4w")))

(define-public crate-jtable-0.2 (crate (name "jtable") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0bgqbiidmjss7xi16g4r9d97gskk5ag1xdkgi1m2di0wvvc1w6px")))

