(define-module (crates-io jt #{10}#) #:use-module (crates-io))

(define-public crate-jt1078-0.1 (crate (name "jt1078") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "jt808") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jt_util") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec" "compat"))) (default-features #t) (kind 0)))) (hash "1kkwmw0y8ld4mp236wvpmfazsz7b7hgicjx3c52x80wy679zj7d5")))

