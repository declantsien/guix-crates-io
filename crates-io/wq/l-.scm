(define-module (crates-io wq l-) #:use-module (crates-io))

(define-public crate-wql-nom-0.1 (crate (name "wql-nom") (vers "0.1.0") (deps (list (crate-dep (name "bcrypt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.121") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1r5ipq5073a0261vy3wxfqz3220dwwrmhz9pxiigkyfps014nwid")))

