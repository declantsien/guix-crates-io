(define-module (crates-io wr ec) #:use-module (crates-io))

(define-public crate-wrecc-0.1 (crate (name "wrecc") (vers "0.1.0") (deps (list (crate-dep (name "wrecc_compiler") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1y74pvd0baixij7wbxpf9kvr5pyjy8826zwln6s4fab2d13gka33")))

(define-public crate-wrecc-0.2 (crate (name "wrecc") (vers "0.2.0") (deps (list (crate-dep (name "wrecc_compiler") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ccvr7f5mn2gqfh21n71cvva7z68994adakl7hxgfvrkn7wgk5kf")))

(define-public crate-wrecc_compiler-0.1 (crate (name "wrecc_compiler") (vers "0.1.0") (hash "0sn816j3gblb3h3i6fvj4za9q3sxblkc09a7q4why4kncdx8m609")))

(define-public crate-wrecc_compiler-0.2 (crate (name "wrecc_compiler") (vers "0.2.0") (hash "1g3xaq1kfaa2yf6ivwf0h12y5dic9k82v116wqm031388v8fkrj5")))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "05s10bw71y82pphpjn0wsa8z8q9q8f26bdzjl8177c9m1g24bhkq") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1yn36yd9v1knlsh7prgavmx879n09qc2vzgpm3qpkzfd096i21lf") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "02bckrfn2xx7ab23fnc6g9p53jw70akzqa63hahky9h7jyydjhn9") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "07am2bj3mdmbdnc7267p6yr0c8q6xkany9vz3zbkqmrh0gndhnqh") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "02ax9sgi290x043mx015isv9v7fxp4plvcdina56fqaj6msqgs80") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cy79c6wnk9vgxasdpgiz0fvkzrcp35hkkdk80pv1fbwkkbfig58") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0is48mv3ln8s79rg7hk2j3dl8dr747rbziaff9kfifyxgzfw0fdw") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1g10lplfyhwniy24a9c6biivwf9w5g5lrf6adjsrk2nb654fb4kv") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gk6ds6j5cpldm4c2qnibchyvahpgj4nzakm67w28pdjljcfmnyg") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "19fq49vpp16n2r0q9vqmmvswl6qhinck5pdh3vdn86crwjjp8nwq") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.11") (deps (list (crate-dep (name "libc") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "termios") (req ">=0.3.0, <0.4.0") (default-features #t) (kind 0)))) (hash "05j89dqpl46q6s7k0yivzkyaic8k04b6bjhax2fyhs7ldf6r2jjz") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1180gclr5nhi5pjhihv2ffvgkchfwd58sg4xjlxil8snbpfakf9w") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ya2qy30h27agmpcshzzvz0q720j6bgsyzh9266ib9llrvh6l35h") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1avgjcpk192g5y87wg2bsmiingbrb5z6rxl4gxfy22pbhaf51v1z") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.15") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qy0d7nwycq151ip6cmzfpxw7n9f8rnv4mqh2ma4sdcsy26w47sr") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.16") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1as1l8833bwfc8vb3s8bbzr4g3pb5iqpzwfzmzxnjlmvdcf3y8mg") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.17") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cf73pjf4masmkz15msmlyr0ak9jyl57l1mca3qy5s3jl7v8giyz") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.18") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qrl85gzgiijf5x59827mf38rvaq6lwn9zj0bs7kjqhpqwhdj91l") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.19") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "106rf4bdkgan74cq54l6hp311d67cb20i5vl7vzzsnr0rfkjrqq4") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.20") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0h9rpwbv9r53p0ax1iq99nns56dp0cdg0xq1smi84vkm1n4hwhps") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.21") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1m3bairzq7idg5z4d2k8zxj9hv7ysk4h9wqgi07gcdk788ccc982") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.22") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pxsh7fy56q3ijwbr1na80my8d4zk9yj8iqcg7v41n1dy84yhnl4") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.23") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bn6ai1bc8hg1gndxjfw1rxh1c19h5bgg5bg73ysbzdxcx5zpgf2") (yanked #t)))

(define-public crate-wrecked-0.1 (crate (name "wrecked") (vers "0.1.24") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0waam02wcjjihnmyablasmgq3n1mga6g3aynavjm9jyj5yqhvnc2") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "09q9c33347gcg8v7rfimfaf88ihbsc5jvxn6vbazh9fg11zsg0vk")))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vg0xysq64yzwrlh5pasrhv58ngd4d5l6xvg8h9v756bh72mfkzj") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hn8c0j4g5ibqql40z21z89wkb91jmljvp0sdxi4kh4hbiqb8vw5") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ly088jzy3dmdiqpv5n92dp2m7rqnyxi4ck59a2cl5zvk4r9zpvg") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "1caww6jlhflyfam1qmaf8q1bjax9g21sr8039vslbxjxsl0k4p89") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "17lgzbcnddc5b4cwkcsdvc3kq8q7nnba03m31yi0dh14gl9vljqg") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "068rb6bncww122wk6bnyzzn3n9371mcjiwz8l6igb7fiq22ill56") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "13ycnb23sg5k31141vlf3kpylwv54y97zsydkbgjmgk5f1c7z1vb") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ki2z58z8lwn3030f0yq3s2clzr4pz316kww03f6aigdzkwjk86l") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1qd0sp4n0jn0lmxaxc4nfss0viv39cml546ra0wap7naiplp82xw") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1ckxd6q757cgx38sqkfnb87ifgmrk4p4fglgqcb9jd1g010cm2sx") (yanked #t)))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.0.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1clw4w0wlq1jlnrxkviq6zjk9bzlbl1dpq2naz4kll4r7l9ly9ni")))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0zc5zrdxayhcvl69f34vx0zfkav3l36v0g41iidaaqcjy87r5pnm")))

(define-public crate-wrecked-1 (crate (name "wrecked") (vers "1.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_System_Console" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0x6hzpl5kzvbd3afg6d6s2qfc2nf2fcvkccwlcsakhxhl05szha3")))

