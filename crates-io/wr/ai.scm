(define-module (crates-io wr ai) #:use-module (crates-io))

(define-public crate-wraited-struct-0.1 (crate (name "wraited-struct") (vers "0.1.0") (hash "1hb2hw6iif3j7v7ggml74ij5l6vq0w0l1f8h6j24np3cnfq7x757")))

(define-public crate-wraited-struct-0.2 (crate (name "wraited-struct") (vers "0.2.0") (hash "1b9fws71yi3q57hr28afz3a36j1xmk25zmc16ngcfd7hiwsvm2ar")))

(define-public crate-wraith-0.0.1 (crate (name "wraith") (vers "0.0.1") (hash "0gf3kzb9i54kain727cbcr68vrgfy4s8qn98i3p7583g3vp75xcd")))

