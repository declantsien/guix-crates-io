(define-module (crates-io wr rm) #:use-module (crates-io))

(define-public crate-wrrm-0.1 (crate (name "wrrm") (vers "0.1.0") (deps (list (crate-dep (name "atomicbox_nostd") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0741lkph2hprmmlys7b169vnpdf3dy034ijl3jc52nw90sg8knzb")))

(define-public crate-wrrm-0.1 (crate (name "wrrm") (vers "0.1.1") (deps (list (crate-dep (name "atomicbox_nostd") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "15lwm023rmf3g508bqz7mwkjqr55h3rjy5jyq74lbg1bcwixr3k1")))

(define-public crate-wrrm-1 (crate (name "wrrm") (vers "1.0.0") (deps (list (crate-dep (name "atomicbox_nostd") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0sf4r1zk0hsghr1dgvxy15jg6gjmxs4birdhdr4k18cl47mx1s1y")))

