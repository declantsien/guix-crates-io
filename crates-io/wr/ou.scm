(define-module (crates-io wr ou) #:use-module (crates-io))

(define-public crate-wrouch-0.0.1 (crate (name "wrouch") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19aimb5iswwp8w9v4sgn60gcl3y2kkd50zabblj6sw0i329j6s4b")))

(define-public crate-wrouch-0.1 (crate (name "wrouch") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0f0pkdv4prnm2sk516k7hydkbskkh70kwv4c62zgj3k0rj92vjy8") (yanked #t)))

(define-public crate-wrouch-0.1 (crate (name "wrouch") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "171x9avzhiihrq7gnd5f532b8cprc91kmika05i0zxpq5hh36932")))

(define-public crate-wrought-0.1 (crate (name "wrought") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "codespan") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "1w2fg05hx1hvx5jkfaqzs6gcyifzddcr57sa23vlf8569c0zn3gp")))

(define-public crate-wrought-0.1 (crate (name "wrought") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "codespan") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "1m7yp63fnbxpci7r3y7cymjpc3lqffir166a4mw199c8b1lrdydc")))

(define-public crate-wrought-0.1 (crate (name "wrought") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "codespan") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "03ksb9l8902h5b228g2daag0dwa1n2i4v2iywrv8ixswca4csjrj")))

