(define-module (crates-io wr _m) #:use-module (crates-io))

(define-public crate-wr_malloc_size_of-0.0.1 (crate (name "wr_malloc_size_of") (vers "0.0.1") (deps (list (crate-dep (name "app_units") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.19") (default-features #t) (kind 0)))) (hash "04bz7bgkrhjnlql9bay3kx3skpcl6sk6n14d3dk1dy0kiagksxx8")))

(define-public crate-wr_malloc_size_of-0.1 (crate (name "wr_malloc_size_of") (vers "0.1.0") (deps (list (crate-dep (name "app_units") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.20") (default-features #t) (kind 0)))) (hash "129wb6g3dz8wdkbcz2jxbcahk07mh12vlgh53cg5khca3ppcx0yv")))

