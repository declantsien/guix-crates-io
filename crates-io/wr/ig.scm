(define-module (crates-io wr ig) #:use-module (crates-io))

(define-public crate-wright-0.0.1 (crate (name "wright") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sdfmjlgqaslpscii1grsqwzwd978zc96njkq87insqhpzhc2z97")))

(define-public crate-wright-0.0.2 (crate (name "wright") (vers "0.0.2") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qyg4xi5s3cnb3nf9hr9gka1a2jwxi4nvrldkg6jp0qm7ifmmyl1")))

(define-public crate-wright-0.0.3 (crate (name "wright") (vers "0.0.3") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "064rjbsjav1adbhxjzgnp5am7ja58h8gpvphgry80912hf6wqmll")))

(define-public crate-wright-0.0.4 (crate (name "wright") (vers "0.0.4") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mpwjsikmvy7nyy5m565yalvk8mf203kdm7w6f39j6iym8dgv7a0")))

(define-public crate-wright-0.0.5 (crate (name "wright") (vers "0.0.5") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bbifvlb88zqxmgpl7dpshywgd20sif8q1f1lqpvyqq7a1f77lk4")))

(define-public crate-wright-0.1 (crate (name "wright") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0l463zqhl62zf98mpjirmy9hv0nsg6b6kpqvbl8hfk59ax0mi056")))

(define-public crate-wright-0.2 (crate (name "wright") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vkqrqkqlqhx55kyakhk2ybniw5h5lsxw4dc2za55g7qd3b1m85x")))

(define-public crate-wright-0.2 (crate (name "wright") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ji0zlgckajb8v63lq0a2y6hfkw5akr02m3wl91cx2gp81nl141c")))

(define-public crate-wright-0.2 (crate (name "wright") (vers "0.2.2") (deps (list (crate-dep (name "ansi_term") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lk1qvxhgd2qnx914yz2dqv403wpay08153h7qa1pmivdw3i7y0l")))

(define-public crate-wright-0.2 (crate (name "wright") (vers "0.2.3") (deps (list (crate-dep (name "ansi_term") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i1klsl8mf1154r3q9nb96lqgx53pscfzmhswkwipbsp1xriyz84")))

(define-public crate-wright-0.3 (crate (name "wright") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fr3bp3q5h65d2f2ngb1gnaw0kwsxgcndz7cxaz0a9xsx532j1vy")))

(define-public crate-wright-0.4 (crate (name "wright") (vers "0.4.0") (deps (list (crate-dep (name "ansi_term") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pvqffwrci36fkvx81zznjdq7pdaq1psjr6c5ymn9zqhg1ig1cfp")))

(define-public crate-wright-0.4 (crate (name "wright") (vers "0.4.1") (deps (list (crate-dep (name "ansi_term") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "18hc4nsfhjb10hkafjd7p7hsp3iwvgf15df4dx7mzj56z2qbjbi1")))

(define-public crate-wright-0.4 (crate (name "wright") (vers "0.4.2") (deps (list (crate-dep (name "ansi_term") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "14nmn1j9h502y5r6cdjlfhqcpbkmcywal4il8nzn5avsz91rwvn7")))

(define-public crate-wright-0.5 (crate (name "wright") (vers "0.5.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n430h7hdi832hxpcry5fn915v89ybxcak0m4s5ni0qclf67xa02")))

(define-public crate-wright-0.6 (crate (name "wright") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1c2j3xqbi72p0j1y6359wbyma5sa9bsph13yc4447ngpy0w2phcp")))

(define-public crate-wright-0.6 (crate (name "wright") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "02k2l1g1gvh7q9nb21dbiy58j5jczmywcph972zcvdjajbj3mf2v")))

(define-public crate-wright-0.6 (crate (name "wright") (vers "0.6.2") (deps (list (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m94mhknvsqzl042cdc2vql3704wk6g67bf7rj5m702z4mq2v3ii")))

(define-public crate-wright-0.7 (crate (name "wright") (vers "0.7.0") (deps (list (crate-dep (name "brain-brainfuck") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "parity-wasm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "14js4lzbsi57870kg7mfibyk2hcybs66w8ar2bg1alm25xmczf53")))

(define-public crate-wright-0.7 (crate (name "wright") (vers "0.7.1") (deps (list (crate-dep (name "brain-brainfuck") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~0.2.0") (default-features #t) (kind 0)))) (hash "0lrndghw9895zbpbvkx8zf5nc8b7kyv1jslv0x5qxhkfx6prcw6n")))

(define-public crate-wright-0.7 (crate (name "wright") (vers "0.7.2") (deps (list (crate-dep (name "brain-brainfuck") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~0.2.0") (default-features #t) (kind 0)))) (hash "0jjilk4vawi1v4q6fh2340kabljnx7091dh857hvm59nhsg1zi6h")))

(define-public crate-wright-0.7 (crate (name "wright") (vers "0.7.3") (deps (list (crate-dep (name "brain-brainfuck") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.31.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~0.2.0") (default-features #t) (kind 0)))) (hash "10xzxxq8hrkvrdllv1pjpnma7d4zhpgbii0y58df17148zpyvny0")))

(define-public crate-wright-0.8 (crate (name "wright") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "codespan") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1gfs3bb7nar0h8b47gjc5r9qk5jdkln583z48pf6gxyg0mcq04vq")))

(define-public crate-wright_omega-0.1 (crate (name "wright_omega") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "fastapprox") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1cgvr6frbsl2gnjgna7gcdz49d2l1jqvkd3f99004c9wnz2qjrfh") (features (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-wright_omega-0.1 (crate (name "wright_omega") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "fastapprox") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "17nd3j8g87m2rdzgghvff97qnscr3zcs6cxygw7p2kj9s14p46c9") (features (quote (("f64") ("f32") ("default" "f32"))))))

