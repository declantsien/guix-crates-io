(define-module (crates-io wr in) #:use-module (crates-io))

(define-public crate-wring-twistree-0.1 (crate (name "wring-twistree") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mod_exp") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.44") (default-features #t) (kind 0)) (crate-dep (name "num-prime") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "08dvsc99ww7h3jwl5s9l5r8ck3phc9q820if137adibj4r2v456m")))

(define-public crate-wringer-0.1 (crate (name "wringer") (vers "0.1.0") (hash "1xmap6slncjlrirmcsnks3pjmp8rbbysh5jk7j0077v8fzi3vr3k")))

