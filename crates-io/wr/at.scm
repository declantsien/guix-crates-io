(define-module (crates-io wr at) #:use-module (crates-io))

(define-public crate-wrat-0.1 (crate (name "wrat") (vers "0.1.0") (deps (list (crate-dep (name "comat") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1xl77s2xs5lmkl2rc4bndy3dqkzfvymqzjz31i47chp7zd009api")))

(define-public crate-wrath-0.1 (crate (name "wrath") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.34.0") (features (quote ("yaml"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)) (crate-dep (name "wrath-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "030m2146nwa2a5h74y454an620p90naq0n5zqvg9n2xd94h7qxpr")))

(define-public crate-wrath-macros-0.1 (crate (name "wrath-macros") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "0rij7i34s00p6bsrpj955sqa62zk1hbq8wmxvbr9agz06wkb9fkn")))

