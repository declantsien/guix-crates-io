(define-module (crates-io dj r-) #:use-module (crates-io))

(define-public crate-djr-cli-0.0.1 (crate (name "djr-cli") (vers "0.0.1") (deps (list (crate-dep (name "cargo-lock") (req "^8.0.3") (default-features #t) (kind 1)) (crate-dep (name "djr") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0xwkjk2imz8m7hlsch129h8fkv224nnw8gbmwg3xgrzrjcaka2yv") (yanked #t)))

