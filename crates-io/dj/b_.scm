(define-module (crates-io dj b_) #:use-module (crates-io))

(define-public crate-djb_hash-0.1 (crate (name "djb_hash") (vers "0.1.0") (hash "105ydn1y8i64z66ikg5n0s636vqw0vvg06iyv7wc07pkm8dg8px1")))

(define-public crate-djb_hash-0.1 (crate (name "djb_hash") (vers "0.1.2") (hash "1xl2m5gdylnfpqss68gnx37k4mdwnkcsr81bwn1ar3wixg6pv4iz")))

(define-public crate-djb_hash-0.1 (crate (name "djb_hash") (vers "0.1.3") (hash "0y45y23xlnnskzbprwg4l3bwv21pgzjlfkzjz2db98r7wrhpvkz8")))

