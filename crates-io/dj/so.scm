(define-module (crates-io dj so) #:use-module (crates-io))

(define-public crate-djson-0.1 (crate (name "djson") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z40mkv55h63sz5nyag210m5s2rx9qmqpib7km1yz860npgwr9rv")))

