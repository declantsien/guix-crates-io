(define-module (crates-io yq ue) #:use-module (crates-io))

(define-public crate-yquery-0.1 (crate (name "yquery") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "05cprfd7rkq2jnv2k2n6p1sh8i5ihafc867j3v27c9hh4vr1g2i8")))

(define-public crate-yquery-0.1 (crate (name "yquery") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "15i4lrifsk69m2md062gf5rh556sgi6ich7pp6ipf9yxa6hnbjyi")))

