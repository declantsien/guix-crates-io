(define-module (crates-io vd pa) #:use-module (crates-io))

(define-public crate-vdpau-sys-0.0.1 (crate (name "vdpau-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18") (features (quote ("xlib"))) (default-features #t) (target "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"openbsd\", target_os = \"netbsd\"))") (kind 0)))) (hash "0s3lkf8p3p7g0xb3hvgrnmvkm36ri8cp2dff2s0hzffwyszsb7gb")))

