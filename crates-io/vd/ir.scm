(define-module (crates-io vd ir) #:use-module (crates-io))

(define-public crate-vdir-0.1 (crate (name "vdir") (vers "0.1.0") (deps (list (crate-dep (name "vfile") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xang8fv650kpcxpaa1fpcdjgqig7drxp106wddzfz1j777xh339") (yanked #t)))

(define-public crate-vdir-0.8 (crate (name "vdir") (vers "0.8.0") (deps (list (crate-dep (name "vfile") (req "^0.8") (default-features #t) (kind 0)))) (hash "1mynhsssz19xysga7wdsh6249yw2prarlry6vlv4cvnsfpikcqkq") (yanked #t)))

(define-public crate-vdirsyncer-0.0.1 (crate (name "vdirsyncer") (vers "0.0.1") (hash "0dhvw54qwm67kc4lhzlhnsv9nh3rbap8lzif2zmx41vijznq2lwv")))

