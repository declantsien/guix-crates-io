(define-module (crates-io vd so) #:use-module (crates-io))

(define-public crate-vdso-0.1 (crate (name "vdso") (vers "0.1.0") (deps (list (crate-dep (name "crt0stack") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (kind 2)))) (hash "1ffb17bhnwq33q2iry786a6vla9y0bs5xdgqxhdl4xrnacxbck47")))

(define-public crate-vdso-0.2 (crate (name "vdso") (vers "0.2.0") (deps (list (crate-dep (name "crt0stack") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (kind 2)))) (hash "1931xqbvcl71p7h534fhv0j3f68k7d4cfkv7dw754hd52jr9kcwc")))

(define-public crate-vdso-0.2 (crate (name "vdso") (vers "0.2.1") (deps (list (crate-dep (name "crt0stack") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (kind 2)))) (hash "1ny9rf96ay402lccv8hcnsd58ra1i4jk9xyhs6hxnaygi6k84qkv")))

(define-public crate-vdso-0.2 (crate (name "vdso") (vers "0.2.2") (deps (list (crate-dep (name "crt0stack") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (kind 2)))) (hash "0w9jd4x9172pfyj7smhxq0ga4xla32yqaxa6xp3jiz6rl8nry47i") (rust-version "1.56")))

