(define-module (crates-io vd ks) #:use-module (crates-io))

(define-public crate-vdksf_guessing_game-0.1 (crate (name "vdksf_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "14winqyq2c2yshrv2g50nwqp7zd0ligrcz157m1pn4bgvnjknzpj")))

(define-public crate-vdksf_guessing_game-0.1 (crate (name "vdksf_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1jzb93h0gi0v98g47b9rym4yl64f52v1jmd0kb9c7y44qr1gcfrk")))

