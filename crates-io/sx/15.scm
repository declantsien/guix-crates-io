(define-module (crates-io sx #{15}#) #:use-module (crates-io))

(define-public crate-sx1509-0.1 (crate (name "sx1509") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "~0.1") (default-features #t) (kind 0)))) (hash "081mqjk6yhx69lbfsqvgrinpb1g547rji5r7bk4d6wvp4jwqb90v")))

(define-public crate-sx1509-0.2 (crate (name "sx1509") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "~0.2") (default-features #t) (kind 0)))) (hash "0jgncbpfdnlw03vnvj1kjddjk1haarmlcrhv5gvlm43lp6kpi43h")))

