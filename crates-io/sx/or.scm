(define-module (crates-io sx or) #:use-module (crates-io))

(define-public crate-sxor-1 (crate (name "sxor") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("color"))) (default-features #t) (kind 0)))) (hash "1626k6wb2hlll83yhhv4cy6yisid26wcvq42xsayy2bylpjsw64a")))

(define-public crate-sxor-1 (crate (name "sxor") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("color"))) (default-features #t) (kind 0)))) (hash "111r7hp843pl1s4rxx1p1ibmfrzbkl084g9vxapa6b78xxrai2b1")))

