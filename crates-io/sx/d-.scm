(define-module (crates-io sx d-) #:use-module (crates-io))

(define-public crate-sxd-document-0.1 (crate (name "sxd-document") (vers "0.1.0") (deps (list (crate-dep (name "peresil") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k024010ydvq2916m87n5124501wlq29jqad2crn2ppddj1niv5w") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.1 (crate (name "sxd-document") (vers "0.1.1") (deps (list (crate-dep (name "peresil") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kfjnbh8vbxpi205d8x8qw8h3j3q9sr26kn3nw3037f15sp44nwb") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.1 (crate (name "sxd-document") (vers "0.1.2") (deps (list (crate-dep (name "peresil") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m0h7w6qx1f27x775m0la32l33lq76xj71r9ylpx2drim9nxr2by") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2 (crate (name "sxd-document") (vers "0.2.0") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c004rjdl5bl9g3jv9005xdqldg7ywms548k742mg1m6gb0zrm74") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2 (crate (name "sxd-document") (vers "0.2.1") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "10f1z165x09fr4ajsxzm7b4y76xcqrmcplrvrwf06jrvrciykanm") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2 (crate (name "sxd-document") (vers "0.2.2") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p1r31k2gq9p18h7fyq1p1qnpb94r42q5dadcvxyapksc0qsm90l") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2 (crate (name "sxd-document") (vers "0.2.3") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "19rfv9isnknhhrvgfvbkj0hqd4l919rwbis31zh8i0g584zasw19") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2 (crate (name "sxd-document") (vers "0.2.4") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "07v0b2ahbgyh953dr5hpabxhmscnwqc765fqsn5i0qi3hnkjbp36") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2 (crate (name "sxd-document") (vers "0.2.5") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y03wdif5xcsci9smn53j0g4mdjxjrwnyf9xn5kcbngzcisl3iiz") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.2 (crate (name "sxd-document") (vers "0.2.6") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "18p028mjmqrgjdq0nrpcl8knssxrp9ysj7440rsq8wxifljhsdhy") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.3 (crate (name "sxd-document") (vers "0.3.0") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yd46vdvffmvibk58a96i3ln4vak3m7mvz81n3f90mhk0igsm7bz") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.3 (crate (name "sxd-document") (vers "0.3.1") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "138z1qxjygaf4kgjd90s3k4shj03vb98wmar0hqzds62an1ld7m2") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-document-0.3 (crate (name "sxd-document") (vers "0.3.2") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y10shqmy9xb73g403rg1108wsagny9d8jrcm081pbwzpqvjzn4l") (features (quote (("unstable") ("compile_failure"))))))

(define-public crate-sxd-xpath-0.1 (crate (name "sxd-xpath") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "peresil") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04b5p8sahv206mvqwdy4kirinbmnqyvj10lxgb7xsf5nyhdagl7j") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.1 (crate (name "sxd-xpath") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "peresil") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "042f1rcfmqpf6rh4nsrsc535p2zbkvbn05sldwyzk3xpzqiby2i5") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.1 (crate (name "sxd-xpath") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "peresil") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14hak02x5fcnzgcnhgvd5mj5qb8gldmqzij0h14aibwsd5v04b3k") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.2 (crate (name "sxd-xpath") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "051lmy1asprcnxn2mmnyd4117131ac8bxgv5gph6fiddhwxjvcb9") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.2 (crate (name "sxd-xpath") (vers "0.2.1") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0z9pxd29dlmdf15bfrwap83ml8k6gwrk70j53055r1ml4cn5xpdp") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.3 (crate (name "sxd-xpath") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qwvgrc5llllys6ay5k5ajr7kpn6kbm2sl9qn2pbqsm2w736xnys") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.4 (crate (name "sxd-xpath") (vers "0.4.0") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1zc5z6k5dbxj5qixgjjgqdw7rgh0xcw5a91zpd8kngrhpar7klpr") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.4 (crate (name "sxd-xpath") (vers "0.4.1") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1gzyf1h62n09fv25c7dja6yzsfs0qk4p6i69bpm1mi0zahp8rkb5") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-0.4 (crate (name "sxd-xpath") (vers "0.4.2") (deps (list (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req ">= 0.2, < 0.4") (default-features #t) (kind 0)))) (hash "1sin3g8lzans065gjcwrpm7gdpwdpdg4rpi91rlvb1q8sfjrvqrn") (features (quote (("unstable"))))))

(define-public crate-sxd-xpath-visitor-0.4 (crate (name "sxd-xpath-visitor") (vers "0.4.3") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "peresil") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sxd-document") (req ">=0.2, <0.4") (default-features #t) (kind 0)))) (hash "1qwz1i9scvbs1m0famiirzlgqcv520r4vjm5vgfl8y6f7i1jm4fm") (features (quote (("unstable"))))))

