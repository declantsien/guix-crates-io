(define-module (crates-io sx s-) #:use-module (crates-io))

(define-public crate-sxs-manifest-0.1 (crate (name "sxs-manifest") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "14j1w2kixxfz54bnv2bk58hs0h3ycj3l9pwycrlwin65j68x43i6")))

