(define-module (crates-io sx #{13}#) #:use-module (crates-io))

(define-public crate-sx13xx-conf-0.1 (crate (name "sx13xx-conf") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0h89hgxwrxcdknyrhyjd8xg0a97kn1k6c8j0571s2an5gfjdhv0z")))

