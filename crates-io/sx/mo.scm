(define-module (crates-io sx mo) #:use-module (crates-io))

(define-public crate-sxmotify-0.1 (crate (name "sxmotify") (vers "0.1.0") (deps (list (crate-dep (name "directories") (req "~4.0") (default-features #t) (kind 0)))) (hash "0c4yc6v3lcsyfa0zp11dm7x4ggla89hb5kdmrqb2sxm2vkbs7z93")))

(define-public crate-sxmotify-0.1 (crate (name "sxmotify") (vers "0.1.1") (deps (list (crate-dep (name "directories") (req "~4.0") (default-features #t) (kind 0)))) (hash "0faqr7bha72wld5vidcx2ik439h8qxijxx8762ngfn1bpwr443hk")))

