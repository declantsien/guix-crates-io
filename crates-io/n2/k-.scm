(define-module (crates-io n2 k-) #:use-module (crates-io))

(define-public crate-n2k-base-0.1 (crate (name "n2k-base") (vers "0.1.0") (hash "0q9m8facsl26bz9n8b8c2hmkwwrf6k5adnxpmnyhyx9zgmsfa0gw")))

(define-public crate-n2k-base-0.2 (crate (name "n2k-base") (vers "0.2.0") (hash "07jfxlv1qzzc66cw5cagidz8lgp4zydcbnwp3sf49fypxmfqsxms")))

(define-public crate-n2k-base-0.2 (crate (name "n2k-base") (vers "0.2.1") (hash "1slylrmpqk5yvh4lbby0hcl99m4ypj0l6habwvjhynlnb4xiiwwh")))

