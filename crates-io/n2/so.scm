(define-module (crates-io n2 so) #:use-module (crates-io))

(define-public crate-n2soc-0.1 (crate (name "n2soc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19hydkjygi8hzh8w4qzf3jwrjqjhgddpq0f1xi8xajxfgz4c7pbv") (rust-version "1.71.0")))

