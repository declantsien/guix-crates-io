(define-module (crates-io n2 qr) #:use-module (crates-io))

(define-public crate-n2qr-0.1 (crate (name "n2qr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "hex-rgb") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "qrcodegen") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0ynny70sraw5dmvr13i3094adzywjh06qrg17w36fx9pf82r0rz2") (rust-version "1.61.0")))

