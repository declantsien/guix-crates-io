(define-module (crates-io kh tm) #:use-module (crates-io))

(define-public crate-khtml-0.1 (crate (name "khtml") (vers "0.1.0") (hash "1gqq0im32vcdjqw75vsq6djfcybkxsa2f75h2ryc626lxn8b4rsk")))

(define-public crate-khtml-0.1 (crate (name "khtml") (vers "0.1.1") (hash "0r33smb8vsfn7cs3p6hbm5g0bfrpcs2pzg5k38m3rkh3qgp12fxs")))

(define-public crate-khtml-0.1 (crate (name "khtml") (vers "0.1.2") (hash "1swc5inij8i2rf8rma2s8kycnv4ysq5dzmilrg0brffvadj4zg9q")))

(define-public crate-khtml-0.1 (crate (name "khtml") (vers "0.1.3") (hash "0ka6i8q7bas6k8g15hs0d5j09a467ljng10fv9dwh5wq02idi0k5")))

(define-public crate-khtml-0.1 (crate (name "khtml") (vers "0.1.4") (deps (list (crate-dep (name "html-escape") (req "^0.2") (default-features #t) (kind 0)))) (hash "10l3wz3lzhbbs8pjs5gm7pdhb1mrfnxwaj9nj4in3nih8kwiidb1")))

