(define-module (crates-io kh ak) #:use-module (crates-io))

(define-public crate-khaki-0.1 (crate (name "khaki") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "hmac-sha256") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "13vf4mh7a0xzwldpijbcbpn7zl6kp8kbgh4x7b83xpq7zqgb0xy3")))

(define-public crate-khaki-0.2 (crate (name "khaki") (vers "0.2.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "hmac-sha256") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1afhq4gshigxrx9k3sjbd2gf164bfk63997d0w7zsi9a4dlb48c1")))

