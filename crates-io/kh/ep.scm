(define-module (crates-io kh ep) #:use-module (crates-io))

(define-public crate-khepera4-sys-0.1 (crate (name "khepera4-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "snafu") (req "^0.7.1") (kind 0)))) (hash "1achz2471wvsm3n8122jbcxx39jn4amyh3yd2qjpmhb234r2abmc")))

