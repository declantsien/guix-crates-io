(define-module (crates-io kh ip) #:use-module (crates-io))

(define-public crate-khipo-0.0.1 (crate (name "khipo") (vers "0.0.1") (hash "1ca2gqs9dhly2c7f5fzph6gpi60id0gywf1fmz6b5pqigjjjgazf")))

(define-public crate-khipu-0.0.6 (crate (name "khipu") (vers "0.0.6") (hash "1izr8rcjdh0nz73nr99k34f9fa14d92g5kpf4gsxymhypw0v4hnq")))

