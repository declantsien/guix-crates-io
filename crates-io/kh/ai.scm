(define-module (crates-io kh ai) #:use-module (crates-io))

(define-public crate-khaiii-rs-0.1 (crate (name "khaiii-rs") (vers "0.1.0") (deps (list (crate-dep (name "khaiii-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mr25fx4d0k4dbgxblpddbfmpa6zk12i4cl1pkcparjcc5fqgq4g") (features (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1 (crate (name "khaiii-rs") (vers "0.1.1") (deps (list (crate-dep (name "khaiii-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1630c9gjksfkd31l802clyrjk49g10jjrbap2ksvkz3yjpwafbz3") (features (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1 (crate (name "khaiii-rs") (vers "0.1.2") (deps (list (crate-dep (name "khaiii-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14sdzcxyash9hzls7rlfqg1ns8jfnjnpzg9f0zi23nmc79bnlxih") (features (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1 (crate (name "khaiii-rs") (vers "0.1.3") (deps (list (crate-dep (name "khaiii-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qk4v5bw8m5cs28ikk7bm320v7zxb66br48p3r7rkiyasw3bbcfh") (features (quote (("vendored-khaiii" "khaiii-sys/vendored"))))))

(define-public crate-khaiii-rs-0.1 (crate (name "khaiii-rs") (vers "0.1.4") (deps (list (crate-dep (name "khaiii-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (optional #t) (default-features #t) (kind 0)))) (hash "1vsfx2gg1zk3k8gs3xbdk0n458qy4kihya2paik9yik3l7v87if9") (features (quote (("vendored-khaiii" "khaiii-sys/vendored") ("serde_support" "serde" "serde_json"))))))

(define-public crate-khaiii-sys-0.1 (crate (name "khaiii-sys") (vers "0.1.0+0.4") (deps (list (crate-dep (name "bindgen") (req "^0.62.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 1)))) (hash "1rq0iqcxjnabk427w3v7sy3nkywymy6wm8xwrbx50cxd9id2gd37") (features (quote (("vendored")))) (links "khaiii")))

