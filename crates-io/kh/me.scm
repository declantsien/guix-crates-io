(define-module (crates-io kh me) #:use-module (crates-io))

(define-public crate-khmercut-0.1 (crate (name "khmercut") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crfs") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0w567lalgcfrrblvd5px3vk432znx7bdsqiz6igrcawyryvi5s3m")))

