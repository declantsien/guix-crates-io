(define-module (crates-io kh at) #:use-module (crates-io))

(define-public crate-khat-0.1 (crate (name "khat") (vers "0.1.0") (hash "0kdfyzfy7spvn8vk532lv8ay0h4fia9in24gbq5zch8jwygw66mi")))

(define-public crate-khat-0.1 (crate (name "khat") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1z5zc10hcivlbfidypn8mw7yj4smib0gcx9xyglb4j8n9db5r0ic")))

(define-public crate-khat-0.1 (crate (name "khat") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0k4y4p1gph91ywapc2mwvmkr2k3qw3fm5a0qig92c33817m6np6b")))

(define-public crate-khat-0.1 (crate (name "khat") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0205f122ni4cnr830daln0pyh44kdf02hkzhfnwz5f28v3ki3qpk")))

(define-public crate-khat-0.1 (crate (name "khat") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "014ybbvjan7glilbzhvnsyv9775pxaiw53iamxd65gia6wmysc7q")))

(define-public crate-khata-0.0.1 (crate (name "khata") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.11") (default-features #t) (kind 0)))) (hash "031f582a4jpvflshdxdxyccmjfdlsxi7kfz69f6p4c4g6lvq475l")))

(define-public crate-khatson-0.1 (crate (name "khatson") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "tch") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ka66n29x7lkxkrrvp2dkn9g22wf834xy80myw4aak1mxjha7wrq")))

(define-public crate-khatson-0.2 (crate (name "khatson") (vers "0.2.0") (deps (list (crate-dep (name "chawuek") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1r6ik5chrjn4ilwxzr7vm7fkjgqirxdiqn23q8l37nivzf7q0v3s")))

