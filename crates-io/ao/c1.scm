(define-module (crates-io ao c1) #:use-module (crates-io))

(define-public crate-aoc19intcode-0.1 (crate (name "aoc19intcode") (vers "0.1.0") (hash "1ij9lgczkpk490c00np9p2r3w3bffxhihnd3ddnp213sq52slskx")))

(define-public crate-aoc19intcode-0.1 (crate (name "aoc19intcode") (vers "0.1.1") (hash "14ma0xmasi7s2cm13g7wp2nzpb22bdvj0yqxc6654v83w8xrsp2y")))

(define-public crate-aoc19intcode-0.1 (crate (name "aoc19intcode") (vers "0.1.2") (hash "13p3z25rbvcgwzcwbsri4n2bxi36gr2vf2w1q4wd6wkjvbgvmq9s")))

(define-public crate-aoc19intcode-0.2 (crate (name "aoc19intcode") (vers "0.2.0") (hash "1nwkl4wk8pyj8plq6z0cwyrdxwxfzbqgnlmz93ryda2mhdgl103b")))

