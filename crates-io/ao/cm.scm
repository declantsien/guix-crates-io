(define-module (crates-io ao cm) #:use-module (crates-io))

(define-public crate-aocmd-dl-0.1 (crate (name "aocmd-dl") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "html2md") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("rustls"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "029wjx63ix2ajsjhhzhzb7jlfvckbfa042vr9yvw8ysrcb0kzc2w")))

