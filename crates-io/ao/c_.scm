(define-module (crates-io ao c_) #:use-module (crates-io))

(define-public crate-aoc_2018_state_machine-0.1 (crate (name "aoc_2018_state_machine") (vers "0.1.0") (hash "1h26zb2s0n8f5nwxkmicdacb3wr8xalk65j0r7632fsjnrz41gxw")))

(define-public crate-aoc_2018_state_machine-0.1 (crate (name "aoc_2018_state_machine") (vers "0.1.1") (hash "0ivagkjr9jy1c48vv88wznf2xwmgs88gvlwpi8naml87lnlxh26a")))

(define-public crate-aoc_2022_01-0.1 (crate (name "aoc_2022_01") (vers "0.1.0") (hash "0r45yfcfva3f74v54sw7bzxabbqalhjxc9wvkpx968l8kx5pvzy2")))

(define-public crate-aoc_codegen-0.1 (crate (name "aoc_codegen") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.21") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k8hx2xvhid85v0f2kh1r00qnrc7kbrw3a8jrwhksa7k6f2sbyy1")))

(define-public crate-aoc_codegen-0.2 (crate (name "aoc_codegen") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.41") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qyfc4s9j4bp8y1r7bkk04fipzjhx8pp6m2hgrccq51dv5l3rzcz")))

(define-public crate-aoc_codegen-0.6 (crate (name "aoc_codegen") (vers "0.6.1") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.41") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19x9m2vhyhkhngj1xv10f206p92zj28jjdiid7js5sj5vx44mx0c")))

(define-public crate-aoc_data-0.1 (crate (name "aoc_data") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req ">=0.99") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req ">=0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req ">=0.10") (features (quote ("blocking" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req ">=1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req ">=1") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req ">=0.9") (features (quote ("client" "gateway" "model" "rustls_backend"))) (kind 0)) (crate-dep (name "thiserror") (req ">=1") (default-features #t) (kind 0)))) (hash "1kd5xs1ccl4d5xj61czkrr9lsdbig5xqk9lhr8f47l8r41h1mf1i")))

(define-public crate-aoc_discord_bot-0.1 (crate (name "aoc_discord_bot") (vers "0.1.0") (deps (list (crate-dep (name "aoc_data") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req ">=0.9") (features (quote ("client" "gateway" "model" "rustls_backend"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("time" "macros"))) (default-features #t) (kind 0)))) (hash "19b2imf4mh880ndpxmlqfspi0fv3h9ijbyxmh1wgjcsmrcfclkqh")))

(define-public crate-aoc_discord_client-0.1 (crate (name "aoc_discord_client") (vers "0.1.0") (deps (list (crate-dep (name "aoc_data") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req ">=0.9") (features (quote ("client" "gateway" "model" "rustls_backend"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("time" "macros"))) (default-features #t) (kind 0)))) (hash "04i1kwgzvrs49mcnbap83sdwsnyv3n8kg9sbcrkj7lz01sglsbnr") (yanked #t)))

(define-public crate-aoc_discord_client-0.1 (crate (name "aoc_discord_client") (vers "0.1.1") (deps (list (crate-dep (name "aoc_data") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req ">=0.9") (features (quote ("client" "gateway" "model" "rustls_backend"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("time" "macros"))) (default-features #t) (kind 0)))) (hash "1lkhxf2a8yh79mq6lrlwd61xsp4kgfkbl0q32zg3rcivf0469lxy") (yanked #t)))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.0") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1dmax9v2hmgcpqzkykr7vs4w3wjfkfcl4yla27bav9gxrfbx6vmc")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.1") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0bf1s5gm5x58blb6rqnrsnbq5wr7fb6b4szfdpy1r1b1wfkcghxk")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.2") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0611fiydrr8alnwsd4gzxlw46ya3rq92jlpscam4am3pgq4a4iph")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.3") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0r0h5nn8f5wb6g1h8rmb7k098kw2nlnqi2md12x01f9vqjvwn4bh")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.4") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0ag7lf4sfk0i6x15mjyl7phc9m1il9l7j61j2ydn8w51ih4gvqsd")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.5") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1l9n9kfwx1sbg23g8plmq273s394ivdxbprj1b9chinxrn9h4m2w")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.6") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "13n6xk5m6i5jyg5f26rwy61rjpmi7kx89jdr3ibhbjafmidb1k01")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.7") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0fxsqdrigglj2pcb8dlay1vrcy2ldp4qmzqvipr9j94jqd00dha9")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.8") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1xiz51bas0s8n4a54498rbi31ka20v0przfmkbv1anrhvxayn2gq")))

(define-public crate-aoc_driver-0.1 (crate (name "aoc_driver") (vers "0.1.9") (deps (list (crate-dep (name "aoc_macros") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0pdsy99hp9sckbqc18psfgh4bp019pqqb16bkb2zz9hgzm6sa6f9")))

(define-public crate-aoc_driver-0.2 (crate (name "aoc_driver") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0hc0jb19icic6nn7wxj5n4nlvkxa9sjdkxwxaky2sdq9z33d3fbx")))

(define-public crate-aoc_driver-0.2 (crate (name "aoc_driver") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "06w2ypwg8d60gr6kgbhyib02jmpg1g1n8g4r0d582mzjvm3fghkr")))

(define-public crate-aoc_driver-0.2 (crate (name "aoc_driver") (vers "0.2.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "04l8slawcl5c1s99jyplp76l4h1gm87617r5sykmvi8r9jlzpnn9")))

(define-public crate-aoc_driver-0.2 (crate (name "aoc_driver") (vers "0.2.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1qxzyqjk6q07fyf3vmpsiw0ppb64bkd43ji00nq7pms4b2gvx6i8")))

(define-public crate-aoc_driver-0.2 (crate (name "aoc_driver") (vers "0.2.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0ln4bbh36nlqyvbc71x6237xysjv7a3gn7xhp195mwpc1pqjfk6i")))

(define-public crate-aoc_driver-0.2 (crate (name "aoc_driver") (vers "0.2.5") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "109mzhm5231d148nb2nwk8avjgc7bqhndjvhz1fkbhfg4q5xcabi") (features (quote (("local_cache" "serde" "serde_json" "chrono")))) (yanked #t)))

(define-public crate-aoc_driver-0.3 (crate (name "aoc_driver") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "09dajrkl0gs0i72vj6znp3vm8cmk7x3l9cp7vay2ikr2xahm9al5") (features (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3 (crate (name "aoc_driver") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0apys58hbmsjis5zzd2x1ph4l8z27b1ya223k9ccpfxzjlxcaphd") (features (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache")))) (yanked #t)))

(define-public crate-aoc_driver-0.3 (crate (name "aoc_driver") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1d75jsg0yinqf6pjfd3ri2fm1n6izmh54gm12fjbqkh4rs4glnls") (features (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3 (crate (name "aoc_driver") (vers "0.3.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0kz09f9jscx2h3lzjcpirgsrhfmk1ml581dvnbbz8dzbv85wy129") (features (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3 (crate (name "aoc_driver") (vers "0.3.4") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1gh89vnmahyxzi6hpflmg6dsxcgcrqqwvkdism1v5j21p1wqnmhc") (features (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3 (crate (name "aoc_driver") (vers "0.3.5") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "16fd08mhpxb0s6lh9fhh7pzk4ag9qdpc0lc3w2hs30mrsv4gd8vf") (features (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_driver-0.3 (crate (name "aoc_driver") (vers "0.3.6") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "std" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "168hpk3sz8lcns730zlgwxmcs529iha960lpvq8d7fymkppmp8bh") (features (quote (("local_cache" "serde" "serde_json" "chrono") ("default" "local_cache"))))))

(define-public crate-aoc_input-0.1 (crate (name "aoc_input") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0c63r2k57sivbica2yf627rbp23l98wl241zas8gf3gldpa2gjvb") (v 2) (features2 (quote (("cli" "dep:clap"))))))

(define-public crate-aoc_input-0.2 (crate (name "aoc_input") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0qxi9bnsvjc7kpabq6m6l4azqhcwxlwm6w2dxywpd5n3hyjb7hdz") (v 2) (features2 (quote (("cli" "dep:clap"))))))

(define-public crate-aoc_input-0.2 (crate (name "aoc_input") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "044fsn636wachcg31dy0bj7v3raqcnpzvrq3cz6ijmb4hd6ddmp1") (v 2) (features2 (quote (("cli" "dep:clap"))))))

(define-public crate-aoc_input-0.2 (crate (name "aoc_input") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "04i7dya466h9v8cc8mnddgdaz4sjxz7d9apd2j619sf40cn9y4c2") (v 2) (features2 (quote (("cli" "dep:clap"))))))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0kk6vxlp0n3w244najx1xkw8qbhamjnbzgag0yg98l6c24zx1ryr")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0j492z6z1mix89z0wchmynkalkc7dc7p92s7pm7x30bcg9z7zkh1")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "11v5wicxx2c4yz9q73wk8d52z7v2wisvw57gmsgx14kv8lpfqf8a")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00a7xqmr5gkq7v21sn7p06839p75l0q2qqbmmnmnrvjp0dljdvpm")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1zjilsxsx2i8zjlrcgya4w8hkmn9zbwrkwr3h7ypibh4w68wzzi5")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1cqxbyll02l1dzqsfv5ix6411d46dc9kkryf9b1c3z0b14g809il")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fcqmqlyxpldiij47iybbkp9nw42ki0ydg9fgmiw1ksdqlvz5rr2")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "05jg2k9yip0ks2r948idaggh0hmnc9brj51p9727g0ra9b6y7jgi")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1dv570idmrj8sjsybh2g2c7a2va1p0xkxf42x86g5j7213zc3h9n")))

(define-public crate-aoc_macros-0.1 (crate (name "aoc_macros") (vers "0.1.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0hp6m7s1j0j6mxnrf1kn04bqx7j2r809gwpv296gvmpl71jmcqww")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.0") (hash "1japw0yvzmnyscc05qvfwabjg1zmngbp5gqnmwzw4icw47hihbbf")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.1") (hash "0m64r86qrlxsdhnm890krfd1yk878m7b6cf6hs3rc1f9cwy37d3a")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.2") (hash "0sbsnafzff2yvg0jvl672nr792iy03aaygp0xi1ymvwm86c8qmzr")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.3") (hash "1zxjwj7vj8ms554xcajxh3p0xfzqn6c0sgvvslay0886hhyai7vf")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.4") (hash "0rz4xrw70pn7mwc69vpq2i8qiy1b2yxjkiqbva3i2zzvky197wyx")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.5") (hash "0l7qp79nsa7x0sq9fc45cdcwc3phqc5n0lg6lna2aczfg5p1z63x")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.6") (hash "0x5jslv133ynyf4jwmfis9l5rhn6638nlhsr8vkl9x7mvanwx64c")))

(define-public crate-aoc_utils_by_nifalu-0.1 (crate (name "aoc_utils_by_nifalu") (vers "0.1.7") (hash "1bar6izy5c4ywyg78c02fqh30q9f7ynrfi5maw3mgspb1n3xj5ik")))

