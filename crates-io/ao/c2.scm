(define-module (crates-io ao c2) #:use-module (crates-io))

(define-public crate-aoc2021-0.1 (crate (name "aoc2021") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0a88sx5a5qbg03bzks6pqcw12ad0v8cwig2g9clbwgiszysjnw9g")))

(define-public crate-aoc2021-0.1 (crate (name "aoc2021") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0wzzfwk655y1nv45if6z5690jcg7d8z1nzzfkbdc7n8gljsjyv6q")))

(define-public crate-aoc2021-0.2 (crate (name "aoc2021") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1i9q221yzyzr16y8yrr88lzh9lq13vcd0ay2y9wq765604cdz78s")))

(define-public crate-aoc2021-0.3 (crate (name "aoc2021") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0dcxnq5jgjf6rsmjpkffbfgpdr95nh2bc607y1wl1ha498nr4735")))

(define-public crate-aoc2023-0.1 (crate (name "aoc2023") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0hbb11fnmxi9wqz3l3gn3rhig9plgrgxx396s65wjjx9la8k3yb7")))

(define-public crate-aoc23_parser-0.1 (crate (name "aoc23_parser") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "152pkfb5zgxn6piz9w84lr3hqv8dn4s16y0335pxrgmzqan09ayh")))

(define-public crate-aoc23_parser-0.1 (crate (name "aoc23_parser") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0w4998wbqkwrmwqvchbcvbhdbfgwm0p19aqiq909mp0172hmw5bw")))

