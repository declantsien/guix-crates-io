(define-module (crates-io ao sp) #:use-module (crates-io))

(define-public crate-aosp-missing-blobs-0.4 (crate (name "aosp-missing-blobs") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)))) (hash "1zv8sdbknlpkzgdn97a00xz3ap1y727c4mslaw8w8rfjwx98h4mh")))

(define-public crate-aosp-missing-blobs-0.5 (crate (name "aosp-missing-blobs") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)))) (hash "1hxky0j3kjzcw7h98ink7bn4qhhm8mrnyhha2fajmwnck5fpqjfw")))

