(define-module (crates-io ao -s) #:use-module (crates-io))

(define-public crate-ao-sys-0.0.1 (crate (name "ao-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1yz749dh7wyss8ji271a2wlf5nj28ibs6qizydnixxm514lb4ihl")))

(define-public crate-ao-sys-0.0.2 (crate (name "ao-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0jcg1rks92aa0skfd9b4hbhmjsknccpb6f2b9l8zvamzjgznqn9l")))

(define-public crate-ao-sys-0.1 (crate (name "ao-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "089d6fljy9fvxs0qg4z3aqyhjp3jkgc433fmi64ckasd928xsh64")))

(define-public crate-ao-sys-0.1 (crate (name "ao-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0j73ni30cliidmq69k4vi1hsjhk5rkb9wf7qm62zw2saflpak4aq")))

(define-public crate-ao-sys-0.1 (crate (name "ao-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "10gvxr2lyff757vijhr4kyxw2q4yfpd1w2mk2ghi0sp2drcwbw8f")))

