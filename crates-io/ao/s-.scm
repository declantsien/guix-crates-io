(define-module (crates-io ao s-) #:use-module (crates-io))

(define-public crate-aos-specs-0.1 (crate (name "aos-specs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "04pa696h3wjyy6hf6rzyh06yy820vd8hkirsv3a57jhl1gdxam6g") (yanked #t)))

(define-public crate-aos-specs-0.1 (crate (name "aos-specs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1fhqpz6qjv1k50mmsm5vlgskcy6hn5fgldx4vr4r6vb8g2xdfgz3") (yanked #t)))

