(define-module (crates-io ao ru) #:use-module (crates-io))

(define-public crate-aorura-0.1 (crate (name "aorura") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0bgqcljrm2v8dxhbnbg8cddwrv2rscjbriqz4ibfhajb49y4knbz")))

