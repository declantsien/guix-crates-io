(define-module (crates-io ao ve) #:use-module (crates-io))

(define-public crate-aovec-1 (crate (name "aovec") (vers "1.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1rf16hzcwv60wyhks8940naiq2rq8wcpzyvckwh88gcln42p98aw") (yanked #t)))

(define-public crate-aovec-1 (crate (name "aovec") (vers "1.0.1") (deps (list (crate-dep (name "parking_lot") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1nfsi8h3nkxnys9bzj5a3xsh70nisydwahp7l1d5vp030qwqcqrd") (yanked #t)))

(define-public crate-aovec-1 (crate (name "aovec") (vers "1.0.2") (deps (list (crate-dep (name "parking_lot") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1yghwrkg5mbfbag0i6xv0yn4shxb8fhi66ir3nc36kmgc7dm61cc")))

(define-public crate-aovec-1 (crate (name "aovec") (vers "1.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1n8wg6mjbq1r2wskc17y5r8kfajdimbs93zxrfdjvm1vyy6xxkz6")))

