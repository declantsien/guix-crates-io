(define-module (crates-io ao tt) #:use-module (crates-io))

(define-public crate-aott-0.1 (crate (name "aott") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1l0ggbq9yxh5rniaf8957v7g64i4wic6acpg88a3palginx458a2") (features (quote (("std") ("default" "std" "builtin-text") ("builtin-text") ("builtin-bytes"))))))

(define-public crate-aott-0.2 (crate (name "aott") (vers "0.2.0") (deps (list (crate-dep (name "aott_derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "unicode-ident") (req "^1.0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zstd-safe") (req "^6.0.6") (features (quote ("std"))) (default-features #t) (kind 2)))) (hash "0w2n7g9kwl6vjlfgbqpg0kc6qxqp122fwg4smnqrzgcxw14kjkw9") (features (quote (("std") ("error-recovery") ("default" "std" "builtin-text" "error-recovery") ("builtin-bytes")))) (v 2) (features2 (quote (("builtin-text" "dep:unicode-ident"))))))

(define-public crate-aott_derive-0.1 (crate (name "aott_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0nzbrr7afkb3300a9xrw86cryii3gxib6ri77kfi60j539qk1mbl")))

(define-public crate-aott_derive-0.2 (crate (name "aott_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yh1lhbpv3jk8g20yx8nxcxxsiqicffa7m9hb84h6c1spwdmx6n0")))

