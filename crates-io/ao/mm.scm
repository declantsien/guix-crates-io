(define-module (crates-io ao mm) #:use-module (crates-io))

(define-public crate-aommap-0.0.0 (crate (name "aommap") (vers "0.0.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0crkyq34jcnqxpf1bpyg4hg4q4qd1cxd4574h1xdfrnmr2n2zm35") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1 (crate (name "aommap") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "166yhbml8s69szhyb6s2clw6fjari6dn1h5q3zslx4yzm2pq3zfz") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1 (crate (name "aommap") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1vy5s9msr1cg6r6j9zvqcv4xdhrq4wml9f2094gi1xw6d2p2z71l") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1 (crate (name "aommap") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "14cswrwxpi8cln0gviv5vr08nw7kh2jk46waqwcdgkz6j85cn308") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1 (crate (name "aommap") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "197yp8h3iqa2xl2c1crxx8kh4sd13lyz94jagjhiz80mk6j2v87b") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1 (crate (name "aommap") (vers "0.1.4") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0i223whrdr0fv8jpniadx2xvabzgd9wwg7fcbz7l3c8ypgzshx02") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1 (crate (name "aommap") (vers "0.1.5") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0xah95aqwcdpr966v8xc8a1a3n2791flimn3xvivn902d7h5nsa3") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.1 (crate (name "aommap") (vers "0.1.6") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0p8m0wn5qm4w3vvgzc1vswr4f2dn106hlgmvymcm9vx96bm074wn") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.2 (crate (name "aommap") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "09kjam6nvizwk5fpyms1v24c3wnwcllcy17d5v4qli64508vcmvg") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.2 (crate (name "aommap") (vers "0.2.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "052idwz1i4ppzd47jg03pc54wxvzmb36s87imwpm7h4qk0dxmyfy") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.3 (crate (name "aommap") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0p2irrf07vh52h0rbyvyz7f8i5jq8z7dvqck8l1546njqkzn4nic") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

(define-public crate-aommap-0.3 (crate (name "aommap") (vers "0.3.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "memmapix") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0qy1zx8d6jkmg2ykhcrq48wj1rqg73ga0lbh838wc0cpf51mn0f1") (features (quote (("std" "bytes/default" "memmapix") ("default" "std"))))))

