(define-module (crates-io ao sa) #:use-module (crates-io))

(define-public crate-aosa-0.1 (crate (name "aosa") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c7iqb8h4pxl2bhg87l3p30bbily1p0yg2v35kca46s812imxb2q")))

(define-public crate-aosa-0.1 (crate (name "aosa") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f93c6xsjlbbi8cbrqivljvfn0ng9c0gmp56jkghszswg8dj3r0y")))

