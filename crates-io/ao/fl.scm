(define-module (crates-io ao fl) #:use-module (crates-io))

(define-public crate-aoflagger_sys-0.1 (crate (name "aoflagger_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0d5kj3r5ifbkal63dy6azkgs8mpwm1szlm6y93gwifxy99wn5wk1")))

(define-public crate-aoflagger_sys-0.1 (crate (name "aoflagger_sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "1z5axgq7w4iyfkx6ymmccd7pg89zv06r2i0gkkxvl4rx5z4d077q") (links "aoflagger")))

