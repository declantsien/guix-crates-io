(define-module (crates-io ao li) #:use-module (crates-io))

(define-public crate-aolifu-rust-0.1 (crate (name "aolifu-rust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1kw70qwiki37plnq50sbg3xg7fb0p1z6wxmgnlvvf1nwnywh2spf")))

