(define-module (crates-io ao bs) #:use-module (crates-io))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "09a8xfm780nh6b00v1n3vszrkm4ig38ki2frahccgg2mb8mdgld3") (yanked #t)))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "02nci9vhk4sndlih6mslz7nd7q105mhm30y0ag18kgnmdmar32sv")))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.2") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "16hllm16i5r03mcxl2z56hpwazn9qcvchz04y6ssjjwnas6hsks1")))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.3") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0i9zbqw2za9hhkvhsqfrf8vf8zdmzg08wa7v2ni40mqchql9762g") (yanked #t)))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.4") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0r5pcmc5r9h8vr40ln2barx1092dw3zg1hk0m3hz9m6si7k23abk")))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.5") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0dlcw260y6xdsmd9g0i7yq77bpm4i6nispxa6jxgdw4a9gvy8cfg")))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.6") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0adkvjhv39r7rwirf2xny7byzdsi4fp7cimh3whyg968hwyyvpd8")))

(define-public crate-aobscan-0.1 (crate (name "aobscan") (vers "0.1.7") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1q6jyd8qxz30bsz7wcgmap8bhfqk7smdzsmagpv803z8dy8c9j8v")))

(define-public crate-aobscan-0.2 (crate (name "aobscan") (vers "0.2.0") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "06wnv7flin1w2qdcnjh4n9ccj0xcjqxw3n44ai9id37w3slj5km3")))

(define-public crate-aobscan-0.3 (crate (name "aobscan") (vers "0.3.0") (deps (list (crate-dep (name "num_cpus") (req "^1.14") (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.29") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0wnbaqlf3i3glsxz2yiyl05bbm773c6a3qljvbh589nx3c6j9s8d") (features (quote (("default" "object-scan")))) (v 2) (features2 (quote (("object-scan" "dep:object"))))))

(define-public crate-aobscan-cli-1 (crate (name "aobscan-cli") (vers "1.0.1") (deps (list (crate-dep (name "aobscan") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "1slfxh8lal21ni9v6w0xmbjndgn22bajms82rwn45nph1bw8p8ir")))

