(define-module (crates-io ao nt) #:use-module (crates-io))

(define-public crate-aont-0.1 (crate (name "aont") (vers "0.1.0") (hash "1rpxb83lqrbh956plyi09cr65ndh0f1vlx76pkl415aya0fz2gdd")))

(define-public crate-aont-0.1 (crate (name "aont") (vers "0.1.1") (deps (list (crate-dep (name "digest") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9.6") (default-features #t) (kind 0)))) (hash "1x4f6wyvz1zwq0pkkh75cr8cbwhnryw53ikzwpy8ng2xw9wgf9bw")))

