(define-module (crates-io ao i_) #:use-module (crates-io))

(define-public crate-aoi_macros-0.0.0 (crate (name "aoi_macros") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jiz13m4bwzzxa3is8snn494hqx4pjc105774y7hqjxdn320c4h5") (yanked #t)))

