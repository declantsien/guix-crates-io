(define-module (crates-io es qu) #:use-module (crates-io))

(define-public crate-esquel-0.1 (crate (name "esquel") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "00lf800is17g5xszffn0blha93k4hkhkircqxzc418326pc167ib")))

(define-public crate-esquel-0.1 (crate (name "esquel") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0x0spjyyq9yk5559giiqflfyn7yhdm4f118x88drbpji7xacg2za")))

(define-public crate-esquel-0.1 (crate (name "esquel") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "metaquery") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1a39qpnjc4z79x8s5csrddwjxlz0zgn26bj3ypa4f5i4gkbaiagn")))

