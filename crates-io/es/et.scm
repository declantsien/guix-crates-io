(define-module (crates-io es et) #:use-module (crates-io))

(define-public crate-eset-0.1 (crate (name "eset") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rkyv") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "03sbdgi4majj24c6qhj1jrvb8di300hlj2gw1v95l5dzgh2sc95i") (features (quote (("default"))))))

