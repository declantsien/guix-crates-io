(define-module (crates-io es ch) #:use-module (crates-io))

(define-public crate-escher-0.1 (crate (name "escher") (vers "0.1.0") (deps (list (crate-dep (name "futures-task") (req "^0.3") (default-features #t) (kind 0)))) (hash "0815czhv089l5rni6hm25aabs4s54fd2yfsklzhngl861fzpkp32")))

(define-public crate-escher-0.2 (crate (name "escher") (vers "0.2.0") (deps (list (crate-dep (name "escher-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "futures-task") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ypjh5rqrlbni510da7g2801cxjqp1ih25dzsbwqhrwd2f11pdkn")))

(define-public crate-escher-0.3 (crate (name "escher") (vers "0.3.0") (deps (list (crate-dep (name "escher-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures-task") (req "^0.3") (default-features #t) (kind 0)))) (hash "08jfp0wc9hc3dwh7vr76djv7g5dkhb4p8ay83451y1358f1j127i")))

(define-public crate-escher-derive-0.1 (crate (name "escher-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aayfvnfvjnx9xgdz0nhk70619hh3vwkgjjiw6gw7ysbwnxily00")))

(define-public crate-escher-derive-0.2 (crate (name "escher-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m930kdyfv6sfnlkc8z6vkvbmvmxq2kslw29lmvl7f9272gh7mgr")))

(define-public crate-eschudt_consul-0.1 (crate (name "eschudt_consul") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "hyper-openssl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0gsd477x2ykqfqy2xclkwzhcrz26adj3f5hxqzb8vpb8c60ipwzs")))

