(define-module (crates-io es ph) #:use-module (crates-io))

(define-public crate-esphome-0.1 (crate (name "esphome") (vers "0.1.0") (deps (list (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen-pure") (req "^2.3") (default-features #t) (kind 1)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "085jadpmijf0rxklzqs3b3rv29aqnqagds6bsqs9mb6ky9mz2qfk")))

