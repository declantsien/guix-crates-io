(define-module (crates-io es cv) #:use-module (crates-io))

(define-public crate-escvpnet-0.1 (crate (name "escvpnet") (vers "0.1.0") (hash "04acqadjx0scxm8v6vi95dk6sr7m0w5f3g873z8xwyw77hmg15cy") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.1 (crate (name "escvpnet") (vers "0.1.1") (hash "12359sq759j5fl70s1khjzyy5fn63m4cal6ig8pms08k691g6j9s") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.1 (crate (name "escvpnet") (vers "0.1.2") (hash "0bnh1a93wm38vx15bwn04cyzy2a22ffa01fbdi08df087xxv44jl") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.1 (crate (name "escvpnet") (vers "0.1.3") (hash "1vsfvs8g0mfmwkg349kr32aa7wxv9j6p9kgxy5zm4cjwqxd3mvxh") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.1 (crate (name "escvpnet") (vers "0.1.4") (hash "0g7pi5a0ack9rd93x3m1zdh14l3s487s4wh2cql533jxcw6jsn9n") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.2 (crate (name "escvpnet") (vers "0.2.1") (hash "0gfygm73gl5aj5g6k24mfpnf7gvbypa20pmkq7p7yy4mpd740z49") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.2 (crate (name "escvpnet") (vers "0.2.2") (hash "0zl2ymyrj49h7iv0w4bfkb66psm2zc6av558ghjy2wyf6wnn0jhd") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.2 (crate (name "escvpnet") (vers "0.2.3") (hash "110j5w7mdmmmf5n7k5jzmrn3wxq8hhl4b1ad3wyx8nxd1issd1im") (rust-version "1.56.0")))

(define-public crate-escvpnet-0.3 (crate (name "escvpnet") (vers "0.3.0") (hash "0nrw62ivclbjjfb9g1531101m8qvr3gwphnaip5h4405g3apr7am") (rust-version "1.68.2")))

(define-public crate-escvpnet-0.3 (crate (name "escvpnet") (vers "0.3.3") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("net" "io-util" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("net" "io-util" "test-util" "macros"))) (default-features #t) (kind 2)))) (hash "158r7hnbj1ikb5kh1y7irmk7krz6r77zh908k6w1b76gz4li3a0r") (rust-version "1.68.2")))

