(define-module (crates-io es o_) #:use-module (crates-io))

(define-public crate-eso_addon_manifest-0.1 (crate (name "eso_addon_manifest") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0w4rpwgmgsqx2a40r46nm30r15d2y35f7w6z6sj85vws4pz4dsqr") (yanked #t)))

(define-public crate-eso_addon_manifest-0.1 (crate (name "eso_addon_manifest") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "12jxkxsjnvnry4d0f842i73vs8qsvndc071ia062v9c75iizcj81")))

