(define-module (crates-io es ti) #:use-module (crates-io))

(define-public crate-estimate-0.1 (crate (name "estimate") (vers "0.1.0") (hash "13nmxiczf52jwkhwf6bi20dql0c9dh1nfn0a32nwgmpjwpdpbxny")))

(define-public crate-estimated_read_time-0.1 (crate (name "estimated_read_time") (vers "0.1.0") (hash "15va3d2i8czkcb6yp2birp0iblmijfhrlh12mxxfwdsw1rsp63l0") (yanked #t)))

(define-public crate-estimated_read_time-1 (crate (name "estimated_read_time") (vers "1.0.0") (hash "1mz8pkgk9v0cfzfjw659zl997gilangb78ccds8gic8h2hsgv734")))

