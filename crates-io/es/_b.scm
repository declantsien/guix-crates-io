(define-module (crates-io es _b) #:use-module (crates-io))

(define-public crate-es_balk-0.1 (crate (name "es_balk") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0imdd2i9x8c0fa16ay99yxznd79zfgm2cxlaviip6f9h2np5lr5k")))

(define-public crate-es_bulk-0.1 (crate (name "es_bulk") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0nw23fn7k37hlvsp2jdmz7l91d2clcrar64m5w0mpvvkrz3cs9x0")))

(define-public crate-es_bulk-0.1 (crate (name "es_bulk") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0pvr5rmn8pahdv7x18wygdgifr0lfqgpzl69mgcpfaykhdn6kwx2")))

