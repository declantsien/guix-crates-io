(define-module (crates-io es c-) #:use-module (crates-io))

(define-public crate-esc-pos-lib-0.1 (crate (name "esc-pos-lib") (vers "0.1.0") (hash "0rrbrf171y1n3kc45vwz9g1rkzf7jv0lrkrqi8c00s64178ll1iz") (yanked #t)))

(define-public crate-esc-pos-lib-0.1 (crate (name "esc-pos-lib") (vers "0.1.1") (hash "0zkad3hz0k26s1j8w5h3vi7v64pma31fcl2vm8y2iilg3lbb0d72") (yanked #t)))

(define-public crate-esc-pos-lib-0.1 (crate (name "esc-pos-lib") (vers "0.1.2") (hash "1g83xjipjf1ps4227z2pd4m6bpsl078mnkv54730h9nxq94mam8m") (yanked #t)))

(define-public crate-esc-pos-lib-0.1 (crate (name "esc-pos-lib") (vers "0.1.3") (hash "1n01nzv8b3bksf4y8wbiahnmsvzfg3p6sg57vka9wmy5i219jdg2") (yanked #t)))

(define-public crate-esc-pos-lib-0.1 (crate (name "esc-pos-lib") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "023q5kzmwsapjzgndpp8v8anqbwcvs4v28sxa7rskybq3iii3b1m")))

(define-public crate-esc-pos-lib-0.1 (crate (name "esc-pos-lib") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "03b7ykxf6lxfwlf01dwrcx3k5zs5gfkzljfd9l09midsdbwbczhk")))

(define-public crate-esc-pos-lib-0.1 (crate (name "esc-pos-lib") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "1dc2zl51yxi44xlhsv8sm3f53cr7v0cz2920iba6mhsw9bg9gg2l")))

(define-public crate-esc-seq-0.1 (crate (name "esc-seq") (vers "0.1.0") (hash "1pgbj04p7iam5s185h485cjkz0b2ppisvvpaasv54l8gzjccg1fd")))

(define-public crate-esc-seq-0.1 (crate (name "esc-seq") (vers "0.1.1") (hash "153ggpwkvw2b0y1l1ayd6axvnw3zl9jc7nn14f5ymlxv77a5ajwz")))

(define-public crate-esc-seq-0.1 (crate (name "esc-seq") (vers "0.1.2") (hash "1xwshipshvh8nv4xph5lig1y2ll9kxjbhi0lv5xia2d9xvg0rvy5")))

