(define-module (crates-io es vm) #:use-module (crates-io))

(define-public crate-esvm-0.0.1 (crate (name "esvm") (vers "0.0.1") (deps (list (crate-dep (name "digest") (req "^0.5") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "esvm-bigint") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "esvm-rlp") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ripemd160") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.2") (default-features #t) (kind 0)))) (hash "064b4nq0czh4mfjg2m72xn2av7xadc2mks4y43wrirqf9ms1fdrk")))

(define-public crate-esvm-bigint-0.0.1 (crate (name "esvm-bigint") (vers "0.0.1") (deps (list (crate-dep (name "esvm-rlp") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0k95hv8ynxrlj0mqapvykvjmpwn24z4100p0nvkvn6fd8nkbr98a")))

(define-public crate-esvm-rlp-0.0.1 (crate (name "esvm-rlp") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "elastic-array") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1v24apkyz485izmjv8sw9hqj3kc7s392rqdlv2ynayjya76br15a")))

