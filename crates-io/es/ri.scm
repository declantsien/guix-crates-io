(define-module (crates-io es ri) #:use-module (crates-io))

(define-public crate-esri_ascii_grid-0.1 (crate (name "esri_ascii_grid") (vers "0.1.0") (hash "02qssh33kb0zgymmzv759a3lxbqsrvhjg9nfc7y2cjziwrivcq3b")))

(define-public crate-esri_ascii_grid-0.1 (crate (name "esri_ascii_grid") (vers "0.1.1") (hash "0swh245nvkh54k69br0bm3b99byz13lgx1bpdhjfv5bkcqrmr9hi")))

(define-public crate-esri_ascii_grid-0.1 (crate (name "esri_ascii_grid") (vers "0.1.2") (hash "05ldkf6d54dh8cgw7fqsdfmv7v72j1i1yy2lx0fq00mkfr699d2f")))

(define-public crate-esri_ascii_grid-0.2 (crate (name "esri_ascii_grid") (vers "0.2.0") (deps (list (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1pwn0ck24wvdsf7h5x0pkg11rzds2ppr5a0hlasmiqr5z65fq0ms")))

(define-public crate-esri_ascii_grid-0.3 (crate (name "esri_ascii_grid") (vers "0.3.0") (deps (list (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1xb5451p7iijvvlxlg4baz94ghwkbsx8ac7n8n3zqiia26bhrjn8")))

(define-public crate-esri_ascii_grid-0.3 (crate (name "esri_ascii_grid") (vers "0.3.1") (deps (list (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "0cgsngqn3iirp8hksjmhk117fc12n1dmi5030f15qsvcscarg0q9")))

(define-public crate-esri_ascii_grid-0.4 (crate (name "esri_ascii_grid") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "14pf71imb6yyr4jqbf4n75zl1k3pnsisfxnkklfmbcxfq14y60aw")))

(define-public crate-esri_ascii_grid-0.4 (crate (name "esri_ascii_grid") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "0h13cwf4n0m4i1sk6x4m69mgn1nmgnqayxplaj3fbba35ld4xqmr")))

(define-public crate-esri_ascii_grid-0.4 (crate (name "esri_ascii_grid") (vers "0.4.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1ppbnjadnsgxiwyibcpfimjkdq72srvi5cj08s1if9ww9x1ybs1l")))

(define-public crate-esri_ascii_grid-0.4 (crate (name "esri_ascii_grid") (vers "0.4.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1wgd8b4l5x43z7ki39kqavpg472yw7br10jl3a7agvfx0b86b13b")))

