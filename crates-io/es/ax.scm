(define-module (crates-io es ax) #:use-module (crates-io))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0li56vm2ccbc4r764kqkd42ak4g22z4hs8vqnp3nm512fwvggwkh")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1742zp2wgvi3z4gci17i2lh700jh4mddz2n77brhhxcgwaf6w6w7")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "19h8ffyahvmrkmwrgbd5ix3bf1qw574f11vls0ddgpkfbf1mj5dz")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0r7ab0napgkqw5ch2x3l6l7a1idffx60ksjys1hj9y8m3libzw53")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ma3sfqg8vfqmzp1d3y3kqvmdwcp37l84sx3gbz0ik28zij3kfg6")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0g2pixys01g1xriwsjzh9lrbd82lnzprn5g658ysv305zrdcfg59")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1iqx3fkfniciimz6qplyk3hp0h72idhg7mvlz8f8v29xcbn0g30k")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "05p7lzgqqmp8xqmi7m1q62m4j4s746kj486k7mxfjd5pa6rifikg")))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.8") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0d8abipgfsaknrj5phlv34133sai73mmr2rgykazxagc7hjqnx0z") (features (quote (("default" "cpp") ("cpp" "cc"))))))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.9") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "0wrl1q61jfn2idrbmpfg51prp7b63fz0w6pry9yk8mrhk8a3b6nx") (features (quote (("default" "cpp") ("cpp" "cc"))))))

(define-public crate-esaxx-rs-0.1 (crate (name "esaxx-rs") (vers "0.1.10") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1rm6vm5yr7s3n5ly7k9x9j6ra5p2l2ld151gnaya8x03qcwf05yq") (features (quote (("default" "cpp") ("cpp" "cc"))))))

