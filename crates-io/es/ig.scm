(define-module (crates-io es ig) #:use-module (crates-io))

(define-public crate-esign-0.0.1 (crate (name "esign") (vers "0.0.1") (hash "18zgibryjgi7qn2af3am8wddscw5bi7bjjhq2wy0s050dnrs8hh4") (yanked #t)))

(define-public crate-esign_cli-0.0.1 (crate (name "esign_cli") (vers "0.0.1") (hash "11gxwjv7c4d9yrhzjrdqxlla58jfjdhpam574chi67gsj1s9hvqx") (yanked #t)))

(define-public crate-esign_ffi-0.0.1 (crate (name "esign_ffi") (vers "0.0.1") (hash "1vmf94cqb35sw4gma9hb2qxjccdx7zk96vgx2sjg0vgvp7zdnn95") (yanked #t)))

(define-public crate-esign_macro-0.0.1 (crate (name "esign_macro") (vers "0.0.1") (hash "0nknpw7cg5hy5yjhl9cyp1wr2iv5l634jmwyivkxm047afgfvp44") (yanked #t)))

(define-public crate-esign_wasm-0.0.1 (crate (name "esign_wasm") (vers "0.0.1") (hash "08pqy9kwf39w2w6n5gajlbx9rw99zxafbzqja70ycc1vv49f0ig5") (yanked #t)))

