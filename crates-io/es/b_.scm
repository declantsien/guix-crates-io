(define-module (crates-io es b_) #:use-module (crates-io))

(define-public crate-esb_fireplace-0.1 (crate (name "esb_fireplace") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0k2ni1739iqzklnzvscfnr0327v5crxf5hxjxa48bhswhsjr7gf4")))

(define-public crate-esb_fireplace-0.2 (crate (name "esb_fireplace") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "109kcv3r5b69dj1p6g26c7ijc28793m6yqg3p6fb486z1sxl0r15")))

