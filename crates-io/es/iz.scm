(define-module (crates-io es iz) #:use-module (crates-io))

(define-public crate-esize-0.1 (crate (name "esize") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sz2npq700agrwirv0gh9apg2amzck3pylh2b6ya1vlnawgah2zl") (yanked #t)))

(define-public crate-esize-0.1 (crate (name "esize") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xpk3sy8yn57a5nwcxcykv9r3pi4ycy90qwja3djmbsaz8inpmfz")))

(define-public crate-esize-0.1 (crate (name "esize") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v0qaaf929s653ymkm5iwgb1x9cqa964dy3r6f3w0471r8g7q0z9")))

