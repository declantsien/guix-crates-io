(define-module (crates-io es bu) #:use-module (crates-io))

(define-public crate-esbuild-0.1 (crate (name "esbuild") (vers "0.1.0") (hash "1y0g3jwgirghn6mcmqm5g678xgbc3nmparp510750mkqgxpmdda0")))

(define-public crate-esbuild-config-0.1 (crate (name "esbuild-config") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "08bflwlpxlzar9imzici91ymwwrh3axpws103790p6zy4ay4qaa6")))

(define-public crate-esbuild-config-0.2 (crate (name "esbuild-config") (vers "0.2.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1rikn0mcpf4lsv1agb8wg6bjsnkqv1axw5dph03mp2bxxcb7zp4b")))

(define-public crate-esbuild-rs-0.0.1 (crate (name "esbuild-rs") (vers "0.0.1") (hash "0r9r1z0iycqyrrhs47n6ry0hkrpy7r6c7qbg3qybwi66swz2rbmd")))

(define-public crate-esbuild-rs-0.0.2 (crate (name "esbuild-rs") (vers "0.0.2") (hash "0vd0964w6gg3b0vc3dx1yxc56ykka0jd6s5fimrn9bmam9h7z027")))

(define-public crate-esbuild-rs-0.0.3 (crate (name "esbuild-rs") (vers "0.0.3") (hash "079bil7lpncbpi0j6cgy7k6ggfl9wp3m586difyqx6700j2d4ka7")))

(define-public crate-esbuild-rs-0.0.4 (crate (name "esbuild-rs") (vers "0.0.4") (hash "0rcrxcr4hq9clpv7bpw98ck203636zsa6jb4asgnd2mnnimv930d")))

(define-public crate-esbuild-rs-0.0.5 (crate (name "esbuild-rs") (vers "0.0.5") (hash "046vb0a3ha8z9a4rqidvrhbcpr38vkwcii7dc4cc6l92lw64slxa")))

(define-public crate-esbuild-rs-0.0.6 (crate (name "esbuild-rs") (vers "0.0.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "1p34y69mrixlld44j9v6bmlk044iyyjgx23k2hwm2xn0msv8vf4c")))

(define-public crate-esbuild-rs-0.0.7 (crate (name "esbuild-rs") (vers "0.0.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "13is9byyhy78a5p390il9zwggr7vl8p451ij72h7sfydsgjzwc83")))

(define-public crate-esbuild-rs-0.0.8 (crate (name "esbuild-rs") (vers "0.0.8") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "0a8pwr7w40507nl2pjxb9m74ixc0285bgdd5wdg4kkpz7y7h2dpd")))

(define-public crate-esbuild-rs-0.1 (crate (name "esbuild-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "1ypanwbm14y1m77zn24xm0di7fhj9f397l7ahbdiph9psz37c79l")))

(define-public crate-esbuild-rs-0.1 (crate (name "esbuild-rs") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "14l5nbz5mcj9rsazlbhragpm5gs0fzcvg5g4abnc5q7j353gbgmg")))

(define-public crate-esbuild-rs-0.1 (crate (name "esbuild-rs") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "0y8a5r705r19k0hfnddrzzhygq5di9gghklbnwmx4kk4ic47gdqf")))

(define-public crate-esbuild-rs-0.1 (crate (name "esbuild-rs") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.3") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "0j2acqh1p10a43vbqc3d2jj24ha80353pdmzv6x2n93razvcwb9b")))

(define-public crate-esbuild-rs-0.2 (crate (name "esbuild-rs") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.3") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "0icd3rh11wwr8ljz9ig6689y27qgg14wv7qa0xxi1p0f2jsv0wl7")))

(define-public crate-esbuild-rs-0.2 (crate (name "esbuild-rs") (vers "0.2.1") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.3") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "11qbnqa5vpnxvxr0nkgpfxj537dnnq2x59dbixyrfr3lyj4jps0g")))

(define-public crate-esbuild-rs-0.8 (crate (name "esbuild-rs") (vers "0.8.30") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.3") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "1sgsqhzv0waaw9a0j2hljbiya7c0zyr7bs70v5jnpg106pm3krnf")))

(define-public crate-esbuild-rs-0.12 (crate (name "esbuild-rs") (vers "0.12.18") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.3") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "0x6nixi1asmzdnjp57y3fnfyygrzrpkvk42c77wznjnr6fpszgwn")))

(define-public crate-esbuild-rs-0.12 (crate (name "esbuild-rs") (vers "0.12.19") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.3") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "1d2ka2610c41x62hg3cy6nyaijssmzci9977dcync9fxld2bplz8")))

(define-public crate-esbuild-rs-0.13 (crate (name "esbuild-rs") (vers "0.13.8") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memorymodule-rs") (req "^0.0.3") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 0)))) (hash "14wvv648i2728ck4ihi7zgp4n4ff9ynr6jc75yf5a05xv99j6sji")))

