(define-module (crates-io es -h) #:use-module (crates-io))

(define-public crate-es-htmlform-0.1 (crate (name "es-htmlform") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "103543j6j3ja95dcny0h4995n2n7xyrfyyvhsb3g3rdizdx61njk")))

(define-public crate-es-htmlform-0.1 (crate (name "es-htmlform") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "07fsrwxz4v85gz4ggj2ymxq42a0vp90hi7d7dy135wil8sc9yzhf")))

