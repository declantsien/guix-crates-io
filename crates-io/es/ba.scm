(define-module (crates-io es ba) #:use-module (crates-io))

(define-public crate-esbat-0.1 (crate (name "esbat") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (kind 0)))) (hash "1yhzpj8xq7lriqbsn5mg3v1srhhxjlv4mwdz7bwziczkd66cqnvk") (features (quote (("clock" "chrono/clock"))))))

