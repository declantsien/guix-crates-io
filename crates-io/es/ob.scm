(define-module (crates-io es ob) #:use-module (crates-io))

(define-public crate-esobox-0.1 (crate (name "esobox") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.6") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "08ig1gql6zjb7hw08r9jy6bqp5gww89ahf96xy35qbkazxczzq7c")))

