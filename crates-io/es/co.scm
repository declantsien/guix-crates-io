(define-module (crates-io es co) #:use-module (crates-io))

(define-public crate-escodegen-0.1 (crate (name "escodegen") (vers "0.1.0") (hash "1vnrq8rx8c0m6g7sql96zv1p9wnfrysi6xiqalxbi6rcxxg3n5sy")))

(define-public crate-escodegen-0.1 (crate (name "escodegen") (vers "0.1.1") (hash "1nffaq7s0fyfihpzzc6gl5fdl4x8ydbzxgm222vic2pgmsfdbhfg")))

(define-public crate-escodegen-0.1 (crate (name "escodegen") (vers "0.1.2") (hash "1jac8zx2a4scx6hnfzah4vz88shy82n0ii5wbq1i833fzh4pprmg")))

(define-public crate-escodegen-0.2 (crate (name "escodegen") (vers "0.2.0") (hash "1w8x1svx96jkq5z73vqy8mwvj5phcrm383xil2pszgkmyhwgx4h1")))

(define-public crate-escodegen-0.2 (crate (name "escodegen") (vers "0.2.1") (hash "0b66ap3kyq8nmdhh7wwp2kzzf0rblyy45w5vszc9sy4xw1idkkfd")))

(define-public crate-escodegen-0.3 (crate (name "escodegen") (vers "0.3.0") (hash "03agrm401ffpl0gqy50ynb67nav92hadb9z7hl7b4dds0g3g63wa")))

(define-public crate-escodegen-0.3 (crate (name "escodegen") (vers "0.3.1") (hash "0309y5j9gq00gmsrswkbyqyr4qp53sh03j6lfalgg53py4yjaa4s") (yanked #t)))

(define-public crate-escodegen-0.3 (crate (name "escodegen") (vers "0.3.2") (hash "12ccl1n7xasrkbhjpi5j07vvj611ihg1lgldfxaa3anqx4i1hxkp")))

(define-public crate-escodegen-0.3 (crate (name "escodegen") (vers "0.3.3") (hash "0akkavij1axjm0gvq118waxbzi7njk98vgjs5r1rkavwvfa3qad1")))

(define-public crate-escodegen-0.4 (crate (name "escodegen") (vers "0.4.0") (hash "0f5h3dw9bhf3ls6613s5cxyz1c0krsg8fi74vspz73afp6rmqdqn")))

(define-public crate-escodegen-0.4 (crate (name "escodegen") (vers "0.4.1") (hash "1lwqd6cqqmyylwx4rmija5r50vww9w4aj860w13f2vlfpqlnkwlr")))

(define-public crate-escodegen-0.4 (crate (name "escodegen") (vers "0.4.2") (hash "1pa56j0s1gdjwb57vrgm84f00cqbvyp9mkn6yz8chw00jgpz3myi")))

(define-public crate-escodegen-0.4 (crate (name "escodegen") (vers "0.4.3") (hash "13m2lx8xkdqh4r4pjfnna3929xx88y1kpcidm8dh6m1x03mc35j7")))

(define-public crate-escodegen-0.4 (crate (name "escodegen") (vers "0.4.4") (hash "0q0na3xn8wwv9z7vh48ss6rskxwhfb8kz0xyqg0qjaph52vyzl2y")))

