(define-module (crates-io es i_) #:use-module (crates-io))

(define-public crate-esi_client-0.8 (crate (name "esi_client") (vers "0.8.6") (deps (list (crate-dep (name "reqwest") (req "~0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "12wvm2kqg1pdyzirj1kwk31qygy5hap5lppsppacybnnsyh1jqx0") (yanked #t)))

(define-public crate-esi_client-0.0.1 (crate (name "esi_client") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "~0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0ga7yiy543bgq6bgprvkbj0k23mlcxid7fiq9icdw6gj3xddk0a7")))

(define-public crate-esi_fastly-0.1 (crate (name "esi_fastly") (vers "0.1.0") (deps (list (crate-dep (name "esi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fastly") (req "^0.7") (default-features #t) (kind 0)))) (hash "0dk7m48fjxab5n5g2fni4ayj2l1r84lk4r4rn9fsd16dfclln5a7")))

(define-public crate-esi_fastly-0.1 (crate (name "esi_fastly") (vers "0.1.1") (deps (list (crate-dep (name "esi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fastly") (req "^0.7") (default-features #t) (kind 0)))) (hash "126sppvv7v9q20bf4kf4zvxn41jw7ngshhwashgap3y8qfd5apl8")))

(define-public crate-esi_fastly-0.1 (crate (name "esi_fastly") (vers "0.1.2") (deps (list (crate-dep (name "esi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fastly") (req "^0.7") (default-features #t) (kind 0)))) (hash "0avv273pwdcx20v0a4ghw2q5qy543qid919cyx0cjw2gmmhwbvvc")))

(define-public crate-esi_fastly-0.1 (crate (name "esi_fastly") (vers "0.1.3") (deps (list (crate-dep (name "esi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fastly") (req "^0.7") (default-features #t) (kind 0)))) (hash "1j5l7hymv167fgabzck6z1c1svp84s972s4pmrbq1bdsc61rj4gs")))

