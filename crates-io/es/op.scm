(define-module (crates-io es op) #:use-module (crates-io))

(define-public crate-esopt-0.2 (crate (name "esopt") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "05zk95f16kjsfkdg87flxpafz443bjym6h1kh9xkzida8kj2migk") (features (quote (("floats-f64"))))))

