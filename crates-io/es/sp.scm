(define-module (crates-io es sp) #:use-module (crates-io))

(define-public crate-essp-0.1 (crate (name "essp") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "=0.2.36") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1b4qh4srmflwa2c8ijh6zpayr9rfmssqanc45hr1y7dw7qh88xav")))

