(define-module (crates-io es so) #:use-module (crates-io))

(define-public crate-esso-1 (crate (name "esso") (vers "1.0.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wm5yfr43qcvpd3dg0l7kpa5i05c02qzqz4gzdsld2wnkyq1ki94")))

