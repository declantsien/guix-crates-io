(define-module (crates-io es p_) #:use-module (crates-io))

(define-public crate-esp_idf-0.1 (crate (name "esp_idf") (vers "0.1.0") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.1") (default-features #t) (kind 0) (package "esp_idf_dev_sys")) (crate-dep (name "failure") (req "^0.1.7") (features (quote ("derive"))) (kind 0)))) (hash "1gs25sr9jaxqn0m74x44sd6m3f8azlwpybbmr80ab4z55fbn6vq5")))

(define-public crate-esp_idf-0.1 (crate (name "esp_idf") (vers "0.1.1") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.1") (default-features #t) (kind 0) (package "esp_idf_dev_sys")) (crate-dep (name "failure") (req "^0.1.7") (features (quote ("derive"))) (kind 0)))) (hash "0187acb3b9qdbshwmlznsjdf3rsm82x5wq9jjc8f4g6z3sbv58ys")))

(define-public crate-esp_idf_build-0.1 (crate (name "esp_idf_build") (vers "0.1.0") (hash "0dbnxlzzir3n9c9939472qimcbsdchhd54zxfjlpjg10nchx6ngd")))

(define-public crate-esp_idf_build-0.1 (crate (name "esp_idf_build") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bdfy1i67vsab5yb5m0xjshjmcfxbdlk48aspkh5q189cv39w4y6")))

(define-public crate-esp_idf_build-0.1 (crate (name "esp_idf_build") (vers "0.1.2") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "05mrw2yzwpf2wyqxqcn168g8c0spzq556zy0a4nvhnxknwl9rh36") (features (quote (("default" "build_idf") ("build_make") ("build_idf"))))))

(define-public crate-esp_idf_build-0.1 (crate (name "esp_idf_build") (vers "0.1.3") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lixs8h111m6cl3h6adgcww5l5l9zq8c7k4hqkx6v4m7llc6sqkp") (features (quote (("default" "build_idf") ("build_make") ("build_idf"))))))

(define-public crate-esp_idf_dev_sys-0.1 (crate (name "esp_idf_dev_sys") (vers "0.1.3") (hash "027kq5n19551dzrd2v6glbfrbxqiazdyg6aipzsb1dmdc1qa3k55")))

(define-public crate-esp_idf_dev_sys-0.1 (crate (name "esp_idf_dev_sys") (vers "0.1.4") (hash "05yc0ska2vb4w70ym7q84mdic1ahrywhqbjp88idf4f807qmnlg9")))

(define-public crate-esp_idf_logger-0.1 (crate (name "esp_idf_logger") (vers "0.1.0") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sh61034lla7rs2qw3rwvnv7igyg8i3dixk17vs8cgwg712hp7sv") (features (quote (("ets_printf") ("default" "ets_printf"))))))

(define-public crate-esp_idf_logger-0.1 (crate (name "esp_idf_logger") (vers "0.1.1") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "05zf03dvra2w9iqlf3xi6wq6gjb5479w1s4mzn618xf0917qshgj") (features (quote (("ets_printf") ("default" "ets_printf"))))))

(define-public crate-esp_macro-0.0.0 (crate (name "esp_macro") (vers "0.0.0") (hash "0bsmm6fhy4844hq9k6m5j2nsabqh4bxl32z999gsbhi8dnsvmmri")))

(define-public crate-esp_pwm_reader-0.1 (crate (name "esp_pwm_reader") (vers "0.1.0") (deps (list (crate-dep (name "embuild") (req "^0.31.3") (default-features #t) (kind 1)) (crate-dep (name "esp-idf-hal") (req "^0.42") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-svc") (req "^0.47") (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.33") (features (quote ("native"))) (default-features #t) (kind 0)))) (hash "152d0vlz2w980nyabsnm5n8lw6jlfip0p5jgnpfk64yqdrfia3r5") (features (quote (("std" "alloc" "esp-idf-svc/binstart" "esp-idf-svc/std") ("pio" "esp-idf-svc/pio") ("nightly" "esp-idf-svc/nightly") ("experimental" "esp-idf-svc/experimental") ("embassy" "esp-idf-svc/embassy-sync" "esp-idf-svc/critical-section" "esp-idf-svc/embassy-time-driver") ("default" "std" "embassy" "esp-idf-svc/native") ("alloc" "esp-idf-svc/alloc"))))))

(define-public crate-esp_pwm_reader-0.1 (crate (name "esp_pwm_reader") (vers "0.1.1") (deps (list (crate-dep (name "embuild") (req "^0.31.3") (default-features #t) (kind 1)) (crate-dep (name "esp-idf-hal") (req "^0.42") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-svc") (req "^0.47") (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.33") (features (quote ("native"))) (default-features #t) (kind 0)))) (hash "0ck82pp874rbg6wk3ggpdf02bplp8306ydf1bxq1552vpr218fqn") (features (quote (("std" "alloc" "esp-idf-svc/binstart" "esp-idf-svc/std") ("pio" "esp-idf-svc/pio") ("nightly" "esp-idf-svc/nightly") ("experimental" "esp-idf-svc/experimental") ("embassy" "esp-idf-svc/embassy-sync" "esp-idf-svc/critical-section" "esp-idf-svc/embassy-time-driver") ("default" "std" "embassy" "esp-idf-svc/native") ("alloc" "esp-idf-svc/alloc"))))))

(define-public crate-esp_pwm_reader-0.1 (crate (name "esp_pwm_reader") (vers "0.1.2") (deps (list (crate-dep (name "embuild") (req "^0.31.3") (default-features #t) (kind 1)) (crate-dep (name "esp-idf-hal") (req "^0.42") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-svc") (req "^0.47") (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.33") (features (quote ("native"))) (default-features #t) (kind 0)))) (hash "0lsh010hw49v4w3v7fhl174yi1fps9x4m7h7fbsdaf3hls0jxfcb") (features (quote (("std" "alloc" "esp-idf-svc/binstart" "esp-idf-svc/std") ("pio" "esp-idf-svc/pio") ("nightly" "esp-idf-svc/nightly") ("experimental" "esp-idf-svc/experimental") ("embassy" "esp-idf-svc/embassy-sync" "esp-idf-svc/critical-section" "esp-idf-svc/embassy-time-driver") ("default" "std" "embassy" "esp-idf-svc/native") ("alloc" "esp-idf-svc/alloc"))))))

