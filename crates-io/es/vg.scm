(define-module (crates-io es vg) #:use-module (crates-io))

(define-public crate-esvg-0.1 (crate (name "esvg") (vers "0.1.0") (deps (list (crate-dep (name "polygonical") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.27") (default-features #t) (kind 0)))) (hash "1s7nhlqczg41ir8ndkq2syadyz11jdp18h69w6jmjdl0fmx2dd3l")))

(define-public crate-esvg-0.2 (crate (name "esvg") (vers "0.2.0") (deps (list (crate-dep (name "polygonical") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.27") (default-features #t) (kind 0)))) (hash "0wh18rkyqj28a6qy9zmm0q4rz3jhdrv2x71j61hcylafzajs4wlq")))

(define-public crate-esvg-0.3 (crate (name "esvg") (vers "0.3.0") (deps (list (crate-dep (name "polygonical") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.27") (default-features #t) (kind 0)))) (hash "0y8pd63b07qspmn9g2i911h24l89yfxjc3c5vi6r9194zhrwp51c")))

(define-public crate-esvg-0.4 (crate (name "esvg") (vers "0.4.0") (deps (list (crate-dep (name "polygonical") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.29") (default-features #t) (kind 0)))) (hash "0m7ixmmii2w50scvlr1zrkjnc5ql3kl60cwgy8ql0ylnadh8f8cw")))

(define-public crate-esvg-0.5 (crate (name "esvg") (vers "0.5.0") (deps (list (crate-dep (name "polygonical") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "179m478yriyis5abbbjv1kws5gkri6dxppkfdd7iymsm11rixxad")))

