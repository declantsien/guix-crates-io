(define-module (crates-io es dm) #:use-module (crates-io))

(define-public crate-esdm-sys-0.0.4 (crate (name "esdm-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (features (quote ("runtime"))) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "1pj7bqd115ncxr3736236g6wiv1x35a7b34skq0a461s590fs75q")))

(define-public crate-esdm-sys-0.0.6 (crate (name "esdm-sys") (vers "0.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (features (quote ("runtime"))) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "1j6ciflqhdm5w7dq7bmgqngq9mdv312y9lqhhz7gkhzjx2mvhlv8")))

(define-public crate-esdm-sys-0.1 (crate (name "esdm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (features (quote ("runtime"))) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "1wng205c7057gz1n0xagn27rr3w9vyvipbsrdgddcn9l63p7jgaw")))

(define-public crate-esdm-sys-0.1 (crate (name "esdm-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (features (quote ("runtime"))) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "05rfh78g4nrzk0w08acnb98ii0dja5p62scki55j7imnhgncf6pm")))

(define-public crate-esdm-tool-0.0.6 (crate (name "esdm-tool") (vers "0.0.6") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand-esdm") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0hfkkims1s5427hgx9nhphkrzzbh62f2sh2vwv07fsyjs5k3cvz5") (features (quote (("default"))))))

(define-public crate-esdm-tool-0.1 (crate (name "esdm-tool") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand-esdm") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g87kqsddbj9d1py5d0wc2zyqlblz0m3lpnq4mhi19zsgb9q2anf") (features (quote (("default"))))))

(define-public crate-esdm-tool-0.1 (crate (name "esdm-tool") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand-esdm") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "02923im02qc75aij8apiqlfb2lbqvcr2j5jnvka7lbj0shlvq2zf") (features (quote (("default"))))))

(define-public crate-esdm-tool-0.1 (crate (name "esdm-tool") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand-esdm") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07vg6hhxpc6fh60ryh4qf28hjqhgqjgx6dmafvv8yx0iy6wdpf79") (features (quote (("default"))))))

