(define-module (crates-io es de) #:use-module (crates-io))

(define-public crate-esde-1 (crate (name "esde") (vers "1.0.0") (deps (list (crate-dep (name "esde_derive") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0fn80inr5gplcni5ffidlnkc3rpg9d4abbkl75sq4xjxnmlmlf1w")))

(define-public crate-esde_derive-1 (crate (name "esde_derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (default-features #t) (kind 0)))) (hash "0mdjd6chr7mvxqflqzjhmvfiqxd04whhf2b2g9qvxxl1k1811f1i")))

