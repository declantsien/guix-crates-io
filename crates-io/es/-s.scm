(define-module (crates-io es -s) #:use-module (crates-io))

(define-public crate-es-sys-0.0.0 (crate (name "es-sys") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)))) (hash "0vdbl0125bh1dg01awcv0sm0ycrisqxia119gqsjx844xk2rh63v")))

(define-public crate-es-sys-0.0.1 (crate (name "es-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)))) (hash "1szy3nrljxqclb7k69cynhd2wjbsq87l5f17j0p4jyp6r0v2bbsc")))

