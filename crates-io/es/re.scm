(define-module (crates-io es re) #:use-module (crates-io))

(define-public crate-esre-0.1 (crate (name "esre") (vers "0.1.0") (deps (list (crate-dep (name "rstest") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "152w3myxbxqfr8623pq8lpn955hh1h46z77n9d12lv93x1d7hbx6")))

(define-public crate-esre-0.1 (crate (name "esre") (vers "0.1.1") (deps (list (crate-dep (name "rstest") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "15lg8dnc4hd6vp5b4rvszngxkzbzp4wprd63q1120rg94r9zg6rm")))

