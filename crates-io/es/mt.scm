(define-module (crates-io es mt) #:use-module (crates-io))

(define-public crate-esmt-0.1 (crate (name "esmt") (vers "0.1.0") (hash "09q6xbag4b8in30l8c4mzr8mrgiapnsy3di9d4xwj16vs34ljf04") (yanked #t)))

(define-public crate-esmt-0.0.1 (crate (name "esmt") (vers "0.0.1") (hash "1l8vasvmc5s8qafcac1a7pglj69m542hjrjddbj8806yk5d4j8l3")))

