(define-module (crates-io fr ou) #:use-module (crates-io))

(define-public crate-frounding-0.0.1 (crate (name "frounding") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0i2b2mrdwycflziy4294z7ri0068sq6kr1vbrd3h5giwzc855rg0")))

(define-public crate-frounding-0.0.2 (crate (name "frounding") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0y8ymm7ls7vwv6b3z55bni12s7czz476brawahhmw6rfvkdps3wb")))

(define-public crate-frounding-0.0.3 (crate (name "frounding") (vers "0.0.3") (deps (list (crate-dep (name "gcc") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0ya1kilvsakklv9rca3884kndjp8pcs95n230v95cniy56clqn2w")))

(define-public crate-frounding-0.0.4 (crate (name "frounding") (vers "0.0.4") (deps (list (crate-dep (name "gcc") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0cy1p3drpq7hspvnc0hqkivbdl67fmcnbx2z0v47cml59xgzmd0h")))

