(define-module (crates-io fr un) #:use-module (crates-io))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.0") (hash "090ib9v2623837bkgqkxhjh8xaw7bhc518almgmg6p2dmlcizskw")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.1") (hash "1shpzp086600yclq086qcm5j6w1ymfi7nrd0vzvpk9lwjwx6m392")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.2") (hash "02gghyiy8k1zgwx67sadq2m5yyh8jicl56w8jyb3fqlcpqcwapr0")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.3") (hash "0315na7700qdni68ya2p4ymrv3q1hriql9cziwww0i43p01ha0ls")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.4") (hash "1i5w3m1lqllyay5nzbdml1n4k7c1i4ijpb1f3l3zbi1fkdgggrcp")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.5") (hash "08zy3qxvq1ziy1qw63714w7i2nfql1p5vh9warqbrmnmz6x9pmd8")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.6") (hash "04yyyh4pbprivwsq0rghjkf3fn63nban9vfklpx3bws3g4pd6r5r")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.7") (hash "1700rb3sba52dqzwa5969bqkwmvg7072fszn8c9ffy3lrvmavyhh")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.8") (deps (list (crate-dep (name "frunk_core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0178kf5z5bp1v5v9b8klrjchyadg1ggfbygzkbfgsq5yxlwii4zg")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.9") (deps (list (crate-dep (name "frunk_core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "14wdc731spxx453mbk9arqkn4v9pn1lp30jdixj7yfvgaqybz56y")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.10") (deps (list (crate-dep (name "frunk_core") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1s23a5yy1jy4bzy63z00rr5xwwn3v0jk5rrcyxlqj2f71swgmw3b")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.11") (deps (list (crate-dep (name "frunk_core") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1h1z2clvdrjq4nn7mq6yllsss3xjvlkd5fgi7h9nnhbb5s9m35jm")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.12") (deps (list (crate-dep (name "frunk_core") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0qlpszfq6v14iw0lv9qhwaawpja54xrww9lg5fvd9m7wbkgzphln")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.13") (deps (list (crate-dep (name "frunk_core") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1z7yw1zwn6f522fdxh6yfbwfhigxsp3n0b88fj2q63cg9cvjkyf7")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.14") (deps (list (crate-dep (name "frunk_core") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0wi8ggh6j929662ml4x1y24bzhggffik41p1zjn4mk9y4c0330f1")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.15") (deps (list (crate-dep (name "frunk_core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1p7pardfv5b1pqihhhlg6r14y6wm4pbs33p9v4wb6xshmsxgxlh1")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.16") (deps (list (crate-dep (name "frunk_core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "12nkgms6j1hhfybps076c1jnkb5fylffh907l1fyfxjsb3hjpf7n")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.17") (deps (list (crate-dep (name "frunk_core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0s12bgkab7pbnnk18dw6qg9xdhi8x39i1l1rbmx2p73rybhr0mqa")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.18") (deps (list (crate-dep (name "frunk_core") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "08sbgabqfcj8rzvx7r5zs12xpzaiya3zh90x17agxg8v4lgm0gg5")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.19") (deps (list (crate-dep (name "frunk_core") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "14rhhfc75hv6jgcz31dakda07f7mgqfcp7y53m020f8igs52qjmf")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.20") (deps (list (crate-dep (name "frunk_core") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "14z4qx4banaw63m6pai1jfl7ky38np1dxd87jnzcjkwd78lhcsli")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.21") (deps (list (crate-dep (name "frunk_core") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0m2ldcsnrf7c9844jra8cy6g718vdfj2pvvnlmj8fd0n6nmjfyn4")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.22") (deps (list (crate-dep (name "frunk_core") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0r8fwq80spy1sd9c8msbglhbi8gm242m35rh8yjsppr5j4jvvcj2")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.23") (deps (list (crate-dep (name "frunk_core") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0ph0d9hsjpd7iwm5sdws3lz24q7knwfifz2yf23m18l0gqj6vfrk")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.24") (deps (list (crate-dep (name "frunk_core") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.2") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1d2mqbhwmkg6wgk5gd0m86hzs5fa00kjciwsa353vp45ckvd2475")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.25") (deps (list (crate-dep (name "frunk_core") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.2") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0lay20xwap6nwl5l6rp3h55hyk0bmg48618rdlgpldfkihj3vr8z")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.26") (deps (list (crate-dep (name "frunk_core") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.15") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.3") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "10hz42cl65ij0gixl5nqflxhfi2xfaam8z09xb4p9n1dm7xbz8rx")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.27") (deps (list (crate-dep (name "frunk_core") (req "^0.0.15") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "038v13dbswdfmb9232pfxb16lpgv2ywczak37rkr5bmlw076za3a")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.28") (deps (list (crate-dep (name "frunk_core") (req "^0.0.15") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.5") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1ycpiir3mv8gwrxsqi8sxjkgv0nxzryqprnazp4rfydbam7jfhmc")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.29") (deps (list (crate-dep (name "frunk_core") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.17") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.6") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1nnmn0vh943sx5fqnwkxk3hs70chxjjmcnz30xbgwmb54rwsqsai")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.30") (deps (list (crate-dep (name "frunk_core") (req "^0.0.17") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.18") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.7") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1r60mw21pi830z57nrbc28qybxjaplw9v23m8q2lcf2gh4v7842s")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.31") (deps (list (crate-dep (name "frunk_core") (req "^0.0.18") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.19") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.8") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0mldj2nwmhn9mvsl23j3y26dck7cmsb5dhaxa4qpkdg1715qaadd")))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.32") (deps (list (crate-dep (name "frunk_core") (req "^0.0.19") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.20") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0hcp7pqhg0b23dvxdqv9gcn41fs4rkc4z60nrbimf1qlymh836k1") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.33") (deps (list (crate-dep (name "frunk_core") (req "^0.0.20") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.21") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1rxbih99a6kn0lamhzdj7r9cy1r8r1a79fm193y1y9db3dswrzsd") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.34") (deps (list (crate-dep (name "frunk_core") (req "^0.0.21") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.22") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1w5la7nv427dkk4ahm25blvbi27i6qgm0lw1wi653r5h6dakddkq") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.35") (deps (list (crate-dep (name "frunk_core") (req "^0.0.22") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.23") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.12") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0vfxwj6n3jdmas6bcjirm4lim8jpr816g1v4mnn9707zahba9a31") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.1 (crate (name "frunk") (vers "0.1.36") (deps (list (crate-dep (name "frunk_core") (req "^0.0.23") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.0.24") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.13") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1cixr73yjnjf0kwd38wb08pkn1fdz0fk048j7rr5pxsgwp5i9p0i") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk-0.2 (crate (name "frunk") (vers "0.2.0") (deps (list (crate-dep (name "frunk_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.0.14") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0kf42i1k3nk1f1hcbyhwwvp2h9nbg5w6vz0y85i6ida7991ng35v")))

(define-public crate-frunk-0.2 (crate (name "frunk") (vers "0.2.1") (deps (list (crate-dep (name "frunk_core") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0i7clqa1kv0fbmjzj0hyjlyhr343277xwqslvnm3k3gghg06ldpf")))

(define-public crate-frunk-0.2 (crate (name "frunk") (vers "0.2.2") (deps (list (crate-dep (name "frunk_core") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1y6ggsfpl2zddd8m972iz3dgjh9j0brmnmpslb2n10gbl1r6r02x")))

(define-public crate-frunk-0.2 (crate (name "frunk") (vers "0.2.4") (deps (list (crate-dep (name "frunk_core") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "frunk_derives") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "frunk_laws") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "07n4zp9klqa2j8kxhi2lcda0c9z3gngmpg9ga25yijzxr5d5a4w3")))

(define-public crate-frunk-0.3 (crate (name "frunk") (vers "0.3.0") (deps (list (crate-dep (name "frunk_core") (req "^0.3.0") (kind 0)) (crate-dep (name "frunk_derives") (req "^0.3.0") (kind 0)) (crate-dep (name "frunk_laws") (req "^0.2.4") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.3") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0pxnvpg1bhz4vjcmsr2h537mfk80qg19css5fbzbqh9cqaik3cdj") (features (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.3 (crate (name "frunk") (vers "0.3.1") (deps (list (crate-dep (name "frunk_core") (req "^0.3.1") (kind 0)) (crate-dep (name "frunk_derives") (req "^0.3.1") (kind 0)) (crate-dep (name "frunk_laws") (req "^0.3.0") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.4") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "04asy0bmv6vwr01ydrm6d4gmbc3zdhsnkq85zm6jvnnmrap4hhjj") (features (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.3 (crate (name "frunk") (vers "0.3.2") (deps (list (crate-dep (name "frunk_core") (req "^0.3.2") (kind 0)) (crate-dep (name "frunk_derives") (req "^0.3.2") (kind 0)) (crate-dep (name "frunk_laws") (req "^0.3.1") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.5") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1jjnlwx037wgmrsk3339kxg85iqrg2cpqlizw9afz0ip2f6800fx") (features (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.4 (crate (name "frunk") (vers "0.4.0") (deps (list (crate-dep (name "frunk_core") (req "^0.4.0") (kind 0)) (crate-dep (name "frunk_derives") (req "^0.4.0") (kind 0)) (crate-dep (name "frunk_laws") (req "^0.3.1") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "0pirlnkkp45pjgcql8g08gp4mms76xf9iwvnxb874zjbspvprmhc") (features (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.4 (crate (name "frunk") (vers "0.4.1") (deps (list (crate-dep (name "frunk_core") (req "^0.4.1") (kind 0)) (crate-dep (name "frunk_derives") (req "^0.4.1") (kind 0)) (crate-dep (name "frunk_laws") (req "^0.4.0") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.1.1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 2)))) (hash "1q71yryms0gyvpz3dy1mqmgsj064ghslaf47l21z6280ylxp1758") (features (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-0.4 (crate (name "frunk") (vers "0.4.2") (deps (list (crate-dep (name "frunk_core") (req "^0.4.2") (kind 0)) (crate-dep (name "frunk_derives") (req "^0.4.2") (kind 0)) (crate-dep (name "frunk_laws") (req "^0.4.1") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.1.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 2)))) (hash "11v242h7zjka0lckxcffn5pjgr3jzxyljy7ffr0ppy8jkssm38qi") (features (quote (("validated" "std") ("std" "frunk_core/std") ("proc-macros" "frunk_proc_macros") ("default" "validated" "proc-macros"))))))

(define-public crate-frunk-enum-core-0.2 (crate (name "frunk-enum-core") (vers "0.2.0") (deps (list (crate-dep (name "frunk") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0l1a893jrwhymm3w4jws6pl3ggz2r68wy0wmd5axw877570f0z7a")))

(define-public crate-frunk-enum-core-0.2 (crate (name "frunk-enum-core") (vers "0.2.1") (deps (list (crate-dep (name "frunk") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0ig8ja3kl2pqw2nfq3lw2ircb6378mm209wz631ckk153xpn4lmp")))

(define-public crate-frunk-enum-core-0.3 (crate (name "frunk-enum-core") (vers "0.3.0") (deps (list (crate-dep (name "frunk") (req "^0.4") (default-features #t) (kind 0)))) (hash "1a5vha5s9vhsvjkbd6wz2yir2zw87cjdfcz95ryzfhli73lmbld2")))

(define-public crate-frunk-enum-derive-0.2 (crate (name "frunk-enum-derive") (vers "0.2.0") (deps (list (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cidjgsgww776yrd54rralnyfyff1jlg2rdir8lbhsj1c0k43bsc")))

(define-public crate-frunk-enum-derive-0.2 (crate (name "frunk-enum-derive") (vers "0.2.1") (deps (list (crate-dep (name "frunk") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "frunk-enum-core") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "frunk_core") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16dpy4wp7nh84q2kvn3qdlx6wf8ksa1v8x6gs4spfsq665pyjbya")))

(define-public crate-frunk-enum-derive-0.3 (crate (name "frunk-enum-derive") (vers "0.3.0") (deps (list (crate-dep (name "frunk") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "frunk-enum-core") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "frunk_core") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "011b7p5n448bb0nwj925hqpza7cnx2kpc1dk388icqgkb44i7in2")))

(define-public crate-frunk_core-0.0.1 (crate (name "frunk_core") (vers "0.0.1") (hash "03qkgq662a5k1swxzvdvp043v86rnvw41n6hvf8j69pc2rlq0a4l")))

(define-public crate-frunk_core-0.0.2 (crate (name "frunk_core") (vers "0.0.2") (hash "0y706nb80sjff17wwsxibmwg4gll9pkkfjwcmpbpqy6z010j2rpr")))

(define-public crate-frunk_core-0.0.3 (crate (name "frunk_core") (vers "0.0.3") (hash "06w45inqgcia0mqbm9x75f3ggs8fgcxzy94dbddl3q7xbq0nwl5s")))

(define-public crate-frunk_core-0.0.4 (crate (name "frunk_core") (vers "0.0.4") (hash "1v926nwr6fxfvga5sglzjp4scavyvbn7dgmv6sljzdgvqg9wcbqg")))

(define-public crate-frunk_core-0.0.5 (crate (name "frunk_core") (vers "0.0.5") (hash "1c50rhsivr8a46dwdidl9lnvrqh4v0s5yxmr6gb1cf9n4yymz39q")))

(define-public crate-frunk_core-0.0.6 (crate (name "frunk_core") (vers "0.0.6") (hash "07xq0nvlmb7mp0826p0anj2i6jgz4npsycfw8ic7v9vzr0kk83xp")))

(define-public crate-frunk_core-0.0.7 (crate (name "frunk_core") (vers "0.0.7") (hash "0lh7cg2gp6n04zh9lih8r4kr55g4zxiqbbw7kwgl4545s96fgx7q")))

(define-public crate-frunk_core-0.0.8 (crate (name "frunk_core") (vers "0.0.8") (hash "13qx3bf7nla7ps27mvnrbf6hfsbia0ql9dlyz2bw79gj5rwmh9xh")))

(define-public crate-frunk_core-0.0.9 (crate (name "frunk_core") (vers "0.0.9") (hash "11yfz9bqbqsb7a598hl10x6yp2ivxiw76bfgcmsr4bhsfs8vcq6x")))

(define-public crate-frunk_core-0.0.10 (crate (name "frunk_core") (vers "0.0.10") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.10") (default-features #t) (kind 2)))) (hash "10x2ic47k6fxx4829wn55v2scb4hgks9qn7k8v7cv2yv6rr3nlg7")))

(define-public crate-frunk_core-0.0.11 (crate (name "frunk_core") (vers "0.0.11") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.11") (default-features #t) (kind 2)))) (hash "0qdfw1lg4a99i1knjnvi2mxgjgjw5qkbk40dvr2iv89001274q6d")))

(define-public crate-frunk_core-0.0.12 (crate (name "frunk_core") (vers "0.0.12") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.12") (default-features #t) (kind 2)))) (hash "0mmsp8bn2mvlb0cp97rnffcd4akxxi45n5fwcsk881m2d063h5db")))

(define-public crate-frunk_core-0.0.13 (crate (name "frunk_core") (vers "0.0.13") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.13") (default-features #t) (kind 2)))) (hash "040mb15nhdcdxnhcy7pkzkz3i91gjc15syvmykhcnv8dh7wcq194")))

(define-public crate-frunk_core-0.0.14 (crate (name "frunk_core") (vers "0.0.14") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.14") (default-features #t) (kind 2)))) (hash "1n02kpmh8nvlwiy9ylfpqrkdv3kaf065b6ddh6nn2q3yqp27yfxb")))

(define-public crate-frunk_core-0.0.15 (crate (name "frunk_core") (vers "0.0.15") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.15") (default-features #t) (kind 2)))) (hash "0kyl698241f5yci3dlhpby41fxvmljabnmcnlwd3zc1vv1kywcaa")))

(define-public crate-frunk_core-0.0.16 (crate (name "frunk_core") (vers "0.0.16") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.16") (default-features #t) (kind 2)))) (hash "1bhzhfdbh2vmygq75yzcp4gvwhmdi0sji0w39ahcxb09fhsi2q65")))

(define-public crate-frunk_core-0.0.17 (crate (name "frunk_core") (vers "0.0.17") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.17") (default-features #t) (kind 2)))) (hash "0lwf61i5rx3av61dc45rmqqnib5j5glk5pyrxq3ckkjd24v60krz")))

(define-public crate-frunk_core-0.0.18 (crate (name "frunk_core") (vers "0.0.18") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.18") (default-features #t) (kind 2)))) (hash "1cz2xiybz1wbwx7rmb65zp14iwhhcb5qizn0srr85n13phpa8gdm")))

(define-public crate-frunk_core-0.0.19 (crate (name "frunk_core") (vers "0.0.19") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.19") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)))) (hash "0f17841s6gjcbfxfw3fxrgb8h23fc82l7bgf8z8pg77rlkyaabi5") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.20 (crate (name "frunk_core") (vers "0.0.20") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.20") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)))) (hash "0gcfnfss2iwcq9gnpbary611kjky1q0ac1np8gwd7216wq6sjb3x") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.21 (crate (name "frunk_core") (vers "0.0.21") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.21") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)))) (hash "0qqlchp3vs5bn0xh9cn9xsiyd03hxsjyg5g4pj0v1588ndyz7h75") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.22 (crate (name "frunk_core") (vers "0.0.22") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.22") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)))) (hash "015jyz7w4v5ijp5kn3gfvsn8b17pil7kz4wvj2yqnkxiddbmaymf") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.0.23 (crate (name "frunk_core") (vers "0.0.23") (deps (list (crate-dep (name "frunk_derives") (req "^0.0.23") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (kind 0)))) (hash "12s4n0hm6wkqgp1vb8nhh969kryi0sjg97s6hg99spnsdjnlhxay") (features (quote (("with-serde" "serde" "serde_derive"))))))

(define-public crate-frunk_core-0.2 (crate (name "frunk_core") (vers "0.2.0") (deps (list (crate-dep (name "frunk") (req "^0.1.36") (default-features #t) (kind 2)) (crate-dep (name "frunk_derives") (req "^0.0.24") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0v2ym7nw8xpk95mxwb52q827xn9kjxzzzp8wz9qn0129vl89zlr0")))

(define-public crate-frunk_core-0.2 (crate (name "frunk_core") (vers "0.2.1") (deps (list (crate-dep (name "frunk") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "frunk_derives") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "10h3srqm6gjmpm0ji5mh7c10mb2cm50ngs9wv8v72jmgl19a5j90")))

(define-public crate-frunk_core-0.2 (crate (name "frunk_core") (vers "0.2.2") (deps (list (crate-dep (name "frunk") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "frunk_derives") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "02pfyjijjh2d376aknmpn5gdvhzvf6zlr782c0y01y265whfas97")))

(define-public crate-frunk_core-0.2 (crate (name "frunk_core") (vers "0.2.3") (deps (list (crate-dep (name "frunk") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "frunk_derives") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1fh6b121ff3f1jk7lyaiwpj4q5afjny4vswx40wzin75cqj186wg")))

(define-public crate-frunk_core-0.2 (crate (name "frunk_core") (vers "0.2.4") (deps (list (crate-dep (name "frunk") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "frunk_derives") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d94qddmlzgjrgqr722r77swvz3r6jxw07f3dadhs8pg4m5rph16")))

(define-public crate-frunk_core-0.3 (crate (name "frunk_core") (vers "0.3.0") (deps (list (crate-dep (name "frunk") (req "^0.2.4") (kind 2)) (crate-dep (name "frunk_derives") (req "^0.2.4") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.2") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1yn90khzlblnfjdlhxfb0d8xazjsx004yp90bgbbm3bcl80jnifw") (features (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.3 (crate (name "frunk_core") (vers "0.3.1") (deps (list (crate-dep (name "frunk") (req "^0.3.0") (kind 2)) (crate-dep (name "frunk_derives") (req "^0.3.0") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.3") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0gj11jby49f2kc516wxpfxyqjn9hw657gplh54ffg56xbajcs10f") (features (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.3 (crate (name "frunk_core") (vers "0.3.2") (deps (list (crate-dep (name "frunk") (req "^0.3.1") (kind 2)) (crate-dep (name "frunk_derives") (req "^0.3.0") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.4") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0n2qklizmzzzrkp0k7qkj9ygnkiycsbbh044l0i6lcps8pb1936x") (features (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.4 (crate (name "frunk_core") (vers "0.4.0") (deps (list (crate-dep (name "frunk") (req "^0.3.2") (kind 2)) (crate-dep (name "frunk_derives") (req "^0.3.2") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.0.5") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "10jlqb5xan2mv02bdnqpppd9g74rzfw61hxm0ljqpgw0xi1wyihj") (features (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.4 (crate (name "frunk_core") (vers "0.4.1") (deps (list (crate-dep (name "frunk") (req "^0.4.0") (kind 2)) (crate-dep (name "frunk_derives") (req "^0.4.0") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.1.0") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0drmkzndbmfmd7vp04z7lbcjpym9chl24hzg5351sc2qll0nsi1a") (features (quote (("std") ("default" "std"))))))

(define-public crate-frunk_core-0.4 (crate (name "frunk_core") (vers "0.4.2") (deps (list (crate-dep (name "frunk") (req "^0.4.1") (kind 2)) (crate-dep (name "frunk_derives") (req "^0.4.0") (kind 2)) (crate-dep (name "frunk_proc_macros") (req "^0.1.1") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mjqnn7dclwn8d5g0mrfkg360cgn70a7mm8arx6fc1xxn3x6j95g") (features (quote (("std") ("default" "std"))))))

(define-public crate-frunk_derives-0.0.1 (crate (name "frunk_derives") (vers "0.0.1") (deps (list (crate-dep (name "frunk_core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "160pm8xlq3nkkk4z47sp350i73ynnysgfx5gcq67mwsq91gi1ssd")))

(define-public crate-frunk_derives-0.0.2 (crate (name "frunk_derives") (vers "0.0.2") (deps (list (crate-dep (name "frunk_core") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "0gdmbblpb49gpa7iyhlna9zqc1h0b4nw9bk3jd0z1jn66lg7zi8q")))

(define-public crate-frunk_derives-0.0.3 (crate (name "frunk_derives") (vers "0.0.3") (deps (list (crate-dep (name "frunk_core") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "127hlv60wfmxv68l8p2s0gnw7bny55m8zi4c0f896y2qdi4x4jfi")))

(define-public crate-frunk_derives-0.0.4 (crate (name "frunk_derives") (vers "0.0.4") (deps (list (crate-dep (name "frunk_core") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.8") (default-features #t) (kind 0)))) (hash "1bk7307nj36n74pc23mx8hf6qy3g2wgkj2yanngix5apzvzx746f")))

(define-public crate-frunk_derives-0.0.5 (crate (name "frunk_derives") (vers "0.0.5") (deps (list (crate-dep (name "frunk_core") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.8") (default-features #t) (kind 0)))) (hash "04npydbzbqrrd3i7pq91v2wks3iwghr0p4bdv25gn2ji0bkg7apm")))

(define-public crate-frunk_derives-0.0.6 (crate (name "frunk_derives") (vers "0.0.6") (deps (list (crate-dep (name "frunk_core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.8") (default-features #t) (kind 0)))) (hash "14rh25pjbz8cmhkw0fv0ahm045mw2bzmlysv6hvrv1jay789p085")))

(define-public crate-frunk_derives-0.0.7 (crate (name "frunk_derives") (vers "0.0.7") (deps (list (crate-dep (name "frunk_core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.8") (default-features #t) (kind 0)))) (hash "0kvc48xdlq0jlc404wl3lf095q8yn9k96h1m9cx54q56iij96kd6")))

(define-public crate-frunk_derives-0.0.8 (crate (name "frunk_derives") (vers "0.0.8") (deps (list (crate-dep (name "frunk_core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.8") (default-features #t) (kind 0)))) (hash "0gh1dd2zrm409hfix7jycj3a0gsidkwns8nj0d52m25kmlwx6a9k")))

(define-public crate-frunk_derives-0.0.9 (crate (name "frunk_derives") (vers "0.0.9") (deps (list (crate-dep (name "frunk_core") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.8") (default-features #t) (kind 0)))) (hash "0f9450f17i0j7p9wnxyhg4il4kmsl2p4wk4ffss5cy63cfnaq8kb")))

(define-public crate-frunk_derives-0.0.10 (crate (name "frunk_derives") (vers "0.0.10") (deps (list (crate-dep (name "frunk_core") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "15q0j406wr3gdhy2zrnn0a2xr1rdzfyqi72rfjzzvz46kvgcrlvh")))

(define-public crate-frunk_derives-0.0.11 (crate (name "frunk_derives") (vers "0.0.11") (deps (list (crate-dep (name "frunk_core") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "0g8gvsb986fhi9937msmrmj0008xsxs8blvw6bk2vh1lzi74ifmz")))

(define-public crate-frunk_derives-0.0.12 (crate (name "frunk_derives") (vers "0.0.12") (deps (list (crate-dep (name "frunk_core") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "1v89y69aghgvacp2r7d5pzrgqydn033jnpfp5sacxydagzmqw33y")))

(define-public crate-frunk_derives-0.0.13 (crate (name "frunk_derives") (vers "0.0.13") (deps (list (crate-dep (name "frunk_core") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "0l8plvr3jmy6ncsm5nhc8hbxdk9ga7x4576hphmd15f22z44fsha")))

(define-public crate-frunk_derives-0.0.14 (crate (name "frunk_derives") (vers "0.0.14") (deps (list (crate-dep (name "frunk_core") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "17nk3mlk2i5rs0dz71ffbra532l5gkgfrlzzilaclfd35c4h7w0f")))

(define-public crate-frunk_derives-0.0.15 (crate (name "frunk_derives") (vers "0.0.15") (deps (list (crate-dep (name "frunk_core") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "0l27hk7mcmwvx9ib50qy8c47bm0d0gn58537d0wvsqj2r7q26qx0")))

(define-public crate-frunk_derives-0.0.16 (crate (name "frunk_derives") (vers "0.0.16") (deps (list (crate-dep (name "frunk_core") (req "^0.0.15") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "0r2w73a2s5hqhh602xr11rg3wqs53nipxr49c3yqhz8i3mkvxnrl")))

(define-public crate-frunk_derives-0.0.17 (crate (name "frunk_derives") (vers "0.0.17") (deps (list (crate-dep (name "frunk_core") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0a37mixzcamslx9hmy89v55qwgr4068x76wkpgim9pwlyf0fnax1")))

(define-public crate-frunk_derives-0.0.18 (crate (name "frunk_derives") (vers "0.0.18") (deps (list (crate-dep (name "frunk_core") (req "^0.0.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "1k5ff6wk8yx1ywvlf9p5jj998pfjckcjkhjf03y1hc19rkpfw0gw")))

(define-public crate-frunk_derives-0.0.19 (crate (name "frunk_derives") (vers "0.0.19") (deps (list (crate-dep (name "frunk_core") (req "^0.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "18cls1c42qaz34zyqkf59bmnkj4nkwwbxwlpvkfi7j74yl6wqfh3")))

(define-public crate-frunk_derives-0.0.20 (crate (name "frunk_derives") (vers "0.0.20") (deps (list (crate-dep (name "frunk_core") (req "^0.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "17qyga9dp8dj2bd3mnvhiary0p22a31z6q8g1z4syiwl0gr6n8yl")))

(define-public crate-frunk_derives-0.0.21 (crate (name "frunk_derives") (vers "0.0.21") (deps (list (crate-dep (name "frunk_core") (req "^0.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "14065wc5af834y5nacxw49map11p1bnl5sv31mmv1qar1b27gi7p")))

(define-public crate-frunk_derives-0.0.22 (crate (name "frunk_derives") (vers "0.0.22") (deps (list (crate-dep (name "frunk_core") (req "^0.0.21") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "01izk653lsh4vv036dzishkmxcvmcmm7wl51izdqlssph17rscv7")))

(define-public crate-frunk_derives-0.0.23 (crate (name "frunk_derives") (vers "0.0.23") (deps (list (crate-dep (name "frunk_core") (req "^0.0.22") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0mkzh5780v2al9qkf4v5hccis9pmcc04dpfirnk86j1qhwgiv975")))

(define-public crate-frunk_derives-0.0.24 (crate (name "frunk_derives") (vers "0.0.24") (deps (list (crate-dep (name "frunk_core") (req "^0.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (default-features #t) (kind 0)))) (hash "0n5c478kwdmpsvn6rp9v8m23yrz9zz598f4nl4bm9flk5kb3knb5")))

(define-public crate-frunk_derives-0.2 (crate (name "frunk_derives") (vers "0.2.0") (deps (list (crate-dep (name "frunk_core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (default-features #t) (kind 0)))) (hash "1l2fb84sziqmymzhqczmp0wwlr53mf5hj39cj1m9mqb8jmb4hs1q")))

(define-public crate-frunk_derives-0.2 (crate (name "frunk_derives") (vers "0.2.1") (deps (list (crate-dep (name "frunk_core") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "07fi7907qivinb1fg6zvnp057p1a94kahfrj1bff3lghrbgfdqp7")))

(define-public crate-frunk_derives-0.2 (crate (name "frunk_derives") (vers "0.2.2") (deps (list (crate-dep (name "frunk_core") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "10w2jkcx5wkb03a3dadbky5g1pbpdkagi7gk6a0gjss2ili7pcll")))

(define-public crate-frunk_derives-0.2 (crate (name "frunk_derives") (vers "0.2.3") (deps (list (crate-dep (name "frunk_core") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "10vkmrsb0bw6p2zd061h885mq54avsm00s5wn8wmbw3ww4my450c")))

(define-public crate-frunk_derives-0.2 (crate (name "frunk_derives") (vers "0.2.4") (deps (list (crate-dep (name "frunk_core") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0gqhbnwm7c53gahshq1s2f84q8zncyyhqmjygbxjx1wp1cw1ga84")))

(define-public crate-frunk_derives-0.3 (crate (name "frunk_derives") (vers "0.3.0") (deps (list (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.3") (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0248cmyffdshyxcxpjw66x7k52qpzsi75nqhm2g8d46kb0zd48ld")))

(define-public crate-frunk_derives-0.3 (crate (name "frunk_derives") (vers "0.3.1") (deps (list (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.4") (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "093pd27kwpda9a8nqy5xi7in4yqqjhw4ksvx48k1w7267dskd3ai")))

(define-public crate-frunk_derives-0.3 (crate (name "frunk_derives") (vers "0.3.2") (deps (list (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.5") (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0rhrmirysfd09k1rvq9jxwmqri3r1hi27kiydhj1dkm73fm3whaj")))

(define-public crate-frunk_derives-0.4 (crate (name "frunk_derives") (vers "0.4.0") (deps (list (crate-dep (name "frunk_proc_macro_helpers") (req "^0.1.0") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "17m0bryq6wwhi1qd1f1k5yd1ihxjgawdxkjcs8qz18y59q44zg1x")))

(define-public crate-frunk_derives-0.4 (crate (name "frunk_derives") (vers "0.4.1") (deps (list (crate-dep (name "frunk_proc_macro_helpers") (req "^0.1.1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0wzir38b6dasnrpn7m5n1khf4ziqyxx3r4bj0zz7rjdl5f8n8cdq")))

(define-public crate-frunk_derives-0.4 (crate (name "frunk_derives") (vers "0.4.2") (deps (list (crate-dep (name "frunk_proc_macro_helpers") (req "^0.1.2") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0blsy6aq6rbvxcc0337g15083w24s8539fmv8rwp1qan2qprkymh")))

(define-public crate-frunk_laws-0.0.1 (crate (name "frunk_laws") (vers "0.0.1") (deps (list (crate-dep (name "frunk") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 0)))) (hash "12gvzwmmjxh6sw5x7yw6xc5as6y447c01j9m2kxh34xswwg9dnsm")))

(define-public crate-frunk_laws-0.0.2 (crate (name "frunk_laws") (vers "0.0.2") (deps (list (crate-dep (name "frunk") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 0)))) (hash "13kjasic35g6c05n5m8vgbfrcxwlwqb9kina33ciw1m0m0afanfi")))

(define-public crate-frunk_laws-0.0.3 (crate (name "frunk_laws") (vers "0.0.3") (deps (list (crate-dep (name "frunk") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 0)))) (hash "05shzmkp23zb46qpcpv1d06lv2qpbw174fdrmf5c6waq4x6jm5vm")))

(define-public crate-frunk_laws-0.0.4 (crate (name "frunk_laws") (vers "0.0.4") (deps (list (crate-dep (name "frunk") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 0)))) (hash "0br12d6xx9n54pqscbrp1s6cxcxynqfm8l3cfdg1kmk01wj4gxih")))

(define-public crate-frunk_laws-0.0.5 (crate (name "frunk_laws") (vers "0.0.5") (deps (list (crate-dep (name "frunk") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 0)))) (hash "058zmm3mcnh6xf56yczb7sd9l1xhsga6dk58b4amwb7k7qwazp8d")))

(define-public crate-frunk_laws-0.0.6 (crate (name "frunk_laws") (vers "0.0.6") (deps (list (crate-dep (name "frunk") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 0)))) (hash "1y1dnmjk72cylvv72wbm7gfkgrlsfiwmrmskvfbw1mvc12x6xg4g")))

(define-public crate-frunk_laws-0.0.7 (crate (name "frunk_laws") (vers "0.0.7") (deps (list (crate-dep (name "frunk") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1zvgnyp4s8fg0m2jd8x9bfwkp4z28n02ws246a393ra75n24ivad")))

(define-public crate-frunk_laws-0.0.8 (crate (name "frunk_laws") (vers "0.0.8") (deps (list (crate-dep (name "frunk") (req "^0.1.30") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1c4wcxbqh4sr3c2ggibn4s70b8np54z35bzamarhj46xgabqw40r")))

(define-public crate-frunk_laws-0.0.9 (crate (name "frunk_laws") (vers "0.0.9") (deps (list (crate-dep (name "frunk") (req "^0.1.31") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1jb3d92ffjbky2qm4gmd7sff28hbkbqb09f79s7ysdmk9qyawx2h")))

(define-public crate-frunk_laws-0.0.10 (crate (name "frunk_laws") (vers "0.0.10") (deps (list (crate-dep (name "frunk") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "19n37s80w54mjfargm8h9qlds2q8hspg9vdpv6qdjij3a4778aqj")))

(define-public crate-frunk_laws-0.0.11 (crate (name "frunk_laws") (vers "0.0.11") (deps (list (crate-dep (name "frunk") (req "^0.1.33") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1vckfpw17x6m45x629byggasb6ajq2wa0c57xy58fai3s09xc4n7")))

(define-public crate-frunk_laws-0.0.12 (crate (name "frunk_laws") (vers "0.0.12") (deps (list (crate-dep (name "frunk") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0py1vcl621qd1yv6hrc80i0ayg8xx0ylidvxkwqzx6q6a61salpq")))

(define-public crate-frunk_laws-0.0.13 (crate (name "frunk_laws") (vers "0.0.13") (deps (list (crate-dep (name "frunk") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0w3mz3fj7hy5li0005ghs8zp98vylbpk36ydanwwxfp05rnly96r")))

(define-public crate-frunk_laws-0.0.14 (crate (name "frunk_laws") (vers "0.0.14") (deps (list (crate-dep (name "frunk") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "17g04171wzabnmzzcy213cqpariplvzd7l3x70vfgnxxs0cd99ds")))

(define-public crate-frunk_laws-0.2 (crate (name "frunk_laws") (vers "0.2.0") (deps (list (crate-dep (name "frunk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "19yqv0d023ddad65lbd9jvn3k1g6rwcc6xx1pgqlxxgf1v3r2j8q")))

(define-public crate-frunk_laws-0.2 (crate (name "frunk_laws") (vers "0.2.1") (deps (list (crate-dep (name "frunk") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1mjhmsxj1y8i04n6ryxkjj9g6zq1hvm01l53zry9466bn0mv1gsr")))

(define-public crate-frunk_laws-0.2 (crate (name "frunk_laws") (vers "0.2.2") (deps (list (crate-dep (name "frunk") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0dynj22c9yqcrvz8x2q8xn9m65fm8fxldfzzymixfrfbdwhhgwi5")))

(define-public crate-frunk_laws-0.2 (crate (name "frunk_laws") (vers "0.2.4") (deps (list (crate-dep (name "frunk") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "127wi590lf9ikbilyjv8cmckd2alc022ivnjh36cy33ilw79iczg")))

(define-public crate-frunk_laws-0.3 (crate (name "frunk_laws") (vers "0.3.0") (deps (list (crate-dep (name "frunk") (req "^0.3.0") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0dmnsjfrcxnkqb1719cdjzicp2v0bbq5v7yxckkdh9lyc0m97ww9")))

(define-public crate-frunk_laws-0.3 (crate (name "frunk_laws") (vers "0.3.1") (deps (list (crate-dep (name "frunk") (req "^0.3.1") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0v31zazjnqvac1qg1x5c6yfdxa5i6yjrvqra1hh8jq98qims7840")))

(define-public crate-frunk_laws-0.3 (crate (name "frunk_laws") (vers "0.3.2") (deps (list (crate-dep (name "frunk") (req "^0.3.2") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0nc2cvfmn51kklnhvspf4rxp2n0xmv44npkn83xck8azgrnwp1h0")))

(define-public crate-frunk_laws-0.4 (crate (name "frunk_laws") (vers "0.4.0") (deps (list (crate-dep (name "frunk") (req "^0.4.0") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0ihl0klb5h49lkrag22ndpd6d0dks2l7ds91h55a54l8ldl5wkwm")))

(define-public crate-frunk_laws-0.4 (crate (name "frunk_laws") (vers "0.4.1") (deps (list (crate-dep (name "frunk") (req "^0.4.1") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1nvm78czqlxyxj3ic6k5s5l85bx7n2r75592aiyilhdnfjaga4a7")))

(define-public crate-frunk_laws-0.4 (crate (name "frunk_laws") (vers "0.4.2") (deps (list (crate-dep (name "frunk") (req "^0.4.2") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0fp8sxlszfwvgnw0fy9nw0wp8qqlh8wk2pai0xapx4h865kiyj83")))

(define-public crate-frunk_laws-0.5 (crate (name "frunk_laws") (vers "0.5.0") (deps (list (crate-dep (name "frunk") (req "^0.4.2") (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "09kj8czxgczg85ni0d4s9r99g08npra124517yg87dadr3iw0pz0")))

(define-public crate-frunk_proc_macro_helpers-0.0.1 (crate (name "frunk_proc_macro_helpers") (vers "0.0.1+pre") (deps (list (crate-dep (name "frunk_core") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0fxb4ywr2rprix9rlv1qi50v1w1rxw1yw1mh257j7k56n0gb1b64") (yanked #t)))

(define-public crate-frunk_proc_macro_helpers-0.0.1 (crate (name "frunk_proc_macro_helpers") (vers "0.0.1") (deps (list (crate-dep (name "frunk_core") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1x8z1p9zvjnh5b0s12p805g7sfxdv51fscq3467ffdnc77p10x6c")))

(define-public crate-frunk_proc_macro_helpers-0.0.2 (crate (name "frunk_proc_macro_helpers") (vers "0.0.2") (deps (list (crate-dep (name "frunk_core") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1xh87x7wn06nk9k622h74hjirw5n0syr8wdqpl8r0skdr79pcw8j")))

(define-public crate-frunk_proc_macro_helpers-0.0.3 (crate (name "frunk_proc_macro_helpers") (vers "0.0.3") (deps (list (crate-dep (name "frunk_core") (req "^0.3.0") (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1w6cmay4p4kvm48z0brws75yx35lak0a07z726py0mzn484qy30r")))

(define-public crate-frunk_proc_macro_helpers-0.0.4 (crate (name "frunk_proc_macro_helpers") (vers "0.0.4") (deps (list (crate-dep (name "frunk_core") (req "^0.3.1") (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1gjd4pv2ja67yqwnz9s6mm5l0whnj7zlw5317vgy0jwpq7d4cig2")))

(define-public crate-frunk_proc_macro_helpers-0.0.5 (crate (name "frunk_proc_macro_helpers") (vers "0.0.5") (deps (list (crate-dep (name "frunk_core") (req "^0.3.2") (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0p7fvnk7jwm9phw6kn4s1l6rfhr39248f489dkk8vb6jdpmvf3fn")))

(define-public crate-frunk_proc_macro_helpers-0.1 (crate (name "frunk_proc_macro_helpers") (vers "0.1.0") (deps (list (crate-dep (name "frunk_core") (req "^0.4.0") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0gpn2pznnlhcnxaslyh1i2h91hrrzc0ydf7wbwzpbih6y5bi5wcr")))

(define-public crate-frunk_proc_macro_helpers-0.1 (crate (name "frunk_proc_macro_helpers") (vers "0.1.1") (deps (list (crate-dep (name "frunk_core") (req "^0.4.1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0l7gczq73i606qg4yyl8ky745bw92w7k94smlywgbc5y3dcjam01")))

(define-public crate-frunk_proc_macro_helpers-0.1 (crate (name "frunk_proc_macro_helpers") (vers "0.1.2") (deps (list (crate-dep (name "frunk_core") (req "^0.4.2") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0b1xl4cfrfai7qi5cb4h9x0967miv3dvwvnsmr1vg4ljhgflmd9m")))

(define-public crate-frunk_proc_macros-0.0.1 (crate (name "frunk_proc_macros") (vers "0.0.1+pre") (deps (list (crate-dep (name "frunk") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "frunk_core") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "06ijf2i1qi7gsznjk5hlgi66pllhrzffp1p8hg3b4qiwgxfxnvgl") (yanked #t)))

(define-public crate-frunk_proc_macros-0.0.1 (crate (name "frunk_proc_macros") (vers "0.0.1") (deps (list (crate-dep (name "frunk") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "frunk_core") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0f9ghnw8p6yw2rpi5j57gnq01nzihj7d3b1ss9kdc08051wz6yii")))

(define-public crate-frunk_proc_macros-0.0.2 (crate (name "frunk_proc_macros") (vers "0.0.2") (deps (list (crate-dep (name "frunk") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "frunk_core") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1yz4rz98fcdxs91dy11bkkx16ns2fgrwscw2yzqg16g2zjkjxa26")))

(define-public crate-frunk_proc_macros-0.0.3 (crate (name "frunk_proc_macros") (vers "0.0.3") (deps (list (crate-dep (name "frunk") (req "^0.2.2") (kind 2)) (crate-dep (name "frunk_core") (req "^0.3.0") (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.0.3") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1w508x5habfsj9i9nbskcc9znagggvjyp9w16apgbhq7xs3akk0y")))

(define-public crate-frunk_proc_macros-0.0.4 (crate (name "frunk_proc_macros") (vers "0.0.4") (deps (list (crate-dep (name "frunk") (req "^0.3.0") (kind 2)) (crate-dep (name "frunk_core") (req "^0.3.1") (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.0.4") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0vq74dj5yh43plmaqbv1sgb9n20npncjhxy10ixcabg5w96cl1xh")))

(define-public crate-frunk_proc_macros-0.0.5 (crate (name "frunk_proc_macros") (vers "0.0.5") (deps (list (crate-dep (name "frunk") (req "^0.3.1") (kind 2)) (crate-dep (name "frunk_core") (req "^0.3.2") (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.0.5") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1fdy5s8w8lakchd8a253l56dzickik4jq7ag4r119mjywvxzfda7")))

(define-public crate-frunk_proc_macros-0.1 (crate (name "frunk_proc_macros") (vers "0.1.0") (deps (list (crate-dep (name "frunk") (req "^0.3.2") (kind 2)) (crate-dep (name "frunk_core") (req "^0.4.0") (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.1.0") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1k6va5ikn71pkcskziggm5r2lxl5cmsqyyq01dgbijzcb62bsy50")))

(define-public crate-frunk_proc_macros-0.1 (crate (name "frunk_proc_macros") (vers "0.1.1") (deps (list (crate-dep (name "frunk") (req "^0.4.0") (kind 2)) (crate-dep (name "frunk_core") (req "^0.4.1") (kind 0)) (crate-dep (name "frunk_proc_macros_impl") (req "^0.1.1") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1q7szgh805skmhhihgww25xnavhqgywl5czzzy5b9sjx517m40ga")))

(define-public crate-frunk_proc_macros-0.1 (crate (name "frunk_proc_macros") (vers "0.1.2") (deps (list (crate-dep (name "frunk_core") (req "^0.4.2") (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.1.2") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "14rsw5znll59xhlpy4il0cza1v1gxw9qwpn0845k0sws98fmmf3i")))

(define-public crate-frunk_proc_macros_impl-0.0.1 (crate (name "frunk_proc_macros_impl") (vers "0.0.1+pre") (deps (list (crate-dep (name "frunk_core") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "110r9fwbzxi6w9amhxl1sgx9x9hn4r5x2zylqdv4wqr9kkxhm3dz") (yanked #t)))

(define-public crate-frunk_proc_macros_impl-0.0.1 (crate (name "frunk_proc_macros_impl") (vers "0.0.1") (deps (list (crate-dep (name "frunk_core") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "01030nmvga2ra6lfih3jalk55a52qmr5pdgdgmmlcgf5b3i0mhp3")))

(define-public crate-frunk_proc_macros_impl-0.0.2 (crate (name "frunk_proc_macros_impl") (vers "0.0.2") (deps (list (crate-dep (name "frunk_core") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1l739w64b1jcd033f0c3jhm9hzaj0dnsjypihl9m149pby277q4q")))

(define-public crate-frunk_proc_macros_impl-0.0.3 (crate (name "frunk_proc_macros_impl") (vers "0.0.3") (deps (list (crate-dep (name "frunk_core") (req "^0.3.0") (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.3") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "03dfjjfqw3yr27b4s2a6xklfnpcwgvlg3s0w846gjvvxvd5k7d21")))

(define-public crate-frunk_proc_macros_impl-0.0.4 (crate (name "frunk_proc_macros_impl") (vers "0.0.4") (deps (list (crate-dep (name "frunk_core") (req "^0.3.1") (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.4") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0z6d0l1j26m77ljmfjm4mm3jhaynqy3sadbaz2ldz59f17gvmkjs")))

(define-public crate-frunk_proc_macros_impl-0.0.5 (crate (name "frunk_proc_macros_impl") (vers "0.0.5") (deps (list (crate-dep (name "frunk_core") (req "^0.3.2") (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.0.5") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1jyiy7pfcsqhd86nfv1qn9fd9bjqbvypc4pyfgkj792dycv6gpg0")))

(define-public crate-frunk_proc_macros_impl-0.1 (crate (name "frunk_proc_macros_impl") (vers "0.1.0") (deps (list (crate-dep (name "frunk_core") (req "^0.4.0") (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.1.0") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0ckcvzapkmgyf1akq7r6nk13p1nal2wzp23kym17xxd41ygskyvg")))

(define-public crate-frunk_proc_macros_impl-0.1 (crate (name "frunk_proc_macros_impl") (vers "0.1.1") (deps (list (crate-dep (name "frunk_core") (req "^0.4.1") (kind 0)) (crate-dep (name "frunk_proc_macro_helpers") (req "^0.1.1") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0lr65bll9jsfls6jz9kbv57jk1hhwffgqs3q3bzfg3n19jbjv00a")))

(define-public crate-frunk_utils-0.1 (crate (name "frunk_utils") (vers "0.1.0") (deps (list (crate-dep (name "frunk") (req "^0.4") (default-features #t) (kind 0)))) (hash "1f50jmqgcj52wg6fn2iqsnqjyysparyqcnb63ppwnifg9gqv6hl4")))

(define-public crate-frunk_utils-0.2 (crate (name "frunk_utils") (vers "0.2.0") (deps (list (crate-dep (name "frunk") (req "^0.4") (default-features #t) (kind 0)))) (hash "1cc2jxl7qzyk2s8xh3ybf2465hzc0kakjpm4p9rj5cjpcahc1nxb")))

(define-public crate-frunk_utils-0.2 (crate (name "frunk_utils") (vers "0.2.1") (deps (list (crate-dep (name "frunk") (req "^0.4") (default-features #t) (kind 0)))) (hash "0w3c7pdj1w3ldamqy0y2881pllwirp8d7rp4paz6q0p4kgkzji50")))

(define-public crate-frunk_utils-0.2 (crate (name "frunk_utils") (vers "0.2.2") (deps (list (crate-dep (name "frunk") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fs5rwwdqgi5zy8nydvzqsbk3a1aj4098bknk7cy8g7hi5f90nmb")))

(define-public crate-frunk_utils_derives-0.1 (crate (name "frunk_utils_derives") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0j1ldwlnd763ws7nj3j8wr2rlpvg6rcwnwb5wcgmnq8cbk3qng63")))

(define-public crate-frunk_utils_derives-0.1 (crate (name "frunk_utils_derives") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "13a9palfnj72hdc751ahj11fvl1wnn457wywhakd6590nndj5i37")))

