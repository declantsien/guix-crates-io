(define-module (crates-io fr oz) #:use-module (crates-io))

(define-public crate-frozen-1 (crate (name "frozen") (vers "1.0.0") (hash "16fn7x0m5k0k25hxlgyfvrkihma9v9qhrbcfxjk6c21h13kpnrsz")))

(define-public crate-frozen-hashbrown-0.1 (crate (name "frozen-hashbrown") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "03l0p3snv7v2lvbyb7jljdqcakwva4kfwxd7y94jhc4i5f3jyjnp") (rust-version "1.72")))

(define-public crate-frozenset-0.1 (crate (name "frozenset") (vers "0.1.0") (hash "0ksrrl7n29sawi3kdl7yfmrlz01p1mxdh5ymlxah7lc8icmvh99v")))

(define-public crate-frozenset-0.1 (crate (name "frozenset") (vers "0.1.1") (hash "12kvq2qqjiizmb8370mp350lwnnhkzw6qf9425xij9k43681vlfb")))

(define-public crate-frozenset-0.2 (crate (name "frozenset") (vers "0.2.0") (hash "175b1py8cypcn77qjv0acjc9yck68x10fw3haci91zbmp2iwj5da")))

(define-public crate-frozenset-0.2 (crate (name "frozenset") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.164") (optional #t) (default-features #t) (kind 0)))) (hash "0gn312ifqiwkjf3l513qpilnx0hsr5p9wpjy2121y8ymlvdkj848")))

(define-public crate-frozenset-0.2 (crate (name "frozenset") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.164") (optional #t) (default-features #t) (kind 0)))) (hash "12ys2xbsy0rwq5mnf9yw6mh8zg8p1dwab8bd0xliwqj6iwr11cg5")))

