(define-module (crates-io fr ib) #:use-module (crates-io))

(define-public crate-fribidi-0.1 (crate (name "fribidi") (vers "0.1.0") (deps (list (crate-dep (name "fribidi-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "0i961kfqbkskyis2258pr1a5cljjxhzwrmrai5ajh99mxgw8y879") (features (quote (("static" "fribidi-sys/static") ("default" "pkg-config"))))))

(define-public crate-fribidi-0.1 (crate (name "fribidi") (vers "0.1.1") (deps (list (crate-dep (name "fribidi-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "1wq7ywrvjxfpz3pxk34was0yk5246v9mfljnr7ksfff15yjgk7qg") (features (quote (("static" "fribidi-sys/static") ("default" "pkg-config"))))))

(define-public crate-fribidi-0.1 (crate (name "fribidi") (vers "0.1.2") (deps (list (crate-dep (name "fribidi-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "1wrys6cy8gyyl48gp530vhf9h9ngljrycn0xbpsjrw5cah66i815") (features (quote (("static" "fribidi-sys/static") ("default" "pkg-config"))))))

(define-public crate-fribidi-sys-0.1 (crate (name "fribidi-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "06qlyqsckibrlynimkfck4cjr29i2jyz4z6931dcj0sh6mgk66bn") (features (quote (("static") ("default" "pkg-config")))) (links "fribidi")))

