(define-module (crates-io fr al) #:use-module (crates-io))

(define-public crate-fral-1 (crate (name "fral") (vers "1.0.0") (deps (list (crate-dep (name "im") (req "^9.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "136pznlbkb2i21yn0kcj8i6p8fv4nmf6sbh2v3zdhcz9qsmfjgip")))

(define-public crate-fral-1 (crate (name "fral") (vers "1.0.1") (deps (list (crate-dep (name "im") (req "^9.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "02zazyz9fx7y5pzbj9g1xsw79dj8v771dajdiz60jx5ph7mfdczl")))

(define-public crate-fral-1 (crate (name "fral") (vers "1.0.2") (deps (list (crate-dep (name "im") (req "^10.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0rj5yy7bfzbhdnl397vqjpx0l95f2c08px3zzmmahg0nmdiar72p")))

