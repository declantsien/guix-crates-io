(define-module (crates-io fr cd) #:use-module (crates-io))

(define-public crate-frcds-0.1 (crate (name "frcds") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "01kpxnaapnd8br0gmr9d1l8q14yghd232lg6c524ka2dfbk6gvqn")))

