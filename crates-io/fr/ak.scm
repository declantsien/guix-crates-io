(define-module (crates-io fr ak) #:use-module (crates-io))

(define-public crate-frakegps-0.1 (crate (name "frakegps") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-view") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1q3zl2pig0gjkpiy0b2yqw3in4cpiygnf94xppmphlf43bfzznyk")))

(define-public crate-frakegps-0.1 (crate (name "frakegps") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "web-view") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0ian07d7nnlj6pwqprm6qk6mzj3ab71f5s0xc80bx9plzar7v24h")))

(define-public crate-frakegps-0.2 (crate (name "frakegps") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.29.0") (default-features #t) (kind 0)))) (hash "03aw6y5zlpbs5bfa8m1jgqkcznymsf9hfwy3lh7nm7qjgjc3labl")))

