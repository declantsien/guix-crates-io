(define-module (crates-io fr es) #:use-module (crates-io))

(define-public crate-fresco-0.1 (crate (name "fresco") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "embed_plist") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "plist") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0wcm2npqb2q83dnnf1qxln3xcyigjcv98gcx2xcbvkw04yx1air9")))

(define-public crate-fresh-0.1 (crate (name "fresh") (vers "0.1.0") (hash "1qxnxa8wqwyn9skm9p1i9izdl5j15kbbxmd96h2kmkfa3d1iq89z")))

(define-public crate-fresnel-0.1 (crate (name "fresnel") (vers "0.1.0") (hash "0m1z4diilwkffdfgbrrny4w5s1ckjmdb5x8qsrh4n6lr0vs15c8l")))

