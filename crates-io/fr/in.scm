(define-module (crates-io fr in) #:use-module (crates-io))

(define-public crate-fring-0.1 (crate (name "fring") (vers "0.1.0") (hash "1zgnxgid5y4lbgvffz94rs25mm31rh5x464s51dbbsgfkyiy2mrm")))

(define-public crate-fring-0.1 (crate (name "fring") (vers "0.1.1") (hash "0sx7mqcckfp452cyzmky3an277af7vb6y9cfzgqchb7b3971ncmp")))

(define-public crate-fring-0.1 (crate (name "fring") (vers "0.1.2") (hash "0pmlc8gr3bsckbyzf6r6n4iy0pm7vdi5k4ifxvfdcmzbabj9k62q")))

(define-public crate-fring-0.1 (crate (name "fring") (vers "0.1.3") (hash "0g0s41w8sdy69x3dzk0v4wpji1i840fdbrz83pl1nsid47g0fwgv")))

(define-public crate-fring-0.1 (crate (name "fring") (vers "0.1.4") (hash "1xm4r9a8a7g8yvjh6bv9hfnmbkgc1v0yx67cr3ivnm9l65macsz2")))

(define-public crate-fring-0.2 (crate (name "fring") (vers "0.2.0") (hash "0qrpr3z1h2jfqgkp4575xay9g5a90jsfjz1l6njr1nkgr2ghmv65")))

(define-public crate-fring-0.2 (crate (name "fring") (vers "0.2.1") (hash "1px7jvd6km8ww8id3nb1lgbmkab4vz6rczpfjdr6rybaq406lmsn")))

(define-public crate-fring-0.3 (crate (name "fring") (vers "0.3.0") (hash "0mdnsl4i32gi3kmkl2b875h8ps7rbm77bwiiiqg7bx995q31sl5s")))

(define-public crate-fring-0.3 (crate (name "fring") (vers "0.3.1") (hash "1gzmwzxv1px3ssdb12v1mir8z31cd9q3lrbb2mlrn66l7rqbnhnn")))

(define-public crate-fring-0.3 (crate (name "fring") (vers "0.3.2") (hash "0za19qfndqxflhq4dakdi840718l25xqv1gz2yw6g1qamp65hvs4")))

(define-public crate-fring-0.3 (crate (name "fring") (vers "0.3.3") (hash "1v593xknxfwgarbnk10xz6174r946p3gmp3883gz3p1kdfxq8sz0")))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.0.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.6") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0417wm7c8km6bgmnda2j0723bw27ib4i3pdnlch417kf6zzcpq4q") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wgc6nl83wddb787yifn144shc6aycva6bb5fx9f9nybqj52sxk3") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "06ma8g44ybrw6whny78jq3sgxsfhbxx9xqji55cj4wzqmalgw4kr") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vb9cw6kdxvp6lh7c9lxrsvrz4nn150r3ppaqx8w6vyqds8maihp") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.0.4") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0a1n37yrfjsc34afcx154ymi2ydipmxv44zg68hggzmy48mlj7rz") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.0.5") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mbrq52rk8dbanp4fkn7ba8vl9yzbh9q58w53a7zrpvf0y2pc9f5") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "12np8ykc8l63pxk2kmc6zdikwj7c85b8cxxlfs6ywpnqzbf8jxlq") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1mbr4bzbvlfzac79babd1647ms7474ylhhaf9j51ccdx8v5bv16i") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-1 (crate (name "fringe") (vers "1.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "valgrind_request") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wbj58qvr849aml31qx59vhib9dry037qf8gl34jk77h2n2svb0y") (features (quote (("valgrind" "valgrind_request") ("default" "alloc" "valgrind") ("alloc"))))))

(define-public crate-fringe-futures-0.1 (crate (name "fringe-futures") (vers "0.1.0") (deps (list (crate-dep (name "fringe") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "00incm1qjm2bym9410lhw3avzdzci7h9yd7k2brf9xdkvab2dbii") (yanked #t)))

