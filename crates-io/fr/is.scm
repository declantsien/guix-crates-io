(define-module (crates-io fr is) #:use-module (crates-io))

(define-public crate-frisbee-0.2 (crate (name "frisbee") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.2") (default-features #t) (kind 0)))) (hash "059igwyhr64pwb0lnshi3bqb6qaxb8bnnd005s73dxabry3r3inn")))

(define-public crate-frisbee-0.3 (crate (name "frisbee") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.2") (default-features #t) (kind 0)))) (hash "0grwdv9lav455m4hla0wp41z4vwhp3q7s8ygczwrq23mn7gq2k4f")))

(define-public crate-frisk-0.1 (crate (name "frisk") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "jwalk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0aif5zsm7x8vw466bk87ck1hbl0psz4gvwkfhb0s15kgr0zgvfqr")))

(define-public crate-frisk-0.1 (crate (name "frisk") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.32") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "jwalk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "17czkm53m42d5isxkdayb5bjm21dx31ym1848jxbp7j5iawcsyqd")))

