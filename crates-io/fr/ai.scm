(define-module (crates-io fr ai) #:use-module (crates-io))

(define-public crate-fraim-0.0.0 (crate (name "fraim") (vers "0.0.0") (hash "0vrjlbsp6xf9ac1ykbxhcs581k67h2zlh2frlz74fz4ydyy2kl7v")))

(define-public crate-fraim-0.0.1 (crate (name "fraim") (vers "0.0.1") (hash "0v90y8h8zwsbmyx71rli7pqv3qh9d6w3cmn84hn6xnj1c40n24gn")))

