(define-module (crates-io fr ot) #:use-module (crates-io))

(define-public crate-frotate-0.1 (crate (name "frotate") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1f8avplwkvaf24vp6mwkqrhh6d9iw3sr7jk3ms620rgq33fag1qn")))

