(define-module (crates-io fr ic) #:use-module (crates-io))

(define-public crate-fricgan-0.1 (crate (name "fricgan") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "05n05k0snxvhw9dgwhj3qpmx325x862cgkvwby0cyjbyhs3zaizh") (features (quote (("vlq-string" "std" "vlq" "num-traits" "num-traits/std") ("vlq-64" "vlq") ("vlq-32" "vlq") ("vlq") ("unsafe") ("std") ("safety-checks") ("io-u8") ("io-u64") ("io-u32") ("io-u16") ("io-string" "std" "num-traits" "num-traits/std") ("io-i8") ("io-i64") ("io-i32") ("io-i16") ("io-f64") ("io-f32") ("default"))))))

