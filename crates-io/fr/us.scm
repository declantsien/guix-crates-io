(define-module (crates-io fr us) #:use-module (crates-io))

(define-public crate-frusa-0.1 (crate (name "frusa") (vers "0.1.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1jcfdxgcww0a3j5zx7h426nylgjbdpb7yb71d1h542wzi8qpk8ll") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-frusa-0.1 (crate (name "frusa") (vers "0.1.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "04dlrdkxfsh6ly1vrmhhkan8bbrrzn61m787cy1rk9ih1kqq11nz") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-frusa-0.1 (crate (name "frusa") (vers "0.1.2") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 2)) (crate-dep (name "talc") (req "^4.2.0") (default-features #t) (kind 2)))) (hash "188hil80h0xficirky8hpwgi35wk8rdvqdl0zpvhl5fslq0291r0") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-frusk-core-0.0.1 (crate (name "frusk-core") (vers "0.0.1") (hash "18j4b6pyrs37bib5zxygrbn39p8b05xwnwlppig3wji6xalb8p8r")))

(define-public crate-frust-0.0.1 (crate (name "frust") (vers "0.0.1") (hash "1hq41qrlw4sap9pcqcy54h06ppk52ax5lxcwwqms0b0li8ydx5hj")))

(define-public crate-frust-0.0.2 (crate (name "frust") (vers "0.0.2") (hash "110f7sqk5rkzgbkzqjs8n3fh6ylszihz0gw2skdhdg7rwlcbihq5") (yanked #t)))

(define-public crate-frust-0.0.3 (crate (name "frust") (vers "0.0.3") (hash "1w9cn6p716n46d5gkndkkf0zlszm4rn8a641cwkr1czq20ivx5r9")))

(define-public crate-frust-0.0.4 (crate (name "frust") (vers "0.0.4") (hash "0c7gq63h11lnf9pvkixipssfxjyy1bahr5q6jzqbkjbndm6ia35w")))

(define-public crate-frust-0.0.5 (crate (name "frust") (vers "0.0.5") (hash "0vkgqhknlqym10jzcbhmkw5pd7vdsk16r0x6pm59al5wlph7058d")))

(define-public crate-frust-0.0.6 (crate (name "frust") (vers "0.0.6") (hash "04qrvvhf9g4nl1fpjbx7ai6g6x2f897yxwysjzmgybyfhvjsfyfs")))

(define-public crate-frust-0.0.7 (crate (name "frust") (vers "0.0.7") (hash "10dxq9fbpd01pd34m4pkf59kx6j80561k9p2xysrxcnyksl63mvi")))

(define-public crate-frustool-0.1 (crate (name "frustool") (vers "0.1.0") (deps (list (crate-dep (name "cpython") (req "^0.5") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1pvcb5kyvq1854a4b8z5h3d56p60ljngl1j2lmwy17cv6ypigbb3")))

(define-public crate-frustum-0.1 (crate (name "frustum") (vers "0.1.0") (deps (list (crate-dep (name "euclid") (req "^0.20.7") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0k462m6i8hyn88c9s8ivpmqywz7jwrxdv4yw0hgpdck0v3jv29l6")))

(define-public crate-frustum-0.1 (crate (name "frustum") (vers "0.1.1") (deps (list (crate-dep (name "euclid") (req "^0.20.7") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "19xfmcspnwvkp5821mfxj5cmlmyq6h66viaq15rbhikjzbi7m6dn")))

(define-public crate-frustum-0.2 (crate (name "frustum") (vers "0.2.1") (deps (list (crate-dep (name "euclid") (req "^0.20.7") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 2)) (crate-dep (name "palette") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gz3b7qrjg70i8z06w9kaynklvqlpp2fkfw2af9a2q8ci6dif3j8") (features (quote (("serialization" "serde" "euclid/serde"))))))

(define-public crate-frustum_query-0.1 (crate (name "frustum_query") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (optional #t) (default-features #t) (kind 0)))) (hash "074zwdn2s5b4f8m6nvp1l0xjhr3ij7xd5g83wswqmnib6i512znn") (features (quote (("multithreaded_rayon" "rayon" "time") ("default"))))))

(define-public crate-frustum_query-0.1 (crate (name "frustum_query") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (optional #t) (default-features #t) (kind 0)))) (hash "0mzym5yx070ybah6zj6mcmzcwnhjyz6pbw06ix6sharrg4z9r472") (features (quote (("multithreaded_rayon" "rayon" "time") ("default"))))))

(define-public crate-frustum_query-0.1 (crate (name "frustum_query") (vers "0.1.2") (deps (list (crate-dep (name "rayon") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (optional #t) (default-features #t) (kind 0)))) (hash "01asxw39rwmb91kq9x2v41nvini7zpzl51w8gm9b49pdmck1qxz1") (features (quote (("multithreaded_rayon" "rayon" "time") ("default"))))))

