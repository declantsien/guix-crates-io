(define-module (crates-io fr cv) #:use-module (crates-io))

(define-public crate-frcv-0.1 (crate (name "frcv") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0mzvdhs211b52cc39d41m19skmfvwlz91iwpnmp15s737a1wj787") (yanked #t)))

(define-public crate-frcv-0.1 (crate (name "frcv") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1nnmyka72425fqpbd3ihhyibv9zl3am5893b06dy969wl2h1bwsi") (yanked #t)))

