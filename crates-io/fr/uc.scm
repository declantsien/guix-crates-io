(define-module (crates-io fr uc) #:use-module (crates-io))

(define-public crate-fructose-0.1 (crate (name "fructose") (vers "0.1.0") (hash "0v3gc6slhrnlqkl9hwpi4xvwz1mn0nrs7xm8b0msbf3b3c1g3pj7")))

(define-public crate-fructose-0.2 (crate (name "fructose") (vers "0.2.0") (hash "139m3ish3f2rhdk6bjcqj36289z3hxfq4qfyqkywkzgb3zl21bwm")))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.0") (hash "13lax1z98d102fqvcasjh2dcwjngp1idxgdnjk2hs0bdi2ykfq7s")))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.1") (hash "0x1vgqvrjmr5xclc9g0rrfllfg73065rmdra2hf2lk652l4hmnhl")))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.2") (hash "1pcnnj2i15hg2w3krs6x2xd7d2x41qz3wch852r2qfjdd3v4bm12") (features (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.3") (hash "115snz63x7fwzgzh55f0zg6v7f68sp2hac2h4mahkx7dwd2wlqqj") (features (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.4") (hash "05zs0cg5kkbirmafwcrgwpzp2jlbhdhz3f7mgdjfq0a77wpjm5z8") (features (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.5") (hash "1b1j7r8f5araqxr4fqs2j8a6arky9bqg4xgfv1j0gc2pjv9lfw98") (features (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.6") (hash "0h5ylgavng3cn89v2a34a0q7h3dl3alz71z5dkag5kgss3dx3f1v") (features (quote (("specifics") ("default" "specifics") ("const") ("Full" "specifics" "const"))))))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.7") (hash "1x5aqm6a68n7j6b1clcjqn5s3h4zxpxfh85cq6a0bbn61qkagl5i") (features (quote (("specifics") ("full" "specifics") ("default" "full"))))))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.8") (hash "0cpks7mg7x3crzyc3s5dvdcg74h54xkbg26llmpb8kbbf4nrjjfw") (features (quote (("specifics") ("full" "specifics") ("default" "full"))))))

(define-public crate-fructose-0.3 (crate (name "fructose") (vers "0.3.9") (hash "1cm14qqzrf0y0khl7zb6f572h121pii3alwcsb0kqdzalqsnf62c") (features (quote (("specifics") ("full" "specifics") ("default" "full"))))))

