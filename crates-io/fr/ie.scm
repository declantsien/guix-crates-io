(define-module (crates-io fr ie) #:use-module (crates-io))

(define-public crate-friedrich-0.1 (crate (name "friedrich") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0h3hcnbfmbjngv7fzf3ncc0qdyk5hi4d67kn19d0dih26vnshfm0")))

(define-public crate-friedrich-0.1 (crate (name "friedrich") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0dm96inw4kwbikv5vzrsr99pnzsglb8kik8nxhhpwxd9ziw523y1")))

(define-public crate-friedrich-0.2 (crate (name "friedrich") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1r7r02zjfwcf2f3ab9b21yif260bz1gwpql7q4aa232b9d8v4bkk")))

(define-public crate-friedrich-0.2 (crate (name "friedrich") (vers "0.2.1") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "112s7lz80qg5rqxalxykh8jfw960vrs3qhwvir856dpzxn39h3wk")))

(define-public crate-friedrich-0.2 (crate (name "friedrich") (vers "0.2.2") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0ac0nsf46npgycb39fnnwcxp5i421lpshkdpjhzflhlhr893p42y")))

(define-public crate-friedrich-0.3 (crate (name "friedrich") (vers "0.3.0") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0nwq3llgcwkb425175nh9i1rxmblcjhl971kv6n97nbkd0vk3gx7")))

(define-public crate-friedrich-0.3 (crate (name "friedrich") (vers "0.3.1") (deps (list (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1fwv4mdhn4h9lb0ci1c1zcfy4ai0f3qn5y5klvmpka8g4sapy7a9") (features (quote (("friedrich_ndarray" "ndarray"))))))

(define-public crate-friedrich-0.3 (crate (name "friedrich") (vers "0.3.2") (deps (list (crate-dep (name "nalgebra") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1schf12qh6nhp3cnqchl5abs505lmzsckwwk9piw28v3h7wrybcg") (features (quote (("friedrich_ndarray" "ndarray"))))))

(define-public crate-friedrich-0.3 (crate (name "friedrich") (vers "0.3.3") (deps (list (crate-dep (name "nalgebra") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1scpc52i0rvcgfnx24x18wvh2q5yjrk3a92rvhrqcjxz8q1r53kq") (features (quote (("friedrich_ndarray" "ndarray"))))))

(define-public crate-friedrich-0.4 (crate (name "friedrich") (vers "0.4.0") (deps (list (crate-dep (name "nalgebra") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1f7ls57kh75wk1cr5ciix1i1inhwij8naqgr29z1v8182x7lvvbf") (features (quote (("friedrich_serde" "serde" "nalgebra/serde-serialize") ("friedrich_ndarray" "ndarray") ("default" "friedrich_serde"))))))

(define-public crate-friedrich-0.4 (crate (name "friedrich") (vers "0.4.1") (deps (list (crate-dep (name "nalgebra") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "18p2yxbcf2bf6wn1ml5kcqipb924mnvrl7a1gn5m59jdb6kgg8s9") (features (quote (("friedrich_serde" "serde" "nalgebra/serde-serialize") ("friedrich_ndarray" "ndarray") ("default" "friedrich_serde"))))))

(define-public crate-friedrich-0.5 (crate (name "friedrich") (vers "0.5.0") (deps (list (crate-dep (name "nalgebra") (req "^0.30.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d7v3923yviaayazprgla83hdgs3c02q183siym2navn0kgyb62h") (features (quote (("friedrich_serde" "serde" "nalgebra/serde-serialize") ("friedrich_ndarray" "ndarray") ("default" "friedrich_serde"))))))

(define-public crate-friend-0.0.0 (crate (name "friend") (vers "0.0.0") (hash "1dmlj0nlsgc1kgpchnwcvgqijbskwrg5154529i55vzsy6z6b8vv") (yanked #t)))

(define-public crate-friendgrow-0.1 (crate (name "friendgrow") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.4.8") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hvmf311naikjkkdfmwk2b9yz34bfnwxxdwncjd55hx2l9ikqzzf")))

(define-public crate-friendgrow-0.2 (crate (name "friendgrow") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.4.8") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jakw9xq5r28vn33vvzz05sp6wfrfks6ldv9y4plcdpz8d3cxmx5")))

(define-public crate-friendgrow-0.2 (crate (name "friendgrow") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.4.8") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02i2nxj2wh7z8q0vv32wqmckq0gpsrd6qqi617azcxynbman8542")))

(define-public crate-friendgrow-0.2 (crate (name "friendgrow") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.4.8") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1w5zck21v735ms9k2vsnqx1fja0zh2m2g3073jf0xva8x9fs6540")))

(define-public crate-friendly-0.1 (crate (name "friendly") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1") (default-features #t) (kind 2)))) (hash "0366pgnsks5v5lj3s8m5yfkfnq12ad8s8s00fpg9ipyjmgj8yl5j")))

(define-public crate-friendly-chess-0.1 (crate (name "friendly-chess") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0r0xg8056jgn4k1b75yrpyvahy75dm2a0vpd2snxlcwsrvl6dz4a")))

(define-public crate-friendly-chess-0.2 (crate (name "friendly-chess") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "04ll9570s9rqr2lfx8swp19nncmpxrlvdx4qxp7zs8hn019gz7lg")))

(define-public crate-friendly-chess-0.3 (crate (name "friendly-chess") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "02lhrxwmsqajlp79f0gbsdcvvx720izcpxk75dzzk92v19ifzwqc")))

(define-public crate-friendly-chess-0.4 (crate (name "friendly-chess") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1vli3xcx4xk8xr29jwfli9b0jmpnf17w5wjnnrlr1ym4jrrk6c3f")))

(define-public crate-friendly-chess-0.5 (crate (name "friendly-chess") (vers "0.5.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "05ial3099zp65qlis3lqarn48n760nxj9fdra8daqxv5ypq39ric")))

(define-public crate-friendly-chess-0.6 (crate (name "friendly-chess") (vers "0.6.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0a3zcwq11dh0riw88c9zykjppccyrcaxyx202ixs8kvjbb3vksa3")))

(define-public crate-friendly-errors-0.1 (crate (name "friendly-errors") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1") (default-features #t) (kind 2)))) (hash "1xnna4pqnpyp6wjdp59r9qa69icq1sc1nliphj4wvqb5s1hrc431")))

(define-public crate-friendly-errors-0.2 (crate (name "friendly-errors") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1") (default-features #t) (kind 2)))) (hash "1d570bs8kmrnlqqzdh5z02s1kbv6p9d5c2c3d481i21mkqd1qing")))

(define-public crate-friendly-zoo-0.1 (crate (name "friendly-zoo") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0x9yd8a2kjbzbfr91d0bm3bid70j1vfg3ys5cl4dmbic1srk3yg8")))

(define-public crate-friendly-zoo-0.1 (crate (name "friendly-zoo") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1frsh6b0csicrg8f35sni2drkkh50im6a1wiyvda1y39s8wcyyw8")))

(define-public crate-friendly-zoo-0.2 (crate (name "friendly-zoo") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0q0m2v8ak8dggip4mx0865xipg3sczpjh482x6gcwikdq8pg7xgb")))

(define-public crate-friendly-zoo-0.2 (crate (name "friendly-zoo") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0xnfn0g8gsi8chqsykb66nib55lpwb558lxsbimmnq5ldk62ql76")))

(define-public crate-friendly-zoo-0.3 (crate (name "friendly-zoo") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0grcz6mla51z9cf9rhcis9j01acd2jyxjwj1pvm6y1hv94s1jn1m")))

(define-public crate-friendly-zoo-1 (crate (name "friendly-zoo") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1x5aq0vrlp7606prm7fdxwz0rkg5jldify9ab7f8ln4qn0n89rkn")))

(define-public crate-friendly-zoo-1 (crate (name "friendly-zoo") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16g6z4q8kgyvq7a666jczsa6qj6b5vjsxcfpljcnd1sg1gqpjfx8")))

(define-public crate-friendly_id-0.1 (crate (name "friendly_id") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0pc3nn7fvyqilmwl6lh0k3scan3djibjpz741i61zyda98mb873c")))

(define-public crate-friendly_id-0.1 (crate (name "friendly_id") (vers "0.1.1") (deps (list (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "17nb5iccgb1bl7css60b6fhhh8pmdhqii1kyrbnz4ni2a76i429m")))

(define-public crate-friendly_id-0.1 (crate (name "friendly_id") (vers "0.1.2") (deps (list (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "125xrjhysl4b8z0bbn7sh6k8rzpz5giwy83f4jsrl6xxhm56b1jv")))

(define-public crate-friendly_id-0.2 (crate (name "friendly_id") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0k54h0g0x87ky3061p4yfmbllaqx355v2gqx6g6y3dj96v3wwn12")))

(define-public crate-friendly_id-0.3 (crate (name "friendly_id") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "10fs0cqjcqcfgzxbsghfizp6s2g34j9l6fsklayb1vj30kkwqmab")))

(define-public crate-friendly_safety_buddy-0.1 (crate (name "friendly_safety_buddy") (vers "0.1.0") (hash "15v0446ahvwr30wykzhfdndzl8lbma0x42d9gq4l3pkpb9p34pqd")))

(define-public crate-friendlyid-0.2 (crate (name "friendlyid") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "friendly_id") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "190mniagwsngazhsjy4n89vzakk44q4952iyzlq5nkb5rn724lqk")))

(define-public crate-friendlyid-0.3 (crate (name "friendlyid") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "friendly_id") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0nhy59q2w82sgq7dmzwyls20d9lynm3wg4sbz14f3h066mjyqxkp")))

(define-public crate-friends-0.0.0 (crate (name "friends") (vers "0.0.0") (hash "15xwn06ncniv3r7f7y0lazdr3f11dyancb5b71nkr8fg678igrgq") (yanked #t)))

