(define-module (crates-io fr bf) #:use-module (crates-io))

(define-public crate-frbf-0.1 (crate (name "frbf") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0rqaqg8pg6hy2nk4gpfji5s3a90xsh4nxgz0hkq71jrkb3y4vryw")))

