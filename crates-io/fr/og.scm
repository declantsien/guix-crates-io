(define-module (crates-io fr og) #:use-module (crates-io))

(define-public crate-frog-0.0.1 (crate (name "frog") (vers "0.0.1") (hash "1z9n0c7dyc9nhpydh3ypgwhzm6w4xmnx8m94vqbmpb8k748hzhb3")))

(define-public crate-froggy-0.1 (crate (name "froggy") (vers "0.1.0") (hash "1bs1x42gz8lfccmlx786kfbf2f0czjvyiws8dyx4b544fzmq8byw")))

(define-public crate-froggy-0.1 (crate (name "froggy") (vers "0.1.1") (hash "17fm7qq6mq22f8lgqbnmwkhfqi8pxhnakan8ckfc6d63jcdjbxqq")))

(define-public crate-froggy-0.2 (crate (name "froggy") (vers "0.2.0") (deps (list (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.9") (default-features #t) (kind 2)))) (hash "1950p5apg7xnxj0k0d084n5ml1hbv8vd482l2g0qvmkpnmmkk7dy")))

(define-public crate-froggy-0.2 (crate (name "froggy") (vers "0.2.1") (hash "0zh3ysx36f5npjcz3cryap98cvarp1h3jw9bcasvqjij9syabivi")))

(define-public crate-froggy-0.3 (crate (name "froggy") (vers "0.3.0") (hash "154a3krxycw202h8cyym3lygivl93gxkhhw0cjjsajjra4rhzrzr")))

(define-public crate-froggy-0.4 (crate (name "froggy") (vers "0.4.0") (deps (list (crate-dep (name "spin") (req "^0.4") (kind 0)))) (hash "1yjr6hn5467l917ia1g6wi46l1if8dfg5914ln26zzzqybnbyif2")))

(define-public crate-froggy-0.4 (crate (name "froggy") (vers "0.4.1") (deps (list (crate-dep (name "spin") (req "^0.4") (kind 0)))) (hash "159jk8x4ki666hz8pw8cngmj5rd33r2r2ar8467zmdwczk324y9m")))

(define-public crate-froggy-0.4 (crate (name "froggy") (vers "0.4.4") (deps (list (crate-dep (name "spin") (req "^0.4") (kind 0)))) (hash "0nrsinkyi19ikb794j5rhikhnvl10vn12nxzzaa10mxcq224vnbh")))

(define-public crate-froggy-0.4 (crate (name "froggy") (vers "0.4.2") (deps (list (crate-dep (name "spin") (req "^0.4") (kind 0)))) (hash "1h1k7a4px6q5hfc8gdkfcjfkdv9l8nqxcmx48gcrc6r5f9k1d5qs")))

(define-public crate-froggy-rand-0.1 (crate (name "froggy-rand") (vers "0.1.0") (deps (list (crate-dep (name "deterministic-hash") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1yfsqrx88by8lp37bi38knbr9h2jrgdcgkwsdr2xf0y13i3kvavm")))

(define-public crate-froggy-rand-0.2 (crate (name "froggy-rand") (vers "0.2.0") (deps (list (crate-dep (name "deterministic-hash") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "022m6v2dnzpcl37fqa084mnjzf33lgfv6d73vx1rby8h6sps26ya")))

(define-public crate-froggy-rand-0.2 (crate (name "froggy-rand") (vers "0.2.1") (deps (list (crate-dep (name "deterministic-hash") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "10rjh1kzwv1xc8xbzfhvn6np5h5vik5f2qlxbg29fwfpdfp7k81m")))

