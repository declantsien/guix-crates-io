(define-module (crates-io fr oo) #:use-module (crates-io))

(define-public crate-froop-0.1 (crate (name "froop") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0pkm1yk5pqvmsapijxfsjj1rr1p505k1wnc93vgz3p9axkg49cn5")))

(define-public crate-froop-0.1 (crate (name "froop") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1bfdcyvy8pgs0ryjxp9g6myr7g2m5za8mrw0v56y37z1s6kfirpm")))

