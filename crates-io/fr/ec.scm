(define-module (crates-io fr ec) #:use-module (crates-io))

(define-public crate-frechet-0.1 (crate (name "frechet") (vers "0.1.0") (hash "16bxyws3yd0s7bqni4mgc2s8fdk844zfkrysxa7cns7skqvk0pay")))

(define-public crate-frecs-0.0.0 (crate (name "frecs") (vers "0.0.0") (hash "1vznsnhxrhmiw28hsd0fq8znbv44fpv24wi21l3xigq5qbfv2gh5")))

