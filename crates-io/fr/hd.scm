(define-module (crates-io fr hd) #:use-module (crates-io))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.0") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "13zcca94ga5npzpfifc81w5c54dkr1j5wp6iabjvj8mr9b4g0db9")))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.1") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "13accg1gj6qjv6f7n0md0k37a2whf4ghjm9h38n4xmc77z551ksh")))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.2") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "02f5pdbjd37r724lsxfgbn6n0hy3vrszdrnjy3k2dbcpkddsjbnp") (yanked #t)))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.3") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g17mvqhwgkjdsqpwnwvz9s8whd99i4sym1ckd4159risbfvvbjp")))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.4") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "149m1300cpjq8ib5v5wj8672rv197mrd5h750y47678gjvch0vx2")))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.5") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dy8p3pfgzjg5kgbriphhxx8qbdj5w2iml5zxva593kf4b6bfgr5")))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.51") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qcydapm5wpv36ibh9z3832sgkmxmrimwah1nmgm09q6f3kgwvwf")))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.52") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y1d4fkp6n3a22k83dmp0xc0l7n4fnq8pr5zgw3awciccbyy70ln")))

(define-public crate-frhd-0.1 (crate (name "frhd") (vers "0.1.53") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base_custom") (req "^0.2") (default-features #t) (kind 0)))) (hash "01mv4rsfmjpnjng6mc2mhiiwf2k181d5116camll7mbs60ba4cnc")))

