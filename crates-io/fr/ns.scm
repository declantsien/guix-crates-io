(define-module (crates-io fr ns) #:use-module (crates-io))

(define-public crate-frnsc-hive-0.8 (crate (name "frnsc-hive") (vers "0.8.0") (deps (list (crate-dep (name "forensic-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1bdl3zz1vsq7vg09vz7a544iy23cpldgswnayc1608mrjx2y828f") (features (quote (("default"))))))

(define-public crate-frnsc-hive-0.9 (crate (name "frnsc-hive") (vers "0.9.0") (deps (list (crate-dep (name "forensic-rs") (req "^0.9") (default-features #t) (kind 0)))) (hash "0dl3696l344k5y9xw3y7p419mvdcgjgixmns3ydq8l5ki8jhgkh6") (features (quote (("default"))))))

(define-public crate-frnsc-hive-0.13 (crate (name "frnsc-hive") (vers "0.13.0") (deps (list (crate-dep (name "forensic-rs") (req "^0.13") (default-features #t) (kind 0)))) (hash "1l9zkchn6lnwbpinpw1arrskwsyh6lf5f8lhwmfsvxz7m2mn6nvr") (features (quote (("default"))))))

(define-public crate-frnsc-liveregistry-rs-0.1 (crate (name "frnsc-liveregistry-rs") (vers "0.1.0") (deps (list (crate-dep (name "forensic-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "03d9l8d1pm3r65zrfrqvshvlfswdmckmrh7gkqzqw2n2d2ajx9bn")))

(define-public crate-frnsc-liveregistry-rs-0.1 (crate (name "frnsc-liveregistry-rs") (vers "0.1.2") (deps (list (crate-dep (name "forensic-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "16zb0r5ihx9g00mqm0hzfbcgyv6gwbyy395jxmyb9cnm5j00i9fd")))

(define-public crate-frnsc-liveregistry-rs-0.2 (crate (name "frnsc-liveregistry-rs") (vers "0.2.0") (deps (list (crate-dep (name "forensic-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0iqnrlqv4jv6l00fc6ycnszgj4zhz8ygwqrp9z2cwzhjxkd79qzx")))

(define-public crate-frnsc-liveregistry-rs-0.2 (crate (name "frnsc-liveregistry-rs") (vers "0.2.1") (deps (list (crate-dep (name "forensic-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "13w5z4m7wc8kbavrrb9qbiapn0gdz30zgyynkfyymvjmb3ma6kyb")))

(define-public crate-frnsc-liveregistry-rs-0.2 (crate (name "frnsc-liveregistry-rs") (vers "0.2.2") (deps (list (crate-dep (name "forensic-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "02v0nksmxsihdksm2a68hcn85sy83bxws7f4dxljc5yizgb5k918")))

(define-public crate-frnsc-liveregistry-rs-0.7 (crate (name "frnsc-liveregistry-rs") (vers "0.7.0") (deps (list (crate-dep (name "forensic-rs") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1gqrcj389q0a1w1rc06gsc6wdwf4r8jr18kiiyxii0909nz6wd00")))

(define-public crate-frnsc-liveregistry-rs-0.8 (crate (name "frnsc-liveregistry-rs") (vers "0.8.0") (deps (list (crate-dep (name "forensic-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0r0grdccxn0dfaq0cdaxq5136aal0pjai77m37sciwag2fi3vrnx")))

(define-public crate-frnsc-liveregistry-rs-0.9 (crate (name "frnsc-liveregistry-rs") (vers "0.9.0") (deps (list (crate-dep (name "forensic-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0cr9rikc9abk4v9sz4r90lcl2xp6hg11hhvh1rvy83w8rac7ds52")))

(define-public crate-frnsc-liveregistry-rs-0.9 (crate (name "frnsc-liveregistry-rs") (vers "0.9.1") (deps (list (crate-dep (name "forensic-rs") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1lilkq7wqaylwv4zslgwn2g95vb1290k4km135qm7ywzmv2a2gbm")))

(define-public crate-frnsc-liveregistry-rs-0.12 (crate (name "frnsc-liveregistry-rs") (vers "0.12.1") (deps (list (crate-dep (name "forensic-rs") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "02lk488r4mkhj435m6m6jjd1by7925hk3d32qgphvbvg0qjhpqwh")))

(define-public crate-frnsc-liveregistry-rs-0.13 (crate (name "frnsc-liveregistry-rs") (vers "0.13.0") (deps (list (crate-dep (name "forensic-rs") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.53") (features (quote ("Win32_Foundation" "Win32_System_Registry"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "01yfhd2dxz7aspmr14553y1hx56jck982nqvbaggpbjq2504sy62")))

(define-public crate-frnsc-prefetch-0.9 (crate (name "frnsc-prefetch") (vers "0.9.0") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "forensic-rs") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0bram9nyi3fr6xy85xr79lvxgdibdk4vrnmjhrs2cwqj14f17lif")))

(define-public crate-frnsc-prefetch-0.9 (crate (name "frnsc-prefetch") (vers "0.9.1") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "forensic-rs") (req "^0.9") (default-features #t) (kind 0)))) (hash "1nbydms9k20sn2l479wh1a0sjmqmf1lqm64ldcmy6hji5724ik68")))

(define-public crate-frnsc-prefetch-0.9 (crate (name "frnsc-prefetch") (vers "0.9.2") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "forensic-rs") (req ">=0.9") (default-features #t) (kind 0)))) (hash "0xsj85q5mj7pdch4sjqlvh07g4xfjfwyq3rlphp9lqd1494p5g1c")))

(define-public crate-frnsc-prefetch-0.13 (crate (name "frnsc-prefetch") (vers "0.13.0") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "forensic-rs") (req "^0.13") (default-features #t) (kind 0)))) (hash "1ppphnk82qwp8jalxd1a7pll0ifzakgvkr2y7vbl6yc1mllw1s7v")))

(define-public crate-frnsc-sqlite-0.1 (crate (name "frnsc-sqlite") (vers "0.1.0") (deps (list (crate-dep (name "forensic-rs") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "sqlite") (req "^0.30.4") (default-features #t) (kind 0)))) (hash "1q83qjawj4vq9jrwlnzycshc8wg3qcim5sxq5plghadcm88sz8sa")))

