(define-module (crates-io fr yi) #:use-module (crates-io))

(define-public crate-fryingpan-0.1 (crate (name "fryingpan") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "grid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0nprvbgmyclbijc5mj8j7rcz6rfr9j2x7zshnsgjr1xykbvg8z4a")))

(define-public crate-fryingpan-0.1 (crate (name "fryingpan") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "grid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "13vkb1m85d08x1d7xsg1g8x54f1fz5zbqp7qgkrd0fl4hhanx7k6")))

(define-public crate-fryingpan-0.1 (crate (name "fryingpan") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "grid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1mwy5dflm6f8d2ajm6fwz6lwfz8wnznnl5hrnfb4rywmfysh504p")))

