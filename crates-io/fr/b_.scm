(define-module (crates-io fr b_) #:use-module (crates-io))

(define-public crate-frb_plugin_tool-0.1 (crate (name "frb_plugin_tool") (vers "0.1.0") (deps (list (crate-dep (name "duct") (req "^0.13.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06lb24bbgxlxzxvgy7q9i7dgx1g888axhjl3xb2jj2wwnk0199dx")))

(define-public crate-frb_plugin_tool-0.1 (crate (name "frb_plugin_tool") (vers "0.1.1") (deps (list (crate-dep (name "duct") (req "^0.13.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0747c4kdn5r7wc61grl4d4dhziwdkbchkdrf43m9i6f58phaq2vq")))

(define-public crate-frb_plugin_tool-0.1 (crate (name "frb_plugin_tool") (vers "0.1.2") (deps (list (crate-dep (name "duct") (req "^0.13.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.26") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rx28lwv6797nn75xwp833ca66lwmdyigy2l2skclxr7x0si0d0y")))

