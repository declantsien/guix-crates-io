(define-module (crates-io n6 #{4-}#) #:use-module (crates-io))

(define-public crate-n64-hal-0.0.0 (crate (name "n64-hal") (vers "0.0.0") (deps (list (crate-dep (name "n64-pac") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dmvr3chpy0jfymmqn6h3bsmcgga05djfhcs58718q1zlv5rz97h")))

(define-public crate-n64-pac-0.1 (crate (name "n64-pac") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rdh7ysaj7ng3iaa7xpa40yk9fvndr0cmnw7ivahs7z1a9n46ps0")))

(define-public crate-n64-pac-0.1 (crate (name "n64-pac") (vers "0.1.1") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nlw3rkm0ai41k2cqcx558bxcjp4bq3wlrr32vma5qv549av4rm8")))

(define-public crate-n64-pac-0.2 (crate (name "n64-pac") (vers "0.2.0") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "16pxhj25zl0zkjwqhywknzs7ks61ysn1jnzagph1zzzc62qb5hax")))

(define-public crate-n64-pac-0.2 (crate (name "n64-pac") (vers "0.2.1") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wvrbl4bgjh7nx2jwqzgnx2d36c65pgc97klgl90q8kpc9wpcizj")))

(define-public crate-n64-pac-0.2 (crate (name "n64-pac") (vers "0.2.2") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qnpfagkxjjcqjrkrvx27xg8i508m2yp3n7gi06qfdcw5pwp6qaj")))

(define-public crate-n64-pac-0.3 (crate (name "n64-pac") (vers "0.3.0") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "09n5xq92ivspynmsabpbbdhvy8p23770faqj26qr93xlb1z7jn33") (yanked #t)))

(define-public crate-n64-pac-0.3 (crate (name "n64-pac") (vers "0.3.1") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "05kf96nx5z6b5sjd9bzm8labrdmvajniwzfvhvajsn70xab0dn2a")))

(define-public crate-n64-pac-0.3 (crate (name "n64-pac") (vers "0.3.2") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jg408x642l24vkqgj1x723gzg0mz5v7y7rzlnb3hj20lp311rzj")))

(define-public crate-n64-pac-0.3 (crate (name "n64-pac") (vers "0.3.3") (deps (list (crate-dep (name "num_enum") (req "^0.5") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-bitfield") (req "^0.2") (default-features #t) (kind 0)))) (hash "02aqanl51gpk6v8m1lsfn5bxjibhlyfkgj1anlf8jhni2ak73a47")))

(define-public crate-n64-project-template-0.0.0 (crate (name "n64-project-template") (vers "0.0.0") (hash "1s8d2kd69sk44hl5gcnaaadbsrlgw0g3amxsn5wva8dvvnb8mqsx")))

