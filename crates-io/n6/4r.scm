(define-module (crates-io n6 #{4r}#) #:use-module (crates-io))

(define-public crate-n64romconvert-0.1 (crate (name "n64romconvert") (vers "0.1.0-alpha") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1bdwa0z8bz5drf8j0nbwcvidmn3w53chyvi4drmn4l4ws51bbv84") (yanked #t)))

(define-public crate-n64romconvert-0.1 (crate (name "n64romconvert") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "08qn2n1ak3h3sgr5i7465pvjy4533qxmqr47mg4gm2nf0mglpgfg") (yanked #t)))

(define-public crate-n64romconvert-1 (crate (name "n64romconvert") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "17n2vj7b3hj08yy9q3qrfgqfr1syid45861riafdnj4j5amx9kwg")))

(define-public crate-n64romconvert-1 (crate (name "n64romconvert") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (kind 0)))) (hash "0g12s67arf03sl89jp6nx1crqnsjjqjk3dwi5f1wqvnfq6ilxi30") (yanked #t)))

(define-public crate-n64romconvert-1 (crate (name "n64romconvert") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (kind 0)))) (hash "1w0jpj9pcqaz5c1f7x48rbcjb9hgq2f07w7ksjg16lv29whl7nil")))

