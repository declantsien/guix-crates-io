(define-module (crates-io ai ff) #:use-module (crates-io))

(define-public crate-aiff-0.1 (crate (name "aiff") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "rust_decimal") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "12z28ydmln16in052jmajzdvqbjxqg8rv2sgvnjd0n4m2alh6vi7") (yanked #t)))

