(define-module (crates-io ai rr) #:use-module (crates-io))

(define-public crate-airrohr-mqtt-0.1 (crate (name "airrohr-mqtt") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "paho-mqtt") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bpyai2gp59rh2zvh8gbxy4vccm70mxjfjml2aa7l1rn1p8dnp03")))

