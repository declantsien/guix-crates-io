(define-module (crates-io ai rl) #:use-module (crates-io))

(define-public crate-airlang-0.0.1 (crate (name "airlang") (vers "0.0.1") (hash "1q1mwayigxaz1anlbcfnpvmmvlspr943662savgrxg0hnkyld6yv")))

(define-public crate-airlang-0.0.2 (crate (name "airlang") (vers "0.0.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0bdmrq51d3jzw0wnqbm2qmydj8c6cv1cmk03ibx0p49p71bsq21a") (features (quote (("syntax") ("semantics") ("default" "syntax" "semantics"))))))

(define-public crate-airlang-0.0.3 (crate (name "airlang") (vers "0.0.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1fwdsvxq6jg5dd2fgwark50amq565pwcj592xnxz0r4n7lhgd8r0")))

(define-public crate-airlang-0.0.4 (crate (name "airlang") (vers "0.0.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1v6afd32n5d4ixmky743bjh24445sfc2m8mvlw8igwygxg48a8r5")))

(define-public crate-airlang-0.0.5 (crate (name "airlang") (vers "0.0.5") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1b5ypkrhxhmrsgsc81a10wipafddm68z8mwm9prp2pjn1yqpka1d")))

(define-public crate-airlang-0.0.6 (crate (name "airlang") (vers "0.0.6") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1rs9gnarywyywaml3qka5abfafd3ssb7iq6g1lqdfmzp7vvs10vx")))

(define-public crate-airlang-0.0.7 (crate (name "airlang") (vers "0.0.7") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "197rbap6ndydgy1xaffnpnpwm3d371j2i3rnskz9zpznfml79fpr")))

(define-public crate-airlang-0.0.8 (crate (name "airlang") (vers "0.0.8") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "138jsmg7afd6wrh34m1nl7vxyxzd06wcs5yhykfiaspzfa6m70hw")))

(define-public crate-airlang-0.0.9 (crate (name "airlang") (vers "0.0.9") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "014grgzcbfn9vi57z1w3f4f7bs7a3dwxy5fc9qfym8wk9jmpvwjp")))

(define-public crate-airlang-0.0.10 (crate (name "airlang") (vers "0.0.10") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1m4wqyjxirfsd1b1i2frqhwr73g0k51gpv5dpkqx6lz92vv7ysnn")))

(define-public crate-airlang-0.0.11 (crate (name "airlang") (vers "0.0.11") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1") (features (quote ("std" "integer" "float"))) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0bzfxgja75xdqsgq9wi0hv16chaycp27rxnqd39nkzin88q9dhwj")))

(define-public crate-airlang-0.0.12 (crate (name "airlang") (vers "0.0.12") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "10rscby192xb1xxdh1wzlk9lcjg6lizcc8ygvypckdy7ly9k19qh") (rust-version "1.60")))

(define-public crate-airlang-0.0.13 (crate (name "airlang") (vers "0.0.13") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0q3izng6drlpfcgfbg8mfg3khlrma91h3wnnq5mkhain17ld975y") (rust-version "1.60")))

(define-public crate-airlang-0.1 (crate (name "airlang") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0jicjsbmfl70ina6lbvm02l75yr92cq6l9ip5w234mmx6gs7x7bi") (rust-version "1.60")))

(define-public crate-airlang-0.2 (crate (name "airlang") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1") (default-features #t) (kind 0)))) (hash "1w6i4rlwd8qw6rs3pjkbawc7wb08yzkczwh66gcfgjhzy2x6dqsb") (rust-version "1.60")))

(define-public crate-airlang_bin-0.0.3 (crate (name "airlang_bin") (vers "0.0.3") (deps (list (crate-dep (name "airlang") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "05srrnkhadlx57pjyxxvy7i2wa2i503jfg46kdihlfrdj35inj7r")))

(define-public crate-airlang_bin-0.0.4 (crate (name "airlang_bin") (vers "0.0.4") (deps (list (crate-dep (name "airlang") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0mv977fxwmbgsklryjcqxldbqpk9hx04bcwq30ljfg476li5bg4h")))

(define-public crate-airlang_bin-0.0.5 (crate (name "airlang_bin") (vers "0.0.5") (deps (list (crate-dep (name "airlang") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1zg1b9brqqbsfpb5m4z7r3m57zigb9adi2yc9gl930qq8nqpm6l2")))

(define-public crate-airlang_bin-0.0.6 (crate (name "airlang_bin") (vers "0.0.6") (deps (list (crate-dep (name "airlang") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1hph7kgyywbdz1a8h8a7phcdw2a55m1fgknch67qlszmchwlv8k2")))

(define-public crate-airlang_bin-0.0.7 (crate (name "airlang_bin") (vers "0.0.7") (deps (list (crate-dep (name "airlang") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1lwrl6qljzpszl9mkkw7ma5b5mr4ayvdgwg6j9bdg90gzhkah2hm")))

(define-public crate-airlang_bin-0.0.8 (crate (name "airlang_bin") (vers "0.0.8") (deps (list (crate-dep (name "airlang") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "1fwd4448dhznfzmwv529ny1w0nqr7cssa9cwjbiw3wwpbrpa4s0q")))

(define-public crate-airlang_bin-0.0.9 (crate (name "airlang_bin") (vers "0.0.9") (deps (list (crate-dep (name "airlang") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "19ymf2crkrk91m2qsgxrms00xv5xlywyliybnpangwp3xpiwskjr")))

(define-public crate-airlang_bin-0.0.10 (crate (name "airlang_bin") (vers "0.0.10") (deps (list (crate-dep (name "airlang") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "1ck25lapb186f12y5fxy1df3k3szbdq7vh2fhxpikf0spsnvafkr")))

(define-public crate-airlang_bin-0.0.11 (crate (name "airlang_bin") (vers "0.0.11") (deps (list (crate-dep (name "airlang") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "179jx9rfabmxa6a230dbas22v209x6hcca8463brl4rd72qv4089")))

(define-public crate-airlang_bin-0.0.12 (crate (name "airlang_bin") (vers "0.0.12") (deps (list (crate-dep (name "airlang") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "0hnvabrpl2ym6jiyzaby0wm8ghcc07sax7zxzjzh6q5ymssvr1gy") (rust-version "1.60")))

(define-public crate-airlang_bin-0.0.13 (crate (name "airlang_bin") (vers "0.0.13") (deps (list (crate-dep (name "airlang") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.0.13") (default-features #t) (kind 0)))) (hash "1z2mn4m78cd4gs1jf4cw89bmyb5sda9yq1wmfpjr0lwib2jv2m5y") (rust-version "1.60")))

(define-public crate-airlang_bin-0.1 (crate (name "airlang_bin") (vers "0.1.0") (deps (list (crate-dep (name "airlang") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)))) (hash "0i1wib7dv5172a8hm4rsvd5krall19cdlw7879zi89q4pdc8v6k0") (rust-version "1.60")))

(define-public crate-airlang_bin-0.2 (crate (name "airlang_bin") (vers "0.2.0") (deps (list (crate-dep (name "airlang") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)))) (hash "0h3hnvmj79w43gwlg89ky1pd9nap04kskggkrb86xark5x2df8yw") (rust-version "1.60")))

(define-public crate-airlang_ext-0.0.10 (crate (name "airlang_ext") (vers "0.0.10") (deps (list (crate-dep (name "airlang") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0zakm197z68w5dlx9bish4k29km3alriyamjk3bp8rl9m4ii09iq")))

(define-public crate-airlang_ext-0.0.11 (crate (name "airlang_ext") (vers "0.0.11") (deps (list (crate-dep (name "airlang") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "0b0gxm6h2cjc86md0h0dcfdczxfg483krkxp3i0icxbzz8j8n1dc")))

(define-public crate-airlang_ext-0.0.12 (crate (name "airlang_ext") (vers "0.0.12") (deps (list (crate-dep (name "airlang") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "0c2akgpc4xqf9ay2m4hz7gqy8205k0dzhiknb1pwzyr0kbrvcy5l") (rust-version "1.60")))

(define-public crate-airlang_ext-0.0.13 (crate (name "airlang_ext") (vers "0.0.13") (deps (list (crate-dep (name "airlang") (req "^0.0.13") (default-features #t) (kind 0)))) (hash "1949zfakazcfwq50n4ah1nxqz9q8dzsk9dwkz40yr5jk3b8rywhx") (rust-version "1.60")))

(define-public crate-airlang_ext-0.1 (crate (name "airlang_ext") (vers "0.1.0") (deps (list (crate-dep (name "airlang") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1b2j5dg02xf68xv4q9zr82z1fnzl5ifhq4yij0hpgd8qb5gsjyiq") (rust-version "1.60")))

(define-public crate-airlang_ext-0.2 (crate (name "airlang_ext") (vers "0.2.0") (deps (list (crate-dep (name "airlang") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "10a35ghw7khv49qy4bc2pahgzrk33mhrpiqbg2bbhpmbf2hfarxh") (rust-version "1.60")))

(define-public crate-airlift-0.1 (crate (name "airlift") (vers "0.1.0") (hash "1znamklzhi8sihi2pbp41i67cqbh5jk3s0lqhh0ilwfvadc9mglk")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.0") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1smpmcpf4rdlwa6hv5v0wcnsgxq43lgmizxncp829z8fa8dfbj4w")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.1") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01ym6vnix2x5snfaps5q79jdc5444gmmj2jkhcdxgmbdw7x36lcm")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.2") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x13nzlmh76ll4innnjkk4n8wpm2l5h0jnxhg90443gcns7h634m")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.3") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lqha3y0nq0c2xlrvy1c9ygjy18r2g3xa979m3k6817224r4b26b")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.4") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06xd5x4w7c9bzy65zqv221dn7cclqb5sl3hhhc1j5ilbygr931pg")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.5") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06kg8ypm8pzwa5mnvl46gk23cvggp18qxyl02qi364pj1x7k0n53")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.6") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12r6hsb6i94rrkxsijg5vf6f4bjpr4145ragvldzpjhg7xa4n3b6")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.7") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vi2108pik249k4mzggn7b4ig2ab5p0zm86j8bdl3y782kixln8s")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.8") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1npv5yfaqg4shmw7m7wy0m4srkh16g7sfpdx70cr23s5gld894z4")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.9") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1anzvw1lkvlj7681kykn3klxs6q2pjg9jhx4f26w9niihjg1r6c3")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.10") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wnymji2q11mq4kbvm0flfwyhpiq7m99a60qzhpnp9a6j6280q7c")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.11") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0amavrqcx08sk4gbz7f55m14j54h5jdd4k68p9ik216fxzsn67c6")))

(define-public crate-airline-0.1 (crate (name "airline") (vers "0.1.12") (deps (list (crate-dep (name "airline-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jlrv8cs5v3ywgllzdk4qxkqxl4ixq2ls646vfsshzsv0x3dnbp2")))

(define-public crate-airline-macros-0.1 (crate (name "airline-macros") (vers "0.1.0") (hash "0lxs7mi7mw0jssn0z7s3q4m64wklnqss6ad23wks0ncc8rd7xry6")))

(define-public crate-airline-macros-0.1 (crate (name "airline-macros") (vers "0.1.1") (hash "0rz2r6155z910xphvs9d5qfk7mhsybfg9d756z1g5cgw5120p6ax")))

(define-public crate-airline_demo-1 (crate (name "airline_demo") (vers "1.0.0") (hash "022j570sslpi9k11q2p2v491mrhs8z1mgyn1qj6j3j487mzqj6za")))

(define-public crate-airlock-0.0.1 (crate (name "airlock") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "16zfhsrwdgvz3imsmd1xzkq88phccmxai2jx9xzhvpjjmhwsx0a6") (yanked #t)))

(define-public crate-airlock-0.0.2 (crate (name "airlock") (vers "0.0.2") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "0n0ssxl1209767wvi9f08nzrjmwfwmfwxsmdl5aa2ib0hb75nls2") (features (quote (("std") ("default")))) (yanked #t) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.3 (crate (name "airlock") (vers "0.0.3") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "1ivxmiqx4zl5ajv6714c9x873577vkpvffb4q8ic94irli192xxz") (features (quote (("std") ("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.4 (crate (name "airlock") (vers "0.0.4") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "14mi3zalmijc7h1cv49dbab284c9x2c9vcwn9icwlwnnf0qmxksh") (features (quote (("std") ("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.5 (crate (name "airlock") (vers "0.0.5") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "0lf6gkn2i32zp301f0kbl8wbxb00hi7a860gsc094lq26bs0grnh") (features (quote (("std") ("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.6 (crate (name "airlock") (vers "0.0.6") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 2)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "0ywm2aisd491a5carawx94c45l75mli183mk7qmp9gpjrwksp2c2") (features (quote (("std") ("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.7 (crate (name "airlock") (vers "0.0.7") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 2)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "1inbdihsb8p7n0gxqfpah5by84afxngx80nnf2pcr4cqjyv47vzb") (features (quote (("std") ("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std"))))))

(define-public crate-airlock-0.0.8 (crate (name "airlock") (vers "0.0.8") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 2)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "04wh15wg9x78s4g20721r56czszgzvrp5llwpwwsrlwplwjbz6jy") (features (quote (("std") ("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std"))))))

