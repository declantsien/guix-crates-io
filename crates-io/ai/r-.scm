(define-module (crates-io ai r-) #:use-module (crates-io))

(define-public crate-air-beautifier-0.1 (crate (name "air-beautifier") (vers "0.1.3") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0jvg3h3cbya0p7yjas5i3kzif1cfj09252svaxgak1sdkj7y23c4")))

(define-public crate-air-beautifier-0.1 (crate (name "air-beautifier") (vers "0.1.4") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1g6545d5bfr4p49zc9qlfyz93bc8hd1m82c32q4g102g057dmi98")))

(define-public crate-air-beautifier-0.2 (crate (name "air-beautifier") (vers "0.2.0") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "12gvv3krqhmlng06sx2za2hlydn7yljhh6kshi9blwiqzajxif55")))

(define-public crate-air-beautifier-0.2 (crate (name "air-beautifier") (vers "0.2.1") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "06r2jw0w27mqkb7arlz3q24yvzd6z5l01mk0ag19swdmg7c2305c")))

(define-public crate-air-beautifier-0.2 (crate (name "air-beautifier") (vers "0.2.2") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "16l4mbrckfd4319v64whpg9539g9w2mks3p7fs07bakh59bzrfp7")))

(define-public crate-air-beautifier-0.3 (crate (name "air-beautifier") (vers "0.3.0") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0w6zjhnxpfr526j3qi8cm0ibv664vzf2daxk0vc6y7xrhm9f5y6l")))

(define-public crate-air-beautifier-0.3 (crate (name "air-beautifier") (vers "0.3.1") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1vy8ac6vwq228zd3kb49bxl4fcqqrrx0p7d7b5qkia2v7gyywwmc")))

(define-public crate-air-beautifier-0.4 (crate (name "air-beautifier") (vers "0.4.0") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "09p28g4rijv1hgpksryp9c8bw24m7mcwnznzrf23p4mb4kpjckn4")))

(define-public crate-air-beautifier-0.4 (crate (name "air-beautifier") (vers "0.4.1") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1bxmx7acc80wd97315wz7zh1b9qy5cx9rpjksk9bv6xfp56q8wv6")))

(define-public crate-air-beautifier-0.4 (crate (name "air-beautifier") (vers "0.4.3") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0z6hvpc55jyfcdap9n75n1izai64gszr3c01w3vhp8lm35gwm3vp")))

(define-public crate-air-codegen-masm-0.1 (crate (name "air-codegen-masm") (vers "0.1.0") (deps (list (crate-dep (name "air-ir") (req "^0.3") (default-features #t) (kind 0) (package "air-ir")) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "miden-assembly") (req "^0.6") (kind 2) (package "miden-assembly")) (crate-dep (name "miden-core") (req "^0.6") (kind 0) (package "miden-core")) (crate-dep (name "miden-diagnostics") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "miden-processor") (req "^0.6") (features (quote ("internals"))) (kind 2) (package "miden-processor")) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winter-air") (req "^0.6") (kind 2) (package "winter-air")) (crate-dep (name "winter-math") (req "^0.6") (kind 0) (package "winter-math")))) (hash "1khhmd9gxddyqf01ca9b3nrn0nb9fdms91dnkyvgzzx0m0kwphar") (rust-version "1.67")))

(define-public crate-air-codegen-winter-0.1 (crate (name "air-codegen-winter") (vers "0.1.0") (deps (list (crate-dep (name "codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ir") (req "^0.1.0") (default-features #t) (kind 0) (package "air-ir")))) (hash "0781wgfgpk3vnn08713w3pb1lbfgrz81frcy3iv894ffa9jnf00w") (rust-version "1.65")))

(define-public crate-air-codegen-winter-0.2 (crate (name "air-codegen-winter") (vers "0.2.0") (deps (list (crate-dep (name "air-script-core") (req "^0.2.0") (default-features #t) (kind 0) (package "air-script-core")) (crate-dep (name "codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ir") (req "^0.2.0") (default-features #t) (kind 0) (package "air-ir")))) (hash "0rb4agmi19cf1bki0ybbms7rx6c5gpx47py28kfirvxi9ddgxfwp") (rust-version "1.65")))

(define-public crate-air-codegen-winter-0.3 (crate (name "air-codegen-winter") (vers "0.3.0") (deps (list (crate-dep (name "air-ir") (req "^0.3") (default-features #t) (kind 0) (package "air-ir")) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "codegen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i565b3vwdmpb8rjfj7c5n97x635ik6h1v6pzg73myilhdk3px99") (rust-version "1.67")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.3") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "095p44nr539nym4jc31akr309kjrhgnmma593hzwq8g0a0p4z4hc")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.4") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1yylx97g0bljbfp6wm4rfpq7kdq1f82n45b20hk9g5mz0pka3psq")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.5") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1wplv9xkc11rcblmbm8ylqq5f53xkhxznpp63r5dlvzv7rj59b8n")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.6") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0brlx9iz4jpgfn49vfcaxxy53ck129dasnc12f7c9f6pgvc2i1i3")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.7") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "12rgh3nmym5077zx18yvcn3mj8i4b6c78g85sxhfy3nlvpb0lnhb")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.8") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1jmm6brkzq481s15342kj3qlqfbljpavlvx0sfd3s8ssbcx02c7d")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.9") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0ksrd5fmhw4qqsr6538ygm4kmy3iivhs4mpy1gpyl81bmq566y9h")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.10") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "05awnhmffi33jx2il30nz7hyq45bb177gq81kj4my0x4lwq9lnzn")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.11") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1hpny45g2injghzm8p0hisl5qy5f0hrd690saczya30a23jakbnr")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.12") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0a9vvjgsyr94zvzsvcs4fcb3wf8az8rrbgmaz7v342mblhadajyd")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.13") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "165zqzj0xvamjc5wc4n938x21nsw8fyk7810i9446xgpkgj2sbff")))

(define-public crate-air-execution-info-collector-0.7 (crate (name "air-execution-info-collector") (vers "0.7.14") (deps (list (crate-dep (name "aquavm-air-parser") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0rbbk8rj9jihc6qz43z34b2zlpsz9y93jmx68fjpdq1ig997sz93")))

(define-public crate-air-interpreter-cid-0.1 (crate (name "air-interpreter-cid") (vers "0.1.0") (deps (list (crate-dep (name "cid") (req "^0.9.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "multihash") (req "^0.17.0") (features (quote ("multihash-impl" "std" "sha2"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "1bb6c2krcqgs22ri00bshk14j6ngpp0ppiyqcfnpbhgvvs9rqj5v")))

(define-public crate-air-interpreter-cid-0.2 (crate (name "air-interpreter-cid") (vers "0.2.0") (deps (list (crate-dep (name "cid") (req "^0.10.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "multihash") (req "^0.18.0") (features (quote ("multihash-impl" "std" "sha2"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "123w5dq4zvz3rqiakfd5k9gg7a9ybyrmkh6idj85xkjf687s6wl0")))

(define-public crate-air-interpreter-cid-0.3 (crate (name "air-interpreter-cid") (vers "0.3.0") (deps (list (crate-dep (name "cid") (req "^0.10.1") (features (quote ("std"))) (kind 0)) (crate-dep (name "multihash") (req "^0.18.1") (features (quote ("multihash-impl" "std" "sha2"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "1k03nj8hisd6hb5hgj8g5xxywla8d7q3gj2g5cdrd8pn7hs1icg7")))

(define-public crate-air-interpreter-cid-0.4 (crate (name "air-interpreter-cid") (vers "0.4.0") (deps (list (crate-dep (name "cid") (req "^0.10.1") (features (quote ("std"))) (kind 0)) (crate-dep (name "multihash") (req "^0.18.1") (features (quote ("multihash-impl" "std" "sha2"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)))) (hash "0hznfpyfx3639jhmgpiyn0zvjjas5vbzmxyf9by4hx5892vjnbq2")))

(define-public crate-air-interpreter-cid-0.6 (crate (name "air-interpreter-cid") (vers "0.6.0") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.10.1") (features (quote ("std"))) (kind 0)) (crate-dep (name "multihash") (req "^0.18.1") (features (quote ("multihash-impl" "std" "blake3"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)))) (hash "0hq251pp9q3zpfcnlbpqr3n4c2zjgxh0900gqlh6gn32mjx4h8s5")))

(define-public crate-air-interpreter-cid-0.7 (crate (name "air-interpreter-cid") (vers "0.7.0") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (features (quote ("traits-preview"))) (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.11.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "digest") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "multihash") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "multihash-codetable") (req "^0.1.1") (features (quote ("blake3" "sha2" "digest"))) (default-features #t) (kind 0)) (crate-dep (name "multihash-codetable") (req "^0.1.1") (features (quote ("ripemd"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "03przcmr0gkwmdigbmzbr44v1szzs8qbdc3ifzpfr7ahcq9lyym1")))

(define-public crate-air-interpreter-cid-0.8 (crate (name "air-interpreter-cid") (vers "0.8.0") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (features (quote ("traits-preview"))) (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.11.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "digest") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "multihash") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "multihash-codetable") (req "^0.1.1") (features (quote ("blake3" "sha2" "digest"))) (default-features #t) (kind 0)) (crate-dep (name "multihash-codetable") (req "^0.1.1") (features (quote ("ripemd"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0fzcdmm9s8if95s2ax6qxwg88yb7il7n023jwv2shvsgrk6wr76z")))

(define-public crate-air-interpreter-cid-0.9 (crate (name "air-interpreter-cid") (vers "0.9.0") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (features (quote ("traits-preview"))) (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.11.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "digest") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "multihash") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "multihash-codetable") (req "^0.1.1") (features (quote ("blake3" "sha2" "digest"))) (default-features #t) (kind 0)) (crate-dep (name "multihash-codetable") (req "^0.1.1") (features (quote ("ripemd"))) (default-features #t) (kind 2)) (crate-dep (name "rkyv") (req "^0.7.43") (features (quote ("validation" "strict"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1fa4ynzrs9bzln63c3iqxywc9iiayjz8d4chzykgb0zss7rhwyg3")))

(define-public crate-air-interpreter-data-0.1 (crate (name "air-interpreter-data") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0.118") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "=1.0.61") (default-features #t) (kind 0)))) (hash "1sc7z3k1cwydvvly8avwccf2hhh1f5093y9ksc29jcml8828rcpn")))

(define-public crate-air-interpreter-data-0.1 (crate (name "air-interpreter-data") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "096ykjh5d4kkba4kqssbz3qcr1g0vh6rvx0q9kxlfzf1d75s932n")))

(define-public crate-air-interpreter-data-0.2 (crate (name "air-interpreter-data") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "1zi8ql8njdw327azb0r5icgalxqzbp5rhzwcwaq493wggyv2piig")))

(define-public crate-air-interpreter-data-0.6 (crate (name "air-interpreter-data") (vers "0.6.3") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "03aipas4dj2fma9anh4c2ysya31r90slfrfpcsk0qw1pqyzf4jnz")))

(define-public crate-air-interpreter-data-0.6 (crate (name "air-interpreter-data") (vers "0.6.4") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0dd69wdvsdh32vysggcn90if2wr4kk43ll5a9xxvlq8n4graa5lj")))

(define-public crate-air-interpreter-data-0.7 (crate (name "air-interpreter-data") (vers "0.7.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.158") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0nxpbzsclyw808v1826r67a01sv4ygxi5b7g444bkfinz6vzcv8k")))

(define-public crate-air-interpreter-data-0.8 (crate (name "air-interpreter-data") (vers "0.8.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1rxzhma2xcbhpnhgyygawfm8ww5ldf50q94h7mgdrqz2y351lalm")))

(define-public crate-air-interpreter-data-0.8 (crate (name "air-interpreter-data") (vers "0.8.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "19a6ccqpzlhk4kfc3wbwfaydl8pxw8k5il5jmm4hmpx7pc7jgfkw")))

(define-public crate-air-interpreter-data-0.9 (crate (name "air-interpreter-data") (vers "0.9.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1rllv8g6n8g3kci409z3jjw92rjyxg9b4acx9n0dfji5j0zfwhmr")))

(define-public crate-air-interpreter-data-0.11 (crate (name "air-interpreter-data") (vers "0.11.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0mr67i326v2zf98m78k6w5p5w4m33ck4wzww629yd9r2dhihhs2w")))

(define-public crate-air-interpreter-data-0.11 (crate (name "air-interpreter-data") (vers "0.11.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "03x5ij2sal64cdiwlj4f1wsapgncklh0b45dsllgq4rhffwvm7fd")))

(define-public crate-air-interpreter-data-0.11 (crate (name "air-interpreter-data") (vers "0.11.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0f4v2hqh66lfiyfzmmm8xgw6as1mnl3vp8820xz5vwg132h9f3dn")))

(define-public crate-air-interpreter-data-0.11 (crate (name "air-interpreter-data") (vers "0.11.3") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1v6yn3a43myffnq70zsfz87kczcd08nvjap5ypjwpnm2zgj4nm4m")))

(define-public crate-air-interpreter-data-0.12 (crate (name "air-interpreter-data") (vers "0.12.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1kb9fkp2xy49khknj8w173yzfydfazw661vg76iq69qajlanp229")))

(define-public crate-air-interpreter-data-0.12 (crate (name "air-interpreter-data") (vers "0.12.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0b9wznnhqhgdv96sj3cbz7vlk9mbg1jgpw93rl7mbc0lw1zrpbr8")))

(define-public crate-air-interpreter-data-0.14 (crate (name "air-interpreter-data") (vers "0.14.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.3") (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0gczw7z6abi9al1iln15allx11885xvzyyqzxx3vbkb73r7p47rg")))

(define-public crate-air-interpreter-data-0.15 (crate (name "air-interpreter-data") (vers "0.15.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.3") (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0d4iv1n4d6xfdv63y20iivn6w8szpqf0wrj39vbcz4fgdyngafpb")))

(define-public crate-air-interpreter-data-0.16 (crate (name "air-interpreter-data") (vers "0.16.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (features (quote ("raw_value"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1h2i8zf8kzmm0njs9zg98a34rml7lf5w5m2isnchh34k5nb2vdki")))

(define-public crate-air-interpreter-data-0.17 (crate (name "air-interpreter-data") (vers "0.17.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (features (quote ("msgpack"))) (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.6.0") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7.43") (features (quote ("validation" "strict"))) (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (features (quote ("raw_value"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0lq8szgfh3zsni22kc37ql2px54ddcs0033d25zf47hm6kx53sjy")))

(define-public crate-air-interpreter-data-0.17 (crate (name "air-interpreter-data") (vers "0.17.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (features (quote ("msgpack"))) (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-value") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-utils") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.7.0") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7.43") (features (quote ("validation" "strict"))) (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (features (quote ("raw_value"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0rmcjjnj72fr7k2kbgxgsmicajhqgdrqr95x38c4bsnnp30sffpz")))

(define-public crate-air-interpreter-interface-0.5 (crate (name "air-interpreter-interface") (vers "0.5.0") (deps (list (crate-dep (name "fluence") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0.118") (default-features #t) (kind 0)))) (hash "0yw0spv89zk92flppgv7pr7nkcyipwb07vpp31whgrk51ff0qp8d")))

(define-public crate-air-interpreter-interface-0.5 (crate (name "air-interpreter-interface") (vers "0.5.1") (deps (list (crate-dep (name "fluence") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0.118") (default-features #t) (kind 0)))) (hash "1kd5kh0dn0mslzxixj67sw2arimv59zz7zhgrmip5y8a98ibikhx")))

(define-public crate-air-interpreter-interface-0.6 (crate (name "air-interpreter-interface") (vers "0.6.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0.118") (default-features #t) (kind 0)))) (hash "08dr5q7ijj1waxi5gw18lxhd5xc06cad5ld85kyq5whnap6938h7")))

(define-public crate-air-interpreter-interface-0.6 (crate (name "air-interpreter-interface") (vers "0.6.1") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)))) (hash "0dm0gb9d3a446gvljwrxmqvaidas2nsx015qr1yqkyja5c3baia5")))

(define-public crate-air-interpreter-interface-0.7 (crate (name "air-interpreter-interface") (vers "0.7.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1jgsh60fgfsvcymd62ibivz7s3vb42hf0wmqylmz5zxjf8lg6v5k")))

(define-public crate-air-interpreter-interface-0.8 (crate (name "air-interpreter-interface") (vers "0.8.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1bkk3a4k5p97q1ayr0pd1mrkbp0j0js967spdzrzj2r0qabkhzlc")))

(define-public crate-air-interpreter-interface-0.9 (crate (name "air-interpreter-interface") (vers "0.9.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "15yk3rgxhsdjam1g4187w347mb0aankqcp4qja0h2q81rynmcd9p")))

(define-public crate-air-interpreter-interface-0.10 (crate (name "air-interpreter-interface") (vers "0.10.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "13vh1mvag0sq4zm3wvqdd8z6if52qkhp7awwm34f1z5pjwji0cb1")))

(define-public crate-air-interpreter-interface-0.11 (crate (name "air-interpreter-interface") (vers "0.11.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0k541inqbb33pv6bmgis8cg808733zmcj328p6cd0j8zzf58sybm")))

(define-public crate-air-interpreter-interface-0.11 (crate (name "air-interpreter-interface") (vers "0.11.1") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "09f2j511a64bf18zi4vvsnida78wsnw9kbmlhvg4ib58hyax8ama") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.11 (crate (name "air-interpreter-interface") (vers "0.11.2") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0krrcccr1ypf0favcw7j9ll7h2bvrxl5h1nj38plv4xg4bxisjzv") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.12 (crate (name "air-interpreter-interface") (vers "0.12.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-it-types") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "0inj36406h099h6m3d3m1k1hdxaqhn9yd8z37bqx2wabqsn8yqnb") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.12 (crate (name "air-interpreter-interface") (vers "0.12.1") (deps (list (crate-dep (name "fluence-it-types") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.151") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "03jdibc7yvp6c3xfwhg8nr3g98q7nw8ji1y9wh3im4jwm5psf20x") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.1 (crate (name "air-interpreter-interface") (vers "0.1.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "00nmk31yxka0lwqyxlpnagmd73alr9y7m1vniz0snwqg2766wq71") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.13 (crate (name "air-interpreter-interface") (vers "0.13.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1ckl57vkv002mzgmln8qw0p69095h0lks2ifbsa2qqplq5fyg5h5") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.14 (crate (name "air-interpreter-interface") (vers "0.14.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "0fxinl8slr79ivagzw614bfgys58h5ryrfah418ljialv26xm90x") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.15 (crate (name "air-interpreter-interface") (vers "0.15.0") (deps (list (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "0qzhgm33lc9abcghp3fz1ca0mxkzq1dd5fnqc8kc0pr9nj1hfg1j") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.15 (crate (name "air-interpreter-interface") (vers "0.15.1") (deps (list (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "1g32ahnff5hhgzzk1zlg0mzcj64b34wqhp1gysypr07bv9rc00y7") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.15 (crate (name "air-interpreter-interface") (vers "0.15.2") (deps (list (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.10.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "09n07m9gvd5il8nk0zswbbjbji5p7s23p6xzv8d3ci5x302cwyz1") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.17 (crate (name "air-interpreter-interface") (vers "0.17.0") (deps (list (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (features (quote ("msgpack"))) (default-features #t) (kind 0)) (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-call-parameters") (req "^0.10.3") (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.10.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1mcvnsfr3v8jr6laynvsxyz8i17ijjcd230f33qzvfxwh0dx1ijy") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types" "air-interpreter-sede/marine" "marine-call-parameters/marine-abi") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.17 (crate (name "air-interpreter-interface") (vers "0.17.1") (deps (list (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (features (quote ("msgpack"))) (default-features #t) (kind 0)) (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-call-parameters") (req "^0.10.3") (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.10.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1dg2xxlk56bj4sn2ldaxsdf82rzqz0bfcafjygpqqc15nn94lw68") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types" "air-interpreter-sede/marine" "marine-call-parameters/marine-abi") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.17 (crate (name "air-interpreter-interface") (vers "0.17.2") (deps (list (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (features (quote ("msgpack"))) (default-features #t) (kind 0)) (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-call-parameters") (req "^0.10.3") (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.10.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1vfnqqrxsc0dp9pkhiv8z9lrp51slzgikh1la9llimpm2y6sxk02") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types" "air-interpreter-sede/marine" "marine-call-parameters/marine-abi") ("default" "marine"))))))

(define-public crate-air-interpreter-interface-0.19 (crate (name "air-interpreter-interface") (vers "0.19.0") (deps (list (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (features (quote ("msgpack"))) (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-value") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-it-types") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "marine-call-parameters") (req "^0.14.0") (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.14.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1facnwcfa4amd7fwxv9fykp72pjqfxsgxpqzpi6532cwflk7kjjj") (features (quote (("marine" "marine-rs-sdk" "fluence-it-types" "air-interpreter-sede/marine" "marine-call-parameters/marine-abi") ("default" "marine"))))))

(define-public crate-air-interpreter-sede-0.1 (crate (name "air-interpreter-sede") (vers "0.1.0") (deps (list (crate-dep (name "marine-rs-sdk") (req "^0.10.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^1.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "unsigned-varint") (req "^0.8.0") (features (quote ("std"))) (kind 0)))) (hash "0qc2nb3ryadj8yzgd4c0ap0hhynm3jk2lsv4hdh6lpwgafiiqfs3") (features (quote (("serde_json") ("msgpack" "rmp-serde") ("json" "serde_json") ("default")))) (v 2) (features2 (quote (("rmp-serde" "dep:rmp-serde") ("marine" "dep:marine-rs-sdk"))))))

(define-public crate-air-interpreter-signatures-0.1 (crate (name "air-interpreter-signatures") (vers "0.1.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1m4prgz5lqla8pd8zq9dq2k5qsi8cck73adpwwb86yqdrnf3vxj5")))

(define-public crate-air-interpreter-signatures-0.1 (crate (name "air-interpreter-signatures") (vers "0.1.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "borsh-derive") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1chy297abygr4hhrzjqhgzzpbn9nbchg3qd33fid9kga2z7yb9bq")))

(define-public crate-air-interpreter-signatures-0.1 (crate (name "air-interpreter-signatures") (vers "0.1.4") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "borsh-derive") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.3") (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0icpsvalpf0z8pijq8rxnm969yy7garmdqf5qkwk9ig4q5vbhy6g")))

(define-public crate-air-interpreter-signatures-0.1 (crate (name "air-interpreter-signatures") (vers "0.1.5") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "borsh-derive") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.3") (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1c6awahmc31nypl3s4fmn8ngdj6m0jz5b3izxc740jy81rwga3ak") (features (quote (("rand") ("default" "rand"))))))

(define-public crate-air-interpreter-signatures-0.1 (crate (name "air-interpreter-signatures") (vers "0.1.6") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "borsh-derive") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0p06a9aavdipk9fkrgqnjhmd5xrr7cyaqwib1biqm95xzq4aq2g0") (features (quote (("rand") ("default" "rand"))))))

(define-public crate-air-interpreter-signatures-0.1 (crate (name "air-interpreter-signatures") (vers "0.1.7") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "borsh-derive") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (kind 0)) (crate-dep (name "rkyv") (req "^0.7.43") (features (quote ("validation" "strict"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "059as1virnpvldkhbjapflgp0ylwmna5bax8d5wwpc7wd641l4y2") (features (quote (("rand") ("default" "rand"))))))

(define-public crate-air-interpreter-value-0.1 (crate (name "air-interpreter-value") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^2.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0a854c5y5aamaq93fv8iwc2ysim523hi3q90a616sjfaym9qw5g3") (features (quote (("preserve_order" "indexmap"))))))

(define-public crate-air-interpreter-wasm-0.0.0 (crate (name "air-interpreter-wasm") (vers "0.0.0") (hash "1sa5rpvc3qcz1wcnz52ws2yxczgxsac3l3dpp4yys8swvjl3v01n")))

(define-public crate-air-interpreter-wasm-0.0.1 (crate (name "air-interpreter-wasm") (vers "0.0.1") (deps (list (crate-dep (name "tinyjson") (req "^2.2.0") (default-features #t) (kind 1)))) (hash "04va3kadfmkbc13dawpx1784fw3bnb04m7girk7np2sy1l9an8d4")))

(define-public crate-air-interpreter-wasm-0.0.2 (crate (name "air-interpreter-wasm") (vers "0.0.2") (deps (list (crate-dep (name "tinyjson") (req "^2.2.0") (default-features #t) (kind 1)))) (hash "0c778kjj086yxapfx91xrhzwmc4z240dgsqk5lkm2bqy1w8km5dg")))

(define-public crate-air-interpreter-wasm-0.0.3 (crate (name "air-interpreter-wasm") (vers "0.0.3") (deps (list (crate-dep (name "tinyjson") (req "^2.2.0") (default-features #t) (kind 1)))) (hash "0c17fdkx5q48iy6xdl5kb9zy89cyad8hnm88312mcmn7nzbmg042")))

(define-public crate-air-interpreter-wasm-0.0.4 (crate (name "air-interpreter-wasm") (vers "0.0.4") (deps (list (crate-dep (name "tinyjson") (req "^2.2.0") (default-features #t) (kind 1)))) (hash "04m6v0x5hg49zjrza8lkszdh7iy26z4plp9y7dwcwknwrzr7alvb")))

(define-public crate-air-interpreter-wasm-0.0.5 (crate (name "air-interpreter-wasm") (vers "0.0.5") (deps (list (crate-dep (name "tinyjson") (req "^2.2.0") (default-features #t) (kind 1)))) (hash "1j1304cwcj07qdlgdfxxhyn2yfahrcp1kvavvbbfpaiw4gswjviw")))

(define-public crate-air-interpreter-wasm-0.0.6 (crate (name "air-interpreter-wasm") (vers "0.0.6") (deps (list (crate-dep (name "tinyjson") (req "^2.2.0") (default-features #t) (kind 1)))) (hash "14kxaxdk6v4yw9ynm3r71vrcdfc0jjl7a0l3zqfzddxb9fry39py")))

(define-public crate-air-interpreter-wasm-0.0.7 (crate (name "air-interpreter-wasm") (vers "0.0.7") (hash "1y01cyr30gxj6rkfp7lkbiknw885nw4i4kxqxdxcnbnccyf0kfgn")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qhvfrnlpi9ipl89blch9zgra2p7z5d1qz653a92n7dkn8izygx3")))

(define-public crate-air-interpreter-wasm-0.0.18 (crate (name "air-interpreter-wasm") (vers "0.0.18") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0cd8br6s700yhhd187d7zyv72aq926vw1li1864r2bxrgsinmxv2")))

(define-public crate-air-interpreter-wasm-0.0.19 (crate (name "air-interpreter-wasm") (vers "0.0.19") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0kblzzg4kp7qaj6alwivbzpjc9idxb1xmxnicy6lqzpc1ys6652w")))

(define-public crate-air-interpreter-wasm-0.0.20 (crate (name "air-interpreter-wasm") (vers "0.0.20") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04g0vmx6rs875jmbvv278jc6ygvvfw0hvbh125ssd8hk39hqa5qw")))

(define-public crate-air-interpreter-wasm-0.0.21 (crate (name "air-interpreter-wasm") (vers "0.0.21") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f2j31fjkfd826c7akpn8452s8ygcm7v6l2awfz4ffcnvav9znfv")))

(define-public crate-air-interpreter-wasm-0.0.22 (crate (name "air-interpreter-wasm") (vers "0.0.22") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1x1za009ga8zcklwdrx1pc4afldz4y7b8h8m1km30wl70cnd848g")))

(define-public crate-air-interpreter-wasm-0.0.23 (crate (name "air-interpreter-wasm") (vers "0.0.23") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0cfrv0da3jj1cp1r9d42r2xwa1k44p93i49b6p0iy7vhjjw8hrgc")))

(define-public crate-air-interpreter-wasm-0.0.24 (crate (name "air-interpreter-wasm") (vers "0.0.24") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0s22mz0pv8ffvcg5insghkl9w75l6w5qajgpq3vlfl3zmllgyy64")))

(define-public crate-air-interpreter-wasm-0.0.25 (crate (name "air-interpreter-wasm") (vers "0.0.25") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "011f8fdlbwkv2qxm1pfy61g9av7jcj4ix7vzga2kirh40734h03c")))

(define-public crate-air-interpreter-wasm-0.0.26 (crate (name "air-interpreter-wasm") (vers "0.0.26") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09ls1i3wlm0wdh0rvqplnh1mbikffavk6cslnn7nhifnzwlkqipc")))

(define-public crate-air-interpreter-wasm-0.0.28 (crate (name "air-interpreter-wasm") (vers "0.0.28") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rfy1vc9kbk1zxis1wm7yim3p5bn8byjjcvq4d2bkwrg697kvi14")))

(define-public crate-air-interpreter-wasm-0.0.29 (crate (name "air-interpreter-wasm") (vers "0.0.29") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yl8jyyc5lmk7sxxg9hmccn3rprx5m3pllyb08ga64r2sqsi0fyi")))

(define-public crate-air-interpreter-wasm-0.0.30 (crate (name "air-interpreter-wasm") (vers "0.0.30") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1m8kw6g28fc8mk2jv3dm74zqk8z1d75v4vs6ws047w6kag8m5zax")))

(define-public crate-air-interpreter-wasm-0.0.31 (crate (name "air-interpreter-wasm") (vers "0.0.31") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qjfc61w8wl38fmy1zwnv1c0l6qn9lgf027m9hfiv2khv6g01lmi")))

(define-public crate-air-interpreter-wasm-0.0.32 (crate (name "air-interpreter-wasm") (vers "0.0.32") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07c3r6sbqaj3q2rp1grvh3lxywick59pdp3yzi97flpvnbyz4ndn")))

(define-public crate-air-interpreter-wasm-0.0.33 (crate (name "air-interpreter-wasm") (vers "0.0.33") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1237zyfny4l7k9i43128nidskkcagb685c75j93ljsr2zj713ik4")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-release-version.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0brg8flhl2ih4drpczqjxpgwhknmxqziy7pmyq2pwflgwwy1i1gd")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-release-version.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1avzml4zhx2wzbswqm7cp445qrw8rkl17gvppc4vw24gzqnj842s")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-release-version.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sk91v1s38fy8z7nhf07gl9s0y2hdyv5iq1fdglsjrsn0y2qm3j4")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-release-version.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bv02jn9lvavnz1grzfl4kcbihzjdl1mn0jcc7ng0az3qba5r4n4")))

(define-public crate-air-interpreter-wasm-0.0.35 (crate (name "air-interpreter-wasm") (vers "0.0.35") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0l6z83bbq2n6ng1sna2jhx6kqmzfd0zjbp08hl76khzl4ja4zi1r")))

(define-public crate-air-interpreter-wasm-0.0.36 (crate (name "air-interpreter-wasm") (vers "0.0.36") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1v9rryp604zzk6y8mr50lhssfw543fkhg4lhh1bcw2ngzbvlzksi")))

(define-public crate-air-interpreter-wasm-0.2 (crate (name "air-interpreter-wasm") (vers "0.2.1-stepper-version.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1988qaiars39g908nm0zlcyq8gi33qliv7fdb7376j4psm175lgm")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-refactor-errors.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ng0g4y2b955pw1r2r81ira08013l7qx20vvrj3jr4h80aqi3nv1")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-refactor-errors.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1x23m3i626nr3083h04y1xgjk6g6l3kyf30qnyc3hysy1yklvx75")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-refactor-errors.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19p0a5x9k1gipiw039p7d0n2g6fvm6fb9v19wlnyk9xawzn41sli")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-xor.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kx1sb3hr09gh84hgdh45z5d536ky6hlh56x31w9zribfd0afmaz")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-fix-cargo-lock.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kv1lc8qb4g0cgwbya6dp1x8a03lhy6hc6fmgrxdgz2knccv33w0")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-refactor-errors.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zcpsm80q4jz0jnqpscm17c58lpsbb4x1ja0kp55hx0061y51g0h")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-release.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wsdp5glln5mf887h7mfza0870h1hf864kwp5lhksyf44jnvri3f")))

(define-public crate-air-interpreter-wasm-0.3 (crate (name "air-interpreter-wasm") (vers "0.3.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mdvnk8qmaa799x2ds6yhswgp0zgvzqhbr5mkjymn21ggjvs9bxf")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-release.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1k2d4jwapiyzlz5dwxiddabpg2idg64885xihkqkbasz8zar59nb")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-release.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1abvqbkhxb4xhql677nv5ykp60jrc2is1h8m2zcpp10ws23jgw2z")))

(define-public crate-air-interpreter-wasm-0.3 (crate (name "air-interpreter-wasm") (vers "0.3.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gpyjjf788mpvf3yv6bm3yp6nr1s68p55mm8izb0lsfmmpzf32a6")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04sahyrf76jr4cm1zgqll3jmbl5r217z4141zqzw7bdr62pa90f6")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vlpxwbky9ff640sq6zqvfj9ivnyvgll0wm0by16qkcxhvrmd3h0")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vhy7wzbxhmjgijbhmawx80jp7d0cnkl3ksdsy42s5sm2fyf3k39")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m1420vdjsqxk1nq6p53l90ldllif672kni86hhjf5wl8qn40s04")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07k699kl30xq6mj8ahmkz5g58x9d8r315q1mghga3z2q8p5j8gfk")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "193lwlyx1wmvc0rqi99vjkavp17jv94wz0ck36gach0lc47mybn2")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08gp85zl5qhvib5sy2flkj7knr27gdrzyf75sf1pmb00ykskaz02")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17gwblqa2sqlil89pyjn11yd7rya00s63kryigb81raw1d60vihh")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-lexer.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ryc1b9yc28ici4xwf4h7lif7d40w1zkcwx8yc3v3rpcysgvqacq")))

(define-public crate-air-interpreter-wasm-0.3 (crate (name "air-interpreter-wasm") (vers "0.3.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "083bvkp1h2x3baia66hrmz8607c6x46ys61x9m7dcl5sk49n7q9g")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-match.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1749sdmxgsrnwi6lj0is0ky9bnn75qqsc3h8v16zh0jnlc982rf2")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-match.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1i9x1x497g90sfzp8qydxszahai5rcj4nnqlkrjk9wwvp9ilpfyl")))

(define-public crate-air-interpreter-wasm-0.3 (crate (name "air-interpreter-wasm") (vers "0.3.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w2bzhvdynx2zd4c32ch0h9l1rn8dz5p38kyssgl00d1ldxk8y24")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-fix-literal-match.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02qz8l44s4s0qp5c242bhwm9jz09bmsc87ayblzbhg9q30qraa63")))

(define-public crate-air-interpreter-wasm-0.4 (crate (name "air-interpreter-wasm") (vers "0.4.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jp1cq47ybpa47pxra7lig4k87phpw1rz4ard5fli2bb14316n57")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-mismatch.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05nqg3w8168fsajiw6b5mgqypj57kb6wnaczpga4g1bmg61p24s5")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-mismatch.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14czkywv4k9zxv0z76p66c18p6khqnicdydd5vvx9h44bd9fj994")))

(define-public crate-air-interpreter-wasm-0.4 (crate (name "air-interpreter-wasm") (vers "0.4.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1nlvrnyj4gzgzf549h5w9qd8zjf4v1bp3kmpk4scqzgv99nahb13")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-new-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yw7fl47sj8glvd0x8ka7zzazdbpqkk6d60s80qadsynzrdv0vkr")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-last-error.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sjw6adf1h6dv943i04p16cwwwl5vf6vqri1vj32cjikz2jhcqj8")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-last-error.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06h4dz5cz90bajyn25293kmjb81a1h2p0l8c46s45nb20wspqclc")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-last-error.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1cgsmmlwcfmc4aamv2l05if6vy6lny9kpnvndrh373g249aq87ly")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-last-error.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ky9jr7dbpkzcxxnmnkglmap7lfsjz7gc32w32lqzhx22k8lb2hx")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-last-error.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0v6ard2pwq8aanljr7n97xbv9l9xqvhrjl8hsf44pxx13h3ar9h4")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-last-error.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0l79gmqcln9lpvicblzn2ywy6xi397nvwpiwdmymk4i84xdnziki")))

(define-public crate-air-interpreter-wasm-0.4 (crate (name "air-interpreter-wasm") (vers "0.4.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09gl4p554hxk7y2ngw5b85sbc3mbx516grzvigdilwq07m7g5yl7")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-support-number-consts.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07q4mf8grnax7xcy432d0p2cyvppmff04b04cnys41xk6gmfh5br")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-support-number-consts.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wfv5q49a8214rz80v3brzs7j6xh0hib0z0r7anhr9wcjsvqv99w")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-switch-to-cjs.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0696g6jxqlys739kcinxpjshq11475myx7dgb8kfkwdkqi4d51lr")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-switch-to-cjs.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gwnmmz7dlkki2gdm46w2h2z93hzaxs6dma9h2d2ws6crs2hxxzh")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-switch-to-cjs.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1b6m1jbgam3a9sxywkklfd6nyx22aw91kbb0jb6fmk9syzplx27w")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-switch-to-cjs.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09rg6rvfglz0w6an28q1hbs27wcjm59nljxdjsh1wyhdjpvacqf8")))

(define-public crate-air-interpreter-wasm-0.4 (crate (name "air-interpreter-wasm") (vers "0.4.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yc35gxjjwg1rniq5187117qmqphr9lhxg2is6n592ppj1z7dv9y")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-support-number-consts.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bfggsyryfkgw6ccr0sf88dzg3rh7xxgjrgx4a99jlmkyf34r5lq")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-support-number-consts.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11dr19p8pqwysmkidrf2prpsfcilgr9cr3jrii4z9xkkqn68ss0i")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-support-number-consts.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w7m74kbb5s6zsw4d1lnb3hzjb5y4pdsqxyc8qbq93rk1w97qjmq")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-support-number-consts.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zq2fyvknhsfyr1sllngim0mhffj9vjynmcdhisfvjhy1vqd2gay")))

(define-public crate-air-interpreter-wasm-0.5 (crate (name "air-interpreter-wasm") (vers "0.5.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qani9qsy4hfr3zmn771dmp5w8s4vszqv5q64k4fcasgjmicy8c9")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-better-instr-formatting.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02jakbb54ax20ql763bnysy9zy997f8aid836z6hvc8n1zmxc36q")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-better-instr-formatting.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0arzz6zwi3p13gf8yw1fn2hkjlvxjbhasfip01a1ciw3ivd3qr78")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-better-instr-formatting.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "031ri7n9wjh7ac1ci4id65nv63gpxdq0v1x7dvmxf9m7wfykbdf1")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-better-instr-formatting.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0h50s689q91n2cdq8imigv7vncckxfpbi22r2mlrc93qljra9pz3")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-better-instr-formatting.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "025fadgl16q5ryph0rpj8f491cwwcwzhr479r3v66a7c0kqbs6a8")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-better-instr-formatting.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ajsk0x9r44x07j10h8xjh7ai77lbbxh76g6mia8viggbjh80w6f")))

(define-public crate-air-interpreter-wasm-0.5 (crate (name "air-interpreter-wasm") (vers "0.5.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hi9vmd3nwixy40wcxqbr2x3gqymgh14vlparq5vcz99wx9p5sqs")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-rename-stepper-to-interpreter.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0j5rj50wvpgk3vbgiqcdqwnjfibxhhjb9981rxzvva3115qqmjlc")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-rename-stepper-to-interpreter.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1dm2m7drf06h7ph172czxhv4ycjwy1aakhvpmndslhkn8gm0fwfy")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-rename-stepper-to-interpreter.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qrlwaacpnpwl4w2zzfdqryrvsizhhik3cdkakk4mjbr5v6arg8s")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-rename-stepper-to-interpreter.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11fqzd9hryxayxpm4z0v7skn173npch5yp6c34157rryhpbn1iyf")))

(define-public crate-air-interpreter-wasm-0.0.8 (crate (name "air-interpreter-wasm") (vers "0.0.8-xor-warn.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fncj7rlcm7f4al2vydhybas2k41r05b9rsh6gpjbb38r1x8fc3b")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-xor-warn.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0pq95afsqfa19714z9y75ln7rsmxx2n5wl3cmgz9ahn41plhr1rj")))

(define-public crate-air-interpreter-wasm-0.6 (crate (name "air-interpreter-wasm") (vers "0.6.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rjn79znk5qb2ng3857vhpvs28l31rjr189pjsqx77ylmx3k332x")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-better-flattening.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0aj1ndhyfz35bw4is5whxa95mhldr2m1nw0lxhiclady5q7nd1zs")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-better-flattening.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "120zfd9bgmqp7dnphng3zmggjsl8xrwfw108206qrx90cya3a9xx")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-better-flattening.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1g217622ypnpz0wvdf3w72jkrv2y3axy2z3vrjnhzaczba71i3dp")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-better-flattening.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0v3nsqsg01nwxm1jk1kx6b5xilhdbxykbmxiji5x217lw96aa32l")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-better-flattening.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rgv02k281rf9f89gn34ycjaai8a3237bb3d8i1bh4g3bzwa893b")))

(define-public crate-air-interpreter-wasm-0.6 (crate (name "air-interpreter-wasm") (vers "0.6.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w1k3zh1zgvfm558m7jfs094215q2d2pn2zdz3apkdx2rqqksb05")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-better-flattening.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zyjbx52mdwjwv1snrl1ir65pdbvgm1b0n5vffk9c0phga1gf7a8")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-better-flattening.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vgqahazgil5v1zyjikharaa9qyn2djypzv473w0l5p53gzli0m7")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vby6a191hh16x51y5sjkciqg4l2dq0rw3lwh206jwii7db6wfl7")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-join-path-test.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14vczrinkgw46dc5ybzkw2wb7vq7jzyw2g3sk8aw3brzn9nzsfj4")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-join-path-test.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10kkqz4ssbraljjw6f70hpmw2764j1gi372vvq7q2qlxaf3yzkvn")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-join-path-test.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wi7dy07qlv7cyjph7p1gislbzpsrc7yb7anp019lvyy2yid1c6x")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "03425nndirx89ha4d6n1ngm28vvgzxw3qdznkmia1v97qc8hj0sj")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-last-error-with-match-mismatch.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "13861qdmsk74mn1iv03km7yh160zxgqb2pg9idb35pbxfw1q4kmj")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-last-error-with-match-mismatch.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0399k3qvdwk011cp4cbbp4l1b86slfbijp32iw4j66da82kcwpym")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rk1hypn8wcpdba6gpfpbpdxc2xch3fnjs95h5jrkpfv841vxa2z")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-logger.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0x9n47d71gbagvdcmqnwaa22cli7y17if2frk0aly8by2qhsc595")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-logger.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1aacyv0z49p049n4vw96579vpmq5c35r5jhv694w3n1f1gcdz26w")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-logger.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bbk4x2ikw0n45l3577195kl1xc6mwldgk81chz44s4zdad8hvb4")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ga4fcsg7np1k3nqmbvb0066zswjjfpc2hip3778q0xvw57jzc57")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0027p29vk4f1in78pvfwpk35clwnldp179835i0wwqzlvydl3zcf")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-new-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zar8ic8bh0az6a67d1yy91nqxjqgs6a7xwpf8irvrzdhhp8my73")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-new-data.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0slz7375qhaajbz59337kqrbb28grf60dma087wfipm36hg20siw")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-new-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gah05rgc05061wk3hs611da2bh5nn4ayyv6jqh8r9zr8d4a993j")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-new-tests.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17b1hk4qilp8p3zc8x2x1yn51mk6s21c6jgm1wch49h7shbvpz9f")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-new-tests.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16zkyiv1i2j8q9zgm5yy2pbd2wz6xknr9d55cdg70sl7k7ir4vb3")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-new-tests.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17zck6zv0an2liykp66kfl47wqnv9sm5b4naxzf6lwvaj62mkvj7")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gidqnar4z02531wm16438b7wfmkld5p4g0bbgrqq3yaag7ixfqc")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19i341rmb6d2qkh1r4zs49dxbsyi9ifms8pc2jrqj1wbpcnnqcva")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jwz8jqh9v5j3cc6vagkxb3didf9c7izwnq1503micgc1gpapzyg")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bz8r7sm9zlw8ard367dk3i9m21df87ny9gj5i5j2pgvbgqsxjyc")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w9dlwvsz6al335pk1498z0f4c3qjd1xpm8727fc010xjki1b8r9")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1i0xxc91fw42wxa6a88lzqrqrs70xm3sri54wrqisrlibqnlb9jm")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xpb5g0ddjjpr4ffz0v9kvw75ginq97qki5v6pjj0b8lqccp6p33")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jpqkniz0hwnd7hy87lp682gw5yd2xwip9gkpyy3hj36qbsd5lhh")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "022s0qgr6q0qarqzc0w72q6ljjp13jh2q96n5h7m8mpz1cg5b563")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "199xgv8w91qkx8vpwcwy7h44akgp4bx8rdh8ia52jrs5z70hgj2v")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vkcx0cb7fm52fwff19ch4vqxpry62iz27damkn9vx17vlh6whbn")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qj8j7cphadr56zhk81isswnzy050v85p5vb1j8lj0nk3skr3pza")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0a3y97h0algbjm880l6zcj4dkcbp7iwn6agkfin9m8bl06pzhlhx")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-check-for-used-variables.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "000krs6m9145m8fs3rmkb51aj8h3yb1jr6ffvd40wxid19gyqd7d")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02nxn0gp814xifgqsaqdz0v6virxpnazvk51vz5plhsb3vkwr8cc")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-joinable-match.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vwnw48abx4xr5qiwsrd71xgy079xfph5hsh209yl2sxzwrpryjw")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-joinable-match.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lbxi0qxxcf2rkz7cq1knxbf6b884dhb7rj44jvymsv53xipcdqi")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-joinable-match.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04nr4y5gx5b46r0m2vlfkcmrmxad5pma8wmb6gqigq47c2lcxjal")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05hvlrw46drmi34f1nggrziics8xahrdqwfh7qxr3si3lc8572q7")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-joinable.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gariiviv69n4n2yk9wzc7d18kr7ia4i5lcpwiz5zss90ncsycz0")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16laf40hmixxlziz93fc626xrra12x4xx8nj51nr5zkwaddh4a7b")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09gb2hw14w7z43mah063drcs6rdhv9qkylx48qr5cpknaiprqjvp")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16byhclz1iwlqwfdp5wcr8i1m1dcakjn8sphmad2i75rm2i5wm1c")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zplkkrdliw86xl4zpv30qll7bs2a27r82ffrjga73ixj5xfchi9")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18z5myccx592bp8f4hxvkxfgz0j367igvapa3y9as51irxyr7fqh")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0051j6k0ldy6wb2caldk8lb8pzcspw5h96phc4ppbgdxr7jn5clg")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0b3lwcvsvdidaxima37nlj07cvhb1digv5zm7w8yzkyi2227rynf")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vc9zigv9m7gpx3iwwfs9gqibw8dmglyzh6pmdw5vg6a698zfghw")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-improve-match.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "120aljqlmmsii97g0yd57842977c758j9yv592lkd2pfw8z7d2lq")))

(define-public crate-air-interpreter-wasm-0.7 (crate (name "air-interpreter-wasm") (vers "0.7.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09afk7vbwni3jyc2x81jwiqmbd7ndk281hikhyr01m7ir1vdlaxs")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fq7c1if4lg0g0spl0bv842z5rnvlylr8gmx6rqj9wdz268xn7sd")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gsz7ax2wdb8zl7sxbjnlz0bg6s7flgay32cchia3w3ynq0mffa9")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-acc-to-streams.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0a8npnpxah6s4j96pnacbd6040dffpq1yv816w3h9pw2afbanv4m")))

(define-public crate-air-interpreter-wasm-0.8 (crate (name "air-interpreter-wasm") (vers "0.8.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fxxygsc44ncvk5y4c0izn8wbadnilwv8lf6maiy3wiv6cawdgfm")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-empty-fold-json-path-error.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0crxi4kh6fz603kvq2pfls54rralp4hdq4k5wypa1n72hjx7ij29")))

(define-public crate-air-interpreter-wasm-0.8 (crate (name "air-interpreter-wasm") (vers "0.8.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vfslik5pww5s21q3j1cng8b3r434mv6n478p8f033z9dmwg5mvf")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-xor-match.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "03r0g0ds3pi2gjzn4j469fxvb1v42jsfrm6jnrg180n7palx4s1m")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-xor-match.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xvvj2x1jb7jgnc0csr3bhlxzmgy3lq1pk7741y45dpkbq7jvc9d")))

(define-public crate-air-interpreter-wasm-0.8 (crate (name "air-interpreter-wasm") (vers "0.8.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lfmpq1vrgxiyykiy6ivlfw47zns4zc9bn5ibp94xzhabvwwiki6")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mkqpbswfr3pzf8282778xhxghnna258hc10skw9n3yrramxvwgi")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qlghhw75h078hy1vawyxps8sqi6gv1kn3aycscsf3lyg4pvgvip")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jg1p716x4isd1ngvlvq2qfx04xbyn1jb1idqlnjq537r8nxbl16")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0y2d8s48xa2jl2sn3s3qr81r0ik6vg4861ly2fpzdqk5rwrra61m")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "103gdpzpk5vg6ab6nbs9lygmxn0q1378pbak8vfch4icp7a9lwj5")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-npm-package.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0as7llph4211mii9723lmzv5dsvgw5ll94f4y372ysl9099mb7ki")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-npm-package.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qzys1bsfjpw6wqwasgr9xs5i7h83chqcr2fc6mdf6k30b9vqlp5")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-npm-package.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sbqgg3kijqr6ff51dzs9zds7lf944i0h4qfq61rk737wpq94yz8")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rhys1s0k70c15z22b4iji5mm732n9ha4ilch4cxh6lpwq7znzg9")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12kim6mrbg0xgbm341bwfii4q2n46bbgw8fcx8wq33zkz20bx14i")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-bwu.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0q7ylsd502rm82zm63lfvxygcs3zn3viiwirfw8bh1yxlm2ya58r")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-number-conversion.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fjs7j9npb9vq9id9mpq84p0zig6sz5ps0dgzsigrs9jqx0ml2q1")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-number-conversion.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "009rd8b4yhq1cnmzgckx57l3wiwaynnf3k0cq6ayzqhp5qby3a3b")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-fix-number-conversion.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wdk3y3x8pzqxd996x7r16fchyqma5kxws8ddkpkdk22dy5hdxib")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yglxzgznwc3b5z9qw83wamv5r1hig1p3sf70pxrkxd6d4wmaxmr")))

(define-public crate-air-interpreter-wasm-0.1 (crate (name "air-interpreter-wasm") (vers "0.1.0-version-fix.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1q4hsc64rl38fw24c1a78r5f1xpig3d6iv1nrw1siqnivzv283am")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-version-fix.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1iq0c13lmwrcy7bvnvpy7j7wyjva2v0q73716zi1k7i7p2s24szc")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-version-fix.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1njpkjfi0w3qjhn5ngp491ngg5ps5fzw6gxwyrq928wa7n6pi9pn")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-add-init-peer-id-match.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nanw6dvjyr11cfv78fdsvd81f7x1cmy1pwqgpvh7hc1yjmdc3m7")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-fix-publish.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nsjlafh0gzjaksvh6bid1dkrlcnxnf6x0rd8sfv6ay2q90kpcky")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05icy6qhiilx2d5rhhq1kkv0927dpf6m5azw6dhp79lsjvmvgni5")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1d0xm1yvxpcphwprn76l5ndbyn9kyvlskqkm7rbpwiad15aff8r2")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sbfp9z08cyx45b109zm2c9wgpjx6czf8xy22sq09dwvvs92jhkm")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pyfhrqb2d98c1c0y2alavd8fvc00sy7wjds9hcxjsgqb4rs6dnh")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zpdgzkcj5kjz3djqxnv6f9bp0x6wvs4vpzmcr92k01v3yp8hwsa")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0spbbk9zmmlqj6hzxvxb5n7a94w3v6a138jza50yqwpkrm8pnk20")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hijfh672j7sxi0d2i2bnzr86664186jwvgyasp19x7fscl5pjrs")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sghppn48mbb9kndxx649if572kya540rqccwi4ba1kz1vlr47b8")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10cjzycgmka00z3vp80cyghwwk1j3333bymn38rya0wg88pjhdy8")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1f861wp1ha0ibbvdr9rlvnaxfzxcsrd301zs5f621yc6hlspw8gr")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1nivy9br0darwma018cqcij5hsd4g9rgh0xipkxg7bi199dvn3l9")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "011290514c5xshz0f8rimibizlqfwi70c9kvv5bmdkvyc7hwmmn7")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-npm-package.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "063jsin1mzx338hb33p4d26p6lmzbibbniz0iw9hdz18i8zkkk9w")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11c25zwbq2drr1ji4smi3rxi227vqc5hax70l754659sjkbvahg4")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xmmh1dl5yxscpw1hbzlgy6cwbrhwcy5v7md3y9667az4d48zk8q")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-avm.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "15237r0yph99jw2r0ssm1yhh6ylnnjjpmwfdqah6w4m097ar7z1f")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-avm.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02f37cr182ppv6a1h0bx8cbi76dc6nrw115ky83vpipag02bm1jf")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-avm.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0psllk9qdip9y9jnpjvh08z7wl1c1msnwwz5r3rrwx235pfrcyvp")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12cww4q5v0zljcx26kk8fm106zf2nh08zs6qqmwi9sr0j9q8kg56")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-avm.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10igvplvsihy4m5craq8d97n7a7b8ipdl4gnrbfimixcy6axqnq7")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hgrx778cxij96fckp6v27a8wmrh9hp7flvns8pa0f3iwq21p962")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-fix-ci.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "15cv9apy12m8h9rir70q8ngbq2y0w0qk9yxsnqh1lk5sgw430cs9")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-fix-ci.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0b4a9iy7c27hkrvgxa0k79x3zrj7084xmmppgbx4cwd8v23p257f")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1f2fvvcsr67469c6f2nxq5x33q49ysc8gjnd143hbdvszfcc61g4")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1704lczgfl0vfb6iy832a02g2xnki31s7wj52b2zgka3rlpb7f1i")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jd7dkmxipsb7p7jqjyirqqrkzah1kqh39p9d8nhrqb53xv14ah3")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12wmj8z9s1jdrpl3anrv2j09kiynvv7j3ybm36mhx967hhj0pv5n")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m0mv5lpm51ah5s6nj8syacj1hkigz3l5dnlmjpqk0hbqfybkv2c")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-text-encoder.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1dmhslj3bkaail59clai4rin0jwgr4grkysy1nvh8fa5r3viz8bl")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-new-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qdljbg6ba51hkdgc95s5y4wnd5bfj3mvfsdq8q8vk1mx2kp0irg")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-new-sdk.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kpn5r5zg24piqn2s2qy0wph3f3x947yqpnr09qai2ms9crwkv36")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-new-sdk.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fwha0jsd9bxwdsiyi2vdjcg82s8wy8qbsxz5c64jjj8ikkcm2zc")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-export-more-datatypes.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "116m693dzx6nyq7gb7zd7y4hd1rirnxkflih0y3qjif5b7lc7yv8")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-new-data.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hykiam6wcb634fjxrs0lr5figr0yykmlw86zdahnxyb82jza0d6")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-new-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0anpl819zb68l5598228xg8a054g0qriadl1bixw6zxy3n5cffb3")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-new-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0a3cfxs9ydx92cypx0zxmvvk3shhj2wa84zqgh2ac571y92shd4f")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-refactor-tests.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06df2z34pdr9i6zyzld2gk0fq4yxjb1hy1vl5hcgwwg5b1jp0imh")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-refactor-tests.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "182xkqlm9mbcyygrnqbjz8jki4dkjr8b444l7sqg5nkb7bcgd9j5")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-refactor-tests.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1m1qkdpfqi3mhkv9c6nrx9wwkq8pcvn4zxgb64f3z9ijcj9lpqhm")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-publish.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "15hswb69mmcc6sj56b7q9aqb7c087hnkvhvyvgqy80whlf8hwmrg")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-publish.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lk9cdsmfjggwgqyvkad45hrz1frz2ivav96x29hqii04nprw14j")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-publish.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dsjl2lmik1xcyvx424d1ydd96ri6czql8b1z5kp55khsz23x4qh")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-publish.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14yy4bbb2x540mym8cvb641bqigidhcmgg3mvip9368gssi6b6dd")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-publish.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1dn331a4m0vfwqsy7xg6pjq5950jxjk3ryx4n8kbz022cvrc0gxm")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-air-interpreter-to-avm.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jlgci2d43ji7xavic7vz174mqhy104hmbcfljjdkp26gwlarvbv")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-memory-leak-test-3.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yn76zps4zm5hv22dhlvy7vslyyh269pfnzpqqnhj89nsabak54h")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-memory-leak-test-4.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yzlf0px0y76sqahj30ym385h0y77vmxnsgm2f38kwqrdinaa3r1")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-memory-leak-test-4.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1i01gwny0m1l7097ifvmnrwjsvxhg8bsq6l27rw1mkpzcic89yai")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-new-data.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10rcj9vgardfjnb5mxw9qnpm3ghcvh37rhskxnhnnjp4dzqhn8fh")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-stream-without-join.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0cbr6jxqnmcnk6ppkms6kxlqijccyf3zw0an57b5r0b26bzb4v8y")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-stream-without-join.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0n48s5yvjcj291lnyrcygr74d5znhnd1dh2q4p2w8r55283xpdsp")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mpg4696kgz9210i8zhkm0bvjmq4ykaiiffnr24ks3qvifx2pwjc")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0j0bjwhz56r9ad78d8alqqbz506g39wb7in02mi9pirxw0c02y42")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-stream-without-join.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0z7imq4sdx51a69jgm3iz0861ym24s6ky3dvbwwm7q59hllp1nm4")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-improve-validator.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vnvf57xa9avcqldqrkcx10pzwz49zgiqmla7dz7zw1yawzskmzz")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-improve-validator.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1niz7jgnikaihrwpdcg8gjpx5d1b558b37jbp4cs7b4rvkjfzx9n")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-improve-validator.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l3dz4d35m03jrn29np5yv2vywsgjp96cxwvvz8nrs9vrhz7nsrk")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-dependabot-npm-and-yarn-avm-client-ws-7-4-6.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rqpa0ahsgzkc7bdk44c4s5nzwhzg07kxbs61ggm74hpz5gcdvk5")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-improve-validator.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xw9kq1f7m7wk2m563b45g55ynrkdj440rrvjqxdmfpl0m5zkzcr")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12y5yaadjwjfwlz5i5cs4fbs89rkak7s4lqyj23qvc64sx3ylc5p")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-json-path-join.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18amlxwfh70jrlx2k7593nvppch5bs7viqqbc84h30za8cn1dkdf")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-json-path-join.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "03csvm9zz7ypff7vmfcxxmvzwfq736ih0xxnahqwhzhnmsci5q9v")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-improve-validator.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1d8jafns976iz55b39w35981i13vajfwjw91gis4kzah9p244vim")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1j08z5a7bqgc0b7klcialrv23fwbndd7xx1jx5495mlgz4q5xzlv")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-json-path-join.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0i751wz6i93xj317ivms4yzagbpy1zmg0w0pn6dk6ll09j28adwh")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-json-path-join.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w88ns3dj6gcwfc8yzd7xzvdpdi10nqjdabvdl0dcvc1wlnpv5pf")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1diwmc6c4fra22j2z717py6pbj3xnhkjazw23fvzyc1sm3xnqzgp")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-fix-badge.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jb3k33kr5153jkfpzn28ybgyyxf2kvbrsympkx5lvbpv94894ab")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-last-error-improve.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "03msz862gyhmxf0j863ji6b13cvaik5c8kiv8jbfmnjh20wz1l9c")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-last-error-improve.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0616phv6afkxwmzrgp9xi103fqkm70dvfz8yzc90aqnda7ya2b8m")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vvs4hn83z1x76i2kgigbfm795j145gr18z0g18dj08x269r0hrg")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-last-error-improve.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zaa3xlx3zrmld70f8cq0cyghwr0amcb9srkqm9gwkdrnrd6gd1h")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-last-error-improve.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1602amh2j8ldlqx5w8d4bshrqvkg8dxzq0q2is91mlmvlpll1h6i")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.0-last-error-improve.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1j58gy6p0q7bki31pm4dfaws2svqw6gdjfkdwb39cf8giq5k3snm")))

(define-public crate-air-interpreter-wasm-0.9 (crate (name "air-interpreter-wasm") (vers "0.9.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pp57vvm7axrfyjacych2dp6kkp54wadgv3ihjxdv4zrly1wkvfd")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-release-10.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10pm305llxmwqcv159jw3s2z8s4a9ddk635rssw41pw8waxfmjjh")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-release-10.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1igilbl8356q14bcjnhihxgyp8j435wwpgpbq1bbhbpras0h4d1q")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18a04i0453znkfgmagi5f9m04gjmaz2x60s9iplnr5m1sq7halsf")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-fix-wasm-bindgen-logger.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1y3pds9fsdlknzqknzinmhlq9k3qayfnp389dfx1mh8z0mv4n23z")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-fix-wasm-bindgen-logger.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jafyil5p9q0z0lv0145b9ladnl4nimwi1x9xz7bwk220hcijkwb")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-fix-logging-interface.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wc8cbm41c42ccndi3i9yr4h88951ifrd2ccp69yd97nk1iwv9gb")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0j37lxzl9b1h62shaaxg721igl1n244aqb69s8bh6vqjap7hn46r")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-fix-logging-interface.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17zjz9axjmziq1876v7vpgbnvp9hk9hpyi457i1ad5qd16qsv204")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wv7bpylkf9dyinki2cklp4j5jhzigk58iavh4b6xvs3c2f0hj7p")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-check-json-path.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19hvnqnbxkgzj886xv5xxminmc3dp1bjwy3x1vr4r6bfy4bby6i0")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-new-data.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1j60p89v4mjbwfgzqxsd1fdryyknf6pr43y60axl48azdgldfqfr")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-last-error-peer-id.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ni0xv2qifq30p2z0zz46pp7f2rghh2q97hwz38cqyh7wkkm0q50")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-last-error-peer-id.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1nv34bm5dgzmw4fa41a27d311vn1nzp0nxrlrrqaj6s3139driin")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-check-json-path.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1n9acfjhw8dd1d5ixwjlknyf59a6xsba7jf9lqv0kvr73hcr4ryx")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-last-error-peer-id.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w39azmscw3ynvh4vz3hkzpg5dn4bfbpc1x2xxyrdsy0fivqzkz4")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jd3v96adm8qc2bvvsqsc4q9x0m8ysifyky8ji7cbdxzhlaq5y50")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-new-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vx08vfjx971ck4ab1i9y2pcrnh24dfi0ljr6dlm8axg4jr2w85k")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0i76fl84r62sa99mx92mqs96q5bqx3v59qqrpwv5mn2kn71dxqmv")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-non-join-on-scalar-by-index-and-key.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0d3df6kcgyn27b5vzb61y1mbazvk9y25mypj06yjq902xgxnram2")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-non-join-on-scalar-by-index-and-key.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17qdq4prn9ccipl5ixhj4q4g8kvsana9q8jz9sbfds9kd4ilgpim")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-non-join-on-scalar-by-index-and-key.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bhg39h31wnw1kcfygdn28lncxx6k9xixzk452n6kyxqdkxkpa85")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-non-join-on-scalar-by-index-and-key.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lhcp6k84kx5i9hraagk2jgrpv7sfxxk762yqbg21imwhri4bwa0")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-non-join-on-scalar-by-index-and-key.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lbiksqig92xbss0v156wlbmnvxxhx7c0r8k92546q0c370s86xm")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0cfdghryrnqk08yqbmxrh59hax7h11i3nd35f3x5rxlhwh0c9bk4")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-fix-error-message.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1q9671w20yjwsf3jymnjkh35z3b3mcajm9g9wxx8l5h2fv68ibjn")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02jvf8hhkfk891b8qw4kjzik113m12681n8ds6clbiamkrn40j2i")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-new-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kbndn0npyhlnpkdcxzzkalkg2wq6r2dmzlfbap6pi0sy1kd2pk9")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-vault.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0riq53fldjmp1zdv3kadx1l1j05363xkd4sl1zghdlk8g6jynzi9")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-vault.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10ybfv8kbs3y4j0p2v7yp4fch2wp3kqqb3jr6d1hkvjbc3vrfsl2")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-vault.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mzj7rwa9nmmavmc67v2qgc6b7lbblig4hf7w07c36008592ajc5")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-vault.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wvnyfqaayfra7mp9y2ffgvvshs5jpdnlrfnrjggmcl7s4v77s8g")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jckasfc7w7s0wxjakhpx0pdpl0f19r4qxbrg052b1qf3lqxhqqn")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-improved-new-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wy203wkrf8slvf1z2lgcgym7n4lf1jpm20bn0kkwg6q5y81w8vn")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-improved-new-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0kq846j18jgrv81pfn8lnq9vbhkmya1bk4il3sfyf6w1p2wybiy8")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-improve-invalid-state-error.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17j6y9vqwhyn9gsap89f8rx46fiyfvgydllmd3gs4rjrhb7y2qjb")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-improve-invalid-state-error.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10lpfylq33p7xq9wab9bb3gwqp9yqim8lwl381az5v70yxv3yw1v")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fljknyw1hjnac9zwn7wj8ym2y6yjcz8nqrail4p4kcyvvimgawd")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-improved-new-data.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0x8spjpcbqdwrx4jcyypwygwhga6bk1zml4kvba7qx7p8p70djgn")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-improved-new-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w9nnkc32g5jb9d3dmbsygwxywvmp3kr2qwfs8fiv296z7y13p41")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1q0kywnj2b6g8hlaap6h1pdxwyx9v44kndsl9n5zlfpbf04ybzkx")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0814rbxggs8xxcy4zmfczz0yj414zjwkimhx9ksgh0f9xk7821x9")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xb16a10jvd3bgw4mn9aws6sm0055fcy10xl3g5nfbxhn7z6id1k")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xi40xfcadf0lmvxbizhgh8ww7rcd87kcz2mqpamvljj7hnz6b0p")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0x24yfa89nw7pvksnhlzrvz4cqxb5lqch15rhrcxjds9wlrqprsj")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-execution-id.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qvjfr36a7lbsrfq2x93xyxa3g1xy5wp3bx2njk2dll3z4knb9if")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "074rdksbj0gjqfkmw0lg1djx7i3qrndng48s9ayv38ymylb6n3yc")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rybk5h8fq05gycxxf3qm465j11wh17khnwb76d7djzhk7chs946")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ydwk5cqkhdpsp904grnw0nbm7p8n3pjr68mp685zfrbk4s045ql")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vn0ybf7g2whhn74kh1l8lh3sak46rqkpa9ay5kd686jwkpr0xwc")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19gyv47irdg70mfxk4m0ryymhfv8l8wig61xl1dxylkxalhry7z2")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-debug-interpreter.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lpfcf8v0jyr1fapg7q5pfd9yfqkxpawkzwra1lgxb4ndmzpdjzh")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-debug-interpreter.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0312g7d1lvq7k9vnsf0rrqd5q5b1zrybkgb04aiyrnh06ncxql1v")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0a8xmcdqr11mvw7hlpqvjwj0g1xxrz65a2wh52335racl1x0vfmp")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09s6h4h3giqzqahmkbxfad18s7dlgwsfg3vrzykrksnycwra0fd3")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19zgrq1l3rfi1aklv6zwbl047x4yvadjwzzrxnd4cif0x3v0n0fi")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-build-automation.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10mf0j4nb3cvw1acqr339nb2c9xwmp1awsyvhhvdn1ayg52sc5xi")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-build-automation.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xk5ci9ycz42p7yk3banx2v7kq78g6xw62pa225laqfnb5prcgz0")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-build-automation.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1dsqxcv0mxa59qrmgn53ihbv6mqzzssmmxcgl7hdsxsp3lycz8g0")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-build-automation.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f5ccwgns176053s3a806vhw3snggpc2hralpbvhwrh0hgzs2mnc")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11f0z8ypmyrn4hk179yn1hl71x1gnjyagbbxl09ficzh553zv1av")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-webworker.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16869skxcvlchww812qbxfsyl04a7qh6j7nidlacj8i5q9xv63j3")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.15") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0pxavx5imdl7l31n7prqss4y9kdlq23wya5s6i9qgry7nq5zng4c")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.16") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1f0gligavgfgwhl3m4wdq4bfni6m7w7yv3dqkdbdchw6fjpi4xfd")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.17") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1khxdq68af1wvkx3h75dy5simqb3fzpc4rnqc4g2san90iwlyamj")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-build-automation.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1d7gfzh1xrp6n5dpjygm68hxj0vb34fl14nxx2kmycgfagvmfkyk")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hxrf42a7x204m8g256j2zkymj7n7br2dlvqxphh3f3azw06s8sf")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.0-new-data.18") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "159rgshb4vsqz6m7hxxphk4p08bg61wglgfv29vjmy15f9qdz4il")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-macos-fix.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "037ad45am8dwax1d5bsx6k2pjrindw4z3amvdadc529psb7zlilk")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-macos-fix.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "126plzl9kmn6y22rl5jqin17p7kj93s0cwx0l0n553ppkvvlhrgz")))

(define-public crate-air-interpreter-wasm-0.11 (crate (name "air-interpreter-wasm") (vers "0.11.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0kpdmhylsmg01zl5lfki05r1mz4ciypmmzj0yic1qzdnlxz1y9af")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0arkclcl3bp945y72nyjs4bn7wp34qfyxw9yq1a58yw7w53p1l2k")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17ly6q3cggbqis0nmirqw5kkrzhia1kjil2vhf50y4izhq4v4d96")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-update-9-0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sldy7jia5342vpfafsi4hs4sjhm1sdc6jjwvy3kfll1nkvvab42")))

(define-public crate-air-interpreter-wasm-0.10 (crate (name "air-interpreter-wasm") (vers "0.10.0-update-9-0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gz5k9bcngn2r82wac5qmfyyw2injsckm0276h8y8px9k0fc86id")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sqc4qbwx3mdm1zky7pcdg11xd4nnccag1l7qbm5diiz50bvsxr3")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09qcmcppd413aw4ax0xhilwx5sida1r467dw92nc2rdqr2w75czs")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wjzhg467ghmv176x1almw5ni8zsvmb1d3cybfdl39vcc55xmnnn")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0isvhaqgmzr11vqs0r90958am63p8by3rnc7hq4c9n7iskhzw4fh")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0n45rymc1v3hdkwsrrmc0xw0is01i5gwn48nqjvcp73fq3v0ir2v")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0d9jwq77701qvpc314ik6db07cwq2hs873qr9qyal5h4znxp8n7g")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bwskvvchzh26zbymfvqkbwdmpcpysmszv0z9dc78wbc3japac7y")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0644ndlr4qagg6nay6177r17i5962qxlvwf7dwk0dyq7xgv2n2vm")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0g5pw70i6agriwx6xyb5hvd43cdagc05d9gkk2mbjb3p2i5l5zdw")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-apply.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14ll1g9rv0v6fc3b4ii4k94cww6kczjy5j4bqag5mzmx5jsrkdz0")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0pmwjsfwxn1iw298cb3fvm951cr159jg0bh9sqznjl81j1a9laz3")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lh70n8vd8lv01b8ry2qkki7xbpjqfnmn48byvm33q5x4yy82j1m")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mpy37zvajhpns8xm1b645x6a6565920q82lq4f6jli6n5gxnwag")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1khjwlnwdjv91vsmwz3sinamdws6ym4vjvm48rs8w19qy6d0sfqq")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0525d20alnbkkdy10k8aviwsdv1mrlinf2pgj4w3ah2gksl6zy1g")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07gpky9idcw792skiwz1dsd840s5d1y3qwnmj8qsmdqlhrnpxa6y")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wv230qka7fbq2bmhbh32p2q3wyld6akcxnaqwr67icyfy00mspj")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nhip5l8rxpd3pb91pj3x10zz78s32dvhz4rjx53ma6ahcsdfg6h")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17b4srfv1msjy3xxczgq1fqjwyskhl565pzv1n827w6189x50zdw")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1y8vk0zzxjx0spcdqvmi8mc84pb2n2d8g9cdgn6ffbibhj7xwxz1")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.15") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18pm4ypc4s8gmaddpbyva6pjzl2fi09dng2jzw2dg5s5in27wvcf")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.16") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10779zrq2pad45pi1yp4db5cw78npd02hnbkgd8wgn3ihxpcmqz7")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.17") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vlbvapz5chznmd0kdq5gn3xkfbk4ycgybrk4m9p8nj50c977d1l")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.18") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lqybfxdwlmh18b8rr8ay44alpr8kc2xw3fq637bja43hviivmyv")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.19") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "192rhv0lisw0fbllm5gjl7aq6sl6s2x2ia6i94rb7frbxrsja5vv")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.20") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sq3d1m419nd6d3f6j40yxngwmz21n5nq36aw289dpl74dzd6w7m")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.21") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08fs91ayvfsa8c0x16q4pw9sjkc80as7hbsc96l4ykpc4i28i86v")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-async.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sgiqmx0rx81qcns3qvh3lq4nn2zcrajwv1vl1lpc6p4yhadw0cc")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-new-data.22") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10np57m0ya13b177jpy0g5z52pdbnql5ag8bbbn7ml990p5wsmpl")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-async.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vj8jw5lbfcn7n4vgv7jdmis3v36bppi27p5wdvkkdbm5micg2y9")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-async.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1i09qxnkk210zhngy8jr6q5zhqr7nq746asr0jbaafrxz44k5wga")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-async.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1r1vdh1bcglrrzzl0dr7b27skkvf1djhpc89hbpzph6d5ql1w400")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-new-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sc6q42hm8v1iih3qwdyhd0qwmznjmlray0sg0g4f1f6rhdpr2ix")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-new-data.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1f1sy1d2csdgw40pcvx9s59lqjl29pbc1ag45kjd0c1nfiay21rl")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-new-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1q4ms582nbv9cql1p09lyxpbpqb81ms2ikcbal081xk267lcy08p")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-new-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xg8qh0f6qv8krsygaz083i35vc14a1misc44bhcw8bz2r9mc6fi")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06czdhnby8a5y0na9zqan4j8n20hly9jda4ygm2a87nj3lkaxbhg")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-publishing.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0h9gh62l28kpkk56mh6489c4hghlccrdkjd2l0l3g6fp1y754lmm")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lcjdby3qaq2jrm9vcyap0vazbvp2vqi0m7x1sifjs8alygixdjk")))

(define-public crate-air-interpreter-wasm-0.12 (crate (name "air-interpreter-wasm") (vers "0.12.0-async.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dr3b8ggpc5pwq3qfylhzqxsimahmzv06jmq9xlsp2k8d9d5aplz")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wmcz4zbzqqnm8zzy2bxg5vbifz6q6gxz5pl9p9h162r46czlfzy")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wi7vam9w33nqfwgai93cdjhpzm640cjx8k74n0aamzrlp5dg2wk")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "087zaviiy9vngqhicj77q5i9vm36cigpya2i3f0d43g0nibxlji1")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ihn78vqn1025bpw8yq0sfcdmwiwca7y596b8cbrvyq0rd7ii3db")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02qr4gyzyhxbndqf59z0w8p80l4sf3x7m4vgpmv44wp94lx2j6s5")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0g9hmkpl1kxrm8dlvg1dv6lxyw7nlda917w7swdihxdsn1a62x4q")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-flatenning.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hfv11r1j28pxxwlwxb8i982c0w1hs5mvcjg3cz086pbyfb8bcpc")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0crwjjlyfy83n6dfqbk7bb72ygdfhlaw5zfb5g97gi98cphhaf2s")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vzndqs8s32kn0w54pbqa9d0inynxzzjxb752k9vjvkyd9ccmbkq")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14s8zmksaf2k0z7dyhhcw8kd2zcqqalba7sgb818qf88qi8r7b5q")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16j07hqiks1315aq4zgwblcj5idmjp3hm4821r74d2qy9c2w33z8")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-stream-join-bug.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hryw20p6a4i9gmhpvdq7n8wv4shjhzbj7q7czjj08wfmmxdgl6c")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-stream-join-bug.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gk4cgi25zm1pjnlr4chgpki5mr6gbws0frwfll5wh82z702xhq4")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-stream-join-bug.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1z37afgid605qf02268lsk8z0bybd6s4hjaw534pc2nlw9q1g9xb")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-stream-join-bug.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18rjc0k03q556mi7596i3zpfvhfr2kysvfyxx4mmvm4lqnk84nih")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0arfnhlvvydx9mk6gbvf2721z6q83wsdyx0m8glmyrjmsz57ww2c")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ak7l8rkm38n1b8jnk02gg5k7jzzg6yk4q1d30g14n9jnnf7lbp3")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dbkfj8yby32l7j86s15n75abdr398rfn8a83vcr4623hi5b8ww5")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lcbpz93j52p6lppm0fqqqm583n1wbn0qz6hvhl4gvh9r1l1rc86")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rbcyzdflpndx55rqvggash1m71gwbng2yjycr2g8qdvypm0xqg2")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m49r8n5bxpnyhjh37zn6fii8d7cpgi6j3dx0bxs1x6scz2nkdjq")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06cm1wxacqri8k46fy5b93r4lhmns89kwjic0pk5jbgiyl3fy3pd")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.15") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1j3mhyqpbx8iyhk68wni84zbh8rpssqf9vzyzab0b8fgriv7wlxl")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.16") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xdvavi744yz8lnyz5np3i8qrdmn9ygcw45b8b6is4k3d0di48dj")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.17") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w7sfm3ngzmqi0brnjrk37ac9iwmcbzrfqgn2fgcrks9drp4x93f")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-de-error-type.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w0wassk6glqgpvx2x5rblzaif4xsbzizl384hh41a7fd65hfshz")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-de-error-type.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gd800kg746vac1yik020iidb2ah2xcg5ymkqhwv7mxwbc2ppdx2")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-de-error-type.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mc9yd7zh7nha80yz67899ps4dqc2cxczw2yz35cpw7avaql5863")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.18") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04ncs8q69z16ar5nh1gdr83svzk6dhk22d476hiiy7mnzs7ilh2f")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0v89if12sj0sf09sdkskwbgkc46m9x7d2mdiybp096scs46dlv5b")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.19") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14gc7cg3s5mfyrav3azhrfj1mx38wzc9mv3yvllrkzphzxvnqdwc")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.20") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0cbkw20dcza4f7q9xgxi6j4ndyid37d94v2klnnql05kalg9h06k")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-remove-serde-version-lock.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lzvwxn2y400qv49a0z37ckg2kw9rirprvvanbsylzn1l4f7m3ay")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async-no-serde-lock.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "158d8rpd915dqdnvlalw0393klxh8niqdl9qvna36msl071qi0ab")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-remove-serde-version-lock.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zha5zvx0cxv2qwkv239y4macns5d4qjqcdkiz31nspmc01xqaxj")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-remove-serde-version-lock.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xw5x8bgcdx0lsnvzy1190sdr9zzspvgscm15fggxjywvya35x2i")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-remove-serde-version-lock.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0g6aby9d08ig9gg21yl4hcd59g8hrl29r2wfw9zl7viap0z7djxx")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fgng45z21z99riiamc3qsg5s65p7vdc2m25syk4p3scbn4x9m5m")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-issue-137.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0r0f003xqwqr6k9qkb322vhdi1iafxrsbja8z84amx6cxyx7vci4")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-total-subtrace-len.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ikmh6ggc1y2kq1vi262s53p41hy535fcb9jh8y16licy23rb3sf")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-total-subtrace-len.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ix2a0qqjic6ix2300r6l1pz0gqnk67csh0s3x1r7a3bq7xp0a21")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-total-subtrace-len.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1br7b6jlb9yiqbgd13a8f74psy85qkkhnicskps6s1j0iq6bc771")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-total-subtrace-len.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yzan7lr6f18l2mqjy3v54w3pwvf90qzviv3lgm8cx7xv8x7q2dw")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.21") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qzln0n7a97ip6nxjwvmyawy5bafc87456zsxsw6lkmq5jxjbdjh")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.22") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12vvxhijhvdbxypmhm7gidww8h55179gni9y77a1k6ajc14j3xbv")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-total-subtrace-len.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0paihaz8gmcji04nh8qlalsq8xdpslibfiim039zc7x8rdx9v545")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-total-subtrace-len.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1y5p9qzf78499yq0z27c9ac6lqaibivkdbabyqp43bga6rwi7gyz")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-issue-137.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0krabsaf3diidjfdkdd94by6z7rqryapcr00hijim4wk0cm7yg4y")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.23") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zq4wnvbyzyfn3rq22505dw1k5hlq5lr3sy2vpdi2irwwrrqxpwg")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.24") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1c59xcr92mdnk82rp667jbmc2zy4acsbjf5pqaqhmvvpdchfc5fg")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-fix-total-subtrace-len.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hvf103iq2xazz6jarl6b125hxg9fwg0m8alfqqzh6faqqkyvccm")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zxhn9yr75rrgj47wib0yjymdr0mk2wg2ar7cdrjn8g82c7hr3a4")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.25") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zqwraq382s9a0v3cmrwqxqjbp0jzf9bnlik4g775gri0r3mri3a")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.26") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0h7aknkhpxjvlgikz0qfg523wl9s8xaz4rsd8zr8iibnffbk74il")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-issue-137.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12isnv1s4zx3g5an8x3m1k3qi1nzgrd3hfj4znc0nwncxxnfq65j")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0cdl1xbqvbfcpsw5bk07i9v4vsyb184hlh47gf5fj8x514yln0i7")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.0-async.27") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yl419307z2f2zx9nldqpm1rdvvgknlicgmki4hv1vxfvz37ckhj")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0a2vi6a8h7kxg46jxngdxwm02cx4sfmgl17czqhpash98mgvlhm5")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vpmqb586kyw0mjnfm6959xl31ki9jz9s9cqp7fp75nh3rl5n86s")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07rrl059iw7i7cn1sf08rn6vkh7dvykdwd6pn8a745abz118dys8")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1may7k5vhl8z1zhdhmhip70gf9mprxjfsbqyxxi0j0pzccbrm3n1")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1cbh8z87q9whn54dz97vsxlf9b5dc5y2zbvhrbb166xkv8n0pjki")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-issue-143.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1n7m2294df60r1xb5a1kh9gs6m08bmkbankdizxpvkgnd5bpwgd7")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "074zcsll9qspbf2cy9c36vih0c84cim5xlzqgf6ixadpa4l9jj3m")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jl6bwmpzx8n9k34k2yf909nkw2rm9v46d8mw95dy1h8jwr9x1qj")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0z6w3nga45wg6a011r1xl50fc4jp4ps373mlvyjpr6haiqmq88dl")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1v3zffg4gvaf528ygb6diw2zcppq4gfsl81pz6h1zi5n1ny8ic99")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "035gig26yjy610zmb2ng31m8kxldjs08ix1gjd9wrlizmjp1rv84")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0by5iah1xzqh5azsbr9m8v9s963m4bdrfljx6djsb43acbad08ri")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-update-jest-version.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zy8cgz6clb38dfsqd0bzl4agw9dry6pp9wg056vnvx7x151bw8s")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-update-jest-version.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0aw0zvymbj6a3yy6479hrb93lz70gb5ic5919kp4f97k4ik74q1w")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0d3v3kn1xvi5r6lsbb3kai8p75y2xmnvlqvh86jd2lwylnxbbpvh")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "00hirb5m0ssjm24k7xf915ycjq25hhmdhlniy2hgxfi6p4wphwfh")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-async.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sz3jpq22m4czzwksjyn80ba2cgasii34ssb5mhhq7wglyifyg5y")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.1-dependabot-ansi-regex.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1frsv5376mprwfm4zv57jk9dxr84c2d5qm21av47y6x0ajppswbd")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yhx2as3zw1iar590c40h88glar8lbs0j1n02mz1l27fgp9q49bx")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-async.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16ydmdr7q9dn5bmhfbyllpsys0f0r3wix2xxgvhc4bm4jklrdxnq")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-async.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0iillk57p69vzs42wmn46qa5s4pq17126pl5885nwkxxkqf4y0i3")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "00y3w6vvg6vcn74l39g3300xmj7kxjz00xblyw8q98jcspldkw08")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-bump-versions.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "048yi18fl0lr8hlri5dgv20g5xbghczwpjv8yq7pczmymwcb3q2d")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1q1m424p4624k7j13pmxq4ymc44vjpkwx2l02krj8wx5mv5hgqvk")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-trace-handler.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nz226rgfbx5s2si5b7i8wmnrqqkkfsk952qalf7vsgrcmlmyqp1")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-trace-handler.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bjp013shdidlxbkdkk6sf84jj5aclbdfvx7qsssza7zw6342sf1")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0brfa2rfbl1llv5wi3bq71iwh16a3pq02cxqd2nzk2lvpn5d3sn3")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-log-targets.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12bqqxbcr6ivw9l7r1img0kd58avq3dpl8lfb2ndswvj9w3fmgrk")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-log-targets.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "196gad2nyj4s2zyacbxwmbapd8mabrg3prg7b5891vh3n2n27iwc")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sqadaq0pmnirxa44q8nzar0q62i21y04ljb6ydhxyhb27j0qphh")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-values.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wx4fmkkp4z4hmy2b244qwmphgnrslidzm74d0s0clqls1a0fh4d")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-values.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "093qwh4l6fpaay2gi0wfwhi26w1q9c81kk8d4xwx7pr9hzmsjm7s")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-values.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07z73i28ip3pnwgys5rbvsb82dkhw63i1khfy4j622rvxyqpqlg5")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-values.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16547czs308m79r8zkzbr905mc9vmif41iz8dalcmhwxk0vna79d")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-values.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1m9ndp0zv8714jadkklj2qxify24n7ya865qvg60r1v8176sb7r3")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "03bjdn8pz87j435mg35l10gf82psf5n7q4i66ggmflmr8f1bkjaw")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-remove-json-path.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05k7fv8yrqqqvv5rq41kp1r237krj65nxxz4qwrcn44ysapvw4vl")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-remove-json-path.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0n5dl8bgr15y9ipr34hrycbvqz98rmssmq675m03piwqgp4pxcy5")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-remove-json-path.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08s7l537mn16vkcfimncvaw3wcxv4llxflvg255cvsnyi2j0p3gp")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-remove-json-path.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gksdmvndgc0lcgbdwvrlz3yy1j7ig8vsavpl0b83yw7g1w6cpjs")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-remove-json-path.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l51nbfbs478z7d97df7q8idh1vas1cs9jcc191a8s9d3bm7ij5x")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-remove-json-path.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09fxnggld171c292kw6rcj4zv959xv6kfm57zvrw05bj4yj12fdm")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-delete-double-description.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06qqn9a7h32r4rrxrgk1b69dvlcs34lsflx37aggy42jrb3cvjdf")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05lfbc31m8d1d9wfp9l4zkmmif8hywz6xdbh42c6gy3zzlpg82v4")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-delete-double-description.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0k3s1fr2qylmcwkjqc6kdw9ws489ncmbsqjgy8p228992zpz4kv3")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1v71ywnhq3dlfw1d5bbbybfczsfa33ss2ddvqqmi9cpqw844q5y0")))

(define-public crate-air-interpreter-wasm-0.15 (crate (name "air-interpreter-wasm") (vers "0.15.0-decouple-instructions-tracker.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wg1ldv1d7fbyspjkpd53d42gkbj4crafx2wjkfnhzsi8vm018jp")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0phl39qds84gqvk7dzxv0fjfxckcyrvca5vsz6kv6ymkk06cfdkw")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wndp78zr1a04hp75kfq7zyignl1kac9k4xica28dzj1bzj9mzmx")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1dfkdy3bzq11lgjlynahpc74058bgx3a1bx680p7d37g5cqylxyg")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19wf2jk7jhg1vp3z8kl03qn9ijlcd1bkzngg2wrwl6ja2xgih9ir")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mbq1fxmw900jychvhhxd4b2q0v1bh2z4rmycxvkxr6xzdnxissa")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1cbb0vgkh3zpa2crj1g8akx1b2y9r6xfcixbjj3kbrk5rdk2rrpy")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19jba1lv58ba78i3g00l9iimyir0hzw24zj5q4z3g2wkca8k2w4b")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-remove-json-path.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rmi2wc3w07akqhlszpw3sv2sx3j2ffivr0d898cwx2aj17k6j04")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "089w22zgq3v05vnldxaacgycmfh34ihba020cmwj67rg9n4r59xn")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-fold-context.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0d8clla57ndgf3g38s0dmlf27l9n2z0aaz5cj33kgqcpj7g5mlnh")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-fold-context.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08acgfbb2kjpa27aizhpdllnr2vasq185fc651801qlr8dv7a07m")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "13m7r16r7pr1j3qxpgpgxrd6rwbssmaqhiz70zzwx92mpaajr0c7")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wsma6wii1kmiiqmgp1ginzj0nv5bl8ibma4g5y6030dnzs24i7d")))

(define-public crate-air-interpreter-wasm-0.14 (crate (name "air-interpreter-wasm") (vers "0.14.11-issue-137.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1k7496zzi726r3m9jiclfzjsw1d6p9li0if2g0v06dxmr253b2vv")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-expose-parse-ast.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1i41aapp87765mnzxjz9n95p0fh20ih32dyrk67sgyxsjjxv26lm")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-expose-parse-ast.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bmnrm9bcq2ssmzzf465bsv9fxr3i1m0yhv0zcic2h7lag73crw5")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-tetraplets.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mnp87wwfh1gncbznsd8wdasksdb3f27pf33p5xhiaxg8dn278dr")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-tetraplets.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vg2wnd7vh5p86ci2c21csn84hvh2pl0kx0b7l5y5wnpbksfzciw")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-tetraplets.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "106jf09fjdrkfsv00b28mnx67l2a79qj9d9sd5b5s39xw8k8i9r9")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-tetraplets.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0p4q2b1q4xf74cg3pb6d2qq4pval3agjwc2mgyf1b65v4074zpy1")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-bool-match.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "188rq3498j2qg40q41dlgsd9ycp6p9wa2cqafm4nz77pj363275m")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-bool-match.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rv8d2ir9a2gs0gzw2pzvsqmpnpipzkzs94d9i5dp3rdl369jvlv")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ihcsjr4wv80knr7kkaq45dcc78hwd8dqyi5dsql2him2kp8sn3r")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-tetraplets.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1n4196d20x4kmhiyzcr8ky9c9aqb5k38gv0l6vhrwfaf7qvcsipq")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0snk4ipvzficv6wzi7l2nwh96pxbpsmn8aq5z3msxppqd702ajd7")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-bump-avm-version.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0silrpqd7zz4xyx0q4dihaplizz88parzbi0khmagm9l63wva7lj")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1c4gii0p0ac6fjff29vgp0lq5gc387ls6i9xl5zykbshkcyp9y24")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xlb9zj217xp9ymd51m4a2pngxb2zivh2cvplz7q2n8wqvh78rx4")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0l78c4lx2k5yxk2k3p1cwz8v80ggyy9pih0pfikskskxwgkpqc1d")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-prepare-errors-mapping.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "011pph1fsjbl7iinq40lyzcfanlw5yqfwihj7hzr2wha175fwyil")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-prepare-errors-mapping.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vhlpb5vkdd630qzbjg207ll72s1349c1c9nq395b3y59nm8dd4n")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18fwx10sm4bmrcj87mdr7hz72zbsnl3x53n9j86ajak0mcjpz4m9")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1z6bqv6dihh403d7xnvwxgcybr2sp787h6zl9rjr9qviqqxgn9pm")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1knyghzzvw8k64ampmlyfgh53q60vscwabi0ycqi0k59kqfh7v4k")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-values-with-lambda.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1a9a97y6shmhqfhxks4vab4vlbvxpph1z34k2j2ci1zzbk2qvr8j")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-values-with-lambda.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ywqq5pwq7flmi14iwkswfv95wpamx7r0gh7r35zj54idbbi0482")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-values-with-lambda.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "00ymw6j88hbc94qa6hdr1c4swr0w65vhwxk97nnixz4ij20nkwkd")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rhcnk1h7q3qnd8n1rh0bd5crv7rpls5ncx3r4hsv1y0f3h1g1ay")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "099yhx93m6k1wjy52icln06cxvhyxl7nszdxxcd4adi0fc8gra24")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0q4b8y0kysrk6jk75qld5man98ldfs0dzb83m28h5h4mwv87qr61")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16nw9g5sfdqaalz5r0f29cr9saq08p1wdxd8cyca0sminl1dnb1g")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-values-with-lambda.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f2hffamkzmq3n1zpzl7anlqlsiqa9h50bi9b04d19hwlak9f492")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-values-with-lambda.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bn35z1jmc8wcpaimxv19qxabgybnrylvkfkjx08nqx80rwdlkjp")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-values-with-lambda.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06lryihhypmz8a64j218inw48hx145p74kdzs5mwkc6zzma07llm")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1amil5jjb3ic73cz62bxg9c5652yvj6dab2r39gpzvv06wg7z4gi")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1d4pbnf37y8wjl9fm8y8hqykz0py71pyqr59hr5vgnfhqdp7ayy7")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1q230va9rll3sxmm6pmx3lr753mgqi5skxvx5fqrywn6nhckhlwv")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ng32abm363gq80iwkl12b5blcw0lzlraq6qrni8hqyqxcbxyhsv")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "148rn4fx7x8wh5favwhm3n2x079fvcpvdxg9l44y82cn57kcx7r7")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14s8f4nm5kx2fq06w88y0dqs4vcgq2jwrnydwv8fjhh0y89yz0ph")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qkargw2vhbnh322w21n2iimbhjggm2jf4kixfn3g6w4siql3hal")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fjp8x7f9ayb8kz5b14aa14cjp0qq2ycwj3nf2gbxnlkirw9c43m")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0p1xiq5hhpbiayad5jwpfs6s55qmzs2xdsmf11c566vhp7jj2g8s")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "164vx2dsjvb1bxkkrkkj8ix2qbrkwgvx6jvh5d00rrhj78aqs3p7")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-restriction-operator.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zk2ipbfs21n11pm309szw3i1pcgy3qndl9iavmp2k7pblhypr3p")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-restriction-operator.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1259kzl89cpag531fn0krgs5wrdby2fc7vs65bnxfzzr9fb1mnw3")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-get-rid-of-several-values-in-lambda.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "064w7gqdm3bh7v7hcgp0xrlgy23c64z8zv6z7rsf8i8k6sg8py5q")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-get-rid-of-several-values-in-lambda.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hh89wlaqgx5sdn2zvbf9m7idilxaacsng491jqwavsgwgdbh3yk")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-par-ap-issue.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0kz6hjja0fpgaycnfadyds0a1icpwsn78c2a9ldwrs01y9f09ib8")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-par-ap-issue.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mf5pdpiydh0kc2awip06xy5k69fn1fy6wsphavml5i1fggabg40")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-webworker.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kbvnfi1479ylj0x134bc2xzw4h397nyi11nrc75nxbvmngl28ly")))

(define-public crate-air-interpreter-wasm-0.16 (crate (name "air-interpreter-wasm") (vers "0.16.0-par-ap-issue.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f05mwjnsblnyjlcrwxhsn6sylpsp6bgk69c1ib7ibz1ddhhpyzv")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-restriction-operator.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zpzl8as3sx84fda437ama3d1jj0gmagvq7bvvkp07c93blppwgh")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rlhmsdhk47lnx459lbv3w5x8d0gbs5qrbvxj29d1plc1v93mjbi")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-par-ap-issue.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10rc6503k4yaxcam292pynyvcag078psd9j5jqy2qvx1gxqcm0gk")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-par-ap-issue.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0p8b6galkq4zblkh0fjfwkyvx08lfq9a40ljxcbrx0i0qwvi9rx7")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-get-rid-of-several-values-in-lambda.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ch53hijx4v53qcc980hrqbjsv46ih0xj38fmkbgy2yh7x874cxz")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "134xlswkcdn6kywxb3w0n92q1i1157bk4f17nfw7v996m94gsfhm")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-par-ap-issue.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dxgj5m664ss42fzjph1yfjri1sfkhp4vhh06ldvbqw5wlh2kmzz")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dklny253sszpzqqliwalycwa8jrsbxq927h2k4ndbils79grkjg")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-call-results-arent-empty.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1niaqx50690axgvga1rnhy1ni9x71rdbmib3c1zdmpmpz1f59rn7")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-180.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1f99rkhpxqp4qzslm8gidpirnswp6430ya81almnm56zix32xrg9")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-180.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vk4i2b19lxqdbc66rjpzcwd5m8jz4934samv9msk5rf1ghr6vg6")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1r036v5s88yfhvxa9bma3j6psdxly62x1qwkhs2jb7fb636gjjci")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-180.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ax8j4qq73j6zlfpsq7fv4kpr1019jgr51wpxcbai839sv0qrzah")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-179.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lswizshysaaa578vzy6c7y2rz8vras0dr9k65av24a590s060cc")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-179.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1das9wna91bqgwz48wh15qp5klvcpsgjmqgsfdzhmjbrc5kmg6q8")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-179.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l1xcvn1hs9p7xgdlpzn83k5blpkzc10vb8x5cgvsclq9ac29ds4")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-call-results-arent-empty.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xz2nxbzg1zh2xab2lbijrnfjmfn5b2hvbv9sic95lqsw00z3yqd")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-179.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "064yigrd8rymnxgxnxrhga8754afipddsax83aa7h0bjlrl3pscw")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-call-results-arent-empty.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "075zjc8dnw0si9kkrpk19myqlcybyxpaag2r8qzkgpz8z5bdhci0")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sbaj8yny17hkm10ddd07lmry8yk93a43q2fw02hc972z843frkx")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-173.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0j8alp5a6l9ify31gvlxwwmvs1h81q7szn2qxs86wf0hjgwqakn4")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-176.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08b2f0j1xlglwy9y3lss1v9kwnpvpm8my0wlyh5fpasknc20syqb")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-176.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yhwm173sz31hc6aks1jaa7n3b86n21cb2biljb0fspfq12y88h9")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-176.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m3l4lj7h1q5vj6z31rrgwniqqdyy81xkfmm8znyky67d9n1pmz6")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-176.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1idpl66ila6nr4jxnmlfv4z3r97kslj40s8panf5d619p7ggb13n")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-176.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "15vllf3cjdf7qls9ii92r421qdsw8agd9fhkixyfi116f20gi2vw")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-176.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wjh5fk2la6dxa6r1bg07qv4my65sxd1434cag8w36xkxchw9sq4")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-176.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hryl81z7gz6n0v35rrls8hwc5wcljx27hwy4vb53cl6vdkxwl74")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l7sydm71yzvyarg419iiz1bd7a0jgaxx3mc4swfziqdg7nl2h29")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-issue-173.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05pf9bkwxwrh9p5mj53lj0js5l0vf1ry36mlmnasngh9w355504h")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18s2ilm1fq1i99gzfgcdjiy9cfvpddqfhnx81yixbqdaz2ca6wb9")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-marine-web.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "084cfrpk44f0pi67izg1pyxwg9kx171b1y7p11f8nn8axsq1113a")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-189-fix-empty-array-parsing.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12gr3ksn55rj00agklp2vq85xl7rsw495zilg2jxycfpfrx40kkc")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-189-fix-empty-array-parsing.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04b1iz21k08da1yqx60ql4s6h2grcsxj27dgh1y7lyq95w6b872j")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-189-fix-empty-array-parsing.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zmjwfj093x5kj0ap9hfpsz00idy0ia362aavg1iw4w2n3wdlkjb")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1r2rxqihnbpx8jdyzcpi8601awhd23jsa9mh3khj5lwvp9fnbd0w")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-191-support-scalars-in-lambda.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11qkg6z26x2lc0fmnvx1xq8xk01x3h4bj2abjs9rckvpsw683pij")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-191-support-scalars-in-lambda.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nswi1aiz0yk1g5sza5wwl688d1zfvnmsgyyzgxyh8s7h0ai5wy7")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-bump-avm.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yay9w34ip3x3kdgmpqjbfvfr5lqnxma06wrv20bz66j4kpzrivm")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vcwy0pnagdz618v2df5xrdx8a29r7vfc4l49bzb8zmq857g50np")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-191-support-scalars-in-lambda.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vjqvi1gc984inkzh31x6mm0rbj1i3zcw1xvwpilrskkgb9kmckx")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-191-support-scalars-in-lambda.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wl36rxn8yh8zjivjzd1iff480w86dv9dh4f5acnk6yjn1dfa6yb")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-191-support-scalars-in-lambda.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pmww02wly482d4xf63qy4j3q1s4qicx5gx6xx344sq5723cvfjd")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w3s89dbrayc3wb3nzy8a3qaxbp4prwh7pv4dqc9lv2173kfy2cr")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-171-add-variable-names.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0pkxjypwz959wjmv7hcajnbvlpafaz9kjlvcyfclyysz35qnbmh7")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-171-add-variable-names.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08iryfqd57am96sbs8nh78i0hg32kg73n2n2hvwap2fiz37s9fdb")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0df5x3ggyyya3yw5q1bbfj2y668qparfix6b2575f661jlg5xgbn")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-195-introduce-fail-instruction.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ji2733xwl7p652wl7rsh3ryywdn58sgxiay40m4kkgsi30grbhz")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-195-introduce-fail-instruction.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1v600yk4rz2pz72sgv74syv804phd228mp7qy3v1h2y43arql612")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-195-introduce-fail-instruction.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ry1jzb21jvw65685ci41bz6l2nxlpyz266rxf8pighgvafsic05")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-195-introduce-fail-instruction.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1g9abyb0csbhrm1vs2ckjgj2llmr0xmlhb577b6sbp6krn7j95xw")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-195-introduce-fail-instruction.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0d8iq3mmww099874rq4w8z6asszsxggvswnzgxdwxdwv48j4icp2")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-195-introduce-fail-instruction.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fxk7djbx498ca3n3a40d17xcwdxn999ab7j8jrbvm965jaxa241")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-dynamic-import-wasm.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ii458pfkmrl34zv7mnrw1c5bqkqbx26r3kdx4ccf7jjc0cm58wz")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-195-introduce-fail-instruction.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0c93danpr8yhbnbjqp1q57kjxi2nljs39h8r0s7sglykpqcl0cwl")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lism9dl0hmn2w0waf3zy39cj8n63snivwfhn2m09qr84j6gmk1w")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-dynamic-import-wasm.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0px52fn3rr5w3gwlk0m9pvhbwknkygsk7z3p99cr2vp3hh1addmq")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ls4hrdcb2njiwmiwfy963wajgh7mgxkpzlwysl44dq3awb9kgjw")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-197-refactor-execution-errors.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qj91d90nb1xh7lsdi2bfrnvnhb8rmdhxyrrwg492kggwbszimgn")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-dynamic-import-wasm.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m7m8404fn6yk59xvlvyarvqn38pj88qpq0kl7yld25c2nm2h6dz")))

(define-public crate-air-interpreter-wasm-0.18 (crate (name "air-interpreter-wasm") (vers "0.18.0-dynamic-import-wasm.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02ln68ihlgb9zbnn94b8bcbaisngdk6i4dv3dnqfjl7kph9lr0pb")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-197-refactor-execution-errors.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xwi61gwyv1c5k981fdcgkkkw3ighnax2wxzh3378vz32r6sdjap")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-197-refactor-execution-errors.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0x2k4gjf4wbkvb4jk7brvwsvvzmi8r6lqg9vksplqdq8qp8a84fm")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-197-refactor-execution-errors.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12w6c8jkpkn7jylg5amli8i074grc4nsrzx1bzqq5a9vf1xb8rlc")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-197-refactor-execution-errors.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0axphaq44ssqdsmyx2n5kh7qxy5226a8isa15da66z9008g2ama9")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-197-refactor-execution-errors.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bh1n5wp4fs91sv2i321m04vaj7czgiw9n464jz02d060l79bqhj")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06p7n4w55qh1kjd0qz0x84hg0klpggjplg0a8fbqw3v92clfk5yf")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-bump-avm-server.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lz4c1yjg8ygs2cnyvnfl82dcl0jbp09ivq3qix5z3zpij3jrg2x")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14hvwjv3kggjsa6ab9slxfr8q6zxj73l5zj8k958h6rcwk3kvmpx")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-dynamic-import-wasm.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gjamph4wbf347c81dnyjzdvf27a0gzkc43m3g2078b5m84mq9w0")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-dynamic-import-wasm.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08p69pf3vdy3qfg50h9vzg5fp7c0y8nk55ic70s2g9cfdbd0ysp3")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-dynamic-import-wasm.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "044l98r9gh6adzj3c8lhqb73jnnbp0bwnd4nrbvdidmbp3dn59z7")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-add-instr-name-to-trace-errors.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1aw5yybnksnwpiq02bym08xd62dkmm54wmay65xvazrkzqybw4aj")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-add-instr-name-to-trace-errors.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wadzql9m7xyscljnkp3j34p501cc20qf9aiqapsdl3ay30wdfm9")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-dynamic-import-wasm.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14l4i2zszzmg6aiadqpf0n45mb3iq1w39pbddksv5nal5lsjkcl7")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-dynamic-import-wasm.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1agr7j6rhkfpwrnc3lp36l2xf0bjjafcrmxa97k2fy5rqm25a40v")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11rl05rj1hwq0zw51r6giv976b3k97ncxpbyznm4zdwg2x2j32c7")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-refactor-last-error.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yrqrm91fnvp60f6v6sh8nml8ph99a1xw70d072xsyp46lgshm40")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-refactor-last-error.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wy26ncp2ys736nlnp8lffi8pw4dfvjnks9zkkvqzswl3dlxdvzb")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-refactor-last-error.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1v661szjy2qx8c5l71358h4cn5k2l1qim02jilc94lx6m4ggfwlv")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-refactor-last-error.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lc149zcpw9zcvdn3yqfdq4ydg9qbyvjfdnvpwrkvfni9fbyn21i")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "00cawhxmxznkmi5ajmplwqjh4igfqgxlgffxv485lx2pv0h382pm")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1s4xkd316qsk8yb8nk1056z6q1n336axvsr8miff1l58mgyx59g2")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-203-add-scalar-to-fail.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1srd8nb9llg9gyc27qm6z66d8pbnmix9zr71b8yfvpzk59qp49ii")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-203-add-scalar-to-fail.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rqh5kprhm03raq451gp2q11is0zkyjm0yxak26iw1148557kz8r")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-203-add-scalar-to-fail.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "161dba1aji3ns5n1yhl9svi86p72238jdjdglpnc1dnvqg5zqffq")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-203-add-scalar-to-fail.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ncxjlp1h3cmvxlfcpj4pc3qhrjlnn9z56gxn2z687l084mxvv13")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-203-add-scalar-to-fail.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0q3gqmk4y90l4r1ddh5j8v1ax3j1a17bxn8qhrh732j81h7wlygc")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gihlnk9sy8jwk23smcxyz3khghqaisbnagqvm5209psawia6byx")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-206-match-with-empty-arrays.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0z2h6kddw80g3zd2xr5sqh4412nir44yy2nkzkgfm110vrmm3gnv")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12x3q79y1vzx2g8qf95ccgb0fg81vpsl08j6m50lqwy26hm3vmsx")))

(define-public crate-air-interpreter-wasm-0.19 (crate (name "air-interpreter-wasm") (vers "0.19.0-print-mem-grow.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14i1wq7dcs00p048rydc946l0rn93qkhjbs1ba5049dh4nwwbi8r")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bump-version.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "034vmcjks6ynjq9azk6zdf8nyg9nhiwl6y5s0ys9z0v3xfwk9hdz")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jdnrncma597sxhvarla1lpgl8la86qdmxdgk8q46mc86bcxklfh")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-marine-web.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0i4slwpx5awln280gcb31qbcwh1rhj5rkjxgn8y6z0c2qrj87zcz")))

(define-public crate-air-interpreter-wasm-0.17 (crate (name "air-interpreter-wasm") (vers "0.17.0-marine-web.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jvx6n4bqql2q1krhz9lrr1ryknxms6siv0q57mz9n3lavl4rk7y")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hzlqk53vv331d3pscjb3qmrqnzfp6ak2pqjsby3rgfag7diq4n0")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kygalbi30gbn491l1a0icnkifszmh0yv5v3ja2zgc0jk84ksb4w")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-204-incompatible-states-in-trace-handler.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1iqzy0qd12l0kv0044n4wbjfisxxafvxmvfmlzkc9cbiswfl281y")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-update-faas.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lc2c2ac8g2rk951jmj9d1rbxa4lw5s2jw39fbgnx37jkmafxaqi")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-update-faas.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zwai1mz9yd2jvqy77s6in13ck9nfifia5q88yv4qw40l23l30r5")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-update-faas.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04k1h9f4gncpk6dd9b61qg96jpmqnsfq1qcl1hd18brppadsn4h2")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rfiblrrpjwrw8j7aa74q0sx6la6lgnh3i99jw7x18pyzsbcsagx")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-debug-version-of-avm.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bx3llc1ayrdlgg81hdcvlvxy4mpl72ap519x8c7xpvsg0nwi6lc")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web-adapted.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m2gckiwxqwzksmhhsiccck36zzzk00rnwji1i7hviv0vya7q1fw")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-211-implement-scalars-in-lambdas-for-streams.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1d48w0j6xndf31iflivm42jzaq5h1jcawn9fqz6x05b001qspfam")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web-adapted.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16g1jnlxdxs95pllh6viljdk5llh52bacqpazs5w94x41zw7k2w3")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-211-implement-scalars-in-lambdas-for-streams.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1y0kxrqms3q4drh1fxx0ywvxf33apy9gffbl173mfgyi7iq7ssv8")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-211-implement-scalars-in-lambdas-for-streams.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hlyprspjznn14ygajvwd1icy51rmjja8k1vznyld17fcgp4yvka")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qnbyc4mrb53kz0ii3zjka6ryzagwday9h4crmcqafrapyvdhdgr")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xm2z5qshax13wf4alqp4xxpmbya8lx82cbhzza45zpw96q3cr20")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xf25hv063qpxm9ildx470xkvmzr5dqjb02pg04hqnmkq53wl4qm")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-with-no-mem-limits.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qnxw0s3x35xdjxya97sqz558vja1872sm7rz28vqymd377vr2ih")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1j2pbr6rv3892njrc8wav1wazq9id6p736ggxwsqi4np7dyygv86")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0in7ri23sph3y1w8wxzz0ri4vs9i85rrw3hsl9k5cc9swlva8cjb")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0q742jsdjj4v8whslzr3xawlwvqrb1zgc772n80k8wacjrrkcwbx")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-214-non-join-errors-in-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0n9qc4pa9rhmqvlcx48p0r275i3glyhvm3r0aa9dgnp6x1smh4c4")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-214-non-join-errors-in-data.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16qvx47ii5ld1830cznky1a7jvqv3zrfmpg6178mq7h5dpdwvbz8")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-214-non-join-errors-in-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1svpdydqvfzqxyfvmxdqxldwf8zg9g9g8ydyy36683f6pbpkmf61")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-214-non-join-errors-in-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ypj858vqicasvkazb7i0b6cr32953vyrdh5xir22c0ws846kh3w")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0l589f2qscr8dsd5vdwflxl0bn8ic7bpsllxbfa2c4nyzb4793f3")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-216-fix-ap-data-handling.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l8921xznalxv925s30jqc5xz27qjdkhs7jfbjmrjq1knj19q4ks")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bg40m3w9k2ffyqddn798ddg36qqhw1fss9sddhb9i0b8qafwlp0")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-check-lambda-scalars-in-validator.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "01iy4v0l5rxy9b3gq0i2nc3y6pnv43c396bv1sbq39glwcj78mb9")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-check-lambda-scalars-in-validator.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "065mqm6rbbjbwa151py11s16hcfc00y9yjz8994ry6p420ci3172")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-219-fix-tetraplets-json-path.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mf6rnn6mbga4276326rqwbyzc0zpnf1xvwhps6iasilv3hwi2p6")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-219-fix-tetraplets-json-path.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "141s68fb4ca74mi4cdd63krv6y1xd63lz13a8vhvb73icc3zaaif")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bf0a3qv15s9n0kk49hkz701sgwvvspk0qid7yh35wm3q2hv1gz5")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-219-fix-tetraplets-json-path.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yp8ysnhbk72j40y1x30l8phkh5gds0a5cqb3szs0mb0yy49n63d")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-219-fix-tetraplets-json-path.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16p2ci8j0y6nbyfjlb0i7b8dsyq0cqs0l3lrhavmif9inss18fk3")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-219-fix-tetraplets-json-path.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qrdkjskwppj41k7nk6m2xy83rd487xvdc9s6df51mbqdfvp2vn7")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-219-fix-tetraplets-json-path.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0swzwngpg495w5zkbl5dx66wbfpd2xaw64y0qkakbpab7mib6s43")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-219-fix-tetraplets-json-path.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jhkxwcd060wxdl3wwxyrgkgzdibyh2rza62l2r9kv97ncrpzc6m")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18r2mzyjw3flw4kj119arvqxyllryp5fgaxhj0qc07sm1aqrhpdb")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11f5mf7ypj47klycnz0x7dvghql6kgfzk9dv9qw2syrnzq5pxj7b")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-marine-web.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0p73b8kg4qgnxr0fi0vg13z3fdh7g2i3zgkpxz5481gpqzj0m0al")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06yy2rc0wzii0ni1vm3k9f56smm8x3i0c9zjkjardsxgvqlrx182")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-recursive-streams.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hrdbwj9hv5yxyzll6pq8r9f0kbal2wh02f1lm43jmpy9cvcfi13")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-recursive-streams.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10y265l1dcv0jxicx2470y2z99134rkz7mp0ls5bxg6zga1f0g2a")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-221-fix-ap-handler.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rahpj77s9gy9cka1w2ssxvliqcnhgbr4qpvir9qnvc3yrmdaj26")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-223-recursive-streams.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ssgk405dlh6dbb802qhn61zyjb1l87gb8bw4dp3ddxbrwzi3h48")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-bug-221-fix-ap-handler.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18qb1z5jp44vivcgaf9xzwddbzac5la2b6k8l07mlxx0snmmsdfz")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nbg9jw1bnqk0nc23z3sba229fzwpxzc4wkyq8v8bihmxq0k6rqm")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-223-recursive-streams.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vnm9mc63by455l5jpkis8rmi4k9k3qksshfl58a2hhlfkdvg293")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-223-recursive-streams.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0gkbyrq2xq785l4jcid2f0p4663di04i7lvjw2zrbvagbfcmbldx")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-223-recursive-streams.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16l8kh0zlcsvr3ayf0n3218igziayjqcqds84d9x9dbnpc76i5vn")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-223-recursive-streams.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sym1r37vb4n3myhb5cck6wfijz0r8bz25bwgf5rgqfm7w080aw7")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.0-feat-223-recursive-streams.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xgkm95b02ir6gaikf6idpamgnicgqnwsvg6j9br2p9klm7242i9")))

(define-public crate-air-interpreter-wasm-0.20 (crate (name "air-interpreter-wasm") (vers "0.20.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ydfxi125r1sjzgg3117q20x4aapb7cx2qfmjjwjfvjadql0ddkl")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-bump-version-21.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1x9k3q5rp3f097iy8yvm5l04hhlwa1j4r0qwc01j4yck53p3cmkh")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16qpqlx0mbjx4ih2wkbspdqxj20iyn859cbzd8ghksvbfqlrdqr4")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-remove-refcell.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0q0mfnw044k47gfv2511g5xqn6y6ndbxcmcjvax7b1zszpgwqa02")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-remove-refcell.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1704w4s4ayqmhj42p3hqsp51ymfnmb2kjj9gs4jb9zkw4wfaz60n")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zkngyyw0hf5gj6w2qpcsyrlmljxq0xh5cvh4ixqgn3gf1qzqj79")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-feat-deterministic-async-aquavm.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0k3h2ll174nbfn18qp9fnh6ganbridddpasi6z5x22aq5rmm710c")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-feat-229-iterate-over-empty-literal-array.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1j33cq59mmfjgkhbs9bdsic3av3rf430m3l328bbrgv7i06cqj4h")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-feat-229-iterate-over-empty-literal-array.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0y4hl2xczffkpvvqic1rbimk7rqpm9p30dz8zpw7qzmgaf86rhl0")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0x81wv5p7qhx3xwap53a19v2hf5cvi7d773m0lhlv26lym4sfsgs")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-test-refactoring.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "160bhf7x8wlskl6wbbfc8lwkyzp54362nr85v44nh647555xszs8")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-test-refactoring.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1f7yc61yicqyj9c06dfsr5fdbhlw7jz1z2zv7m5ncpcbcvb740x8")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-test-refactoring.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "00cfxci13jvkhhx0zc3l9vbx4yn2mqyfhxrnyb89x888dpkjwhbl")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-test-refactoring.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1g3rv5sg9zzs9lgilnqw0pxhpz4y4k2y15szkrs8d1l6npav4fkg")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-feat-deterministic-async-aquavm.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07m2f65za4gslhcmjh0n6mnryhmb6l3gqh4c5paw4viwmw4nmfqz")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-feat-deterministic-async-aquavm.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1d4b1h7kxyynjlmwm7r3crf4kynqjpvmpn20z75hzjq482j5wrvk")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09j122xrbm1kdjazz1k42zkj8b93q7z25k8d443a4xjh77r1dkm3")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-test-refactoring.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1x5jmil545rmqqs59p84hwd2wjq5v46nydv28gzx4f1q0pnk95ms")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-test-refactoring.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lsk2d3944441x3f76ml5rwqk0235drd672bj89alca3f1yrf83w")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-test-refactoring.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gd67c6i9x7abg7mpddfixkfs0wg9k9adwy3b8xbvyrkmy6g8qca")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m145lzq9sql9g80nlicdzxcbcn9371i5b7p0a71wn17j5xg5kxr")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-next-hardfork.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0y1zcm677hwv9fi7k09ds81xxiwcwb11p4jn7qxqrb15ljs52w5v")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0kxqhcv3wgk8jpvzjmk2kj6373cgrnkixcasank9i4r7g6fnfrkm")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-update-ci-rust-to-2022-02-24.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1907gnaijnm77pp6lv3y0pyf6qqzwvihzrs57jqcqwghc2nmwsji")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rj8w9b5g4pr3s686br0bfdb36pvnrl4ans4cigbddhi33asmgcf")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-bump-faas-version.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17pzsnh1rcrdmvi2b8v7mvh93cilrb1xnnz39b0rqm4h977596jp")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-bump-faas-version.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w6b6j7pkwvrd735yv0r1l85qs73wqmib3zyxh9fvn3q8m6hka6r")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ixrc2xckgmwjji5bsagpldkb18jb7ahpjxi5s0ygcdqqhn0h10y")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18gfhr9d73vqbqppbmxadl2pmwqb7wjskb94rs6r0cf9n74brsv9")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1807diadkhra2hmz8w57yidar5mrmsi3qlyvkq2cz29gmqmkipk2")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-update-marine.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nrygsjp3m7ilg0dmhk116zb0lbcaljvxkz6d8kqlfhqd9rg48fw")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-update-marine.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f3z4qjnl5szm2259k9r7ypjhczi8qxskdzyhagjcpvngwhv39ks")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "01ccbvd8b9qc3k9d7c818rj3vszdmgw4g7akfa6p0d4lm0alyc1m")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-try-xlarge.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1m4silnm50psxlnvx2nxbmh9l4qrvqw7gmjbgmrvpv3l7kpq2i4n")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-try-xlarge.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08xvrzckk0ccdnh7ccgxflc3ldkj2ah5xnkj4j6vjjr3ayvb8ri5")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04n8si837mr6cjfp97ki4rdqvgfzi3cn75fhpaynp359pd2cpmy1")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-expose-max-memory-size.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1skzcmnyi8bsz5v2mdqy1p9cws8g58whv1n7yasv90vbkprd11k0")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-try-xlarge.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1n6vigvqlwa2qq57bap0q776dmdr4z0b32rcrkx56ilz3rhl9y51")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-try-xlarge.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dnj5ck66pf388yd4iygg3dndbfp29vr6fz9y5ddwb8xk0nhpdd1")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-try-xlarge.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zmbrfzxv3qia4vdl0cqg963kz85yjlzbpr669kqf3s7f2kmd2gh")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-try-xlarge.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "197wjj2rcs3gq2nmzwfp31nabphzskgfr8s5kib9vz30a6xgig2j")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-try-xlarge.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1p6nnvz8cvlzqa69p4shhmvrg85mv9b4s2prjpasq23f438xjdpk")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yxnihhkkn6iygi7lrn6920bz25fhbqmckk1kwd03lg0yiv3kh2k")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ymdnsym3rj6lqqxq3ddxr9arz06riffklycnwk3wji4fwnblpf8")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0kax8f0vzq6jqjgvngr9xncknalb8ka4qk90mzkiq9xfy5782c3g")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bnmxj50h0b4dbzpm05nbh9kqncfgr8yrx7yjwqdzcwpgzm15sh6")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-bug-memory-out-of-bounds.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08kdrgyfmd9cg7axnr0pxls06hs8p46a56y01alygbkz0jl4ck2x")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-bug-241-clean-scalars-at-the-end-of-fold.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1la44rc6n5r9p2kgh3wwx7wdzzsbv6afx41qyircxvnjpn3jmm59")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-bug-241-clean-scalars-at-the-end-of-fold.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nlb5r2khadnibif1fi66ydqqpcdd66pl030flhialavfb19i15h")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-bug-241-clean-scalars-at-the-end-of-fold.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wrc3215hx58fgbyzkpnp308sfk92yn2cyvvrqg1scv6g4x27kkx")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-bug-241-clean-scalars-at-the-end-of-fold.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0v3nzssn3n54bxgmk00m4cli0aqzjxklg3nracs9gwjmmpd8cp5s")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-bug-memory-out-of-bounds.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "170jza2bp0psvja6v6gj7d2z7i6bcmsh3bwd38756n8h1d2f7lwb")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-feat-set-stack-limit-to-50-mb.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l91185xkdv2ldq5inc380gknd5scpbbi23rh3pjk08lg6alik2c")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1319insg2gc1dyxd6s8kksivnzk5phlcy8cw8ckjbgkpfy52lpb8")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pnw24hvvj0ihaszkihh3sasfmksjlpj3mabcc463vljf748yw9f")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-bug-241-clean-scalars-at-the-end-of-fold.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14mi287lkafpqybmixdsapqqs7fdlnafc7z2x38hx81wsz1zh2bz")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-bug-241-clean-scalars-at-the-end-of-fold.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0b9kfw3cshrqhf4igjsl4wwnivf2qq7lsvvx08v67f4f62szw392")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-bug-241-clean-scalars-at-the-end-of-fold.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1x02sd1q33zb8228d512a0150jkcdp750a981ppd70iikjkvqz4r")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-bug-241-clean-scalars-at-the-end-of-fold.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "189hphrj4q9505c9qpaw05a8rrprwl64qhr0hdad1vi3j42bwwv0")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16da776s3f7dk7w4g95fv491idyzj2dlpgzb1nv8i0xa49lfm5m1")))

(define-public crate-air-interpreter-wasm-0.21 (crate (name "air-interpreter-wasm") (vers "0.21.0-marine-js-stage-2.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zd0l1mb4y216xsp4wdvzk8173zk8wqg65id05fv1znnjx218hi6")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1i0w09hdj50pn1nhrra1psl6jvcrxmn3wlr1ir2k5x1vxvzsxz9z")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-241-introduce-new-for-scalars.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jw8pqslwsnb2psmgazh2s3r3lx2zms4lgbpgfpih1hdaz96n4c9")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-241-introduce-new-for-scalars.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lvyjc9gx7k893slbqk9bxfv2x8rhbbg8sswmn7hjcmajp0vzfw7")))

(define-public crate-air-interpreter-wasm-0.22 (crate (name "air-interpreter-wasm") (vers "0.22.0-feat-249-introduce-timestamp.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19n00sncsvrb7vl6sdmgpqyyl4b6i2rsv42486kirv3yp9qi31aj")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-241-introduce-new-for-scalars.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hwmcibvf9d64ph1ip08m7rwrs5dv47zx96b04gsgyabvpnfhnbq")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-241-introduce-new-for-scalars.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lbxihbc6vqa3bl73v5hwx8x6xgkx7f5dbd014zfa4k99knv733z")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jk26i0i88f0zqjjqmamy3phsgvvf8ac0r62q8wjggnqrn8gf32v")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-249-introduce-timestamp.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fd4yl0psn79pdwr1z2944kgdxx8pff7ivwns082ww51xczbbl84")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-249-introduce-timestamp.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gcbli67icyqc0s4dy0v857nw0339789hq0l1hwnh5prsm0ksyhl")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0zi92jnsfrwgk56jvswl3r760zkdk3k7lpzqxji6iz3d4dxxbhpw")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-249-introduce-timestamp.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1was25plcl2jfs1xbskhwi9b4irl1f08mzr0avny5yva735ndb0f")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-249-introduce-timestamp.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qj3scwamg7nq46ybhd6d3nra3bad40qpcp18zxrzzh6q7wfj3dq")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wvbrny2nzdv8n1qrcj5a33d6l50q6za8nhvjg7wfpna0fgm0s20")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-improve-scope-error-handling.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hrhfyv65fb63a16vv4mb39lqpmycasvvf9jrplkghas78jywfpy")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-252-add-support-of-ttl.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wbch15y0prxhiirzvl14f8k1lashyyff4wvv2r1lgplkbp8207v")))

(define-public crate-air-interpreter-wasm-0.23 (crate (name "air-interpreter-wasm") (vers "0.23.0-feat-252-add-support-of-ttl.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1in6v014sz9x4ndnksrxagacixkzx25jgmn8393iaw1ydqqh5rya")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-252-add-support-of-ttl.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bllbq7vjwn4iw18rh4gxh736kx5yqdnasgpiw2lzxql6kj3s19z")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ypsp623nyj4vvg9986bh3pxxvl3dnfy9qc01wwh9d23hfy4knvv")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-improve-scope-error-handling.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "004jr4zgpzdlhrn9q5qvn07fclqf5lcrhal1vc2mkc8c8ag76c42")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nvhx2li5ripxqdyd0w2pf9vywp6hv28aixmqj699g9m4mljh36k")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-misc-add-developer-notes.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1cnwyl0jwbpqipb9d05y7bp7gcs4h1p7dkm77fslvpwz9vihyw16")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-misc-add-developer-notes.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0p7dcj209h3f4lr8q47avx9lilcsrv7l0qgv8cmidgz9qnwbsav1")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-ttl-in-js.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rpfq8y8ppi36a5vdrkmm45sgwcssmmdamv4q5chb42j5273p2y6")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0v6bxyy2x8467ck23a140qv1h7xjbnfd5g6qv0cyhpq132spxjk3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-faas.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "030w5s7lcwcd9fvhl2fxpparkzv80svpnibyg7gh1yh4sfqc5nfq")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-faas.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mb6a8l0zam550hj6afpmzaqkqwmnd48d76cxx2psg0jmplfqq9f")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-hotfix.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mfjgdz1vm0c7bfpvhqg50vp38jpgbw1yhhaqpfl4iawz7jmzybi")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vzc50jpw07ddqvcwy1as09dc81vr3bxnb064asif7q6m3iyns6c")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pk5xy85pqir0cv1g2kq8fvmz6gxji44zgjslcsam5lw9m2wcis5")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-faas-again.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wb5zhxzcm282rppzy2jrz67c8sc6549vr5abj23cbydp8dz9416")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jmca1amh10vra310bpwwkpbj4vsl8my6p56vlyayr9vq3b7z113")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l23r4419a9bq303hqraaw22dqlsflkcjf04yjpjg4v0hm2v6gb7")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0r5qpjaabkhcgm5dv2g2ngfc4fx6jz64m608q2avwryaqrqgblwi")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "01d0awh8alr8qmpacb18rcja3l0iy0526rmcprgac02108jrlh3a")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sb4i7ns69hkwcig2wjvik4lwbnz9y6prxmdq0rqqh14vyaxvz38")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wcdxs63m9mlwpv9x8jl04licm2nksq2ygn79rmilfa8xgfm5dl9")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rjhg6n745q321vyvccybpz9jky0lyg2x46w1jkcl9bi2c4cvnh2")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18y0p87f4zsjg1kalyafa2kk4wxq34z38dhbsv12hfq6ryr5dkv1")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yp4z4x2agsjmmmabw3a3a7nbxwm615az8fgj8ygdyvmskm7pbp5")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lqrm1dh0va35d320dsqgavcfsri4d1bp87g4f0sq8gzycg4c8f1")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "110my3zxqqygfxkmcx3djjxf2kxvrqz510mw5qk6wr65lhw5wcv0")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-faster-tests.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06bl07qj59wjwvwihb9jagbpcnizw5rg0p3vpcf0jjf6nphjdxiy")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-faster-tests.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gi9pzzzq5l4ihfvpjc4526qslxba3gb6jp86dd4cfv8y32ssz9i")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-edit-readme.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rhic2qn5bznd6hpamxi1py9nnqf68k0kpkphzr73qfjrggirsaz")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-faster-tests.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "03qvfsx35i07xhgygzi1alm9pav5knidhsvxi049akhvy0fzcl57")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-faster-tests.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wkw1kbgsj1642h9r7jn1jszxblqiyqnpk7wf96cmnxlm98517r3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-edit-readme.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16j08jwp0gblmbrpagxcnx0aiqr1szcpjf40winjhlsyy9ci3h7n")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-edit-readme.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0894sx5pmjlk18c6k4cw78qp483wqqyzlxz86dqhv7zggb5940m7")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-edit-readme.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "01is2kn0rdhm7w9lncivmd2gxz39a0fb1id2x7qgjh27y1rgxsid")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-edit-readme.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06cli93i4f6w272kc5g9fjl5wxj2kg2fh1s0lxgrwm7zs1fv51lh")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-readme.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0c08wykn53qnyyyx2zcvrwp1zkqr01gk22x7f4q24j09b3cfd98r")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vnl6nx3l0b4pchbb82v0dpa2iqcc275zy525pp054rb9dnixs22")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-faster-tests.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0j6fdhvqnjyjgd9g307vxj12lsq0qfp0rpdrh1smqq8qm8pwf9fc")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bahbgl6868376nw6m0xs93c15jcjl0hic454kbk57cjxhxbpksx")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-263-last-error-lambda-fix.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bixs77whd1126nk292gcp77pla1585dyk6gl4ibd912cqrz72bn")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-rename-subtree.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0b5f5cr39fywphij9907b1pbazqsymhdwrihzwfgazvk5gxv9fw6")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-rename-subtree.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1dk3hizz5z53i1p0i43c7s99rqh3gql5g7xzgylw365r73bx2h0m")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-263-last-error-lambda-fix.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rf7iwrkrgawg275g59qnlck7ww8j66swk0l014asw9mbfbgvca0")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-263-last-error-lambda-fix.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ijwgya8dai9v74y143fy2lhlnlywiil4wjrbh2avmcx5m9kcl8f")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-263-last-error-lambda-fix.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "18pqr2w054x43dfw4wcp0gpw1ggl1pj2n965zlbqf4xs8rvjqv50")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08v1q8j5ja4wddyd4z701wbywbfm1hs14bdhj05dygqjs0d4srlb")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0pq4lk5sz33lyqd7vh394xlfhx7ll82lhvphnivfypfymbx13v4c")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wabj4l570bzcqk3yi9ah62243sxzx40whglmlqizwjvmwhasm33")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0agl7f5d6xm0rqvfvdacpkljgckc7an6p8bl58dbykd79qngnwgp")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vm14la9r58b60icgn4m49hhbqq8jb176gyh3pijbp1zswybc4b2")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bnync1h0rsknp2lwyhyz065gjf3hif3gfhrp6xzyinyj7871wwy")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-style-guide-docs.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w3gyp6v6ch5hgm45vmwlymf2llxfgsaw213jik76139cpihmpm0")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1yksvrn6cikl5qq5nz53pzsnz9ls918d90rwkb4flvar142ihd7v")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1izsyxfgzkxjfshjlcc9knhjqv48vn94g7vqqn8kwj8aaq8kq3k0")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zpki3cahzdyqpcxvdh5g9zjfiy7ml6cqkbfncnslz9xy2drcqdb")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12hgcmbl1al03brs58mxzc2j6xkg2vx9r5ayrdkmlfb0iw0j99cg")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04w34c8f81vsdh6z0b8177amwd25v5g5qnkjkdm7fhnl4sfpj98d")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0k058nzbzm8im6zgpw93ig9vl5mdyq0cm1yzrih1wmbr8m17pjr5")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-267-call-triplet-refactoring.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fqk93ianh2c9cj05p55dd0dmm770a7zicwwpif4k0n5i0nc0h67")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-267-call-triplet-refactoring.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "04c8f1g3al4xhgsxzf2yyd6vg9xykdn9vjvmxfvl8s3vqv3idlpz")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vx9yixblvnypyzvbds17qw48qja6zmvjz33bl02fwsbrv5qz0zl")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fkbsl2nqnwaajkxcchwcx2rxi4rba94mfx8lkhc58mgsfwapfyf")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16yrl9g15ygz05bzhavq5za71lig0chfkakwqjprpvyn14im2ssr")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1l9r0r5wf1gql6fnc0jbiv81ng2c1nrxvk0cm53hm85vkbxikph8")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wymgca0cjgz4gbfmp8hq9qdfk71kl305hwf5yvplb93cfhjcl30")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1g1las4fbvjfn68a80j3n4kw9hlwlyjzj766l2qmvm4kpi1l59ia")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-184-air-beautifier.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1z3nj8rcp6kgm60pyn9lna0b7mm8fbljkfjzfl4hj27b3p6wq6sh")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05n11zspvxpi8gbpx0sp1ird1gwbs55414hhzrgsjs89sjmjd1ak")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0793daycv7zfh5v4divmb06z6x0l4i5qa75crzsgvyrz0n9r0j8r")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09njw9qrxzf6n99p3kvvzd2wly2lr9q0cfdfqs3qbml6r4bxl8kd")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0psdk0kc2ryc4l3dqqi8f8jb6i1l5dkn8dzw1y8zwrj7358c0iwr")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lp6zr4qlryrpx7pc8v8rnlxsfwgbk1556lqfibhpzxph72c8b07")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0y7xvx1q4s6h874zi830ijqzm098xgvgpvg4misrcx951fjcy6r3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-make-clippy-happier-20220531.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jkj2949h88x6jqm52wv9gbr5fbkf0nhb7dg463af4g3rinynp8f")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1f0zlrrajicsqqrvn8h243kg87gglmax5c5f3bdygzw1176x1pqj")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jsq7hz0fpszl6f71327zchj0nx7fi15mf55d0ivq7qi7d9larq3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1188x1ibx2hxp77zjz518s5694lq44jpngjgraigjh6ncrksx8d1")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-222-possibly-caused-by.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17g036bk7dkr7hjw0jjkmkdcpzappilp1x4g6c8zps86hp5vgia8")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hawcrj688aa7n5m7488axc64994mh8y80xsmqqs3qv2sxbrj7a9")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-refactor-272-trace-pos-type.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mwx3b4n07j1p9mjj1ph5qz2v6lss5k8bf1acp43i99c0l4z7d3l")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-refactor-272-trace-pos-type.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1i8jc944ckjksk42sl4drvqb5jhphnymihrl707w42x6s2sgzs90")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-tests-247-sanitizers.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06hvh1asfc45d6nyl5l2mgbfx1hlvf041zn9zra0knzk0vls13z8")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-tests-247-sanitizers.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hlb67kc1wxq7614hpc5z4cnjfyqkr4in1x4spq6b3nyr5bizirx")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-tests-247-sanitizers.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0n48zw52kpg018fkahkynbsvsy4xs37vcvp6n13jg50avz76h4id")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-tests-247-sanitizers.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14fswlq42lyprh2a4b6zq0k3p7xgpwd6y3vgbh7vnwkani4klpng")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-tests-247-sanitizers.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0na0n3m8ncgslh6r6sk42aaskin51zdwqmxjg5bylxi7g94x8c7i")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-refactor-272-trace-pos-type.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02p5sad3h2xzf8bq8xw4shich1nbijmgyzmcncak586lb0lvkvjm")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-tests-247-sanitizers.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wh5nz5cmy4q99wwkqj84vqha90hx39cg6cl6dwg1wlj4xvvbic9")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-refactor-272-trace-pos-type.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fd1jv3kx2faq8d827qagdj2x334gyp1ygvxpd00y4jr4zzq6q6s")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.15") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0n82kg0bbhyas32rlrb6f7gyg0m9wr62lrf123gwmxndmwsafv44")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-VM-104-performance.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "00m3l29m2lykklis55y7j8y6vcsrkxgc98h0qhqinmzigfr75f2s")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-VM-104-performance.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ibwzz0hlk1wa12fxdh92hrd7irh677p40g572cxrr3mli5bbrmp")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.16") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ml6rs4sdmx732zrczs9b8jc8axr1ss6hddhaw9yj9lgi680gybd")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0z03cxzs8s04c0m25gxmycg5gm8l78whrkw1x90rzm4w0p13sb0v")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19bpydm8vgrcjnxyw9z2msw8bv1hvhlkhixq1h6yjk951s9a75mb")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1939pi52smiskg0kzvwza8vsg2ccjrqb60vwmw3dxx4653j7kz71")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1sq73prc3l1v5bq69wz6lli3wsybfdz5xrlg3xdwn1wp58fsk65m")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ms4a7vzb78phc0yhswvyrdjgn0cixnmy7jj8b7j97f8b0x3xfg7")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rid53lg5rg7432w23fz0qjxp3647l840c7qili0ncx8l1snjdz1")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1965h4d0xs1vw4qimyazaprna7yiyxjriasx6myb7napcja4ch31")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-114-add-api-to-save-spikes.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1cwjx994ispffar0hwng3i337pcr8cgnsypa3kh42x0fnyz1kakv")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.17") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10rmhx6xnhig4lb6gmklfhcwpdll1c6kgil4a1i8g54ch1hbql4p")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.18") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1a835jd0vcarl69lbj1qbk5bhlzc3c128vcq3j3zzf2l6mjvg6i2")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-fix-avm-serde.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1faxfi12yygy1yra9f7gwzqdfl0i3msrsi6m271zqvayh126aj0i")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ccck67kwgfhx33j8v308725hd8wf9587r98x0dqcg3i53kvfrmp")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mqabricsq3kxlkhjqmdygxm6hkbn4qkb46gl5szxadl8zjm1iic")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dlgy1lwyagk6nmf9sibhp365qwvy3yyi3az2v1sljqa45kfrj3v")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1b6mffa5alz67x16nza08aai0d1wf5hbrqkxqvv0hc4gk3143lz9")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0jna3b220027f4l32qm2cybqc6hgg6i2mcx61711pflfdn5rs8x1")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fnw1ry62ysxy8hfcfh926kwx5l9gc2imknj2gpvqhma2sjry29x")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lqvmph3zg42abzizrx4l9dzrf66zkw6m1n03vbp8lhwk2pa6im5")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-update-style-guide-docs.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14l6i27dxpm367msxmpfc24zjj8mlvir2zx0ck6jlrpp86sp2gxw")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.19") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06zxc93i2nih3ajmqmcpjlk1injbya1vzaznhisa5gcqny1gnj36")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bs5zwnfdd3f05s1b9dbflfdmc9fnyvwjkhjbjkyxc4kzqv21wq4")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-VM-117-air-script-to-anomaly-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "024gm1a8fqp8agdcd21ajbzbmacjip4cksnp4wkz0hbw5xlcn9vg")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07fjh8qn1cdk6rzch3v2s7bhha249rlnc61s8wvf1sb79byi0h36")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hxpg0mjalbyrf2mh878f9y6bnypxzlf1ip9xq7l266cbsj3syms")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-VM-117-air-script-to-anomaly-data.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w7h7hfz26253frsp2xyg99ds253zaf8dw2f7b3x3qh32mf0lc71")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-VM-117-air-script-to-anomaly-data.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wsydyvd5jcnrdbny22dswsh7a5zddy538wkhnyzvv5ai1w1rc8x")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bw0s7yrvw7z32a9cvfidxav19c9ypsrp7yzacqzx0kdrhlhfgp2")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-VM-117-air-script-to-anomaly-data.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0g28kzplvr89g9s8igizyzv7yqi82xnjkmpjn63jykjqjd9h68vs")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zds8psbv1l2627panr55gag0rcwba93q3ah8rpih7q6vw1z4xn3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14k3k907532ywim3hclgcmiqv3by2s30z8k29l35jffhpmn7m1jw")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.20") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1a3xljm22dlcwm4881xvgaklb9ls1pcdpijv5zkfbdry4xnl3wfj")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11nldxrhksvw5iksmp5lh1ri5ciapr9dl53v657dd2qpa38pc0i4")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0imjbk2glk7xs2x7vb2yz76s16gxa6knvk1dq9ykmyav2r198vz2")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0g4mc9gfhfybwslmjylszkagm0qqk2r165rqdmkr56n1v0mgxik8")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lnys3j8vy3q1zwmw46lfi2b1ngvm4p1csbz90pv0ci6a52cz0ns")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1prb2ynsikw81ryawppch1bxlkml4qvf5lbafh0ly73j8dfg9db4")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qra25ab6402spbkdq9w5wbabzgn5h42l0vw5wkc6j590yf9rd5p")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0daha0x9xpqf1n3iiaq6ql6p2vrnqy0kz1pjalacd19xmg9kfnk3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lxrwcn71h86y2v7dva7hfv1i6a9lwr5n0y4rjx681ix55h72r91")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.21") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1h5dnzsa2wpsavwwdy9ggc42irkb1h6y96597i9xdlv9gcwg9hlh")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-circleci-to-github-runners.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rhkj4c9rnys2jigi24kbxnpr3ic0qrmz7cl33kjgy183fbsajd4")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ay2dl6sv30yr0w38mc4msxibr7cndvrgzwvyxim2v1yk7297vbk")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08pmgxcw0fxznvq016h43563mx36ag3j9h0f4kaihsa9lc52px26")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0124nqnf96rg8bmmnyryb9jpd151nd9h4dni5lgldc4106hrwmd2")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lkvcnbfgj30ipkjqn6y7sai4agn8xq43vc4kaz1lnzxiydc849a")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14v43dscsj7yk4wbzcmwrgnjh87bwlprkb553p1l7xpqwy6pl481")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-performance-tracing.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14p3za9hgp9x1bi2vgb090729y592n1rcrxyi77m8gb66yfrm02g")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.22") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rs6myi7s7a4jq0067621brq7xprckfxabb44kilbb9c3d47gx7w")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-benchmarks.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08s62cfh968gi3r5laq42i6cakmxkzsp1ifv4xpdxranl4kag9sh")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ww7j7ac15mpngf2yl9d4wy8r3ajx1fh3hbvapvr8a1k267ixzp3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.23") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14pxpwravapvkmfqvc525al94y448l23ml7j0dkbd4bna7rbr2di")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.24") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lacysjpqsiglifz850yb4fb3cajzn5ifaakdhhzd3y6iilfpp1z")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0sy0k0gzcpwl716bh50arbcp58ks291g65c5kk7rp13r4pinjrqa")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gfzv1zxwzsc9xfvkqlw3vp3v48pk9iyhwf1s2r2al45a8bg7qf7")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pphga5jb3a8yg0k2l01yl9qfb55qb2f38liw9ihq3vzbvyw1w1v")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-tracing-benches.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yl1lffmrnrxirjx9q8bq81bk9lvdi3bk46l8alpvb67ywg32281")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0074g40227jnb07appwiq9c4lvrca6k1sw5k3d71gj4pgf4an62d")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0751v7xvvx624vxlmyqdsxn0i546zg4fa6wcn26aqhd1asffyyyj")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0cm5h46p1zx6xrwcffcbfdlgpbgqj2w2dda4i99r5cwzfcq1j1jd")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1m75x8ibz3ngvc2yka2354yfqk8vic6cqxp2h7j1d50wj7ckn4d7")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fcd5v953p6kf89mfzipa67fhg6xpymh4w2ksi08zvsvfpyg4rg6")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-doc-air-trace.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0h12ka3kvyqmzc8701jzqx2rxhngf87hv1030jqgvp5haigmsc8x")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.25") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rrzjslk8gizw3h1l9fpjysa3cjigm8vi8pv70xsn24nqm914rz4")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-feat-VM-113-tracing-benches.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0h2kxvkrnf9fx6zpxfwawmx847xgf2b5a359dk3mcqimar9848qf")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.26") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pw55lwdya8fvmzdz24fydx8x9bs75sj00z0zgvij0ajm54nhfqx")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-fix-unstable-docs.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0h3xfr6p5lgb5lgxyqv7913vz45r0dn7xy7h0y95q2i3pdswvl35")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xhbpdswdz74l6chx1987a95x9b8961d3yzblrlf82ysl8sz9097")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1bdgjraa4dmzr1kx5kpdfbqhlix7g1w08g0h0xp2dc833wjwrp4v")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xzzn0arhz2jdpz64vfr2pr5zb0pngnhx3qmqsg40bkxqd5g950g")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rg9ag42shcc3vmixhfg8ac8cy919bc1wnvy275ypn1gb8j0k9a3")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ab27y8rlafkmz76yijkchkcwzib5016r8xg3ajyha23r49cyifa")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0kfb4szq433daz0m5s0a2rfi1dj1cc70zyp63dvcgxl8rpv0rchf")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "132jazf8k3aicr4xrlif6d86rs7z5avbzx16i9cvd2amc578pb5z")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hhzbfzks6h67xn59jpsrskhzv5qivkhnaflzvij4fdv80pgr3ki")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gafnby9jf5495cc47wphdlzz1y01wn04r1qh554wp4rk6p96yqz")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "10kgqv8njixqz9dgn2zij8ansld649irdn80kfsyqjsf8v1qdbvp")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qbxw89r7861wsn37s8day9iysrwx4vrsgj421gbicbpr50jqc3n")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02v0nh8v3dx53ad7ykqf3h1g964jvq8jdr6vi3f7r8drc6wmwxph")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mcgmj8hxas1r90laywv4mizfxhf895zw1ysw2lh3q1zqm3j8728")))

(define-public crate-air-interpreter-wasm-0.24 (crate (name "air-interpreter-wasm") (vers "0.24.0-bug-wasm-memory-leak.15") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0v91zhizypn9ydrkly4f7qng7v37x98qj9x5l0vf2834z9fnkfgj")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-bug-wasm-memory-leak.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kw4vyf2wijg3zlq9pm7d2amkayj2ql3j145jvhgdk1810zhbiy8")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-bug-wasm-memory-leak.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09pjdwjcgszafqwm4dy85f54vzci744crs3a4ai1zllji0g8q7ja")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-bug-wasm-memory-leak.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jxsxxv7pkbw7h27n32npz93bsysrkgf21whpgl846y5h7xnsbfg")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-bug-wasm-memory-leak.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bzra06dj7jhqamhzf03j346dd6vbz8fxahk6ry3f48ypcxklnhs")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-bug-wasm-memory-leak.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ss4v96l88b3hgnimdxg2mm5ixhv0j06s1pvsrrj571xrki1qwsw")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f549y8qxjvx1q3q7hkvws25g1cphr92xkvzj3paq25jfnibhdg8")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-renovate-configure.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1r8szfrhs42d873f226ybw2l2s3j1hm2yqaryrnic2fl4kkk29ka")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-fix-VM-113-tracing-adjustment.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "040z42jbdg9sq1j612x3mkbd9yvg6ix3vcjbaz1cww6wbmbwa65p")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fbfxi0jzxpmchi4c6040ir8icg1rvh9x1lv35m2fc0lmqaycmbj")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-renovate-configure.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ynr27s40k5vxxkmsafffk5hqxizgjcvwvr0jylzxgy1qyyi8zzk")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-misc-remove-jsonpathlib-filter.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17008qip7w1gcp2ca682n0c8631wivkpicrmp5d2i1mqzm231gwa")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-misc-make-clippy-happy.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "16a1jnqn7ssw8sm2xjk6hscchxcc5a6qvd75sxsgrcqmm419n51l")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xff3nayq6m316jwww01qvlbds8k74jhbbwwp7pq53x3lqvm70s2")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14bl610mrlav6cyb65nvw219pk5z5bq6i52rd1yn3n2v8vw8qq2m")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zx7zbqq1vzw5y08liifimqx8w72jnr1rmi5v7a9y7snkmnm0i2f")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kxn3ikcfnc9wg7l1zwysn5jaw0z9hhadhksvyqd4j2nw6hjh961")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1hh1913sm59dg0pvrwdgsb6wajzjq4f8rab7lb7gxvcp1sgvb1hm")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0c1wk4rbfxs8b3bzg7s87hv4gi397dalydsvky22lhkn1bj88b0h")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "15n7np87m8vx2l7k9h77pg8n48q431flzpsm1dsqnc681xf37s5j")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1cbqksl0lrsrpm6svb0bjwmch8aaiar44zrcafavzprwl017667h")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-22-introduce-canon-instr.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0y07prgf1ddz7d53s61l4wra7bi374d2iyjkl57s5r2zpq89mxyz")))

(define-public crate-air-interpreter-wasm-0.26 (crate (name "air-interpreter-wasm") (vers "0.26.0-feat-VM-130-avm-server-interface.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "13p8nffnb6czkwmysmanvq5sx42dn7991pi9l04lcr073101fnhx")))

(define-public crate-air-interpreter-wasm-0.26 (crate (name "air-interpreter-wasm") (vers "0.26.0-feat-VM-130-avm-server-interface.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pb0jy7700485cybdh8r4q6cbg1nrmk7gbzh9y8z12ss9crr03g3")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-22-introduce-canon-instr.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ji47ciql2w4n8kh2xnrcqqmmijnc2p3yhjzqp9qnwmdl477zk84")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-22-introduce-canon-instr.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1crlzd6ppakj212n513m24r234mdhwr6vl2ghhk4bxfl7wwd126v")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-bug-VM-132-fix-ap-states-merging.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11w0k1zvb6w4mm8qcy5147i6sx72y20q59h04ij6kj4g07zmgfqq")))

(define-public crate-air-interpreter-wasm-0.26 (crate (name "air-interpreter-wasm") (vers "0.26.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yxmyjmsxarm16f3lpg7i4rz7a0wn83fbg4c095pr8j5fsby4pjr")))

(define-public crate-air-interpreter-wasm-0.26 (crate (name "air-interpreter-wasm") (vers "0.26.0-feat-VM-130-avm-server-interface.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11a6bszlb5zplw1ll2dsgs57d6f0vr030yj7wcy1d82fkfmhw1r0")))

(define-public crate-air-interpreter-wasm-0.26 (crate (name "air-interpreter-wasm") (vers "0.26.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08g5wz1ckwfpzacnjsfw6ig18hklbr0jr6jghf5lg3v6jb8g9cz0")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1w50wx5w5hrjlj52g1nfsyq5yk4xi3gffc2jvckdzbb5c0ipz24s")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-22-introduce-canon-instr.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1phgwqyxa8hjfc7s2aajnjmmg1w9zqzc642cy1my24536j59c8p5")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-22-introduce-canon-instr.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ls3k6q650g0wcnqsnf7jf9s1zk7v72k4pax8klr0pn1685dh2v7")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-22-introduce-canon-instr.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zc69w3p123nd8h451ybsijy8dak00m25apan96i078m8cdxyxbj")))

(define-public crate-air-interpreter-wasm-0.26 (crate (name "air-interpreter-wasm") (vers "0.26.0-feat-VM-22-introduce-canon-instr.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w0d6zd6l0iaa33zr1riirqr19rv6s6ah0hlb61pk1hn1k4m792b")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-22-introduce-canon-instr.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1ysm2if7369xc3hlr1sqlxgjnfq8ghnjcvpcmfv58l6fihi91zzs")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-22-introduce-canon-instr.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ing5nxcpiimlpph9h8n84a0rjdrhwbrb3sr967j0msslc9mx39q")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05qac6g8y9d49igka1j2pa7qadxp5iw25hyccry3hjv522rjz92g")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-22-introduce-canon-instr.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11wyflxi6s6qy5mwzgg76sww4klrih8i51hncpjr4vjmh6h6hwas")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-22-introduce-canon-instr.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1y3n0h2a3h0zyzlnzfi5cw6bwl39c8lh4k8dk5yqv9n4y1254l3n")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bgaxfm1dqvrxdr6wrh3kvx1hvavzd85c8ry7jmwv3f0a08a15nh")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-rename-clj-air.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09xjinx7np8ygbk1a52za0qs4rx56pcklrcanfhcb3128xmbg1fx")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "02gz10kb8n6vh0piqdf2dzari558ncx9g9c1q5lbmdjpi8cgayhz")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0c9vd9b0jpbcw88s7529vxj78728nkqqpci6b7qq7gmiax6z3glg")))

(define-public crate-air-interpreter-wasm-0.25 (crate (name "air-interpreter-wasm") (vers "0.25.0-feat-VM-7-testing-framework-part-1.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vn0iplnca1x52gdpsfl9xn1v5skcbf5qj94kkg3qyq6gcywlvlf")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0m3vf9aki5dvb19k2v1xp3vk8q07jyrk3zzjnqnsm6qphxav38v3")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "00lx56jg2c28gmpnvkwzalh8xyjanam2nkfcmkv6q05vc6x1q2hc")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-137-reduce-stream-usage-scope.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fhxq5bfd0n8v5cnm6clv3anvhjazk4qj3pya6fxdghi8dcw4vd3")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dbqhzig9py6lqmw7yjcsap4b4afab0q3s2yndp6dp2nllyqqjb5")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pki68jlhdhawl4r3g18109qp1s0zbz38n590q16qasn8b6jqgzx")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "070zlxa7npmbf6pim178xrm53kg4nslq2z1dn64i2iq1yybgba9j")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0i2kz38lwf7ijmglc5j73fk0iqb11irhacd4h32xmpc97vx5i3wf")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yqx9qjcafd21lgwczmpl8jrwgn86cgsdg9l31449cqclw1g0vgg")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pchwvcj582qg5r6gcyrgh4glfnywqvbyxw5c4jy6jvhfb5gwq2m")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05n6s2cr5k6pc252rx4hhgpavwbidgfn6lxpamghr9r3r04ngps5")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "19yvb251lxg96xmvjxqghvfpxaqp51cg9qk263wwiic6i16bx44b")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mj44ikh2wbr2nappv035zw009yrcjmnbqga7m8wx7mvv1br2mz9")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-7-testing-framework-part-1.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "08vch44s0r507ma8385cdi6zhglwji50hsfkz8lmvafcmwg5gfkh")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qv89hfh406lw1bhzg72sk234haf7mslqfwg6mzn5xcskhsa7s54")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bump-marine-runtime.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12s1ybz7sz629ihr3ym86wvkvajv92rmlnqnrnp5v7zyqrmc1wby")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bump-marine-runtime.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0r8lcq7imr2p3gkwnqr5lsdq0d5by5n963infbbl5in80xlmngj0")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xj1csgmnnk6nmi2a1ffd7jx8iwgcnynv94kxs3ysw4nxih3zz27")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-143-restricted-streams-generations.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vvxljszfbqk92vwcmpfixi76a7xi1kf0gn919jcm47cy3dmfy55")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-143-restricted-streams-generations.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0mhy17ax05320h7x8cd8il4fwdyiyw931s7nnmwsnw16jrw3784c")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-143-restricted-streams-generations.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1qv250l8kjy4gphpb4q41w94q4hb18ppwy6zi0x95fsh909b0gz9")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-144-restricted-streams-from-current-data.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f87skvvkgj7z0faa2l9hi9c0xxapjamkvn67mlsxysxpn3a13zn")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-143-restricted-streams-generations.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0d15hf3wfbivj4wp13wsidb518mrwz1qfyhb1igshcx58a3n8ixk")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-145-no-unsafe.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0afmqg77sadjv0k9vaq3y8vbd64cs458w66drmgwcgml4l00i4gz")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-143-restricted-streams-generations.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1h3mx08wxk9lxfmd8iav9drqyzgkk3xgc1354zs02kcwh7lyli2y")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11x7nd4jibn4jnp42sm00j1hwc28lzxh93di52a35qxcv5fmqpdz")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-304-canon-join-behavior.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0aw3xk7pp9w68mlibbfjphkd326n300g2i3zyxmdp3y09dsmcpy5")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0viv9ng1afp8fj8rpf6d5pkyg44ldyn2gi2xv916fyz5gam5dbwl")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-peek-on-empty-fold.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1gbzvb8rkagfra80hghm4s401cb25f7wysjwi7kifp027c48a9yr")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-146-fix-fold-with-empty-canon-stream.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lg1l3q7a7mi49nriah85i9r6s18kqdjpfvdqwqadrsh9agk92gd")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0dkqm7vbinjlpxcznxwfhkn5miaym3c79zk6p203ww8xnxpm5whb")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-pin-dependencies.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0p6xx1yjmi52q0zcz850yxpwn9fwblmw6m2zc8d6dg8bql26ihn3")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-all-non-major-rust-dependencies.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0n1xsjzxdv1wsqgx871rfppdzccv1wfngb5l1cdwvx406gqk83mb")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-304-canon-join-behavior.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "040xs7dsn5vqmz233zksqding41yld8xijr7drnph8b6j8383x6s")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xl0m7kn6p1sq48wlbrk665xf94kp76whm9dlbhqzfg2naiwgfpa")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-bug-VM-146-fix-fold-with-empty-canon-stream.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0l9di12b1f2szpwgq1zf4wk86rj5bxwmjs1x4w0cqp1nivvdhlfd")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-pin-dependencies.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12ryqc2p12qask9na6d04kqkyf9xmj3r4jywgmf6k7g8jajn047k")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1h1j19c83i5189rc4h5sz0h11mzwzs5ydnzmiq9xcn8n3bj6xy1y")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06p8wp9pj5rq0dad6n5clzns2ksl9ij458ynijdq9i3lhgj995f6")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-all-non-major-rust-dependencies.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1hxmb98kzzka5zgplz4p6xa8ixq1mw5njvzspksvi4zbl5k4h9i8")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-pin-dependencies.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fr6fw0b2k4apvxzkbx4b1riqas82bsjqa4ly9mwbw2d38dj3i39")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-exclude-wasm-bindgen.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12zx0q05jrdz757czvf5q97q8dsw6z3xpq8qwkqxhp47lrqk2paz")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-all-non-major-js-dependencies.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1lsvl0jq6lq0fs0vrz8pb0s3rpd3aa8hkc7z2n0n4x39k0lz1ra8")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-all-non-major-rust-dependencies.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1zkypqqz7nvykcdsbb5rq87rd3kwff43zxafm5rgc496rhk4vcds")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-all-non-major-js-dependencies.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07aaq0m50ylxl4ibmynpx2ama63302v7w42wrzgqf1d7zps0hi5n")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-no-undefined-data-in-js.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wnl2s23viwclv6x2j3fmjispjpxl651bkay2qf4vncv2znlk6x7")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-no-undefined-data-in-js.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1wn8ds7jbkslbh5flydghbk3makj15hfcdd91fd8qms4rjzcyqdh")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-141-introduce-length-functor.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0w7x1j31abc5ldbk2vb1ba4c8pvg8x8phqbhccv16ai3x0280h5y")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-141-introduce-length-functor.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "05ijk4zn3rzqfpb12gbvriy27s57dmnvgjfkhrqg0qhp0z8nya8d")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-141-introduce-length-functor.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1crnqkf9kmq9v8s94qwc39j6nyxz1pfrxbzp84qbrpd979s1dvhi")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-141-introduce-length-functor.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bncxz4r0hd5zffrr46bn9g295lsr77jkaywlk35ph56ikjlnajp")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fcpq9qmjqx862mhqvw4yrnbq00k9ifwcvxsxby0a4am96dh507l")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-update-wasm-bindgen.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0v8rx0lin08p231c8nzfsc7366cb6vkr0rdfqr3zphxhwccwnb61")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1n9sv4jz68xmx89brx3jcsn726vavdqv2nxv1waki46x5cd59wlg")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-all-non-major-rust-dependencies.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "06nz4vnygzarp6h5pcyz5r496wx5ck19qlzkwjv9i2mm77sbng0v")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-renovate-all-non-major-rust-dependencies.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0rix7l77zy0by075s1xxrmsl0qc6ssgfx1i1cx5qsmgaf25s7rmc")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0jhh7lzhz4jz30r7ql0nhzckmm85gy9qs1r27lcrpnmfs3qzrp4k")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1va3bw99cq6i4frax41wrwqgfwagc4w7zks9z79rb2xc16r4c5xw")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-141-introduce-length-functor.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1y4yrph32jqb99yakfz1rgh2yvbvgw7n1y43vbs4k6308367cqph")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-json-types-in-js.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0cwsgbcnmv4bqjbg51gn5jl2cjd7lq1647r9yjpqxg84yn58y35p")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-update-avm-server-version.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1p08x9mni4y8nf6ky7dxvq6g5mcfiklpr9yvlpfxanjz5q1g2lwk")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "125m4z4slpwvk3vkp2l48p3jyn1ddx4g5pvr17yp7d5c9jnnh4dx")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "03rn9l0alv0pziyxh442kiq7mrarc3p64fapblm939z8xlh75a1j")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "10924b2cz48dcphvbb0v974swz84pj3z0lr36rcpns46hbf6n68b")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1yp350zzrvnl33xdnpz0pxjricr2gd2130pzkav6bfga9smw1hh7")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "17kcw4l04pnwb7k6hgf9b1kly8r5f2qlhfss8f74l3060pqwrkbg")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1njlh73hsr95bgm857g7b46q1f68677rxi77rx3xvvbz2h21v8ps")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0hgm3y8f0g2n4wsp0f9pmag26nx3wyszk7ymjp8wyd2i20zm8n0g")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.7") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0bgq123qhv680jrcnjmj88plqcz1ssd2y3j72w9icpw8q7in0ba9")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.8") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1ca6wa3zwrf25wqwp29yvmrx3c629rnxy6ckm9vjmxwnhbi45irw")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.9") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "12ixdphl2623zyj0wszhh7n6g9kky3j3vrlfy5c49glx1vrixkxl")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-141-introduce-length-functor.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1diyzk78wsb23i987iackb54d5w2z5klhhdi3ms133d44l7xqvjk")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-141-introduce-length-functor.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "047dyx97d5bl0qw0hnbmlz1issacg2h7nkp9yp4ygfmz17n9cp85")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-fix-clippy-warnings.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1p8xak7ifn3mn3cfihkwzc5xq082m76rx7r0wv2kilw9bi57f914")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.10") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0lg30bmcsny749hyg6qxi0xxxsh3294hppww0dizn5slyy61gy48")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-141-introduce-length-functor.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xi3lfa0nclmb6lnvqa9vs2bjdyllr4qjd443qrcmfmaif7g5mip")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0mqylm95yj5jg3mywj1vcj8xhibkzi0fschqbg68y7mph8v6rbgk")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-141-introduce-length-functor.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "13ap3vcy8miasy1aq4f897kpi8c40gimhbkmmqz62yj0pkbycryi")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0b2l9bw4smg1w07ysw63rn1cv0a1zj54w8hbmjmvs0l4h3s66xyy")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1jr4pi4q4y0izmf6y59y32j74jskqybsqb5fcjq2jp7f5ckk3c0a")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1ly0nyr405r1ajs6snpnk8lkccarbs1mfgnhcaa1xz5d7g4x45ym")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "18b21s9xwybx5z82z7qcy1zjvavwdp7vy49mfzsz5z9yjx962kx8")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-json-types-in-js.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1j8miy4178imd7bwdsaf0z8kmv3ksd8b8wis34y304ayv12acr11")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0ndyz1fbk8bdvpa4kpfjalnkn9j3s8j1nmadp6rgrcdk5g35xvwj")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "182gkmadhhwhrnxk703dhr6lpbanc2mklwcb900r9zcldbbpv1jw")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-update-avm-server-version.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "07yj3np1i9vzq7p6x892qs98bwidp7d8abpgqqrsnzy91ijc52h8")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-add-rust-toolchain-file.11") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1cclgkrwnraahhm3igk51znpdknlnwq9r2fchwmwwd0jrwskjdvf")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.7") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0y46lg95x51ihlq0xqr4i9pyi1dmr5jmw11r6grrjg2lrh6w7sza")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.8") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1km8zny5pm6163ryx7c88xc8kyv89031a3ry8vzxx4xxd1gy72xg")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0y0q6gi6m2w9y5idyc66dvc4yi198ypm7clqsgvzfahx0y641am7")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "078ck28qncf4igfv2qya3g2dhfhlga5hvv9yjg4nzc5mln8l7arm")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1smhz7bkvnsbdslpx57d9ckgvlidc7qfhxmn5vqmn25q8i2d5dbl")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "11b8z7ai4wchsi5y60b6q5dzw04f95jk1svx8axbskygwgwac3ak")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1djzl4vr5x6fwg63vvn8r1vvrllkf8lbxa1hwfhfbnxcakcrl558")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0wzf55ckq1943q9iiij69m45ll4v5liqfz9cic9y274i4kka1xng")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0pdagsz2rpvi7rx8wj4kippk4w7rd25szsg9miyhsmxa07m1sq6h")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0mb5i6axn2bxmhg78n01m466d8am8x9prfdkza7j7qia3xx3pddv")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0y5kqa2nsqxyvbl86xmsh57fc70c3bxk5n638bbfixnymnc9ay6l")))

(define-public crate-air-interpreter-wasm-0.27 (crate (name "air-interpreter-wasm") (vers "0.27.0-feat-VM-142-wasm-binary-for-air-beautify.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0i1nsg42rhga0cirk5x3cphss4g3jxgx5kwajigv227ynw664aw5")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "03xdy5y38p27y5lsi9ycqkghccp0gp2rfar9gql6hk41f5kpn3r8")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "02w33f0kq2j88wranar2a113nq1k27jwnf332hq3z69cj561qkfw")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0c20xd0qg7jjajfv1nrxy20jfl61h9myf3pjqc2fbxx5n4vpxxi3")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "010prrym2wm3r4xrg1ipppf0l68n8a6mi4rlhgzg541lywg4d6fx")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.7") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1i2h8458ih38i112kzka1zln9sw64f4zfgxbc5mmnwq4ykv1d583")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.8") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0hbwd9jwsfy1kvzq1wy9xkjkybhhjdgm808g7l6m6fsq62d8cksh")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-misc-clippy-fixes-20220913.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "09wwmd0900snh3hpah3q62mnby33ql2djlypcp73wk7lmp6i7p8p")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.9") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1nm2dra05l9iwcj5nyb07909zfdch43l73pfjz8pv7g6h2hz0wax")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.10") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "13n4nsvzq5k1345ay4xprj7y7cjsvk9r88rkklfgyis539q5m54y")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.11") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "132jy96dzsywjla7qp9b4gc0pfjf6wkka053qkd8fmr0s1r7cdzq")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.12") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1iq1ppis0dpssmrj3d8jj2wx2b91simmslcazngvql62cmsnz32g")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.13") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1a5kipglqvbpqd1lcpp70clyph17vjl9v6w0f3s7qyx4pvawl1ys")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.14") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "149rlvwdqjc6b0r7dnn75mw9aqh7amb6vc9vaqxlcj857r59m64x")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.9") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "096s39z45nx08jp75n6w5fahjkk3fpxz1qig7fdmd07dj3frw7z2")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.7") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1fjs85nj6l5nlbbb3g63xzqhnk2k2j6bffsxb24r4d431mb5vr0a")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0cakil7k0ii6kx5hkjzk1w9v4xpl53jslhpfqgayy1g0s50spsbp")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.8") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0lwplqahrj94jws3b3rnj7x7r7p8vx35hzqmm1lpc01cbjmxz988")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-142-wasm-binary-for-air-beautify.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1z08lsh0iykw95jnv93326gdl9nn6nps4kxrsmbhqx5drygakfg0")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.9") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "18rwmf4h41n5is7iy55w764k1rx1q3p7kg2fp0l0fnza8a1pyvq3")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.10") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "05cn678cda6ir9dhzydhps62hrghsplcj1ivgrri88sqz2jy849g")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1wjzi1bak51k4bsaf7ayq1gdsq0gikf0cy31kkwvkfbm9h4lxxkc")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.11") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "07ff1519bqniw2jd4znz1vamz8pkp14s3ir57sp3yshxkzhzq4x1")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-release-plz-2022-09-13T16-04-24Z.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1nkg7l449c3z379gnqiqcrcq9q34ziiv2aw9d2afp2jhzjkgrcp7")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-release-plz-2022-09-13T16-06-08Z.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1za0296ina9i66k18zah8hdfb0x0zx6g1x0m5n4sc9k41xkmk270")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.15") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "153xb5s0agnwfqxp7s5k77da6sgc95p7w1x231k1jj0kvn53698h")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1n3dwwsfk81rp549mn05w9m3vlxdzq0pns61xa12jjfq7mbf2zha")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-bug-VM-156-fix-ap-states.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0j37673pzv46pg7hkdm18phrbbff841imj6hg4277lav2b2jn3kx")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.10") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0fyv05rpa7dwjl9654qb3fascrc8xhs2i7xgjrxviam4m47c7y8d")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.16") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0jl7234r3nc8di7sifd8dmxs3ysqjm75p9mighhqnfx06mjkpr30")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.17") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1g9hg7fsb3g49krj86ymqraka34c3s8xiajzv8sg3n0b4r7aq1ky")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.18") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "10xxzf8fqdc11mm6qkbql36lvkzympm5i543skrbm0xy25kz98vj")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.19") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "12xaf215swqzkl8x4vl39giidr7mbac0hk1nkd840ggrkw474bjq")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.20") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0sz2bzf0087d7wnzw99wb9kcwbzvbjg38lcsxb9gc597qzkb0mb6")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.21") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "13wj4yns9hfg722z57b2pg1hswmwrkyiy55jls7wyk88qxy24mii")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-bug-VM-157-fix-canon-positions.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1rdzis0g4jdsldrnha7pkak6f8ilr9q20bfvv4iyrs3r9m2fq0iw")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-bug-VM-157-fix-canon-positions.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0zahjhjhjisvxib9mnq9244ddd1dpvc473n3y0b31czrdj189qci")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.22") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0v1kgw1accbnczxlhbwk0bnlsvmmlawlwd7j3p35aagb5q0dlmc7")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.12") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0rqbpn9qabhg2y4p4hmpn8mjy60larfkp105783mnkrgya3gjvh3")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.23") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0nwygmfr0j0h07627w51ibvrfb746xikk86i7lzqivx7c3vx52dr")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.24") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "18daszzcw1dcfvvn30qxm4v2h7rxk5lzy415sfqrnpwcln3w4r89")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.25") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0zkfb33fsmvm1im2cwa1a884lk2mxvw99xls8sbmffsmvsxjm2qb")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.26") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "17i7cd70c3kgdwhw7s6p9izrsfpf9ssxdyklwa90297nrdjzrpxf")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0ps5sg230ndd3xq7lahg1ll0xqvkiqb1micqaw51xkj2ycplswpd")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.11") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0bp759ag94vyv701b1m8lx2f5cjc7wkmnjh0d3cw7sgv2dswgamh")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-e2e.27") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0818gyab6xkab2bpbda45q6ykpxssvapf36m07vk97p965phh9yf")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.12") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0akr0g3d3rs5p1a681x81njw4d6jgij34gbrcmny870jdgbcsb0r")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-rust-dependencies.13") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0120d3449lr122fnbsnghixp793sz9w1b0gpk7f2788bf3mb2h4d")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-158-introduce-never-instruction.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "02m7syydchknx4wnn53qkm1q9z9qhamqn90iwrnrv0q9130b4iqc")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.13") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0k9n17abq971wzvxbg0crkjqgsjzxc99szbyr3wc5jcy9ai8h2d0")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-renovate-all-non-major-js-dependencies.7") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0av06nhj4rwch0krwkdkfxr4f7jkrxy8h515lmcg03lbrj9ybi4m")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.14") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0kxd0f2llq0aiah1h3achngpd0j5ga3ln269jhhpfs6y9b1r7i16")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-158-introduce-never-instruction.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0w20fzi4jn2kbjg191yd2gpjvibcxnggxw6i5zjvi13rp15wbrmr")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-158-introduce-never-instruction.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "041r59wv9wryvyvd50al47n7ks6z2b0qhv472k35mv74dn4nq7i1")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0ml3xqcswl5c7q7l65p9b5a26y5c98rbqjsgilm53l7520azwxs5")))

(define-public crate-air-interpreter-wasm-0.28 (crate (name "air-interpreter-wasm") (vers "0.28.0-feat-VM-142-wasm-binary-for-air-beautify.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "14w18s68rlcyn0zydppy9mjdbqi8paww7dbia51diagjlwaaf0db")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1sy9rhpzqd3hxx94rdq5p2j57i5w66d6fmqfj91jvsrqnhsgnyif")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-142-wasm-air-beautify-in-tree.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "12li0g54k2p698vv14ywfvf3nz8ca4qnjjybalxmm31z40052ysi")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-159-change-behaviour-of-fold-over-stream.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "17f68xg9a46n421y2amyf0ad5507d2bqvxz8i1r05nh044xbz25j")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-bug-VM-144-restricted-streams-from-current-data-try-2.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0bkcdcif4hnlc1jcb2ajys976ri794m7myasq8kkrg7rgaxiafq3")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-debug.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0jvraqb8ficrgk05p47ibinm8y6fglfn14zxn2nl1yvpv3vxkm5y")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0p6icbrxwqr0sl6kgq9kn8z8vbnl1vcg1r2n1306wxfs8r9mny4q")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-159-change-behaviour-of-fold-over-stream.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "04pbc4mqkfv6zdnixhmfnckb5056l52gs5khb16qcvf3gqbafg0s")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-bug-VM-144-restricted-streams-from-current-data-try-2.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1xyzlg89yczp07k8rml1y6mip92i2afn312q2zjvk300y0h6vvl3")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-bug-VM-144-restricted-streams-from-current-data-try-2.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1zswkn95zpbsxlhf865i68ydjl8p6gihf565nvg3kjd0jaaknclx")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-bug-VM-144-restricted-streams-from-current-data-try-2.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0vjs59hdk23c7hhjiw8vch1y8ixr2k8a9wgcssjdy0zihjnzn9hn")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-159-change-behaviour-of-fold-over-stream.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0dw3cvskmlpvf3ijw4rl3y9ib5b78rl2lr804d0grhclzp0yxm8x")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-159-change-behaviour-of-fold-over-stream.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0ad31q60xwkp0x6azskrd1n6d0ppi9c5455w65wv6wj1gsynwd37")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-bug-VM-144-restricted-streams-from-current-data-try-2.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1kdvhxkh3fsdaabnl6l6fyjly6ghl75n3zsrfqaf9lzd2xbvzdmr")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-159-change-behaviour-of-fold-over-stream.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "14rn6fnam7qskd4yv92fsixh34w827rz8grad8qjqawq34ymgblm")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.0-feat-VM-159-change-behaviour-of-fold-over-stream.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1wav9nzxvdigsr1j9j4nj5l2jzvkr6miadpc4a1irdswrg4xdp17")))

(define-public crate-air-interpreter-wasm-0.29 (crate (name "air-interpreter-wasm") (vers "0.29.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "031rpim79sw4szp27f13z8xz2hlwlm70dyrw2jgjrqgsiads2f3v")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.0-feat-VM-159-change-behaviour-of-fold-over-stream.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1as8ng50rhnmj4bynyr8d5ic2szg8xcrihml31i20c3wgbml6wky")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.0-feat-VM-159-change-behaviour-of-fold-over-stream.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "180b6s202lvyjf6r7pxnnf6q755wh43vd8xd0cmrc5lq3a0djh8c")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0jxdg0gh8fgpfxcfxm3vr5vsfijfp6pwxnry9icirnvfa20cpa75")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0y05djplnagv1ay9ksjai9k3vsrgg1sqaziicrvcc1aj4aqs988q")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1ps2isn0ynxffpkrflwnmbbbyqgplhcyrl4lggh9byvln9i8xc01")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0783lq65raawprks22fg085y01wm7g8k5lmlfrbpg3m10dvw5n9b")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1n2k1mdks57qqd0l3qxs5mdvz1qbrh8fn3h9n0ycjls8bg0pqmjx")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1ygj9nvlki9h5b2g4s5z37c4dm1wi88426pzvcw4hprdwagh14g8")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "01v2yzhq58z69xpmlcyhp9ay7f9cllrnyqfscmswa9ymwbhg90ls")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.7") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0i8qln8p9np46nrv1rs16rna9gdzqzbirj9vkdrxqp8yd91sg5kh")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.8") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0374580vyyd1jsz0d9sk8j1fd5z77a00gpsdgj14bgj93zb4w8jd")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.9") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1ihga2wlc7ryz07q0x1scyg57x0s6c7yfw4c9dpmky0vfz7vcaa7")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.10") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0c1416gddzyhj7g0d3qa66nyswyf6yh68ylpj65frhdalr7293s0")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.11") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "05m03ziyhbigzda2qshvksn8rxi6q1pa6w8nbdhqh1jv4c970s3h")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.12") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0gr6vpgrnllpawyz5arcv7x7nhy28am1ayy4k2g3ki5pai1zmr9w")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.13") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "03dry8zdgdvmshrikf54w22dgr98qhy71c5jwivkyqfff1wjjdl7")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.14") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1y0xdxnyx9b2gjlzy9z2rly9j7kycijpygcrp7a3c9js96z1g5gx")))

(define-public crate-air-interpreter-wasm-0.30 (crate (name "air-interpreter-wasm") (vers "0.30.15") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0fckr4s3vvsg845l9aj6q04wmclyllxbr70c8581nn7qb40ghv4q")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1c6inmkdjqm4qpvwp92x5cwkwss82h2nrbdkak0vlqmhykdkln2h")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "00x39956nn2llz4qm7jkk2bhp1lhj4l8qjsnz1740xv2dwx5cky5")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.2") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1silrr75y0c1pd7x99h2dy31cvhfphgrcyk68c29iih0swwb7xc5")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.3") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "067ffqhbwyflyy0zcf643s9ycl534vyy5hgswv1nl0vsvjcas0fp")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.4") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0h1q9zgziv3mhk3bg03lmfx4h7n76wh4x905q83rzfmpvgs45qfq")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.5") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1vl1fpz1brbpk4lwc8474cbj6q7kvybhd2qxygpw4jmbpqpjq5zx")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.6") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0c3m5qpd6djx3qdpfyz81p1q14l37kxsi0x7pl8wzdj9sp244mjj")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.7") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "13jhx8b5ha728d16ch53y5k2dzs4iwxw86czdwh1a510lzi34aww")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.8") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "05fs8v1563r9k82pxcbrjil2fnxcq2mm0fvzpx0qkaqs4w9p2zi8")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.9") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1l0lb8hg3i9y2pajk0p3xkzqc85mlnknmln7q5nzi82j8nbs6zh1")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.10") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1qm4a4iib0f1s9z79wrf6947xk5j89xl3sy36xbiw0ckmdz8gzwi")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.11") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1r6p8r320950hflz37wcx9iy8v1qagsry6v2ayhpl6fij6fzg8hr")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.12") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1synz748pw4y6a1qxjsn4xpfn0jf9rr0vqgbd0y157ncmyr67f4c")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.13") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1234q13fkm7i3397qycfz40kaph7gyj54ikc5qqkbkhg00jqs3bl")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.14") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0vxw22g4flg9alhr4vsd5rlpl168gni1al6qm77sivc1bqgp5k1v")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.15") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "16pd59sdf2c72j0wjyhcziq1f9qp53xwbd56xqi1zkp7z3valhqb")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.16") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0fbb71ip822yqk3as0vy9vrskiz4gbzacj8g8dslyd58sr910j7f")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.17") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1f8nzni0ac928xgddadax36kga6dgvkkawsws8bwz9gk1v39018y")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.18") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0yja123zl3afwag55cxzd0lqg15fajy5m9l2rwxz7f4j7fzk1kwh")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.19") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "09nmzplhdnn4d7xw38g1pl3r2q73qmsl5sca9afahzqmiknbw5d2")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.20") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1fzk1ncndqsch35125gxvn8r8m8d6f1phfdzh8l1lv920xlrkp3a")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.21") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1mjxkmsyyvmccs82lk5wmdwb5v9a9dwbiwncbv8fgc8qdpkb5f73")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.22") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "0h9whjkcgybs9shkb9xpj54jl037spnl819aah7dn9qaa9cy51fa")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.23") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "17abxinhfq5ls9sfjn9ivajhfy61ywqj0abyn6i5ywsppaxgbh2q")))

(define-public crate-air-interpreter-wasm-0.31 (crate (name "air-interpreter-wasm") (vers "0.31.24") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1jl7wg7w8ql8v00hr8x2vpqvlzml5zrjsi4069zki7k2y4h6nkxg")))

(define-public crate-air-interpreter-wasm-0.32 (crate (name "air-interpreter-wasm") (vers "0.32.0") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "1qvn4s97vxqn3y4768lw01dxmx5qhkw1l9nqz3iib6j820j4hki0")))

(define-public crate-air-interpreter-wasm-0.32 (crate (name "air-interpreter-wasm") (vers "0.32.1") (deps (list (crate-dep (name "built") (req "^0.5.1") (default-features #t) (kind 1)))) (hash "10nk98hvi78w47m3vvlzxb3cyncv5hpslm29w7ijndi18m8hdyra")))

(define-public crate-air-interpreter-wasm-0.34 (crate (name "air-interpreter-wasm") (vers "0.34.0") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "0n81cmqpfq9d28zvwgxg422im78d9hxgcs8vq6jhkdfcf8w63qy0")))

(define-public crate-air-interpreter-wasm-0.34 (crate (name "air-interpreter-wasm") (vers "0.34.1") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "05skwh1hj4g22ld5kjcjy8gz8xpz99fzi44ap9sm75cjigndp0p5")))

(define-public crate-air-interpreter-wasm-0.34 (crate (name "air-interpreter-wasm") (vers "0.34.2") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "0fh2gsgw2svybxhwrdlc4k5sdavg0h53myw0sn6rpr3lf07b6n60")))

(define-public crate-air-interpreter-wasm-0.34 (crate (name "air-interpreter-wasm") (vers "0.34.3") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "1l4maixvb7f0bmzpdzm4ccwh702h7wdil5rhhsbw1892wdsgpmzw")))

(define-public crate-air-interpreter-wasm-0.34 (crate (name "air-interpreter-wasm") (vers "0.34.4") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "0mlmrg06mp1wwc9k0jxz9fzmikwr97xpxjap5k8q40z7bc73hxzs")))

(define-public crate-air-interpreter-wasm-0.35 (crate (name "air-interpreter-wasm") (vers "0.35.0") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "0pl5ymkwmc1a47xqq6nwzxla97vnag29fa8czjjb9phhzpfdp9d4")))

(define-public crate-air-interpreter-wasm-0.35 (crate (name "air-interpreter-wasm") (vers "0.35.1") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "0wf4xsd1r6d7rndsm9n6dgm8ijwxsd748l2906dlww6z2p5707ry")))

(define-public crate-air-interpreter-wasm-0.35 (crate (name "air-interpreter-wasm") (vers "0.35.2") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "1s21qnsf40zm31mgxmy62aysfkmijfjq17k8cj5bz5v01wa62l4r")))

(define-public crate-air-interpreter-wasm-0.35 (crate (name "air-interpreter-wasm") (vers "0.35.3") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "05wdg0k33hdnhaf2bzi81jvmywqdc9x4z52xpn0qphcmmhmdxch9")))

(define-public crate-air-interpreter-wasm-0.35 (crate (name "air-interpreter-wasm") (vers "0.35.4") (deps (list (crate-dep (name "built") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "0brxacvhmp3dxqhck10rcryv148s7hw4jpz4wjqzb506l9jcchl0")))

(define-public crate-air-interpreter-wasm-0.35 (crate (name "air-interpreter-wasm") (vers "0.35.5") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "0i1h664mv9f8sfll8165s6f10rhfclql77yjz3dm2937kyszz3ix")))

(define-public crate-air-interpreter-wasm-0.37 (crate (name "air-interpreter-wasm") (vers "0.37.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "00mwknb44ay950n72v19rvc8bgx6yahncj6wq6yzk873qlb0a1gy")))

(define-public crate-air-interpreter-wasm-0.38 (crate (name "air-interpreter-wasm") (vers "0.38.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "0bx2pvnn3wn64kszjm3l2062mh3d2ccxfa9ysfgjgg3bivz8vjlc")))

(define-public crate-air-interpreter-wasm-0.39 (crate (name "air-interpreter-wasm") (vers "0.39.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "1kdn5drjd83s2xsfg729774k81b8pc9awsq54hdxk71amshl3nbj")))

(define-public crate-air-interpreter-wasm-0.40 (crate (name "air-interpreter-wasm") (vers "0.40.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "0wpvjhkciw0z2hh1pjfw6y36yag7lqjapsvq6rjdbbji6mh97gbb")))

(define-public crate-air-interpreter-wasm-0.41 (crate (name "air-interpreter-wasm") (vers "0.41.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "1gfgrnnwy36rhjnn70ffcd8750kh1nhya7m1xvb1mxpck1zxcr1b")))

(define-public crate-air-interpreter-wasm-0.42 (crate (name "air-interpreter-wasm") (vers "0.42.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "0wkrv7hbq213m3ffsb6hhm126nb8i0gmbi6dmckmy6cl9r57n8d6")))

(define-public crate-air-interpreter-wasm-0.43 (crate (name "air-interpreter-wasm") (vers "0.43.1") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "02r2vcyx42bpmjq74vzcsq0j64hrm2dxh9r55s7r5jnikhc1m6ld")))

(define-public crate-air-interpreter-wasm-0.45 (crate (name "air-interpreter-wasm") (vers "0.45.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "063fmaw4096bbbrjjbbycbpj9nycxncm6fjmn5lxcxgkc42j6cy6")))

(define-public crate-air-interpreter-wasm-0.46 (crate (name "air-interpreter-wasm") (vers "0.46.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "0cbcbxvkfkywrwr6hw8cfqblyr69nliaj0gpxchl61vn12c8gjak")))

(define-public crate-air-interpreter-wasm-0.47 (crate (name "air-interpreter-wasm") (vers "0.47.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "0pv2klblxf8phnggmyag19xn7s8m1c0n1ynrlsjp0z03r8mpx4c0")))

(define-public crate-air-interpreter-wasm-0.48 (crate (name "air-interpreter-wasm") (vers "0.48.0") (deps (list (crate-dep (name "built") (req "^0.6.0") (default-features #t) (kind 1)))) (hash "01rdj3x4pg0djncimi3cf1hh9z36xpxz8g0hrprjcwbvy76002kg")))

(define-public crate-air-interpreter-wasm-0.49 (crate (name "air-interpreter-wasm") (vers "0.49.0") (deps (list (crate-dep (name "built") (req "^0.7.0") (default-features #t) (kind 1)))) (hash "0hlxg1b4k1xpyywlx0150zzdjarcwplgrfxvpkcfpi6sbzh8i4sy")))

(define-public crate-air-interpreter-wasm-0.50 (crate (name "air-interpreter-wasm") (vers "0.50.0") (deps (list (crate-dep (name "built") (req "^0.7.0") (default-features #t) (kind 1)))) (hash "1qicrsd39ybss9zrxbafpc69r59ns3fd1mmqn5bhq4ij9mz3va95")))

(define-public crate-air-interpreter-wasm-0.52 (crate (name "air-interpreter-wasm") (vers "0.52.0") (deps (list (crate-dep (name "built") (req "^0.7.0") (default-features #t) (kind 1)))) (hash "0x33ni40gqhk10wvf521ay08mrix84l9nmpr4cl7j5w8lyrwwm72")))

(define-public crate-air-interpreter-wasm-0.54 (crate (name "air-interpreter-wasm") (vers "0.54.0") (deps (list (crate-dep (name "built") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "07qd0iy1frfk15p0k0skaa5nc3gc1g3rfda9nd8dz6ryqfli8fy5")))

(define-public crate-air-interpreter-wasm-0.55 (crate (name "air-interpreter-wasm") (vers "0.55.0") (deps (list (crate-dep (name "built") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "1vv96zygg56zxan4n0zcpfvspqvzd870x5idjsncpbbd3adh9l13")))

(define-public crate-air-interpreter-wasm-0.57 (crate (name "air-interpreter-wasm") (vers "0.57.0") (deps (list (crate-dep (name "built") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "0515m0ryadp9l5mih4lb1p5pljyi6wxx3y1x8zjdkpcwj7mvg5b8")))

(define-public crate-air-interpreter-wasm-0.58 (crate (name "air-interpreter-wasm") (vers "0.58.0") (deps (list (crate-dep (name "built") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "10cp097yy0jwc825sd017z4ryqf0mvrnxfbgy8y2fvvazm0r1dml")))

(define-public crate-air-interpreter-wasm-0.59 (crate (name "air-interpreter-wasm") (vers "0.59.0") (deps (list (crate-dep (name "built") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "0fa8rhxl311svi12rji5pwxrbia6zjz5r8sfvvclcq3y2cnrmw1j")))

(define-public crate-air-interpreter-wasm-0.62 (crate (name "air-interpreter-wasm") (vers "0.62.0") (deps (list (crate-dep (name "built") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "1c0hqyzinvl6pvqsk27bapz8x2f2ql3pk4cx222y1x7bp954whij")))

(define-public crate-air-interpreter-wasm-0.63 (crate (name "air-interpreter-wasm") (vers "0.63.0") (deps (list (crate-dep (name "built") (req "^0.7.1") (default-features #t) (kind 1)))) (hash "0v1wdy21ia90gk9flywnql3vnn5sq1m2wfjvwblqsjprg9bf66r9")))

(define-public crate-air-interpreter-wasm_branch_release-0.0.0 (crate (name "air-interpreter-wasm_branch_release") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1n45ycpd3kzyzczl6k56gly8h8yf59yzf91zcx68z213dzr0svv5")))

(define-public crate-air-interpreter-wasm_ci_fix_version_increment-0.0.1 (crate (name "air-interpreter-wasm_ci_fix_version_increment") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09x85hc8mv5sc5ahhv1znrcprmz6h3jgvjzvdk7hf87f7376s0c4")))

(define-public crate-air-interpreter-wasm_crate_release-0.0.14 (crate (name "air-interpreter-wasm_crate_release") (vers "0.0.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xhgd6fcmyqhwfs7ay4mfr1rmx4midgyb04dk3181hanwgqadr06")))

(define-public crate-air-interpreter-wasm_crate_release-0.0.15 (crate (name "air-interpreter-wasm_crate_release") (vers "0.0.15") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "13fzn290fdkq41sj69qviwq8a10izdhsm2v0disj790fyl1xn7bc")))

(define-public crate-air-interpreter-wasm_crate_release-0.0.17 (crate (name "air-interpreter-wasm_crate_release") (vers "0.0.17") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "12higw51ljsz56ink3g1kxm76py76ks45154byycvf9rsn1xqkfq")))

(define-public crate-air-interpreter-wasm_crate_release-0.0.18 (crate (name "air-interpreter-wasm_crate_release") (vers "0.0.18") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0f9kwk9dzv1ywm2miwykprhcxylmy3qsk7qyn6lc6n6hrkpzw8rk")))

(define-public crate-air-interpreter-wasm_fix-0.0.0 (crate (name "air-interpreter-wasm_fix") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ip48m5l0jlmn6vgvdwr2b274lx8yakjsknhnihyx48kxk612vfq")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.1 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hi604sc3313dc3aybvy452saz74grzlhal4cky7biy10wj0pv6h")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.2 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0q3hmd6nnqmkwkhyv72qr1q35czla7f6k3lcibsnjc72ibhxkd6w")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.3 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "165liqyry7qiqspxxav2p99fhgz2vxhkc3qjj19k83vwmjdzcg6p")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.4 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "09zzd358nz08h9swqxgw93v7r27735im0b2gq48rvqdmgl3608f4")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.5 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0q22s7q6g69yi0gq0iyar9h87wc5s8hzn5f0y8z3il3zbzw5nl07")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.6 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "13qcbxr7kzaga0pahzrvj8xl8z6x6w7gh43x2djbk24z0hsj4n3j")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.7 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fqpv1irpqidmqwd9fz69pd0vwdd7wdq4dahgfykpbfkxyfpza1y")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.8 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0qp6hr88llzjrjwg3qj9as2ggy0ixys62xp2s05mf4bgy9ikp93v")))

(define-public crate-air-interpreter-wasm_fold_iterable-0.0.9 (crate (name "air-interpreter-wasm_fold_iterable") (vers "0.0.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "01vl7p0grz2hxysa1vg2xyhaz3z4411619lly1ibdgrx90553311")))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.1 (crate (name "air-interpreter-wasm_get_rid_current_peer_id") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1j3p01pzn4ajxrbyf239ppmwpmwh51wy81x3ggwg6xp4b0gw1v7s")))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.2 (crate (name "air-interpreter-wasm_get_rid_current_peer_id") (vers "0.0.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0vpvyjzvm2kyf2srydn3v1d480sgb95wbvrxjwpg7wpxj6sp18fa")))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.3 (crate (name "air-interpreter-wasm_get_rid_current_peer_id") (vers "0.0.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "17jvvbppkssgx323wgb9a2ar210h7a3vmv4cc5hm1da5zjhkibr5")))

(define-public crate-air-interpreter-wasm_get_rid_current_peer_id-0.0.4 (crate (name "air-interpreter-wasm_get_rid_current_peer_id") (vers "0.0.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "14qgpl95fzz2ss6662rihwa9220kfa88p0pvnyy152m7yagqdd2l")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.0 (crate (name "air-interpreter-wasm_jvalue_flatten") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1jfxli38f30lzmssmwkfnq9ng43pnb9ydznby8jx71mpjmwmpg2l")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.1 (crate (name "air-interpreter-wasm_jvalue_flatten") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0wrr2ppdvx334mz7ndrbn62dkza9aasnx5r04m1sbdfhbz30zn36")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.2 (crate (name "air-interpreter-wasm_jvalue_flatten") (vers "0.0.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1rhkglis5bbzy8zv2f24pishdim8pcv6hzjmwy79h7w4hvywrwfz")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.3 (crate (name "air-interpreter-wasm_jvalue_flatten") (vers "0.0.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0yc48wlxm5s43sv62fqz3cpjvkfd0bzbi136x9m8mbmkxncwh8f4")))

(define-public crate-air-interpreter-wasm_jvalue_flatten-0.0.5 (crate (name "air-interpreter-wasm_jvalue_flatten") (vers "0.0.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1pmzdcr4d06aknjpdybh0y42jznshclnrwppzs298269qsr39v2a")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.0 (crate (name "air-interpreter-wasm_logger-i32") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1v547ahn1z4hiqg537wwi4v0q40lawgr44wjw72s5gljd75nh2wf")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.1 (crate (name "air-interpreter-wasm_logger-i32") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0hlkl2vgy9gdxa6vpfaq2lhr9r9n81p3sjp63jmna4cgma5dgf7p")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.2 (crate (name "air-interpreter-wasm_logger-i32") (vers "0.0.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ys0jpb8wzngzs6icxgcj3zig5pbzqpx91r8k0y2qd8imbqgf4vp")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.3 (crate (name "air-interpreter-wasm_logger-i32") (vers "0.0.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1x4jbbqjv4kdk4jqbhaf4wph0zg6l3h14ak5np3ryf6i5f0mcbvx")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.4 (crate (name "air-interpreter-wasm_logger-i32") (vers "0.0.4") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0pjcxfzm07jkbxkxb12xyfshn2pp3d69gy7594mmdqbb1y9a7l8b")))

(define-public crate-air-interpreter-wasm_logger-i32-0.0.5 (crate (name "air-interpreter-wasm_logger-i32") (vers "0.0.5") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1s93vrqck2jfsyplgxlh4j4nrx2k3q8g6v16481c12r4qrynmkw6")))

(define-public crate-air-interpreter-wasm_new_it_types-0.0.1 (crate (name "air-interpreter-wasm_new_it_types") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "044vlx17gwn8b6j8w366z8ckf88fk9zy92m6292js14vv7rnl1v8")))

(define-public crate-air-interpreter-wasm_new_it_types-0.0.2 (crate (name "air-interpreter-wasm_new_it_types") (vers "0.0.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0nl2yy17id9gjkppvny8qfxb2ifjhlpir1q1fc859548lh5njxch")))

(define-public crate-air-interpreter-wasm_new_it_types-0.0.3 (crate (name "air-interpreter-wasm_new_it_types") (vers "0.0.3") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1fh4cfsq5g43xp7ijb201386r11iv4w3wzjwm4kziawnzfszs8d6")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.7 (crate (name "air-interpreter-wasm_new_stepper_outcome") (vers "0.0.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1mm5zs6blwbbx1fq30xzg892a6nw06p5wrakar0lmppgif189sgx")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.8 (crate (name "air-interpreter-wasm_new_stepper_outcome") (vers "0.0.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1p7l9sk3rgfqvqs78pbj7icfap0l7imjsn7jqlrzbb56s2rk1jn4")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.9 (crate (name "air-interpreter-wasm_new_stepper_outcome") (vers "0.0.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0bjarl4z1wqrp2zng71ap1n8c31qx6yz5r5xwdpn0azq1l25zdj4")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.10 (crate (name "air-interpreter-wasm_new_stepper_outcome") (vers "0.0.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1100a7v4dwxriivgrkbzl75mkhc503nrrhji59ivlpsww33i8cbk")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.11 (crate (name "air-interpreter-wasm_new_stepper_outcome") (vers "0.0.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1zzf829b6l4nb5mbar8af0x3z3fx6x63x8jb12h1ard7lgfwd4fl")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.12 (crate (name "air-interpreter-wasm_new_stepper_outcome") (vers "0.0.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rn5bj4wn0vxiqiap9xb2bqr4kp8mr00qsmd27jxa26j8rs4s5za")))

(define-public crate-air-interpreter-wasm_new_stepper_outcome-0.0.13 (crate (name "air-interpreter-wasm_new_stepper_outcome") (vers "0.0.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0ckc0bm4lnl5kwc7v0mv045k9lba7l6r391xdzln29fc910vl35k")))

(define-public crate-air-interpreter-wasm_prerelease-0.0.0 (crate (name "air-interpreter-wasm_prerelease") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vvrl3ffznw1n1mdwp9lfp76yli64rdf4v3s63jv3w0ai1l89r2w")))

(define-public crate-air-interpreter-wasm_prerelease-0.0.1 (crate (name "air-interpreter-wasm_prerelease") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1kqk0z1gszbhypp9aikdw5kdyv9adk4r9ag9s65p5d49sn8jpr3g")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.6 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.6") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "128vcq920civ6v4k0krpwsbbh57xzw918g1xl80x9rbd1f82l1an")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.7 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.7") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "06a8p1xkx8qwf024f8i6kw9svk93x89mh5r1j4cb9cmm09z6c9vx")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.8 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.8") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "11s2k01119acvymgvnz6gvgw3y594j9lwhjgmgynhbmjivn1cxsv")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.9 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.9") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1xncg2j3b5yjjnzyhm9cyg9k49jnpwnbi2z3ni5nhr1flpr3xhcx")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.10 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.10") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0j5681pp3i2j7lx41ilq9lj1ab6r7zhyvlb65rvl8rgalq4wa4b8")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.11 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.11") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0z3x7m5cw8c8xrsj86974rzx45ja9zw6hcfz435h798apal1fsxv")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.12 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.12") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "07d1rjdrfl042s2glilpf59hl126r9kznzl97h8pkk192ka5ag0m")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.13 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.13") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0l1f2xg0592ds92s9472iiqrpdzzqmi2r82hglv14iz997clkr62")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.14 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.14") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0rkiwwwacdj5n0kqfk6gvpf6gf52zsjbpfn7n4yjsgv6ap5d8s0b")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.15 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.15") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "1vllilc2rbm84ywvkbqi3rgr66rapfj5pb8hw9ijl5qk3r4w22j5")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.16 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.16") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0xx45zkfqanj6vlzcdg870k6lv6d4cq6s262zxrf5fx8g3x5hir2")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.17 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.17") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0s53cpmasg63byidi93swccc4s1cwwyw51d85rkcyg1123389vp8")))

(define-public crate-air-interpreter-wasm_security_tetraplets_reborn-0.0.18 (crate (name "air-interpreter-wasm_security_tetraplets_reborn") (vers "0.0.18") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0lyb294gchpglzgd2fcdnkgfxbr27mdw8jvv8m0g4p42f0f8rrih")))

(define-public crate-air-interpreter-wasm_update_example-0.0.0 (crate (name "air-interpreter-wasm_update_example") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0fih6hg9fdpbp81fgjqwrq8l3yrydivwr79pp9nx550932z3raik")))

(define-public crate-air-interpreter-wasm_update_images-0.0.0 (crate (name "air-interpreter-wasm_update_images") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "0iaqi4rh4a056ssvl21hyyic88rbanv7ghhqh5l8v84fvqfv4ypv")))

(define-public crate-air-interpreter-wasm_update_images-0.0.1 (crate (name "air-interpreter-wasm_update_images") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "061iyrb4kvl1aq3if50dbaf62d0dd2q3g1zl43sw87cggi2s0ck7")))

(define-public crate-air-interpreter-wasm_wasm_log-0.0.1 (crate (name "air-interpreter-wasm_wasm_log") (vers "0.0.1") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "190zyrfs7k3jd1jk37lzd17qajivd9whyzih7cbbaw6w5sqgbid3")))

(define-public crate-air-interpreter-wasm_wasm_log-0.0.2 (crate (name "air-interpreter-wasm_wasm_log") (vers "0.0.2") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "03kjicsq62iqc7ad50wqd3b59xj628flqkyigngzh5mzg5sb0ab6")))

(define-public crate-air-interpreter-wasm_wit-0.0.0 (crate (name "air-interpreter-wasm_wit") (vers "0.0.0") (deps (list (crate-dep (name "built") (req "^0.4.4") (default-features #t) (kind 1)))) (hash "13dxph6nz4gyr7bjdz4jsa14l6zfhhskyk7604lx7hv362509mpp")))

(define-public crate-air-ir-0.1 (crate (name "air-ir") (vers "0.1.0") (deps (list (crate-dep (name "parser") (req "^0.1.0") (default-features #t) (kind 0) (package "air-parser")))) (hash "0hhl9zz36pz3kmws141g7lva9x0vshm164n3526andxa4aspvq2i") (rust-version "1.65")))

(define-public crate-air-ir-0.2 (crate (name "air-ir") (vers "0.2.0") (deps (list (crate-dep (name "air-script-core") (req "^0.2.0") (default-features #t) (kind 0) (package "air-script-core")) (crate-dep (name "parser") (req "^0.2.0") (default-features #t) (kind 0) (package "air-parser")))) (hash "18pmgx8h0zh7vrh2b6vcpjpl790awi5hlxjfbhgxfyw476szbii4") (rust-version "1.65")))

(define-public crate-air-ir-0.3 (crate (name "air-ir") (vers "0.3.0") (deps (list (crate-dep (name "air-parser") (req "^0.3") (default-features #t) (kind 0) (package "air-parser")) (crate-dep (name "air-pass") (req "^0.1") (default-features #t) (kind 0) (package "air-pass")) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "miden-diagnostics") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yssixs27c1aj14jih1d5995lxlj7jq3z59wv4l9k8x1x4r5rf69") (rust-version "1.67")))

(define-public crate-air-lambda-ast-0.1 (crate (name "air-lambda-ast") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "non-empty-vec") (req "^0.2.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1ixizgin39gj7229n422r3257ivimhazfvzwvnzri21fxnbjvaqj")))

(define-public crate-air-lambda-parser-0.1 (crate (name "air-lambda-parser") (vers "0.1.0") (deps (list (crate-dep (name "air-lambda-ast") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "codespan") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "lalrpop-util") (req "^0.19.8") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.8") (default-features #t) (kind 1)))) (hash "1wb9jjbydjffnqv2pdni5a7mmaw9r8ql12qq366l292ggv887zb0")))

(define-public crate-air-log-targets-0.1 (crate (name "air-log-targets") (vers "0.1.0") (hash "02097dhzq4ljsx4aza11rn5746v69njdv4lakc2dfb7wpj23bj25")))

(define-public crate-air-parser-0.1 (crate (name "air-parser") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hmhhwdknjycpw0id54j97rdfrhnhkjmabx86bbkw630w7jwfkhd") (rust-version "1.65")))

(define-public crate-air-parser-0.2 (crate (name "air-parser") (vers "0.2.0") (deps (list (crate-dep (name "air-script-core") (req "^0.2.0") (default-features #t) (kind 0) (package "air-script-core")) (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1kg533fkvkiwnqxypf9c7g3b367rq9frrs5zmm1g9ggaq5gcjp7w") (rust-version "1.65")))

(define-public crate-air-parser-0.3 (crate (name "air-parser") (vers "0.3.0") (deps (list (crate-dep (name "air-pass") (req "^0.1") (default-features #t) (kind 0) (package "air-pass")) (crate-dep (name "lalrpop") (req "^0.20") (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "miden-diagnostics") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "miden-parsing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0an3ba9v3j38x1hngz2v24pz7csmfmi00jjd0azw5ky0ynvmw9nf") (rust-version "1.67")))

(define-public crate-air-pass-0.1 (crate (name "air-pass") (vers "0.1.0") (hash "1ihjwab0s8khv387my1wbh0503rf1v49bk8vcvnlg9w5kzpc5r5k") (rust-version "1.67")))

(define-public crate-air-script-0.1 (crate (name "air-script") (vers "0.1.0") (deps (list (crate-dep (name "codegen-winter") (req "^0.1.0") (default-features #t) (kind 0) (package "air-codegen-winter")) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "ir") (req "^0.1.0") (default-features #t) (kind 0) (package "air-ir")) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "parser") (req "^0.1.0") (default-features #t) (kind 0) (package "air-parser")) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1xy2d0nzq46awl6i4sg47nryid5nhayrrs5b25jw6y3x9v9wb2yx") (rust-version "1.65")))

(define-public crate-air-script-0.2 (crate (name "air-script") (vers "0.2.0") (deps (list (crate-dep (name "codegen-winter") (req "^0.2.0") (default-features #t) (kind 0) (package "air-codegen-winter")) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "ir") (req "^0.2.0") (default-features #t) (kind 0) (package "air-ir")) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "parser") (req "^0.2.0") (default-features #t) (kind 0) (package "air-parser")) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "winter-air") (req "^0.5.1") (kind 2) (package "winter-air")) (crate-dep (name "winter-math") (req "^0.5.1") (kind 2) (package "winter-math")) (crate-dep (name "winter-utils") (req "^0.5.1") (kind 2) (package "winter-utils")))) (hash "1an7d536j2kcqm018vk9yxjb1325bblpj0f8lqhhzjrlf423hkfz") (rust-version "1.65")))

(define-public crate-air-script-0.3 (crate (name "air-script") (vers "0.3.0") (deps (list (crate-dep (name "air-codegen-masm") (req "^0.1") (default-features #t) (kind 0) (package "air-codegen-masm")) (crate-dep (name "air-codegen-winter") (req "^0.3") (default-features #t) (kind 0) (package "air-codegen-winter")) (crate-dep (name "air-ir") (req "^0.3") (default-features #t) (kind 0) (package "air-ir")) (crate-dep (name "air-parser") (req "^0.3") (default-features #t) (kind 0) (package "air-parser")) (crate-dep (name "air-pass") (req "^0.1") (default-features #t) (kind 0) (package "air-pass")) (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "miden-diagnostics") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winter-air") (req "^0.6") (kind 2) (package "winter-air")) (crate-dep (name "winter-math") (req "^0.6") (kind 2) (package "winter-math")) (crate-dep (name "winter-utils") (req "^0.6") (kind 2) (package "winter-utils")))) (hash "0j4anwrbdn04vx8jwb0ff8drhy7m7rrvrqvshqrg0cxnkk1wjf4h") (rust-version "1.67")))

(define-public crate-air-script-core-0.2 (crate (name "air-script-core") (vers "0.2.0") (hash "0j7j33a58w7axxpgwf6b47c9k3xn1zhwqsmw6vllgy9nibid0563") (rust-version "1.65")))

(define-public crate-air-test-utils-0.4 (crate (name "air-test-utils") (vers "0.4.7") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.38.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.28.3") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "fstrings") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0lbkapz8f04rlrmkbznhj7n4ybxf57pky57n76pay5sw69yla23v") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.5 (crate (name "air-test-utils") (vers "0.5.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.28.3") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "fstrings") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.158") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1r0pl8jmlkgr1fsjz513n0n7rzr0nprxig7f46giba02vpcw6fg5") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.6 (crate (name "air-test-utils") (vers "0.6.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.40.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.28.4") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "fstrings") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0j2pls936jg26nyj65cnfyif618sn6r8rl95s435sgvfx2cdqyy5") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.7 (crate (name "air-test-utils") (vers "0.7.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.28.4") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "fstrings") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0g3s1bhrljxzsm0d0k0r46vimml6n2hvkxklpnlymsfgak7z4kg3") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.7 (crate (name "air-test-utils") (vers "0.7.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.28.4") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0a399cl0qqxxz6k1h21fsvpdg2is96rj5aibkqly0qs80rbnq0fc") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.8 (crate (name "air-test-utils") (vers "0.8.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.43.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.28.5") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0kwaggyyyfn2i0xbhxd4r6zcqbnp6a6gl20mnkfcxzh776gnj2iz") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.8 (crate (name "air-test-utils") (vers "0.8.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.43.1") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.28.5") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0pz6r6q1p8ib2zvvw3ian65nxg1k7dxvlg4v9qfpgif0z37zck4p") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.10 (crate (name "air-test-utils") (vers "0.10.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0vcnrjiqni3q8w5fyx6qjjw9hz4azrxp0rf598hr429c9c2sfhvc") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.10 (crate (name "air-test-utils") (vers "0.10.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.45.1") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "19nwxa2cwq09h9cvdw2nggmi6g47c7h0mgfaniajil1l8k6p4khq") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.10 (crate (name "air-test-utils") (vers "0.10.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.46.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1xbdjqszgdxpsf27ff91zxgrvi71w47b0cwiinyncwr0wyk1x6q2") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.10 (crate (name "air-test-utils") (vers "0.10.3") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.47.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "01lgy6anqfms1wd75acwbhhjwiph3r527kb5faj0bk5lbh98i90q") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.10 (crate (name "air-test-utils") (vers "0.10.4") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.48.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.1") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1cpx1x2pqzzpsd31xancgbcrmg9c17m8vr8k0cn4vinvh2gh5zg7") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.10 (crate (name "air-test-utils") (vers "0.10.5") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.49.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.1") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "017lkv11awkiqwczqyq3gf6pv7n736885nzpl7awvfh446cwcczr") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.10 (crate (name "air-test-utils") (vers "0.10.6") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.50.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.1") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0z30lyi0g783lw7wp2kv23ww1x0s2gihikc6x9pkz3pg3b5bjf5w") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.11 (crate (name "air-test-utils") (vers "0.11.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.51.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.1") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0fx474f64mpvbnzkwcc7fkm1h6q780v1ms3103irb85kpgmhhzc4") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.11 (crate (name "air-test-utils") (vers "0.11.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.52.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.2") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.2") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0x5qx1i3wv9pv1rgnylx8nia353qc984xdn04648gvcbrz2f71h5") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.12 (crate (name "air-test-utils") (vers "0.12.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.54.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.2") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.3") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "02qf9kcks2x6dxkx2fbwm797l46pv02y2fkzsp6bd3xsnnxjknv9") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.13 (crate (name "air-test-utils") (vers "0.13.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.55.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.29.3") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.33.4") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1a0qhpjiynsx824614656v0lrnyb8m828qsldbdj1r42sm2hizy2") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.14 (crate (name "air-test-utils") (vers "0.14.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.57.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.34.1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "05wkvwjq5p8jvn1bwhaggv89v5wrklai75d60kkaq5iqkmi92svk") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.15 (crate (name "air-test-utils") (vers "0.15.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.58.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.31.1") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.34.2") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "19mfqh4cd63z7vqikgyr3m7r45k8pxjgyyvq7hi4jp5v5sxngaxq") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.15 (crate (name "air-test-utils") (vers "0.15.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.59.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.31.2") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.35.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0lm0f8miydiiad4akpaqb3jvdfznyxxnwkd31wlsargygr5r6ig3") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.17 (crate (name "air-test-utils") (vers "0.17.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-value") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.62.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.37.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0ix7wyizg626v61m5h09apm3paxq36iz42jmbyw5gij0c1d2as0h") (features (quote (("test_with_native_code"))))))

(define-public crate-air-test-utils-0.18 (crate (name "air-test-utils") (vers "0.18.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-sede") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-value") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air") (req "^0.63.0") (default-features #t) (kind 0)) (crate-dep (name "avm-interface") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "avm-server") (req "^0.38.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "marine-rs-sdk") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "marine-wasm-backend-traits") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "marine-wasmtime-backend") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "object-pool") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "1dqd843rkwlvd36rh5ys6bqmljbnirh8vx0f6m5njx8rj42yidjn") (features (quote (("test_with_native_code"))))))

(define-public crate-air-testing-framework-0.1 (crate (name "air-testing-framework") (vers "0.1.7") (deps (list (crate-dep (name "air-test-utils") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.4.7") (features (quote ("test_with_native_code"))) (default-features #t) (kind 2)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pzpi5yxh9p82g76wwwm1hffbzsxswjv424fqqbg247znkw4d69l")))

(define-public crate-air-testing-framework-0.2 (crate (name "air-testing-framework") (vers "0.2.0") (deps (list (crate-dep (name "air-test-utils") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.5.0") (features (quote ("test_with_native_code"))) (default-features #t) (kind 2)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ipbrdlygpvyy6a6ask60s31zrpi540bsf0lj7hlj0rcaxchppv3")))

(define-public crate-air-testing-framework-0.2 (crate (name "air-testing-framework") (vers "0.2.1") (deps (list (crate-dep (name "air-test-utils") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0r5mp6zpill6cfxbij07li4644sjxw657h1wmk280m15rpzn45v7")))

(define-public crate-air-testing-framework-0.3 (crate (name "air-testing-framework") (vers "0.3.0") (deps (list (crate-dep (name "air-test-utils") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wraysh3qwa101qbrpffzv5g7l5vg1si4xl35glyp4rp77jw11nj")))

(define-public crate-air-testing-framework-0.4 (crate (name "air-testing-framework") (vers "0.4.0") (deps (list (crate-dep (name "air-test-utils") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rn89zvkg2cj0z97rwwmhnivi9ghz4fw1f340i2dqml47djzxqkj")))

(define-public crate-air-testing-framework-0.4 (crate (name "air-testing-framework") (vers "0.4.2") (deps (list (crate-dep (name "air-test-utils") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1drirpz9mqaa5g487c99jywv7pkilfq74vhq2hliihql6gncp1fq")))

(define-public crate-air-testing-framework-0.4 (crate (name "air-testing-framework") (vers "0.4.3") (deps (list (crate-dep (name "air-test-utils") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1zhxbzxac0blw0iffrpxgz5pkr5vl3gbycb2jqs4qdwp6fa3f1sz")))

(define-public crate-air-testing-framework-0.5 (crate (name "air-testing-framework") (vers "0.5.1") (deps (list (crate-dep (name "air-test-utils") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1h9gwvzl8al9vpla9w2jsg0b3qb4drlb5fipiap059qb7q7wwyd1")))

(define-public crate-air-testing-framework-0.5 (crate (name "air-testing-framework") (vers "0.5.2") (deps (list (crate-dep (name "air-test-utils") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sx7cxspas67b0ssci6vn4hir06p83h963fvnsm70cq6kja8yr1r")))

(define-public crate-air-testing-framework-0.5 (crate (name "air-testing-framework") (vers "0.5.3") (deps (list (crate-dep (name "air-test-utils") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hqgm112jr5iy80hiph2klqq0fv1vdflkpdym1n0q7ma3w2bznk5")))

(define-public crate-air-testing-framework-0.5 (crate (name "air-testing-framework") (vers "0.5.4") (deps (list (crate-dep (name "air-test-utils") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1h4hsrgnzqzl0vrhwk41q7q4rgxk98vliphbz0cccz138yxl7nmh")))

(define-public crate-air-testing-framework-0.5 (crate (name "air-testing-framework") (vers "0.5.5") (deps (list (crate-dep (name "air-test-utils") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1g4cn4qny73rpf4v8gggbcg7r781dz9w27qcgd2lqa21z6xz52pr")))

(define-public crate-air-testing-framework-0.6 (crate (name "air-testing-framework") (vers "0.6.0") (deps (list (crate-dep (name "air-test-utils") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cb1cfifrpxq039pvj6fdpbs6a0azp3hg1c1gk480h2fa9qipw76")))

(define-public crate-air-testing-framework-0.6 (crate (name "air-testing-framework") (vers "0.6.1") (deps (list (crate-dep (name "air-test-utils") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1x4rqagpmqpbn0f7g3sy5zbpcrkkgbagd0a0bgffklj5r1mfqp6n")))

(define-public crate-air-testing-framework-0.7 (crate (name "air-testing-framework") (vers "0.7.0") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "081ryp1xcb2m0yzf9n8dycgrwz24xb56pxvwrq4mqir2jan652lv")))

(define-public crate-air-testing-framework-0.7 (crate (name "air-testing-framework") (vers "0.7.1") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0vv1k07cir43gla6sara0xid04nrzx07sxqh1f0v7j1m660xr61q")))

(define-public crate-air-testing-framework-0.7 (crate (name "air-testing-framework") (vers "0.7.3") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0d39lfwafns92k97g8wmdpniisgszw1acr1yyw65365ywx4i3dkd")))

(define-public crate-air-testing-framework-0.8 (crate (name "air-testing-framework") (vers "0.8.0") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pnv6pkz5n6p8xpbcknlvjavh5kf6g515h01dhd3fsq7gijnrqni")))

(define-public crate-air-testing-framework-0.8 (crate (name "air-testing-framework") (vers "0.8.2") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fj90nirrjnphfmq4bg8ca5z7pp4wb39966dsmhjiv2r6ang66lv")))

(define-public crate-air-testing-framework-0.8 (crate (name "air-testing-framework") (vers "0.8.3") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wx4m6vvlc7fdr2yp9x5zrizmzjikdj4dzbh6m5j5mkqik0qphrm")))

(define-public crate-air-testing-framework-0.8 (crate (name "air-testing-framework") (vers "0.8.4") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1i82zdd935wva0xfjd46f8n4y3gprcf1nrqy7yp271aqlzyrczl0")))

(define-public crate-air-testing-framework-0.10 (crate (name "air-testing-framework") (vers "0.10.1") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bs144lrnffx21sliqmws09nl0zkz7jvcll19pvf416cab81mkxd")))

(define-public crate-air-testing-framework-0.11 (crate (name "air-testing-framework") (vers "0.11.0") (deps (list (crate-dep (name "air-interpreter-signatures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "air-test-utils") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "async-recursion") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "fluence-keypair") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "14byi21wdp656j3lyp4k1g7v4jc5sacrcff38akmd8hsg9yd5yz8")))

(define-public crate-air-trace-handler-0.1 (crate (name "air-trace-handler") (vers "0.1.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1jshls6k8swr2h7czk1pdkpc822sap60l3267abrpj8ggz1gdzhl")))

(define-public crate-air-trace-handler-0.1 (crate (name "air-trace-handler") (vers "0.1.3") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1xn3rgl3afbx44b0lsxia471z9w7rzlx4kndawzc8mqz4c1hx8qz")))

(define-public crate-air-trace-handler-0.2 (crate (name "air-trace-handler") (vers "0.2.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1nh29v1y8jrr3bic4z2q5748r5zf2960kb7n24wgpigmxihxbhgv")))

(define-public crate-air-trace-handler-0.2 (crate (name "air-trace-handler") (vers "0.2.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-interface") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0gjw2p7xvqkd4s8fd4abf7826rdkivjnlmcpgq0bq2ix67kcxffh")))

(define-public crate-air-trace-handler-0.2 (crate (name "air-trace-handler") (vers "0.2.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0mx6mf647gypfv3dm7zxvlwg53c4yp64hg5az9yc3gll7yzj1zj7")))

(define-public crate-air-trace-handler-0.3 (crate (name "air-trace-handler") (vers "0.3.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0sz46hmw4c7zhxdzikacdlqid3nlvjhjr77mqvlxxqly0phr8ga9")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.0") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1yp07b07a8lpw5g4cmlwmf9bixz2q0xlvx93k5zn1mq1pfp25brl")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.1") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1l5rsma3kqin2m7wkwmmr6k1lyaak2hx34dz420sxn682ch7zxal")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.2") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1f1z2n3k7pkkid2zx0h06pdsp3yg3qr0gly4833r31wk7p6dvk72")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.3") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0vvb4izjr2smamigqblgpb1v1sxg2c7ln2xaslwl95xhvb3prv2b")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.4") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "089b9mb9y99hd3v02w3gg7hlalzndw57603lqa04ib6ygcb75fg2")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.5") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1zhd2yy72jxmjj35r4jn1qzhnhlrk0s6b64wwlj3dv35l9cpwxcd")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.7") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0iw2xqilmynccl3vm61bif7w407ig2vsv2wdql4qx9nj6zs8jssd")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.8") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "055xkm640srx5nmx7pynf0js4vqbjjkc9vx51cs4fkssvcvgxay0")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.9") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "07wqbr732blwcmjn4sfkhz18c51fv5rkflvabmc420hjl97r4835")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.10") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "18xaw1h6map55z8nhgsxn2xpzbphfzl22d2vwy6qk8f1c4h4m2pb")))

(define-public crate-air-trace-handler-0.5 (crate (name "air-trace-handler") (vers "0.5.12") (deps (list (crate-dep (name "air-interpreter-cid") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "air-interpreter-data") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "air-log-targets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aquavm-air-parser") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "polyplets") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1mkb92aaaw3w4zv0p8qmnils1vxrgxymv4nlm3zxziqddq5b9l5g")))

(define-public crate-air-utils-0.1 (crate (name "air-utils") (vers "0.1.0") (hash "1d2cpqsiqkv9n2v5f7l23n1440yhn6bzl0dy0ann14fvig2jxpqr")))

(define-public crate-air-utils-0.1 (crate (name "air-utils") (vers "0.1.1") (hash "129snqa2lc12rdivhf1fyjc3v7kr5qcs5g2z9j1h5ilgm8n91r97")))

(define-public crate-air-utils-0.2 (crate (name "air-utils") (vers "0.2.0") (hash "1sqir087zl580phjy7mly2h889ya4rnv7ixgnhif3srzm9mkby7y")))

(define-public crate-air-utils-0.3 (crate (name "air-utils") (vers "0.3.0") (hash "0raip18j0bkmg94fgx2l21fkzhglxnb6srlc3hwid8rwcr3by19p")))

