(define-module (crates-io ai -w) #:use-module (crates-io))

(define-public crate-ai-workshop-0.1 (crate (name "ai-workshop") (vers "0.1.0") (hash "09xrs97dm83jwjdbxwgrkfzsbf0lhrkykvfp0bpqianxs5bsgdcl")))

(define-public crate-ai-write-0.1 (crate (name "ai-write") (vers "0.1.0") (deps (list (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f05411k8k5wc2x7li0swqafb8xy57iby524rs4v7xvrrdn6vs5c")))

(define-public crate-ai-write-0.1 (crate (name "ai-write") (vers "0.1.1") (deps (list (crate-dep (name "openai_api_rust") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yz3jwrvzrl0psa4ijdj8qb84zj7fp6ppgc76ygajjy9q4abbvi6")))

