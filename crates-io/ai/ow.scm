(define-module (crates-io ai ow) #:use-module (crates-io))

(define-public crate-aiowrap-0.1 (crate (name "aiowrap") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xsa0558989fswy1bg65gag0vgykz4id14a44qs0r3xjzwivyxbf")))

