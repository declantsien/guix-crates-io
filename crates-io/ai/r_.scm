(define-module (crates-io ai r_) #:use-module (crates-io))

(define-public crate-air_main-0.0.1 (crate (name "air_main") (vers "0.0.1") (deps (list (crate-dep (name "airlang") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wl6jm0hjvlwssij45lmfwskhv4qr14znqxlhcm09vdsxmwgyvpp") (rust-version "1.60")))

