(define-module (crates-io ai ra) #:use-module (crates-io))

(define-public crate-airac-0.1 (crate (name "airac") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "10i24gwf3hb5hja6fq1mkpx5gjf9il4ckqpxwvmak3n0mp175mp1")))

(define-public crate-airac-0.1 (crate (name "airac") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1gf46cjb3hk4d9wizj17zq3x22rry94shijwv3n6i4524g26ndp6")))

(define-public crate-airagi-0.0.1 (crate (name "airagi") (vers "0.0.1") (deps (list (crate-dep (name "airlang") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "airlang_ext") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "1jy860a5g6byb1rv5phy2gf45xw26q13ypw7f9yq5hfq4yfjis11")))

(define-public crate-airagi-bin-0.0.1 (crate (name "airagi-bin") (vers "0.0.1") (deps (list (crate-dep (name "airagi") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "07khgnqv6dl9saqqlz06d3kg2bgqjrg0n9qidrp2424ad7j9n8l2")))

