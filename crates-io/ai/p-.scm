(define-module (crates-io ai p-) #:use-module (crates-io))

(define-public crate-aip-filter-0.0.0 (crate (name "aip-filter") (vers "0.0.0") (hash "0zpb9r78rhvmb398b7p7477j9a13msyqlg700nhp3jw70bdlpgi4")))

(define-public crate-aip-filtering-0.1 (crate (name "aip-filtering") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest-ast") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "0jzc38lag3m4ksa81lsrx12hzp9bycjnfn2bmyy4brxkv4az0l3b")))

