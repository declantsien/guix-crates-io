(define-module (crates-io ai da) #:use-module (crates-io))

(define-public crate-aidanhs-tmp-parse-generics-shim-0.1 (crate (name "aidanhs-tmp-parse-generics-shim") (vers "0.1.3") (deps (list (crate-dep (name "parse-generics-poc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 2)))) (hash "19n318dx6z0fww9ymh0mp4r0sdxx82sm781m986g1xybxin4x6cv") (features (quote (("use-parse-generics-poc" "parse-generics-poc"))))))

(define-public crate-aidantestund-0.1 (crate (name "aidantestund") (vers "0.1.0") (hash "1jqi0g0ksjnmxi5gnkvs0cb5mbcl398xqn5hzy2nj4qz80lcla8z") (features (quote (("transfer_charge_accounts" "aidanhs") ("aliases" "USD") ("aidanhs") ("USD"))))))

