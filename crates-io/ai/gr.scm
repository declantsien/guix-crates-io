(define-module (crates-io ai gr) #:use-module (crates-io))

(define-public crate-aigraph1-0.1 (crate (name "aigraph1") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "02nwx4ad64fbb831jrnb33pz8w034qz9r6z7vn715dcz5dxq90wy")))

