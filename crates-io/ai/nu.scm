(define-module (crates-io ai nu) #:use-module (crates-io))

(define-public crate-ainu-utils-0.1 (crate (name "ainu-utils") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "11cdr8vsjj9s6mmhqr1qnkclz82k4idnpbwvm4dgjfsrx1iq487s")))

(define-public crate-ainu-utils-0.2 (crate (name "ainu-utils") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0jxvb0sy2hirkkm35qsgs4brcawnyia5a3abb2y7xj390ig4dndg")))

