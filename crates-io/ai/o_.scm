(define-module (crates-io ai o_) #:use-module (crates-io))

(define-public crate-aio_event-0.1 (crate (name "aio_event") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("sync"))) (default-features #t) (kind 0)))) (hash "0qjdsqgyyphvnw8hidp275w7fngjf026gvabxff8hkrgldvgk863")))

