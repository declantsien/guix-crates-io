(define-module (crates-io ai ri) #:use-module (crates-io))

(define-public crate-airinfo-0.1 (crate (name "airinfo") (vers "0.1.0") (deps (list (crate-dep (name "btleplug") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "09yy100fynqz5csdjk6abziss9sfh30q6whawmps2ipz1vmjvi7b")))

(define-public crate-airis-0.0.0 (crate (name "airis") (vers "0.0.0") (deps (list (crate-dep (name "airs-layers") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "airs-types") (req "0.0.*") (default-features #t) (kind 0)))) (hash "025jr1ddf1i1plljsfxybc5mkw6i0gjqj4fd2z3rqr4f23zzncbj") (features (quote (("default"))))))

(define-public crate-airis-layers-0.0.0 (crate (name "airis-layers") (vers "0.0.0") (deps (list (crate-dep (name "airs-types") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "02ckr8jgrdns7bpi1v2k01vcm61fymf6d3ra3s2zy612f4ri444h") (features (quote (("default"))))))

(define-public crate-airisu-0.1 (crate (name "airisu") (vers "0.1.0") (hash "0s37ap2xj08phhdrydxabr76knyq3h3xrdyf89w3a707xknabg68")))

