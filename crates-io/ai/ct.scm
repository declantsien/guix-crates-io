(define-module (crates-io ai ct) #:use-module (crates-io))

(define-public crate-aict-0.1 (crate (name "aict") (vers "0.1.0") (hash "1nrp671az4npqj4vanj51dwy0fc128m5jad69fg9kb6rwdqd24hl")))

(define-public crate-aict-0.2 (crate (name "aict") (vers "0.2.0") (hash "0kh74fmrk3m2hc6m70pcms39bfpx0qvqgcfzh8rh21j4d28gzhd7")))

