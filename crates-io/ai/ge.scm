(define-module (crates-io ai ge) #:use-module (crates-io))

(define-public crate-aigen_macros-0.1 (crate (name "aigen_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1q06n6s4bdlrlqad0m9chp0g7cv3h2wrhfkn57snvm899ckj9qqa")))

(define-public crate-aiger-0.1 (crate (name "aiger") (vers "0.1.0") (hash "1bdmr8kq15f0kp7b5khyxvx7qqh05xjzyjym4wc8239m6vvahnaj")))

(define-public crate-aiger-0.1 (crate (name "aiger") (vers "0.1.1") (hash "0k0abnakz50zgv6w9ivsmpxmhzwn78q68xc8p36wm1z9cph8fjxn")))

(define-public crate-aiger-0.2 (crate (name "aiger") (vers "0.2.0") (hash "0ilzd9pqd9ssrlb6fxfnaza014mvzdn7y69rbqhlc7qw0cd9hk7x")))

