(define-module (crates-io ai ts) #:use-module (crates-io))

(define-public crate-AitSar-0.1 (crate (name "AitSar") (vers "0.1.1") (deps (list (crate-dep (name "shaderc") (req "^0.6.2") (default-features #t) (kind 1)) (crate-dep (name "winit") (req "^0.22") (default-features #t) (kind 2)))) (hash "1dxkxayha8wpyq8awnqpmsws4a4vmxlnif3qmz849pd0jsvzzzdp")))

