(define-module (crates-io ai rc) #:use-module (crates-io))

(define-public crate-aircontrol-1 (crate (name "aircontrol") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^2.5.1") (default-features #t) (kind 0)))) (hash "1xav2z8amijqbpw65wbpf02sn921hlcqkfv6vhlkba3q8d22ykar")))

(define-public crate-aircraft_icao_country-1 (crate (name "aircraft_icao_country") (vers "1.0.0") (hash "1h9cprw907iq8p4f5nzqp3swsm1fvq74kzwcvhc2djv3a6sbqgzz")))

(define-public crate-aircraft_icao_country-1 (crate (name "aircraft_icao_country") (vers "1.0.1") (hash "1k9bxmc3i7nglh9fgyf2zb4slcmg2qm41a1jz1q5j8hhk3zlx5dl")))

(define-public crate-aircraft_icao_country-1 (crate (name "aircraft_icao_country") (vers "1.0.2") (hash "1gbkvhl6q33gwj2ax48p857vmdi2yzj3hb6g05iy74kf8c9mg33k")))

(define-public crate-aircraft_icao_country-1 (crate (name "aircraft_icao_country") (vers "1.0.3") (hash "15axgkxww6mzh09prk4l0yq79gcijxwp902kb1n8f6nxm94025zf")))

(define-public crate-aircraft_icao_country-1 (crate (name "aircraft_icao_country") (vers "1.0.4") (hash "1s5cmc04biq0ks71184a03js89h4zd0w69cz1k5nzvpb2ghrghxi")))

