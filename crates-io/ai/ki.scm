(define-module (crates-io ai ki) #:use-module (crates-io))

(define-public crate-aikido-0.0.1 (crate (name "aikido") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "gitoxide") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0xyk8gwwzffvcma5zlxnsdgxvv0j7spzqbygls3ymsj6hfmxyxll")))

(define-public crate-aikit-0.0.0 (crate (name "aikit") (vers "0.0.0") (hash "18z77zm8qqvg94nr87ylgjdx0qs3jbjplsl1y6bzh0n5qqdgzvpk")))

(define-public crate-aikit-0.0.1 (crate (name "aikit") (vers "0.0.1") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)))) (hash "17wf0jf0ig2h6npk3ianxby4gy58dsfhvvc2wpniw6bkay42y141")))

(define-public crate-aikit-0.0.2 (crate (name "aikit") (vers "0.0.2") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)))) (hash "1yk577giz86g8dkrggmp9fiqiwfc1ybij5szk3vszbgv2x0xnlkq")))

