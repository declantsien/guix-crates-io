(define-module (crates-io ai nt) #:use-module (crates-io))

(define-public crate-aint-0.1 (crate (name "aint") (vers "0.1.0") (deps (list (crate-dep (name "num-integer") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "18rqlyd9kpqqjmvnr2gyqmzbamk7vhmx083xj5gjqn95n365z935") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde") ("num" "dep:num-traits" "dep:num-integer"))))))

(define-public crate-aintnobody-0.0.0 (crate (name "aintnobody") (vers "0.0.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (features (quote ("minimp3"))) (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "14i31dldmn7crfpwwqqxik62nwmdby189bx0dxydzzmx9bh83nb1")))

(define-public crate-aintnobody-1 (crate (name "aintnobody") (vers "1.0.1") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (features (quote ("minimp3"))) (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "1vics7adlxh3mrwmwwbbkdvqh3x5y05h4qf2ajd0cabps2zgrs6b")))

(define-public crate-aintnobody-1 (crate (name "aintnobody") (vers "1.1.1") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (features (quote ("minimp3"))) (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "1z8hkkjkr151qjc8d7w2rzjsql4z2n46dghq4pfzza7lxhd5ciab")))

