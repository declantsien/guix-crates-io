(define-module (crates-io ai #{21}#) #:use-module (crates-io))

(define-public crate-ai21-0.1 (crate (name "ai21") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "07g0g9q94glqfbclkbqwijq98bgdx3q7vzqsbby5hcvnakn25dnw")))

(define-public crate-ai21-0.1 (crate (name "ai21") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "0id9ys4xahvcmg8disa8k2azxgmxsk9pj1y6bw4k4qfh6m6l7rhd")))

(define-public crate-ai21-0.1 (crate (name "ai21") (vers "0.1.1-ohno") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1yw0q2cn3cz9pczw7ipx0c9l2syhqpw3lgl3zv8hk3qwaigjbzin")))

(define-public crate-ai21-0.1 (crate (name "ai21") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "0k7k9lp00h9dswf96p40gacvsvvmp77y180c0c4rsca3pk4yrcba")))

(define-public crate-ai21-0.1 (crate (name "ai21") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt"))) (default-features #t) (kind 0)))) (hash "0zh1p8ff69q8jpz0mp1g2mxkalg8wirlwx6j5kk0szvia40yhrii")))

(define-public crate-ai21-0.1 (crate (name "ai21") (vers "0.1.4") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt"))) (default-features #t) (kind 0)))) (hash "1c6fak6fx56ck3y13fm8b6kr321j1khhdwihpphp9dhf515cfp7x")))

