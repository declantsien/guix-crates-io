(define-module (crates-io ai id) #:use-module (crates-io))

(define-public crate-aiid-0.0.1 (crate (name "aiid") (vers "0.0.1") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "reed-solomon") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "05ywz95ilg5x1d17v9xfqqia6v2ak5acp9vip0bbaa6pvbprr92h")))

(define-public crate-aiid-0.0.2 (crate (name "aiid") (vers "0.0.2") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "reed-solomon") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0vzrzhi21525sf4x9m0n4js19nf0i9288pp7c61j4714h53z4gra")))

(define-public crate-aiid_js-0.0.1 (crate (name "aiid_js") (vers "0.0.1") (deps (list (crate-dep (name "aiid") (req "=0.0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "=0.2.40") (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "=0.4.4") (default-features #t) (kind 0)))) (hash "0xqvan1rpicgj9rf6dwpcw2j0pn55l3xnv2flwyr80hl242rcs9p")))

