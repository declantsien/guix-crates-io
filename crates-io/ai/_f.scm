(define-module (crates-io ai _f) #:use-module (crates-io))

(define-public crate-ai_function-0.1 (crate (name "ai_function") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zlj1zmnmhq1g6f865bn5ik56lhdhrnfvnp10px0c9yczkz80ddx")))

(define-public crate-ai_functions-0.1 (crate (name "ai_functions") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1aa21hags950pl8vvn2yz1xmkk07kv7z03cff9q7f2yzpcxnfhd7")))

(define-public crate-ai_functions-0.1 (crate (name "ai_functions") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06yisgp6ngswpbasdlxazx0nb7lipwarsvv7adn3lh07gqi4j5li")))

