(define-module (crates-io ai ur) #:use-module (crates-io))

(define-public crate-aiur-0.0.1 (crate (name "aiur") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winerror" "winuser" "consoleapi" "winnls" "processenv" "winbase" "sysinfoapi" "synchapi" "impl-default"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1fwhb7lxirq99vrimjnlscni21pp4ysbyx9383z928h50fvxfqwa")))

(define-public crate-aiur-0.0.2 (crate (name "aiur") (vers "0.0.2") (hash "0pi4a70i0rj9q2zva77xx9jrm1mhh6v8n3dvc60w1pq3ds5b4sxv")))

(define-public crate-aiur-0.0.3 (crate (name "aiur") (vers "0.0.3") (hash "1wjy68v5vksry1j113lsjbxfzb4rc71yv2cm8s569hqf5z8agxx1")))

(define-public crate-aiur-0.0.5 (crate (name "aiur") (vers "0.0.5") (hash "0w2h31ynpz06kf3fxccilb77p40p55yz8v1h79i1iy7b3adlmhi9")))

(define-public crate-aiur-0.0.6 (crate (name "aiur") (vers "0.0.6") (hash "1idkp4h4a7hlx4y4gxlr24z7q5f69niriywg2lh8kp7v4jz4vfjd")))

(define-public crate-aiur-0.0.7 (crate (name "aiur") (vers "0.0.7") (hash "0xq2d0fi9ikyvvi4i0p86pdbhxrnyz2pdqm3sai2nm0inc9cgjjm")))

(define-public crate-aiur-0.0.8 (crate (name "aiur") (vers "0.0.8") (hash "1gmyrizflwrgqqrf2dhl3xzjj0c0cyclf5rb492w1k8vkjwdvm3b")))

