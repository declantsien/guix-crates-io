(define-module (crates-io ai rp) #:use-module (crates-io))

(define-public crate-airport-0.1 (crate (name "airport") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1jgdfagicrzav5vz1l0rq2bx647d61jzv3lhwgbdpv8dyvxb7nra")))

(define-public crate-airport-0.1 (crate (name "airport") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1dhvww10dhablipaw0ngadizz6p0fpd5vyjf8gaxsb1337wq2455")))

(define-public crate-airport-0.1 (crate (name "airport") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "04y7kmk1gf7cp76hffjlqzzlmgz5rg82rjaybbcfxw44plkls6rb")))

(define-public crate-airports-0.0.1 (crate (name "airports") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "1h98vlp9libn4xfyvnmbaw7sfwayrkbqz7wi9mb7fbsch098b73m")))

(define-public crate-airports-0.0.2 (crate (name "airports") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "1d39avjal6671vylaricqj01fkczrzr8943f069dcwzwvwiinv30")))

