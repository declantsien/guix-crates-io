(define-module (crates-io en fo) #:use-module (crates-io))

(define-public crate-enforce-0.0.1 (crate (name "enforce") (vers "0.0.1") (deps (list (crate-dep (name "stainless") (req "> 0.0.0") (default-features #t) (kind 0)))) (hash "12rdpnq71lj747cjwzi15sl2p5yzd8xv3wxmxlvv0dgz1bfnr8yi")))

(define-public crate-enforcer_backend-0.1 (crate (name "enforcer_backend") (vers "0.1.0") (deps (list (crate-dep (name "psutil") (req "^3.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "1jxkyibc53fzfjy2336xglzdq3358zr5n0ns0b4vkzsvvzwq1rl1")))

(define-public crate-enforcer_backend-0.1 (crate (name "enforcer_backend") (vers "0.1.1") (deps (list (crate-dep (name "psutil") (req "^3.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "1zrhd8w6zg0gynmrs5b38ms5bkfx0q5jnszyyc6xxrrzd9jicscp")))

(define-public crate-enforcer_backend-0.1 (crate (name "enforcer_backend") (vers "0.1.2") (deps (list (crate-dep (name "psutil") (req "^3.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0bj2fqjh0fqy16s3nailw6zcpikg71cnk8hkms9q3n1w3jygs99s")))

