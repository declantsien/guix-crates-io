(define-module (crates-io en #{-3}#) #:use-module (crates-io))

(define-public crate-en-300-468-reader-0.1 (crate (name "en-300-468-reader") (vers "0.1.0") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mpeg2ts-reader") (req "^0.13") (default-features #t) (kind 0)))) (hash "0p3h1iqg9qiy5dxigkkqn3fg3dvzv2c63pblibw50cmk0yll03by")))

(define-public crate-en-300-468-reader-0.2 (crate (name "en-300-468-reader") (vers "0.2.0") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mpeg2ts-reader") (req "^0.14") (default-features #t) (kind 0)))) (hash "0qlnwnr53agpz25d0s9brdry6mjl229bxbpm1nz2424q523cj7l7")))

(define-public crate-en-300-468-reader-0.3 (crate (name "en-300-468-reader") (vers "0.3.0") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mpeg2ts-reader") (req "^0.15") (default-features #t) (kind 0)))) (hash "10gik6a1pj322nnfykpzz90jq162pq9j57bhkjb01dr1dn5db94v")))

