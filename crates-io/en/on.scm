(define-module (crates-io en on) #:use-module (crates-io))

(define-public crate-enontekio-0.1 (crate (name "enontekio") (vers "0.1.0") (hash "13j0qf53vlkz8d80vfyblnz7aingscdi0421q7ycm1vn1m7rg8v0")))

(define-public crate-enontekio-0.1 (crate (name "enontekio") (vers "0.1.1") (hash "0rbwpjkdvi9pqh3fv6lg1lp715q8zi5zyfh94w4x0q5590cirzkb")))

(define-public crate-enontekio-0.1 (crate (name "enontekio") (vers "0.1.2") (hash "1l5g0gr79rbj2vqns8fy2hwdcnbic7jlklgvb8pmpbj4bhk4q6qc")))

(define-public crate-enontekio-0.1 (crate (name "enontekio") (vers "0.1.3") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)))) (hash "0yaqx2l50qxzljy8m9l5h3gwzpx3a8fqcfslq4lhamiar5qax1qn") (yanked #t)))

(define-public crate-enontekio-0.2 (crate (name "enontekio") (vers "0.2.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)))) (hash "1b1iyygrr99w53csknm10bfjzfz59r6bkwwnyvzvl4lk0814fizj")))

