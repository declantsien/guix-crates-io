(define-module (crates-io en tt) #:use-module (crates-io))

(define-public crate-entt-0.0.1 (crate (name "entt") (vers "0.0.1") (hash "027yff0mxy3r6iivjad5fsh0z2iaw597rfyfr35zfi9x7k1himxg")))

(define-public crate-enttecopendmx-0.1 (crate (name "enttecopendmx") (vers "0.1.0") (deps (list (crate-dep (name "libftd2xx") (req "~0.31.0") (features (quote ("static"))) (default-features #t) (kind 0)))) (hash "1xmwxc1vkskga4v9115krrnyl965mj6b5phxbr14xyzizxgsr0hp")))

(define-public crate-enttecopendmx-0.1 (crate (name "enttecopendmx") (vers "0.1.1") (deps (list (crate-dep (name "libftd2xx") (req "~0.31.0") (features (quote ("static"))) (default-features #t) (kind 0)))) (hash "1id0ia145cfy9cr2bkaaxrv9y5f5cln78mc1hf05rmdlcxhmm6s3")))

