(define-module (crates-io en v4) #:use-module (crates-io))

(define-public crate-env4rs-0.1 (crate (name "env4rs") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0cj1bz7hc32crp9vlxia9v8fdlpbzlj1cvvw8g3b3qs6s91lwq8l")))

