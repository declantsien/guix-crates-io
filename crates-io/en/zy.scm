(define-module (crates-io en zy) #:use-module (crates-io))

(define-public crate-enzyme-0.1 (crate (name "enzyme") (vers "0.1.0") (hash "1ig130ky81s878907cgdfpm4kd2cn8ljqkxbfakq1x6xsdlnb5rj")))

(define-public crate-enzyme-0.2 (crate (name "enzyme") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (kind 0)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (kind 0)))) (hash "0097lrs79r9w6hv4aj5nqr0hsbf9x74ygpdcvf07il2fm467xb7h")))

(define-public crate-enzyme-0.3 (crate (name "enzyme") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (kind 0)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (kind 0)))) (hash "0vghff3jcly69dqcz2h3306v2zv2s7na66f6biqxf1c28ynpl75v")))

(define-public crate-enzyme-0.4 (crate (name "enzyme") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (kind 0)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (kind 0)))) (hash "1gckjfcvpf5yzcylbjnq2fhh2dvh54h0s0s6md49yi69d1crvs9r")))

