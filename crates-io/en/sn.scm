(define-module (crates-io en sn) #:use-module (crates-io))

(define-public crate-ensnare-0.1 (crate (name "ensnare") (vers "0.1.0") (hash "1jmbbvbzgirb6r5vshnkb5yrbs15xgbxvq0ylqnw7r5vrdrjiklg") (yanked #t)))

(define-public crate-ensnare-0.0.1 (crate (name "ensnare") (vers "0.0.1") (hash "1p4619ni35m8sn51g2wr5czzjx50w0gbwhvfscm7bvi2rhy3f8nd") (yanked #t)))

(define-public crate-ensnare-0.0.2 (crate (name "ensnare") (vers "0.0.2-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0jhcydmgbv0274l5l6c32syzkynikxjfrn60igyx6jfk4yq05a11") (features (quote (("std" "getrandom/std") ("default" "std"))))))

