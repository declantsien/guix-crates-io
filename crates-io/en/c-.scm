(define-module (crates-io en c-) #:use-module (crates-io))

(define-public crate-enc-check-0.1 (crate (name "enc-check") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1x9k2dwaqfgly1m1chzmayq1vcd1ssiadv4r4dnq8hyj70kxcmba")))

(define-public crate-enc-check-0.1 (crate (name "enc-check") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "052ly4sxifcqlgcv739gdg22f9jxqqcqqa08j5apcq7jjkcw4xma")))

