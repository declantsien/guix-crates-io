(define-module (crates-io en vl) #:use-module (crates-io))

(define-public crate-envload-0.1 (crate (name "envload") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "envload_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1czi4qpzfzycyvqskjajpprq6im073sy1mqsz0475kd6firy5p67")))

(define-public crate-envload-0.1 (crate (name "envload") (vers "0.1.1") (deps (list (crate-dep (name "envload_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1agyp7j7dvqf9ffmm11l50i26a0aypw8rvcs6qksbz9790nnyy6n")))

(define-public crate-envload_derive-0.1 (crate (name "envload_derive") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.95") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "13cl8z9nfh4bk780dhbngaz37wm8r36dmsab2dyaq8xdzcnqkmq5")))

(define-public crate-envload_derive-0.1 (crate (name "envload_derive") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.95") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1w2nkc2sqjy11m96zfga2xp0pm6hkki710wc6vh8cxgjzyfhhn71")))

(define-public crate-envloader-0.1 (crate (name "envloader") (vers "0.1.0") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0xjm6xc9ap4mlpm8k65aa8x37q9v25ykxlim76z58f2i3kswwjpq")))

(define-public crate-envloader-0.1 (crate (name "envloader") (vers "0.1.1") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1fvy20fz8kl9f1a2q053zxkazbdd98kan9lqx7inw95jq1nws6fs")))

(define-public crate-envloader-0.1 (crate (name "envloader") (vers "0.1.2") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "045s8az8zcqw1w25vjmmnkvd90hhhbb6avxmqri5m7wrg6h29f4m")))

(define-public crate-envloader-0.1 (crate (name "envloader") (vers "0.1.3") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0hlixlbaqvjicjx5rkzhn0jj83bplyb6s9h0i2gn6sayk00m0wy6")))

