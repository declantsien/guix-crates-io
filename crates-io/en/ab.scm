(define-module (crates-io en ab) #:use-module (crates-io))

(define-public crate-enable-1 (crate (name "enable") (vers "1.1.0") (hash "1pw6dkmhnabjyhgc985kg6rscw9514jnw175k1pz9z7hk488q240")))

(define-public crate-enable-ansi-support-0.1 (crate (name "enable-ansi-support") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0d0mlaml513dqgfrwvm9zpkq3amkgl3j5cv7fk8lc86z67yb0vfj")))

(define-public crate-enable-ansi-support-0.1 (crate (name "enable-ansi-support") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "013hphfgkr5inx16lbabj7x1jz30sw5d25rik468xw0gabf8wv0n")))

(define-public crate-enable-ansi-support-0.1 (crate (name "enable-ansi-support") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1arinv5cxlq99dww036321nw3s3kyg3pp3bz84vc655sm4y9vli8")))

(define-public crate-enable-ansi-support-0.2 (crate (name "enable-ansi-support") (vers "0.2.1") (deps (list (crate-dep (name "windows-sys") (req "^0.42.0") (features (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_Security" "Win32_System_Console"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0q5wv5b9inh7kzc2464ch51ffk920f9yb0q9xvvlp9cs5apg6kxa") (rust-version "1.49")))

