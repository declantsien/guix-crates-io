(define-module (crates-io en st) #:use-module (crates-io))

(define-public crate-enstate-0.1 (crate (name "enstate") (vers "0.1.0") (hash "1wp5z9rrmcnzjw1pd29xascpa3f1gnpcsggngyrnn6kkx4p0qz0n")))

(define-public crate-enstream-0.1 (crate (name "enstream") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0.3.21") (kind 0)) (crate-dep (name "pin-project") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "pinned-aliasable") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "03ngzbm8l8r6i7brwc15bs3g7lh7w7yxbmzxagwwrvwnjnqry5ki") (yanked #t)))

(define-public crate-enstream-0.2 (crate (name "enstream") (vers "0.2.0") (deps (list (crate-dep (name "futures-util") (req "^0.3.21") (kind 0)) (crate-dep (name "pin-project") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "pinned-aliasable") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1yb4q9a85014amj31apdhvqaxc7cvbjwvqb69a70g2fir9zj739b") (yanked #t)))

(define-public crate-enstream-0.2 (crate (name "enstream") (vers "0.2.1") (deps (list (crate-dep (name "futures-util") (req "^0.3.21") (kind 0)) (crate-dep (name "pin-project") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "pinned-aliasable") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0mq23j8zizx727qrb31l58b6q2v9s45gq1lqgj2dhdnc8s9pd500") (yanked #t)))

(define-public crate-enstream-0.3 (crate (name "enstream") (vers "0.3.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.21") (kind 0)) (crate-dep (name "futures-util") (req "^0.3.21") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "pinned-aliasable") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "07iib23da10x8mmv84lahncinw3hvvsgl7mkc5lvz1p4zdrjv7r7")))

