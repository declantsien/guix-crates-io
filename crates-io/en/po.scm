(define-module (crates-io en po) #:use-module (crates-io))

(define-public crate-enpow-1 (crate (name "enpow") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0vvminjgx5cg6vxbayb62vigb2ri11ax6jxjdpj5rzl8m9rcap6d")))

(define-public crate-enpow-1 (crate (name "enpow") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0c12y04mm1a411igh9b1yz9v300g26437fx9j0c7c2hnzx6fgxvs")))

(define-public crate-enpow-1 (crate (name "enpow") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "04bc9k83irwjir0iidp3hpg0amgbg10ls1j4h8yn4q3rsdxkrk3g")))

(define-public crate-enpow-2 (crate (name "enpow") (vers "2.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "1n2zdd4y0k5n4x898hmf6v3dabrd42swpl3dflmvm6yvdimd6xrj")))

(define-public crate-enpow-2 (crate (name "enpow") (vers "2.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0i77cz7vmkdp7li4lzyxx8bzd5i0r1vansjns4rhyppm2ad1sq7s") (rust-version "1.58")))

(define-public crate-enpow-2 (crate (name "enpow") (vers "2.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "1ykw49z2xjalw0w66b46lmar82z48r6ap5gdrm057apva2b5nwvr") (rust-version "1.58")))

