(define-module (crates-io en c2) #:use-module (crates-io))

(define-public crate-enc28j60-0.1 (crate (name "enc28j60") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (kind 0)) (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.2") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1r554wgi339qy43z5d27kn1ciann4644b0cmap3491ndicz16i9q")))

(define-public crate-enc28j60-0.2 (crate (name "enc28j60") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (kind 0)) (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1gpwd8bs9w4kn8jwzidwi3pqar886480f0j1dkgmzpnvcyll8bxb")))

(define-public crate-enc28j60-0.2 (crate (name "enc28j60") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (kind 0)) (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1q8684wwkcs91irhdchw9w8157w2sdz6003kill75lynqk2rvi28")))

