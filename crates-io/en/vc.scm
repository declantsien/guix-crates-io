(define-module (crates-io en vc) #:use-module (crates-io))

(define-public crate-envcache-0.1 (crate (name "envcache") (vers "0.1.0") (hash "0qalfwpb8a0swhdlj80xcv2yk3sw38v1lqz41bb83y6xyzqvcdl9")))

(define-public crate-envcache-0.1 (crate (name "envcache") (vers "0.1.1") (hash "0h9s2nl3lb8y3dwggbms02zrkrjb2hc8zkwn6h90wayvdg5j65rq")))

(define-public crate-envcache-0.1 (crate (name "envcache") (vers "0.1.2") (hash "14681k3sxx4ijw2idzcfh0yilybpjacm7yak4hgisy0ry5acwzf6")))

(define-public crate-envcache-0.1 (crate (name "envcache") (vers "0.1.3") (hash "0fvfib9nj3q092vb9q1gsiq60k3m3fcifzd77by39m6yavyv7hxn")))

(define-public crate-envch-0.1 (crate (name "envch") (vers "0.1.0") (deps (list (crate-dep (name "shellexpand") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1wp4njs9x32i0yq3v3df44m793q0xqrklcx7m7dilbdgdr12scls")))

(define-public crate-envch-0.1 (crate (name "envch") (vers "0.1.1") (deps (list (crate-dep (name "shellexpand") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "09d8rr8vcna6xj7ylj8mlxw2mj14iyifdkcsbihg1kqz4klhjz2z")))

(define-public crate-envchain-0.1 (crate (name "envchain") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "secret-service") (req "^3") (features (quote ("rt-tokio-crypto-rust"))) (default-features #t) (kind 0)))) (hash "0lb979a3qk6vyhskiynhvhsnvhjwg7l5ly3n4nw5ab286asrb3ll")))

(define-public crate-envchain-rs-0.1 (crate (name "envchain-rs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "secret-service") (req "^3") (features (quote ("rt-tokio-crypto-rust"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1afdbaf0qk8s29688hl2kl808zpm83ndxqka2p2pbvpy1ac2f79b")))

(define-public crate-envchain-rs-0.1 (crate (name "envchain-rs") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "secret-service") (req "^3") (features (quote ("rt-tokio-crypto-rust"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "10hp8b351bh9w6c3iwf27anh2r9lfy0y3jdm17pxw2g5768bg5gg")))

(define-public crate-envchain-rs-0.1 (crate (name "envchain-rs") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "secret-service") (req "^3") (features (quote ("rt-tokio-crypto-rust"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1gh0cpwbkrzrv185dxdrz2py3z2q294h00mv2gc87vzhryyaylv5")))

(define-public crate-envchain-rs-0.1 (crate (name "envchain-rs") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "secret-service") (req "^3") (features (quote ("rt-tokio-crypto-rust"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1pl1jfmc3qr1kd9ypimw6nzq4l6glfh9x450fdd9h3pfmc6rw7wg")))

(define-public crate-envconf-0.1 (crate (name "envconf") (vers "0.1.0") (deps (list (crate-dep (name "envconf_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "140cw0vkd96355np3bnwkan1f18ipqpzyngsiisk31936y79yn47")))

(define-public crate-envconf-0.1 (crate (name "envconf") (vers "0.1.1") (deps (list (crate-dep (name "envconf_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hm5kwr17b20nfp34rsdjgixa9asj320cdsjk7jv537nxa021wgs")))

(define-public crate-envconf_derive-0.1 (crate (name "envconf_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "18k590qm8jh5hpxkjz1rqwlm38sywxr77hd6z8kd0wprlnbsmddq")))

(define-public crate-envconfig-0.1 (crate (name "envconfig") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "01l1i9s4bw555jc6wbf7b5gbjmy1wzwkips3jxp4s0f8knbdjw77")))

(define-public crate-envconfig-0.2 (crate (name "envconfig") (vers "0.2.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0k44kflp84y62h9wnqgkj948nqavd7a45vl9zbvpg70kjqirzw6y")))

(define-public crate-envconfig-0.3 (crate (name "envconfig") (vers "0.3.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17nrskarqz7icjrl09ydpiaplgc6wvcw9vgwdwybpvrzwa8jffxw")))

(define-public crate-envconfig-0.4 (crate (name "envconfig") (vers "0.4.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1a1j0b0v8x26iq1ywag4gbfywpszd3q1p2bd571ri52c2cp3jllg")))

(define-public crate-envconfig-0.5 (crate (name "envconfig") (vers "0.5.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0cbfbddz7ibikalsi0y2ahg9jiick390xbq3q9lqknr8admz8q59")))

(define-public crate-envconfig-0.5 (crate (name "envconfig") (vers "0.5.1") (deps (list (crate-dep (name "envconfig_derive") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ibq27xb5l60fd7yffcm20v8q435qnbafgcin25rs4viygwr6hp8")))

(define-public crate-envconfig-0.6 (crate (name "envconfig") (vers "0.6.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0wb5d1d2c0ryc1a7y63pc77a8531nx9yy735w66z5rpd7mlq7b3w")))

(define-public crate-envconfig-0.7 (crate (name "envconfig") (vers "0.7.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "1p3b6kvmh7g8gr05ba62lllj25ib768gj5270w44dkydqbcb76ia")))

(define-public crate-envconfig-0.8 (crate (name "envconfig") (vers "0.8.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "0pcg22r1g7q6dp9s9bwpspql6pcsngs8rh7w9p3y7h60pp35niya")))

(define-public crate-envconfig-0.9 (crate (name "envconfig") (vers "0.9.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "1swgvx4nl1il1waqwbcgakg56xwrw9lwf4vh0p9ilgps5a4f60g8")))

(define-public crate-envconfig-0.9 (crate (name "envconfig") (vers "0.9.1") (deps (list (crate-dep (name "envconfig_derive") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1jmr39b9iw8zkrc33xsqywr0kcpi29fms5xb2nap02d91m3z03iw")))

(define-public crate-envconfig-0.10 (crate (name "envconfig") (vers "0.10.0") (deps (list (crate-dep (name "envconfig_derive") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "16xcpij5brxj4k5lf0x566zbzl3q9681cs7v3sdrsnpm45zcr0ga")))

(define-public crate-envconfig_derive-0.2 (crate (name "envconfig_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0g9mnq6w8k30ppp1nm2yrn8n5zk2y277jx6ixi8ncvb7pbi3hc7s")))

(define-public crate-envconfig_derive-0.3 (crate (name "envconfig_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "1lq773f57zk4p20v7czb4ckp0nxvb5ygg1n563d2dhmshk9s2lxq")))

(define-public crate-envconfig_derive-0.4 (crate (name "envconfig_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.4") (default-features #t) (kind 0)))) (hash "166733qsxv8fgjz9jyp2rhf0l262h2ai8y6855v3iidrp0693vkx")))

(define-public crate-envconfig_derive-0.5 (crate (name "envconfig_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.4") (default-features #t) (kind 0)))) (hash "0i9f3953ngl8jsz4x25425vgfgvsnysf355jy99yapj3a6lppdmw")))

(define-public crate-envconfig_derive-0.5 (crate (name "envconfig_derive") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.4") (default-features #t) (kind 0)))) (hash "0a32i326d0ki32jmm21x0951ayz7b5r3c0pi3vwwsik2llswzxiq")))

(define-public crate-envconfig_derive-0.6 (crate (name "envconfig_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.4") (default-features #t) (kind 0)))) (hash "0x26a8fkpprqadf2zfxb5r8x3ziw17afn38szjxswa3pvvwh3a7h")))

(define-public crate-envconfig_derive-0.7 (crate (name "envconfig_derive") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.4") (default-features #t) (kind 0)))) (hash "0ma9z2g19rdk52gqvk83gkymw6f7bhkxm55kjz815llwjd4ll35r")))

(define-public crate-envconfig_derive-0.8 (crate (name "envconfig_derive") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "05rgpdrv1n3hg6arqp96qzp7kli0ylk9rff5717v9k93l33wxdhb")))

(define-public crate-envconfig_derive-0.9 (crate (name "envconfig_derive") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "0dr4jr42m98sq5905l5zsvngysgfx55zy5ijn087cqfb65d9j5wq")))

(define-public crate-envconfig_derive-0.9 (crate (name "envconfig_derive") (vers "0.9.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "0h1lsxd8ybgx9rwi8wk8a56hdrpa3f8h29dw7mk2a3g7cq9m39rw")))

(define-public crate-envconfig_derive-0.10 (crate (name "envconfig_derive") (vers "0.10.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1gxr0kc2swhvix5w52wrjs7g20gsxdcggbyak98lajzqwmwa5z3x")))

(define-public crate-envcrypt-0.1 (crate (name "envcrypt") (vers "0.1.0") (deps (list (crate-dep (name "envcrypt-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "magic-crypt") (req "^3.1.10") (default-features #t) (kind 0)))) (hash "0ny36m80b4z15gakgbqzrs7qb9ggsvx6ff7flh3s1m6hm0dizmjb")))

(define-public crate-envcrypt-0.2 (crate (name "envcrypt") (vers "0.2.0") (deps (list (crate-dep (name "dotenvy") (req "^0.15.3") (default-features #t) (kind 2)) (crate-dep (name "envcrypt-macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "magic-crypt") (req "^3.1.10") (default-features #t) (kind 0)))) (hash "0jq2r26y6plqyaac6k9iv6c0mj4p159s2v8srrgr6qlgw0kidis4")))

(define-public crate-envcrypt-0.2 (crate (name "envcrypt") (vers "0.2.1") (deps (list (crate-dep (name "dotenvy") (req "^0.15.3") (default-features #t) (kind 2)) (crate-dep (name "envcrypt-macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "magic-crypt") (req "^3.1.10") (default-features #t) (kind 0)))) (hash "1q8qmcr8qnq0zd1xqbdjd2imdjvv0r2i4878n4crndrsm06njy4w")))

(define-public crate-envcrypt-0.3 (crate (name "envcrypt") (vers "0.3.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.3") (default-features #t) (kind 2)) (crate-dep (name "envcrypt-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "07x51rzzpfxh0hs8a064d360ggv88j33jp1jbszsxw3wsmx7y7pn") (yanked #t)))

(define-public crate-envcrypt-0.3 (crate (name "envcrypt") (vers "0.3.1") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.3") (default-features #t) (kind 2)) (crate-dep (name "envcrypt-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06ndpgjf79pwwygg7nm4c1g2r6pb1263lza997qh9by78b76n2r4") (yanked #t)))

(define-public crate-envcrypt-0.3 (crate (name "envcrypt") (vers "0.3.2") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.3") (default-features #t) (kind 2)) (crate-dep (name "envcrypt-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1s871ynl0qbnkh1pr3rv0ym4fdi90bb2bh9xcmvdflkf007i4wr6") (yanked #t)))

(define-public crate-envcrypt-0.4 (crate (name "envcrypt") (vers "0.4.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.3") (default-features #t) (kind 2)) (crate-dep (name "envcrypt-macro") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0igldbhmkcagbffyv00j3lsjhrxk5jsrf9sldmwhlm3ambbv7avk") (yanked #t)))

(define-public crate-envcrypt-0.5 (crate (name "envcrypt") (vers "0.5.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.3") (default-features #t) (kind 2)) (crate-dep (name "envcrypt-macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0zl50h0dc6r21cz40zlv2dly0zym027fvf24kmjyfa9qd49wvrg6")))

(define-public crate-envcrypt-macro-0.1 (crate (name "envcrypt-macro") (vers "0.1.0") (deps (list (crate-dep (name "magic-crypt") (req "^3.1.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0r860jpym1ql0l7b97v26bj341m6sfysbd3zp2ir1vkrklmdg4vf")))

(define-public crate-envcrypt-macro-0.2 (crate (name "envcrypt-macro") (vers "0.2.0") (deps (list (crate-dep (name "magic-crypt") (req "^3.1.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1fgfxzqmv4sk67rcy3f2rdfpj8km77cjpnj0bxz7kbpayh24d7ds")))

(define-public crate-envcrypt-macro-0.2 (crate (name "envcrypt-macro") (vers "0.2.1") (deps (list (crate-dep (name "magic-crypt") (req "^3.1.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15861xga0lxxp1qbc468j573h83ii3p7dhwsfgazf88bjhl6rpr8")))

(define-public crate-envcrypt-macro-0.3 (crate (name "envcrypt-macro") (vers "0.3.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "0a5vgaslyy6jfqa2ahcnha78f05l5x1g79bxg0w4wksya9368lbk") (yanked #t)))

(define-public crate-envcrypt-macro-0.3 (crate (name "envcrypt-macro") (vers "0.3.1") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "07p5jh0glm3x8bzsyg6p2wwppvsgdkc3l5dqnx0flzi3fm3c8wi3") (yanked #t)))

(define-public crate-envcrypt-macro-0.3 (crate (name "envcrypt-macro") (vers "0.3.2") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "0a2b93f87zwngw8v133a839rsblb5a8cwy9134f7698hlaav4mms") (yanked #t)))

(define-public crate-envcrypt-macro-0.4 (crate (name "envcrypt-macro") (vers "0.4.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "1csyr321xl9wrk8z41fm18drcwp4344k8kaz5d42l3fnqhmhgbd4") (yanked #t)))

(define-public crate-envcrypt-macro-0.5 (crate (name "envcrypt-macro") (vers "0.5.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "1ps2sk9wrrbllp4xc12233ghal2lklchp0bsfx48s2yl9m7n2cs1")))

(define-public crate-envctl-1 (crate (name "envctl") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^4.6.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12h117rkvf6d8qjvywjk8j5rc2qzpa32izcxxm4jwagz5ixawyzk")))

(define-public crate-envctl-1 (crate (name "envctl") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^4.6.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03c3k7wjnmqs85p639mwfhis6gr1nx7nnnw063nirhbjsj3bdkrl")))

