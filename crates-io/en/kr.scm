(define-module (crates-io en kr) #:use-module (crates-io))

(define-public crate-enkrypt-0.1 (crate (name "enkrypt") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0wj9xm9m0bkwry8cvjskk53h4kwybs8rpnfzzdnaa9rn8yb8vi0w")))

