(define-module (crates-io en by) #:use-module (crates-io))

(define-public crate-enby-0.1 (crate (name "enby") (vers "0.1.0") (hash "149n1qyxlpf95s0jk18nva0ig2622jh4jdh9bdizk4cr6cjiydzr") (yanked #t)))

(define-public crate-enby-0.0.0 (crate (name "enby") (vers "0.0.0") (hash "1jlnmb7z59q1jc16q9ihmh0kl19raldvhpbija2nrky42fnp86m7")))

