(define-module (crates-io en oc) #:use-module (crates-io))

(define-public crate-enocean-0.1 (crate (name "enocean") (vers "0.1.0") (deps (list (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1a9k6jam7g6n2a21f9mbpw4h9cyq417bq45wwh9i8kqmq4syy9jd")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0szi5x5wczcq92i0xnz1i2vrwj48zqzyn3r6frynkjar70d4j4c2")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1jhylpzbs3izb70ysf412jr7isy2g2k40iws80l8fz1wxki0im74")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0s6fl0hvl69zfc6cmn33wi0b0xd6jzc3053p3jgb2lkrfs482amg")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0nnq3rb8yzgsmv81bijnyj6vmjh5vlyfcm9a6378jbg0fbrgyjik")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1w9ngcydd4jax8ml90dc0ny14cqkb35qb3hwqldj68b544zq06c4")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "18333hfslyavzadhqrnmp9ms43kmb1n1wvjna9b67x8whxcx16xx")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.51") (deps (list (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1yb0k2w8zrfll2fhpbiikjkmw38w9qc81c710wa45nb2l3swr47d")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.52") (deps (list (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "181ngfrazkrqlxmhbi7qrv4ykcld0w4kjzwkmmixb5qyjavs8m8g")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.53") (deps (list (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "00cf9nj4pm66zbirjprn1240maq1gaw6xa4c985lcd5wsxbg5bbq")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.54") (deps (list (crate-dep (name "serialport") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1rl5l5hbfnacmh42jqaszvqg76r1y83hgcig3h8iv8g328b9rxny")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.56") (deps (list (crate-dep (name "serialport") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "03mg7305j5vgw8v8l97hng4zmwlc00kslkrzyb4ci9v15a079f46")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.57") (deps (list (crate-dep (name "serialport") (req ">=3.3.0, <4.0.0") (default-features #t) (kind 0)))) (hash "03jk9g3idf3my8r2598j5bpa221wx3ywnjdi7hgwig67bi5mn6ng")))

(define-public crate-enocean-0.2 (crate (name "enocean") (vers "0.2.58") (deps (list (crate-dep (name "serialport") (req ">=3.3.0, <4.0.0") (default-features #t) (kind 0)))) (hash "09yb8134wywvdba6fvnhxblnspnw16kln2ayxxklirwjd8mnj0sg")))

(define-public crate-enoceanesp-0.1 (crate (name "enoceanesp") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.129") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mnbwag66wk7qgcirh6ncnaynp12nhgx679wvx3f54d3jnanv2m2") (yanked #t)))

(define-public crate-enocoro128v2-0.1 (crate (name "enocoro128v2") (vers "0.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (features (quote ("zeroize_derive"))) (kind 0)))) (hash "1d8wfrki8mvm9pcf9myimd58yy7vdacld7kr14maabj16380jf9x")))

(define-public crate-enocoro128v2-0.1 (crate (name "enocoro128v2") (vers "0.1.1") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (features (quote ("zeroize_derive"))) (kind 0)))) (hash "0sawbzfgw0ypazjrnjbhvqrqwsa3pj40rf71jyvmaard4a05qf55")))

(define-public crate-enocoro128v2-0.1 (crate (name "enocoro128v2") (vers "0.1.2") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (features (quote ("zeroize_derive"))) (kind 0)))) (hash "0k2hfaw3r2iwad3kk3ahbi6dgrn69nqxrag3mahgs56mf2y0wd7f")))

(define-public crate-enocoro128v2-0.1 (crate (name "enocoro128v2") (vers "0.1.3") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (features (quote ("zeroize_derive"))) (kind 0)))) (hash "17smdpbikscx6x5iagqc1kj75zpqp0qxjazq9s6kkq5c9ya61a8l")))

(define-public crate-enocoro128v2-0.1 (crate (name "enocoro128v2") (vers "0.1.4") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (features (quote ("zeroize_derive"))) (kind 0)))) (hash "0ya7pb8dpac2zgh4i2q945r1qx4hijz4wiz3yfbmyacfa4q83abf")))

(define-public crate-enocoro128v2-0.1 (crate (name "enocoro128v2") (vers "0.1.5") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (features (quote ("zeroize_derive"))) (kind 0)))) (hash "1g42qrs16miz11fir1zps81xz4m4sqnfrjwbn7b5avmsayfqi62f")))

(define-public crate-enocoro128v2-0.1 (crate (name "enocoro128v2") (vers "0.1.6") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (features (quote ("zeroize_derive"))) (kind 0)))) (hash "0nlbghh4i6wipwj5jmim465r7v7yybwvjc7n2hl66cr6nkb6wvy3")))

