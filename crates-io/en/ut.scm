(define-module (crates-io en ut) #:use-module (crates-io))

(define-public crate-enutil-0.1 (crate (name "enutil") (vers "0.1.0") (deps (list (crate-dep (name "enutil_macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ay968yggn6jnc9hmlhz0hszi579k8y2z5szdr9vjwrnmna59r8v") (features (quote (("derive" "enutil_macros"))))))

(define-public crate-enutil_macros-0.1 (crate (name "enutil_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0lh090ykcypfxm44v86a2bv9ih4plzlvhbqlnx28fnxwqr94jcxv")))

