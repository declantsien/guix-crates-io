(define-module (crates-io en ch) #:use-module (crates-io))

(define-public crate-enchainte-merge-0.1 (crate (name "enchainte-merge") (vers "0.1.0") (deps (list (crate-dep (name "blake2b_simd") (req "^0.5.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^2.0.1") (features (quote ("keccak"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wsyzxw7jgz3lhcga2i51qyl6hvcgmsm6w6x6cazvd3lfk2l7r14") (features (quote (("keccak" "tiny-keccak") ("blake2b" "blake2b_simd"))))))

(define-public crate-enchant-0.1 (crate (name "enchant") (vers "0.1.0") (deps (list (crate-dep (name "enchant-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jwq019p7h37xqs810zg3lsk6gf36k30g1v3dwv494q4viygj3w1")))

(define-public crate-enchant-0.1 (crate (name "enchant") (vers "0.1.1") (deps (list (crate-dep (name "enchant-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jrx6aaqbpnknfdvickscyw1pkpiylq1kis2g496xy1512kpa2xf")))

(define-public crate-enchant-0.2 (crate (name "enchant") (vers "0.2.0") (deps (list (crate-dep (name "enchant-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j9p6kjvdzdi7q2v6i1i0fhjrprjkpxnq95z4dsxwyycjyc4i44c")))

(define-public crate-enchant-0.3 (crate (name "enchant") (vers "0.3.0") (deps (list (crate-dep (name "enchant-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ns4fr9hn8xdpaf78lk94kr90n0j65f0k6nqz463p4zmky7h3zfs")))

(define-public crate-enchant-sys-0.1 (crate (name "enchant-sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0cd9g3gbnbzzglsqg6k18pn2943cnz18999gr8pjkabgb8r4nd9w")))

(define-public crate-enchant-sys-0.2 (crate (name "enchant-sys") (vers "0.2.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0n140gbb00g496y49jx3bzhp2pfgzcxa8k3wi3gw5kgwxdh35p0f") (links "enchant")))

(define-public crate-enchant-sys-0.2 (crate (name "enchant-sys") (vers "0.2.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0kx3kfjzybrs5z5fan0bwqi46wsi430kvwlqamlig8nqlvvmkbwp") (links "enchant")))

(define-public crate-enchode-0.1 (crate (name "enchode") (vers "0.1.0") (deps (list (crate-dep (name "libchode") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zs1nn0725nbr8pj9hhdi5w0g8v2pr4vx7ngcc2a7kydwjwn46b8")))

