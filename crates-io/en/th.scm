(define-module (crates-io en th) #:use-module (crates-io))

(define-public crate-entheogen-0.1 (crate (name "entheogen") (vers "0.1.0") (deps (list (crate-dep (name "alga") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "cairo-rs") (req "^0.6") (features (quote ("png"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.4") (default-features #t) (kind 0)))) (hash "0lj4z45m1q8s3rw7i6js45qyz9cbbfikmjsizbc38q1qkm5bn53n")))

