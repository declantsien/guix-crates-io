(define-module (crates-io en vg) #:use-module (crates-io))

(define-public crate-envgrep-0.1 (crate (name "envgrep") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0zxq8md7fix7s8q4yin10igrpbkrp60wm0g2xpwsc21jlcl1i6l6")))

