(define-module (crates-io en re) #:use-module (crates-io))

(define-public crate-enrede-0.1 (crate (name "enrede") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.16") (features (quote ("derive" "must_cast"))) (default-features #t) (kind 0)))) (hash "16zl846fwn60jk5lq3m0r72jyqama15a228gvrxq8hb7iq4l81r2")))

(define-public crate-enrede-0.1 (crate (name "enrede") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.16") (features (quote ("derive" "must_cast"))) (default-features #t) (kind 0)))) (hash "0xkc3nqzlsxv396x6q4kj0nhafaj916hr03aa9ldjl1zjpzhn3xh") (features (quote (("std" "alloc") ("defaults" "std") ("alloc"))))))

