(define-module (crates-io en am) #:use-module (crates-io))

(define-public crate-enamel-0.1 (crate (name "enamel") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "glium_text") (req "^0.8") (default-features #t) (kind 0)))) (hash "0mspmzy70ckfdh0gcskph7sgyfxmj00qbnyrc506pzxgldfyvnhb")))

(define-public crate-enamel-0.2 (crate (name "enamel") (vers "0.2.0") (deps (list (crate-dep (name "colorify") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "find_folder") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "glium_text") (req "^0.8") (default-features #t) (kind 0)))) (hash "1bsris4fs5vn9sh4ydxdg0z7clllghh9d1z6gm4gbw939dq0fpv7")))

(define-public crate-enamel-0.3 (crate (name "enamel") (vers "0.3.0") (deps (list (crate-dep (name "colorify") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "find_folder") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "glium_text") (req "^0.9") (default-features #t) (kind 0)))) (hash "1js608rijrr7qaim5lwnlr1lsmgk2a2jqv56pr1nndansbn159lh")))

