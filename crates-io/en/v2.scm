(define-module (crates-io en v2) #:use-module (crates-io))

(define-public crate-env2-0.1 (crate (name "env2") (vers "0.1.0") (hash "130l8n2zwvcmrysvn9mqg502xhscd4p6l7qlrlqz4dqwykwk2xah")))

(define-public crate-env2-0.2 (crate (name "env2") (vers "0.2.0") (hash "05nzq4kmqrwgipdm82d06w1h8zkvvgq5w20krnl1lzi2nf8an0bw")))

(define-public crate-env2-0.3 (crate (name "env2") (vers "0.3.0") (hash "1qhww0hclslifdgaaaf8shhcjrxywy5mmw637fpvwgbwflcq3rpx")))

(define-public crate-env2-0.3 (crate (name "env2") (vers "0.3.1") (hash "039l4yxbayvmm6jih7g4ad84mjlifcawxqg190g0pnk0690n92av")))

(define-public crate-env2-0.3 (crate (name "env2") (vers "0.3.2") (hash "06ycibclsp97ksv72l1scdjz55f6yzfm5pkk3c6csb60yagi6rrc")))

(define-public crate-env2js-1 (crate (name "env2js") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vd6b3g2pgf5x6zb6bqg98kxqga80nxblnhfha6bwlf7npk0dgv0")))

(define-public crate-env2toml-0.1 (crate (name "env2toml") (vers "0.1.0") (deps (list (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.8.11") (default-features #t) (kind 2)))) (hash "05lw45y0462bk68k916cliwxvqig70majxx4rhnxbsdl7fmd0958")))

(define-public crate-env2toml-0.2 (crate (name "env2toml") (vers "0.2.0") (deps (list (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.11") (default-features #t) (kind 0)))) (hash "08a2sdjrlfz7jf2n8mc4hc64y8fin2hjs6shfvyn5xg6k85ic4f5")))

