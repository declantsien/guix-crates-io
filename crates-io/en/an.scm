(define-module (crates-io en an) #:use-module (crates-io))

(define-public crate-enande-0.1 (crate (name "enande") (vers "0.1.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.0-alpha.6") (default-features #t) (kind 2)))) (hash "0xgf2r5jkgrp7v85lxzd8spk4nr8s1q3a2di4ig0d5sijv4pqc15") (yanked #t)))

(define-public crate-enande-0.1 (crate (name "enande") (vers "0.1.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2.0-alpha.6") (default-features #t) (kind 2)))) (hash "136nnynrnkry6kpfzrb4vjx2i8cs0bxgp6swy8g88ryhppx662hp") (yanked #t)))

(define-public crate-enande-0.2 (crate (name "enande") (vers "0.2.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2.0-alpha.6") (default-features #t) (kind 2)))) (hash "1pswh7jnbn9kd1d6ry75sqz101vi75dfk717j0da31wiy0846v9p") (yanked #t)))

(define-public crate-enande-0.2 (crate (name "enande") (vers "0.2.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2.0-alpha.6") (default-features #t) (kind 2)))) (hash "11bzk31m9kfj30s94cy5m8skcjp7r0nvr3z2fakazjccflzy1d7r") (yanked #t)))

