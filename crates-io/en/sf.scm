(define-module (crates-io en sf) #:use-module (crates-io))

(define-public crate-ensf594-project-mmap-0.1 (crate (name "ensf594-project-mmap") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive" "std" "color"))) (kind 2)) (crate-dep (name "rand") (req "^0") (features (quote ("getrandom" "std" "std_rng"))) (kind 2)))) (hash "004r1f9aqa7j6flcdzkv9mb6p4zk7j02qvj46pxc1ka9652rnrcm")))

(define-public crate-ensf594-project-mmap-0.1 (crate (name "ensf594-project-mmap") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive" "std" "color"))) (kind 2)) (crate-dep (name "rand") (req "^0") (features (quote ("getrandom" "std" "std_rng"))) (kind 2)))) (hash "0yxaska247586zdyk6a05y93ghg3m21mbj4vn4ap6g6isqg4k9nj")))

(define-public crate-ensf594-project-mmap-0.3 (crate (name "ensf594-project-mmap") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive" "std" "color"))) (kind 2)) (crate-dep (name "rand") (req "^0") (features (quote ("getrandom" "std" "std_rng"))) (kind 2)))) (hash "0wh8k60jva2y5v8vbzwi8vxx4ahwnajpi9sihz1f3mp2mslmzrr7")))

(define-public crate-ensf594-project-mmap-0.4 (crate (name "ensf594-project-mmap") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive" "std" "color"))) (kind 2)) (crate-dep (name "rand") (req "^0") (features (quote ("getrandom" "std" "std_rng"))) (kind 2)))) (hash "1gd6qcb4qvr6hmxalbamqm5kdsgy3cg0f4aqhck8did11f7gb763")))

(define-public crate-ensf594-project-mmap-0.5 (crate (name "ensf594-project-mmap") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive" "std" "color"))) (kind 2)) (crate-dep (name "rand") (req "^0") (features (quote ("getrandom" "std" "std_rng"))) (kind 2)))) (hash "0hzb0bxcs43pb3pwb7scybiq21f1a989yxfjnzh246l6v634vv6l")))

