(define-module (crates-io en g_) #:use-module (crates-io))

(define-public crate-eng_fmt-0.1 (crate (name "eng_fmt") (vers "0.1.0") (hash "0na4d7sbgmm5kws9abcjbmhgca63z5ggpv0gv7d3vwazamj05l1y")))

(define-public crate-eng_fmt-0.1 (crate (name "eng_fmt") (vers "0.1.1") (hash "0qh9xzpay630f50h2lw93isfy1bsy764lr3w9fq34kphxzbjl71a")))

(define-public crate-eng_fmt-0.1 (crate (name "eng_fmt") (vers "0.1.2") (hash "1w7k983lsq0v8i8snfnjsggsg9pnd1wxjjj57b322r5vwr5ajqy2")))

