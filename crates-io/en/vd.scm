(define-module (crates-io en vd) #:use-module (crates-io))

(define-public crate-envdot-0.1 (crate (name "envdot") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1jjbjs1a9s7mabm19d9ggfxh88ym5gsnqk0ipwxwj1pivg6k0hsm")))

