(define-module (crates-io en di) #:use-module (crates-io))

(define-public crate-endi-1 (crate (name "endi") (vers "1.0.0") (hash "0lgaif02yymh7rwpqlykrzdpp5nw3gffmid6rj3fppgypy28zg5j") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-endi-1 (crate (name "endi") (vers "1.0.1") (hash "1bqsm5dqq62qxs7zv889svjrv1pdmxy08dmcm14i8li7xiadqnl3") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-endi-1 (crate (name "endi") (vers "1.0.2") (hash "0s4nmysrlc6d99m1180n4x326jwfb9fl3mh6jww284h6fcyxkw21") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-endi-1 (crate (name "endi") (vers "1.1.0") (hash "1gxp388g2zzbncp3rdn60wxkr49xbhhx94nl9p4a6c41w4ma7n53") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-endian-0.1 (crate (name "endian") (vers "0.1.3") (deps (list (crate-dep (name "bswap") (req "*") (default-features #t) (kind 0)))) (hash "04sfw64g1wcvm5i9bmgf44888hwifi2wc96cawa3kfi2zs7fx5kl")))

(define-public crate-endian-hasher-0.1 (crate (name "endian-hasher") (vers "0.1.0") (hash "1zbn8fa3y0hvm31gdi1rm976a7x09cifzh9l1nf3v1ykyp6hjalz") (yanked #t)))

(define-public crate-endian-hasher-0.1 (crate (name "endian-hasher") (vers "0.1.1") (hash "0pabkdasfng6fv22f9xm106la5mxj61kkxngrifz93019jrz7scm")))

(define-public crate-endian-num-0.0.0 (crate (name "endian-num") (vers "0.0.0") (hash "03py29dgm7jb1nbvgk3i5qdrwaxvlynwdc0pkmjlnw5ydldc62h2")))

(define-public crate-endian-num-0.1 (crate (name "endian-num") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "zerocopy-derive") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1f9la5lvgysgwjsqyg7rcha3mrzjm5rxmah3sxq22gqj8y0mdw8d") (features (quote (("linux-types")))) (v 2) (features2 (quote (("zerocopy" "dep:zerocopy" "dep:zerocopy-derive") ("bytemuck" "dep:bytemuck" "dep:bytemuck_derive") ("bitflags" "dep:bitflags"))))))

(define-public crate-endian-num-0.1 (crate (name "endian-num") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "zerocopy-derive") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "053drg9q1mi0ajc973lmg3cnymd49j4n99bgvnxi1wcl42xlgn0s") (features (quote (("linux-types")))) (v 2) (features2 (quote (("zerocopy" "dep:zerocopy" "dep:zerocopy-derive") ("bytemuck" "dep:bytemuck" "dep:bytemuck_derive") ("bitflags" "dep:bitflags"))))))

(define-public crate-endian-type-0.1 (crate (name "endian-type") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1k8wvgkywcf7s03anpyi7wbhdir10sv78ig6mppk743d5ih37vjg")))

(define-public crate-endian-type-0.1 (crate (name "endian-type") (vers "0.1.1") (hash "027laz6mn2wzv5yvpnygkr2r6plh7gs9xf5wlpa080akb9ypclm4")))

(define-public crate-endian-type-0.1 (crate (name "endian-type") (vers "0.1.2") (hash "0bbh88zaig1jfqrm7w3gx0pz81kw2jakk3055vbgapw3dmk08ky3")))

(define-public crate-endian-type-0.2 (crate (name "endian-type") (vers "0.2.0") (hash "1wk235wxf0kqwlbjp3racbl55jwzmh52fg8cbjf1lr93vbdhm6w6")))

(define-public crate-endian-type-rs-0.1 (crate (name "endian-type-rs") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.2.1") (optional #t) (kind 0)))) (hash "1vyjvwl0v9nw1jw2c10wr08akr30jvjxc1y9r3znhg0mnj50x77g") (features (quote (("std") ("default" "std"))))))

(define-public crate-endian-type-rs-0.1 (crate (name "endian-type-rs") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.2.1") (optional #t) (kind 0)))) (hash "0xjwfn536jwpqnf7w6xy5w9an1j0w8zxnx01zswi2074fmf9lhdn") (features (quote (("std") ("default" "std"))))))

(define-public crate-endian-types-0.0.0 (crate (name "endian-types") (vers "0.0.0") (hash "1q2m08wck4m26h2zs4jhbvljybpjxhb7s1pzh35w2kv4a2lxm1a9")))

(define-public crate-endian_codec-0.1 (crate (name "endian_codec") (vers "0.1.0") (deps (list (crate-dep (name "endian_codec_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1mnhcy1w9d1xky94kgx8grzry0dw6ilamy427mhs29r4d6hc3sw4") (features (quote (("derive" "endian_codec_derive") ("default" "derive"))))))

(define-public crate-endian_codec-0.1 (crate (name "endian_codec") (vers "0.1.1") (deps (list (crate-dep (name "endian_codec_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0wrdcd14217yksms8wqvdp1m1w5f48c88z3f40bhafvyzf8cikn6") (features (quote (("derive" "endian_codec_derive") ("default" "derive"))))))

(define-public crate-endian_codec_derive-0.1 (crate (name "endian_codec_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f9hpy7adj21da3lpfpwylbfkw584m1blwxbbpj749qa1f5f8aab")))

(define-public crate-endian_codec_derive-0.1 (crate (name "endian_codec_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ga1zj8q8p8j2j3ccs9jqc9m8yzcgknhkwz6fnf9l8ba1jw7i1qp")))

(define-public crate-endian_trait-0.1 (crate (name "endian_trait") (vers "0.1.0") (deps (list (crate-dep (name "endian_trait_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "02sjn9xlkipwr28w468xvfz7mkdmb9jffmalgbq69777y58l6b5a")))

(define-public crate-endian_trait-0.1 (crate (name "endian_trait") (vers "0.1.1") (deps (list (crate-dep (name "endian_trait_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "08mj5qqkr9fk101cfm9g08pvrp4l93awjkqnympr8j1b4lv18gmv")))

(define-public crate-endian_trait-0.2 (crate (name "endian_trait") (vers "0.2.0") (hash "1ljzahanr7nnxy89d0n5an94rn0g9dmx6x5sglw13n2qsm4dxavh")))

(define-public crate-endian_trait-0.3 (crate (name "endian_trait") (vers "0.3.0") (hash "0xppjhizaqainw9srz5ci5jin5dm325pq7byylqjpggfm462gk8c") (features (quote (("e128") ("arrays"))))))

(define-public crate-endian_trait-0.4 (crate (name "endian_trait") (vers "0.4.0") (deps (list (crate-dep (name "endian_trait_derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0nhqngcb12j6fjy85f3w30n3x3b3y5djg4nm8smivmd1lbrjyqiy") (features (quote (("e128") ("arrays"))))))

(define-public crate-endian_trait-0.5 (crate (name "endian_trait") (vers "0.5.0") (deps (list (crate-dep (name "endian_trait_derive") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0kly05d1rwl2kvd6c3yn3h2xfckq19kjl4bilgg53gsdgc6r5p0z") (features (quote (("e128") ("arrays"))))))

(define-public crate-endian_trait-0.6 (crate (name "endian_trait") (vers "0.6.0") (deps (list (crate-dep (name "endian_trait_derive") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0ipxg6n63hipxsh54dwdnv67jb5ripmnx114f3z5dnrk5nb49j3p") (features (quote (("arrays"))))))

(define-public crate-endian_trait_derive-0.1 (crate (name "endian_trait_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "03v6f7cnivw8sn7q389gs3mw1w7809ir4gi0dc5i36mhrlciaf4r")))

(define-public crate-endian_trait_derive-0.1 (crate (name "endian_trait_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "10a235ycj2cxy7a51kirpb7xsv6a7j7gdz6f76q455zkrqx1rlbq")))

(define-public crate-endian_trait_derive-0.2 (crate (name "endian_trait_derive") (vers "0.2.0") (deps (list (crate-dep (name "endian_trait") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "15n16jmgkdqpi70vmfkpvyhg9vbc2fsmywq0p0jpalf3pm2r9ayj")))

(define-public crate-endian_trait_derive-0.3 (crate (name "endian_trait_derive") (vers "0.3.0") (deps (list (crate-dep (name "endian_trait") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1gl1l3isfhms2bl43d6ck7nicgzr3fykkq0h6bw8xcx26qzr9dh0")))

(define-public crate-endian_trait_derive-0.3 (crate (name "endian_trait_derive") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l73l4niwbly6kazxmc015wj103fiw4l2djhm9gy96qjd1gj2phv")))

(define-public crate-endian_trait_derive-0.3 (crate (name "endian_trait_derive") (vers "0.3.2") (deps (list (crate-dep (name "endian_trait") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13h7zcxdn60y6n58jnwd1hs7kz3ic6yxj04v43ab4i82768wyhbj")))

(define-public crate-endian_trait_derive-0.4 (crate (name "endian_trait_derive") (vers "0.4.0") (deps (list (crate-dep (name "endian_trait") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0v8d7qjb7s3nlc1jbq277j43hhqpywh4ydlymvndscgb77m4fibw")))

(define-public crate-endian_trait_derive-0.5 (crate (name "endian_trait_derive") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0pgw6h5b61fjy36hnmvxglyk8j41z85xd4fy0wb6077gdhlmm2j1")))

(define-public crate-endian_trait_derive-0.6 (crate (name "endian_trait_derive") (vers "0.6.0") (deps (list (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "180wnv93433q8ykdy7yzqg0ac77cdp840nfl4xqr0sqagrrgyfm0")))

(define-public crate-endianness-0.1 (crate (name "endianness") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0z49f5yad46bhfhzfbgkcz00kqzawvjm1c63zh5gwpnf7z6cibiv")))

(define-public crate-endianness-0.2 (crate (name "endianness") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0mmj7lm8c8wlqsy8f9iwys7d3q62aprn4cfkvkc58vbfl73dsdm4")))

(define-public crate-endiannezz-0.1 (crate (name "endiannezz") (vers "0.1.0") (hash "0rpq59pvkkc815848s248hazds20q81f4m1xbx900542c7bsgik4")))

(define-public crate-endiannezz-0.1 (crate (name "endiannezz") (vers "0.1.1") (hash "0kjjx9cp4lqfzrqp7zmgzn562r85ps0bg8l7i3l9pvjh55pwzis7")))

(define-public crate-endiannezz-0.1 (crate (name "endiannezz") (vers "0.1.2") (hash "17hybn3mmb6zkc8vlky6ncs0sszarf51pxr5p9i0qhz4b5pijd77")))

(define-public crate-endiannezz-0.1 (crate (name "endiannezz") (vers "0.1.3") (hash "0vmi5mlilryppp00vff2ggjb9m6n9ff6dhxsyis26zn6mfbnzfjk")))

(define-public crate-endiannezz-0.2 (crate (name "endiannezz") (vers "0.2.0") (hash "1pgwj0fs2jx72mgix4bccj02g194zjm1d8c1vypwpg8g517alywn")))

(define-public crate-endiannezz-0.3 (crate (name "endiannezz") (vers "0.3.0") (hash "1m7q72swcjw416vlsl5zdddirm1im7ihsj11j9zbz5j3iy7yirv3")))

(define-public crate-endiannezz-0.4 (crate (name "endiannezz") (vers "0.4.0") (deps (list (crate-dep (name "endiannezz_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1bvq6acqclmz36g399wivy581zsxkjy6j1pg97hl7g3dsmj3ylh2") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.4 (crate (name "endiannezz") (vers "0.4.1") (deps (list (crate-dep (name "endiannezz_derive") (req "=0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1l9cl2vr0r64rqsz1sl3g85kwqakqdviwvvlyl0dick666wdqfs1") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.4 (crate (name "endiannezz") (vers "0.4.2") (deps (list (crate-dep (name "endiannezz_derive") (req "=0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0azxm7y396jl57z0dkzwi59pf8i806l1d67g42q1nx5m14a4s96r") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.5 (crate (name "endiannezz") (vers "0.5.0") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1kjb8wwqkkrabzmyl2bywc2b7fc7ny0mgwyywkag7im1lprinn2l") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive")))) (yanked #t)))

(define-public crate-endiannezz-0.5 (crate (name "endiannezz") (vers "0.5.1") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "05bpjd0vry57h1x2icpd6gfmr0acjmabb14ipmfwxfwfna4rik9g") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.5 (crate (name "endiannezz") (vers "0.5.2") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1k8gxl3ghy81vj49w6pqs4z3q9fk5h9cjxq3fvk7kxzw3hh4yky4") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6 (crate (name "endiannezz") (vers "0.6.0") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "115n2n62hsl0xfy6w3aab10i1nlz4rp767zfybyzv000rgw3yc38") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6 (crate (name "endiannezz") (vers "0.6.1") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1zj5kyyvgicjzd9c36vdssp6iisbgzrd3m6xml46i5qms6n79q4x") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6 (crate (name "endiannezz") (vers "0.6.2") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "062hjkjaww7accl67m5vzqmcs34fm2rlrnf3qgvnlpv4r0hy4w3i") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6 (crate (name "endiannezz") (vers "0.6.3") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1rmcj2fd5w9qa2j2543j8m5b6fsiw842jvplv3q256l6861ff44a") (features (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6 (crate (name "endiannezz") (vers "0.6.4") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0lnh9d26gm1amys241zfjbyrb6fph3h2vnap8427xn20la77d3ls") (features (quote (("unchecked_bool") ("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6 (crate (name "endiannezz") (vers "0.6.5") (deps (list (crate-dep (name "automod") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "endiannezz_derive") (req "=0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1q6jphzf32avbp4wlps8dq8zb5gk97n1vprcc489ad688k7v7xy4") (features (quote (("unchecked_bool") ("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz_derive-0.1 (crate (name "endiannezz_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1icjhcz5y9qvb4cchin8n3lw87v1c99nnlpjfi67h70k90cmzhyk")))

(define-public crate-endiannezz_derive-0.1 (crate (name "endiannezz_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r2rj6yb8c2fhaiwvbavd2yni5gd4scymfrp6pvwxgmjl9123wg6")))

(define-public crate-endiannezz_derive-0.2 (crate (name "endiannezz_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dd3kycyls9wxfmzqcqzr5m57z46dzy5pdblan3lkrrl9xmj54lk")))

(define-public crate-endiannezz_derive-0.2 (crate (name "endiannezz_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dj0bzf853sq1ck6lya38pv3vv4g56ilnkwg0aflxdiqx272wqck")))

(define-public crate-endiannezz_derive-0.2 (crate (name "endiannezz_derive") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "147wmyzwnzglhwhfilmcislgx06r5fbr6d3rmryzngbnvdx85krq")))

(define-public crate-endianrw-0.2 (crate (name "endianrw") (vers "0.2.0") (hash "09cqy1d97a7awg5b1d33ilj2v8hq8b7fpwiaw4b4xz4wqrix17cd") (yanked #t)))

(define-public crate-endianrw-0.2 (crate (name "endianrw") (vers "0.2.1") (hash "08z3zgcyb5hx3jp1fsw5l1738xy17y5628mm890qf557lp48bqxc")))

(define-public crate-endianrw-0.2 (crate (name "endianrw") (vers "0.2.2") (hash "1d6nss8dfdr68izrmrw7h80cdwzjbyj4lg16hdbb76kg9vbi36ls")))

(define-public crate-endiantype-0.1 (crate (name "endiantype") (vers "0.1.0") (hash "1ckvdgsgpjayvpcnx7nvfqhv625l8vh9n61lb5ypw52pmr835yqw")))

(define-public crate-endiantype-0.1 (crate (name "endiantype") (vers "0.1.1") (hash "1i71k0gvg38vhv2zaklgn7ph58ah3an1w0ccxp7ymlvm1f6q44kb")))

(define-public crate-endiantype-0.1 (crate (name "endiantype") (vers "0.1.2") (hash "09mq2913qxjdlwwdif8zs3lakwd2166zacp4wr1flrhhzifj6wnn") (features (quote (("std") ("default" "std"))))))

(define-public crate-endiantype-0.1 (crate (name "endiantype") (vers "0.1.3") (hash "0f0xq056yhbapxzjk931wkk68j9cnh1a32hqs7c4r518fjhwk7k1") (features (quote (("std") ("default" "std"))))))

(define-public crate-endicon-1 (crate (name "endicon") (vers "1.0.0") (deps (list (crate-dep (name "codicon") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "11wha96r30s9xv892ppqhdzizmwhcz6az95rikzwl3d04nmzss73")))

(define-public crate-endicon-1 (crate (name "endicon") (vers "1.1.0") (deps (list (crate-dep (name "codicon") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1a5s8wignrvywllc9sylf37qsvcc905jiqp117adr8ayq33rd641")))

(define-public crate-endicon-1 (crate (name "endicon") (vers "1.2.0") (deps (list (crate-dep (name "codicon") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1dfwzhl9inx9zbs7c1gaxysm65sqfw00ncs2dc66qr7a858nvwkc")))

(define-public crate-endicon-2 (crate (name "endicon") (vers "2.0.0") (deps (list (crate-dep (name "codicon") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1923yb83j0n974g72dg1fl12y722qxn991ww125mch4ql7krda1r")))

(define-public crate-endicon-3 (crate (name "endicon") (vers "3.0.0") (deps (list (crate-dep (name "codicon") (req "^3.0") (default-features #t) (kind 0)))) (hash "0pgmlfgd2lf60p91hv7crfmf65nnd2ixzk9mbgnsyy2nzvalrdwx")))

(define-public crate-endinero-0.1 (crate (name "endinero") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1c7q0c7zji58av8lmqz51g8lgpz9x8dd0vl2fdnrmh6pzz7g7425") (rust-version "1.65")))

(define-public crate-endinero-0.1 (crate (name "endinero") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1dahzir897j1y9pd1r1mddx8lk868fgq7lgm30f026i2rzyybpal") (rust-version "1.65")))

(define-public crate-endinero-0.1 (crate (name "endinero") (vers "0.1.3") (hash "1jl0c326jrrv2p77sy3qyxk4j1y9c07r8ikadn2886j92mgxw20l") (rust-version "1.65")))

(define-public crate-endinero-0.1 (crate (name "endinero") (vers "0.1.4") (hash "1sy7m6pgn8s9776fd9d34byj490k0p2jl0x6c4l11yy4s9zi9ykd") (rust-version "1.65")))

(define-public crate-endinero-0.1 (crate (name "endinero") (vers "0.1.5") (hash "0zqqwyjrqh5nciflaz269xmwb53kmwkggliph6vr4vlx9wgpryff") (rust-version "1.65")))

(define-public crate-endinero-0.1 (crate (name "endinero") (vers "0.1.6") (hash "01a53qzjksracqp92fg7bprfriidd8xnvzblag13r9wfl1kzw5l0") (rust-version "1.65")))

(define-public crate-endinero-0.1 (crate (name "endinero") (vers "0.1.7") (hash "168g9l4ya2nld9mp8snwj2m54gvs21jr3vniyi1wb8r05gx36h2w") (rust-version "1.65")))

(define-public crate-endio-0.1 (crate (name "endio") (vers "0.1.0") (hash "1gbd5bpiwfpwp30h4133l28rqqxhsw1y008q61qnw9pjs1sbzcch")))

(define-public crate-endio_bit-0.1 (crate (name "endio_bit") (vers "0.1.0") (hash "0ymzkcb1267z6xiaphm914q5f8jwarpr6jm4gc71mqcs4cdcm7yx")))

(define-public crate-endio_bit-0.2 (crate (name "endio_bit") (vers "0.2.0") (hash "0igm25005kjdvhnq5wkd6lp3bpcppdapwkmw83myjh7vm2bdkma1")))

(define-public crate-endixk_minigrep-0.1 (crate (name "endixk_minigrep") (vers "0.1.0") (hash "05z4bsdmxmp40wyf8azkphd2ic31c88pqbzhpz3m1lcsvqscadz9")))

