(define-module (crates-io en vt) #:use-module (crates-io))

(define-public crate-envtestkit-1 (crate (name "envtestkit") (vers "1.0.0") (deps (list (crate-dep (name "fake") (req "^2.2.3") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0jmc9cgzhk08jyqwsj8biky44gl6ajrnk31w65zwffavbana14m6")))

(define-public crate-envtestkit-1 (crate (name "envtestkit") (vers "1.1.0") (deps (list (crate-dep (name "fake") (req "^2.2.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1grf96crd87xy4ci0lh44jym0azfigylmqqfln8hf4ns78bqpml7") (features (quote (("lock" "lazy_static" "parking_lot") ("default" "lock"))))))

(define-public crate-envtestkit-1 (crate (name "envtestkit") (vers "1.1.1") (deps (list (crate-dep (name "fake") (req "^2.2.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1k1j1dvib6dsnvqarplas5pv6gwka78mk311cfs6g296nq0q9sr0") (features (quote (("lock" "lazy_static" "parking_lot") ("default" "lock"))))))

(define-public crate-envtestkit-1 (crate (name "envtestkit") (vers "1.1.2") (deps (list (crate-dep (name "fake") (req "^2.2.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0bsl27xrpb22259qhk28rpqybqhsagsqzpw3m1v0wnwsx534b8af") (features (quote (("lock" "lazy_static" "parking_lot") ("default" "lock"))))))

(define-public crate-envtime-0.0.1 (crate (name "envtime") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)))) (hash "0zazxwjgkz2lvgdkdx2fcrxpdc6nvppns0lynjgpy07civ6c6kkd")))

(define-public crate-envtime-0.0.2 (crate (name "envtime") (vers "0.0.2") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)))) (hash "1sgakxcwnlnysx51k8afmixdbjrbhzgypnicz8g10sjxszmy39d7")))

(define-public crate-envtime-0.0.3 (crate (name "envtime") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)))) (hash "0s9yy0cc3ngy010b3nj64dfvyz904r7mww6vyf5da896jk1cc4i4")))

(define-public crate-envtime-0.0.4 (crate (name "envtime") (vers "0.0.4") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)))) (hash "1s98103wc289xjypaj1ab15qfbzfqjr9r8b2a86fx9zl66j4w2v2")))

