(define-module (crates-io en tw) #:use-module (crates-io))

(define-public crate-entwine-0.1 (crate (name "entwine") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1gx5slsx4hgk0mx16az4q42rpm2vayw3yj21dmbw2iwx0diq8cfz") (features (quote (("std") ("nightly") ("extended_random_tests") ("default" "std"))))))

