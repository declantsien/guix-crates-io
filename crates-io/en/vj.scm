(define-module (crates-io en vj) #:use-module (crates-io))

(define-public crate-envja-0.1 (crate (name "envja") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1l35m7n5k684f2ilvdd782idfi10kfw4v4wgxfpm9jx6k0ci58v2")))

(define-public crate-envja-0.2 (crate (name "envja") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0q82n4a31r6x6s7xm5nc4fvsx44dx97bbv0cqjp87lp4s4g0pz5n")))

(define-public crate-envja-cli-0.1 (crate (name "envja-cli") (vers "0.1.0") (deps (list (crate-dep (name "envja") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "02zxbazdq0x30hx3k12ifcf9ymr0lhjm1626s9vvv9x2mgfqkm7w")))

(define-public crate-envja-cli-0.2 (crate (name "envja-cli") (vers "0.2.0") (deps (list (crate-dep (name "envja") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "09nnghl8nsryz7l389179d5b1gkwbw02z8i7r3h9gm658ya8m0yc")))

