(define-module (crates-io en su) #:use-module (crates-io))

(define-public crate-ensure-0.1 (crate (name "ensure") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1b9cgsjzj42djlsk9zvq0x0rwhgsmdrgxdvb6rdvajzs3yyr8hi3")))

(define-public crate-ensure-0.2 (crate (name "ensure") (vers "0.2.0") (hash "0yyl5jzldbklw95fllmhj42sg64a0y1vpz77ywvcc1wa8494yz8l")))

(define-public crate-ensure-0.2 (crate (name "ensure") (vers "0.2.1") (hash "0qqy0cb2gb25nd8v7s6n9s534g45d0c3qnlfppc2hs1ijkcbxq9q")))

(define-public crate-ensure-0.2 (crate (name "ensure") (vers "0.2.2") (hash "02g44wsik20k3z2vfwnn6qp1wi9bdnijxbc3s2wsjhla52hqb8s5")))

(define-public crate-ensure-0.3 (crate (name "ensure") (vers "0.3.0") (hash "1c09ncaahg2mx8as5lhs0qf3abc2gx1f1cl3n86kxfiyh0pnvc8m")))

(define-public crate-ensure-0.3 (crate (name "ensure") (vers "0.3.1") (hash "0y64fyv4ng1ji8v1svh642cxrqzzhx2jyrbrl8vsi2rk9vhfi67n")))

(define-public crate-ensure-mamba-0.1 (crate (name "ensure-mamba") (vers "0.1.0") (deps (list (crate-dep (name "bzip2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "expanduser") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trauma") (req "^2.2.4") (default-features #t) (kind 0)))) (hash "0swvi340k8simqx3chq0v1hiqvjp1l6x8xlx5i38r85i2km03m9n")))

(define-public crate-ensured_bufreader-0.1 (crate (name "ensured_bufreader") (vers "0.1.0") (hash "0irmb0pr3z2z0v2cz8417i9x1j62z3zkdix2kg0h5f397aaxyrdl")))

(define-public crate-ensured_bufreader-0.2 (crate (name "ensured_bufreader") (vers "0.2.0") (hash "0fvfr7791rx5snipv6gvnzj9gvmc9ww8135dfdz2w7gbiiihsb5x")))

