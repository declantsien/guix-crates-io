(define-module (crates-io en vr) #:use-module (crates-io))

(define-public crate-envr-0.1 (crate (name "envr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fv0704bxx4gqr8nkjc00mp2jl0xvvnc8gssmj93zb0pd74phrsg")))

(define-public crate-envr-0.1 (crate (name "envr") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wv533dlzk0n9pcbinkkz92nhsplx73y24v9c4a8g0x35sb6wmrz")))

(define-public crate-envrc-0.2 (crate (name "envrc") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "0h2x7j04vb8mbkf5394g3abf3x85hacld72kxlmpbhj04m9yhnr4")))

(define-public crate-envrc-0.3 (crate (name "envrc") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "0ss20pn8vr7r8j1cc14h3m5r359clckrjwy664s4dmx5lg42450p")))

(define-public crate-envrc-0.4 (crate (name "envrc") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "145kr4rfx5p1v5h260vcwicii8kwfb69hbr0wsfca38r2ijk19fz")))

(define-public crate-envrc-0.5 (crate (name "envrc") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "04skppxn1cliji54rm8frzj0pq8m1vmwlqfbniwdkas52mlm5glk")))

(define-public crate-envro-0.1 (crate (name "envro") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1srj1q9x157ixlp4hjwm0m5r2vcqqvs5bxc86j3679jr2fsxii2y")))

(define-public crate-envro-0.2 (crate (name "envro") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1xszf707hmn6ls7c98pvbjj2i0f8mipp04qyxkm10m15prqdb5c8")))

(define-public crate-envro-0.3 (crate (name "envro") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "176b36xixv1zpy28licwsvwc67i7pn8lbm519d2avz9j1v9j6r15")))

