(define-module (crates-io en vu) #:use-module (crates-io))

(define-public crate-envuse-parser-0.1 (crate (name "envuse-parser") (vers "0.1.0-beta.0") (hash "11qb1pvbicrfz3mg7wnwwbrn7v266p4r1xbp0cl2jfq3q1vgqviy")))

(define-public crate-envuse-parser-0.1 (crate (name "envuse-parser") (vers "0.1.0-beta.1") (hash "1z7xw8smvjnp7r4zkfhv6jvsjhiscnjv3m5dbgv9j3yh2pl19v1g")))

(define-public crate-envuse-parser-0.1 (crate (name "envuse-parser") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ziggvi3q9qlpd29gay7csgl38iximbdl7wkh9vinrbhjf8gnmvl")))

(define-public crate-envuse-parser-0.1 (crate (name "envuse-parser") (vers "0.1.0-beta.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0l9nshphzqqk31vi8rx2pf50a7n1dlbhw6g7g10lccbw3pqrn66w")))

(define-public crate-envuse-parser-0.2 (crate (name "envuse-parser") (vers "0.2.0-beta.4") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15ypd3rc014g5b9f48zhkpsa1v653mghq6vkr96ay2h8wii288s7")))

(define-public crate-envuse-parser-0.2 (crate (name "envuse-parser") (vers "0.2.0-beta.5") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rhj2vscgbrih8mdw1xnsgcwcwsi1p50g7vmgsi3qd1ky4n6115f")))

(define-public crate-envuse-parser-0.4 (crate (name "envuse-parser") (vers "0.4.0") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)))) (hash "0phk6wn27397fq39b0npjx5jhx6arxmw93gw75q3rckmpvqqkkjj") (features (quote (("with-js"))))))

(define-public crate-envuse-parser-0.5 (crate (name "envuse-parser") (vers "0.5.1") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)))) (hash "0y186xy7198nibnzaf3f6qgx9jg136sbsr6scfq9lfdnnnbyhn2p") (features (quote (("with-js"))))))

(define-public crate-envuse-parser-0.6 (crate (name "envuse-parser") (vers "0.6.0") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)))) (hash "0k8ax5q6fk6wsaj8di88xicakagp7kk1nxprdbmdsrcpm1f0175d") (features (quote (("with-js"))))))

(define-public crate-envuse-parser-0.7 (crate (name "envuse-parser") (vers "0.7.0") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l4psmkcy7yfv37hk4cypnz9fcff46aadm6painfkaxwl5rrqspn")))

(define-public crate-envuse-parser-0.8 (crate (name "envuse-parser") (vers "0.8.0") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03mhcjaakkih9nq9a2qj7yldrvcnd1gzk5lnr9rmhiqapysv8yay")))

(define-public crate-envuse-parser-0.8 (crate (name "envuse-parser") (vers "0.8.1") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1r23wxv202d1sdpbmz62mwxv6hglm177dlsmbx9jdjynvpph17k1")))

(define-public crate-envuse-parser-0.8 (crate (name "envuse-parser") (vers "0.8.2") (deps (list (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lrhly6jhdd8mi6js3csndsr186q4xz0wkxf599d272cqpb6c0a5")))

(define-public crate-envuse-parser-0.9 (crate (name "envuse-parser") (vers "0.9.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)))) (hash "0hzbhdg4bf1xq5hxhh250c6fa5qycw34lhswjmx4zf8kklxafd2j")))

(define-public crate-envuse-parser-0.9 (crate (name "envuse-parser") (vers "0.9.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.21.0") (features (quote ("yaml" "serde"))) (default-features #t) (kind 2)))) (hash "1aia4yzm8kx418kl8yl00l2fxhb0v4pywkc8yly4gisdbb4dwhpl")))

(define-public crate-envutil-0.0.1 (crate (name "envutil") (vers "0.0.1") (hash "1wl2swdvm8vhj8aknq5qxicrcggwrvaqms1zgslv5w8v01kn95zq") (yanked #t)))

(define-public crate-envutil-0.0.2 (crate (name "envutil") (vers "0.0.2") (hash "1p0v0mi5k6wbxb34rw9i3pkl1d0zffkwwy6x6rwzsxir2a744av4")))

(define-public crate-envutil-0.0.3 (crate (name "envutil") (vers "0.0.3") (hash "1511an1mvrm0lyw5mhhk8y5rlvl8d18qqkvdwda9jdahmsg64j3c")))

(define-public crate-envutil-0.0.4 (crate (name "envutil") (vers "0.0.4") (hash "0kajyf8bs0cx0qh90a56h07qib93rljxnwkkhw2cp1xi5xzin3g9")))

