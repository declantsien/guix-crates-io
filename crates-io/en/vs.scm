(define-module (crates-io en vs) #:use-module (crates-io))

(define-public crate-envshim-0.1 (crate (name "envshim") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.9.4") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "12nz5x714zmwyh8yv3cxcy9dpingsh2h43jh1zwkg25l913ijz7n")))

(define-public crate-envshim-0.2 (crate (name "envshim") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.9.4") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "165grh4q3fmdzbr4mj7wvq8l71fl7mndz6mpb5rf78cf81a4qxj2")))

(define-public crate-envsub-0.1 (crate (name "envsub") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)))) (hash "1sbicsy14inysn0jmxn3fz7gawsm290dypifpsjn7fq2x9rs3kww")))

(define-public crate-envsub-0.1 (crate (name "envsub") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)))) (hash "01grkzd1ib4xi51mf877x97g9wrg7gmbljflvw9bkr45dqb5jyjl")))

(define-public crate-envsubst-0.0.1 (crate (name "envsubst") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1lxrbjw08bhcvfznpbkhvln5d25vlbcz10n9rimxxf9ilcq3nmf8")))

(define-public crate-envsubst-0.1 (crate (name "envsubst") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "046zgi0aazb5ayrjs9anpcb71a91ri1m3k4lhna2x45zkfg11fwn")))

(define-public crate-envsubst-0.1 (crate (name "envsubst") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0sirv9k1jzh7f69yrw9vwkq3j2l16lcmldhkx9b39j1lsnl1kkqk")))

(define-public crate-envsubst-0.2 (crate (name "envsubst") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qj1akgkvj3lj0ja9jg2dqczvz6xwhbw1p8ikic8zy39ix3rxgw7")))

(define-public crate-envsubst-0.2 (crate (name "envsubst") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hr6ahqqprid0cv9x5361ni9ahzi4izgqpbiwlli4kb7xvv2jbyg")))

