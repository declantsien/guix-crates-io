(define-module (crates-io en df) #:use-module (crates-io))

(define-public crate-endf_format-0.0.1 (crate (name "endf_format") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0jbjf16yl7h6xmn7yyzcln3440yiyayqplh2n822bjpz84sh0n9n")))

(define-public crate-endf_format-0.0.2 (crate (name "endf_format") (vers "0.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0r9sg1fngpzm7710sv8nknraz8v8xhy2lkag9bkvvyll91q326b9")))

(define-public crate-endf_format-0.0.3 (crate (name "endf_format") (vers "0.0.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0saizj869vqq8knd8y0g4nwykhzmdmci9zcmmqcq1imm4faczs0s")))

(define-public crate-endf_format-0.0.4 (crate (name "endf_format") (vers "0.0.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0fwpsz3q00l46svw1khgin4zs18nkx4jxsjgh12vn7j836ll11hq")))

(define-public crate-endf_parser-0.1 (crate (name "endf_parser") (vers "0.1.0") (hash "0davv56yn37ambghfqp8yp8r8b6mnzrrp46x5fqvxw2848hm652g")))

(define-public crate-endf_parser-0.1 (crate (name "endf_parser") (vers "0.1.1") (hash "0iwwkrnlfjjn6aj62a1sbfj5jv911fl467pdydr2nvygsaz4s4mc")))

(define-public crate-endf_parser-0.2 (crate (name "endf_parser") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "1jnra8jzyvi768szj0qm9k26fs6vi79ha189g8zm50shcw91ki7y")))

