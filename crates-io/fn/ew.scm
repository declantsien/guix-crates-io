(define-module (crates-io fn ew) #:use-module (crates-io))

(define-public crate-fnew-1 (crate (name "fnew") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1a1k7a8bpnirv34qqf7fsslj5r43y0fs9dj51nj4gsn1qkc66mim")))

(define-public crate-fnew-1 (crate (name "fnew") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0v4clk94jkhpd880xbdjaqwdjfq3j8ra5kgjpqli8nvnr3c0zpww")))

