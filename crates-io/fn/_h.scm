(define-module (crates-io fn _h) #:use-module (crates-io))

(define-public crate-fn_has_this-0.1 (crate (name "fn_has_this") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn_squash") (req "^0") (default-features #t) (kind 0)))) (hash "1l89vdgk5yl36f026q92m6w529sg31pjwdbb31w5ysyj1nv81cnv") (yanked #t)))

(define-public crate-fn_has_this-0.1 (crate (name "fn_has_this") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn_squash") (req "^0") (default-features #t) (kind 0)))) (hash "1advip3hxfkmn3cwrbqkr3knv4jya59jl0cjf2b7cdi1cy0974zb")))

