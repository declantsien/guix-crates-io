(define-module (crates-io fn io) #:use-module (crates-io))

(define-public crate-fnio-0.0.0 (crate (name "fnio") (vers "0.0.0") (deps (list (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("rt" "sync"))) (default-features #t) (kind 0)))) (hash "1d937sq36j5n2f5wdqjlk6zvghg04xlkb84xaairkdvrgixkv59q")))

