(define-module (crates-io fn ma) #:use-module (crates-io))

(define-public crate-fnmatch-regex-0.1 (crate (name "fnmatch-regex") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "06b1a81gd3axqkhqj3gmrzniw3k6nx40q36dw5xdc5qdnn9mrlx4")))

(define-public crate-fnmatch-regex-0.2 (crate (name "fnmatch-regex") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.13") (default-features #t) (kind 2)))) (hash "11kiqq52gqz5pp47d2bp3gp3r09x8qg23mw6w9750ilgapcx8djx")))

(define-public crate-fnmatch-regex2-0.3 (crate (name "fnmatch-regex2") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18") (default-features #t) (kind 2)))) (hash "1msiaqcddziwggr2nyqcml2210rvsizff2yxf141bsidaq2khps6")))

(define-public crate-fnmatch-sys-1 (crate (name "fnmatch-sys") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1cs5dwcl05yxrrzyca2vjaicxsvfgwsiw6q1sb601vvldx9fhs96") (links "fnmatch")))

