(define-module (crates-io fn -u) #:use-module (crates-io))

(define-public crate-fn-util-0.1 (crate (name "fn-util") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ayg8jq4z002wzn3kg1z2pbq1xds2c0pm2j9p2sgsc0diwxkv7q0")))

