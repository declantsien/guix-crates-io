(define-module (crates-io fn um) #:use-module (crates-io))

(define-public crate-fnum-0.0.1 (crate (name "fnum") (vers "0.0.1") (deps (list (crate-dep (name "fnum-derive") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)))) (hash "1sfdksmmhq5s2i0p08kchq8njqxh4rdg9shjhpqn0vkim76ffaw3")))

(define-public crate-fnum-derive-0.0.1 (crate (name "fnum-derive") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.14") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qqqf1rib1gf99pimx7yrfbmf9642vr46zppidzc5agqaky40f7d")))

