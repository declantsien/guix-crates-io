(define-module (crates-io fn _o) #:use-module (crates-io))

(define-public crate-fn_once-0.1 (crate (name "fn_once") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rr4vmkz1kgm9armxlr5g43b98q9vq9xrr9bjliydi1ph74jiykm")))

(define-public crate-fn_once-0.2 (crate (name "fn_once") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14q92wlx0vglwbphlrs6davmydjm6w3fy2121k5bry4vphchwhx3")))

(define-public crate-fn_once-0.3 (crate (name "fn_once") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ygi65sklvq7bf0d71bdymwk5qdffdsqy3mphcdlsfix5qwx5g1n")))

(define-public crate-fn_ops-0.1 (crate (name "fn_ops") (vers "0.1.0") (hash "1lnijgdqyd6pfag2924d9kfqazhyhiz6cf2sknd7dka3ykzi92dw")))

(define-public crate-fn_overloads-0.1 (crate (name "fn_overloads") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dsb526iilikjac3m18nzccmfmwj75fsbgnljxwpn68ay4anvdf1") (features (quote (("std") ("impl_futures") ("default" "std"))))))

(define-public crate-fn_overloads-0.2 (crate (name "fn_overloads") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "1dxcnd32kq3pvay1jynqk8xv3gzw3q6j3ibg5j8xrz8fb9kj2ksk") (features (quote (("std") ("impl_futures") ("default" "std"))))))

