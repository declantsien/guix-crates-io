(define-module (crates-io fn _t) #:use-module (crates-io))

(define-public crate-fn_type_alias-0.1 (crate (name "fn_type_alias") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07vcrh975yvh5aqf3z3i77pvawb41k8ypglzjn4iyzr105zq17l0")))

