(define-module (crates-io fn -t) #:use-module (crates-io))

(define-public crate-fn-traits-0.1 (crate (name "fn-traits") (vers "0.1.0") (hash "0xs8iyq2wfmi6h22650w1h3jlf6i1cacck6x9d0bb4w00w7iqbna")))

(define-public crate-fn-traits-0.1 (crate (name "fn-traits") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "0zikqmfjfqly1y62pwk5hhhw8vnkx0wbbn9qqx81jjcxljp5wrnk")))

(define-public crate-fn-traits-0.1 (crate (name "fn-traits") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)))) (hash "0312r1yykcx8lmp0g2siqrb8a195v5zm9asi3y3ri3fg31izy3nd")))

