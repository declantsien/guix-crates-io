(define-module (crates-io fn _z) #:use-module (crates-io))

(define-public crate-fn_zip-0.1 (crate (name "fn_zip") (vers "0.1.0") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "tupleops") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1a7zid1z67bnnp0s9yqx84bv7ss8kzf0h98sfx7z62rhlw3rra4p") (features (quote (("par") ("default"))))))

(define-public crate-fn_zip-0.1 (crate (name "fn_zip") (vers "0.1.1") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "tupleops") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cm4ii4r0wgazdavbc3r517qbx7jifpqkvi7hx8xb2vdf8f9pyla") (features (quote (("par") ("default"))))))

(define-public crate-fn_zip-0.1 (crate (name "fn_zip") (vers "0.1.2") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "tupleops") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ap2yqzyxspiv958v6afvajiqijm7h0b43117jqcpsa56f55hsa7") (features (quote (("par") ("default"))))))

