(define-module (crates-io fn -c) #:use-module (crates-io))

(define-public crate-fn-cache-0.1 (crate (name "fn-cache") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 2)))) (hash "1xj0a9vqir5p6mj2dccmwyxmgi50gqcyac2094q3yj05xvwvv67x")))

(define-public crate-fn-cache-0.1 (crate (name "fn-cache") (vers "0.1.1") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 2)))) (hash "00sxygx3gxmzhzamz91dpv3rad1mmgsykw36jkmwpc2yl58p2n51")))

(define-public crate-fn-cache-0.2 (crate (name "fn-cache") (vers "0.2.0") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 2)))) (hash "0jshj3pkgpy6lnjcqr9jx97zksx4jc2p048734kni2zi3ysqf83y")))

(define-public crate-fn-cache-0.3 (crate (name "fn-cache") (vers "0.3.0") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 2)))) (hash "0wjpwbas3qmni9q9sjs1yf9mnxy7i1m5xwz4529kcv1kw9qmvbv3")))

(define-public crate-fn-cache-0.4 (crate (name "fn-cache") (vers "0.4.0") (deps (list (crate-dep (name "hashers") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 2)))) (hash "0nh4kgx6m4wn4q3g18c4m2qk25cf4cndk59k0yalrlsa51kvvasx")))

(define-public crate-fn-cache-0.4 (crate (name "fn-cache") (vers "0.4.1") (deps (list (crate-dep (name "hashers") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 2)))) (hash "1n5rfid7djw521lz36sysxx97hzv821vs0g6zqjpqskv2khiq77c")))

(define-public crate-fn-cache-0.5 (crate (name "fn-cache") (vers "0.5.0") (deps (list (crate-dep (name "hashers") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ii0fbw6h9pzl6wkvcmdf6lzm6brlka2vxvqh6b48rzg4msz92hm")))

(define-public crate-fn-cache-1 (crate (name "fn-cache") (vers "1.0.0") (deps (list (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 2)))) (hash "1y0gv6k8w1c8sglyrkb0bavmmlcffljdjflir20w2rha9k19dwqf")))

(define-public crate-fn-cache-1 (crate (name "fn-cache") (vers "1.1.0") (deps (list (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 2)))) (hash "14qcdy9ay07s2jm158xvw7l80kblxnlhzmafxyx3z4sp20g2a9lh")))

(define-public crate-fn-cache-1 (crate (name "fn-cache") (vers "1.1.1") (deps (list (crate-dep (name "hashers") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 2)))) (hash "13ifky861wxdzl7l20jmq8rjbr58gdhnxmrv8z9xzwk1y506cbbr")))

(define-public crate-fn-compose-0.1 (crate (name "fn-compose") (vers "0.1.0") (hash "0mz08f1qsd2wkyrmag5wc9mc6hy97q8dcavnpl4wnjgdp5csy7wm")))

