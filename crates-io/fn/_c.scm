(define-module (crates-io fn _c) #:use-module (crates-io))

(define-public crate-fn_chain-0.1 (crate (name "fn_chain") (vers "0.1.0") (hash "0v9mknh66nrhi2nprj3y9mhy6h08nn3mnzyns69fcmmigdl9hksr")))

(define-public crate-fn_chain-0.1 (crate (name "fn_chain") (vers "0.1.1") (hash "1imyqz6vr14ph4iyzmxsqgagv5c5gpjk05g84j5j87ihsffj2zfz")))

(define-public crate-fn_chain-0.1 (crate (name "fn_chain") (vers "0.1.2") (hash "0jd19yr4pqz32s74bhwgs9vsrz5b6i1gqrdd8hlda7a6mxf1y5lc")))

