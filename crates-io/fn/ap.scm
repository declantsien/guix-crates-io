(define-module (crates-io fn ap) #:use-module (crates-io))

(define-public crate-fnapi-0.1 (crate (name "fnapi") (vers "0.1.0") (hash "1rkjvfqd0x0c2yjnhwjsagd1nynfjilx99c854wpxkahaqd51zrz")))

(define-public crate-fnapi-cli-0.1 (crate (name "fnapi-cli") (vers "0.1.0") (hash "09n03xx8gy7rqnjb8ndsirvvsbsxldil9fznismpf8zfc1b2s8wb")))

