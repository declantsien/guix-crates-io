(define-module (crates-io fn rs) #:use-module (crates-io))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.0") (hash "00pnmv67khg033q8phhdph6rak0cah0sxflkfz33crf7hh0h9jsy") (yanked #t)))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.1") (hash "1cp03k7fqzh93alnnlc3v6vzd2pwc1qb7d71w0lk956a0k4y8p7g") (yanked #t)))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.2") (hash "1dk38vb23li9lsmg7f5729hlgj1xm3r3qwp1zd1g8y7icgji0hvf") (yanked #t)))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.3") (hash "1y358in47v15csqpqnnvadqlki6f58mp4aaxzg9529cl21bqdrij") (yanked #t)))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.4") (hash "14z5zqqqqrs254l3h2m40drbimvbjwf9kfc7qymm3vlgy7pb0paz") (yanked #t)))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.5") (hash "0almpknq283f0800zj3kka90cr49vmcmn6vaa30wbj8il7zh9wm6") (yanked #t)))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.6") (hash "06k2n673w7s60hdy43ildanvcmw69j6mlr5sypl4g7rqkzzvgmq9") (yanked #t)))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.7") (hash "1nsq6dd54xc3w3pwg3jyi3va3sb68f3k23a0cba0in2hh98d1ckr")))

(define-public crate-fnrs-0.1 (crate (name "fnrs") (vers "0.1.8") (hash "1cvhjx1qw5wc9c6v3kkz2fkpar1rw0ashq00gyjbr8gy92mrbgkp")))

