(define-module (crates-io fn lo) #:use-module (crates-io))

(define-public crate-fnlog-0.1 (crate (name "fnlog") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req ">=0.8.2, <0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req ">=0.4.11, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "stdext") (req ">=0.2.1, <0.3.0") (default-features #t) (kind 0)))) (hash "1h28b8809ip9pfdf1x8a0b95jk785arhpplil3cr8kx07k63bqqs")))

(define-public crate-fnlog-0.1 (crate (name "fnlog") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req ">=0.8.2, <0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req ">=0.4.11, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "stdext") (req ">=0.2.1, <0.3.0") (default-features #t) (kind 0)))) (hash "1985n9dxajq1yjg65zlmfxh70rf529pyl4mm0xjx9hrhjwzhhgkf")))

(define-public crate-fnlog-0.1 (crate (name "fnlog") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req ">=0.8.2, <0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req ">=0.4.11, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "stdext") (req ">=0.2.1, <0.3.0") (default-features #t) (kind 0)))) (hash "08dnp2bj2hxlawhpvmv1dy2c2917rvpiwb175pg864m9h8m9zpx3")))

