(define-module (crates-io fn _m) #:use-module (crates-io))

(define-public crate-fn_macro-0.1 (crate (name "fn_macro") (vers "0.1.0") (hash "175dwg0q64pcsk5m6wcw0451ps5mhp0vbcdyrb6rpfw30dydrvk9")))

(define-public crate-fn_macro-0.1 (crate (name "fn_macro") (vers "0.1.1") (hash "0lmm2vgb0p4cw9k55y3kcqv5blrz24dg9m2ihq336l1f7cqy31gz")))

(define-public crate-fn_macro-0.1 (crate (name "fn_macro") (vers "0.1.2") (hash "0ahbh100m2jr7vygyfs6ba7rp0fmpbfp1g0daalgcp38yl4cmfif")))

(define-public crate-fn_match-0.1 (crate (name "fn_match") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "1q5g5g3g8czr9d280fk58nhvxd3cg6l9fldyx8ff22zsp91haj8q")))

(define-public crate-fn_meta-0.1 (crate (name "fn_meta") (vers "0.1.0") (hash "1wz68mvhs166ackg5abdailqp2hvmkq8af3w3988r7hwya99006y")))

(define-public crate-fn_meta-0.2 (crate (name "fn_meta") (vers "0.2.0") (hash "18mh8ly6fly5y9h28kddyc9v5s4sbw1ynjvfbfdcsxsl8n7y8vp7")))

(define-public crate-fn_meta-0.3 (crate (name "fn_meta") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (optional #t) (default-features #t) (kind 0)))) (hash "0k9cz8k51bl82hh42910pl760bl3dkw58fhwq2xq70gqmcxyp906") (features (quote (("fn_meta_ext" "arrayvec") ("default"))))))

(define-public crate-fn_meta-0.4 (crate (name "fn_meta") (vers "0.4.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (optional #t) (default-features #t) (kind 0)))) (hash "0miikjfp82is82xqwi8va23mycpvqz118ksrchrk4v9im6990iqp") (features (quote (("high_arg_count") ("fn_meta_ext" "arrayvec") ("default"))))))

(define-public crate-fn_meta-0.4 (crate (name "fn_meta") (vers "0.4.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (optional #t) (default-features #t) (kind 0)))) (hash "0ji392jbhcc9p42bk0kaf4pypwgpl2d4jkgvg3k997y5yvgmjfp1") (features (quote (("high_arg_count") ("fn_meta_ext" "arrayvec") ("default"))))))

(define-public crate-fn_meta-0.5 (crate (name "fn_meta") (vers "0.5.0") (deps (list (crate-dep (name "smallvec") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "1m3r27xrijvihhmcz7jlsbv0g2w1dsp4jc2i6kwhvjxbqp5nmvsg") (features (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.6 (crate (name "fn_meta") (vers "0.6.0") (deps (list (crate-dep (name "smallvec") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "0vbci77x7nplrfkqaqpcgcvrj7bar8k8my73pap92g3zgnx66bml") (features (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7 (crate (name "fn_meta") (vers "0.7.0") (deps (list (crate-dep (name "smallvec") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "18my0yk9y80a38f0xh5rfxf242pq2h5s5s56rlk759d06lshl7qq") (features (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7 (crate (name "fn_meta") (vers "0.7.1") (deps (list (crate-dep (name "smallvec") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "00rdzl3jjvrifzvg798v3nslsbkhxrpl8x5ibyx5ap2xgm16l57x") (features (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7 (crate (name "fn_meta") (vers "0.7.2") (deps (list (crate-dep (name "smallvec") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "0kvwd3a73488h224j895fdwr2xq0kmk7zfc1fw09bxmkb1gwvryp") (features (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_meta-0.7 (crate (name "fn_meta") (vers "0.7.3") (deps (list (crate-dep (name "smallvec") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zqq454x1xvxr8nyrdff2zni1y9wdc9vdfpyxcn308v28qf1bs15") (features (quote (("high_arg_count") ("fn_meta_ext" "smallvec") ("default"))))))

(define-public crate-fn_move-0.1 (crate (name "fn_move") (vers "0.1.0") (hash "0nvb79q3dr16dwjm9xbaw9v0f88p72q8d5idba4z82mh8kddx1bf")))

(define-public crate-fn_mut-0.1 (crate (name "fn_mut") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l1bx7v0rrd044syamp9gjkprrkbjpfh7ls6ha15m5ghs2x4hbxf")))

