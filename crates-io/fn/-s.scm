(define-module (crates-io fn -s) #:use-module (crates-io))

(define-public crate-fn-sdk-0.0.0 (crate (name "fn-sdk") (vers "0.0.0") (hash "16rdbdixrf9yr8fhzx2qpbldhbznw0jj4lggav3f4p6m52dqgn2x")))

(define-public crate-fn-sdk-macros-0.0.0 (crate (name "fn-sdk-macros") (vers "0.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_tokenstream") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "02j6gzqwx7qm9ng562vpsp76bv01jvfq4mdkmm7mayyhmqnkbjsb")))

(define-public crate-fn-store-0.1 (crate (name "fn-store") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "19554vp86l1am1mq5bkqlfkwnzqa8fsbvm1fzagb6gszxk76cwg6")))

(define-public crate-fn-store-0.1 (crate (name "fn-store") (vers "0.1.1") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "0vrklk6slb5dp4vwqldr8fi08i546d1fqk69z6yr5vwww82xwsyy")))

(define-public crate-fn-store-0.1 (crate (name "fn-store") (vers "0.1.2") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "1z4x2vj0vd0mnnvinvv8p5j3as0bv0vqmw28jmigspf2pqi1kwbh") (yanked #t)))

(define-public crate-fn-store-0.1 (crate (name "fn-store") (vers "0.1.3") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "08p4gnjvl6mlx1w7byzs3rzbi0rrfk0xlhvv5w94wwr33a52i3w1") (yanked #t)))

(define-public crate-fn-store-0.1 (crate (name "fn-store") (vers "0.1.4") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "0a52a748fqfxcvazca8f1yw57vscfazbgxcl2995a37ia06rwsyk")))

(define-public crate-fn-store-0.1 (crate (name "fn-store") (vers "0.1.5") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "1wa28zakyjpg6i50shkp5sa31qgnmzxw16va0yz4hna5fp3b6973")))

(define-public crate-fn-store-0.2 (crate (name "fn-store") (vers "0.2.0") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "12bqx6h4ll2wlsk7zhk5l43h8i6h7d3n284gckgfzlrhppr2v0yr")))

(define-public crate-fn-store-0.3 (crate (name "fn-store") (vers "0.3.0") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "008wfdpgafgvblwhdih1g322ahyb0c36sjyjbbqv1iq5y3r92jn8")))

(define-public crate-fn-store-0.3 (crate (name "fn-store") (vers "0.3.1") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "0qdm2d49cw1lypby280pczpjay8dwi0m2l90pa0brw3sczwzzqw9")))

(define-public crate-fn-store-1 (crate (name "fn-store") (vers "1.0.0") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "062c5qwawmjkrmhflyjdk66m30kla94m3qik2py29v0p1p2wflsx")))

(define-public crate-fn-store-1 (crate (name "fn-store") (vers "1.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "1b5v22izx4ipb1gj4xq0s8j0hby5mrsbs7chla9bpgafi59p8n30")))

(define-public crate-fn-store-1 (crate (name "fn-store") (vers "1.2.0") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "0h16kamyfsd6h311qzg838nyc189986c4m1czbic3c8fsks3qq45") (yanked #t)))

(define-public crate-fn-store-1 (crate (name "fn-store") (vers "1.2.1") (deps (list (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "type-key") (req "^1") (default-features #t) (kind 0)))) (hash "1mhc9pw1qwl0ippymlan3azyav1qjy2w4rdlg5707wkmhbbkby1k")))

