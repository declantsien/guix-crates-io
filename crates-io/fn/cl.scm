(define-module (crates-io fn cl) #:use-module (crates-io))

(define-public crate-fncli-0.1 (crate (name "fncli") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17pfyhqgj122ap3a6q63jfmp43zpnlym12sv5frmf2iwxas50i6n")))

(define-public crate-fncli-0.2 (crate (name "fncli") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s6pnd1w6v8qkr2lnsa4rfkis0n6axfk5ahb061xqwqq9km20wa4")))

(define-public crate-fncli-0.2 (crate (name "fncli") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dswflcj2fz62jk8nj3fzz8p903k9fhp3f220k3j2i15sbfck6v2")))

(define-public crate-fncli-0.2 (crate (name "fncli") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xwqqrq2wpx7ir5gxwyl0vb2sxifw0yfg9ykvy7ds7vkdxqik5n2")))

(define-public crate-fncli-0.3 (crate (name "fncli") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01vzpacx8zfbbm1ngw9ymgzmr86drdg4c398g2npsjm26b9dhqks")))

(define-public crate-fncli-0.3 (crate (name "fncli") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k5jrl17nvbfin0g2ymgs8sj0pb10ipm6rk1hr6nvzqpd5vy0nhp")))

(define-public crate-fncli-0.3 (crate (name "fncli") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14hwxxs5hvqv76sx0vilbfad99sfgpp2xqm94m7av2ryk7qqpb39")))

(define-public crate-fncli-0.3 (crate (name "fncli") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18qk9cmkbms8s62ay6j9cl8jdsxvpdf2sxql104cywhki1yzj32l")))

(define-public crate-fncli-0.3 (crate (name "fncli") (vers "0.3.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0a9q7qn7812r0bgx6515h8x45bsm0drr3zgwyi5xna42g7j54v93")))

