(define-module (crates-io fn _b) #:use-module (crates-io))

(define-public crate-fn_block-0.1 (crate (name "fn_block") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0xzlv40gwn9gi6maskgzzvg3ibibahp8bda020k4027p29fa6lws")))

(define-public crate-fn_block-0.2 (crate (name "fn_block") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "11p4i2a1f55dkxr8shcx7bqq5drkzwfvprrhh9a2pnlq4k1g1d82") (features (quote (("unproven"))))))

(define-public crate-fn_block-0.2 (crate (name "fn_block") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0qjxx7nhgmg9c32d1hiyya5qiylxyygs0n0caqkgr2fq7cfkzamb") (features (quote (("unproven"))))))

(define-public crate-fn_box-1 (crate (name "fn_box") (vers "1.0.0") (hash "1zrgr9raq829rkx2cqj2m2dc4impm4qp2sv8f3sid7kqdy1q71qy") (yanked #t)))

(define-public crate-fn_box-1 (crate (name "fn_box") (vers "1.0.1") (hash "1m2rry2qkm5nsbl8m9y6wmyywrxgkjfcds8x9s35k7idr5yjlxx9") (yanked #t)))

(define-public crate-fn_box-1 (crate (name "fn_box") (vers "1.0.2") (hash "1cs7cskk94bmg9wr4vjy2w7vjps7p3cpxj3vgarif37slw1b4zh3") (yanked #t)))

(define-public crate-fn_box-1 (crate (name "fn_box") (vers "1.0.4") (hash "0fx35da1mjgahynfb0x1r993cflgwk1whxwml98xkjk8c1iwxi1c") (yanked #t)))

(define-public crate-fn_box-1 (crate (name "fn_box") (vers "1.0.5") (hash "1181y77cchmsj5va7bfprh5xmph7i9d9zrznbbxs9cmdm78r7yvh") (yanked #t)))

