(define-module (crates-io fn st) #:use-module (crates-io))

(define-public crate-fnstack-build-utils-0.1 (crate (name "fnstack-build-utils") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0p9bz8qm38xrzkaslpcrxlpbk2s59wlg6bz54blxh0wa82rsx5in") (yanked #t)))

(define-public crate-fnstack-cf-macro-0.1 (crate (name "fnstack-cf-macro") (vers "0.1.0") (hash "1cww5n18hz21pyyg2gx59p04hgnlm2aaarl72dbwdclnrnxhqprr") (yanked #t)))

(define-public crate-fnstack-cf-macro-0.2 (crate (name "fnstack-cf-macro") (vers "0.2.0") (hash "04mfm1kd65x6zq5jw8gpfm2lsfpvi1a6vp8l8kxj5czwh280c4ic") (yanked #t)))

(define-public crate-fnstack-cf-macro-0.2 (crate (name "fnstack-cf-macro") (vers "0.2.1") (hash "1ln2msl8ia9s4r9dhc4xp3w66a0ldn444vbidwz9f7lgwgg1gi28") (features (quote (("axum")))) (yanked #t)))

(define-public crate-fnstack-cf-macro-0.2 (crate (name "fnstack-cf-macro") (vers "0.2.2") (hash "0r0y56z6svb2yzdqvb820qvh5n5rz4xf1hx5bi8mjnrc0wn05xxx") (features (quote (("cloudflare") ("axum")))) (yanked #t)))

(define-public crate-fnstack-macros-0.2 (crate (name "fnstack-macros") (vers "0.2.2") (hash "1s4s1y8fjw6ps7rw4qkgv1jb94ximprn8m4blxlmgbvyl8vqcszs") (features (quote (("cloudflare") ("axum")))) (yanked #t)))

