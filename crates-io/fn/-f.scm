(define-module (crates-io fn -f) #:use-module (crates-io))

(define-public crate-fn-fixture-1 (crate (name "fn-fixture") (vers "1.0.0") (deps (list (crate-dep (name "fn-fixture-lib") (req "=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "1cj10qvcknl41c5vnw7h7nbchprjal6v0457rq2bnjqy70zrb5yh")))

(define-public crate-fn-fixture-1 (crate (name "fn-fixture") (vers "1.0.1") (deps (list (crate-dep (name "fn-fixture-lib") (req "=1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "0js3pjpdr5zqsb2j9vi5jqw72mnkpdc7pajfzyk9j14r80jqs0r3")))

(define-public crate-fn-fixture-1 (crate (name "fn-fixture") (vers "1.0.2") (deps (list (crate-dep (name "fn-fixture-lib") (req "=1.0.2") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "0f3x99vhm7lrx88d3rpdfzrj4lmmvi5bsqzcg04lm2c0zrs3rjvr")))

(define-public crate-fn-fixture-lib-1 (crate (name "fn-fixture-lib") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g8s4mdba2idgxapjxkv6r4y7sjh25b40891qgyjxi3pj352a4fl")))

(define-public crate-fn-fixture-lib-1 (crate (name "fn-fixture-lib") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c8lnqw200bg6z76y7fqj4kisrhwiwsr13rg1a0m0f365gdhh5a9")))

(define-public crate-fn-fixture-lib-1 (crate (name "fn-fixture-lib") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.60") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v6yq772ac2wkizg7dfvhzchjn4vlqwdbbbsxy2sy0db29l2fcd7")))

(define-public crate-fn-formats-0.0.1 (crate (name "fn-formats") (vers "0.0.1") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1b8ajfkh4shxj6c6zpxpb9phyabj2lb0z5hj9pq6gqf8vriy1fph")))

(define-public crate-fn-formats-0.0.2 (crate (name "fn-formats") (vers "0.0.2") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "0zh7iywygqgzbrlq1n97zfrrzjmay76zz8rl37kp6b4k1hyn1fwi")))

(define-public crate-fn-formats-0.0.3 (crate (name "fn-formats") (vers "0.0.3") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "07nig0l3jddj8c126iwnjg9z38az0v4klniv1rcwfc15y51nx224")))

(define-public crate-fn-formats-0.0.4 (crate (name "fn-formats") (vers "0.0.4") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0d4kllnx0nr0bixyssw0nq7vhxg06ljvk3s1mp6bvbm00piag4lz")))

(define-public crate-fn-formats-0.0.5 (crate (name "fn-formats") (vers "0.0.5") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "10p38k7502pxxb7jgcj5k33fak18jril1hlh2gfr8ilv715lgh11")))

