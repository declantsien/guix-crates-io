(define-module (crates-io fn ic) #:use-module (crates-io))

(define-public crate-fnichol-cime-0.1 (crate (name "fnichol-cime") (vers "0.1.1-dev") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "111v4525820cx2q3dhd0w03r1py0yc26jgrjxgqrg1rw3vbycphq") (features (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.2 (crate (name "fnichol-cime") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0qbsfcjpyvk4kqvf5lbg63dcdkp4dlccj2x0mk2b6vp55qjcys1z") (features (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.4 (crate (name "fnichol-cime") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "1a7avn42zzpbbdmak5cisv98bf6sx6c2y2p479ybnhpp29qzf651") (features (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.5 (crate (name "fnichol-cime") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "02c2r2anm71wb02i38fnaxv41bda1nyj98wh3ycna0lb7kxlgwxn") (features (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.6 (crate (name "fnichol-cime") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0dx813z7h7ziiadz7jnfxq8lv2wspkxr5sl8ky34xm1kx5qvf8zr") (features (quote (("default" "application") ("application" "clap"))))))

(define-public crate-fnichol-cime-0.7 (crate (name "fnichol-cime") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0q6am1ywyfp7rnsw9jsppxm9ncd1xa894h3rn7qfm5xy4nny6dcf") (features (quote (("default" "application") ("application" "clap"))))))

