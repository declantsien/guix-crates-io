(define-module (crates-io fn _a) #:use-module (crates-io))

(define-public crate-fn_abi-0.1 (crate (name "fn_abi") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b42hhixi4kkgz6qf4alslgli8cnhnv3a83sw5hpx9pm02w2n968") (yanked #t)))

(define-public crate-fn_abi-0.1 (crate (name "fn_abi") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05qjl57i12zi5jm8phhyl67m18440dd71makc3qfs88lgbf7ghxj") (yanked #t)))

(define-public crate-fn_abi-0.1 (crate (name "fn_abi") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn_squash") (req "^0") (default-features #t) (kind 0)))) (hash "1cyb6a157nwv19gh1mc518dxrpgpxxkjhr8zknmp8ng3qfrk3fhk") (yanked #t)))

(define-public crate-fn_abi-2 (crate (name "fn_abi") (vers "2.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn_squash") (req "^0") (default-features #t) (kind 0)))) (hash "003lqs83s18krb96ic5am82q3yfi5c5kadwnznq7d7rb0cazj2xs") (yanked #t)))

(define-public crate-fn_abi-2 (crate (name "fn_abi") (vers "2.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn_squash") (req "^0") (default-features #t) (kind 0)))) (hash "0029ydccvh2hxix2npxavh20cry3725zad18494rkwf5sf5q46s5")))

(define-public crate-fn_abi-3 (crate (name "fn_abi") (vers "3.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn_squash") (req "^0") (default-features #t) (kind 0)))) (hash "0akcwcv741xhbssrxgbm26iq2biw1hqyhmi2jlfj6zh8bh9m4mcy")))

