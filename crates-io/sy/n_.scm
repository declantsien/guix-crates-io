(define-module (crates-io sy n_) #:use-module (crates-io))

(define-public crate-syn_builder-0.1 (crate (name "syn_builder") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.25") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17wdpcdzbhmbsr6wiz4j7wic72wsdwbc06x6avxpi8i772nz38br")))

(define-public crate-syn_builder-0.2 (crate (name "syn_builder") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.25") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xnlvs83h8p0aa4fa131zn1x6qf2vncl08axrinlypf5d35jnkj8")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03fvnm7by3xab49rq6c5m9nhcjwr2c7qig4pdyp0d0ka43nxb3gm")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h47jjqwgp3j6pxvg9n6gyk87c03r5vv961x38qfxnm8slpfqi81")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y9c2cbh2fv0zkkbz0rmz0fjkwwcpxzlws9gb34z3r8bn84j7ja2")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "04mxvrq9gpa84lcyakzrba0h8cq0293nvp536zxdgyg98jf8sxk7") (yanked #t)))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1gn2fk07jc7vmil6g7l1285kvz3vl8dsscv7z3k257r9qa17jjb0")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "06w6a7cb9hk4iw485wl2b0snry5wa4phqzj52gm926jpnpnbl0cw")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0pk0bi4rqzixqxx4f9hf4k5x6cnbg0h2bbfrwsni98gr096qfa41")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1999dww2b2h2ggh3hbmp9pjbbammm22flwvqzp6jx8f4000fyvmf")))

(define-public crate-syn_derive-0.1 (crate (name "syn_derive") (vers "0.1.8") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "derive"))) (default-features #t) (kind 0)))) (hash "0yxydi22apcisjg0hff6dfm5x8hd6cqicav56sblx67z0af1ha8k") (features (quote (("full" "syn/full") ("default" "full"))))))

(define-public crate-syn_query-0.1 (crate (name "syn_query") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.2") (features (quote ("full" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00ar80cyddmy3ipwr0ajl8jasjlb2nz80wvdf25xzi7ccvqc7kcn")))

(define-public crate-syn_query-0.1 (crate (name "syn_query") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.2") (features (quote ("full" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "04ndrv16i5al8ca7hf5ncv3an4v5mb0khgxb0m6ds1wv2psn1vfv")))

(define-public crate-syn_query-0.1 (crate (name "syn_query") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.2") (features (quote ("full" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1mdw78ikhfbgxm0llhlksckxhp1hsicgxn07cg1g5pfcrq9vfswd")))

(define-public crate-syn_query-0.2 (crate (name "syn_query") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.2") (features (quote ("full" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "130885mfdhiss1a8pz66ajm5145p92l7y0sxpz1w48gfdlw7zmwr")))

(define-public crate-syn_squash-0.1 (crate (name "syn_squash") (vers "0.1.0") (hash "1dcv9cbjvg2r5mbild15yymbm94cfp42g7ibmdljaayh11brhjqk")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.0") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "00k6kzz0j6dsi4ka2nfba7ksbx9rf9kp0c1zhw85glqgnygn3bp5") (yanked #t)))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.1") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "084ixm730p5kva1617xzinaihgi4p683b1svg85a42kizr8c40f7")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.2") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1pq0q2m58rwdq656hm6hgkbgcb5qwiw9i3ix27djdck588bb5zhl")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.3") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "0qgq1zy3d2zs7rzigj35miblj123hhmsqbd0a2ghisq9zqa36bqi")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.32") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1s0fp53rxii5s35356mbixx4xxxca68d6zz7ssiai6q71ab2lf2r")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.34") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "045nq0iy0360sbl1rjlw06dm8ay330ba1kxhvlqb7yz53x9iacz2")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.35") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1mgqbrg7l6r01ri5i19aj6drf8mrb4ikhh2pbz0x8w9vas0hkdbh")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.37") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "16l4a2g4mkg62hby0a8wg9fs44lq9sgrwpnr6c0raw7zvl2qlfyl")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.39") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1ddbmdsrxhs5kirqvr1hyycf7h0c1dwkb03rnv968j0i9p8r6pr2")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.41") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "109xwxs3qqkk905d6ndayc0167v8g47j8x5fv7mw9fnr5vhfma44")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.50") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1sxbs6lk3g3mffkbch5wmf1j42k4bl6rrw3mvxbzmpg0r1pzlzhm")))

(define-public crate-syn_str-0.1 (crate (name "syn_str") (vers "0.1.58") (deps (list (crate-dep (name "fixed_len_str") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1xbw9c6il0zbb3hpmm4mfz0c2nxhgbjwm08h218drdgxqyrh48xy")))

(define-public crate-syn_util-0.1 (crate (name "syn_util") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1rak1bf44dyxsrg4z61x0k7rcx4bwp2b9f7rz1l3s8hg9z1n8gff")))

(define-public crate-syn_util-0.1 (crate (name "syn_util") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.14") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0xzc5vs1ffl95jqh5gvcp2m014xj5js3jzjkprbv3n7c7qwnb3c8")))

(define-public crate-syn_util-0.2 (crate (name "syn_util") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "13hf6h20l7jji32sv8700vmd9yim20v383midgj0hv76wi8683yp")))

(define-public crate-syn_util-0.2 (crate (name "syn_util") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0lkky3i76jxqas9r8l346g5dd7wk3bvz1ddajbj3jr72xch2ypi9") (yanked #t)))

(define-public crate-syn_util-0.3 (crate (name "syn_util") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "18743qjbq959f77nzgr25yh20ancrrbgyyqxk2qa3ghxln5qslah")))

(define-public crate-syn_util-0.4 (crate (name "syn_util") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0fcmwlw21p6f8hii7ijxcy8lxmmrv4l2xnxb51vm8vxfd6vmmlnh") (yanked #t)))

(define-public crate-syn_util-0.4 (crate (name "syn_util") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "12gb40gr3czrqcka8z0izgz9yz4vmxnagy46qh6n69g0s2k5ilsl")))

(define-public crate-syn_util-0.4 (crate (name "syn_util") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0g52vqjl3v34hc82771b71nwg474cmpdb86qx5a7arbrkdaw8m37")))

