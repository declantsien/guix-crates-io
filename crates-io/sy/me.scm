(define-module (crates-io sy me) #:use-module (crates-io))

(define-public crate-symengine-0.1 (crate (name "symengine") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "symengine-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rmyvxi2hw9xil6kr4f0gvmfjsfsh4rngnjmgk95rq2x2dclr6il") (features (quote (("default" "serde"))))))

(define-public crate-symengine-0.1 (crate (name "symengine") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "symengine-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wqy036rxn360ii7pl228g0jywjys6xizsyxy6l6w02kxd1x8p6c") (features (quote (("default" "serde"))))))

(define-public crate-symengine-0.1 (crate (name "symengine") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "symengine-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b2f0bm9q8qrs0yikw4p2hxal98hgy2zp8hgwa41hzilmwcb8cb7") (features (quote (("default" "serde"))))))

(define-public crate-symengine-0.2 (crate (name "symengine") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "symengine-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0f056yk470y96yh09dmka1v0hg01zf5klb0fmi5nks3r3pdw8045") (features (quote (("default" "serde"))))))

(define-public crate-symengine-0.2 (crate (name "symengine") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "symengine-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mpjfqpdj0sp4352s6y00ql8js1j730ca4920f5q4d8aabsf6hyi") (features (quote (("default" "serde"))))))

(define-public crate-symengine-0.2 (crate (name "symengine") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "symengine-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0h615hqll15f31hhsj1jb9gmzd9q61kvfz7bickqjgzkv57f6gac") (features (quote (("default" "serde"))))))

(define-public crate-symengine-sys-0.1 (crate (name "symengine-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)))) (hash "18i6ybwdwgwrnqa8q9d7rl55kqd9qfzpfpbra8sai61d558ijrcy")))

(define-public crate-symengine-sys-0.1 (crate (name "symengine-sys") (vers "0.1.0+1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)))) (hash "0cnykh0k27b4nk0igdci30c56rlwbbnafycz5cbqsh4ax1zx611h")))

