(define-module (crates-io sy kl) #:use-module (crates-io))

(define-public crate-sykl-0.1 (crate (name "sykl") (vers "0.1.0") (deps (list (crate-dep (name "attohttpc") (req "^0.9.0") (features (quote ("json" "charsets"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0q0pyic601vxksf48k3xqqzyjssh5mn6rv4516nkx1mawqsql9ym")))

