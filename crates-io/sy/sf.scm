(define-module (crates-io sy sf) #:use-module (crates-io))

(define-public crate-sysfetch-1 (crate (name "sysfetch") (vers "1.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0i1vsri3qb1q8ccac82rghdv2zjvbl8m46zwgvm0sjww602n2b0l") (yanked #t)))

(define-public crate-sysfetch-1 (crate (name "sysfetch") (vers "1.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "12jp8h3p3adz6y3hy10iknmg3jar8bizr57qakwynv2zvl986gvi") (yanked #t)))

(define-public crate-sysfp-0.0.1 (crate (name "sysfp") (vers "0.0.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "18x391dk0k4hgv0vmzm2gzh5nymrfphjm3nz8509gr40jy921n9l")))

(define-public crate-sysfs-class-0.1 (crate (name "sysfs-class") (vers "0.1.0") (hash "1ybryi8jyrk8b5jrkk8rkdwlkblzih3ayywrkjbdv559mkzm8z3h")))

(define-public crate-sysfs-class-0.1 (crate (name "sysfs-class") (vers "0.1.1") (hash "04pnrkm7yd2lar8zdkip2a6vw9gl7s1sq8s0m6s6wmqsk0ia1va8")))

(define-public crate-sysfs-class-0.1 (crate (name "sysfs-class") (vers "0.1.2") (deps (list (crate-dep (name "numtoa") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0gkg83pka9fadf12bkvlxk6h32bxk6j1i1mv0lgg0kql1rflm3ki")))

(define-public crate-sysfs-class-0.1 (crate (name "sysfs-class") (vers "0.1.3") (deps (list (crate-dep (name "numtoa") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1lani118vxa29127wl8kfv1xy4flsqggdxwqg2klab3kd7wbq6sy")))

(define-public crate-sysfs-pwm-0.1 (crate (name "sysfs-pwm") (vers "0.1.0") (hash "0j98w8qp549nv8n9bawhxarl5k3b784fcfy24gdf88baz3aqjx96")))

(define-public crate-sysfs-rs-0.0.1 (crate (name "sysfs-rs") (vers "0.0.1") (hash "1iy54dy8ks4j2plfiskpdc762hzg0yw9x7y1g5inw55kip3xigjv")))

(define-public crate-sysfs-rs-0.0.2 (crate (name "sysfs-rs") (vers "0.0.2") (hash "1xmnhigmxqgbl0lc01ph8fkf3bw70cib72sx0f43vqk1arswq3qg")))

(define-public crate-sysfs-rs-0.0.3 (crate (name "sysfs-rs") (vers "0.0.3") (hash "02vbmd0sziswkx41jqxjyw24x8rj7sgn44w869xsq0lh1rc425j1")))

(define-public crate-sysfs-rs-0.0.4 (crate (name "sysfs-rs") (vers "0.0.4") (hash "02c5f65ydl3rfimh3iiih9lys331dcm811zfb7sz8wqbccxfqj49")))

(define-public crate-sysfs-rs-0.0.5 (crate (name "sysfs-rs") (vers "0.0.5") (hash "1vn7h9rn08rsd2q5981xm0d3rbvicrcwsa557k16h30vb449afaa")))

(define-public crate-sysfs-rs-0.0.6 (crate (name "sysfs-rs") (vers "0.0.6") (hash "0n920nzi0nwqk3n9cwi3r64pfvj60266q86asa8940zpah6w1shd")))

(define-public crate-sysfs-rs-0.0.7 (crate (name "sysfs-rs") (vers "0.0.7") (hash "0jbqwybynxqahirkssjc3wrr501q9lv19byvqzv37xww50gd7gad")))

(define-public crate-sysfs-rs-0.0.8 (crate (name "sysfs-rs") (vers "0.0.8") (hash "1rw1jvi403js0sfn1534lwqafm9pw1l8cllakinmh65340xnwkh4")))

(define-public crate-sysfs-rs-0.0.9 (crate (name "sysfs-rs") (vers "0.0.9") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0fbnh8hy8wc0a4p8qrmk1mhnbl76jqyvjq3s58nxn73s1cddmpk8")))

(define-public crate-sysfs-rs-0.0.10 (crate (name "sysfs-rs") (vers "0.0.10") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "02k0nhwi8cqflzjr2ivmijgba57ya2596xhv682ahj258wfn01x4")))

(define-public crate-sysfs-rs-0.0.11 (crate (name "sysfs-rs") (vers "0.0.11") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "15zf13636ac41jrbjyfvhy15d1ichj6cp9p45ipq24qdsqgzfjm4")))

(define-public crate-sysfs-rs-0.0.12 (crate (name "sysfs-rs") (vers "0.0.12") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1f76limx6s6bza8gj1zjkl9vb76rgnykb20apph7k461nk4g1i03")))

(define-public crate-sysfs-serde-0.2 (crate (name "sysfs-serde") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "12cbv5s53y28sd8kfwh7s8p4w609h9wyl9gml9ivpifgq69pgxn9")))

(define-public crate-sysfs-serde-0.2 (crate (name "sysfs-serde") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1zgfk094l74blyqkfgl18gx8w1li8k5h9g9c6pizfxzqj5swq9b2")))

(define-public crate-sysfs-serde-0.2 (crate (name "sysfs-serde") (vers "0.2.2") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1yxcjn3dlv4if2kggyvf61l4q1h63lads6b19zl2xnbrcy8kq8ac")))

(define-public crate-sysfs-serde-0.2 (crate (name "sysfs-serde") (vers "0.2.3") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0sh62np8sl06ckgm5fqi9mxf4rvffp9gbljc9ay0p7bxpiw4mswm")))

(define-public crate-sysfs_gpio-0.1 (crate (name "sysfs_gpio") (vers "0.1.0") (hash "1g2m1h01l5fb2f5nlkn4hyg8ipra9cq25m3rvjayfghkr45s8mbz")))

(define-public crate-sysfs_gpio-0.1 (crate (name "sysfs_gpio") (vers "0.1.1") (hash "1ds48ilf7n896anj650n9jjsfv9blnpdsvm1g86602pqmls9y6mi")))

(define-public crate-sysfs_gpio-0.2 (crate (name "sysfs_gpio") (vers "0.2.0") (hash "1vqsyk8sdm64q9lzdqriy7jfnkcb9kk916lkyhr0kpgr68qqbksz")))

(define-public crate-sysfs_gpio-0.2 (crate (name "sysfs_gpio") (vers "0.2.1") (hash "04z3lis27414hn5hx3v9ba8ph6521c4pyf6l26q5adld75a9j7vj")))

(define-public crate-sysfs_gpio-0.3 (crate (name "sysfs_gpio") (vers "0.3.0") (deps (list (crate-dep (name "nix") (req "*") (default-features #t) (kind 0)))) (hash "07favkf08qkg8qxwvv99j2przx5320ybriqrq1ifa4h4mplnqc28")))

(define-public crate-sysfs_gpio-0.3 (crate (name "sysfs_gpio") (vers "0.3.1") (deps (list (crate-dep (name "nix") (req "*") (default-features #t) (kind 0)))) (hash "099h2l2m517lg74k1ym5s8fnpzinzw525bc01awmj8dpbs05is4m")))

(define-public crate-sysfs_gpio-0.3 (crate (name "sysfs_gpio") (vers "0.3.2") (deps (list (crate-dep (name "nix") (req "*") (default-features #t) (kind 0)))) (hash "11wzvynld62f4g2f40sxi8rb9r6rd7042jj8a9visz1v4b2ky512")))

(define-public crate-sysfs_gpio-0.3 (crate (name "sysfs_gpio") (vers "0.3.3") (deps (list (crate-dep (name "nix") (req "*") (default-features #t) (kind 0)))) (hash "1gb149hv6chyzsfigsl100x7ddvhfczdhj299bcsxfwkbl3ikrfg")))

(define-public crate-sysfs_gpio-0.4 (crate (name "sysfs_gpio") (vers "0.4.0") (deps (list (crate-dep (name "nix") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0wqyiwrjhjvymqw58vry0is270943q4m1h999b6dh3zp9bx72q64")))

(define-public crate-sysfs_gpio-0.4 (crate (name "sysfs_gpio") (vers "0.4.1") (deps (list (crate-dep (name "nix") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1arzh82vcmq00kadjip2544fafmbay6vb7bxy5iswfjl06vaas1l")))

(define-public crate-sysfs_gpio-0.4 (crate (name "sysfs_gpio") (vers "0.4.2") (deps (list (crate-dep (name "nix") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "04gmrad1hvg2ahilriqzgaah0h5sxndhzndf2gz82cssl28mq59q")))

(define-public crate-sysfs_gpio-0.4 (crate (name "sysfs_gpio") (vers "0.4.3") (deps (list (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09xahkfq8fbwl5x87iigfca5869y4sxv52n1pag5fyp65l9mmp9d")))

(define-public crate-sysfs_gpio-0.4 (crate (name "sysfs_gpio") (vers "0.4.4") (deps (list (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14055kc38xv3pvvis9w481him67x8h57mfvcxdd1qb5kjpibx2c9")))

(define-public crate-sysfs_gpio-0.5 (crate (name "sysfs_gpio") (vers "0.5.0") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0xdvdqf9hrphyadbv1sf2d8f9pw55fv18yz61p8iq3qs7rmhbz4h") (features (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5 (crate (name "sysfs_gpio") (vers "0.5.1") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0hx5gxacpn2vfx288y70110pc0y3rkgbyrw0r0xzwgrfipqqkp6i") (features (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5 (crate (name "sysfs_gpio") (vers "0.5.2") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "06k9g6qr2rksl448fylj2fbcbjsvn9bpk725551gdqg4ds9mhzx6") (features (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5 (crate (name "sysfs_gpio") (vers "0.5.3") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0gqyd76kfq4vk6sfargf8i3rf0gv3z1qr1carra9zly7wg5g4s1x") (features (quote (("tokio" "futures" "tokio-core" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.5 (crate (name "sysfs_gpio") (vers "0.5.4") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ldmm5xa040726kkc0wpsvcgw5jijhc8ghyfyvjdh8v6hiaim5i4") (features (quote (("use_tokio" "futures" "tokio" "mio-evented") ("mio-evented" "mio"))))))

(define-public crate-sysfs_gpio-0.6 (crate (name "sysfs_gpio") (vers "0.6.0") (deps (list (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.7") (features (quote ("os-ext"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1izpcffcij1v844x05lf7g0bnky7nzlqhnhrbz7mc3rf447vm9yf") (features (quote (("mio-evented" "mio") ("async-tokio" "futures" "tokio" "mio-evented"))))))

(define-public crate-sysfs_gpio-0.6 (crate (name "sysfs_gpio") (vers "0.6.1") (deps (list (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8") (features (quote ("os-ext"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0ms149rl90qhx9c3kw0nspf3apr36sbwbcjrvbj6qngbpyyckycy") (features (quote (("mio-evented" "mio") ("async-tokio" "futures" "tokio" "mio-evented"))))))

(define-public crate-sysfs_gpio-0.6 (crate (name "sysfs_gpio") (vers "0.6.2") (deps (list (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8") (features (quote ("os-ext"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "05symfiy6whlzayy40v84f2ibmddm358p0zp5v36arcjpiaqr078") (features (quote (("mio-evented" "mio") ("async-tokio" "futures" "tokio" "mio-evented"))))))

(define-public crate-sysfs_input-0.0.0 (crate (name "sysfs_input") (vers "0.0.0") (hash "02vz61a6sghxqbxyq1aiy902anq2v2jcb4f4qi6ba5q7ahwmgbcl")))

(define-public crate-sysfunc-blockcipher-xtea-0.1 (crate (name "sysfunc-blockcipher-xtea") (vers "0.1.0") (hash "1dh6az0536kqwgx7y5fga036bddqngbm23xaz0hnaraqgadvr126")))

(define-public crate-sysfunc-blockcipher-xtea-0.1 (crate (name "sysfunc-blockcipher-xtea") (vers "0.1.1") (hash "1z69r89wvgm1dzxrpv4rb7dxmrncljnjh1fy96qdx6ya40rg6zi7")))

(define-public crate-sysfunc-byteorder-0.1 (crate (name "sysfunc-byteorder") (vers "0.1.0") (hash "166lnpn3225aami5fmkqicrmjpsk48mn9gqyc9flbb84zkz4in9s") (features (quote (("no-core") ("enable-128") ("default")))) (yanked #t)))

(define-public crate-sysfunc-byteorder-0.1 (crate (name "sysfunc-byteorder") (vers "0.1.1") (hash "0g5xyz4hgkq6rdx4yk0p0p020ay1nf4kym74drf2s60yrcipkh2i") (features (quote (("no-core") ("enable-128") ("default")))) (yanked #t)))

(define-public crate-sysfunc-byteorder-0.1 (crate (name "sysfunc-byteorder") (vers "0.1.2") (hash "1ws3m7k67wjyahj2gnlhzkij6rk1zzrngxw9qapybkg5kq7d7nhp") (features (quote (("no-core") ("force-conversion") ("enable-128") ("default"))))))

(define-public crate-sysfunc-dynamac-transmute-0.1 (crate (name "sysfunc-dynamac-transmute") (vers "0.1.1") (hash "0jwp1c3nrqfc22m7ha66186rxckn6sz6a9h8c292fvcck3kgaajd") (features (quote (("rust-documentation") ("no-core") ("enable-std") ("default"))))))

(define-public crate-sysfuss-0.1 (crate (name "sysfuss") (vers "0.1.0") (hash "1wjgxib5h30swc84jkg16afcslm7hii9nvby9ypygrs4msnglzak") (features (quote (("derive") ("default"))))))

(define-public crate-sysfuss-0.2 (crate (name "sysfuss") (vers "0.2.0") (hash "0icz9n87jwlkl1fhr4nmy3bcxzndf2csb49ryspl3zdkg5cdv92g") (features (quote (("derive") ("default"))))))

(define-public crate-sysfuss-0.3 (crate (name "sysfuss") (vers "0.3.0") (hash "1p0lg7zqda55jnzfxxyc0f6dzk5jw1mc8k7dyasp39hijm9awfzk") (features (quote (("derive") ("default"))))))

