(define-module (crates-io sy ll) #:use-module (crates-io))

(define-public crate-syllabize-es-0.1 (crate (name "syllabize-es") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "01f5kjjh0qzajqwbgr57yvxfalpi34hpii1d4glys2ysx1wfhhnw")))

(define-public crate-syllabize-es-0.2 (crate (name "syllabize-es") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17297ccbc839bil4khjw8m8rc7nhynzbqs893yvibngcrkzlqjrf")))

(define-public crate-syllabize-es-0.3 (crate (name "syllabize-es") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0ci1s4n6x1wqwwda8ff9mx1dns8rxdqn8wkbvqgry1n8kfpj266y")))

(define-public crate-syllabize-es-0.3 (crate (name "syllabize-es") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vxvz3cpk5wqlfmg1nc64glxw108a67vmkmvnxhmlbx54wb499gw")))

(define-public crate-syllabize-es-0.3 (crate (name "syllabize-es") (vers "0.3.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1j7bsgqg15mjdjacga9gp09v096qh7fgln78l5bs482c5cv12r3l")))

(define-public crate-syllabize-es-0.3 (crate (name "syllabize-es") (vers "0.3.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1iydvbpykiqc1pws84ck00r3q29rdjg8nj6biv1j1d3zdddg4wkn")))

(define-public crate-syllabize-es-0.4 (crate (name "syllabize-es") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1bc0pi4iq02vmkqnqh3v1j5zf984yzc3mdjvv82cwwx7kmzf4khd")))

(define-public crate-syllabize-es-0.4 (crate (name "syllabize-es") (vers "0.4.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0rkxbx9q5536ddg4h9rgcd6wnnjpixrhziv2w8v1m209lzagqsal")))

(define-public crate-syllabize-es-0.4 (crate (name "syllabize-es") (vers "0.4.2") (hash "1g4rf3s8njxz3v0k9amwdc8yv237xf6p2yrx4y78wzq2zlj9n6xi")))

(define-public crate-syllabize-es-0.4 (crate (name "syllabize-es") (vers "0.4.3") (hash "1rvlda0i9qhfrraiyxsa3x3dzm53j05axczi6p34gy5903yrfa52")))

(define-public crate-syllabize-es-0.4 (crate (name "syllabize-es") (vers "0.4.4") (hash "1yzgj8r16siiipjb1hf9gjcl7lnif7950i7jgcrgj9801khh9g9m")))

(define-public crate-syllabize-es-0.4 (crate (name "syllabize-es") (vers "0.4.5") (hash "020gdygljcd2z2xcdb2r72h0yfmhr7kq7lgil1zmrxry91gc66m5")))

(define-public crate-syllabize-es-0.5 (crate (name "syllabize-es") (vers "0.5.0") (hash "1242ii1lyni2hqlnjlzcw1jssil1abmsbx4k29rxpb7z5530sa1h")))

(define-public crate-syllabize-es-0.5 (crate (name "syllabize-es") (vers "0.5.1") (hash "06sbkvip0n0cn8z4b7a2lrvwxmfkc1k6pc6bazndksdlxg4ygmz2")))

(define-public crate-syllabize-es-0.5 (crate (name "syllabize-es") (vers "0.5.2") (hash "086jgvi3f591qx3c4s83x5hz7cdkg94jjipnax7npf8x7yi8n6lx")))

(define-public crate-syllable-0.1 (crate (name "syllable") (vers "0.1.0") (hash "006j2zlxln1g7dwjm93drhqfcqm7140mzlxnj75j03f8yiqygrkv")))

(define-public crate-syllarust-0.1 (crate (name "syllarust") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "1y1qgn5a0d669v30zjix1j9q1c2skc1na9rdznlqfixcg148ag5a")))

(define-public crate-syllarust-0.1 (crate (name "syllarust") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "047xaf75gla6yczc57px3ximvn6kj1qnsfci5mjy3z8zp1lhbzf9")))

(define-public crate-syllogism-0.1 (crate (name "syllogism") (vers "0.1.0") (hash "08g0j2nwc37f32fbvs2yi220k7br83k4352laxp1zr2adgpksgag")))

(define-public crate-syllogism-0.1 (crate (name "syllogism") (vers "0.1.1") (hash "10vsmrcd58rvv1z1w3qzncy62cjzh7qql1x23bp9aa7k911kkv26")))

(define-public crate-syllogism-0.1 (crate (name "syllogism") (vers "0.1.2") (hash "1hm5yg1w9g09igv4ilk8gvv95qglfp78hm5w929p626ihckz5gw7")))

(define-public crate-syllogism-0.1 (crate (name "syllogism") (vers "0.1.3") (hash "1bvj4df7m2yj7fc0pzzslr76fgqcppiqh06w1nfgi887ihr6vs21")))

(define-public crate-syllogism-macro-0.1 (crate (name "syllogism-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syllogism") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (default-features #t) (kind 0)))) (hash "06aw53jzb975n50y9c7295x06bwgxslmjrcxxlmch6lqcfr8b5rq")))

(define-public crate-syllogism-macro-0.1 (crate (name "syllogism-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syllogism") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (default-features #t) (kind 0)))) (hash "150qg5z5scpqb1nmg4fgaz5g627k9yny0156b9l089mvprhxkks4")))

