(define-module (crates-io sy nq) #:use-module (crates-io))

(define-public crate-synqueue-0.1 (crate (name "synqueue") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "loom") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "profiling") (req "^1") (default-features #t) (kind 0)))) (hash "0i0m5m6fb00ry06zglr345ijn56v3pq9qwi0qdcl81a01xpqkg4d")))

