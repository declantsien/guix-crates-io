(define-module (crates-io sy sq) #:use-module (crates-io))

(define-public crate-sysquery-0.8 (crate (name "sysquery") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.29.10") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "16l89apgl7c82is9wl8dzqqr8lif2kh4h776w4q3fllb7k4x2bxg")))

