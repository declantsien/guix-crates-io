(define-module (crates-io sy la) #:use-module (crates-io))

(define-public crate-sylasteven-0.1 (crate (name "sylasteven") (vers "0.1.0") (deps (list (crate-dep (name "layer-system") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "15j1v6m4zk4i8gy9i4y95f9mwkkpzpymkrn8dvsbjvz6yngmf20n")))

(define-public crate-sylasteven-0.1 (crate (name "sylasteven") (vers "0.1.1") (deps (list (crate-dep (name "layer-system") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0h49j320g4hgiw5hfnxccdjvj0xy1vbgwfvbf3bkg8f4w16myzk7")))

(define-public crate-sylasteven-system-input-default-0.1 (crate (name "sylasteven-system-input-default") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "sylasteven") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zp653kq32nwzzjnijk8d60iz6bvlg6bgpc27asagf1bsbwsrbri")))

(define-public crate-sylasteven-system-pns-0.1 (crate (name "sylasteven-system-pns") (vers "0.1.0") (deps (list (crate-dep (name "pns") (req "^0.5.1") (features (quote ("static-build"))) (default-features #t) (kind 0)) (crate-dep (name "sylasteven") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zkciy38zpb3ilxn5g0vyr3smzhari17xjafxlf71wwzcq8pc09n")))

(define-public crate-sylasteven-system-pns-0.1 (crate (name "sylasteven-system-pns") (vers "0.1.1") (deps (list (crate-dep (name "pns") (req "^0.5.1") (features (quote ("static-build"))) (default-features #t) (kind 0)) (crate-dep (name "sylasteven") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0925ihspv32by6f2pc35wqq0sx41mqvbhswm80ayidh3h935avq0")))

(define-public crate-sylasteven-system-pns-0.1 (crate (name "sylasteven-system-pns") (vers "0.1.2") (deps (list (crate-dep (name "pns") (req "^0.5.2") (features (quote ("static-build"))) (default-features #t) (kind 0)) (crate-dep (name "sylasteven") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "06kmva344x4zy6c0bji55kv18j0m8i0pyk9k97ag0352w4s3g53n")))

(define-public crate-sylasteven-system-ui-nanovg-0.1 (crate (name "sylasteven-system-ui-nanovg") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nanovg") (req "^1.0.2") (features (quote ("gl3"))) (default-features #t) (kind 0)) (crate-dep (name "sylasteven") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "02p4zyghpjrf466li5smcbk96smcpsv79qpvk62jbr83biic6y3g")))

(define-public crate-sylasteven-system-ui-nanovg-0.1 (crate (name "sylasteven-system-ui-nanovg") (vers "0.1.1") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nanovg") (req "^1.0.2") (features (quote ("gl3"))) (default-features #t) (kind 0)) (crate-dep (name "sylasteven") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1skdl64px8dxnysrgj3m9hn6l24pf81qnzh7s8g2n3ggchp4k3qm")))

